﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AuthUIHandler : MonoBehaviour {

	public GameObject mainMenu;
	public GameObject loginForm;
	public GameObject signUpForm;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SignUp() {
		mainMenu.SetActive(false);
		signUpForm.SetActive(true);
	}

	public void Login() {
		mainMenu.SetActive(false);
		loginForm.SetActive(true);
	}
}
