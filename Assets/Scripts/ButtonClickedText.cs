﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonClickedText : MonoBehaviour {

	public Vector3 posDelta = new Vector3(0, 0.1f, 0);
	public float alphaDelta = 0.5f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = transform.position + posDelta * Time.deltaTime;

		GetComponent<Text>().color = new Color(
			GetComponent<Text>().color.r, 
			GetComponent<Text>().color.g, 
			GetComponent<Text>().color.b, 
			GetComponent<Text>().color.a - alphaDelta
		);
		
	}
}
