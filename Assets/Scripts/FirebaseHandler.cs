﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase;
using Firebase.Unity.Editor;
using Firebase.Auth;
using Firebase.Database;

public class FirebaseHandler : MonoBehaviour {

	public static Firebase.Auth.FirebaseAuth auth;
	DatabaseReference reference;
	string passwordInput;
	string emailInput;
	string usernameInput;
	string DEFAULT_EMAIL = "zac@gmail.com";
	string DEFAULT_PASS = "Dash2233";
	public static string username = "";

	// Use this for initialization
	void Start () {

		emailInput = DEFAULT_EMAIL;
		passwordInput = DEFAULT_PASS;

		if (auth == null) {
			setUpAuth();
		}

	} 
	
	// Update is called once per frame
	void Update () {
		
	}

	public static void setUpAuth() {
		auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
	}

	public void Login() {
		print(emailInput + ", " + passwordInput);
		auth.SignInWithEmailAndPasswordAsync(emailInput, passwordInput).ContinueWith(task => {
			if (task.IsCanceled) {
				Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
				return;
			}
			if (task.IsFaulted) {
				Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
				return;
			}

			Firebase.Auth.FirebaseUser newUser = task.Result;
			Debug.LogFormat("User signed in successfully: {0} ({1})",
				newUser.DisplayName, newUser.UserId);

			Application.LoadLevel("Game");

		});	
	}

	public void SignUp() {
		auth.CreateUserWithEmailAndPasswordAsync(emailInput, passwordInput).ContinueWith(task => {
			if (task.IsCanceled) {
				Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
				return;
			}
			if (task.IsFaulted) {
				Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
				return;
			}

			// Firebase user has been created.
			Firebase.Auth.FirebaseUser newUser = task.Result;
			Debug.LogFormat("Firebase user created successfully: {0} ({1})",
				newUser.DisplayName, newUser.UserId);

			AddUserData(usernameInput);
		});
	}

	public void AddUserData(string username) {
		Firebase.Auth.FirebaseUser user = auth.CurrentUser;
		if (user != null) {
			string uid = user.UserId;
			User u = new User(username, uid);
			string json = JsonUtility.ToJson(u);
			// Set up the Editor before calling into the realtime database.
			FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://thebutton-a0a08.firebaseio.com/");
			// Get the root reference location of the database.
			reference = FirebaseDatabase.DefaultInstance.RootReference;
			reference.Child("users").Child(uid).SetRawJsonValueAsync(json);

			Application.LoadLevel("Game");
		}
	}

	public static void getUsername() {
		//Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
		Firebase.Auth.FirebaseUser user = auth.CurrentUser;
		// Set up the Editor before calling into the realtime database.
		FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://thebutton-a0a08.firebaseio.com/");
		// Get the root reference location of the database.
		DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;

		FirebaseDatabase.DefaultInstance
		.GetReference("/users/"+(string)user.UserId+"/username")
		.GetValueAsync().ContinueWith(task => {
			if (task.IsFaulted) {
				
			}
			else if (task.IsCompleted) {
				DataSnapshot snapshot = task.Result;
				username = (string)snapshot.Value;
				print("Username: " + username);
			}
		});
	}

	public void onEmailChange(GameObject input) {
		string val = input.GetComponent<InputField>().text;
		this.emailInput = val;
	}

	public void onUsernameChange(GameObject input) {
		string val = input.GetComponent<InputField>().text;
		this.usernameInput = val;
	}

	public void onPasswordChange(GameObject input) {
		string val = input.GetComponent<InputField>().text;
		this.passwordInput = val;
	}
}

public class User {
    public string username;
    public string email;

    public User() {
    }

    public User(string username, string email) {
        this.username = username;
        this.email = email;
    }
}
