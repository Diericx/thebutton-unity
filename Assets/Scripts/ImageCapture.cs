﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class ImageCapture : MonoBehaviour {
	WebCamTexture webCamTexture;
	public bool stop = false;
	int photoCount = 0;
	// Use this for initialization
	void Start () {
		GetComponent<Image>().preserveAspect = true;
		webCamTexture = new WebCamTexture();
        GetComponent<Image>().material.mainTexture = webCamTexture;
        webCamTexture.Play();
	}
	
	// Update is called once per frame
	void Update () {
		if (stop) {
			if (webCamTexture.isPlaying)
				webCamTexture.Stop();
		} else {
			if (!webCamTexture.isPlaying) {
				webCamTexture.Play();
			}
				
		}
	}

	public byte[] TakePhoto()
    {

    // NOTE - you almost certainly have to do this here:

     //yield return new WaitForEndOfFrame(); 

    // it's a rare case where the Unity doco is pretty clear,
    // http://docs.unity3d.com/ScriptReference/WaitForEndOfFrame.html
    // be sure to scroll down to the SECOND long example on that doco page 


        Texture2D photo = new Texture2D(webCamTexture.width, webCamTexture.height);
        photo.SetPixels(webCamTexture.GetPixels());
        photo.Apply();
		return photo.EncodeToJPG(30);

        //Encode to a PNG
        //byte[] bytes = photo.EncodeToPNG();
		//Return image
		//return bytes;
        //Write out the PNG. Of course you have to substitute your_path for something sensible
        //File.WriteAllBytes("/Users/Zac/Documents/2016/" + "photo.png", bytes);
    }
}
