﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PubNubMessaging.Core;
using UnityEngine.UI;
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Unity.Editor;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;
public class PubnubHandler : MonoBehaviour {

	Pubnub pubnub;
	public GameObject textPrefab;
	public GameObject canvas;
	public GameObject theButton;
	public RectTransform theButtonRect;
	public List<GameObject> fireworksPrefabs;
	//fireworks vars
	public GameObject blackoutCube;
	public GameObject winnerTxtObj;
	public GameObject imageCaptureHolderObj;
	public GameObject winnerPhotoObj;
	private float showWinnerTime = 0f;
	private float showWinnerTimeMax = 5f;
	private float minSpawnTime = 0.5f;
    private float maxSpawnTime = 1.0f;
	private float timer = 0.0f;
	private float nextTime;
	private string winner = "";
	//Firebase 
	Firebase.Auth.FirebaseAuth auth;
	Firebase.Auth.FirebaseUser user;
	// Use this for initialization
	void Start () {

		pubnub = new Pubnub( "pub-c-9598bf00-2785-41d4-ad2f-d2362b2738d9", "sub-c-8a0a7138-e751-11e6-94bb-0619f8945a4f");
		FirebaseHandler.setUpAuth();
		FirebaseHandler.getUsername();

		pubnub.Subscribe<string>(
			"global", 
			DisplaySubscribeReturnMessage, 
			DisplaySubscribeConnectStatusMessage, 
			DisplayErrorMessage); 
		
		theButtonRect = theButton.GetComponent<RectTransform>();

		nextTime = Random.Range(minSpawnTime, maxSpawnTime); 
	}
	
	// Update is called once per frame
	void Update () {
		if (showWinnerTime > 0) {
			if (!blackoutCube.activeSelf) {
				blackoutCube.SetActive(true);
			}
			if (!winnerTxtObj.activeSelf) {
				winnerTxtObj.SetActive(true);
				winnerTxtObj.GetComponent<Text>().text = winner + " Won!";
			}

			showWinnerTime -= Time.deltaTime;

			timer += Time.deltaTime;

			// if (timer > nextTime && showWinnerTime > showWinnerTimeMax/4) {
				
			// 	Vector3 pos = new Vector3(Random.value, Random.value, 1);
			// 	pos = Camera.main.ViewportToWorldPoint(pos);
	
			// 	int randPref = (int)Random.Range(0,fireworksPrefabs.Count);
			// 	Instantiate(fireworksPrefabs[randPref], pos, Quaternion.identity);
							
			// 	timer = 0.0f;
			// 	nextTime = Random.Range(minSpawnTime, maxSpawnTime);

			// }
		} else {
			if (blackoutCube.activeSelf) {
				blackoutCube.SetActive(false);
			}
			if (winnerTxtObj.activeSelf) {
				winnerTxtObj.SetActive(false);
			}
		}
	}

	void DisplaySubscribeConnectStatusMessage(string connectMessage)
	{
		// pubnub.Publish<string>(
		// 	"global", 
		// 	"Hello from the PubNub C# SDK", 
		// 	DisplayReturnMessage, 
		// 	DisplayErrorMessage); 
	}
	
	void DisplaySubscribeReturnMessage(string result) {
		if (!string.IsNullOrEmpty(result) && !string.IsNullOrEmpty(result.Trim()))
				{
					List<object> deserializedMessage = pubnub.JsonPluggableLibrary.DeserializeToListOfObject(result);
					if (deserializedMessage != null && deserializedMessage.Count > 0)
					{
						object subscribedObject = (object)deserializedMessage[0];
						if (subscribedObject != null)
						{
							Dictionary<string, System.Object> packet = new Dictionary<string, System.Object>();
							
							try {
								packet = (Dictionary<string, System.Object>)subscribedObject;
							} catch (System.Exception e) {
								Debug.LogException(e);
							}

							string action = (string)packet["action"];

							if (action == "button-pressed") {
								DisplayButtonPressAnimation((string)packet["name"]);
							} else if (action == "win") {
								print((string)packet["name"] + "JUST WON!");
								DisplayWinAnimation((string)packet["name"]);
								if (FirebaseHandler.username == (string)packet["name"])  {
									DisplayImageCapture();
								}
							} else if (action == "photo") {
								print("GOT PHOTO");
								byte[] photoBytes = ObjectToByteArray(packet["photo"]);
								print("Photo Length: " + photoBytes.Length);
								try {
									Texture2D photoTex = new Texture2D(2, 2);
									photoTex.LoadImage(photoBytes);
									winnerPhotoObj.GetComponent<Image>().material.mainTexture = photoTex;
								} catch () {
									print("ERROR!");
								}
								print("Success!");
							}
							//IF CUSTOM OBJECT IS EXCEPTED, YOU CAN CAST THIS OBJECT TO YOUR CUSTOM CLASS TYPE
							string resultActualMessage = pubnub.JsonPluggableLibrary.SerializeToJsonString(subscribedObject);
					}
					}
				}
	}
	
	public static byte[] ObjectToByteArray(System.Object obj)
	{
		BinaryFormatter bf = new BinaryFormatter();
		using (var ms = new MemoryStream())
		{
			bf.Serialize(ms, obj);
			return ms.ToArray();
		}
	}

	void DisplayErrorMessage(PubnubClientError pubnubError)
	{
		UnityEngine.Debug.Log(pubnubError.StatusCode);
	}
	
	void DisplayReturnMessage(string result)
	{
		//UnityEngine.Debug.Log("PUBLISH STATUS CALLBACK");
		//UnityEngine.Debug.Log(result);
	}

	public void DisplayButtonPressAnimation(string name) {
		GameObject textObj = Instantiate(textPrefab, theButton.transform.position, Quaternion.identity, canvas.transform);
		textObj.transform.SetParent(canvas.transform);
		textObj.transform.localPosition = new Vector2(Random.Range(-50, 50), Random.Range(200, 215));
		//textObj.GetComponent<Rect>().x = 1;// = new Vector2(1, 1);
		//textObj.transform.position = theButton.transform.position + new Vector3(0, theButtonRect.sizeDelta.y/2, 0);
		textObj.GetComponent<Text>().text = name;
	}

	public void DisplayWinAnimation(string name) {
		// GameObject textObj = Instantiate(textPrefab, theButton.transform.position, Quaternion.identity);
		// textObj.transform.SetParent(canvas.transform);
		// textObj.transform.position = theButton.transform.position + new Vector3(0, theButtonRect.sizeDelta.y/2, 0);
		// textObj.GetComponent<TextMesh>().text = name + " Won!";
		//DisplayButtonPressAnimation(name + " Won!");
		winnerPhotoObj.SetActive(true);
		winner = name;
		showWinnerTime = showWinnerTimeMax;
		timer = nextTime;
	}

	public void DisplayImageCapture() {
		imageCaptureHolderObj.gameObject.SetActive(true);
	}

	public void TakePictureBtnCallback() {
		GameObject imgCapObj = GameObject.Find("ImageCapture_obj");
		PhotoPacket packet = new PhotoPacket();
		packet.action = "photo";
		packet.photo = imgCapObj.GetComponent<ImageCapture>().TakePhoto();
		print("Image: " + packet.photo.Length );
		pubnub.Publish<string>(
			"global", 
			packet, 
			DisplayReturnMessage, 
			DisplayErrorMessage); 
		imageCaptureHolderObj.SetActive(false);
	}

	public static void CopyStream(Stream input, Stream output)
	{
		byte[] buffer = new byte[32768];
		int read;
		while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
		{
			output.Write (buffer, 0, read);
		}
	}

	public void OnButtonPress() {
		ButtonPressPacket packet = new ButtonPressPacket();
		packet.action = "button-pressed";
		packet.name = FirebaseHandler.username;
		print(FirebaseHandler.username);
		//packet.name = "Zac Holland";
		pubnub.Publish<string>(
			"global", 
			packet, 
			DisplayReturnMessage, 
			DisplayErrorMessage); 
	}
}

public struct ButtonPressPacket {
	public string action;
	public string name;
}

public struct PhotoPacket {
	public string action;
	public byte[] photo;
}



// export default (request) => {
//     const db = require("kvstore");
//     const pubnub = require("pubnub");
    
//     if (request.message.action != "button-pressed") {
//         return request.ok();
//     }
 
//     return db.get("randomRange").then((randomRange) => {
//         // db.removeItem("randNum");
         
//          //get random number range
//         if (!randomRange) {
//             randomRange = 1000;
//             db.set("randomRange", randomRange)
//             .catch((err) => {
//                 console.log("An error occured saving the random number.", err);
//             });
//             console.log("randomRange does not exist. Creating.");
//         } else {
//             console.log("randomRange already exists:", randomRange);
//         }
        
//         //create random number for check
//         var rand = Math.floor(Math.random() * (randomRange - 0) + 0);
//         console.log("Your random number: ", rand);
        
//         //if the user wins, send the message then exit
//         if (rand == 0) {
//             randomRange = 100
            
//             request.message.action = "win";
            
//             pubnub.publish({
//                 "channel": "global",
//                 "message": request.message
//             }).then((publishResponse) => {
//                 console.log(`Publish Status: ${publishResponse[0]}:${publishResponse[1]} with TT ${publishResponse[2]}`);
//             });
            
//         }
        
//         //lower the range
//         randomRange -= 1;
//         if (randomRange < 0) {
//             randomRange = 0;
//         }
//         db.set("randomRange", randomRange)
//         .catch((err) => {
//             console.log("An error occured saving the random number.", err);
//         });
     
//         return request.ok();
//     });
// };