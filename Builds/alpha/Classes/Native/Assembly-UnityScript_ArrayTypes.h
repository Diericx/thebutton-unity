﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// ExplosionPart
struct ExplosionPart_t2473625634;
// SelfIlluminationBlink
struct SelfIlluminationBlink_t1392230251;
// MoveAnimation
struct MoveAnimation_t3061523661;
// ObjectCache
struct ObjectCache_t960934699;
// ReceiverItem
struct ReceiverItem_t169526838;
// Health
struct Health_t2683907638;
// Joystick
struct Joystick_t549888914;

#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DUnityScript_ExplosionPart2473625634.h"
#include "AssemblyU2DUnityScript_SelfIlluminationBlink1392230251.h"
#include "AssemblyU2DUnityScript_MoveAnimation3061523661.h"
#include "AssemblyU2DUnityScript_ObjectCache960934699.h"
#include "AssemblyU2DUnityScript_ReceiverItem169526838.h"
#include "AssemblyU2DUnityScript_Health2683907638.h"
#include "AssemblyU2DUnityScript_Joystick549888914.h"

#pragma once
// ExplosionPart[]
struct ExplosionPartU5BU5D_t10352919  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ExplosionPart_t2473625634 * m_Items[1];

public:
	inline ExplosionPart_t2473625634 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ExplosionPart_t2473625634 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ExplosionPart_t2473625634 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ExplosionPart_t2473625634 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ExplosionPart_t2473625634 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ExplosionPart_t2473625634 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SelfIlluminationBlink[]
struct SelfIlluminationBlinkU5BU5D_t4009522826  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SelfIlluminationBlink_t1392230251 * m_Items[1];

public:
	inline SelfIlluminationBlink_t1392230251 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SelfIlluminationBlink_t1392230251 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SelfIlluminationBlink_t1392230251 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SelfIlluminationBlink_t1392230251 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SelfIlluminationBlink_t1392230251 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SelfIlluminationBlink_t1392230251 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// MoveAnimation[]
struct MoveAnimationU5BU5D_t1691628896  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MoveAnimation_t3061523661 * m_Items[1];

public:
	inline MoveAnimation_t3061523661 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MoveAnimation_t3061523661 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MoveAnimation_t3061523661 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline MoveAnimation_t3061523661 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MoveAnimation_t3061523661 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MoveAnimation_t3061523661 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ObjectCache[]
struct ObjectCacheU5BU5D_t1759149002  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ObjectCache_t960934699 * m_Items[1];

public:
	inline ObjectCache_t960934699 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ObjectCache_t960934699 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ObjectCache_t960934699 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ObjectCache_t960934699 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ObjectCache_t960934699 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ObjectCache_t960934699 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// ReceiverItem[]
struct ReceiverItemU5BU5D_t252820531  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ReceiverItem_t169526838 * m_Items[1];

public:
	inline ReceiverItem_t169526838 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ReceiverItem_t169526838 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ReceiverItem_t169526838 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ReceiverItem_t169526838 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ReceiverItem_t169526838 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ReceiverItem_t169526838 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Health[]
struct HealthU5BU5D_t2451878963  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Health_t2683907638 * m_Items[1];

public:
	inline Health_t2683907638 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Health_t2683907638 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Health_t2683907638 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Health_t2683907638 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Health_t2683907638 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Health_t2683907638 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Joystick[]
struct JoystickU5BU5D_t1912885735  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Joystick_t549888914 * m_Items[1];

public:
	inline Joystick_t549888914 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Joystick_t549888914 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Joystick_t549888914 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Joystick_t549888914 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Joystick_t549888914 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Joystick_t549888914 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
