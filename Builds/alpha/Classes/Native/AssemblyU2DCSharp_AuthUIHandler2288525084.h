﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AuthUIHandler
struct  AuthUIHandler_t2288525084  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject AuthUIHandler::mainMenu
	GameObject_t1756533147 * ___mainMenu_2;
	// UnityEngine.GameObject AuthUIHandler::loginForm
	GameObject_t1756533147 * ___loginForm_3;
	// UnityEngine.GameObject AuthUIHandler::signUpForm
	GameObject_t1756533147 * ___signUpForm_4;

public:
	inline static int32_t get_offset_of_mainMenu_2() { return static_cast<int32_t>(offsetof(AuthUIHandler_t2288525084, ___mainMenu_2)); }
	inline GameObject_t1756533147 * get_mainMenu_2() const { return ___mainMenu_2; }
	inline GameObject_t1756533147 ** get_address_of_mainMenu_2() { return &___mainMenu_2; }
	inline void set_mainMenu_2(GameObject_t1756533147 * value)
	{
		___mainMenu_2 = value;
		Il2CppCodeGenWriteBarrier(&___mainMenu_2, value);
	}

	inline static int32_t get_offset_of_loginForm_3() { return static_cast<int32_t>(offsetof(AuthUIHandler_t2288525084, ___loginForm_3)); }
	inline GameObject_t1756533147 * get_loginForm_3() const { return ___loginForm_3; }
	inline GameObject_t1756533147 ** get_address_of_loginForm_3() { return &___loginForm_3; }
	inline void set_loginForm_3(GameObject_t1756533147 * value)
	{
		___loginForm_3 = value;
		Il2CppCodeGenWriteBarrier(&___loginForm_3, value);
	}

	inline static int32_t get_offset_of_signUpForm_4() { return static_cast<int32_t>(offsetof(AuthUIHandler_t2288525084, ___signUpForm_4)); }
	inline GameObject_t1756533147 * get_signUpForm_4() const { return ___signUpForm_4; }
	inline GameObject_t1756533147 ** get_address_of_signUpForm_4() { return &___signUpForm_4; }
	inline void set_signUpForm_4(GameObject_t1756533147 * value)
	{
		___signUpForm_4 = value;
		Il2CppCodeGenWriteBarrier(&___signUpForm_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
