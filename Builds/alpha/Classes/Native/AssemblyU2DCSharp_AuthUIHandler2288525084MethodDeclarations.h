﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AuthUIHandler
struct AuthUIHandler_t2288525084;

#include "codegen/il2cpp-codegen.h"

// System.Void AuthUIHandler::.ctor()
extern "C"  void AuthUIHandler__ctor_m2373035075 (AuthUIHandler_t2288525084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AuthUIHandler::Start()
extern "C"  void AuthUIHandler_Start_m3216786487 (AuthUIHandler_t2288525084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AuthUIHandler::Update()
extern "C"  void AuthUIHandler_Update_m183406206 (AuthUIHandler_t2288525084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AuthUIHandler::SignUp()
extern "C"  void AuthUIHandler_SignUp_m2086662865 (AuthUIHandler_t2288525084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AuthUIHandler::Login()
extern "C"  void AuthUIHandler_Login_m4049687378 (AuthUIHandler_t2288525084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
