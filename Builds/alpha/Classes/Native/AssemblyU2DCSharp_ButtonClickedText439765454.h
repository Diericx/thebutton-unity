﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonClickedText
struct  ButtonClickedText_t439765454  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 ButtonClickedText::posDelta
	Vector3_t2243707580  ___posDelta_2;
	// System.Single ButtonClickedText::alphaDelta
	float ___alphaDelta_3;

public:
	inline static int32_t get_offset_of_posDelta_2() { return static_cast<int32_t>(offsetof(ButtonClickedText_t439765454, ___posDelta_2)); }
	inline Vector3_t2243707580  get_posDelta_2() const { return ___posDelta_2; }
	inline Vector3_t2243707580 * get_address_of_posDelta_2() { return &___posDelta_2; }
	inline void set_posDelta_2(Vector3_t2243707580  value)
	{
		___posDelta_2 = value;
	}

	inline static int32_t get_offset_of_alphaDelta_3() { return static_cast<int32_t>(offsetof(ButtonClickedText_t439765454, ___alphaDelta_3)); }
	inline float get_alphaDelta_3() const { return ___alphaDelta_3; }
	inline float* get_address_of_alphaDelta_3() { return &___alphaDelta_3; }
	inline void set_alphaDelta_3(float value)
	{
		___alphaDelta_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
