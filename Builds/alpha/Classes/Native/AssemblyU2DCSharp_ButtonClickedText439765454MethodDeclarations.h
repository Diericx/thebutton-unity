﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonClickedText
struct ButtonClickedText_t439765454;

#include "codegen/il2cpp-codegen.h"

// System.Void ButtonClickedText::.ctor()
extern "C"  void ButtonClickedText__ctor_m3397148327 (ButtonClickedText_t439765454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickedText::Start()
extern "C"  void ButtonClickedText_Start_m290369811 (ButtonClickedText_t439765454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickedText::Update()
extern "C"  void ButtonClickedText_Update_m3264841692 (ButtonClickedText_t439765454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
