﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonPressPacket
struct  ButtonPressPacket_t3076793663 
{
public:
	// System.String ButtonPressPacket::action
	String_t* ___action_0;
	// System.String ButtonPressPacket::name
	String_t* ___name_1;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(ButtonPressPacket_t3076793663, ___action_0)); }
	inline String_t* get_action_0() const { return ___action_0; }
	inline String_t** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(String_t* value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier(&___action_0, value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(ButtonPressPacket_t3076793663, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of ButtonPressPacket
struct ButtonPressPacket_t3076793663_marshaled_pinvoke
{
	char* ___action_0;
	char* ___name_1;
};
// Native definition for COM marshalling of ButtonPressPacket
struct ButtonPressPacket_t3076793663_marshaled_com
{
	Il2CppChar* ___action_0;
	Il2CppChar* ___name_1;
};
