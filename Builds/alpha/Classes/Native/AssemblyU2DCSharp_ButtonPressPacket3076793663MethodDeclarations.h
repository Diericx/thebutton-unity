﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonPressPacket
struct ButtonPressPacket_t3076793663;
struct ButtonPressPacket_t3076793663_marshaled_pinvoke;
struct ButtonPressPacket_t3076793663_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct ButtonPressPacket_t3076793663;
struct ButtonPressPacket_t3076793663_marshaled_pinvoke;

extern "C" void ButtonPressPacket_t3076793663_marshal_pinvoke(const ButtonPressPacket_t3076793663& unmarshaled, ButtonPressPacket_t3076793663_marshaled_pinvoke& marshaled);
extern "C" void ButtonPressPacket_t3076793663_marshal_pinvoke_back(const ButtonPressPacket_t3076793663_marshaled_pinvoke& marshaled, ButtonPressPacket_t3076793663& unmarshaled);
extern "C" void ButtonPressPacket_t3076793663_marshal_pinvoke_cleanup(ButtonPressPacket_t3076793663_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ButtonPressPacket_t3076793663;
struct ButtonPressPacket_t3076793663_marshaled_com;

extern "C" void ButtonPressPacket_t3076793663_marshal_com(const ButtonPressPacket_t3076793663& unmarshaled, ButtonPressPacket_t3076793663_marshaled_com& marshaled);
extern "C" void ButtonPressPacket_t3076793663_marshal_com_back(const ButtonPressPacket_t3076793663_marshaled_com& marshaled, ButtonPressPacket_t3076793663& unmarshaled);
extern "C" void ButtonPressPacket_t3076793663_marshal_com_cleanup(ButtonPressPacket_t3076793663_marshaled_com& marshaled);
