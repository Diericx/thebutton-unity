﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CodeBasedAssertionExample
struct CodeBasedAssertionExample_t389301950;

#include "codegen/il2cpp-codegen.h"

// System.Void CodeBasedAssertionExample::.ctor()
extern "C"  void CodeBasedAssertionExample__ctor_m1225462585 (CodeBasedAssertionExample_t389301950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CodeBasedAssertionExample::Awake()
extern "C"  void CodeBasedAssertionExample_Awake_m3600740666 (CodeBasedAssertionExample_t389301950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
