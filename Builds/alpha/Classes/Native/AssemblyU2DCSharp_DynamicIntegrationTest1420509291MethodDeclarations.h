﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DynamicIntegrationTest
struct DynamicIntegrationTest_t1420509291;

#include "codegen/il2cpp-codegen.h"

// System.Void DynamicIntegrationTest::.ctor()
extern "C"  void DynamicIntegrationTest__ctor_m2033292672 (DynamicIntegrationTest_t1420509291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicIntegrationTest::Start()
extern "C"  void DynamicIntegrationTest_Start_m1617973836 (DynamicIntegrationTest_t1420509291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
