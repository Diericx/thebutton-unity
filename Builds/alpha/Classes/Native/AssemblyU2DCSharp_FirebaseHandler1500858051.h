﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Firebase.Auth.FirebaseAuth
struct FirebaseAuth_t3105883899;
// Firebase.Database.DatabaseReference
struct DatabaseReference_t1167676104;
// System.String
struct String_t;
// System.Action`1<System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>>
struct Action_1_t2968794991;
// System.Action`1<System.Threading.Tasks.Task`1<Firebase.Database.DataSnapshot>>
struct Action_1_t209723739;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FirebaseHandler
struct  FirebaseHandler_t1500858051  : public MonoBehaviour_t1158329972
{
public:
	// Firebase.Database.DatabaseReference FirebaseHandler::reference
	DatabaseReference_t1167676104 * ___reference_3;
	// System.String FirebaseHandler::passwordInput
	String_t* ___passwordInput_4;
	// System.String FirebaseHandler::emailInput
	String_t* ___emailInput_5;
	// System.String FirebaseHandler::usernameInput
	String_t* ___usernameInput_6;
	// System.String FirebaseHandler::DEFAULT_EMAIL
	String_t* ___DEFAULT_EMAIL_7;
	// System.String FirebaseHandler::DEFAULT_PASS
	String_t* ___DEFAULT_PASS_8;

public:
	inline static int32_t get_offset_of_reference_3() { return static_cast<int32_t>(offsetof(FirebaseHandler_t1500858051, ___reference_3)); }
	inline DatabaseReference_t1167676104 * get_reference_3() const { return ___reference_3; }
	inline DatabaseReference_t1167676104 ** get_address_of_reference_3() { return &___reference_3; }
	inline void set_reference_3(DatabaseReference_t1167676104 * value)
	{
		___reference_3 = value;
		Il2CppCodeGenWriteBarrier(&___reference_3, value);
	}

	inline static int32_t get_offset_of_passwordInput_4() { return static_cast<int32_t>(offsetof(FirebaseHandler_t1500858051, ___passwordInput_4)); }
	inline String_t* get_passwordInput_4() const { return ___passwordInput_4; }
	inline String_t** get_address_of_passwordInput_4() { return &___passwordInput_4; }
	inline void set_passwordInput_4(String_t* value)
	{
		___passwordInput_4 = value;
		Il2CppCodeGenWriteBarrier(&___passwordInput_4, value);
	}

	inline static int32_t get_offset_of_emailInput_5() { return static_cast<int32_t>(offsetof(FirebaseHandler_t1500858051, ___emailInput_5)); }
	inline String_t* get_emailInput_5() const { return ___emailInput_5; }
	inline String_t** get_address_of_emailInput_5() { return &___emailInput_5; }
	inline void set_emailInput_5(String_t* value)
	{
		___emailInput_5 = value;
		Il2CppCodeGenWriteBarrier(&___emailInput_5, value);
	}

	inline static int32_t get_offset_of_usernameInput_6() { return static_cast<int32_t>(offsetof(FirebaseHandler_t1500858051, ___usernameInput_6)); }
	inline String_t* get_usernameInput_6() const { return ___usernameInput_6; }
	inline String_t** get_address_of_usernameInput_6() { return &___usernameInput_6; }
	inline void set_usernameInput_6(String_t* value)
	{
		___usernameInput_6 = value;
		Il2CppCodeGenWriteBarrier(&___usernameInput_6, value);
	}

	inline static int32_t get_offset_of_DEFAULT_EMAIL_7() { return static_cast<int32_t>(offsetof(FirebaseHandler_t1500858051, ___DEFAULT_EMAIL_7)); }
	inline String_t* get_DEFAULT_EMAIL_7() const { return ___DEFAULT_EMAIL_7; }
	inline String_t** get_address_of_DEFAULT_EMAIL_7() { return &___DEFAULT_EMAIL_7; }
	inline void set_DEFAULT_EMAIL_7(String_t* value)
	{
		___DEFAULT_EMAIL_7 = value;
		Il2CppCodeGenWriteBarrier(&___DEFAULT_EMAIL_7, value);
	}

	inline static int32_t get_offset_of_DEFAULT_PASS_8() { return static_cast<int32_t>(offsetof(FirebaseHandler_t1500858051, ___DEFAULT_PASS_8)); }
	inline String_t* get_DEFAULT_PASS_8() const { return ___DEFAULT_PASS_8; }
	inline String_t** get_address_of_DEFAULT_PASS_8() { return &___DEFAULT_PASS_8; }
	inline void set_DEFAULT_PASS_8(String_t* value)
	{
		___DEFAULT_PASS_8 = value;
		Il2CppCodeGenWriteBarrier(&___DEFAULT_PASS_8, value);
	}
};

struct FirebaseHandler_t1500858051_StaticFields
{
public:
	// Firebase.Auth.FirebaseAuth FirebaseHandler::auth
	FirebaseAuth_t3105883899 * ___auth_2;
	// System.String FirebaseHandler::username
	String_t* ___username_9;
	// System.Action`1<System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>> FirebaseHandler::<>f__am$cache0
	Action_1_t2968794991 * ___U3CU3Ef__amU24cache0_10;
	// System.Action`1<System.Threading.Tasks.Task`1<Firebase.Database.DataSnapshot>> FirebaseHandler::<>f__am$cache1
	Action_1_t209723739 * ___U3CU3Ef__amU24cache1_11;

public:
	inline static int32_t get_offset_of_auth_2() { return static_cast<int32_t>(offsetof(FirebaseHandler_t1500858051_StaticFields, ___auth_2)); }
	inline FirebaseAuth_t3105883899 * get_auth_2() const { return ___auth_2; }
	inline FirebaseAuth_t3105883899 ** get_address_of_auth_2() { return &___auth_2; }
	inline void set_auth_2(FirebaseAuth_t3105883899 * value)
	{
		___auth_2 = value;
		Il2CppCodeGenWriteBarrier(&___auth_2, value);
	}

	inline static int32_t get_offset_of_username_9() { return static_cast<int32_t>(offsetof(FirebaseHandler_t1500858051_StaticFields, ___username_9)); }
	inline String_t* get_username_9() const { return ___username_9; }
	inline String_t** get_address_of_username_9() { return &___username_9; }
	inline void set_username_9(String_t* value)
	{
		___username_9 = value;
		Il2CppCodeGenWriteBarrier(&___username_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_10() { return static_cast<int32_t>(offsetof(FirebaseHandler_t1500858051_StaticFields, ___U3CU3Ef__amU24cache0_10)); }
	inline Action_1_t2968794991 * get_U3CU3Ef__amU24cache0_10() const { return ___U3CU3Ef__amU24cache0_10; }
	inline Action_1_t2968794991 ** get_address_of_U3CU3Ef__amU24cache0_10() { return &___U3CU3Ef__amU24cache0_10; }
	inline void set_U3CU3Ef__amU24cache0_10(Action_1_t2968794991 * value)
	{
		___U3CU3Ef__amU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_11() { return static_cast<int32_t>(offsetof(FirebaseHandler_t1500858051_StaticFields, ___U3CU3Ef__amU24cache1_11)); }
	inline Action_1_t209723739 * get_U3CU3Ef__amU24cache1_11() const { return ___U3CU3Ef__amU24cache1_11; }
	inline Action_1_t209723739 ** get_address_of_U3CU3Ef__amU24cache1_11() { return &___U3CU3Ef__amU24cache1_11; }
	inline void set_U3CU3Ef__amU24cache1_11(Action_1_t209723739 * value)
	{
		___U3CU3Ef__amU24cache1_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
