﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FirebaseHandler
struct FirebaseHandler_t1500858051;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>
struct Task_1_t3166995609;
// System.Threading.Tasks.Task`1<Firebase.Database.DataSnapshot>
struct Task_1_t407924357;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void FirebaseHandler::.ctor()
extern "C"  void FirebaseHandler__ctor_m1961374258 (FirebaseHandler_t1500858051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirebaseHandler::Start()
extern "C"  void FirebaseHandler_Start_m2357722270 (FirebaseHandler_t1500858051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirebaseHandler::Update()
extern "C"  void FirebaseHandler_Update_m3327569543 (FirebaseHandler_t1500858051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirebaseHandler::setUpAuth()
extern "C"  void FirebaseHandler_setUpAuth_m426295653 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirebaseHandler::Login()
extern "C"  void FirebaseHandler_Login_m3985246073 (FirebaseHandler_t1500858051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirebaseHandler::SignUp()
extern "C"  void FirebaseHandler_SignUp_m3489182576 (FirebaseHandler_t1500858051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirebaseHandler::AddUserData(System.String)
extern "C"  void FirebaseHandler_AddUserData_m2711538944 (FirebaseHandler_t1500858051 * __this, String_t* ___username0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirebaseHandler::getUsername()
extern "C"  void FirebaseHandler_getUsername_m558656606 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirebaseHandler::onEmailChange(UnityEngine.GameObject)
extern "C"  void FirebaseHandler_onEmailChange_m2138028481 (FirebaseHandler_t1500858051 * __this, GameObject_t1756533147 * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirebaseHandler::onUsernameChange(UnityEngine.GameObject)
extern "C"  void FirebaseHandler_onUsernameChange_m961081335 (FirebaseHandler_t1500858051 * __this, GameObject_t1756533147 * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirebaseHandler::onPasswordChange(UnityEngine.GameObject)
extern "C"  void FirebaseHandler_onPasswordChange_m291652658 (FirebaseHandler_t1500858051 * __this, GameObject_t1756533147 * ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirebaseHandler::.cctor()
extern "C"  void FirebaseHandler__cctor_m2927528337 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirebaseHandler::<Login>m__0(System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>)
extern "C"  void FirebaseHandler_U3CLoginU3Em__0_m1131167118 (Il2CppObject * __this /* static, unused */, Task_1_t3166995609 * ___task0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirebaseHandler::<SignUp>m__1(System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>)
extern "C"  void FirebaseHandler_U3CSignUpU3Em__1_m3552527648 (FirebaseHandler_t1500858051 * __this, Task_1_t3166995609 * ___task0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FirebaseHandler::<getUsername>m__2(System.Threading.Tasks.Task`1<Firebase.Database.DataSnapshot>)
extern "C"  void FirebaseHandler_U3CgetUsernameU3Em__2_m2301977698 (Il2CppObject * __this /* static, unused */, Task_1_t407924357 * ___task0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
