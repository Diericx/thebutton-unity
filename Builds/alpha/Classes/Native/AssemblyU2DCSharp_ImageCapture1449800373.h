﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WebCamTexture
struct WebCamTexture_t1079476942;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImageCapture
struct  ImageCapture_t1449800373  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.WebCamTexture ImageCapture::webCamTexture
	WebCamTexture_t1079476942 * ___webCamTexture_2;
	// System.Boolean ImageCapture::stop
	bool ___stop_3;
	// System.Int32 ImageCapture::photoCount
	int32_t ___photoCount_4;

public:
	inline static int32_t get_offset_of_webCamTexture_2() { return static_cast<int32_t>(offsetof(ImageCapture_t1449800373, ___webCamTexture_2)); }
	inline WebCamTexture_t1079476942 * get_webCamTexture_2() const { return ___webCamTexture_2; }
	inline WebCamTexture_t1079476942 ** get_address_of_webCamTexture_2() { return &___webCamTexture_2; }
	inline void set_webCamTexture_2(WebCamTexture_t1079476942 * value)
	{
		___webCamTexture_2 = value;
		Il2CppCodeGenWriteBarrier(&___webCamTexture_2, value);
	}

	inline static int32_t get_offset_of_stop_3() { return static_cast<int32_t>(offsetof(ImageCapture_t1449800373, ___stop_3)); }
	inline bool get_stop_3() const { return ___stop_3; }
	inline bool* get_address_of_stop_3() { return &___stop_3; }
	inline void set_stop_3(bool value)
	{
		___stop_3 = value;
	}

	inline static int32_t get_offset_of_photoCount_4() { return static_cast<int32_t>(offsetof(ImageCapture_t1449800373, ___photoCount_4)); }
	inline int32_t get_photoCount_4() const { return ___photoCount_4; }
	inline int32_t* get_address_of_photoCount_4() { return &___photoCount_4; }
	inline void set_photoCount_4(int32_t value)
	{
		___photoCount_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
