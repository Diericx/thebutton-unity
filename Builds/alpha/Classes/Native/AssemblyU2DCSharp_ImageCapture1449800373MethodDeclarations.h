﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ImageCapture
struct ImageCapture_t1449800373;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"

// System.Void ImageCapture::.ctor()
extern "C"  void ImageCapture__ctor_m1291992468 (ImageCapture_t1449800373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageCapture::Start()
extern "C"  void ImageCapture_Start_m1617569460 (ImageCapture_t1449800373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageCapture::Update()
extern "C"  void ImageCapture_Update_m1535969565 (ImageCapture_t1449800373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ImageCapture::TakePhoto()
extern "C"  ByteU5BU5D_t3397334013* ImageCapture_TakePhoto_m481505463 (ImageCapture_t1449800373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
