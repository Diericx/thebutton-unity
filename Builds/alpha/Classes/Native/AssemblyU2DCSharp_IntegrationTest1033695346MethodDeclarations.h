﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_String2029220233.h"

// System.Void IntegrationTest::Pass()
extern "C"  void IntegrationTest_Pass_m3912360736 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IntegrationTest::Pass(UnityEngine.GameObject)
extern "C"  void IntegrationTest_Pass_m145489534 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IntegrationTest::Fail(System.String)
extern "C"  void IntegrationTest_Fail_m2767016635 (Il2CppObject * __this /* static, unused */, String_t* ___reason0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IntegrationTest::Fail(UnityEngine.GameObject,System.String)
extern "C"  void IntegrationTest_Fail_m3489787443 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, String_t* ___reason1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IntegrationTest::Fail()
extern "C"  void IntegrationTest_Fail_m911796857 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IntegrationTest::Fail(UnityEngine.GameObject)
extern "C"  void IntegrationTest_Fail_m1011454941 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IntegrationTest::Assert(System.Boolean)
extern "C"  void IntegrationTest_Assert_m3986767150 (Il2CppObject * __this /* static, unused */, bool ___condition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IntegrationTest::Assert(UnityEngine.GameObject,System.Boolean)
extern "C"  void IntegrationTest_Assert_m4096372956 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, bool ___condition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IntegrationTest::Assert(System.Boolean,System.String)
extern "C"  void IntegrationTest_Assert_m3783023590 (Il2CppObject * __this /* static, unused */, bool ___condition0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IntegrationTest::Assert(UnityEngine.GameObject,System.Boolean,System.String)
extern "C"  void IntegrationTest_Assert_m2365431100 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, bool ___condition1, String_t* ___message2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IntegrationTest::LogResult(System.String)
extern "C"  void IntegrationTest_LogResult_m3824692484 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IntegrationTest::LogResult(UnityEngine.GameObject,System.String)
extern "C"  void IntegrationTest_LogResult_m2863157564 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject IntegrationTest::FindTestObject(UnityEngine.GameObject)
extern "C"  GameObject_t1756533147 * IntegrationTest_FindTestObject_m2133775528 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
