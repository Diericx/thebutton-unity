﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IntegrationTestAttribute
struct IntegrationTestAttribute_t3722261662;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void IntegrationTestAttribute::.ctor(System.String)
extern "C"  void IntegrationTestAttribute__ctor_m3095454937 (IntegrationTestAttribute_t3722261662 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IntegrationTestAttribute::IncludeOnScene(System.String)
extern "C"  bool IntegrationTestAttribute_IncludeOnScene_m3590759650 (IntegrationTestAttribute_t3722261662 * __this, String_t* ___scenePath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
