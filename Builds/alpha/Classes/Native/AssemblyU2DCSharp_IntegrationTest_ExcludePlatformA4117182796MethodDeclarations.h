﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IntegrationTest/ExcludePlatformAttribute
struct ExcludePlatformAttribute_t4117182796;
// UnityEngine.RuntimePlatform[]
struct RuntimePlatformU5BU5D_t1634724030;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"

// System.Void IntegrationTest/ExcludePlatformAttribute::.ctor(UnityEngine.RuntimePlatform[])
extern "C"  void ExcludePlatformAttribute__ctor_m999138835 (ExcludePlatformAttribute_t4117182796 * __this, RuntimePlatformU5BU5D_t1634724030* ___platformsToExclude0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String IntegrationTest/ExcludePlatformAttribute::<ExcludePlatformAttribute>m__0(UnityEngine.RuntimePlatform)
extern "C"  String_t* ExcludePlatformAttribute_U3CExcludePlatformAttributeU3Em__0_m666965694 (Il2CppObject * __this /* static, unused */, int32_t ___platform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
