﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IntegrationTest/IgnoreAttribute
struct IgnoreAttribute_t323259989;

#include "codegen/il2cpp-codegen.h"

// System.Void IntegrationTest/IgnoreAttribute::.ctor()
extern "C"  void IgnoreAttribute__ctor_m1482944438 (IgnoreAttribute_t323259989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
