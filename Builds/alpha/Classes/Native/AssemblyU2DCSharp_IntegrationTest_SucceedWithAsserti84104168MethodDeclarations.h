﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IntegrationTest/SucceedWithAssertions
struct SucceedWithAssertions_t84104168;

#include "codegen/il2cpp-codegen.h"

// System.Void IntegrationTest/SucceedWithAssertions::.ctor()
extern "C"  void SucceedWithAssertions__ctor_m1993018229 (SucceedWithAssertions_t84104168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
