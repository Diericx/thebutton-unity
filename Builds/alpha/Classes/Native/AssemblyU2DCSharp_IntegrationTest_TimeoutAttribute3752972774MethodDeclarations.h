﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IntegrationTest/TimeoutAttribute
struct TimeoutAttribute_t3752972774;

#include "codegen/il2cpp-codegen.h"

// System.Void IntegrationTest/TimeoutAttribute::.ctor(System.Single)
extern "C"  void TimeoutAttribute__ctor_m2933454222 (TimeoutAttribute_t3752972774 * __this, float ___seconds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
