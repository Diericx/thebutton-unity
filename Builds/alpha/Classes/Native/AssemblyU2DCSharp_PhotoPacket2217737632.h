﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotoPacket
struct  PhotoPacket_t2217737632 
{
public:
	// System.String PhotoPacket::action
	String_t* ___action_0;
	// System.Byte[] PhotoPacket::photo
	ByteU5BU5D_t3397334013* ___photo_1;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(PhotoPacket_t2217737632, ___action_0)); }
	inline String_t* get_action_0() const { return ___action_0; }
	inline String_t** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(String_t* value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier(&___action_0, value);
	}

	inline static int32_t get_offset_of_photo_1() { return static_cast<int32_t>(offsetof(PhotoPacket_t2217737632, ___photo_1)); }
	inline ByteU5BU5D_t3397334013* get_photo_1() const { return ___photo_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_photo_1() { return &___photo_1; }
	inline void set_photo_1(ByteU5BU5D_t3397334013* value)
	{
		___photo_1 = value;
		Il2CppCodeGenWriteBarrier(&___photo_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PhotoPacket
struct PhotoPacket_t2217737632_marshaled_pinvoke
{
	char* ___action_0;
	uint8_t* ___photo_1;
};
// Native definition for COM marshalling of PhotoPacket
struct PhotoPacket_t2217737632_marshaled_com
{
	Il2CppChar* ___action_0;
	uint8_t* ___photo_1;
};
