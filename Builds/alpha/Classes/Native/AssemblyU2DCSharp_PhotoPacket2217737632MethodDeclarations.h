﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotoPacket
struct PhotoPacket_t2217737632;
struct PhotoPacket_t2217737632_marshaled_pinvoke;
struct PhotoPacket_t2217737632_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct PhotoPacket_t2217737632;
struct PhotoPacket_t2217737632_marshaled_pinvoke;

extern "C" void PhotoPacket_t2217737632_marshal_pinvoke(const PhotoPacket_t2217737632& unmarshaled, PhotoPacket_t2217737632_marshaled_pinvoke& marshaled);
extern "C" void PhotoPacket_t2217737632_marshal_pinvoke_back(const PhotoPacket_t2217737632_marshaled_pinvoke& marshaled, PhotoPacket_t2217737632& unmarshaled);
extern "C" void PhotoPacket_t2217737632_marshal_pinvoke_cleanup(PhotoPacket_t2217737632_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct PhotoPacket_t2217737632;
struct PhotoPacket_t2217737632_marshaled_com;

extern "C" void PhotoPacket_t2217737632_marshal_com(const PhotoPacket_t2217737632& unmarshaled, PhotoPacket_t2217737632_marshaled_com& marshaled);
extern "C" void PhotoPacket_t2217737632_marshal_com_back(const PhotoPacket_t2217737632_marshaled_com& marshaled, PhotoPacket_t2217737632& unmarshaled);
extern "C" void PhotoPacket_t2217737632_marshal_com_cleanup(PhotoPacket_t2217737632_marshaled_com& marshaled);
