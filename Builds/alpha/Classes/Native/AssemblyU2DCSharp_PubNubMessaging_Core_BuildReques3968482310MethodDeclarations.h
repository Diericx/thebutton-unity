﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.BuildRequests
struct BuildRequests_t3968482310;
// System.Uri
struct Uri_t19570940;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PushTypeSer3531847941.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ResponseTyp1248099179.h"

// System.Void PubNubMessaging.Core.BuildRequests::.ctor()
extern "C"  void BuildRequests__ctor_m437494552 (BuildRequests_t3968482310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildRegisterDevicePushRequest(System.String,PubNubMessaging.Core.PushTypeService,System.String,System.String,System.Boolean,System.String,System.String,System.String)
extern "C"  Uri_t19570940 * BuildRequests_BuildRegisterDevicePushRequest_m1332398601 (Il2CppObject * __this /* static, unused */, String_t* ___channel0, int32_t ___pushType1, String_t* ___pushToken2, String_t* ___uuid3, bool ___ssl4, String_t* ___origin5, String_t* ___authenticationKey6, String_t* ___subscribeKey7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildRemoveChannelPushRequest(System.String,PubNubMessaging.Core.PushTypeService,System.String,System.String,System.Boolean,System.String,System.String,System.String)
extern "C"  Uri_t19570940 * BuildRequests_BuildRemoveChannelPushRequest_m3280515625 (Il2CppObject * __this /* static, unused */, String_t* ___channel0, int32_t ___pushType1, String_t* ___pushToken2, String_t* ___uuid3, bool ___ssl4, String_t* ___origin5, String_t* ___authenticationKey6, String_t* ___subscribeKey7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildGetChannelsPushRequest(PubNubMessaging.Core.PushTypeService,System.String,System.String,System.Boolean,System.String,System.String,System.String)
extern "C"  Uri_t19570940 * BuildRequests_BuildGetChannelsPushRequest_m1831877496 (Il2CppObject * __this /* static, unused */, int32_t ___pushType0, String_t* ___pushToken1, String_t* ___uuid2, bool ___ssl3, String_t* ___origin4, String_t* ___authenticationKey5, String_t* ___subscribeKey6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildUnregisterDevicePushRequest(PubNubMessaging.Core.PushTypeService,System.String,System.String,System.Boolean,System.String,System.String,System.String)
extern "C"  Uri_t19570940 * BuildRequests_BuildUnregisterDevicePushRequest_m2739609334 (Il2CppObject * __this /* static, unused */, int32_t ___pushType0, String_t* ___pushToken1, String_t* ___uuid2, bool ___ssl3, String_t* ___origin4, String_t* ___authenticationKey5, String_t* ___subscribeKey6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildPublishRequest(System.String,System.String,System.Boolean,System.String,System.Boolean,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.UInt32,System.Int32)
extern "C"  Uri_t19570940 * BuildRequests_BuildPublishRequest_m3551445661 (Il2CppObject * __this /* static, unused */, String_t* ___channel0, String_t* ___message1, bool ___storeInHistory2, String_t* ___uuid3, bool ___ssl4, String_t* ___origin5, String_t* ___authenticationKey6, String_t* ___publishKey7, String_t* ___subscribeKey8, String_t* ___cipherKey9, String_t* ___secretKey10, String_t* ___metadata11, uint32_t ___messageCounter12, int32_t ___ttl13, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildDetailedHistoryRequest(System.String,System.Int64,System.Int64,System.Int32,System.Boolean,System.Boolean,System.String,System.Boolean,System.String,System.String,System.String)
extern "C"  Uri_t19570940 * BuildRequests_BuildDetailedHistoryRequest_m1017793459 (Il2CppObject * __this /* static, unused */, String_t* ___channel0, int64_t ___start1, int64_t ___end2, int32_t ___count3, bool ___reverse4, bool ___includeToken5, String_t* ___uuid6, bool ___ssl7, String_t* ___origin8, String_t* ___authenticationKey9, String_t* ___subscribeKey10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildHereNowRequest(System.String,System.String,System.Boolean,System.Boolean,System.String,System.Boolean,System.String,System.String,System.String)
extern "C"  Uri_t19570940 * BuildRequests_BuildHereNowRequest_m929361920 (Il2CppObject * __this /* static, unused */, String_t* ___channel0, String_t* ___channelGroups1, bool ___showUUIDList2, bool ___includeUserState3, String_t* ___uuid4, bool ___ssl5, String_t* ___origin6, String_t* ___authenticationKey7, String_t* ___subscribeKey8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildGlobalHereNowRequest(System.Boolean,System.Boolean,System.String,System.Boolean,System.String,System.String,System.String)
extern "C"  Uri_t19570940 * BuildRequests_BuildGlobalHereNowRequest_m203893901 (Il2CppObject * __this /* static, unused */, bool ___showUUIDList0, bool ___includeUserState1, String_t* ___uuid2, bool ___ssl3, String_t* ___origin4, String_t* ___authenticationKey5, String_t* ___subscribeKey6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildWhereNowRequest(System.String,System.String,System.Boolean,System.String,System.String,System.String)
extern "C"  Uri_t19570940 * BuildRequests_BuildWhereNowRequest_m2868495319 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, String_t* ___sessionUUID1, bool ___ssl2, String_t* ___origin3, String_t* ___authenticationKey4, String_t* ___subscribeKey5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildTimeRequest(System.String,System.Boolean,System.String)
extern "C"  Uri_t19570940 * BuildRequests_BuildTimeRequest_m4234119859 (Il2CppObject * __this /* static, unused */, String_t* ___uuid0, bool ___ssl1, String_t* ___origin2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildGrantAccessRequest(System.String,System.Boolean,System.Boolean,System.Int32,System.String,System.Boolean,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  Uri_t19570940 * BuildRequests_BuildGrantAccessRequest_m998124205 (Il2CppObject * __this /* static, unused */, String_t* ___channel0, bool ___read1, bool ___write2, int32_t ___ttl3, String_t* ___uuid4, bool ___ssl5, String_t* ___origin6, String_t* ___authenticationKey7, String_t* ___publishKey8, String_t* ___subscribeKey9, String_t* ___cipherKey10, String_t* ___secretKey11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildAuditAccessRequest(System.String,System.String,System.Boolean,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  Uri_t19570940 * BuildRequests_BuildAuditAccessRequest_m2867928183 (Il2CppObject * __this /* static, unused */, String_t* ___channel0, String_t* ___uuid1, bool ___ssl2, String_t* ___origin3, String_t* ___authenticationKey4, String_t* ___publishKey5, String_t* ___subscribeKey6, String_t* ___cipherKey7, String_t* ___secretKey8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildSetUserStateRequest(System.String,System.String,System.String,System.String,System.String,System.Boolean,System.String,System.String,System.String)
extern "C"  Uri_t19570940 * BuildRequests_BuildSetUserStateRequest_m900478546 (Il2CppObject * __this /* static, unused */, String_t* ___channel0, String_t* ___channelGroup1, String_t* ___jsonUserState2, String_t* ___uuid3, String_t* ___sessionUUID4, bool ___ssl5, String_t* ___origin6, String_t* ___authenticationKey7, String_t* ___subscribeKey8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildGetUserStateRequest(System.String,System.String,System.String,System.String,System.Boolean,System.String,System.String,System.String)
extern "C"  Uri_t19570940 * BuildRequests_BuildGetUserStateRequest_m1959766770 (Il2CppObject * __this /* static, unused */, String_t* ___channel0, String_t* ___channelGroup1, String_t* ___uuid2, String_t* ___sessionUUID3, bool ___ssl4, String_t* ___origin5, String_t* ___authenticationKey6, String_t* ___subscribeKey7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildPresenceHeartbeatRequest(System.String,System.String,System.String,System.String,System.Boolean,System.String,System.String,System.String)
extern "C"  Uri_t19570940 * BuildRequests_BuildPresenceHeartbeatRequest_m2776862877 (Il2CppObject * __this /* static, unused */, String_t* ___channels0, String_t* ___channelGroups1, String_t* ___channelsJsonState2, String_t* ___uuid3, bool ___ssl4, String_t* ___origin5, String_t* ___authenticationKey6, String_t* ___subscribeKey7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildMultiChannelLeaveRequest(System.String,System.String,System.String,System.Boolean,System.String,System.String,System.String)
extern "C"  Uri_t19570940 * BuildRequests_BuildMultiChannelLeaveRequest_m1115348403 (Il2CppObject * __this /* static, unused */, String_t* ___channels0, String_t* ___channelGroups1, String_t* ___uuid2, bool ___ssl3, String_t* ___origin4, String_t* ___authenticationKey5, String_t* ___subscribeKey6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildMultiChannelSubscribeRequestV2(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.Boolean,System.String,System.String,System.String,System.Int32)
extern "C"  Uri_t19570940 * BuildRequests_BuildMultiChannelSubscribeRequestV2_m763931869 (Il2CppObject * __this /* static, unused */, String_t* ___channels0, String_t* ___channelGroups1, String_t* ___timetoken2, String_t* ___channelsJsonState3, String_t* ___uuid4, String_t* ___region5, String_t* ___filterExpr6, bool ___ssl7, String_t* ___origin8, String_t* ___authenticationKey9, String_t* ___subscribeKey10, int32_t ___presenceHeartbeat11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildAddChannelsToChannelGroupRequest(System.String[],System.String,System.String,System.String,System.Boolean,System.String,System.String,System.String)
extern "C"  Uri_t19570940 * BuildRequests_BuildAddChannelsToChannelGroupRequest_m3401488892 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___channels0, String_t* ___nameSpace1, String_t* ___groupName2, String_t* ___uuid3, bool ___ssl4, String_t* ___origin5, String_t* ___authenticationKey6, String_t* ___subscribeKey7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildRemoveChannelsFromChannelGroupRequest(System.String[],System.String,System.String,System.String,System.Boolean,System.String,System.String,System.String)
extern "C"  Uri_t19570940 * BuildRequests_BuildRemoveChannelsFromChannelGroupRequest_m3448913796 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___channels0, String_t* ___nameSpace1, String_t* ___groupName2, String_t* ___uuid3, bool ___ssl4, String_t* ___origin5, String_t* ___authenticationKey6, String_t* ___subscribeKey7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildGetChannelsForChannelGroupRequest(System.String,System.String,System.Boolean,System.String,System.Boolean,System.String,System.String,System.String)
extern "C"  Uri_t19570940 * BuildRequests_BuildGetChannelsForChannelGroupRequest_m2439615128 (Il2CppObject * __this /* static, unused */, String_t* ___nameSpace0, String_t* ___groupName1, bool ___limitToChannelGroupScopeOnly2, String_t* ___uuid3, bool ___ssl4, String_t* ___origin5, String_t* ___authenticationKey6, String_t* ___subscribeKey7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildChannelGroupAuditAccessRequest(System.String,System.String,System.Boolean,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  Uri_t19570940 * BuildRequests_BuildChannelGroupAuditAccessRequest_m337289351 (Il2CppObject * __this /* static, unused */, String_t* ___channelGroup0, String_t* ___uuid1, bool ___ssl2, String_t* ___origin3, String_t* ___authenticationKey4, String_t* ___publishKey5, String_t* ___subscribeKey6, String_t* ___cipherKey7, String_t* ___secretKey8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri PubNubMessaging.Core.BuildRequests::BuildChannelGroupGrantAccessRequest(System.String,System.Boolean,System.Boolean,System.Boolean,System.Int32,System.String,System.Boolean,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  Uri_t19570940 * BuildRequests_BuildChannelGroupGrantAccessRequest_m663126166 (Il2CppObject * __this /* static, unused */, String_t* ___channelGroup0, bool ___read1, bool ___write2, bool ___manage3, int32_t ___ttl4, String_t* ___uuid5, bool ___ssl6, String_t* ___origin7, String_t* ___authenticationKey8, String_t* ___publishKey9, String_t* ___subscribeKey10, String_t* ___cipherKey11, String_t* ___secretKey12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder PubNubMessaging.Core.BuildRequests::AppendAuthKeyToURL(System.Text.StringBuilder,System.String,PubNubMessaging.Core.ResponseType)
extern "C"  StringBuilder_t1221177846 * BuildRequests_AppendAuthKeyToURL_m2550813234 (Il2CppObject * __this /* static, unused */, StringBuilder_t1221177846 * ___url0, String_t* ___authenticationKey1, int32_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder PubNubMessaging.Core.BuildRequests::AppendUUIDToURL(System.Text.StringBuilder,System.String,System.Boolean)
extern "C"  StringBuilder_t1221177846 * BuildRequests_AppendUUIDToURL_m3536657801 (Il2CppObject * __this /* static, unused */, StringBuilder_t1221177846 * ___url0, String_t* ___uuid1, bool ___firstInQS2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder PubNubMessaging.Core.BuildRequests::AppendPresenceHeartbeatToURL(System.Text.StringBuilder,System.Int32)
extern "C"  StringBuilder_t1221177846 * BuildRequests_AppendPresenceHeartbeatToURL_m1440843861 (Il2CppObject * __this /* static, unused */, StringBuilder_t1221177846 * ___url0, int32_t ___pubnubPresenceHeartbeatInSeconds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.StringBuilder PubNubMessaging.Core.BuildRequests::AppendPNSDKVersionToURL(System.Text.StringBuilder,System.String,PubNubMessaging.Core.ResponseType)
extern "C"  StringBuilder_t1221177846 * BuildRequests_AppendPNSDKVersionToURL_m1308510647 (Il2CppObject * __this /* static, unused */, StringBuilder_t1221177846 * ___url0, String_t* ___pnsdkVersion1, int32_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
