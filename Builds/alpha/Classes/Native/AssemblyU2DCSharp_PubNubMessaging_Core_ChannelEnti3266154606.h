﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PubNubMessaging.Core.ChannelParameters
struct ChannelParameters_t547936593;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelIden1147162267.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.ChannelEntity
struct  ChannelEntity_t3266154606  : public Il2CppObject
{
public:
	// PubNubMessaging.Core.ChannelIdentity PubNubMessaging.Core.ChannelEntity::ChannelID
	ChannelIdentity_t1147162267  ___ChannelID_0;
	// PubNubMessaging.Core.ChannelParameters PubNubMessaging.Core.ChannelEntity::ChannelParams
	ChannelParameters_t547936593 * ___ChannelParams_1;

public:
	inline static int32_t get_offset_of_ChannelID_0() { return static_cast<int32_t>(offsetof(ChannelEntity_t3266154606, ___ChannelID_0)); }
	inline ChannelIdentity_t1147162267  get_ChannelID_0() const { return ___ChannelID_0; }
	inline ChannelIdentity_t1147162267 * get_address_of_ChannelID_0() { return &___ChannelID_0; }
	inline void set_ChannelID_0(ChannelIdentity_t1147162267  value)
	{
		___ChannelID_0 = value;
	}

	inline static int32_t get_offset_of_ChannelParams_1() { return static_cast<int32_t>(offsetof(ChannelEntity_t3266154606, ___ChannelParams_1)); }
	inline ChannelParameters_t547936593 * get_ChannelParams_1() const { return ___ChannelParams_1; }
	inline ChannelParameters_t547936593 ** get_address_of_ChannelParams_1() { return &___ChannelParams_1; }
	inline void set_ChannelParams_1(ChannelParameters_t547936593 * value)
	{
		___ChannelParams_1 = value;
		Il2CppCodeGenWriteBarrier(&___ChannelParams_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
