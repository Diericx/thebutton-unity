﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.ChannelEntity
struct ChannelEntity_t3266154606;
// PubNubMessaging.Core.ChannelParameters
struct ChannelParameters_t547936593;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelIden1147162267.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelParam547936593.h"

// System.Void PubNubMessaging.Core.ChannelEntity::.ctor(PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters)
extern "C"  void ChannelEntity__ctor_m768090726 (ChannelEntity_t3266154606 * __this, ChannelIdentity_t1147162267  ___channelID0, ChannelParameters_t547936593 * ___channelParams1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
