﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.ChannelIdentity
struct  ChannelIdentity_t1147162267 
{
public:
	union
	{
		struct
		{
			// System.String PubNubMessaging.Core.ChannelIdentity::<ChannelOrChannelGroupName>k__BackingField
			String_t* ___U3CChannelOrChannelGroupNameU3Ek__BackingField_0;
			// System.Boolean PubNubMessaging.Core.ChannelIdentity::<IsChannelGroup>k__BackingField
			bool ___U3CIsChannelGroupU3Ek__BackingField_1;
			// System.Boolean PubNubMessaging.Core.ChannelIdentity::<IsPresenceChannel>k__BackingField
			bool ___U3CIsPresenceChannelU3Ek__BackingField_2;
		};
		uint8_t ChannelIdentity_t1147162267__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CChannelOrChannelGroupNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ChannelIdentity_t1147162267, ___U3CChannelOrChannelGroupNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CChannelOrChannelGroupNameU3Ek__BackingField_0() const { return ___U3CChannelOrChannelGroupNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CChannelOrChannelGroupNameU3Ek__BackingField_0() { return &___U3CChannelOrChannelGroupNameU3Ek__BackingField_0; }
	inline void set_U3CChannelOrChannelGroupNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CChannelOrChannelGroupNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CChannelOrChannelGroupNameU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CIsChannelGroupU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ChannelIdentity_t1147162267, ___U3CIsChannelGroupU3Ek__BackingField_1)); }
	inline bool get_U3CIsChannelGroupU3Ek__BackingField_1() const { return ___U3CIsChannelGroupU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsChannelGroupU3Ek__BackingField_1() { return &___U3CIsChannelGroupU3Ek__BackingField_1; }
	inline void set_U3CIsChannelGroupU3Ek__BackingField_1(bool value)
	{
		___U3CIsChannelGroupU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CIsPresenceChannelU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ChannelIdentity_t1147162267, ___U3CIsPresenceChannelU3Ek__BackingField_2)); }
	inline bool get_U3CIsPresenceChannelU3Ek__BackingField_2() const { return ___U3CIsPresenceChannelU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsPresenceChannelU3Ek__BackingField_2() { return &___U3CIsPresenceChannelU3Ek__BackingField_2; }
	inline void set_U3CIsPresenceChannelU3Ek__BackingField_2(bool value)
	{
		___U3CIsPresenceChannelU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PubNubMessaging.Core.ChannelIdentity
struct ChannelIdentity_t1147162267_marshaled_pinvoke
{
	union
	{
		struct
		{
			char* ___U3CChannelOrChannelGroupNameU3Ek__BackingField_0;
			int32_t ___U3CIsChannelGroupU3Ek__BackingField_1;
			int32_t ___U3CIsPresenceChannelU3Ek__BackingField_2;
		};
		uint8_t ChannelIdentity_t1147162267__padding[1];
	};
};
// Native definition for COM marshalling of PubNubMessaging.Core.ChannelIdentity
struct ChannelIdentity_t1147162267_marshaled_com
{
	union
	{
		struct
		{
			Il2CppChar* ___U3CChannelOrChannelGroupNameU3Ek__BackingField_0;
			int32_t ___U3CIsChannelGroupU3Ek__BackingField_1;
			int32_t ___U3CIsPresenceChannelU3Ek__BackingField_2;
		};
		uint8_t ChannelIdentity_t1147162267__padding[1];
	};
};
