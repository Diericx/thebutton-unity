﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// PubNubMessaging.Core.ChannelIdentity
struct ChannelIdentity_t1147162267;
struct ChannelIdentity_t1147162267_marshaled_pinvoke;
struct ChannelIdentity_t1147162267_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelIden1147162267.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PubNubMessaging.Core.ChannelIdentity::.ctor(System.String,System.Boolean,System.Boolean)
extern "C"  void ChannelIdentity__ctor_m4225011207 (ChannelIdentity_t1147162267 * __this, String_t* ___channelOrChannelGroupName0, bool ___isChannelGroup1, bool ___isPresenceChannel2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.ChannelIdentity::get_ChannelOrChannelGroupName()
extern "C"  String_t* ChannelIdentity_get_ChannelOrChannelGroupName_m3649725426 (ChannelIdentity_t1147162267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.ChannelIdentity::set_ChannelOrChannelGroupName(System.String)
extern "C"  void ChannelIdentity_set_ChannelOrChannelGroupName_m2045537847 (ChannelIdentity_t1147162267 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.ChannelIdentity::get_IsChannelGroup()
extern "C"  bool ChannelIdentity_get_IsChannelGroup_m993016464 (ChannelIdentity_t1147162267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.ChannelIdentity::set_IsChannelGroup(System.Boolean)
extern "C"  void ChannelIdentity_set_IsChannelGroup_m13316953 (ChannelIdentity_t1147162267 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.ChannelIdentity::get_IsPresenceChannel()
extern "C"  bool ChannelIdentity_get_IsPresenceChannel_m1925596672 (ChannelIdentity_t1147162267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.ChannelIdentity::set_IsPresenceChannel(System.Boolean)
extern "C"  void ChannelIdentity_set_IsPresenceChannel_m3586774059 (ChannelIdentity_t1147162267 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct ChannelIdentity_t1147162267;
struct ChannelIdentity_t1147162267_marshaled_pinvoke;

extern "C" void ChannelIdentity_t1147162267_marshal_pinvoke(const ChannelIdentity_t1147162267& unmarshaled, ChannelIdentity_t1147162267_marshaled_pinvoke& marshaled);
extern "C" void ChannelIdentity_t1147162267_marshal_pinvoke_back(const ChannelIdentity_t1147162267_marshaled_pinvoke& marshaled, ChannelIdentity_t1147162267& unmarshaled);
extern "C" void ChannelIdentity_t1147162267_marshal_pinvoke_cleanup(ChannelIdentity_t1147162267_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ChannelIdentity_t1147162267;
struct ChannelIdentity_t1147162267_marshaled_com;

extern "C" void ChannelIdentity_t1147162267_marshal_com(const ChannelIdentity_t1147162267& unmarshaled, ChannelIdentity_t1147162267_marshaled_com& marshaled);
extern "C" void ChannelIdentity_t1147162267_marshal_com_back(const ChannelIdentity_t1147162267_marshaled_com& marshaled, ChannelIdentity_t1147162267& unmarshaled);
extern "C" void ChannelIdentity_t1147162267_marshal_com_cleanup(ChannelIdentity_t1147162267_marshaled_com& marshaled);
