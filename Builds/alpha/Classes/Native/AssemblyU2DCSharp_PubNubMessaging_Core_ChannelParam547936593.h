﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Type
struct Type_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.ChannelParameters
struct  ChannelParameters_t547936593  : public Il2CppObject
{
public:
	// System.Boolean PubNubMessaging.Core.ChannelParameters::<IsAwaitingConnectCallback>k__BackingField
	bool ___U3CIsAwaitingConnectCallbackU3Ek__BackingField_0;
	// System.Boolean PubNubMessaging.Core.ChannelParameters::<IsSubscribed>k__BackingField
	bool ___U3CIsSubscribedU3Ek__BackingField_1;
	// System.Object PubNubMessaging.Core.ChannelParameters::<Callbacks>k__BackingField
	Il2CppObject * ___U3CCallbacksU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> PubNubMessaging.Core.ChannelParameters::<UserState>k__BackingField
	Dictionary_2_t309261261 * ___U3CUserStateU3Ek__BackingField_3;
	// System.Type PubNubMessaging.Core.ChannelParameters::<TypeParameterType>k__BackingField
	Type_t * ___U3CTypeParameterTypeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CIsAwaitingConnectCallbackU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ChannelParameters_t547936593, ___U3CIsAwaitingConnectCallbackU3Ek__BackingField_0)); }
	inline bool get_U3CIsAwaitingConnectCallbackU3Ek__BackingField_0() const { return ___U3CIsAwaitingConnectCallbackU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsAwaitingConnectCallbackU3Ek__BackingField_0() { return &___U3CIsAwaitingConnectCallbackU3Ek__BackingField_0; }
	inline void set_U3CIsAwaitingConnectCallbackU3Ek__BackingField_0(bool value)
	{
		___U3CIsAwaitingConnectCallbackU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CIsSubscribedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ChannelParameters_t547936593, ___U3CIsSubscribedU3Ek__BackingField_1)); }
	inline bool get_U3CIsSubscribedU3Ek__BackingField_1() const { return ___U3CIsSubscribedU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsSubscribedU3Ek__BackingField_1() { return &___U3CIsSubscribedU3Ek__BackingField_1; }
	inline void set_U3CIsSubscribedU3Ek__BackingField_1(bool value)
	{
		___U3CIsSubscribedU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCallbacksU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ChannelParameters_t547936593, ___U3CCallbacksU3Ek__BackingField_2)); }
	inline Il2CppObject * get_U3CCallbacksU3Ek__BackingField_2() const { return ___U3CCallbacksU3Ek__BackingField_2; }
	inline Il2CppObject ** get_address_of_U3CCallbacksU3Ek__BackingField_2() { return &___U3CCallbacksU3Ek__BackingField_2; }
	inline void set_U3CCallbacksU3Ek__BackingField_2(Il2CppObject * value)
	{
		___U3CCallbacksU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCallbacksU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CUserStateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ChannelParameters_t547936593, ___U3CUserStateU3Ek__BackingField_3)); }
	inline Dictionary_2_t309261261 * get_U3CUserStateU3Ek__BackingField_3() const { return ___U3CUserStateU3Ek__BackingField_3; }
	inline Dictionary_2_t309261261 ** get_address_of_U3CUserStateU3Ek__BackingField_3() { return &___U3CUserStateU3Ek__BackingField_3; }
	inline void set_U3CUserStateU3Ek__BackingField_3(Dictionary_2_t309261261 * value)
	{
		___U3CUserStateU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUserStateU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CTypeParameterTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ChannelParameters_t547936593, ___U3CTypeParameterTypeU3Ek__BackingField_4)); }
	inline Type_t * get_U3CTypeParameterTypeU3Ek__BackingField_4() const { return ___U3CTypeParameterTypeU3Ek__BackingField_4; }
	inline Type_t ** get_address_of_U3CTypeParameterTypeU3Ek__BackingField_4() { return &___U3CTypeParameterTypeU3Ek__BackingField_4; }
	inline void set_U3CTypeParameterTypeU3Ek__BackingField_4(Type_t * value)
	{
		___U3CTypeParameterTypeU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTypeParameterTypeU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
