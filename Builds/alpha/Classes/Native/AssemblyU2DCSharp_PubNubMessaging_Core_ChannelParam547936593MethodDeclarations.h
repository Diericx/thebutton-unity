﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.ChannelParameters
struct ChannelParameters_t547936593;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void PubNubMessaging.Core.ChannelParameters::.ctor()
extern "C"  void ChannelParameters__ctor_m4282988693 (ChannelParameters_t547936593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.ChannelParameters::get_IsAwaitingConnectCallback()
extern "C"  bool ChannelParameters_get_IsAwaitingConnectCallback_m1500882003 (ChannelParameters_t547936593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.ChannelParameters::set_IsAwaitingConnectCallback(System.Boolean)
extern "C"  void ChannelParameters_set_IsAwaitingConnectCallback_m3376925398 (ChannelParameters_t547936593 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.ChannelParameters::get_IsSubscribed()
extern "C"  bool ChannelParameters_get_IsSubscribed_m2698030780 (ChannelParameters_t547936593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.ChannelParameters::set_IsSubscribed(System.Boolean)
extern "C"  void ChannelParameters_set_IsSubscribed_m2665766917 (ChannelParameters_t547936593 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Core.ChannelParameters::get_Callbacks()
extern "C"  Il2CppObject * ChannelParameters_get_Callbacks_m2911511739 (ChannelParameters_t547936593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.ChannelParameters::set_Callbacks(System.Object)
extern "C"  void ChannelParameters_set_Callbacks_m989393044 (ChannelParameters_t547936593 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> PubNubMessaging.Core.ChannelParameters::get_UserState()
extern "C"  Dictionary_2_t309261261 * ChannelParameters_get_UserState_m946416800 (ChannelParameters_t547936593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.ChannelParameters::set_UserState(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void ChannelParameters_set_UserState_m1484944809 (ChannelParameters_t547936593 * __this, Dictionary_2_t309261261 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type PubNubMessaging.Core.ChannelParameters::get_TypeParameterType()
extern "C"  Type_t * ChannelParameters_get_TypeParameterType_m2170954209 (ChannelParameters_t547936593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.ChannelParameters::set_TypeParameterType(System.Type)
extern "C"  void ChannelParameters_set_TypeParameterType_m3445567428 (ChannelParameters_t547936593 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
