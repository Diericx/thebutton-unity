﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.CoroutineClass/<SendRequestPresenceHeartbeat>c__Iterator3`1<System.Object>
struct U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1006883627;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestPresenceHeartbeat>c__Iterator3`1<System.Object>::.ctor()
extern "C"  void U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1__ctor_m2415533718_gshared (U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1006883627 * __this, const MethodInfo* method);
#define U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1__ctor_m2415533718(__this, method) ((  void (*) (U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1006883627 *, const MethodInfo*))U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1__ctor_m2415533718_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.CoroutineClass/<SendRequestPresenceHeartbeat>c__Iterator3`1<System.Object>::MoveNext()
extern "C"  bool U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_MoveNext_m3875715858_gshared (U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1006883627 * __this, const MethodInfo* method);
#define U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_MoveNext_m3875715858(__this, method) ((  bool (*) (U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1006883627 *, const MethodInfo*))U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_MoveNext_m3875715858_gshared)(__this, method)
// System.Object PubNubMessaging.Core.CoroutineClass/<SendRequestPresenceHeartbeat>c__Iterator3`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2999441612_gshared (U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1006883627 * __this, const MethodInfo* method);
#define U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2999441612(__this, method) ((  Il2CppObject * (*) (U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1006883627 *, const MethodInfo*))U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2999441612_gshared)(__this, method)
// System.Object PubNubMessaging.Core.CoroutineClass/<SendRequestPresenceHeartbeat>c__Iterator3`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_System_Collections_IEnumerator_get_Current_m2677775284_gshared (U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1006883627 * __this, const MethodInfo* method);
#define U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_System_Collections_IEnumerator_get_Current_m2677775284(__this, method) ((  Il2CppObject * (*) (U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1006883627 *, const MethodInfo*))U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_System_Collections_IEnumerator_get_Current_m2677775284_gshared)(__this, method)
// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestPresenceHeartbeat>c__Iterator3`1<System.Object>::Dispose()
extern "C"  void U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_Dispose_m4134861085_gshared (U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1006883627 * __this, const MethodInfo* method);
#define U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_Dispose_m4134861085(__this, method) ((  void (*) (U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1006883627 *, const MethodInfo*))U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_Dispose_m4134861085_gshared)(__this, method)
// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestPresenceHeartbeat>c__Iterator3`1<System.Object>::Reset()
extern "C"  void U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_Reset_m1260784827_gshared (U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1006883627 * __this, const MethodInfo* method);
#define U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_Reset_m1260784827(__this, method) ((  void (*) (U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1006883627 *, const MethodInfo*))U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_Reset_m1260784827_gshared)(__this, method)
