﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Core.RequestState`1<System.Object>
struct RequestState_1_t8940997;
// PubNubMessaging.Core.CoroutineClass
struct CoroutineClass_t2476503358;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1<System.Object>
struct  U3CDelayRequestU3Ec__Iterator0_1_t1605486090  : public Il2CppObject
{
public:
	// System.Int32 PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1::pause
	int32_t ___pause_0;
	// System.String PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1::url
	String_t* ___url_1;
	// PubNubMessaging.Core.RequestState`1<T> PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1::pubnubRequestState
	RequestState_1_t8940997 * ___pubnubRequestState_2;
	// System.Int32 PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1::timeout
	int32_t ___timeout_3;
	// PubNubMessaging.Core.CurrentRequestType PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1::crt
	int32_t ___crt_4;
	// PubNubMessaging.Core.CoroutineClass PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1::$this
	CoroutineClass_t2476503358 * ___U24this_5;
	// System.Object PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1::$disposing
	bool ___U24disposing_7;
	// System.Int32 PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_pause_0() { return static_cast<int32_t>(offsetof(U3CDelayRequestU3Ec__Iterator0_1_t1605486090, ___pause_0)); }
	inline int32_t get_pause_0() const { return ___pause_0; }
	inline int32_t* get_address_of_pause_0() { return &___pause_0; }
	inline void set_pause_0(int32_t value)
	{
		___pause_0 = value;
	}

	inline static int32_t get_offset_of_url_1() { return static_cast<int32_t>(offsetof(U3CDelayRequestU3Ec__Iterator0_1_t1605486090, ___url_1)); }
	inline String_t* get_url_1() const { return ___url_1; }
	inline String_t** get_address_of_url_1() { return &___url_1; }
	inline void set_url_1(String_t* value)
	{
		___url_1 = value;
		Il2CppCodeGenWriteBarrier(&___url_1, value);
	}

	inline static int32_t get_offset_of_pubnubRequestState_2() { return static_cast<int32_t>(offsetof(U3CDelayRequestU3Ec__Iterator0_1_t1605486090, ___pubnubRequestState_2)); }
	inline RequestState_1_t8940997 * get_pubnubRequestState_2() const { return ___pubnubRequestState_2; }
	inline RequestState_1_t8940997 ** get_address_of_pubnubRequestState_2() { return &___pubnubRequestState_2; }
	inline void set_pubnubRequestState_2(RequestState_1_t8940997 * value)
	{
		___pubnubRequestState_2 = value;
		Il2CppCodeGenWriteBarrier(&___pubnubRequestState_2, value);
	}

	inline static int32_t get_offset_of_timeout_3() { return static_cast<int32_t>(offsetof(U3CDelayRequestU3Ec__Iterator0_1_t1605486090, ___timeout_3)); }
	inline int32_t get_timeout_3() const { return ___timeout_3; }
	inline int32_t* get_address_of_timeout_3() { return &___timeout_3; }
	inline void set_timeout_3(int32_t value)
	{
		___timeout_3 = value;
	}

	inline static int32_t get_offset_of_crt_4() { return static_cast<int32_t>(offsetof(U3CDelayRequestU3Ec__Iterator0_1_t1605486090, ___crt_4)); }
	inline int32_t get_crt_4() const { return ___crt_4; }
	inline int32_t* get_address_of_crt_4() { return &___crt_4; }
	inline void set_crt_4(int32_t value)
	{
		___crt_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CDelayRequestU3Ec__Iterator0_1_t1605486090, ___U24this_5)); }
	inline CoroutineClass_t2476503358 * get_U24this_5() const { return ___U24this_5; }
	inline CoroutineClass_t2476503358 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(CoroutineClass_t2476503358 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CDelayRequestU3Ec__Iterator0_1_t1605486090, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CDelayRequestU3Ec__Iterator0_1_t1605486090, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CDelayRequestU3Ec__Iterator0_1_t1605486090, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
