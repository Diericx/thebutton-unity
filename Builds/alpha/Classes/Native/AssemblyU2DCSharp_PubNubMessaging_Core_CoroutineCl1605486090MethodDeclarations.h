﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1<System.Object>
struct U3CDelayRequestU3Ec__Iterator0_1_t1605486090;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1<System.Object>::.ctor()
extern "C"  void U3CDelayRequestU3Ec__Iterator0_1__ctor_m3187285153_gshared (U3CDelayRequestU3Ec__Iterator0_1_t1605486090 * __this, const MethodInfo* method);
#define U3CDelayRequestU3Ec__Iterator0_1__ctor_m3187285153(__this, method) ((  void (*) (U3CDelayRequestU3Ec__Iterator0_1_t1605486090 *, const MethodInfo*))U3CDelayRequestU3Ec__Iterator0_1__ctor_m3187285153_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1<System.Object>::MoveNext()
extern "C"  bool U3CDelayRequestU3Ec__Iterator0_1_MoveNext_m1180190031_gshared (U3CDelayRequestU3Ec__Iterator0_1_t1605486090 * __this, const MethodInfo* method);
#define U3CDelayRequestU3Ec__Iterator0_1_MoveNext_m1180190031(__this, method) ((  bool (*) (U3CDelayRequestU3Ec__Iterator0_1_t1605486090 *, const MethodInfo*))U3CDelayRequestU3Ec__Iterator0_1_MoveNext_m1180190031_gshared)(__this, method)
// System.Object PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayRequestU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m442173987_gshared (U3CDelayRequestU3Ec__Iterator0_1_t1605486090 * __this, const MethodInfo* method);
#define U3CDelayRequestU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m442173987(__this, method) ((  Il2CppObject * (*) (U3CDelayRequestU3Ec__Iterator0_1_t1605486090 *, const MethodInfo*))U3CDelayRequestU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m442173987_gshared)(__this, method)
// System.Object PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayRequestU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m3215447499_gshared (U3CDelayRequestU3Ec__Iterator0_1_t1605486090 * __this, const MethodInfo* method);
#define U3CDelayRequestU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m3215447499(__this, method) ((  Il2CppObject * (*) (U3CDelayRequestU3Ec__Iterator0_1_t1605486090 *, const MethodInfo*))U3CDelayRequestU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m3215447499_gshared)(__this, method)
// System.Void PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1<System.Object>::Dispose()
extern "C"  void U3CDelayRequestU3Ec__Iterator0_1_Dispose_m640606148_gshared (U3CDelayRequestU3Ec__Iterator0_1_t1605486090 * __this, const MethodInfo* method);
#define U3CDelayRequestU3Ec__Iterator0_1_Dispose_m640606148(__this, method) ((  void (*) (U3CDelayRequestU3Ec__Iterator0_1_t1605486090 *, const MethodInfo*))U3CDelayRequestU3Ec__Iterator0_1_Dispose_m640606148_gshared)(__this, method)
// System.Void PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1<System.Object>::Reset()
extern "C"  void U3CDelayRequestU3Ec__Iterator0_1_Reset_m3747843318_gshared (U3CDelayRequestU3Ec__Iterator0_1_t1605486090 * __this, const MethodInfo* method);
#define U3CDelayRequestU3Ec__Iterator0_1_Reset_m3747843318(__this, method) ((  void (*) (U3CDelayRequestU3Ec__Iterator0_1_t1605486090 *, const MethodInfo*))U3CDelayRequestU3Ec__Iterator0_1_Reset_m3747843318_gshared)(__this, method)
