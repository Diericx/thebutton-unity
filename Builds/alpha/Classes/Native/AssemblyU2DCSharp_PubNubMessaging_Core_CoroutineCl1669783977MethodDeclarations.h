﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.CoroutineClass/<SendRequestHeartbeat>c__Iterator4`1<System.Object>
struct U3CSendRequestHeartbeatU3Ec__Iterator4_1_t1669783977;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestHeartbeat>c__Iterator4`1<System.Object>::.ctor()
extern "C"  void U3CSendRequestHeartbeatU3Ec__Iterator4_1__ctor_m330375226_gshared (U3CSendRequestHeartbeatU3Ec__Iterator4_1_t1669783977 * __this, const MethodInfo* method);
#define U3CSendRequestHeartbeatU3Ec__Iterator4_1__ctor_m330375226(__this, method) ((  void (*) (U3CSendRequestHeartbeatU3Ec__Iterator4_1_t1669783977 *, const MethodInfo*))U3CSendRequestHeartbeatU3Ec__Iterator4_1__ctor_m330375226_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.CoroutineClass/<SendRequestHeartbeat>c__Iterator4`1<System.Object>::MoveNext()
extern "C"  bool U3CSendRequestHeartbeatU3Ec__Iterator4_1_MoveNext_m2485563690_gshared (U3CSendRequestHeartbeatU3Ec__Iterator4_1_t1669783977 * __this, const MethodInfo* method);
#define U3CSendRequestHeartbeatU3Ec__Iterator4_1_MoveNext_m2485563690(__this, method) ((  bool (*) (U3CSendRequestHeartbeatU3Ec__Iterator4_1_t1669783977 *, const MethodInfo*))U3CSendRequestHeartbeatU3Ec__Iterator4_1_MoveNext_m2485563690_gshared)(__this, method)
// System.Object PubNubMessaging.Core.CoroutineClass/<SendRequestHeartbeat>c__Iterator4`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendRequestHeartbeatU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2749007252_gshared (U3CSendRequestHeartbeatU3Ec__Iterator4_1_t1669783977 * __this, const MethodInfo* method);
#define U3CSendRequestHeartbeatU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2749007252(__this, method) ((  Il2CppObject * (*) (U3CSendRequestHeartbeatU3Ec__Iterator4_1_t1669783977 *, const MethodInfo*))U3CSendRequestHeartbeatU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2749007252_gshared)(__this, method)
// System.Object PubNubMessaging.Core.CoroutineClass/<SendRequestHeartbeat>c__Iterator4`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendRequestHeartbeatU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m2404150444_gshared (U3CSendRequestHeartbeatU3Ec__Iterator4_1_t1669783977 * __this, const MethodInfo* method);
#define U3CSendRequestHeartbeatU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m2404150444(__this, method) ((  Il2CppObject * (*) (U3CSendRequestHeartbeatU3Ec__Iterator4_1_t1669783977 *, const MethodInfo*))U3CSendRequestHeartbeatU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m2404150444_gshared)(__this, method)
// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestHeartbeat>c__Iterator4`1<System.Object>::Dispose()
extern "C"  void U3CSendRequestHeartbeatU3Ec__Iterator4_1_Dispose_m3301539467_gshared (U3CSendRequestHeartbeatU3Ec__Iterator4_1_t1669783977 * __this, const MethodInfo* method);
#define U3CSendRequestHeartbeatU3Ec__Iterator4_1_Dispose_m3301539467(__this, method) ((  void (*) (U3CSendRequestHeartbeatU3Ec__Iterator4_1_t1669783977 *, const MethodInfo*))U3CSendRequestHeartbeatU3Ec__Iterator4_1_Dispose_m3301539467_gshared)(__this, method)
// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestHeartbeat>c__Iterator4`1<System.Object>::Reset()
extern "C"  void U3CSendRequestHeartbeatU3Ec__Iterator4_1_Reset_m1908191165_gshared (U3CSendRequestHeartbeatU3Ec__Iterator4_1_t1669783977 * __this, const MethodInfo* method);
#define U3CSendRequestHeartbeatU3Ec__Iterator4_1_Reset_m1908191165(__this, method) ((  void (*) (U3CSendRequestHeartbeatU3Ec__Iterator4_1_t1669783977 *, const MethodInfo*))U3CSendRequestHeartbeatU3Ec__Iterator4_1_Reset_m1908191165_gshared)(__this, method)
