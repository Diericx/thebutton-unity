﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PubNubMessaging.Core.CoroutineParams`1<System.Object>
struct CoroutineParams_1_t3789821357;
// PubNubMessaging.Core.CoroutineClass
struct CoroutineClass_t2476503358;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.CoroutineClass/<CheckTimeoutNonSub>c__Iterator6`1<System.Object>
struct  U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296  : public Il2CppObject
{
public:
	// PubNubMessaging.Core.CoroutineParams`1<T> PubNubMessaging.Core.CoroutineClass/<CheckTimeoutNonSub>c__Iterator6`1::cp
	CoroutineParams_1_t3789821357 * ___cp_0;
	// PubNubMessaging.Core.CoroutineClass PubNubMessaging.Core.CoroutineClass/<CheckTimeoutNonSub>c__Iterator6`1::$this
	CoroutineClass_t2476503358 * ___U24this_1;
	// System.Object PubNubMessaging.Core.CoroutineClass/<CheckTimeoutNonSub>c__Iterator6`1::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean PubNubMessaging.Core.CoroutineClass/<CheckTimeoutNonSub>c__Iterator6`1::$disposing
	bool ___U24disposing_3;
	// System.Int32 PubNubMessaging.Core.CoroutineClass/<CheckTimeoutNonSub>c__Iterator6`1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_cp_0() { return static_cast<int32_t>(offsetof(U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296, ___cp_0)); }
	inline CoroutineParams_1_t3789821357 * get_cp_0() const { return ___cp_0; }
	inline CoroutineParams_1_t3789821357 ** get_address_of_cp_0() { return &___cp_0; }
	inline void set_cp_0(CoroutineParams_1_t3789821357 * value)
	{
		___cp_0 = value;
		Il2CppCodeGenWriteBarrier(&___cp_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296, ___U24this_1)); }
	inline CoroutineClass_t2476503358 * get_U24this_1() const { return ___U24this_1; }
	inline CoroutineClass_t2476503358 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(CoroutineClass_t2476503358 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
