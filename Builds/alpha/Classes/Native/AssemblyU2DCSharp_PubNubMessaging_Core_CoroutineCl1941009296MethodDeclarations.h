﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.CoroutineClass/<CheckTimeoutNonSub>c__Iterator6`1<System.Object>
struct U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutNonSub>c__Iterator6`1<System.Object>::.ctor()
extern "C"  void U3CCheckTimeoutNonSubU3Ec__Iterator6_1__ctor_m2525531305_gshared (U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296 * __this, const MethodInfo* method);
#define U3CCheckTimeoutNonSubU3Ec__Iterator6_1__ctor_m2525531305(__this, method) ((  void (*) (U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296 *, const MethodInfo*))U3CCheckTimeoutNonSubU3Ec__Iterator6_1__ctor_m2525531305_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.CoroutineClass/<CheckTimeoutNonSub>c__Iterator6`1<System.Object>::MoveNext()
extern "C"  bool U3CCheckTimeoutNonSubU3Ec__Iterator6_1_MoveNext_m4271675359_gshared (U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296 * __this, const MethodInfo* method);
#define U3CCheckTimeoutNonSubU3Ec__Iterator6_1_MoveNext_m4271675359(__this, method) ((  bool (*) (U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296 *, const MethodInfo*))U3CCheckTimeoutNonSubU3Ec__Iterator6_1_MoveNext_m4271675359_gshared)(__this, method)
// System.Object PubNubMessaging.Core.CoroutineClass/<CheckTimeoutNonSub>c__Iterator6`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckTimeoutNonSubU3Ec__Iterator6_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3929667235_gshared (U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296 * __this, const MethodInfo* method);
#define U3CCheckTimeoutNonSubU3Ec__Iterator6_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3929667235(__this, method) ((  Il2CppObject * (*) (U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296 *, const MethodInfo*))U3CCheckTimeoutNonSubU3Ec__Iterator6_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3929667235_gshared)(__this, method)
// System.Object PubNubMessaging.Core.CoroutineClass/<CheckTimeoutNonSub>c__Iterator6`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckTimeoutNonSubU3Ec__Iterator6_1_System_Collections_IEnumerator_get_Current_m1300662283_gshared (U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296 * __this, const MethodInfo* method);
#define U3CCheckTimeoutNonSubU3Ec__Iterator6_1_System_Collections_IEnumerator_get_Current_m1300662283(__this, method) ((  Il2CppObject * (*) (U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296 *, const MethodInfo*))U3CCheckTimeoutNonSubU3Ec__Iterator6_1_System_Collections_IEnumerator_get_Current_m1300662283_gshared)(__this, method)
// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutNonSub>c__Iterator6`1<System.Object>::Dispose()
extern "C"  void U3CCheckTimeoutNonSubU3Ec__Iterator6_1_Dispose_m2291771386_gshared (U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296 * __this, const MethodInfo* method);
#define U3CCheckTimeoutNonSubU3Ec__Iterator6_1_Dispose_m2291771386(__this, method) ((  void (*) (U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296 *, const MethodInfo*))U3CCheckTimeoutNonSubU3Ec__Iterator6_1_Dispose_m2291771386_gshared)(__this, method)
// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutNonSub>c__Iterator6`1<System.Object>::Reset()
extern "C"  void U3CCheckTimeoutNonSubU3Ec__Iterator6_1_Reset_m4282713708_gshared (U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296 * __this, const MethodInfo* method);
#define U3CCheckTimeoutNonSubU3Ec__Iterator6_1_Reset_m4282713708(__this, method) ((  void (*) (U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296 *, const MethodInfo*))U3CCheckTimeoutNonSubU3Ec__Iterator6_1_Reset_m4282713708_gshared)(__this, method)
