﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[][]
struct StringU5BU5DU5BU5D_t2190260861;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t3750818532;
// System.Int64[][]
struct Int64U5BU5DU5BU5D_t1808128809;
// System.Int16[][]
struct Int16U5BU5DU5BU5D_t3712714918;
// System.UInt16[][]
struct UInt16U5BU5DU5BU5D_t880806807;
// System.UInt64[][]
struct UInt64U5BU5DU5BU5D_t595110526;
// System.UInt32[][]
struct UInt32U5BU5DU5BU5D_t1156922361;
// System.Decimal[][]
struct DecimalU5BU5DU5BU5D_t418021545;
// System.Double[][]
struct DoubleU5BU5DU5BU5D_t2187755765;
// System.Boolean[][]
struct BooleanU5BU5DU5BU5D_t2798867818;
// System.Object[][]
struct ObjectU5BU5DU5BU5D_t1712241747;
// System.Single[][]
struct SingleU5BU5DU5BU5D_t2115305192;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.CoroutineClass
struct  CoroutineClass_t2476503358  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean PubNubMessaging.Core.CoroutineClass::isHearbeatComplete
	bool ___isHearbeatComplete_19;
	// System.Boolean PubNubMessaging.Core.CoroutineClass::isPresenceHeartbeatComplete
	bool ___isPresenceHeartbeatComplete_20;
	// System.Boolean PubNubMessaging.Core.CoroutineClass::isSubscribeComplete
	bool ___isSubscribeComplete_21;
	// System.Boolean PubNubMessaging.Core.CoroutineClass::isNonSubscribeComplete
	bool ___isNonSubscribeComplete_22;
	// System.Collections.IEnumerator PubNubMessaging.Core.CoroutineClass::SubCoroutine
	Il2CppObject * ___SubCoroutine_23;
	// System.Collections.IEnumerator PubNubMessaging.Core.CoroutineClass::SubTimeoutCoroutine
	Il2CppObject * ___SubTimeoutCoroutine_24;
	// System.Collections.IEnumerator PubNubMessaging.Core.CoroutineClass::NonSubCoroutine
	Il2CppObject * ___NonSubCoroutine_25;
	// System.Collections.IEnumerator PubNubMessaging.Core.CoroutineClass::NonSubTimeoutCoroutine
	Il2CppObject * ___NonSubTimeoutCoroutine_26;
	// System.Collections.IEnumerator PubNubMessaging.Core.CoroutineClass::PresenceHeartbeatCoroutine
	Il2CppObject * ___PresenceHeartbeatCoroutine_27;
	// System.Collections.IEnumerator PubNubMessaging.Core.CoroutineClass::PresenceHeartbeatTimeoutCoroutine
	Il2CppObject * ___PresenceHeartbeatTimeoutCoroutine_28;
	// System.Collections.IEnumerator PubNubMessaging.Core.CoroutineClass::HeartbeatCoroutine
	Il2CppObject * ___HeartbeatCoroutine_29;
	// System.Collections.IEnumerator PubNubMessaging.Core.CoroutineClass::HeartbeatTimeoutCoroutine
	Il2CppObject * ___HeartbeatTimeoutCoroutine_30;
	// System.Collections.IEnumerator PubNubMessaging.Core.CoroutineClass::DelayRequestCoroutineHB
	Il2CppObject * ___DelayRequestCoroutineHB_31;
	// System.Collections.IEnumerator PubNubMessaging.Core.CoroutineClass::DelayRequestCoroutinePHB
	Il2CppObject * ___DelayRequestCoroutinePHB_32;
	// System.Collections.IEnumerator PubNubMessaging.Core.CoroutineClass::DelayRequestCoroutineSub
	Il2CppObject * ___DelayRequestCoroutineSub_33;
	// UnityEngine.WWW PubNubMessaging.Core.CoroutineClass::subscribeWww
	WWW_t2919945039 * ___subscribeWww_34;
	// UnityEngine.WWW PubNubMessaging.Core.CoroutineClass::heartbeatWww
	WWW_t2919945039 * ___heartbeatWww_35;
	// UnityEngine.WWW PubNubMessaging.Core.CoroutineClass::presenceHeartbeatWww
	WWW_t2919945039 * ___presenceHeartbeatWww_36;
	// UnityEngine.WWW PubNubMessaging.Core.CoroutineClass::nonSubscribeWww
	WWW_t2919945039 * ___nonSubscribeWww_37;
	// System.EventHandler`1<System.EventArgs> PubNubMessaging.Core.CoroutineClass::subCoroutineComplete
	EventHandler_1_t1880931879 * ___subCoroutineComplete_38;
	// System.EventHandler`1<System.EventArgs> PubNubMessaging.Core.CoroutineClass::nonSubCoroutineComplete
	EventHandler_1_t1880931879 * ___nonSubCoroutineComplete_39;
	// System.EventHandler`1<System.EventArgs> PubNubMessaging.Core.CoroutineClass::presenceHeartbeatCoroutineComplete
	EventHandler_1_t1880931879 * ___presenceHeartbeatCoroutineComplete_40;
	// System.EventHandler`1<System.EventArgs> PubNubMessaging.Core.CoroutineClass::heartbeatCoroutineComplete
	EventHandler_1_t1880931879 * ___heartbeatCoroutineComplete_41;
	// System.Single PubNubMessaging.Core.CoroutineClass::subscribeTimer
	float ___subscribeTimer_42;
	// System.Single PubNubMessaging.Core.CoroutineClass::heartbeatTimer
	float ___heartbeatTimer_43;
	// System.Single PubNubMessaging.Core.CoroutineClass::presenceHeartbeatTimer
	float ___presenceHeartbeatTimer_44;
	// System.Single PubNubMessaging.Core.CoroutineClass::nonSubscribeTimer
	float ___nonSubscribeTimer_45;
	// System.Single PubNubMessaging.Core.CoroutineClass::heartbeatPauseTimer
	float ___heartbeatPauseTimer_46;
	// System.Single PubNubMessaging.Core.CoroutineClass::presenceHeartbeatPauseTimer
	float ___presenceHeartbeatPauseTimer_47;
	// System.Single PubNubMessaging.Core.CoroutineClass::subscribePauseTimer
	float ___subscribePauseTimer_48;

public:
	inline static int32_t get_offset_of_isHearbeatComplete_19() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___isHearbeatComplete_19)); }
	inline bool get_isHearbeatComplete_19() const { return ___isHearbeatComplete_19; }
	inline bool* get_address_of_isHearbeatComplete_19() { return &___isHearbeatComplete_19; }
	inline void set_isHearbeatComplete_19(bool value)
	{
		___isHearbeatComplete_19 = value;
	}

	inline static int32_t get_offset_of_isPresenceHeartbeatComplete_20() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___isPresenceHeartbeatComplete_20)); }
	inline bool get_isPresenceHeartbeatComplete_20() const { return ___isPresenceHeartbeatComplete_20; }
	inline bool* get_address_of_isPresenceHeartbeatComplete_20() { return &___isPresenceHeartbeatComplete_20; }
	inline void set_isPresenceHeartbeatComplete_20(bool value)
	{
		___isPresenceHeartbeatComplete_20 = value;
	}

	inline static int32_t get_offset_of_isSubscribeComplete_21() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___isSubscribeComplete_21)); }
	inline bool get_isSubscribeComplete_21() const { return ___isSubscribeComplete_21; }
	inline bool* get_address_of_isSubscribeComplete_21() { return &___isSubscribeComplete_21; }
	inline void set_isSubscribeComplete_21(bool value)
	{
		___isSubscribeComplete_21 = value;
	}

	inline static int32_t get_offset_of_isNonSubscribeComplete_22() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___isNonSubscribeComplete_22)); }
	inline bool get_isNonSubscribeComplete_22() const { return ___isNonSubscribeComplete_22; }
	inline bool* get_address_of_isNonSubscribeComplete_22() { return &___isNonSubscribeComplete_22; }
	inline void set_isNonSubscribeComplete_22(bool value)
	{
		___isNonSubscribeComplete_22 = value;
	}

	inline static int32_t get_offset_of_SubCoroutine_23() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___SubCoroutine_23)); }
	inline Il2CppObject * get_SubCoroutine_23() const { return ___SubCoroutine_23; }
	inline Il2CppObject ** get_address_of_SubCoroutine_23() { return &___SubCoroutine_23; }
	inline void set_SubCoroutine_23(Il2CppObject * value)
	{
		___SubCoroutine_23 = value;
		Il2CppCodeGenWriteBarrier(&___SubCoroutine_23, value);
	}

	inline static int32_t get_offset_of_SubTimeoutCoroutine_24() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___SubTimeoutCoroutine_24)); }
	inline Il2CppObject * get_SubTimeoutCoroutine_24() const { return ___SubTimeoutCoroutine_24; }
	inline Il2CppObject ** get_address_of_SubTimeoutCoroutine_24() { return &___SubTimeoutCoroutine_24; }
	inline void set_SubTimeoutCoroutine_24(Il2CppObject * value)
	{
		___SubTimeoutCoroutine_24 = value;
		Il2CppCodeGenWriteBarrier(&___SubTimeoutCoroutine_24, value);
	}

	inline static int32_t get_offset_of_NonSubCoroutine_25() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___NonSubCoroutine_25)); }
	inline Il2CppObject * get_NonSubCoroutine_25() const { return ___NonSubCoroutine_25; }
	inline Il2CppObject ** get_address_of_NonSubCoroutine_25() { return &___NonSubCoroutine_25; }
	inline void set_NonSubCoroutine_25(Il2CppObject * value)
	{
		___NonSubCoroutine_25 = value;
		Il2CppCodeGenWriteBarrier(&___NonSubCoroutine_25, value);
	}

	inline static int32_t get_offset_of_NonSubTimeoutCoroutine_26() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___NonSubTimeoutCoroutine_26)); }
	inline Il2CppObject * get_NonSubTimeoutCoroutine_26() const { return ___NonSubTimeoutCoroutine_26; }
	inline Il2CppObject ** get_address_of_NonSubTimeoutCoroutine_26() { return &___NonSubTimeoutCoroutine_26; }
	inline void set_NonSubTimeoutCoroutine_26(Il2CppObject * value)
	{
		___NonSubTimeoutCoroutine_26 = value;
		Il2CppCodeGenWriteBarrier(&___NonSubTimeoutCoroutine_26, value);
	}

	inline static int32_t get_offset_of_PresenceHeartbeatCoroutine_27() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___PresenceHeartbeatCoroutine_27)); }
	inline Il2CppObject * get_PresenceHeartbeatCoroutine_27() const { return ___PresenceHeartbeatCoroutine_27; }
	inline Il2CppObject ** get_address_of_PresenceHeartbeatCoroutine_27() { return &___PresenceHeartbeatCoroutine_27; }
	inline void set_PresenceHeartbeatCoroutine_27(Il2CppObject * value)
	{
		___PresenceHeartbeatCoroutine_27 = value;
		Il2CppCodeGenWriteBarrier(&___PresenceHeartbeatCoroutine_27, value);
	}

	inline static int32_t get_offset_of_PresenceHeartbeatTimeoutCoroutine_28() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___PresenceHeartbeatTimeoutCoroutine_28)); }
	inline Il2CppObject * get_PresenceHeartbeatTimeoutCoroutine_28() const { return ___PresenceHeartbeatTimeoutCoroutine_28; }
	inline Il2CppObject ** get_address_of_PresenceHeartbeatTimeoutCoroutine_28() { return &___PresenceHeartbeatTimeoutCoroutine_28; }
	inline void set_PresenceHeartbeatTimeoutCoroutine_28(Il2CppObject * value)
	{
		___PresenceHeartbeatTimeoutCoroutine_28 = value;
		Il2CppCodeGenWriteBarrier(&___PresenceHeartbeatTimeoutCoroutine_28, value);
	}

	inline static int32_t get_offset_of_HeartbeatCoroutine_29() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___HeartbeatCoroutine_29)); }
	inline Il2CppObject * get_HeartbeatCoroutine_29() const { return ___HeartbeatCoroutine_29; }
	inline Il2CppObject ** get_address_of_HeartbeatCoroutine_29() { return &___HeartbeatCoroutine_29; }
	inline void set_HeartbeatCoroutine_29(Il2CppObject * value)
	{
		___HeartbeatCoroutine_29 = value;
		Il2CppCodeGenWriteBarrier(&___HeartbeatCoroutine_29, value);
	}

	inline static int32_t get_offset_of_HeartbeatTimeoutCoroutine_30() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___HeartbeatTimeoutCoroutine_30)); }
	inline Il2CppObject * get_HeartbeatTimeoutCoroutine_30() const { return ___HeartbeatTimeoutCoroutine_30; }
	inline Il2CppObject ** get_address_of_HeartbeatTimeoutCoroutine_30() { return &___HeartbeatTimeoutCoroutine_30; }
	inline void set_HeartbeatTimeoutCoroutine_30(Il2CppObject * value)
	{
		___HeartbeatTimeoutCoroutine_30 = value;
		Il2CppCodeGenWriteBarrier(&___HeartbeatTimeoutCoroutine_30, value);
	}

	inline static int32_t get_offset_of_DelayRequestCoroutineHB_31() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___DelayRequestCoroutineHB_31)); }
	inline Il2CppObject * get_DelayRequestCoroutineHB_31() const { return ___DelayRequestCoroutineHB_31; }
	inline Il2CppObject ** get_address_of_DelayRequestCoroutineHB_31() { return &___DelayRequestCoroutineHB_31; }
	inline void set_DelayRequestCoroutineHB_31(Il2CppObject * value)
	{
		___DelayRequestCoroutineHB_31 = value;
		Il2CppCodeGenWriteBarrier(&___DelayRequestCoroutineHB_31, value);
	}

	inline static int32_t get_offset_of_DelayRequestCoroutinePHB_32() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___DelayRequestCoroutinePHB_32)); }
	inline Il2CppObject * get_DelayRequestCoroutinePHB_32() const { return ___DelayRequestCoroutinePHB_32; }
	inline Il2CppObject ** get_address_of_DelayRequestCoroutinePHB_32() { return &___DelayRequestCoroutinePHB_32; }
	inline void set_DelayRequestCoroutinePHB_32(Il2CppObject * value)
	{
		___DelayRequestCoroutinePHB_32 = value;
		Il2CppCodeGenWriteBarrier(&___DelayRequestCoroutinePHB_32, value);
	}

	inline static int32_t get_offset_of_DelayRequestCoroutineSub_33() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___DelayRequestCoroutineSub_33)); }
	inline Il2CppObject * get_DelayRequestCoroutineSub_33() const { return ___DelayRequestCoroutineSub_33; }
	inline Il2CppObject ** get_address_of_DelayRequestCoroutineSub_33() { return &___DelayRequestCoroutineSub_33; }
	inline void set_DelayRequestCoroutineSub_33(Il2CppObject * value)
	{
		___DelayRequestCoroutineSub_33 = value;
		Il2CppCodeGenWriteBarrier(&___DelayRequestCoroutineSub_33, value);
	}

	inline static int32_t get_offset_of_subscribeWww_34() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___subscribeWww_34)); }
	inline WWW_t2919945039 * get_subscribeWww_34() const { return ___subscribeWww_34; }
	inline WWW_t2919945039 ** get_address_of_subscribeWww_34() { return &___subscribeWww_34; }
	inline void set_subscribeWww_34(WWW_t2919945039 * value)
	{
		___subscribeWww_34 = value;
		Il2CppCodeGenWriteBarrier(&___subscribeWww_34, value);
	}

	inline static int32_t get_offset_of_heartbeatWww_35() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___heartbeatWww_35)); }
	inline WWW_t2919945039 * get_heartbeatWww_35() const { return ___heartbeatWww_35; }
	inline WWW_t2919945039 ** get_address_of_heartbeatWww_35() { return &___heartbeatWww_35; }
	inline void set_heartbeatWww_35(WWW_t2919945039 * value)
	{
		___heartbeatWww_35 = value;
		Il2CppCodeGenWriteBarrier(&___heartbeatWww_35, value);
	}

	inline static int32_t get_offset_of_presenceHeartbeatWww_36() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___presenceHeartbeatWww_36)); }
	inline WWW_t2919945039 * get_presenceHeartbeatWww_36() const { return ___presenceHeartbeatWww_36; }
	inline WWW_t2919945039 ** get_address_of_presenceHeartbeatWww_36() { return &___presenceHeartbeatWww_36; }
	inline void set_presenceHeartbeatWww_36(WWW_t2919945039 * value)
	{
		___presenceHeartbeatWww_36 = value;
		Il2CppCodeGenWriteBarrier(&___presenceHeartbeatWww_36, value);
	}

	inline static int32_t get_offset_of_nonSubscribeWww_37() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___nonSubscribeWww_37)); }
	inline WWW_t2919945039 * get_nonSubscribeWww_37() const { return ___nonSubscribeWww_37; }
	inline WWW_t2919945039 ** get_address_of_nonSubscribeWww_37() { return &___nonSubscribeWww_37; }
	inline void set_nonSubscribeWww_37(WWW_t2919945039 * value)
	{
		___nonSubscribeWww_37 = value;
		Il2CppCodeGenWriteBarrier(&___nonSubscribeWww_37, value);
	}

	inline static int32_t get_offset_of_subCoroutineComplete_38() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___subCoroutineComplete_38)); }
	inline EventHandler_1_t1880931879 * get_subCoroutineComplete_38() const { return ___subCoroutineComplete_38; }
	inline EventHandler_1_t1880931879 ** get_address_of_subCoroutineComplete_38() { return &___subCoroutineComplete_38; }
	inline void set_subCoroutineComplete_38(EventHandler_1_t1880931879 * value)
	{
		___subCoroutineComplete_38 = value;
		Il2CppCodeGenWriteBarrier(&___subCoroutineComplete_38, value);
	}

	inline static int32_t get_offset_of_nonSubCoroutineComplete_39() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___nonSubCoroutineComplete_39)); }
	inline EventHandler_1_t1880931879 * get_nonSubCoroutineComplete_39() const { return ___nonSubCoroutineComplete_39; }
	inline EventHandler_1_t1880931879 ** get_address_of_nonSubCoroutineComplete_39() { return &___nonSubCoroutineComplete_39; }
	inline void set_nonSubCoroutineComplete_39(EventHandler_1_t1880931879 * value)
	{
		___nonSubCoroutineComplete_39 = value;
		Il2CppCodeGenWriteBarrier(&___nonSubCoroutineComplete_39, value);
	}

	inline static int32_t get_offset_of_presenceHeartbeatCoroutineComplete_40() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___presenceHeartbeatCoroutineComplete_40)); }
	inline EventHandler_1_t1880931879 * get_presenceHeartbeatCoroutineComplete_40() const { return ___presenceHeartbeatCoroutineComplete_40; }
	inline EventHandler_1_t1880931879 ** get_address_of_presenceHeartbeatCoroutineComplete_40() { return &___presenceHeartbeatCoroutineComplete_40; }
	inline void set_presenceHeartbeatCoroutineComplete_40(EventHandler_1_t1880931879 * value)
	{
		___presenceHeartbeatCoroutineComplete_40 = value;
		Il2CppCodeGenWriteBarrier(&___presenceHeartbeatCoroutineComplete_40, value);
	}

	inline static int32_t get_offset_of_heartbeatCoroutineComplete_41() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___heartbeatCoroutineComplete_41)); }
	inline EventHandler_1_t1880931879 * get_heartbeatCoroutineComplete_41() const { return ___heartbeatCoroutineComplete_41; }
	inline EventHandler_1_t1880931879 ** get_address_of_heartbeatCoroutineComplete_41() { return &___heartbeatCoroutineComplete_41; }
	inline void set_heartbeatCoroutineComplete_41(EventHandler_1_t1880931879 * value)
	{
		___heartbeatCoroutineComplete_41 = value;
		Il2CppCodeGenWriteBarrier(&___heartbeatCoroutineComplete_41, value);
	}

	inline static int32_t get_offset_of_subscribeTimer_42() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___subscribeTimer_42)); }
	inline float get_subscribeTimer_42() const { return ___subscribeTimer_42; }
	inline float* get_address_of_subscribeTimer_42() { return &___subscribeTimer_42; }
	inline void set_subscribeTimer_42(float value)
	{
		___subscribeTimer_42 = value;
	}

	inline static int32_t get_offset_of_heartbeatTimer_43() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___heartbeatTimer_43)); }
	inline float get_heartbeatTimer_43() const { return ___heartbeatTimer_43; }
	inline float* get_address_of_heartbeatTimer_43() { return &___heartbeatTimer_43; }
	inline void set_heartbeatTimer_43(float value)
	{
		___heartbeatTimer_43 = value;
	}

	inline static int32_t get_offset_of_presenceHeartbeatTimer_44() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___presenceHeartbeatTimer_44)); }
	inline float get_presenceHeartbeatTimer_44() const { return ___presenceHeartbeatTimer_44; }
	inline float* get_address_of_presenceHeartbeatTimer_44() { return &___presenceHeartbeatTimer_44; }
	inline void set_presenceHeartbeatTimer_44(float value)
	{
		___presenceHeartbeatTimer_44 = value;
	}

	inline static int32_t get_offset_of_nonSubscribeTimer_45() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___nonSubscribeTimer_45)); }
	inline float get_nonSubscribeTimer_45() const { return ___nonSubscribeTimer_45; }
	inline float* get_address_of_nonSubscribeTimer_45() { return &___nonSubscribeTimer_45; }
	inline void set_nonSubscribeTimer_45(float value)
	{
		___nonSubscribeTimer_45 = value;
	}

	inline static int32_t get_offset_of_heartbeatPauseTimer_46() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___heartbeatPauseTimer_46)); }
	inline float get_heartbeatPauseTimer_46() const { return ___heartbeatPauseTimer_46; }
	inline float* get_address_of_heartbeatPauseTimer_46() { return &___heartbeatPauseTimer_46; }
	inline void set_heartbeatPauseTimer_46(float value)
	{
		___heartbeatPauseTimer_46 = value;
	}

	inline static int32_t get_offset_of_presenceHeartbeatPauseTimer_47() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___presenceHeartbeatPauseTimer_47)); }
	inline float get_presenceHeartbeatPauseTimer_47() const { return ___presenceHeartbeatPauseTimer_47; }
	inline float* get_address_of_presenceHeartbeatPauseTimer_47() { return &___presenceHeartbeatPauseTimer_47; }
	inline void set_presenceHeartbeatPauseTimer_47(float value)
	{
		___presenceHeartbeatPauseTimer_47 = value;
	}

	inline static int32_t get_offset_of_subscribePauseTimer_48() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358, ___subscribePauseTimer_48)); }
	inline float get_subscribePauseTimer_48() const { return ___subscribePauseTimer_48; }
	inline float* get_address_of_subscribePauseTimer_48() { return &___subscribePauseTimer_48; }
	inline void set_subscribePauseTimer_48(float value)
	{
		___subscribePauseTimer_48 = value;
	}
};

struct CoroutineClass_t2476503358_StaticFields
{
public:
	// System.String[][] PubNubMessaging.Core.CoroutineClass::_unused
	StringU5BU5DU5BU5D_t2190260861* ____unused_2;
	// System.Int32[][] PubNubMessaging.Core.CoroutineClass::_unused2
	Int32U5BU5DU5BU5D_t3750818532* ____unused2_3;
	// System.Int64[][] PubNubMessaging.Core.CoroutineClass::_unused3
	Int64U5BU5DU5BU5D_t1808128809* ____unused3_4;
	// System.Int16[][] PubNubMessaging.Core.CoroutineClass::_unused4
	Int16U5BU5DU5BU5D_t3712714918* ____unused4_5;
	// System.UInt16[][] PubNubMessaging.Core.CoroutineClass::_unused5
	UInt16U5BU5DU5BU5D_t880806807* ____unused5_6;
	// System.UInt64[][] PubNubMessaging.Core.CoroutineClass::_unused6
	UInt64U5BU5DU5BU5D_t595110526* ____unused6_7;
	// System.UInt32[][] PubNubMessaging.Core.CoroutineClass::_unused7
	UInt32U5BU5DU5BU5D_t1156922361* ____unused7_8;
	// System.Decimal[][] PubNubMessaging.Core.CoroutineClass::_unused8
	DecimalU5BU5DU5BU5D_t418021545* ____unused8_9;
	// System.Double[][] PubNubMessaging.Core.CoroutineClass::_unused9
	DoubleU5BU5DU5BU5D_t2187755765* ____unused9_10;
	// System.Boolean[][] PubNubMessaging.Core.CoroutineClass::_unused91
	BooleanU5BU5DU5BU5D_t2798867818* ____unused91_11;
	// System.Object[][] PubNubMessaging.Core.CoroutineClass::_unused92
	ObjectU5BU5DU5BU5D_t1712241747* ____unused92_12;
	// System.Int64[][] PubNubMessaging.Core.CoroutineClass::_unused10
	Int64U5BU5DU5BU5D_t1808128809* ____unused10_13;
	// System.Int32[][] PubNubMessaging.Core.CoroutineClass::_unused11
	Int32U5BU5DU5BU5D_t3750818532* ____unused11_14;
	// System.Single[][] PubNubMessaging.Core.CoroutineClass::_unused12
	SingleU5BU5DU5BU5D_t2115305192* ____unused12_15;
	// System.Decimal[][] PubNubMessaging.Core.CoroutineClass::_unused13
	DecimalU5BU5DU5BU5D_t418021545* ____unused13_16;
	// System.UInt32[][] PubNubMessaging.Core.CoroutineClass::_unused14
	UInt32U5BU5DU5BU5D_t1156922361* ____unused14_17;
	// System.UInt64[][] PubNubMessaging.Core.CoroutineClass::_unused15
	UInt64U5BU5DU5BU5D_t595110526* ____unused15_18;

public:
	inline static int32_t get_offset_of__unused_2() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358_StaticFields, ____unused_2)); }
	inline StringU5BU5DU5BU5D_t2190260861* get__unused_2() const { return ____unused_2; }
	inline StringU5BU5DU5BU5D_t2190260861** get_address_of__unused_2() { return &____unused_2; }
	inline void set__unused_2(StringU5BU5DU5BU5D_t2190260861* value)
	{
		____unused_2 = value;
		Il2CppCodeGenWriteBarrier(&____unused_2, value);
	}

	inline static int32_t get_offset_of__unused2_3() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358_StaticFields, ____unused2_3)); }
	inline Int32U5BU5DU5BU5D_t3750818532* get__unused2_3() const { return ____unused2_3; }
	inline Int32U5BU5DU5BU5D_t3750818532** get_address_of__unused2_3() { return &____unused2_3; }
	inline void set__unused2_3(Int32U5BU5DU5BU5D_t3750818532* value)
	{
		____unused2_3 = value;
		Il2CppCodeGenWriteBarrier(&____unused2_3, value);
	}

	inline static int32_t get_offset_of__unused3_4() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358_StaticFields, ____unused3_4)); }
	inline Int64U5BU5DU5BU5D_t1808128809* get__unused3_4() const { return ____unused3_4; }
	inline Int64U5BU5DU5BU5D_t1808128809** get_address_of__unused3_4() { return &____unused3_4; }
	inline void set__unused3_4(Int64U5BU5DU5BU5D_t1808128809* value)
	{
		____unused3_4 = value;
		Il2CppCodeGenWriteBarrier(&____unused3_4, value);
	}

	inline static int32_t get_offset_of__unused4_5() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358_StaticFields, ____unused4_5)); }
	inline Int16U5BU5DU5BU5D_t3712714918* get__unused4_5() const { return ____unused4_5; }
	inline Int16U5BU5DU5BU5D_t3712714918** get_address_of__unused4_5() { return &____unused4_5; }
	inline void set__unused4_5(Int16U5BU5DU5BU5D_t3712714918* value)
	{
		____unused4_5 = value;
		Il2CppCodeGenWriteBarrier(&____unused4_5, value);
	}

	inline static int32_t get_offset_of__unused5_6() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358_StaticFields, ____unused5_6)); }
	inline UInt16U5BU5DU5BU5D_t880806807* get__unused5_6() const { return ____unused5_6; }
	inline UInt16U5BU5DU5BU5D_t880806807** get_address_of__unused5_6() { return &____unused5_6; }
	inline void set__unused5_6(UInt16U5BU5DU5BU5D_t880806807* value)
	{
		____unused5_6 = value;
		Il2CppCodeGenWriteBarrier(&____unused5_6, value);
	}

	inline static int32_t get_offset_of__unused6_7() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358_StaticFields, ____unused6_7)); }
	inline UInt64U5BU5DU5BU5D_t595110526* get__unused6_7() const { return ____unused6_7; }
	inline UInt64U5BU5DU5BU5D_t595110526** get_address_of__unused6_7() { return &____unused6_7; }
	inline void set__unused6_7(UInt64U5BU5DU5BU5D_t595110526* value)
	{
		____unused6_7 = value;
		Il2CppCodeGenWriteBarrier(&____unused6_7, value);
	}

	inline static int32_t get_offset_of__unused7_8() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358_StaticFields, ____unused7_8)); }
	inline UInt32U5BU5DU5BU5D_t1156922361* get__unused7_8() const { return ____unused7_8; }
	inline UInt32U5BU5DU5BU5D_t1156922361** get_address_of__unused7_8() { return &____unused7_8; }
	inline void set__unused7_8(UInt32U5BU5DU5BU5D_t1156922361* value)
	{
		____unused7_8 = value;
		Il2CppCodeGenWriteBarrier(&____unused7_8, value);
	}

	inline static int32_t get_offset_of__unused8_9() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358_StaticFields, ____unused8_9)); }
	inline DecimalU5BU5DU5BU5D_t418021545* get__unused8_9() const { return ____unused8_9; }
	inline DecimalU5BU5DU5BU5D_t418021545** get_address_of__unused8_9() { return &____unused8_9; }
	inline void set__unused8_9(DecimalU5BU5DU5BU5D_t418021545* value)
	{
		____unused8_9 = value;
		Il2CppCodeGenWriteBarrier(&____unused8_9, value);
	}

	inline static int32_t get_offset_of__unused9_10() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358_StaticFields, ____unused9_10)); }
	inline DoubleU5BU5DU5BU5D_t2187755765* get__unused9_10() const { return ____unused9_10; }
	inline DoubleU5BU5DU5BU5D_t2187755765** get_address_of__unused9_10() { return &____unused9_10; }
	inline void set__unused9_10(DoubleU5BU5DU5BU5D_t2187755765* value)
	{
		____unused9_10 = value;
		Il2CppCodeGenWriteBarrier(&____unused9_10, value);
	}

	inline static int32_t get_offset_of__unused91_11() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358_StaticFields, ____unused91_11)); }
	inline BooleanU5BU5DU5BU5D_t2798867818* get__unused91_11() const { return ____unused91_11; }
	inline BooleanU5BU5DU5BU5D_t2798867818** get_address_of__unused91_11() { return &____unused91_11; }
	inline void set__unused91_11(BooleanU5BU5DU5BU5D_t2798867818* value)
	{
		____unused91_11 = value;
		Il2CppCodeGenWriteBarrier(&____unused91_11, value);
	}

	inline static int32_t get_offset_of__unused92_12() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358_StaticFields, ____unused92_12)); }
	inline ObjectU5BU5DU5BU5D_t1712241747* get__unused92_12() const { return ____unused92_12; }
	inline ObjectU5BU5DU5BU5D_t1712241747** get_address_of__unused92_12() { return &____unused92_12; }
	inline void set__unused92_12(ObjectU5BU5DU5BU5D_t1712241747* value)
	{
		____unused92_12 = value;
		Il2CppCodeGenWriteBarrier(&____unused92_12, value);
	}

	inline static int32_t get_offset_of__unused10_13() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358_StaticFields, ____unused10_13)); }
	inline Int64U5BU5DU5BU5D_t1808128809* get__unused10_13() const { return ____unused10_13; }
	inline Int64U5BU5DU5BU5D_t1808128809** get_address_of__unused10_13() { return &____unused10_13; }
	inline void set__unused10_13(Int64U5BU5DU5BU5D_t1808128809* value)
	{
		____unused10_13 = value;
		Il2CppCodeGenWriteBarrier(&____unused10_13, value);
	}

	inline static int32_t get_offset_of__unused11_14() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358_StaticFields, ____unused11_14)); }
	inline Int32U5BU5DU5BU5D_t3750818532* get__unused11_14() const { return ____unused11_14; }
	inline Int32U5BU5DU5BU5D_t3750818532** get_address_of__unused11_14() { return &____unused11_14; }
	inline void set__unused11_14(Int32U5BU5DU5BU5D_t3750818532* value)
	{
		____unused11_14 = value;
		Il2CppCodeGenWriteBarrier(&____unused11_14, value);
	}

	inline static int32_t get_offset_of__unused12_15() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358_StaticFields, ____unused12_15)); }
	inline SingleU5BU5DU5BU5D_t2115305192* get__unused12_15() const { return ____unused12_15; }
	inline SingleU5BU5DU5BU5D_t2115305192** get_address_of__unused12_15() { return &____unused12_15; }
	inline void set__unused12_15(SingleU5BU5DU5BU5D_t2115305192* value)
	{
		____unused12_15 = value;
		Il2CppCodeGenWriteBarrier(&____unused12_15, value);
	}

	inline static int32_t get_offset_of__unused13_16() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358_StaticFields, ____unused13_16)); }
	inline DecimalU5BU5DU5BU5D_t418021545* get__unused13_16() const { return ____unused13_16; }
	inline DecimalU5BU5DU5BU5D_t418021545** get_address_of__unused13_16() { return &____unused13_16; }
	inline void set__unused13_16(DecimalU5BU5DU5BU5D_t418021545* value)
	{
		____unused13_16 = value;
		Il2CppCodeGenWriteBarrier(&____unused13_16, value);
	}

	inline static int32_t get_offset_of__unused14_17() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358_StaticFields, ____unused14_17)); }
	inline UInt32U5BU5DU5BU5D_t1156922361* get__unused14_17() const { return ____unused14_17; }
	inline UInt32U5BU5DU5BU5D_t1156922361** get_address_of__unused14_17() { return &____unused14_17; }
	inline void set__unused14_17(UInt32U5BU5DU5BU5D_t1156922361* value)
	{
		____unused14_17 = value;
		Il2CppCodeGenWriteBarrier(&____unused14_17, value);
	}

	inline static int32_t get_offset_of__unused15_18() { return static_cast<int32_t>(offsetof(CoroutineClass_t2476503358_StaticFields, ____unused15_18)); }
	inline UInt64U5BU5DU5BU5D_t595110526* get__unused15_18() const { return ____unused15_18; }
	inline UInt64U5BU5DU5BU5D_t595110526** get_address_of__unused15_18() { return &____unused15_18; }
	inline void set__unused15_18(UInt64U5BU5DU5BU5D_t595110526* value)
	{
		____unused15_18 = value;
		Il2CppCodeGenWriteBarrier(&____unused15_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
