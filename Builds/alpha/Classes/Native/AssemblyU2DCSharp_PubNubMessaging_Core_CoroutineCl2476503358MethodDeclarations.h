﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.CoroutineClass
struct CoroutineClass_t2476503358;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"

// System.Void PubNubMessaging.Core.CoroutineClass::.ctor()
extern "C"  void CoroutineClass__ctor_m3898683198 (CoroutineClass_t2476503358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.CoroutineClass::add_SubCoroutineComplete(System.EventHandler`1<System.EventArgs>)
extern "C"  void CoroutineClass_add_SubCoroutineComplete_m1835498425 (CoroutineClass_t2476503358 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.CoroutineClass::remove_SubCoroutineComplete(System.EventHandler`1<System.EventArgs>)
extern "C"  void CoroutineClass_remove_SubCoroutineComplete_m600903876 (CoroutineClass_t2476503358 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.CoroutineClass::add_NonSubCoroutineComplete(System.EventHandler`1<System.EventArgs>)
extern "C"  void CoroutineClass_add_NonSubCoroutineComplete_m683202958 (CoroutineClass_t2476503358 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.CoroutineClass::remove_NonSubCoroutineComplete(System.EventHandler`1<System.EventArgs>)
extern "C"  void CoroutineClass_remove_NonSubCoroutineComplete_m200472425 (CoroutineClass_t2476503358 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.CoroutineClass::add_PresenceHeartbeatCoroutineComplete(System.EventHandler`1<System.EventArgs>)
extern "C"  void CoroutineClass_add_PresenceHeartbeatCoroutineComplete_m1553798936 (CoroutineClass_t2476503358 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.CoroutineClass::remove_PresenceHeartbeatCoroutineComplete(System.EventHandler`1<System.EventArgs>)
extern "C"  void CoroutineClass_remove_PresenceHeartbeatCoroutineComplete_m1482644035 (CoroutineClass_t2476503358 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.CoroutineClass::add_HeartbeatCoroutineComplete(System.EventHandler`1<System.EventArgs>)
extern "C"  void CoroutineClass_add_HeartbeatCoroutineComplete_m76062233 (CoroutineClass_t2476503358 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.CoroutineClass::remove_HeartbeatCoroutineComplete(System.EventHandler`1<System.EventArgs>)
extern "C"  void CoroutineClass_remove_HeartbeatCoroutineComplete_m853930674 (CoroutineClass_t2476503358 * __this, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.CoroutineClass::SetComplete(PubNubMessaging.Core.CurrentRequestType)
extern "C"  void CoroutineClass_SetComplete_m2061919568 (CoroutineClass_t2476503358 * __this, int32_t ___crt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.CoroutineClass::StopRunningCoroutines(PubNubMessaging.Core.CurrentRequestType)
extern "C"  void CoroutineClass_StopRunningCoroutines_m439252029 (CoroutineClass_t2476503358 * __this, int32_t ___crt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.CoroutineClass::CheckComplete(PubNubMessaging.Core.CurrentRequestType)
extern "C"  bool CoroutineClass_CheckComplete_m560524322 (CoroutineClass_t2476503358 * __this, int32_t ___crt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
