﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.CoroutineClass/<CheckTimeoutHeartbeat>c__Iterator8`1<System.Object>
struct U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t2997308713;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutHeartbeat>c__Iterator8`1<System.Object>::.ctor()
extern "C"  void U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1__ctor_m4000749742_gshared (U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t2997308713 * __this, const MethodInfo* method);
#define U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1__ctor_m4000749742(__this, method) ((  void (*) (U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t2997308713 *, const MethodInfo*))U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1__ctor_m4000749742_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.CoroutineClass/<CheckTimeoutHeartbeat>c__Iterator8`1<System.Object>::MoveNext()
extern "C"  bool U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_MoveNext_m3032790654_gshared (U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t2997308713 * __this, const MethodInfo* method);
#define U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_MoveNext_m3032790654(__this, method) ((  bool (*) (U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t2997308713 *, const MethodInfo*))U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_MoveNext_m3032790654_gshared)(__this, method)
// System.Object PubNubMessaging.Core.CoroutineClass/<CheckTimeoutHeartbeat>c__Iterator8`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m13828898_gshared (U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t2997308713 * __this, const MethodInfo* method);
#define U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m13828898(__this, method) ((  Il2CppObject * (*) (U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t2997308713 *, const MethodInfo*))U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m13828898_gshared)(__this, method)
// System.Object PubNubMessaging.Core.CoroutineClass/<CheckTimeoutHeartbeat>c__Iterator8`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_System_Collections_IEnumerator_get_Current_m2831832106_gshared (U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t2997308713 * __this, const MethodInfo* method);
#define U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_System_Collections_IEnumerator_get_Current_m2831832106(__this, method) ((  Il2CppObject * (*) (U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t2997308713 *, const MethodInfo*))U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_System_Collections_IEnumerator_get_Current_m2831832106_gshared)(__this, method)
// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutHeartbeat>c__Iterator8`1<System.Object>::Dispose()
extern "C"  void U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_Dispose_m2970090467_gshared (U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t2997308713 * __this, const MethodInfo* method);
#define U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_Dispose_m2970090467(__this, method) ((  void (*) (U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t2997308713 *, const MethodInfo*))U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_Dispose_m2970090467_gshared)(__this, method)
// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutHeartbeat>c__Iterator8`1<System.Object>::Reset()
extern "C"  void U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_Reset_m2035169677_gshared (U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t2997308713 * __this, const MethodInfo* method);
#define U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_Reset_m2035169677(__this, method) ((  void (*) (U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t2997308713 *, const MethodInfo*))U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_Reset_m2035169677_gshared)(__this, method)
