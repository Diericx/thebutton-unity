﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.CoroutineClass/<CheckTimeoutSub>c__Iterator5`1<System.Object>
struct U3CCheckTimeoutSubU3Ec__Iterator5_1_t3644537212;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutSub>c__Iterator5`1<System.Object>::.ctor()
extern "C"  void U3CCheckTimeoutSubU3Ec__Iterator5_1__ctor_m1337258217_gshared (U3CCheckTimeoutSubU3Ec__Iterator5_1_t3644537212 * __this, const MethodInfo* method);
#define U3CCheckTimeoutSubU3Ec__Iterator5_1__ctor_m1337258217(__this, method) ((  void (*) (U3CCheckTimeoutSubU3Ec__Iterator5_1_t3644537212 *, const MethodInfo*))U3CCheckTimeoutSubU3Ec__Iterator5_1__ctor_m1337258217_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.CoroutineClass/<CheckTimeoutSub>c__Iterator5`1<System.Object>::MoveNext()
extern "C"  bool U3CCheckTimeoutSubU3Ec__Iterator5_1_MoveNext_m761937791_gshared (U3CCheckTimeoutSubU3Ec__Iterator5_1_t3644537212 * __this, const MethodInfo* method);
#define U3CCheckTimeoutSubU3Ec__Iterator5_1_MoveNext_m761937791(__this, method) ((  bool (*) (U3CCheckTimeoutSubU3Ec__Iterator5_1_t3644537212 *, const MethodInfo*))U3CCheckTimeoutSubU3Ec__Iterator5_1_MoveNext_m761937791_gshared)(__this, method)
// System.Object PubNubMessaging.Core.CoroutineClass/<CheckTimeoutSub>c__Iterator5`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckTimeoutSubU3Ec__Iterator5_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2123449087_gshared (U3CCheckTimeoutSubU3Ec__Iterator5_1_t3644537212 * __this, const MethodInfo* method);
#define U3CCheckTimeoutSubU3Ec__Iterator5_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2123449087(__this, method) ((  Il2CppObject * (*) (U3CCheckTimeoutSubU3Ec__Iterator5_1_t3644537212 *, const MethodInfo*))U3CCheckTimeoutSubU3Ec__Iterator5_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2123449087_gshared)(__this, method)
// System.Object PubNubMessaging.Core.CoroutineClass/<CheckTimeoutSub>c__Iterator5`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckTimeoutSubU3Ec__Iterator5_1_System_Collections_IEnumerator_get_Current_m1780862263_gshared (U3CCheckTimeoutSubU3Ec__Iterator5_1_t3644537212 * __this, const MethodInfo* method);
#define U3CCheckTimeoutSubU3Ec__Iterator5_1_System_Collections_IEnumerator_get_Current_m1780862263(__this, method) ((  Il2CppObject * (*) (U3CCheckTimeoutSubU3Ec__Iterator5_1_t3644537212 *, const MethodInfo*))U3CCheckTimeoutSubU3Ec__Iterator5_1_System_Collections_IEnumerator_get_Current_m1780862263_gshared)(__this, method)
// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutSub>c__Iterator5`1<System.Object>::Dispose()
extern "C"  void U3CCheckTimeoutSubU3Ec__Iterator5_1_Dispose_m2444302366_gshared (U3CCheckTimeoutSubU3Ec__Iterator5_1_t3644537212 * __this, const MethodInfo* method);
#define U3CCheckTimeoutSubU3Ec__Iterator5_1_Dispose_m2444302366(__this, method) ((  void (*) (U3CCheckTimeoutSubU3Ec__Iterator5_1_t3644537212 *, const MethodInfo*))U3CCheckTimeoutSubU3Ec__Iterator5_1_Dispose_m2444302366_gshared)(__this, method)
// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutSub>c__Iterator5`1<System.Object>::Reset()
extern "C"  void U3CCheckTimeoutSubU3Ec__Iterator5_1_Reset_m2412827112_gshared (U3CCheckTimeoutSubU3Ec__Iterator5_1_t3644537212 * __this, const MethodInfo* method);
#define U3CCheckTimeoutSubU3Ec__Iterator5_1_Reset_m2412827112(__this, method) ((  void (*) (U3CCheckTimeoutSubU3Ec__Iterator5_1_t3644537212 *, const MethodInfo*))U3CCheckTimeoutSubU3Ec__Iterator5_1_Reset_m2412827112_gshared)(__this, method)
