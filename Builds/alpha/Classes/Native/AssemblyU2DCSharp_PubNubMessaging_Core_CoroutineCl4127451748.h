﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PubNubMessaging.Core.CoroutineParams`1<System.Object>
struct CoroutineParams_1_t3789821357;
// UnityEngine.WWW
struct WWW_t2919945039;
// PubNubMessaging.Core.CoroutineClass
struct CoroutineClass_t2476503358;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1<System.Object>
struct  U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748  : public Il2CppObject
{
public:
	// PubNubMessaging.Core.CoroutineParams`1<T> PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1::cp
	CoroutineParams_1_t3789821357 * ___cp_0;
	// UnityEngine.WWW PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_1;
	// PubNubMessaging.Core.CoroutineClass PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1::$this
	CoroutineClass_t2476503358 * ___U24this_2;
	// System.Object PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1::$disposing
	bool ___U24disposing_4;
	// System.Int32 PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_cp_0() { return static_cast<int32_t>(offsetof(U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748, ___cp_0)); }
	inline CoroutineParams_1_t3789821357 * get_cp_0() const { return ___cp_0; }
	inline CoroutineParams_1_t3789821357 ** get_address_of_cp_0() { return &___cp_0; }
	inline void set_cp_0(CoroutineParams_1_t3789821357 * value)
	{
		___cp_0 = value;
		Il2CppCodeGenWriteBarrier(&___cp_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748, ___U3CwwwU3E__0_1)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748, ___U24this_2)); }
	inline CoroutineClass_t2476503358 * get_U24this_2() const { return ___U24this_2; }
	inline CoroutineClass_t2476503358 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(CoroutineClass_t2476503358 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
