﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1<System.Object>
struct U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1<System.Object>::.ctor()
extern "C"  void U3CSendRequestNonSubU3Ec__Iterator2_1__ctor_m4150949169_gshared (U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748 * __this, const MethodInfo* method);
#define U3CSendRequestNonSubU3Ec__Iterator2_1__ctor_m4150949169(__this, method) ((  void (*) (U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748 *, const MethodInfo*))U3CSendRequestNonSubU3Ec__Iterator2_1__ctor_m4150949169_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1<System.Object>::MoveNext()
extern "C"  bool U3CSendRequestNonSubU3Ec__Iterator2_1_MoveNext_m4027539895_gshared (U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748 * __this, const MethodInfo* method);
#define U3CSendRequestNonSubU3Ec__Iterator2_1_MoveNext_m4027539895(__this, method) ((  bool (*) (U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748 *, const MethodInfo*))U3CSendRequestNonSubU3Ec__Iterator2_1_MoveNext_m4027539895_gshared)(__this, method)
// System.Object PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendRequestNonSubU3Ec__Iterator2_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1805857471_gshared (U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748 * __this, const MethodInfo* method);
#define U3CSendRequestNonSubU3Ec__Iterator2_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1805857471(__this, method) ((  Il2CppObject * (*) (U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748 *, const MethodInfo*))U3CSendRequestNonSubU3Ec__Iterator2_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1805857471_gshared)(__this, method)
// System.Object PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendRequestNonSubU3Ec__Iterator2_1_System_Collections_IEnumerator_get_Current_m3649035015_gshared (U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748 * __this, const MethodInfo* method);
#define U3CSendRequestNonSubU3Ec__Iterator2_1_System_Collections_IEnumerator_get_Current_m3649035015(__this, method) ((  Il2CppObject * (*) (U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748 *, const MethodInfo*))U3CSendRequestNonSubU3Ec__Iterator2_1_System_Collections_IEnumerator_get_Current_m3649035015_gshared)(__this, method)
// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1<System.Object>::Dispose()
extern "C"  void U3CSendRequestNonSubU3Ec__Iterator2_1_Dispose_m3207820422_gshared (U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748 * __this, const MethodInfo* method);
#define U3CSendRequestNonSubU3Ec__Iterator2_1_Dispose_m3207820422(__this, method) ((  void (*) (U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748 *, const MethodInfo*))U3CSendRequestNonSubU3Ec__Iterator2_1_Dispose_m3207820422_gshared)(__this, method)
// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1<System.Object>::Reset()
extern "C"  void U3CSendRequestNonSubU3Ec__Iterator2_1_Reset_m1576087936_gshared (U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748 * __this, const MethodInfo* method);
#define U3CSendRequestNonSubU3Ec__Iterator2_1_Reset_m1576087936(__this, method) ((  void (*) (U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748 *, const MethodInfo*))U3CSendRequestNonSubU3Ec__Iterator2_1_Reset_m1576087936_gshared)(__this, method)
