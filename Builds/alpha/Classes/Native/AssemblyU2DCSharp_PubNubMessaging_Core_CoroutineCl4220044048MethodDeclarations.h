﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.CoroutineClass/<SendRequestSub>c__Iterator1`1<System.Object>
struct U3CSendRequestSubU3Ec__Iterator1_1_t4220044048;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestSub>c__Iterator1`1<System.Object>::.ctor()
extern "C"  void U3CSendRequestSubU3Ec__Iterator1_1__ctor_m3574381273_gshared (U3CSendRequestSubU3Ec__Iterator1_1_t4220044048 * __this, const MethodInfo* method);
#define U3CSendRequestSubU3Ec__Iterator1_1__ctor_m3574381273(__this, method) ((  void (*) (U3CSendRequestSubU3Ec__Iterator1_1_t4220044048 *, const MethodInfo*))U3CSendRequestSubU3Ec__Iterator1_1__ctor_m3574381273_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.CoroutineClass/<SendRequestSub>c__Iterator1`1<System.Object>::MoveNext()
extern "C"  bool U3CSendRequestSubU3Ec__Iterator1_1_MoveNext_m1609050287_gshared (U3CSendRequestSubU3Ec__Iterator1_1_t4220044048 * __this, const MethodInfo* method);
#define U3CSendRequestSubU3Ec__Iterator1_1_MoveNext_m1609050287(__this, method) ((  bool (*) (U3CSendRequestSubU3Ec__Iterator1_1_t4220044048 *, const MethodInfo*))U3CSendRequestSubU3Ec__Iterator1_1_MoveNext_m1609050287_gshared)(__this, method)
// System.Object PubNubMessaging.Core.CoroutineClass/<SendRequestSub>c__Iterator1`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendRequestSubU3Ec__Iterator1_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2997727339_gshared (U3CSendRequestSubU3Ec__Iterator1_1_t4220044048 * __this, const MethodInfo* method);
#define U3CSendRequestSubU3Ec__Iterator1_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2997727339(__this, method) ((  Il2CppObject * (*) (U3CSendRequestSubU3Ec__Iterator1_1_t4220044048 *, const MethodInfo*))U3CSendRequestSubU3Ec__Iterator1_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2997727339_gshared)(__this, method)
// System.Object PubNubMessaging.Core.CoroutineClass/<SendRequestSub>c__Iterator1`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendRequestSubU3Ec__Iterator1_1_System_Collections_IEnumerator_get_Current_m1369846019_gshared (U3CSendRequestSubU3Ec__Iterator1_1_t4220044048 * __this, const MethodInfo* method);
#define U3CSendRequestSubU3Ec__Iterator1_1_System_Collections_IEnumerator_get_Current_m1369846019(__this, method) ((  Il2CppObject * (*) (U3CSendRequestSubU3Ec__Iterator1_1_t4220044048 *, const MethodInfo*))U3CSendRequestSubU3Ec__Iterator1_1_System_Collections_IEnumerator_get_Current_m1369846019_gshared)(__this, method)
// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestSub>c__Iterator1`1<System.Object>::Dispose()
extern "C"  void U3CSendRequestSubU3Ec__Iterator1_1_Dispose_m1923455418_gshared (U3CSendRequestSubU3Ec__Iterator1_1_t4220044048 * __this, const MethodInfo* method);
#define U3CSendRequestSubU3Ec__Iterator1_1_Dispose_m1923455418(__this, method) ((  void (*) (U3CSendRequestSubU3Ec__Iterator1_1_t4220044048 *, const MethodInfo*))U3CSendRequestSubU3Ec__Iterator1_1_Dispose_m1923455418_gshared)(__this, method)
// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestSub>c__Iterator1`1<System.Object>::Reset()
extern "C"  void U3CSendRequestSubU3Ec__Iterator1_1_Reset_m2352479036_gshared (U3CSendRequestSubU3Ec__Iterator1_1_t4220044048 * __this, const MethodInfo* method);
#define U3CSendRequestSubU3Ec__Iterator1_1_Reset_m2352479036(__this, method) ((  void (*) (U3CSendRequestSubU3Ec__Iterator1_1_t4220044048 *, const MethodInfo*))U3CSendRequestSubU3Ec__Iterator1_1_Reset_m2352479036_gshared)(__this, method)
