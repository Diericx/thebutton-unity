﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.CoroutineClass/<CheckTimeoutPresenceHeartbeat>c__Iterator7`1<System.Object>
struct U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t163451287;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutPresenceHeartbeat>c__Iterator7`1<System.Object>::.ctor()
extern "C"  void U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1__ctor_m1342820818_gshared (U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t163451287 * __this, const MethodInfo* method);
#define U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1__ctor_m1342820818(__this, method) ((  void (*) (U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t163451287 *, const MethodInfo*))U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1__ctor_m1342820818_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.CoroutineClass/<CheckTimeoutPresenceHeartbeat>c__Iterator7`1<System.Object>::MoveNext()
extern "C"  bool U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_MoveNext_m90650718_gshared (U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t163451287 * __this, const MethodInfo* method);
#define U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_MoveNext_m90650718(__this, method) ((  bool (*) (U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t163451287 *, const MethodInfo*))U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_MoveNext_m90650718_gshared)(__this, method)
// System.Object PubNubMessaging.Core.CoroutineClass/<CheckTimeoutPresenceHeartbeat>c__Iterator7`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3648101918_gshared (U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t163451287 * __this, const MethodInfo* method);
#define U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3648101918(__this, method) ((  Il2CppObject * (*) (U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t163451287 *, const MethodInfo*))U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3648101918_gshared)(__this, method)
// System.Object PubNubMessaging.Core.CoroutineClass/<CheckTimeoutPresenceHeartbeat>c__Iterator7`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_System_Collections_IEnumerator_get_Current_m1738581670_gshared (U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t163451287 * __this, const MethodInfo* method);
#define U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_System_Collections_IEnumerator_get_Current_m1738581670(__this, method) ((  Il2CppObject * (*) (U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t163451287 *, const MethodInfo*))U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_System_Collections_IEnumerator_get_Current_m1738581670_gshared)(__this, method)
// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutPresenceHeartbeat>c__Iterator7`1<System.Object>::Dispose()
extern "C"  void U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_Dispose_m4258893909_gshared (U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t163451287 * __this, const MethodInfo* method);
#define U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_Dispose_m4258893909(__this, method) ((  void (*) (U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t163451287 *, const MethodInfo*))U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_Dispose_m4258893909_gshared)(__this, method)
// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutPresenceHeartbeat>c__Iterator7`1<System.Object>::Reset()
extern "C"  void U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_Reset_m2783278763_gshared (U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t163451287 * __this, const MethodInfo* method);
#define U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_Reset_m2783278763(__this, method) ((  void (*) (U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t163451287 *, const MethodInfo*))U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_Reset_m2783278763_gshared)(__this, method)
