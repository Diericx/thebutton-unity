﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Type
struct Type_t;
// PubNubMessaging.Core.RequestState`1<System.Object>
struct RequestState_1_t8940997;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.CoroutineParams`1<System.Object>
struct  CoroutineParams_1_t3789821357  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Core.CoroutineParams`1::url
	String_t* ___url_0;
	// System.Int32 PubNubMessaging.Core.CoroutineParams`1::timeout
	int32_t ___timeout_1;
	// System.Int32 PubNubMessaging.Core.CoroutineParams`1::pause
	int32_t ___pause_2;
	// PubNubMessaging.Core.CurrentRequestType PubNubMessaging.Core.CoroutineParams`1::crt
	int32_t ___crt_3;
	// System.Type PubNubMessaging.Core.CoroutineParams`1::typeParameterType
	Type_t * ___typeParameterType_4;
	// PubNubMessaging.Core.RequestState`1<T> PubNubMessaging.Core.CoroutineParams`1::requestState
	RequestState_1_t8940997 * ___requestState_5;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(CoroutineParams_1_t3789821357, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_timeout_1() { return static_cast<int32_t>(offsetof(CoroutineParams_1_t3789821357, ___timeout_1)); }
	inline int32_t get_timeout_1() const { return ___timeout_1; }
	inline int32_t* get_address_of_timeout_1() { return &___timeout_1; }
	inline void set_timeout_1(int32_t value)
	{
		___timeout_1 = value;
	}

	inline static int32_t get_offset_of_pause_2() { return static_cast<int32_t>(offsetof(CoroutineParams_1_t3789821357, ___pause_2)); }
	inline int32_t get_pause_2() const { return ___pause_2; }
	inline int32_t* get_address_of_pause_2() { return &___pause_2; }
	inline void set_pause_2(int32_t value)
	{
		___pause_2 = value;
	}

	inline static int32_t get_offset_of_crt_3() { return static_cast<int32_t>(offsetof(CoroutineParams_1_t3789821357, ___crt_3)); }
	inline int32_t get_crt_3() const { return ___crt_3; }
	inline int32_t* get_address_of_crt_3() { return &___crt_3; }
	inline void set_crt_3(int32_t value)
	{
		___crt_3 = value;
	}

	inline static int32_t get_offset_of_typeParameterType_4() { return static_cast<int32_t>(offsetof(CoroutineParams_1_t3789821357, ___typeParameterType_4)); }
	inline Type_t * get_typeParameterType_4() const { return ___typeParameterType_4; }
	inline Type_t ** get_address_of_typeParameterType_4() { return &___typeParameterType_4; }
	inline void set_typeParameterType_4(Type_t * value)
	{
		___typeParameterType_4 = value;
		Il2CppCodeGenWriteBarrier(&___typeParameterType_4, value);
	}

	inline static int32_t get_offset_of_requestState_5() { return static_cast<int32_t>(offsetof(CoroutineParams_1_t3789821357, ___requestState_5)); }
	inline RequestState_1_t8940997 * get_requestState_5() const { return ___requestState_5; }
	inline RequestState_1_t8940997 ** get_address_of_requestState_5() { return &___requestState_5; }
	inline void set_requestState_5(RequestState_1_t8940997 * value)
	{
		___requestState_5 = value;
		Il2CppCodeGenWriteBarrier(&___requestState_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
