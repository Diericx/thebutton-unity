﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.CoroutineParams`1<System.Object>
struct CoroutineParams_1_t3789821357;
// System.String
struct String_t;
// System.Type
struct Type_t;
// PubNubMessaging.Core.RequestState`1<System.Object>
struct RequestState_1_t8940997;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void PubNubMessaging.Core.CoroutineParams`1<System.Object>::.ctor(System.String,System.Int32,System.Int32,PubNubMessaging.Core.CurrentRequestType,System.Type,PubNubMessaging.Core.RequestState`1<T>)
extern "C"  void CoroutineParams_1__ctor_m1002892099_gshared (CoroutineParams_1_t3789821357 * __this, String_t* ___url0, int32_t ___timeout1, int32_t ___pause2, int32_t ___crt3, Type_t * ___typeParameterType4, RequestState_1_t8940997 * ___requestState5, const MethodInfo* method);
#define CoroutineParams_1__ctor_m1002892099(__this, ___url0, ___timeout1, ___pause2, ___crt3, ___typeParameterType4, ___requestState5, method) ((  void (*) (CoroutineParams_1_t3789821357 *, String_t*, int32_t, int32_t, int32_t, Type_t *, RequestState_1_t8940997 *, const MethodInfo*))CoroutineParams_1__ctor_m1002892099_gshared)(__this, ___url0, ___timeout1, ___pause2, ___crt3, ___typeParameterType4, ___requestState5, method)
