﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.Counter
struct  Counter_t3542383138  : public Il2CppObject
{
public:
	// System.UInt32 PubNubMessaging.Core.Counter::current
	uint32_t ___current_0;
	// System.Object PubNubMessaging.Core.Counter::syncRoot
	Il2CppObject * ___syncRoot_1;

public:
	inline static int32_t get_offset_of_current_0() { return static_cast<int32_t>(offsetof(Counter_t3542383138, ___current_0)); }
	inline uint32_t get_current_0() const { return ___current_0; }
	inline uint32_t* get_address_of_current_0() { return &___current_0; }
	inline void set_current_0(uint32_t value)
	{
		___current_0 = value;
	}

	inline static int32_t get_offset_of_syncRoot_1() { return static_cast<int32_t>(offsetof(Counter_t3542383138, ___syncRoot_1)); }
	inline Il2CppObject * get_syncRoot_1() const { return ___syncRoot_1; }
	inline Il2CppObject ** get_address_of_syncRoot_1() { return &___syncRoot_1; }
	inline void set_syncRoot_1(Il2CppObject * value)
	{
		___syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier(&___syncRoot_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
