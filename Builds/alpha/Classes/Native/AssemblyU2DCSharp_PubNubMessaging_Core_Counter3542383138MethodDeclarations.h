﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.Counter
struct Counter_t3542383138;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Core.Counter::.ctor()
extern "C"  void Counter__ctor_m659701090 (Counter_t3542383138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PubNubMessaging.Core.Counter::NextValue()
extern "C"  uint32_t Counter_NextValue_m796254471 (Counter_t3542383138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Counter::Reset()
extern "C"  void Counter_Reset_m2382758041 (Counter_t3542383138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
