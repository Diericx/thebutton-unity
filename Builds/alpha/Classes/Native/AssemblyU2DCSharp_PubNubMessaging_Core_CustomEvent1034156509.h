﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Core.RequestState`1<System.Object>
struct RequestState_1_t8940997;

#include "mscorlib_System_EventArgs3289624707.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.CustomEventArgs`1<System.Object>
struct  CustomEventArgs_1_t1034156509  : public EventArgs_t3289624707
{
public:
	// System.String PubNubMessaging.Core.CustomEventArgs`1::Message
	String_t* ___Message_1;
	// PubNubMessaging.Core.RequestState`1<T> PubNubMessaging.Core.CustomEventArgs`1::PubnubRequestState
	RequestState_1_t8940997 * ___PubnubRequestState_2;
	// System.Boolean PubNubMessaging.Core.CustomEventArgs`1::IsError
	bool ___IsError_3;
	// System.Boolean PubNubMessaging.Core.CustomEventArgs`1::IsTimeout
	bool ___IsTimeout_4;
	// PubNubMessaging.Core.CurrentRequestType PubNubMessaging.Core.CustomEventArgs`1::CurrRequestType
	int32_t ___CurrRequestType_5;

public:
	inline static int32_t get_offset_of_Message_1() { return static_cast<int32_t>(offsetof(CustomEventArgs_1_t1034156509, ___Message_1)); }
	inline String_t* get_Message_1() const { return ___Message_1; }
	inline String_t** get_address_of_Message_1() { return &___Message_1; }
	inline void set_Message_1(String_t* value)
	{
		___Message_1 = value;
		Il2CppCodeGenWriteBarrier(&___Message_1, value);
	}

	inline static int32_t get_offset_of_PubnubRequestState_2() { return static_cast<int32_t>(offsetof(CustomEventArgs_1_t1034156509, ___PubnubRequestState_2)); }
	inline RequestState_1_t8940997 * get_PubnubRequestState_2() const { return ___PubnubRequestState_2; }
	inline RequestState_1_t8940997 ** get_address_of_PubnubRequestState_2() { return &___PubnubRequestState_2; }
	inline void set_PubnubRequestState_2(RequestState_1_t8940997 * value)
	{
		___PubnubRequestState_2 = value;
		Il2CppCodeGenWriteBarrier(&___PubnubRequestState_2, value);
	}

	inline static int32_t get_offset_of_IsError_3() { return static_cast<int32_t>(offsetof(CustomEventArgs_1_t1034156509, ___IsError_3)); }
	inline bool get_IsError_3() const { return ___IsError_3; }
	inline bool* get_address_of_IsError_3() { return &___IsError_3; }
	inline void set_IsError_3(bool value)
	{
		___IsError_3 = value;
	}

	inline static int32_t get_offset_of_IsTimeout_4() { return static_cast<int32_t>(offsetof(CustomEventArgs_1_t1034156509, ___IsTimeout_4)); }
	inline bool get_IsTimeout_4() const { return ___IsTimeout_4; }
	inline bool* get_address_of_IsTimeout_4() { return &___IsTimeout_4; }
	inline void set_IsTimeout_4(bool value)
	{
		___IsTimeout_4 = value;
	}

	inline static int32_t get_offset_of_CurrRequestType_5() { return static_cast<int32_t>(offsetof(CustomEventArgs_1_t1034156509, ___CurrRequestType_5)); }
	inline int32_t get_CurrRequestType_5() const { return ___CurrRequestType_5; }
	inline int32_t* get_address_of_CurrRequestType_5() { return &___CurrRequestType_5; }
	inline void set_CurrRequestType_5(int32_t value)
	{
		___CurrRequestType_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
