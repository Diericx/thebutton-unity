﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.CustomEventArgs`1<System.Object>
struct CustomEventArgs_1_t1034156509;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Core.CustomEventArgs`1<System.Object>::.ctor()
extern "C"  void CustomEventArgs_1__ctor_m635681693_gshared (CustomEventArgs_1_t1034156509 * __this, const MethodInfo* method);
#define CustomEventArgs_1__ctor_m635681693(__this, method) ((  void (*) (CustomEventArgs_1_t1034156509 *, const MethodInfo*))CustomEventArgs_1__ctor_m635681693_gshared)(__this, method)
