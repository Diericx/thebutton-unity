﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.ExceptionHandlers
struct  ExceptionHandlers_t3031412530  : public Il2CppObject
{
public:

public:
};

struct ExceptionHandlers_t3031412530_StaticFields
{
public:
	// System.EventHandler`1<System.EventArgs> PubNubMessaging.Core.ExceptionHandlers::multiplexException
	EventHandler_1_t1880931879 * ___multiplexException_0;

public:
	inline static int32_t get_offset_of_multiplexException_0() { return static_cast<int32_t>(offsetof(ExceptionHandlers_t3031412530_StaticFields, ___multiplexException_0)); }
	inline EventHandler_1_t1880931879 * get_multiplexException_0() const { return ___multiplexException_0; }
	inline EventHandler_1_t1880931879 ** get_address_of_multiplexException_0() { return &___multiplexException_0; }
	inline void set_multiplexException_0(EventHandler_1_t1880931879 * value)
	{
		___multiplexException_0 = value;
		Il2CppCodeGenWriteBarrier(&___multiplexException_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
