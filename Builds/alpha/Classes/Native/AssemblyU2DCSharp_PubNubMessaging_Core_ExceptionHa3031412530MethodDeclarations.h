﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.ExceptionHandlers
struct ExceptionHandlers_t3031412530;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1880931879;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Core.ExceptionHandlers::.ctor()
extern "C"  void ExceptionHandlers__ctor_m441775742 (ExceptionHandlers_t3031412530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.ExceptionHandlers::add_MultiplexException(System.EventHandler`1<System.EventArgs>)
extern "C"  void ExceptionHandlers_add_MultiplexException_m1489336029 (Il2CppObject * __this /* static, unused */, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.ExceptionHandlers::remove_MultiplexException(System.EventHandler`1<System.EventArgs>)
extern "C"  void ExceptionHandlers_remove_MultiplexException_m4239810430 (Il2CppObject * __this /* static, unused */, EventHandler_1_t1880931879 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
