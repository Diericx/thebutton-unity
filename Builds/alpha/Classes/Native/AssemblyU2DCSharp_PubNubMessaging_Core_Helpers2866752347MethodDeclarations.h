﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.TimetokenMetadata
struct TimetokenMetadata_t3476199713;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>
struct List_1_t3108551771;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>
struct List_1_t2635275738;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// System.String[]
struct StringU5BU5D_t1642385972;
// PubNubMessaging.Core.IJsonPluggableLibrary
struct IJsonPluggableLibrary_t1579330875;
// PubNubMessaging.Core.PubnubCrypto
struct PubnubCrypto_t3412661541;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// PubNubMessaging.Core.SubscribeMessage
struct SubscribeMessage_t3739430639;
// PubNubMessaging.Core.PNMessageResult
struct PNMessageResult_t756120242;
// PubNubMessaging.Core.PNPresenceEvent
struct PNPresenceEvent_t3577011079;
// PubNubMessaging.Core.PNPresenceEventResult
struct PNPresenceEventResult_t939915784;
// PubNubMessaging.Core.ChannelEntity
struct ChannelEntity_t3266154606;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubCrypt3412661541.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubErrorF397889342.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_SubscribeMe3739430639.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PNMessageRes756120242.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PNPresenceEv939915784.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelEnti3266154606.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubError2349065665.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ResponseTyp1248099179.h"

// PubNubMessaging.Core.TimetokenMetadata PubNubMessaging.Core.Helpers::CreateTimetokenMetadata(System.Object,System.String)
extern "C"  TimetokenMetadata_t3476199713 * Helpers_CreateTimetokenMetadata_m3520316965 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___timeTokenDataObject0, String_t* ___whichTT1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Helpers::AddToSubscribeMessageList(System.Object,System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>&)
extern "C"  void Helpers_AddToSubscribeMessageList_m2743554980 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___dictObject0, List_1_t3108551771 ** ___subscribeMessages1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage> PubNubMessaging.Core.Helpers::CreateListOfSubscribeMessage(System.Object)
extern "C"  List_1_t3108551771 * Helpers_CreateListOfSubscribeMessage_m3870070886 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.Helpers::BuildJsonUserState(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  String_t* Helpers_BuildJsonUserState_m750256651 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___userStateDictionary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.Helpers::BuildJsonUserState(System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>)
extern "C"  String_t* Helpers_BuildJsonUserState_m2615698005 (Il2CppObject * __this /* static, unused */, List_1_t2635275738 * ___ce0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.Helpers::GetNamesFromChannelEntities(System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>,System.Boolean)
extern "C"  String_t* Helpers_GetNamesFromChannelEntities_m3925475410 (Il2CppObject * __this /* static, unused */, List_1_t2635275738 * ___channelEntities0, bool ___isChannelGroup1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.Helpers::GetNamesFromChannelEntities(System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>)
extern "C"  String_t* Helpers_GetNamesFromChannelEntities_m2607970179 (Il2CppObject * __this /* static, unused */, List_1_t2635275738 * ___channelEntities0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.String> PubNubMessaging.Core.Helpers::GetDuplicates(System.String[])
extern "C"  Il2CppObject* Helpers_GetDuplicates_m3156930473 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___rawChannels0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.Helpers::JsonEncodePublishMsg(System.Object,System.String,PubNubMessaging.Core.IJsonPluggableLibrary)
extern "C"  String_t* Helpers_JsonEncodePublishMsg_m1658662340 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___originalMessage0, String_t* ___cipherKey1, Il2CppObject * ___jsonPluggableLibrary2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Core.Helpers::DecodeMessage(PubNubMessaging.Core.PubnubCrypto,System.Object,System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>,PubNubMessaging.Core.IJsonPluggableLibrary,PubNubMessaging.Core.PubnubErrorFilter/Level)
extern "C"  Il2CppObject * Helpers_DecodeMessage_m4199652636 (Il2CppObject * __this /* static, unused */, PubnubCrypto_t3412661541 * ___aes0, Il2CppObject * ___element1, List_1_t2635275738 * ___channelEntities2, Il2CppObject * ___jsonPluggableLibrary3, int32_t ___errorLevel4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> PubNubMessaging.Core.Helpers::DecryptCipheredMessage(System.Collections.Generic.List`1<System.Object>,System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>,System.String,PubNubMessaging.Core.IJsonPluggableLibrary,PubNubMessaging.Core.PubnubErrorFilter/Level)
extern "C"  List_1_t2058570427 * Helpers_DecryptCipheredMessage_m747276913 (Il2CppObject * __this /* static, unused */, List_1_t2058570427 * ___message0, List_1_t2635275738 * ___channelEntities1, String_t* ___cipherKey2, Il2CppObject * ___jsonPluggableLibrary3, int32_t ___errorLevel4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> PubNubMessaging.Core.Helpers::DecryptNonCipheredMessage(System.Collections.Generic.List`1<System.Object>)
extern "C"  List_1_t2058570427 * Helpers_DecryptNonCipheredMessage_m3711997955 (Il2CppObject * __this /* static, unused */, List_1_t2058570427 * ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> PubNubMessaging.Core.Helpers::DecodeDecryptLoop(System.Collections.Generic.List`1<System.Object>,System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>,System.String,PubNubMessaging.Core.IJsonPluggableLibrary,PubNubMessaging.Core.PubnubErrorFilter/Level)
extern "C"  List_1_t2058570427 * Helpers_DecodeDecryptLoop_m1345882518 (Il2CppObject * __this /* static, unused */, List_1_t2058570427 * ___message0, List_1_t2635275738 * ___channelEntities1, String_t* ___cipherKey2, Il2CppObject * ___jsonPluggableLibrary3, int32_t ___errorLevel4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> PubNubMessaging.Core.Helpers::DeserializeAndAddToResult(System.String,System.String,PubNubMessaging.Core.IJsonPluggableLibrary,System.Boolean)
extern "C"  List_1_t2058570427 * Helpers_DeserializeAndAddToResult_m2855542954 (Il2CppObject * __this /* static, unused */, String_t* ___jsonString0, String_t* ___multiChannel1, Il2CppObject * ___jsonPluggableLibrary2, bool ___addChannel3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Helpers::CreatePNMessageResult(PubNubMessaging.Core.SubscribeMessage,PubNubMessaging.Core.PNMessageResult&)
extern "C"  void Helpers_CreatePNMessageResult_m1294314998 (Il2CppObject * __this /* static, unused */, SubscribeMessage_t3739430639 * ___subscribeMessage0, PNMessageResult_t756120242 ** ___messageResult1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.PNPresenceEvent PubNubMessaging.Core.Helpers::CreatePNPresenceEvent(System.Object)
extern "C"  PNPresenceEvent_t3577011079 * Helpers_CreatePNPresenceEvent_m227183187 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___payload0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Helpers::CreatePNPresenceEventResult(PubNubMessaging.Core.SubscribeMessage,PubNubMessaging.Core.PNPresenceEventResult&)
extern "C"  void Helpers_CreatePNPresenceEventResult_m1039684746 (Il2CppObject * __this /* static, unused */, SubscribeMessage_t3739430639 * ___subscribeMessage0, PNPresenceEventResult_t939915784 ** ___messageResult1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Helpers::AddMessageToListV2(System.String,PubNubMessaging.Core.IJsonPluggableLibrary,PubNubMessaging.Core.SubscribeMessage,PubNubMessaging.Core.ChannelEntity,System.Collections.Generic.List`1<System.Object>&)
extern "C"  void Helpers_AddMessageToListV2_m3553301055 (Il2CppObject * __this /* static, unused */, String_t* ___cipherKey0, Il2CppObject * ___jsonPluggableLibrary1, SubscribeMessage_t3739430639 * ___subscribeMessage2, ChannelEntity_t3266154606 * ___ce3, List_1_t2058570427 ** ___itemMessage4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> PubNubMessaging.Core.Helpers::CreateJsonResponse(System.String,System.String,PubNubMessaging.Core.IJsonPluggableLibrary)
extern "C"  List_1_t2058570427 * Helpers_CreateJsonResponse_m2186286855 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___channel1, Il2CppObject * ___jsonPluggableLibrary2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.PubnubErrorCode PubNubMessaging.Core.Helpers::GetTimeOutErrorCode(PubNubMessaging.Core.ResponseType)
extern "C"  int32_t Helpers_GetTimeOutErrorCode_m2483351586 (Il2CppObject * __this /* static, unused */, int32_t ___responseType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Core.Helpers::<CreateListOfSubscribeMessage>m__0(System.Object)
extern "C"  Il2CppObject * Helpers_U3CCreateListOfSubscribeMessageU3Em__0_m3100244553 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Core.Helpers::<DecryptCipheredMessage>m__2(System.Object)
extern "C"  Il2CppObject * Helpers_U3CDecryptCipheredMessageU3Em__2_m3829391325 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Core.Helpers::<DecryptNonCipheredMessage>m__3(System.Object)
extern "C"  Il2CppObject * Helpers_U3CDecryptNonCipheredMessageU3Em__3_m3532034951 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
