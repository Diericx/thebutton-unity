﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.IEnumerable`1<System.Linq.IGrouping`2<System.String,System.String>>
struct IEnumerable_1_t2861554478;
// System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<System.String,System.String>>
struct IEnumerator_1_t44951260;
// System.Linq.IGrouping`2<System.String,System.String>
struct IGrouping_2_t2569427433;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t3799711356;
// System.String
struct String_t;
// System.Func`2<System.String,System.String>
struct Func_2_t193026957;
// System.Func`2<System.Linq.IGrouping`2<System.String,System.String>,System.Boolean>
struct Func_2_t2800555874;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0
struct  U3CGetDuplicatesU3Ec__Iterator0_t2641118174  : public Il2CppObject
{
public:
	// System.String[] PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::rawChannels
	StringU5BU5D_t1642385972* ___rawChannels_0;
	// System.Collections.Generic.IEnumerable`1<System.Linq.IGrouping`2<System.String,System.String>> PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::<results>__0
	Il2CppObject* ___U3CresultsU3E__0_1;
	// System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<System.String,System.String>> PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::$locvar0
	Il2CppObject* ___U24locvar0_2;
	// System.Linq.IGrouping`2<System.String,System.String> PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::<group>__1
	Il2CppObject* ___U3CgroupU3E__1_3;
	// System.Collections.Generic.IEnumerator`1<System.String> PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::$locvar1
	Il2CppObject* ___U24locvar1_4;
	// System.String PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::<item>__2
	String_t* ___U3CitemU3E__2_5;
	// System.String PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::$current
	String_t* ___U24current_6;
	// System.Boolean PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_rawChannels_0() { return static_cast<int32_t>(offsetof(U3CGetDuplicatesU3Ec__Iterator0_t2641118174, ___rawChannels_0)); }
	inline StringU5BU5D_t1642385972* get_rawChannels_0() const { return ___rawChannels_0; }
	inline StringU5BU5D_t1642385972** get_address_of_rawChannels_0() { return &___rawChannels_0; }
	inline void set_rawChannels_0(StringU5BU5D_t1642385972* value)
	{
		___rawChannels_0 = value;
		Il2CppCodeGenWriteBarrier(&___rawChannels_0, value);
	}

	inline static int32_t get_offset_of_U3CresultsU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetDuplicatesU3Ec__Iterator0_t2641118174, ___U3CresultsU3E__0_1)); }
	inline Il2CppObject* get_U3CresultsU3E__0_1() const { return ___U3CresultsU3E__0_1; }
	inline Il2CppObject** get_address_of_U3CresultsU3E__0_1() { return &___U3CresultsU3E__0_1; }
	inline void set_U3CresultsU3E__0_1(Il2CppObject* value)
	{
		___U3CresultsU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CresultsU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U24locvar0_2() { return static_cast<int32_t>(offsetof(U3CGetDuplicatesU3Ec__Iterator0_t2641118174, ___U24locvar0_2)); }
	inline Il2CppObject* get_U24locvar0_2() const { return ___U24locvar0_2; }
	inline Il2CppObject** get_address_of_U24locvar0_2() { return &___U24locvar0_2; }
	inline void set_U24locvar0_2(Il2CppObject* value)
	{
		___U24locvar0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_2, value);
	}

	inline static int32_t get_offset_of_U3CgroupU3E__1_3() { return static_cast<int32_t>(offsetof(U3CGetDuplicatesU3Ec__Iterator0_t2641118174, ___U3CgroupU3E__1_3)); }
	inline Il2CppObject* get_U3CgroupU3E__1_3() const { return ___U3CgroupU3E__1_3; }
	inline Il2CppObject** get_address_of_U3CgroupU3E__1_3() { return &___U3CgroupU3E__1_3; }
	inline void set_U3CgroupU3E__1_3(Il2CppObject* value)
	{
		___U3CgroupU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CgroupU3E__1_3, value);
	}

	inline static int32_t get_offset_of_U24locvar1_4() { return static_cast<int32_t>(offsetof(U3CGetDuplicatesU3Ec__Iterator0_t2641118174, ___U24locvar1_4)); }
	inline Il2CppObject* get_U24locvar1_4() const { return ___U24locvar1_4; }
	inline Il2CppObject** get_address_of_U24locvar1_4() { return &___U24locvar1_4; }
	inline void set_U24locvar1_4(Il2CppObject* value)
	{
		___U24locvar1_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar1_4, value);
	}

	inline static int32_t get_offset_of_U3CitemU3E__2_5() { return static_cast<int32_t>(offsetof(U3CGetDuplicatesU3Ec__Iterator0_t2641118174, ___U3CitemU3E__2_5)); }
	inline String_t* get_U3CitemU3E__2_5() const { return ___U3CitemU3E__2_5; }
	inline String_t** get_address_of_U3CitemU3E__2_5() { return &___U3CitemU3E__2_5; }
	inline void set_U3CitemU3E__2_5(String_t* value)
	{
		___U3CitemU3E__2_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CitemU3E__2_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CGetDuplicatesU3Ec__Iterator0_t2641118174, ___U24current_6)); }
	inline String_t* get_U24current_6() const { return ___U24current_6; }
	inline String_t** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(String_t* value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CGetDuplicatesU3Ec__Iterator0_t2641118174, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CGetDuplicatesU3Ec__Iterator0_t2641118174, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

struct U3CGetDuplicatesU3Ec__Iterator0_t2641118174_StaticFields
{
public:
	// System.Func`2<System.String,System.String> PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::<>f__am$cache0
	Func_2_t193026957 * ___U3CU3Ef__amU24cache0_9;
	// System.Func`2<System.Linq.IGrouping`2<System.String,System.String>,System.Boolean> PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::<>f__am$cache1
	Func_2_t2800555874 * ___U3CU3Ef__amU24cache1_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(U3CGetDuplicatesU3Ec__Iterator0_t2641118174_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Func_2_t193026957 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Func_2_t193026957 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Func_2_t193026957 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_10() { return static_cast<int32_t>(offsetof(U3CGetDuplicatesU3Ec__Iterator0_t2641118174_StaticFields, ___U3CU3Ef__amU24cache1_10)); }
	inline Func_2_t2800555874 * get_U3CU3Ef__amU24cache1_10() const { return ___U3CU3Ef__amU24cache1_10; }
	inline Func_2_t2800555874 ** get_address_of_U3CU3Ef__amU24cache1_10() { return &___U3CU3Ef__amU24cache1_10; }
	inline void set_U3CU3Ef__amU24cache1_10(Func_2_t2800555874 * value)
	{
		___U3CU3Ef__amU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
