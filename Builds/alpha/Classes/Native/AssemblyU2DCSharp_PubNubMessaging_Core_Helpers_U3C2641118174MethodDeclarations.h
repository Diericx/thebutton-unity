﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0
struct U3CGetDuplicatesU3Ec__Iterator0_t2641118174;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t3799711356;
// System.Linq.IGrouping`2<System.String,System.String>
struct IGrouping_2_t2569427433;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::.ctor()
extern "C"  void U3CGetDuplicatesU3Ec__Iterator0__ctor_m3931866303 (U3CGetDuplicatesU3Ec__Iterator0_t2641118174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::MoveNext()
extern "C"  bool U3CGetDuplicatesU3Ec__Iterator0_MoveNext_m3767735985 (U3CGetDuplicatesU3Ec__Iterator0_t2641118174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::System.Collections.Generic.IEnumerator<string>.get_Current()
extern "C"  String_t* U3CGetDuplicatesU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CstringU3E_get_Current_m1660009837 (U3CGetDuplicatesU3Ec__Iterator0_t2641118174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetDuplicatesU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m286149173 (U3CGetDuplicatesU3Ec__Iterator0_t2641118174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::Dispose()
extern "C"  void U3CGetDuplicatesU3Ec__Iterator0_Dispose_m2442326100 (U3CGetDuplicatesU3Ec__Iterator0_t2641118174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::Reset()
extern "C"  void U3CGetDuplicatesU3Ec__Iterator0_Reset_m3132419278 (U3CGetDuplicatesU3Ec__Iterator0_t2641118174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetDuplicatesU3Ec__Iterator0_System_Collections_IEnumerable_GetEnumerator_m323726600 (U3CGetDuplicatesU3Ec__Iterator0_t2641118174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.String> PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::System.Collections.Generic.IEnumerable<string>.GetEnumerator()
extern "C"  Il2CppObject* U3CGetDuplicatesU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m377654608 (U3CGetDuplicatesU3Ec__Iterator0_t2641118174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::<>m__0(System.String)
extern "C"  String_t* U3CGetDuplicatesU3Ec__Iterator0_U3CU3Em__0_m211348555 (Il2CppObject * __this /* static, unused */, String_t* ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Helpers/<GetDuplicates>c__Iterator0::<>m__1(System.Linq.IGrouping`2<System.String,System.String>)
extern "C"  bool U3CGetDuplicatesU3Ec__Iterator0_U3CU3Em__1_m1572341934 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___g0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
