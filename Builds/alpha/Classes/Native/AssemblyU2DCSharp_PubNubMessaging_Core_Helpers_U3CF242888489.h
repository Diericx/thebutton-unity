﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelIden1147162267.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.Helpers/<FindChannelEntityAndCallback>c__AnonStorey1`1<System.Object>
struct  U3CFindChannelEntityAndCallbackU3Ec__AnonStorey1_1_t242888489  : public Il2CppObject
{
public:
	// PubNubMessaging.Core.ChannelIdentity PubNubMessaging.Core.Helpers/<FindChannelEntityAndCallback>c__AnonStorey1`1::ci
	ChannelIdentity_t1147162267  ___ci_0;

public:
	inline static int32_t get_offset_of_ci_0() { return static_cast<int32_t>(offsetof(U3CFindChannelEntityAndCallbackU3Ec__AnonStorey1_1_t242888489, ___ci_0)); }
	inline ChannelIdentity_t1147162267  get_ci_0() const { return ___ci_0; }
	inline ChannelIdentity_t1147162267 * get_address_of_ci_0() { return &___ci_0; }
	inline void set_ci_0(ChannelIdentity_t1147162267  value)
	{
		___ci_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
