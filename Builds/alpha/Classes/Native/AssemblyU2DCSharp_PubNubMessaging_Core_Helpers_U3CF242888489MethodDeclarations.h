﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.Helpers/<FindChannelEntityAndCallback>c__AnonStorey1`1<System.Object>
struct U3CFindChannelEntityAndCallbackU3Ec__AnonStorey1_1_t242888489;
// PubNubMessaging.Core.ChannelEntity
struct ChannelEntity_t3266154606;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelEnti3266154606.h"

// System.Void PubNubMessaging.Core.Helpers/<FindChannelEntityAndCallback>c__AnonStorey1`1<System.Object>::.ctor()
extern "C"  void U3CFindChannelEntityAndCallbackU3Ec__AnonStorey1_1__ctor_m1813777470_gshared (U3CFindChannelEntityAndCallbackU3Ec__AnonStorey1_1_t242888489 * __this, const MethodInfo* method);
#define U3CFindChannelEntityAndCallbackU3Ec__AnonStorey1_1__ctor_m1813777470(__this, method) ((  void (*) (U3CFindChannelEntityAndCallbackU3Ec__AnonStorey1_1_t242888489 *, const MethodInfo*))U3CFindChannelEntityAndCallbackU3Ec__AnonStorey1_1__ctor_m1813777470_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.Helpers/<FindChannelEntityAndCallback>c__AnonStorey1`1<System.Object>::<>m__0(PubNubMessaging.Core.ChannelEntity)
extern "C"  bool U3CFindChannelEntityAndCallbackU3Ec__AnonStorey1_1_U3CU3Em__0_m594612182_gshared (U3CFindChannelEntityAndCallbackU3Ec__AnonStorey1_1_t242888489 * __this, ChannelEntity_t3266154606 * ___x0, const MethodInfo* method);
#define U3CFindChannelEntityAndCallbackU3Ec__AnonStorey1_1_U3CU3Em__0_m594612182(__this, ___x0, method) ((  bool (*) (U3CFindChannelEntityAndCallbackU3Ec__AnonStorey1_1_t242888489 *, ChannelEntity_t3266154606 *, const MethodInfo*))U3CFindChannelEntityAndCallbackU3Ec__AnonStorey1_1_U3CU3Em__0_m594612182_gshared)(__this, ___x0, method)
