﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action`1<PubNubMessaging.Core.PubnubClientError>
struct Action_1_t4207084599;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.InternetState`1<System.Object>
struct  InternetState_1_t2062608855  : public Il2CppObject
{
public:
	// System.Action`1<System.Boolean> PubNubMessaging.Core.InternetState`1::Callback
	Action_1_t3627374100 * ___Callback_0;
	// System.Action`1<PubNubMessaging.Core.PubnubClientError> PubNubMessaging.Core.InternetState`1::ErrorCallback
	Action_1_t4207084599 * ___ErrorCallback_1;
	// System.String[] PubNubMessaging.Core.InternetState`1::Channels
	StringU5BU5D_t1642385972* ___Channels_2;

public:
	inline static int32_t get_offset_of_Callback_0() { return static_cast<int32_t>(offsetof(InternetState_1_t2062608855, ___Callback_0)); }
	inline Action_1_t3627374100 * get_Callback_0() const { return ___Callback_0; }
	inline Action_1_t3627374100 ** get_address_of_Callback_0() { return &___Callback_0; }
	inline void set_Callback_0(Action_1_t3627374100 * value)
	{
		___Callback_0 = value;
		Il2CppCodeGenWriteBarrier(&___Callback_0, value);
	}

	inline static int32_t get_offset_of_ErrorCallback_1() { return static_cast<int32_t>(offsetof(InternetState_1_t2062608855, ___ErrorCallback_1)); }
	inline Action_1_t4207084599 * get_ErrorCallback_1() const { return ___ErrorCallback_1; }
	inline Action_1_t4207084599 ** get_address_of_ErrorCallback_1() { return &___ErrorCallback_1; }
	inline void set_ErrorCallback_1(Action_1_t4207084599 * value)
	{
		___ErrorCallback_1 = value;
		Il2CppCodeGenWriteBarrier(&___ErrorCallback_1, value);
	}

	inline static int32_t get_offset_of_Channels_2() { return static_cast<int32_t>(offsetof(InternetState_1_t2062608855, ___Channels_2)); }
	inline StringU5BU5D_t1642385972* get_Channels_2() const { return ___Channels_2; }
	inline StringU5BU5D_t1642385972** get_address_of_Channels_2() { return &___Channels_2; }
	inline void set_Channels_2(StringU5BU5D_t1642385972* value)
	{
		___Channels_2 = value;
		Il2CppCodeGenWriteBarrier(&___Channels_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
