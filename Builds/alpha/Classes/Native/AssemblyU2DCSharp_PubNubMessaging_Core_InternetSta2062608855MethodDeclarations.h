﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.InternetState`1<System.Object>
struct InternetState_1_t2062608855;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Core.InternetState`1<System.Object>::.ctor()
extern "C"  void InternetState_1__ctor_m974510633_gshared (InternetState_1_t2062608855 * __this, const MethodInfo* method);
#define InternetState_1__ctor_m974510633(__this, method) ((  void (*) (InternetState_1_t2062608855 *, const MethodInfo*))InternetState_1__ctor_m974510633_gshared)(__this, method)
