﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PubNubMessaging.Core.IJsonPluggableLibrary
struct IJsonPluggableLibrary_t1579330875;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.JSONSerializer
struct  JSONSerializer_t2861936230  : public Il2CppObject
{
public:

public:
};

struct JSONSerializer_t2861936230_StaticFields
{
public:
	// PubNubMessaging.Core.IJsonPluggableLibrary PubNubMessaging.Core.JSONSerializer::jsonPluggableLibrary
	Il2CppObject * ___jsonPluggableLibrary_0;

public:
	inline static int32_t get_offset_of_jsonPluggableLibrary_0() { return static_cast<int32_t>(offsetof(JSONSerializer_t2861936230_StaticFields, ___jsonPluggableLibrary_0)); }
	inline Il2CppObject * get_jsonPluggableLibrary_0() const { return ___jsonPluggableLibrary_0; }
	inline Il2CppObject ** get_address_of_jsonPluggableLibrary_0() { return &___jsonPluggableLibrary_0; }
	inline void set_jsonPluggableLibrary_0(Il2CppObject * value)
	{
		___jsonPluggableLibrary_0 = value;
		Il2CppCodeGenWriteBarrier(&___jsonPluggableLibrary_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
