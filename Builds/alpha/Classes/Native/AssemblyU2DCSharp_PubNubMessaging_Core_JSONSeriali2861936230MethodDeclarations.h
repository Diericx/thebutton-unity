﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.IJsonPluggableLibrary
struct IJsonPluggableLibrary_t1579330875;

#include "codegen/il2cpp-codegen.h"

// PubNubMessaging.Core.IJsonPluggableLibrary PubNubMessaging.Core.JSONSerializer::get_JsonPluggableLibrary()
extern "C"  Il2CppObject * JSONSerializer_get_JsonPluggableLibrary_m2308298444 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.JSONSerializer::.cctor()
extern "C"  void JSONSerializer__cctor_m1604760323 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
