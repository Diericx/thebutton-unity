﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.JsonFxUnitySerializer
struct JsonFxUnitySerializer_t4175252755;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void PubNubMessaging.Core.JsonFxUnitySerializer::.ctor()
extern "C"  void JsonFxUnitySerializer__ctor_m609572309 (JsonFxUnitySerializer_t4175252755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.JsonFxUnitySerializer::IsArrayCompatible(System.String)
extern "C"  bool JsonFxUnitySerializer_IsArrayCompatible_m1154241614 (JsonFxUnitySerializer_t4175252755 * __this, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.JsonFxUnitySerializer::IsDictionaryCompatible(System.String)
extern "C"  bool JsonFxUnitySerializer_IsDictionaryCompatible_m827223315 (JsonFxUnitySerializer_t4175252755 * __this, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.JsonFxUnitySerializer::SerializeToJsonString(System.Object)
extern "C"  String_t* JsonFxUnitySerializer_SerializeToJsonString_m1327670698 (JsonFxUnitySerializer_t4175252755 * __this, Il2CppObject * ___objectToSerialize0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> PubNubMessaging.Core.JsonFxUnitySerializer::DeserializeToListOfObject(System.String)
extern "C"  List_1_t2058570427 * JsonFxUnitySerializer_DeserializeToListOfObject_m2090834532 (JsonFxUnitySerializer_t4175252755 * __this, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Core.JsonFxUnitySerializer::DeserializeToObject(System.String)
extern "C"  Il2CppObject * JsonFxUnitySerializer_DeserializeToObject_m1040881607 (JsonFxUnitySerializer_t4175252755 * __this, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> PubNubMessaging.Core.JsonFxUnitySerializer::DeserializeToDictionaryOfObject(System.String)
extern "C"  Dictionary_2_t309261261 * JsonFxUnitySerializer_DeserializeToDictionaryOfObject_m412894271 (JsonFxUnitySerializer_t4175252755 * __this, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
