﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.LoggingMethod
struct  LoggingMethod_t447643578  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct LoggingMethod_t447643578_StaticFields
{
public:
	// System.Int32 PubNubMessaging.Core.LoggingMethod::logLevel
	int32_t ___logLevel_2;

public:
	inline static int32_t get_offset_of_logLevel_2() { return static_cast<int32_t>(offsetof(LoggingMethod_t447643578_StaticFields, ___logLevel_2)); }
	inline int32_t get_logLevel_2() const { return ___logLevel_2; }
	inline int32_t* get_address_of_logLevel_2() { return &___logLevel_2; }
	inline void set_logLevel_2(int32_t value)
	{
		___logLevel_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
