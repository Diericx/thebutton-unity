﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.LoggingMethod
struct LoggingMethod_t447643578;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_LoggingMeth2240454990.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PubNubMessaging.Core.LoggingMethod::.ctor()
extern "C"  void LoggingMethod__ctor_m2483611810 (LoggingMethod_t447643578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.LoggingMethod/Level PubNubMessaging.Core.LoggingMethod::get_LogLevel()
extern "C"  int32_t LoggingMethod_get_LogLevel_m1091022748 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.LoggingMethod::set_LogLevel(PubNubMessaging.Core.LoggingMethod/Level)
extern "C"  void LoggingMethod_set_LogLevel_m3092519893 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.LoggingMethod::get_LevelError()
extern "C"  bool LoggingMethod_get_LevelError_m1692536951 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.LoggingMethod::get_LevelInfo()
extern "C"  bool LoggingMethod_get_LevelInfo_m1534786405 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.LoggingMethod::get_LevelVerbose()
extern "C"  bool LoggingMethod_get_LevelVerbose_m4141308225 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.LoggingMethod::get_LevelWarning()
extern "C"  bool LoggingMethod_get_LevelWarning_m2938531405 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.LoggingMethod::WriteToLog(System.String,System.Boolean)
extern "C"  void LoggingMethod_WriteToLog_m4101749167 (Il2CppObject * __this /* static, unused */, String_t* ___logText0, bool ___writeToLog1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.LoggingMethod::.cctor()
extern "C"  void LoggingMethod__cctor_m2459177903 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
