﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.MD5
struct  MD5_t3967221866  : public Il2CppObject
{
public:
	// System.UInt32[] PubNubMessaging.Core.MD5::state
	UInt32U5BU5D_t59386216* ___state_17;
	// System.UInt32[] PubNubMessaging.Core.MD5::count
	UInt32U5BU5D_t59386216* ___count_18;
	// System.Byte[] PubNubMessaging.Core.MD5::buffer
	ByteU5BU5D_t3397334013* ___buffer_19;
	// System.Byte[] PubNubMessaging.Core.MD5::HashValue
	ByteU5BU5D_t3397334013* ___HashValue_20;
	// System.Int32 PubNubMessaging.Core.MD5::State
	int32_t ___State_21;
	// System.Int32 PubNubMessaging.Core.MD5::HashSizeValue
	int32_t ___HashSizeValue_22;

public:
	inline static int32_t get_offset_of_state_17() { return static_cast<int32_t>(offsetof(MD5_t3967221866, ___state_17)); }
	inline UInt32U5BU5D_t59386216* get_state_17() const { return ___state_17; }
	inline UInt32U5BU5D_t59386216** get_address_of_state_17() { return &___state_17; }
	inline void set_state_17(UInt32U5BU5D_t59386216* value)
	{
		___state_17 = value;
		Il2CppCodeGenWriteBarrier(&___state_17, value);
	}

	inline static int32_t get_offset_of_count_18() { return static_cast<int32_t>(offsetof(MD5_t3967221866, ___count_18)); }
	inline UInt32U5BU5D_t59386216* get_count_18() const { return ___count_18; }
	inline UInt32U5BU5D_t59386216** get_address_of_count_18() { return &___count_18; }
	inline void set_count_18(UInt32U5BU5D_t59386216* value)
	{
		___count_18 = value;
		Il2CppCodeGenWriteBarrier(&___count_18, value);
	}

	inline static int32_t get_offset_of_buffer_19() { return static_cast<int32_t>(offsetof(MD5_t3967221866, ___buffer_19)); }
	inline ByteU5BU5D_t3397334013* get_buffer_19() const { return ___buffer_19; }
	inline ByteU5BU5D_t3397334013** get_address_of_buffer_19() { return &___buffer_19; }
	inline void set_buffer_19(ByteU5BU5D_t3397334013* value)
	{
		___buffer_19 = value;
		Il2CppCodeGenWriteBarrier(&___buffer_19, value);
	}

	inline static int32_t get_offset_of_HashValue_20() { return static_cast<int32_t>(offsetof(MD5_t3967221866, ___HashValue_20)); }
	inline ByteU5BU5D_t3397334013* get_HashValue_20() const { return ___HashValue_20; }
	inline ByteU5BU5D_t3397334013** get_address_of_HashValue_20() { return &___HashValue_20; }
	inline void set_HashValue_20(ByteU5BU5D_t3397334013* value)
	{
		___HashValue_20 = value;
		Il2CppCodeGenWriteBarrier(&___HashValue_20, value);
	}

	inline static int32_t get_offset_of_State_21() { return static_cast<int32_t>(offsetof(MD5_t3967221866, ___State_21)); }
	inline int32_t get_State_21() const { return ___State_21; }
	inline int32_t* get_address_of_State_21() { return &___State_21; }
	inline void set_State_21(int32_t value)
	{
		___State_21 = value;
	}

	inline static int32_t get_offset_of_HashSizeValue_22() { return static_cast<int32_t>(offsetof(MD5_t3967221866, ___HashSizeValue_22)); }
	inline int32_t get_HashSizeValue_22() const { return ___HashSizeValue_22; }
	inline int32_t* get_address_of_HashSizeValue_22() { return &___HashSizeValue_22; }
	inline void set_HashSizeValue_22(int32_t value)
	{
		___HashSizeValue_22 = value;
	}
};

struct MD5_t3967221866_StaticFields
{
public:
	// System.Byte[] PubNubMessaging.Core.MD5::PADDING
	ByteU5BU5D_t3397334013* ___PADDING_16;

public:
	inline static int32_t get_offset_of_PADDING_16() { return static_cast<int32_t>(offsetof(MD5_t3967221866_StaticFields, ___PADDING_16)); }
	inline ByteU5BU5D_t3397334013* get_PADDING_16() const { return ___PADDING_16; }
	inline ByteU5BU5D_t3397334013** get_address_of_PADDING_16() { return &___PADDING_16; }
	inline void set_PADDING_16(ByteU5BU5D_t3397334013* value)
	{
		___PADDING_16 = value;
		Il2CppCodeGenWriteBarrier(&___PADDING_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
