﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.MD5
struct MD5_t3967221866;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// System.IO.Stream
struct Stream_t3255436806;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_IO_Stream3255436806.h"

// System.Void PubNubMessaging.Core.MD5::.ctor()
extern "C"  void MD5__ctor_m1538820762 (MD5_t3967221866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.MD5 PubNubMessaging.Core.MD5::Create(System.String)
extern "C"  MD5_t3967221866 * MD5_Create_m1532628172 (Il2CppObject * __this /* static, unused */, String_t* ___hashName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.MD5::GetMd5String(System.String)
extern "C"  String_t* MD5_GetMd5String_m3230555370 (Il2CppObject * __this /* static, unused */, String_t* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.MD5 PubNubMessaging.Core.MD5::Create()
extern "C"  MD5_t3967221866 * MD5_Create_m2089112074 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PubNubMessaging.Core.MD5::F(System.UInt32,System.UInt32,System.UInt32)
extern "C"  uint32_t MD5_F_m2396222113 (Il2CppObject * __this /* static, unused */, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PubNubMessaging.Core.MD5::G(System.UInt32,System.UInt32,System.UInt32)
extern "C"  uint32_t MD5_G_m1461318850 (Il2CppObject * __this /* static, unused */, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PubNubMessaging.Core.MD5::H(System.UInt32,System.UInt32,System.UInt32)
extern "C"  uint32_t MD5_H_m3241652335 (Il2CppObject * __this /* static, unused */, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PubNubMessaging.Core.MD5::I(System.UInt32,System.UInt32,System.UInt32)
extern "C"  uint32_t MD5_I_m712290704 (Il2CppObject * __this /* static, unused */, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 PubNubMessaging.Core.MD5::ROTATE_LEFT(System.UInt32,System.Byte)
extern "C"  uint32_t MD5_ROTATE_LEFT_m2553090587 (Il2CppObject * __this /* static, unused */, uint32_t ___x0, uint8_t ___n1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.MD5::FF(System.UInt32&,System.UInt32,System.UInt32,System.UInt32,System.UInt32,System.Byte,System.UInt32)
extern "C"  void MD5_FF_m2024077203 (Il2CppObject * __this /* static, unused */, uint32_t* ___a0, uint32_t ___b1, uint32_t ___c2, uint32_t ___d3, uint32_t ___x4, uint8_t ___s5, uint32_t ___ac6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.MD5::GG(System.UInt32&,System.UInt32,System.UInt32,System.UInt32,System.UInt32,System.Byte,System.UInt32)
extern "C"  void MD5_GG_m127337079 (Il2CppObject * __this /* static, unused */, uint32_t* ___a0, uint32_t ___b1, uint32_t ___c2, uint32_t ___d3, uint32_t ___x4, uint8_t ___s5, uint32_t ___ac6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.MD5::HH(System.UInt32&,System.UInt32,System.UInt32,System.UInt32,System.UInt32,System.Byte,System.UInt32)
extern "C"  void MD5_HH_m3819633507 (Il2CppObject * __this /* static, unused */, uint32_t* ___a0, uint32_t ___b1, uint32_t ___c2, uint32_t ___d3, uint32_t ___x4, uint8_t ___s5, uint32_t ___ac6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.MD5::II(System.UInt32&,System.UInt32,System.UInt32,System.UInt32,System.UInt32,System.Byte,System.UInt32)
extern "C"  void MD5_II_m1110852423 (Il2CppObject * __this /* static, unused */, uint32_t* ___a0, uint32_t ___b1, uint32_t ___c2, uint32_t ___d3, uint32_t ___x4, uint8_t ___s5, uint32_t ___ac6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.MD5::Initialize()
extern "C"  void MD5_Initialize_m3658528946 (MD5_t3967221866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.MD5::HashCore(System.Byte[],System.Int32,System.Int32)
extern "C"  void MD5_HashCore_m3835246784 (MD5_t3967221866 * __this, ByteU5BU5D_t3397334013* ___input0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] PubNubMessaging.Core.MD5::HashFinal()
extern "C"  ByteU5BU5D_t3397334013* MD5_HashFinal_m3722814786 (MD5_t3967221866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.MD5::Transform(System.Byte[],System.Int32)
extern "C"  void MD5_Transform_m1719415432 (MD5_t3967221866 * __this, ByteU5BU5D_t3397334013* ___block0, int32_t ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.MD5::Encode(System.Byte[],System.Int32,System.UInt32[],System.Int32,System.Int32)
extern "C"  void MD5_Encode_m2903826454 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___output0, int32_t ___outputOffset1, UInt32U5BU5D_t59386216* ___input2, int32_t ___inputOffset3, int32_t ___count4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.MD5::Decode(System.UInt32[],System.Int32,System.Byte[],System.Int32,System.Int32)
extern "C"  void MD5_Decode_m2787315432 (Il2CppObject * __this /* static, unused */, UInt32U5BU5D_t59386216* ___output0, int32_t ___outputOffset1, ByteU5BU5D_t3397334013* ___input2, int32_t ___inputOffset3, int32_t ___count4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.MD5::get_CanReuseTransform()
extern "C"  bool MD5_get_CanReuseTransform_m1953737881 (MD5_t3967221866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.MD5::get_CanTransformMultipleBlocks()
extern "C"  bool MD5_get_CanTransformMultipleBlocks_m1044308171 (MD5_t3967221866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] PubNubMessaging.Core.MD5::get_Hash()
extern "C"  ByteU5BU5D_t3397334013* MD5_get_Hash_m448722163 (MD5_t3967221866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.MD5::get_HashSize()
extern "C"  int32_t MD5_get_HashSize_m2481575170 (MD5_t3967221866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.MD5::get_InputBlockSize()
extern "C"  int32_t MD5_get_InputBlockSize_m466548217 (MD5_t3967221866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.MD5::get_OutputBlockSize()
extern "C"  int32_t MD5_get_OutputBlockSize_m155268180 (MD5_t3967221866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.MD5::Clear()
extern "C"  void MD5_Clear_m3396468937 (MD5_t3967221866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] PubNubMessaging.Core.MD5::ComputeHash(System.Byte[])
extern "C"  ByteU5BU5D_t3397334013* MD5_ComputeHash_m4291204706 (MD5_t3967221866 * __this, ByteU5BU5D_t3397334013* ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] PubNubMessaging.Core.MD5::ComputeHash(System.Byte[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t3397334013* MD5_ComputeHash_m4106843212 (MD5_t3967221866 * __this, ByteU5BU5D_t3397334013* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] PubNubMessaging.Core.MD5::ComputeHash(System.IO.Stream)
extern "C"  ByteU5BU5D_t3397334013* MD5_ComputeHash_m3760025248 (MD5_t3967221866 * __this, Stream_t3255436806 * ___inputStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.MD5::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C"  int32_t MD5_TransformBlock_m4054071364 (MD5_t3967221866 * __this, ByteU5BU5D_t3397334013* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, ByteU5BU5D_t3397334013* ___outputBuffer3, int32_t ___outputOffset4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] PubNubMessaging.Core.MD5::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t3397334013* MD5_TransformFinalBlock_m2311387584 (MD5_t3967221866 * __this, ByteU5BU5D_t3397334013* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.MD5::Dispose(System.Boolean)
extern "C"  void MD5_Dispose_m3733015598 (MD5_t3967221866 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.MD5::Dispose()
extern "C"  void MD5_Dispose_m1048999079 (MD5_t3967221866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.MD5::.cctor()
extern "C"  void MD5__cctor_m3325788519 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
