﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.MD5CryptoServiceProvider
struct MD5CryptoServiceProvider_t1077286933;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Core.MD5CryptoServiceProvider::.ctor()
extern "C"  void MD5CryptoServiceProvider__ctor_m1262272415 (MD5CryptoServiceProvider_t1077286933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
