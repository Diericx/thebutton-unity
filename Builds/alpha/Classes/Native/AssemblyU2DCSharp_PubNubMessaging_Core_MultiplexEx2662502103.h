﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>
struct List_1_t2635275738;

#include "mscorlib_System_EventArgs3289624707.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ResponseTyp1248099179.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.MultiplexExceptionEventArgs`1<System.Object>
struct  MultiplexExceptionEventArgs_1_t2662502103  : public EventArgs_t3289624707
{
public:
	// System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity> PubNubMessaging.Core.MultiplexExceptionEventArgs`1::channelEntities
	List_1_t2635275738 * ___channelEntities_1;
	// System.Boolean PubNubMessaging.Core.MultiplexExceptionEventArgs`1::reconnectMaxTried
	bool ___reconnectMaxTried_2;
	// System.Boolean PubNubMessaging.Core.MultiplexExceptionEventArgs`1::resumeOnReconnect
	bool ___resumeOnReconnect_3;
	// PubNubMessaging.Core.ResponseType PubNubMessaging.Core.MultiplexExceptionEventArgs`1::responseType
	int32_t ___responseType_4;

public:
	inline static int32_t get_offset_of_channelEntities_1() { return static_cast<int32_t>(offsetof(MultiplexExceptionEventArgs_1_t2662502103, ___channelEntities_1)); }
	inline List_1_t2635275738 * get_channelEntities_1() const { return ___channelEntities_1; }
	inline List_1_t2635275738 ** get_address_of_channelEntities_1() { return &___channelEntities_1; }
	inline void set_channelEntities_1(List_1_t2635275738 * value)
	{
		___channelEntities_1 = value;
		Il2CppCodeGenWriteBarrier(&___channelEntities_1, value);
	}

	inline static int32_t get_offset_of_reconnectMaxTried_2() { return static_cast<int32_t>(offsetof(MultiplexExceptionEventArgs_1_t2662502103, ___reconnectMaxTried_2)); }
	inline bool get_reconnectMaxTried_2() const { return ___reconnectMaxTried_2; }
	inline bool* get_address_of_reconnectMaxTried_2() { return &___reconnectMaxTried_2; }
	inline void set_reconnectMaxTried_2(bool value)
	{
		___reconnectMaxTried_2 = value;
	}

	inline static int32_t get_offset_of_resumeOnReconnect_3() { return static_cast<int32_t>(offsetof(MultiplexExceptionEventArgs_1_t2662502103, ___resumeOnReconnect_3)); }
	inline bool get_resumeOnReconnect_3() const { return ___resumeOnReconnect_3; }
	inline bool* get_address_of_resumeOnReconnect_3() { return &___resumeOnReconnect_3; }
	inline void set_resumeOnReconnect_3(bool value)
	{
		___resumeOnReconnect_3 = value;
	}

	inline static int32_t get_offset_of_responseType_4() { return static_cast<int32_t>(offsetof(MultiplexExceptionEventArgs_1_t2662502103, ___responseType_4)); }
	inline int32_t get_responseType_4() const { return ___responseType_4; }
	inline int32_t* get_address_of_responseType_4() { return &___responseType_4; }
	inline void set_responseType_4(int32_t value)
	{
		___responseType_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
