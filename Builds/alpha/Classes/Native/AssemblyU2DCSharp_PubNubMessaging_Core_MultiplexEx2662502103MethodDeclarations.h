﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.MultiplexExceptionEventArgs`1<System.Object>
struct MultiplexExceptionEventArgs_1_t2662502103;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Core.MultiplexExceptionEventArgs`1<System.Object>::.ctor()
extern "C"  void MultiplexExceptionEventArgs_1__ctor_m506391253_gshared (MultiplexExceptionEventArgs_1_t2662502103 * __this, const MethodInfo* method);
#define MultiplexExceptionEventArgs_1__ctor_m506391253(__this, method) ((  void (*) (MultiplexExceptionEventArgs_1_t2662502103 *, const MethodInfo*))MultiplexExceptionEventArgs_1__ctor_m506391253_gshared)(__this, method)
