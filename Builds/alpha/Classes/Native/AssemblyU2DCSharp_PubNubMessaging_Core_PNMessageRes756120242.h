﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.PNMessageResult
struct  PNMessageResult_t756120242  : public Il2CppObject
{
public:
	// System.Object PubNubMessaging.Core.PNMessageResult::<Payload>k__BackingField
	Il2CppObject * ___U3CPayloadU3Ek__BackingField_0;
	// System.String PubNubMessaging.Core.PNMessageResult::<Subscription>k__BackingField
	String_t* ___U3CSubscriptionU3Ek__BackingField_1;
	// System.String PubNubMessaging.Core.PNMessageResult::<Channel>k__BackingField
	String_t* ___U3CChannelU3Ek__BackingField_2;
	// System.Int64 PubNubMessaging.Core.PNMessageResult::<Timetoken>k__BackingField
	int64_t ___U3CTimetokenU3Ek__BackingField_3;
	// System.Int64 PubNubMessaging.Core.PNMessageResult::<OriginatingTimetoken>k__BackingField
	int64_t ___U3COriginatingTimetokenU3Ek__BackingField_4;
	// System.Object PubNubMessaging.Core.PNMessageResult::<UserMetadata>k__BackingField
	Il2CppObject * ___U3CUserMetadataU3Ek__BackingField_5;
	// System.String PubNubMessaging.Core.PNMessageResult::<IssuingClientId>k__BackingField
	String_t* ___U3CIssuingClientIdU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CPayloadU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PNMessageResult_t756120242, ___U3CPayloadU3Ek__BackingField_0)); }
	inline Il2CppObject * get_U3CPayloadU3Ek__BackingField_0() const { return ___U3CPayloadU3Ek__BackingField_0; }
	inline Il2CppObject ** get_address_of_U3CPayloadU3Ek__BackingField_0() { return &___U3CPayloadU3Ek__BackingField_0; }
	inline void set_U3CPayloadU3Ek__BackingField_0(Il2CppObject * value)
	{
		___U3CPayloadU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPayloadU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CSubscriptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PNMessageResult_t756120242, ___U3CSubscriptionU3Ek__BackingField_1)); }
	inline String_t* get_U3CSubscriptionU3Ek__BackingField_1() const { return ___U3CSubscriptionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CSubscriptionU3Ek__BackingField_1() { return &___U3CSubscriptionU3Ek__BackingField_1; }
	inline void set_U3CSubscriptionU3Ek__BackingField_1(String_t* value)
	{
		___U3CSubscriptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSubscriptionU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CChannelU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PNMessageResult_t756120242, ___U3CChannelU3Ek__BackingField_2)); }
	inline String_t* get_U3CChannelU3Ek__BackingField_2() const { return ___U3CChannelU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CChannelU3Ek__BackingField_2() { return &___U3CChannelU3Ek__BackingField_2; }
	inline void set_U3CChannelU3Ek__BackingField_2(String_t* value)
	{
		___U3CChannelU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CChannelU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CTimetokenU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PNMessageResult_t756120242, ___U3CTimetokenU3Ek__BackingField_3)); }
	inline int64_t get_U3CTimetokenU3Ek__BackingField_3() const { return ___U3CTimetokenU3Ek__BackingField_3; }
	inline int64_t* get_address_of_U3CTimetokenU3Ek__BackingField_3() { return &___U3CTimetokenU3Ek__BackingField_3; }
	inline void set_U3CTimetokenU3Ek__BackingField_3(int64_t value)
	{
		___U3CTimetokenU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3COriginatingTimetokenU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PNMessageResult_t756120242, ___U3COriginatingTimetokenU3Ek__BackingField_4)); }
	inline int64_t get_U3COriginatingTimetokenU3Ek__BackingField_4() const { return ___U3COriginatingTimetokenU3Ek__BackingField_4; }
	inline int64_t* get_address_of_U3COriginatingTimetokenU3Ek__BackingField_4() { return &___U3COriginatingTimetokenU3Ek__BackingField_4; }
	inline void set_U3COriginatingTimetokenU3Ek__BackingField_4(int64_t value)
	{
		___U3COriginatingTimetokenU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CUserMetadataU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PNMessageResult_t756120242, ___U3CUserMetadataU3Ek__BackingField_5)); }
	inline Il2CppObject * get_U3CUserMetadataU3Ek__BackingField_5() const { return ___U3CUserMetadataU3Ek__BackingField_5; }
	inline Il2CppObject ** get_address_of_U3CUserMetadataU3Ek__BackingField_5() { return &___U3CUserMetadataU3Ek__BackingField_5; }
	inline void set_U3CUserMetadataU3Ek__BackingField_5(Il2CppObject * value)
	{
		___U3CUserMetadataU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUserMetadataU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CIssuingClientIdU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PNMessageResult_t756120242, ___U3CIssuingClientIdU3Ek__BackingField_6)); }
	inline String_t* get_U3CIssuingClientIdU3Ek__BackingField_6() const { return ___U3CIssuingClientIdU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CIssuingClientIdU3Ek__BackingField_6() { return &___U3CIssuingClientIdU3Ek__BackingField_6; }
	inline void set_U3CIssuingClientIdU3Ek__BackingField_6(String_t* value)
	{
		___U3CIssuingClientIdU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CIssuingClientIdU3Ek__BackingField_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
