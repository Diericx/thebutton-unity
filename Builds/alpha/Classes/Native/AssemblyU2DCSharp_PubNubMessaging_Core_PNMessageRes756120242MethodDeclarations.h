﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.PNMessageResult
struct PNMessageResult_t756120242;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void PubNubMessaging.Core.PNMessageResult::.ctor(System.String,System.String,System.Object,System.Int64,System.Int64,System.Object,System.String)
extern "C"  void PNMessageResult__ctor_m3208200980 (PNMessageResult_t756120242 * __this, String_t* ___subscribedChannel0, String_t* ___actualchannel1, Il2CppObject * ___payload2, int64_t ___timetoken3, int64_t ___originatingTimetoken4, Il2CppObject * ___userMetadata5, String_t* ___issuingClientId6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Core.PNMessageResult::get_Payload()
extern "C"  Il2CppObject * PNMessageResult_get_Payload_m1538626042 (PNMessageResult_t756120242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNMessageResult::set_Payload(System.Object)
extern "C"  void PNMessageResult_set_Payload_m1899345745 (PNMessageResult_t756120242 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PNMessageResult::get_Subscription()
extern "C"  String_t* PNMessageResult_get_Subscription_m1497214067 (PNMessageResult_t756120242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNMessageResult::set_Subscription(System.String)
extern "C"  void PNMessageResult_set_Subscription_m4169799866 (PNMessageResult_t756120242 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PNMessageResult::get_Channel()
extern "C"  String_t* PNMessageResult_get_Channel_m2732410735 (PNMessageResult_t756120242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNMessageResult::set_Channel(System.String)
extern "C"  void PNMessageResult_set_Channel_m3063551776 (PNMessageResult_t756120242 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 PubNubMessaging.Core.PNMessageResult::get_Timetoken()
extern "C"  int64_t PNMessageResult_get_Timetoken_m1896280974 (PNMessageResult_t756120242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNMessageResult::set_Timetoken(System.Int64)
extern "C"  void PNMessageResult_set_Timetoken_m2797659485 (PNMessageResult_t756120242 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 PubNubMessaging.Core.PNMessageResult::get_OriginatingTimetoken()
extern "C"  int64_t PNMessageResult_get_OriginatingTimetoken_m2434075179 (PNMessageResult_t756120242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNMessageResult::set_OriginatingTimetoken(System.Int64)
extern "C"  void PNMessageResult_set_OriginatingTimetoken_m2320581580 (PNMessageResult_t756120242 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Core.PNMessageResult::get_UserMetadata()
extern "C"  Il2CppObject * PNMessageResult_get_UserMetadata_m3294155100 (PNMessageResult_t756120242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNMessageResult::set_UserMetadata(System.Object)
extern "C"  void PNMessageResult_set_UserMetadata_m1959961651 (PNMessageResult_t756120242 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PNMessageResult::get_IssuingClientId()
extern "C"  String_t* PNMessageResult_get_IssuingClientId_m2500575904 (PNMessageResult_t756120242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNMessageResult::set_IssuingClientId(System.String)
extern "C"  void PNMessageResult_set_IssuingClientId_m865846959 (PNMessageResult_t756120242 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
