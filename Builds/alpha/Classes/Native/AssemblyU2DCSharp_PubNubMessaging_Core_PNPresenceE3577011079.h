﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.PNPresenceEvent
struct  PNPresenceEvent_t3577011079  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Core.PNPresenceEvent::<Action>k__BackingField
	String_t* ___U3CActionU3Ek__BackingField_0;
	// System.String PubNubMessaging.Core.PNPresenceEvent::<UUID>k__BackingField
	String_t* ___U3CUUIDU3Ek__BackingField_1;
	// System.Int32 PubNubMessaging.Core.PNPresenceEvent::<Occupancy>k__BackingField
	int32_t ___U3COccupancyU3Ek__BackingField_2;
	// System.Int64 PubNubMessaging.Core.PNPresenceEvent::<Timestamp>k__BackingField
	int64_t ___U3CTimestampU3Ek__BackingField_3;
	// System.Object PubNubMessaging.Core.PNPresenceEvent::<State>k__BackingField
	Il2CppObject * ___U3CStateU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CActionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PNPresenceEvent_t3577011079, ___U3CActionU3Ek__BackingField_0)); }
	inline String_t* get_U3CActionU3Ek__BackingField_0() const { return ___U3CActionU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CActionU3Ek__BackingField_0() { return &___U3CActionU3Ek__BackingField_0; }
	inline void set_U3CActionU3Ek__BackingField_0(String_t* value)
	{
		___U3CActionU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CActionU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CUUIDU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PNPresenceEvent_t3577011079, ___U3CUUIDU3Ek__BackingField_1)); }
	inline String_t* get_U3CUUIDU3Ek__BackingField_1() const { return ___U3CUUIDU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CUUIDU3Ek__BackingField_1() { return &___U3CUUIDU3Ek__BackingField_1; }
	inline void set_U3CUUIDU3Ek__BackingField_1(String_t* value)
	{
		___U3CUUIDU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUUIDU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3COccupancyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PNPresenceEvent_t3577011079, ___U3COccupancyU3Ek__BackingField_2)); }
	inline int32_t get_U3COccupancyU3Ek__BackingField_2() const { return ___U3COccupancyU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3COccupancyU3Ek__BackingField_2() { return &___U3COccupancyU3Ek__BackingField_2; }
	inline void set_U3COccupancyU3Ek__BackingField_2(int32_t value)
	{
		___U3COccupancyU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CTimestampU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PNPresenceEvent_t3577011079, ___U3CTimestampU3Ek__BackingField_3)); }
	inline int64_t get_U3CTimestampU3Ek__BackingField_3() const { return ___U3CTimestampU3Ek__BackingField_3; }
	inline int64_t* get_address_of_U3CTimestampU3Ek__BackingField_3() { return &___U3CTimestampU3Ek__BackingField_3; }
	inline void set_U3CTimestampU3Ek__BackingField_3(int64_t value)
	{
		___U3CTimestampU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PNPresenceEvent_t3577011079, ___U3CStateU3Ek__BackingField_4)); }
	inline Il2CppObject * get_U3CStateU3Ek__BackingField_4() const { return ___U3CStateU3Ek__BackingField_4; }
	inline Il2CppObject ** get_address_of_U3CStateU3Ek__BackingField_4() { return &___U3CStateU3Ek__BackingField_4; }
	inline void set_U3CStateU3Ek__BackingField_4(Il2CppObject * value)
	{
		___U3CStateU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStateU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
