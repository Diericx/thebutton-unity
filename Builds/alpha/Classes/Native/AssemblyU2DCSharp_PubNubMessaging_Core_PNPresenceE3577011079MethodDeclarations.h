﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.PNPresenceEvent
struct PNPresenceEvent_t3577011079;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void PubNubMessaging.Core.PNPresenceEvent::.ctor(System.String,System.String,System.Int32,System.Int64,System.Object)
extern "C"  void PNPresenceEvent__ctor_m1937328928 (PNPresenceEvent_t3577011079 * __this, String_t* ___action0, String_t* ___uuid1, int32_t ___Occupancy2, int64_t ___timestamp3, Il2CppObject * ___state4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PNPresenceEvent::get_Action()
extern "C"  String_t* PNPresenceEvent_get_Action_m373682741 (PNPresenceEvent_t3577011079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNPresenceEvent::set_Action(System.String)
extern "C"  void PNPresenceEvent_set_Action_m373453900 (PNPresenceEvent_t3577011079 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PNPresenceEvent::get_UUID()
extern "C"  String_t* PNPresenceEvent_get_UUID_m4208488046 (PNPresenceEvent_t3577011079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNPresenceEvent::set_UUID(System.String)
extern "C"  void PNPresenceEvent_set_UUID_m2688542889 (PNPresenceEvent_t3577011079 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.PNPresenceEvent::get_Occupancy()
extern "C"  int32_t PNPresenceEvent_get_Occupancy_m4131268959 (PNPresenceEvent_t3577011079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNPresenceEvent::set_Occupancy(System.Int32)
extern "C"  void PNPresenceEvent_set_Occupancy_m351261302 (PNPresenceEvent_t3577011079 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 PubNubMessaging.Core.PNPresenceEvent::get_Timestamp()
extern "C"  int64_t PNPresenceEvent_get_Timestamp_m3040017217 (PNPresenceEvent_t3577011079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNPresenceEvent::set_Timestamp(System.Int64)
extern "C"  void PNPresenceEvent_set_Timestamp_m3009003154 (PNPresenceEvent_t3577011079 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Core.PNPresenceEvent::get_State()
extern "C"  Il2CppObject * PNPresenceEvent_get_State_m1990028062 (PNPresenceEvent_t3577011079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNPresenceEvent::set_State(System.Object)
extern "C"  void PNPresenceEvent_set_State_m3237553339 (PNPresenceEvent_t3577011079 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
