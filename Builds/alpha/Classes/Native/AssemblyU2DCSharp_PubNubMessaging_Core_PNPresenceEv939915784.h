﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.PNPresenceEventResult
struct  PNPresenceEventResult_t939915784  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Core.PNPresenceEventResult::<Event>k__BackingField
	String_t* ___U3CEventU3Ek__BackingField_0;
	// System.String PubNubMessaging.Core.PNPresenceEventResult::<Subscription>k__BackingField
	String_t* ___U3CSubscriptionU3Ek__BackingField_1;
	// System.String PubNubMessaging.Core.PNPresenceEventResult::<Channel>k__BackingField
	String_t* ___U3CChannelU3Ek__BackingField_2;
	// System.String PubNubMessaging.Core.PNPresenceEventResult::<UUID>k__BackingField
	String_t* ___U3CUUIDU3Ek__BackingField_3;
	// System.Int64 PubNubMessaging.Core.PNPresenceEventResult::<Timestamp>k__BackingField
	int64_t ___U3CTimestampU3Ek__BackingField_4;
	// System.Int64 PubNubMessaging.Core.PNPresenceEventResult::<Timetoken>k__BackingField
	int64_t ___U3CTimetokenU3Ek__BackingField_5;
	// System.Int32 PubNubMessaging.Core.PNPresenceEventResult::<Occupancy>k__BackingField
	int32_t ___U3COccupancyU3Ek__BackingField_6;
	// System.Object PubNubMessaging.Core.PNPresenceEventResult::<State>k__BackingField
	Il2CppObject * ___U3CStateU3Ek__BackingField_7;
	// System.Object PubNubMessaging.Core.PNPresenceEventResult::<UserMetadata>k__BackingField
	Il2CppObject * ___U3CUserMetadataU3Ek__BackingField_8;
	// System.String PubNubMessaging.Core.PNPresenceEventResult::<IssuingClientId>k__BackingField
	String_t* ___U3CIssuingClientIdU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CEventU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PNPresenceEventResult_t939915784, ___U3CEventU3Ek__BackingField_0)); }
	inline String_t* get_U3CEventU3Ek__BackingField_0() const { return ___U3CEventU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CEventU3Ek__BackingField_0() { return &___U3CEventU3Ek__BackingField_0; }
	inline void set_U3CEventU3Ek__BackingField_0(String_t* value)
	{
		___U3CEventU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEventU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CSubscriptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PNPresenceEventResult_t939915784, ___U3CSubscriptionU3Ek__BackingField_1)); }
	inline String_t* get_U3CSubscriptionU3Ek__BackingField_1() const { return ___U3CSubscriptionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CSubscriptionU3Ek__BackingField_1() { return &___U3CSubscriptionU3Ek__BackingField_1; }
	inline void set_U3CSubscriptionU3Ek__BackingField_1(String_t* value)
	{
		___U3CSubscriptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSubscriptionU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CChannelU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PNPresenceEventResult_t939915784, ___U3CChannelU3Ek__BackingField_2)); }
	inline String_t* get_U3CChannelU3Ek__BackingField_2() const { return ___U3CChannelU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CChannelU3Ek__BackingField_2() { return &___U3CChannelU3Ek__BackingField_2; }
	inline void set_U3CChannelU3Ek__BackingField_2(String_t* value)
	{
		___U3CChannelU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CChannelU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CUUIDU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PNPresenceEventResult_t939915784, ___U3CUUIDU3Ek__BackingField_3)); }
	inline String_t* get_U3CUUIDU3Ek__BackingField_3() const { return ___U3CUUIDU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CUUIDU3Ek__BackingField_3() { return &___U3CUUIDU3Ek__BackingField_3; }
	inline void set_U3CUUIDU3Ek__BackingField_3(String_t* value)
	{
		___U3CUUIDU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUUIDU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CTimestampU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PNPresenceEventResult_t939915784, ___U3CTimestampU3Ek__BackingField_4)); }
	inline int64_t get_U3CTimestampU3Ek__BackingField_4() const { return ___U3CTimestampU3Ek__BackingField_4; }
	inline int64_t* get_address_of_U3CTimestampU3Ek__BackingField_4() { return &___U3CTimestampU3Ek__BackingField_4; }
	inline void set_U3CTimestampU3Ek__BackingField_4(int64_t value)
	{
		___U3CTimestampU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CTimetokenU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PNPresenceEventResult_t939915784, ___U3CTimetokenU3Ek__BackingField_5)); }
	inline int64_t get_U3CTimetokenU3Ek__BackingField_5() const { return ___U3CTimetokenU3Ek__BackingField_5; }
	inline int64_t* get_address_of_U3CTimetokenU3Ek__BackingField_5() { return &___U3CTimetokenU3Ek__BackingField_5; }
	inline void set_U3CTimetokenU3Ek__BackingField_5(int64_t value)
	{
		___U3CTimetokenU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3COccupancyU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PNPresenceEventResult_t939915784, ___U3COccupancyU3Ek__BackingField_6)); }
	inline int32_t get_U3COccupancyU3Ek__BackingField_6() const { return ___U3COccupancyU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3COccupancyU3Ek__BackingField_6() { return &___U3COccupancyU3Ek__BackingField_6; }
	inline void set_U3COccupancyU3Ek__BackingField_6(int32_t value)
	{
		___U3COccupancyU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PNPresenceEventResult_t939915784, ___U3CStateU3Ek__BackingField_7)); }
	inline Il2CppObject * get_U3CStateU3Ek__BackingField_7() const { return ___U3CStateU3Ek__BackingField_7; }
	inline Il2CppObject ** get_address_of_U3CStateU3Ek__BackingField_7() { return &___U3CStateU3Ek__BackingField_7; }
	inline void set_U3CStateU3Ek__BackingField_7(Il2CppObject * value)
	{
		___U3CStateU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStateU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CUserMetadataU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PNPresenceEventResult_t939915784, ___U3CUserMetadataU3Ek__BackingField_8)); }
	inline Il2CppObject * get_U3CUserMetadataU3Ek__BackingField_8() const { return ___U3CUserMetadataU3Ek__BackingField_8; }
	inline Il2CppObject ** get_address_of_U3CUserMetadataU3Ek__BackingField_8() { return &___U3CUserMetadataU3Ek__BackingField_8; }
	inline void set_U3CUserMetadataU3Ek__BackingField_8(Il2CppObject * value)
	{
		___U3CUserMetadataU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CUserMetadataU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CIssuingClientIdU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(PNPresenceEventResult_t939915784, ___U3CIssuingClientIdU3Ek__BackingField_9)); }
	inline String_t* get_U3CIssuingClientIdU3Ek__BackingField_9() const { return ___U3CIssuingClientIdU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CIssuingClientIdU3Ek__BackingField_9() { return &___U3CIssuingClientIdU3Ek__BackingField_9; }
	inline void set_U3CIssuingClientIdU3Ek__BackingField_9(String_t* value)
	{
		___U3CIssuingClientIdU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CIssuingClientIdU3Ek__BackingField_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
