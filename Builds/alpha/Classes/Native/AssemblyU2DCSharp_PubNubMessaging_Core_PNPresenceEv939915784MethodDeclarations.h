﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.PNPresenceEventResult
struct PNPresenceEventResult_t939915784;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void PubNubMessaging.Core.PNPresenceEventResult::.ctor(System.String,System.String,System.String,System.Int64,System.Int64,System.Object,System.Object,System.String,System.Int32,System.String)
extern "C"  void PNPresenceEventResult__ctor_m1265269909 (PNPresenceEventResult_t939915784 * __this, String_t* ___subscribedChannel0, String_t* ___actualchannel1, String_t* ___presenceEvent2, int64_t ___timetoken3, int64_t ___timestamp4, Il2CppObject * ___userMetadata5, Il2CppObject * ___state6, String_t* ___uuid7, int32_t ___occupancy8, String_t* ___issuingClientId9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PNPresenceEventResult::get_Event()
extern "C"  String_t* PNPresenceEventResult_get_Event_m3975578838 (PNPresenceEventResult_t939915784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNPresenceEventResult::set_Event(System.String)
extern "C"  void PNPresenceEventResult_set_Event_m1742717109 (PNPresenceEventResult_t939915784 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PNPresenceEventResult::get_Subscription()
extern "C"  String_t* PNPresenceEventResult_get_Subscription_m1071626513 (PNPresenceEventResult_t939915784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNPresenceEventResult::set_Subscription(System.String)
extern "C"  void PNPresenceEventResult_set_Subscription_m2531987018 (PNPresenceEventResult_t939915784 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PNPresenceEventResult::get_Channel()
extern "C"  String_t* PNPresenceEventResult_get_Channel_m1854586997 (PNPresenceEventResult_t939915784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNPresenceEventResult::set_Channel(System.String)
extern "C"  void PNPresenceEventResult_set_Channel_m332581872 (PNPresenceEventResult_t939915784 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PNPresenceEventResult::get_UUID()
extern "C"  String_t* PNPresenceEventResult_get_UUID_m34137111 (PNPresenceEventResult_t939915784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNPresenceEventResult::set_UUID(System.String)
extern "C"  void PNPresenceEventResult_set_UUID_m489418000 (PNPresenceEventResult_t939915784 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 PubNubMessaging.Core.PNPresenceEventResult::get_Timestamp()
extern "C"  int64_t PNPresenceEventResult_get_Timestamp_m1937776172 (PNPresenceEventResult_t939915784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNPresenceEventResult::set_Timestamp(System.Int64)
extern "C"  void PNPresenceEventResult_set_Timestamp_m4244286435 (PNPresenceEventResult_t939915784 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 PubNubMessaging.Core.PNPresenceEventResult::get_Timetoken()
extern "C"  int64_t PNPresenceEventResult_get_Timetoken_m2044714310 (PNPresenceEventResult_t939915784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNPresenceEventResult::set_Timetoken(System.Int64)
extern "C"  void PNPresenceEventResult_set_Timetoken_m1772848063 (PNPresenceEventResult_t939915784 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.PNPresenceEventResult::get_Occupancy()
extern "C"  int32_t PNPresenceEventResult_get_Occupancy_m4073719034 (PNPresenceEventResult_t939915784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNPresenceEventResult::set_Occupancy(System.Int32)
extern "C"  void PNPresenceEventResult_set_Occupancy_m2614065557 (PNPresenceEventResult_t939915784 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Core.PNPresenceEventResult::get_State()
extern "C"  Il2CppObject * PNPresenceEventResult_get_State_m2488378021 (PNPresenceEventResult_t939915784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNPresenceEventResult::set_State(System.Object)
extern "C"  void PNPresenceEventResult_set_State_m2580104980 (PNPresenceEventResult_t939915784 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Core.PNPresenceEventResult::get_UserMetadata()
extern "C"  Il2CppObject * PNPresenceEventResult_get_UserMetadata_m4030614492 (PNPresenceEventResult_t939915784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNPresenceEventResult::set_UserMetadata(System.Object)
extern "C"  void PNPresenceEventResult_set_UserMetadata_m3545954117 (PNPresenceEventResult_t939915784 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PNPresenceEventResult::get_IssuingClientId()
extern "C"  String_t* PNPresenceEventResult_get_IssuingClientId_m1555453456 (PNPresenceEventResult_t939915784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PNPresenceEventResult::set_IssuingClientId(System.String)
extern "C"  void PNPresenceEventResult_set_IssuingClientId_m1922395029 (PNPresenceEventResult_t939915784 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
