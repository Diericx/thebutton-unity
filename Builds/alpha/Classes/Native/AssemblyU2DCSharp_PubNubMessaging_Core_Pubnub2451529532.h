﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PubNubMessaging.Core.PubnubUnity
struct PubnubUnity_t2122492879;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.Pubnub
struct  Pubnub_t2451529532  : public Il2CppObject
{
public:
	// PubNubMessaging.Core.PubnubUnity PubNubMessaging.Core.Pubnub::pubnub
	PubnubUnity_t2122492879 * ___pubnub_0;

public:
	inline static int32_t get_offset_of_pubnub_0() { return static_cast<int32_t>(offsetof(Pubnub_t2451529532, ___pubnub_0)); }
	inline PubnubUnity_t2122492879 * get_pubnub_0() const { return ___pubnub_0; }
	inline PubnubUnity_t2122492879 ** get_address_of_pubnub_0() { return &___pubnub_0; }
	inline void set_pubnub_0(PubnubUnity_t2122492879 * value)
	{
		___pubnub_0 = value;
		Il2CppCodeGenWriteBarrier(&___pubnub_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
