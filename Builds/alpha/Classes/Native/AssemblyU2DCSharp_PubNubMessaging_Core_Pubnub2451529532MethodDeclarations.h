﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.Pubnub
struct Pubnub_t2451529532;
// System.String
struct String_t;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action`1<PubNubMessaging.Core.PubnubClientError>
struct Action_1_t4207084599;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.String[]
struct StringU5BU5D_t1642385972;
// PubNubMessaging.Core.IPubnubUnitTest
struct IPubnubUnitTest_t1671935683;
// PubNubMessaging.Core.IJsonPluggableLibrary
struct IJsonPluggableLibrary_t1579330875;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22361573779.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PushTypeSer3531847941.h"
#include "mscorlib_System_Guid2533601593.h"
#include "mscorlib_System_DateTime693205669.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_LoggingMeth2240454990.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubErrorF397889342.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void PubNubMessaging.Core.Pubnub::.ctor(System.String,System.String,System.String,System.String,System.Boolean)
extern "C"  void Pubnub__ctor_m2242449837 (Pubnub_t2451529532 * __this, String_t* ___publishKey0, String_t* ___subscribeKey1, String_t* ___secretKey2, String_t* ___cipherKey3, bool ___sslOn4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::.ctor(System.String,System.String,System.String)
extern "C"  void Pubnub__ctor_m512313730 (Pubnub_t2451529532 * __this, String_t* ___publishKey0, String_t* ___subscribeKey1, String_t* ___secretKey2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::.ctor(System.String,System.String)
extern "C"  void Pubnub__ctor_m2149715534 (Pubnub_t2451529532 * __this, String_t* ___publishKey0, String_t* ___subscribeKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::Subscribe(System.String,System.Action`1<System.Object>,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_Subscribe_m569927868 (Pubnub_t2451529532 * __this, String_t* ___channel0, Action_1_t2491248677 * ___subscribeCallback1, Action_1_t2491248677 * ___connectCallback2, Action_1_t4207084599 * ___errorCallback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::Subscribe(System.String,System.Action`1<System.Object>,System.Action`1<System.Object>,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_Subscribe_m676929898 (Pubnub_t2451529532 * __this, String_t* ___channel0, Action_1_t2491248677 * ___subscribeCallback1, Action_1_t2491248677 * ___connectCallback2, Action_1_t2491248677 * ___wildcardPresenceCallback3, Action_1_t4207084599 * ___errorCallback4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::Subscribe(System.String,System.String,System.Action`1<System.Object>,System.Action`1<System.Object>,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_Subscribe_m1315079970 (Pubnub_t2451529532 * __this, String_t* ___channel0, String_t* ___channelGroup1, Action_1_t2491248677 * ___subscribeCallback2, Action_1_t2491248677 * ___connectCallback3, Action_1_t2491248677 * ___wildcardPresenceCallback4, Action_1_t4207084599 * ___errorCallback5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::Subscribe(System.String,System.String,System.Action`1<System.Object>,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_Subscribe_m391431528 (Pubnub_t2451529532 * __this, String_t* ___channel0, String_t* ___channelGroup1, Action_1_t2491248677 * ___subscribeCallback2, Action_1_t2491248677 * ___connectCallback3, Action_1_t4207084599 * ___errorCallback4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::Publish(System.String,System.Object,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_Publish_m2934497461 (Pubnub_t2451529532 * __this, String_t* ___channel0, Il2CppObject * ___message1, Action_1_t2491248677 * ___userCallback2, Action_1_t4207084599 * ___errorCallback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::Publish(System.String,System.Object,System.Boolean,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_Publish_m276362738 (Pubnub_t2451529532 * __this, String_t* ___channel0, Il2CppObject * ___message1, bool ___storeInHistory2, Action_1_t2491248677 * ___userCallback3, Action_1_t4207084599 * ___errorCallback4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::Publish(System.String,System.Object,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_Publish_m3166709278 (Pubnub_t2451529532 * __this, String_t* ___channel0, Il2CppObject * ___message1, Dictionary_2_t3943999495 * ___metadata2, Action_1_t2491248677 * ___userCallback3, Action_1_t4207084599 * ___errorCallback4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::Publish(System.String,System.Object,System.Boolean,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_Publish_m1351065209 (Pubnub_t2451529532 * __this, String_t* ___channel0, Il2CppObject * ___message1, bool ___storeInHistory2, Dictionary_2_t3943999495 * ___metadata3, Action_1_t2491248677 * ___userCallback4, Action_1_t4207084599 * ___errorCallback5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::Publish(System.String,System.Object,System.Boolean,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Int32,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_Publish_m2557011908 (Pubnub_t2451529532 * __this, String_t* ___channel0, Il2CppObject * ___message1, bool ___storeInHistory2, Dictionary_2_t3943999495 * ___metadata3, int32_t ___ttl4, Action_1_t2491248677 * ___userCallback5, Action_1_t4207084599 * ___errorCallback6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::Presence(System.String,System.String,System.Action`1<System.Object>,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_Presence_m1350261757 (Pubnub_t2451529532 * __this, String_t* ___channel0, String_t* ___channelGroup1, Action_1_t2491248677 * ___userCallback2, Action_1_t2491248677 * ___connectCallback3, Action_1_t4207084599 * ___errorCallback4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::Presence(System.String,System.Action`1<System.Object>,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_Presence_m2892069755 (Pubnub_t2451529532 * __this, String_t* ___channel0, Action_1_t2491248677 * ___userCallback1, Action_1_t2491248677 * ___connectCallback2, Action_1_t4207084599 * ___errorCallback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::DetailedHistory(System.String,System.Int64,System.Int64,System.Int32,System.Boolean,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_DetailedHistory_m221591438 (Pubnub_t2451529532 * __this, String_t* ___channel0, int64_t ___start1, int64_t ___end2, int32_t ___count3, bool ___reverse4, Action_1_t2491248677 * ___userCallback5, Action_1_t4207084599 * ___errorCallback6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::DetailedHistory(System.String,System.Int64,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>,System.Boolean)
extern "C"  bool Pubnub_DetailedHistory_m1795384677 (Pubnub_t2451529532 * __this, String_t* ___channel0, int64_t ___start1, Action_1_t2491248677 * ___userCallback2, Action_1_t4207084599 * ___errorCallback3, bool ___reverse4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::DetailedHistory(System.String,System.Int32,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_DetailedHistory_m896900401 (Pubnub_t2451529532 * __this, String_t* ___channel0, int32_t ___count1, Action_1_t2491248677 * ___userCallback2, Action_1_t4207084599 * ___errorCallback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::DetailedHistory(System.String,System.Int64,System.Int64,System.Int32,System.Boolean,System.Boolean,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_DetailedHistory_m1239418117 (Pubnub_t2451529532 * __this, String_t* ___channel0, int64_t ___start1, int64_t ___end2, int32_t ___count3, bool ___reverse4, bool ___includeTimetoken5, Action_1_t2491248677 * ___userCallback6, Action_1_t4207084599 * ___errorCallback7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::DetailedHistory(System.String,System.Int64,System.Boolean,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>,System.Boolean)
extern "C"  bool Pubnub_DetailedHistory_m1714499278 (Pubnub_t2451529532 * __this, String_t* ___channel0, int64_t ___start1, bool ___includeTimetoken2, Action_1_t2491248677 * ___userCallback3, Action_1_t4207084599 * ___errorCallback4, bool ___reverse5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::DetailedHistory(System.String,System.Int32,System.Boolean,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_DetailedHistory_m577887446 (Pubnub_t2451529532 * __this, String_t* ___channel0, int32_t ___count1, bool ___includeTimetoken2, Action_1_t2491248677 * ___userCallback3, Action_1_t4207084599 * ___errorCallback4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::HereNow(System.String,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_HereNow_m119957414 (Pubnub_t2451529532 * __this, String_t* ___channel0, Action_1_t2491248677 * ___userCallback1, Action_1_t4207084599 * ___errorCallback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::HereNow(System.String,System.Boolean,System.Boolean,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_HereNow_m1298613732 (Pubnub_t2451529532 * __this, String_t* ___channel0, bool ___showUUIDList1, bool ___includeUserState2, Action_1_t2491248677 * ___userCallback3, Action_1_t4207084599 * ___errorCallback4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::HereNow(System.String,System.String,System.Boolean,System.Boolean,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_HereNow_m2999329432 (Pubnub_t2451529532 * __this, String_t* ___channel0, String_t* ___channelGroup1, bool ___showUUIDList2, bool ___includeUserState3, Action_1_t2491248677 * ___userCallback4, Action_1_t4207084599 * ___errorCallback5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::GlobalHereNow(System.Boolean,System.Boolean,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_GlobalHereNow_m2382505303 (Pubnub_t2451529532 * __this, bool ___showUUIDList0, bool ___includeUserState1, Action_1_t2491248677 * ___userCallback2, Action_1_t4207084599 * ___errorCallback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::GlobalHereNow(System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_GlobalHereNow_m1402283139 (Pubnub_t2451529532 * __this, Action_1_t2491248677 * ___userCallback0, Action_1_t4207084599 * ___errorCallback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::WhereNow(System.String,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_WhereNow_m1791830301 (Pubnub_t2451529532 * __this, String_t* ___uuid0, Action_1_t2491248677 * ___userCallback1, Action_1_t4207084599 * ___errorCallback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::Unsubscribe(System.String,System.String,System.Action`1<System.Object>,System.Action`1<System.Object>,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_Unsubscribe_m286763275 (Pubnub_t2451529532 * __this, String_t* ___channel0, String_t* ___channelGroup1, Action_1_t2491248677 * ___userCallback2, Action_1_t2491248677 * ___connectCallback3, Action_1_t2491248677 * ___disconnectCallback4, Action_1_t4207084599 * ___errorCallback5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::Unsubscribe(System.String,System.Action`1<System.Object>,System.Action`1<System.Object>,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_Unsubscribe_m1815822113 (Pubnub_t2451529532 * __this, String_t* ___channel0, Action_1_t2491248677 * ___userCallback1, Action_1_t2491248677 * ___connectCallback2, Action_1_t2491248677 * ___disconnectCallback3, Action_1_t4207084599 * ___errorCallback4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::PresenceUnsubscribe(System.String,System.Action`1<System.Object>,System.Action`1<System.Object>,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_PresenceUnsubscribe_m3428408328 (Pubnub_t2451529532 * __this, String_t* ___channel0, Action_1_t2491248677 * ___userCallback1, Action_1_t2491248677 * ___connectCallback2, Action_1_t2491248677 * ___disconnectCallback3, Action_1_t4207084599 * ___errorCallback4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::PresenceUnsubscribe(System.String,System.String,System.Action`1<System.Object>,System.Action`1<System.Object>,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_PresenceUnsubscribe_m703048192 (Pubnub_t2451529532 * __this, String_t* ___channel0, String_t* ___channelGroup1, Action_1_t2491248677 * ___userCallback2, Action_1_t2491248677 * ___connectCallback3, Action_1_t2491248677 * ___disconnectCallback4, Action_1_t4207084599 * ___errorCallback5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::Time(System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_Time_m2636330233 (Pubnub_t2451529532 * __this, Action_1_t2491248677 * ___userCallback0, Action_1_t4207084599 * ___errorCallback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::AuditAccess(System.String,System.String,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_AuditAccess_m2310480701 (Pubnub_t2451529532 * __this, String_t* ___channel0, String_t* ___authenticationKey1, Action_1_t2491248677 * ___userCallback2, Action_1_t4207084599 * ___errorCallback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::AuditAccess(System.String,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_AuditAccess_m2794749227 (Pubnub_t2451529532 * __this, String_t* ___channel0, Action_1_t2491248677 * ___userCallback1, Action_1_t4207084599 * ___errorCallback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::AuditAccess(System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_AuditAccess_m3445268221 (Pubnub_t2451529532 * __this, Action_1_t2491248677 * ___userCallback0, Action_1_t4207084599 * ___errorCallback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::AuditPresenceAccess(System.String,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_AuditPresenceAccess_m3286500858 (Pubnub_t2451529532 * __this, String_t* ___channel0, Action_1_t2491248677 * ___userCallback1, Action_1_t4207084599 * ___errorCallback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::AuditPresenceAccess(System.String,System.String,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_AuditPresenceAccess_m1902444786 (Pubnub_t2451529532 * __this, String_t* ___channel0, String_t* ___authenticationKey1, Action_1_t2491248677 * ___userCallback2, Action_1_t4207084599 * ___errorCallback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::GrantAccess(System.String,System.Boolean,System.Boolean,System.Int32,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_GrantAccess_m943276765 (Pubnub_t2451529532 * __this, String_t* ___channel0, bool ___read1, bool ___write2, int32_t ___ttl3, Action_1_t2491248677 * ___userCallback4, Action_1_t4207084599 * ___errorCallback5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::GrantAccess(System.String,System.Boolean,System.Boolean,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_GrantAccess_m2605968682 (Pubnub_t2451529532 * __this, String_t* ___channel0, bool ___read1, bool ___write2, Action_1_t2491248677 * ___userCallback3, Action_1_t4207084599 * ___errorCallback4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::GrantAccess(System.String,System.String,System.Boolean,System.Boolean,System.Int32,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_GrantAccess_m2605614527 (Pubnub_t2451529532 * __this, String_t* ___channel0, String_t* ___authenticationKey1, bool ___read2, bool ___write3, int32_t ___ttl4, Action_1_t2491248677 * ___userCallback5, Action_1_t4207084599 * ___errorCallback6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::GrantAccess(System.String,System.String,System.Boolean,System.Boolean,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_GrantAccess_m4207390986 (Pubnub_t2451529532 * __this, String_t* ___channel0, String_t* ___authenticationKey1, bool ___read2, bool ___write3, Action_1_t2491248677 * ___userCallback4, Action_1_t4207084599 * ___errorCallback5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::GrantPresenceAccess(System.String,System.Boolean,System.Boolean,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_GrantPresenceAccess_m522113969 (Pubnub_t2451529532 * __this, String_t* ___channel0, bool ___read1, bool ___write2, Action_1_t2491248677 * ___userCallback3, Action_1_t4207084599 * ___errorCallback4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::GrantPresenceAccess(System.String,System.Boolean,System.Boolean,System.Int32,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_GrantPresenceAccess_m2585464836 (Pubnub_t2451529532 * __this, String_t* ___channel0, bool ___read1, bool ___write2, int32_t ___ttl3, Action_1_t2491248677 * ___userCallback4, Action_1_t4207084599 * ___errorCallback5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::GrantPresenceAccess(System.String,System.String,System.Boolean,System.Boolean,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_GrantPresenceAccess_m2742555151 (Pubnub_t2451529532 * __this, String_t* ___channel0, String_t* ___authenticationKey1, bool ___read2, bool ___write3, Action_1_t2491248677 * ___userCallback4, Action_1_t4207084599 * ___errorCallback5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::GrantPresenceAccess(System.String,System.String,System.Boolean,System.Boolean,System.Int32,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  bool Pubnub_GrantPresenceAccess_m3723092580 (Pubnub_t2451529532 * __this, String_t* ___channel0, String_t* ___authenticationKey1, bool ___read2, bool ___write3, int32_t ___ttl4, Action_1_t2491248677 * ___userCallback5, Action_1_t4207084599 * ___errorCallback6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::SetUserState(System.String,System.String,System.String,System.String,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_SetUserState_m2191049140 (Pubnub_t2451529532 * __this, String_t* ___channel0, String_t* ___channelGroup1, String_t* ___uuid2, String_t* ___jsonUserState3, Action_1_t2491248677 * ___userCallback4, Action_1_t4207084599 * ___errorCallback5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::SetUserState(System.String,System.String,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_SetUserState_m2583620468 (Pubnub_t2451529532 * __this, String_t* ___channel0, String_t* ___jsonUserState1, Action_1_t2491248677 * ___userCallback2, Action_1_t4207084599 * ___errorCallback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::SetUserState(System.String,System.String,System.String,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_SetUserState_m4197991220 (Pubnub_t2451529532 * __this, String_t* ___channel0, String_t* ___uuid1, String_t* ___jsonUserState2, Action_1_t2491248677 * ___userCallback3, Action_1_t4207084599 * ___errorCallback4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::SetUserState(System.String,System.String,System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_SetUserState_m1276587185 (Pubnub_t2451529532 * __this, String_t* ___channel0, String_t* ___uuid1, KeyValuePair_2_t2361573779  ___keyValuePair2, Action_1_t2491248677 * ___userCallback3, Action_1_t4207084599 * ___errorCallback4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::SetUserState(System.String,System.Collections.Generic.KeyValuePair`2<System.String,System.Object>,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_SetUserState_m3326740583 (Pubnub_t2451529532 * __this, String_t* ___channel0, KeyValuePair_2_t2361573779  ___keyValuePair1, Action_1_t2491248677 * ___userCallback2, Action_1_t4207084599 * ___errorCallback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::GetUserState(System.String,System.String,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_GetUserState_m1137514160 (Pubnub_t2451529532 * __this, String_t* ___channel0, String_t* ___uuid1, Action_1_t2491248677 * ___userCallback2, Action_1_t4207084599 * ___errorCallback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::GetUserState(System.String,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_GetUserState_m3651131952 (Pubnub_t2451529532 * __this, String_t* ___channel0, Action_1_t2491248677 * ___userCallback1, Action_1_t4207084599 * ___errorCallback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::GetUserState(System.String,System.String,System.String,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_GetUserState_m2351318000 (Pubnub_t2451529532 * __this, String_t* ___channel0, String_t* ___channelGroup1, String_t* ___uuid2, Action_1_t2491248677 * ___userCallback3, Action_1_t4207084599 * ___errorCallback4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::RegisterDeviceForPush(System.String,PubNubMessaging.Core.PushTypeService,System.String,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_RegisterDeviceForPush_m3448164514 (Pubnub_t2451529532 * __this, String_t* ___channel0, int32_t ___pushType1, String_t* ___pushToken2, Action_1_t2491248677 * ___userCallback3, Action_1_t4207084599 * ___errorCallback4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::UnregisterDeviceForPush(PubNubMessaging.Core.PushTypeService,System.String,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_UnregisterDeviceForPush_m395603505 (Pubnub_t2451529532 * __this, int32_t ___pushType0, String_t* ___pushToken1, Action_1_t2491248677 * ___userCallback2, Action_1_t4207084599 * ___errorCallback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::RemoveChannelForDevicePush(System.String,PubNubMessaging.Core.PushTypeService,System.String,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_RemoveChannelForDevicePush_m3453509782 (Pubnub_t2451529532 * __this, String_t* ___channel0, int32_t ___pushType1, String_t* ___pushToken2, Action_1_t2491248677 * ___userCallback3, Action_1_t4207084599 * ___errorCallback4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::GetChannelsForDevicePush(PubNubMessaging.Core.PushTypeService,System.String,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_GetChannelsForDevicePush_m1861390641 (Pubnub_t2451529532 * __this, int32_t ___pushType0, String_t* ___pushToken1, Action_1_t2491248677 * ___userCallback2, Action_1_t4207084599 * ___errorCallback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::AddChannelsToChannelGroup(System.String[],System.String,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_AddChannelsToChannelGroup_m2469899046 (Pubnub_t2451529532 * __this, StringU5BU5D_t1642385972* ___channels0, String_t* ___channelGroup1, Action_1_t2491248677 * ___userCallback2, Action_1_t4207084599 * ___errorCallback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::RemoveChannelsFromChannelGroup(System.String[],System.String,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_RemoveChannelsFromChannelGroup_m4052559530 (Pubnub_t2451529532 * __this, StringU5BU5D_t1642385972* ___channels0, String_t* ___channelGroup1, Action_1_t2491248677 * ___userCallback2, Action_1_t4207084599 * ___errorCallback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::RemoveChannelGroup(System.String,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_RemoveChannelGroup_m1010419842 (Pubnub_t2451529532 * __this, String_t* ___channelGroup0, Action_1_t2491248677 * ___userCallback1, Action_1_t4207084599 * ___errorCallback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::GetChannelsForChannelGroup(System.String,System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_GetChannelsForChannelGroup_m4291927371 (Pubnub_t2451529532 * __this, String_t* ___channelGroup0, Action_1_t2491248677 * ___userCallback1, Action_1_t4207084599 * ___errorCallback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::GetAllChannelGroups(System.Action`1<System.Object>,System.Action`1<PubNubMessaging.Core.PubnubClientError>)
extern "C"  void Pubnub_GetAllChannelGroups_m836756172 (Pubnub_t2451529532 * __this, Action_1_t2491248677 * ___userCallback0, Action_1_t4207084599 * ___errorCallback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::ResetPublishMessageCounter()
extern "C"  void Pubnub_ResetPublishMessageCounter_m2791329785 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::CleanUp()
extern "C"  void Pubnub_CleanUp_m3616753212 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid PubNubMessaging.Core.Pubnub::GenerateGuid()
extern "C"  Guid_t2533601593  Pubnub_GenerateGuid_m256018945 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::TerminateCurrentSubscriberRequest()
extern "C"  void Pubnub_TerminateCurrentSubscriberRequest_m3329387325 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::EndPendingRequests()
extern "C"  void Pubnub_EndPendingRequests_m2437671124 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::ChangeUUID(System.String)
extern "C"  void Pubnub_ChangeUUID_m1807290335 (Pubnub_t2451529532 * __this, String_t* ___newUUID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 PubNubMessaging.Core.Pubnub::TranslateDateTimeToPubnubUnixNanoSeconds(System.DateTime)
extern "C"  int64_t Pubnub_TranslateDateTimeToPubnubUnixNanoSeconds_m3181893166 (Il2CppObject * __this /* static, unused */, DateTime_t693205669  ___dotNetUTCDateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime PubNubMessaging.Core.Pubnub::TranslatePubnubUnixNanoSecondsToDateTime(System.Int64)
extern "C"  DateTime_t693205669  Pubnub_TranslatePubnubUnixNanoSecondsToDateTime_m958233646 (Il2CppObject * __this /* static, unused */, int64_t ___unixNanoSecondTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.Pubnub::get_FilterExpression()
extern "C"  String_t* Pubnub_get_FilterExpression_m585264476 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::set_FilterExpression(System.String)
extern "C"  void Pubnub_set_FilterExpression_m3898888095 (Pubnub_t2451529532 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.Pubnub::get_AuthenticationKey()
extern "C"  String_t* Pubnub_get_AuthenticationKey_m2995397417 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::set_AuthenticationKey(System.String)
extern "C"  void Pubnub_set_AuthenticationKey_m4180133050 (Pubnub_t2451529532 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.LoggingMethod/Level PubNubMessaging.Core.Pubnub::get_PubnubLogLevel()
extern "C"  int32_t Pubnub_get_PubnubLogLevel_m428215216 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::set_PubnubLogLevel(PubNubMessaging.Core.LoggingMethod/Level)
extern "C"  void Pubnub_set_PubnubLogLevel_m1887277777 (Pubnub_t2451529532 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.PubnubErrorFilter/Level PubNubMessaging.Core.Pubnub::get_PubnubErrorLevel()
extern "C"  int32_t Pubnub_get_PubnubErrorLevel_m2362746032 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::set_PubnubErrorLevel(PubNubMessaging.Core.PubnubErrorFilter/Level)
extern "C"  void Pubnub_set_PubnubErrorLevel_m3351109295 (Pubnub_t2451529532 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.Pubnub::get_LocalClientHeartbeatInterval()
extern "C"  int32_t Pubnub_get_LocalClientHeartbeatInterval_m2765571048 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::set_LocalClientHeartbeatInterval(System.Int32)
extern "C"  void Pubnub_set_LocalClientHeartbeatInterval_m2458089905 (Pubnub_t2451529532 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.Pubnub::get_NetworkCheckRetryInterval()
extern "C"  int32_t Pubnub_get_NetworkCheckRetryInterval_m1563916964 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::set_NetworkCheckRetryInterval(System.Int32)
extern "C"  void Pubnub_set_NetworkCheckRetryInterval_m4069542621 (Pubnub_t2451529532 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.Pubnub::get_NetworkCheckMaxRetries()
extern "C"  int32_t Pubnub_get_NetworkCheckMaxRetries_m3149353667 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::set_NetworkCheckMaxRetries(System.Int32)
extern "C"  void Pubnub_set_NetworkCheckMaxRetries_m2593194660 (Pubnub_t2451529532 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.Pubnub::get_NonSubscribeTimeout()
extern "C"  int32_t Pubnub_get_NonSubscribeTimeout_m2178013621 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::set_NonSubscribeTimeout(System.Int32)
extern "C"  void Pubnub_set_NonSubscribeTimeout_m766500718 (Pubnub_t2451529532 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.Pubnub::get_SubscribeTimeout()
extern "C"  int32_t Pubnub_get_SubscribeTimeout_m890109226 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::set_SubscribeTimeout(System.Int32)
extern "C"  void Pubnub_set_SubscribeTimeout_m2847999361 (Pubnub_t2451529532 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::get_EnableResumeOnReconnect()
extern "C"  bool Pubnub_get_EnableResumeOnReconnect_m1361302235 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::set_EnableResumeOnReconnect(System.Boolean)
extern "C"  void Pubnub_set_EnableResumeOnReconnect_m1679692608 (Pubnub_t2451529532 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.Pubnub::get_SessionUUID()
extern "C"  String_t* Pubnub_get_SessionUUID_m997055021 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::set_SessionUUID(System.String)
extern "C"  void Pubnub_set_SessionUUID_m616112486 (Pubnub_t2451529532 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.Pubnub::get_Origin()
extern "C"  String_t* Pubnub_get_Origin_m810041386 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::set_Origin(System.String)
extern "C"  void Pubnub_set_Origin_m2733234845 (Pubnub_t2451529532 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.Pubnub::get_PresenceHeartbeat()
extern "C"  int32_t Pubnub_get_PresenceHeartbeat_m2821732694 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::set_PresenceHeartbeat(System.Int32)
extern "C"  void Pubnub_set_PresenceHeartbeat_m2842151215 (Pubnub_t2451529532 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.Pubnub::get_PresenceHeartbeatInterval()
extern "C"  int32_t Pubnub_get_PresenceHeartbeatInterval_m2284209987 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::set_PresenceHeartbeatInterval(System.Int32)
extern "C"  void Pubnub_set_PresenceHeartbeatInterval_m1846252984 (Pubnub_t2451529532 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.IPubnubUnitTest PubNubMessaging.Core.Pubnub::get_PubnubUnitTest()
extern "C"  Il2CppObject * Pubnub_get_PubnubUnitTest_m299317490 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::set_PubnubUnitTest(PubNubMessaging.Core.IPubnubUnitTest)
extern "C"  void Pubnub_set_PubnubUnitTest_m2942769069 (Pubnub_t2451529532 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Pubnub::get_EnableJsonEncodingForPublish()
extern "C"  bool Pubnub_get_EnableJsonEncodingForPublish_m3764199815 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::set_EnableJsonEncodingForPublish(System.Boolean)
extern "C"  void Pubnub_set_EnableJsonEncodingForPublish_m1299350120 (Pubnub_t2451529532 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.IJsonPluggableLibrary PubNubMessaging.Core.Pubnub::get_JsonPluggableLibrary()
extern "C"  Il2CppObject * Pubnub_get_JsonPluggableLibrary_m1194463730 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::set_JsonPluggableLibrary(PubNubMessaging.Core.IJsonPluggableLibrary)
extern "C"  void Pubnub_set_JsonPluggableLibrary_m2798941153 (Pubnub_t2451529532 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.Pubnub::get_Version()
extern "C"  String_t* Pubnub_get_Version_m3400259386 (Pubnub_t2451529532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject PubNubMessaging.Core.Pubnub::get_SetGameObject()
extern "C"  GameObject_t1756533147 * Pubnub_get_SetGameObject_m1729419881 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Pubnub::set_SetGameObject(UnityEngine.GameObject)
extern "C"  void Pubnub_set_SetGameObject_m3521040876 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
