﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// PubNubMessaging.Core.IJsonPluggableLibrary
struct IJsonPluggableLibrary_t1579330875;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// PubNubMessaging.Core.PubnubClientError
struct PubnubClientError_t110317921;
// System.Action`1<PubNubMessaging.Core.PubnubClientError>
struct Action_1_t4207084599;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubClient110317921.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubErrorF397889342.h"

// System.Void PubNubMessaging.Core.PubnubCallbacks::GoToCallback(System.Object,System.Action`1<System.String>,PubNubMessaging.Core.IJsonPluggableLibrary)
extern "C"  void PubnubCallbacks_GoToCallback_m1331148564 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___result0, Action_1_t1831019615 * ___Callback1, Il2CppObject * ___jsonPluggableLibrary2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubCallbacks::GoToCallback(System.Object,System.Action`1<System.Object>)
extern "C"  void PubnubCallbacks_GoToCallback_m3236236034 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___result0, Action_1_t2491248677 * ___Callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubCallbacks::GoToCallback(PubNubMessaging.Core.PubnubClientError,System.Action`1<PubNubMessaging.Core.PubnubClientError>,PubNubMessaging.Core.PubnubErrorFilter/Level)
extern "C"  void PubnubCallbacks_GoToCallback_m305221034 (Il2CppObject * __this /* static, unused */, PubnubClientError_t110317921 * ___error0, Action_1_t4207084599 * ___Callback1, int32_t ___errorLevel2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
