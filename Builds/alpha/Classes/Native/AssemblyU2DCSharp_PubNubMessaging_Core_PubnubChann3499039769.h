﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action`1<PubNubMessaging.Core.PubnubClientError>
struct Action_1_t4207084599;
// System.Action`1<PubNubMessaging.Core.PNMessageResult>
struct Action_1_t557919624;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.PubnubChannelCallback`1<System.Object>
struct  PubnubChannelCallback_1_t3499039769  : public Il2CppObject
{
public:
	// System.Action`1<T> PubNubMessaging.Core.PubnubChannelCallback`1::SuccessCallback
	Action_1_t2491248677 * ___SuccessCallback_0;
	// System.Action`1<PubNubMessaging.Core.PubnubClientError> PubNubMessaging.Core.PubnubChannelCallback`1::ErrorCallback
	Action_1_t4207084599 * ___ErrorCallback_1;
	// System.Action`1<PubNubMessaging.Core.PNMessageResult> PubNubMessaging.Core.PubnubChannelCallback`1::MessageCallback
	Action_1_t557919624 * ___MessageCallback_2;
	// System.Action`1<T> PubNubMessaging.Core.PubnubChannelCallback`1::ConnectCallback
	Action_1_t2491248677 * ___ConnectCallback_3;
	// System.Action`1<T> PubNubMessaging.Core.PubnubChannelCallback`1::DisconnectCallback
	Action_1_t2491248677 * ___DisconnectCallback_4;
	// System.Action`1<T> PubNubMessaging.Core.PubnubChannelCallback`1::WildcardPresenceCallback
	Action_1_t2491248677 * ___WildcardPresenceCallback_5;

public:
	inline static int32_t get_offset_of_SuccessCallback_0() { return static_cast<int32_t>(offsetof(PubnubChannelCallback_1_t3499039769, ___SuccessCallback_0)); }
	inline Action_1_t2491248677 * get_SuccessCallback_0() const { return ___SuccessCallback_0; }
	inline Action_1_t2491248677 ** get_address_of_SuccessCallback_0() { return &___SuccessCallback_0; }
	inline void set_SuccessCallback_0(Action_1_t2491248677 * value)
	{
		___SuccessCallback_0 = value;
		Il2CppCodeGenWriteBarrier(&___SuccessCallback_0, value);
	}

	inline static int32_t get_offset_of_ErrorCallback_1() { return static_cast<int32_t>(offsetof(PubnubChannelCallback_1_t3499039769, ___ErrorCallback_1)); }
	inline Action_1_t4207084599 * get_ErrorCallback_1() const { return ___ErrorCallback_1; }
	inline Action_1_t4207084599 ** get_address_of_ErrorCallback_1() { return &___ErrorCallback_1; }
	inline void set_ErrorCallback_1(Action_1_t4207084599 * value)
	{
		___ErrorCallback_1 = value;
		Il2CppCodeGenWriteBarrier(&___ErrorCallback_1, value);
	}

	inline static int32_t get_offset_of_MessageCallback_2() { return static_cast<int32_t>(offsetof(PubnubChannelCallback_1_t3499039769, ___MessageCallback_2)); }
	inline Action_1_t557919624 * get_MessageCallback_2() const { return ___MessageCallback_2; }
	inline Action_1_t557919624 ** get_address_of_MessageCallback_2() { return &___MessageCallback_2; }
	inline void set_MessageCallback_2(Action_1_t557919624 * value)
	{
		___MessageCallback_2 = value;
		Il2CppCodeGenWriteBarrier(&___MessageCallback_2, value);
	}

	inline static int32_t get_offset_of_ConnectCallback_3() { return static_cast<int32_t>(offsetof(PubnubChannelCallback_1_t3499039769, ___ConnectCallback_3)); }
	inline Action_1_t2491248677 * get_ConnectCallback_3() const { return ___ConnectCallback_3; }
	inline Action_1_t2491248677 ** get_address_of_ConnectCallback_3() { return &___ConnectCallback_3; }
	inline void set_ConnectCallback_3(Action_1_t2491248677 * value)
	{
		___ConnectCallback_3 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectCallback_3, value);
	}

	inline static int32_t get_offset_of_DisconnectCallback_4() { return static_cast<int32_t>(offsetof(PubnubChannelCallback_1_t3499039769, ___DisconnectCallback_4)); }
	inline Action_1_t2491248677 * get_DisconnectCallback_4() const { return ___DisconnectCallback_4; }
	inline Action_1_t2491248677 ** get_address_of_DisconnectCallback_4() { return &___DisconnectCallback_4; }
	inline void set_DisconnectCallback_4(Action_1_t2491248677 * value)
	{
		___DisconnectCallback_4 = value;
		Il2CppCodeGenWriteBarrier(&___DisconnectCallback_4, value);
	}

	inline static int32_t get_offset_of_WildcardPresenceCallback_5() { return static_cast<int32_t>(offsetof(PubnubChannelCallback_1_t3499039769, ___WildcardPresenceCallback_5)); }
	inline Action_1_t2491248677 * get_WildcardPresenceCallback_5() const { return ___WildcardPresenceCallback_5; }
	inline Action_1_t2491248677 ** get_address_of_WildcardPresenceCallback_5() { return &___WildcardPresenceCallback_5; }
	inline void set_WildcardPresenceCallback_5(Action_1_t2491248677 * value)
	{
		___WildcardPresenceCallback_5 = value;
		Il2CppCodeGenWriteBarrier(&___WildcardPresenceCallback_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
