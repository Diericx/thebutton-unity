﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.PubnubChannelCallback`1<System.Object>
struct PubnubChannelCallback_1_t3499039769;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Core.PubnubChannelCallback`1<System.Object>::.ctor()
extern "C"  void PubnubChannelCallback_1__ctor_m3406287877_gshared (PubnubChannelCallback_1_t3499039769 * __this, const MethodInfo* method);
#define PubnubChannelCallback_1__ctor_m3406287877(__this, method) ((  void (*) (PubnubChannelCallback_1_t3499039769 *, const MethodInfo*))PubnubChannelCallback_1__ctor_m3406287877_gshared)(__this, method)
