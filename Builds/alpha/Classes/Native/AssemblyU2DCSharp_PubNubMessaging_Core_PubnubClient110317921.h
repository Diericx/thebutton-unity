﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Exception
struct Exception_t1927440687;
// PubNubMessaging.Core.PubnubWebRequest
struct PubnubWebRequest_t3863823607;
// PubNubMessaging.Core.PubnubWebResponse
struct PubnubWebResponse_t647984363;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubErrorSe59688891.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubMessa1890139634.h"
#include "mscorlib_System_DateTime693205669.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.PubnubClientError
struct  PubnubClientError_t110317921  : public Il2CppObject
{
public:
	// System.Int32 PubNubMessaging.Core.PubnubClientError::_statusCode
	int32_t ____statusCode_0;
	// PubNubMessaging.Core.PubnubErrorSeverity PubNubMessaging.Core.PubnubClientError::_errorSeverity
	int32_t ____errorSeverity_1;
	// System.Boolean PubNubMessaging.Core.PubnubClientError::_isDotNetException
	bool ____isDotNetException_2;
	// PubNubMessaging.Core.PubnubMessageSource PubNubMessaging.Core.PubnubClientError::_messageSource
	int32_t ____messageSource_3;
	// System.String PubNubMessaging.Core.PubnubClientError::_message
	String_t* ____message_4;
	// System.String PubNubMessaging.Core.PubnubClientError::_channel
	String_t* ____channel_5;
	// System.String PubNubMessaging.Core.PubnubClientError::_channelGroup
	String_t* ____channelGroup_6;
	// System.Exception PubNubMessaging.Core.PubnubClientError::_detailedDotNetException
	Exception_t1927440687 * ____detailedDotNetException_7;
	// PubNubMessaging.Core.PubnubWebRequest PubNubMessaging.Core.PubnubClientError::_pubnubWebRequest
	PubnubWebRequest_t3863823607 * ____pubnubWebRequest_8;
	// PubNubMessaging.Core.PubnubWebResponse PubNubMessaging.Core.PubnubClientError::_pubnubWebResponse
	PubnubWebResponse_t647984363 * ____pubnubWebResponse_9;
	// System.String PubNubMessaging.Core.PubnubClientError::_description
	String_t* ____description_10;
	// System.DateTime PubNubMessaging.Core.PubnubClientError::_dateTimeGMT
	DateTime_t693205669  ____dateTimeGMT_11;

public:
	inline static int32_t get_offset_of__statusCode_0() { return static_cast<int32_t>(offsetof(PubnubClientError_t110317921, ____statusCode_0)); }
	inline int32_t get__statusCode_0() const { return ____statusCode_0; }
	inline int32_t* get_address_of__statusCode_0() { return &____statusCode_0; }
	inline void set__statusCode_0(int32_t value)
	{
		____statusCode_0 = value;
	}

	inline static int32_t get_offset_of__errorSeverity_1() { return static_cast<int32_t>(offsetof(PubnubClientError_t110317921, ____errorSeverity_1)); }
	inline int32_t get__errorSeverity_1() const { return ____errorSeverity_1; }
	inline int32_t* get_address_of__errorSeverity_1() { return &____errorSeverity_1; }
	inline void set__errorSeverity_1(int32_t value)
	{
		____errorSeverity_1 = value;
	}

	inline static int32_t get_offset_of__isDotNetException_2() { return static_cast<int32_t>(offsetof(PubnubClientError_t110317921, ____isDotNetException_2)); }
	inline bool get__isDotNetException_2() const { return ____isDotNetException_2; }
	inline bool* get_address_of__isDotNetException_2() { return &____isDotNetException_2; }
	inline void set__isDotNetException_2(bool value)
	{
		____isDotNetException_2 = value;
	}

	inline static int32_t get_offset_of__messageSource_3() { return static_cast<int32_t>(offsetof(PubnubClientError_t110317921, ____messageSource_3)); }
	inline int32_t get__messageSource_3() const { return ____messageSource_3; }
	inline int32_t* get_address_of__messageSource_3() { return &____messageSource_3; }
	inline void set__messageSource_3(int32_t value)
	{
		____messageSource_3 = value;
	}

	inline static int32_t get_offset_of__message_4() { return static_cast<int32_t>(offsetof(PubnubClientError_t110317921, ____message_4)); }
	inline String_t* get__message_4() const { return ____message_4; }
	inline String_t** get_address_of__message_4() { return &____message_4; }
	inline void set__message_4(String_t* value)
	{
		____message_4 = value;
		Il2CppCodeGenWriteBarrier(&____message_4, value);
	}

	inline static int32_t get_offset_of__channel_5() { return static_cast<int32_t>(offsetof(PubnubClientError_t110317921, ____channel_5)); }
	inline String_t* get__channel_5() const { return ____channel_5; }
	inline String_t** get_address_of__channel_5() { return &____channel_5; }
	inline void set__channel_5(String_t* value)
	{
		____channel_5 = value;
		Il2CppCodeGenWriteBarrier(&____channel_5, value);
	}

	inline static int32_t get_offset_of__channelGroup_6() { return static_cast<int32_t>(offsetof(PubnubClientError_t110317921, ____channelGroup_6)); }
	inline String_t* get__channelGroup_6() const { return ____channelGroup_6; }
	inline String_t** get_address_of__channelGroup_6() { return &____channelGroup_6; }
	inline void set__channelGroup_6(String_t* value)
	{
		____channelGroup_6 = value;
		Il2CppCodeGenWriteBarrier(&____channelGroup_6, value);
	}

	inline static int32_t get_offset_of__detailedDotNetException_7() { return static_cast<int32_t>(offsetof(PubnubClientError_t110317921, ____detailedDotNetException_7)); }
	inline Exception_t1927440687 * get__detailedDotNetException_7() const { return ____detailedDotNetException_7; }
	inline Exception_t1927440687 ** get_address_of__detailedDotNetException_7() { return &____detailedDotNetException_7; }
	inline void set__detailedDotNetException_7(Exception_t1927440687 * value)
	{
		____detailedDotNetException_7 = value;
		Il2CppCodeGenWriteBarrier(&____detailedDotNetException_7, value);
	}

	inline static int32_t get_offset_of__pubnubWebRequest_8() { return static_cast<int32_t>(offsetof(PubnubClientError_t110317921, ____pubnubWebRequest_8)); }
	inline PubnubWebRequest_t3863823607 * get__pubnubWebRequest_8() const { return ____pubnubWebRequest_8; }
	inline PubnubWebRequest_t3863823607 ** get_address_of__pubnubWebRequest_8() { return &____pubnubWebRequest_8; }
	inline void set__pubnubWebRequest_8(PubnubWebRequest_t3863823607 * value)
	{
		____pubnubWebRequest_8 = value;
		Il2CppCodeGenWriteBarrier(&____pubnubWebRequest_8, value);
	}

	inline static int32_t get_offset_of__pubnubWebResponse_9() { return static_cast<int32_t>(offsetof(PubnubClientError_t110317921, ____pubnubWebResponse_9)); }
	inline PubnubWebResponse_t647984363 * get__pubnubWebResponse_9() const { return ____pubnubWebResponse_9; }
	inline PubnubWebResponse_t647984363 ** get_address_of__pubnubWebResponse_9() { return &____pubnubWebResponse_9; }
	inline void set__pubnubWebResponse_9(PubnubWebResponse_t647984363 * value)
	{
		____pubnubWebResponse_9 = value;
		Il2CppCodeGenWriteBarrier(&____pubnubWebResponse_9, value);
	}

	inline static int32_t get_offset_of__description_10() { return static_cast<int32_t>(offsetof(PubnubClientError_t110317921, ____description_10)); }
	inline String_t* get__description_10() const { return ____description_10; }
	inline String_t** get_address_of__description_10() { return &____description_10; }
	inline void set__description_10(String_t* value)
	{
		____description_10 = value;
		Il2CppCodeGenWriteBarrier(&____description_10, value);
	}

	inline static int32_t get_offset_of__dateTimeGMT_11() { return static_cast<int32_t>(offsetof(PubnubClientError_t110317921, ____dateTimeGMT_11)); }
	inline DateTime_t693205669  get__dateTimeGMT_11() const { return ____dateTimeGMT_11; }
	inline DateTime_t693205669 * get_address_of__dateTimeGMT_11() { return &____dateTimeGMT_11; }
	inline void set__dateTimeGMT_11(DateTime_t693205669  value)
	{
		____dateTimeGMT_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
