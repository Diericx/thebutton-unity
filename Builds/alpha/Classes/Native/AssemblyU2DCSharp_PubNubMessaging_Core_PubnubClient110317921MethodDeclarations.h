﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.PubnubClientError
struct PubnubClientError_t110317921;
// System.String
struct String_t;
// System.Exception
struct Exception_t1927440687;
// PubNubMessaging.Core.PubnubWebRequest
struct PubnubWebRequest_t3863823607;
// PubNubMessaging.Core.PubnubWebResponse
struct PubnubWebResponse_t647984363;
// System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>
struct List_1_t2635275738;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubErrorSe59688891.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Exception1927440687.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubMessa1890139634.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubWebRe3863823607.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubWebRes647984363.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Void PubNubMessaging.Core.PubnubClientError::.ctor()
extern "C"  void PubnubClientError__ctor_m2557315591 (PubnubClientError_t110317921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubClientError::.ctor(System.Int32,PubNubMessaging.Core.PubnubErrorSeverity,System.Boolean,System.String,System.Exception,PubNubMessaging.Core.PubnubMessageSource,PubNubMessaging.Core.PubnubWebRequest,PubNubMessaging.Core.PubnubWebResponse,System.String,System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>)
extern "C"  void PubnubClientError__ctor_m2764926875 (PubnubClientError_t110317921 * __this, int32_t ___statusCode0, int32_t ___errorSeverity1, bool ___isDotNetException2, String_t* ___message3, Exception_t1927440687 * ___detailedDotNetException4, int32_t ___source5, PubnubWebRequest_t3863823607 * ___pubnubWebRequest6, PubnubWebResponse_t647984363 * ___pubnubWebResponse7, String_t* ___description8, List_1_t2635275738 * ___channelEntitles9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubClientError::.ctor(System.Int32,PubNubMessaging.Core.PubnubErrorSeverity,System.Boolean,System.String,System.Exception,PubNubMessaging.Core.PubnubMessageSource,PubNubMessaging.Core.PubnubWebRequest,PubNubMessaging.Core.PubnubWebResponse,System.String,System.String,System.String)
extern "C"  void PubnubClientError__ctor_m3347328438 (PubnubClientError_t110317921 * __this, int32_t ___statusCode0, int32_t ___errorSeverity1, bool ___isDotNetException2, String_t* ___message3, Exception_t1927440687 * ___detailedDotNetException4, int32_t ___source5, PubnubWebRequest_t3863823607 * ___pubnubWebRequest6, PubnubWebResponse_t647984363 * ___pubnubWebResponse7, String_t* ___description8, String_t* ___channels9, String_t* ___channelGroups10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubClientError::.ctor(System.Int32,PubNubMessaging.Core.PubnubErrorSeverity,System.String,PubNubMessaging.Core.PubnubMessageSource,PubNubMessaging.Core.PubnubWebRequest,PubNubMessaging.Core.PubnubWebResponse,System.String,System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>)
extern "C"  void PubnubClientError__ctor_m883049172 (PubnubClientError_t110317921 * __this, int32_t ___statusCode0, int32_t ___errorSeverity1, String_t* ___message2, int32_t ___source3, PubnubWebRequest_t3863823607 * ___pubnubWebRequest4, PubnubWebResponse_t647984363 * ___pubnubWebResponse5, String_t* ___description6, List_1_t2635275738 * ___channelEntitles7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubClientError::.ctor(System.Int32,PubNubMessaging.Core.PubnubErrorSeverity,System.String,PubNubMessaging.Core.PubnubMessageSource,PubNubMessaging.Core.PubnubWebRequest,PubNubMessaging.Core.PubnubWebResponse,System.String,System.String,System.String)
extern "C"  void PubnubClientError__ctor_m3498777945 (PubnubClientError_t110317921 * __this, int32_t ___statusCode0, int32_t ___errorSeverity1, String_t* ___message2, int32_t ___source3, PubnubWebRequest_t3863823607 * ___pubnubWebRequest4, PubnubWebResponse_t647984363 * ___pubnubWebResponse5, String_t* ___description6, String_t* ___channels7, String_t* ___channelGroups8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.PubnubClientError::get_StatusCode()
extern "C"  int32_t PubnubClientError_get_StatusCode_m3226553933 (PubnubClientError_t110317921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.PubnubErrorSeverity PubNubMessaging.Core.PubnubClientError::get_Severity()
extern "C"  int32_t PubnubClientError_get_Severity_m424110984 (PubnubClientError_t110317921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.PubnubMessageSource PubNubMessaging.Core.PubnubClientError::get_MessageSource()
extern "C"  int32_t PubnubClientError_get_MessageSource_m841806442 (PubnubClientError_t110317921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.PubnubClientError::get_IsDotNetException()
extern "C"  bool PubnubClientError_get_IsDotNetException_m2442537095 (PubnubClientError_t110317921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubClientError::get_Message()
extern "C"  String_t* PubnubClientError_get_Message_m2996717984 (PubnubClientError_t110317921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception PubNubMessaging.Core.PubnubClientError::get_DetailedDotNetException()
extern "C"  Exception_t1927440687 * PubnubClientError_get_DetailedDotNetException_m325729592 (PubnubClientError_t110317921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.PubnubWebRequest PubNubMessaging.Core.PubnubClientError::get_PubnubWebRequest()
extern "C"  PubnubWebRequest_t3863823607 * PubnubClientError_get_PubnubWebRequest_m18077090 (PubnubClientError_t110317921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.PubnubWebResponse PubNubMessaging.Core.PubnubClientError::get_PubnubWebResponse()
extern "C"  PubnubWebResponse_t647984363 * PubnubClientError_get_PubnubWebResponse_m861669450 (PubnubClientError_t110317921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubClientError::get_Channel()
extern "C"  String_t* PubnubClientError_get_Channel_m2107700108 (PubnubClientError_t110317921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubClientError::get_ChannelGroup()
extern "C"  String_t* PubnubClientError_get_ChannelGroup_m2677612191 (PubnubClientError_t110317921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubClientError::get_Description()
extern "C"  String_t* PubnubClientError_get_Description_m1095120233 (PubnubClientError_t110317921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime PubNubMessaging.Core.PubnubClientError::get_ErrorDateTimeGMT()
extern "C"  DateTime_t693205669  PubnubClientError_get_ErrorDateTimeGMT_m703035182 (PubnubClientError_t110317921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubClientError::ToString()
extern "C"  String_t* PubnubClientError_ToString_m3572737566 (PubnubClientError_t110317921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
