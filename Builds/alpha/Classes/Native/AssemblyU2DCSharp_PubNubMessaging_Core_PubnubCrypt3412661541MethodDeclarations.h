﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.PubnubCrypto
struct PubnubCrypto_t3412661541;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PubNubMessaging.Core.PubnubCrypto::.ctor(System.String)
extern "C"  void PubnubCrypto__ctor_m811257883 (PubnubCrypto_t3412661541 * __this, String_t* ___cipher_key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubCrypto::ComputeHashRaw(System.String)
extern "C"  String_t* PubnubCrypto_ComputeHashRaw_m1837676053 (PubnubCrypto_t3412661541 * __this, String_t* ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubCrypto::EncryptOrDecrypt(System.Boolean,System.String)
extern "C"  String_t* PubnubCrypto_EncryptOrDecrypt_m2808961036 (PubnubCrypto_t3412661541 * __this, bool ___type0, String_t* ___plainStr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
