﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Text.RegularExpressions.MatchEvaluator
struct MatchEvaluator_t710107290;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.PubnubCryptoBase
struct  PubnubCryptoBase_t3627838878  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Core.PubnubCryptoBase::cipherKey
	String_t* ___cipherKey_0;

public:
	inline static int32_t get_offset_of_cipherKey_0() { return static_cast<int32_t>(offsetof(PubnubCryptoBase_t3627838878, ___cipherKey_0)); }
	inline String_t* get_cipherKey_0() const { return ___cipherKey_0; }
	inline String_t** get_address_of_cipherKey_0() { return &___cipherKey_0; }
	inline void set_cipherKey_0(String_t* value)
	{
		___cipherKey_0 = value;
		Il2CppCodeGenWriteBarrier(&___cipherKey_0, value);
	}
};

struct PubnubCryptoBase_t3627838878_StaticFields
{
public:
	// System.Text.RegularExpressions.MatchEvaluator PubNubMessaging.Core.PubnubCryptoBase::<>f__am$cache0
	MatchEvaluator_t710107290 * ___U3CU3Ef__amU24cache0_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(PubnubCryptoBase_t3627838878_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline MatchEvaluator_t710107290 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline MatchEvaluator_t710107290 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(MatchEvaluator_t710107290 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
