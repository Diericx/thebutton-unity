﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.PubnubCryptoBase
struct PubnubCryptoBase_t3627838878;
// System.String
struct String_t;
// System.Text.RegularExpressions.Match
struct Match_t3164245899;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Text_RegularExpressions_Match3164245899.h"

// System.Void PubNubMessaging.Core.PubnubCryptoBase::.ctor(System.String)
extern "C"  void PubnubCryptoBase__ctor_m3229657794 (PubnubCryptoBase_t3627838878 * __this, String_t* ___cipher_key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubCryptoBase::GetEncryptionKey()
extern "C"  String_t* PubnubCryptoBase_GetEncryptionKey_m3823821927 (PubnubCryptoBase_t3627838878 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubCryptoBase::Encrypt(System.String)
extern "C"  String_t* PubnubCryptoBase_Encrypt_m3638809278 (PubnubCryptoBase_t3627838878 * __this, String_t* ___plainText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubCryptoBase::Decrypt(System.String)
extern "C"  String_t* PubnubCryptoBase_Decrypt_m3996517154 (PubnubCryptoBase_t3627838878 * __this, String_t* ___cipherText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubCryptoBase::ConvertHexToUnicodeChars(System.String)
extern "C"  String_t* PubnubCryptoBase_ConvertHexToUnicodeChars_m208462970 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubCryptoBase::EncodeNonAsciiCharacters(System.String)
extern "C"  String_t* PubnubCryptoBase_EncodeNonAsciiCharacters_m449197837 (PubnubCryptoBase_t3627838878 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubCryptoBase::PubnubAccessManagerSign(System.String,System.String)
extern "C"  String_t* PubnubCryptoBase_PubnubAccessManagerSign_m2904523367 (PubnubCryptoBase_t3627838878 * __this, String_t* ___key0, String_t* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubCryptoBase::<ConvertHexToUnicodeChars>m__0(System.Text.RegularExpressions.Match)
extern "C"  String_t* PubnubCryptoBase_U3CConvertHexToUnicodeCharsU3Em__0_m1126876669 (Il2CppObject * __this /* static, unused */, Match_t3164245899 * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
