﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubError2349065665.h"
#include "System_System_Net_WebExceptionStatus1169373531.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Exception1927440687.h"

// PubNubMessaging.Core.PubnubErrorCode PubNubMessaging.Core.PubnubErrorCodeHelper::GetErrorType(System.Net.WebExceptionStatus,System.String)
extern "C"  int32_t PubnubErrorCodeHelper_GetErrorType_m1319832987 (Il2CppObject * __this /* static, unused */, int32_t ___webExceptionStatus0, String_t* ___webExceptionMessage1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.PubnubErrorCode PubNubMessaging.Core.PubnubErrorCodeHelper::GetErrorType(System.Exception)
extern "C"  int32_t PubnubErrorCodeHelper_GetErrorType_m2685864776 (Il2CppObject * __this /* static, unused */, Exception_t1927440687 * ___ex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.PubnubErrorCode PubNubMessaging.Core.PubnubErrorCodeHelper::GetErrorType(System.Int32,System.String)
extern "C"  int32_t PubnubErrorCodeHelper_GetErrorType_m2529193219 (Il2CppObject * __this /* static, unused */, int32_t ___statusCode0, String_t* ___httpErrorCodeMessage1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
