﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.PubnubErrorFilter
struct  PubnubErrorFilter_t3669959072  : public Il2CppObject
{
public:

public:
};

struct PubnubErrorFilter_t3669959072_StaticFields
{
public:
	// System.Int32 PubNubMessaging.Core.PubnubErrorFilter::errorLevel
	int32_t ___errorLevel_0;

public:
	inline static int32_t get_offset_of_errorLevel_0() { return static_cast<int32_t>(offsetof(PubnubErrorFilter_t3669959072_StaticFields, ___errorLevel_0)); }
	inline int32_t get_errorLevel_0() const { return ___errorLevel_0; }
	inline int32_t* get_address_of_errorLevel_0() { return &___errorLevel_0; }
	inline void set_errorLevel_0(int32_t value)
	{
		___errorLevel_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
