﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.PubnubErrorFilter
struct PubnubErrorFilter_t3669959072;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubErrorF397889342.h"

// System.Void PubNubMessaging.Core.PubnubErrorFilter::.ctor()
extern "C"  void PubnubErrorFilter__ctor_m1884684338 (PubnubErrorFilter_t3669959072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.PubnubErrorFilter/Level PubNubMessaging.Core.PubnubErrorFilter::get_ErrorLevel()
extern "C"  int32_t PubnubErrorFilter_get_ErrorLevel_m404305296 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubErrorFilter::set_ErrorLevel(PubNubMessaging.Core.PubnubErrorFilter/Level)
extern "C"  void PubnubErrorFilter_set_ErrorLevel_m1921103129 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.PubnubErrorFilter::get_Critical()
extern "C"  bool PubnubErrorFilter_get_Critical_m1762027470 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.PubnubErrorFilter::get_Warn()
extern "C"  bool PubnubErrorFilter_get_Warn_m2721715949 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.PubnubErrorFilter::get_Info()
extern "C"  bool PubnubErrorFilter_get_Info_m2623479485 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubErrorFilter::.cctor()
extern "C"  void PubnubErrorFilter__cctor_m559255445 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
