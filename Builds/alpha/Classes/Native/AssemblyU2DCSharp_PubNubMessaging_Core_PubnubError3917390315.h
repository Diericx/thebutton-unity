﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int32,System.String>
struct Dictionary_2_t1037045868;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.PubnubErrorCodeDescription
struct  PubnubErrorCodeDescription_t3917390315  : public Il2CppObject
{
public:

public:
};

struct PubnubErrorCodeDescription_t3917390315_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> PubNubMessaging.Core.PubnubErrorCodeDescription::dictionaryCodes
	Dictionary_2_t1037045868 * ___dictionaryCodes_0;

public:
	inline static int32_t get_offset_of_dictionaryCodes_0() { return static_cast<int32_t>(offsetof(PubnubErrorCodeDescription_t3917390315_StaticFields, ___dictionaryCodes_0)); }
	inline Dictionary_2_t1037045868 * get_dictionaryCodes_0() const { return ___dictionaryCodes_0; }
	inline Dictionary_2_t1037045868 ** get_address_of_dictionaryCodes_0() { return &___dictionaryCodes_0; }
	inline void set_dictionaryCodes_0(Dictionary_2_t1037045868 * value)
	{
		___dictionaryCodes_0 = value;
		Il2CppCodeGenWriteBarrier(&___dictionaryCodes_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
