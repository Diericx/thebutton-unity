﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubError2349065665.h"

// System.Void PubNubMessaging.Core.PubnubErrorCodeDescription::.cctor()
extern "C"  void PubnubErrorCodeDescription__cctor_m193954448 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubErrorCodeDescription::GetStatusCodeDescription(PubNubMessaging.Core.PubnubErrorCode)
extern "C"  String_t* PubnubErrorCodeDescription_GetStatusCodeDescription_m2463303677 (Il2CppObject * __this /* static, unused */, int32_t ___pubnubErrorCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
