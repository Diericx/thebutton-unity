﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t3042952059;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// PubNubMessaging.Core.Counter
struct Counter_t3542383138;
// PubNubMessaging.Core.CoroutineClass
struct CoroutineClass_t2476503358;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// PubNubMessaging.Core.IPubnubUnitTest
struct IPubnubUnitTest_t1671935683;
// PubNubMessaging.Core.IJsonPluggableLibrary
struct IJsonPluggableLibrary_t1579330875;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t2756269959;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_LoggingMeth2240454990.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubErrorF397889342.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.PubnubUnity
struct  PubnubUnity_t2122492879  : public Il2CppObject
{
public:
	// System.ComponentModel.PropertyChangedEventHandler PubNubMessaging.Core.PubnubUnity::PropertyChanged
	PropertyChangedEventHandler_t3042952059 * ___PropertyChanged_0;
	// System.Boolean PubNubMessaging.Core.PubnubUnity::localGobj
	bool ___localGobj_2;
	// PubNubMessaging.Core.Counter PubNubMessaging.Core.PubnubUnity::publishMessageCounter
	Counter_t3542383138 * ___publishMessageCounter_3;
	// PubNubMessaging.Core.CoroutineClass PubNubMessaging.Core.PubnubUnity::coroutine
	CoroutineClass_t2476503358 * ___coroutine_4;
	// System.String PubNubMessaging.Core.PubnubUnity::origin
	String_t* ___origin_5;
	// System.String PubNubMessaging.Core.PubnubUnity::publishKey
	String_t* ___publishKey_6;
	// System.String PubNubMessaging.Core.PubnubUnity::subscribeKey
	String_t* ___subscribeKey_7;
	// System.String PubNubMessaging.Core.PubnubUnity::secretKey
	String_t* ___secretKey_8;
	// System.String PubNubMessaging.Core.PubnubUnity::cipherKey
	String_t* ___cipherKey_9;
	// System.Boolean PubNubMessaging.Core.PubnubUnity::ssl
	bool ___ssl_10;
	// System.Int32 PubNubMessaging.Core.PubnubUnity::pubnubWebRequestCallbackIntervalInSeconds
	int32_t ___pubnubWebRequestCallbackIntervalInSeconds_15;
	// System.Int32 PubNubMessaging.Core.PubnubUnity::pubnubOperationTimeoutIntervalInSeconds
	int32_t ___pubnubOperationTimeoutIntervalInSeconds_16;
	// System.Int32 PubNubMessaging.Core.PubnubUnity::pubnubHeartbeatTimeoutIntervalInSeconds
	int32_t ___pubnubHeartbeatTimeoutIntervalInSeconds_17;
	// System.Int32 PubNubMessaging.Core.PubnubUnity::pubnubNetworkTcpCheckIntervalInSeconds
	int32_t ___pubnubNetworkTcpCheckIntervalInSeconds_18;
	// System.Int32 PubNubMessaging.Core.PubnubUnity::pubnubNetworkCheckRetries
	int32_t ___pubnubNetworkCheckRetries_19;
	// System.Int32 PubNubMessaging.Core.PubnubUnity::pubnubWebRequestRetryIntervalInSeconds
	int32_t ___pubnubWebRequestRetryIntervalInSeconds_20;
	// System.Int32 PubNubMessaging.Core.PubnubUnity::pubnubPresenceHeartbeatInSeconds
	int32_t ___pubnubPresenceHeartbeatInSeconds_21;
	// System.Int32 PubNubMessaging.Core.PubnubUnity::presenceHeartbeatIntervalInSeconds
	int32_t ___presenceHeartbeatIntervalInSeconds_22;
	// System.Int32 PubNubMessaging.Core.PubnubUnity::requestDelayTime
	int32_t ___requestDelayTime_23;
	// System.Boolean PubNubMessaging.Core.PubnubUnity::enableResumeOnReconnect
	bool ___enableResumeOnReconnect_24;
	// System.Boolean PubNubMessaging.Core.PubnubUnity::uuidChanged
	bool ___uuidChanged_25;
	// System.Boolean PubNubMessaging.Core.PubnubUnity::overrideTcpKeepAlive
	bool ___overrideTcpKeepAlive_26;
	// System.Boolean PubNubMessaging.Core.PubnubUnity::enableJsonEncodingForPublish
	bool ___enableJsonEncodingForPublish_27;
	// PubNubMessaging.Core.LoggingMethod/Level PubNubMessaging.Core.PubnubUnity::pubnubLogLevel
	int32_t ___pubnubLogLevel_28;
	// PubNubMessaging.Core.PubnubErrorFilter/Level PubNubMessaging.Core.PubnubUnity::errorLevel
	int32_t ___errorLevel_29;
	// System.Boolean PubNubMessaging.Core.PubnubUnity::resetTimetoken
	bool ___resetTimetoken_30;
	// System.Boolean PubNubMessaging.Core.PubnubUnity::keepHearbeatRunning
	bool ___keepHearbeatRunning_31;
	// System.Boolean PubNubMessaging.Core.PubnubUnity::isHearbeatRunning
	bool ___isHearbeatRunning_32;
	// System.Boolean PubNubMessaging.Core.PubnubUnity::keepPresenceHearbeatRunning
	bool ___keepPresenceHearbeatRunning_33;
	// System.Boolean PubNubMessaging.Core.PubnubUnity::isPresenceHearbeatRunning
	bool ___isPresenceHearbeatRunning_34;
	// System.Boolean PubNubMessaging.Core.PubnubUnity::internetStatus
	bool ___internetStatus_35;
	// System.Boolean PubNubMessaging.Core.PubnubUnity::retriesExceeded
	bool ___retriesExceeded_36;
	// System.Int32 PubNubMessaging.Core.PubnubUnity::retryCount
	int32_t ___retryCount_37;
	// System.Collections.Generic.List`1<System.Object> PubNubMessaging.Core.PubnubUnity::history
	List_1_t2058570427 * ___history_38;
	// System.String PubNubMessaging.Core.PubnubUnity::authenticationKey
	String_t* ___authenticationKey_39;
	// PubNubMessaging.Core.IPubnubUnitTest PubNubMessaging.Core.PubnubUnity::pubnubUnitTest
	Il2CppObject * ___pubnubUnitTest_40;
	// System.String PubNubMessaging.Core.PubnubUnity::sessionUUID
	String_t* ___sessionUUID_42;
	// System.String PubNubMessaging.Core.PubnubUnity::filterExpr
	String_t* ___filterExpr_43;
	// System.String PubNubMessaging.Core.PubnubUnity::<Region>k__BackingField
	String_t* ___U3CRegionU3Ek__BackingField_44;

public:
	inline static int32_t get_offset_of_PropertyChanged_0() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___PropertyChanged_0)); }
	inline PropertyChangedEventHandler_t3042952059 * get_PropertyChanged_0() const { return ___PropertyChanged_0; }
	inline PropertyChangedEventHandler_t3042952059 ** get_address_of_PropertyChanged_0() { return &___PropertyChanged_0; }
	inline void set_PropertyChanged_0(PropertyChangedEventHandler_t3042952059 * value)
	{
		___PropertyChanged_0 = value;
		Il2CppCodeGenWriteBarrier(&___PropertyChanged_0, value);
	}

	inline static int32_t get_offset_of_localGobj_2() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___localGobj_2)); }
	inline bool get_localGobj_2() const { return ___localGobj_2; }
	inline bool* get_address_of_localGobj_2() { return &___localGobj_2; }
	inline void set_localGobj_2(bool value)
	{
		___localGobj_2 = value;
	}

	inline static int32_t get_offset_of_publishMessageCounter_3() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___publishMessageCounter_3)); }
	inline Counter_t3542383138 * get_publishMessageCounter_3() const { return ___publishMessageCounter_3; }
	inline Counter_t3542383138 ** get_address_of_publishMessageCounter_3() { return &___publishMessageCounter_3; }
	inline void set_publishMessageCounter_3(Counter_t3542383138 * value)
	{
		___publishMessageCounter_3 = value;
		Il2CppCodeGenWriteBarrier(&___publishMessageCounter_3, value);
	}

	inline static int32_t get_offset_of_coroutine_4() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___coroutine_4)); }
	inline CoroutineClass_t2476503358 * get_coroutine_4() const { return ___coroutine_4; }
	inline CoroutineClass_t2476503358 ** get_address_of_coroutine_4() { return &___coroutine_4; }
	inline void set_coroutine_4(CoroutineClass_t2476503358 * value)
	{
		___coroutine_4 = value;
		Il2CppCodeGenWriteBarrier(&___coroutine_4, value);
	}

	inline static int32_t get_offset_of_origin_5() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___origin_5)); }
	inline String_t* get_origin_5() const { return ___origin_5; }
	inline String_t** get_address_of_origin_5() { return &___origin_5; }
	inline void set_origin_5(String_t* value)
	{
		___origin_5 = value;
		Il2CppCodeGenWriteBarrier(&___origin_5, value);
	}

	inline static int32_t get_offset_of_publishKey_6() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___publishKey_6)); }
	inline String_t* get_publishKey_6() const { return ___publishKey_6; }
	inline String_t** get_address_of_publishKey_6() { return &___publishKey_6; }
	inline void set_publishKey_6(String_t* value)
	{
		___publishKey_6 = value;
		Il2CppCodeGenWriteBarrier(&___publishKey_6, value);
	}

	inline static int32_t get_offset_of_subscribeKey_7() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___subscribeKey_7)); }
	inline String_t* get_subscribeKey_7() const { return ___subscribeKey_7; }
	inline String_t** get_address_of_subscribeKey_7() { return &___subscribeKey_7; }
	inline void set_subscribeKey_7(String_t* value)
	{
		___subscribeKey_7 = value;
		Il2CppCodeGenWriteBarrier(&___subscribeKey_7, value);
	}

	inline static int32_t get_offset_of_secretKey_8() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___secretKey_8)); }
	inline String_t* get_secretKey_8() const { return ___secretKey_8; }
	inline String_t** get_address_of_secretKey_8() { return &___secretKey_8; }
	inline void set_secretKey_8(String_t* value)
	{
		___secretKey_8 = value;
		Il2CppCodeGenWriteBarrier(&___secretKey_8, value);
	}

	inline static int32_t get_offset_of_cipherKey_9() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___cipherKey_9)); }
	inline String_t* get_cipherKey_9() const { return ___cipherKey_9; }
	inline String_t** get_address_of_cipherKey_9() { return &___cipherKey_9; }
	inline void set_cipherKey_9(String_t* value)
	{
		___cipherKey_9 = value;
		Il2CppCodeGenWriteBarrier(&___cipherKey_9, value);
	}

	inline static int32_t get_offset_of_ssl_10() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___ssl_10)); }
	inline bool get_ssl_10() const { return ___ssl_10; }
	inline bool* get_address_of_ssl_10() { return &___ssl_10; }
	inline void set_ssl_10(bool value)
	{
		___ssl_10 = value;
	}

	inline static int32_t get_offset_of_pubnubWebRequestCallbackIntervalInSeconds_15() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___pubnubWebRequestCallbackIntervalInSeconds_15)); }
	inline int32_t get_pubnubWebRequestCallbackIntervalInSeconds_15() const { return ___pubnubWebRequestCallbackIntervalInSeconds_15; }
	inline int32_t* get_address_of_pubnubWebRequestCallbackIntervalInSeconds_15() { return &___pubnubWebRequestCallbackIntervalInSeconds_15; }
	inline void set_pubnubWebRequestCallbackIntervalInSeconds_15(int32_t value)
	{
		___pubnubWebRequestCallbackIntervalInSeconds_15 = value;
	}

	inline static int32_t get_offset_of_pubnubOperationTimeoutIntervalInSeconds_16() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___pubnubOperationTimeoutIntervalInSeconds_16)); }
	inline int32_t get_pubnubOperationTimeoutIntervalInSeconds_16() const { return ___pubnubOperationTimeoutIntervalInSeconds_16; }
	inline int32_t* get_address_of_pubnubOperationTimeoutIntervalInSeconds_16() { return &___pubnubOperationTimeoutIntervalInSeconds_16; }
	inline void set_pubnubOperationTimeoutIntervalInSeconds_16(int32_t value)
	{
		___pubnubOperationTimeoutIntervalInSeconds_16 = value;
	}

	inline static int32_t get_offset_of_pubnubHeartbeatTimeoutIntervalInSeconds_17() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___pubnubHeartbeatTimeoutIntervalInSeconds_17)); }
	inline int32_t get_pubnubHeartbeatTimeoutIntervalInSeconds_17() const { return ___pubnubHeartbeatTimeoutIntervalInSeconds_17; }
	inline int32_t* get_address_of_pubnubHeartbeatTimeoutIntervalInSeconds_17() { return &___pubnubHeartbeatTimeoutIntervalInSeconds_17; }
	inline void set_pubnubHeartbeatTimeoutIntervalInSeconds_17(int32_t value)
	{
		___pubnubHeartbeatTimeoutIntervalInSeconds_17 = value;
	}

	inline static int32_t get_offset_of_pubnubNetworkTcpCheckIntervalInSeconds_18() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___pubnubNetworkTcpCheckIntervalInSeconds_18)); }
	inline int32_t get_pubnubNetworkTcpCheckIntervalInSeconds_18() const { return ___pubnubNetworkTcpCheckIntervalInSeconds_18; }
	inline int32_t* get_address_of_pubnubNetworkTcpCheckIntervalInSeconds_18() { return &___pubnubNetworkTcpCheckIntervalInSeconds_18; }
	inline void set_pubnubNetworkTcpCheckIntervalInSeconds_18(int32_t value)
	{
		___pubnubNetworkTcpCheckIntervalInSeconds_18 = value;
	}

	inline static int32_t get_offset_of_pubnubNetworkCheckRetries_19() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___pubnubNetworkCheckRetries_19)); }
	inline int32_t get_pubnubNetworkCheckRetries_19() const { return ___pubnubNetworkCheckRetries_19; }
	inline int32_t* get_address_of_pubnubNetworkCheckRetries_19() { return &___pubnubNetworkCheckRetries_19; }
	inline void set_pubnubNetworkCheckRetries_19(int32_t value)
	{
		___pubnubNetworkCheckRetries_19 = value;
	}

	inline static int32_t get_offset_of_pubnubWebRequestRetryIntervalInSeconds_20() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___pubnubWebRequestRetryIntervalInSeconds_20)); }
	inline int32_t get_pubnubWebRequestRetryIntervalInSeconds_20() const { return ___pubnubWebRequestRetryIntervalInSeconds_20; }
	inline int32_t* get_address_of_pubnubWebRequestRetryIntervalInSeconds_20() { return &___pubnubWebRequestRetryIntervalInSeconds_20; }
	inline void set_pubnubWebRequestRetryIntervalInSeconds_20(int32_t value)
	{
		___pubnubWebRequestRetryIntervalInSeconds_20 = value;
	}

	inline static int32_t get_offset_of_pubnubPresenceHeartbeatInSeconds_21() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___pubnubPresenceHeartbeatInSeconds_21)); }
	inline int32_t get_pubnubPresenceHeartbeatInSeconds_21() const { return ___pubnubPresenceHeartbeatInSeconds_21; }
	inline int32_t* get_address_of_pubnubPresenceHeartbeatInSeconds_21() { return &___pubnubPresenceHeartbeatInSeconds_21; }
	inline void set_pubnubPresenceHeartbeatInSeconds_21(int32_t value)
	{
		___pubnubPresenceHeartbeatInSeconds_21 = value;
	}

	inline static int32_t get_offset_of_presenceHeartbeatIntervalInSeconds_22() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___presenceHeartbeatIntervalInSeconds_22)); }
	inline int32_t get_presenceHeartbeatIntervalInSeconds_22() const { return ___presenceHeartbeatIntervalInSeconds_22; }
	inline int32_t* get_address_of_presenceHeartbeatIntervalInSeconds_22() { return &___presenceHeartbeatIntervalInSeconds_22; }
	inline void set_presenceHeartbeatIntervalInSeconds_22(int32_t value)
	{
		___presenceHeartbeatIntervalInSeconds_22 = value;
	}

	inline static int32_t get_offset_of_requestDelayTime_23() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___requestDelayTime_23)); }
	inline int32_t get_requestDelayTime_23() const { return ___requestDelayTime_23; }
	inline int32_t* get_address_of_requestDelayTime_23() { return &___requestDelayTime_23; }
	inline void set_requestDelayTime_23(int32_t value)
	{
		___requestDelayTime_23 = value;
	}

	inline static int32_t get_offset_of_enableResumeOnReconnect_24() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___enableResumeOnReconnect_24)); }
	inline bool get_enableResumeOnReconnect_24() const { return ___enableResumeOnReconnect_24; }
	inline bool* get_address_of_enableResumeOnReconnect_24() { return &___enableResumeOnReconnect_24; }
	inline void set_enableResumeOnReconnect_24(bool value)
	{
		___enableResumeOnReconnect_24 = value;
	}

	inline static int32_t get_offset_of_uuidChanged_25() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___uuidChanged_25)); }
	inline bool get_uuidChanged_25() const { return ___uuidChanged_25; }
	inline bool* get_address_of_uuidChanged_25() { return &___uuidChanged_25; }
	inline void set_uuidChanged_25(bool value)
	{
		___uuidChanged_25 = value;
	}

	inline static int32_t get_offset_of_overrideTcpKeepAlive_26() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___overrideTcpKeepAlive_26)); }
	inline bool get_overrideTcpKeepAlive_26() const { return ___overrideTcpKeepAlive_26; }
	inline bool* get_address_of_overrideTcpKeepAlive_26() { return &___overrideTcpKeepAlive_26; }
	inline void set_overrideTcpKeepAlive_26(bool value)
	{
		___overrideTcpKeepAlive_26 = value;
	}

	inline static int32_t get_offset_of_enableJsonEncodingForPublish_27() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___enableJsonEncodingForPublish_27)); }
	inline bool get_enableJsonEncodingForPublish_27() const { return ___enableJsonEncodingForPublish_27; }
	inline bool* get_address_of_enableJsonEncodingForPublish_27() { return &___enableJsonEncodingForPublish_27; }
	inline void set_enableJsonEncodingForPublish_27(bool value)
	{
		___enableJsonEncodingForPublish_27 = value;
	}

	inline static int32_t get_offset_of_pubnubLogLevel_28() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___pubnubLogLevel_28)); }
	inline int32_t get_pubnubLogLevel_28() const { return ___pubnubLogLevel_28; }
	inline int32_t* get_address_of_pubnubLogLevel_28() { return &___pubnubLogLevel_28; }
	inline void set_pubnubLogLevel_28(int32_t value)
	{
		___pubnubLogLevel_28 = value;
	}

	inline static int32_t get_offset_of_errorLevel_29() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___errorLevel_29)); }
	inline int32_t get_errorLevel_29() const { return ___errorLevel_29; }
	inline int32_t* get_address_of_errorLevel_29() { return &___errorLevel_29; }
	inline void set_errorLevel_29(int32_t value)
	{
		___errorLevel_29 = value;
	}

	inline static int32_t get_offset_of_resetTimetoken_30() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___resetTimetoken_30)); }
	inline bool get_resetTimetoken_30() const { return ___resetTimetoken_30; }
	inline bool* get_address_of_resetTimetoken_30() { return &___resetTimetoken_30; }
	inline void set_resetTimetoken_30(bool value)
	{
		___resetTimetoken_30 = value;
	}

	inline static int32_t get_offset_of_keepHearbeatRunning_31() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___keepHearbeatRunning_31)); }
	inline bool get_keepHearbeatRunning_31() const { return ___keepHearbeatRunning_31; }
	inline bool* get_address_of_keepHearbeatRunning_31() { return &___keepHearbeatRunning_31; }
	inline void set_keepHearbeatRunning_31(bool value)
	{
		___keepHearbeatRunning_31 = value;
	}

	inline static int32_t get_offset_of_isHearbeatRunning_32() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___isHearbeatRunning_32)); }
	inline bool get_isHearbeatRunning_32() const { return ___isHearbeatRunning_32; }
	inline bool* get_address_of_isHearbeatRunning_32() { return &___isHearbeatRunning_32; }
	inline void set_isHearbeatRunning_32(bool value)
	{
		___isHearbeatRunning_32 = value;
	}

	inline static int32_t get_offset_of_keepPresenceHearbeatRunning_33() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___keepPresenceHearbeatRunning_33)); }
	inline bool get_keepPresenceHearbeatRunning_33() const { return ___keepPresenceHearbeatRunning_33; }
	inline bool* get_address_of_keepPresenceHearbeatRunning_33() { return &___keepPresenceHearbeatRunning_33; }
	inline void set_keepPresenceHearbeatRunning_33(bool value)
	{
		___keepPresenceHearbeatRunning_33 = value;
	}

	inline static int32_t get_offset_of_isPresenceHearbeatRunning_34() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___isPresenceHearbeatRunning_34)); }
	inline bool get_isPresenceHearbeatRunning_34() const { return ___isPresenceHearbeatRunning_34; }
	inline bool* get_address_of_isPresenceHearbeatRunning_34() { return &___isPresenceHearbeatRunning_34; }
	inline void set_isPresenceHearbeatRunning_34(bool value)
	{
		___isPresenceHearbeatRunning_34 = value;
	}

	inline static int32_t get_offset_of_internetStatus_35() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___internetStatus_35)); }
	inline bool get_internetStatus_35() const { return ___internetStatus_35; }
	inline bool* get_address_of_internetStatus_35() { return &___internetStatus_35; }
	inline void set_internetStatus_35(bool value)
	{
		___internetStatus_35 = value;
	}

	inline static int32_t get_offset_of_retriesExceeded_36() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___retriesExceeded_36)); }
	inline bool get_retriesExceeded_36() const { return ___retriesExceeded_36; }
	inline bool* get_address_of_retriesExceeded_36() { return &___retriesExceeded_36; }
	inline void set_retriesExceeded_36(bool value)
	{
		___retriesExceeded_36 = value;
	}

	inline static int32_t get_offset_of_retryCount_37() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___retryCount_37)); }
	inline int32_t get_retryCount_37() const { return ___retryCount_37; }
	inline int32_t* get_address_of_retryCount_37() { return &___retryCount_37; }
	inline void set_retryCount_37(int32_t value)
	{
		___retryCount_37 = value;
	}

	inline static int32_t get_offset_of_history_38() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___history_38)); }
	inline List_1_t2058570427 * get_history_38() const { return ___history_38; }
	inline List_1_t2058570427 ** get_address_of_history_38() { return &___history_38; }
	inline void set_history_38(List_1_t2058570427 * value)
	{
		___history_38 = value;
		Il2CppCodeGenWriteBarrier(&___history_38, value);
	}

	inline static int32_t get_offset_of_authenticationKey_39() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___authenticationKey_39)); }
	inline String_t* get_authenticationKey_39() const { return ___authenticationKey_39; }
	inline String_t** get_address_of_authenticationKey_39() { return &___authenticationKey_39; }
	inline void set_authenticationKey_39(String_t* value)
	{
		___authenticationKey_39 = value;
		Il2CppCodeGenWriteBarrier(&___authenticationKey_39, value);
	}

	inline static int32_t get_offset_of_pubnubUnitTest_40() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___pubnubUnitTest_40)); }
	inline Il2CppObject * get_pubnubUnitTest_40() const { return ___pubnubUnitTest_40; }
	inline Il2CppObject ** get_address_of_pubnubUnitTest_40() { return &___pubnubUnitTest_40; }
	inline void set_pubnubUnitTest_40(Il2CppObject * value)
	{
		___pubnubUnitTest_40 = value;
		Il2CppCodeGenWriteBarrier(&___pubnubUnitTest_40, value);
	}

	inline static int32_t get_offset_of_sessionUUID_42() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___sessionUUID_42)); }
	inline String_t* get_sessionUUID_42() const { return ___sessionUUID_42; }
	inline String_t** get_address_of_sessionUUID_42() { return &___sessionUUID_42; }
	inline void set_sessionUUID_42(String_t* value)
	{
		___sessionUUID_42 = value;
		Il2CppCodeGenWriteBarrier(&___sessionUUID_42, value);
	}

	inline static int32_t get_offset_of_filterExpr_43() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___filterExpr_43)); }
	inline String_t* get_filterExpr_43() const { return ___filterExpr_43; }
	inline String_t** get_address_of_filterExpr_43() { return &___filterExpr_43; }
	inline void set_filterExpr_43(String_t* value)
	{
		___filterExpr_43 = value;
		Il2CppCodeGenWriteBarrier(&___filterExpr_43, value);
	}

	inline static int32_t get_offset_of_U3CRegionU3Ek__BackingField_44() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879, ___U3CRegionU3Ek__BackingField_44)); }
	inline String_t* get_U3CRegionU3Ek__BackingField_44() const { return ___U3CRegionU3Ek__BackingField_44; }
	inline String_t** get_address_of_U3CRegionU3Ek__BackingField_44() { return &___U3CRegionU3Ek__BackingField_44; }
	inline void set_U3CRegionU3Ek__BackingField_44(String_t* value)
	{
		___U3CRegionU3Ek__BackingField_44 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRegionU3Ek__BackingField_44, value);
	}
};

struct PubnubUnity_t2122492879_StaticFields
{
public:
	// UnityEngine.GameObject PubNubMessaging.Core.PubnubUnity::gobj
	GameObject_t1756533147 * ___gobj_1;
	// System.Int64 PubNubMessaging.Core.PubnubUnity::lastSubscribeTimetoken
	int64_t ___lastSubscribeTimetoken_11;
	// System.Int64 PubNubMessaging.Core.PubnubUnity::lastSubscribeTimetokenForNewMultiplex
	int64_t ___lastSubscribeTimetokenForNewMultiplex_12;
	// System.String PubNubMessaging.Core.PubnubUnity::pnsdkVersion
	String_t* ___pnsdkVersion_14;
	// PubNubMessaging.Core.IJsonPluggableLibrary PubNubMessaging.Core.PubnubUnity::jsonPluggableLibrary
	Il2CppObject * ___jsonPluggableLibrary_41;
	// System.Net.Security.RemoteCertificateValidationCallback PubNubMessaging.Core.PubnubUnity::<>f__mg$cache0
	RemoteCertificateValidationCallback_t2756269959 * ___U3CU3Ef__mgU24cache0_45;

public:
	inline static int32_t get_offset_of_gobj_1() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879_StaticFields, ___gobj_1)); }
	inline GameObject_t1756533147 * get_gobj_1() const { return ___gobj_1; }
	inline GameObject_t1756533147 ** get_address_of_gobj_1() { return &___gobj_1; }
	inline void set_gobj_1(GameObject_t1756533147 * value)
	{
		___gobj_1 = value;
		Il2CppCodeGenWriteBarrier(&___gobj_1, value);
	}

	inline static int32_t get_offset_of_lastSubscribeTimetoken_11() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879_StaticFields, ___lastSubscribeTimetoken_11)); }
	inline int64_t get_lastSubscribeTimetoken_11() const { return ___lastSubscribeTimetoken_11; }
	inline int64_t* get_address_of_lastSubscribeTimetoken_11() { return &___lastSubscribeTimetoken_11; }
	inline void set_lastSubscribeTimetoken_11(int64_t value)
	{
		___lastSubscribeTimetoken_11 = value;
	}

	inline static int32_t get_offset_of_lastSubscribeTimetokenForNewMultiplex_12() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879_StaticFields, ___lastSubscribeTimetokenForNewMultiplex_12)); }
	inline int64_t get_lastSubscribeTimetokenForNewMultiplex_12() const { return ___lastSubscribeTimetokenForNewMultiplex_12; }
	inline int64_t* get_address_of_lastSubscribeTimetokenForNewMultiplex_12() { return &___lastSubscribeTimetokenForNewMultiplex_12; }
	inline void set_lastSubscribeTimetokenForNewMultiplex_12(int64_t value)
	{
		___lastSubscribeTimetokenForNewMultiplex_12 = value;
	}

	inline static int32_t get_offset_of_pnsdkVersion_14() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879_StaticFields, ___pnsdkVersion_14)); }
	inline String_t* get_pnsdkVersion_14() const { return ___pnsdkVersion_14; }
	inline String_t** get_address_of_pnsdkVersion_14() { return &___pnsdkVersion_14; }
	inline void set_pnsdkVersion_14(String_t* value)
	{
		___pnsdkVersion_14 = value;
		Il2CppCodeGenWriteBarrier(&___pnsdkVersion_14, value);
	}

	inline static int32_t get_offset_of_jsonPluggableLibrary_41() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879_StaticFields, ___jsonPluggableLibrary_41)); }
	inline Il2CppObject * get_jsonPluggableLibrary_41() const { return ___jsonPluggableLibrary_41; }
	inline Il2CppObject ** get_address_of_jsonPluggableLibrary_41() { return &___jsonPluggableLibrary_41; }
	inline void set_jsonPluggableLibrary_41(Il2CppObject * value)
	{
		___jsonPluggableLibrary_41 = value;
		Il2CppCodeGenWriteBarrier(&___jsonPluggableLibrary_41, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_45() { return static_cast<int32_t>(offsetof(PubnubUnity_t2122492879_StaticFields, ___U3CU3Ef__mgU24cache0_45)); }
	inline RemoteCertificateValidationCallback_t2756269959 * get_U3CU3Ef__mgU24cache0_45() const { return ___U3CU3Ef__mgU24cache0_45; }
	inline RemoteCertificateValidationCallback_t2756269959 ** get_address_of_U3CU3Ef__mgU24cache0_45() { return &___U3CU3Ef__mgU24cache0_45; }
	inline void set_U3CU3Ef__mgU24cache0_45(RemoteCertificateValidationCallback_t2756269959 * value)
	{
		___U3CU3Ef__mgU24cache0_45 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_45, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
