﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.PubnubUnity
struct PubnubUnity_t2122492879;
// System.String
struct String_t;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t3042952059;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// PubNubMessaging.Core.IPubnubUnitTest
struct IPubnubUnitTest_t1671935683;
// PubNubMessaging.Core.IJsonPluggableLibrary
struct IJsonPluggableLibrary_t1579330875;
// System.Object
struct Il2CppObject;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t283079845;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t777637347;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_ComponentModel_PropertyChangedEventH3042952059.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_LoggingMeth2240454990.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubErrorF397889342.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Security_Cryptography_X509Certifica283079845.h"
#include "System_System_Security_Cryptography_X509Certificate777637347.h"
#include "System_System_Net_Security_SslPolicyErrors1928581431.h"

// System.Void PubNubMessaging.Core.PubnubUnity::.ctor(System.String,System.String,System.String,System.String,System.Boolean)
extern "C"  void PubnubUnity__ctor_m2207048128 (PubnubUnity_t2122492879 * __this, String_t* ___publishKey0, String_t* ___subscribeKey1, String_t* ___secretKey2, String_t* ___cipherKey3, bool ___sslOn4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::.ctor(System.String,System.String,System.String)
extern "C"  void PubnubUnity__ctor_m1151877839 (PubnubUnity_t2122492879 * __this, String_t* ___publishKey0, String_t* ___subscribeKey1, String_t* ___secretKey2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::.ctor(System.String,System.String)
extern "C"  void PubnubUnity__ctor_m2053184433 (PubnubUnity_t2122492879 * __this, String_t* ___publishKey0, String_t* ___subscribeKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::add_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern "C"  void PubnubUnity_add_PropertyChanged_m3917337492 (PubnubUnity_t2122492879 * __this, PropertyChangedEventHandler_t3042952059 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::remove_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern "C"  void PubnubUnity_remove_PropertyChanged_m1858293705 (PubnubUnity_t2122492879 * __this, PropertyChangedEventHandler_t3042952059 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::RaisePropertyChanged(System.String)
extern "C"  void PubnubUnity_RaisePropertyChanged_m1653262932 (PubnubUnity_t2122492879 * __this, String_t* ___propertyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubUnity::get_PublishKey()
extern "C"  String_t* PubnubUnity_get_PublishKey_m3036169399 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubUnity::get_SubscribeKey()
extern "C"  String_t* PubnubUnity_get_SubscribeKey_m2734449782 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubUnity::get_SecretKey()
extern "C"  String_t* PubnubUnity_get_SecretKey_m1620993422 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject PubNubMessaging.Core.PubnubUnity::get_SetGameObject()
extern "C"  GameObject_t1756533147 * PubnubUnity_get_SetGameObject_m2382265144 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_SetGameObject(UnityEngine.GameObject)
extern "C"  void PubnubUnity_set_SetGameObject_m1945821349 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubUnity::get_Version()
extern "C"  String_t* PubnubUnity_get_Version_m4193113571 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_Version(System.String)
extern "C"  void PubnubUnity_set_Version_m773184360 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> PubNubMessaging.Core.PubnubUnity::get_History()
extern "C"  List_1_t2058570427 * PubnubUnity_get_History_m3070839609 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_History(System.Collections.Generic.List`1<System.Object>)
extern "C"  void PubnubUnity_set_History_m1795127356 (PubnubUnity_t2122492879 * __this, List_1_t2058570427 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.PubnubUnity::get_SubscribeTimeout()
extern "C"  int32_t PubnubUnity_get_SubscribeTimeout_m1339426501 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_SubscribeTimeout(System.Int32)
extern "C"  void PubnubUnity_set_SubscribeTimeout_m447598846 (PubnubUnity_t2122492879 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.PubnubUnity::get_HeartbeatTimeout()
extern "C"  int32_t PubnubUnity_get_HeartbeatTimeout_m48934889 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_HeartbeatTimeout(System.Int32)
extern "C"  void PubnubUnity_set_HeartbeatTimeout_m2566089910 (PubnubUnity_t2122492879 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.PubnubUnity::get_NonSubscribeTimeout()
extern "C"  int32_t PubnubUnity_get_NonSubscribeTimeout_m2770708664 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_NonSubscribeTimeout(System.Int32)
extern "C"  void PubnubUnity_set_NonSubscribeTimeout_m3931333145 (PubnubUnity_t2122492879 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.PubnubUnity::get_NetworkCheckMaxRetries()
extern "C"  int32_t PubnubUnity_get_NetworkCheckMaxRetries_m3320063072 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_NetworkCheckMaxRetries(System.Int32)
extern "C"  void PubnubUnity_set_NetworkCheckMaxRetries_m2057176479 (PubnubUnity_t2122492879 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.PubnubUnity::get_NetworkCheckRetryInterval()
extern "C"  int32_t PubnubUnity_get_NetworkCheckRetryInterval_m1969968021 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_NetworkCheckRetryInterval(System.Int32)
extern "C"  void PubnubUnity_set_NetworkCheckRetryInterval_m144801250 (PubnubUnity_t2122492879 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.PubnubUnity::get_LocalClientHeartbeatInterval()
extern "C"  int32_t PubnubUnity_get_LocalClientHeartbeatInterval_m959974133 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_LocalClientHeartbeatInterval(System.Int32)
extern "C"  void PubnubUnity_set_LocalClientHeartbeatInterval_m3710836428 (PubnubUnity_t2122492879 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.PubnubUnity::get_EnableResumeOnReconnect()
extern "C"  bool PubnubUnity_get_EnableResumeOnReconnect_m833577022 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_EnableResumeOnReconnect(System.Boolean)
extern "C"  void PubnubUnity_set_EnableResumeOnReconnect_m3520444299 (PubnubUnity_t2122492879 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.PubnubUnity::get_EnableJsonEncodingForPublish()
extern "C"  bool PubnubUnity_get_EnableJsonEncodingForPublish_m3151055308 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_EnableJsonEncodingForPublish(System.Boolean)
extern "C"  void PubnubUnity_set_EnableJsonEncodingForPublish_m4169507611 (PubnubUnity_t2122492879 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubUnity::get_AuthenticationKey()
extern "C"  String_t* PubnubUnity_get_AuthenticationKey_m619815166 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_AuthenticationKey(System.String)
extern "C"  void PubnubUnity_set_AuthenticationKey_m3565858733 (PubnubUnity_t2122492879 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.IPubnubUnitTest PubNubMessaging.Core.PubnubUnity::get_PubnubUnitTest()
extern "C"  Il2CppObject * PubnubUnity_get_PubnubUnitTest_m2711277321 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_PubnubUnitTest(PubNubMessaging.Core.IPubnubUnitTest)
extern "C"  void PubnubUnity_set_PubnubUnitTest_m1441935548 (PubnubUnity_t2122492879 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.IJsonPluggableLibrary PubNubMessaging.Core.PubnubUnity::get_JsonPluggableLibrary()
extern "C"  Il2CppObject * PubnubUnity_get_JsonPluggableLibrary_m2875418973 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_JsonPluggableLibrary(PubNubMessaging.Core.IJsonPluggableLibrary)
extern "C"  void PubnubUnity_set_JsonPluggableLibrary_m2255390644 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubUnity::get_Origin()
extern "C"  String_t* PubnubUnity_get_Origin_m1951968825 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_Origin(System.String)
extern "C"  void PubnubUnity_set_Origin_m68562340 (PubnubUnity_t2122492879 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubUnity::get_SessionUUID()
extern "C"  String_t* PubnubUnity_get_SessionUUID_m3090797794 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_SessionUUID(System.String)
extern "C"  void PubnubUnity_set_SessionUUID_m659471097 (PubnubUnity_t2122492879 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.PubnubUnity::get_PresenceHeartbeat()
extern "C"  int32_t PubnubUnity_get_PresenceHeartbeat_m4101072959 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_PresenceHeartbeat(System.Int32)
extern "C"  void PubnubUnity_set_PresenceHeartbeat_m439144376 (PubnubUnity_t2122492879 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.PubnubUnity::get_PresenceHeartbeatInterval()
extern "C"  int32_t PubnubUnity_get_PresenceHeartbeatInterval_m3892269442 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_PresenceHeartbeatInterval(System.Int32)
extern "C"  void PubnubUnity_set_PresenceHeartbeatInterval_m4194887179 (PubnubUnity_t2122492879 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.LoggingMethod/Level PubNubMessaging.Core.PubnubUnity::get_PubnubLogLevel()
extern "C"  int32_t PubnubUnity_get_PubnubLogLevel_m1299365645 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_PubnubLogLevel(PubNubMessaging.Core.LoggingMethod/Level)
extern "C"  void PubnubUnity_set_PubnubLogLevel_m1625662518 (PubnubUnity_t2122492879 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.PubnubErrorFilter/Level PubNubMessaging.Core.PubnubUnity::get_PubnubErrorLevel()
extern "C"  int32_t PubnubUnity_get_PubnubErrorLevel_m2724330515 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_PubnubErrorLevel(PubNubMessaging.Core.PubnubErrorFilter/Level)
extern "C"  void PubnubUnity_set_PubnubErrorLevel_m1936532046 (PubnubUnity_t2122492879 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubUnity::get_FilterExpr()
extern "C"  String_t* PubnubUnity_get_FilterExpr_m1316357148 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_FilterExpr(System.String)
extern "C"  void PubnubUnity_set_FilterExpr_m2280114453 (PubnubUnity_t2122492879 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubUnity::get_Region()
extern "C"  String_t* PubnubUnity_get_Region_m3896310649 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::set_Region(System.String)
extern "C"  void PubnubUnity_set_Region_m529956722 (PubnubUnity_t2122492879 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::Finalize()
extern "C"  void PubnubUnity_Finalize_m457249771 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::Init(System.String,System.String,System.String,System.String,System.Boolean)
extern "C"  void PubnubUnity_Init_m2646679566 (PubnubUnity_t2122492879 * __this, String_t* ___publishKey0, String_t* ___subscribeKey1, String_t* ___secretKey2, String_t* ___cipherKey3, bool ___sslOn4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.PubnubUnity::ValidatorUnity(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern "C"  bool PubnubUnity_ValidatorUnity_m1004165163 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender0, X509Certificate_t283079845 * ___certificate1, X509Chain_t777637347 * ___chain2, int32_t ___sslPolicyErrors3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::TerminateCurrentSubscriberRequest()
extern "C"  void PubnubUnity_TerminateCurrentSubscriberRequest_m1519878410 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::ResetPublishMessageCounter()
extern "C"  void PubnubUnity_ResetPublishMessageCounter_m474384226 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::CleanUp()
extern "C"  void PubnubUnity_CleanUp_m2048575373 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::StopHeartbeat()
extern "C"  void PubnubUnity_StopHeartbeat_m115641261 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::StopPresenceHeartbeat()
extern "C"  void PubnubUnity_StopPresenceHeartbeat_m824786706 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::ResetInternetCheckSettings()
extern "C"  void PubnubUnity_ResetInternetCheckSettings_m3643188614 (PubnubUnity_t2122492879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 PubNubMessaging.Core.PubnubUnity::SaveLastTimetoken(System.Int64)
extern "C"  int64_t PubnubUnity_SaveLastTimetoken_m3147726143 (PubnubUnity_t2122492879 * __this, int64_t ___timetoken0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.PubnubUnity::.cctor()
extern "C"  void PubnubUnity__cctor_m467932452 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
