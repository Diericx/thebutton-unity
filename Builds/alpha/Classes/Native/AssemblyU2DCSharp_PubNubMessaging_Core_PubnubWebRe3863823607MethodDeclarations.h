﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.PubnubWebRequest
struct PubnubWebRequest_t3863823607;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"

// System.Void PubNubMessaging.Core.PubnubWebRequest::.ctor(UnityEngine.WWW)
extern "C"  void PubnubWebRequest__ctor_m3924503287 (PubnubWebRequest_t3863823607 * __this, WWW_t2919945039 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubWebRequest::get_RequestUri()
extern "C"  String_t* PubnubWebRequest_get_RequestUri_m2307984454 (PubnubWebRequest_t3863823607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PubNubMessaging.Core.PubnubWebRequest::get_Headers()
extern "C"  Dictionary_2_t3943999495 * PubnubWebRequest_get_Headers_m3240899726 (PubnubWebRequest_t3863823607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
