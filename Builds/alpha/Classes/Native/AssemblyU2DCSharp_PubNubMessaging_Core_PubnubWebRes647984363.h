﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t2919945039;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.PubnubWebResponse
struct  PubnubWebResponse_t647984363  : public Il2CppObject
{
public:
	// UnityEngine.WWW PubNubMessaging.Core.PubnubWebResponse::www
	WWW_t2919945039 * ___www_0;

public:
	inline static int32_t get_offset_of_www_0() { return static_cast<int32_t>(offsetof(PubnubWebResponse_t647984363, ___www_0)); }
	inline WWW_t2919945039 * get_www_0() const { return ___www_0; }
	inline WWW_t2919945039 ** get_address_of_www_0() { return &___www_0; }
	inline void set_www_0(WWW_t2919945039 * value)
	{
		___www_0 = value;
		Il2CppCodeGenWriteBarrier(&___www_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
