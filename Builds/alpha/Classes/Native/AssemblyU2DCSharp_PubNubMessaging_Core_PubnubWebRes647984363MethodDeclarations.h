﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.PubnubWebResponse
struct PubnubWebResponse_t647984363;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"

// System.Void PubNubMessaging.Core.PubnubWebResponse::.ctor(UnityEngine.WWW)
extern "C"  void PubnubWebResponse__ctor_m29808421 (PubnubWebResponse_t647984363 * __this, WWW_t2919945039 * ___www0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.PubnubWebResponse::get_ResponseUri()
extern "C"  String_t* PubnubWebResponse_get_ResponseUri_m2491110422 (PubnubWebResponse_t647984363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PubNubMessaging.Core.PubnubWebResponse::get_Headers()
extern "C"  Dictionary_2_t3943999495 * PubnubWebResponse_get_Headers_m3397064746 (PubnubWebResponse_t647984363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
