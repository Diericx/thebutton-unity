﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1642385972;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action`1<PubNubMessaging.Core.PubnubClientError>
struct Action_1_t4207084599;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ResponseTyp1248099179.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.ReconnectState`1<System.Object>
struct  ReconnectState_1_t3075722957  : public Il2CppObject
{
public:
	// System.String[] PubNubMessaging.Core.ReconnectState`1::Channels
	StringU5BU5D_t1642385972* ___Channels_0;
	// PubNubMessaging.Core.ResponseType PubNubMessaging.Core.ReconnectState`1::Type
	int32_t ___Type_1;
	// System.Action`1<T> PubNubMessaging.Core.ReconnectState`1::Callback
	Action_1_t2491248677 * ___Callback_2;
	// System.Action`1<PubNubMessaging.Core.PubnubClientError> PubNubMessaging.Core.ReconnectState`1::ErrorCallback
	Action_1_t4207084599 * ___ErrorCallback_3;
	// System.Action`1<T> PubNubMessaging.Core.ReconnectState`1::ConnectCallback
	Action_1_t2491248677 * ___ConnectCallback_4;
	// System.Object PubNubMessaging.Core.ReconnectState`1::Timetoken
	Il2CppObject * ___Timetoken_5;
	// System.Boolean PubNubMessaging.Core.ReconnectState`1::Reconnect
	bool ___Reconnect_6;

public:
	inline static int32_t get_offset_of_Channels_0() { return static_cast<int32_t>(offsetof(ReconnectState_1_t3075722957, ___Channels_0)); }
	inline StringU5BU5D_t1642385972* get_Channels_0() const { return ___Channels_0; }
	inline StringU5BU5D_t1642385972** get_address_of_Channels_0() { return &___Channels_0; }
	inline void set_Channels_0(StringU5BU5D_t1642385972* value)
	{
		___Channels_0 = value;
		Il2CppCodeGenWriteBarrier(&___Channels_0, value);
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(ReconnectState_1_t3075722957, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Callback_2() { return static_cast<int32_t>(offsetof(ReconnectState_1_t3075722957, ___Callback_2)); }
	inline Action_1_t2491248677 * get_Callback_2() const { return ___Callback_2; }
	inline Action_1_t2491248677 ** get_address_of_Callback_2() { return &___Callback_2; }
	inline void set_Callback_2(Action_1_t2491248677 * value)
	{
		___Callback_2 = value;
		Il2CppCodeGenWriteBarrier(&___Callback_2, value);
	}

	inline static int32_t get_offset_of_ErrorCallback_3() { return static_cast<int32_t>(offsetof(ReconnectState_1_t3075722957, ___ErrorCallback_3)); }
	inline Action_1_t4207084599 * get_ErrorCallback_3() const { return ___ErrorCallback_3; }
	inline Action_1_t4207084599 ** get_address_of_ErrorCallback_3() { return &___ErrorCallback_3; }
	inline void set_ErrorCallback_3(Action_1_t4207084599 * value)
	{
		___ErrorCallback_3 = value;
		Il2CppCodeGenWriteBarrier(&___ErrorCallback_3, value);
	}

	inline static int32_t get_offset_of_ConnectCallback_4() { return static_cast<int32_t>(offsetof(ReconnectState_1_t3075722957, ___ConnectCallback_4)); }
	inline Action_1_t2491248677 * get_ConnectCallback_4() const { return ___ConnectCallback_4; }
	inline Action_1_t2491248677 ** get_address_of_ConnectCallback_4() { return &___ConnectCallback_4; }
	inline void set_ConnectCallback_4(Action_1_t2491248677 * value)
	{
		___ConnectCallback_4 = value;
		Il2CppCodeGenWriteBarrier(&___ConnectCallback_4, value);
	}

	inline static int32_t get_offset_of_Timetoken_5() { return static_cast<int32_t>(offsetof(ReconnectState_1_t3075722957, ___Timetoken_5)); }
	inline Il2CppObject * get_Timetoken_5() const { return ___Timetoken_5; }
	inline Il2CppObject ** get_address_of_Timetoken_5() { return &___Timetoken_5; }
	inline void set_Timetoken_5(Il2CppObject * value)
	{
		___Timetoken_5 = value;
		Il2CppCodeGenWriteBarrier(&___Timetoken_5, value);
	}

	inline static int32_t get_offset_of_Reconnect_6() { return static_cast<int32_t>(offsetof(ReconnectState_1_t3075722957, ___Reconnect_6)); }
	inline bool get_Reconnect_6() const { return ___Reconnect_6; }
	inline bool* get_address_of_Reconnect_6() { return &___Reconnect_6; }
	inline void set_Reconnect_6(bool value)
	{
		___Reconnect_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
