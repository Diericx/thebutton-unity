﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.ReconnectState`1<System.Object>
struct ReconnectState_1_t3075722957;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Core.ReconnectState`1<System.Object>::.ctor()
extern "C"  void ReconnectState_1__ctor_m4216862355_gshared (ReconnectState_1_t3075722957 * __this, const MethodInfo* method);
#define ReconnectState_1__ctor_m4216862355(__this, method) ((  void (*) (ReconnectState_1_t3075722957 *, const MethodInfo*))ReconnectState_1__ctor_m4216862355_gshared)(__this, method)
