﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_RequestState_18940997MethodDeclarations.h"

// System.Void PubNubMessaging.Core.RequestState`1<System.String>::.ctor()
#define RequestState_1__ctor_m1309068839(__this, method) ((  void (*) (RequestState_1_t3643679231 *, const MethodInfo*))RequestState_1__ctor_m3717890875_gshared)(__this, method)
// System.Void PubNubMessaging.Core.RequestState`1<System.String>::.ctor(PubNubMessaging.Core.RequestState`1<T>)
#define RequestState_1__ctor_m1189525657(__this, ___requestState0, method) ((  void (*) (RequestState_1_t3643679231 *, RequestState_1_t3643679231 *, const MethodInfo*))RequestState_1__ctor_m4012044253_gshared)(__this, ___requestState0, method)
