﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action`1<PubNubMessaging.Core.PubnubClientError>
struct Action_1_t4207084599;
// PubNubMessaging.Core.PubnubWebRequest
struct PubnubWebRequest_t3863823607;
// PubNubMessaging.Core.PubnubWebResponse
struct PubnubWebResponse_t647984363;
// System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>
struct List_1_t2635275738;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ResponseTyp1248099179.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.RequestState`1<System.Object>
struct  RequestState_1_t8940997  : public Il2CppObject
{
public:
	// System.Action`1<T> PubNubMessaging.Core.RequestState`1::SuccessCallback
	Action_1_t2491248677 * ___SuccessCallback_0;
	// System.Action`1<PubNubMessaging.Core.PubnubClientError> PubNubMessaging.Core.RequestState`1::ErrorCallback
	Action_1_t4207084599 * ___ErrorCallback_1;
	// PubNubMessaging.Core.PubnubWebRequest PubNubMessaging.Core.RequestState`1::Request
	PubnubWebRequest_t3863823607 * ___Request_2;
	// PubNubMessaging.Core.PubnubWebResponse PubNubMessaging.Core.RequestState`1::Response
	PubnubWebResponse_t647984363 * ___Response_3;
	// PubNubMessaging.Core.ResponseType PubNubMessaging.Core.RequestState`1::RespType
	int32_t ___RespType_4;
	// System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity> PubNubMessaging.Core.RequestState`1::ChannelEntities
	List_1_t2635275738 * ___ChannelEntities_5;
	// System.Boolean PubNubMessaging.Core.RequestState`1::Timeout
	bool ___Timeout_6;
	// System.Boolean PubNubMessaging.Core.RequestState`1::Reconnect
	bool ___Reconnect_7;
	// System.Int64 PubNubMessaging.Core.RequestState`1::Timetoken
	int64_t ___Timetoken_8;
	// System.Type PubNubMessaging.Core.RequestState`1::TypeParameterType
	Type_t * ___TypeParameterType_9;
	// System.Int64 PubNubMessaging.Core.RequestState`1::ID
	int64_t ___ID_10;
	// System.String PubNubMessaging.Core.RequestState`1::UUID
	String_t* ___UUID_11;

public:
	inline static int32_t get_offset_of_SuccessCallback_0() { return static_cast<int32_t>(offsetof(RequestState_1_t8940997, ___SuccessCallback_0)); }
	inline Action_1_t2491248677 * get_SuccessCallback_0() const { return ___SuccessCallback_0; }
	inline Action_1_t2491248677 ** get_address_of_SuccessCallback_0() { return &___SuccessCallback_0; }
	inline void set_SuccessCallback_0(Action_1_t2491248677 * value)
	{
		___SuccessCallback_0 = value;
		Il2CppCodeGenWriteBarrier(&___SuccessCallback_0, value);
	}

	inline static int32_t get_offset_of_ErrorCallback_1() { return static_cast<int32_t>(offsetof(RequestState_1_t8940997, ___ErrorCallback_1)); }
	inline Action_1_t4207084599 * get_ErrorCallback_1() const { return ___ErrorCallback_1; }
	inline Action_1_t4207084599 ** get_address_of_ErrorCallback_1() { return &___ErrorCallback_1; }
	inline void set_ErrorCallback_1(Action_1_t4207084599 * value)
	{
		___ErrorCallback_1 = value;
		Il2CppCodeGenWriteBarrier(&___ErrorCallback_1, value);
	}

	inline static int32_t get_offset_of_Request_2() { return static_cast<int32_t>(offsetof(RequestState_1_t8940997, ___Request_2)); }
	inline PubnubWebRequest_t3863823607 * get_Request_2() const { return ___Request_2; }
	inline PubnubWebRequest_t3863823607 ** get_address_of_Request_2() { return &___Request_2; }
	inline void set_Request_2(PubnubWebRequest_t3863823607 * value)
	{
		___Request_2 = value;
		Il2CppCodeGenWriteBarrier(&___Request_2, value);
	}

	inline static int32_t get_offset_of_Response_3() { return static_cast<int32_t>(offsetof(RequestState_1_t8940997, ___Response_3)); }
	inline PubnubWebResponse_t647984363 * get_Response_3() const { return ___Response_3; }
	inline PubnubWebResponse_t647984363 ** get_address_of_Response_3() { return &___Response_3; }
	inline void set_Response_3(PubnubWebResponse_t647984363 * value)
	{
		___Response_3 = value;
		Il2CppCodeGenWriteBarrier(&___Response_3, value);
	}

	inline static int32_t get_offset_of_RespType_4() { return static_cast<int32_t>(offsetof(RequestState_1_t8940997, ___RespType_4)); }
	inline int32_t get_RespType_4() const { return ___RespType_4; }
	inline int32_t* get_address_of_RespType_4() { return &___RespType_4; }
	inline void set_RespType_4(int32_t value)
	{
		___RespType_4 = value;
	}

	inline static int32_t get_offset_of_ChannelEntities_5() { return static_cast<int32_t>(offsetof(RequestState_1_t8940997, ___ChannelEntities_5)); }
	inline List_1_t2635275738 * get_ChannelEntities_5() const { return ___ChannelEntities_5; }
	inline List_1_t2635275738 ** get_address_of_ChannelEntities_5() { return &___ChannelEntities_5; }
	inline void set_ChannelEntities_5(List_1_t2635275738 * value)
	{
		___ChannelEntities_5 = value;
		Il2CppCodeGenWriteBarrier(&___ChannelEntities_5, value);
	}

	inline static int32_t get_offset_of_Timeout_6() { return static_cast<int32_t>(offsetof(RequestState_1_t8940997, ___Timeout_6)); }
	inline bool get_Timeout_6() const { return ___Timeout_6; }
	inline bool* get_address_of_Timeout_6() { return &___Timeout_6; }
	inline void set_Timeout_6(bool value)
	{
		___Timeout_6 = value;
	}

	inline static int32_t get_offset_of_Reconnect_7() { return static_cast<int32_t>(offsetof(RequestState_1_t8940997, ___Reconnect_7)); }
	inline bool get_Reconnect_7() const { return ___Reconnect_7; }
	inline bool* get_address_of_Reconnect_7() { return &___Reconnect_7; }
	inline void set_Reconnect_7(bool value)
	{
		___Reconnect_7 = value;
	}

	inline static int32_t get_offset_of_Timetoken_8() { return static_cast<int32_t>(offsetof(RequestState_1_t8940997, ___Timetoken_8)); }
	inline int64_t get_Timetoken_8() const { return ___Timetoken_8; }
	inline int64_t* get_address_of_Timetoken_8() { return &___Timetoken_8; }
	inline void set_Timetoken_8(int64_t value)
	{
		___Timetoken_8 = value;
	}

	inline static int32_t get_offset_of_TypeParameterType_9() { return static_cast<int32_t>(offsetof(RequestState_1_t8940997, ___TypeParameterType_9)); }
	inline Type_t * get_TypeParameterType_9() const { return ___TypeParameterType_9; }
	inline Type_t ** get_address_of_TypeParameterType_9() { return &___TypeParameterType_9; }
	inline void set_TypeParameterType_9(Type_t * value)
	{
		___TypeParameterType_9 = value;
		Il2CppCodeGenWriteBarrier(&___TypeParameterType_9, value);
	}

	inline static int32_t get_offset_of_ID_10() { return static_cast<int32_t>(offsetof(RequestState_1_t8940997, ___ID_10)); }
	inline int64_t get_ID_10() const { return ___ID_10; }
	inline int64_t* get_address_of_ID_10() { return &___ID_10; }
	inline void set_ID_10(int64_t value)
	{
		___ID_10 = value;
	}

	inline static int32_t get_offset_of_UUID_11() { return static_cast<int32_t>(offsetof(RequestState_1_t8940997, ___UUID_11)); }
	inline String_t* get_UUID_11() const { return ___UUID_11; }
	inline String_t** get_address_of_UUID_11() { return &___UUID_11; }
	inline void set_UUID_11(String_t* value)
	{
		___UUID_11 = value;
		Il2CppCodeGenWriteBarrier(&___UUID_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
