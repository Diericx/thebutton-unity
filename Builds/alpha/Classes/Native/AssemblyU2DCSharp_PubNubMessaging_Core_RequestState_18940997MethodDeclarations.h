﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.RequestState`1<System.Object>
struct RequestState_1_t8940997;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Core.RequestState`1<System.Object>::.ctor()
extern "C"  void RequestState_1__ctor_m3717890875_gshared (RequestState_1_t8940997 * __this, const MethodInfo* method);
#define RequestState_1__ctor_m3717890875(__this, method) ((  void (*) (RequestState_1_t8940997 *, const MethodInfo*))RequestState_1__ctor_m3717890875_gshared)(__this, method)
// System.Void PubNubMessaging.Core.RequestState`1<System.Object>::.ctor(PubNubMessaging.Core.RequestState`1<T>)
extern "C"  void RequestState_1__ctor_m4012044253_gshared (RequestState_1_t8940997 * __this, RequestState_1_t8940997 * ___requestState0, const MethodInfo* method);
#define RequestState_1__ctor_m4012044253(__this, ___requestState0, method) ((  void (*) (RequestState_1_t8940997 *, RequestState_1_t8940997 *, const MethodInfo*))RequestState_1__ctor_m4012044253_gshared)(__this, ___requestState0, method)
