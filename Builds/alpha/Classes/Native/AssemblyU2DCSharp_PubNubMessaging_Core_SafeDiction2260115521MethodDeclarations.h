﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>
struct SafeDictionary_2_t2260115521;
// System.Object
struct Il2CppObject;
// System.Func`3<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Object>
struct Func_3_t1343333346;
// System.Collections.Generic.ICollection`1<PubNubMessaging.Core.CurrentRequestType>
struct ICollection_1_t4202303291;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>[]
struct KeyValuePair_2U5BU5D_t828907107;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>
struct IEnumerator_1_t579852473;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23104328646.h"

// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::.ctor()
extern "C"  void SafeDictionary_2__ctor_m3879644162_gshared (SafeDictionary_2_t2260115521 * __this, const MethodInfo* method);
#define SafeDictionary_2__ctor_m3879644162(__this, method) ((  void (*) (SafeDictionary_2_t2260115521 *, const MethodInfo*))SafeDictionary_2__ctor_m3879644162_gshared)(__this, method)
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Add(TKey,TValue)
extern "C"  void SafeDictionary_2_Add_m314587869_gshared (SafeDictionary_2_t2260115521 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SafeDictionary_2_Add_m314587869(__this, ___key0, ___value1, method) ((  void (*) (SafeDictionary_2_t2260115521 *, int32_t, Il2CppObject *, const MethodInfo*))SafeDictionary_2_Add_m314587869_gshared)(__this, ___key0, ___value1, method)
// TValue PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::AddOrUpdate(TKey,TValue,System.Func`3<TKey,TValue,TValue>)
extern "C"  Il2CppObject * SafeDictionary_2_AddOrUpdate_m451755460_gshared (SafeDictionary_2_t2260115521 * __this, int32_t ___key0, Il2CppObject * ___value1, Func_3_t1343333346 * ___f2, const MethodInfo* method);
#define SafeDictionary_2_AddOrUpdate_m451755460(__this, ___key0, ___value1, ___f2, method) ((  Il2CppObject * (*) (SafeDictionary_2_t2260115521 *, int32_t, Il2CppObject *, Func_3_t1343333346 *, const MethodInfo*))SafeDictionary_2_AddOrUpdate_m451755460_gshared)(__this, ___key0, ___value1, ___f2, method)
// TValue PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::GetOrAdd(TKey,TValue)
extern "C"  Il2CppObject * SafeDictionary_2_GetOrAdd_m1063728122_gshared (SafeDictionary_2_t2260115521 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SafeDictionary_2_GetOrAdd_m1063728122(__this, ___key0, ___value1, method) ((  Il2CppObject * (*) (SafeDictionary_2_t2260115521 *, int32_t, Il2CppObject *, const MethodInfo*))SafeDictionary_2_GetOrAdd_m1063728122_gshared)(__this, ___key0, ___value1, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::ContainsKey(TKey)
extern "C"  bool SafeDictionary_2_ContainsKey_m98092443_gshared (SafeDictionary_2_t2260115521 * __this, int32_t ___key0, const MethodInfo* method);
#define SafeDictionary_2_ContainsKey_m98092443(__this, ___key0, method) ((  bool (*) (SafeDictionary_2_t2260115521 *, int32_t, const MethodInfo*))SafeDictionary_2_ContainsKey_m98092443_gshared)(__this, ___key0, method)
// System.Collections.Generic.ICollection`1<TKey> PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Keys()
extern "C"  Il2CppObject* SafeDictionary_2_get_Keys_m49952234_gshared (SafeDictionary_2_t2260115521 * __this, const MethodInfo* method);
#define SafeDictionary_2_get_Keys_m49952234(__this, method) ((  Il2CppObject* (*) (SafeDictionary_2_t2260115521 *, const MethodInfo*))SafeDictionary_2_get_Keys_m49952234_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Remove(TKey)
extern "C"  bool SafeDictionary_2_Remove_m1715787805_gshared (SafeDictionary_2_t2260115521 * __this, int32_t ___key0, const MethodInfo* method);
#define SafeDictionary_2_Remove_m1715787805(__this, ___key0, method) ((  bool (*) (SafeDictionary_2_t2260115521 *, int32_t, const MethodInfo*))SafeDictionary_2_Remove_m1715787805_gshared)(__this, ___key0, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Remove(TKey,TValue&)
extern "C"  bool SafeDictionary_2_Remove_m3758221292_gshared (SafeDictionary_2_t2260115521 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define SafeDictionary_2_Remove_m3758221292(__this, ___key0, ___value1, method) ((  bool (*) (SafeDictionary_2_t2260115521 *, int32_t, Il2CppObject **, const MethodInfo*))SafeDictionary_2_Remove_m3758221292_gshared)(__this, ___key0, ___value1, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::TryRemove(TKey,TValue&)
extern "C"  bool SafeDictionary_2_TryRemove_m807166159_gshared (SafeDictionary_2_t2260115521 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define SafeDictionary_2_TryRemove_m807166159(__this, ___key0, ___value1, method) ((  bool (*) (SafeDictionary_2_t2260115521 *, int32_t, Il2CppObject **, const MethodInfo*))SafeDictionary_2_TryRemove_m807166159_gshared)(__this, ___key0, ___value1, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool SafeDictionary_2_TryGetValue_m2503777870_gshared (SafeDictionary_2_t2260115521 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define SafeDictionary_2_TryGetValue_m2503777870(__this, ___key0, ___value1, method) ((  bool (*) (SafeDictionary_2_t2260115521 *, int32_t, Il2CppObject **, const MethodInfo*))SafeDictionary_2_TryGetValue_m2503777870_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.ICollection`1<TValue> PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Values()
extern "C"  Il2CppObject* SafeDictionary_2_get_Values_m2932400594_gshared (SafeDictionary_2_t2260115521 * __this, const MethodInfo* method);
#define SafeDictionary_2_get_Values_m2932400594(__this, method) ((  Il2CppObject* (*) (SafeDictionary_2_t2260115521 *, const MethodInfo*))SafeDictionary_2_get_Values_m2932400594_gshared)(__this, method)
// TValue PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * SafeDictionary_2_get_Item_m3585663459_gshared (SafeDictionary_2_t2260115521 * __this, int32_t ___key0, const MethodInfo* method);
#define SafeDictionary_2_get_Item_m3585663459(__this, ___key0, method) ((  Il2CppObject * (*) (SafeDictionary_2_t2260115521 *, int32_t, const MethodInfo*))SafeDictionary_2_get_Item_m3585663459_gshared)(__this, ___key0, method)
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::set_Item(TKey,TValue)
extern "C"  void SafeDictionary_2_set_Item_m1676305360_gshared (SafeDictionary_2_t2260115521 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SafeDictionary_2_set_Item_m1676305360(__this, ___key0, ___value1, method) ((  void (*) (SafeDictionary_2_t2260115521 *, int32_t, Il2CppObject *, const MethodInfo*))SafeDictionary_2_set_Item_m1676305360_gshared)(__this, ___key0, ___value1, method)
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void SafeDictionary_2_Add_m4012251184_gshared (SafeDictionary_2_t2260115521 * __this, KeyValuePair_2_t3104328646  ___item0, const MethodInfo* method);
#define SafeDictionary_2_Add_m4012251184(__this, ___item0, method) ((  void (*) (SafeDictionary_2_t2260115521 *, KeyValuePair_2_t3104328646 , const MethodInfo*))SafeDictionary_2_Add_m4012251184_gshared)(__this, ___item0, method)
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Clear()
extern "C"  void SafeDictionary_2_Clear_m2336160809_gshared (SafeDictionary_2_t2260115521 * __this, const MethodInfo* method);
#define SafeDictionary_2_Clear_m2336160809(__this, method) ((  void (*) (SafeDictionary_2_t2260115521 *, const MethodInfo*))SafeDictionary_2_Clear_m2336160809_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SafeDictionary_2_Contains_m2506908634_gshared (SafeDictionary_2_t2260115521 * __this, KeyValuePair_2_t3104328646  ___item0, const MethodInfo* method);
#define SafeDictionary_2_Contains_m2506908634(__this, ___item0, method) ((  bool (*) (SafeDictionary_2_t2260115521 *, KeyValuePair_2_t3104328646 , const MethodInfo*))SafeDictionary_2_Contains_m2506908634_gshared)(__this, ___item0, method)
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void SafeDictionary_2_CopyTo_m804417436_gshared (SafeDictionary_2_t2260115521 * __this, KeyValuePair_2U5BU5D_t828907107* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define SafeDictionary_2_CopyTo_m804417436(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SafeDictionary_2_t2260115521 *, KeyValuePair_2U5BU5D_t828907107*, int32_t, const MethodInfo*))SafeDictionary_2_CopyTo_m804417436_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Count()
extern "C"  int32_t SafeDictionary_2_get_Count_m877073808_gshared (SafeDictionary_2_t2260115521 * __this, const MethodInfo* method);
#define SafeDictionary_2_get_Count_m877073808(__this, method) ((  int32_t (*) (SafeDictionary_2_t2260115521 *, const MethodInfo*))SafeDictionary_2_get_Count_m877073808_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_IsReadOnly()
extern "C"  bool SafeDictionary_2_get_IsReadOnly_m3793001841_gshared (SafeDictionary_2_t2260115521 * __this, const MethodInfo* method);
#define SafeDictionary_2_get_IsReadOnly_m3793001841(__this, method) ((  bool (*) (SafeDictionary_2_t2260115521 *, const MethodInfo*))SafeDictionary_2_get_IsReadOnly_m3793001841_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SafeDictionary_2_Remove_m179165939_gshared (SafeDictionary_2_t2260115521 * __this, KeyValuePair_2_t3104328646  ___item0, const MethodInfo* method);
#define SafeDictionary_2_Remove_m179165939(__this, ___item0, method) ((  bool (*) (SafeDictionary_2_t2260115521 *, KeyValuePair_2_t3104328646 , const MethodInfo*))SafeDictionary_2_Remove_m179165939_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* SafeDictionary_2_GetEnumerator_m1940543661_gshared (SafeDictionary_2_t2260115521 * __this, const MethodInfo* method);
#define SafeDictionary_2_GetEnumerator_m1940543661(__this, method) ((  Il2CppObject* (*) (SafeDictionary_2_t2260115521 *, const MethodInfo*))SafeDictionary_2_GetEnumerator_m1940543661_gshared)(__this, method)
// System.Collections.IEnumerator PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * SafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m4022993505_gshared (SafeDictionary_2_t2260115521 * __this, const MethodInfo* method);
#define SafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m4022993505(__this, method) ((  Il2CppObject * (*) (SafeDictionary_2_t2260115521 *, const MethodInfo*))SafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m4022993505_gshared)(__this, method)
