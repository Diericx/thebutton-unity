﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>
struct Dictionary_2_t1482986037;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>
struct  SafeDictionary_2_t2691085430  : public Il2CppObject
{
public:
	// System.Object PubNubMessaging.Core.SafeDictionary`2::syncRoot
	Il2CppObject * ___syncRoot_0;
	// System.Collections.Generic.Dictionary`2<TKey,TValue> PubNubMessaging.Core.SafeDictionary`2::d
	Dictionary_2_t1482986037 * ___d_1;

public:
	inline static int32_t get_offset_of_syncRoot_0() { return static_cast<int32_t>(offsetof(SafeDictionary_2_t2691085430, ___syncRoot_0)); }
	inline Il2CppObject * get_syncRoot_0() const { return ___syncRoot_0; }
	inline Il2CppObject ** get_address_of_syncRoot_0() { return &___syncRoot_0; }
	inline void set_syncRoot_0(Il2CppObject * value)
	{
		___syncRoot_0 = value;
		Il2CppCodeGenWriteBarrier(&___syncRoot_0, value);
	}

	inline static int32_t get_offset_of_d_1() { return static_cast<int32_t>(offsetof(SafeDictionary_2_t2691085430, ___d_1)); }
	inline Dictionary_2_t1482986037 * get_d_1() const { return ___d_1; }
	inline Dictionary_2_t1482986037 ** get_address_of_d_1() { return &___d_1; }
	inline void set_d_1(Dictionary_2_t1482986037 * value)
	{
		___d_1 = value;
		Il2CppCodeGenWriteBarrier(&___d_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
