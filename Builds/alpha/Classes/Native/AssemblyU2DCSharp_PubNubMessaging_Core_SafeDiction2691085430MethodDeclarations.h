﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_SafeDictiona537630836MethodDeclarations.h"

// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::.ctor()
#define SafeDictionary_2__ctor_m828600601(__this, method) ((  void (*) (SafeDictionary_2_t2691085430 *, const MethodInfo*))SafeDictionary_2__ctor_m477735165_gshared)(__this, method)
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::Add(TKey,TValue)
#define SafeDictionary_2_Add_m423951692(__this, ___key0, ___value1, method) ((  void (*) (SafeDictionary_2_t2691085430 *, ChannelIdentity_t1147162267 , ChannelParameters_t547936593 *, const MethodInfo*))SafeDictionary_2_Add_m2132422254_gshared)(__this, ___key0, ___value1, method)
// TValue PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::AddOrUpdate(TKey,TValue,System.Func`3<TKey,TValue,TValue>)
#define SafeDictionary_2_AddOrUpdate_m2416384151(__this, ___key0, ___value1, ___f2, method) ((  ChannelParameters_t547936593 * (*) (SafeDictionary_2_t2691085430 *, ChannelIdentity_t1147162267 , ChannelParameters_t547936593 *, Func_3_t3197735739 *, const MethodInfo*))SafeDictionary_2_AddOrUpdate_m4069671251_gshared)(__this, ___key0, ___value1, ___f2, method)
// TValue PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::GetOrAdd(TKey,TValue)
#define SafeDictionary_2_GetOrAdd_m1553673455(__this, ___key0, ___value1, method) ((  ChannelParameters_t547936593 * (*) (SafeDictionary_2_t2691085430 *, ChannelIdentity_t1147162267 , ChannelParameters_t547936593 *, const MethodInfo*))SafeDictionary_2_GetOrAdd_m1330376947_gshared)(__this, ___key0, ___value1, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::ContainsKey(TKey)
#define SafeDictionary_2_ContainsKey_m591965710(__this, ___key0, method) ((  bool (*) (SafeDictionary_2_t2691085430 *, ChannelIdentity_t1147162267 , const MethodInfo*))SafeDictionary_2_ContainsKey_m1555033472_gshared)(__this, ___key0, method)
// System.Collections.Generic.ICollection`1<TKey> PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::get_Keys()
#define SafeDictionary_2_get_Keys_m2049701743(__this, method) ((  Il2CppObject* (*) (SafeDictionary_2_t2691085430 *, const MethodInfo*))SafeDictionary_2_get_Keys_m3127251543_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::Remove(TKey)
#define SafeDictionary_2_Remove_m3177639050(__this, ___key0, method) ((  bool (*) (SafeDictionary_2_t2691085430 *, ChannelIdentity_t1147162267 , const MethodInfo*))SafeDictionary_2_Remove_m3302682888_gshared)(__this, ___key0, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::Remove(TKey,TValue&)
#define SafeDictionary_2_Remove_m2923393297(__this, ___key0, ___value1, method) ((  bool (*) (SafeDictionary_2_t2691085430 *, ChannelIdentity_t1147162267 , ChannelParameters_t547936593 **, const MethodInfo*))SafeDictionary_2_Remove_m1992196365_gshared)(__this, ___key0, ___value1, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::TryRemove(TKey,TValue&)
#define SafeDictionary_2_TryRemove_m2616744332(__this, ___key0, ___value1, method) ((  bool (*) (SafeDictionary_2_t2691085430 *, ChannelIdentity_t1147162267 , ChannelParameters_t547936593 **, const MethodInfo*))SafeDictionary_2_TryRemove_m315854466_gshared)(__this, ___key0, ___value1, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::TryGetValue(TKey,TValue&)
#define SafeDictionary_2_TryGetValue_m2797874881(__this, ___key0, ___value1, method) ((  bool (*) (SafeDictionary_2_t2691085430 *, ChannelIdentity_t1147162267 , ChannelParameters_t547936593 **, const MethodInfo*))SafeDictionary_2_TryGetValue_m187104957_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.ICollection`1<TValue> PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::get_Values()
#define SafeDictionary_2_get_Values_m2628278391(__this, method) ((  Il2CppObject* (*) (SafeDictionary_2_t2691085430 *, const MethodInfo*))SafeDictionary_2_get_Values_m3845365399_gshared)(__this, method)
// TValue PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::get_Item(TKey)
#define SafeDictionary_2_get_Item_m3333396918(__this, ___key0, method) ((  ChannelParameters_t547936593 * (*) (SafeDictionary_2_t2691085430 *, ChannelIdentity_t1147162267 , const MethodInfo*))SafeDictionary_2_get_Item_m1670049536_gshared)(__this, ___key0, method)
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::set_Item(TKey,TValue)
#define SafeDictionary_2_set_Item_m243789825(__this, ___key0, ___value1, method) ((  void (*) (SafeDictionary_2_t2691085430 *, ChannelIdentity_t1147162267 , ChannelParameters_t547936593 *, const MethodInfo*))SafeDictionary_2_set_Item_m651937877_gshared)(__this, ___key0, ___value1, method)
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SafeDictionary_2_Add_m508112337(__this, ___item0, method) ((  void (*) (SafeDictionary_2_t2691085430 *, KeyValuePair_2_t3535298555 , const MethodInfo*))SafeDictionary_2_Add_m2696016565_gshared)(__this, ___item0, method)
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::Clear()
#define SafeDictionary_2_Clear_m1799548104(__this, method) ((  void (*) (SafeDictionary_2_t2691085430 *, const MethodInfo*))SafeDictionary_2_Clear_m3344381162_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SafeDictionary_2_Contains_m3624433423(__this, ___item0, method) ((  bool (*) (SafeDictionary_2_t2691085430 *, KeyValuePair_2_t3535298555 , const MethodInfo*))SafeDictionary_2_Contains_m1452309491_gshared)(__this, ___item0, method)
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define SafeDictionary_2_CopyTo_m1031672389(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SafeDictionary_2_t2691085430 *, KeyValuePair_2U5BU5D_t529854906*, int32_t, const MethodInfo*))SafeDictionary_2_CopyTo_m3279940313_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::get_Count()
#define SafeDictionary_2_get_Count_m3277333149(__this, method) ((  int32_t (*) (SafeDictionary_2_t2691085430 *, const MethodInfo*))SafeDictionary_2_get_Count_m2353695869_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::get_IsReadOnly()
#define SafeDictionary_2_get_IsReadOnly_m3007675156(__this, method) ((  bool (*) (SafeDictionary_2_t2691085430 *, const MethodInfo*))SafeDictionary_2_get_IsReadOnly_m3203599390_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define SafeDictionary_2_Remove_m3675583446(__this, ___item0, method) ((  bool (*) (SafeDictionary_2_t2691085430 *, KeyValuePair_2_t3535298555 , const MethodInfo*))SafeDictionary_2_Remove_m2963108272_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::GetEnumerator()
#define SafeDictionary_2_GetEnumerator_m4226669280(__this, method) ((  Il2CppObject* (*) (SafeDictionary_2_t2691085430 *, const MethodInfo*))SafeDictionary_2_GetEnumerator_m4239269880_gshared)(__this, method)
// System.Collections.IEnumerator PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::System.Collections.IEnumerable.GetEnumerator()
#define SafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m2568330812(__this, method) ((  Il2CppObject * (*) (SafeDictionary_2_t2691085430 *, const MethodInfo*))SafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1565768858_gshared)(__this, method)
