﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>
struct SafeDictionary_2_t3489608816;
// System.Object
struct Il2CppObject;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t3369346583;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2854920344;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t1809345768;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"

// System.Void PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void SafeDictionary_2__ctor_m185996061_gshared (SafeDictionary_2_t3489608816 * __this, const MethodInfo* method);
#define SafeDictionary_2__ctor_m185996061(__this, method) ((  void (*) (SafeDictionary_2_t3489608816 *, const MethodInfo*))SafeDictionary_2__ctor_m185996061_gshared)(__this, method)
// System.Void PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C"  void SafeDictionary_2_Add_m3293730050_gshared (SafeDictionary_2_t3489608816 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SafeDictionary_2_Add_m3293730050(__this, ___key0, ___value1, method) ((  void (*) (SafeDictionary_2_t3489608816 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SafeDictionary_2_Add_m3293730050_gshared)(__this, ___key0, ___value1, method)
// TValue PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::AddOrUpdate(TKey,TValue,System.Func`3<TKey,TValue,TValue>)
extern "C"  Il2CppObject * SafeDictionary_2_AddOrUpdate_m2465249327_gshared (SafeDictionary_2_t3489608816 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, Func_3_t3369346583 * ___f2, const MethodInfo* method);
#define SafeDictionary_2_AddOrUpdate_m2465249327(__this, ___key0, ___value1, ___f2, method) ((  Il2CppObject * (*) (SafeDictionary_2_t3489608816 *, Il2CppObject *, Il2CppObject *, Func_3_t3369346583 *, const MethodInfo*))SafeDictionary_2_AddOrUpdate_m2465249327_gshared)(__this, ___key0, ___value1, ___f2, method)
// TValue PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::GetOrAdd(TKey,TValue)
extern "C"  Il2CppObject * SafeDictionary_2_GetOrAdd_m4159390623_gshared (SafeDictionary_2_t3489608816 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SafeDictionary_2_GetOrAdd_m4159390623(__this, ___key0, ___value1, method) ((  Il2CppObject * (*) (SafeDictionary_2_t3489608816 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SafeDictionary_2_GetOrAdd_m4159390623_gshared)(__this, ___key0, ___value1, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C"  bool SafeDictionary_2_ContainsKey_m2132572568_gshared (SafeDictionary_2_t3489608816 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SafeDictionary_2_ContainsKey_m2132572568(__this, ___key0, method) ((  bool (*) (SafeDictionary_2_t3489608816 *, Il2CppObject *, const MethodInfo*))SafeDictionary_2_ContainsKey_m2132572568_gshared)(__this, ___key0, method)
// System.Collections.Generic.ICollection`1<TKey> PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::get_Keys()
extern "C"  Il2CppObject* SafeDictionary_2_get_Keys_m1102826111_gshared (SafeDictionary_2_t3489608816 * __this, const MethodInfo* method);
#define SafeDictionary_2_get_Keys_m1102826111(__this, method) ((  Il2CppObject* (*) (SafeDictionary_2_t3489608816 *, const MethodInfo*))SafeDictionary_2_get_Keys_m1102826111_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::Remove(TKey)
extern "C"  bool SafeDictionary_2_Remove_m595894796_gshared (SafeDictionary_2_t3489608816 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SafeDictionary_2_Remove_m595894796(__this, ___key0, method) ((  bool (*) (SafeDictionary_2_t3489608816 *, Il2CppObject *, const MethodInfo*))SafeDictionary_2_Remove_m595894796_gshared)(__this, ___key0, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::Remove(TKey,TValue&)
extern "C"  bool SafeDictionary_2_Remove_m428347805_gshared (SafeDictionary_2_t3489608816 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define SafeDictionary_2_Remove_m428347805(__this, ___key0, ___value1, method) ((  bool (*) (SafeDictionary_2_t3489608816 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))SafeDictionary_2_Remove_m428347805_gshared)(__this, ___key0, ___value1, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::TryRemove(TKey,TValue&)
extern "C"  bool SafeDictionary_2_TryRemove_m127498602_gshared (SafeDictionary_2_t3489608816 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define SafeDictionary_2_TryRemove_m127498602(__this, ___key0, ___value1, method) ((  bool (*) (SafeDictionary_2_t3489608816 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))SafeDictionary_2_TryRemove_m127498602_gshared)(__this, ___key0, ___value1, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool SafeDictionary_2_TryGetValue_m2854498045_gshared (SafeDictionary_2_t3489608816 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define SafeDictionary_2_TryGetValue_m2854498045(__this, ___key0, ___value1, method) ((  bool (*) (SafeDictionary_2_t3489608816 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))SafeDictionary_2_TryGetValue_m2854498045_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.ICollection`1<TValue> PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::get_Values()
extern "C"  Il2CppObject* SafeDictionary_2_get_Values_m1477461839_gshared (SafeDictionary_2_t3489608816 * __this, const MethodInfo* method);
#define SafeDictionary_2_get_Values_m1477461839(__this, method) ((  Il2CppObject* (*) (SafeDictionary_2_t3489608816 *, const MethodInfo*))SafeDictionary_2_get_Values_m1477461839_gshared)(__this, method)
// TValue PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * SafeDictionary_2_get_Item_m713319416_gshared (SafeDictionary_2_t3489608816 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define SafeDictionary_2_get_Item_m713319416(__this, ___key0, method) ((  Il2CppObject * (*) (SafeDictionary_2_t3489608816 *, Il2CppObject *, const MethodInfo*))SafeDictionary_2_get_Item_m713319416_gshared)(__this, ___key0, method)
// System.Void PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C"  void SafeDictionary_2_set_Item_m1589065141_gshared (SafeDictionary_2_t3489608816 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SafeDictionary_2_set_Item_m1589065141(__this, ___key0, ___value1, method) ((  void (*) (SafeDictionary_2_t3489608816 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))SafeDictionary_2_set_Item_m1589065141_gshared)(__this, ___key0, ___value1, method)
// System.Void PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void SafeDictionary_2_Add_m2131419317_gshared (SafeDictionary_2_t3489608816 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method);
#define SafeDictionary_2_Add_m2131419317(__this, ___item0, method) ((  void (*) (SafeDictionary_2_t3489608816 *, KeyValuePair_2_t38854645 , const MethodInfo*))SafeDictionary_2_Add_m2131419317_gshared)(__this, ___item0, method)
// System.Void PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::Clear()
extern "C"  void SafeDictionary_2_Clear_m2775642342_gshared (SafeDictionary_2_t3489608816 * __this, const MethodInfo* method);
#define SafeDictionary_2_Clear_m2775642342(__this, method) ((  void (*) (SafeDictionary_2_t3489608816 *, const MethodInfo*))SafeDictionary_2_Clear_m2775642342_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SafeDictionary_2_Contains_m3251567199_gshared (SafeDictionary_2_t3489608816 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method);
#define SafeDictionary_2_Contains_m3251567199(__this, ___item0, method) ((  bool (*) (SafeDictionary_2_t3489608816 *, KeyValuePair_2_t38854645 , const MethodInfo*))SafeDictionary_2_Contains_m3251567199_gshared)(__this, ___item0, method)
// System.Void PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void SafeDictionary_2_CopyTo_m1628504833_gshared (SafeDictionary_2_t3489608816 * __this, KeyValuePair_2U5BU5D_t2854920344* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define SafeDictionary_2_CopyTo_m1628504833(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SafeDictionary_2_t3489608816 *, KeyValuePair_2U5BU5D_t2854920344*, int32_t, const MethodInfo*))SafeDictionary_2_CopyTo_m1628504833_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t SafeDictionary_2_get_Count_m2126862281_gshared (SafeDictionary_2_t3489608816 * __this, const MethodInfo* method);
#define SafeDictionary_2_get_Count_m2126862281(__this, method) ((  int32_t (*) (SafeDictionary_2_t3489608816 *, const MethodInfo*))SafeDictionary_2_get_Count_m2126862281_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::get_IsReadOnly()
extern "C"  bool SafeDictionary_2_get_IsReadOnly_m2796149634_gshared (SafeDictionary_2_t3489608816 * __this, const MethodInfo* method);
#define SafeDictionary_2_get_IsReadOnly_m2796149634(__this, method) ((  bool (*) (SafeDictionary_2_t3489608816 *, const MethodInfo*))SafeDictionary_2_get_IsReadOnly_m2796149634_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SafeDictionary_2_Remove_m503522568_gshared (SafeDictionary_2_t3489608816 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method);
#define SafeDictionary_2_Remove_m503522568(__this, ___item0, method) ((  bool (*) (SafeDictionary_2_t3489608816 *, KeyValuePair_2_t38854645 , const MethodInfo*))SafeDictionary_2_Remove_m503522568_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* SafeDictionary_2_GetEnumerator_m4207349278_gshared (SafeDictionary_2_t3489608816 * __this, const MethodInfo* method);
#define SafeDictionary_2_GetEnumerator_m4207349278(__this, method) ((  Il2CppObject* (*) (SafeDictionary_2_t3489608816 *, const MethodInfo*))SafeDictionary_2_GetEnumerator_m4207349278_gshared)(__this, method)
// System.Collections.IEnumerator PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * SafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m146141658_gshared (SafeDictionary_2_t3489608816 * __this, const MethodInfo* method);
#define SafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m146141658(__this, method) ((  Il2CppObject * (*) (SafeDictionary_2_t3489608816 *, const MethodInfo*))SafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m146141658_gshared)(__this, method)
