﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>
struct SafeDictionary_2_t537630836;
// System.Object
struct Il2CppObject;
// System.Func`3<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Object>
struct Func_3_t916471171;
// System.Collections.Generic.ICollection`1<PubNubMessaging.Core.ChannelIdentity>
struct ICollection_1_t2099237572;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>[]
struct KeyValuePair_2U5BU5D_t402044932;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>
struct IEnumerator_1_t3152335084;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelIden1147162267.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21381843961.h"

// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::.ctor()
extern "C"  void SafeDictionary_2__ctor_m477735165_gshared (SafeDictionary_2_t537630836 * __this, const MethodInfo* method);
#define SafeDictionary_2__ctor_m477735165(__this, method) ((  void (*) (SafeDictionary_2_t537630836 *, const MethodInfo*))SafeDictionary_2__ctor_m477735165_gshared)(__this, method)
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Add(TKey,TValue)
extern "C"  void SafeDictionary_2_Add_m2132422254_gshared (SafeDictionary_2_t537630836 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SafeDictionary_2_Add_m2132422254(__this, ___key0, ___value1, method) ((  void (*) (SafeDictionary_2_t537630836 *, ChannelIdentity_t1147162267 , Il2CppObject *, const MethodInfo*))SafeDictionary_2_Add_m2132422254_gshared)(__this, ___key0, ___value1, method)
// TValue PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::AddOrUpdate(TKey,TValue,System.Func`3<TKey,TValue,TValue>)
extern "C"  Il2CppObject * SafeDictionary_2_AddOrUpdate_m4069671251_gshared (SafeDictionary_2_t537630836 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, Func_3_t916471171 * ___f2, const MethodInfo* method);
#define SafeDictionary_2_AddOrUpdate_m4069671251(__this, ___key0, ___value1, ___f2, method) ((  Il2CppObject * (*) (SafeDictionary_2_t537630836 *, ChannelIdentity_t1147162267 , Il2CppObject *, Func_3_t916471171 *, const MethodInfo*))SafeDictionary_2_AddOrUpdate_m4069671251_gshared)(__this, ___key0, ___value1, ___f2, method)
// TValue PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::GetOrAdd(TKey,TValue)
extern "C"  Il2CppObject * SafeDictionary_2_GetOrAdd_m1330376947_gshared (SafeDictionary_2_t537630836 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SafeDictionary_2_GetOrAdd_m1330376947(__this, ___key0, ___value1, method) ((  Il2CppObject * (*) (SafeDictionary_2_t537630836 *, ChannelIdentity_t1147162267 , Il2CppObject *, const MethodInfo*))SafeDictionary_2_GetOrAdd_m1330376947_gshared)(__this, ___key0, ___value1, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::ContainsKey(TKey)
extern "C"  bool SafeDictionary_2_ContainsKey_m1555033472_gshared (SafeDictionary_2_t537630836 * __this, ChannelIdentity_t1147162267  ___key0, const MethodInfo* method);
#define SafeDictionary_2_ContainsKey_m1555033472(__this, ___key0, method) ((  bool (*) (SafeDictionary_2_t537630836 *, ChannelIdentity_t1147162267 , const MethodInfo*))SafeDictionary_2_ContainsKey_m1555033472_gshared)(__this, ___key0, method)
// System.Collections.Generic.ICollection`1<TKey> PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Keys()
extern "C"  Il2CppObject* SafeDictionary_2_get_Keys_m3127251543_gshared (SafeDictionary_2_t537630836 * __this, const MethodInfo* method);
#define SafeDictionary_2_get_Keys_m3127251543(__this, method) ((  Il2CppObject* (*) (SafeDictionary_2_t537630836 *, const MethodInfo*))SafeDictionary_2_get_Keys_m3127251543_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Remove(TKey)
extern "C"  bool SafeDictionary_2_Remove_m3302682888_gshared (SafeDictionary_2_t537630836 * __this, ChannelIdentity_t1147162267  ___key0, const MethodInfo* method);
#define SafeDictionary_2_Remove_m3302682888(__this, ___key0, method) ((  bool (*) (SafeDictionary_2_t537630836 *, ChannelIdentity_t1147162267 , const MethodInfo*))SafeDictionary_2_Remove_m3302682888_gshared)(__this, ___key0, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Remove(TKey,TValue&)
extern "C"  bool SafeDictionary_2_Remove_m1992196365_gshared (SafeDictionary_2_t537630836 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define SafeDictionary_2_Remove_m1992196365(__this, ___key0, ___value1, method) ((  bool (*) (SafeDictionary_2_t537630836 *, ChannelIdentity_t1147162267 , Il2CppObject **, const MethodInfo*))SafeDictionary_2_Remove_m1992196365_gshared)(__this, ___key0, ___value1, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::TryRemove(TKey,TValue&)
extern "C"  bool SafeDictionary_2_TryRemove_m315854466_gshared (SafeDictionary_2_t537630836 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define SafeDictionary_2_TryRemove_m315854466(__this, ___key0, ___value1, method) ((  bool (*) (SafeDictionary_2_t537630836 *, ChannelIdentity_t1147162267 , Il2CppObject **, const MethodInfo*))SafeDictionary_2_TryRemove_m315854466_gshared)(__this, ___key0, ___value1, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool SafeDictionary_2_TryGetValue_m187104957_gshared (SafeDictionary_2_t537630836 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define SafeDictionary_2_TryGetValue_m187104957(__this, ___key0, ___value1, method) ((  bool (*) (SafeDictionary_2_t537630836 *, ChannelIdentity_t1147162267 , Il2CppObject **, const MethodInfo*))SafeDictionary_2_TryGetValue_m187104957_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.ICollection`1<TValue> PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Values()
extern "C"  Il2CppObject* SafeDictionary_2_get_Values_m3845365399_gshared (SafeDictionary_2_t537630836 * __this, const MethodInfo* method);
#define SafeDictionary_2_get_Values_m3845365399(__this, method) ((  Il2CppObject* (*) (SafeDictionary_2_t537630836 *, const MethodInfo*))SafeDictionary_2_get_Values_m3845365399_gshared)(__this, method)
// TValue PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * SafeDictionary_2_get_Item_m1670049536_gshared (SafeDictionary_2_t537630836 * __this, ChannelIdentity_t1147162267  ___key0, const MethodInfo* method);
#define SafeDictionary_2_get_Item_m1670049536(__this, ___key0, method) ((  Il2CppObject * (*) (SafeDictionary_2_t537630836 *, ChannelIdentity_t1147162267 , const MethodInfo*))SafeDictionary_2_get_Item_m1670049536_gshared)(__this, ___key0, method)
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::set_Item(TKey,TValue)
extern "C"  void SafeDictionary_2_set_Item_m651937877_gshared (SafeDictionary_2_t537630836 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define SafeDictionary_2_set_Item_m651937877(__this, ___key0, ___value1, method) ((  void (*) (SafeDictionary_2_t537630836 *, ChannelIdentity_t1147162267 , Il2CppObject *, const MethodInfo*))SafeDictionary_2_set_Item_m651937877_gshared)(__this, ___key0, ___value1, method)
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void SafeDictionary_2_Add_m2696016565_gshared (SafeDictionary_2_t537630836 * __this, KeyValuePair_2_t1381843961  ___item0, const MethodInfo* method);
#define SafeDictionary_2_Add_m2696016565(__this, ___item0, method) ((  void (*) (SafeDictionary_2_t537630836 *, KeyValuePair_2_t1381843961 , const MethodInfo*))SafeDictionary_2_Add_m2696016565_gshared)(__this, ___item0, method)
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Clear()
extern "C"  void SafeDictionary_2_Clear_m3344381162_gshared (SafeDictionary_2_t537630836 * __this, const MethodInfo* method);
#define SafeDictionary_2_Clear_m3344381162(__this, method) ((  void (*) (SafeDictionary_2_t537630836 *, const MethodInfo*))SafeDictionary_2_Clear_m3344381162_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SafeDictionary_2_Contains_m1452309491_gshared (SafeDictionary_2_t537630836 * __this, KeyValuePair_2_t1381843961  ___item0, const MethodInfo* method);
#define SafeDictionary_2_Contains_m1452309491(__this, ___item0, method) ((  bool (*) (SafeDictionary_2_t537630836 *, KeyValuePair_2_t1381843961 , const MethodInfo*))SafeDictionary_2_Contains_m1452309491_gshared)(__this, ___item0, method)
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void SafeDictionary_2_CopyTo_m3279940313_gshared (SafeDictionary_2_t537630836 * __this, KeyValuePair_2U5BU5D_t402044932* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define SafeDictionary_2_CopyTo_m3279940313(__this, ___array0, ___arrayIndex1, method) ((  void (*) (SafeDictionary_2_t537630836 *, KeyValuePair_2U5BU5D_t402044932*, int32_t, const MethodInfo*))SafeDictionary_2_CopyTo_m3279940313_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Count()
extern "C"  int32_t SafeDictionary_2_get_Count_m2353695869_gshared (SafeDictionary_2_t537630836 * __this, const MethodInfo* method);
#define SafeDictionary_2_get_Count_m2353695869(__this, method) ((  int32_t (*) (SafeDictionary_2_t537630836 *, const MethodInfo*))SafeDictionary_2_get_Count_m2353695869_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_IsReadOnly()
extern "C"  bool SafeDictionary_2_get_IsReadOnly_m3203599390_gshared (SafeDictionary_2_t537630836 * __this, const MethodInfo* method);
#define SafeDictionary_2_get_IsReadOnly_m3203599390(__this, method) ((  bool (*) (SafeDictionary_2_t537630836 *, const MethodInfo*))SafeDictionary_2_get_IsReadOnly_m3203599390_gshared)(__this, method)
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SafeDictionary_2_Remove_m2963108272_gshared (SafeDictionary_2_t537630836 * __this, KeyValuePair_2_t1381843961  ___item0, const MethodInfo* method);
#define SafeDictionary_2_Remove_m2963108272(__this, ___item0, method) ((  bool (*) (SafeDictionary_2_t537630836 *, KeyValuePair_2_t1381843961 , const MethodInfo*))SafeDictionary_2_Remove_m2963108272_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* SafeDictionary_2_GetEnumerator_m4239269880_gshared (SafeDictionary_2_t537630836 * __this, const MethodInfo* method);
#define SafeDictionary_2_GetEnumerator_m4239269880(__this, method) ((  Il2CppObject* (*) (SafeDictionary_2_t537630836 *, const MethodInfo*))SafeDictionary_2_GetEnumerator_m4239269880_gshared)(__this, method)
// System.Collections.IEnumerator PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * SafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1565768858_gshared (SafeDictionary_2_t537630836 * __this, const MethodInfo* method);
#define SafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1565768858(__this, method) ((  Il2CppObject * (*) (SafeDictionary_2_t537630836 *, const MethodInfo*))SafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1565768858_gshared)(__this, method)
