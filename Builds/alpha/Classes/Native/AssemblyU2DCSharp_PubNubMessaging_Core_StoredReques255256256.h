﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.StoredRequestState/<SetRequestState>c__AnonStorey0
struct  U3CSetRequestStateU3Ec__AnonStorey0_t255256256  : public Il2CppObject
{
public:
	// System.Object PubNubMessaging.Core.StoredRequestState/<SetRequestState>c__AnonStorey0::reqState
	Il2CppObject * ___reqState_0;

public:
	inline static int32_t get_offset_of_reqState_0() { return static_cast<int32_t>(offsetof(U3CSetRequestStateU3Ec__AnonStorey0_t255256256, ___reqState_0)); }
	inline Il2CppObject * get_reqState_0() const { return ___reqState_0; }
	inline Il2CppObject ** get_address_of_reqState_0() { return &___reqState_0; }
	inline void set_reqState_0(Il2CppObject * value)
	{
		___reqState_0 = value;
		Il2CppCodeGenWriteBarrier(&___reqState_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
