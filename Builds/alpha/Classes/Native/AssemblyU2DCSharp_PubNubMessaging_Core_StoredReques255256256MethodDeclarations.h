﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.StoredRequestState/<SetRequestState>c__AnonStorey0
struct U3CSetRequestStateU3Ec__AnonStorey0_t255256256;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void PubNubMessaging.Core.StoredRequestState/<SetRequestState>c__AnonStorey0::.ctor()
extern "C"  void U3CSetRequestStateU3Ec__AnonStorey0__ctor_m2242807603 (U3CSetRequestStateU3Ec__AnonStorey0_t255256256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Core.StoredRequestState/<SetRequestState>c__AnonStorey0::<>m__0(PubNubMessaging.Core.CurrentRequestType,System.Object)
extern "C"  Il2CppObject * U3CSetRequestStateU3Ec__AnonStorey0_U3CU3Em__0_m1469588036 (U3CSetRequestStateU3Ec__AnonStorey0_t255256256 * __this, int32_t ___oldData0, Il2CppObject * ___newData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
