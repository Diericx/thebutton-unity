﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PubNubMessaging.Core.StoredRequestState
struct StoredRequestState_t340008349;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>
struct SafeDictionary_2_t2260115521;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.StoredRequestState
struct  StoredRequestState_t340008349  : public Il2CppObject
{
public:
	// PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object> PubNubMessaging.Core.StoredRequestState::requestStates
	SafeDictionary_2_t2260115521 * ___requestStates_2;

public:
	inline static int32_t get_offset_of_requestStates_2() { return static_cast<int32_t>(offsetof(StoredRequestState_t340008349, ___requestStates_2)); }
	inline SafeDictionary_2_t2260115521 * get_requestStates_2() const { return ___requestStates_2; }
	inline SafeDictionary_2_t2260115521 ** get_address_of_requestStates_2() { return &___requestStates_2; }
	inline void set_requestStates_2(SafeDictionary_2_t2260115521 * value)
	{
		___requestStates_2 = value;
		Il2CppCodeGenWriteBarrier(&___requestStates_2, value);
	}
};

struct StoredRequestState_t340008349_StaticFields
{
public:
	// PubNubMessaging.Core.StoredRequestState modreq(System.Runtime.CompilerServices.IsVolatile) PubNubMessaging.Core.StoredRequestState::instance
	StoredRequestState_t340008349 * ___instance_0;
	// System.Object PubNubMessaging.Core.StoredRequestState::syncRoot
	Il2CppObject * ___syncRoot_1;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(StoredRequestState_t340008349_StaticFields, ___instance_0)); }
	inline StoredRequestState_t340008349 * get_instance_0() const { return ___instance_0; }
	inline StoredRequestState_t340008349 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(StoredRequestState_t340008349 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___instance_0, value);
	}

	inline static int32_t get_offset_of_syncRoot_1() { return static_cast<int32_t>(offsetof(StoredRequestState_t340008349_StaticFields, ___syncRoot_1)); }
	inline Il2CppObject * get_syncRoot_1() const { return ___syncRoot_1; }
	inline Il2CppObject ** get_address_of_syncRoot_1() { return &___syncRoot_1; }
	inline void set_syncRoot_1(Il2CppObject * value)
	{
		___syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier(&___syncRoot_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
