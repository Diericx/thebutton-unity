﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.StoredRequestState
struct StoredRequestState_t340008349;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void PubNubMessaging.Core.StoredRequestState::.ctor()
extern "C"  void StoredRequestState__ctor_m3363016277 (StoredRequestState_t340008349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.StoredRequestState PubNubMessaging.Core.StoredRequestState::get_Instance()
extern "C"  StoredRequestState_t340008349 * StoredRequestState_get_Instance_m847617522 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.StoredRequestState::SetRequestState(PubNubMessaging.Core.CurrentRequestType,System.Object)
extern "C"  void StoredRequestState_SetRequestState_m2256660582 (StoredRequestState_t340008349 * __this, int32_t ___key0, Il2CppObject * ___requestState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Core.StoredRequestState::GetStoredRequestState(PubNubMessaging.Core.CurrentRequestType)
extern "C"  Il2CppObject * StoredRequestState_GetStoredRequestState_m1485328768 (StoredRequestState_t340008349 * __this, int32_t ___aKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.StoredRequestState::.cctor()
extern "C"  void StoredRequestState__cctor_m11349162 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
