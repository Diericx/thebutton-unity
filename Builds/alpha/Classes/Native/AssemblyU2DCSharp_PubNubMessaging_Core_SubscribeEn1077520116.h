﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>
struct List_1_t3108551771;
// PubNubMessaging.Core.TimetokenMetadata
struct TimetokenMetadata_t3476199713;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.SubscribeEnvelope
struct  SubscribeEnvelope_t1077520116  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage> PubNubMessaging.Core.SubscribeEnvelope::<m>k__BackingField
	List_1_t3108551771 * ___U3CmU3Ek__BackingField_0;
	// PubNubMessaging.Core.TimetokenMetadata PubNubMessaging.Core.SubscribeEnvelope::<t>k__BackingField
	TimetokenMetadata_t3476199713 * ___U3CtU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SubscribeEnvelope_t1077520116, ___U3CmU3Ek__BackingField_0)); }
	inline List_1_t3108551771 * get_U3CmU3Ek__BackingField_0() const { return ___U3CmU3Ek__BackingField_0; }
	inline List_1_t3108551771 ** get_address_of_U3CmU3Ek__BackingField_0() { return &___U3CmU3Ek__BackingField_0; }
	inline void set_U3CmU3Ek__BackingField_0(List_1_t3108551771 * value)
	{
		___U3CmU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CtU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SubscribeEnvelope_t1077520116, ___U3CtU3Ek__BackingField_1)); }
	inline TimetokenMetadata_t3476199713 * get_U3CtU3Ek__BackingField_1() const { return ___U3CtU3Ek__BackingField_1; }
	inline TimetokenMetadata_t3476199713 ** get_address_of_U3CtU3Ek__BackingField_1() { return &___U3CtU3Ek__BackingField_1; }
	inline void set_U3CtU3Ek__BackingField_1(TimetokenMetadata_t3476199713 * value)
	{
		___U3CtU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
