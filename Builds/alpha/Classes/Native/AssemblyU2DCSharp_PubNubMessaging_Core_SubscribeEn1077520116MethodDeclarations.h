﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.SubscribeEnvelope
struct SubscribeEnvelope_t1077520116;
// System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>
struct List_1_t3108551771;
// PubNubMessaging.Core.TimetokenMetadata
struct TimetokenMetadata_t3476199713;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_TimetokenMe3476199713.h"

// System.Void PubNubMessaging.Core.SubscribeEnvelope::.ctor()
extern "C"  void SubscribeEnvelope__ctor_m3506078410 (SubscribeEnvelope_t1077520116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage> PubNubMessaging.Core.SubscribeEnvelope::get_m()
extern "C"  List_1_t3108551771 * SubscribeEnvelope_get_m_m1643665871 (SubscribeEnvelope_t1077520116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.SubscribeEnvelope::set_m(System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>)
extern "C"  void SubscribeEnvelope_set_m_m3915000816 (SubscribeEnvelope_t1077520116 * __this, List_1_t3108551771 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.TimetokenMetadata PubNubMessaging.Core.SubscribeEnvelope::get_t()
extern "C"  TimetokenMetadata_t3476199713 * SubscribeEnvelope_get_t_m2061963712 (SubscribeEnvelope_t1077520116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.SubscribeEnvelope::set_t(PubNubMessaging.Core.TimetokenMetadata)
extern "C"  void SubscribeEnvelope_set_t_m4104280367 (SubscribeEnvelope_t1077520116 * __this, TimetokenMetadata_t3476199713 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage> PubNubMessaging.Core.SubscribeEnvelope::get_Messages()
extern "C"  List_1_t3108551771 * SubscribeEnvelope_get_Messages_m311804684 (SubscribeEnvelope_t1077520116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.SubscribeEnvelope::set_Messages(System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>)
extern "C"  void SubscribeEnvelope_set_Messages_m3757763985 (SubscribeEnvelope_t1077520116 * __this, List_1_t3108551771 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.TimetokenMetadata PubNubMessaging.Core.SubscribeEnvelope::get_TimetokenMeta()
extern "C"  TimetokenMetadata_t3476199713 * SubscribeEnvelope_get_TimetokenMeta_m2409814771 (SubscribeEnvelope_t1077520116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.SubscribeEnvelope::set_TimetokenMeta(PubNubMessaging.Core.TimetokenMetadata)
extern "C"  void SubscribeEnvelope_set_TimetokenMeta_m2658713010 (SubscribeEnvelope_t1077520116 * __this, TimetokenMetadata_t3476199713 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
