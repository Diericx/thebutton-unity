﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Core.TimetokenMetadata
struct TimetokenMetadata_t3476199713;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.SubscribeMessage
struct  SubscribeMessage_t3739430639  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Core.SubscribeMessage::<a>k__BackingField
	String_t* ___U3CaU3Ek__BackingField_0;
	// System.String PubNubMessaging.Core.SubscribeMessage::<b>k__BackingField
	String_t* ___U3CbU3Ek__BackingField_1;
	// System.String PubNubMessaging.Core.SubscribeMessage::<c>k__BackingField
	String_t* ___U3CcU3Ek__BackingField_2;
	// System.Object PubNubMessaging.Core.SubscribeMessage::<d>k__BackingField
	Il2CppObject * ___U3CdU3Ek__BackingField_3;
	// System.String PubNubMessaging.Core.SubscribeMessage::<f>k__BackingField
	String_t* ___U3CfU3Ek__BackingField_4;
	// System.String PubNubMessaging.Core.SubscribeMessage::<i>k__BackingField
	String_t* ___U3CiU3Ek__BackingField_5;
	// System.String PubNubMessaging.Core.SubscribeMessage::<k>k__BackingField
	String_t* ___U3CkU3Ek__BackingField_6;
	// System.Int64 PubNubMessaging.Core.SubscribeMessage::<s>k__BackingField
	int64_t ___U3CsU3Ek__BackingField_7;
	// PubNubMessaging.Core.TimetokenMetadata PubNubMessaging.Core.SubscribeMessage::<o>k__BackingField
	TimetokenMetadata_t3476199713 * ___U3CoU3Ek__BackingField_8;
	// PubNubMessaging.Core.TimetokenMetadata PubNubMessaging.Core.SubscribeMessage::<p>k__BackingField
	TimetokenMetadata_t3476199713 * ___U3CpU3Ek__BackingField_9;
	// System.Object PubNubMessaging.Core.SubscribeMessage::<u>k__BackingField
	Il2CppObject * ___U3CuU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CaU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SubscribeMessage_t3739430639, ___U3CaU3Ek__BackingField_0)); }
	inline String_t* get_U3CaU3Ek__BackingField_0() const { return ___U3CaU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CaU3Ek__BackingField_0() { return &___U3CaU3Ek__BackingField_0; }
	inline void set_U3CaU3Ek__BackingField_0(String_t* value)
	{
		___U3CaU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CaU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CbU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SubscribeMessage_t3739430639, ___U3CbU3Ek__BackingField_1)); }
	inline String_t* get_U3CbU3Ek__BackingField_1() const { return ___U3CbU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CbU3Ek__BackingField_1() { return &___U3CbU3Ek__BackingField_1; }
	inline void set_U3CbU3Ek__BackingField_1(String_t* value)
	{
		___U3CbU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CcU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubscribeMessage_t3739430639, ___U3CcU3Ek__BackingField_2)); }
	inline String_t* get_U3CcU3Ek__BackingField_2() const { return ___U3CcU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CcU3Ek__BackingField_2() { return &___U3CcU3Ek__BackingField_2; }
	inline void set_U3CcU3Ek__BackingField_2(String_t* value)
	{
		___U3CcU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubscribeMessage_t3739430639, ___U3CdU3Ek__BackingField_3)); }
	inline Il2CppObject * get_U3CdU3Ek__BackingField_3() const { return ___U3CdU3Ek__BackingField_3; }
	inline Il2CppObject ** get_address_of_U3CdU3Ek__BackingField_3() { return &___U3CdU3Ek__BackingField_3; }
	inline void set_U3CdU3Ek__BackingField_3(Il2CppObject * value)
	{
		___U3CdU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CfU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SubscribeMessage_t3739430639, ___U3CfU3Ek__BackingField_4)); }
	inline String_t* get_U3CfU3Ek__BackingField_4() const { return ___U3CfU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CfU3Ek__BackingField_4() { return &___U3CfU3Ek__BackingField_4; }
	inline void set_U3CfU3Ek__BackingField_4(String_t* value)
	{
		___U3CfU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CiU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SubscribeMessage_t3739430639, ___U3CiU3Ek__BackingField_5)); }
	inline String_t* get_U3CiU3Ek__BackingField_5() const { return ___U3CiU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CiU3Ek__BackingField_5() { return &___U3CiU3Ek__BackingField_5; }
	inline void set_U3CiU3Ek__BackingField_5(String_t* value)
	{
		___U3CiU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CiU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CkU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SubscribeMessage_t3739430639, ___U3CkU3Ek__BackingField_6)); }
	inline String_t* get_U3CkU3Ek__BackingField_6() const { return ___U3CkU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CkU3Ek__BackingField_6() { return &___U3CkU3Ek__BackingField_6; }
	inline void set_U3CkU3Ek__BackingField_6(String_t* value)
	{
		___U3CkU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CkU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(SubscribeMessage_t3739430639, ___U3CsU3Ek__BackingField_7)); }
	inline int64_t get_U3CsU3Ek__BackingField_7() const { return ___U3CsU3Ek__BackingField_7; }
	inline int64_t* get_address_of_U3CsU3Ek__BackingField_7() { return &___U3CsU3Ek__BackingField_7; }
	inline void set_U3CsU3Ek__BackingField_7(int64_t value)
	{
		___U3CsU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CoU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(SubscribeMessage_t3739430639, ___U3CoU3Ek__BackingField_8)); }
	inline TimetokenMetadata_t3476199713 * get_U3CoU3Ek__BackingField_8() const { return ___U3CoU3Ek__BackingField_8; }
	inline TimetokenMetadata_t3476199713 ** get_address_of_U3CoU3Ek__BackingField_8() { return &___U3CoU3Ek__BackingField_8; }
	inline void set_U3CoU3Ek__BackingField_8(TimetokenMetadata_t3476199713 * value)
	{
		___U3CoU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CoU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CpU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(SubscribeMessage_t3739430639, ___U3CpU3Ek__BackingField_9)); }
	inline TimetokenMetadata_t3476199713 * get_U3CpU3Ek__BackingField_9() const { return ___U3CpU3Ek__BackingField_9; }
	inline TimetokenMetadata_t3476199713 ** get_address_of_U3CpU3Ek__BackingField_9() { return &___U3CpU3Ek__BackingField_9; }
	inline void set_U3CpU3Ek__BackingField_9(TimetokenMetadata_t3476199713 * value)
	{
		___U3CpU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CuU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(SubscribeMessage_t3739430639, ___U3CuU3Ek__BackingField_10)); }
	inline Il2CppObject * get_U3CuU3Ek__BackingField_10() const { return ___U3CuU3Ek__BackingField_10; }
	inline Il2CppObject ** get_address_of_U3CuU3Ek__BackingField_10() { return &___U3CuU3Ek__BackingField_10; }
	inline void set_U3CuU3Ek__BackingField_10(Il2CppObject * value)
	{
		___U3CuU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuU3Ek__BackingField_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
