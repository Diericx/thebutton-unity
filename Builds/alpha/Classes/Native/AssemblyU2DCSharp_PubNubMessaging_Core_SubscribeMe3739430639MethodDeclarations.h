﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.SubscribeMessage
struct SubscribeMessage_t3739430639;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Core.TimetokenMetadata
struct TimetokenMetadata_t3476199713;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_TimetokenMe3476199713.h"

// System.Void PubNubMessaging.Core.SubscribeMessage::.ctor(System.String,System.String,System.String,System.Object,System.String,System.String,System.String,System.Int64,PubNubMessaging.Core.TimetokenMetadata,PubNubMessaging.Core.TimetokenMetadata,System.Object)
extern "C"  void SubscribeMessage__ctor_m3074176917 (SubscribeMessage_t3739430639 * __this, String_t* ___shard0, String_t* ___subscriptionMatch1, String_t* ___channel2, Il2CppObject * ___payload3, String_t* ___flags4, String_t* ___issuingClientId5, String_t* ___subscribeKey6, int64_t ___sequenceNumber7, TimetokenMetadata_t3476199713 * ___originatingTimetoken8, TimetokenMetadata_t3476199713 * ___publishMetadata9, Il2CppObject * ___userMetadata10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.SubscribeMessage::get_a()
extern "C"  String_t* SubscribeMessage_get_a_m577396760 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.SubscribeMessage::set_a(System.String)
extern "C"  void SubscribeMessage_set_a_m1978619545 (SubscribeMessage_t3739430639 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.SubscribeMessage::get_b()
extern "C"  String_t* SubscribeMessage_get_b_m153909257 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.SubscribeMessage::set_b(System.String)
extern "C"  void SubscribeMessage_set_b_m1828654026 (SubscribeMessage_t3739430639 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.SubscribeMessage::get_c()
extern "C"  String_t* SubscribeMessage_get_c_m295071758 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.SubscribeMessage::set_c(System.String)
extern "C"  void SubscribeMessage_set_c_m3675708175 (SubscribeMessage_t3739430639 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Core.SubscribeMessage::get_d()
extern "C"  Il2CppObject * SubscribeMessage_get_d_m1903150803 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.SubscribeMessage::set_d(System.Object)
extern "C"  void SubscribeMessage_set_d_m3374117646 (SubscribeMessage_t3739430639 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.SubscribeMessage::get_f()
extern "C"  String_t* SubscribeMessage_get_f_m3884226549 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.SubscribeMessage::set_f(System.String)
extern "C"  void SubscribeMessage_set_f_m1577778142 (SubscribeMessage_t3739430639 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.SubscribeMessage::get_i()
extern "C"  String_t* SubscribeMessage_get_i_m2419157488 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.SubscribeMessage::set_i(System.String)
extern "C"  void SubscribeMessage_set_i_m1195851969 (SubscribeMessage_t3739430639 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.SubscribeMessage::get_k()
extern "C"  String_t* SubscribeMessage_get_k_m2136832486 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.SubscribeMessage::set_k(System.String)
extern "C"  void SubscribeMessage_set_k_m2892940599 (SubscribeMessage_t3739430639 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 PubNubMessaging.Core.SubscribeMessage::get_s()
extern "C"  int64_t SubscribeMessage_get_s_m1306376516 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.SubscribeMessage::set_s(System.Int64)
extern "C"  void SubscribeMessage_set_s_m2128897383 (SubscribeMessage_t3739430639 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.TimetokenMetadata PubNubMessaging.Core.SubscribeMessage::get_o()
extern "C"  TimetokenMetadata_t3476199713 * SubscribeMessage_get_o_m803289696 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.SubscribeMessage::set_o(PubNubMessaging.Core.TimetokenMetadata)
extern "C"  void SubscribeMessage_set_o_m4289732129 (SubscribeMessage_t3739430639 * __this, TimetokenMetadata_t3476199713 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.TimetokenMetadata PubNubMessaging.Core.SubscribeMessage::get_p()
extern "C"  TimetokenMetadata_t3476199713 * SubscribeMessage_get_p_m803288949 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.SubscribeMessage::set_p(PubNubMessaging.Core.TimetokenMetadata)
extern "C"  void SubscribeMessage_set_p_m2916038014 (SubscribeMessage_t3739430639 * __this, TimetokenMetadata_t3476199713 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Core.SubscribeMessage::get_u()
extern "C"  Il2CppObject * SubscribeMessage_get_u_m2615869854 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.SubscribeMessage::set_u(System.Object)
extern "C"  void SubscribeMessage_set_u_m2049281593 (SubscribeMessage_t3739430639 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.SubscribeMessage::get_Shard()
extern "C"  String_t* SubscribeMessage_get_Shard_m581147279 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.SubscribeMessage::get_SubscriptionMatch()
extern "C"  String_t* SubscribeMessage_get_SubscriptionMatch_m2235369895 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.SubscribeMessage::get_Channel()
extern "C"  String_t* SubscribeMessage_get_Channel_m3009381108 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Core.SubscribeMessage::get_Payload()
extern "C"  Il2CppObject * SubscribeMessage_get_Payload_m3546733601 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.SubscribeMessage::get_Flags()
extern "C"  String_t* SubscribeMessage_get_Flags_m3049020520 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.SubscribeMessage::get_IssuingClientId()
extern "C"  String_t* SubscribeMessage_get_IssuingClientId_m3169670807 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.SubscribeMessage::get_SubscribeKey()
extern "C"  String_t* SubscribeMessage_get_SubscribeKey_m1132915372 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 PubNubMessaging.Core.SubscribeMessage::get_SequenceNumber()
extern "C"  int64_t SubscribeMessage_get_SequenceNumber_m1254947455 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.TimetokenMetadata PubNubMessaging.Core.SubscribeMessage::get_OriginatingTimetoken()
extern "C"  TimetokenMetadata_t3476199713 * SubscribeMessage_get_OriginatingTimetoken_m1382359136 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.TimetokenMetadata PubNubMessaging.Core.SubscribeMessage::get_PublishTimetokenMetadata()
extern "C"  TimetokenMetadata_t3476199713 * SubscribeMessage_get_PublishTimetokenMetadata_m2177206287 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Core.SubscribeMessage::get_UserMetadata()
extern "C"  Il2CppObject * SubscribeMessage_get_UserMetadata_m2590457051 (SubscribeMessage_t3739430639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
