﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PubNubMessaging.Core.Subscription
struct Subscription_t3881812953;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>
struct List_1_t2635275738;
// System.String
struct String_t;
// PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>
struct SafeDictionary_2_t2691085430;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.Subscription
struct  Subscription_t3881812953  : public Il2CppObject
{
public:
	// System.Boolean PubNubMessaging.Core.Subscription::<HasChannelGroups>k__BackingField
	bool ___U3CHasChannelGroupsU3Ek__BackingField_2;
	// System.Boolean PubNubMessaging.Core.Subscription::<HasPresenceChannels>k__BackingField
	bool ___U3CHasPresenceChannelsU3Ek__BackingField_3;
	// System.Boolean PubNubMessaging.Core.Subscription::<HasChannelsOrChannelGroups>k__BackingField
	bool ___U3CHasChannelsOrChannelGroupsU3Ek__BackingField_4;
	// System.Boolean PubNubMessaging.Core.Subscription::<HasChannels>k__BackingField
	bool ___U3CHasChannelsU3Ek__BackingField_5;
	// System.Int32 PubNubMessaging.Core.Subscription::<CurrentSubscribedChannelsCount>k__BackingField
	int32_t ___U3CCurrentSubscribedChannelsCountU3Ek__BackingField_6;
	// System.Int32 PubNubMessaging.Core.Subscription::<CurrentSubscribedChannelGroupsCount>k__BackingField
	int32_t ___U3CCurrentSubscribedChannelGroupsCountU3Ek__BackingField_7;
	// System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity> PubNubMessaging.Core.Subscription::<ChannelsAndChannelGroupsAwaitingConnectCallback>k__BackingField
	List_1_t2635275738 * ___U3CChannelsAndChannelGroupsAwaitingConnectCallbackU3Ek__BackingField_8;
	// System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity> PubNubMessaging.Core.Subscription::<AllChannels>k__BackingField
	List_1_t2635275738 * ___U3CAllChannelsU3Ek__BackingField_9;
	// System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity> PubNubMessaging.Core.Subscription::<AllChannelGroups>k__BackingField
	List_1_t2635275738 * ___U3CAllChannelGroupsU3Ek__BackingField_10;
	// System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity> PubNubMessaging.Core.Subscription::<AllSubscribedChannelsAndChannelGroups>k__BackingField
	List_1_t2635275738 * ___U3CAllSubscribedChannelsAndChannelGroupsU3Ek__BackingField_11;
	// System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity> PubNubMessaging.Core.Subscription::<AllPresenceChannelsOrChannelGroups>k__BackingField
	List_1_t2635275738 * ___U3CAllPresenceChannelsOrChannelGroupsU3Ek__BackingField_12;
	// System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity> PubNubMessaging.Core.Subscription::<AllNonPresenceChannelsOrChannelGroups>k__BackingField
	List_1_t2635275738 * ___U3CAllNonPresenceChannelsOrChannelGroupsU3Ek__BackingField_13;
	// System.String PubNubMessaging.Core.Subscription::<CompiledUserState>k__BackingField
	String_t* ___U3CCompiledUserStateU3Ek__BackingField_14;
	// System.Boolean PubNubMessaging.Core.Subscription::<ConnectCallbackSent>k__BackingField
	bool ___U3CConnectCallbackSentU3Ek__BackingField_15;
	// PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters> PubNubMessaging.Core.Subscription::channelEntitiesDictionary
	SafeDictionary_2_t2691085430 * ___channelEntitiesDictionary_16;

public:
	inline static int32_t get_offset_of_U3CHasChannelGroupsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Subscription_t3881812953, ___U3CHasChannelGroupsU3Ek__BackingField_2)); }
	inline bool get_U3CHasChannelGroupsU3Ek__BackingField_2() const { return ___U3CHasChannelGroupsU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CHasChannelGroupsU3Ek__BackingField_2() { return &___U3CHasChannelGroupsU3Ek__BackingField_2; }
	inline void set_U3CHasChannelGroupsU3Ek__BackingField_2(bool value)
	{
		___U3CHasChannelGroupsU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CHasPresenceChannelsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Subscription_t3881812953, ___U3CHasPresenceChannelsU3Ek__BackingField_3)); }
	inline bool get_U3CHasPresenceChannelsU3Ek__BackingField_3() const { return ___U3CHasPresenceChannelsU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CHasPresenceChannelsU3Ek__BackingField_3() { return &___U3CHasPresenceChannelsU3Ek__BackingField_3; }
	inline void set_U3CHasPresenceChannelsU3Ek__BackingField_3(bool value)
	{
		___U3CHasPresenceChannelsU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CHasChannelsOrChannelGroupsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Subscription_t3881812953, ___U3CHasChannelsOrChannelGroupsU3Ek__BackingField_4)); }
	inline bool get_U3CHasChannelsOrChannelGroupsU3Ek__BackingField_4() const { return ___U3CHasChannelsOrChannelGroupsU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CHasChannelsOrChannelGroupsU3Ek__BackingField_4() { return &___U3CHasChannelsOrChannelGroupsU3Ek__BackingField_4; }
	inline void set_U3CHasChannelsOrChannelGroupsU3Ek__BackingField_4(bool value)
	{
		___U3CHasChannelsOrChannelGroupsU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CHasChannelsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Subscription_t3881812953, ___U3CHasChannelsU3Ek__BackingField_5)); }
	inline bool get_U3CHasChannelsU3Ek__BackingField_5() const { return ___U3CHasChannelsU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CHasChannelsU3Ek__BackingField_5() { return &___U3CHasChannelsU3Ek__BackingField_5; }
	inline void set_U3CHasChannelsU3Ek__BackingField_5(bool value)
	{
		___U3CHasChannelsU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentSubscribedChannelsCountU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Subscription_t3881812953, ___U3CCurrentSubscribedChannelsCountU3Ek__BackingField_6)); }
	inline int32_t get_U3CCurrentSubscribedChannelsCountU3Ek__BackingField_6() const { return ___U3CCurrentSubscribedChannelsCountU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CCurrentSubscribedChannelsCountU3Ek__BackingField_6() { return &___U3CCurrentSubscribedChannelsCountU3Ek__BackingField_6; }
	inline void set_U3CCurrentSubscribedChannelsCountU3Ek__BackingField_6(int32_t value)
	{
		___U3CCurrentSubscribedChannelsCountU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentSubscribedChannelGroupsCountU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Subscription_t3881812953, ___U3CCurrentSubscribedChannelGroupsCountU3Ek__BackingField_7)); }
	inline int32_t get_U3CCurrentSubscribedChannelGroupsCountU3Ek__BackingField_7() const { return ___U3CCurrentSubscribedChannelGroupsCountU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CCurrentSubscribedChannelGroupsCountU3Ek__BackingField_7() { return &___U3CCurrentSubscribedChannelGroupsCountU3Ek__BackingField_7; }
	inline void set_U3CCurrentSubscribedChannelGroupsCountU3Ek__BackingField_7(int32_t value)
	{
		___U3CCurrentSubscribedChannelGroupsCountU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CChannelsAndChannelGroupsAwaitingConnectCallbackU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Subscription_t3881812953, ___U3CChannelsAndChannelGroupsAwaitingConnectCallbackU3Ek__BackingField_8)); }
	inline List_1_t2635275738 * get_U3CChannelsAndChannelGroupsAwaitingConnectCallbackU3Ek__BackingField_8() const { return ___U3CChannelsAndChannelGroupsAwaitingConnectCallbackU3Ek__BackingField_8; }
	inline List_1_t2635275738 ** get_address_of_U3CChannelsAndChannelGroupsAwaitingConnectCallbackU3Ek__BackingField_8() { return &___U3CChannelsAndChannelGroupsAwaitingConnectCallbackU3Ek__BackingField_8; }
	inline void set_U3CChannelsAndChannelGroupsAwaitingConnectCallbackU3Ek__BackingField_8(List_1_t2635275738 * value)
	{
		___U3CChannelsAndChannelGroupsAwaitingConnectCallbackU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CChannelsAndChannelGroupsAwaitingConnectCallbackU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CAllChannelsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Subscription_t3881812953, ___U3CAllChannelsU3Ek__BackingField_9)); }
	inline List_1_t2635275738 * get_U3CAllChannelsU3Ek__BackingField_9() const { return ___U3CAllChannelsU3Ek__BackingField_9; }
	inline List_1_t2635275738 ** get_address_of_U3CAllChannelsU3Ek__BackingField_9() { return &___U3CAllChannelsU3Ek__BackingField_9; }
	inline void set_U3CAllChannelsU3Ek__BackingField_9(List_1_t2635275738 * value)
	{
		___U3CAllChannelsU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAllChannelsU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CAllChannelGroupsU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Subscription_t3881812953, ___U3CAllChannelGroupsU3Ek__BackingField_10)); }
	inline List_1_t2635275738 * get_U3CAllChannelGroupsU3Ek__BackingField_10() const { return ___U3CAllChannelGroupsU3Ek__BackingField_10; }
	inline List_1_t2635275738 ** get_address_of_U3CAllChannelGroupsU3Ek__BackingField_10() { return &___U3CAllChannelGroupsU3Ek__BackingField_10; }
	inline void set_U3CAllChannelGroupsU3Ek__BackingField_10(List_1_t2635275738 * value)
	{
		___U3CAllChannelGroupsU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAllChannelGroupsU3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of_U3CAllSubscribedChannelsAndChannelGroupsU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Subscription_t3881812953, ___U3CAllSubscribedChannelsAndChannelGroupsU3Ek__BackingField_11)); }
	inline List_1_t2635275738 * get_U3CAllSubscribedChannelsAndChannelGroupsU3Ek__BackingField_11() const { return ___U3CAllSubscribedChannelsAndChannelGroupsU3Ek__BackingField_11; }
	inline List_1_t2635275738 ** get_address_of_U3CAllSubscribedChannelsAndChannelGroupsU3Ek__BackingField_11() { return &___U3CAllSubscribedChannelsAndChannelGroupsU3Ek__BackingField_11; }
	inline void set_U3CAllSubscribedChannelsAndChannelGroupsU3Ek__BackingField_11(List_1_t2635275738 * value)
	{
		___U3CAllSubscribedChannelsAndChannelGroupsU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAllSubscribedChannelsAndChannelGroupsU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CAllPresenceChannelsOrChannelGroupsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Subscription_t3881812953, ___U3CAllPresenceChannelsOrChannelGroupsU3Ek__BackingField_12)); }
	inline List_1_t2635275738 * get_U3CAllPresenceChannelsOrChannelGroupsU3Ek__BackingField_12() const { return ___U3CAllPresenceChannelsOrChannelGroupsU3Ek__BackingField_12; }
	inline List_1_t2635275738 ** get_address_of_U3CAllPresenceChannelsOrChannelGroupsU3Ek__BackingField_12() { return &___U3CAllPresenceChannelsOrChannelGroupsU3Ek__BackingField_12; }
	inline void set_U3CAllPresenceChannelsOrChannelGroupsU3Ek__BackingField_12(List_1_t2635275738 * value)
	{
		___U3CAllPresenceChannelsOrChannelGroupsU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAllPresenceChannelsOrChannelGroupsU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3CAllNonPresenceChannelsOrChannelGroupsU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Subscription_t3881812953, ___U3CAllNonPresenceChannelsOrChannelGroupsU3Ek__BackingField_13)); }
	inline List_1_t2635275738 * get_U3CAllNonPresenceChannelsOrChannelGroupsU3Ek__BackingField_13() const { return ___U3CAllNonPresenceChannelsOrChannelGroupsU3Ek__BackingField_13; }
	inline List_1_t2635275738 ** get_address_of_U3CAllNonPresenceChannelsOrChannelGroupsU3Ek__BackingField_13() { return &___U3CAllNonPresenceChannelsOrChannelGroupsU3Ek__BackingField_13; }
	inline void set_U3CAllNonPresenceChannelsOrChannelGroupsU3Ek__BackingField_13(List_1_t2635275738 * value)
	{
		___U3CAllNonPresenceChannelsOrChannelGroupsU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAllNonPresenceChannelsOrChannelGroupsU3Ek__BackingField_13, value);
	}

	inline static int32_t get_offset_of_U3CCompiledUserStateU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Subscription_t3881812953, ___U3CCompiledUserStateU3Ek__BackingField_14)); }
	inline String_t* get_U3CCompiledUserStateU3Ek__BackingField_14() const { return ___U3CCompiledUserStateU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CCompiledUserStateU3Ek__BackingField_14() { return &___U3CCompiledUserStateU3Ek__BackingField_14; }
	inline void set_U3CCompiledUserStateU3Ek__BackingField_14(String_t* value)
	{
		___U3CCompiledUserStateU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCompiledUserStateU3Ek__BackingField_14, value);
	}

	inline static int32_t get_offset_of_U3CConnectCallbackSentU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Subscription_t3881812953, ___U3CConnectCallbackSentU3Ek__BackingField_15)); }
	inline bool get_U3CConnectCallbackSentU3Ek__BackingField_15() const { return ___U3CConnectCallbackSentU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CConnectCallbackSentU3Ek__BackingField_15() { return &___U3CConnectCallbackSentU3Ek__BackingField_15; }
	inline void set_U3CConnectCallbackSentU3Ek__BackingField_15(bool value)
	{
		___U3CConnectCallbackSentU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_channelEntitiesDictionary_16() { return static_cast<int32_t>(offsetof(Subscription_t3881812953, ___channelEntitiesDictionary_16)); }
	inline SafeDictionary_2_t2691085430 * get_channelEntitiesDictionary_16() const { return ___channelEntitiesDictionary_16; }
	inline SafeDictionary_2_t2691085430 ** get_address_of_channelEntitiesDictionary_16() { return &___channelEntitiesDictionary_16; }
	inline void set_channelEntitiesDictionary_16(SafeDictionary_2_t2691085430 * value)
	{
		___channelEntitiesDictionary_16 = value;
		Il2CppCodeGenWriteBarrier(&___channelEntitiesDictionary_16, value);
	}
};

struct Subscription_t3881812953_StaticFields
{
public:
	// PubNubMessaging.Core.Subscription modreq(System.Runtime.CompilerServices.IsVolatile) PubNubMessaging.Core.Subscription::instance
	Subscription_t3881812953 * ___instance_0;
	// System.Object PubNubMessaging.Core.Subscription::syncRoot
	Il2CppObject * ___syncRoot_1;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(Subscription_t3881812953_StaticFields, ___instance_0)); }
	inline Subscription_t3881812953 * get_instance_0() const { return ___instance_0; }
	inline Subscription_t3881812953 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(Subscription_t3881812953 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier(&___instance_0, value);
	}

	inline static int32_t get_offset_of_syncRoot_1() { return static_cast<int32_t>(offsetof(Subscription_t3881812953_StaticFields, ___syncRoot_1)); }
	inline Il2CppObject * get_syncRoot_1() const { return ___syncRoot_1; }
	inline Il2CppObject ** get_address_of_syncRoot_1() { return &___syncRoot_1; }
	inline void set_syncRoot_1(Il2CppObject * value)
	{
		___syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier(&___syncRoot_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
