﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.Subscription
struct Subscription_t3881812953;
// System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>
struct List_1_t2635275738;
// System.String
struct String_t;
// PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>
struct SafeDictionary_2_t2691085430;
// PubNubMessaging.Core.ChannelEntity
struct ChannelEntity_t3266154606;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelEnti3266154606.h"

// System.Void PubNubMessaging.Core.Subscription::.ctor()
extern "C"  void Subscription__ctor_m4117761655 (Subscription_t3881812953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.Subscription PubNubMessaging.Core.Subscription::get_Instance()
extern "C"  Subscription_t3881812953 * Subscription_get_Instance_m1151506994 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Subscription::get_HasChannelGroups()
extern "C"  bool Subscription_get_HasChannelGroups_m3535973127 (Subscription_t3881812953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Subscription::set_HasChannelGroups(System.Boolean)
extern "C"  void Subscription_set_HasChannelGroups_m684171512 (Subscription_t3881812953 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Subscription::get_HasPresenceChannels()
extern "C"  bool Subscription_get_HasPresenceChannels_m983334045 (Subscription_t3881812953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Subscription::set_HasPresenceChannels(System.Boolean)
extern "C"  void Subscription_set_HasPresenceChannels_m3513446472 (Subscription_t3881812953 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Subscription::get_HasChannelsOrChannelGroups()
extern "C"  bool Subscription_get_HasChannelsOrChannelGroups_m2061089368 (Subscription_t3881812953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Subscription::set_HasChannelsOrChannelGroups(System.Boolean)
extern "C"  void Subscription_set_HasChannelsOrChannelGroups_m4019045153 (Subscription_t3881812953 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Subscription::get_HasChannels()
extern "C"  bool Subscription_get_HasChannels_m4204422014 (Subscription_t3881812953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Subscription::set_HasChannels(System.Boolean)
extern "C"  void Subscription_set_HasChannels_m2621900855 (Subscription_t3881812953 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.Subscription::get_CurrentSubscribedChannelsCount()
extern "C"  int32_t Subscription_get_CurrentSubscribedChannelsCount_m1055852880 (Subscription_t3881812953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Subscription::set_CurrentSubscribedChannelsCount(System.Int32)
extern "C"  void Subscription_set_CurrentSubscribedChannelsCount_m2673772153 (Subscription_t3881812953 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.Subscription::get_CurrentSubscribedChannelGroupsCount()
extern "C"  int32_t Subscription_get_CurrentSubscribedChannelGroupsCount_m4002844989 (Subscription_t3881812953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Subscription::set_CurrentSubscribedChannelGroupsCount(System.Int32)
extern "C"  void Subscription_set_CurrentSubscribedChannelGroupsCount_m4209202518 (Subscription_t3881812953 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity> PubNubMessaging.Core.Subscription::get_ChannelsAndChannelGroupsAwaitingConnectCallback()
extern "C"  List_1_t2635275738 * Subscription_get_ChannelsAndChannelGroupsAwaitingConnectCallback_m3146648321 (Subscription_t3881812953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Subscription::set_ChannelsAndChannelGroupsAwaitingConnectCallback(System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>)
extern "C"  void Subscription_set_ChannelsAndChannelGroupsAwaitingConnectCallback_m4140840576 (Subscription_t3881812953 * __this, List_1_t2635275738 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity> PubNubMessaging.Core.Subscription::get_AllChannels()
extern "C"  List_1_t2635275738 * Subscription_get_AllChannels_m1089200469 (Subscription_t3881812953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Subscription::set_AllChannels(System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>)
extern "C"  void Subscription_set_AllChannels_m3526081952 (Subscription_t3881812953 * __this, List_1_t2635275738 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity> PubNubMessaging.Core.Subscription::get_AllChannelGroups()
extern "C"  List_1_t2635275738 * Subscription_get_AllChannelGroups_m2913218110 (Subscription_t3881812953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Subscription::set_AllChannelGroups(System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>)
extern "C"  void Subscription_set_AllChannelGroups_m2354955025 (Subscription_t3881812953 * __this, List_1_t2635275738 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity> PubNubMessaging.Core.Subscription::get_AllSubscribedChannelsAndChannelGroups()
extern "C"  List_1_t2635275738 * Subscription_get_AllSubscribedChannelsAndChannelGroups_m2535874653 (Subscription_t3881812953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Subscription::set_AllSubscribedChannelsAndChannelGroups(System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>)
extern "C"  void Subscription_set_AllSubscribedChannelsAndChannelGroups_m1441075224 (Subscription_t3881812953 * __this, List_1_t2635275738 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity> PubNubMessaging.Core.Subscription::get_AllPresenceChannelsOrChannelGroups()
extern "C"  List_1_t2635275738 * Subscription_get_AllPresenceChannelsOrChannelGroups_m2141519474 (Subscription_t3881812953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Subscription::set_AllPresenceChannelsOrChannelGroups(System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>)
extern "C"  void Subscription_set_AllPresenceChannelsOrChannelGroups_m1058532661 (Subscription_t3881812953 * __this, List_1_t2635275738 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity> PubNubMessaging.Core.Subscription::get_AllNonPresenceChannelsOrChannelGroups()
extern "C"  List_1_t2635275738 * Subscription_get_AllNonPresenceChannelsOrChannelGroups_m4054583737 (Subscription_t3881812953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Subscription::set_AllNonPresenceChannelsOrChannelGroups(System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>)
extern "C"  void Subscription_set_AllNonPresenceChannelsOrChannelGroups_m1938746454 (Subscription_t3881812953 * __this, List_1_t2635275738 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.Subscription::get_CompiledUserState()
extern "C"  String_t* Subscription_get_CompiledUserState_m1750862600 (Subscription_t3881812953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Subscription::set_CompiledUserState(System.String)
extern "C"  void Subscription_set_CompiledUserState_m1262206491 (Subscription_t3881812953 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Subscription::get_ConnectCallbackSent()
extern "C"  bool Subscription_get_ConnectCallbackSent_m610019693 (Subscription_t3881812953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Subscription::set_ConnectCallbackSent(System.Boolean)
extern "C"  void Subscription_set_ConnectCallbackSent_m3680889538 (Subscription_t3881812953 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters> PubNubMessaging.Core.Subscription::get_ChannelEntitiesDictionary()
extern "C"  SafeDictionary_2_t2691085430 * Subscription_get_ChannelEntitiesDictionary_m951426553 (Subscription_t3881812953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Subscription::Add(PubNubMessaging.Core.ChannelEntity,System.Boolean)
extern "C"  void Subscription_Add_m3815914984 (Subscription_t3881812953 * __this, ChannelEntity_t3266154606 * ___channelEntity0, bool ___reset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Subscription::Add(System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>)
extern "C"  void Subscription_Add_m4052548239 (Subscription_t3881812953 * __this, List_1_t2635275738 * ___channelEntities0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Subscription::Delete(PubNubMessaging.Core.ChannelEntity)
extern "C"  bool Subscription_Delete_m1932280037 (Subscription_t3881812953 * __this, ChannelEntity_t3266154606 * ___channelEntity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Subscription::ResetChannelsAndChannelGroupsAndBuildState()
extern "C"  void Subscription_ResetChannelsAndChannelGroupsAndBuildState_m14413550 (Subscription_t3881812953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> PubNubMessaging.Core.Subscription::EditUserState(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Boolean)
extern "C"  Dictionary_2_t309261261 * Subscription_EditUserState_m2995768168 (Subscription_t3881812953 * __this, Dictionary_2_t309261261 * ___newUserState0, Dictionary_2_t309261261 * ___oldUserState1, bool ___edit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Subscription::UpdateOrAddUserStateOfEntity(PubNubMessaging.Core.ChannelEntity&,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Boolean)
extern "C"  bool Subscription_UpdateOrAddUserStateOfEntity_m1984835043 (Subscription_t3881812953 * __this, ChannelEntity_t3266154606 ** ___channelEntity0, Dictionary_2_t309261261 * ___userState1, bool ___edit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Subscription::UpdateIsAwaitingConnectCallbacksOfEntity(System.Collections.Generic.List`1<PubNubMessaging.Core.ChannelEntity>,System.Boolean)
extern "C"  void Subscription_UpdateIsAwaitingConnectCallbacksOfEntity_m189581664 (Subscription_t3881812953 * __this, List_1_t2635275738 * ___channelEntity0, bool ___isAwaitingConnectCallback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Subscription::CleanUp()
extern "C"  void Subscription_CleanUp_m3625053491 (Subscription_t3881812953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Subscription::.cctor()
extern "C"  void Subscription__cctor_m4068432914 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
