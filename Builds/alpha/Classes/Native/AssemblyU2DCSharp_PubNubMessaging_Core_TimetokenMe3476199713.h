﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Core.TimetokenMetadata
struct  TimetokenMetadata_t3476199713  : public Il2CppObject
{
public:
	// System.Int64 PubNubMessaging.Core.TimetokenMetadata::<t>k__BackingField
	int64_t ___U3CtU3Ek__BackingField_0;
	// System.String PubNubMessaging.Core.TimetokenMetadata::<r>k__BackingField
	String_t* ___U3CrU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CtU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TimetokenMetadata_t3476199713, ___U3CtU3Ek__BackingField_0)); }
	inline int64_t get_U3CtU3Ek__BackingField_0() const { return ___U3CtU3Ek__BackingField_0; }
	inline int64_t* get_address_of_U3CtU3Ek__BackingField_0() { return &___U3CtU3Ek__BackingField_0; }
	inline void set_U3CtU3Ek__BackingField_0(int64_t value)
	{
		___U3CtU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CrU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TimetokenMetadata_t3476199713, ___U3CrU3Ek__BackingField_1)); }
	inline String_t* get_U3CrU3Ek__BackingField_1() const { return ___U3CrU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CrU3Ek__BackingField_1() { return &___U3CrU3Ek__BackingField_1; }
	inline void set_U3CrU3Ek__BackingField_1(String_t* value)
	{
		___U3CrU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrU3Ek__BackingField_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
