﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Core.TimetokenMetadata
struct TimetokenMetadata_t3476199713;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PubNubMessaging.Core.TimetokenMetadata::.ctor(System.Int64,System.String)
extern "C"  void TimetokenMetadata__ctor_m1803943043 (TimetokenMetadata_t3476199713 * __this, int64_t ___timetoken0, String_t* ___region1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 PubNubMessaging.Core.TimetokenMetadata::get_t()
extern "C"  int64_t TimetokenMetadata_get_t_m2369913111 (TimetokenMetadata_t3476199713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.TimetokenMetadata::set_t(System.Int64)
extern "C"  void TimetokenMetadata_set_t_m841692144 (TimetokenMetadata_t3476199713 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.TimetokenMetadata::get_r()
extern "C"  String_t* TimetokenMetadata_get_r_m4292088525 (TimetokenMetadata_t3476199713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.TimetokenMetadata::set_r(System.String)
extern "C"  void TimetokenMetadata_set_r_m3750701288 (TimetokenMetadata_t3476199713 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 PubNubMessaging.Core.TimetokenMetadata::get_Timetoken()
extern "C"  int64_t TimetokenMetadata_get_Timetoken_m1419287289 (TimetokenMetadata_t3476199713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.TimetokenMetadata::get_Region()
extern "C"  String_t* TimetokenMetadata_get_Region_m22440027 (TimetokenMetadata_t3476199713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
