﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PushTypeSer3531847941.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Guid2533601593.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ResponseTyp1248099179.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Int32 PubNubMessaging.Core.Utility::CheckTimeoutValue(System.Int32)
extern "C"  int32_t Utility_CheckTimeoutValue_m1654374993 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 PubNubMessaging.Core.Utility::CheckKeyAndParseLong(System.Collections.IDictionary,System.String,System.String)
extern "C"  int64_t Utility_CheckKeyAndParseLong_m1884809373 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___dict0, String_t* ___what1, String_t* ___key2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PubNubMessaging.Core.Utility::CheckKeyAndParseInt(System.Collections.IDictionary,System.String,System.String)
extern "C"  int32_t Utility_CheckKeyAndParseInt_m3825992135 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___dict0, String_t* ___what1, String_t* ___key2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 PubNubMessaging.Core.Utility::ValidateTimetoken(System.String,System.Boolean)
extern "C"  int64_t Utility_ValidateTimetoken_m3488030782 (Il2CppObject * __this /* static, unused */, String_t* ___timetoken0, bool ___raiseError1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.Utility::CheckChannelGroup(System.String,System.Boolean)
extern "C"  String_t* Utility_CheckChannelGroup_m3025703750 (Il2CppObject * __this /* static, unused */, String_t* ___channelGroup0, bool ___convertToPresence1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PubNubMessaging.Core.Utility::CheckAndAddNameSpace(System.String)
extern "C"  List_1_t1398341365 * Utility_CheckAndAddNameSpace_m428050118 (Il2CppObject * __this /* static, unused */, String_t* ___nameSpace0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Utility::CheckPushType(PubNubMessaging.Core.PushTypeService)
extern "C"  void Utility_CheckPushType_m1624198890 (Il2CppObject * __this /* static, unused */, int32_t ___pushType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Utility::CheckChannelOrChannelGroup(System.String,System.String)
extern "C"  void Utility_CheckChannelOrChannelGroup_m948974558 (Il2CppObject * __this /* static, unused */, String_t* ___channel0, String_t* ___channelGroup1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Utility::CheckChannels(System.String[])
extern "C"  void Utility_CheckChannels_m10655420 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___channels0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Utility::CheckChannel(System.String)
extern "C"  void Utility_CheckChannel_m1118277375 (Il2CppObject * __this /* static, unused */, String_t* ___channel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Utility::CheckMessage(System.Object)
extern "C"  void Utility_CheckMessage_m4193762427 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Utility::CheckString(System.String,System.String)
extern "C"  void Utility_CheckString_m259231743 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___what1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Utility::CheckPublishKey(System.String)
extern "C"  void Utility_CheckPublishKey_m2383313102 (Il2CppObject * __this /* static, unused */, String_t* ___publishKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Utility::CheckJSONPluggableLibrary()
extern "C"  void Utility_CheckJSONPluggableLibrary_m2636008232 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Utility::CheckUserState(System.String)
extern "C"  void Utility_CheckUserState_m2604829998 (Il2CppObject * __this /* static, unused */, String_t* ___jsonUserState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Core.Utility::CheckSecretKey(System.String)
extern "C"  void Utility_CheckSecretKey_m1237497645 (Il2CppObject * __this /* static, unused */, String_t* ___secretKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Guid PubNubMessaging.Core.Utility::GenerateGuid()
extern "C"  Guid_t2533601593  Utility_GenerateGuid_m3114630767 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Utility::IsPresenceChannel(System.String)
extern "C"  bool Utility_IsPresenceChannel_m135478766 (Il2CppObject * __this /* static, unused */, String_t* ___channel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Core.Utility::IsUnsafe(System.Char,System.Boolean)
extern "C"  bool Utility_IsUnsafe_m2626757232 (Il2CppObject * __this /* static, unused */, Il2CppChar ___ch0, bool ___ignoreComma1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char PubNubMessaging.Core.Utility::ToHex(System.Int32)
extern "C"  Il2CppChar Utility_ToHex_m2647444567 (Il2CppObject * __this /* static, unused */, int32_t ___ch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.Utility::EncodeUricomponent(System.String,PubNubMessaging.Core.ResponseType,System.Boolean,System.Boolean)
extern "C"  String_t* Utility_EncodeUricomponent_m725310252 (Il2CppObject * __this /* static, unused */, String_t* ___s0, int32_t ___type1, bool ___ignoreComma2, bool ___ignorePercent2fEncode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Core.Utility::Md5(System.String)
extern "C"  String_t* Utility_Md5_m1326605159 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 PubNubMessaging.Core.Utility::TranslateDateTimeToSeconds(System.DateTime)
extern "C"  int64_t Utility_TranslateDateTimeToSeconds_m2533290910 (Il2CppObject * __this /* static, unused */, DateTime_t693205669  ___dotNetUTCDateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 PubNubMessaging.Core.Utility::TranslateDateTimeToPubnubUnixNanoSeconds(System.DateTime)
extern "C"  int64_t Utility_TranslateDateTimeToPubnubUnixNanoSeconds_m2939756338 (Il2CppObject * __this /* static, unused */, DateTime_t693205669  ___dotNetUTCDateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime PubNubMessaging.Core.Utility::TranslatePubnubUnixNanoSecondsToDateTime(System.Int64)
extern "C"  DateTime_t693205669  Utility_TranslatePubnubUnixNanoSecondsToDateTime_m223465586 (Il2CppObject * __this /* static, unused */, int64_t ___unixNanoSecondTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
