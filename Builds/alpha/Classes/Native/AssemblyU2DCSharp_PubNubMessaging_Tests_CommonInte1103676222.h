﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Tests.CommonIntergrationTests
struct CommonIntergrationTests_t1691354350;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator5
struct  U3CDoPublishAndParseU3Ec__Iterator5_t1103676222  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator5::<channel>__0
	String_t* ___U3CchannelU3E__0_0;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator5::noSecretKey
	bool ___noSecretKey_1;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator5::testName
	String_t* ___testName_2;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator5::ssl
	bool ___ssl_3;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator5::withCipher
	bool ___withCipher_4;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator5::asObject
	bool ___asObject_5;
	// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator5::message
	Il2CppObject * ___message_6;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator5::expected
	String_t* ___expected_7;
	// PubNubMessaging.Tests.CommonIntergrationTests PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator5::$this
	CommonIntergrationTests_t1691354350 * ___U24this_8;
	// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator5::$current
	Il2CppObject * ___U24current_9;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator5::$disposing
	bool ___U24disposing_10;
	// System.Int32 PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator5::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3CchannelU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator5_t1103676222, ___U3CchannelU3E__0_0)); }
	inline String_t* get_U3CchannelU3E__0_0() const { return ___U3CchannelU3E__0_0; }
	inline String_t** get_address_of_U3CchannelU3E__0_0() { return &___U3CchannelU3E__0_0; }
	inline void set_U3CchannelU3E__0_0(String_t* value)
	{
		___U3CchannelU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchannelU3E__0_0, value);
	}

	inline static int32_t get_offset_of_noSecretKey_1() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator5_t1103676222, ___noSecretKey_1)); }
	inline bool get_noSecretKey_1() const { return ___noSecretKey_1; }
	inline bool* get_address_of_noSecretKey_1() { return &___noSecretKey_1; }
	inline void set_noSecretKey_1(bool value)
	{
		___noSecretKey_1 = value;
	}

	inline static int32_t get_offset_of_testName_2() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator5_t1103676222, ___testName_2)); }
	inline String_t* get_testName_2() const { return ___testName_2; }
	inline String_t** get_address_of_testName_2() { return &___testName_2; }
	inline void set_testName_2(String_t* value)
	{
		___testName_2 = value;
		Il2CppCodeGenWriteBarrier(&___testName_2, value);
	}

	inline static int32_t get_offset_of_ssl_3() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator5_t1103676222, ___ssl_3)); }
	inline bool get_ssl_3() const { return ___ssl_3; }
	inline bool* get_address_of_ssl_3() { return &___ssl_3; }
	inline void set_ssl_3(bool value)
	{
		___ssl_3 = value;
	}

	inline static int32_t get_offset_of_withCipher_4() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator5_t1103676222, ___withCipher_4)); }
	inline bool get_withCipher_4() const { return ___withCipher_4; }
	inline bool* get_address_of_withCipher_4() { return &___withCipher_4; }
	inline void set_withCipher_4(bool value)
	{
		___withCipher_4 = value;
	}

	inline static int32_t get_offset_of_asObject_5() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator5_t1103676222, ___asObject_5)); }
	inline bool get_asObject_5() const { return ___asObject_5; }
	inline bool* get_address_of_asObject_5() { return &___asObject_5; }
	inline void set_asObject_5(bool value)
	{
		___asObject_5 = value;
	}

	inline static int32_t get_offset_of_message_6() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator5_t1103676222, ___message_6)); }
	inline Il2CppObject * get_message_6() const { return ___message_6; }
	inline Il2CppObject ** get_address_of_message_6() { return &___message_6; }
	inline void set_message_6(Il2CppObject * value)
	{
		___message_6 = value;
		Il2CppCodeGenWriteBarrier(&___message_6, value);
	}

	inline static int32_t get_offset_of_expected_7() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator5_t1103676222, ___expected_7)); }
	inline String_t* get_expected_7() const { return ___expected_7; }
	inline String_t** get_address_of_expected_7() { return &___expected_7; }
	inline void set_expected_7(String_t* value)
	{
		___expected_7 = value;
		Il2CppCodeGenWriteBarrier(&___expected_7, value);
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator5_t1103676222, ___U24this_8)); }
	inline CommonIntergrationTests_t1691354350 * get_U24this_8() const { return ___U24this_8; }
	inline CommonIntergrationTests_t1691354350 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(CommonIntergrationTests_t1691354350 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_8, value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator5_t1103676222, ___U24current_9)); }
	inline Il2CppObject * get_U24current_9() const { return ___U24current_9; }
	inline Il2CppObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(Il2CppObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_9, value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator5_t1103676222, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator5_t1103676222, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
