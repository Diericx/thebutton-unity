﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator5
struct U3CDoPublishAndParseU3Ec__Iterator5_t1103676222;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator5::.ctor()
extern "C"  void U3CDoPublishAndParseU3Ec__Iterator5__ctor_m2629392447 (U3CDoPublishAndParseU3Ec__Iterator5_t1103676222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator5::MoveNext()
extern "C"  bool U3CDoPublishAndParseU3Ec__Iterator5_MoveNext_m3443163553 (U3CDoPublishAndParseU3Ec__Iterator5_t1103676222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoPublishAndParseU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2917959481 (U3CDoPublishAndParseU3Ec__Iterator5_t1103676222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoPublishAndParseU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m2090891601 (U3CDoPublishAndParseU3Ec__Iterator5_t1103676222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator5::Dispose()
extern "C"  void U3CDoPublishAndParseU3Ec__Iterator5_Dispose_m116080464 (U3CDoPublishAndParseU3Ec__Iterator5_t1103676222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator5::Reset()
extern "C"  void U3CDoPublishAndParseU3Ec__Iterator5_Reset_m3218371106 (U3CDoPublishAndParseU3Ec__Iterator5_t1103676222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
