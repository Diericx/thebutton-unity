﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator4
struct U3CDoPublishAndParseU3Ec__Iterator4_t1103676223;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator4::.ctor()
extern "C"  void U3CDoPublishAndParseU3Ec__Iterator4__ctor_m2632869888 (U3CDoPublishAndParseU3Ec__Iterator4_t1103676223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator4::MoveNext()
extern "C"  bool U3CDoPublishAndParseU3Ec__Iterator4_MoveNext_m2629411196 (U3CDoPublishAndParseU3Ec__Iterator4_t1103676223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoPublishAndParseU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2923136410 (U3CDoPublishAndParseU3Ec__Iterator4_t1103676223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoPublishAndParseU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1128128050 (U3CDoPublishAndParseU3Ec__Iterator4_t1103676223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator4::Dispose()
extern "C"  void U3CDoPublishAndParseU3Ec__Iterator4_Dispose_m231112753 (U3CDoPublishAndParseU3Ec__Iterator4_t1103676223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator4::Reset()
extern "C"  void U3CDoPublishAndParseU3Ec__Iterator4_Reset_m3221856995 (U3CDoPublishAndParseU3Ec__Iterator4_t1103676223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
