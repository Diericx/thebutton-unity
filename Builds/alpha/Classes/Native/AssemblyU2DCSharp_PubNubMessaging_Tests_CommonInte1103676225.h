﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Tests.CommonIntergrationTests
struct CommonIntergrationTests_t1691354350;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator6
struct  U3CDoPublishAndParseU3Ec__Iterator6_t1103676225  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator6::testName
	String_t* ___testName_0;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator6::ssl
	bool ___ssl_1;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator6::withCipher
	bool ___withCipher_2;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator6::<channel>__0
	String_t* ___U3CchannelU3E__0_3;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator6::asObject
	bool ___asObject_4;
	// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator6::message
	Il2CppObject * ___message_5;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator6::expected
	String_t* ___expected_6;
	// PubNubMessaging.Tests.CommonIntergrationTests PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator6::$this
	CommonIntergrationTests_t1691354350 * ___U24this_7;
	// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator6::$current
	Il2CppObject * ___U24current_8;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator6::$disposing
	bool ___U24disposing_9;
	// System.Int32 PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator6::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_testName_0() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator6_t1103676225, ___testName_0)); }
	inline String_t* get_testName_0() const { return ___testName_0; }
	inline String_t** get_address_of_testName_0() { return &___testName_0; }
	inline void set_testName_0(String_t* value)
	{
		___testName_0 = value;
		Il2CppCodeGenWriteBarrier(&___testName_0, value);
	}

	inline static int32_t get_offset_of_ssl_1() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator6_t1103676225, ___ssl_1)); }
	inline bool get_ssl_1() const { return ___ssl_1; }
	inline bool* get_address_of_ssl_1() { return &___ssl_1; }
	inline void set_ssl_1(bool value)
	{
		___ssl_1 = value;
	}

	inline static int32_t get_offset_of_withCipher_2() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator6_t1103676225, ___withCipher_2)); }
	inline bool get_withCipher_2() const { return ___withCipher_2; }
	inline bool* get_address_of_withCipher_2() { return &___withCipher_2; }
	inline void set_withCipher_2(bool value)
	{
		___withCipher_2 = value;
	}

	inline static int32_t get_offset_of_U3CchannelU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator6_t1103676225, ___U3CchannelU3E__0_3)); }
	inline String_t* get_U3CchannelU3E__0_3() const { return ___U3CchannelU3E__0_3; }
	inline String_t** get_address_of_U3CchannelU3E__0_3() { return &___U3CchannelU3E__0_3; }
	inline void set_U3CchannelU3E__0_3(String_t* value)
	{
		___U3CchannelU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchannelU3E__0_3, value);
	}

	inline static int32_t get_offset_of_asObject_4() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator6_t1103676225, ___asObject_4)); }
	inline bool get_asObject_4() const { return ___asObject_4; }
	inline bool* get_address_of_asObject_4() { return &___asObject_4; }
	inline void set_asObject_4(bool value)
	{
		___asObject_4 = value;
	}

	inline static int32_t get_offset_of_message_5() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator6_t1103676225, ___message_5)); }
	inline Il2CppObject * get_message_5() const { return ___message_5; }
	inline Il2CppObject ** get_address_of_message_5() { return &___message_5; }
	inline void set_message_5(Il2CppObject * value)
	{
		___message_5 = value;
		Il2CppCodeGenWriteBarrier(&___message_5, value);
	}

	inline static int32_t get_offset_of_expected_6() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator6_t1103676225, ___expected_6)); }
	inline String_t* get_expected_6() const { return ___expected_6; }
	inline String_t** get_address_of_expected_6() { return &___expected_6; }
	inline void set_expected_6(String_t* value)
	{
		___expected_6 = value;
		Il2CppCodeGenWriteBarrier(&___expected_6, value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator6_t1103676225, ___U24this_7)); }
	inline CommonIntergrationTests_t1691354350 * get_U24this_7() const { return ___U24this_7; }
	inline CommonIntergrationTests_t1691354350 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(CommonIntergrationTests_t1691354350 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_7, value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator6_t1103676225, ___U24current_8)); }
	inline Il2CppObject * get_U24current_8() const { return ___U24current_8; }
	inline Il2CppObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(Il2CppObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_8, value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator6_t1103676225, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CDoPublishAndParseU3Ec__Iterator6_t1103676225, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
