﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator6
struct U3CDoPublishAndParseU3Ec__Iterator6_t1103676225;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator6::.ctor()
extern "C"  void U3CDoPublishAndParseU3Ec__Iterator6__ctor_m2635106430 (U3CDoPublishAndParseU3Ec__Iterator6_t1103676225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator6::MoveNext()
extern "C"  bool U3CDoPublishAndParseU3Ec__Iterator6_MoveNext_m2692330162 (U3CDoPublishAndParseU3Ec__Iterator6_t1103676225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoPublishAndParseU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3130793948 (U3CDoPublishAndParseU3Ec__Iterator6_t1103676225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoPublishAndParseU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m4254955892 (U3CDoPublishAndParseU3Ec__Iterator6_t1103676225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator6::Dispose()
extern "C"  void U3CDoPublishAndParseU3Ec__Iterator6_Dispose_m304500595 (U3CDoPublishAndParseU3Ec__Iterator6_t1103676225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishAndParse>c__Iterator6::Reset()
extern "C"  void U3CDoPublishAndParseU3Ec__Iterator6_Reset_m3224080865 (U3CDoPublishAndParseU3Ec__Iterator6_t1103676225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
