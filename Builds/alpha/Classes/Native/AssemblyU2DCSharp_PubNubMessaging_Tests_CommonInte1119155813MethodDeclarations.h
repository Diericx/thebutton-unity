﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParse>c__Iterator8
struct U3CSetAndGetStateAndParseU3Ec__Iterator8_t1119155813;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParse>c__Iterator8::.ctor()
extern "C"  void U3CSetAndGetStateAndParseU3Ec__Iterator8__ctor_m1749595204 (U3CSetAndGetStateAndParseU3Ec__Iterator8_t1119155813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParse>c__Iterator8::MoveNext()
extern "C"  bool U3CSetAndGetStateAndParseU3Ec__Iterator8_MoveNext_m510914020 (U3CSetAndGetStateAndParseU3Ec__Iterator8_t1119155813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParse>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSetAndGetStateAndParseU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1348961160 (U3CSetAndGetStateAndParseU3Ec__Iterator8_t1119155813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParse>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSetAndGetStateAndParseU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m4144802048 (U3CSetAndGetStateAndParseU3Ec__Iterator8_t1119155813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParse>c__Iterator8::Dispose()
extern "C"  void U3CSetAndGetStateAndParseU3Ec__Iterator8_Dispose_m1134281071 (U3CSetAndGetStateAndParseU3Ec__Iterator8_t1119155813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParse>c__Iterator8::Reset()
extern "C"  void U3CSetAndGetStateAndParseU3Ec__Iterator8_Reset_m675607701 (U3CSetAndGetStateAndParseU3Ec__Iterator8_t1119155813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
