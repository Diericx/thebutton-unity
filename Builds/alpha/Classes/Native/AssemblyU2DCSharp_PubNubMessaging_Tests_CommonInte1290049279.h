﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Tests.CommonIntergrationTests
struct CommonIntergrationTests_t1691354350;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoWhereNowAndParse>c__IteratorB
struct  U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoWhereNowAndParse>c__IteratorB::testName
	String_t* ___testName_0;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoWhereNowAndParse>c__IteratorB::ssl
	bool ___ssl_1;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoWhereNowAndParse>c__IteratorB::<channel>__0
	String_t* ___U3CchannelU3E__0_2;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoWhereNowAndParse>c__IteratorB::parseAsString
	bool ___parseAsString_3;
	// PubNubMessaging.Tests.CommonIntergrationTests PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoWhereNowAndParse>c__IteratorB::$this
	CommonIntergrationTests_t1691354350 * ___U24this_4;
	// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoWhereNowAndParse>c__IteratorB::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoWhereNowAndParse>c__IteratorB::$disposing
	bool ___U24disposing_6;
	// System.Int32 PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoWhereNowAndParse>c__IteratorB::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_testName_0() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279, ___testName_0)); }
	inline String_t* get_testName_0() const { return ___testName_0; }
	inline String_t** get_address_of_testName_0() { return &___testName_0; }
	inline void set_testName_0(String_t* value)
	{
		___testName_0 = value;
		Il2CppCodeGenWriteBarrier(&___testName_0, value);
	}

	inline static int32_t get_offset_of_ssl_1() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279, ___ssl_1)); }
	inline bool get_ssl_1() const { return ___ssl_1; }
	inline bool* get_address_of_ssl_1() { return &___ssl_1; }
	inline void set_ssl_1(bool value)
	{
		___ssl_1 = value;
	}

	inline static int32_t get_offset_of_U3CchannelU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279, ___U3CchannelU3E__0_2)); }
	inline String_t* get_U3CchannelU3E__0_2() const { return ___U3CchannelU3E__0_2; }
	inline String_t** get_address_of_U3CchannelU3E__0_2() { return &___U3CchannelU3E__0_2; }
	inline void set_U3CchannelU3E__0_2(String_t* value)
	{
		___U3CchannelU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchannelU3E__0_2, value);
	}

	inline static int32_t get_offset_of_parseAsString_3() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279, ___parseAsString_3)); }
	inline bool get_parseAsString_3() const { return ___parseAsString_3; }
	inline bool* get_address_of_parseAsString_3() { return &___parseAsString_3; }
	inline void set_parseAsString_3(bool value)
	{
		___parseAsString_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279, ___U24this_4)); }
	inline CommonIntergrationTests_t1691354350 * get_U24this_4() const { return ___U24this_4; }
	inline CommonIntergrationTests_t1691354350 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(CommonIntergrationTests_t1691354350 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
