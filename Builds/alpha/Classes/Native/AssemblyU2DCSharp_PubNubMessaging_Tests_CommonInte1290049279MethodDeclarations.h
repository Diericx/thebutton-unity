﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoWhereNowAndParse>c__IteratorB
struct U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoWhereNowAndParse>c__IteratorB::.ctor()
extern "C"  void U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB__ctor_m246603626 (U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoWhereNowAndParse>c__IteratorB::MoveNext()
extern "C"  bool U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_MoveNext_m4069629742 (U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoWhereNowAndParse>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3904947408 (U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoWhereNowAndParse>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m1905435688 (U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoWhereNowAndParse>c__IteratorB::Dispose()
extern "C"  void U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_Dispose_m3497309657 (U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoWhereNowAndParse>c__IteratorB::Reset()
extern "C"  void U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_Reset_m3518031751 (U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
