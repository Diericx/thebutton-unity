﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<DoAlreadySubscribeTest>c__Iterator11
struct U3CDoAlreadySubscribeTestU3Ec__Iterator11_t1364000944;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoAlreadySubscribeTest>c__Iterator11::.ctor()
extern "C"  void U3CDoAlreadySubscribeTestU3Ec__Iterator11__ctor_m1185488857 (U3CDoAlreadySubscribeTestU3Ec__Iterator11_t1364000944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoAlreadySubscribeTest>c__Iterator11::MoveNext()
extern "C"  bool U3CDoAlreadySubscribeTestU3Ec__Iterator11_MoveNext_m1096704175 (U3CDoAlreadySubscribeTestU3Ec__Iterator11_t1364000944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoAlreadySubscribeTest>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoAlreadySubscribeTestU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1213783179 (U3CDoAlreadySubscribeTestU3Ec__Iterator11_t1364000944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoAlreadySubscribeTest>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoAlreadySubscribeTestU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m693008627 (U3CDoAlreadySubscribeTestU3Ec__Iterator11_t1364000944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoAlreadySubscribeTest>c__Iterator11::Dispose()
extern "C"  void U3CDoAlreadySubscribeTestU3Ec__Iterator11_Dispose_m4260361690 (U3CDoAlreadySubscribeTestU3Ec__Iterator11_t1364000944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoAlreadySubscribeTest>c__Iterator11::Reset()
extern "C"  void U3CDoAlreadySubscribeTestU3Ec__Iterator11_Reset_m142960540 (U3CDoAlreadySubscribeTestU3Ec__Iterator11_t1364000944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
