﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Core.Pubnub
struct Pubnub_t2451529532;
// System.Object
struct Il2CppObject;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2825504181;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ResponseTyp1248099179.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.CommonIntergrationTests
struct  CommonIntergrationTests_t1691354350  : public Il2CppObject
{
public:
	// PubNubMessaging.Core.Pubnub PubNubMessaging.Tests.CommonIntergrationTests::pubnub
	Pubnub_t2451529532 * ___pubnub_7;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests::<TimedOut>k__BackingField
	bool ___U3CTimedOutU3Ek__BackingField_8;
	// System.Object PubNubMessaging.Tests.CommonIntergrationTests::<Response>k__BackingField
	Il2CppObject * ___U3CResponseU3Ek__BackingField_9;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests::<ResponseString>k__BackingField
	String_t* ___U3CResponseStringU3Ek__BackingField_10;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests::<SubChannel>k__BackingField
	String_t* ___U3CSubChannelU3Ek__BackingField_11;
	// System.Object PubNubMessaging.Tests.CommonIntergrationTests::<Name>k__BackingField
	Il2CppObject * ___U3CNameU3Ek__BackingField_12;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests::<ErrorResponse>k__BackingField
	String_t* ___U3CErrorResponseU3Ek__BackingField_13;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests::<DeliveryStatus>k__BackingField
	bool ___U3CDeliveryStatusU3Ek__BackingField_14;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests::ExpectedMessage
	String_t* ___ExpectedMessage_15;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests::ExpectedChannels
	String_t* ___ExpectedChannels_16;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests::IsError
	bool ___IsError_17;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests::IsTimeout
	bool ___IsTimeout_18;
	// PubNubMessaging.Core.CurrentRequestType PubNubMessaging.Tests.CommonIntergrationTests::Crt
	int32_t ___Crt_19;
	// PubNubMessaging.Core.ResponseType PubNubMessaging.Tests.CommonIntergrationTests::RespType
	int32_t ___RespType_20;

public:
	inline static int32_t get_offset_of_pubnub_7() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350, ___pubnub_7)); }
	inline Pubnub_t2451529532 * get_pubnub_7() const { return ___pubnub_7; }
	inline Pubnub_t2451529532 ** get_address_of_pubnub_7() { return &___pubnub_7; }
	inline void set_pubnub_7(Pubnub_t2451529532 * value)
	{
		___pubnub_7 = value;
		Il2CppCodeGenWriteBarrier(&___pubnub_7, value);
	}

	inline static int32_t get_offset_of_U3CTimedOutU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350, ___U3CTimedOutU3Ek__BackingField_8)); }
	inline bool get_U3CTimedOutU3Ek__BackingField_8() const { return ___U3CTimedOutU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CTimedOutU3Ek__BackingField_8() { return &___U3CTimedOutU3Ek__BackingField_8; }
	inline void set_U3CTimedOutU3Ek__BackingField_8(bool value)
	{
		___U3CTimedOutU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CResponseU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350, ___U3CResponseU3Ek__BackingField_9)); }
	inline Il2CppObject * get_U3CResponseU3Ek__BackingField_9() const { return ___U3CResponseU3Ek__BackingField_9; }
	inline Il2CppObject ** get_address_of_U3CResponseU3Ek__BackingField_9() { return &___U3CResponseU3Ek__BackingField_9; }
	inline void set_U3CResponseU3Ek__BackingField_9(Il2CppObject * value)
	{
		___U3CResponseU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResponseU3Ek__BackingField_9, value);
	}

	inline static int32_t get_offset_of_U3CResponseStringU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350, ___U3CResponseStringU3Ek__BackingField_10)); }
	inline String_t* get_U3CResponseStringU3Ek__BackingField_10() const { return ___U3CResponseStringU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CResponseStringU3Ek__BackingField_10() { return &___U3CResponseStringU3Ek__BackingField_10; }
	inline void set_U3CResponseStringU3Ek__BackingField_10(String_t* value)
	{
		___U3CResponseStringU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CResponseStringU3Ek__BackingField_10, value);
	}

	inline static int32_t get_offset_of_U3CSubChannelU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350, ___U3CSubChannelU3Ek__BackingField_11)); }
	inline String_t* get_U3CSubChannelU3Ek__BackingField_11() const { return ___U3CSubChannelU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CSubChannelU3Ek__BackingField_11() { return &___U3CSubChannelU3Ek__BackingField_11; }
	inline void set_U3CSubChannelU3Ek__BackingField_11(String_t* value)
	{
		___U3CSubChannelU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSubChannelU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350, ___U3CNameU3Ek__BackingField_12)); }
	inline Il2CppObject * get_U3CNameU3Ek__BackingField_12() const { return ___U3CNameU3Ek__BackingField_12; }
	inline Il2CppObject ** get_address_of_U3CNameU3Ek__BackingField_12() { return &___U3CNameU3Ek__BackingField_12; }
	inline void set_U3CNameU3Ek__BackingField_12(Il2CppObject * value)
	{
		___U3CNameU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNameU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3CErrorResponseU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350, ___U3CErrorResponseU3Ek__BackingField_13)); }
	inline String_t* get_U3CErrorResponseU3Ek__BackingField_13() const { return ___U3CErrorResponseU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CErrorResponseU3Ek__BackingField_13() { return &___U3CErrorResponseU3Ek__BackingField_13; }
	inline void set_U3CErrorResponseU3Ek__BackingField_13(String_t* value)
	{
		___U3CErrorResponseU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CErrorResponseU3Ek__BackingField_13, value);
	}

	inline static int32_t get_offset_of_U3CDeliveryStatusU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350, ___U3CDeliveryStatusU3Ek__BackingField_14)); }
	inline bool get_U3CDeliveryStatusU3Ek__BackingField_14() const { return ___U3CDeliveryStatusU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CDeliveryStatusU3Ek__BackingField_14() { return &___U3CDeliveryStatusU3Ek__BackingField_14; }
	inline void set_U3CDeliveryStatusU3Ek__BackingField_14(bool value)
	{
		___U3CDeliveryStatusU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_ExpectedMessage_15() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350, ___ExpectedMessage_15)); }
	inline String_t* get_ExpectedMessage_15() const { return ___ExpectedMessage_15; }
	inline String_t** get_address_of_ExpectedMessage_15() { return &___ExpectedMessage_15; }
	inline void set_ExpectedMessage_15(String_t* value)
	{
		___ExpectedMessage_15 = value;
		Il2CppCodeGenWriteBarrier(&___ExpectedMessage_15, value);
	}

	inline static int32_t get_offset_of_ExpectedChannels_16() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350, ___ExpectedChannels_16)); }
	inline String_t* get_ExpectedChannels_16() const { return ___ExpectedChannels_16; }
	inline String_t** get_address_of_ExpectedChannels_16() { return &___ExpectedChannels_16; }
	inline void set_ExpectedChannels_16(String_t* value)
	{
		___ExpectedChannels_16 = value;
		Il2CppCodeGenWriteBarrier(&___ExpectedChannels_16, value);
	}

	inline static int32_t get_offset_of_IsError_17() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350, ___IsError_17)); }
	inline bool get_IsError_17() const { return ___IsError_17; }
	inline bool* get_address_of_IsError_17() { return &___IsError_17; }
	inline void set_IsError_17(bool value)
	{
		___IsError_17 = value;
	}

	inline static int32_t get_offset_of_IsTimeout_18() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350, ___IsTimeout_18)); }
	inline bool get_IsTimeout_18() const { return ___IsTimeout_18; }
	inline bool* get_address_of_IsTimeout_18() { return &___IsTimeout_18; }
	inline void set_IsTimeout_18(bool value)
	{
		___IsTimeout_18 = value;
	}

	inline static int32_t get_offset_of_Crt_19() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350, ___Crt_19)); }
	inline int32_t get_Crt_19() const { return ___Crt_19; }
	inline int32_t* get_address_of_Crt_19() { return &___Crt_19; }
	inline void set_Crt_19(int32_t value)
	{
		___Crt_19 = value;
	}

	inline static int32_t get_offset_of_RespType_20() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350, ___RespType_20)); }
	inline int32_t get_RespType_20() const { return ___RespType_20; }
	inline int32_t* get_address_of_RespType_20() { return &___RespType_20; }
	inline void set_RespType_20(int32_t value)
	{
		___RespType_20 = value;
	}
};

struct CommonIntergrationTests_t1691354350_StaticFields
{
public:
	// System.String PubNubMessaging.Tests.CommonIntergrationTests::PublishKey
	String_t* ___PublishKey_0;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests::SubscribeKey
	String_t* ___SubscribeKey_1;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests::SecretKey
	String_t* ___SecretKey_2;
	// System.Single PubNubMessaging.Tests.CommonIntergrationTests::WaitTimeBetweenCalls
	float ___WaitTimeBetweenCalls_3;
	// System.Single PubNubMessaging.Tests.CommonIntergrationTests::WaitTimeBetweenCallsLow
	float ___WaitTimeBetweenCallsLow_4;
	// System.Single PubNubMessaging.Tests.CommonIntergrationTests::WaitTimeToReadResponse
	float ___WaitTimeToReadResponse_5;
	// System.Single PubNubMessaging.Tests.CommonIntergrationTests::WaitTime
	float ___WaitTime_6;
	// System.Func`2<System.Object,System.Object> PubNubMessaging.Tests.CommonIntergrationTests::<>f__am$cache0
	Func_2_t2825504181 * ___U3CU3Ef__amU24cache0_21;

public:
	inline static int32_t get_offset_of_PublishKey_0() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350_StaticFields, ___PublishKey_0)); }
	inline String_t* get_PublishKey_0() const { return ___PublishKey_0; }
	inline String_t** get_address_of_PublishKey_0() { return &___PublishKey_0; }
	inline void set_PublishKey_0(String_t* value)
	{
		___PublishKey_0 = value;
		Il2CppCodeGenWriteBarrier(&___PublishKey_0, value);
	}

	inline static int32_t get_offset_of_SubscribeKey_1() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350_StaticFields, ___SubscribeKey_1)); }
	inline String_t* get_SubscribeKey_1() const { return ___SubscribeKey_1; }
	inline String_t** get_address_of_SubscribeKey_1() { return &___SubscribeKey_1; }
	inline void set_SubscribeKey_1(String_t* value)
	{
		___SubscribeKey_1 = value;
		Il2CppCodeGenWriteBarrier(&___SubscribeKey_1, value);
	}

	inline static int32_t get_offset_of_SecretKey_2() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350_StaticFields, ___SecretKey_2)); }
	inline String_t* get_SecretKey_2() const { return ___SecretKey_2; }
	inline String_t** get_address_of_SecretKey_2() { return &___SecretKey_2; }
	inline void set_SecretKey_2(String_t* value)
	{
		___SecretKey_2 = value;
		Il2CppCodeGenWriteBarrier(&___SecretKey_2, value);
	}

	inline static int32_t get_offset_of_WaitTimeBetweenCalls_3() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350_StaticFields, ___WaitTimeBetweenCalls_3)); }
	inline float get_WaitTimeBetweenCalls_3() const { return ___WaitTimeBetweenCalls_3; }
	inline float* get_address_of_WaitTimeBetweenCalls_3() { return &___WaitTimeBetweenCalls_3; }
	inline void set_WaitTimeBetweenCalls_3(float value)
	{
		___WaitTimeBetweenCalls_3 = value;
	}

	inline static int32_t get_offset_of_WaitTimeBetweenCallsLow_4() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350_StaticFields, ___WaitTimeBetweenCallsLow_4)); }
	inline float get_WaitTimeBetweenCallsLow_4() const { return ___WaitTimeBetweenCallsLow_4; }
	inline float* get_address_of_WaitTimeBetweenCallsLow_4() { return &___WaitTimeBetweenCallsLow_4; }
	inline void set_WaitTimeBetweenCallsLow_4(float value)
	{
		___WaitTimeBetweenCallsLow_4 = value;
	}

	inline static int32_t get_offset_of_WaitTimeToReadResponse_5() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350_StaticFields, ___WaitTimeToReadResponse_5)); }
	inline float get_WaitTimeToReadResponse_5() const { return ___WaitTimeToReadResponse_5; }
	inline float* get_address_of_WaitTimeToReadResponse_5() { return &___WaitTimeToReadResponse_5; }
	inline void set_WaitTimeToReadResponse_5(float value)
	{
		___WaitTimeToReadResponse_5 = value;
	}

	inline static int32_t get_offset_of_WaitTime_6() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350_StaticFields, ___WaitTime_6)); }
	inline float get_WaitTime_6() const { return ___WaitTime_6; }
	inline float* get_address_of_WaitTime_6() { return &___WaitTime_6; }
	inline void set_WaitTime_6(float value)
	{
		___WaitTime_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_21() { return static_cast<int32_t>(offsetof(CommonIntergrationTests_t1691354350_StaticFields, ___U3CU3Ef__amU24cache0_21)); }
	inline Func_2_t2825504181 * get_U3CU3Ef__amU24cache0_21() const { return ___U3CU3Ef__amU24cache0_21; }
	inline Func_2_t2825504181 ** get_address_of_U3CU3Ef__amU24cache0_21() { return &___U3CU3Ef__amU24cache0_21; }
	inline void set_U3CU3Ef__amU24cache0_21(Func_2_t2825504181 * value)
	{
		___U3CU3Ef__amU24cache0_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
