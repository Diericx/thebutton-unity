﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests
struct CommonIntergrationTests_t1691354350;
// PubNubMessaging.Core.Pubnub
struct Pubnub_t2451529532;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PubNubMessaging.Core.PubnubClientError
struct PubnubClientError_t110317921;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3230389896;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_Pubnub2451529532.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubClient110317921.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ResponseTyp1248099179.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests::.ctor()
extern "C"  void CommonIntergrationTests__ctor_m292980710 (CommonIntergrationTests_t1691354350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests::get_TimedOut()
extern "C"  bool CommonIntergrationTests_get_TimedOut_m2881830538 (CommonIntergrationTests_t1691354350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::set_TimedOut(System.Boolean)
extern "C"  void CommonIntergrationTests_set_TimedOut_m176848415 (CommonIntergrationTests_t1691354350 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::set_SetPubnub(PubNubMessaging.Core.Pubnub)
extern "C"  void CommonIntergrationTests_set_SetPubnub_m2467821638 (CommonIntergrationTests_t1691354350 * __this, Pubnub_t2451529532 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests::get_TestingUsingMiniJSON()
extern "C"  bool CommonIntergrationTests_get_TestingUsingMiniJSON_m741523042 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests::get_Response()
extern "C"  Il2CppObject * CommonIntergrationTests_get_Response_m1966511709 (CommonIntergrationTests_t1691354350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::set_Response(System.Object)
extern "C"  void CommonIntergrationTests_set_Response_m2813177002 (CommonIntergrationTests_t1691354350 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Tests.CommonIntergrationTests::get_ResponseString()
extern "C"  String_t* CommonIntergrationTests_get_ResponseString_m53734316 (CommonIntergrationTests_t1691354350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::set_ResponseString(System.String)
extern "C"  void CommonIntergrationTests_set_ResponseString_m3506826991 (CommonIntergrationTests_t1691354350 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Tests.CommonIntergrationTests::get_SubChannel()
extern "C"  String_t* CommonIntergrationTests_get_SubChannel_m3120406493 (CommonIntergrationTests_t1691354350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::set_SubChannel(System.String)
extern "C"  void CommonIntergrationTests_set_SubChannel_m259035998 (CommonIntergrationTests_t1691354350 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests::get_Name()
extern "C"  Il2CppObject * CommonIntergrationTests_get_Name_m4025431101 (CommonIntergrationTests_t1691354350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::set_Name(System.Object)
extern "C"  void CommonIntergrationTests_set_Name_m2404746286 (CommonIntergrationTests_t1691354350 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Tests.CommonIntergrationTests::get_ErrorResponse()
extern "C"  String_t* CommonIntergrationTests_get_ErrorResponse_m72102641 (CommonIntergrationTests_t1691354350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::set_ErrorResponse(System.String)
extern "C"  void CommonIntergrationTests_set_ErrorResponse_m593545250 (CommonIntergrationTests_t1691354350 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests::get_DeliveryStatus()
extern "C"  bool CommonIntergrationTests_get_DeliveryStatus_m922329095 (CommonIntergrationTests_t1691354350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::set_DeliveryStatus(System.Boolean)
extern "C"  void CommonIntergrationTests_set_DeliveryStatus_m2669239706 (CommonIntergrationTests_t1691354350 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::DisplayErrorMessage(PubNubMessaging.Core.PubnubClientError)
extern "C"  void CommonIntergrationTests_DisplayErrorMessage_m414332483 (CommonIntergrationTests_t1691354350 * __this, PubnubClientError_t110317921 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::DisplayReturnMessageDummy(System.Object)
extern "C"  void CommonIntergrationTests_DisplayReturnMessageDummy_m3159547375 (CommonIntergrationTests_t1691354350 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::DisplayReturnMessage(System.Object)
extern "C"  void CommonIntergrationTests_DisplayReturnMessage_m4169800483 (CommonIntergrationTests_t1691354350 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::DisplayReturnMessagePresence(System.String)
extern "C"  void CommonIntergrationTests_DisplayReturnMessagePresence_m3723073780 (CommonIntergrationTests_t1691354350 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::OnSubConnectedUnsub(System.String)
extern "C"  void CommonIntergrationTests_OnSubConnectedUnsub_m2050640133 (CommonIntergrationTests_t1691354350 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::DisplayReturnMessage(System.String)
extern "C"  void CommonIntergrationTests_DisplayReturnMessage_m855718991 (CommonIntergrationTests_t1691354350 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Tests.CommonIntergrationTests::Init(System.String,System.Boolean,System.Boolean,System.Boolean)
extern "C"  String_t* CommonIntergrationTests_Init_m26942422 (CommonIntergrationTests_t1691354350 * __this, String_t* ___testName0, bool ___ssl1, bool ___withCipher2, bool ___secretEmpty3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Tests.CommonIntergrationTests::Init(System.String,System.Boolean,System.Boolean)
extern "C"  String_t* CommonIntergrationTests_Init_m2996254151 (CommonIntergrationTests_t1691354350 * __this, String_t* ___testName0, bool ___ssl1, bool ___withCipher2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PubNubMessaging.Tests.CommonIntergrationTests::Init(System.String,System.Boolean)
extern "C"  String_t* CommonIntergrationTests_Init_m3966775844 (CommonIntergrationTests_t1691354350 * __this, String_t* ___testName0, bool ___ssl1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::GetTimeFromServerUsingNewPubNub(System.String,System.Boolean)
extern "C"  void CommonIntergrationTests_GetTimeFromServerUsingNewPubNub_m726336179 (CommonIntergrationTests_t1691354350 * __this, String_t* ___testName0, bool ___ssl1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::DoCGAddListRemoveSubscribeStateHereNowUnsub(System.Boolean,System.String,System.Boolean,System.Boolean,System.Object,System.String,System.Boolean)
extern "C"  Il2CppObject * CommonIntergrationTests_DoCGAddListRemoveSubscribeStateHereNowUnsub_m2128499132 (CommonIntergrationTests_t1691354350 * __this, bool ___ssl0, String_t* ___testName1, bool ___asObject2, bool ___withCipher3, Il2CppObject * ___message4, String_t* ___expectedStringResponse5, bool ___matchExpectedStringResponse6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::DoSubscribeThenPublishAndParse(System.Boolean,System.String,System.Boolean,System.Boolean,System.Object,System.String,System.Boolean)
extern "C"  Il2CppObject * CommonIntergrationTests_DoSubscribeThenPublishAndParse_m940107253 (CommonIntergrationTests_t1691354350 * __this, bool ___ssl0, String_t* ___testName1, bool ___asObject2, bool ___withCipher3, Il2CppObject * ___message4, String_t* ___expectedStringResponse5, bool ___matchExpectedStringResponse6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::DoTimeAndParse(System.Boolean,System.String,System.Boolean)
extern "C"  Il2CppObject * CommonIntergrationTests_DoTimeAndParse_m3893354620 (CommonIntergrationTests_t1691354350 * __this, bool ___ssl0, String_t* ___testName1, bool ___asObject2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::DoPublishThenDetailedHistoryAndParse(System.Boolean,System.String,System.Object[],System.Boolean,System.Boolean,System.Boolean,System.Int32,System.Boolean)
extern "C"  Il2CppObject * CommonIntergrationTests_DoPublishThenDetailedHistoryAndParse_m784527841 (CommonIntergrationTests_t1691354350 * __this, bool ___ssl0, String_t* ___testName1, ObjectU5BU5D_t3614634134* ___messages2, bool ___asObject3, bool ___withCipher4, bool ___noStore5, int32_t ___numberOfMessages6, bool ___isParamsTest7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests::ParseDetailedHistoryResponse(System.Object[],System.String,System.Boolean,System.Int32,System.Int32)
extern "C"  bool CommonIntergrationTests_ParseDetailedHistoryResponse_m1573516227 (CommonIntergrationTests_t1691354350 * __this, ObjectU5BU5D_t3614634134* ___messages0, String_t* ___testName1, bool ___asObject2, int32_t ___messageStart3, int32_t ___messageEnd4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests::ParseFields(System.Collections.Generic.IList`1<System.Object>,System.Object[],System.String,System.Int32,System.Int32)
extern "C"  bool CommonIntergrationTests_ParseFields_m1064131221 (CommonIntergrationTests_t1691354350 * __this, Il2CppObject* ___fields0, ObjectU5BU5D_t3614634134* ___messages1, String_t* ___testName2, int32_t ___messageStart3, int32_t ___messageEnd4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests::ParseResponseNoStore(System.Object[],System.String)
extern "C"  bool CommonIntergrationTests_ParseResponseNoStore_m2130042766 (CommonIntergrationTests_t1691354350 * __this, ObjectU5BU5D_t3614634134* ___messages0, String_t* ___testName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::DoPublishAndParse(System.Boolean,System.String,System.Object,System.String,System.Boolean)
extern "C"  Il2CppObject * CommonIntergrationTests_DoPublishAndParse_m4142579072 (CommonIntergrationTests_t1691354350 * __this, bool ___ssl0, String_t* ___testName1, Il2CppObject * ___message2, String_t* ___expected3, bool ___asObject4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::DoPublishAndParse(System.Boolean,System.String,System.Object,System.String,System.Boolean,System.Boolean,System.Boolean)
extern "C"  Il2CppObject * CommonIntergrationTests_DoPublishAndParse_m1559557650 (CommonIntergrationTests_t1691354350 * __this, bool ___ssl0, String_t* ___testName1, Il2CppObject * ___message2, String_t* ___expected3, bool ___asObject4, bool ___withCipher5, bool ___noSecretKey6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::ParsePublishResponse(System.String,System.String,System.Boolean)
extern "C"  void CommonIntergrationTests_ParsePublishResponse_m3661412056 (CommonIntergrationTests_t1691354350 * __this, String_t* ___testName0, String_t* ___expected1, bool ___asObject2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::DoPublishAndParse(System.Boolean,System.String,System.Object,System.String,System.Boolean,System.Boolean)
extern "C"  Il2CppObject * CommonIntergrationTests_DoPublishAndParse_m430466321 (CommonIntergrationTests_t1691354350 * __this, bool ___ssl0, String_t* ___testName1, Il2CppObject * ___message2, String_t* ___expected3, bool ___asObject4, bool ___withCipher5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::SubscribeUsingSeparateCommon(System.String,System.String)
extern "C"  void CommonIntergrationTests_SubscribeUsingSeparateCommon_m2269795722 (CommonIntergrationTests_t1691354350 * __this, String_t* ___channel0, String_t* ___testName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::SetState(System.String,System.String,System.String)
extern "C"  void CommonIntergrationTests_SetState_m3239891289 (CommonIntergrationTests_t1691354350 * __this, String_t* ___channel0, String_t* ___testName1, String_t* ___state2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::SetStateUUID(System.String,System.String,System.String,System.String)
extern "C"  void CommonIntergrationTests_SetStateUUID_m1601943028 (CommonIntergrationTests_t1691354350 * __this, String_t* ___channel0, String_t* ___testName1, String_t* ___state2, String_t* ___uuid3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::SetAndDeleteStateAndParse(System.Boolean,System.String)
extern "C"  Il2CppObject * CommonIntergrationTests_SetAndDeleteStateAndParse_m1287896008 (CommonIntergrationTests_t1691354350 * __this, bool ___ssl0, String_t* ___testName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::SetAndGetStateAndParse(System.Boolean,System.String)
extern "C"  Il2CppObject * CommonIntergrationTests_SetAndGetStateAndParse_m263108655 (CommonIntergrationTests_t1691354350 * __this, bool ___ssl0, String_t* ___testName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::SetAndGetStateAndParseUUID(System.Boolean,System.String)
extern "C"  Il2CppObject * CommonIntergrationTests_SetAndGetStateAndParseUUID_m99028758 (CommonIntergrationTests_t1691354350 * __this, bool ___ssl0, String_t* ___testName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::DoSubscribeThenDoGlobalHereNowAndParse(System.Boolean,System.String,System.Boolean)
extern "C"  Il2CppObject * CommonIntergrationTests_DoSubscribeThenDoGlobalHereNowAndParse_m106071240 (CommonIntergrationTests_t1691354350 * __this, bool ___ssl0, String_t* ___testName1, bool ___parseAsString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::DoSubscribeThenDoWhereNowAndParse(System.Boolean,System.String,System.Boolean)
extern "C"  Il2CppObject * CommonIntergrationTests_DoSubscribeThenDoWhereNowAndParse_m1500044910 (CommonIntergrationTests_t1691354350 * __this, bool ___ssl0, String_t* ___testName1, bool ___parseAsString2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::DoSubscribeThenHereNowAndParse(System.Boolean,System.String,System.Boolean,System.Boolean,System.String)
extern "C"  Il2CppObject * CommonIntergrationTests_DoSubscribeThenHereNowAndParse_m884408733 (CommonIntergrationTests_t1691354350 * __this, bool ___ssl0, String_t* ___testName1, bool ___parseAsString2, bool ___doWithState3, String_t* ___customUUID4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests::ParseDict(System.String,System.Object)
extern "C"  bool CommonIntergrationTests_ParseDict_m3122045953 (CommonIntergrationTests_t1691354350 * __this, String_t* ___matchUUID0, Il2CppObject * ___uuids1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::DoSubscribe(PubNubMessaging.Core.Pubnub,System.String,System.String)
extern "C"  Il2CppObject * CommonIntergrationTests_DoSubscribe_m3026382038 (CommonIntergrationTests_t1691354350 * __this, Pubnub_t2451529532 * ___pn0, String_t* ___channel1, String_t* ___testName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::DoConnectedTest(System.Boolean,System.String,System.Boolean,System.Boolean)
extern "C"  Il2CppObject * CommonIntergrationTests_DoConnectedTest_m1801189575 (CommonIntergrationTests_t1691354350 * __this, bool ___ssl0, String_t* ___testName1, bool ___asObject2, bool ___isPresence3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::DoNotSubscribedTest(System.Boolean,System.String,System.Boolean,System.Boolean)
extern "C"  Il2CppObject * CommonIntergrationTests_DoNotSubscribedTest_m1974319269 (CommonIntergrationTests_t1691354350 * __this, bool ___ssl0, String_t* ___testName1, bool ___asObject2, bool ___isPresence3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::DoUnsubscribeTest(System.Boolean,System.String,System.Boolean,System.Boolean)
extern "C"  Il2CppObject * CommonIntergrationTests_DoUnsubscribeTest_m1328087871 (CommonIntergrationTests_t1691354350 * __this, bool ___ssl0, String_t* ___testName1, bool ___asObject2, bool ___isPresence3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::DoAlreadySubscribeTest(System.Boolean,System.String,System.Boolean,System.Boolean)
extern "C"  Il2CppObject * CommonIntergrationTests_DoAlreadySubscribeTest_m3109541694 (CommonIntergrationTests_t1691354350 * __this, bool ___ssl0, String_t* ___testName1, bool ___asObject2, bool ___isPresence3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::SubscribeCall(System.String,System.String)
extern "C"  void CommonIntergrationTests_SubscribeCall_m3863887280 (CommonIntergrationTests_t1691354350 * __this, String_t* ___channel0, String_t* ___testName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::DoPresenceThenSubscribeAndParse(System.Boolean,System.String,System.Boolean)
extern "C"  Il2CppObject * CommonIntergrationTests_DoPresenceThenSubscribeAndParse_m512131553 (CommonIntergrationTests_t1691354350 * __this, bool ___ssl0, String_t* ___testName1, bool ___asObject2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::TestCoroutineRun(System.String,System.Int32,System.Int32,System.String[],System.Boolean,System.Boolean,System.String,System.String,System.String,System.Boolean,System.Boolean,System.Boolean,System.Int64,PubNubMessaging.Core.CurrentRequestType,PubNubMessaging.Core.ResponseType)
extern "C"  void CommonIntergrationTests_TestCoroutineRun_m2672463553 (CommonIntergrationTests_t1691354350 * __this, String_t* ___url0, int32_t ___timeout1, int32_t ___pause2, StringU5BU5D_t1642385972* ___channels3, bool ___resumeOnReconnect4, bool ___ssl5, String_t* ___testName6, String_t* ___expectedMessage7, String_t* ___expectedChannels8, bool ___isError9, bool ___isTimeout10, bool ___asObject11, int64_t ___timetoken12, int32_t ___crt13, int32_t ___respType14, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::TestCoroutineRunProcessResponse(System.String,System.Int32,System.Int32,System.String[],System.Boolean,System.Boolean,System.String,System.String,System.String,System.Boolean,System.Boolean,System.Boolean,System.Int64,PubNubMessaging.Core.CurrentRequestType,PubNubMessaging.Core.ResponseType)
extern "C"  Il2CppObject * CommonIntergrationTests_TestCoroutineRunProcessResponse_m3525210899 (CommonIntergrationTests_t1691354350 * __this, String_t* ___url0, int32_t ___timeout1, int32_t ___pause2, StringU5BU5D_t1642385972* ___channels3, bool ___resumeOnReconnect4, bool ___ssl5, String_t* ___testName6, String_t* ___expectedMessage7, String_t* ___expectedChannels8, bool ___isError9, bool ___isTimeout10, bool ___asObject11, int64_t ___timetoken12, int32_t ___crt13, int32_t ___respType14, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::ErrorCallbackCommonExceptionHandler(PubNubMessaging.Core.PubnubClientError)
extern "C"  void CommonIntergrationTests_ErrorCallbackCommonExceptionHandler_m651716309 (CommonIntergrationTests_t1691354350 * __this, PubnubClientError_t110317921 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.CommonIntergrationTests::TestCoroutineRunError(System.String,System.Int32,System.Int32,System.String[],System.Boolean,System.Boolean,System.String,System.String,System.String,System.Boolean,System.Boolean,System.Boolean,System.Int64,PubNubMessaging.Core.CurrentRequestType,PubNubMessaging.Core.ResponseType)
extern "C"  Il2CppObject * CommonIntergrationTests_TestCoroutineRunError_m3731514379 (CommonIntergrationTests_t1691354350 * __this, String_t* ___url0, int32_t ___timeout1, int32_t ___pause2, StringU5BU5D_t1642385972* ___channels3, bool ___resumeOnReconnect4, bool ___ssl5, String_t* ___testName6, String_t* ___expectedMessage7, String_t* ___expectedChannels8, bool ___isError9, bool ___isTimeout10, bool ___asObject11, int64_t ___timetoken12, int32_t ___crt13, int32_t ___respType14, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::ErrorCallbackCommonExceptionHandlerError(PubNubMessaging.Core.PubnubClientError)
extern "C"  void CommonIntergrationTests_ErrorCallbackCommonExceptionHandlerError_m2702500557 (CommonIntergrationTests_t1691354350 * __this, PubnubClientError_t110317921 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::UserCallbackCommonExceptionHandler(System.String)
extern "C"  void CommonIntergrationTests_UserCallbackCommonExceptionHandler_m35853516 (CommonIntergrationTests_t1691354350 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::UserCallbackCommonExceptionHandler(System.Object)
extern "C"  void CommonIntergrationTests_UserCallbackCommonExceptionHandler_m258177430 (CommonIntergrationTests_t1691354350 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::DisconnectCallbackCommonExceptionHandler(System.String)
extern "C"  void CommonIntergrationTests_DisconnectCallbackCommonExceptionHandler_m1867468551 (CommonIntergrationTests_t1691354350 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::ConnectCallbackCommonExceptionHandler(System.String)
extern "C"  void CommonIntergrationTests_ConnectCallbackCommonExceptionHandler_m3129887901 (CommonIntergrationTests_t1691354350 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::ConnectCallbackCommonExceptionHandler(System.Object)
extern "C"  void CommonIntergrationTests_ConnectCallbackCommonExceptionHandler_m1087822457 (CommonIntergrationTests_t1691354350 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::TestCoroutineBounce(System.String,System.Int32,System.Int32,System.String[],System.Boolean,System.Boolean,System.String,System.String,System.String,System.Boolean,System.Boolean,System.Boolean,System.Int64,PubNubMessaging.Core.CurrentRequestType,PubNubMessaging.Core.ResponseType)
extern "C"  void CommonIntergrationTests_TestCoroutineBounce_m3058796860 (CommonIntergrationTests_t1691354350 * __this, String_t* ___url0, int32_t ___timeout1, int32_t ___pause2, StringU5BU5D_t1642385972* ___channels3, bool ___resumeOnReconnect4, bool ___ssl5, String_t* ___testName6, String_t* ___expectedMessage7, String_t* ___expectedChannels8, bool ___isError9, bool ___isTimeout10, bool ___asObject11, int64_t ___timetoken12, int32_t ___crt13, int32_t ___respType14, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests::.cctor()
extern "C"  void CommonIntergrationTests__cctor_m4079686373 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests::<ParseFields>m__0(System.Object)
extern "C"  Il2CppObject * CommonIntergrationTests_U3CParseFieldsU3Em__0_m2020830966 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
