﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoGlobalHereNowAndParse>c__IteratorA
struct U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_t2341681674;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoGlobalHereNowAndParse>c__IteratorA::.ctor()
extern "C"  void U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA__ctor_m345321473 (U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_t2341681674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoGlobalHereNowAndParse>c__IteratorA::MoveNext()
extern "C"  bool U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_MoveNext_m1339648459 (U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_t2341681674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoGlobalHereNowAndParse>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m827178691 (U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_t2341681674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoGlobalHereNowAndParse>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m2227956155 (U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_t2341681674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoGlobalHereNowAndParse>c__IteratorA::Dispose()
extern "C"  void U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_Dispose_m3874829004 (U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_t2341681674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenDoGlobalHereNowAndParse>c__IteratorA::Reset()
extern "C"  void U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_Reset_m527627778 (U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_t2341681674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
