﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunProcessResponse>c__Iterator13
struct U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunProcessResponse>c__Iterator13::.ctor()
extern "C"  void U3CTestCoroutineRunProcessResponseU3Ec__Iterator13__ctor_m2068227069 (U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunProcessResponse>c__Iterator13::MoveNext()
extern "C"  bool U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_MoveNext_m3481783911 (U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunProcessResponse>c__Iterator13::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2059305095 (U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunProcessResponse>c__Iterator13::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m573980751 (U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunProcessResponse>c__Iterator13::Dispose()
extern "C"  void U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_Dispose_m140319110 (U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunProcessResponse>c__Iterator13::Reset()
extern "C"  void U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_Reset_m343011580 (U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
