﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Tests.CommonIntergrationTests
struct CommonIntergrationTests_t1691354350;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1
struct  U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1::testName
	String_t* ___testName_0;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1::ssl
	bool ___ssl_1;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1::withCipher
	bool ___withCipher_2;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1::<channel>__0
	String_t* ___U3CchannelU3E__0_3;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1::asObject
	bool ___asObject_4;
	// PubNubMessaging.Tests.CommonIntergrationTests PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1::<commonPublish>__1
	CommonIntergrationTests_t1691354350 * ___U3CcommonPublishU3E__1_5;
	// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1::message
	Il2CppObject * ___message_6;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1::expectedStringResponse
	String_t* ___expectedStringResponse_7;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1::matchExpectedStringResponse
	bool ___matchExpectedStringResponse_8;
	// PubNubMessaging.Tests.CommonIntergrationTests PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1::$this
	CommonIntergrationTests_t1691354350 * ___U24this_9;
	// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1::$current
	Il2CppObject * ___U24current_10;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1::$disposing
	bool ___U24disposing_11;
	// System.Int32 PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_testName_0() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653, ___testName_0)); }
	inline String_t* get_testName_0() const { return ___testName_0; }
	inline String_t** get_address_of_testName_0() { return &___testName_0; }
	inline void set_testName_0(String_t* value)
	{
		___testName_0 = value;
		Il2CppCodeGenWriteBarrier(&___testName_0, value);
	}

	inline static int32_t get_offset_of_ssl_1() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653, ___ssl_1)); }
	inline bool get_ssl_1() const { return ___ssl_1; }
	inline bool* get_address_of_ssl_1() { return &___ssl_1; }
	inline void set_ssl_1(bool value)
	{
		___ssl_1 = value;
	}

	inline static int32_t get_offset_of_withCipher_2() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653, ___withCipher_2)); }
	inline bool get_withCipher_2() const { return ___withCipher_2; }
	inline bool* get_address_of_withCipher_2() { return &___withCipher_2; }
	inline void set_withCipher_2(bool value)
	{
		___withCipher_2 = value;
	}

	inline static int32_t get_offset_of_U3CchannelU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653, ___U3CchannelU3E__0_3)); }
	inline String_t* get_U3CchannelU3E__0_3() const { return ___U3CchannelU3E__0_3; }
	inline String_t** get_address_of_U3CchannelU3E__0_3() { return &___U3CchannelU3E__0_3; }
	inline void set_U3CchannelU3E__0_3(String_t* value)
	{
		___U3CchannelU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchannelU3E__0_3, value);
	}

	inline static int32_t get_offset_of_asObject_4() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653, ___asObject_4)); }
	inline bool get_asObject_4() const { return ___asObject_4; }
	inline bool* get_address_of_asObject_4() { return &___asObject_4; }
	inline void set_asObject_4(bool value)
	{
		___asObject_4 = value;
	}

	inline static int32_t get_offset_of_U3CcommonPublishU3E__1_5() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653, ___U3CcommonPublishU3E__1_5)); }
	inline CommonIntergrationTests_t1691354350 * get_U3CcommonPublishU3E__1_5() const { return ___U3CcommonPublishU3E__1_5; }
	inline CommonIntergrationTests_t1691354350 ** get_address_of_U3CcommonPublishU3E__1_5() { return &___U3CcommonPublishU3E__1_5; }
	inline void set_U3CcommonPublishU3E__1_5(CommonIntergrationTests_t1691354350 * value)
	{
		___U3CcommonPublishU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcommonPublishU3E__1_5, value);
	}

	inline static int32_t get_offset_of_message_6() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653, ___message_6)); }
	inline Il2CppObject * get_message_6() const { return ___message_6; }
	inline Il2CppObject ** get_address_of_message_6() { return &___message_6; }
	inline void set_message_6(Il2CppObject * value)
	{
		___message_6 = value;
		Il2CppCodeGenWriteBarrier(&___message_6, value);
	}

	inline static int32_t get_offset_of_expectedStringResponse_7() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653, ___expectedStringResponse_7)); }
	inline String_t* get_expectedStringResponse_7() const { return ___expectedStringResponse_7; }
	inline String_t** get_address_of_expectedStringResponse_7() { return &___expectedStringResponse_7; }
	inline void set_expectedStringResponse_7(String_t* value)
	{
		___expectedStringResponse_7 = value;
		Il2CppCodeGenWriteBarrier(&___expectedStringResponse_7, value);
	}

	inline static int32_t get_offset_of_matchExpectedStringResponse_8() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653, ___matchExpectedStringResponse_8)); }
	inline bool get_matchExpectedStringResponse_8() const { return ___matchExpectedStringResponse_8; }
	inline bool* get_address_of_matchExpectedStringResponse_8() { return &___matchExpectedStringResponse_8; }
	inline void set_matchExpectedStringResponse_8(bool value)
	{
		___matchExpectedStringResponse_8 = value;
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653, ___U24this_9)); }
	inline CommonIntergrationTests_t1691354350 * get_U24this_9() const { return ___U24this_9; }
	inline CommonIntergrationTests_t1691354350 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(CommonIntergrationTests_t1691354350 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_9, value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653, ___U24current_10)); }
	inline Il2CppObject * get_U24current_10() const { return ___U24current_10; }
	inline Il2CppObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(Il2CppObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_10, value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
