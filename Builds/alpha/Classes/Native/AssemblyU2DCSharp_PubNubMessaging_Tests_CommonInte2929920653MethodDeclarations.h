﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1
struct U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1::.ctor()
extern "C"  void U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1__ctor_m3950837662 (U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1::MoveNext()
extern "C"  bool U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_MoveNext_m4275169402 (U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1730204582 (U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m55028014 (U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1::Dispose()
extern "C"  void U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_Dispose_m4075168367 (U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenPublishAndParse>c__Iterator1::Reset()
extern "C"  void U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_Reset_m448564165 (U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
