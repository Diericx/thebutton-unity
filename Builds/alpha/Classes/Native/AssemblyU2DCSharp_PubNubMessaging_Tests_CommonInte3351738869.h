﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0
struct U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15
struct  U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::testName
	String_t* ___testName_0;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::bAddChannel
	bool ___bAddChannel_1;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::cg
	String_t* ___cg_2;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::channel
	String_t* ___channel_3;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::bGetChannel
	bool ___bGetChannel_4;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::pubMessage
	String_t* ___pubMessage_5;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::bSubMessage
	bool ___bSubMessage_6;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::bSubConnected
	bool ___bSubConnected_7;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::state
	String_t* ___state_8;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::bSetState
	bool ___bSetState_9;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::bGetState
	bool ___bGetState_10;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::uuid
	String_t* ___uuid_11;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::bHereNow
	bool ___bHereNow_12;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::bHereNowState
	bool ___bHereNowState_13;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::bSetUserState2
	bool ___bSetUserState2_14;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::bGetUserState2
	bool ___bGetUserState2_15;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::bUnsub
	bool ___bUnsub_16;
	// PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0 PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::<>f__ref$0
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773 * ___U3CU3Ef__refU240_17;

public:
	inline static int32_t get_offset_of_testName_0() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869, ___testName_0)); }
	inline String_t* get_testName_0() const { return ___testName_0; }
	inline String_t** get_address_of_testName_0() { return &___testName_0; }
	inline void set_testName_0(String_t* value)
	{
		___testName_0 = value;
		Il2CppCodeGenWriteBarrier(&___testName_0, value);
	}

	inline static int32_t get_offset_of_bAddChannel_1() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869, ___bAddChannel_1)); }
	inline bool get_bAddChannel_1() const { return ___bAddChannel_1; }
	inline bool* get_address_of_bAddChannel_1() { return &___bAddChannel_1; }
	inline void set_bAddChannel_1(bool value)
	{
		___bAddChannel_1 = value;
	}

	inline static int32_t get_offset_of_cg_2() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869, ___cg_2)); }
	inline String_t* get_cg_2() const { return ___cg_2; }
	inline String_t** get_address_of_cg_2() { return &___cg_2; }
	inline void set_cg_2(String_t* value)
	{
		___cg_2 = value;
		Il2CppCodeGenWriteBarrier(&___cg_2, value);
	}

	inline static int32_t get_offset_of_channel_3() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869, ___channel_3)); }
	inline String_t* get_channel_3() const { return ___channel_3; }
	inline String_t** get_address_of_channel_3() { return &___channel_3; }
	inline void set_channel_3(String_t* value)
	{
		___channel_3 = value;
		Il2CppCodeGenWriteBarrier(&___channel_3, value);
	}

	inline static int32_t get_offset_of_bGetChannel_4() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869, ___bGetChannel_4)); }
	inline bool get_bGetChannel_4() const { return ___bGetChannel_4; }
	inline bool* get_address_of_bGetChannel_4() { return &___bGetChannel_4; }
	inline void set_bGetChannel_4(bool value)
	{
		___bGetChannel_4 = value;
	}

	inline static int32_t get_offset_of_pubMessage_5() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869, ___pubMessage_5)); }
	inline String_t* get_pubMessage_5() const { return ___pubMessage_5; }
	inline String_t** get_address_of_pubMessage_5() { return &___pubMessage_5; }
	inline void set_pubMessage_5(String_t* value)
	{
		___pubMessage_5 = value;
		Il2CppCodeGenWriteBarrier(&___pubMessage_5, value);
	}

	inline static int32_t get_offset_of_bSubMessage_6() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869, ___bSubMessage_6)); }
	inline bool get_bSubMessage_6() const { return ___bSubMessage_6; }
	inline bool* get_address_of_bSubMessage_6() { return &___bSubMessage_6; }
	inline void set_bSubMessage_6(bool value)
	{
		___bSubMessage_6 = value;
	}

	inline static int32_t get_offset_of_bSubConnected_7() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869, ___bSubConnected_7)); }
	inline bool get_bSubConnected_7() const { return ___bSubConnected_7; }
	inline bool* get_address_of_bSubConnected_7() { return &___bSubConnected_7; }
	inline void set_bSubConnected_7(bool value)
	{
		___bSubConnected_7 = value;
	}

	inline static int32_t get_offset_of_state_8() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869, ___state_8)); }
	inline String_t* get_state_8() const { return ___state_8; }
	inline String_t** get_address_of_state_8() { return &___state_8; }
	inline void set_state_8(String_t* value)
	{
		___state_8 = value;
		Il2CppCodeGenWriteBarrier(&___state_8, value);
	}

	inline static int32_t get_offset_of_bSetState_9() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869, ___bSetState_9)); }
	inline bool get_bSetState_9() const { return ___bSetState_9; }
	inline bool* get_address_of_bSetState_9() { return &___bSetState_9; }
	inline void set_bSetState_9(bool value)
	{
		___bSetState_9 = value;
	}

	inline static int32_t get_offset_of_bGetState_10() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869, ___bGetState_10)); }
	inline bool get_bGetState_10() const { return ___bGetState_10; }
	inline bool* get_address_of_bGetState_10() { return &___bGetState_10; }
	inline void set_bGetState_10(bool value)
	{
		___bGetState_10 = value;
	}

	inline static int32_t get_offset_of_uuid_11() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869, ___uuid_11)); }
	inline String_t* get_uuid_11() const { return ___uuid_11; }
	inline String_t** get_address_of_uuid_11() { return &___uuid_11; }
	inline void set_uuid_11(String_t* value)
	{
		___uuid_11 = value;
		Il2CppCodeGenWriteBarrier(&___uuid_11, value);
	}

	inline static int32_t get_offset_of_bHereNow_12() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869, ___bHereNow_12)); }
	inline bool get_bHereNow_12() const { return ___bHereNow_12; }
	inline bool* get_address_of_bHereNow_12() { return &___bHereNow_12; }
	inline void set_bHereNow_12(bool value)
	{
		___bHereNow_12 = value;
	}

	inline static int32_t get_offset_of_bHereNowState_13() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869, ___bHereNowState_13)); }
	inline bool get_bHereNowState_13() const { return ___bHereNowState_13; }
	inline bool* get_address_of_bHereNowState_13() { return &___bHereNowState_13; }
	inline void set_bHereNowState_13(bool value)
	{
		___bHereNowState_13 = value;
	}

	inline static int32_t get_offset_of_bSetUserState2_14() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869, ___bSetUserState2_14)); }
	inline bool get_bSetUserState2_14() const { return ___bSetUserState2_14; }
	inline bool* get_address_of_bSetUserState2_14() { return &___bSetUserState2_14; }
	inline void set_bSetUserState2_14(bool value)
	{
		___bSetUserState2_14 = value;
	}

	inline static int32_t get_offset_of_bGetUserState2_15() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869, ___bGetUserState2_15)); }
	inline bool get_bGetUserState2_15() const { return ___bGetUserState2_15; }
	inline bool* get_address_of_bGetUserState2_15() { return &___bGetUserState2_15; }
	inline void set_bGetUserState2_15(bool value)
	{
		___bGetUserState2_15 = value;
	}

	inline static int32_t get_offset_of_bUnsub_16() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869, ___bUnsub_16)); }
	inline bool get_bUnsub_16() const { return ___bUnsub_16; }
	inline bool* get_address_of_bUnsub_16() { return &___bUnsub_16; }
	inline void set_bUnsub_16(bool value)
	{
		___bUnsub_16 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_17() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869, ___U3CU3Ef__refU240_17)); }
	inline U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773 * get_U3CU3Ef__refU240_17() const { return ___U3CU3Ef__refU240_17; }
	inline U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773 ** get_address_of_U3CU3Ef__refU240_17() { return &___U3CU3Ef__refU240_17; }
	inline void set_U3CU3Ef__refU240_17(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773 * value)
	{
		___U3CU3Ef__refU240_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU240_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
