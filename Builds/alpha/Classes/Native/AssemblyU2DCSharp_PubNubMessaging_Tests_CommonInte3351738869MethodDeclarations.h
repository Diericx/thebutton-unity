﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15
struct U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::.ctor()
extern "C"  void U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15__ctor_m783582290 (U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::<>m__0(System.String)
extern "C"  void U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_U3CU3Em__0_m2305509501 (U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::<>m__1(System.String)
extern "C"  void U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_U3CU3Em__1_m1698198968 (U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::<>m__2(System.String)
extern "C"  void U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_U3CU3Em__2_m2269397747 (U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869 * __this, String_t* ___retConnect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::<>m__3(System.String)
extern "C"  void U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_U3CU3Em__3_m1662087214 (U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::<>m__4(System.String)
extern "C"  void U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_U3CU3Em__4_m2054633617 (U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::<>m__5(System.String)
extern "C"  void U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_U3CU3Em__5_m1447323084 (U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::<>m__6(System.String)
extern "C"  void U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_U3CU3Em__6_m2018521863 (U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::<>m__7(System.String)
extern "C"  void U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_U3CU3Em__7_m1411211330 (U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::<>m__8(System.String)
extern "C"  void U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_U3CU3Em__8_m1522741925 (U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869 * __this, String_t* ___result20, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::<>m__9(System.String)
extern "C"  void U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_U3CU3Em__9_m915431392 (U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869 * __this, String_t* ___pub0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::<>m__A(System.String)
extern "C"  void U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_U3CU3Em__A_m1330217960 (U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869 * __this, String_t* ___pub0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15::<>m__B(System.String)
extern "C"  void U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_U3CU3Em__B_m1217119139 (U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869 * __this, String_t* ___pub0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
