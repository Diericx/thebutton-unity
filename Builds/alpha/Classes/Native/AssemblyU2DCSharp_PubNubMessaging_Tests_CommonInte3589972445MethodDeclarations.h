﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<DoTimeAndParse>c__Iterator2
struct U3CDoTimeAndParseU3Ec__Iterator2_t3589972445;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoTimeAndParse>c__Iterator2::.ctor()
extern "C"  void U3CDoTimeAndParseU3Ec__Iterator2__ctor_m484029038 (U3CDoTimeAndParseU3Ec__Iterator2_t3589972445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoTimeAndParse>c__Iterator2::MoveNext()
extern "C"  bool U3CDoTimeAndParseU3Ec__Iterator2_MoveNext_m2588772586 (U3CDoTimeAndParseU3Ec__Iterator2_t3589972445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoTimeAndParse>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoTimeAndParseU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2184557910 (U3CDoTimeAndParseU3Ec__Iterator2_t3589972445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoTimeAndParse>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoTimeAndParseU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1885219918 (U3CDoTimeAndParseU3Ec__Iterator2_t3589972445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoTimeAndParse>c__Iterator2::Dispose()
extern "C"  void U3CDoTimeAndParseU3Ec__Iterator2_Dispose_m3438953631 (U3CDoTimeAndParseU3Ec__Iterator2_t3589972445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoTimeAndParse>c__Iterator2::Reset()
extern "C"  void U3CDoTimeAndParseU3Ec__Iterator2_Reset_m2448524917 (U3CDoTimeAndParseU3Ec__Iterator2_t3589972445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
