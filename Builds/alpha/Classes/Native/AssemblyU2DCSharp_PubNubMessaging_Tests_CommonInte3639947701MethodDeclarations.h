﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<DoNotSubscribedTest>c__IteratorF
struct U3CDoNotSubscribedTestU3Ec__IteratorF_t3639947701;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoNotSubscribedTest>c__IteratorF::.ctor()
extern "C"  void U3CDoNotSubscribedTestU3Ec__IteratorF__ctor_m2726486812 (U3CDoNotSubscribedTestU3Ec__IteratorF_t3639947701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoNotSubscribedTest>c__IteratorF::MoveNext()
extern "C"  bool U3CDoNotSubscribedTestU3Ec__IteratorF_MoveNext_m2654322312 (U3CDoNotSubscribedTestU3Ec__IteratorF_t3639947701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoNotSubscribedTest>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoNotSubscribedTestU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2855174214 (U3CDoNotSubscribedTestU3Ec__IteratorF_t3639947701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoNotSubscribedTest>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoNotSubscribedTestU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m3572694686 (U3CDoNotSubscribedTestU3Ec__IteratorF_t3639947701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoNotSubscribedTest>c__IteratorF::Dispose()
extern "C"  void U3CDoNotSubscribedTestU3Ec__IteratorF_Dispose_m3971491351 (U3CDoNotSubscribedTestU3Ec__IteratorF_t3639947701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoNotSubscribedTest>c__IteratorF::Reset()
extern "C"  void U3CDoNotSubscribedTestU3Ec__IteratorF_Reset_m2569579001 (U3CDoNotSubscribedTestU3Ec__IteratorF_t3639947701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
