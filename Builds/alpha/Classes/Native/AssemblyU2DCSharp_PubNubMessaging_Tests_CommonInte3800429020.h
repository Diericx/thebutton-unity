﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PubNubMessaging.Tests.CommonIntergrationTests
struct CommonIntergrationTests_t1691354350;
// System.String
struct String_t;
// PubNubMessaging.Core.Pubnub
struct Pubnub_t2451529532;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribe>c__IteratorD
struct  U3CDoSubscribeU3Ec__IteratorD_t3800429020  : public Il2CppObject
{
public:
	// PubNubMessaging.Tests.CommonIntergrationTests PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribe>c__IteratorD::<commonSubscribe>__0
	CommonIntergrationTests_t1691354350 * ___U3CcommonSubscribeU3E__0_0;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribe>c__IteratorD::testName
	String_t* ___testName_1;
	// PubNubMessaging.Core.Pubnub PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribe>c__IteratorD::pn
	Pubnub_t2451529532 * ___pn_2;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribe>c__IteratorD::channel
	String_t* ___channel_3;
	// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribe>c__IteratorD::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribe>c__IteratorD::$disposing
	bool ___U24disposing_5;
	// System.Int32 PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribe>c__IteratorD::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CcommonSubscribeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoSubscribeU3Ec__IteratorD_t3800429020, ___U3CcommonSubscribeU3E__0_0)); }
	inline CommonIntergrationTests_t1691354350 * get_U3CcommonSubscribeU3E__0_0() const { return ___U3CcommonSubscribeU3E__0_0; }
	inline CommonIntergrationTests_t1691354350 ** get_address_of_U3CcommonSubscribeU3E__0_0() { return &___U3CcommonSubscribeU3E__0_0; }
	inline void set_U3CcommonSubscribeU3E__0_0(CommonIntergrationTests_t1691354350 * value)
	{
		___U3CcommonSubscribeU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcommonSubscribeU3E__0_0, value);
	}

	inline static int32_t get_offset_of_testName_1() { return static_cast<int32_t>(offsetof(U3CDoSubscribeU3Ec__IteratorD_t3800429020, ___testName_1)); }
	inline String_t* get_testName_1() const { return ___testName_1; }
	inline String_t** get_address_of_testName_1() { return &___testName_1; }
	inline void set_testName_1(String_t* value)
	{
		___testName_1 = value;
		Il2CppCodeGenWriteBarrier(&___testName_1, value);
	}

	inline static int32_t get_offset_of_pn_2() { return static_cast<int32_t>(offsetof(U3CDoSubscribeU3Ec__IteratorD_t3800429020, ___pn_2)); }
	inline Pubnub_t2451529532 * get_pn_2() const { return ___pn_2; }
	inline Pubnub_t2451529532 ** get_address_of_pn_2() { return &___pn_2; }
	inline void set_pn_2(Pubnub_t2451529532 * value)
	{
		___pn_2 = value;
		Il2CppCodeGenWriteBarrier(&___pn_2, value);
	}

	inline static int32_t get_offset_of_channel_3() { return static_cast<int32_t>(offsetof(U3CDoSubscribeU3Ec__IteratorD_t3800429020, ___channel_3)); }
	inline String_t* get_channel_3() const { return ___channel_3; }
	inline String_t** get_address_of_channel_3() { return &___channel_3; }
	inline void set_channel_3(String_t* value)
	{
		___channel_3 = value;
		Il2CppCodeGenWriteBarrier(&___channel_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CDoSubscribeU3Ec__IteratorD_t3800429020, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CDoSubscribeU3Ec__IteratorD_t3800429020, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CDoSubscribeU3Ec__IteratorD_t3800429020, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
