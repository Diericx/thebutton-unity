﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribe>c__IteratorD
struct U3CDoSubscribeU3Ec__IteratorD_t3800429020;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribe>c__IteratorD::.ctor()
extern "C"  void U3CDoSubscribeU3Ec__IteratorD__ctor_m586245121 (U3CDoSubscribeU3Ec__IteratorD_t3800429020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribe>c__IteratorD::MoveNext()
extern "C"  bool U3CDoSubscribeU3Ec__IteratorD_MoveNext_m2547422915 (U3CDoSubscribeU3Ec__IteratorD_t3800429020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribe>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoSubscribeU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3843537815 (U3CDoSubscribeU3Ec__IteratorD_t3800429020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribe>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoSubscribeU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m3848643375 (U3CDoSubscribeU3Ec__IteratorD_t3800429020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribe>c__IteratorD::Dispose()
extern "C"  void U3CDoSubscribeU3Ec__IteratorD_Dispose_m2570966654 (U3CDoSubscribeU3Ec__IteratorD_t3800429020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribe>c__IteratorD::Reset()
extern "C"  void U3CDoSubscribeU3Ec__IteratorD_Reset_m1465579516 (U3CDoSubscribeU3Ec__IteratorD_t3800429020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
