﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Tests.CommonIntergrationTests
struct CommonIntergrationTests_t1691354350;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.CommonIntergrationTests/<DoUnsubscribeTest>c__Iterator10
struct  U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoUnsubscribeTest>c__Iterator10::testName
	String_t* ___testName_0;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoUnsubscribeTest>c__Iterator10::ssl
	bool ___ssl_1;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoUnsubscribeTest>c__Iterator10::<channel>__0
	String_t* ___U3CchannelU3E__0_2;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoUnsubscribeTest>c__Iterator10::asObject
	bool ___asObject_3;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoUnsubscribeTest>c__Iterator10::isPresence
	bool ___isPresence_4;
	// PubNubMessaging.Tests.CommonIntergrationTests PubNubMessaging.Tests.CommonIntergrationTests/<DoUnsubscribeTest>c__Iterator10::$this
	CommonIntergrationTests_t1691354350 * ___U24this_5;
	// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoUnsubscribeTest>c__Iterator10::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoUnsubscribeTest>c__Iterator10::$disposing
	bool ___U24disposing_7;
	// System.Int32 PubNubMessaging.Tests.CommonIntergrationTests/<DoUnsubscribeTest>c__Iterator10::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_testName_0() { return static_cast<int32_t>(offsetof(U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012, ___testName_0)); }
	inline String_t* get_testName_0() const { return ___testName_0; }
	inline String_t** get_address_of_testName_0() { return &___testName_0; }
	inline void set_testName_0(String_t* value)
	{
		___testName_0 = value;
		Il2CppCodeGenWriteBarrier(&___testName_0, value);
	}

	inline static int32_t get_offset_of_ssl_1() { return static_cast<int32_t>(offsetof(U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012, ___ssl_1)); }
	inline bool get_ssl_1() const { return ___ssl_1; }
	inline bool* get_address_of_ssl_1() { return &___ssl_1; }
	inline void set_ssl_1(bool value)
	{
		___ssl_1 = value;
	}

	inline static int32_t get_offset_of_U3CchannelU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012, ___U3CchannelU3E__0_2)); }
	inline String_t* get_U3CchannelU3E__0_2() const { return ___U3CchannelU3E__0_2; }
	inline String_t** get_address_of_U3CchannelU3E__0_2() { return &___U3CchannelU3E__0_2; }
	inline void set_U3CchannelU3E__0_2(String_t* value)
	{
		___U3CchannelU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchannelU3E__0_2, value);
	}

	inline static int32_t get_offset_of_asObject_3() { return static_cast<int32_t>(offsetof(U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012, ___asObject_3)); }
	inline bool get_asObject_3() const { return ___asObject_3; }
	inline bool* get_address_of_asObject_3() { return &___asObject_3; }
	inline void set_asObject_3(bool value)
	{
		___asObject_3 = value;
	}

	inline static int32_t get_offset_of_isPresence_4() { return static_cast<int32_t>(offsetof(U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012, ___isPresence_4)); }
	inline bool get_isPresence_4() const { return ___isPresence_4; }
	inline bool* get_address_of_isPresence_4() { return &___isPresence_4; }
	inline void set_isPresence_4(bool value)
	{
		___isPresence_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012, ___U24this_5)); }
	inline CommonIntergrationTests_t1691354350 * get_U24this_5() const { return ___U24this_5; }
	inline CommonIntergrationTests_t1691354350 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(CommonIntergrationTests_t1691354350 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
