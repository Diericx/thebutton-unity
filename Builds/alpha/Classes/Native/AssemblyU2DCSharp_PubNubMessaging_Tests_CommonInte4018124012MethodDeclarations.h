﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<DoUnsubscribeTest>c__Iterator10
struct U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoUnsubscribeTest>c__Iterator10::.ctor()
extern "C"  void U3CDoUnsubscribeTestU3Ec__Iterator10__ctor_m371045877 (U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoUnsubscribeTest>c__Iterator10::MoveNext()
extern "C"  bool U3CDoUnsubscribeTestU3Ec__Iterator10_MoveNext_m2619226511 (U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoUnsubscribeTest>c__Iterator10::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoUnsubscribeTestU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3992640367 (U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoUnsubscribeTest>c__Iterator10::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoUnsubscribeTestU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m681018023 (U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoUnsubscribeTest>c__Iterator10::Dispose()
extern "C"  void U3CDoUnsubscribeTestU3Ec__Iterator10_Dispose_m2402820318 (U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoUnsubscribeTest>c__Iterator10::Reset()
extern "C"  void U3CDoUnsubscribeTestU3Ec__Iterator10_Reset_m1165607908 (U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
