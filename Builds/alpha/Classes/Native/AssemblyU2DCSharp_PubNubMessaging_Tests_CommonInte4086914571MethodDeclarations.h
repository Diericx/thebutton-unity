﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<DoPresenceThenSubscribeAndParse>c__Iterator12
struct U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_t4086914571;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoPresenceThenSubscribeAndParse>c__Iterator12::.ctor()
extern "C"  void U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12__ctor_m4004053544 (U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_t4086914571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPresenceThenSubscribeAndParse>c__Iterator12::MoveNext()
extern "C"  bool U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_MoveNext_m3364322472 (U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_t4086914571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoPresenceThenSubscribeAndParse>c__Iterator12::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2343207784 (U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_t4086914571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoPresenceThenSubscribeAndParse>c__Iterator12::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m4002665312 (U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_t4086914571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoPresenceThenSubscribeAndParse>c__Iterator12::Dispose()
extern "C"  void U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_Dispose_m1120918953 (U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_t4086914571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoPresenceThenSubscribeAndParse>c__Iterator12::Reset()
extern "C"  void U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_Reset_m544286079 (U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_t4086914571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
