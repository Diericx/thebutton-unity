﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Tests.CommonIntergrationTests
struct CommonIntergrationTests_t1691354350;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenHereNowAndParse>c__IteratorC
struct  U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenHereNowAndParse>c__IteratorC::testName
	String_t* ___testName_0;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenHereNowAndParse>c__IteratorC::ssl
	bool ___ssl_1;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenHereNowAndParse>c__IteratorC::<channel>__0
	String_t* ___U3CchannelU3E__0_2;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenHereNowAndParse>c__IteratorC::<matchUUID>__1
	String_t* ___U3CmatchUUIDU3E__1_3;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenHereNowAndParse>c__IteratorC::customUUID
	String_t* ___customUUID_4;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenHereNowAndParse>c__IteratorC::<state>__2
	String_t* ___U3CstateU3E__2_5;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenHereNowAndParse>c__IteratorC::doWithState
	bool ___doWithState_6;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenHereNowAndParse>c__IteratorC::parseAsString
	bool ___parseAsString_7;
	// PubNubMessaging.Tests.CommonIntergrationTests PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenHereNowAndParse>c__IteratorC::$this
	CommonIntergrationTests_t1691354350 * ___U24this_8;
	// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenHereNowAndParse>c__IteratorC::$current
	Il2CppObject * ___U24current_9;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenHereNowAndParse>c__IteratorC::$disposing
	bool ___U24disposing_10;
	// System.Int32 PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenHereNowAndParse>c__IteratorC::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_testName_0() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886, ___testName_0)); }
	inline String_t* get_testName_0() const { return ___testName_0; }
	inline String_t** get_address_of_testName_0() { return &___testName_0; }
	inline void set_testName_0(String_t* value)
	{
		___testName_0 = value;
		Il2CppCodeGenWriteBarrier(&___testName_0, value);
	}

	inline static int32_t get_offset_of_ssl_1() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886, ___ssl_1)); }
	inline bool get_ssl_1() const { return ___ssl_1; }
	inline bool* get_address_of_ssl_1() { return &___ssl_1; }
	inline void set_ssl_1(bool value)
	{
		___ssl_1 = value;
	}

	inline static int32_t get_offset_of_U3CchannelU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886, ___U3CchannelU3E__0_2)); }
	inline String_t* get_U3CchannelU3E__0_2() const { return ___U3CchannelU3E__0_2; }
	inline String_t** get_address_of_U3CchannelU3E__0_2() { return &___U3CchannelU3E__0_2; }
	inline void set_U3CchannelU3E__0_2(String_t* value)
	{
		___U3CchannelU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchannelU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3CmatchUUIDU3E__1_3() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886, ___U3CmatchUUIDU3E__1_3)); }
	inline String_t* get_U3CmatchUUIDU3E__1_3() const { return ___U3CmatchUUIDU3E__1_3; }
	inline String_t** get_address_of_U3CmatchUUIDU3E__1_3() { return &___U3CmatchUUIDU3E__1_3; }
	inline void set_U3CmatchUUIDU3E__1_3(String_t* value)
	{
		___U3CmatchUUIDU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmatchUUIDU3E__1_3, value);
	}

	inline static int32_t get_offset_of_customUUID_4() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886, ___customUUID_4)); }
	inline String_t* get_customUUID_4() const { return ___customUUID_4; }
	inline String_t** get_address_of_customUUID_4() { return &___customUUID_4; }
	inline void set_customUUID_4(String_t* value)
	{
		___customUUID_4 = value;
		Il2CppCodeGenWriteBarrier(&___customUUID_4, value);
	}

	inline static int32_t get_offset_of_U3CstateU3E__2_5() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886, ___U3CstateU3E__2_5)); }
	inline String_t* get_U3CstateU3E__2_5() const { return ___U3CstateU3E__2_5; }
	inline String_t** get_address_of_U3CstateU3E__2_5() { return &___U3CstateU3E__2_5; }
	inline void set_U3CstateU3E__2_5(String_t* value)
	{
		___U3CstateU3E__2_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstateU3E__2_5, value);
	}

	inline static int32_t get_offset_of_doWithState_6() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886, ___doWithState_6)); }
	inline bool get_doWithState_6() const { return ___doWithState_6; }
	inline bool* get_address_of_doWithState_6() { return &___doWithState_6; }
	inline void set_doWithState_6(bool value)
	{
		___doWithState_6 = value;
	}

	inline static int32_t get_offset_of_parseAsString_7() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886, ___parseAsString_7)); }
	inline bool get_parseAsString_7() const { return ___parseAsString_7; }
	inline bool* get_address_of_parseAsString_7() { return &___parseAsString_7; }
	inline void set_parseAsString_7(bool value)
	{
		___parseAsString_7 = value;
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886, ___U24this_8)); }
	inline CommonIntergrationTests_t1691354350 * get_U24this_8() const { return ___U24this_8; }
	inline CommonIntergrationTests_t1691354350 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(CommonIntergrationTests_t1691354350 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_8, value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886, ___U24current_9)); }
	inline Il2CppObject * get_U24current_9() const { return ___U24current_9; }
	inline Il2CppObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(Il2CppObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_9, value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
