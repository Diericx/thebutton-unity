﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenHereNowAndParse>c__IteratorC
struct U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenHereNowAndParse>c__IteratorC::.ctor()
extern "C"  void U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC__ctor_m3494320311 (U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenHereNowAndParse>c__IteratorC::MoveNext()
extern "C"  bool U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_MoveNext_m83947289 (U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenHereNowAndParse>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3668754197 (U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenHereNowAndParse>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m399942237 (U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenHereNowAndParse>c__IteratorC::Dispose()
extern "C"  void U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_Dispose_m3940986636 (U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoSubscribeThenHereNowAndParse>c__IteratorC::Reset()
extern "C"  void U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_Reset_m3311206294 (U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
