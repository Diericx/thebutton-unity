﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9
struct U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9::.ctor()
extern "C"  void U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9__ctor_m2114737310 (U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9::MoveNext()
extern "C"  bool U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_MoveNext_m4182105978 (U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2507531686 (U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m2066983774 (U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9::Dispose()
extern "C"  void U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_Dispose_m104147215 (U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9::Reset()
extern "C"  void U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_Reset_m2294327909 (U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
