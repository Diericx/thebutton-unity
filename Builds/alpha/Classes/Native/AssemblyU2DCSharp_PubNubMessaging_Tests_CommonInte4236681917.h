﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Tests.CommonIntergrationTests
struct CommonIntergrationTests_t1691354350;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3230389896;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3
struct  U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::testName
	String_t* ___testName_0;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::ssl
	bool ___ssl_1;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::withCipher
	bool ___withCipher_2;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::<channel>__0
	String_t* ___U3CchannelU3E__0_3;
	// PubNubMessaging.Tests.CommonIntergrationTests PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::<commonPublish>__1
	CommonIntergrationTests_t1691354350 * ___U3CcommonPublishU3E__1_4;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::<storeInHistory>__2
	bool ___U3CstoreInHistoryU3E__2_5;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::noStore
	bool ___noStore_6;
	// System.Collections.Generic.IList`1<System.Object> PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::<fields>__3
	Il2CppObject* ___U3CfieldsU3E__3_7;
	// System.Int64 PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::<starttime>__4
	int64_t ___U3CstarttimeU3E__4_8;
	// System.Int64 PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::<midtime>__5
	int64_t ___U3CmidtimeU3E__5_9;
	// System.Int32 PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::<count>__6
	int32_t ___U3CcountU3E__6_10;
	// System.Int32 PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::numberOfMessages
	int32_t ___numberOfMessages_11;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::asObject
	bool ___asObject_12;
	// System.Object[] PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::messages
	ObjectU5BU5D_t3614634134* ___messages_13;
	// System.Object[] PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::$locvar0
	ObjectU5BU5D_t3614634134* ___U24locvar0_14;
	// System.Int32 PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::$locvar1
	int32_t ___U24locvar1_15;
	// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::<message>__7
	Il2CppObject * ___U3CmessageU3E__7_16;
	// System.Object[] PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::$locvar2
	ObjectU5BU5D_t3614634134* ___U24locvar2_17;
	// System.Int32 PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::$locvar3
	int32_t ___U24locvar3_18;
	// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::<message>__8
	Il2CppObject * ___U3CmessageU3E__8_19;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::isParamsTest
	bool ___isParamsTest_20;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::<passed>__9
	bool ___U3CpassedU3E__9_21;
	// PubNubMessaging.Tests.CommonIntergrationTests PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::$this
	CommonIntergrationTests_t1691354350 * ___U24this_22;
	// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::$current
	Il2CppObject * ___U24current_23;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::$disposing
	bool ___U24disposing_24;
	// System.Int32 PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::$PC
	int32_t ___U24PC_25;

public:
	inline static int32_t get_offset_of_testName_0() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___testName_0)); }
	inline String_t* get_testName_0() const { return ___testName_0; }
	inline String_t** get_address_of_testName_0() { return &___testName_0; }
	inline void set_testName_0(String_t* value)
	{
		___testName_0 = value;
		Il2CppCodeGenWriteBarrier(&___testName_0, value);
	}

	inline static int32_t get_offset_of_ssl_1() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___ssl_1)); }
	inline bool get_ssl_1() const { return ___ssl_1; }
	inline bool* get_address_of_ssl_1() { return &___ssl_1; }
	inline void set_ssl_1(bool value)
	{
		___ssl_1 = value;
	}

	inline static int32_t get_offset_of_withCipher_2() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___withCipher_2)); }
	inline bool get_withCipher_2() const { return ___withCipher_2; }
	inline bool* get_address_of_withCipher_2() { return &___withCipher_2; }
	inline void set_withCipher_2(bool value)
	{
		___withCipher_2 = value;
	}

	inline static int32_t get_offset_of_U3CchannelU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___U3CchannelU3E__0_3)); }
	inline String_t* get_U3CchannelU3E__0_3() const { return ___U3CchannelU3E__0_3; }
	inline String_t** get_address_of_U3CchannelU3E__0_3() { return &___U3CchannelU3E__0_3; }
	inline void set_U3CchannelU3E__0_3(String_t* value)
	{
		___U3CchannelU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchannelU3E__0_3, value);
	}

	inline static int32_t get_offset_of_U3CcommonPublishU3E__1_4() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___U3CcommonPublishU3E__1_4)); }
	inline CommonIntergrationTests_t1691354350 * get_U3CcommonPublishU3E__1_4() const { return ___U3CcommonPublishU3E__1_4; }
	inline CommonIntergrationTests_t1691354350 ** get_address_of_U3CcommonPublishU3E__1_4() { return &___U3CcommonPublishU3E__1_4; }
	inline void set_U3CcommonPublishU3E__1_4(CommonIntergrationTests_t1691354350 * value)
	{
		___U3CcommonPublishU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcommonPublishU3E__1_4, value);
	}

	inline static int32_t get_offset_of_U3CstoreInHistoryU3E__2_5() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___U3CstoreInHistoryU3E__2_5)); }
	inline bool get_U3CstoreInHistoryU3E__2_5() const { return ___U3CstoreInHistoryU3E__2_5; }
	inline bool* get_address_of_U3CstoreInHistoryU3E__2_5() { return &___U3CstoreInHistoryU3E__2_5; }
	inline void set_U3CstoreInHistoryU3E__2_5(bool value)
	{
		___U3CstoreInHistoryU3E__2_5 = value;
	}

	inline static int32_t get_offset_of_noStore_6() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___noStore_6)); }
	inline bool get_noStore_6() const { return ___noStore_6; }
	inline bool* get_address_of_noStore_6() { return &___noStore_6; }
	inline void set_noStore_6(bool value)
	{
		___noStore_6 = value;
	}

	inline static int32_t get_offset_of_U3CfieldsU3E__3_7() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___U3CfieldsU3E__3_7)); }
	inline Il2CppObject* get_U3CfieldsU3E__3_7() const { return ___U3CfieldsU3E__3_7; }
	inline Il2CppObject** get_address_of_U3CfieldsU3E__3_7() { return &___U3CfieldsU3E__3_7; }
	inline void set_U3CfieldsU3E__3_7(Il2CppObject* value)
	{
		___U3CfieldsU3E__3_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CfieldsU3E__3_7, value);
	}

	inline static int32_t get_offset_of_U3CstarttimeU3E__4_8() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___U3CstarttimeU3E__4_8)); }
	inline int64_t get_U3CstarttimeU3E__4_8() const { return ___U3CstarttimeU3E__4_8; }
	inline int64_t* get_address_of_U3CstarttimeU3E__4_8() { return &___U3CstarttimeU3E__4_8; }
	inline void set_U3CstarttimeU3E__4_8(int64_t value)
	{
		___U3CstarttimeU3E__4_8 = value;
	}

	inline static int32_t get_offset_of_U3CmidtimeU3E__5_9() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___U3CmidtimeU3E__5_9)); }
	inline int64_t get_U3CmidtimeU3E__5_9() const { return ___U3CmidtimeU3E__5_9; }
	inline int64_t* get_address_of_U3CmidtimeU3E__5_9() { return &___U3CmidtimeU3E__5_9; }
	inline void set_U3CmidtimeU3E__5_9(int64_t value)
	{
		___U3CmidtimeU3E__5_9 = value;
	}

	inline static int32_t get_offset_of_U3CcountU3E__6_10() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___U3CcountU3E__6_10)); }
	inline int32_t get_U3CcountU3E__6_10() const { return ___U3CcountU3E__6_10; }
	inline int32_t* get_address_of_U3CcountU3E__6_10() { return &___U3CcountU3E__6_10; }
	inline void set_U3CcountU3E__6_10(int32_t value)
	{
		___U3CcountU3E__6_10 = value;
	}

	inline static int32_t get_offset_of_numberOfMessages_11() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___numberOfMessages_11)); }
	inline int32_t get_numberOfMessages_11() const { return ___numberOfMessages_11; }
	inline int32_t* get_address_of_numberOfMessages_11() { return &___numberOfMessages_11; }
	inline void set_numberOfMessages_11(int32_t value)
	{
		___numberOfMessages_11 = value;
	}

	inline static int32_t get_offset_of_asObject_12() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___asObject_12)); }
	inline bool get_asObject_12() const { return ___asObject_12; }
	inline bool* get_address_of_asObject_12() { return &___asObject_12; }
	inline void set_asObject_12(bool value)
	{
		___asObject_12 = value;
	}

	inline static int32_t get_offset_of_messages_13() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___messages_13)); }
	inline ObjectU5BU5D_t3614634134* get_messages_13() const { return ___messages_13; }
	inline ObjectU5BU5D_t3614634134** get_address_of_messages_13() { return &___messages_13; }
	inline void set_messages_13(ObjectU5BU5D_t3614634134* value)
	{
		___messages_13 = value;
		Il2CppCodeGenWriteBarrier(&___messages_13, value);
	}

	inline static int32_t get_offset_of_U24locvar0_14() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___U24locvar0_14)); }
	inline ObjectU5BU5D_t3614634134* get_U24locvar0_14() const { return ___U24locvar0_14; }
	inline ObjectU5BU5D_t3614634134** get_address_of_U24locvar0_14() { return &___U24locvar0_14; }
	inline void set_U24locvar0_14(ObjectU5BU5D_t3614634134* value)
	{
		___U24locvar0_14 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_14, value);
	}

	inline static int32_t get_offset_of_U24locvar1_15() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___U24locvar1_15)); }
	inline int32_t get_U24locvar1_15() const { return ___U24locvar1_15; }
	inline int32_t* get_address_of_U24locvar1_15() { return &___U24locvar1_15; }
	inline void set_U24locvar1_15(int32_t value)
	{
		___U24locvar1_15 = value;
	}

	inline static int32_t get_offset_of_U3CmessageU3E__7_16() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___U3CmessageU3E__7_16)); }
	inline Il2CppObject * get_U3CmessageU3E__7_16() const { return ___U3CmessageU3E__7_16; }
	inline Il2CppObject ** get_address_of_U3CmessageU3E__7_16() { return &___U3CmessageU3E__7_16; }
	inline void set_U3CmessageU3E__7_16(Il2CppObject * value)
	{
		___U3CmessageU3E__7_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmessageU3E__7_16, value);
	}

	inline static int32_t get_offset_of_U24locvar2_17() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___U24locvar2_17)); }
	inline ObjectU5BU5D_t3614634134* get_U24locvar2_17() const { return ___U24locvar2_17; }
	inline ObjectU5BU5D_t3614634134** get_address_of_U24locvar2_17() { return &___U24locvar2_17; }
	inline void set_U24locvar2_17(ObjectU5BU5D_t3614634134* value)
	{
		___U24locvar2_17 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar2_17, value);
	}

	inline static int32_t get_offset_of_U24locvar3_18() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___U24locvar3_18)); }
	inline int32_t get_U24locvar3_18() const { return ___U24locvar3_18; }
	inline int32_t* get_address_of_U24locvar3_18() { return &___U24locvar3_18; }
	inline void set_U24locvar3_18(int32_t value)
	{
		___U24locvar3_18 = value;
	}

	inline static int32_t get_offset_of_U3CmessageU3E__8_19() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___U3CmessageU3E__8_19)); }
	inline Il2CppObject * get_U3CmessageU3E__8_19() const { return ___U3CmessageU3E__8_19; }
	inline Il2CppObject ** get_address_of_U3CmessageU3E__8_19() { return &___U3CmessageU3E__8_19; }
	inline void set_U3CmessageU3E__8_19(Il2CppObject * value)
	{
		___U3CmessageU3E__8_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmessageU3E__8_19, value);
	}

	inline static int32_t get_offset_of_isParamsTest_20() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___isParamsTest_20)); }
	inline bool get_isParamsTest_20() const { return ___isParamsTest_20; }
	inline bool* get_address_of_isParamsTest_20() { return &___isParamsTest_20; }
	inline void set_isParamsTest_20(bool value)
	{
		___isParamsTest_20 = value;
	}

	inline static int32_t get_offset_of_U3CpassedU3E__9_21() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___U3CpassedU3E__9_21)); }
	inline bool get_U3CpassedU3E__9_21() const { return ___U3CpassedU3E__9_21; }
	inline bool* get_address_of_U3CpassedU3E__9_21() { return &___U3CpassedU3E__9_21; }
	inline void set_U3CpassedU3E__9_21(bool value)
	{
		___U3CpassedU3E__9_21 = value;
	}

	inline static int32_t get_offset_of_U24this_22() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___U24this_22)); }
	inline CommonIntergrationTests_t1691354350 * get_U24this_22() const { return ___U24this_22; }
	inline CommonIntergrationTests_t1691354350 ** get_address_of_U24this_22() { return &___U24this_22; }
	inline void set_U24this_22(CommonIntergrationTests_t1691354350 * value)
	{
		___U24this_22 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_22, value);
	}

	inline static int32_t get_offset_of_U24current_23() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___U24current_23)); }
	inline Il2CppObject * get_U24current_23() const { return ___U24current_23; }
	inline Il2CppObject ** get_address_of_U24current_23() { return &___U24current_23; }
	inline void set_U24current_23(Il2CppObject * value)
	{
		___U24current_23 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_23, value);
	}

	inline static int32_t get_offset_of_U24disposing_24() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___U24disposing_24)); }
	inline bool get_U24disposing_24() const { return ___U24disposing_24; }
	inline bool* get_address_of_U24disposing_24() { return &___U24disposing_24; }
	inline void set_U24disposing_24(bool value)
	{
		___U24disposing_24 = value;
	}

	inline static int32_t get_offset_of_U24PC_25() { return static_cast<int32_t>(offsetof(U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917, ___U24PC_25)); }
	inline int32_t get_U24PC_25() const { return ___U24PC_25; }
	inline int32_t* get_address_of_U24PC_25() { return &___U24PC_25; }
	inline void set_U24PC_25(int32_t value)
	{
		___U24PC_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
