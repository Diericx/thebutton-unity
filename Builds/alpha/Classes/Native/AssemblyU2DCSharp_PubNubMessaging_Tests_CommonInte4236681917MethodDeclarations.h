﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3
struct U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::.ctor()
extern "C"  void U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3__ctor_m2917949706 (U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::MoveNext()
extern "C"  bool U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_MoveNext_m871754802 (U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3049379014 (U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1644047294 (U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::Dispose()
extern "C"  void U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_Dispose_m1078465591 (U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoPublishThenDetailedHistoryAndParse>c__Iterator3::Reset()
extern "C"  void U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_Reset_m2732099761 (U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
