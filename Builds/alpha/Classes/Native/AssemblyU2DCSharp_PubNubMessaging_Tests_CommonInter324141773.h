﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Random
struct Random_t1044426839;
// PubNubMessaging.Tests.CommonIntergrationTests
struct CommonIntergrationTests_t1691354350;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15
struct U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0
struct  U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0::testName
	String_t* ___testName_0;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0::ssl
	bool ___ssl_1;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0::withCipher
	bool ___withCipher_2;
	// System.Random PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0::<r>__1
	Random_t1044426839 * ___U3CrU3E__1_3;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0::<bGetAllCG>__5
	bool ___U3CbGetAllCGU3E__5_4;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0::<strLog>__7
	String_t* ___U3CstrLogU3E__7_5;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0::<bRemoveCh>__13
	bool ___U3CbRemoveChU3E__13_6;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0::<bGetChannel2>__14
	bool ___U3CbGetChannel2U3E__14_7;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0::<strLog2>__15
	String_t* ___U3CstrLog2U3E__15_8;
	// PubNubMessaging.Tests.CommonIntergrationTests PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0::$this
	CommonIntergrationTests_t1691354350 * ___U24this_9;
	// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0::$current
	Il2CppObject * ___U24current_10;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0::$disposing
	bool ___U24disposing_11;
	// System.Int32 PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0::$PC
	int32_t ___U24PC_12;
	// PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey15 PubNubMessaging.Tests.CommonIntergrationTests/<DoCGAddListRemoveSubscribeStateHereNowUnsub>c__Iterator0::$locvar0
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869 * ___U24locvar0_13;

public:
	inline static int32_t get_offset_of_testName_0() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773, ___testName_0)); }
	inline String_t* get_testName_0() const { return ___testName_0; }
	inline String_t** get_address_of_testName_0() { return &___testName_0; }
	inline void set_testName_0(String_t* value)
	{
		___testName_0 = value;
		Il2CppCodeGenWriteBarrier(&___testName_0, value);
	}

	inline static int32_t get_offset_of_ssl_1() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773, ___ssl_1)); }
	inline bool get_ssl_1() const { return ___ssl_1; }
	inline bool* get_address_of_ssl_1() { return &___ssl_1; }
	inline void set_ssl_1(bool value)
	{
		___ssl_1 = value;
	}

	inline static int32_t get_offset_of_withCipher_2() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773, ___withCipher_2)); }
	inline bool get_withCipher_2() const { return ___withCipher_2; }
	inline bool* get_address_of_withCipher_2() { return &___withCipher_2; }
	inline void set_withCipher_2(bool value)
	{
		___withCipher_2 = value;
	}

	inline static int32_t get_offset_of_U3CrU3E__1_3() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773, ___U3CrU3E__1_3)); }
	inline Random_t1044426839 * get_U3CrU3E__1_3() const { return ___U3CrU3E__1_3; }
	inline Random_t1044426839 ** get_address_of_U3CrU3E__1_3() { return &___U3CrU3E__1_3; }
	inline void set_U3CrU3E__1_3(Random_t1044426839 * value)
	{
		___U3CrU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrU3E__1_3, value);
	}

	inline static int32_t get_offset_of_U3CbGetAllCGU3E__5_4() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773, ___U3CbGetAllCGU3E__5_4)); }
	inline bool get_U3CbGetAllCGU3E__5_4() const { return ___U3CbGetAllCGU3E__5_4; }
	inline bool* get_address_of_U3CbGetAllCGU3E__5_4() { return &___U3CbGetAllCGU3E__5_4; }
	inline void set_U3CbGetAllCGU3E__5_4(bool value)
	{
		___U3CbGetAllCGU3E__5_4 = value;
	}

	inline static int32_t get_offset_of_U3CstrLogU3E__7_5() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773, ___U3CstrLogU3E__7_5)); }
	inline String_t* get_U3CstrLogU3E__7_5() const { return ___U3CstrLogU3E__7_5; }
	inline String_t** get_address_of_U3CstrLogU3E__7_5() { return &___U3CstrLogU3E__7_5; }
	inline void set_U3CstrLogU3E__7_5(String_t* value)
	{
		___U3CstrLogU3E__7_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstrLogU3E__7_5, value);
	}

	inline static int32_t get_offset_of_U3CbRemoveChU3E__13_6() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773, ___U3CbRemoveChU3E__13_6)); }
	inline bool get_U3CbRemoveChU3E__13_6() const { return ___U3CbRemoveChU3E__13_6; }
	inline bool* get_address_of_U3CbRemoveChU3E__13_6() { return &___U3CbRemoveChU3E__13_6; }
	inline void set_U3CbRemoveChU3E__13_6(bool value)
	{
		___U3CbRemoveChU3E__13_6 = value;
	}

	inline static int32_t get_offset_of_U3CbGetChannel2U3E__14_7() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773, ___U3CbGetChannel2U3E__14_7)); }
	inline bool get_U3CbGetChannel2U3E__14_7() const { return ___U3CbGetChannel2U3E__14_7; }
	inline bool* get_address_of_U3CbGetChannel2U3E__14_7() { return &___U3CbGetChannel2U3E__14_7; }
	inline void set_U3CbGetChannel2U3E__14_7(bool value)
	{
		___U3CbGetChannel2U3E__14_7 = value;
	}

	inline static int32_t get_offset_of_U3CstrLog2U3E__15_8() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773, ___U3CstrLog2U3E__15_8)); }
	inline String_t* get_U3CstrLog2U3E__15_8() const { return ___U3CstrLog2U3E__15_8; }
	inline String_t** get_address_of_U3CstrLog2U3E__15_8() { return &___U3CstrLog2U3E__15_8; }
	inline void set_U3CstrLog2U3E__15_8(String_t* value)
	{
		___U3CstrLog2U3E__15_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstrLog2U3E__15_8, value);
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773, ___U24this_9)); }
	inline CommonIntergrationTests_t1691354350 * get_U24this_9() const { return ___U24this_9; }
	inline CommonIntergrationTests_t1691354350 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(CommonIntergrationTests_t1691354350 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_9, value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773, ___U24current_10)); }
	inline Il2CppObject * get_U24current_10() const { return ___U24current_10; }
	inline Il2CppObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(Il2CppObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_10, value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_13() { return static_cast<int32_t>(offsetof(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773, ___U24locvar0_13)); }
	inline U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869 * get_U24locvar0_13() const { return ___U24locvar0_13; }
	inline U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869 ** get_address_of_U24locvar0_13() { return &___U24locvar0_13; }
	inline void set_U24locvar0_13(U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869 * value)
	{
		___U24locvar0_13 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
