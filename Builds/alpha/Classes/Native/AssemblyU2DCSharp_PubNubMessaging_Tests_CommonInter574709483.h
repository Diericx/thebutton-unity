﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.String[]
struct StringU5BU5D_t1642385972;
// PubNubMessaging.Tests.CommonIntergrationTests
struct CommonIntergrationTests_t1691354350;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ResponseTyp1248099179.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14
struct  U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::url
	String_t* ___url_0;
	// UnityEngine.WWW PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_1;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::expectedMessage
	String_t* ___expectedMessage_2;
	// System.String[] PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::channels
	StringU5BU5D_t1642385972* ___channels_3;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::isError
	bool ___isError_4;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::isTimeout
	bool ___isTimeout_5;
	// PubNubMessaging.Core.CurrentRequestType PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::crt
	int32_t ___crt_6;
	// PubNubMessaging.Core.ResponseType PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::respType
	int32_t ___respType_7;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::asObject
	bool ___asObject_8;
	// System.Int32 PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::timeout
	int32_t ___timeout_9;
	// System.Int32 PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::pause
	int32_t ___pause_10;
	// System.Int64 PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::timetoken
	int64_t ___timetoken_11;
	// PubNubMessaging.Tests.CommonIntergrationTests PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::$this
	CommonIntergrationTests_t1691354350 * ___U24this_12;
	// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::$current
	Il2CppObject * ___U24current_13;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::$disposing
	bool ___U24disposing_14;
	// System.Int32 PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::$PC
	int32_t ___U24PC_15;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483, ___U3CwwwU3E__0_1)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_1, value);
	}

	inline static int32_t get_offset_of_expectedMessage_2() { return static_cast<int32_t>(offsetof(U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483, ___expectedMessage_2)); }
	inline String_t* get_expectedMessage_2() const { return ___expectedMessage_2; }
	inline String_t** get_address_of_expectedMessage_2() { return &___expectedMessage_2; }
	inline void set_expectedMessage_2(String_t* value)
	{
		___expectedMessage_2 = value;
		Il2CppCodeGenWriteBarrier(&___expectedMessage_2, value);
	}

	inline static int32_t get_offset_of_channels_3() { return static_cast<int32_t>(offsetof(U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483, ___channels_3)); }
	inline StringU5BU5D_t1642385972* get_channels_3() const { return ___channels_3; }
	inline StringU5BU5D_t1642385972** get_address_of_channels_3() { return &___channels_3; }
	inline void set_channels_3(StringU5BU5D_t1642385972* value)
	{
		___channels_3 = value;
		Il2CppCodeGenWriteBarrier(&___channels_3, value);
	}

	inline static int32_t get_offset_of_isError_4() { return static_cast<int32_t>(offsetof(U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483, ___isError_4)); }
	inline bool get_isError_4() const { return ___isError_4; }
	inline bool* get_address_of_isError_4() { return &___isError_4; }
	inline void set_isError_4(bool value)
	{
		___isError_4 = value;
	}

	inline static int32_t get_offset_of_isTimeout_5() { return static_cast<int32_t>(offsetof(U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483, ___isTimeout_5)); }
	inline bool get_isTimeout_5() const { return ___isTimeout_5; }
	inline bool* get_address_of_isTimeout_5() { return &___isTimeout_5; }
	inline void set_isTimeout_5(bool value)
	{
		___isTimeout_5 = value;
	}

	inline static int32_t get_offset_of_crt_6() { return static_cast<int32_t>(offsetof(U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483, ___crt_6)); }
	inline int32_t get_crt_6() const { return ___crt_6; }
	inline int32_t* get_address_of_crt_6() { return &___crt_6; }
	inline void set_crt_6(int32_t value)
	{
		___crt_6 = value;
	}

	inline static int32_t get_offset_of_respType_7() { return static_cast<int32_t>(offsetof(U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483, ___respType_7)); }
	inline int32_t get_respType_7() const { return ___respType_7; }
	inline int32_t* get_address_of_respType_7() { return &___respType_7; }
	inline void set_respType_7(int32_t value)
	{
		___respType_7 = value;
	}

	inline static int32_t get_offset_of_asObject_8() { return static_cast<int32_t>(offsetof(U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483, ___asObject_8)); }
	inline bool get_asObject_8() const { return ___asObject_8; }
	inline bool* get_address_of_asObject_8() { return &___asObject_8; }
	inline void set_asObject_8(bool value)
	{
		___asObject_8 = value;
	}

	inline static int32_t get_offset_of_timeout_9() { return static_cast<int32_t>(offsetof(U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483, ___timeout_9)); }
	inline int32_t get_timeout_9() const { return ___timeout_9; }
	inline int32_t* get_address_of_timeout_9() { return &___timeout_9; }
	inline void set_timeout_9(int32_t value)
	{
		___timeout_9 = value;
	}

	inline static int32_t get_offset_of_pause_10() { return static_cast<int32_t>(offsetof(U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483, ___pause_10)); }
	inline int32_t get_pause_10() const { return ___pause_10; }
	inline int32_t* get_address_of_pause_10() { return &___pause_10; }
	inline void set_pause_10(int32_t value)
	{
		___pause_10 = value;
	}

	inline static int32_t get_offset_of_timetoken_11() { return static_cast<int32_t>(offsetof(U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483, ___timetoken_11)); }
	inline int64_t get_timetoken_11() const { return ___timetoken_11; }
	inline int64_t* get_address_of_timetoken_11() { return &___timetoken_11; }
	inline void set_timetoken_11(int64_t value)
	{
		___timetoken_11 = value;
	}

	inline static int32_t get_offset_of_U24this_12() { return static_cast<int32_t>(offsetof(U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483, ___U24this_12)); }
	inline CommonIntergrationTests_t1691354350 * get_U24this_12() const { return ___U24this_12; }
	inline CommonIntergrationTests_t1691354350 ** get_address_of_U24this_12() { return &___U24this_12; }
	inline void set_U24this_12(CommonIntergrationTests_t1691354350 * value)
	{
		___U24this_12 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_12, value);
	}

	inline static int32_t get_offset_of_U24current_13() { return static_cast<int32_t>(offsetof(U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483, ___U24current_13)); }
	inline Il2CppObject * get_U24current_13() const { return ___U24current_13; }
	inline Il2CppObject ** get_address_of_U24current_13() { return &___U24current_13; }
	inline void set_U24current_13(Il2CppObject * value)
	{
		___U24current_13 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_13, value);
	}

	inline static int32_t get_offset_of_U24disposing_14() { return static_cast<int32_t>(offsetof(U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483, ___U24disposing_14)); }
	inline bool get_U24disposing_14() const { return ___U24disposing_14; }
	inline bool* get_address_of_U24disposing_14() { return &___U24disposing_14; }
	inline void set_U24disposing_14(bool value)
	{
		___U24disposing_14 = value;
	}

	inline static int32_t get_offset_of_U24PC_15() { return static_cast<int32_t>(offsetof(U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483, ___U24PC_15)); }
	inline int32_t get_U24PC_15() const { return ___U24PC_15; }
	inline int32_t* get_address_of_U24PC_15() { return &___U24PC_15; }
	inline void set_U24PC_15(int32_t value)
	{
		___U24PC_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
