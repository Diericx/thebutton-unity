﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14
struct U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::.ctor()
extern "C"  void U3CTestCoroutineRunErrorU3Ec__Iterator14__ctor_m2437533192 (U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::MoveNext()
extern "C"  bool U3CTestCoroutineRunErrorU3Ec__Iterator14_MoveNext_m190041032 (U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTestCoroutineRunErrorU3Ec__Iterator14_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4023365352 (U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTestCoroutineRunErrorU3Ec__Iterator14_System_Collections_IEnumerator_get_Current_m2477924144 (U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::Dispose()
extern "C"  void U3CTestCoroutineRunErrorU3Ec__Iterator14_Dispose_m2829315593 (U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<TestCoroutineRunError>c__Iterator14::Reset()
extern "C"  void U3CTestCoroutineRunErrorU3Ec__Iterator14_Reset_m1363327263 (U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
