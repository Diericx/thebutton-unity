﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<DoConnectedTest>c__IteratorE
struct U3CDoConnectedTestU3Ec__IteratorE_t612063986;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoConnectedTest>c__IteratorE::.ctor()
extern "C"  void U3CDoConnectedTestU3Ec__IteratorE__ctor_m2350524633 (U3CDoConnectedTestU3Ec__IteratorE_t612063986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<DoConnectedTest>c__IteratorE::MoveNext()
extern "C"  bool U3CDoConnectedTestU3Ec__IteratorE_MoveNext_m1414893815 (U3CDoConnectedTestU3Ec__IteratorE_t612063986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoConnectedTest>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoConnectedTestU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1878366755 (U3CDoConnectedTestU3Ec__IteratorE_t612063986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.CommonIntergrationTests/<DoConnectedTest>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoConnectedTestU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m1962014651 (U3CDoConnectedTestU3Ec__IteratorE_t612063986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoConnectedTest>c__IteratorE::Dispose()
extern "C"  void U3CDoConnectedTestU3Ec__IteratorE_Dispose_m3106898076 (U3CDoConnectedTestU3Ec__IteratorE_t612063986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<DoConnectedTest>c__IteratorE::Reset()
extern "C"  void U3CDoConnectedTestU3Ec__IteratorE_Reset_m2914407390 (U3CDoConnectedTestU3Ec__IteratorE_t612063986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
