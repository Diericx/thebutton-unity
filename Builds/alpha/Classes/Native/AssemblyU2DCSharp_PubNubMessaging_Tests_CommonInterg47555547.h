﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9
struct U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9/<SetAndGetStateAndParseUUID>c__AnonStorey16
struct  U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_t47555547  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9/<SetAndGetStateAndParseUUID>c__AnonStorey16::state1
	String_t* ___state1_0;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9/<SetAndGetStateAndParseUUID>c__AnonStorey16::b1
	bool ___b1_1;
	// System.String PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9/<SetAndGetStateAndParseUUID>c__AnonStorey16::state2
	String_t* ___state2_2;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9/<SetAndGetStateAndParseUUID>c__AnonStorey16::b2
	bool ___b2_3;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9/<SetAndGetStateAndParseUUID>c__AnonStorey16::b3
	bool ___b3_4;
	// System.Boolean PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9/<SetAndGetStateAndParseUUID>c__AnonStorey16::b4
	bool ___b4_5;
	// PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9 PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9/<SetAndGetStateAndParseUUID>c__AnonStorey16::<>f__ref$9
	U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517 * ___U3CU3Ef__refU249_6;

public:
	inline static int32_t get_offset_of_state1_0() { return static_cast<int32_t>(offsetof(U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_t47555547, ___state1_0)); }
	inline String_t* get_state1_0() const { return ___state1_0; }
	inline String_t** get_address_of_state1_0() { return &___state1_0; }
	inline void set_state1_0(String_t* value)
	{
		___state1_0 = value;
		Il2CppCodeGenWriteBarrier(&___state1_0, value);
	}

	inline static int32_t get_offset_of_b1_1() { return static_cast<int32_t>(offsetof(U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_t47555547, ___b1_1)); }
	inline bool get_b1_1() const { return ___b1_1; }
	inline bool* get_address_of_b1_1() { return &___b1_1; }
	inline void set_b1_1(bool value)
	{
		___b1_1 = value;
	}

	inline static int32_t get_offset_of_state2_2() { return static_cast<int32_t>(offsetof(U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_t47555547, ___state2_2)); }
	inline String_t* get_state2_2() const { return ___state2_2; }
	inline String_t** get_address_of_state2_2() { return &___state2_2; }
	inline void set_state2_2(String_t* value)
	{
		___state2_2 = value;
		Il2CppCodeGenWriteBarrier(&___state2_2, value);
	}

	inline static int32_t get_offset_of_b2_3() { return static_cast<int32_t>(offsetof(U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_t47555547, ___b2_3)); }
	inline bool get_b2_3() const { return ___b2_3; }
	inline bool* get_address_of_b2_3() { return &___b2_3; }
	inline void set_b2_3(bool value)
	{
		___b2_3 = value;
	}

	inline static int32_t get_offset_of_b3_4() { return static_cast<int32_t>(offsetof(U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_t47555547, ___b3_4)); }
	inline bool get_b3_4() const { return ___b3_4; }
	inline bool* get_address_of_b3_4() { return &___b3_4; }
	inline void set_b3_4(bool value)
	{
		___b3_4 = value;
	}

	inline static int32_t get_offset_of_b4_5() { return static_cast<int32_t>(offsetof(U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_t47555547, ___b4_5)); }
	inline bool get_b4_5() const { return ___b4_5; }
	inline bool* get_address_of_b4_5() { return &___b4_5; }
	inline void set_b4_5(bool value)
	{
		___b4_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU249_6() { return static_cast<int32_t>(offsetof(U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_t47555547, ___U3CU3Ef__refU249_6)); }
	inline U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517 * get_U3CU3Ef__refU249_6() const { return ___U3CU3Ef__refU249_6; }
	inline U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517 ** get_address_of_U3CU3Ef__refU249_6() { return &___U3CU3Ef__refU249_6; }
	inline void set_U3CU3Ef__refU249_6(U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517 * value)
	{
		___U3CU3Ef__refU249_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU249_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
