﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9/<SetAndGetStateAndParseUUID>c__AnonStorey16
struct U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_t47555547;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9/<SetAndGetStateAndParseUUID>c__AnonStorey16::.ctor()
extern "C"  void U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16__ctor_m3987783258 (U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_t47555547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9/<SetAndGetStateAndParseUUID>c__AnonStorey16::<>m__0(System.String)
extern "C"  void U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_U3CU3Em__0_m1116551599 (U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_t47555547 * __this, String_t* ___returnMessage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.CommonIntergrationTests/<SetAndGetStateAndParseUUID>c__Iterator9/<SetAndGetStateAndParseUUID>c__AnonStorey16::<>m__1(System.String)
extern "C"  void U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_U3CU3Em__1_m1185743860 (U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_t47555547 * __this, String_t* ___returnMessage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
