﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.CustomClass
struct  CustomClass_t1119119311  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.CustomClass::foo
	String_t* ___foo_0;
	// System.Int32[] PubNubMessaging.Tests.CustomClass::bar
	Int32U5BU5D_t3030399641* ___bar_1;

public:
	inline static int32_t get_offset_of_foo_0() { return static_cast<int32_t>(offsetof(CustomClass_t1119119311, ___foo_0)); }
	inline String_t* get_foo_0() const { return ___foo_0; }
	inline String_t** get_address_of_foo_0() { return &___foo_0; }
	inline void set_foo_0(String_t* value)
	{
		___foo_0 = value;
		Il2CppCodeGenWriteBarrier(&___foo_0, value);
	}

	inline static int32_t get_offset_of_bar_1() { return static_cast<int32_t>(offsetof(CustomClass_t1119119311, ___bar_1)); }
	inline Int32U5BU5D_t3030399641* get_bar_1() const { return ___bar_1; }
	inline Int32U5BU5D_t3030399641** get_address_of_bar_1() { return &___bar_1; }
	inline void set_bar_1(Int32U5BU5D_t3030399641* value)
	{
		___bar_1 = value;
		Il2CppCodeGenWriteBarrier(&___bar_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
