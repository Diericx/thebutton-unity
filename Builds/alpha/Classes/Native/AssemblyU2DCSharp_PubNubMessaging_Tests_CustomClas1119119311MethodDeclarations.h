﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.CustomClass
struct CustomClass_t1119119311;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.CustomClass::.ctor()
extern "C"  void CustomClass__ctor_m3718379423 (CustomClass_t1119119311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
