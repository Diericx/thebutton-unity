﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// PubNubMessaging.Tests.PubnubDemoMessage
struct PubnubDemoMessage_t254076932;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.PubnubDemoObject
struct  PubnubDemoObject_t1053894922  : public Il2CppObject
{
public:
	// System.Double PubNubMessaging.Tests.PubnubDemoObject::VersionID
	double ___VersionID_0;
	// System.Int64 PubNubMessaging.Tests.PubnubDemoObject::Timetoken
	int64_t ___Timetoken_1;
	// System.String PubNubMessaging.Tests.PubnubDemoObject::OperationName
	String_t* ___OperationName_2;
	// System.String[] PubNubMessaging.Tests.PubnubDemoObject::Channels
	StringU5BU5D_t1642385972* ___Channels_3;
	// PubNubMessaging.Tests.PubnubDemoMessage PubNubMessaging.Tests.PubnubDemoObject::DemoMessage
	PubnubDemoMessage_t254076932 * ___DemoMessage_4;
	// PubNubMessaging.Tests.PubnubDemoMessage PubNubMessaging.Tests.PubnubDemoObject::CustomMessage
	PubnubDemoMessage_t254076932 * ___CustomMessage_5;
	// System.Xml.XmlDocument PubNubMessaging.Tests.PubnubDemoObject::SampleXml
	XmlDocument_t3649534162 * ___SampleXml_6;

public:
	inline static int32_t get_offset_of_VersionID_0() { return static_cast<int32_t>(offsetof(PubnubDemoObject_t1053894922, ___VersionID_0)); }
	inline double get_VersionID_0() const { return ___VersionID_0; }
	inline double* get_address_of_VersionID_0() { return &___VersionID_0; }
	inline void set_VersionID_0(double value)
	{
		___VersionID_0 = value;
	}

	inline static int32_t get_offset_of_Timetoken_1() { return static_cast<int32_t>(offsetof(PubnubDemoObject_t1053894922, ___Timetoken_1)); }
	inline int64_t get_Timetoken_1() const { return ___Timetoken_1; }
	inline int64_t* get_address_of_Timetoken_1() { return &___Timetoken_1; }
	inline void set_Timetoken_1(int64_t value)
	{
		___Timetoken_1 = value;
	}

	inline static int32_t get_offset_of_OperationName_2() { return static_cast<int32_t>(offsetof(PubnubDemoObject_t1053894922, ___OperationName_2)); }
	inline String_t* get_OperationName_2() const { return ___OperationName_2; }
	inline String_t** get_address_of_OperationName_2() { return &___OperationName_2; }
	inline void set_OperationName_2(String_t* value)
	{
		___OperationName_2 = value;
		Il2CppCodeGenWriteBarrier(&___OperationName_2, value);
	}

	inline static int32_t get_offset_of_Channels_3() { return static_cast<int32_t>(offsetof(PubnubDemoObject_t1053894922, ___Channels_3)); }
	inline StringU5BU5D_t1642385972* get_Channels_3() const { return ___Channels_3; }
	inline StringU5BU5D_t1642385972** get_address_of_Channels_3() { return &___Channels_3; }
	inline void set_Channels_3(StringU5BU5D_t1642385972* value)
	{
		___Channels_3 = value;
		Il2CppCodeGenWriteBarrier(&___Channels_3, value);
	}

	inline static int32_t get_offset_of_DemoMessage_4() { return static_cast<int32_t>(offsetof(PubnubDemoObject_t1053894922, ___DemoMessage_4)); }
	inline PubnubDemoMessage_t254076932 * get_DemoMessage_4() const { return ___DemoMessage_4; }
	inline PubnubDemoMessage_t254076932 ** get_address_of_DemoMessage_4() { return &___DemoMessage_4; }
	inline void set_DemoMessage_4(PubnubDemoMessage_t254076932 * value)
	{
		___DemoMessage_4 = value;
		Il2CppCodeGenWriteBarrier(&___DemoMessage_4, value);
	}

	inline static int32_t get_offset_of_CustomMessage_5() { return static_cast<int32_t>(offsetof(PubnubDemoObject_t1053894922, ___CustomMessage_5)); }
	inline PubnubDemoMessage_t254076932 * get_CustomMessage_5() const { return ___CustomMessage_5; }
	inline PubnubDemoMessage_t254076932 ** get_address_of_CustomMessage_5() { return &___CustomMessage_5; }
	inline void set_CustomMessage_5(PubnubDemoMessage_t254076932 * value)
	{
		___CustomMessage_5 = value;
		Il2CppCodeGenWriteBarrier(&___CustomMessage_5, value);
	}

	inline static int32_t get_offset_of_SampleXml_6() { return static_cast<int32_t>(offsetof(PubnubDemoObject_t1053894922, ___SampleXml_6)); }
	inline XmlDocument_t3649534162 * get_SampleXml_6() const { return ___SampleXml_6; }
	inline XmlDocument_t3649534162 ** get_address_of_SampleXml_6() { return &___SampleXml_6; }
	inline void set_SampleXml_6(XmlDocument_t3649534162 * value)
	{
		___SampleXml_6 = value;
		Il2CppCodeGenWriteBarrier(&___SampleXml_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
