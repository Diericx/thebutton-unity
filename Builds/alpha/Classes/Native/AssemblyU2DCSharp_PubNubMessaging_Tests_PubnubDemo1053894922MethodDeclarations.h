﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.PubnubDemoObject
struct PubnubDemoObject_t1053894922;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.PubnubDemoObject::.ctor()
extern "C"  void PubnubDemoObject__ctor_m2177709474 (PubnubDemoObject_t1053894922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
