﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.PubnubDemoMessage
struct PubnubDemoMessage_t254076932;
// System.String
struct String_t;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PubNubMessaging.Tests.PubnubDemoMessage::.ctor()
extern "C"  void PubnubDemoMessage__ctor_m4092235274 (PubnubDemoMessage_t254076932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.PubnubDemoMessage::.ctor(System.String)
extern "C"  void PubnubDemoMessage__ctor_m1769868088 (PubnubDemoMessage_t254076932 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlDocument PubNubMessaging.Tests.PubnubDemoMessage::TryXmlDemo()
extern "C"  XmlDocument_t3649534162 * PubnubDemoMessage_TryXmlDemo_m3486052420 (PubnubDemoMessage_t254076932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
