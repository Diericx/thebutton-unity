﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestAbortSubscribe
struct TestAbortSubscribe_t2694853646;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestAbortSubscribe::.ctor()
extern "C"  void TestAbortSubscribe__ctor_m3213681852 (TestAbortSubscribe_t2694853646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestAbortSubscribe::Start()
extern "C"  Il2CppObject * TestAbortSubscribe_Start_m3228949488 (TestAbortSubscribe_t2694853646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
