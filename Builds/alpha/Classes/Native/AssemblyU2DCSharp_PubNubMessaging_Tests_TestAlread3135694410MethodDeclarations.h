﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestAlreadySubscribed
struct TestAlreadySubscribed_t3135694410;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestAlreadySubscribed::.ctor()
extern "C"  void TestAlreadySubscribed__ctor_m3528821700 (TestAlreadySubscribed_t3135694410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestAlreadySubscribed::Start()
extern "C"  Il2CppObject * TestAlreadySubscribed_Start_m921473216 (TestAlreadySubscribed_t3135694410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
