﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestBouncePHB
struct TestBouncePHB_t1324311378;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestBouncePHB::.ctor()
extern "C"  void TestBouncePHB__ctor_m4243225654 (TestBouncePHB_t1324311378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestBouncePHB::Start()
extern "C"  Il2CppObject * TestBouncePHB_Start_m3335325050 (TestBouncePHB_t1324311378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
