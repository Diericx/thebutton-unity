﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestBounceSubscribe
struct TestBounceSubscribe_t3713701440;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestBounceSubscribe::.ctor()
extern "C"  void TestBounceSubscribe__ctor_m4017290252 (TestBounceSubscribe_t3713701440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestBounceSubscribe::Start()
extern "C"  Il2CppObject * TestBounceSubscribe_Start_m1083122272 (TestBounceSubscribe_t3713701440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
