﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestBounceHB
struct TestBounceHB_t3845500086;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestBounceHB::.ctor()
extern "C"  void TestBounceHB__ctor_m3341672372 (TestBounceHB_t3845500086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestBounceHB::Start()
extern "C"  Il2CppObject * TestBounceHB_Start_m3769491784 (TestBounceHB_t3845500086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
