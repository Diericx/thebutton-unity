﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestBounceNonSub
struct TestBounceNonSub_t4134311263;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestBounceNonSub::.ctor()
extern "C"  void TestBounceNonSub__ctor_m1003049987 (TestBounceNonSub_t4134311263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestBounceNonSub::Start()
extern "C"  Il2CppObject * TestBounceNonSub_Start_m2793120197 (TestBounceNonSub_t4134311263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
