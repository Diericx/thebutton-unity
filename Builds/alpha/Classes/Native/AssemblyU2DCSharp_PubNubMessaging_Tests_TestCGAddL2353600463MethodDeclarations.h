﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestCGAddListRemoveSubscribeStateHereNowUnsub
struct TestCGAddListRemoveSubscribeStateHereNowUnsub_t2353600463;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestCGAddListRemoveSubscribeStateHereNowUnsub::.ctor()
extern "C"  void TestCGAddListRemoveSubscribeStateHereNowUnsub__ctor_m1832734725 (TestCGAddListRemoveSubscribeStateHereNowUnsub_t2353600463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestCGAddListRemoveSubscribeStateHereNowUnsub::Start()
extern "C"  Il2CppObject * TestCGAddListRemoveSubscribeStateHereNowUnsub_Start_m2464819715 (TestCGAddListRemoveSubscribeStateHereNowUnsub_t2353600463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
