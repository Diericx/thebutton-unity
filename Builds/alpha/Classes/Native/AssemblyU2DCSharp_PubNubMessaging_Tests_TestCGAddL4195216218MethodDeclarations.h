﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestCGAddListRemoveSubscribeStateHereNowUnsub/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t4195216218;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestCGAddListRemoveSubscribeStateHereNowUnsub/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m525073445 (U3CStartU3Ec__Iterator0_t4195216218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.TestCGAddListRemoveSubscribeStateHereNowUnsub/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m3813742935 (U3CStartU3Ec__Iterator0_t4195216218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestCGAddListRemoveSubscribeStateHereNowUnsub/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3186835851 (U3CStartU3Ec__Iterator0_t4195216218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestCGAddListRemoveSubscribeStateHereNowUnsub/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4143633139 (U3CStartU3Ec__Iterator0_t4195216218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestCGAddListRemoveSubscribeStateHereNowUnsub/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m502521644 (U3CStartU3Ec__Iterator0_t4195216218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestCGAddListRemoveSubscribeStateHereNowUnsub/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m2968786586 (U3CStartU3Ec__Iterator0_t4195216218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
