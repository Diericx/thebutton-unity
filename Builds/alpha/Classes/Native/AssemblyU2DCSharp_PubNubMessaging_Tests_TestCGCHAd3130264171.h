﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Random
struct Random_t1044426839;
// System.String
struct String_t;
// PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub
struct TestCGCHAddListRemoveSubscribeStateHereNowUnsub_t3525210006;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey2
struct U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1
struct  U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171  : public Il2CppObject
{
public:
	// System.Random PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::<r>__0
	Random_t1044426839 * ___U3CrU3E__0_0;
	// System.String PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::testName
	String_t* ___testName_1;
	// System.Boolean PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::<bGetAllCG>__6
	bool ___U3CbGetAllCGU3E__6_2;
	// System.String PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::<strLog>__8
	String_t* ___U3CstrLogU3E__8_3;
	// System.Boolean PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::<bSubPublished>__C
	bool ___U3CbSubPublishedU3E__C_4;
	// System.Boolean PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::<bGetState>__F
	bool ___U3CbGetStateU3E__F_5;
	// System.Boolean PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::<bSetUserState2>__13
	bool ___U3CbSetUserState2U3E__13_6;
	// System.Boolean PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::<bGetUserState2>__14
	bool ___U3CbGetUserState2U3E__14_7;
	// System.Boolean PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::<bRemoveCh>__16
	bool ___U3CbRemoveChU3E__16_8;
	// System.Boolean PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::<bGetChannel2>__17
	bool ___U3CbGetChannel2U3E__17_9;
	// System.String PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::<strLog2>__18
	String_t* ___U3CstrLog2U3E__18_10;
	// PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::$this
	TestCGCHAddListRemoveSubscribeStateHereNowUnsub_t3525210006 * ___U24this_11;
	// System.Object PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::$current
	Il2CppObject * ___U24current_12;
	// System.Boolean PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::$disposing
	bool ___U24disposing_13;
	// System.Int32 PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::$PC
	int32_t ___U24PC_14;
	// PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey2 PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::$locvar0
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560 * ___U24locvar0_15;

public:
	inline static int32_t get_offset_of_U3CrU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171, ___U3CrU3E__0_0)); }
	inline Random_t1044426839 * get_U3CrU3E__0_0() const { return ___U3CrU3E__0_0; }
	inline Random_t1044426839 ** get_address_of_U3CrU3E__0_0() { return &___U3CrU3E__0_0; }
	inline void set_U3CrU3E__0_0(Random_t1044426839 * value)
	{
		___U3CrU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrU3E__0_0, value);
	}

	inline static int32_t get_offset_of_testName_1() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171, ___testName_1)); }
	inline String_t* get_testName_1() const { return ___testName_1; }
	inline String_t** get_address_of_testName_1() { return &___testName_1; }
	inline void set_testName_1(String_t* value)
	{
		___testName_1 = value;
		Il2CppCodeGenWriteBarrier(&___testName_1, value);
	}

	inline static int32_t get_offset_of_U3CbGetAllCGU3E__6_2() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171, ___U3CbGetAllCGU3E__6_2)); }
	inline bool get_U3CbGetAllCGU3E__6_2() const { return ___U3CbGetAllCGU3E__6_2; }
	inline bool* get_address_of_U3CbGetAllCGU3E__6_2() { return &___U3CbGetAllCGU3E__6_2; }
	inline void set_U3CbGetAllCGU3E__6_2(bool value)
	{
		___U3CbGetAllCGU3E__6_2 = value;
	}

	inline static int32_t get_offset_of_U3CstrLogU3E__8_3() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171, ___U3CstrLogU3E__8_3)); }
	inline String_t* get_U3CstrLogU3E__8_3() const { return ___U3CstrLogU3E__8_3; }
	inline String_t** get_address_of_U3CstrLogU3E__8_3() { return &___U3CstrLogU3E__8_3; }
	inline void set_U3CstrLogU3E__8_3(String_t* value)
	{
		___U3CstrLogU3E__8_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstrLogU3E__8_3, value);
	}

	inline static int32_t get_offset_of_U3CbSubPublishedU3E__C_4() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171, ___U3CbSubPublishedU3E__C_4)); }
	inline bool get_U3CbSubPublishedU3E__C_4() const { return ___U3CbSubPublishedU3E__C_4; }
	inline bool* get_address_of_U3CbSubPublishedU3E__C_4() { return &___U3CbSubPublishedU3E__C_4; }
	inline void set_U3CbSubPublishedU3E__C_4(bool value)
	{
		___U3CbSubPublishedU3E__C_4 = value;
	}

	inline static int32_t get_offset_of_U3CbGetStateU3E__F_5() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171, ___U3CbGetStateU3E__F_5)); }
	inline bool get_U3CbGetStateU3E__F_5() const { return ___U3CbGetStateU3E__F_5; }
	inline bool* get_address_of_U3CbGetStateU3E__F_5() { return &___U3CbGetStateU3E__F_5; }
	inline void set_U3CbGetStateU3E__F_5(bool value)
	{
		___U3CbGetStateU3E__F_5 = value;
	}

	inline static int32_t get_offset_of_U3CbSetUserState2U3E__13_6() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171, ___U3CbSetUserState2U3E__13_6)); }
	inline bool get_U3CbSetUserState2U3E__13_6() const { return ___U3CbSetUserState2U3E__13_6; }
	inline bool* get_address_of_U3CbSetUserState2U3E__13_6() { return &___U3CbSetUserState2U3E__13_6; }
	inline void set_U3CbSetUserState2U3E__13_6(bool value)
	{
		___U3CbSetUserState2U3E__13_6 = value;
	}

	inline static int32_t get_offset_of_U3CbGetUserState2U3E__14_7() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171, ___U3CbGetUserState2U3E__14_7)); }
	inline bool get_U3CbGetUserState2U3E__14_7() const { return ___U3CbGetUserState2U3E__14_7; }
	inline bool* get_address_of_U3CbGetUserState2U3E__14_7() { return &___U3CbGetUserState2U3E__14_7; }
	inline void set_U3CbGetUserState2U3E__14_7(bool value)
	{
		___U3CbGetUserState2U3E__14_7 = value;
	}

	inline static int32_t get_offset_of_U3CbRemoveChU3E__16_8() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171, ___U3CbRemoveChU3E__16_8)); }
	inline bool get_U3CbRemoveChU3E__16_8() const { return ___U3CbRemoveChU3E__16_8; }
	inline bool* get_address_of_U3CbRemoveChU3E__16_8() { return &___U3CbRemoveChU3E__16_8; }
	inline void set_U3CbRemoveChU3E__16_8(bool value)
	{
		___U3CbRemoveChU3E__16_8 = value;
	}

	inline static int32_t get_offset_of_U3CbGetChannel2U3E__17_9() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171, ___U3CbGetChannel2U3E__17_9)); }
	inline bool get_U3CbGetChannel2U3E__17_9() const { return ___U3CbGetChannel2U3E__17_9; }
	inline bool* get_address_of_U3CbGetChannel2U3E__17_9() { return &___U3CbGetChannel2U3E__17_9; }
	inline void set_U3CbGetChannel2U3E__17_9(bool value)
	{
		___U3CbGetChannel2U3E__17_9 = value;
	}

	inline static int32_t get_offset_of_U3CstrLog2U3E__18_10() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171, ___U3CstrLog2U3E__18_10)); }
	inline String_t* get_U3CstrLog2U3E__18_10() const { return ___U3CstrLog2U3E__18_10; }
	inline String_t** get_address_of_U3CstrLog2U3E__18_10() { return &___U3CstrLog2U3E__18_10; }
	inline void set_U3CstrLog2U3E__18_10(String_t* value)
	{
		___U3CstrLog2U3E__18_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstrLog2U3E__18_10, value);
	}

	inline static int32_t get_offset_of_U24this_11() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171, ___U24this_11)); }
	inline TestCGCHAddListRemoveSubscribeStateHereNowUnsub_t3525210006 * get_U24this_11() const { return ___U24this_11; }
	inline TestCGCHAddListRemoveSubscribeStateHereNowUnsub_t3525210006 ** get_address_of_U24this_11() { return &___U24this_11; }
	inline void set_U24this_11(TestCGCHAddListRemoveSubscribeStateHereNowUnsub_t3525210006 * value)
	{
		___U24this_11 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_11, value);
	}

	inline static int32_t get_offset_of_U24current_12() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171, ___U24current_12)); }
	inline Il2CppObject * get_U24current_12() const { return ___U24current_12; }
	inline Il2CppObject ** get_address_of_U24current_12() { return &___U24current_12; }
	inline void set_U24current_12(Il2CppObject * value)
	{
		___U24current_12 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_12, value);
	}

	inline static int32_t get_offset_of_U24disposing_13() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171, ___U24disposing_13)); }
	inline bool get_U24disposing_13() const { return ___U24disposing_13; }
	inline bool* get_address_of_U24disposing_13() { return &___U24disposing_13; }
	inline void set_U24disposing_13(bool value)
	{
		___U24disposing_13 = value;
	}

	inline static int32_t get_offset_of_U24PC_14() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171, ___U24PC_14)); }
	inline int32_t get_U24PC_14() const { return ___U24PC_14; }
	inline int32_t* get_address_of_U24PC_14() { return &___U24PC_14; }
	inline void set_U24PC_14(int32_t value)
	{
		___U24PC_14 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_15() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171, ___U24locvar0_15)); }
	inline U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560 * get_U24locvar0_15() const { return ___U24locvar0_15; }
	inline U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560 ** get_address_of_U24locvar0_15() { return &___U24locvar0_15; }
	inline void set_U24locvar0_15(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560 * value)
	{
		___U24locvar0_15 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
