﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1
struct U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::.ctor()
extern "C"  void U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1__ctor_m563573320 (U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::MoveNext()
extern "C"  bool U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_MoveNext_m1735336080 (U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3897905126 (U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2598758478 (U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::Dispose()
extern "C"  void U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_Dispose_m3529946205 (U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1::Reset()
extern "C"  void U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_Reset_m450178347 (U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
