﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub
struct TestCGCHAddListRemoveSubscribeStateHereNowUnsub_t3525210006;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Core.PubnubClientError
struct PubnubClientError_t110317921;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubClient110317921.h"

// System.Void PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub::.ctor()
extern "C"  void TestCGCHAddListRemoveSubscribeStateHereNowUnsub__ctor_m3534583552 (TestCGCHAddListRemoveSubscribeStateHereNowUnsub_t3525210006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub::Start()
extern "C"  Il2CppObject * TestCGCHAddListRemoveSubscribeStateHereNowUnsub_Start_m3750786732 (TestCGCHAddListRemoveSubscribeStateHereNowUnsub_t3525210006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub::DoCGCHAddListRemoveSubscribeStateHereNowUnsub(System.Boolean,System.String,System.Boolean,System.Boolean,System.Object,System.String,System.Boolean)
extern "C"  Il2CppObject * TestCGCHAddListRemoveSubscribeStateHereNowUnsub_DoCGCHAddListRemoveSubscribeStateHereNowUnsub_m981825403 (TestCGCHAddListRemoveSubscribeStateHereNowUnsub_t3525210006 * __this, bool ___ssl0, String_t* ___testName1, bool ___asObject2, bool ___withCipher3, Il2CppObject * ___message4, String_t* ___expectedStringResponse5, bool ___matchExpectedStringResponse6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub::DisplayErrorMessage(PubNubMessaging.Core.PubnubClientError)
extern "C"  void TestCGCHAddListRemoveSubscribeStateHereNowUnsub_DisplayErrorMessage_m3905994715 (TestCGCHAddListRemoveSubscribeStateHereNowUnsub_t3525210006 * __this, PubnubClientError_t110317921 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub::DisplayReturnMessageDummy(System.Object)
extern "C"  void TestCGCHAddListRemoveSubscribeStateHereNowUnsub_DisplayReturnMessageDummy_m3233977615 (TestCGCHAddListRemoveSubscribeStateHereNowUnsub_t3525210006 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
