﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t3787879071;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m3230430366 (U3CStartU3Ec__Iterator0_t3787879071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m1926705774 (U3CStartU3Ec__Iterator0_t3787879071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1189918928 (U3CStartU3Ec__Iterator0_t3787879071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3572097832 (U3CStartU3Ec__Iterator0_t3787879071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m3059004801 (U3CStartU3Ec__Iterator0_t3787879071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m1355110627 (U3CStartU3Ec__Iterator0_t3787879071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
