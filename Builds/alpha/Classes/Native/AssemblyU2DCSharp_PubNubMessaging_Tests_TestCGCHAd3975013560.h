﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1
struct U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey2
struct  U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey2::testName
	String_t* ___testName_0;
	// System.Boolean PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey2::bAddChannel
	bool ___bAddChannel_1;
	// System.String PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey2::cg
	String_t* ___cg_2;
	// System.String PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey2::channel
	String_t* ___channel_3;
	// System.Boolean PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey2::bGetChannel
	bool ___bGetChannel_4;
	// System.String PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey2::pubMessage
	String_t* ___pubMessage_5;
	// System.Boolean PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey2::bSubMessage
	bool ___bSubMessage_6;
	// System.String PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey2::ch
	String_t* ___ch_7;
	// System.Boolean PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey2::bSubMessage2
	bool ___bSubMessage2_8;
	// System.Boolean PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey2::bSubConnected
	bool ___bSubConnected_9;
	// System.String PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey2::state
	String_t* ___state_10;
	// System.Boolean PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey2::bSetState
	bool ___bSetState_11;
	// System.String PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey2::uuid
	String_t* ___uuid_12;
	// System.Boolean PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey2::bHereNow
	bool ___bHereNow_13;
	// System.Boolean PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey2::bHereNowState
	bool ___bHereNowState_14;
	// System.Boolean PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey2::bUnsub
	bool ___bUnsub_15;
	// PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1 PubNubMessaging.Tests.TestCGCHAddListRemoveSubscribeStateHereNowUnsub/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__Iterator1/<DoCGCHAddListRemoveSubscribeStateHereNowUnsub>c__AnonStorey2::<>f__ref$1
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171 * ___U3CU3Ef__refU241_16;

public:
	inline static int32_t get_offset_of_testName_0() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560, ___testName_0)); }
	inline String_t* get_testName_0() const { return ___testName_0; }
	inline String_t** get_address_of_testName_0() { return &___testName_0; }
	inline void set_testName_0(String_t* value)
	{
		___testName_0 = value;
		Il2CppCodeGenWriteBarrier(&___testName_0, value);
	}

	inline static int32_t get_offset_of_bAddChannel_1() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560, ___bAddChannel_1)); }
	inline bool get_bAddChannel_1() const { return ___bAddChannel_1; }
	inline bool* get_address_of_bAddChannel_1() { return &___bAddChannel_1; }
	inline void set_bAddChannel_1(bool value)
	{
		___bAddChannel_1 = value;
	}

	inline static int32_t get_offset_of_cg_2() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560, ___cg_2)); }
	inline String_t* get_cg_2() const { return ___cg_2; }
	inline String_t** get_address_of_cg_2() { return &___cg_2; }
	inline void set_cg_2(String_t* value)
	{
		___cg_2 = value;
		Il2CppCodeGenWriteBarrier(&___cg_2, value);
	}

	inline static int32_t get_offset_of_channel_3() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560, ___channel_3)); }
	inline String_t* get_channel_3() const { return ___channel_3; }
	inline String_t** get_address_of_channel_3() { return &___channel_3; }
	inline void set_channel_3(String_t* value)
	{
		___channel_3 = value;
		Il2CppCodeGenWriteBarrier(&___channel_3, value);
	}

	inline static int32_t get_offset_of_bGetChannel_4() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560, ___bGetChannel_4)); }
	inline bool get_bGetChannel_4() const { return ___bGetChannel_4; }
	inline bool* get_address_of_bGetChannel_4() { return &___bGetChannel_4; }
	inline void set_bGetChannel_4(bool value)
	{
		___bGetChannel_4 = value;
	}

	inline static int32_t get_offset_of_pubMessage_5() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560, ___pubMessage_5)); }
	inline String_t* get_pubMessage_5() const { return ___pubMessage_5; }
	inline String_t** get_address_of_pubMessage_5() { return &___pubMessage_5; }
	inline void set_pubMessage_5(String_t* value)
	{
		___pubMessage_5 = value;
		Il2CppCodeGenWriteBarrier(&___pubMessage_5, value);
	}

	inline static int32_t get_offset_of_bSubMessage_6() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560, ___bSubMessage_6)); }
	inline bool get_bSubMessage_6() const { return ___bSubMessage_6; }
	inline bool* get_address_of_bSubMessage_6() { return &___bSubMessage_6; }
	inline void set_bSubMessage_6(bool value)
	{
		___bSubMessage_6 = value;
	}

	inline static int32_t get_offset_of_ch_7() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560, ___ch_7)); }
	inline String_t* get_ch_7() const { return ___ch_7; }
	inline String_t** get_address_of_ch_7() { return &___ch_7; }
	inline void set_ch_7(String_t* value)
	{
		___ch_7 = value;
		Il2CppCodeGenWriteBarrier(&___ch_7, value);
	}

	inline static int32_t get_offset_of_bSubMessage2_8() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560, ___bSubMessage2_8)); }
	inline bool get_bSubMessage2_8() const { return ___bSubMessage2_8; }
	inline bool* get_address_of_bSubMessage2_8() { return &___bSubMessage2_8; }
	inline void set_bSubMessage2_8(bool value)
	{
		___bSubMessage2_8 = value;
	}

	inline static int32_t get_offset_of_bSubConnected_9() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560, ___bSubConnected_9)); }
	inline bool get_bSubConnected_9() const { return ___bSubConnected_9; }
	inline bool* get_address_of_bSubConnected_9() { return &___bSubConnected_9; }
	inline void set_bSubConnected_9(bool value)
	{
		___bSubConnected_9 = value;
	}

	inline static int32_t get_offset_of_state_10() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560, ___state_10)); }
	inline String_t* get_state_10() const { return ___state_10; }
	inline String_t** get_address_of_state_10() { return &___state_10; }
	inline void set_state_10(String_t* value)
	{
		___state_10 = value;
		Il2CppCodeGenWriteBarrier(&___state_10, value);
	}

	inline static int32_t get_offset_of_bSetState_11() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560, ___bSetState_11)); }
	inline bool get_bSetState_11() const { return ___bSetState_11; }
	inline bool* get_address_of_bSetState_11() { return &___bSetState_11; }
	inline void set_bSetState_11(bool value)
	{
		___bSetState_11 = value;
	}

	inline static int32_t get_offset_of_uuid_12() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560, ___uuid_12)); }
	inline String_t* get_uuid_12() const { return ___uuid_12; }
	inline String_t** get_address_of_uuid_12() { return &___uuid_12; }
	inline void set_uuid_12(String_t* value)
	{
		___uuid_12 = value;
		Il2CppCodeGenWriteBarrier(&___uuid_12, value);
	}

	inline static int32_t get_offset_of_bHereNow_13() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560, ___bHereNow_13)); }
	inline bool get_bHereNow_13() const { return ___bHereNow_13; }
	inline bool* get_address_of_bHereNow_13() { return &___bHereNow_13; }
	inline void set_bHereNow_13(bool value)
	{
		___bHereNow_13 = value;
	}

	inline static int32_t get_offset_of_bHereNowState_14() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560, ___bHereNowState_14)); }
	inline bool get_bHereNowState_14() const { return ___bHereNowState_14; }
	inline bool* get_address_of_bHereNowState_14() { return &___bHereNowState_14; }
	inline void set_bHereNowState_14(bool value)
	{
		___bHereNowState_14 = value;
	}

	inline static int32_t get_offset_of_bUnsub_15() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560, ___bUnsub_15)); }
	inline bool get_bUnsub_15() const { return ___bUnsub_15; }
	inline bool* get_address_of_bUnsub_15() { return &___bUnsub_15; }
	inline void set_bUnsub_15(bool value)
	{
		___bUnsub_15 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_16() { return static_cast<int32_t>(offsetof(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560, ___U3CU3Ef__refU241_16)); }
	inline U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171 * get_U3CU3Ef__refU241_16() const { return ___U3CU3Ef__refU241_16; }
	inline U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171 ** get_address_of_U3CU3Ef__refU241_16() { return &___U3CU3Ef__refU241_16; }
	inline void set_U3CU3Ef__refU241_16(U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171 * value)
	{
		___U3CU3Ef__refU241_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
