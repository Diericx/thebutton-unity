﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestConnectedMessage
struct TestConnectedMessage_t4278862304;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestConnectedMessage::.ctor()
extern "C"  void TestConnectedMessage__ctor_m3217542516 (TestConnectedMessage_t4278862304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestConnectedMessage::Start()
extern "C"  Il2CppObject * TestConnectedMessage_Start_m2440975604 (TestConnectedMessage_t4278862304 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
