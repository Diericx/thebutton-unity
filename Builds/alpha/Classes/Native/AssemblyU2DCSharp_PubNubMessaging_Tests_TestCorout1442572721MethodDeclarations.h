﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestCoroutineRunIntegrationHBError
struct TestCoroutineRunIntegrationHBError_t1442572721;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestCoroutineRunIntegrationHBError::.ctor()
extern "C"  void TestCoroutineRunIntegrationHBError__ctor_m3016527259 (TestCoroutineRunIntegrationHBError_t1442572721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestCoroutineRunIntegrationHBError::Start()
extern "C"  Il2CppObject * TestCoroutineRunIntegrationHBError_Start_m3549876169 (TestCoroutineRunIntegrationHBError_t1442572721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
