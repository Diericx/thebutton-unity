﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestCoroutineRunIntegerationPHBError
struct TestCoroutineRunIntegerationPHBError_t1479832066;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestCoroutineRunIntegerationPHBError::.ctor()
extern "C"  void TestCoroutineRunIntegerationPHBError__ctor_m2688793526 (TestCoroutineRunIntegerationPHBError_t1479832066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestCoroutineRunIntegerationPHBError::Start()
extern "C"  Il2CppObject * TestCoroutineRunIntegerationPHBError_Start_m1089854750 (TestCoroutineRunIntegerationPHBError_t1479832066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
