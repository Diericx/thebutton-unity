﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestCoroutineRunSubscribeMultiple
struct TestCoroutineRunSubscribeMultiple_t1944684459;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestCoroutineRunSubscribeMultiple::.ctor()
extern "C"  void TestCoroutineRunSubscribeMultiple__ctor_m3944661727 (TestCoroutineRunSubscribeMultiple_t1944684459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestCoroutineRunSubscribeMultiple::Start()
extern "C"  Il2CppObject * TestCoroutineRunSubscribeMultiple_Start_m4278145149 (TestCoroutineRunSubscribeMultiple_t1944684459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
