﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestCoroutineRunIntegrationPHB
struct TestCoroutineRunIntegrationPHB_t2070611827;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestCoroutineRunIntegrationPHB::.ctor()
extern "C"  void TestCoroutineRunIntegrationPHB__ctor_m789332247 (TestCoroutineRunIntegrationPHB_t2070611827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestCoroutineRunIntegrationPHB::Start()
extern "C"  Il2CppObject * TestCoroutineRunIntegrationPHB_Start_m3115866905 (TestCoroutineRunIntegrationPHB_t2070611827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
