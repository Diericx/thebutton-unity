﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestCoroutineRunIntegrationTests
struct TestCoroutineRunIntegrationTests_t3041896496;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestCoroutineRunIntegrationTests::.ctor()
extern "C"  void TestCoroutineRunIntegrationTests__ctor_m2136118726 (TestCoroutineRunIntegrationTests_t3041896496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestCoroutineRunIntegrationTests::Start()
extern "C"  Il2CppObject * TestCoroutineRunIntegrationTests_Start_m574541882 (TestCoroutineRunIntegrationTests_t3041896496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
