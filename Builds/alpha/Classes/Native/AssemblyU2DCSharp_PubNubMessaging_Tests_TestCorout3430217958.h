﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PubNubMessaging.Tests.CommonIntergrationTests
struct CommonIntergrationTests_t1691354350;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.String
struct String_t;
// PubNubMessaging.Core.Pubnub
struct Pubnub_t2451529532;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// PubNubMessaging.Tests.TestCoroutineRunIntegrationSub
struct TestCoroutineRunIntegrationSub_t786852867;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ResponseTyp1248099179.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestCoroutineRunIntegrationSub/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t3430217958  : public Il2CppObject
{
public:
	// PubNubMessaging.Tests.CommonIntergrationTests PubNubMessaging.Tests.TestCoroutineRunIntegrationSub/<Start>c__Iterator0::<common>__0
	CommonIntergrationTests_t1691354350 * ___U3CcommonU3E__0_0;
	// System.String[] PubNubMessaging.Tests.TestCoroutineRunIntegrationSub/<Start>c__Iterator0::<multiChannel>__1
	StringU5BU5D_t1642385972* ___U3CmultiChannelU3E__1_1;
	// PubNubMessaging.Core.CurrentRequestType PubNubMessaging.Tests.TestCoroutineRunIntegrationSub/<Start>c__Iterator0::<crt>__2
	int32_t ___U3CcrtU3E__2_2;
	// System.String PubNubMessaging.Tests.TestCoroutineRunIntegrationSub/<Start>c__Iterator0::<expectedMessage>__3
	String_t* ___U3CexpectedMessageU3E__3_3;
	// System.String PubNubMessaging.Tests.TestCoroutineRunIntegrationSub/<Start>c__Iterator0::<expectedChannels>__4
	String_t* ___U3CexpectedChannelsU3E__4_4;
	// PubNubMessaging.Core.ResponseType PubNubMessaging.Tests.TestCoroutineRunIntegrationSub/<Start>c__Iterator0::<respType>__5
	int32_t ___U3CrespTypeU3E__5_5;
	// PubNubMessaging.Core.Pubnub PubNubMessaging.Tests.TestCoroutineRunIntegrationSub/<Start>c__Iterator0::<pubnub>__6
	Pubnub_t2451529532 * ___U3CpubnubU3E__6_6;
	// System.Int64 PubNubMessaging.Tests.TestCoroutineRunIntegrationSub/<Start>c__Iterator0::<nanoSecondTime>__7
	int64_t ___U3CnanoSecondTimeU3E__7_7;
	// System.String PubNubMessaging.Tests.TestCoroutineRunIntegrationSub/<Start>c__Iterator0::<url>__8
	String_t* ___U3CurlU3E__8_8;
	// System.Collections.IEnumerator PubNubMessaging.Tests.TestCoroutineRunIntegrationSub/<Start>c__Iterator0::<ienum>__9
	Il2CppObject * ___U3CienumU3E__9_9;
	// PubNubMessaging.Tests.TestCoroutineRunIntegrationSub PubNubMessaging.Tests.TestCoroutineRunIntegrationSub/<Start>c__Iterator0::$this
	TestCoroutineRunIntegrationSub_t786852867 * ___U24this_10;
	// System.Object PubNubMessaging.Tests.TestCoroutineRunIntegrationSub/<Start>c__Iterator0::$current
	Il2CppObject * ___U24current_11;
	// System.Boolean PubNubMessaging.Tests.TestCoroutineRunIntegrationSub/<Start>c__Iterator0::$disposing
	bool ___U24disposing_12;
	// System.Int32 PubNubMessaging.Tests.TestCoroutineRunIntegrationSub/<Start>c__Iterator0::$PC
	int32_t ___U24PC_13;

public:
	inline static int32_t get_offset_of_U3CcommonU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3430217958, ___U3CcommonU3E__0_0)); }
	inline CommonIntergrationTests_t1691354350 * get_U3CcommonU3E__0_0() const { return ___U3CcommonU3E__0_0; }
	inline CommonIntergrationTests_t1691354350 ** get_address_of_U3CcommonU3E__0_0() { return &___U3CcommonU3E__0_0; }
	inline void set_U3CcommonU3E__0_0(CommonIntergrationTests_t1691354350 * value)
	{
		___U3CcommonU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcommonU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CmultiChannelU3E__1_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3430217958, ___U3CmultiChannelU3E__1_1)); }
	inline StringU5BU5D_t1642385972* get_U3CmultiChannelU3E__1_1() const { return ___U3CmultiChannelU3E__1_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CmultiChannelU3E__1_1() { return &___U3CmultiChannelU3E__1_1; }
	inline void set_U3CmultiChannelU3E__1_1(StringU5BU5D_t1642385972* value)
	{
		___U3CmultiChannelU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmultiChannelU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CcrtU3E__2_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3430217958, ___U3CcrtU3E__2_2)); }
	inline int32_t get_U3CcrtU3E__2_2() const { return ___U3CcrtU3E__2_2; }
	inline int32_t* get_address_of_U3CcrtU3E__2_2() { return &___U3CcrtU3E__2_2; }
	inline void set_U3CcrtU3E__2_2(int32_t value)
	{
		___U3CcrtU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CexpectedMessageU3E__3_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3430217958, ___U3CexpectedMessageU3E__3_3)); }
	inline String_t* get_U3CexpectedMessageU3E__3_3() const { return ___U3CexpectedMessageU3E__3_3; }
	inline String_t** get_address_of_U3CexpectedMessageU3E__3_3() { return &___U3CexpectedMessageU3E__3_3; }
	inline void set_U3CexpectedMessageU3E__3_3(String_t* value)
	{
		___U3CexpectedMessageU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CexpectedMessageU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CexpectedChannelsU3E__4_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3430217958, ___U3CexpectedChannelsU3E__4_4)); }
	inline String_t* get_U3CexpectedChannelsU3E__4_4() const { return ___U3CexpectedChannelsU3E__4_4; }
	inline String_t** get_address_of_U3CexpectedChannelsU3E__4_4() { return &___U3CexpectedChannelsU3E__4_4; }
	inline void set_U3CexpectedChannelsU3E__4_4(String_t* value)
	{
		___U3CexpectedChannelsU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CexpectedChannelsU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CrespTypeU3E__5_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3430217958, ___U3CrespTypeU3E__5_5)); }
	inline int32_t get_U3CrespTypeU3E__5_5() const { return ___U3CrespTypeU3E__5_5; }
	inline int32_t* get_address_of_U3CrespTypeU3E__5_5() { return &___U3CrespTypeU3E__5_5; }
	inline void set_U3CrespTypeU3E__5_5(int32_t value)
	{
		___U3CrespTypeU3E__5_5 = value;
	}

	inline static int32_t get_offset_of_U3CpubnubU3E__6_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3430217958, ___U3CpubnubU3E__6_6)); }
	inline Pubnub_t2451529532 * get_U3CpubnubU3E__6_6() const { return ___U3CpubnubU3E__6_6; }
	inline Pubnub_t2451529532 ** get_address_of_U3CpubnubU3E__6_6() { return &___U3CpubnubU3E__6_6; }
	inline void set_U3CpubnubU3E__6_6(Pubnub_t2451529532 * value)
	{
		___U3CpubnubU3E__6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpubnubU3E__6_6, value);
	}

	inline static int32_t get_offset_of_U3CnanoSecondTimeU3E__7_7() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3430217958, ___U3CnanoSecondTimeU3E__7_7)); }
	inline int64_t get_U3CnanoSecondTimeU3E__7_7() const { return ___U3CnanoSecondTimeU3E__7_7; }
	inline int64_t* get_address_of_U3CnanoSecondTimeU3E__7_7() { return &___U3CnanoSecondTimeU3E__7_7; }
	inline void set_U3CnanoSecondTimeU3E__7_7(int64_t value)
	{
		___U3CnanoSecondTimeU3E__7_7 = value;
	}

	inline static int32_t get_offset_of_U3CurlU3E__8_8() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3430217958, ___U3CurlU3E__8_8)); }
	inline String_t* get_U3CurlU3E__8_8() const { return ___U3CurlU3E__8_8; }
	inline String_t** get_address_of_U3CurlU3E__8_8() { return &___U3CurlU3E__8_8; }
	inline void set_U3CurlU3E__8_8(String_t* value)
	{
		___U3CurlU3E__8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CurlU3E__8_8, value);
	}

	inline static int32_t get_offset_of_U3CienumU3E__9_9() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3430217958, ___U3CienumU3E__9_9)); }
	inline Il2CppObject * get_U3CienumU3E__9_9() const { return ___U3CienumU3E__9_9; }
	inline Il2CppObject ** get_address_of_U3CienumU3E__9_9() { return &___U3CienumU3E__9_9; }
	inline void set_U3CienumU3E__9_9(Il2CppObject * value)
	{
		___U3CienumU3E__9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CienumU3E__9_9, value);
	}

	inline static int32_t get_offset_of_U24this_10() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3430217958, ___U24this_10)); }
	inline TestCoroutineRunIntegrationSub_t786852867 * get_U24this_10() const { return ___U24this_10; }
	inline TestCoroutineRunIntegrationSub_t786852867 ** get_address_of_U24this_10() { return &___U24this_10; }
	inline void set_U24this_10(TestCoroutineRunIntegrationSub_t786852867 * value)
	{
		___U24this_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_10, value);
	}

	inline static int32_t get_offset_of_U24current_11() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3430217958, ___U24current_11)); }
	inline Il2CppObject * get_U24current_11() const { return ___U24current_11; }
	inline Il2CppObject ** get_address_of_U24current_11() { return &___U24current_11; }
	inline void set_U24current_11(Il2CppObject * value)
	{
		___U24current_11 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_11, value);
	}

	inline static int32_t get_offset_of_U24disposing_12() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3430217958, ___U24disposing_12)); }
	inline bool get_U24disposing_12() const { return ___U24disposing_12; }
	inline bool* get_address_of_U24disposing_12() { return &___U24disposing_12; }
	inline void set_U24disposing_12(bool value)
	{
		___U24disposing_12 = value;
	}

	inline static int32_t get_offset_of_U24PC_13() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3430217958, ___U24PC_13)); }
	inline int32_t get_U24PC_13() const { return ___U24PC_13; }
	inline int32_t* get_address_of_U24PC_13() { return &___U24PC_13; }
	inline void set_U24PC_13(int32_t value)
	{
		___U24PC_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
