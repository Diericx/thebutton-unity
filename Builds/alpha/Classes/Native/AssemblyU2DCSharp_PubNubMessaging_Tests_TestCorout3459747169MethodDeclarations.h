﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestCoroutineRunSubscribeAbort
struct TestCoroutineRunSubscribeAbort_t3459747169;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestCoroutineRunSubscribeAbort::.ctor()
extern "C"  void TestCoroutineRunSubscribeAbort__ctor_m1957146945 (TestCoroutineRunSubscribeAbort_t3459747169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestCoroutineRunSubscribeAbort::Start()
extern "C"  Il2CppObject * TestCoroutineRunSubscribeAbort_Start_m3194876335 (TestCoroutineRunSubscribeAbort_t3459747169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
