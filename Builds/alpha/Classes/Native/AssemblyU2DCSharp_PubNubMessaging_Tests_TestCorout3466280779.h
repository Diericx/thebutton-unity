﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PubNubMessaging.Tests.CommonIntergrationTests
struct CommonIntergrationTests_t1691354350;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// PubNubMessaging.Tests.TestCoroutineRunIntegerationPHBError
struct TestCoroutineRunIntegerationPHBError_t1479832066;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ResponseTyp1248099179.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestCoroutineRunIntegerationPHBError/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t3466280779  : public Il2CppObject
{
public:
	// PubNubMessaging.Tests.CommonIntergrationTests PubNubMessaging.Tests.TestCoroutineRunIntegerationPHBError/<Start>c__Iterator0::<common>__0
	CommonIntergrationTests_t1691354350 * ___U3CcommonU3E__0_0;
	// System.String PubNubMessaging.Tests.TestCoroutineRunIntegerationPHBError/<Start>c__Iterator0::<url>__1
	String_t* ___U3CurlU3E__1_1;
	// System.String[] PubNubMessaging.Tests.TestCoroutineRunIntegerationPHBError/<Start>c__Iterator0::<multiChannel>__2
	StringU5BU5D_t1642385972* ___U3CmultiChannelU3E__2_2;
	// PubNubMessaging.Core.CurrentRequestType PubNubMessaging.Tests.TestCoroutineRunIntegerationPHBError/<Start>c__Iterator0::<crt>__3
	int32_t ___U3CcrtU3E__3_3;
	// System.String PubNubMessaging.Tests.TestCoroutineRunIntegerationPHBError/<Start>c__Iterator0::<expectedMessage>__4
	String_t* ___U3CexpectedMessageU3E__4_4;
	// System.String PubNubMessaging.Tests.TestCoroutineRunIntegerationPHBError/<Start>c__Iterator0::<expectedChannels>__5
	String_t* ___U3CexpectedChannelsU3E__5_5;
	// PubNubMessaging.Core.ResponseType PubNubMessaging.Tests.TestCoroutineRunIntegerationPHBError/<Start>c__Iterator0::<respType>__6
	int32_t ___U3CrespTypeU3E__6_6;
	// System.Collections.IEnumerator PubNubMessaging.Tests.TestCoroutineRunIntegerationPHBError/<Start>c__Iterator0::<ienum>__7
	Il2CppObject * ___U3CienumU3E__7_7;
	// PubNubMessaging.Tests.TestCoroutineRunIntegerationPHBError PubNubMessaging.Tests.TestCoroutineRunIntegerationPHBError/<Start>c__Iterator0::$this
	TestCoroutineRunIntegerationPHBError_t1479832066 * ___U24this_8;
	// System.Object PubNubMessaging.Tests.TestCoroutineRunIntegerationPHBError/<Start>c__Iterator0::$current
	Il2CppObject * ___U24current_9;
	// System.Boolean PubNubMessaging.Tests.TestCoroutineRunIntegerationPHBError/<Start>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 PubNubMessaging.Tests.TestCoroutineRunIntegerationPHBError/<Start>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3CcommonU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3466280779, ___U3CcommonU3E__0_0)); }
	inline CommonIntergrationTests_t1691354350 * get_U3CcommonU3E__0_0() const { return ___U3CcommonU3E__0_0; }
	inline CommonIntergrationTests_t1691354350 ** get_address_of_U3CcommonU3E__0_0() { return &___U3CcommonU3E__0_0; }
	inline void set_U3CcommonU3E__0_0(CommonIntergrationTests_t1691354350 * value)
	{
		___U3CcommonU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcommonU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CurlU3E__1_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3466280779, ___U3CurlU3E__1_1)); }
	inline String_t* get_U3CurlU3E__1_1() const { return ___U3CurlU3E__1_1; }
	inline String_t** get_address_of_U3CurlU3E__1_1() { return &___U3CurlU3E__1_1; }
	inline void set_U3CurlU3E__1_1(String_t* value)
	{
		___U3CurlU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CurlU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CmultiChannelU3E__2_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3466280779, ___U3CmultiChannelU3E__2_2)); }
	inline StringU5BU5D_t1642385972* get_U3CmultiChannelU3E__2_2() const { return ___U3CmultiChannelU3E__2_2; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CmultiChannelU3E__2_2() { return &___U3CmultiChannelU3E__2_2; }
	inline void set_U3CmultiChannelU3E__2_2(StringU5BU5D_t1642385972* value)
	{
		___U3CmultiChannelU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmultiChannelU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CcrtU3E__3_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3466280779, ___U3CcrtU3E__3_3)); }
	inline int32_t get_U3CcrtU3E__3_3() const { return ___U3CcrtU3E__3_3; }
	inline int32_t* get_address_of_U3CcrtU3E__3_3() { return &___U3CcrtU3E__3_3; }
	inline void set_U3CcrtU3E__3_3(int32_t value)
	{
		___U3CcrtU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3CexpectedMessageU3E__4_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3466280779, ___U3CexpectedMessageU3E__4_4)); }
	inline String_t* get_U3CexpectedMessageU3E__4_4() const { return ___U3CexpectedMessageU3E__4_4; }
	inline String_t** get_address_of_U3CexpectedMessageU3E__4_4() { return &___U3CexpectedMessageU3E__4_4; }
	inline void set_U3CexpectedMessageU3E__4_4(String_t* value)
	{
		___U3CexpectedMessageU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CexpectedMessageU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CexpectedChannelsU3E__5_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3466280779, ___U3CexpectedChannelsU3E__5_5)); }
	inline String_t* get_U3CexpectedChannelsU3E__5_5() const { return ___U3CexpectedChannelsU3E__5_5; }
	inline String_t** get_address_of_U3CexpectedChannelsU3E__5_5() { return &___U3CexpectedChannelsU3E__5_5; }
	inline void set_U3CexpectedChannelsU3E__5_5(String_t* value)
	{
		___U3CexpectedChannelsU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CexpectedChannelsU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3CrespTypeU3E__6_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3466280779, ___U3CrespTypeU3E__6_6)); }
	inline int32_t get_U3CrespTypeU3E__6_6() const { return ___U3CrespTypeU3E__6_6; }
	inline int32_t* get_address_of_U3CrespTypeU3E__6_6() { return &___U3CrespTypeU3E__6_6; }
	inline void set_U3CrespTypeU3E__6_6(int32_t value)
	{
		___U3CrespTypeU3E__6_6 = value;
	}

	inline static int32_t get_offset_of_U3CienumU3E__7_7() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3466280779, ___U3CienumU3E__7_7)); }
	inline Il2CppObject * get_U3CienumU3E__7_7() const { return ___U3CienumU3E__7_7; }
	inline Il2CppObject ** get_address_of_U3CienumU3E__7_7() { return &___U3CienumU3E__7_7; }
	inline void set_U3CienumU3E__7_7(Il2CppObject * value)
	{
		___U3CienumU3E__7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CienumU3E__7_7, value);
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3466280779, ___U24this_8)); }
	inline TestCoroutineRunIntegerationPHBError_t1479832066 * get_U24this_8() const { return ___U24this_8; }
	inline TestCoroutineRunIntegerationPHBError_t1479832066 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(TestCoroutineRunIntegerationPHBError_t1479832066 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_8, value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3466280779, ___U24current_9)); }
	inline Il2CppObject * get_U24current_9() const { return ___U24current_9; }
	inline Il2CppObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(Il2CppObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_9, value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3466280779, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3466280779, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
