﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestCoroutineRunIntegrationSubError
struct TestCoroutineRunIntegrationSubError_t3597817705;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestCoroutineRunIntegrationSubError::.ctor()
extern "C"  void TestCoroutineRunIntegrationSubError__ctor_m3282067321 (TestCoroutineRunIntegrationSubError_t3597817705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestCoroutineRunIntegrationSubError::Start()
extern "C"  Il2CppObject * TestCoroutineRunIntegrationSubError_Start_m1380522915 (TestCoroutineRunIntegrationSubError_t3597817705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
