﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestCoroutineRunIntegrationSubErrorTimeout
struct TestCoroutineRunIntegrationSubErrorTimeout_t3898362486;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestCoroutineRunIntegrationSubErrorTimeout::.ctor()
extern "C"  void TestCoroutineRunIntegrationSubErrorTimeout__ctor_m2548052076 (TestCoroutineRunIntegrationSubErrorTimeout_t3898362486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestCoroutineRunIntegrationSubErrorTimeout::Start()
extern "C"  Il2CppObject * TestCoroutineRunIntegrationSubErrorTimeout_Start_m1784217496 (TestCoroutineRunIntegrationSubErrorTimeout_t3898362486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
