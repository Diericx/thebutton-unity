﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestCoroutineRunIntegrationHB
struct TestCoroutineRunIntegrationHB_t277379567;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestCoroutineRunIntegrationHB::.ctor()
extern "C"  void TestCoroutineRunIntegrationHB__ctor_m2864604371 (TestCoroutineRunIntegrationHB_t277379567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestCoroutineRunIntegrationHB::Start()
extern "C"  Il2CppObject * TestCoroutineRunIntegrationHB_Start_m1239005857 (TestCoroutineRunIntegrationHB_t277379567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
