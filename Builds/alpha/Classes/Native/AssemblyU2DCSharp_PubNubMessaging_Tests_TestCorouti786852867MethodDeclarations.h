﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestCoroutineRunIntegrationSub
struct TestCoroutineRunIntegrationSub_t786852867;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestCoroutineRunIntegrationSub::.ctor()
extern "C"  void TestCoroutineRunIntegrationSub__ctor_m3050689745 (TestCoroutineRunIntegrationSub_t786852867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestCoroutineRunIntegrationSub::Start()
extern "C"  Il2CppObject * TestCoroutineRunIntegrationSub_Start_m664824499 (TestCoroutineRunIntegrationSub_t786852867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
