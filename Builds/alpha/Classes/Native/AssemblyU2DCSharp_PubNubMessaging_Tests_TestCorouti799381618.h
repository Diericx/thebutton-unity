﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PubNubMessaging.Tests.CommonIntergrationTests
struct CommonIntergrationTests_t1691354350;
// System.String[]
struct StringU5BU5D_t1642385972;
// PubNubMessaging.Core.Pubnub
struct Pubnub_t2451529532;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// PubNubMessaging.Tests.TestCoroutineRunIntegrationPHB
struct TestCoroutineRunIntegrationPHB_t2070611827;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ResponseTyp1248099179.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestCoroutineRunIntegrationPHB/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t799381618  : public Il2CppObject
{
public:
	// PubNubMessaging.Tests.CommonIntergrationTests PubNubMessaging.Tests.TestCoroutineRunIntegrationPHB/<Start>c__Iterator0::<common>__0
	CommonIntergrationTests_t1691354350 * ___U3CcommonU3E__0_0;
	// System.String[] PubNubMessaging.Tests.TestCoroutineRunIntegrationPHB/<Start>c__Iterator0::<multiChannel>__1
	StringU5BU5D_t1642385972* ___U3CmultiChannelU3E__1_1;
	// PubNubMessaging.Core.Pubnub PubNubMessaging.Tests.TestCoroutineRunIntegrationPHB/<Start>c__Iterator0::<pubnub>__2
	Pubnub_t2451529532 * ___U3CpubnubU3E__2_2;
	// PubNubMessaging.Core.CurrentRequestType PubNubMessaging.Tests.TestCoroutineRunIntegrationPHB/<Start>c__Iterator0::<crt>__3
	int32_t ___U3CcrtU3E__3_3;
	// System.String PubNubMessaging.Tests.TestCoroutineRunIntegrationPHB/<Start>c__Iterator0::<expectedMessage>__4
	String_t* ___U3CexpectedMessageU3E__4_4;
	// System.String PubNubMessaging.Tests.TestCoroutineRunIntegrationPHB/<Start>c__Iterator0::<expectedChannels>__5
	String_t* ___U3CexpectedChannelsU3E__5_5;
	// System.String PubNubMessaging.Tests.TestCoroutineRunIntegrationPHB/<Start>c__Iterator0::<url>__6
	String_t* ___U3CurlU3E__6_6;
	// PubNubMessaging.Core.ResponseType PubNubMessaging.Tests.TestCoroutineRunIntegrationPHB/<Start>c__Iterator0::<respType>__7
	int32_t ___U3CrespTypeU3E__7_7;
	// System.Collections.IEnumerator PubNubMessaging.Tests.TestCoroutineRunIntegrationPHB/<Start>c__Iterator0::<ienum>__8
	Il2CppObject * ___U3CienumU3E__8_8;
	// PubNubMessaging.Tests.TestCoroutineRunIntegrationPHB PubNubMessaging.Tests.TestCoroutineRunIntegrationPHB/<Start>c__Iterator0::$this
	TestCoroutineRunIntegrationPHB_t2070611827 * ___U24this_9;
	// System.Object PubNubMessaging.Tests.TestCoroutineRunIntegrationPHB/<Start>c__Iterator0::$current
	Il2CppObject * ___U24current_10;
	// System.Boolean PubNubMessaging.Tests.TestCoroutineRunIntegrationPHB/<Start>c__Iterator0::$disposing
	bool ___U24disposing_11;
	// System.Int32 PubNubMessaging.Tests.TestCoroutineRunIntegrationPHB/<Start>c__Iterator0::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_U3CcommonU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t799381618, ___U3CcommonU3E__0_0)); }
	inline CommonIntergrationTests_t1691354350 * get_U3CcommonU3E__0_0() const { return ___U3CcommonU3E__0_0; }
	inline CommonIntergrationTests_t1691354350 ** get_address_of_U3CcommonU3E__0_0() { return &___U3CcommonU3E__0_0; }
	inline void set_U3CcommonU3E__0_0(CommonIntergrationTests_t1691354350 * value)
	{
		___U3CcommonU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcommonU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CmultiChannelU3E__1_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t799381618, ___U3CmultiChannelU3E__1_1)); }
	inline StringU5BU5D_t1642385972* get_U3CmultiChannelU3E__1_1() const { return ___U3CmultiChannelU3E__1_1; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CmultiChannelU3E__1_1() { return &___U3CmultiChannelU3E__1_1; }
	inline void set_U3CmultiChannelU3E__1_1(StringU5BU5D_t1642385972* value)
	{
		___U3CmultiChannelU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CmultiChannelU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CpubnubU3E__2_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t799381618, ___U3CpubnubU3E__2_2)); }
	inline Pubnub_t2451529532 * get_U3CpubnubU3E__2_2() const { return ___U3CpubnubU3E__2_2; }
	inline Pubnub_t2451529532 ** get_address_of_U3CpubnubU3E__2_2() { return &___U3CpubnubU3E__2_2; }
	inline void set_U3CpubnubU3E__2_2(Pubnub_t2451529532 * value)
	{
		___U3CpubnubU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpubnubU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CcrtU3E__3_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t799381618, ___U3CcrtU3E__3_3)); }
	inline int32_t get_U3CcrtU3E__3_3() const { return ___U3CcrtU3E__3_3; }
	inline int32_t* get_address_of_U3CcrtU3E__3_3() { return &___U3CcrtU3E__3_3; }
	inline void set_U3CcrtU3E__3_3(int32_t value)
	{
		___U3CcrtU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3CexpectedMessageU3E__4_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t799381618, ___U3CexpectedMessageU3E__4_4)); }
	inline String_t* get_U3CexpectedMessageU3E__4_4() const { return ___U3CexpectedMessageU3E__4_4; }
	inline String_t** get_address_of_U3CexpectedMessageU3E__4_4() { return &___U3CexpectedMessageU3E__4_4; }
	inline void set_U3CexpectedMessageU3E__4_4(String_t* value)
	{
		___U3CexpectedMessageU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CexpectedMessageU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CexpectedChannelsU3E__5_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t799381618, ___U3CexpectedChannelsU3E__5_5)); }
	inline String_t* get_U3CexpectedChannelsU3E__5_5() const { return ___U3CexpectedChannelsU3E__5_5; }
	inline String_t** get_address_of_U3CexpectedChannelsU3E__5_5() { return &___U3CexpectedChannelsU3E__5_5; }
	inline void set_U3CexpectedChannelsU3E__5_5(String_t* value)
	{
		___U3CexpectedChannelsU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CexpectedChannelsU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3CurlU3E__6_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t799381618, ___U3CurlU3E__6_6)); }
	inline String_t* get_U3CurlU3E__6_6() const { return ___U3CurlU3E__6_6; }
	inline String_t** get_address_of_U3CurlU3E__6_6() { return &___U3CurlU3E__6_6; }
	inline void set_U3CurlU3E__6_6(String_t* value)
	{
		___U3CurlU3E__6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CurlU3E__6_6, value);
	}

	inline static int32_t get_offset_of_U3CrespTypeU3E__7_7() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t799381618, ___U3CrespTypeU3E__7_7)); }
	inline int32_t get_U3CrespTypeU3E__7_7() const { return ___U3CrespTypeU3E__7_7; }
	inline int32_t* get_address_of_U3CrespTypeU3E__7_7() { return &___U3CrespTypeU3E__7_7; }
	inline void set_U3CrespTypeU3E__7_7(int32_t value)
	{
		___U3CrespTypeU3E__7_7 = value;
	}

	inline static int32_t get_offset_of_U3CienumU3E__8_8() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t799381618, ___U3CienumU3E__8_8)); }
	inline Il2CppObject * get_U3CienumU3E__8_8() const { return ___U3CienumU3E__8_8; }
	inline Il2CppObject ** get_address_of_U3CienumU3E__8_8() { return &___U3CienumU3E__8_8; }
	inline void set_U3CienumU3E__8_8(Il2CppObject * value)
	{
		___U3CienumU3E__8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CienumU3E__8_8, value);
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t799381618, ___U24this_9)); }
	inline TestCoroutineRunIntegrationPHB_t2070611827 * get_U24this_9() const { return ___U24this_9; }
	inline TestCoroutineRunIntegrationPHB_t2070611827 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(TestCoroutineRunIntegrationPHB_t2070611827 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_9, value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t799381618, ___U24current_10)); }
	inline Il2CppObject * get_U24current_10() const { return ___U24current_10; }
	inline Il2CppObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(Il2CppObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_10, value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t799381618, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t799381618, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
