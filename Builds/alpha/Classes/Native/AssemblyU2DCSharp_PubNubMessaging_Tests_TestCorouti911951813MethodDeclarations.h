﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestCoroutineRunIntegerationNonSubError
struct TestCoroutineRunIntegerationNonSubError_t911951813;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestCoroutineRunIntegerationNonSubError::.ctor()
extern "C"  void TestCoroutineRunIntegerationNonSubError__ctor_m2303038595 (TestCoroutineRunIntegerationNonSubError_t911951813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestCoroutineRunIntegerationNonSubError::Start()
extern "C"  Il2CppObject * TestCoroutineRunIntegerationNonSubError_Start_m1134670801 (TestCoroutineRunIntegerationNonSubError_t911951813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
