﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1
struct U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2
struct  U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2::testName
	String_t* ___testName_0;
	// System.Boolean PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2::bAddChannel
	bool ___bAddChannel_1;
	// System.Boolean PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2::bSubConnected
	bool ___bSubConnected_2;
	// System.String PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2::ch
	String_t* ___ch_3;
	// System.String PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2::cg
	String_t* ___cg_4;
	// System.String PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2::state
	String_t* ___state_5;
	// System.Boolean PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2::bSetState
	bool ___bSetState_6;
	// System.String PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2::channel
	String_t* ___channel_7;
	// System.Boolean PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2::bSetUserState2
	bool ___bSetUserState2_8;
	// System.Boolean PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2::bGetUserState2
	bool ___bGetUserState2_9;
	// System.Boolean PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2::bRemoveAll
	bool ___bRemoveAll_10;
	// PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1 PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2::<>f__ref$1
	U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263 * ___U3CU3Ef__refU241_11;

public:
	inline static int32_t get_offset_of_testName_0() { return static_cast<int32_t>(offsetof(U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319, ___testName_0)); }
	inline String_t* get_testName_0() const { return ___testName_0; }
	inline String_t** get_address_of_testName_0() { return &___testName_0; }
	inline void set_testName_0(String_t* value)
	{
		___testName_0 = value;
		Il2CppCodeGenWriteBarrier(&___testName_0, value);
	}

	inline static int32_t get_offset_of_bAddChannel_1() { return static_cast<int32_t>(offsetof(U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319, ___bAddChannel_1)); }
	inline bool get_bAddChannel_1() const { return ___bAddChannel_1; }
	inline bool* get_address_of_bAddChannel_1() { return &___bAddChannel_1; }
	inline void set_bAddChannel_1(bool value)
	{
		___bAddChannel_1 = value;
	}

	inline static int32_t get_offset_of_bSubConnected_2() { return static_cast<int32_t>(offsetof(U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319, ___bSubConnected_2)); }
	inline bool get_bSubConnected_2() const { return ___bSubConnected_2; }
	inline bool* get_address_of_bSubConnected_2() { return &___bSubConnected_2; }
	inline void set_bSubConnected_2(bool value)
	{
		___bSubConnected_2 = value;
	}

	inline static int32_t get_offset_of_ch_3() { return static_cast<int32_t>(offsetof(U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319, ___ch_3)); }
	inline String_t* get_ch_3() const { return ___ch_3; }
	inline String_t** get_address_of_ch_3() { return &___ch_3; }
	inline void set_ch_3(String_t* value)
	{
		___ch_3 = value;
		Il2CppCodeGenWriteBarrier(&___ch_3, value);
	}

	inline static int32_t get_offset_of_cg_4() { return static_cast<int32_t>(offsetof(U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319, ___cg_4)); }
	inline String_t* get_cg_4() const { return ___cg_4; }
	inline String_t** get_address_of_cg_4() { return &___cg_4; }
	inline void set_cg_4(String_t* value)
	{
		___cg_4 = value;
		Il2CppCodeGenWriteBarrier(&___cg_4, value);
	}

	inline static int32_t get_offset_of_state_5() { return static_cast<int32_t>(offsetof(U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319, ___state_5)); }
	inline String_t* get_state_5() const { return ___state_5; }
	inline String_t** get_address_of_state_5() { return &___state_5; }
	inline void set_state_5(String_t* value)
	{
		___state_5 = value;
		Il2CppCodeGenWriteBarrier(&___state_5, value);
	}

	inline static int32_t get_offset_of_bSetState_6() { return static_cast<int32_t>(offsetof(U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319, ___bSetState_6)); }
	inline bool get_bSetState_6() const { return ___bSetState_6; }
	inline bool* get_address_of_bSetState_6() { return &___bSetState_6; }
	inline void set_bSetState_6(bool value)
	{
		___bSetState_6 = value;
	}

	inline static int32_t get_offset_of_channel_7() { return static_cast<int32_t>(offsetof(U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319, ___channel_7)); }
	inline String_t* get_channel_7() const { return ___channel_7; }
	inline String_t** get_address_of_channel_7() { return &___channel_7; }
	inline void set_channel_7(String_t* value)
	{
		___channel_7 = value;
		Il2CppCodeGenWriteBarrier(&___channel_7, value);
	}

	inline static int32_t get_offset_of_bSetUserState2_8() { return static_cast<int32_t>(offsetof(U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319, ___bSetUserState2_8)); }
	inline bool get_bSetUserState2_8() const { return ___bSetUserState2_8; }
	inline bool* get_address_of_bSetUserState2_8() { return &___bSetUserState2_8; }
	inline void set_bSetUserState2_8(bool value)
	{
		___bSetUserState2_8 = value;
	}

	inline static int32_t get_offset_of_bGetUserState2_9() { return static_cast<int32_t>(offsetof(U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319, ___bGetUserState2_9)); }
	inline bool get_bGetUserState2_9() const { return ___bGetUserState2_9; }
	inline bool* get_address_of_bGetUserState2_9() { return &___bGetUserState2_9; }
	inline void set_bGetUserState2_9(bool value)
	{
		___bGetUserState2_9 = value;
	}

	inline static int32_t get_offset_of_bRemoveAll_10() { return static_cast<int32_t>(offsetof(U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319, ___bRemoveAll_10)); }
	inline bool get_bRemoveAll_10() const { return ___bRemoveAll_10; }
	inline bool* get_address_of_bRemoveAll_10() { return &___bRemoveAll_10; }
	inline void set_bRemoveAll_10(bool value)
	{
		___bRemoveAll_10 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_11() { return static_cast<int32_t>(offsetof(U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319, ___U3CU3Ef__refU241_11)); }
	inline U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263 * get_U3CU3Ef__refU241_11() const { return ___U3CU3Ef__refU241_11; }
	inline U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263 ** get_address_of_U3CU3Ef__refU241_11() { return &___U3CU3Ef__refU241_11; }
	inline void set_U3CU3Ef__refU241_11(U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263 * value)
	{
		___U3CU3Ef__refU241_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
