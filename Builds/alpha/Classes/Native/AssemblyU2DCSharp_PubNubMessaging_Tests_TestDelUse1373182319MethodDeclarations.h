﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2
struct U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2::.ctor()
extern "C"  void U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2__ctor_m415832434 (U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2::<>m__0(System.String)
extern "C"  void U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_U3CU3Em__0_m3887776199 (U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2::<>m__1(System.String)
extern "C"  void U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_U3CU3Em__1_m1977981324 (U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2::<>m__2(System.String)
extern "C"  void U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_U3CU3Em__2_m4114728785 (U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319 * __this, String_t* ___retConnect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2::<>m__3(System.String)
extern "C"  void U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_U3CU3Em__3_m2204933910 (U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2::<>m__4(System.String)
extern "C"  void U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_U3CU3Em__4_m4138652083 (U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2::<>m__5(System.String)
extern "C"  void U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_U3CU3Em__5_m2228857208 (U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1/<DoSubscribeSetStateDelStateCG>c__AnonStorey2::<>m__6(System.String)
extern "C"  void U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_U3CU3Em__6_m70637373 (U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319 * __this, String_t* ___pub0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
