﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1
struct U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1::.ctor()
extern "C"  void U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1__ctor_m2673719092 (U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1::MoveNext()
extern "C"  bool U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_MoveNext_m1213042924 (U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m676928476 (U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2860141028 (U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1::Dispose()
extern "C"  void U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_Dispose_m2185623261 (U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestDelUserStateCG/<DoSubscribeSetStateDelStateCG>c__Iterator1::Reset()
extern "C"  void U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_Reset_m1604638195 (U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
