﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestDelUserStateCG
struct TestDelUserStateCG_t3399981929;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Core.PubnubClientError
struct PubnubClientError_t110317921;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubClient110317921.h"

// System.Void PubNubMessaging.Tests.TestDelUserStateCG::.ctor()
extern "C"  void TestDelUserStateCG__ctor_m283940395 (TestDelUserStateCG_t3399981929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestDelUserStateCG::Start()
extern "C"  Il2CppObject * TestDelUserStateCG_Start_m872607249 (TestDelUserStateCG_t3399981929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestDelUserStateCG::DoSubscribeSetStateDelStateCG(System.Boolean,System.String,System.Boolean,System.Boolean,System.Object,System.String,System.Boolean)
extern "C"  Il2CppObject * TestDelUserStateCG_DoSubscribeSetStateDelStateCG_m1610582475 (TestDelUserStateCG_t3399981929 * __this, bool ___ssl0, String_t* ___testName1, bool ___asObject2, bool ___withCipher3, Il2CppObject * ___message4, String_t* ___expectedStringResponse5, bool ___matchExpectedStringResponse6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestDelUserStateCG::DisplayErrorMessage(PubNubMessaging.Core.PubnubClientError)
extern "C"  void TestDelUserStateCG_DisplayErrorMessage_m2442068138 (TestDelUserStateCG_t3399981929 * __this, PubnubClientError_t110317921 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestDelUserStateCG::DisplayReturnMessageDummy(System.Object)
extern "C"  void TestDelUserStateCG_DisplayReturnMessageDummy_m3170765968 (TestDelUserStateCG_t3399981929 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
