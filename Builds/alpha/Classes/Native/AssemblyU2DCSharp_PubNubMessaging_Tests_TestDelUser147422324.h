﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Int64>
struct Dictionary_2_t2823857299;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// PubNubMessaging.Tests.TestDelUserStateCG
struct TestDelUserStateCG_t3399981929;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestDelUserStateCG/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t147422324  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int64> PubNubMessaging.Tests.TestDelUserStateCG/<Start>c__Iterator0::<Message1>__0
	Dictionary_2_t2823857299 * ___U3CMessage1U3E__0_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PubNubMessaging.Tests.TestDelUserStateCG/<Start>c__Iterator0::<Message2>__1
	Dictionary_2_t3943999495 * ___U3CMessage2U3E__1_1;
	// System.Object PubNubMessaging.Tests.TestDelUserStateCG/<Start>c__Iterator0::<Message>__2
	Il2CppObject * ___U3CMessageU3E__2_2;
	// System.String PubNubMessaging.Tests.TestDelUserStateCG/<Start>c__Iterator0::<expectedMessage>__3
	String_t* ___U3CexpectedMessageU3E__3_3;
	// PubNubMessaging.Tests.TestDelUserStateCG PubNubMessaging.Tests.TestDelUserStateCG/<Start>c__Iterator0::$this
	TestDelUserStateCG_t3399981929 * ___U24this_4;
	// System.Object PubNubMessaging.Tests.TestDelUserStateCG/<Start>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean PubNubMessaging.Tests.TestDelUserStateCG/<Start>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 PubNubMessaging.Tests.TestDelUserStateCG/<Start>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CMessage1U3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t147422324, ___U3CMessage1U3E__0_0)); }
	inline Dictionary_2_t2823857299 * get_U3CMessage1U3E__0_0() const { return ___U3CMessage1U3E__0_0; }
	inline Dictionary_2_t2823857299 ** get_address_of_U3CMessage1U3E__0_0() { return &___U3CMessage1U3E__0_0; }
	inline void set_U3CMessage1U3E__0_0(Dictionary_2_t2823857299 * value)
	{
		___U3CMessage1U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMessage1U3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CMessage2U3E__1_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t147422324, ___U3CMessage2U3E__1_1)); }
	inline Dictionary_2_t3943999495 * get_U3CMessage2U3E__1_1() const { return ___U3CMessage2U3E__1_1; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CMessage2U3E__1_1() { return &___U3CMessage2U3E__1_1; }
	inline void set_U3CMessage2U3E__1_1(Dictionary_2_t3943999495 * value)
	{
		___U3CMessage2U3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMessage2U3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CMessageU3E__2_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t147422324, ___U3CMessageU3E__2_2)); }
	inline Il2CppObject * get_U3CMessageU3E__2_2() const { return ___U3CMessageU3E__2_2; }
	inline Il2CppObject ** get_address_of_U3CMessageU3E__2_2() { return &___U3CMessageU3E__2_2; }
	inline void set_U3CMessageU3E__2_2(Il2CppObject * value)
	{
		___U3CMessageU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMessageU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CexpectedMessageU3E__3_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t147422324, ___U3CexpectedMessageU3E__3_3)); }
	inline String_t* get_U3CexpectedMessageU3E__3_3() const { return ___U3CexpectedMessageU3E__3_3; }
	inline String_t** get_address_of_U3CexpectedMessageU3E__3_3() { return &___U3CexpectedMessageU3E__3_3; }
	inline void set_U3CexpectedMessageU3E__3_3(String_t* value)
	{
		___U3CexpectedMessageU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CexpectedMessageU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t147422324, ___U24this_4)); }
	inline TestDelUserStateCG_t3399981929 * get_U24this_4() const { return ___U24this_4; }
	inline TestDelUserStateCG_t3399981929 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TestDelUserStateCG_t3399981929 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t147422324, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t147422324, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t147422324, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
