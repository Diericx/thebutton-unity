﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestDetailedHistory
struct TestDetailedHistory_t3933854162;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestDetailedHistory::.ctor()
extern "C"  void TestDetailedHistory__ctor_m2362210012 (TestDetailedHistory_t3933854162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestDetailedHistory::Start()
extern "C"  Il2CppObject * TestDetailedHistory_Start_m912071816 (TestDetailedHistory_t3933854162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
