﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestDetailedHistoryParams
struct TestDetailedHistoryParams_t206760270;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestDetailedHistoryParams::.ctor()
extern "C"  void TestDetailedHistoryParams__ctor_m3000565938 (TestDetailedHistoryParams_t206760270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestDetailedHistoryParams::Start()
extern "C"  Il2CppObject * TestDetailedHistoryParams_Start_m2828689510 (TestDetailedHistoryParams_t206760270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
