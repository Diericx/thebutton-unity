﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestGlobalHereNow
struct  TestGlobalHereNow_t1271793713  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean PubNubMessaging.Tests.TestGlobalHereNow::SslOn
	bool ___SslOn_2;
	// System.Boolean PubNubMessaging.Tests.TestGlobalHereNow::CipherOn
	bool ___CipherOn_3;
	// System.Boolean PubNubMessaging.Tests.TestGlobalHereNow::AsObject
	bool ___AsObject_4;

public:
	inline static int32_t get_offset_of_SslOn_2() { return static_cast<int32_t>(offsetof(TestGlobalHereNow_t1271793713, ___SslOn_2)); }
	inline bool get_SslOn_2() const { return ___SslOn_2; }
	inline bool* get_address_of_SslOn_2() { return &___SslOn_2; }
	inline void set_SslOn_2(bool value)
	{
		___SslOn_2 = value;
	}

	inline static int32_t get_offset_of_CipherOn_3() { return static_cast<int32_t>(offsetof(TestGlobalHereNow_t1271793713, ___CipherOn_3)); }
	inline bool get_CipherOn_3() const { return ___CipherOn_3; }
	inline bool* get_address_of_CipherOn_3() { return &___CipherOn_3; }
	inline void set_CipherOn_3(bool value)
	{
		___CipherOn_3 = value;
	}

	inline static int32_t get_offset_of_AsObject_4() { return static_cast<int32_t>(offsetof(TestGlobalHereNow_t1271793713, ___AsObject_4)); }
	inline bool get_AsObject_4() const { return ___AsObject_4; }
	inline bool* get_address_of_AsObject_4() { return &___AsObject_4; }
	inline void set_AsObject_4(bool value)
	{
		___AsObject_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
