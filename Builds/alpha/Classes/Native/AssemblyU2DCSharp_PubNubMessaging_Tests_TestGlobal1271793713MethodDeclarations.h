﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestGlobalHereNow
struct TestGlobalHereNow_t1271793713;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestGlobalHereNow::.ctor()
extern "C"  void TestGlobalHereNow__ctor_m410950895 (TestGlobalHereNow_t1271793713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestGlobalHereNow::Start()
extern "C"  Il2CppObject * TestGlobalHereNow_Start_m2714241005 (TestGlobalHereNow_t1271793713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
