﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestHBTimeout
struct TestHBTimeout_t1340712739;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestHBTimeout::.ctor()
extern "C"  void TestHBTimeout__ctor_m2686953865 (TestHBTimeout_t1340712739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestHBTimeout::Start()
extern "C"  Il2CppObject * TestHBTimeout_Start_m2245288471 (TestHBTimeout_t1340712739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
