﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestHereNow
struct  TestHereNow_t236862272  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean PubNubMessaging.Tests.TestHereNow::SslOn
	bool ___SslOn_2;
	// System.Boolean PubNubMessaging.Tests.TestHereNow::CipherOn
	bool ___CipherOn_3;
	// System.Boolean PubNubMessaging.Tests.TestHereNow::AsObject
	bool ___AsObject_4;
	// System.Boolean PubNubMessaging.Tests.TestHereNow::WithState
	bool ___WithState_5;
	// System.String PubNubMessaging.Tests.TestHereNow::CustomUUID
	String_t* ___CustomUUID_6;

public:
	inline static int32_t get_offset_of_SslOn_2() { return static_cast<int32_t>(offsetof(TestHereNow_t236862272, ___SslOn_2)); }
	inline bool get_SslOn_2() const { return ___SslOn_2; }
	inline bool* get_address_of_SslOn_2() { return &___SslOn_2; }
	inline void set_SslOn_2(bool value)
	{
		___SslOn_2 = value;
	}

	inline static int32_t get_offset_of_CipherOn_3() { return static_cast<int32_t>(offsetof(TestHereNow_t236862272, ___CipherOn_3)); }
	inline bool get_CipherOn_3() const { return ___CipherOn_3; }
	inline bool* get_address_of_CipherOn_3() { return &___CipherOn_3; }
	inline void set_CipherOn_3(bool value)
	{
		___CipherOn_3 = value;
	}

	inline static int32_t get_offset_of_AsObject_4() { return static_cast<int32_t>(offsetof(TestHereNow_t236862272, ___AsObject_4)); }
	inline bool get_AsObject_4() const { return ___AsObject_4; }
	inline bool* get_address_of_AsObject_4() { return &___AsObject_4; }
	inline void set_AsObject_4(bool value)
	{
		___AsObject_4 = value;
	}

	inline static int32_t get_offset_of_WithState_5() { return static_cast<int32_t>(offsetof(TestHereNow_t236862272, ___WithState_5)); }
	inline bool get_WithState_5() const { return ___WithState_5; }
	inline bool* get_address_of_WithState_5() { return &___WithState_5; }
	inline void set_WithState_5(bool value)
	{
		___WithState_5 = value;
	}

	inline static int32_t get_offset_of_CustomUUID_6() { return static_cast<int32_t>(offsetof(TestHereNow_t236862272, ___CustomUUID_6)); }
	inline String_t* get_CustomUUID_6() const { return ___CustomUUID_6; }
	inline String_t** get_address_of_CustomUUID_6() { return &___CustomUUID_6; }
	inline void set_CustomUUID_6(String_t* value)
	{
		___CustomUUID_6 = value;
		Il2CppCodeGenWriteBarrier(&___CustomUUID_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
