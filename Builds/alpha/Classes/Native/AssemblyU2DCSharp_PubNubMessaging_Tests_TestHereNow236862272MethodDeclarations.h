﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestHereNow
struct TestHereNow_t236862272;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestHereNow::.ctor()
extern "C"  void TestHereNow__ctor_m3827334094 (TestHereNow_t236862272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestHereNow::Start()
extern "C"  Il2CppObject * TestHereNow_Start_m2172459966 (TestHereNow_t236862272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
