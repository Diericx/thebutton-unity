﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestNonSubTimeout
struct TestNonSubTimeout_t4015492332;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestNonSubTimeout::.ctor()
extern "C"  void TestNonSubTimeout__ctor_m3847022218 (TestNonSubTimeout_t4015492332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestNonSubTimeout::Start()
extern "C"  Il2CppObject * TestNonSubTimeout_Start_m1785492650 (TestNonSubTimeout_t4015492332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
