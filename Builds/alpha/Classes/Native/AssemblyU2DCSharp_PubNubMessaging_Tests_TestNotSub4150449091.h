﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestNotSubscribed
struct  TestNotSubscribed_t4150449091  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean PubNubMessaging.Tests.TestNotSubscribed::SslOn
	bool ___SslOn_2;
	// System.Boolean PubNubMessaging.Tests.TestNotSubscribed::AsObject
	bool ___AsObject_3;
	// System.Boolean PubNubMessaging.Tests.TestNotSubscribed::IsPresence
	bool ___IsPresence_4;

public:
	inline static int32_t get_offset_of_SslOn_2() { return static_cast<int32_t>(offsetof(TestNotSubscribed_t4150449091, ___SslOn_2)); }
	inline bool get_SslOn_2() const { return ___SslOn_2; }
	inline bool* get_address_of_SslOn_2() { return &___SslOn_2; }
	inline void set_SslOn_2(bool value)
	{
		___SslOn_2 = value;
	}

	inline static int32_t get_offset_of_AsObject_3() { return static_cast<int32_t>(offsetof(TestNotSubscribed_t4150449091, ___AsObject_3)); }
	inline bool get_AsObject_3() const { return ___AsObject_3; }
	inline bool* get_address_of_AsObject_3() { return &___AsObject_3; }
	inline void set_AsObject_3(bool value)
	{
		___AsObject_3 = value;
	}

	inline static int32_t get_offset_of_IsPresence_4() { return static_cast<int32_t>(offsetof(TestNotSubscribed_t4150449091, ___IsPresence_4)); }
	inline bool get_IsPresence_4() const { return ___IsPresence_4; }
	inline bool* get_address_of_IsPresence_4() { return &___IsPresence_4; }
	inline void set_IsPresence_4(bool value)
	{
		___IsPresence_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
