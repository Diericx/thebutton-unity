﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestNotSubscribed
struct TestNotSubscribed_t4150449091;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestNotSubscribed::.ctor()
extern "C"  void TestNotSubscribed__ctor_m1502319869 (TestNotSubscribed_t4150449091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestNotSubscribed::Start()
extern "C"  Il2CppObject * TestNotSubscribed_Start_m2895036247 (TestNotSubscribed_t4150449091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
