﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestNullAsEmptyOnPublish
struct TestNullAsEmptyOnPublish_t398058888;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestNullAsEmptyOnPublish::.ctor()
extern "C"  void TestNullAsEmptyOnPublish__ctor_m2866442712 (TestNullAsEmptyOnPublish_t398058888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestNullAsEmptyOnPublish::Start()
extern "C"  Il2CppObject * TestNullAsEmptyOnPublish_Start_m667859148 (TestNullAsEmptyOnPublish_t398058888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
