﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestPHBTimeout
struct TestPHBTimeout_t475805337;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestPHBTimeout::.ctor()
extern "C"  void TestPHBTimeout__ctor_m1287968489 (TestPHBTimeout_t475805337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestPHBTimeout::Start()
extern "C"  Il2CppObject * TestPHBTimeout_Start_m3707790023 (TestPHBTimeout_t475805337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
