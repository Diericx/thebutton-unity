﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Random
struct Random_t1044426839;
// System.String
struct String_t;
// PubNubMessaging.Tests.TestPresenceCG
struct TestPresenceCG_t3679081271;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2
struct U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1
struct  U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313  : public Il2CppObject
{
public:
	// System.Random PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1::<r>__0
	Random_t1044426839 * ___U3CrU3E__0_0;
	// System.String PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1::<ch>__2
	String_t* ___U3CchU3E__2_1;
	// System.String PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1::testName
	String_t* ___testName_2;
	// System.String PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1::<uuid>__4
	String_t* ___U3CuuidU3E__4_3;
	// System.String PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1::<pubMessage>__6
	String_t* ___U3CpubMessageU3E__6_4;
	// System.String PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1::<chToSub>__7
	String_t* ___U3CchToSubU3E__7_5;
	// PubNubMessaging.Tests.TestPresenceCG PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1::$this
	TestPresenceCG_t3679081271 * ___U24this_6;
	// System.Object PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1::$disposing
	bool ___U24disposing_8;
	// System.Int32 PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1::$PC
	int32_t ___U24PC_9;
	// PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2 PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1::$locvar0
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685 * ___U24locvar0_10;

public:
	inline static int32_t get_offset_of_U3CrU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313, ___U3CrU3E__0_0)); }
	inline Random_t1044426839 * get_U3CrU3E__0_0() const { return ___U3CrU3E__0_0; }
	inline Random_t1044426839 ** get_address_of_U3CrU3E__0_0() { return &___U3CrU3E__0_0; }
	inline void set_U3CrU3E__0_0(Random_t1044426839 * value)
	{
		___U3CrU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CchU3E__2_1() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313, ___U3CchU3E__2_1)); }
	inline String_t* get_U3CchU3E__2_1() const { return ___U3CchU3E__2_1; }
	inline String_t** get_address_of_U3CchU3E__2_1() { return &___U3CchU3E__2_1; }
	inline void set_U3CchU3E__2_1(String_t* value)
	{
		___U3CchU3E__2_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchU3E__2_1, value);
	}

	inline static int32_t get_offset_of_testName_2() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313, ___testName_2)); }
	inline String_t* get_testName_2() const { return ___testName_2; }
	inline String_t** get_address_of_testName_2() { return &___testName_2; }
	inline void set_testName_2(String_t* value)
	{
		___testName_2 = value;
		Il2CppCodeGenWriteBarrier(&___testName_2, value);
	}

	inline static int32_t get_offset_of_U3CuuidU3E__4_3() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313, ___U3CuuidU3E__4_3)); }
	inline String_t* get_U3CuuidU3E__4_3() const { return ___U3CuuidU3E__4_3; }
	inline String_t** get_address_of_U3CuuidU3E__4_3() { return &___U3CuuidU3E__4_3; }
	inline void set_U3CuuidU3E__4_3(String_t* value)
	{
		___U3CuuidU3E__4_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuuidU3E__4_3, value);
	}

	inline static int32_t get_offset_of_U3CpubMessageU3E__6_4() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313, ___U3CpubMessageU3E__6_4)); }
	inline String_t* get_U3CpubMessageU3E__6_4() const { return ___U3CpubMessageU3E__6_4; }
	inline String_t** get_address_of_U3CpubMessageU3E__6_4() { return &___U3CpubMessageU3E__6_4; }
	inline void set_U3CpubMessageU3E__6_4(String_t* value)
	{
		___U3CpubMessageU3E__6_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpubMessageU3E__6_4, value);
	}

	inline static int32_t get_offset_of_U3CchToSubU3E__7_5() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313, ___U3CchToSubU3E__7_5)); }
	inline String_t* get_U3CchToSubU3E__7_5() const { return ___U3CchToSubU3E__7_5; }
	inline String_t** get_address_of_U3CchToSubU3E__7_5() { return &___U3CchToSubU3E__7_5; }
	inline void set_U3CchToSubU3E__7_5(String_t* value)
	{
		___U3CchToSubU3E__7_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchToSubU3E__7_5, value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313, ___U24this_6)); }
	inline TestPresenceCG_t3679081271 * get_U24this_6() const { return ___U24this_6; }
	inline TestPresenceCG_t3679081271 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(TestPresenceCG_t3679081271 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_10() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313, ___U24locvar0_10)); }
	inline U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685 * get_U24locvar0_10() const { return ___U24locvar0_10; }
	inline U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685 ** get_address_of_U24locvar0_10() { return &___U24locvar0_10; }
	inline void set_U24locvar0_10(U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685 * value)
	{
		___U24locvar0_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
