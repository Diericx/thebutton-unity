﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1
struct U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1::.ctor()
extern "C"  void U3CDoTestPresenceCGU3Ec__Iterator1__ctor_m1093458980 (U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1::MoveNext()
extern "C"  bool U3CDoTestPresenceCGU3Ec__Iterator1_MoveNext_m3475401244 (U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoTestPresenceCGU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3225717802 (U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoTestPresenceCGU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2220865346 (U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1::Dispose()
extern "C"  void U3CDoTestPresenceCGU3Ec__Iterator1_Dispose_m3336000363 (U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1::Reset()
extern "C"  void U3CDoTestPresenceCGU3Ec__Iterator1_Reset_m2481719193 (U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
