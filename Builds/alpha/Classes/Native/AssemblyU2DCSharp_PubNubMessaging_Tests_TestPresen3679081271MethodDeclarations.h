﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestPresenceCG
struct TestPresenceCG_t3679081271;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// PubNubMessaging.Core.PubnubClientError
struct PubnubClientError_t110317921;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubClient110317921.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void PubNubMessaging.Tests.TestPresenceCG::.ctor()
extern "C"  void TestPresenceCG__ctor_m4097782141 (TestPresenceCG_t3679081271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestPresenceCG::Start()
extern "C"  Il2CppObject * TestPresenceCG_Start_m3018117767 (TestPresenceCG_t3679081271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestPresenceCG::DoTestPresenceCG(System.String)
extern "C"  Il2CppObject * TestPresenceCG_DoTestPresenceCG_m3447728933 (TestPresenceCG_t3679081271 * __this, String_t* ___testName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPresenceCG::DisplayErrorMessage(PubNubMessaging.Core.PubnubClientError)
extern "C"  void TestPresenceCG_DisplayErrorMessage_m3450344332 (TestPresenceCG_t3679081271 * __this, PubnubClientError_t110317921 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPresenceCG::DisplayReturnMessageDummy(System.Object)
extern "C"  void TestPresenceCG_DisplayReturnMessageDummy_m2867320114 (TestPresenceCG_t3679081271 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
