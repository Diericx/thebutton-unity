﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1
struct U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2
struct  U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::testName
	String_t* ___testName_0;
	// System.Boolean PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::bAddChannel
	bool ___bAddChannel_1;
	// System.String PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::cg
	String_t* ___cg_2;
	// System.String PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::channel
	String_t* ___channel_3;
	// System.Boolean PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::bGetChannel
	bool ___bGetChannel_4;
	// System.Boolean PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::bSubConnect
	bool ___bSubConnect_5;
	// System.Boolean PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::bUnsub
	bool ___bUnsub_6;
	// PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1 PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::<>f__ref$1
	U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313 * ___U3CU3Ef__refU241_7;

public:
	inline static int32_t get_offset_of_testName_0() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685, ___testName_0)); }
	inline String_t* get_testName_0() const { return ___testName_0; }
	inline String_t** get_address_of_testName_0() { return &___testName_0; }
	inline void set_testName_0(String_t* value)
	{
		___testName_0 = value;
		Il2CppCodeGenWriteBarrier(&___testName_0, value);
	}

	inline static int32_t get_offset_of_bAddChannel_1() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685, ___bAddChannel_1)); }
	inline bool get_bAddChannel_1() const { return ___bAddChannel_1; }
	inline bool* get_address_of_bAddChannel_1() { return &___bAddChannel_1; }
	inline void set_bAddChannel_1(bool value)
	{
		___bAddChannel_1 = value;
	}

	inline static int32_t get_offset_of_cg_2() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685, ___cg_2)); }
	inline String_t* get_cg_2() const { return ___cg_2; }
	inline String_t** get_address_of_cg_2() { return &___cg_2; }
	inline void set_cg_2(String_t* value)
	{
		___cg_2 = value;
		Il2CppCodeGenWriteBarrier(&___cg_2, value);
	}

	inline static int32_t get_offset_of_channel_3() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685, ___channel_3)); }
	inline String_t* get_channel_3() const { return ___channel_3; }
	inline String_t** get_address_of_channel_3() { return &___channel_3; }
	inline void set_channel_3(String_t* value)
	{
		___channel_3 = value;
		Il2CppCodeGenWriteBarrier(&___channel_3, value);
	}

	inline static int32_t get_offset_of_bGetChannel_4() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685, ___bGetChannel_4)); }
	inline bool get_bGetChannel_4() const { return ___bGetChannel_4; }
	inline bool* get_address_of_bGetChannel_4() { return &___bGetChannel_4; }
	inline void set_bGetChannel_4(bool value)
	{
		___bGetChannel_4 = value;
	}

	inline static int32_t get_offset_of_bSubConnect_5() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685, ___bSubConnect_5)); }
	inline bool get_bSubConnect_5() const { return ___bSubConnect_5; }
	inline bool* get_address_of_bSubConnect_5() { return &___bSubConnect_5; }
	inline void set_bSubConnect_5(bool value)
	{
		___bSubConnect_5 = value;
	}

	inline static int32_t get_offset_of_bUnsub_6() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685, ___bUnsub_6)); }
	inline bool get_bUnsub_6() const { return ___bUnsub_6; }
	inline bool* get_address_of_bUnsub_6() { return &___bUnsub_6; }
	inline void set_bUnsub_6(bool value)
	{
		___bUnsub_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_7() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685, ___U3CU3Ef__refU241_7)); }
	inline U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313 * get_U3CU3Ef__refU241_7() const { return ___U3CU3Ef__refU241_7; }
	inline U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313 ** get_address_of_U3CU3Ef__refU241_7() { return &___U3CU3Ef__refU241_7; }
	inline void set_U3CU3Ef__refU241_7(U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313 * value)
	{
		___U3CU3Ef__refU241_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
