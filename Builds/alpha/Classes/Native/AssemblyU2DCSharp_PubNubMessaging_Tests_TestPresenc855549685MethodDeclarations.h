﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2
struct U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::.ctor()
extern "C"  void U3CDoTestPresenceCGU3Ec__AnonStorey2__ctor_m3474929374 (U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::<>m__0(System.String)
extern "C"  void U3CDoTestPresenceCGU3Ec__AnonStorey2_U3CU3Em__0_m481877961 (U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::<>m__1(System.String)
extern "C"  void U3CDoTestPresenceCGU3Ec__AnonStorey2_U3CU3Em__1_m4069927300 (U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::<>m__2(System.String)
extern "C"  void U3CDoTestPresenceCGU3Ec__AnonStorey2_U3CU3Em__2_m2442030931 (U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685 * __this, String_t* ___retConnect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::<>m__3(System.String)
extern "C"  void U3CDoTestPresenceCGU3Ec__AnonStorey2_U3CU3Em__3_m2372838670 (U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPresenceCG/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::<>m__4(System.String)
extern "C"  void U3CDoTestPresenceCGU3Ec__AnonStorey2_U3CU3Em__4_m231002077 (U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685 * __this, String_t* ___result20, const MethodInfo* method) IL2CPP_METHOD_ATTR;
