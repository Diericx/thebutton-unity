﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestPresenceHeartbeat
struct TestPresenceHeartbeat_t57511345;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestPresenceHeartbeat::.ctor()
extern "C"  void TestPresenceHeartbeat__ctor_m1291712261 (TestPresenceHeartbeat_t57511345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestPresenceHeartbeat::Start()
extern "C"  Il2CppObject * TestPresenceHeartbeat_Start_m847418299 (TestPresenceHeartbeat_t57511345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
