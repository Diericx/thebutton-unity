﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestProcessTimeout
struct TestProcessTimeout_t1733042336;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestProcessTimeout::.ctor()
extern "C"  void TestProcessTimeout__ctor_m2870648182 (TestProcessTimeout_t1733042336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestProcessTimeout::Start()
extern "C"  Il2CppObject * TestProcessTimeout_Start_m3457153866 (TestProcessTimeout_t1733042336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
