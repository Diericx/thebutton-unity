﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1
struct U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2
struct  U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::testName
	String_t* ___testName_0;
	// System.String PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::pubMessage
	String_t* ___pubMessage_1;
	// System.Boolean PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::bSubConnect
	bool ___bSubConnect_2;
	// System.String PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::ch
	String_t* ___ch_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::dict
	Dictionary_2_t3943999495 * ___dict_4;
	// System.Boolean PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::bUnsub
	bool ___bUnsub_5;
	// PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1 PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::<>f__ref$1
	U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496 * ___U3CU3Ef__refU241_6;

public:
	inline static int32_t get_offset_of_testName_0() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156, ___testName_0)); }
	inline String_t* get_testName_0() const { return ___testName_0; }
	inline String_t** get_address_of_testName_0() { return &___testName_0; }
	inline void set_testName_0(String_t* value)
	{
		___testName_0 = value;
		Il2CppCodeGenWriteBarrier(&___testName_0, value);
	}

	inline static int32_t get_offset_of_pubMessage_1() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156, ___pubMessage_1)); }
	inline String_t* get_pubMessage_1() const { return ___pubMessage_1; }
	inline String_t** get_address_of_pubMessage_1() { return &___pubMessage_1; }
	inline void set_pubMessage_1(String_t* value)
	{
		___pubMessage_1 = value;
		Il2CppCodeGenWriteBarrier(&___pubMessage_1, value);
	}

	inline static int32_t get_offset_of_bSubConnect_2() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156, ___bSubConnect_2)); }
	inline bool get_bSubConnect_2() const { return ___bSubConnect_2; }
	inline bool* get_address_of_bSubConnect_2() { return &___bSubConnect_2; }
	inline void set_bSubConnect_2(bool value)
	{
		___bSubConnect_2 = value;
	}

	inline static int32_t get_offset_of_ch_3() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156, ___ch_3)); }
	inline String_t* get_ch_3() const { return ___ch_3; }
	inline String_t** get_address_of_ch_3() { return &___ch_3; }
	inline void set_ch_3(String_t* value)
	{
		___ch_3 = value;
		Il2CppCodeGenWriteBarrier(&___ch_3, value);
	}

	inline static int32_t get_offset_of_dict_4() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156, ___dict_4)); }
	inline Dictionary_2_t3943999495 * get_dict_4() const { return ___dict_4; }
	inline Dictionary_2_t3943999495 ** get_address_of_dict_4() { return &___dict_4; }
	inline void set_dict_4(Dictionary_2_t3943999495 * value)
	{
		___dict_4 = value;
		Il2CppCodeGenWriteBarrier(&___dict_4, value);
	}

	inline static int32_t get_offset_of_bUnsub_5() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156, ___bUnsub_5)); }
	inline bool get_bUnsub_5() const { return ___bUnsub_5; }
	inline bool* get_address_of_bUnsub_5() { return &___bUnsub_5; }
	inline void set_bUnsub_5(bool value)
	{
		___bUnsub_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_6() { return static_cast<int32_t>(offsetof(U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156, ___U3CU3Ef__refU241_6)); }
	inline U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496 * get_U3CU3Ef__refU241_6() const { return ___U3CU3Ef__refU241_6; }
	inline U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496 ** get_address_of_U3CU3Ef__refU241_6() { return &___U3CU3Ef__refU241_6; }
	inline void set_U3CU3Ef__refU241_6(U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496 * value)
	{
		___U3CU3Ef__refU241_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
