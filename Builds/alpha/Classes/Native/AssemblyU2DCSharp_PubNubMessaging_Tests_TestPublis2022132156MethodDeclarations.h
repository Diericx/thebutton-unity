﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2
struct U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::.ctor()
extern "C"  void U3CDoTestPresenceCGU3Ec__AnonStorey2__ctor_m3371861987 (U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::<>m__0(System.String)
extern "C"  void U3CDoTestPresenceCGU3Ec__AnonStorey2_U3CU3Em__0_m1918117860 (U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::<>m__1(System.String)
extern "C"  void U3CDoTestPresenceCGU3Ec__AnonStorey2_U3CU3Em__1_m3298853891 (U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156 * __this, String_t* ___retConnect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::<>m__2(System.String)
extern "C"  void U3CDoTestPresenceCGU3Ec__AnonStorey2_U3CU3Em__2_m1347878946 (U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
