﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestPublishWithMetaNeg/<DoTestPresenceCG>c__Iterator1
struct U3CDoTestPresenceCGU3Ec__Iterator1_t2226981140;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestPublishWithMetaNeg/<DoTestPresenceCG>c__Iterator1::.ctor()
extern "C"  void U3CDoTestPresenceCGU3Ec__Iterator1__ctor_m3764231403 (U3CDoTestPresenceCGU3Ec__Iterator1_t2226981140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.TestPublishWithMetaNeg/<DoTestPresenceCG>c__Iterator1::MoveNext()
extern "C"  bool U3CDoTestPresenceCGU3Ec__Iterator1_MoveNext_m395492781 (U3CDoTestPresenceCGU3Ec__Iterator1_t2226981140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestPublishWithMetaNeg/<DoTestPresenceCG>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoTestPresenceCGU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1668019141 (U3CDoTestPresenceCGU3Ec__Iterator1_t2226981140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestPublishWithMetaNeg/<DoTestPresenceCG>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoTestPresenceCGU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m104495197 (U3CDoTestPresenceCGU3Ec__Iterator1_t2226981140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPublishWithMetaNeg/<DoTestPresenceCG>c__Iterator1::Dispose()
extern "C"  void U3CDoTestPresenceCGU3Ec__Iterator1_Dispose_m352142854 (U3CDoTestPresenceCGU3Ec__Iterator1_t2226981140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPublishWithMetaNeg/<DoTestPresenceCG>c__Iterator1::Reset()
extern "C"  void U3CDoTestPresenceCGU3Ec__Iterator1_Reset_m2718392152 (U3CDoTestPresenceCGU3Ec__Iterator1_t2226981140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
