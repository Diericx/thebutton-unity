﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1
struct U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1::.ctor()
extern "C"  void U3CDoTestPresenceCGU3Ec__Iterator1__ctor_m2782212563 (U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1::MoveNext()
extern "C"  bool U3CDoTestPresenceCGU3Ec__Iterator1_MoveNext_m4045522705 (U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoTestPresenceCGU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m782172797 (U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoTestPresenceCGU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m662446277 (U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1::Dispose()
extern "C"  void U3CDoTestPresenceCGU3Ec__Iterator1_Dispose_m1253549406 (U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPublishWithMeta/<DoTestPresenceCG>c__Iterator1::Reset()
extern "C"  void U3CDoTestPresenceCGU3Ec__Iterator1_Reset_m4223474388 (U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
