﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestPublishKeyPresent
struct TestPublishKeyPresent_t2456351671;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestPublishKeyPresent::.ctor()
extern "C"  void TestPublishKeyPresent__ctor_m1250299925 (TestPublishKeyPresent_t2456351671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestPublishKeyPresent::Start()
extern "C"  Il2CppObject * TestPublishKeyPresent_Start_m2469971819 (TestPublishKeyPresent_t2456351671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
