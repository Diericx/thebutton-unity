﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestPublishSimple
struct  TestPublishSimple_t2553593815  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean PubNubMessaging.Tests.TestPublishSimple::SslOn
	bool ___SslOn_2;
	// System.Boolean PubNubMessaging.Tests.TestPublishSimple::AsObject
	bool ___AsObject_3;
	// System.Boolean PubNubMessaging.Tests.TestPublishSimple::WithCipher
	bool ___WithCipher_4;

public:
	inline static int32_t get_offset_of_SslOn_2() { return static_cast<int32_t>(offsetof(TestPublishSimple_t2553593815, ___SslOn_2)); }
	inline bool get_SslOn_2() const { return ___SslOn_2; }
	inline bool* get_address_of_SslOn_2() { return &___SslOn_2; }
	inline void set_SslOn_2(bool value)
	{
		___SslOn_2 = value;
	}

	inline static int32_t get_offset_of_AsObject_3() { return static_cast<int32_t>(offsetof(TestPublishSimple_t2553593815, ___AsObject_3)); }
	inline bool get_AsObject_3() const { return ___AsObject_3; }
	inline bool* get_address_of_AsObject_3() { return &___AsObject_3; }
	inline void set_AsObject_3(bool value)
	{
		___AsObject_3 = value;
	}

	inline static int32_t get_offset_of_WithCipher_4() { return static_cast<int32_t>(offsetof(TestPublishSimple_t2553593815, ___WithCipher_4)); }
	inline bool get_WithCipher_4() const { return ___WithCipher_4; }
	inline bool* get_address_of_WithCipher_4() { return &___WithCipher_4; }
	inline void set_WithCipher_4(bool value)
	{
		___WithCipher_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
