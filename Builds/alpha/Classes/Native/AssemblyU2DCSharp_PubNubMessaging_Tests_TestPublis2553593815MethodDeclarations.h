﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestPublishSimple
struct TestPublishSimple_t2553593815;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestPublishSimple::.ctor()
extern "C"  void TestPublishSimple__ctor_m1842469203 (TestPublishSimple_t2553593815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestPublishSimple::Start()
extern "C"  Il2CppObject * TestPublishSimple_Start_m3653738153 (TestPublishSimple_t2553593815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
