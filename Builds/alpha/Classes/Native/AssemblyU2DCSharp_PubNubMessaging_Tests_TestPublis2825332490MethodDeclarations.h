﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestPublishKeyOverride
struct TestPublishKeyOverride_t2825332490;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestPublishKeyOverride::.ctor()
extern "C"  void TestPublishKeyOverride__ctor_m3275791820 (TestPublishKeyOverride_t2825332490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestPublishKeyOverride::Start()
extern "C"  Il2CppObject * TestPublishKeyOverride_Start_m1329901308 (TestPublishKeyOverride_t2825332490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
