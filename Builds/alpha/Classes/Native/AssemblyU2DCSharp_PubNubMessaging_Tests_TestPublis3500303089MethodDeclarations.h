﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestPublishNoStore
struct TestPublishNoStore_t3500303089;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// PubNubMessaging.Core.PubnubClientError
struct PubnubClientError_t110317921;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubClient110317921.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void PubNubMessaging.Tests.TestPublishNoStore::.ctor()
extern "C"  void TestPublishNoStore__ctor_m2762207651 (TestPublishNoStore_t3500303089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestPublishNoStore::Start()
extern "C"  Il2CppObject * TestPublishNoStore_Start_m2091985353 (TestPublishNoStore_t3500303089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestPublishNoStore::DoPublishNoStore(System.String)
extern "C"  Il2CppObject * TestPublishNoStore_DoPublishNoStore_m2332596067 (TestPublishNoStore_t3500303089 * __this, String_t* ___testName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPublishNoStore::DisplayErrorMessage(PubNubMessaging.Core.PubnubClientError)
extern "C"  void TestPublishNoStore_DisplayErrorMessage_m2701345794 (TestPublishNoStore_t3500303089 * __this, PubnubClientError_t110317921 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPublishNoStore::DisplayReturnMessageDummy(System.Object)
extern "C"  void TestPublishNoStore_DisplayReturnMessageDummy_m4091252248 (TestPublishNoStore_t3500303089 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
