﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestPublishWithMetaNeg/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2
struct U3CDoTestPresenceCGU3Ec__AnonStorey2_t3776434096;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PubNubMessaging.Tests.TestPublishWithMetaNeg/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::.ctor()
extern "C"  void U3CDoTestPresenceCGU3Ec__AnonStorey2__ctor_m2970860091 (U3CDoTestPresenceCGU3Ec__AnonStorey2_t3776434096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPublishWithMetaNeg/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::<>m__0(System.String)
extern "C"  void U3CDoTestPresenceCGU3Ec__AnonStorey2_U3CU3Em__0_m1649126256 (U3CDoTestPresenceCGU3Ec__AnonStorey2_t3776434096 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPublishWithMetaNeg/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::<>m__1(System.String)
extern "C"  void U3CDoTestPresenceCGU3Ec__AnonStorey2_U3CU3Em__1_m942208299 (U3CDoTestPresenceCGU3Ec__AnonStorey2_t3776434096 * __this, String_t* ___retConnect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPublishWithMetaNeg/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::<>m__2(System.String)
extern "C"  void U3CDoTestPresenceCGU3Ec__AnonStorey2_U3CU3Em__2_m3609279226 (U3CDoTestPresenceCGU3Ec__AnonStorey2_t3776434096 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPublishWithMetaNeg/<DoTestPresenceCG>c__Iterator1/<DoTestPresenceCG>c__AnonStorey2::<>m__3(System.String)
extern "C"  void U3CDoTestPresenceCGU3Ec__AnonStorey2_U3CU3Em__3_m3540086965 (U3CDoTestPresenceCGU3Ec__AnonStorey2_t3776434096 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
