﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestPublishComplex
struct TestPublishComplex_t3915767209;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestPublishComplex::.ctor()
extern "C"  void TestPublishComplex__ctor_m160734477 (TestPublishComplex_t3915767209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestPublishComplex::Start()
extern "C"  Il2CppObject * TestPublishComplex_Start_m899141783 (TestPublishComplex_t3915767209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
