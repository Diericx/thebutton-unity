﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PubNubMessaging.Tests.CommonIntergrationTests
struct CommonIntergrationTests_t1691354350;
// System.String
struct String_t;
// PubNubMessaging.Core.Pubnub
struct Pubnub_t2451529532;
// PubNubMessaging.Tests.TestPublishKeyOverride
struct TestPublishKeyOverride_t2825332490;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestPublishKeyOverride/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t4164458955  : public Il2CppObject
{
public:
	// PubNubMessaging.Tests.CommonIntergrationTests PubNubMessaging.Tests.TestPublishKeyOverride/<Start>c__Iterator0::<common>__0
	CommonIntergrationTests_t1691354350 * ___U3CcommonU3E__0_0;
	// System.String PubNubMessaging.Tests.TestPublishKeyOverride/<Start>c__Iterator0::<TestName>__1
	String_t* ___U3CTestNameU3E__1_1;
	// PubNubMessaging.Core.Pubnub PubNubMessaging.Tests.TestPublishKeyOverride/<Start>c__Iterator0::<pubnub>__2
	Pubnub_t2451529532 * ___U3CpubnubU3E__2_2;
	// PubNubMessaging.Tests.TestPublishKeyOverride PubNubMessaging.Tests.TestPublishKeyOverride/<Start>c__Iterator0::$this
	TestPublishKeyOverride_t2825332490 * ___U24this_3;
	// System.Object PubNubMessaging.Tests.TestPublishKeyOverride/<Start>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean PubNubMessaging.Tests.TestPublishKeyOverride/<Start>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 PubNubMessaging.Tests.TestPublishKeyOverride/<Start>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CcommonU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t4164458955, ___U3CcommonU3E__0_0)); }
	inline CommonIntergrationTests_t1691354350 * get_U3CcommonU3E__0_0() const { return ___U3CcommonU3E__0_0; }
	inline CommonIntergrationTests_t1691354350 ** get_address_of_U3CcommonU3E__0_0() { return &___U3CcommonU3E__0_0; }
	inline void set_U3CcommonU3E__0_0(CommonIntergrationTests_t1691354350 * value)
	{
		___U3CcommonU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcommonU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CTestNameU3E__1_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t4164458955, ___U3CTestNameU3E__1_1)); }
	inline String_t* get_U3CTestNameU3E__1_1() const { return ___U3CTestNameU3E__1_1; }
	inline String_t** get_address_of_U3CTestNameU3E__1_1() { return &___U3CTestNameU3E__1_1; }
	inline void set_U3CTestNameU3E__1_1(String_t* value)
	{
		___U3CTestNameU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTestNameU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CpubnubU3E__2_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t4164458955, ___U3CpubnubU3E__2_2)); }
	inline Pubnub_t2451529532 * get_U3CpubnubU3E__2_2() const { return ___U3CpubnubU3E__2_2; }
	inline Pubnub_t2451529532 ** get_address_of_U3CpubnubU3E__2_2() { return &___U3CpubnubU3E__2_2; }
	inline void set_U3CpubnubU3E__2_2(Pubnub_t2451529532 * value)
	{
		___U3CpubnubU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpubnubU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t4164458955, ___U24this_3)); }
	inline TestPublishKeyOverride_t2825332490 * get_U24this_3() const { return ___U24this_3; }
	inline TestPublishKeyOverride_t2825332490 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(TestPublishKeyOverride_t2825332490 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t4164458955, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t4164458955, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t4164458955, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
