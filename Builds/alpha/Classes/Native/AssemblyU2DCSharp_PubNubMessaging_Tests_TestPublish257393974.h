﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PubNubMessaging.Core.Pubnub
struct Pubnub_t2451529532;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestPublishWithMetaNeg
struct  TestPublishWithMetaNeg_t257393974  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean PubNubMessaging.Tests.TestPublishWithMetaNeg::SslOn
	bool ___SslOn_2;
	// System.Boolean PubNubMessaging.Tests.TestPublishWithMetaNeg::CipherOn
	bool ___CipherOn_3;
	// System.Boolean PubNubMessaging.Tests.TestPublishWithMetaNeg::AsObject
	bool ___AsObject_4;
	// System.Boolean PubNubMessaging.Tests.TestPublishWithMetaNeg::BothString
	bool ___BothString_5;
	// PubNubMessaging.Core.Pubnub PubNubMessaging.Tests.TestPublishWithMetaNeg::pubnub
	Pubnub_t2451529532 * ___pubnub_6;

public:
	inline static int32_t get_offset_of_SslOn_2() { return static_cast<int32_t>(offsetof(TestPublishWithMetaNeg_t257393974, ___SslOn_2)); }
	inline bool get_SslOn_2() const { return ___SslOn_2; }
	inline bool* get_address_of_SslOn_2() { return &___SslOn_2; }
	inline void set_SslOn_2(bool value)
	{
		___SslOn_2 = value;
	}

	inline static int32_t get_offset_of_CipherOn_3() { return static_cast<int32_t>(offsetof(TestPublishWithMetaNeg_t257393974, ___CipherOn_3)); }
	inline bool get_CipherOn_3() const { return ___CipherOn_3; }
	inline bool* get_address_of_CipherOn_3() { return &___CipherOn_3; }
	inline void set_CipherOn_3(bool value)
	{
		___CipherOn_3 = value;
	}

	inline static int32_t get_offset_of_AsObject_4() { return static_cast<int32_t>(offsetof(TestPublishWithMetaNeg_t257393974, ___AsObject_4)); }
	inline bool get_AsObject_4() const { return ___AsObject_4; }
	inline bool* get_address_of_AsObject_4() { return &___AsObject_4; }
	inline void set_AsObject_4(bool value)
	{
		___AsObject_4 = value;
	}

	inline static int32_t get_offset_of_BothString_5() { return static_cast<int32_t>(offsetof(TestPublishWithMetaNeg_t257393974, ___BothString_5)); }
	inline bool get_BothString_5() const { return ___BothString_5; }
	inline bool* get_address_of_BothString_5() { return &___BothString_5; }
	inline void set_BothString_5(bool value)
	{
		___BothString_5 = value;
	}

	inline static int32_t get_offset_of_pubnub_6() { return static_cast<int32_t>(offsetof(TestPublishWithMetaNeg_t257393974, ___pubnub_6)); }
	inline Pubnub_t2451529532 * get_pubnub_6() const { return ___pubnub_6; }
	inline Pubnub_t2451529532 ** get_address_of_pubnub_6() { return &___pubnub_6; }
	inline void set_pubnub_6(Pubnub_t2451529532 * value)
	{
		___pubnub_6 = value;
		Il2CppCodeGenWriteBarrier(&___pubnub_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
