﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestPublishWithMetaNeg
struct TestPublishWithMetaNeg_t257393974;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// PubNubMessaging.Core.PubnubClientError
struct PubnubClientError_t110317921;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubClient110317921.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void PubNubMessaging.Tests.TestPublishWithMetaNeg::.ctor()
extern "C"  void TestPublishWithMetaNeg__ctor_m3739865750 (TestPublishWithMetaNeg_t257393974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestPublishWithMetaNeg::Start()
extern "C"  Il2CppObject * TestPublishWithMetaNeg_Start_m312735362 (TestPublishWithMetaNeg_t257393974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestPublishWithMetaNeg::DoTestPresenceCG(System.String)
extern "C"  Il2CppObject * TestPublishWithMetaNeg_DoTestPresenceCG_m583286932 (TestPublishWithMetaNeg_t257393974 * __this, String_t* ___testName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPublishWithMetaNeg::DisplayErrorMessage(PubNubMessaging.Core.PubnubClientError)
extern "C"  void TestPublishWithMetaNeg_DisplayErrorMessage_m1098323919 (TestPublishWithMetaNeg_t257393974 * __this, PubnubClientError_t110317921 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPublishWithMetaNeg::DisplayReturnMessageDummy(System.Object)
extern "C"  void TestPublishWithMetaNeg_DisplayReturnMessageDummy_m2769314291 (TestPublishWithMetaNeg_t257393974 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
