﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Tests.TestPublishNoStore/<DoPublishNoStore>c__Iterator1
struct U3CDoPublishNoStoreU3Ec__Iterator1_t3340936991;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestPublishNoStore/<DoPublishNoStore>c__Iterator1/<DoPublishNoStore>c__AnonStorey2
struct  U3CDoPublishNoStoreU3Ec__AnonStorey2_t56554715  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.TestPublishNoStore/<DoPublishNoStore>c__Iterator1/<DoPublishNoStore>c__AnonStorey2::ch
	String_t* ___ch_0;
	// System.String PubNubMessaging.Tests.TestPublishNoStore/<DoPublishNoStore>c__Iterator1/<DoPublishNoStore>c__AnonStorey2::pubMessage
	String_t* ___pubMessage_1;
	// PubNubMessaging.Tests.TestPublishNoStore/<DoPublishNoStore>c__Iterator1 PubNubMessaging.Tests.TestPublishNoStore/<DoPublishNoStore>c__Iterator1/<DoPublishNoStore>c__AnonStorey2::<>f__ref$1
	U3CDoPublishNoStoreU3Ec__Iterator1_t3340936991 * ___U3CU3Ef__refU241_2;

public:
	inline static int32_t get_offset_of_ch_0() { return static_cast<int32_t>(offsetof(U3CDoPublishNoStoreU3Ec__AnonStorey2_t56554715, ___ch_0)); }
	inline String_t* get_ch_0() const { return ___ch_0; }
	inline String_t** get_address_of_ch_0() { return &___ch_0; }
	inline void set_ch_0(String_t* value)
	{
		___ch_0 = value;
		Il2CppCodeGenWriteBarrier(&___ch_0, value);
	}

	inline static int32_t get_offset_of_pubMessage_1() { return static_cast<int32_t>(offsetof(U3CDoPublishNoStoreU3Ec__AnonStorey2_t56554715, ___pubMessage_1)); }
	inline String_t* get_pubMessage_1() const { return ___pubMessage_1; }
	inline String_t** get_address_of_pubMessage_1() { return &___pubMessage_1; }
	inline void set_pubMessage_1(String_t* value)
	{
		___pubMessage_1 = value;
		Il2CppCodeGenWriteBarrier(&___pubMessage_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_2() { return static_cast<int32_t>(offsetof(U3CDoPublishNoStoreU3Ec__AnonStorey2_t56554715, ___U3CU3Ef__refU241_2)); }
	inline U3CDoPublishNoStoreU3Ec__Iterator1_t3340936991 * get_U3CU3Ef__refU241_2() const { return ___U3CU3Ef__refU241_2; }
	inline U3CDoPublishNoStoreU3Ec__Iterator1_t3340936991 ** get_address_of_U3CU3Ef__refU241_2() { return &___U3CU3Ef__refU241_2; }
	inline void set_U3CU3Ef__refU241_2(U3CDoPublishNoStoreU3Ec__Iterator1_t3340936991 * value)
	{
		___U3CU3Ef__refU241_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
