﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestPublishNoStore/<DoPublishNoStore>c__Iterator1/<DoPublishNoStore>c__AnonStorey2
struct U3CDoPublishNoStoreU3Ec__AnonStorey2_t56554715;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PubNubMessaging.Tests.TestPublishNoStore/<DoPublishNoStore>c__Iterator1/<DoPublishNoStore>c__AnonStorey2::.ctor()
extern "C"  void U3CDoPublishNoStoreU3Ec__AnonStorey2__ctor_m2938904240 (U3CDoPublishNoStoreU3Ec__AnonStorey2_t56554715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPublishNoStore/<DoPublishNoStore>c__Iterator1/<DoPublishNoStore>c__AnonStorey2::<>m__0(System.String)
extern "C"  void U3CDoPublishNoStoreU3Ec__AnonStorey2_U3CU3Em__0_m812960515 (U3CDoPublishNoStoreU3Ec__AnonStorey2_t56554715 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestPublishNoStore/<DoPublishNoStore>c__Iterator1/<DoPublishNoStore>c__AnonStorey2::<>m__1(System.String)
extern "C"  void U3CDoPublishNoStoreU3Ec__AnonStorey2_U3CU3Em__1_m106042558 (U3CDoPublishNoStoreU3Ec__AnonStorey2_t56554715 * __this, String_t* ___retM20, const MethodInfo* method) IL2CPP_METHOD_ATTR;
