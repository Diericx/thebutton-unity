﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Random
struct Random_t1044426839;
// System.String
struct String_t;
// PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG
struct TestRemoveCGAndRemoveAllCG_t911744142;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1/<DoRemoveCGAndRemoveAllCG>c__AnonStorey2
struct U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1
struct  U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343  : public Il2CppObject
{
public:
	// System.Random PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1::<r>__0
	Random_t1044426839 * ___U3CrU3E__0_0;
	// System.String PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1::<ch>__2
	String_t* ___U3CchU3E__2_1;
	// System.String PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1::testName
	String_t* ___testName_2;
	// System.Boolean PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1::<bGetAllCG>__6
	bool ___U3CbGetAllCGU3E__6_3;
	// System.String PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1::<uuid>__7
	String_t* ___U3CuuidU3E__7_4;
	// System.String PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1::<strLog>__8
	String_t* ___U3CstrLogU3E__8_5;
	// System.String PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1::<strLog2>__C
	String_t* ___U3CstrLog2U3E__C_6;
	// PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1::$this
	TestRemoveCGAndRemoveAllCG_t911744142 * ___U24this_7;
	// System.Object PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1::$current
	Il2CppObject * ___U24current_8;
	// System.Boolean PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1::$disposing
	bool ___U24disposing_9;
	// System.Int32 PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1::$PC
	int32_t ___U24PC_10;
	// PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1/<DoRemoveCGAndRemoveAllCG>c__AnonStorey2 PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1::$locvar0
	U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458 * ___U24locvar0_11;

public:
	inline static int32_t get_offset_of_U3CrU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343, ___U3CrU3E__0_0)); }
	inline Random_t1044426839 * get_U3CrU3E__0_0() const { return ___U3CrU3E__0_0; }
	inline Random_t1044426839 ** get_address_of_U3CrU3E__0_0() { return &___U3CrU3E__0_0; }
	inline void set_U3CrU3E__0_0(Random_t1044426839 * value)
	{
		___U3CrU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CchU3E__2_1() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343, ___U3CchU3E__2_1)); }
	inline String_t* get_U3CchU3E__2_1() const { return ___U3CchU3E__2_1; }
	inline String_t** get_address_of_U3CchU3E__2_1() { return &___U3CchU3E__2_1; }
	inline void set_U3CchU3E__2_1(String_t* value)
	{
		___U3CchU3E__2_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchU3E__2_1, value);
	}

	inline static int32_t get_offset_of_testName_2() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343, ___testName_2)); }
	inline String_t* get_testName_2() const { return ___testName_2; }
	inline String_t** get_address_of_testName_2() { return &___testName_2; }
	inline void set_testName_2(String_t* value)
	{
		___testName_2 = value;
		Il2CppCodeGenWriteBarrier(&___testName_2, value);
	}

	inline static int32_t get_offset_of_U3CbGetAllCGU3E__6_3() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343, ___U3CbGetAllCGU3E__6_3)); }
	inline bool get_U3CbGetAllCGU3E__6_3() const { return ___U3CbGetAllCGU3E__6_3; }
	inline bool* get_address_of_U3CbGetAllCGU3E__6_3() { return &___U3CbGetAllCGU3E__6_3; }
	inline void set_U3CbGetAllCGU3E__6_3(bool value)
	{
		___U3CbGetAllCGU3E__6_3 = value;
	}

	inline static int32_t get_offset_of_U3CuuidU3E__7_4() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343, ___U3CuuidU3E__7_4)); }
	inline String_t* get_U3CuuidU3E__7_4() const { return ___U3CuuidU3E__7_4; }
	inline String_t** get_address_of_U3CuuidU3E__7_4() { return &___U3CuuidU3E__7_4; }
	inline void set_U3CuuidU3E__7_4(String_t* value)
	{
		___U3CuuidU3E__7_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuuidU3E__7_4, value);
	}

	inline static int32_t get_offset_of_U3CstrLogU3E__8_5() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343, ___U3CstrLogU3E__8_5)); }
	inline String_t* get_U3CstrLogU3E__8_5() const { return ___U3CstrLogU3E__8_5; }
	inline String_t** get_address_of_U3CstrLogU3E__8_5() { return &___U3CstrLogU3E__8_5; }
	inline void set_U3CstrLogU3E__8_5(String_t* value)
	{
		___U3CstrLogU3E__8_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstrLogU3E__8_5, value);
	}

	inline static int32_t get_offset_of_U3CstrLog2U3E__C_6() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343, ___U3CstrLog2U3E__C_6)); }
	inline String_t* get_U3CstrLog2U3E__C_6() const { return ___U3CstrLog2U3E__C_6; }
	inline String_t** get_address_of_U3CstrLog2U3E__C_6() { return &___U3CstrLog2U3E__C_6; }
	inline void set_U3CstrLog2U3E__C_6(String_t* value)
	{
		___U3CstrLog2U3E__C_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstrLog2U3E__C_6, value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343, ___U24this_7)); }
	inline TestRemoveCGAndRemoveAllCG_t911744142 * get_U24this_7() const { return ___U24this_7; }
	inline TestRemoveCGAndRemoveAllCG_t911744142 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(TestRemoveCGAndRemoveAllCG_t911744142 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_7, value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343, ___U24current_8)); }
	inline Il2CppObject * get_U24current_8() const { return ___U24current_8; }
	inline Il2CppObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(Il2CppObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_8, value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_11() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343, ___U24locvar0_11)); }
	inline U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458 * get_U24locvar0_11() const { return ___U24locvar0_11; }
	inline U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458 ** get_address_of_U24locvar0_11() { return &___U24locvar0_11; }
	inline void set_U24locvar0_11(U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458 * value)
	{
		___U24locvar0_11 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
