﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1
struct U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1::.ctor()
extern "C"  void U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1__ctor_m2402070936 (U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1::MoveNext()
extern "C"  bool U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_MoveNext_m3978051268 (U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m993589978 (U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3601834354 (U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1::Dispose()
extern "C"  void U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_Dispose_m3522339705 (U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1::Reset()
extern "C"  void U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_Reset_m3218824427 (U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
