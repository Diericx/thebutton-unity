﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1
struct U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1/<DoRemoveCGAndRemoveAllCG>c__AnonStorey2
struct  U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1/<DoRemoveCGAndRemoveAllCG>c__AnonStorey2::testName
	String_t* ___testName_0;
	// System.Boolean PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1/<DoRemoveCGAndRemoveAllCG>c__AnonStorey2::bAddChannel
	bool ___bAddChannel_1;
	// System.String PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1/<DoRemoveCGAndRemoveAllCG>c__AnonStorey2::cg
	String_t* ___cg_2;
	// System.String PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1/<DoRemoveCGAndRemoveAllCG>c__AnonStorey2::channel
	String_t* ___channel_3;
	// System.Boolean PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1/<DoRemoveCGAndRemoveAllCG>c__AnonStorey2::bGetChannel
	bool ___bGetChannel_4;
	// System.Boolean PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1/<DoRemoveCGAndRemoveAllCG>c__AnonStorey2::bRemoveCh
	bool ___bRemoveCh_5;
	// System.Boolean PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1/<DoRemoveCGAndRemoveAllCG>c__AnonStorey2::bGetChannel2
	bool ___bGetChannel2_6;
	// System.Boolean PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1/<DoRemoveCGAndRemoveAllCG>c__AnonStorey2::bRemoveAll
	bool ___bRemoveAll_7;
	// PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1 PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1/<DoRemoveCGAndRemoveAllCG>c__AnonStorey2::<>f__ref$1
	U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343 * ___U3CU3Ef__refU241_8;

public:
	inline static int32_t get_offset_of_testName_0() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458, ___testName_0)); }
	inline String_t* get_testName_0() const { return ___testName_0; }
	inline String_t** get_address_of_testName_0() { return &___testName_0; }
	inline void set_testName_0(String_t* value)
	{
		___testName_0 = value;
		Il2CppCodeGenWriteBarrier(&___testName_0, value);
	}

	inline static int32_t get_offset_of_bAddChannel_1() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458, ___bAddChannel_1)); }
	inline bool get_bAddChannel_1() const { return ___bAddChannel_1; }
	inline bool* get_address_of_bAddChannel_1() { return &___bAddChannel_1; }
	inline void set_bAddChannel_1(bool value)
	{
		___bAddChannel_1 = value;
	}

	inline static int32_t get_offset_of_cg_2() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458, ___cg_2)); }
	inline String_t* get_cg_2() const { return ___cg_2; }
	inline String_t** get_address_of_cg_2() { return &___cg_2; }
	inline void set_cg_2(String_t* value)
	{
		___cg_2 = value;
		Il2CppCodeGenWriteBarrier(&___cg_2, value);
	}

	inline static int32_t get_offset_of_channel_3() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458, ___channel_3)); }
	inline String_t* get_channel_3() const { return ___channel_3; }
	inline String_t** get_address_of_channel_3() { return &___channel_3; }
	inline void set_channel_3(String_t* value)
	{
		___channel_3 = value;
		Il2CppCodeGenWriteBarrier(&___channel_3, value);
	}

	inline static int32_t get_offset_of_bGetChannel_4() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458, ___bGetChannel_4)); }
	inline bool get_bGetChannel_4() const { return ___bGetChannel_4; }
	inline bool* get_address_of_bGetChannel_4() { return &___bGetChannel_4; }
	inline void set_bGetChannel_4(bool value)
	{
		___bGetChannel_4 = value;
	}

	inline static int32_t get_offset_of_bRemoveCh_5() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458, ___bRemoveCh_5)); }
	inline bool get_bRemoveCh_5() const { return ___bRemoveCh_5; }
	inline bool* get_address_of_bRemoveCh_5() { return &___bRemoveCh_5; }
	inline void set_bRemoveCh_5(bool value)
	{
		___bRemoveCh_5 = value;
	}

	inline static int32_t get_offset_of_bGetChannel2_6() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458, ___bGetChannel2_6)); }
	inline bool get_bGetChannel2_6() const { return ___bGetChannel2_6; }
	inline bool* get_address_of_bGetChannel2_6() { return &___bGetChannel2_6; }
	inline void set_bGetChannel2_6(bool value)
	{
		___bGetChannel2_6 = value;
	}

	inline static int32_t get_offset_of_bRemoveAll_7() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458, ___bRemoveAll_7)); }
	inline bool get_bRemoveAll_7() const { return ___bRemoveAll_7; }
	inline bool* get_address_of_bRemoveAll_7() { return &___bRemoveAll_7; }
	inline void set_bRemoveAll_7(bool value)
	{
		___bRemoveAll_7 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_8() { return static_cast<int32_t>(offsetof(U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458, ___U3CU3Ef__refU241_8)); }
	inline U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343 * get_U3CU3Ef__refU241_8() const { return ___U3CU3Ef__refU241_8; }
	inline U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343 ** get_address_of_U3CU3Ef__refU241_8() { return &___U3CU3Ef__refU241_8; }
	inline void set_U3CU3Ef__refU241_8(U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343 * value)
	{
		___U3CU3Ef__refU241_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
