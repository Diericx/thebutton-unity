﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1/<DoRemoveCGAndRemoveAllCG>c__AnonStorey2
struct U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1/<DoRemoveCGAndRemoveAllCG>c__AnonStorey2::.ctor()
extern "C"  void U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2__ctor_m2128630391 (U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1/<DoRemoveCGAndRemoveAllCG>c__AnonStorey2::<>m__0(System.String)
extern "C"  void U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_U3CU3Em__0_m1421073922 (U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1/<DoRemoveCGAndRemoveAllCG>c__AnonStorey2::<>m__1(System.String)
extern "C"  void U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_U3CU3Em__1_m2127991879 (U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1/<DoRemoveCGAndRemoveAllCG>c__AnonStorey2::<>m__2(System.String)
extern "C"  void U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_U3CU3Em__2_m4018952588 (U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458 * __this, String_t* ___result20, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1/<DoRemoveCGAndRemoveAllCG>c__AnonStorey2::<>m__3(System.String)
extern "C"  void U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_U3CU3Em__3_m4088144849 (U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG/<DoRemoveCGAndRemoveAllCG>c__Iterator1/<DoRemoveCGAndRemoveAllCG>c__AnonStorey2::<>m__4(System.String)
extern "C"  void U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_U3CU3Em__4_m1170198038 (U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458 * __this, String_t* ___result20, const MethodInfo* method) IL2CPP_METHOD_ATTR;
