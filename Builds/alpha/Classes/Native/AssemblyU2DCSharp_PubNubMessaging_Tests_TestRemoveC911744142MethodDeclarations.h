﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG
struct TestRemoveCGAndRemoveAllCG_t911744142;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Core.PubnubClientError
struct PubnubClientError_t110317921;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubClient110317921.h"

// System.Void PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG::.ctor()
extern "C"  void TestRemoveCGAndRemoveAllCG__ctor_m3874602086 (TestRemoveCGAndRemoveAllCG_t911744142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG::Start()
extern "C"  Il2CppObject * TestRemoveCGAndRemoveAllCG_Start_m4262324746 (TestRemoveCGAndRemoveAllCG_t911744142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG::DoRemoveCGAndRemoveAllCG(System.Boolean,System.String,System.Boolean,System.Boolean,System.Object,System.String,System.Boolean)
extern "C"  Il2CppObject * TestRemoveCGAndRemoveAllCG_DoRemoveCGAndRemoveAllCG_m4064247783 (TestRemoveCGAndRemoveAllCG_t911744142 * __this, bool ___ssl0, String_t* ___testName1, bool ___asObject2, bool ___withCipher3, Il2CppObject * ___message4, String_t* ___expectedStringResponse5, bool ___matchExpectedStringResponse6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG::DisplayErrorMessage(PubNubMessaging.Core.PubnubClientError)
extern "C"  void TestRemoveCGAndRemoveAllCG_DisplayErrorMessage_m551128495 (TestRemoveCGAndRemoveAllCG_t911744142 * __this, PubnubClientError_t110317921 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestRemoveCGAndRemoveAllCG::DisplayReturnMessageDummy(System.Object)
extern "C"  void TestRemoveCGAndRemoveAllCG_DisplayReturnMessageDummy_m809976211 (TestRemoveCGAndRemoveAllCG_t911744142 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
