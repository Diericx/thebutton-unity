﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSecretKeyOptional
struct TestSecretKeyOptional_t4164925165;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSecretKeyOptional::.ctor()
extern "C"  void TestSecretKeyOptional__ctor_m2024507175 (TestSecretKeyOptional_t4164925165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSecretKeyOptional::Start()
extern "C"  Il2CppObject * TestSecretKeyOptional_Start_m1149679577 (TestSecretKeyOptional_t4164925165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
