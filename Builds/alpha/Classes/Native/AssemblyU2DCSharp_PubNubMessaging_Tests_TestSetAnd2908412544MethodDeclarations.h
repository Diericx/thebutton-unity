﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSetAndDeleteGlobalState
struct TestSetAndDeleteGlobalState_t2908412544;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSetAndDeleteGlobalState::.ctor()
extern "C"  void TestSetAndDeleteGlobalState__ctor_m3825207054 (TestSetAndDeleteGlobalState_t2908412544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSetAndDeleteGlobalState::Start()
extern "C"  Il2CppObject * TestSetAndDeleteGlobalState_Start_m3129275086 (TestSetAndDeleteGlobalState_t2908412544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
