﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSetAndGetGlobalState
struct TestSetAndGetGlobalState_t3454205617;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSetAndGetGlobalState::.ctor()
extern "C"  void TestSetAndGetGlobalState__ctor_m1034288167 (TestSetAndGetGlobalState_t3454205617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSetAndGetGlobalState::Start()
extern "C"  Il2CppObject * TestSetAndGetGlobalState_Start_m300515801 (TestSetAndGetGlobalState_t3454205617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
