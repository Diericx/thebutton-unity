﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSetAndGetGlobalStateUUID
struct TestSetAndGetGlobalStateUUID_t667934322;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSetAndGetGlobalStateUUID::.ctor()
extern "C"  void TestSetAndGetGlobalStateUUID__ctor_m3753437552 (TestSetAndGetGlobalStateUUID_t667934322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSetAndGetGlobalStateUUID::Start()
extern "C"  Il2CppObject * TestSetAndGetGlobalStateUUID_Start_m2847312708 (TestSetAndGetGlobalStateUUID_t667934322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
