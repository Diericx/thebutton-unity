﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int64[]
struct Int64U5BU5D_t717125112;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestSubscribeLongArray
struct  TestSubscribeLongArray_t1054294135  : public MonoBehaviour_t1158329972
{
public:
	// System.Int64[] PubNubMessaging.Tests.TestSubscribeLongArray::Message
	Int64U5BU5D_t717125112* ___Message_2;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeLongArray::SslOn
	bool ___SslOn_3;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeLongArray::CipherOn
	bool ___CipherOn_4;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeLongArray::AsObject
	bool ___AsObject_5;

public:
	inline static int32_t get_offset_of_Message_2() { return static_cast<int32_t>(offsetof(TestSubscribeLongArray_t1054294135, ___Message_2)); }
	inline Int64U5BU5D_t717125112* get_Message_2() const { return ___Message_2; }
	inline Int64U5BU5D_t717125112** get_address_of_Message_2() { return &___Message_2; }
	inline void set_Message_2(Int64U5BU5D_t717125112* value)
	{
		___Message_2 = value;
		Il2CppCodeGenWriteBarrier(&___Message_2, value);
	}

	inline static int32_t get_offset_of_SslOn_3() { return static_cast<int32_t>(offsetof(TestSubscribeLongArray_t1054294135, ___SslOn_3)); }
	inline bool get_SslOn_3() const { return ___SslOn_3; }
	inline bool* get_address_of_SslOn_3() { return &___SslOn_3; }
	inline void set_SslOn_3(bool value)
	{
		___SslOn_3 = value;
	}

	inline static int32_t get_offset_of_CipherOn_4() { return static_cast<int32_t>(offsetof(TestSubscribeLongArray_t1054294135, ___CipherOn_4)); }
	inline bool get_CipherOn_4() const { return ___CipherOn_4; }
	inline bool* get_address_of_CipherOn_4() { return &___CipherOn_4; }
	inline void set_CipherOn_4(bool value)
	{
		___CipherOn_4 = value;
	}

	inline static int32_t get_offset_of_AsObject_5() { return static_cast<int32_t>(offsetof(TestSubscribeLongArray_t1054294135, ___AsObject_5)); }
	inline bool get_AsObject_5() const { return ___AsObject_5; }
	inline bool* get_address_of_AsObject_5() { return &___AsObject_5; }
	inline void set_AsObject_5(bool value)
	{
		___AsObject_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
