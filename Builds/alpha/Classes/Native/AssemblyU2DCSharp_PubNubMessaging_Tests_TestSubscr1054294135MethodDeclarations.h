﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribeLongArray
struct TestSubscribeLongArray_t1054294135;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSubscribeLongArray::.ctor()
extern "C"  void TestSubscribeLongArray__ctor_m1604239635 (TestSubscribeLongArray_t1054294135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSubscribeLongArray::Start()
extern "C"  Il2CppObject * TestSubscribeLongArray_Start_m3638913501 (TestSubscribeLongArray_t1054294135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
