﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribeComplexMessage
struct TestSubscribeComplexMessage_t1078578741;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSubscribeComplexMessage::.ctor()
extern "C"  void TestSubscribeComplexMessage__ctor_m2404173811 (TestSubscribeComplexMessage_t1078578741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSubscribeComplexMessage::Start()
extern "C"  Il2CppObject * TestSubscribeComplexMessage_Start_m2795878561 (TestSubscribeComplexMessage_t1078578741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
