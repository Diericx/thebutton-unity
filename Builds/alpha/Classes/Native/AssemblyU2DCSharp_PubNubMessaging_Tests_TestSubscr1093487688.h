﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1
struct U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2
struct  U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2::testName
	String_t* ___testName_0;
	// System.String PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2::pubMessage
	String_t* ___pubMessage_1;
	// System.String PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2::ch
	String_t* ___ch_2;
	// System.Boolean PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2::bSubMessage2
	bool ___bSubMessage2_3;
	// System.Boolean PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2::bSubConnect
	bool ___bSubConnect_4;
	// PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1 PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2::<>f__ref$1
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694 * ___U3CU3Ef__refU241_5;

public:
	inline static int32_t get_offset_of_testName_0() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688, ___testName_0)); }
	inline String_t* get_testName_0() const { return ___testName_0; }
	inline String_t** get_address_of_testName_0() { return &___testName_0; }
	inline void set_testName_0(String_t* value)
	{
		___testName_0 = value;
		Il2CppCodeGenWriteBarrier(&___testName_0, value);
	}

	inline static int32_t get_offset_of_pubMessage_1() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688, ___pubMessage_1)); }
	inline String_t* get_pubMessage_1() const { return ___pubMessage_1; }
	inline String_t** get_address_of_pubMessage_1() { return &___pubMessage_1; }
	inline void set_pubMessage_1(String_t* value)
	{
		___pubMessage_1 = value;
		Il2CppCodeGenWriteBarrier(&___pubMessage_1, value);
	}

	inline static int32_t get_offset_of_ch_2() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688, ___ch_2)); }
	inline String_t* get_ch_2() const { return ___ch_2; }
	inline String_t** get_address_of_ch_2() { return &___ch_2; }
	inline void set_ch_2(String_t* value)
	{
		___ch_2 = value;
		Il2CppCodeGenWriteBarrier(&___ch_2, value);
	}

	inline static int32_t get_offset_of_bSubMessage2_3() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688, ___bSubMessage2_3)); }
	inline bool get_bSubMessage2_3() const { return ___bSubMessage2_3; }
	inline bool* get_address_of_bSubMessage2_3() { return &___bSubMessage2_3; }
	inline void set_bSubMessage2_3(bool value)
	{
		___bSubMessage2_3 = value;
	}

	inline static int32_t get_offset_of_bSubConnect_4() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688, ___bSubConnect_4)); }
	inline bool get_bSubConnect_4() const { return ___bSubConnect_4; }
	inline bool* get_address_of_bSubConnect_4() { return &___bSubConnect_4; }
	inline void set_bSubConnect_4(bool value)
	{
		___bSubConnect_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_5() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688, ___U3CU3Ef__refU241_5)); }
	inline U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694 * get_U3CU3Ef__refU241_5() const { return ___U3CU3Ef__refU241_5; }
	inline U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694 ** get_address_of_U3CU3Ef__refU241_5() { return &___U3CU3Ef__refU241_5; }
	inline void set_U3CU3Ef__refU241_5(U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694 * value)
	{
		___U3CU3Ef__refU241_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
