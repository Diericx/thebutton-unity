﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2
struct U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2::.ctor()
extern "C"  void U3CDoTestSubscribePSV2U3Ec__AnonStorey2__ctor_m3980850241 (U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2::<>m__0(System.Object)
extern "C"  void U3CDoTestSubscribePSV2U3Ec__AnonStorey2_U3CU3Em__0_m1015431446 (U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688 * __this, Il2CppObject * ___returnMessage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2::<>m__1(System.Object)
extern "C"  void U3CDoTestSubscribePSV2U3Ec__AnonStorey2_U3CU3Em__1_m3929662709 (U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688 * __this, Il2CppObject * ___retConnect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2::<>m__2(System.Object)
extern "C"  void U3CDoTestSubscribePSV2U3Ec__AnonStorey2_U3CU3Em__2_m44632148 (U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688 * __this, Il2CppObject * ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2::<>m__3(System.String)
extern "C"  void U3CDoTestSubscribePSV2U3Ec__AnonStorey2_U3CU3Em__3_m3939722847 (U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688 * __this, String_t* ___returnMessage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2::<>m__4(System.String)
extern "C"  void U3CDoTestSubscribePSV2U3Ec__AnonStorey2_U3CU3Em__4_m3330406588 (U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688 * __this, String_t* ___retConnect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2::<>m__5(System.String)
extern "C"  void U3CDoTestSubscribePSV2U3Ec__AnonStorey2_U3CU3Em__5_m1949679005 (U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2::<>m__6(System.String)
extern "C"  void U3CDoTestSubscribePSV2U3Ec__AnonStorey2_U3CU3Em__6_m2359611642 (U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688 * __this, String_t* ___pub0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
