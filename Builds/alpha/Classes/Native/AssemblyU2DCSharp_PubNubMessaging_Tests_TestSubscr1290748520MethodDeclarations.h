﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribeStringArray
struct TestSubscribeStringArray_t1290748520;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSubscribeStringArray::.ctor()
extern "C"  void TestSubscribeStringArray__ctor_m2435029328 (TestSubscribeStringArray_t1290748520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSubscribeStringArray::Start()
extern "C"  Il2CppObject * TestSubscribeStringArray_Start_m3490071804 (TestSubscribeStringArray_t1290748520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
