﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribeWildcard
struct TestSubscribeWildcard_t1329469586;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// PubNubMessaging.Core.PubnubClientError
struct PubnubClientError_t110317921;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubClient110317921.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void PubNubMessaging.Tests.TestSubscribeWildcard::.ctor()
extern "C"  void TestSubscribeWildcard__ctor_m1713862582 (TestSubscribeWildcard_t1329469586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSubscribeWildcard::Start()
extern "C"  Il2CppObject * TestSubscribeWildcard_Start_m1537378906 (TestSubscribeWildcard_t1329469586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSubscribeWildcard::DoTestSubscribeWildcard(System.String)
extern "C"  Il2CppObject * TestSubscribeWildcard_DoTestSubscribeWildcard_m1108936885 (TestSubscribeWildcard_t1329469586 * __this, String_t* ___testName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribeWildcard::DisplayErrorMessage(PubNubMessaging.Core.PubnubClientError)
extern "C"  void TestSubscribeWildcard_DisplayErrorMessage_m1812339971 (TestSubscribeWildcard_t1329469586 * __this, PubnubClientError_t110317921 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribeWildcard::DisplayReturnMessageDummy(System.Object)
extern "C"  void TestSubscribeWildcard_DisplayReturnMessageDummy_m2994616959 (TestSubscribeWildcard_t1329469586 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
