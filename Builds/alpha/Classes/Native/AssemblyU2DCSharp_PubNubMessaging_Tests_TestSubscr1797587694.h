﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Random
struct Random_t1044426839;
// System.String
struct String_t;
// PubNubMessaging.Tests.TestSubscribePubSubV2Response
struct TestSubscribePubSubV2Response_t3284876308;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2
struct U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1
struct  U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694  : public Il2CppObject
{
public:
	// System.Random PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1::<r>__0
	Random_t1044426839 * ___U3CrU3E__0_0;
	// System.String PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1::testName
	String_t* ___testName_1;
	// System.String PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1::<uuid>__2
	String_t* ___U3CuuidU3E__2_2;
	// System.Boolean PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1::<bUnsub>__6
	bool ___U3CbUnsubU3E__6_3;
	// System.String PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1::<strLog2>__7
	String_t* ___U3CstrLog2U3E__7_4;
	// PubNubMessaging.Tests.TestSubscribePubSubV2Response PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1::$this
	TestSubscribePubSubV2Response_t3284876308 * ___U24this_5;
	// System.Object PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1::$PC
	int32_t ___U24PC_8;
	// PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2 PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1::$locvar0
	U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688 * ___U24locvar0_9;

public:
	inline static int32_t get_offset_of_U3CrU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694, ___U3CrU3E__0_0)); }
	inline Random_t1044426839 * get_U3CrU3E__0_0() const { return ___U3CrU3E__0_0; }
	inline Random_t1044426839 ** get_address_of_U3CrU3E__0_0() { return &___U3CrU3E__0_0; }
	inline void set_U3CrU3E__0_0(Random_t1044426839 * value)
	{
		___U3CrU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrU3E__0_0, value);
	}

	inline static int32_t get_offset_of_testName_1() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694, ___testName_1)); }
	inline String_t* get_testName_1() const { return ___testName_1; }
	inline String_t** get_address_of_testName_1() { return &___testName_1; }
	inline void set_testName_1(String_t* value)
	{
		___testName_1 = value;
		Il2CppCodeGenWriteBarrier(&___testName_1, value);
	}

	inline static int32_t get_offset_of_U3CuuidU3E__2_2() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694, ___U3CuuidU3E__2_2)); }
	inline String_t* get_U3CuuidU3E__2_2() const { return ___U3CuuidU3E__2_2; }
	inline String_t** get_address_of_U3CuuidU3E__2_2() { return &___U3CuuidU3E__2_2; }
	inline void set_U3CuuidU3E__2_2(String_t* value)
	{
		___U3CuuidU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuuidU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CbUnsubU3E__6_3() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694, ___U3CbUnsubU3E__6_3)); }
	inline bool get_U3CbUnsubU3E__6_3() const { return ___U3CbUnsubU3E__6_3; }
	inline bool* get_address_of_U3CbUnsubU3E__6_3() { return &___U3CbUnsubU3E__6_3; }
	inline void set_U3CbUnsubU3E__6_3(bool value)
	{
		___U3CbUnsubU3E__6_3 = value;
	}

	inline static int32_t get_offset_of_U3CstrLog2U3E__7_4() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694, ___U3CstrLog2U3E__7_4)); }
	inline String_t* get_U3CstrLog2U3E__7_4() const { return ___U3CstrLog2U3E__7_4; }
	inline String_t** get_address_of_U3CstrLog2U3E__7_4() { return &___U3CstrLog2U3E__7_4; }
	inline void set_U3CstrLog2U3E__7_4(String_t* value)
	{
		___U3CstrLog2U3E__7_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstrLog2U3E__7_4, value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694, ___U24this_5)); }
	inline TestSubscribePubSubV2Response_t3284876308 * get_U24this_5() const { return ___U24this_5; }
	inline TestSubscribePubSubV2Response_t3284876308 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(TestSubscribePubSubV2Response_t3284876308 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_9() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694, ___U24locvar0_9)); }
	inline U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688 * get_U24locvar0_9() const { return ___U24locvar0_9; }
	inline U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688 ** get_address_of_U24locvar0_9() { return &___U24locvar0_9; }
	inline void set_U24locvar0_9(U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688 * value)
	{
		___U24locvar0_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
