﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1
struct U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1::.ctor()
extern "C"  void U3CDoTestSubscribePSV2U3Ec__Iterator1__ctor_m2020101091 (U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1::MoveNext()
extern "C"  bool U3CDoTestSubscribePSV2U3Ec__Iterator1_MoveNext_m286114409 (U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoTestSubscribePSV2U3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1580783913 (U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoTestSubscribePSV2U3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3198098897 (U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1::Dispose()
extern "C"  void U3CDoTestSubscribePSV2U3Ec__Iterator1_Dispose_m4019886656 (U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2Response/<DoTestSubscribePSV2>c__Iterator1::Reset()
extern "C"  void U3CDoTestSubscribePSV2U3Ec__Iterator1_Reset_m201138670 (U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
