﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Random
struct Random_t1044426839;
// System.String
struct String_t;
// PubNubMessaging.Tests.TestSubscribeWildcard
struct TestSubscribeWildcard_t1329469586;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1/<DoTestSubscribeWildcard>c__AnonStorey2
struct U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1
struct  U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785  : public Il2CppObject
{
public:
	// System.Random PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1::<r>__0
	Random_t1044426839 * ___U3CrU3E__0_0;
	// System.String PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1::testName
	String_t* ___testName_1;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1::<bUnsub>__A
	bool ___U3CbUnsubU3E__A_2;
	// System.String PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1::<strLog2>__B
	String_t* ___U3CstrLog2U3E__B_3;
	// PubNubMessaging.Tests.TestSubscribeWildcard PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1::$this
	TestSubscribeWildcard_t1329469586 * ___U24this_4;
	// System.Object PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1::$PC
	int32_t ___U24PC_7;
	// PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1/<DoTestSubscribeWildcard>c__AnonStorey2 PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1::$locvar0
	U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922 * ___U24locvar0_8;

public:
	inline static int32_t get_offset_of_U3CrU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785, ___U3CrU3E__0_0)); }
	inline Random_t1044426839 * get_U3CrU3E__0_0() const { return ___U3CrU3E__0_0; }
	inline Random_t1044426839 ** get_address_of_U3CrU3E__0_0() { return &___U3CrU3E__0_0; }
	inline void set_U3CrU3E__0_0(Random_t1044426839 * value)
	{
		___U3CrU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrU3E__0_0, value);
	}

	inline static int32_t get_offset_of_testName_1() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785, ___testName_1)); }
	inline String_t* get_testName_1() const { return ___testName_1; }
	inline String_t** get_address_of_testName_1() { return &___testName_1; }
	inline void set_testName_1(String_t* value)
	{
		___testName_1 = value;
		Il2CppCodeGenWriteBarrier(&___testName_1, value);
	}

	inline static int32_t get_offset_of_U3CbUnsubU3E__A_2() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785, ___U3CbUnsubU3E__A_2)); }
	inline bool get_U3CbUnsubU3E__A_2() const { return ___U3CbUnsubU3E__A_2; }
	inline bool* get_address_of_U3CbUnsubU3E__A_2() { return &___U3CbUnsubU3E__A_2; }
	inline void set_U3CbUnsubU3E__A_2(bool value)
	{
		___U3CbUnsubU3E__A_2 = value;
	}

	inline static int32_t get_offset_of_U3CstrLog2U3E__B_3() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785, ___U3CstrLog2U3E__B_3)); }
	inline String_t* get_U3CstrLog2U3E__B_3() const { return ___U3CstrLog2U3E__B_3; }
	inline String_t** get_address_of_U3CstrLog2U3E__B_3() { return &___U3CstrLog2U3E__B_3; }
	inline void set_U3CstrLog2U3E__B_3(String_t* value)
	{
		___U3CstrLog2U3E__B_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstrLog2U3E__B_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785, ___U24this_4)); }
	inline TestSubscribeWildcard_t1329469586 * get_U24this_4() const { return ___U24this_4; }
	inline TestSubscribeWildcard_t1329469586 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TestSubscribeWildcard_t1329469586 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_8() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785, ___U24locvar0_8)); }
	inline U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922 * get_U24locvar0_8() const { return ___U24locvar0_8; }
	inline U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922 ** get_address_of_U24locvar0_8() { return &___U24locvar0_8; }
	inline void set_U24locvar0_8(U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922 * value)
	{
		___U24locvar0_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
