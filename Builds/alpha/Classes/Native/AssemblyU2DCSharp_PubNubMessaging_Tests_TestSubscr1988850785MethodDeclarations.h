﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1
struct U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1::.ctor()
extern "C"  void U3CDoTestSubscribeWildcardU3Ec__Iterator1__ctor_m1903592308 (U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1::MoveNext()
extern "C"  bool U3CDoTestSubscribeWildcardU3Ec__Iterator1_MoveNext_m511579756 (U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoTestSubscribeWildcardU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m30360562 (U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoTestSubscribeWildcardU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m865290698 (U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1::Dispose()
extern "C"  void U3CDoTestSubscribeWildcardU3Ec__Iterator1_Dispose_m3810224731 (U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1::Reset()
extern "C"  void U3CDoTestSubscribeWildcardU3Ec__Iterator1_Reset_m1578530953 (U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
