﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribeWithTimetoken
struct TestSubscribeWithTimetoken_t2144859056;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Core.PubnubClientError
struct PubnubClientError_t110317921;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubClient110317921.h"

// System.Void PubNubMessaging.Tests.TestSubscribeWithTimetoken::.ctor()
extern "C"  void TestSubscribeWithTimetoken__ctor_m1936594692 (TestSubscribeWithTimetoken_t2144859056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSubscribeWithTimetoken::Start()
extern "C"  Il2CppObject * TestSubscribeWithTimetoken_Start_m2377277444 (TestSubscribeWithTimetoken_t2144859056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSubscribeWithTimetoken::DoTestSubscribeWithTimetoken(System.Boolean,System.String,System.Boolean,System.Boolean,System.Object,System.String,System.Boolean)
extern "C"  Il2CppObject * TestSubscribeWithTimetoken_DoTestSubscribeWithTimetoken_m3290977149 (TestSubscribeWithTimetoken_t2144859056 * __this, bool ___ssl0, String_t* ___testName1, bool ___asObject2, bool ___withCipher3, Il2CppObject * ___message4, String_t* ___expectedStringResponse5, bool ___matchExpectedStringResponse6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribeWithTimetoken::DisplayErrorMessage(PubNubMessaging.Core.PubnubClientError)
extern "C"  void TestSubscribeWithTimetoken_DisplayErrorMessage_m2527412709 (TestSubscribeWithTimetoken_t2144859056 * __this, PubnubClientError_t110317921 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribeWithTimetoken::DisplayReturnMessageDummy(System.Object)
extern "C"  void TestSubscribeWithTimetoken_DisplayReturnMessageDummy_m3253362297 (TestSubscribeWithTimetoken_t2144859056 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
