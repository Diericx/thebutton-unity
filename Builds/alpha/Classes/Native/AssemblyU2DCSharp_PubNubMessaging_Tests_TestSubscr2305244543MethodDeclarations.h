﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribeDouble
struct TestSubscribeDouble_t2305244543;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSubscribeDouble::.ctor()
extern "C"  void TestSubscribeDouble__ctor_m1486994805 (TestSubscribeDouble_t2305244543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSubscribeDouble::Start()
extern "C"  Il2CppObject * TestSubscribeDouble_Start_m994058115 (TestSubscribeDouble_t2305244543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
