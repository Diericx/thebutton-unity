﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1
struct U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2
struct  U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t2364701085  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2::testName
	String_t* ___testName_0;
	// System.String PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2::ch
	String_t* ___ch_1;
	// System.Boolean PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2::bSubMessage2
	bool ___bSubMessage2_2;
	// System.Boolean PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2::bSubConnect
	bool ___bSubConnect_3;
	// PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1 PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2::<>f__ref$1
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727 * ___U3CU3Ef__refU241_4;

public:
	inline static int32_t get_offset_of_testName_0() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t2364701085, ___testName_0)); }
	inline String_t* get_testName_0() const { return ___testName_0; }
	inline String_t** get_address_of_testName_0() { return &___testName_0; }
	inline void set_testName_0(String_t* value)
	{
		___testName_0 = value;
		Il2CppCodeGenWriteBarrier(&___testName_0, value);
	}

	inline static int32_t get_offset_of_ch_1() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t2364701085, ___ch_1)); }
	inline String_t* get_ch_1() const { return ___ch_1; }
	inline String_t** get_address_of_ch_1() { return &___ch_1; }
	inline void set_ch_1(String_t* value)
	{
		___ch_1 = value;
		Il2CppCodeGenWriteBarrier(&___ch_1, value);
	}

	inline static int32_t get_offset_of_bSubMessage2_2() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t2364701085, ___bSubMessage2_2)); }
	inline bool get_bSubMessage2_2() const { return ___bSubMessage2_2; }
	inline bool* get_address_of_bSubMessage2_2() { return &___bSubMessage2_2; }
	inline void set_bSubMessage2_2(bool value)
	{
		___bSubMessage2_2 = value;
	}

	inline static int32_t get_offset_of_bSubConnect_3() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t2364701085, ___bSubConnect_3)); }
	inline bool get_bSubConnect_3() const { return ___bSubConnect_3; }
	inline bool* get_address_of_bSubConnect_3() { return &___bSubConnect_3; }
	inline void set_bSubConnect_3(bool value)
	{
		___bSubConnect_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_4() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t2364701085, ___U3CU3Ef__refU241_4)); }
	inline U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727 * get_U3CU3Ef__refU241_4() const { return ___U3CU3Ef__refU241_4; }
	inline U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727 ** get_address_of_U3CU3Ef__refU241_4() { return &___U3CU3Ef__refU241_4; }
	inline void set_U3CU3Ef__refU241_4(U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727 * value)
	{
		___U3CU3Ef__refU241_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
