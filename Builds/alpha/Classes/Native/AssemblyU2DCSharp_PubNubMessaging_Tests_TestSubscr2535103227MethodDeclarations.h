﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribeTimeout
struct TestSubscribeTimeout_t2535103227;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSubscribeTimeout::.ctor()
extern "C"  void TestSubscribeTimeout__ctor_m1847187619 (TestSubscribeTimeout_t2535103227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSubscribeTimeout::Start()
extern "C"  Il2CppObject * TestSubscribeTimeout_Start_m2580020369 (TestSubscribeTimeout_t2535103227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
