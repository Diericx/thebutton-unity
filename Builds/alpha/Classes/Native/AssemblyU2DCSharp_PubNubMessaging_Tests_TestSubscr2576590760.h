﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1
struct U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2
struct  U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2::testName
	String_t* ___testName_0;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2::bAddChannel
	bool ___bAddChannel_1;
	// System.String PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2::cg
	String_t* ___cg_2;
	// System.String PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2::channel
	String_t* ___channel_3;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2::bGetChannel
	bool ___bGetChannel_4;
	// System.String PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2::tt
	String_t* ___tt_5;
	// System.String PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2::pubMessage
	String_t* ___pubMessage_6;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2::bSubMessage
	bool ___bSubMessage_7;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2::bUnsub
	bool ___bUnsub_8;
	// PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1 PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2::<>f__ref$1
	U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321 * ___U3CU3Ef__refU241_9;

public:
	inline static int32_t get_offset_of_testName_0() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760, ___testName_0)); }
	inline String_t* get_testName_0() const { return ___testName_0; }
	inline String_t** get_address_of_testName_0() { return &___testName_0; }
	inline void set_testName_0(String_t* value)
	{
		___testName_0 = value;
		Il2CppCodeGenWriteBarrier(&___testName_0, value);
	}

	inline static int32_t get_offset_of_bAddChannel_1() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760, ___bAddChannel_1)); }
	inline bool get_bAddChannel_1() const { return ___bAddChannel_1; }
	inline bool* get_address_of_bAddChannel_1() { return &___bAddChannel_1; }
	inline void set_bAddChannel_1(bool value)
	{
		___bAddChannel_1 = value;
	}

	inline static int32_t get_offset_of_cg_2() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760, ___cg_2)); }
	inline String_t* get_cg_2() const { return ___cg_2; }
	inline String_t** get_address_of_cg_2() { return &___cg_2; }
	inline void set_cg_2(String_t* value)
	{
		___cg_2 = value;
		Il2CppCodeGenWriteBarrier(&___cg_2, value);
	}

	inline static int32_t get_offset_of_channel_3() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760, ___channel_3)); }
	inline String_t* get_channel_3() const { return ___channel_3; }
	inline String_t** get_address_of_channel_3() { return &___channel_3; }
	inline void set_channel_3(String_t* value)
	{
		___channel_3 = value;
		Il2CppCodeGenWriteBarrier(&___channel_3, value);
	}

	inline static int32_t get_offset_of_bGetChannel_4() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760, ___bGetChannel_4)); }
	inline bool get_bGetChannel_4() const { return ___bGetChannel_4; }
	inline bool* get_address_of_bGetChannel_4() { return &___bGetChannel_4; }
	inline void set_bGetChannel_4(bool value)
	{
		___bGetChannel_4 = value;
	}

	inline static int32_t get_offset_of_tt_5() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760, ___tt_5)); }
	inline String_t* get_tt_5() const { return ___tt_5; }
	inline String_t** get_address_of_tt_5() { return &___tt_5; }
	inline void set_tt_5(String_t* value)
	{
		___tt_5 = value;
		Il2CppCodeGenWriteBarrier(&___tt_5, value);
	}

	inline static int32_t get_offset_of_pubMessage_6() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760, ___pubMessage_6)); }
	inline String_t* get_pubMessage_6() const { return ___pubMessage_6; }
	inline String_t** get_address_of_pubMessage_6() { return &___pubMessage_6; }
	inline void set_pubMessage_6(String_t* value)
	{
		___pubMessage_6 = value;
		Il2CppCodeGenWriteBarrier(&___pubMessage_6, value);
	}

	inline static int32_t get_offset_of_bSubMessage_7() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760, ___bSubMessage_7)); }
	inline bool get_bSubMessage_7() const { return ___bSubMessage_7; }
	inline bool* get_address_of_bSubMessage_7() { return &___bSubMessage_7; }
	inline void set_bSubMessage_7(bool value)
	{
		___bSubMessage_7 = value;
	}

	inline static int32_t get_offset_of_bUnsub_8() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760, ___bUnsub_8)); }
	inline bool get_bUnsub_8() const { return ___bUnsub_8; }
	inline bool* get_address_of_bUnsub_8() { return &___bUnsub_8; }
	inline void set_bUnsub_8(bool value)
	{
		___bUnsub_8 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_9() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760, ___U3CU3Ef__refU241_9)); }
	inline U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321 * get_U3CU3Ef__refU241_9() const { return ___U3CU3Ef__refU241_9; }
	inline U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321 ** get_address_of_U3CU3Ef__refU241_9() { return &___U3CU3Ef__refU241_9; }
	inline void set_U3CU3Ef__refU241_9(U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321 * value)
	{
		___U3CU3Ef__refU241_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
