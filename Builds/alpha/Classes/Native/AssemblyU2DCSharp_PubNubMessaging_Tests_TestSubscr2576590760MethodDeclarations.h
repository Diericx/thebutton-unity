﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2
struct U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2::.ctor()
extern "C"  void U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2__ctor_m2276375873 (U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2::<>m__0(System.String)
extern "C"  void U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_U3CU3Em__0_m713850508 (U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2::<>m__1(System.String)
extern "C"  void U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_U3CU3Em__1_m3099022929 (U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2::<>m__2(System.String)
extern "C"  void U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_U3CU3Em__2_m940803094 (U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760 * __this, String_t* ___pub0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2::<>m__3(System.String)
extern "C"  void U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_U3CU3Em__3_m3325975515 (U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2::<>m__4(System.String)
extern "C"  void U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_U3CU3Em__4_m462974624 (U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760 * __this, String_t* ___retConnect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2::<>m__5(System.String)
extern "C"  void U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_U3CU3Em__5_m2848147045 (U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2::<>m__6(System.String)
extern "C"  void U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_U3CU3Em__6_m689927210 (U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760 * __this, String_t* ___result20, const MethodInfo* method) IL2CPP_METHOD_ATTR;
