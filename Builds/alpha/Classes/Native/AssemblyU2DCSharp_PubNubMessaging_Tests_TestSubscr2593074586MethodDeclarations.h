﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t2593074586;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m3664316505 (U3CStartU3Ec__Iterator0_t2593074586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m1081369335 (U3CStartU3Ec__Iterator0_t2593074586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2701106219 (U3CStartU3Ec__Iterator0_t2593074586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4128417091 (U3CStartU3Ec__Iterator0_t2593074586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m2676094908 (U3CStartU3Ec__Iterator0_t2593074586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m4200787966 (U3CStartU3Ec__Iterator0_t2593074586 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
