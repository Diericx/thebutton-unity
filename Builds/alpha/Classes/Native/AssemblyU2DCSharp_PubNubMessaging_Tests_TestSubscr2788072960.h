﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestSubscribeEmptyArray
struct  TestSubscribeEmptyArray_t2788072960  : public MonoBehaviour_t1158329972
{
public:
	// System.Object[] PubNubMessaging.Tests.TestSubscribeEmptyArray::Message
	ObjectU5BU5D_t3614634134* ___Message_2;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeEmptyArray::SslOn
	bool ___SslOn_3;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeEmptyArray::CipherOn
	bool ___CipherOn_4;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeEmptyArray::AsObject
	bool ___AsObject_5;

public:
	inline static int32_t get_offset_of_Message_2() { return static_cast<int32_t>(offsetof(TestSubscribeEmptyArray_t2788072960, ___Message_2)); }
	inline ObjectU5BU5D_t3614634134* get_Message_2() const { return ___Message_2; }
	inline ObjectU5BU5D_t3614634134** get_address_of_Message_2() { return &___Message_2; }
	inline void set_Message_2(ObjectU5BU5D_t3614634134* value)
	{
		___Message_2 = value;
		Il2CppCodeGenWriteBarrier(&___Message_2, value);
	}

	inline static int32_t get_offset_of_SslOn_3() { return static_cast<int32_t>(offsetof(TestSubscribeEmptyArray_t2788072960, ___SslOn_3)); }
	inline bool get_SslOn_3() const { return ___SslOn_3; }
	inline bool* get_address_of_SslOn_3() { return &___SslOn_3; }
	inline void set_SslOn_3(bool value)
	{
		___SslOn_3 = value;
	}

	inline static int32_t get_offset_of_CipherOn_4() { return static_cast<int32_t>(offsetof(TestSubscribeEmptyArray_t2788072960, ___CipherOn_4)); }
	inline bool get_CipherOn_4() const { return ___CipherOn_4; }
	inline bool* get_address_of_CipherOn_4() { return &___CipherOn_4; }
	inline void set_CipherOn_4(bool value)
	{
		___CipherOn_4 = value;
	}

	inline static int32_t get_offset_of_AsObject_5() { return static_cast<int32_t>(offsetof(TestSubscribeEmptyArray_t2788072960, ___AsObject_5)); }
	inline bool get_AsObject_5() const { return ___AsObject_5; }
	inline bool* get_address_of_AsObject_5() { return &___AsObject_5; }
	inline void set_AsObject_5(bool value)
	{
		___AsObject_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
