﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribeEmptyArray
struct TestSubscribeEmptyArray_t2788072960;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSubscribeEmptyArray::.ctor()
extern "C"  void TestSubscribeEmptyArray__ctor_m3770220970 (TestSubscribeEmptyArray_t2788072960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSubscribeEmptyArray::Start()
extern "C"  Il2CppObject * TestSubscribeEmptyArray_Start_m554000686 (TestSubscribeEmptyArray_t2788072960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
