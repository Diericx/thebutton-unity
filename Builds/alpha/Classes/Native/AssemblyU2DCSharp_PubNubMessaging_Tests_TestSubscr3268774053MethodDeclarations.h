﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribeSimpleMessage
struct TestSubscribeSimpleMessage_t3268774053;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSubscribeSimpleMessage::.ctor()
extern "C"  void TestSubscribeSimpleMessage__ctor_m1717543045 (TestSubscribeSimpleMessage_t3268774053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSubscribeSimpleMessage::Start()
extern "C"  Il2CppObject * TestSubscribeSimpleMessage_Start_m845812547 (TestSubscribeSimpleMessage_t3268774053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
