﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Double[]
struct DoubleU5BU5D_t1889952540;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestSubscribeDoubeArray
struct  TestSubscribeDoubeArray_t3441415060  : public MonoBehaviour_t1158329972
{
public:
	// System.Double[] PubNubMessaging.Tests.TestSubscribeDoubeArray::Message
	DoubleU5BU5D_t1889952540* ___Message_2;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeDoubeArray::SslOn
	bool ___SslOn_3;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeDoubeArray::CipherOn
	bool ___CipherOn_4;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeDoubeArray::AsObject
	bool ___AsObject_5;

public:
	inline static int32_t get_offset_of_Message_2() { return static_cast<int32_t>(offsetof(TestSubscribeDoubeArray_t3441415060, ___Message_2)); }
	inline DoubleU5BU5D_t1889952540* get_Message_2() const { return ___Message_2; }
	inline DoubleU5BU5D_t1889952540** get_address_of_Message_2() { return &___Message_2; }
	inline void set_Message_2(DoubleU5BU5D_t1889952540* value)
	{
		___Message_2 = value;
		Il2CppCodeGenWriteBarrier(&___Message_2, value);
	}

	inline static int32_t get_offset_of_SslOn_3() { return static_cast<int32_t>(offsetof(TestSubscribeDoubeArray_t3441415060, ___SslOn_3)); }
	inline bool get_SslOn_3() const { return ___SslOn_3; }
	inline bool* get_address_of_SslOn_3() { return &___SslOn_3; }
	inline void set_SslOn_3(bool value)
	{
		___SslOn_3 = value;
	}

	inline static int32_t get_offset_of_CipherOn_4() { return static_cast<int32_t>(offsetof(TestSubscribeDoubeArray_t3441415060, ___CipherOn_4)); }
	inline bool get_CipherOn_4() const { return ___CipherOn_4; }
	inline bool* get_address_of_CipherOn_4() { return &___CipherOn_4; }
	inline void set_CipherOn_4(bool value)
	{
		___CipherOn_4 = value;
	}

	inline static int32_t get_offset_of_AsObject_5() { return static_cast<int32_t>(offsetof(TestSubscribeDoubeArray_t3441415060, ___AsObject_5)); }
	inline bool get_AsObject_5() const { return ___AsObject_5; }
	inline bool* get_address_of_AsObject_5() { return &___AsObject_5; }
	inline void set_AsObject_5(bool value)
	{
		___AsObject_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
