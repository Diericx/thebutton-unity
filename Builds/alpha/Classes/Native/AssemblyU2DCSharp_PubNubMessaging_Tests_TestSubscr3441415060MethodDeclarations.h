﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribeDoubeArray
struct TestSubscribeDoubeArray_t3441415060;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSubscribeDoubeArray::.ctor()
extern "C"  void TestSubscribeDoubeArray__ctor_m878420558 (TestSubscribeDoubeArray_t3441415060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSubscribeDoubeArray::Start()
extern "C"  Il2CppObject * TestSubscribeDoubeArray_Start_m1385995266 (TestSubscribeDoubeArray_t3441415060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
