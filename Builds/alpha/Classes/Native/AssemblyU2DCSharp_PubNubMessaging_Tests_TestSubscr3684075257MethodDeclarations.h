﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribeInt
struct TestSubscribeInt_t3684075257;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSubscribeInt::.ctor()
extern "C"  void TestSubscribeInt__ctor_m1607227583 (TestSubscribeInt_t3684075257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSubscribeInt::Start()
extern "C"  Il2CppObject * TestSubscribeInt_Start_m890465057 (TestSubscribeInt_t3684075257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
