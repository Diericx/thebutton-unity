﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribeDict
struct TestSubscribeDict_t3825317694;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSubscribeDict::.ctor()
extern "C"  void TestSubscribeDict__ctor_m2326556804 (TestSubscribeDict_t3825317694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSubscribeDict::Start()
extern "C"  Il2CppObject * TestSubscribeDict_Start_m4215047508 (TestSubscribeDict_t3825317694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
