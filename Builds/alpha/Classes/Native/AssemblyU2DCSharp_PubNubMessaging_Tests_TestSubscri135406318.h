﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PubNubMessaging.Tests.CommonIntergrationTests
struct CommonIntergrationTests_t1691354350;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestSubscribeJoin
struct  TestSubscribeJoin_t135406318  : public MonoBehaviour_t1158329972
{
public:
	// PubNubMessaging.Tests.CommonIntergrationTests PubNubMessaging.Tests.TestSubscribeJoin::common
	CommonIntergrationTests_t1691354350 * ___common_2;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeJoin::SslOn
	bool ___SslOn_3;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeJoin::AsObject
	bool ___AsObject_4;

public:
	inline static int32_t get_offset_of_common_2() { return static_cast<int32_t>(offsetof(TestSubscribeJoin_t135406318, ___common_2)); }
	inline CommonIntergrationTests_t1691354350 * get_common_2() const { return ___common_2; }
	inline CommonIntergrationTests_t1691354350 ** get_address_of_common_2() { return &___common_2; }
	inline void set_common_2(CommonIntergrationTests_t1691354350 * value)
	{
		___common_2 = value;
		Il2CppCodeGenWriteBarrier(&___common_2, value);
	}

	inline static int32_t get_offset_of_SslOn_3() { return static_cast<int32_t>(offsetof(TestSubscribeJoin_t135406318, ___SslOn_3)); }
	inline bool get_SslOn_3() const { return ___SslOn_3; }
	inline bool* get_address_of_SslOn_3() { return &___SslOn_3; }
	inline void set_SslOn_3(bool value)
	{
		___SslOn_3 = value;
	}

	inline static int32_t get_offset_of_AsObject_4() { return static_cast<int32_t>(offsetof(TestSubscribeJoin_t135406318, ___AsObject_4)); }
	inline bool get_AsObject_4() const { return ___AsObject_4; }
	inline bool* get_address_of_AsObject_4() { return &___AsObject_4; }
	inline void set_AsObject_4(bool value)
	{
		___AsObject_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
