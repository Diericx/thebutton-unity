﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribeJoin
struct TestSubscribeJoin_t135406318;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSubscribeJoin::.ctor()
extern "C"  void TestSubscribeJoin__ctor_m3972697852 (TestSubscribeJoin_t135406318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSubscribeJoin::Start()
extern "C"  Il2CppObject * TestSubscribeJoin_Start_m2053159012 (TestSubscribeJoin_t135406318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
