﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// PubNubMessaging.Tests.CommonIntergrationTests
struct CommonIntergrationTests_t1691354350;
// PubNubMessaging.Tests.TestSubscribeComplexMessage
struct TestSubscribeComplexMessage_t1078578741;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestSubscribeComplexMessage/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t196500632  : public Il2CppObject
{
public:
	// System.Object PubNubMessaging.Tests.TestSubscribeComplexMessage/<Start>c__Iterator0::<Message>__0
	Il2CppObject * ___U3CMessageU3E__0_0;
	// PubNubMessaging.Tests.CommonIntergrationTests PubNubMessaging.Tests.TestSubscribeComplexMessage/<Start>c__Iterator0::<common>__1
	CommonIntergrationTests_t1691354350 * ___U3CcommonU3E__1_1;
	// PubNubMessaging.Tests.TestSubscribeComplexMessage PubNubMessaging.Tests.TestSubscribeComplexMessage/<Start>c__Iterator0::$this
	TestSubscribeComplexMessage_t1078578741 * ___U24this_2;
	// System.Object PubNubMessaging.Tests.TestSubscribeComplexMessage/<Start>c__Iterator0::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeComplexMessage/<Start>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 PubNubMessaging.Tests.TestSubscribeComplexMessage/<Start>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CMessageU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t196500632, ___U3CMessageU3E__0_0)); }
	inline Il2CppObject * get_U3CMessageU3E__0_0() const { return ___U3CMessageU3E__0_0; }
	inline Il2CppObject ** get_address_of_U3CMessageU3E__0_0() { return &___U3CMessageU3E__0_0; }
	inline void set_U3CMessageU3E__0_0(Il2CppObject * value)
	{
		___U3CMessageU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMessageU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CcommonU3E__1_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t196500632, ___U3CcommonU3E__1_1)); }
	inline CommonIntergrationTests_t1691354350 * get_U3CcommonU3E__1_1() const { return ___U3CcommonU3E__1_1; }
	inline CommonIntergrationTests_t1691354350 ** get_address_of_U3CcommonU3E__1_1() { return &___U3CcommonU3E__1_1; }
	inline void set_U3CcommonU3E__1_1(CommonIntergrationTests_t1691354350 * value)
	{
		___U3CcommonU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcommonU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t196500632, ___U24this_2)); }
	inline TestSubscribeComplexMessage_t1078578741 * get_U24this_2() const { return ___U24this_2; }
	inline TestSubscribeComplexMessage_t1078578741 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(TestSubscribeComplexMessage_t1078578741 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t196500632, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t196500632, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t196500632, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
