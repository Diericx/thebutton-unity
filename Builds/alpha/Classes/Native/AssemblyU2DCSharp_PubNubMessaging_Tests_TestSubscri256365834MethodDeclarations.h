﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribeLong
struct TestSubscribeLong_t256365834;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSubscribeLong::.ctor()
extern "C"  void TestSubscribeLong__ctor_m4226016510 (TestSubscribeLong_t256365834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSubscribeLong::Start()
extern "C"  Il2CppObject * TestSubscribeLong_Start_m4143336002 (TestSubscribeLong_t256365834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
