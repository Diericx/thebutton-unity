﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribeEmptyDict
struct TestSubscribeEmptyDict_t256670965;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSubscribeEmptyDict::.ctor()
extern "C"  void TestSubscribeEmptyDict__ctor_m1463592843 (TestSubscribeEmptyDict_t256670965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSubscribeEmptyDict::Start()
extern "C"  Il2CppObject * TestSubscribeEmptyDict_Start_m497470013 (TestSubscribeEmptyDict_t256670965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
