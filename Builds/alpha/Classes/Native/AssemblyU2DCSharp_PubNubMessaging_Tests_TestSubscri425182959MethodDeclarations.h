﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse
struct TestSubscribePubSubV2PresenceResponse_t425182959;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// PubNubMessaging.Core.PubnubClientError
struct PubnubClientError_t110317921;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubClient110317921.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse::.ctor()
extern "C"  void TestSubscribePubSubV2PresenceResponse__ctor_m206283545 (TestSubscribePubSubV2PresenceResponse_t425182959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse::Start()
extern "C"  Il2CppObject * TestSubscribePubSubV2PresenceResponse_Start_m2299214147 (TestSubscribePubSubV2PresenceResponse_t425182959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse::DoTestSubscribePSV2(System.String)
extern "C"  Il2CppObject * TestSubscribePubSubV2PresenceResponse_DoTestSubscribePSV2_m678134031 (TestSubscribePubSubV2PresenceResponse_t425182959 * __this, String_t* ___testName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse::DisplayErrorMessage(PubNubMessaging.Core.PubnubClientError)
extern "C"  void TestSubscribePubSubV2PresenceResponse_DisplayErrorMessage_m1102750676 (TestSubscribePubSubV2PresenceResponse_t425182959 * __this, PubnubClientError_t110317921 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse::DisplayReturnMessageDummy(System.Object)
extern "C"  void TestSubscribePubSubV2PresenceResponse_DisplayReturnMessageDummy_m4228214666 (TestSubscribePubSubV2PresenceResponse_t425182959 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
