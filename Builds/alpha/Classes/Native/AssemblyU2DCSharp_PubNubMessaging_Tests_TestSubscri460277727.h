﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Random
struct Random_t1044426839;
// System.String
struct String_t;
// PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse
struct TestSubscribePubSubV2PresenceResponse_t425182959;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2
struct U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t2364701085;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action`1<System.String>
struct Action_1_t1831019615;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1
struct  U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727  : public Il2CppObject
{
public:
	// System.Random PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::<r>__0
	Random_t1044426839 * ___U3CrU3E__0_0;
	// System.String PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::testName
	String_t* ___testName_1;
	// System.String PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::<uuid>__2
	String_t* ___U3CuuidU3E__2_2;
	// System.String PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::<pubMessage>__5
	String_t* ___U3CpubMessageU3E__5_3;
	// System.Boolean PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::<bUnsub>__6
	bool ___U3CbUnsubU3E__6_4;
	// System.String PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::<strLog2>__7
	String_t* ___U3CstrLog2U3E__7_5;
	// PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::$this
	TestSubscribePubSubV2PresenceResponse_t425182959 * ___U24this_6;
	// System.Object PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::$disposing
	bool ___U24disposing_8;
	// System.Int32 PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::$PC
	int32_t ___U24PC_9;
	// PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1/<DoTestSubscribePSV2>c__AnonStorey2 PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::$locvar0
	U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t2364701085 * ___U24locvar0_10;

public:
	inline static int32_t get_offset_of_U3CrU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727, ___U3CrU3E__0_0)); }
	inline Random_t1044426839 * get_U3CrU3E__0_0() const { return ___U3CrU3E__0_0; }
	inline Random_t1044426839 ** get_address_of_U3CrU3E__0_0() { return &___U3CrU3E__0_0; }
	inline void set_U3CrU3E__0_0(Random_t1044426839 * value)
	{
		___U3CrU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrU3E__0_0, value);
	}

	inline static int32_t get_offset_of_testName_1() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727, ___testName_1)); }
	inline String_t* get_testName_1() const { return ___testName_1; }
	inline String_t** get_address_of_testName_1() { return &___testName_1; }
	inline void set_testName_1(String_t* value)
	{
		___testName_1 = value;
		Il2CppCodeGenWriteBarrier(&___testName_1, value);
	}

	inline static int32_t get_offset_of_U3CuuidU3E__2_2() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727, ___U3CuuidU3E__2_2)); }
	inline String_t* get_U3CuuidU3E__2_2() const { return ___U3CuuidU3E__2_2; }
	inline String_t** get_address_of_U3CuuidU3E__2_2() { return &___U3CuuidU3E__2_2; }
	inline void set_U3CuuidU3E__2_2(String_t* value)
	{
		___U3CuuidU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuuidU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CpubMessageU3E__5_3() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727, ___U3CpubMessageU3E__5_3)); }
	inline String_t* get_U3CpubMessageU3E__5_3() const { return ___U3CpubMessageU3E__5_3; }
	inline String_t** get_address_of_U3CpubMessageU3E__5_3() { return &___U3CpubMessageU3E__5_3; }
	inline void set_U3CpubMessageU3E__5_3(String_t* value)
	{
		___U3CpubMessageU3E__5_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpubMessageU3E__5_3, value);
	}

	inline static int32_t get_offset_of_U3CbUnsubU3E__6_4() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727, ___U3CbUnsubU3E__6_4)); }
	inline bool get_U3CbUnsubU3E__6_4() const { return ___U3CbUnsubU3E__6_4; }
	inline bool* get_address_of_U3CbUnsubU3E__6_4() { return &___U3CbUnsubU3E__6_4; }
	inline void set_U3CbUnsubU3E__6_4(bool value)
	{
		___U3CbUnsubU3E__6_4 = value;
	}

	inline static int32_t get_offset_of_U3CstrLog2U3E__7_5() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727, ___U3CstrLog2U3E__7_5)); }
	inline String_t* get_U3CstrLog2U3E__7_5() const { return ___U3CstrLog2U3E__7_5; }
	inline String_t** get_address_of_U3CstrLog2U3E__7_5() { return &___U3CstrLog2U3E__7_5; }
	inline void set_U3CstrLog2U3E__7_5(String_t* value)
	{
		___U3CstrLog2U3E__7_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstrLog2U3E__7_5, value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727, ___U24this_6)); }
	inline TestSubscribePubSubV2PresenceResponse_t425182959 * get_U24this_6() const { return ___U24this_6; }
	inline TestSubscribePubSubV2PresenceResponse_t425182959 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(TestSubscribePubSubV2PresenceResponse_t425182959 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_10() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727, ___U24locvar0_10)); }
	inline U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t2364701085 * get_U24locvar0_10() const { return ___U24locvar0_10; }
	inline U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t2364701085 ** get_address_of_U24locvar0_10() { return &___U24locvar0_10; }
	inline void set_U24locvar0_10(U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t2364701085 * value)
	{
		___U24locvar0_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_10, value);
	}
};

struct U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727_StaticFields
{
public:
	// System.Action`1<System.Object> PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::<>f__am$cache0
	Action_1_t2491248677 * ___U3CU3Ef__amU24cache0_11;
	// System.Action`1<System.Object> PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::<>f__am$cache1
	Action_1_t2491248677 * ___U3CU3Ef__amU24cache1_12;
	// System.Action`1<System.Object> PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::<>f__am$cache2
	Action_1_t2491248677 * ___U3CU3Ef__amU24cache2_13;
	// System.Action`1<System.String> PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::<>f__am$cache3
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache3_14;
	// System.Action`1<System.String> PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::<>f__am$cache4
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache4_15;
	// System.Action`1<System.String> PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::<>f__am$cache5
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache5_16;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline Action_1_t2491248677 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline Action_1_t2491248677 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(Action_1_t2491248677 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_12() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727_StaticFields, ___U3CU3Ef__amU24cache1_12)); }
	inline Action_1_t2491248677 * get_U3CU3Ef__amU24cache1_12() const { return ___U3CU3Ef__amU24cache1_12; }
	inline Action_1_t2491248677 ** get_address_of_U3CU3Ef__amU24cache1_12() { return &___U3CU3Ef__amU24cache1_12; }
	inline void set_U3CU3Ef__amU24cache1_12(Action_1_t2491248677 * value)
	{
		___U3CU3Ef__amU24cache1_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_13() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727_StaticFields, ___U3CU3Ef__amU24cache2_13)); }
	inline Action_1_t2491248677 * get_U3CU3Ef__amU24cache2_13() const { return ___U3CU3Ef__amU24cache2_13; }
	inline Action_1_t2491248677 ** get_address_of_U3CU3Ef__amU24cache2_13() { return &___U3CU3Ef__amU24cache2_13; }
	inline void set_U3CU3Ef__amU24cache2_13(Action_1_t2491248677 * value)
	{
		___U3CU3Ef__amU24cache2_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_14() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727_StaticFields, ___U3CU3Ef__amU24cache3_14)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache3_14() const { return ___U3CU3Ef__amU24cache3_14; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache3_14() { return &___U3CU3Ef__amU24cache3_14; }
	inline void set_U3CU3Ef__amU24cache3_14(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache3_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_15() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727_StaticFields, ___U3CU3Ef__amU24cache4_15)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache4_15() const { return ___U3CU3Ef__amU24cache4_15; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache4_15() { return &___U3CU3Ef__amU24cache4_15; }
	inline void set_U3CU3Ef__amU24cache4_15(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache4_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_16() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727_StaticFields, ___U3CU3Ef__amU24cache5_16)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache5_16() const { return ___U3CU3Ef__amU24cache5_16; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache5_16() { return &___U3CU3Ef__amU24cache5_16; }
	inline void set_U3CU3Ef__amU24cache5_16(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache5_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
