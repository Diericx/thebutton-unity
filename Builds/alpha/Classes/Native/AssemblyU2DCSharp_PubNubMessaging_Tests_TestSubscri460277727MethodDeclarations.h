﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1
struct U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::.ctor()
extern "C"  void U3CDoTestSubscribePSV2U3Ec__Iterator1__ctor_m642974046 (U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::MoveNext()
extern "C"  bool U3CDoTestSubscribePSV2U3Ec__Iterator1_MoveNext_m2058153262 (U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoTestSubscribePSV2U3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m566345872 (U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoTestSubscribePSV2U3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m4145561272 (U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::Dispose()
extern "C"  void U3CDoTestSubscribePSV2U3Ec__Iterator1_Dispose_m368559553 (U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::Reset()
extern "C"  void U3CDoTestSubscribePSV2U3Ec__Iterator1_Reset_m3049561891 (U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::<>m__0(System.Object)
extern "C"  void U3CDoTestSubscribePSV2U3Ec__Iterator1_U3CU3Em__0_m1444755803 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___pub0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::<>m__1(System.Object)
extern "C"  void U3CDoTestSubscribePSV2U3Ec__Iterator1_U3CU3Em__1_m64019770 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___pub0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::<>m__2(System.Object)
extern "C"  void U3CDoTestSubscribePSV2U3Ec__Iterator1_U3CU3Em__2_m473956505 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___pub0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::<>m__3(System.String)
extern "C"  void U3CDoTestSubscribePSV2U3Ec__Iterator1_U3CU3Em__3_m3165884574 (Il2CppObject * __this /* static, unused */, String_t* ___pub0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::<>m__4(System.String)
extern "C"  void U3CDoTestSubscribePSV2U3Ec__Iterator1_U3CU3Em__4_m4183278595 (Il2CppObject * __this /* static, unused */, String_t* ___pub0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribePubSubV2PresenceResponse/<DoTestSubscribePSV2>c__Iterator1::<>m__5(System.String)
extern "C"  void U3CDoTestSubscribePSV2U3Ec__Iterator1_U3CU3Em__5_m2802551012 (Il2CppObject * __this /* static, unused */, String_t* ___pub0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
