﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribeIntArray
struct TestSubscribeIntArray_t499442242;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSubscribeIntArray::.ctor()
extern "C"  void TestSubscribeIntArray__ctor_m729892286 (TestSubscribeIntArray_t499442242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestSubscribeIntArray::Start()
extern "C"  Il2CppObject * TestSubscribeIntArray_Start_m3667736170 (TestSubscribeIntArray_t499442242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
