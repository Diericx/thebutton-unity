﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribeDouble/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t639752250;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestSubscribeDouble/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m2785146277 (U3CStartU3Ec__Iterator0_t639752250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.TestSubscribeDouble/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m763029495 (U3CStartU3Ec__Iterator0_t639752250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestSubscribeDouble/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1647493419 (U3CStartU3Ec__Iterator0_t639752250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestSubscribeDouble/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m257672387 (U3CStartU3Ec__Iterator0_t639752250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribeDouble/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m2775514124 (U3CStartU3Ec__Iterator0_t639752250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribeDouble/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m717396634 (U3CStartU3Ec__Iterator0_t639752250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
