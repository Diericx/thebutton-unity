﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1
struct U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1/<DoTestSubscribeWildcard>c__AnonStorey2
struct  U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1/<DoTestSubscribeWildcard>c__AnonStorey2::testName
	String_t* ___testName_0;
	// System.String PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1/<DoTestSubscribeWildcard>c__AnonStorey2::pubMessage
	String_t* ___pubMessage_1;
	// System.String PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1/<DoTestSubscribeWildcard>c__AnonStorey2::channel
	String_t* ___channel_2;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1/<DoTestSubscribeWildcard>c__AnonStorey2::bSubMessage
	bool ___bSubMessage_3;
	// System.String PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1/<DoTestSubscribeWildcard>c__AnonStorey2::ch
	String_t* ___ch_4;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1/<DoTestSubscribeWildcard>c__AnonStorey2::bSubMessage2
	bool ___bSubMessage2_5;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1/<DoTestSubscribeWildcard>c__AnonStorey2::bSubConnect
	bool ___bSubConnect_6;
	// System.String PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1/<DoTestSubscribeWildcard>c__AnonStorey2::uuid
	String_t* ___uuid_7;
	// System.String PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1/<DoTestSubscribeWildcard>c__AnonStorey2::chToSub
	String_t* ___chToSub_8;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1/<DoTestSubscribeWildcard>c__AnonStorey2::bSubWC
	bool ___bSubWC_9;
	// PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1 PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1/<DoTestSubscribeWildcard>c__AnonStorey2::<>f__ref$1
	U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785 * ___U3CU3Ef__refU241_10;

public:
	inline static int32_t get_offset_of_testName_0() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922, ___testName_0)); }
	inline String_t* get_testName_0() const { return ___testName_0; }
	inline String_t** get_address_of_testName_0() { return &___testName_0; }
	inline void set_testName_0(String_t* value)
	{
		___testName_0 = value;
		Il2CppCodeGenWriteBarrier(&___testName_0, value);
	}

	inline static int32_t get_offset_of_pubMessage_1() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922, ___pubMessage_1)); }
	inline String_t* get_pubMessage_1() const { return ___pubMessage_1; }
	inline String_t** get_address_of_pubMessage_1() { return &___pubMessage_1; }
	inline void set_pubMessage_1(String_t* value)
	{
		___pubMessage_1 = value;
		Il2CppCodeGenWriteBarrier(&___pubMessage_1, value);
	}

	inline static int32_t get_offset_of_channel_2() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922, ___channel_2)); }
	inline String_t* get_channel_2() const { return ___channel_2; }
	inline String_t** get_address_of_channel_2() { return &___channel_2; }
	inline void set_channel_2(String_t* value)
	{
		___channel_2 = value;
		Il2CppCodeGenWriteBarrier(&___channel_2, value);
	}

	inline static int32_t get_offset_of_bSubMessage_3() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922, ___bSubMessage_3)); }
	inline bool get_bSubMessage_3() const { return ___bSubMessage_3; }
	inline bool* get_address_of_bSubMessage_3() { return &___bSubMessage_3; }
	inline void set_bSubMessage_3(bool value)
	{
		___bSubMessage_3 = value;
	}

	inline static int32_t get_offset_of_ch_4() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922, ___ch_4)); }
	inline String_t* get_ch_4() const { return ___ch_4; }
	inline String_t** get_address_of_ch_4() { return &___ch_4; }
	inline void set_ch_4(String_t* value)
	{
		___ch_4 = value;
		Il2CppCodeGenWriteBarrier(&___ch_4, value);
	}

	inline static int32_t get_offset_of_bSubMessage2_5() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922, ___bSubMessage2_5)); }
	inline bool get_bSubMessage2_5() const { return ___bSubMessage2_5; }
	inline bool* get_address_of_bSubMessage2_5() { return &___bSubMessage2_5; }
	inline void set_bSubMessage2_5(bool value)
	{
		___bSubMessage2_5 = value;
	}

	inline static int32_t get_offset_of_bSubConnect_6() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922, ___bSubConnect_6)); }
	inline bool get_bSubConnect_6() const { return ___bSubConnect_6; }
	inline bool* get_address_of_bSubConnect_6() { return &___bSubConnect_6; }
	inline void set_bSubConnect_6(bool value)
	{
		___bSubConnect_6 = value;
	}

	inline static int32_t get_offset_of_uuid_7() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922, ___uuid_7)); }
	inline String_t* get_uuid_7() const { return ___uuid_7; }
	inline String_t** get_address_of_uuid_7() { return &___uuid_7; }
	inline void set_uuid_7(String_t* value)
	{
		___uuid_7 = value;
		Il2CppCodeGenWriteBarrier(&___uuid_7, value);
	}

	inline static int32_t get_offset_of_chToSub_8() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922, ___chToSub_8)); }
	inline String_t* get_chToSub_8() const { return ___chToSub_8; }
	inline String_t** get_address_of_chToSub_8() { return &___chToSub_8; }
	inline void set_chToSub_8(String_t* value)
	{
		___chToSub_8 = value;
		Il2CppCodeGenWriteBarrier(&___chToSub_8, value);
	}

	inline static int32_t get_offset_of_bSubWC_9() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922, ___bSubWC_9)); }
	inline bool get_bSubWC_9() const { return ___bSubWC_9; }
	inline bool* get_address_of_bSubWC_9() { return &___bSubWC_9; }
	inline void set_bSubWC_9(bool value)
	{
		___bSubWC_9 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_10() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922, ___U3CU3Ef__refU241_10)); }
	inline U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785 * get_U3CU3Ef__refU241_10() const { return ___U3CU3Ef__refU241_10; }
	inline U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785 ** get_address_of_U3CU3Ef__refU241_10() { return &___U3CU3Ef__refU241_10; }
	inline void set_U3CU3Ef__refU241_10(U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785 * value)
	{
		___U3CU3Ef__refU241_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
