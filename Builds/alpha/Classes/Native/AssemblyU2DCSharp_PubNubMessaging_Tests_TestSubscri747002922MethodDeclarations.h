﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1/<DoTestSubscribeWildcard>c__AnonStorey2
struct U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1/<DoTestSubscribeWildcard>c__AnonStorey2::.ctor()
extern "C"  void U3CDoTestSubscribeWildcardU3Ec__AnonStorey2__ctor_m1447912779 (U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1/<DoTestSubscribeWildcard>c__AnonStorey2::<>m__0(System.String)
extern "C"  void U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_U3CU3Em__0_m570184970 (U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1/<DoTestSubscribeWildcard>c__AnonStorey2::<>m__1(System.String)
extern "C"  void U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_U3CU3Em__1_m1288591979 (U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922 * __this, String_t* ___retConnect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1/<DoTestSubscribeWildcard>c__AnonStorey2::<>m__2(System.String)
extern "C"  void U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_U3CU3Em__2_m4198231880 (U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestSubscribeWildcard/<DoTestSubscribeWildcard>c__Iterator1/<DoTestSubscribeWildcard>c__AnonStorey2::<>m__3(System.String)
extern "C"  void U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_U3CU3Em__3_m414478505 (U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922 * __this, String_t* ___pub0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
