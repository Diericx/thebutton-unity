﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Random
struct Random_t1044426839;
// System.String
struct String_t;
// PubNubMessaging.Tests.TestSubscribeWithTimetoken
struct TestSubscribeWithTimetoken_t2144859056;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2
struct U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1
struct  U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321  : public Il2CppObject
{
public:
	// System.Random PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1::<r>__0
	Random_t1044426839 * ___U3CrU3E__0_0;
	// System.String PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1::<ch>__2
	String_t* ___U3CchU3E__2_1;
	// System.String PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1::testName
	String_t* ___testName_2;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1::<bGetAllCG>__6
	bool ___U3CbGetAllCGU3E__6_3;
	// System.String PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1::<uuid>__7
	String_t* ___U3CuuidU3E__7_4;
	// System.String PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1::<strLog>__8
	String_t* ___U3CstrLogU3E__8_5;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1::<bRemoveCh>__D
	bool ___U3CbRemoveChU3E__D_6;
	// System.String PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1::<strLog2>__E
	String_t* ___U3CstrLog2U3E__E_7;
	// PubNubMessaging.Tests.TestSubscribeWithTimetoken PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1::$this
	TestSubscribeWithTimetoken_t2144859056 * ___U24this_8;
	// System.Object PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1::$current
	Il2CppObject * ___U24current_9;
	// System.Boolean PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1::$disposing
	bool ___U24disposing_10;
	// System.Int32 PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1::$PC
	int32_t ___U24PC_11;
	// PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1/<DoTestSubscribeWithTimetoken>c__AnonStorey2 PubNubMessaging.Tests.TestSubscribeWithTimetoken/<DoTestSubscribeWithTimetoken>c__Iterator1::$locvar0
	U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760 * ___U24locvar0_12;

public:
	inline static int32_t get_offset_of_U3CrU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321, ___U3CrU3E__0_0)); }
	inline Random_t1044426839 * get_U3CrU3E__0_0() const { return ___U3CrU3E__0_0; }
	inline Random_t1044426839 ** get_address_of_U3CrU3E__0_0() { return &___U3CrU3E__0_0; }
	inline void set_U3CrU3E__0_0(Random_t1044426839 * value)
	{
		___U3CrU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CchU3E__2_1() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321, ___U3CchU3E__2_1)); }
	inline String_t* get_U3CchU3E__2_1() const { return ___U3CchU3E__2_1; }
	inline String_t** get_address_of_U3CchU3E__2_1() { return &___U3CchU3E__2_1; }
	inline void set_U3CchU3E__2_1(String_t* value)
	{
		___U3CchU3E__2_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchU3E__2_1, value);
	}

	inline static int32_t get_offset_of_testName_2() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321, ___testName_2)); }
	inline String_t* get_testName_2() const { return ___testName_2; }
	inline String_t** get_address_of_testName_2() { return &___testName_2; }
	inline void set_testName_2(String_t* value)
	{
		___testName_2 = value;
		Il2CppCodeGenWriteBarrier(&___testName_2, value);
	}

	inline static int32_t get_offset_of_U3CbGetAllCGU3E__6_3() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321, ___U3CbGetAllCGU3E__6_3)); }
	inline bool get_U3CbGetAllCGU3E__6_3() const { return ___U3CbGetAllCGU3E__6_3; }
	inline bool* get_address_of_U3CbGetAllCGU3E__6_3() { return &___U3CbGetAllCGU3E__6_3; }
	inline void set_U3CbGetAllCGU3E__6_3(bool value)
	{
		___U3CbGetAllCGU3E__6_3 = value;
	}

	inline static int32_t get_offset_of_U3CuuidU3E__7_4() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321, ___U3CuuidU3E__7_4)); }
	inline String_t* get_U3CuuidU3E__7_4() const { return ___U3CuuidU3E__7_4; }
	inline String_t** get_address_of_U3CuuidU3E__7_4() { return &___U3CuuidU3E__7_4; }
	inline void set_U3CuuidU3E__7_4(String_t* value)
	{
		___U3CuuidU3E__7_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CuuidU3E__7_4, value);
	}

	inline static int32_t get_offset_of_U3CstrLogU3E__8_5() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321, ___U3CstrLogU3E__8_5)); }
	inline String_t* get_U3CstrLogU3E__8_5() const { return ___U3CstrLogU3E__8_5; }
	inline String_t** get_address_of_U3CstrLogU3E__8_5() { return &___U3CstrLogU3E__8_5; }
	inline void set_U3CstrLogU3E__8_5(String_t* value)
	{
		___U3CstrLogU3E__8_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstrLogU3E__8_5, value);
	}

	inline static int32_t get_offset_of_U3CbRemoveChU3E__D_6() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321, ___U3CbRemoveChU3E__D_6)); }
	inline bool get_U3CbRemoveChU3E__D_6() const { return ___U3CbRemoveChU3E__D_6; }
	inline bool* get_address_of_U3CbRemoveChU3E__D_6() { return &___U3CbRemoveChU3E__D_6; }
	inline void set_U3CbRemoveChU3E__D_6(bool value)
	{
		___U3CbRemoveChU3E__D_6 = value;
	}

	inline static int32_t get_offset_of_U3CstrLog2U3E__E_7() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321, ___U3CstrLog2U3E__E_7)); }
	inline String_t* get_U3CstrLog2U3E__E_7() const { return ___U3CstrLog2U3E__E_7; }
	inline String_t** get_address_of_U3CstrLog2U3E__E_7() { return &___U3CstrLog2U3E__E_7; }
	inline void set_U3CstrLog2U3E__E_7(String_t* value)
	{
		___U3CstrLog2U3E__E_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CstrLog2U3E__E_7, value);
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321, ___U24this_8)); }
	inline TestSubscribeWithTimetoken_t2144859056 * get_U24this_8() const { return ___U24this_8; }
	inline TestSubscribeWithTimetoken_t2144859056 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(TestSubscribeWithTimetoken_t2144859056 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_8, value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321, ___U24current_9)); }
	inline Il2CppObject * get_U24current_9() const { return ___U24current_9; }
	inline Il2CppObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(Il2CppObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_9, value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_12() { return static_cast<int32_t>(offsetof(U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321, ___U24locvar0_12)); }
	inline U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760 * get_U24locvar0_12() const { return ___U24locvar0_12; }
	inline U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760 ** get_address_of_U24locvar0_12() { return &___U24locvar0_12; }
	inline void set_U24locvar0_12(U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760 * value)
	{
		___U24locvar0_12 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
