﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestTimeResponse
struct TestTimeResponse_t2677318202;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestTimeResponse::.ctor()
extern "C"  void TestTimeResponse__ctor_m1983831112 (TestTimeResponse_t2677318202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestTimeResponse::Start()
extern "C"  Il2CppObject * TestTimeResponse_Start_m2355425372 (TestTimeResponse_t2677318202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
