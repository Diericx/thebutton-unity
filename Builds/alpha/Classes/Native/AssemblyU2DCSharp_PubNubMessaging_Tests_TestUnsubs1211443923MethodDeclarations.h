﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestUnsubscribe
struct TestUnsubscribe_t1211443923;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestUnsubscribe::.ctor()
extern "C"  void TestUnsubscribe__ctor_m2668825423 (TestUnsubscribe_t1211443923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestUnsubscribe::Start()
extern "C"  Il2CppObject * TestUnsubscribe_Start_m1000973717 (TestUnsubscribe_t1211443923 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
