﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestUnsubscribeWildcard
struct TestUnsubscribeWildcard_t2019897593;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// PubNubMessaging.Core.PubnubClientError
struct PubnubClientError_t110317921;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubClient110317921.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void PubNubMessaging.Tests.TestUnsubscribeWildcard::.ctor()
extern "C"  void TestUnsubscribeWildcard__ctor_m1560587733 (TestUnsubscribeWildcard_t2019897593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestUnsubscribeWildcard::Start()
extern "C"  Il2CppObject * TestUnsubscribeWildcard_Start_m138128211 (TestUnsubscribeWildcard_t2019897593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestUnsubscribeWildcard::DoTestUnsubscribeWildcard(System.String)
extern "C"  Il2CppObject * TestUnsubscribeWildcard_DoTestUnsubscribeWildcard_m506640925 (TestUnsubscribeWildcard_t2019897593 * __this, String_t* ___testName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestUnsubscribeWildcard::DisplayErrorMessage(PubNubMessaging.Core.PubnubClientError)
extern "C"  void TestUnsubscribeWildcard_DisplayErrorMessage_m2990673970 (TestUnsubscribeWildcard_t2019897593 * __this, PubnubClientError_t110317921 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestUnsubscribeWildcard::DisplayReturnMessageDummy(System.Object)
extern "C"  void TestUnsubscribeWildcard_DisplayReturnMessageDummy_m1179840924 (TestUnsubscribeWildcard_t2019897593 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
