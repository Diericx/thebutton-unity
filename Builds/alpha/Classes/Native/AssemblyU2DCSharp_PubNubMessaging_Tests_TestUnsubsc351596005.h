﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1
struct U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1/<DoTestUnsubscribeWildcard>c__AnonStorey2
struct  U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005  : public Il2CppObject
{
public:
	// System.String PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1/<DoTestUnsubscribeWildcard>c__AnonStorey2::testName
	String_t* ___testName_0;
	// System.Boolean PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1/<DoTestUnsubscribeWildcard>c__AnonStorey2::bSubConnect
	bool ___bSubConnect_1;
	// System.String PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1/<DoTestUnsubscribeWildcard>c__AnonStorey2::uuid
	String_t* ___uuid_2;
	// System.String PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1/<DoTestUnsubscribeWildcard>c__AnonStorey2::chToSub
	String_t* ___chToSub_3;
	// System.Boolean PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1/<DoTestUnsubscribeWildcard>c__AnonStorey2::bSubWC
	bool ___bSubWC_4;
	// System.Boolean PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1/<DoTestUnsubscribeWildcard>c__AnonStorey2::bUnsub
	bool ___bUnsub_5;
	// PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1 PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1/<DoTestUnsubscribeWildcard>c__AnonStorey2::<>f__ref$1
	U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701 * ___U3CU3Ef__refU241_6;

public:
	inline static int32_t get_offset_of_testName_0() { return static_cast<int32_t>(offsetof(U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005, ___testName_0)); }
	inline String_t* get_testName_0() const { return ___testName_0; }
	inline String_t** get_address_of_testName_0() { return &___testName_0; }
	inline void set_testName_0(String_t* value)
	{
		___testName_0 = value;
		Il2CppCodeGenWriteBarrier(&___testName_0, value);
	}

	inline static int32_t get_offset_of_bSubConnect_1() { return static_cast<int32_t>(offsetof(U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005, ___bSubConnect_1)); }
	inline bool get_bSubConnect_1() const { return ___bSubConnect_1; }
	inline bool* get_address_of_bSubConnect_1() { return &___bSubConnect_1; }
	inline void set_bSubConnect_1(bool value)
	{
		___bSubConnect_1 = value;
	}

	inline static int32_t get_offset_of_uuid_2() { return static_cast<int32_t>(offsetof(U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005, ___uuid_2)); }
	inline String_t* get_uuid_2() const { return ___uuid_2; }
	inline String_t** get_address_of_uuid_2() { return &___uuid_2; }
	inline void set_uuid_2(String_t* value)
	{
		___uuid_2 = value;
		Il2CppCodeGenWriteBarrier(&___uuid_2, value);
	}

	inline static int32_t get_offset_of_chToSub_3() { return static_cast<int32_t>(offsetof(U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005, ___chToSub_3)); }
	inline String_t* get_chToSub_3() const { return ___chToSub_3; }
	inline String_t** get_address_of_chToSub_3() { return &___chToSub_3; }
	inline void set_chToSub_3(String_t* value)
	{
		___chToSub_3 = value;
		Il2CppCodeGenWriteBarrier(&___chToSub_3, value);
	}

	inline static int32_t get_offset_of_bSubWC_4() { return static_cast<int32_t>(offsetof(U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005, ___bSubWC_4)); }
	inline bool get_bSubWC_4() const { return ___bSubWC_4; }
	inline bool* get_address_of_bSubWC_4() { return &___bSubWC_4; }
	inline void set_bSubWC_4(bool value)
	{
		___bSubWC_4 = value;
	}

	inline static int32_t get_offset_of_bUnsub_5() { return static_cast<int32_t>(offsetof(U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005, ___bUnsub_5)); }
	inline bool get_bUnsub_5() const { return ___bUnsub_5; }
	inline bool* get_address_of_bUnsub_5() { return &___bUnsub_5; }
	inline void set_bUnsub_5(bool value)
	{
		___bUnsub_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_6() { return static_cast<int32_t>(offsetof(U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005, ___U3CU3Ef__refU241_6)); }
	inline U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701 * get_U3CU3Ef__refU241_6() const { return ___U3CU3Ef__refU241_6; }
	inline U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701 ** get_address_of_U3CU3Ef__refU241_6() { return &___U3CU3Ef__refU241_6; }
	inline void set_U3CU3Ef__refU241_6(U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701 * value)
	{
		___U3CU3Ef__refU241_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
