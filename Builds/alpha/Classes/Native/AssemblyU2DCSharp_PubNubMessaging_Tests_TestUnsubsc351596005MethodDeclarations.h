﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1/<DoTestUnsubscribeWildcard>c__AnonStorey2
struct U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1/<DoTestUnsubscribeWildcard>c__AnonStorey2::.ctor()
extern "C"  void U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2__ctor_m1436007502 (U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1/<DoTestUnsubscribeWildcard>c__AnonStorey2::<>m__0(System.String)
extern "C"  void U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_U3CU3Em__0_m2956551973 (U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1/<DoTestUnsubscribeWildcard>c__AnonStorey2::<>m__1(System.String)
extern "C"  void U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_U3CU3Em__1_m42320708 (U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005 * __this, String_t* ___retConnect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1/<DoTestUnsubscribeWildcard>c__AnonStorey2::<>m__2(System.String)
extern "C"  void U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_U3CU3Em__2_m2386313059 (U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1/<DoTestUnsubscribeWildcard>c__AnonStorey2::<>m__3(System.String)
extern "C"  void U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_U3CU3Em__3_m3767049090 (U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005 * __this, String_t* ___retM0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
