﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Random
struct Random_t1044426839;
// System.String
struct String_t;
// PubNubMessaging.Tests.TestUnsubscribeWildcard
struct TestUnsubscribeWildcard_t2019897593;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1/<DoTestUnsubscribeWildcard>c__AnonStorey2
struct U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1
struct  U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701  : public Il2CppObject
{
public:
	// System.Random PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1::<r>__0
	Random_t1044426839 * ___U3CrU3E__0_0;
	// System.String PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1::<ch>__1
	String_t* ___U3CchU3E__1_1;
	// System.String PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1::<channel>__2
	String_t* ___U3CchannelU3E__2_2;
	// System.String PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1::testName
	String_t* ___testName_3;
	// System.String PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1::<pubMessage>__6
	String_t* ___U3CpubMessageU3E__6_4;
	// PubNubMessaging.Tests.TestUnsubscribeWildcard PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1::$this
	TestUnsubscribeWildcard_t2019897593 * ___U24this_5;
	// System.Object PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1::$PC
	int32_t ___U24PC_8;
	// PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1/<DoTestUnsubscribeWildcard>c__AnonStorey2 PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1::$locvar0
	U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005 * ___U24locvar0_9;

public:
	inline static int32_t get_offset_of_U3CrU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701, ___U3CrU3E__0_0)); }
	inline Random_t1044426839 * get_U3CrU3E__0_0() const { return ___U3CrU3E__0_0; }
	inline Random_t1044426839 ** get_address_of_U3CrU3E__0_0() { return &___U3CrU3E__0_0; }
	inline void set_U3CrU3E__0_0(Random_t1044426839 * value)
	{
		___U3CrU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CchU3E__1_1() { return static_cast<int32_t>(offsetof(U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701, ___U3CchU3E__1_1)); }
	inline String_t* get_U3CchU3E__1_1() const { return ___U3CchU3E__1_1; }
	inline String_t** get_address_of_U3CchU3E__1_1() { return &___U3CchU3E__1_1; }
	inline void set_U3CchU3E__1_1(String_t* value)
	{
		___U3CchU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CchannelU3E__2_2() { return static_cast<int32_t>(offsetof(U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701, ___U3CchannelU3E__2_2)); }
	inline String_t* get_U3CchannelU3E__2_2() const { return ___U3CchannelU3E__2_2; }
	inline String_t** get_address_of_U3CchannelU3E__2_2() { return &___U3CchannelU3E__2_2; }
	inline void set_U3CchannelU3E__2_2(String_t* value)
	{
		___U3CchannelU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchannelU3E__2_2, value);
	}

	inline static int32_t get_offset_of_testName_3() { return static_cast<int32_t>(offsetof(U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701, ___testName_3)); }
	inline String_t* get_testName_3() const { return ___testName_3; }
	inline String_t** get_address_of_testName_3() { return &___testName_3; }
	inline void set_testName_3(String_t* value)
	{
		___testName_3 = value;
		Il2CppCodeGenWriteBarrier(&___testName_3, value);
	}

	inline static int32_t get_offset_of_U3CpubMessageU3E__6_4() { return static_cast<int32_t>(offsetof(U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701, ___U3CpubMessageU3E__6_4)); }
	inline String_t* get_U3CpubMessageU3E__6_4() const { return ___U3CpubMessageU3E__6_4; }
	inline String_t** get_address_of_U3CpubMessageU3E__6_4() { return &___U3CpubMessageU3E__6_4; }
	inline void set_U3CpubMessageU3E__6_4(String_t* value)
	{
		___U3CpubMessageU3E__6_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpubMessageU3E__6_4, value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701, ___U24this_5)); }
	inline TestUnsubscribeWildcard_t2019897593 * get_U24this_5() const { return ___U24this_5; }
	inline TestUnsubscribeWildcard_t2019897593 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(TestUnsubscribeWildcard_t2019897593 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_9() { return static_cast<int32_t>(offsetof(U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701, ___U24locvar0_9)); }
	inline U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005 * get_U24locvar0_9() const { return ___U24locvar0_9; }
	inline U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005 ** get_address_of_U24locvar0_9() { return &___U24locvar0_9; }
	inline void set_U24locvar0_9(U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005 * value)
	{
		___U24locvar0_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
