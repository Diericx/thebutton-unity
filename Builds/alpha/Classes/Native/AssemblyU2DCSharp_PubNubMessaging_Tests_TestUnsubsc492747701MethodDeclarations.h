﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1
struct U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1::.ctor()
extern "C"  void U3CDoTestUnsubscribeWildcardU3Ec__Iterator1__ctor_m3917557078 (U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1::MoveNext()
extern "C"  bool U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_MoveNext_m374504830 (U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3005834768 (U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2841084408 (U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1::Dispose()
extern "C"  void U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_Dispose_m984300879 (U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubNubMessaging.Tests.TestUnsubscribeWildcard/<DoTestUnsubscribeWildcard>c__Iterator1::Reset()
extern "C"  void U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_Reset_m1379772449 (U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
