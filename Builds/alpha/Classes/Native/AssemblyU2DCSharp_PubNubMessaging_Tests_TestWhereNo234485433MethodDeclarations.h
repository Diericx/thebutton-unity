﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubNubMessaging.Tests.TestWhereNow
struct TestWhereNow_t234485433;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void PubNubMessaging.Tests.TestWhereNow::.ctor()
extern "C"  void TestWhereNow__ctor_m1287359103 (TestWhereNow_t234485433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubNubMessaging.Tests.TestWhereNow::Start()
extern "C"  Il2CppObject * TestWhereNow_Start_m953604721 (TestWhereNow_t234485433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
