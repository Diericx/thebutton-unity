﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// PubNubMessaging.Core.Pubnub
struct Pubnub_t2451529532;
// System.Collections.Generic.Queue`1<System.String>
struct Queue_1_t1848877068;
// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2825504181;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_PubnubExample_PubnubState2523872396.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubnubExample
struct  PubnubExample_t3094214262  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean PubnubExample::ssl
	bool ___ssl_2;
	// System.Boolean PubnubExample::resumeOnReconnect
	bool ___resumeOnReconnect_3;
	// System.String PubnubExample::cipherKey
	String_t* ___cipherKey_4;
	// System.String PubnubExample::secretKey
	String_t* ___secretKey_5;
	// System.String PubnubExample::publishKey
	String_t* ___publishKey_6;
	// System.String PubnubExample::subscribeKey
	String_t* ___subscribeKey_7;
	// System.String PubnubExample::uuid
	String_t* ___uuid_8;
	// System.String PubnubExample::subscribeTimeoutInSeconds
	String_t* ___subscribeTimeoutInSeconds_9;
	// System.String PubnubExample::operationTimeoutInSeconds
	String_t* ___operationTimeoutInSeconds_10;
	// System.String PubnubExample::networkMaxRetries
	String_t* ___networkMaxRetries_11;
	// System.String PubnubExample::networkRetryIntervalInSeconds
	String_t* ___networkRetryIntervalInSeconds_12;
	// System.String PubnubExample::heartbeatIntervalInSeconds
	String_t* ___heartbeatIntervalInSeconds_13;
	// System.String PubnubExample::channel
	String_t* ___channel_15;
	// System.String PubnubExample::publishedMessage
	String_t* ___publishedMessage_16;
	// System.String PubnubExample::publishedMetadataKey
	String_t* ___publishedMetadataKey_17;
	// System.String PubnubExample::publishedMetadataValue
	String_t* ___publishedMetadataValue_18;
	// System.String PubnubExample::pubChannel
	String_t* ___pubChannel_19;
	// System.String PubnubExample::input
	String_t* ___input_20;
	// PubnubExample/PubnubState PubnubExample::state
	int32_t ___state_22;
	// UnityEngine.Vector2 PubnubExample::scrollPosition
	Vector2_t2243707579  ___scrollPosition_24;
	// System.String PubnubExample::pubnubApiResult
	String_t* ___pubnubApiResult_25;
	// System.Boolean PubnubExample::requestInProcess
	bool ___requestInProcess_26;
	// System.Boolean PubnubExample::showPublishPopupWindow
	bool ___showPublishPopupWindow_27;
	// System.Boolean PubnubExample::showGrantWindow
	bool ___showGrantWindow_28;
	// System.Boolean PubnubExample::showAuthWindow
	bool ___showAuthWindow_29;
	// System.Boolean PubnubExample::showTextWindow
	bool ___showTextWindow_30;
	// System.Boolean PubnubExample::showActionsPopupWindow
	bool ___showActionsPopupWindow_31;
	// System.Boolean PubnubExample::showCGPopupWindow
	bool ___showCGPopupWindow_32;
	// System.Boolean PubnubExample::showPamPopupWindow
	bool ___showPamPopupWindow_33;
	// System.Boolean PubnubExample::toggle1
	bool ___toggle1_34;
	// System.Boolean PubnubExample::toggle2
	bool ___toggle2_35;
	// System.String PubnubExample::valueToSet
	String_t* ___valueToSet_36;
	// System.String PubnubExample::valueToSetSubs
	String_t* ___valueToSetSubs_37;
	// System.String PubnubExample::valueToSetAuthKey
	String_t* ___valueToSetAuthKey_38;
	// System.String PubnubExample::text1
	String_t* ___text1_39;
	// System.String PubnubExample::text2
	String_t* ___text2_40;
	// System.String PubnubExample::text3
	String_t* ___text3_41;
	// System.String PubnubExample::text4
	String_t* ___text4_42;
	// System.Boolean PubnubExample::storeInHistory
	bool ___storeInHistory_43;
	// UnityEngine.Rect PubnubExample::publishWindowRect
	Rect_t3681755626  ___publishWindowRect_44;
	// UnityEngine.Rect PubnubExample::authWindowRect
	Rect_t3681755626  ___authWindowRect_45;
	// UnityEngine.Rect PubnubExample::textWindowRect
	Rect_t3681755626  ___textWindowRect_46;
	// UnityEngine.Rect PubnubExample::textWindowRect2
	Rect_t3681755626  ___textWindowRect2_47;
	// System.Boolean PubnubExample::allowUserSettingsChange
	bool ___allowUserSettingsChange_48;
	// System.Single PubnubExample::fLeft
	float ___fLeft_49;
	// System.Single PubnubExample::fLeftInit
	float ___fLeftInit_50;
	// System.Single PubnubExample::fTop
	float ___fTop_51;
	// System.Single PubnubExample::fTopInit
	float ___fTopInit_52;
	// System.Single PubnubExample::fRowHeight
	float ___fRowHeight_53;
	// System.Single PubnubExample::fHeight
	float ___fHeight_54;
	// System.Single PubnubExample::fButtonHeight
	float ___fButtonHeight_55;
	// System.Single PubnubExample::fButtonWidth
	float ___fButtonWidth_56;
	// System.String PubnubExample::currentRTT
	String_t* ___currentRTT_57;

public:
	inline static int32_t get_offset_of_ssl_2() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___ssl_2)); }
	inline bool get_ssl_2() const { return ___ssl_2; }
	inline bool* get_address_of_ssl_2() { return &___ssl_2; }
	inline void set_ssl_2(bool value)
	{
		___ssl_2 = value;
	}

	inline static int32_t get_offset_of_resumeOnReconnect_3() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___resumeOnReconnect_3)); }
	inline bool get_resumeOnReconnect_3() const { return ___resumeOnReconnect_3; }
	inline bool* get_address_of_resumeOnReconnect_3() { return &___resumeOnReconnect_3; }
	inline void set_resumeOnReconnect_3(bool value)
	{
		___resumeOnReconnect_3 = value;
	}

	inline static int32_t get_offset_of_cipherKey_4() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___cipherKey_4)); }
	inline String_t* get_cipherKey_4() const { return ___cipherKey_4; }
	inline String_t** get_address_of_cipherKey_4() { return &___cipherKey_4; }
	inline void set_cipherKey_4(String_t* value)
	{
		___cipherKey_4 = value;
		Il2CppCodeGenWriteBarrier(&___cipherKey_4, value);
	}

	inline static int32_t get_offset_of_secretKey_5() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___secretKey_5)); }
	inline String_t* get_secretKey_5() const { return ___secretKey_5; }
	inline String_t** get_address_of_secretKey_5() { return &___secretKey_5; }
	inline void set_secretKey_5(String_t* value)
	{
		___secretKey_5 = value;
		Il2CppCodeGenWriteBarrier(&___secretKey_5, value);
	}

	inline static int32_t get_offset_of_publishKey_6() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___publishKey_6)); }
	inline String_t* get_publishKey_6() const { return ___publishKey_6; }
	inline String_t** get_address_of_publishKey_6() { return &___publishKey_6; }
	inline void set_publishKey_6(String_t* value)
	{
		___publishKey_6 = value;
		Il2CppCodeGenWriteBarrier(&___publishKey_6, value);
	}

	inline static int32_t get_offset_of_subscribeKey_7() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___subscribeKey_7)); }
	inline String_t* get_subscribeKey_7() const { return ___subscribeKey_7; }
	inline String_t** get_address_of_subscribeKey_7() { return &___subscribeKey_7; }
	inline void set_subscribeKey_7(String_t* value)
	{
		___subscribeKey_7 = value;
		Il2CppCodeGenWriteBarrier(&___subscribeKey_7, value);
	}

	inline static int32_t get_offset_of_uuid_8() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___uuid_8)); }
	inline String_t* get_uuid_8() const { return ___uuid_8; }
	inline String_t** get_address_of_uuid_8() { return &___uuid_8; }
	inline void set_uuid_8(String_t* value)
	{
		___uuid_8 = value;
		Il2CppCodeGenWriteBarrier(&___uuid_8, value);
	}

	inline static int32_t get_offset_of_subscribeTimeoutInSeconds_9() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___subscribeTimeoutInSeconds_9)); }
	inline String_t* get_subscribeTimeoutInSeconds_9() const { return ___subscribeTimeoutInSeconds_9; }
	inline String_t** get_address_of_subscribeTimeoutInSeconds_9() { return &___subscribeTimeoutInSeconds_9; }
	inline void set_subscribeTimeoutInSeconds_9(String_t* value)
	{
		___subscribeTimeoutInSeconds_9 = value;
		Il2CppCodeGenWriteBarrier(&___subscribeTimeoutInSeconds_9, value);
	}

	inline static int32_t get_offset_of_operationTimeoutInSeconds_10() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___operationTimeoutInSeconds_10)); }
	inline String_t* get_operationTimeoutInSeconds_10() const { return ___operationTimeoutInSeconds_10; }
	inline String_t** get_address_of_operationTimeoutInSeconds_10() { return &___operationTimeoutInSeconds_10; }
	inline void set_operationTimeoutInSeconds_10(String_t* value)
	{
		___operationTimeoutInSeconds_10 = value;
		Il2CppCodeGenWriteBarrier(&___operationTimeoutInSeconds_10, value);
	}

	inline static int32_t get_offset_of_networkMaxRetries_11() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___networkMaxRetries_11)); }
	inline String_t* get_networkMaxRetries_11() const { return ___networkMaxRetries_11; }
	inline String_t** get_address_of_networkMaxRetries_11() { return &___networkMaxRetries_11; }
	inline void set_networkMaxRetries_11(String_t* value)
	{
		___networkMaxRetries_11 = value;
		Il2CppCodeGenWriteBarrier(&___networkMaxRetries_11, value);
	}

	inline static int32_t get_offset_of_networkRetryIntervalInSeconds_12() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___networkRetryIntervalInSeconds_12)); }
	inline String_t* get_networkRetryIntervalInSeconds_12() const { return ___networkRetryIntervalInSeconds_12; }
	inline String_t** get_address_of_networkRetryIntervalInSeconds_12() { return &___networkRetryIntervalInSeconds_12; }
	inline void set_networkRetryIntervalInSeconds_12(String_t* value)
	{
		___networkRetryIntervalInSeconds_12 = value;
		Il2CppCodeGenWriteBarrier(&___networkRetryIntervalInSeconds_12, value);
	}

	inline static int32_t get_offset_of_heartbeatIntervalInSeconds_13() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___heartbeatIntervalInSeconds_13)); }
	inline String_t* get_heartbeatIntervalInSeconds_13() const { return ___heartbeatIntervalInSeconds_13; }
	inline String_t** get_address_of_heartbeatIntervalInSeconds_13() { return &___heartbeatIntervalInSeconds_13; }
	inline void set_heartbeatIntervalInSeconds_13(String_t* value)
	{
		___heartbeatIntervalInSeconds_13 = value;
		Il2CppCodeGenWriteBarrier(&___heartbeatIntervalInSeconds_13, value);
	}

	inline static int32_t get_offset_of_channel_15() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___channel_15)); }
	inline String_t* get_channel_15() const { return ___channel_15; }
	inline String_t** get_address_of_channel_15() { return &___channel_15; }
	inline void set_channel_15(String_t* value)
	{
		___channel_15 = value;
		Il2CppCodeGenWriteBarrier(&___channel_15, value);
	}

	inline static int32_t get_offset_of_publishedMessage_16() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___publishedMessage_16)); }
	inline String_t* get_publishedMessage_16() const { return ___publishedMessage_16; }
	inline String_t** get_address_of_publishedMessage_16() { return &___publishedMessage_16; }
	inline void set_publishedMessage_16(String_t* value)
	{
		___publishedMessage_16 = value;
		Il2CppCodeGenWriteBarrier(&___publishedMessage_16, value);
	}

	inline static int32_t get_offset_of_publishedMetadataKey_17() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___publishedMetadataKey_17)); }
	inline String_t* get_publishedMetadataKey_17() const { return ___publishedMetadataKey_17; }
	inline String_t** get_address_of_publishedMetadataKey_17() { return &___publishedMetadataKey_17; }
	inline void set_publishedMetadataKey_17(String_t* value)
	{
		___publishedMetadataKey_17 = value;
		Il2CppCodeGenWriteBarrier(&___publishedMetadataKey_17, value);
	}

	inline static int32_t get_offset_of_publishedMetadataValue_18() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___publishedMetadataValue_18)); }
	inline String_t* get_publishedMetadataValue_18() const { return ___publishedMetadataValue_18; }
	inline String_t** get_address_of_publishedMetadataValue_18() { return &___publishedMetadataValue_18; }
	inline void set_publishedMetadataValue_18(String_t* value)
	{
		___publishedMetadataValue_18 = value;
		Il2CppCodeGenWriteBarrier(&___publishedMetadataValue_18, value);
	}

	inline static int32_t get_offset_of_pubChannel_19() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___pubChannel_19)); }
	inline String_t* get_pubChannel_19() const { return ___pubChannel_19; }
	inline String_t** get_address_of_pubChannel_19() { return &___pubChannel_19; }
	inline void set_pubChannel_19(String_t* value)
	{
		___pubChannel_19 = value;
		Il2CppCodeGenWriteBarrier(&___pubChannel_19, value);
	}

	inline static int32_t get_offset_of_input_20() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___input_20)); }
	inline String_t* get_input_20() const { return ___input_20; }
	inline String_t** get_address_of_input_20() { return &___input_20; }
	inline void set_input_20(String_t* value)
	{
		___input_20 = value;
		Il2CppCodeGenWriteBarrier(&___input_20, value);
	}

	inline static int32_t get_offset_of_state_22() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___state_22)); }
	inline int32_t get_state_22() const { return ___state_22; }
	inline int32_t* get_address_of_state_22() { return &___state_22; }
	inline void set_state_22(int32_t value)
	{
		___state_22 = value;
	}

	inline static int32_t get_offset_of_scrollPosition_24() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___scrollPosition_24)); }
	inline Vector2_t2243707579  get_scrollPosition_24() const { return ___scrollPosition_24; }
	inline Vector2_t2243707579 * get_address_of_scrollPosition_24() { return &___scrollPosition_24; }
	inline void set_scrollPosition_24(Vector2_t2243707579  value)
	{
		___scrollPosition_24 = value;
	}

	inline static int32_t get_offset_of_pubnubApiResult_25() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___pubnubApiResult_25)); }
	inline String_t* get_pubnubApiResult_25() const { return ___pubnubApiResult_25; }
	inline String_t** get_address_of_pubnubApiResult_25() { return &___pubnubApiResult_25; }
	inline void set_pubnubApiResult_25(String_t* value)
	{
		___pubnubApiResult_25 = value;
		Il2CppCodeGenWriteBarrier(&___pubnubApiResult_25, value);
	}

	inline static int32_t get_offset_of_requestInProcess_26() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___requestInProcess_26)); }
	inline bool get_requestInProcess_26() const { return ___requestInProcess_26; }
	inline bool* get_address_of_requestInProcess_26() { return &___requestInProcess_26; }
	inline void set_requestInProcess_26(bool value)
	{
		___requestInProcess_26 = value;
	}

	inline static int32_t get_offset_of_showPublishPopupWindow_27() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___showPublishPopupWindow_27)); }
	inline bool get_showPublishPopupWindow_27() const { return ___showPublishPopupWindow_27; }
	inline bool* get_address_of_showPublishPopupWindow_27() { return &___showPublishPopupWindow_27; }
	inline void set_showPublishPopupWindow_27(bool value)
	{
		___showPublishPopupWindow_27 = value;
	}

	inline static int32_t get_offset_of_showGrantWindow_28() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___showGrantWindow_28)); }
	inline bool get_showGrantWindow_28() const { return ___showGrantWindow_28; }
	inline bool* get_address_of_showGrantWindow_28() { return &___showGrantWindow_28; }
	inline void set_showGrantWindow_28(bool value)
	{
		___showGrantWindow_28 = value;
	}

	inline static int32_t get_offset_of_showAuthWindow_29() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___showAuthWindow_29)); }
	inline bool get_showAuthWindow_29() const { return ___showAuthWindow_29; }
	inline bool* get_address_of_showAuthWindow_29() { return &___showAuthWindow_29; }
	inline void set_showAuthWindow_29(bool value)
	{
		___showAuthWindow_29 = value;
	}

	inline static int32_t get_offset_of_showTextWindow_30() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___showTextWindow_30)); }
	inline bool get_showTextWindow_30() const { return ___showTextWindow_30; }
	inline bool* get_address_of_showTextWindow_30() { return &___showTextWindow_30; }
	inline void set_showTextWindow_30(bool value)
	{
		___showTextWindow_30 = value;
	}

	inline static int32_t get_offset_of_showActionsPopupWindow_31() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___showActionsPopupWindow_31)); }
	inline bool get_showActionsPopupWindow_31() const { return ___showActionsPopupWindow_31; }
	inline bool* get_address_of_showActionsPopupWindow_31() { return &___showActionsPopupWindow_31; }
	inline void set_showActionsPopupWindow_31(bool value)
	{
		___showActionsPopupWindow_31 = value;
	}

	inline static int32_t get_offset_of_showCGPopupWindow_32() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___showCGPopupWindow_32)); }
	inline bool get_showCGPopupWindow_32() const { return ___showCGPopupWindow_32; }
	inline bool* get_address_of_showCGPopupWindow_32() { return &___showCGPopupWindow_32; }
	inline void set_showCGPopupWindow_32(bool value)
	{
		___showCGPopupWindow_32 = value;
	}

	inline static int32_t get_offset_of_showPamPopupWindow_33() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___showPamPopupWindow_33)); }
	inline bool get_showPamPopupWindow_33() const { return ___showPamPopupWindow_33; }
	inline bool* get_address_of_showPamPopupWindow_33() { return &___showPamPopupWindow_33; }
	inline void set_showPamPopupWindow_33(bool value)
	{
		___showPamPopupWindow_33 = value;
	}

	inline static int32_t get_offset_of_toggle1_34() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___toggle1_34)); }
	inline bool get_toggle1_34() const { return ___toggle1_34; }
	inline bool* get_address_of_toggle1_34() { return &___toggle1_34; }
	inline void set_toggle1_34(bool value)
	{
		___toggle1_34 = value;
	}

	inline static int32_t get_offset_of_toggle2_35() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___toggle2_35)); }
	inline bool get_toggle2_35() const { return ___toggle2_35; }
	inline bool* get_address_of_toggle2_35() { return &___toggle2_35; }
	inline void set_toggle2_35(bool value)
	{
		___toggle2_35 = value;
	}

	inline static int32_t get_offset_of_valueToSet_36() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___valueToSet_36)); }
	inline String_t* get_valueToSet_36() const { return ___valueToSet_36; }
	inline String_t** get_address_of_valueToSet_36() { return &___valueToSet_36; }
	inline void set_valueToSet_36(String_t* value)
	{
		___valueToSet_36 = value;
		Il2CppCodeGenWriteBarrier(&___valueToSet_36, value);
	}

	inline static int32_t get_offset_of_valueToSetSubs_37() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___valueToSetSubs_37)); }
	inline String_t* get_valueToSetSubs_37() const { return ___valueToSetSubs_37; }
	inline String_t** get_address_of_valueToSetSubs_37() { return &___valueToSetSubs_37; }
	inline void set_valueToSetSubs_37(String_t* value)
	{
		___valueToSetSubs_37 = value;
		Il2CppCodeGenWriteBarrier(&___valueToSetSubs_37, value);
	}

	inline static int32_t get_offset_of_valueToSetAuthKey_38() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___valueToSetAuthKey_38)); }
	inline String_t* get_valueToSetAuthKey_38() const { return ___valueToSetAuthKey_38; }
	inline String_t** get_address_of_valueToSetAuthKey_38() { return &___valueToSetAuthKey_38; }
	inline void set_valueToSetAuthKey_38(String_t* value)
	{
		___valueToSetAuthKey_38 = value;
		Il2CppCodeGenWriteBarrier(&___valueToSetAuthKey_38, value);
	}

	inline static int32_t get_offset_of_text1_39() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___text1_39)); }
	inline String_t* get_text1_39() const { return ___text1_39; }
	inline String_t** get_address_of_text1_39() { return &___text1_39; }
	inline void set_text1_39(String_t* value)
	{
		___text1_39 = value;
		Il2CppCodeGenWriteBarrier(&___text1_39, value);
	}

	inline static int32_t get_offset_of_text2_40() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___text2_40)); }
	inline String_t* get_text2_40() const { return ___text2_40; }
	inline String_t** get_address_of_text2_40() { return &___text2_40; }
	inline void set_text2_40(String_t* value)
	{
		___text2_40 = value;
		Il2CppCodeGenWriteBarrier(&___text2_40, value);
	}

	inline static int32_t get_offset_of_text3_41() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___text3_41)); }
	inline String_t* get_text3_41() const { return ___text3_41; }
	inline String_t** get_address_of_text3_41() { return &___text3_41; }
	inline void set_text3_41(String_t* value)
	{
		___text3_41 = value;
		Il2CppCodeGenWriteBarrier(&___text3_41, value);
	}

	inline static int32_t get_offset_of_text4_42() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___text4_42)); }
	inline String_t* get_text4_42() const { return ___text4_42; }
	inline String_t** get_address_of_text4_42() { return &___text4_42; }
	inline void set_text4_42(String_t* value)
	{
		___text4_42 = value;
		Il2CppCodeGenWriteBarrier(&___text4_42, value);
	}

	inline static int32_t get_offset_of_storeInHistory_43() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___storeInHistory_43)); }
	inline bool get_storeInHistory_43() const { return ___storeInHistory_43; }
	inline bool* get_address_of_storeInHistory_43() { return &___storeInHistory_43; }
	inline void set_storeInHistory_43(bool value)
	{
		___storeInHistory_43 = value;
	}

	inline static int32_t get_offset_of_publishWindowRect_44() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___publishWindowRect_44)); }
	inline Rect_t3681755626  get_publishWindowRect_44() const { return ___publishWindowRect_44; }
	inline Rect_t3681755626 * get_address_of_publishWindowRect_44() { return &___publishWindowRect_44; }
	inline void set_publishWindowRect_44(Rect_t3681755626  value)
	{
		___publishWindowRect_44 = value;
	}

	inline static int32_t get_offset_of_authWindowRect_45() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___authWindowRect_45)); }
	inline Rect_t3681755626  get_authWindowRect_45() const { return ___authWindowRect_45; }
	inline Rect_t3681755626 * get_address_of_authWindowRect_45() { return &___authWindowRect_45; }
	inline void set_authWindowRect_45(Rect_t3681755626  value)
	{
		___authWindowRect_45 = value;
	}

	inline static int32_t get_offset_of_textWindowRect_46() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___textWindowRect_46)); }
	inline Rect_t3681755626  get_textWindowRect_46() const { return ___textWindowRect_46; }
	inline Rect_t3681755626 * get_address_of_textWindowRect_46() { return &___textWindowRect_46; }
	inline void set_textWindowRect_46(Rect_t3681755626  value)
	{
		___textWindowRect_46 = value;
	}

	inline static int32_t get_offset_of_textWindowRect2_47() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___textWindowRect2_47)); }
	inline Rect_t3681755626  get_textWindowRect2_47() const { return ___textWindowRect2_47; }
	inline Rect_t3681755626 * get_address_of_textWindowRect2_47() { return &___textWindowRect2_47; }
	inline void set_textWindowRect2_47(Rect_t3681755626  value)
	{
		___textWindowRect2_47 = value;
	}

	inline static int32_t get_offset_of_allowUserSettingsChange_48() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___allowUserSettingsChange_48)); }
	inline bool get_allowUserSettingsChange_48() const { return ___allowUserSettingsChange_48; }
	inline bool* get_address_of_allowUserSettingsChange_48() { return &___allowUserSettingsChange_48; }
	inline void set_allowUserSettingsChange_48(bool value)
	{
		___allowUserSettingsChange_48 = value;
	}

	inline static int32_t get_offset_of_fLeft_49() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___fLeft_49)); }
	inline float get_fLeft_49() const { return ___fLeft_49; }
	inline float* get_address_of_fLeft_49() { return &___fLeft_49; }
	inline void set_fLeft_49(float value)
	{
		___fLeft_49 = value;
	}

	inline static int32_t get_offset_of_fLeftInit_50() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___fLeftInit_50)); }
	inline float get_fLeftInit_50() const { return ___fLeftInit_50; }
	inline float* get_address_of_fLeftInit_50() { return &___fLeftInit_50; }
	inline void set_fLeftInit_50(float value)
	{
		___fLeftInit_50 = value;
	}

	inline static int32_t get_offset_of_fTop_51() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___fTop_51)); }
	inline float get_fTop_51() const { return ___fTop_51; }
	inline float* get_address_of_fTop_51() { return &___fTop_51; }
	inline void set_fTop_51(float value)
	{
		___fTop_51 = value;
	}

	inline static int32_t get_offset_of_fTopInit_52() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___fTopInit_52)); }
	inline float get_fTopInit_52() const { return ___fTopInit_52; }
	inline float* get_address_of_fTopInit_52() { return &___fTopInit_52; }
	inline void set_fTopInit_52(float value)
	{
		___fTopInit_52 = value;
	}

	inline static int32_t get_offset_of_fRowHeight_53() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___fRowHeight_53)); }
	inline float get_fRowHeight_53() const { return ___fRowHeight_53; }
	inline float* get_address_of_fRowHeight_53() { return &___fRowHeight_53; }
	inline void set_fRowHeight_53(float value)
	{
		___fRowHeight_53 = value;
	}

	inline static int32_t get_offset_of_fHeight_54() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___fHeight_54)); }
	inline float get_fHeight_54() const { return ___fHeight_54; }
	inline float* get_address_of_fHeight_54() { return &___fHeight_54; }
	inline void set_fHeight_54(float value)
	{
		___fHeight_54 = value;
	}

	inline static int32_t get_offset_of_fButtonHeight_55() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___fButtonHeight_55)); }
	inline float get_fButtonHeight_55() const { return ___fButtonHeight_55; }
	inline float* get_address_of_fButtonHeight_55() { return &___fButtonHeight_55; }
	inline void set_fButtonHeight_55(float value)
	{
		___fButtonHeight_55 = value;
	}

	inline static int32_t get_offset_of_fButtonWidth_56() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___fButtonWidth_56)); }
	inline float get_fButtonWidth_56() const { return ___fButtonWidth_56; }
	inline float* get_address_of_fButtonWidth_56() { return &___fButtonWidth_56; }
	inline void set_fButtonWidth_56(float value)
	{
		___fButtonWidth_56 = value;
	}

	inline static int32_t get_offset_of_currentRTT_57() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262, ___currentRTT_57)); }
	inline String_t* get_currentRTT_57() const { return ___currentRTT_57; }
	inline String_t** get_address_of_currentRTT_57() { return &___currentRTT_57; }
	inline void set_currentRTT_57(String_t* value)
	{
		___currentRTT_57 = value;
		Il2CppCodeGenWriteBarrier(&___currentRTT_57, value);
	}
};

struct PubnubExample_t3094214262_StaticFields
{
public:
	// System.Boolean PubnubExample::showErrorMessageSegments
	bool ___showErrorMessageSegments_14;
	// PubNubMessaging.Core.Pubnub PubnubExample::pubnub
	Pubnub_t2451529532 * ___pubnub_21;
	// System.Collections.Generic.Queue`1<System.String> PubnubExample::recordQueue
	Queue_1_t1848877068 * ___recordQueue_23;
	// System.Action`1<System.String> PubnubExample::<>f__am$cache0
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache0_58;
	// System.Func`2<System.Object,System.Object> PubnubExample::<>f__am$cache1
	Func_2_t2825504181 * ___U3CU3Ef__amU24cache1_59;

public:
	inline static int32_t get_offset_of_showErrorMessageSegments_14() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262_StaticFields, ___showErrorMessageSegments_14)); }
	inline bool get_showErrorMessageSegments_14() const { return ___showErrorMessageSegments_14; }
	inline bool* get_address_of_showErrorMessageSegments_14() { return &___showErrorMessageSegments_14; }
	inline void set_showErrorMessageSegments_14(bool value)
	{
		___showErrorMessageSegments_14 = value;
	}

	inline static int32_t get_offset_of_pubnub_21() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262_StaticFields, ___pubnub_21)); }
	inline Pubnub_t2451529532 * get_pubnub_21() const { return ___pubnub_21; }
	inline Pubnub_t2451529532 ** get_address_of_pubnub_21() { return &___pubnub_21; }
	inline void set_pubnub_21(Pubnub_t2451529532 * value)
	{
		___pubnub_21 = value;
		Il2CppCodeGenWriteBarrier(&___pubnub_21, value);
	}

	inline static int32_t get_offset_of_recordQueue_23() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262_StaticFields, ___recordQueue_23)); }
	inline Queue_1_t1848877068 * get_recordQueue_23() const { return ___recordQueue_23; }
	inline Queue_1_t1848877068 ** get_address_of_recordQueue_23() { return &___recordQueue_23; }
	inline void set_recordQueue_23(Queue_1_t1848877068 * value)
	{
		___recordQueue_23 = value;
		Il2CppCodeGenWriteBarrier(&___recordQueue_23, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_58() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262_StaticFields, ___U3CU3Ef__amU24cache0_58)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache0_58() const { return ___U3CU3Ef__amU24cache0_58; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache0_58() { return &___U3CU3Ef__amU24cache0_58; }
	inline void set_U3CU3Ef__amU24cache0_58(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache0_58 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_58, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_59() { return static_cast<int32_t>(offsetof(PubnubExample_t3094214262_StaticFields, ___U3CU3Ef__amU24cache1_59)); }
	inline Func_2_t2825504181 * get_U3CU3Ef__amU24cache1_59() const { return ___U3CU3Ef__amU24cache1_59; }
	inline Func_2_t2825504181 ** get_address_of_U3CU3Ef__amU24cache1_59() { return &___U3CU3Ef__amU24cache1_59; }
	inline void set_U3CU3Ef__amU24cache1_59(Func_2_t2825504181 * value)
	{
		___U3CU3Ef__amU24cache1_59 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_59, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
