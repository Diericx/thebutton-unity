﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubnubExample
struct PubnubExample_t3094214262;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Object
struct Il2CppObject;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t283079845;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t777637347;
// PubNubMessaging.Core.PubnubClientError
struct PubnubClientError_t110317921;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PubnubExample_PubnubState2523872396.h"
#include "UnityEngine_UnityEngine_LogType1559732862.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Security_Cryptography_X509Certifica283079845.h"
#include "System_System_Security_Cryptography_X509Certificate777637347.h"
#include "System_System_Net_Security_SslPolicyErrors1928581431.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubClient110317921.h"

// System.Void PubnubExample::.ctor()
extern "C"  void PubnubExample__ctor_m1380574017 (PubnubExample_t3094214262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::OnDisable()
extern "C"  void PubnubExample_OnDisable_m4253062202 (PubnubExample_t3094214262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::OnGUI()
extern "C"  void PubnubExample_OnGUI_m2228330995 (PubnubExample_t3094214262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::ShowPamActions(System.Single,System.Single,System.Single)
extern "C"  void PubnubExample_ShowPamActions_m2651636470 (PubnubExample_t3094214262 * __this, float ___fLeft0, float ___fTop1, float ___fButtonHeight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::ShowCGActions(System.Single,System.Single,System.Single)
extern "C"  void PubnubExample_ShowCGActions_m2870641676 (PubnubExample_t3094214262 * __this, float ___fLeft0, float ___fTop1, float ___fButtonHeight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::ShowActions(System.Single,System.Single,System.Single)
extern "C"  void PubnubExample_ShowActions_m3063679426 (PubnubExample_t3094214262 * __this, float ___fLeft0, float ___fTop1, float ___fButtonHeight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DoTextWindow(System.Int32)
extern "C"  void PubnubExample_DoTextWindow_m3410025654 (PubnubExample_t3094214262 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DoAuditPresenceWindow(System.Int32)
extern "C"  void PubnubExample_DoAuditPresenceWindow_m3043950443 (PubnubExample_t3094214262 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DoAuditSubscribeWindow(System.Int32)
extern "C"  void PubnubExample_DoAuditSubscribeWindow_m3100043164 (PubnubExample_t3094214262 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DoRevokePresenceWindow(System.Int32)
extern "C"  void PubnubExample_DoRevokePresenceWindow_m1200874962 (PubnubExample_t3094214262 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DoRevokeSubscribeWindow(System.Int32)
extern "C"  void PubnubExample_DoRevokeSubscribeWindow_m1369086199 (PubnubExample_t3094214262 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DoAuthWindow(System.Int32)
extern "C"  void PubnubExample_DoAuthWindow_m4071391575 (PubnubExample_t3094214262 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DoChangeUuidWindow(System.Int32)
extern "C"  void PubnubExample_DoChangeUuidWindow_m1180905758 (PubnubExample_t3094214262 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DoWhereNowWindow(System.Int32)
extern "C"  void PubnubExample_DoWhereNowWindow_m3677648072 (PubnubExample_t3094214262 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DoPresenceHeartbeatWindow(System.Int32)
extern "C"  void PubnubExample_DoPresenceHeartbeatWindow_m3932143792 (PubnubExample_t3094214262 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DoPresenceIntervalWindow(System.Int32)
extern "C"  void PubnubExample_DoPresenceIntervalWindow_m564323537 (PubnubExample_t3094214262 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DoDetailedHistory(System.Int32)
extern "C"  void PubnubExample_DoDetailedHistory_m3366845989 (PubnubExample_t3094214262 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DoRemoveChannelGroup(System.Int32)
extern "C"  void PubnubExample_DoRemoveChannelGroup_m3484164973 (PubnubExample_t3094214262 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DoListAllChannelsOfChannelGroups(System.Int32)
extern "C"  void PubnubExample_DoListAllChannelsOfChannelGroups_m4024182448 (PubnubExample_t3094214262 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DoSetFilterExpression(System.Int32)
extern "C"  void PubnubExample_DoSetFilterExpression_m12982247 (PubnubExample_t3094214262 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::ShowGuiButton(System.String,PubnubExample/PubnubState)
extern "C"  void PubnubExample_ShowGuiButton_m3020354281 (PubnubExample_t3094214262 * __this, String_t* ___buttonTitle0, int32_t ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::ShowWindow(PubnubExample/PubnubState)
extern "C"  void PubnubExample_ShowWindow_m3722047618 (PubnubExample_t3094214262 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DoGrantWindow(System.Int32)
extern "C"  void PubnubExample_DoGrantWindow_m3064834369 (PubnubExample_t3094214262 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DoCGActionWindow(System.Int32)
extern "C"  void PubnubExample_DoCGActionWindow_m3147739245 (PubnubExample_t3094214262 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DoPamActionWindow(System.Int32)
extern "C"  void PubnubExample_DoPamActionWindow_m2493786445 (PubnubExample_t3094214262 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubnubExample::PublishMultiple()
extern "C"  Il2CppObject * PubnubExample_PublishMultiple_m2031364990 (PubnubExample_t3094214262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::RunDetailedHistoryForMultipleChannels(System.String[],System.Int32)
extern "C"  void PubnubExample_RunDetailedHistoryForMultipleChannels_m1431343158 (PubnubExample_t3094214262 * __this, StringU5BU5D_t1642385972* ___chArr0, int32_t ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DoActionWindow(System.Int32)
extern "C"  void PubnubExample_DoActionWindow_m12788337 (PubnubExample_t3094214262 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::AsyncOrNonAsyncCall(PubnubExample/PubnubState)
extern "C"  void PubnubExample_AsyncOrNonAsyncCall_m1591259301 (PubnubExample_t3094214262 * __this, int32_t ___pubnubState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PubnubExample::RunCoroutine()
extern "C"  Il2CppObject * PubnubExample_RunCoroutine_m4095378852 (PubnubExample_t3094214262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::Awake()
extern "C"  void PubnubExample_Awake_m2362299362 (PubnubExample_t3094214262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::CaptureLogs(System.String,System.String,UnityEngine.LogType)
extern "C"  void PubnubExample_CaptureLogs_m3720060769 (PubnubExample_t3094214262 * __this, String_t* ___condition0, String_t* ___stacktrace1, int32_t ___logType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DoAction(System.Object)
extern "C"  void PubnubExample_DoAction_m125293470 (PubnubExample_t3094214262 * __this, Il2CppObject * ___pubnubState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::InstantiatePubnub()
extern "C"  void PubnubExample_InstantiatePubnub_m1095871637 (PubnubExample_t3094214262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DoPublishWindow(System.Int32)
extern "C"  void PubnubExample_DoPublishWindow_m2371771312 (PubnubExample_t3094214262 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::Start()
extern "C"  void PubnubExample_Start_m1800994705 (PubnubExample_t3094214262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubnubExample::ValidateServerCertificate(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern "C"  bool PubnubExample_ValidateServerCertificate_m471270792 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender0, X509Certificate_t283079845 * ___certificate1, X509Chain_t777637347 * ___chain2, int32_t ___sslPolicyErrors3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::Update()
extern "C"  void PubnubExample_Update_m2496883988 (PubnubExample_t3094214262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::OnApplicationQuit()
extern "C"  void PubnubExample_OnApplicationQuit_m1941086391 (PubnubExample_t3094214262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::ResetPubnubInstance()
extern "C"  void PubnubExample_ResetPubnubInstance_m2034012955 (PubnubExample_t3094214262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DisplayReturnMessageHistory(System.String)
extern "C"  void PubnubExample_DisplayReturnMessageHistory_m1308556962 (PubnubExample_t3094214262 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DisplayReturnMessageSubscribe(System.Object)
extern "C"  void PubnubExample_DisplayReturnMessageSubscribe_m2569111684 (PubnubExample_t3094214262 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DisplayReturnMessageSubscribe(System.String)
extern "C"  void PubnubExample_DisplayReturnMessageSubscribe_m2920257834 (PubnubExample_t3094214262 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DisplayConnectMessage(System.String)
extern "C"  void PubnubExample_DisplayConnectMessage_m2356072912 (PubnubExample_t3094214262 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DisplayConnectMessage(System.Object)
extern "C"  void PubnubExample_DisplayConnectMessage_m582793862 (PubnubExample_t3094214262 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DisplayReturnMessage(System.String)
extern "C"  void PubnubExample_DisplayReturnMessage_m1614659564 (PubnubExample_t3094214262 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DisplayReturnMessage(System.Object)
extern "C"  void PubnubExample_DisplayReturnMessage_m633761218 (PubnubExample_t3094214262 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DisplayWildcardReturnMessage(System.String)
extern "C"  void PubnubExample_DisplayWildcardReturnMessage_m2967567642 (PubnubExample_t3094214262 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DisplayWildcardReturnMessage(System.Object)
extern "C"  void PubnubExample_DisplayWildcardReturnMessage_m3972887376 (PubnubExample_t3094214262 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DisplayDisconnectReturnMessage(System.String)
extern "C"  void PubnubExample_DisplayDisconnectReturnMessage_m4029751530 (PubnubExample_t3094214262 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DisplayReturnMessageObj(System.Object)
extern "C"  void PubnubExample_DisplayReturnMessageObj_m3553784619 (PubnubExample_t3094214262 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DisplayConnectStatusMessage(System.String)
extern "C"  void PubnubExample_DisplayConnectStatusMessage_m1348712398 (PubnubExample_t3094214262 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DisplayConnectStatusMessageObj(System.Object)
extern "C"  void PubnubExample_DisplayConnectStatusMessageObj_m1891932219 (PubnubExample_t3094214262 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DisplayDisconnectStatusMessage(System.String)
extern "C"  void PubnubExample_DisplayDisconnectStatusMessage_m660271126 (PubnubExample_t3094214262 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DisplayErrorMessage(System.String)
extern "C"  void PubnubExample_DisplayErrorMessage_m456964112 (PubnubExample_t3094214262 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DisplayErrorMessage(PubNubMessaging.Core.PubnubClientError)
extern "C"  void PubnubExample_DisplayErrorMessage_m645983206 (PubnubExample_t3094214262 * __this, PubnubClientError_t110317921 * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::DisplayErrorMessageSegments(PubNubMessaging.Core.PubnubClientError)
extern "C"  void PubnubExample_DisplayErrorMessageSegments_m782905880 (PubnubExample_t3094214262 * __this, PubnubClientError_t110317921 * ___pubnubError0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::AddToPubnubResultContainer(System.String)
extern "C"  void PubnubExample_AddToPubnubResultContainer_m2145836457 (PubnubExample_t3094214262 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::.cctor()
extern "C"  void PubnubExample__cctor_m1461536160 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::<RunDetailedHistoryForMultipleChannels>m__0(System.String)
extern "C"  void PubnubExample_U3CRunDetailedHistoryForMultipleChannelsU3Em__0_m2009602352 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample::<RunDetailedHistoryForMultipleChannels>m__1(System.String)
extern "C"  void PubnubExample_U3CRunDetailedHistoryForMultipleChannelsU3Em__1_m2616912885 (PubnubExample_t3094214262 * __this, String_t* ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubnubExample::<DisplayReturnMessageHistory>m__2(System.Object)
extern "C"  Il2CppObject * PubnubExample_U3CDisplayReturnMessageHistoryU3Em__2_m675161030 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
