﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PubnubExample
struct PubnubExample_t3094214262;
// System.Object
struct Il2CppObject;
// System.Action`1<System.String>
struct Action_1_t1831019615;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubnubExample/<PublishMultiple>c__Iterator0
struct  U3CPublishMultipleU3Ec__Iterator0_t1336100689  : public Il2CppObject
{
public:
	// System.Int32 PubnubExample/<PublishMultiple>c__Iterator0::<i>__0
	int32_t ___U3CiU3E__0_0;
	// PubnubExample PubnubExample/<PublishMultiple>c__Iterator0::$this
	PubnubExample_t3094214262 * ___U24this_1;
	// System.Object PubnubExample/<PublishMultiple>c__Iterator0::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean PubnubExample/<PublishMultiple>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 PubnubExample/<PublishMultiple>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__0_0() { return static_cast<int32_t>(offsetof(U3CPublishMultipleU3Ec__Iterator0_t1336100689, ___U3CiU3E__0_0)); }
	inline int32_t get_U3CiU3E__0_0() const { return ___U3CiU3E__0_0; }
	inline int32_t* get_address_of_U3CiU3E__0_0() { return &___U3CiU3E__0_0; }
	inline void set_U3CiU3E__0_0(int32_t value)
	{
		___U3CiU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CPublishMultipleU3Ec__Iterator0_t1336100689, ___U24this_1)); }
	inline PubnubExample_t3094214262 * get_U24this_1() const { return ___U24this_1; }
	inline PubnubExample_t3094214262 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(PubnubExample_t3094214262 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CPublishMultipleU3Ec__Iterator0_t1336100689, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CPublishMultipleU3Ec__Iterator0_t1336100689, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CPublishMultipleU3Ec__Iterator0_t1336100689, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

struct U3CPublishMultipleU3Ec__Iterator0_t1336100689_StaticFields
{
public:
	// System.Action`1<System.String> PubnubExample/<PublishMultiple>c__Iterator0::<>f__am$cache0
	Action_1_t1831019615 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(U3CPublishMultipleU3Ec__Iterator0_t1336100689_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Action_1_t1831019615 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Action_1_t1831019615 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Action_1_t1831019615 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
