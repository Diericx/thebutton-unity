﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubnubExample/<PublishMultiple>c__Iterator0
struct U3CPublishMultipleU3Ec__Iterator0_t1336100689;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PubnubExample/<PublishMultiple>c__Iterator0::.ctor()
extern "C"  void U3CPublishMultipleU3Ec__Iterator0__ctor_m3835553668 (U3CPublishMultipleU3Ec__Iterator0_t1336100689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubnubExample/<PublishMultiple>c__Iterator0::MoveNext()
extern "C"  bool U3CPublishMultipleU3Ec__Iterator0_MoveNext_m3453156956 (U3CPublishMultipleU3Ec__Iterator0_t1336100689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubnubExample/<PublishMultiple>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPublishMultipleU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m562939650 (U3CPublishMultipleU3Ec__Iterator0_t1336100689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubnubExample/<PublishMultiple>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPublishMultipleU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m335335530 (U3CPublishMultipleU3Ec__Iterator0_t1336100689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample/<PublishMultiple>c__Iterator0::Dispose()
extern "C"  void U3CPublishMultipleU3Ec__Iterator0_Dispose_m3555840683 (U3CPublishMultipleU3Ec__Iterator0_t1336100689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample/<PublishMultiple>c__Iterator0::Reset()
extern "C"  void U3CPublishMultipleU3Ec__Iterator0_Reset_m917768633 (U3CPublishMultipleU3Ec__Iterator0_t1336100689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample/<PublishMultiple>c__Iterator0::<>m__0(System.String)
extern "C"  void U3CPublishMultipleU3Ec__Iterator0_U3CU3Em__0_m4014701933 (Il2CppObject * __this /* static, unused */, String_t* ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
