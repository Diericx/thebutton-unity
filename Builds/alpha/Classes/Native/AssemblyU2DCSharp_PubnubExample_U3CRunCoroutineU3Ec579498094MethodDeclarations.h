﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubnubExample/<RunCoroutine>c__Iterator1
struct U3CRunCoroutineU3Ec__Iterator1_t579498094;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PubnubExample/<RunCoroutine>c__Iterator1::.ctor()
extern "C"  void U3CRunCoroutineU3Ec__Iterator1__ctor_m3105723695 (U3CRunCoroutineU3Ec__Iterator1_t579498094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PubnubExample/<RunCoroutine>c__Iterator1::MoveNext()
extern "C"  bool U3CRunCoroutineU3Ec__Iterator1_MoveNext_m4160274369 (U3CRunCoroutineU3Ec__Iterator1_t579498094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubnubExample/<RunCoroutine>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRunCoroutineU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2532316093 (U3CRunCoroutineU3Ec__Iterator1_t579498094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PubnubExample/<RunCoroutine>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRunCoroutineU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3122020341 (U3CRunCoroutineU3Ec__Iterator1_t579498094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample/<RunCoroutine>c__Iterator1::Dispose()
extern "C"  void U3CRunCoroutineU3Ec__Iterator1_Dispose_m2683086308 (U3CRunCoroutineU3Ec__Iterator1_t579498094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubExample/<RunCoroutine>c__Iterator1::Reset()
extern "C"  void U3CRunCoroutineU3Ec__Iterator1_Reset_m2925273342 (U3CRunCoroutineU3Ec__Iterator1_t579498094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
