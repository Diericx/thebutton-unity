﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PubNubMessaging.Core.Pubnub
struct Pubnub_t2451529532;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.String
struct String_t;
// Firebase.Auth.FirebaseAuth
struct FirebaseAuth_t3105883899;
// Firebase.Auth.FirebaseUser
struct FirebaseUser_t4046966602;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PubnubHandler
struct  PubnubHandler_t3174799440  : public MonoBehaviour_t1158329972
{
public:
	// PubNubMessaging.Core.Pubnub PubnubHandler::pubnub
	Pubnub_t2451529532 * ___pubnub_2;
	// UnityEngine.GameObject PubnubHandler::textPrefab
	GameObject_t1756533147 * ___textPrefab_3;
	// UnityEngine.GameObject PubnubHandler::canvas
	GameObject_t1756533147 * ___canvas_4;
	// UnityEngine.GameObject PubnubHandler::theButton
	GameObject_t1756533147 * ___theButton_5;
	// UnityEngine.RectTransform PubnubHandler::theButtonRect
	RectTransform_t3349966182 * ___theButtonRect_6;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PubnubHandler::fireworksPrefabs
	List_1_t1125654279 * ___fireworksPrefabs_7;
	// UnityEngine.GameObject PubnubHandler::blackoutCube
	GameObject_t1756533147 * ___blackoutCube_8;
	// UnityEngine.GameObject PubnubHandler::winnerTxtObj
	GameObject_t1756533147 * ___winnerTxtObj_9;
	// UnityEngine.GameObject PubnubHandler::imageCaptureHolderObj
	GameObject_t1756533147 * ___imageCaptureHolderObj_10;
	// UnityEngine.GameObject PubnubHandler::winnerPhotoObj
	GameObject_t1756533147 * ___winnerPhotoObj_11;
	// System.Single PubnubHandler::showWinnerTime
	float ___showWinnerTime_12;
	// System.Single PubnubHandler::showWinnerTimeMax
	float ___showWinnerTimeMax_13;
	// System.Single PubnubHandler::minSpawnTime
	float ___minSpawnTime_14;
	// System.Single PubnubHandler::maxSpawnTime
	float ___maxSpawnTime_15;
	// System.Single PubnubHandler::timer
	float ___timer_16;
	// System.Single PubnubHandler::nextTime
	float ___nextTime_17;
	// System.String PubnubHandler::winner
	String_t* ___winner_18;
	// Firebase.Auth.FirebaseAuth PubnubHandler::auth
	FirebaseAuth_t3105883899 * ___auth_19;
	// Firebase.Auth.FirebaseUser PubnubHandler::user
	FirebaseUser_t4046966602 * ___user_20;

public:
	inline static int32_t get_offset_of_pubnub_2() { return static_cast<int32_t>(offsetof(PubnubHandler_t3174799440, ___pubnub_2)); }
	inline Pubnub_t2451529532 * get_pubnub_2() const { return ___pubnub_2; }
	inline Pubnub_t2451529532 ** get_address_of_pubnub_2() { return &___pubnub_2; }
	inline void set_pubnub_2(Pubnub_t2451529532 * value)
	{
		___pubnub_2 = value;
		Il2CppCodeGenWriteBarrier(&___pubnub_2, value);
	}

	inline static int32_t get_offset_of_textPrefab_3() { return static_cast<int32_t>(offsetof(PubnubHandler_t3174799440, ___textPrefab_3)); }
	inline GameObject_t1756533147 * get_textPrefab_3() const { return ___textPrefab_3; }
	inline GameObject_t1756533147 ** get_address_of_textPrefab_3() { return &___textPrefab_3; }
	inline void set_textPrefab_3(GameObject_t1756533147 * value)
	{
		___textPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___textPrefab_3, value);
	}

	inline static int32_t get_offset_of_canvas_4() { return static_cast<int32_t>(offsetof(PubnubHandler_t3174799440, ___canvas_4)); }
	inline GameObject_t1756533147 * get_canvas_4() const { return ___canvas_4; }
	inline GameObject_t1756533147 ** get_address_of_canvas_4() { return &___canvas_4; }
	inline void set_canvas_4(GameObject_t1756533147 * value)
	{
		___canvas_4 = value;
		Il2CppCodeGenWriteBarrier(&___canvas_4, value);
	}

	inline static int32_t get_offset_of_theButton_5() { return static_cast<int32_t>(offsetof(PubnubHandler_t3174799440, ___theButton_5)); }
	inline GameObject_t1756533147 * get_theButton_5() const { return ___theButton_5; }
	inline GameObject_t1756533147 ** get_address_of_theButton_5() { return &___theButton_5; }
	inline void set_theButton_5(GameObject_t1756533147 * value)
	{
		___theButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___theButton_5, value);
	}

	inline static int32_t get_offset_of_theButtonRect_6() { return static_cast<int32_t>(offsetof(PubnubHandler_t3174799440, ___theButtonRect_6)); }
	inline RectTransform_t3349966182 * get_theButtonRect_6() const { return ___theButtonRect_6; }
	inline RectTransform_t3349966182 ** get_address_of_theButtonRect_6() { return &___theButtonRect_6; }
	inline void set_theButtonRect_6(RectTransform_t3349966182 * value)
	{
		___theButtonRect_6 = value;
		Il2CppCodeGenWriteBarrier(&___theButtonRect_6, value);
	}

	inline static int32_t get_offset_of_fireworksPrefabs_7() { return static_cast<int32_t>(offsetof(PubnubHandler_t3174799440, ___fireworksPrefabs_7)); }
	inline List_1_t1125654279 * get_fireworksPrefabs_7() const { return ___fireworksPrefabs_7; }
	inline List_1_t1125654279 ** get_address_of_fireworksPrefabs_7() { return &___fireworksPrefabs_7; }
	inline void set_fireworksPrefabs_7(List_1_t1125654279 * value)
	{
		___fireworksPrefabs_7 = value;
		Il2CppCodeGenWriteBarrier(&___fireworksPrefabs_7, value);
	}

	inline static int32_t get_offset_of_blackoutCube_8() { return static_cast<int32_t>(offsetof(PubnubHandler_t3174799440, ___blackoutCube_8)); }
	inline GameObject_t1756533147 * get_blackoutCube_8() const { return ___blackoutCube_8; }
	inline GameObject_t1756533147 ** get_address_of_blackoutCube_8() { return &___blackoutCube_8; }
	inline void set_blackoutCube_8(GameObject_t1756533147 * value)
	{
		___blackoutCube_8 = value;
		Il2CppCodeGenWriteBarrier(&___blackoutCube_8, value);
	}

	inline static int32_t get_offset_of_winnerTxtObj_9() { return static_cast<int32_t>(offsetof(PubnubHandler_t3174799440, ___winnerTxtObj_9)); }
	inline GameObject_t1756533147 * get_winnerTxtObj_9() const { return ___winnerTxtObj_9; }
	inline GameObject_t1756533147 ** get_address_of_winnerTxtObj_9() { return &___winnerTxtObj_9; }
	inline void set_winnerTxtObj_9(GameObject_t1756533147 * value)
	{
		___winnerTxtObj_9 = value;
		Il2CppCodeGenWriteBarrier(&___winnerTxtObj_9, value);
	}

	inline static int32_t get_offset_of_imageCaptureHolderObj_10() { return static_cast<int32_t>(offsetof(PubnubHandler_t3174799440, ___imageCaptureHolderObj_10)); }
	inline GameObject_t1756533147 * get_imageCaptureHolderObj_10() const { return ___imageCaptureHolderObj_10; }
	inline GameObject_t1756533147 ** get_address_of_imageCaptureHolderObj_10() { return &___imageCaptureHolderObj_10; }
	inline void set_imageCaptureHolderObj_10(GameObject_t1756533147 * value)
	{
		___imageCaptureHolderObj_10 = value;
		Il2CppCodeGenWriteBarrier(&___imageCaptureHolderObj_10, value);
	}

	inline static int32_t get_offset_of_winnerPhotoObj_11() { return static_cast<int32_t>(offsetof(PubnubHandler_t3174799440, ___winnerPhotoObj_11)); }
	inline GameObject_t1756533147 * get_winnerPhotoObj_11() const { return ___winnerPhotoObj_11; }
	inline GameObject_t1756533147 ** get_address_of_winnerPhotoObj_11() { return &___winnerPhotoObj_11; }
	inline void set_winnerPhotoObj_11(GameObject_t1756533147 * value)
	{
		___winnerPhotoObj_11 = value;
		Il2CppCodeGenWriteBarrier(&___winnerPhotoObj_11, value);
	}

	inline static int32_t get_offset_of_showWinnerTime_12() { return static_cast<int32_t>(offsetof(PubnubHandler_t3174799440, ___showWinnerTime_12)); }
	inline float get_showWinnerTime_12() const { return ___showWinnerTime_12; }
	inline float* get_address_of_showWinnerTime_12() { return &___showWinnerTime_12; }
	inline void set_showWinnerTime_12(float value)
	{
		___showWinnerTime_12 = value;
	}

	inline static int32_t get_offset_of_showWinnerTimeMax_13() { return static_cast<int32_t>(offsetof(PubnubHandler_t3174799440, ___showWinnerTimeMax_13)); }
	inline float get_showWinnerTimeMax_13() const { return ___showWinnerTimeMax_13; }
	inline float* get_address_of_showWinnerTimeMax_13() { return &___showWinnerTimeMax_13; }
	inline void set_showWinnerTimeMax_13(float value)
	{
		___showWinnerTimeMax_13 = value;
	}

	inline static int32_t get_offset_of_minSpawnTime_14() { return static_cast<int32_t>(offsetof(PubnubHandler_t3174799440, ___minSpawnTime_14)); }
	inline float get_minSpawnTime_14() const { return ___minSpawnTime_14; }
	inline float* get_address_of_minSpawnTime_14() { return &___minSpawnTime_14; }
	inline void set_minSpawnTime_14(float value)
	{
		___minSpawnTime_14 = value;
	}

	inline static int32_t get_offset_of_maxSpawnTime_15() { return static_cast<int32_t>(offsetof(PubnubHandler_t3174799440, ___maxSpawnTime_15)); }
	inline float get_maxSpawnTime_15() const { return ___maxSpawnTime_15; }
	inline float* get_address_of_maxSpawnTime_15() { return &___maxSpawnTime_15; }
	inline void set_maxSpawnTime_15(float value)
	{
		___maxSpawnTime_15 = value;
	}

	inline static int32_t get_offset_of_timer_16() { return static_cast<int32_t>(offsetof(PubnubHandler_t3174799440, ___timer_16)); }
	inline float get_timer_16() const { return ___timer_16; }
	inline float* get_address_of_timer_16() { return &___timer_16; }
	inline void set_timer_16(float value)
	{
		___timer_16 = value;
	}

	inline static int32_t get_offset_of_nextTime_17() { return static_cast<int32_t>(offsetof(PubnubHandler_t3174799440, ___nextTime_17)); }
	inline float get_nextTime_17() const { return ___nextTime_17; }
	inline float* get_address_of_nextTime_17() { return &___nextTime_17; }
	inline void set_nextTime_17(float value)
	{
		___nextTime_17 = value;
	}

	inline static int32_t get_offset_of_winner_18() { return static_cast<int32_t>(offsetof(PubnubHandler_t3174799440, ___winner_18)); }
	inline String_t* get_winner_18() const { return ___winner_18; }
	inline String_t** get_address_of_winner_18() { return &___winner_18; }
	inline void set_winner_18(String_t* value)
	{
		___winner_18 = value;
		Il2CppCodeGenWriteBarrier(&___winner_18, value);
	}

	inline static int32_t get_offset_of_auth_19() { return static_cast<int32_t>(offsetof(PubnubHandler_t3174799440, ___auth_19)); }
	inline FirebaseAuth_t3105883899 * get_auth_19() const { return ___auth_19; }
	inline FirebaseAuth_t3105883899 ** get_address_of_auth_19() { return &___auth_19; }
	inline void set_auth_19(FirebaseAuth_t3105883899 * value)
	{
		___auth_19 = value;
		Il2CppCodeGenWriteBarrier(&___auth_19, value);
	}

	inline static int32_t get_offset_of_user_20() { return static_cast<int32_t>(offsetof(PubnubHandler_t3174799440, ___user_20)); }
	inline FirebaseUser_t4046966602 * get_user_20() const { return ___user_20; }
	inline FirebaseUser_t4046966602 ** get_address_of_user_20() { return &___user_20; }
	inline void set_user_20(FirebaseUser_t4046966602 * value)
	{
		___user_20 = value;
		Il2CppCodeGenWriteBarrier(&___user_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
