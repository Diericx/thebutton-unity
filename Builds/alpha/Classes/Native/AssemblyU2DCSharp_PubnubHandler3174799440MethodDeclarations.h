﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PubnubHandler
struct PubnubHandler_t3174799440;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Core.PubnubClientError
struct PubnubClientError_t110317921;
// System.IO.Stream
struct Stream_t3255436806;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubClient110317921.h"
#include "mscorlib_System_IO_Stream3255436806.h"

// System.Void PubnubHandler::.ctor()
extern "C"  void PubnubHandler__ctor_m2812332709 (PubnubHandler_t3174799440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubHandler::Start()
extern "C"  void PubnubHandler_Start_m3550005525 (PubnubHandler_t3174799440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubHandler::Update()
extern "C"  void PubnubHandler_Update_m2071341754 (PubnubHandler_t3174799440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubHandler::DisplaySubscribeConnectStatusMessage(System.String)
extern "C"  void PubnubHandler_DisplaySubscribeConnectStatusMessage_m1546144896 (PubnubHandler_t3174799440 * __this, String_t* ___connectMessage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubHandler::DisplaySubscribeReturnMessage(System.String)
extern "C"  void PubnubHandler_DisplaySubscribeReturnMessage_m145535994 (PubnubHandler_t3174799440 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] PubnubHandler::ObjectToByteArray(System.Object)
extern "C"  ByteU5BU5D_t3397334013* PubnubHandler_ObjectToByteArray_m2738643054 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubHandler::DisplayErrorMessage(PubNubMessaging.Core.PubnubClientError)
extern "C"  void PubnubHandler_DisplayErrorMessage_m2695665072 (PubnubHandler_t3174799440 * __this, PubnubClientError_t110317921 * ___pubnubError0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubHandler::DisplayReturnMessage(System.String)
extern "C"  void PubnubHandler_DisplayReturnMessage_m2828954322 (PubnubHandler_t3174799440 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubHandler::DisplayButtonPressAnimation(System.String)
extern "C"  void PubnubHandler_DisplayButtonPressAnimation_m3515277236 (PubnubHandler_t3174799440 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubHandler::DisplayWinAnimation(System.String)
extern "C"  void PubnubHandler_DisplayWinAnimation_m212166039 (PubnubHandler_t3174799440 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubHandler::DisplayImageCapture()
extern "C"  void PubnubHandler_DisplayImageCapture_m2246936648 (PubnubHandler_t3174799440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubHandler::TakePictureBtnCallback()
extern "C"  void PubnubHandler_TakePictureBtnCallback_m1755024499 (PubnubHandler_t3174799440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubHandler::CopyStream(System.IO.Stream,System.IO.Stream)
extern "C"  void PubnubHandler_CopyStream_m594706334 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___input0, Stream_t3255436806 * ___output1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PubnubHandler::OnButtonPress()
extern "C"  void PubnubHandler_OnButtonPress_m3641013897 (PubnubHandler_t3174799440 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
