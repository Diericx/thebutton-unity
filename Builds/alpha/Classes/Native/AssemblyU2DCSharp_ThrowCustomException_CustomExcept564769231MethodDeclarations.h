﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ThrowCustomException/CustomException
struct CustomException_t564769231;

#include "codegen/il2cpp-codegen.h"

// System.Void ThrowCustomException/CustomException::.ctor()
extern "C"  void CustomException__ctor_m51005574 (CustomException_t564769231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
