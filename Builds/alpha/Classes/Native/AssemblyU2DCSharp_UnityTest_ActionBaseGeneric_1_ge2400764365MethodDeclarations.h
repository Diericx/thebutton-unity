﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.ActionBaseGeneric`1<System.Object>
struct ActionBaseGeneric_1_t2400764365;
// System.Object
struct Il2CppObject;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void UnityTest.ActionBaseGeneric`1<System.Object>::.ctor()
extern "C"  void ActionBaseGeneric_1__ctor_m1387476675_gshared (ActionBaseGeneric_1_t2400764365 * __this, const MethodInfo* method);
#define ActionBaseGeneric_1__ctor_m1387476675(__this, method) ((  void (*) (ActionBaseGeneric_1_t2400764365 *, const MethodInfo*))ActionBaseGeneric_1__ctor_m1387476675_gshared)(__this, method)
// System.Boolean UnityTest.ActionBaseGeneric`1<System.Object>::Compare(System.Object)
extern "C"  bool ActionBaseGeneric_1_Compare_m3149683026_gshared (ActionBaseGeneric_1_t2400764365 * __this, Il2CppObject * ___objVal0, const MethodInfo* method);
#define ActionBaseGeneric_1_Compare_m3149683026(__this, ___objVal0, method) ((  bool (*) (ActionBaseGeneric_1_t2400764365 *, Il2CppObject *, const MethodInfo*))ActionBaseGeneric_1_Compare_m3149683026_gshared)(__this, ___objVal0, method)
// System.Type[] UnityTest.ActionBaseGeneric`1<System.Object>::GetAccepatbleTypesForA()
extern "C"  TypeU5BU5D_t1664964607* ActionBaseGeneric_1_GetAccepatbleTypesForA_m286774308_gshared (ActionBaseGeneric_1_t2400764365 * __this, const MethodInfo* method);
#define ActionBaseGeneric_1_GetAccepatbleTypesForA_m286774308(__this, method) ((  TypeU5BU5D_t1664964607* (*) (ActionBaseGeneric_1_t2400764365 *, const MethodInfo*))ActionBaseGeneric_1_GetAccepatbleTypesForA_m286774308_gshared)(__this, method)
// System.Type UnityTest.ActionBaseGeneric`1<System.Object>::GetParameterType()
extern "C"  Type_t * ActionBaseGeneric_1_GetParameterType_m1855856438_gshared (ActionBaseGeneric_1_t2400764365 * __this, const MethodInfo* method);
#define ActionBaseGeneric_1_GetParameterType_m1855856438(__this, method) ((  Type_t * (*) (ActionBaseGeneric_1_t2400764365 *, const MethodInfo*))ActionBaseGeneric_1_GetParameterType_m1855856438_gshared)(__this, method)
// System.Boolean UnityTest.ActionBaseGeneric`1<System.Object>::get_UseCache()
extern "C"  bool ActionBaseGeneric_1_get_UseCache_m3785163109_gshared (ActionBaseGeneric_1_t2400764365 * __this, const MethodInfo* method);
#define ActionBaseGeneric_1_get_UseCache_m3785163109(__this, method) ((  bool (*) (ActionBaseGeneric_1_t2400764365 *, const MethodInfo*))ActionBaseGeneric_1_get_UseCache_m3785163109_gshared)(__this, method)
