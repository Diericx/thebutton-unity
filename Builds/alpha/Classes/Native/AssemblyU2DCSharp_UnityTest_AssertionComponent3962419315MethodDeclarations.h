﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.AssertionComponent
struct AssertionComponent_t3962419315;
// UnityTest.ActionBase
struct ActionBase_t1823832315;
// UnityEngine.Object
struct Object_t1021602117;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UnityTest_ActionBase1823832315.h"
#include "AssemblyU2DCSharp_UnityTest_CheckMethod3605307967.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"

// System.Void UnityTest.AssertionComponent::.ctor()
extern "C"  void AssertionComponent__ctor_m52941877 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityTest.ActionBase UnityTest.AssertionComponent::get_Action()
extern "C"  ActionBase_t1823832315 * AssertionComponent_get_Action_m991659445 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::set_Action(UnityTest.ActionBase)
extern "C"  void AssertionComponent_set_Action_m2795050252 (AssertionComponent_t3962419315 * __this, ActionBase_t1823832315 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityTest.AssertionComponent::GetFailureReferenceObject()
extern "C"  Object_t1021602117 * AssertionComponent_GetFailureReferenceObject_m1708036584 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityTest.AssertionComponent::GetCreationLocation()
extern "C"  String_t* AssertionComponent_GetCreationLocation_m372952432 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::Awake()
extern "C"  void AssertionComponent_Awake_m679649834 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnValidate()
extern "C"  void AssertionComponent_OnValidate_m3874909766 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnComponentCopy()
extern "C"  void AssertionComponent_OnComponentCopy_m4034867336 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::Start()
extern "C"  void AssertionComponent_Start_m258612725 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityTest.AssertionComponent::CheckPeriodically()
extern "C"  Il2CppObject * AssertionComponent_CheckPeriodically_m4065624866 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.AssertionComponent::ShouldCheckOnFrame()
extern "C"  bool AssertionComponent_ShouldCheckOnFrame_m1287344530 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnDisable()
extern "C"  void AssertionComponent_OnDisable_m1429134442 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnEnable()
extern "C"  void AssertionComponent_OnEnable_m563745333 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnDestroy()
extern "C"  void AssertionComponent_OnDestroy_m2270964056 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::Update()
extern "C"  void AssertionComponent_Update_m2326843660 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::FixedUpdate()
extern "C"  void AssertionComponent_FixedUpdate_m1025267294 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::LateUpdate()
extern "C"  void AssertionComponent_LateUpdate_m2747647784 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnControllerColliderHit()
extern "C"  void AssertionComponent_OnControllerColliderHit_m3287486107 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnParticleCollision()
extern "C"  void AssertionComponent_OnParticleCollision_m3497183688 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnJointBreak()
extern "C"  void AssertionComponent_OnJointBreak_m4212969669 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnBecameInvisible()
extern "C"  void AssertionComponent_OnBecameInvisible_m1694599440 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnBecameVisible()
extern "C"  void AssertionComponent_OnBecameVisible_m3083986795 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnTriggerEnter()
extern "C"  void AssertionComponent_OnTriggerEnter_m3375505442 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnTriggerExit()
extern "C"  void AssertionComponent_OnTriggerExit_m2900804214 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnTriggerStay()
extern "C"  void AssertionComponent_OnTriggerStay_m3385344659 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnCollisionEnter()
extern "C"  void AssertionComponent_OnCollisionEnter_m1450361174 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnCollisionExit()
extern "C"  void AssertionComponent_OnCollisionExit_m1334096934 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnCollisionStay()
extern "C"  void AssertionComponent_OnCollisionStay_m2105819113 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnTriggerEnter2D()
extern "C"  void AssertionComponent_OnTriggerEnter2D_m1994943684 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnTriggerExit2D()
extern "C"  void AssertionComponent_OnTriggerExit2D_m645500204 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnTriggerStay2D()
extern "C"  void AssertionComponent_OnTriggerStay2D_m2084111349 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnCollisionEnter2D()
extern "C"  void AssertionComponent_OnCollisionEnter2D_m4084957328 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnCollisionExit2D()
extern "C"  void AssertionComponent_OnCollisionExit2D_m1445049096 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::OnCollisionStay2D()
extern "C"  void AssertionComponent_OnCollisionStay2D_m3252872799 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::CheckAssertionFor(UnityTest.CheckMethod)
extern "C"  void AssertionComponent_CheckAssertionFor_m1465289114 (AssertionComponent_t3962419315 * __this, int32_t ___checkMethod0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.AssertionComponent::IsCheckMethodSelected(UnityTest.CheckMethod)
extern "C"  bool AssertionComponent_IsCheckMethodSelected_m704689161 (AssertionComponent_t3962419315 * __this, int32_t ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::set_UpdateCheckStartOnFrame(System.Int32)
extern "C"  void AssertionComponent_set_UpdateCheckStartOnFrame_m157643926 (AssertionComponent_t3962419315 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::set_UpdateCheckRepeatFrequency(System.Int32)
extern "C"  void AssertionComponent_set_UpdateCheckRepeatFrequency_m2306380729 (AssertionComponent_t3962419315 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::set_UpdateCheckRepeat(System.Boolean)
extern "C"  void AssertionComponent_set_UpdateCheckRepeat_m3286883015 (AssertionComponent_t3962419315 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::set_TimeCheckStartAfter(System.Single)
extern "C"  void AssertionComponent_set_TimeCheckStartAfter_m3087334528 (AssertionComponent_t3962419315 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::set_TimeCheckRepeatFrequency(System.Single)
extern "C"  void AssertionComponent_set_TimeCheckRepeatFrequency_m2251161325 (AssertionComponent_t3962419315 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent::set_TimeCheckRepeat(System.Boolean)
extern "C"  void AssertionComponent_set_TimeCheckRepeat_m3002406387 (AssertionComponent_t3962419315 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityTest.AssertionComponent UnityTest.AssertionComponent::get_Component()
extern "C"  AssertionComponent_t3962419315 * AssertionComponent_get_Component_m1473795898 (AssertionComponent_t3962419315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.AssertionComponent::<OnComponentCopy>m__0(UnityEngine.Object)
extern "C"  bool AssertionComponent_U3COnComponentCopyU3Em__0_m3271956507 (AssertionComponent_t3962419315 * __this, Object_t1021602117 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
