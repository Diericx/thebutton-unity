﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.AssertionComponent/<CheckPeriodically>c__Iterator0
struct U3CCheckPeriodicallyU3Ec__Iterator0_t1299100433;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityTest.AssertionComponent/<CheckPeriodically>c__Iterator0::.ctor()
extern "C"  void U3CCheckPeriodicallyU3Ec__Iterator0__ctor_m615965068 (U3CCheckPeriodicallyU3Ec__Iterator0_t1299100433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.AssertionComponent/<CheckPeriodically>c__Iterator0::MoveNext()
extern "C"  bool U3CCheckPeriodicallyU3Ec__Iterator0_MoveNext_m2885790320 (U3CCheckPeriodicallyU3Ec__Iterator0_t1299100433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityTest.AssertionComponent/<CheckPeriodically>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckPeriodicallyU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1815771492 (U3CCheckPeriodicallyU3Ec__Iterator0_t1299100433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityTest.AssertionComponent/<CheckPeriodically>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckPeriodicallyU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3533280668 (U3CCheckPeriodicallyU3Ec__Iterator0_t1299100433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent/<CheckPeriodically>c__Iterator0::Dispose()
extern "C"  void U3CCheckPeriodicallyU3Ec__Iterator0_Dispose_m939201179 (U3CCheckPeriodicallyU3Ec__Iterator0_t1299100433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.AssertionComponent/<CheckPeriodically>c__Iterator0::Reset()
extern "C"  void U3CCheckPeriodicallyU3Ec__Iterator0_Reset_m2334415557 (U3CCheckPeriodicallyU3Ec__Iterator0_t1299100433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
