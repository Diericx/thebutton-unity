﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.AssertionComponent
struct AssertionComponent_t3962419315;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityTest.AssertionComponent[]
struct AssertionComponentU5BU5D_t2921566306;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UnityTest_AssertionComponent3962419315.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void UnityTest.Assertions::CheckAssertions()
extern "C"  void Assertions_CheckAssertions_m1491481282 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.Assertions::CheckAssertions(UnityTest.AssertionComponent)
extern "C"  void Assertions_CheckAssertions_m4156310132 (Il2CppObject * __this /* static, unused */, AssertionComponent_t3962419315 * ___assertion0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.Assertions::CheckAssertions(UnityEngine.GameObject)
extern "C"  void Assertions_CheckAssertions_m3867351988 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.Assertions::CheckAssertions(UnityTest.AssertionComponent[])
extern "C"  void Assertions_CheckAssertions_m404891956 (Il2CppObject * __this /* static, unused */, AssertionComponentU5BU5D_t2921566306* ___assertions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
