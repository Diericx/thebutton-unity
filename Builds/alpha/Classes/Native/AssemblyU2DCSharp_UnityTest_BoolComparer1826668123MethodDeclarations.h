﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.BoolComparer
struct BoolComparer_t1826668123;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityTest.BoolComparer::.ctor()
extern "C"  void BoolComparer__ctor_m2320175875 (BoolComparer_t1826668123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.BoolComparer::Compare(System.Boolean,System.Boolean)
extern "C"  bool BoolComparer_Compare_m2632754784 (BoolComparer_t1826668123 * __this, bool ___a0, bool ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
