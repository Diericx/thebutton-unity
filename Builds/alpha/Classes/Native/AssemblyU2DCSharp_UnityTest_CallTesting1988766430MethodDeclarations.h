﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.CallTesting
struct CallTesting_t1988766430;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UnityTest_CallTesting_Functions3020774339.h"

// System.Void UnityTest.CallTesting::.ctor()
extern "C"  void CallTesting__ctor_m2754259626 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::TryToCallTesting(UnityTest.CallTesting/Functions)
extern "C"  void CallTesting_TryToCallTesting_m1883257545 (CallTesting_t1988766430 * __this, int32_t ___invokingMethod0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::Start()
extern "C"  void CallTesting_Start_m3966504058 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::Update()
extern "C"  void CallTesting_Update_m1327354877 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::CallAfterFrames()
extern "C"  void CallTesting_CallAfterFrames_m1111170198 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::CallAfterSeconds()
extern "C"  void CallTesting_CallAfterSeconds_m3208111185 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::OnDisable()
extern "C"  void CallTesting_OnDisable_m2741282897 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::OnEnable()
extern "C"  void CallTesting_OnEnable_m1502436250 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::OnDestroy()
extern "C"  void CallTesting_OnDestroy_m462227099 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::FixedUpdate()
extern "C"  void CallTesting_FixedUpdate_m3033860131 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::LateUpdate()
extern "C"  void CallTesting_LateUpdate_m1145311985 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::OnControllerColliderHit()
extern "C"  void CallTesting_OnControllerColliderHit_m4289406700 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::OnParticleCollision()
extern "C"  void CallTesting_OnParticleCollision_m391991739 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::OnJointBreak()
extern "C"  void CallTesting_OnJointBreak_m1904066794 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::OnBecameInvisible()
extern "C"  void CallTesting_OnBecameInvisible_m540150505 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::OnBecameVisible()
extern "C"  void CallTesting_OnBecameVisible_m4251528658 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::OnTriggerEnter()
extern "C"  void CallTesting_OnTriggerEnter_m2244751273 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::OnTriggerExit()
extern "C"  void CallTesting_OnTriggerExit_m3642651893 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::OnTriggerStay()
extern "C"  void CallTesting_OnTriggerStay_m377793172 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::OnCollisionEnter()
extern "C"  void CallTesting_OnCollisionEnter_m1564842267 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::OnCollisionExit()
extern "C"  void CallTesting_OnCollisionExit_m2301481051 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::OnCollisionStay()
extern "C"  void CallTesting_OnCollisionStay_m3788504284 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::OnTriggerEnter2D()
extern "C"  void CallTesting_OnTriggerEnter2D_m3542662559 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::OnTriggerExit2D()
extern "C"  void CallTesting_OnTriggerExit2D_m1139371127 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::OnTriggerStay2D()
extern "C"  void CallTesting_OnTriggerStay2D_m3178591706 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::OnCollisionEnter2D()
extern "C"  void CallTesting_OnCollisionEnter2D_m1256594009 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::OnCollisionExit2D()
extern "C"  void CallTesting_OnCollisionExit2D_m22945889 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CallTesting::OnCollisionStay2D()
extern "C"  void CallTesting_OnCollisionStay2D_m3739661534 (CallTesting_t1988766430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
