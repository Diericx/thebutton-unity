﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.ColliderComparer
struct ColliderComparer_t340179063;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"

// System.Void UnityTest.ColliderComparer::.ctor()
extern "C"  void ColliderComparer__ctor_m1184340571 (ColliderComparer_t340179063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.ColliderComparer::Compare(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool ColliderComparer_Compare_m2915136914 (ColliderComparer_t340179063 * __this, Bounds_t3033363703  ___a0, Bounds_t3033363703  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
