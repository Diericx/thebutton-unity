﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.ComparerBase
struct ComparerBase_t429484414;
// System.Object
struct Il2CppObject;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void UnityTest.ComparerBase::.ctor()
extern "C"  void ComparerBase__ctor_m731120606 (ComparerBase_t429484414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.ComparerBase::Compare(System.Object)
extern "C"  bool ComparerBase_Compare_m249251297 (ComparerBase_t429484414 * __this, Il2CppObject * ___objValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] UnityTest.ComparerBase::GetAccepatbleTypesForB()
extern "C"  TypeU5BU5D_t1664964607* ComparerBase_GetAccepatbleTypesForB_m2626510446 (ComparerBase_t429484414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityTest.ComparerBase::get_ConstValue()
extern "C"  Il2CppObject * ComparerBase_get_ConstValue_m3018520946 (ComparerBase_t429484414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.ComparerBase::set_ConstValue(System.Object)
extern "C"  void ComparerBase_set_ConstValue_m124939977 (ComparerBase_t429484414 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityTest.ComparerBase::GetDefaultConstValue()
extern "C"  Il2CppObject * ComparerBase_GetDefaultConstValue_m600883014 (ComparerBase_t429484414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityTest.ComparerBase::GetFailureMessage()
extern "C"  String_t* ComparerBase_GetFailureMessage_m3136748708 (ComparerBase_t429484414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
