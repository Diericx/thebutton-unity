﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.ComparerBaseGeneric`1<System.Boolean>
struct ComparerBaseGeneric_1_t1096640445;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityTest.ComparerBaseGeneric`1<System.Boolean>::.ctor()
extern "C"  void ComparerBaseGeneric_1__ctor_m2951158077_gshared (ComparerBaseGeneric_1_t1096640445 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_1__ctor_m2951158077(__this, method) ((  void (*) (ComparerBaseGeneric_1_t1096640445 *, const MethodInfo*))ComparerBaseGeneric_1__ctor_m2951158077_gshared)(__this, method)
