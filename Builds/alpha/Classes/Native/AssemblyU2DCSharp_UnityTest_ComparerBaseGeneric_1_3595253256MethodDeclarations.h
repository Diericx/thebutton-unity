﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_1_4255482318MethodDeclarations.h"

// System.Void UnityTest.ComparerBaseGeneric`1<System.String>::.ctor()
#define ComparerBaseGeneric_1__ctor_m3225913122(__this, method) ((  void (*) (ComparerBaseGeneric_1_t3595253256 *, const MethodInfo*))ComparerBaseGeneric_1__ctor_m1652265756_gshared)(__this, method)
