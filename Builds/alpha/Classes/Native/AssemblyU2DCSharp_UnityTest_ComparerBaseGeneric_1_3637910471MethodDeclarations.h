﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.ComparerBaseGeneric`1<System.Int32>
struct ComparerBaseGeneric_1_t3637910471;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityTest.ComparerBaseGeneric`1<System.Int32>::.ctor()
extern "C"  void ComparerBaseGeneric_1__ctor_m2778793987_gshared (ComparerBaseGeneric_1_t3637910471 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_1__ctor_m2778793987(__this, method) ((  void (*) (ComparerBaseGeneric_1_t3637910471 *, const MethodInfo*))ComparerBaseGeneric_1__ctor_m2778793987_gshared)(__this, method)
