﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.ComparerBaseGeneric`1<System.Object>
struct ComparerBaseGeneric_1_t4255482318;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityTest.ComparerBaseGeneric`1<System.Object>::.ctor()
extern "C"  void ComparerBaseGeneric_1__ctor_m1652265756_gshared (ComparerBaseGeneric_1_t4255482318 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_1__ctor_m1652265756(__this, method) ((  void (*) (ComparerBaseGeneric_1_t4255482318 *, const MethodInfo*))ComparerBaseGeneric_1__ctor_m1652265756_gshared)(__this, method)
