﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.ComparerBaseGeneric`1<UnityEngine.Bounds>
struct ComparerBaseGeneric_1_t304429430;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityTest.ComparerBaseGeneric`1<UnityEngine.Bounds>::.ctor()
extern "C"  void ComparerBaseGeneric_1__ctor_m3668702_gshared (ComparerBaseGeneric_1_t304429430 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_1__ctor_m3668702(__this, method) ((  void (*) (ComparerBaseGeneric_1_t304429430 *, const MethodInfo*))ComparerBaseGeneric_1__ctor_m3668702_gshared)(__this, method)
