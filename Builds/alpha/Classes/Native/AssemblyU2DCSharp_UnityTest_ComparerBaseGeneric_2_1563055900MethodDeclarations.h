﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>
struct ComparerBaseGeneric_2_t1563055900;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1664964607;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>::.ctor()
extern "C"  void ComparerBaseGeneric_2__ctor_m554013261_gshared (ComparerBaseGeneric_2_t1563055900 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2__ctor_m554013261(__this, method) ((  void (*) (ComparerBaseGeneric_2_t1563055900 *, const MethodInfo*))ComparerBaseGeneric_2__ctor_m554013261_gshared)(__this, method)
// System.Object UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>::get_ConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_get_ConstValue_m1615861079_gshared (ComparerBaseGeneric_2_t1563055900 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_get_ConstValue_m1615861079(__this, method) ((  Il2CppObject * (*) (ComparerBaseGeneric_2_t1563055900 *, const MethodInfo*))ComparerBaseGeneric_2_get_ConstValue_m1615861079_gshared)(__this, method)
// System.Void UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>::set_ConstValue(System.Object)
extern "C"  void ComparerBaseGeneric_2_set_ConstValue_m2429710928_gshared (ComparerBaseGeneric_2_t1563055900 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ComparerBaseGeneric_2_set_ConstValue_m2429710928(__this, ___value0, method) ((  void (*) (ComparerBaseGeneric_2_t1563055900 *, Il2CppObject *, const MethodInfo*))ComparerBaseGeneric_2_set_ConstValue_m2429710928_gshared)(__this, ___value0, method)
// System.Object UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>::GetDefaultConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_GetDefaultConstValue_m2519031227_gshared (ComparerBaseGeneric_2_t1563055900 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetDefaultConstValue_m2519031227(__this, method) ((  Il2CppObject * (*) (ComparerBaseGeneric_2_t1563055900 *, const MethodInfo*))ComparerBaseGeneric_2_GetDefaultConstValue_m2519031227_gshared)(__this, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>::IsValueType(System.Type)
extern "C"  bool ComparerBaseGeneric_2_IsValueType_m383119759_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method);
#define ComparerBaseGeneric_2_IsValueType_m383119759(__this /* static, unused */, ___type0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))ComparerBaseGeneric_2_IsValueType_m383119759_gshared)(__this /* static, unused */, ___type0, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>::Compare(System.Object,System.Object)
extern "C"  bool ComparerBaseGeneric_2_Compare_m2198560970_gshared (ComparerBaseGeneric_2_t1563055900 * __this, Il2CppObject * ___a0, Il2CppObject * ___b1, const MethodInfo* method);
#define ComparerBaseGeneric_2_Compare_m2198560970(__this, ___a0, ___b1, method) ((  bool (*) (ComparerBaseGeneric_2_t1563055900 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ComparerBaseGeneric_2_Compare_m2198560970_gshared)(__this, ___a0, ___b1, method)
// System.Type[] UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>::GetAccepatbleTypesForA()
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForA_m678438718_gshared (ComparerBaseGeneric_2_t1563055900 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetAccepatbleTypesForA_m678438718(__this, method) ((  TypeU5BU5D_t1664964607* (*) (ComparerBaseGeneric_2_t1563055900 *, const MethodInfo*))ComparerBaseGeneric_2_GetAccepatbleTypesForA_m678438718_gshared)(__this, method)
// System.Type[] UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>::GetAccepatbleTypesForB()
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForB_m1101926221_gshared (ComparerBaseGeneric_2_t1563055900 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetAccepatbleTypesForB_m1101926221(__this, method) ((  TypeU5BU5D_t1664964607* (*) (ComparerBaseGeneric_2_t1563055900 *, const MethodInfo*))ComparerBaseGeneric_2_GetAccepatbleTypesForB_m1101926221_gshared)(__this, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>::get_UseCache()
extern "C"  bool ComparerBaseGeneric_2_get_UseCache_m3664638855_gshared (ComparerBaseGeneric_2_t1563055900 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_get_UseCache_m3664638855(__this, method) ((  bool (*) (ComparerBaseGeneric_2_t1563055900 *, const MethodInfo*))ComparerBaseGeneric_2_get_UseCache_m3664638855_gshared)(__this, method)
