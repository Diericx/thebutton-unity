﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>
struct ComparerBaseGeneric_2_t2764862240;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1664964607;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>::.ctor()
extern "C"  void ComparerBaseGeneric_2__ctor_m2944922553_gshared (ComparerBaseGeneric_2_t2764862240 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2__ctor_m2944922553(__this, method) ((  void (*) (ComparerBaseGeneric_2_t2764862240 *, const MethodInfo*))ComparerBaseGeneric_2__ctor_m2944922553_gshared)(__this, method)
// System.Object UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>::get_ConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_get_ConstValue_m3589533995_gshared (ComparerBaseGeneric_2_t2764862240 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_get_ConstValue_m3589533995(__this, method) ((  Il2CppObject * (*) (ComparerBaseGeneric_2_t2764862240 *, const MethodInfo*))ComparerBaseGeneric_2_get_ConstValue_m3589533995_gshared)(__this, method)
// System.Void UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>::set_ConstValue(System.Object)
extern "C"  void ComparerBaseGeneric_2_set_ConstValue_m1223600742_gshared (ComparerBaseGeneric_2_t2764862240 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ComparerBaseGeneric_2_set_ConstValue_m1223600742(__this, ___value0, method) ((  void (*) (ComparerBaseGeneric_2_t2764862240 *, Il2CppObject *, const MethodInfo*))ComparerBaseGeneric_2_set_ConstValue_m1223600742_gshared)(__this, ___value0, method)
// System.Object UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>::GetDefaultConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_GetDefaultConstValue_m4190662983_gshared (ComparerBaseGeneric_2_t2764862240 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetDefaultConstValue_m4190662983(__this, method) ((  Il2CppObject * (*) (ComparerBaseGeneric_2_t2764862240 *, const MethodInfo*))ComparerBaseGeneric_2_GetDefaultConstValue_m4190662983_gshared)(__this, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>::IsValueType(System.Type)
extern "C"  bool ComparerBaseGeneric_2_IsValueType_m3892472595_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method);
#define ComparerBaseGeneric_2_IsValueType_m3892472595(__this /* static, unused */, ___type0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))ComparerBaseGeneric_2_IsValueType_m3892472595_gshared)(__this /* static, unused */, ___type0, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>::Compare(System.Object,System.Object)
extern "C"  bool ComparerBaseGeneric_2_Compare_m3999256872_gshared (ComparerBaseGeneric_2_t2764862240 * __this, Il2CppObject * ___a0, Il2CppObject * ___b1, const MethodInfo* method);
#define ComparerBaseGeneric_2_Compare_m3999256872(__this, ___a0, ___b1, method) ((  bool (*) (ComparerBaseGeneric_2_t2764862240 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ComparerBaseGeneric_2_Compare_m3999256872_gshared)(__this, ___a0, ___b1, method)
// System.Type[] UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>::GetAccepatbleTypesForA()
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForA_m3612969492_gshared (ComparerBaseGeneric_2_t2764862240 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetAccepatbleTypesForA_m3612969492(__this, method) ((  TypeU5BU5D_t1664964607* (*) (ComparerBaseGeneric_2_t2764862240 *, const MethodInfo*))ComparerBaseGeneric_2_GetAccepatbleTypesForA_m3612969492_gshared)(__this, method)
// System.Type[] UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>::GetAccepatbleTypesForB()
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForB_m3754131993_gshared (ComparerBaseGeneric_2_t2764862240 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetAccepatbleTypesForB_m3754131993(__this, method) ((  TypeU5BU5D_t1664964607* (*) (ComparerBaseGeneric_2_t2764862240 *, const MethodInfo*))ComparerBaseGeneric_2_GetAccepatbleTypesForB_m3754131993_gshared)(__this, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>::get_UseCache()
extern "C"  bool ComparerBaseGeneric_2_get_UseCache_m3855553179_gshared (ComparerBaseGeneric_2_t2764862240 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_get_UseCache_m3855553179(__this, method) ((  bool (*) (ComparerBaseGeneric_2_t2764862240 *, const MethodInfo*))ComparerBaseGeneric_2_get_UseCache_m3855553179_gshared)(__this, method)
