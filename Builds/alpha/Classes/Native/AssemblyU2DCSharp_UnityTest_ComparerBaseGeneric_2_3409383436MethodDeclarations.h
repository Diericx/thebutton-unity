﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>
struct ComparerBaseGeneric_2_t3409383436;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1664964607;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>::.ctor()
extern "C"  void ComparerBaseGeneric_2__ctor_m1173068249_gshared (ComparerBaseGeneric_2_t3409383436 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2__ctor_m1173068249(__this, method) ((  void (*) (ComparerBaseGeneric_2_t3409383436 *, const MethodInfo*))ComparerBaseGeneric_2__ctor_m1173068249_gshared)(__this, method)
// System.Object UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>::get_ConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_get_ConstValue_m4270912331_gshared (ComparerBaseGeneric_2_t3409383436 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_get_ConstValue_m4270912331(__this, method) ((  Il2CppObject * (*) (ComparerBaseGeneric_2_t3409383436 *, const MethodInfo*))ComparerBaseGeneric_2_get_ConstValue_m4270912331_gshared)(__this, method)
// System.Void UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>::set_ConstValue(System.Object)
extern "C"  void ComparerBaseGeneric_2_set_ConstValue_m2713844870_gshared (ComparerBaseGeneric_2_t3409383436 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ComparerBaseGeneric_2_set_ConstValue_m2713844870(__this, ___value0, method) ((  void (*) (ComparerBaseGeneric_2_t3409383436 *, Il2CppObject *, const MethodInfo*))ComparerBaseGeneric_2_set_ConstValue_m2713844870_gshared)(__this, ___value0, method)
// System.Object UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>::GetDefaultConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_GetDefaultConstValue_m2772259111_gshared (ComparerBaseGeneric_2_t3409383436 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetDefaultConstValue_m2772259111(__this, method) ((  Il2CppObject * (*) (ComparerBaseGeneric_2_t3409383436 *, const MethodInfo*))ComparerBaseGeneric_2_GetDefaultConstValue_m2772259111_gshared)(__this, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>::IsValueType(System.Type)
extern "C"  bool ComparerBaseGeneric_2_IsValueType_m2742662963_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method);
#define ComparerBaseGeneric_2_IsValueType_m2742662963(__this /* static, unused */, ___type0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))ComparerBaseGeneric_2_IsValueType_m2742662963_gshared)(__this /* static, unused */, ___type0, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>::Compare(System.Object,System.Object)
extern "C"  bool ComparerBaseGeneric_2_Compare_m560240328_gshared (ComparerBaseGeneric_2_t3409383436 * __this, Il2CppObject * ___a0, Il2CppObject * ___b1, const MethodInfo* method);
#define ComparerBaseGeneric_2_Compare_m560240328(__this, ___a0, ___b1, method) ((  bool (*) (ComparerBaseGeneric_2_t3409383436 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ComparerBaseGeneric_2_Compare_m560240328_gshared)(__this, ___a0, ___b1, method)
// System.Type[] UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>::GetAccepatbleTypesForA()
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForA_m1665858868_gshared (ComparerBaseGeneric_2_t3409383436 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetAccepatbleTypesForA_m1665858868(__this, method) ((  TypeU5BU5D_t1664964607* (*) (ComparerBaseGeneric_2_t3409383436 *, const MethodInfo*))ComparerBaseGeneric_2_GetAccepatbleTypesForA_m1665858868_gshared)(__this, method)
// System.Type[] UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>::GetAccepatbleTypesForB()
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForB_m402302905_gshared (ComparerBaseGeneric_2_t3409383436 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetAccepatbleTypesForB_m402302905(__this, method) ((  TypeU5BU5D_t1664964607* (*) (ComparerBaseGeneric_2_t3409383436 *, const MethodInfo*))ComparerBaseGeneric_2_GetAccepatbleTypesForB_m402302905_gshared)(__this, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>::get_UseCache()
extern "C"  bool ComparerBaseGeneric_2_get_UseCache_m504648699_gshared (ComparerBaseGeneric_2_t3409383436 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_get_UseCache_m504648699(__this, method) ((  bool (*) (ComparerBaseGeneric_2_t3409383436 *, const MethodInfo*))ComparerBaseGeneric_2_get_UseCache_m504648699_gshared)(__this, method)
