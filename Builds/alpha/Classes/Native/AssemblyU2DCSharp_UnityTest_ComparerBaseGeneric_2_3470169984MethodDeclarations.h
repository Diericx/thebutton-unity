﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>
struct ComparerBaseGeneric_2_t3470169984;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1664964607;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>::.ctor()
extern "C"  void ComparerBaseGeneric_2__ctor_m536379533_gshared (ComparerBaseGeneric_2_t3470169984 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2__ctor_m536379533(__this, method) ((  void (*) (ComparerBaseGeneric_2_t3470169984 *, const MethodInfo*))ComparerBaseGeneric_2__ctor_m536379533_gshared)(__this, method)
// System.Object UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>::get_ConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_get_ConstValue_m789496931_gshared (ComparerBaseGeneric_2_t3470169984 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_get_ConstValue_m789496931(__this, method) ((  Il2CppObject * (*) (ComparerBaseGeneric_2_t3470169984 *, const MethodInfo*))ComparerBaseGeneric_2_get_ConstValue_m789496931_gshared)(__this, method)
// System.Void UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>::set_ConstValue(System.Object)
extern "C"  void ComparerBaseGeneric_2_set_ConstValue_m1771588154_gshared (ComparerBaseGeneric_2_t3470169984 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ComparerBaseGeneric_2_set_ConstValue_m1771588154(__this, ___value0, method) ((  void (*) (ComparerBaseGeneric_2_t3470169984 *, Il2CppObject *, const MethodInfo*))ComparerBaseGeneric_2_set_ConstValue_m1771588154_gshared)(__this, ___value0, method)
// System.Object UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>::GetDefaultConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_GetDefaultConstValue_m4272022687_gshared (ComparerBaseGeneric_2_t3470169984 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetDefaultConstValue_m4272022687(__this, method) ((  Il2CppObject * (*) (ComparerBaseGeneric_2_t3470169984 *, const MethodInfo*))ComparerBaseGeneric_2_GetDefaultConstValue_m4272022687_gshared)(__this, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>::IsValueType(System.Type)
extern "C"  bool ComparerBaseGeneric_2_IsValueType_m198709307_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method);
#define ComparerBaseGeneric_2_IsValueType_m198709307(__this /* static, unused */, ___type0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))ComparerBaseGeneric_2_IsValueType_m198709307_gshared)(__this /* static, unused */, ___type0, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>::Compare(System.Object,System.Object)
extern "C"  bool ComparerBaseGeneric_2_Compare_m383510304_gshared (ComparerBaseGeneric_2_t3470169984 * __this, Il2CppObject * ___a0, Il2CppObject * ___b1, const MethodInfo* method);
#define ComparerBaseGeneric_2_Compare_m383510304(__this, ___a0, ___b1, method) ((  bool (*) (ComparerBaseGeneric_2_t3470169984 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ComparerBaseGeneric_2_Compare_m383510304_gshared)(__this, ___a0, ___b1, method)
// System.Type[] UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>::GetAccepatbleTypesForA()
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForA_m3955461180_gshared (ComparerBaseGeneric_2_t3470169984 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetAccepatbleTypesForA_m3955461180(__this, method) ((  TypeU5BU5D_t1664964607* (*) (ComparerBaseGeneric_2_t3470169984 *, const MethodInfo*))ComparerBaseGeneric_2_GetAccepatbleTypesForA_m3955461180_gshared)(__this, method)
// System.Type[] UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>::GetAccepatbleTypesForB()
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForB_m3531973677_gshared (ComparerBaseGeneric_2_t3470169984 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetAccepatbleTypesForB_m3531973677(__this, method) ((  TypeU5BU5D_t1664964607* (*) (ComparerBaseGeneric_2_t3470169984 *, const MethodInfo*))ComparerBaseGeneric_2_GetAccepatbleTypesForB_m3531973677_gshared)(__this, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>::get_UseCache()
extern "C"  bool ComparerBaseGeneric_2_get_UseCache_m3970344979_gshared (ComparerBaseGeneric_2_t3470169984 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_get_UseCache_m3970344979(__this, method) ((  bool (*) (ComparerBaseGeneric_2_t3470169984 *, const MethodInfo*))ComparerBaseGeneric_2_get_UseCache_m3970344979_gshared)(__this, method)
