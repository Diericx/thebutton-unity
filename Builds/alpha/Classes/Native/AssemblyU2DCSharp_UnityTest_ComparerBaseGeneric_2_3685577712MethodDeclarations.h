﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>
struct ComparerBaseGeneric_2_t3685577712;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1664964607;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>::.ctor()
extern "C"  void ComparerBaseGeneric_2__ctor_m3916373049_gshared (ComparerBaseGeneric_2_t3685577712 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2__ctor_m3916373049(__this, method) ((  void (*) (ComparerBaseGeneric_2_t3685577712 *, const MethodInfo*))ComparerBaseGeneric_2__ctor_m3916373049_gshared)(__this, method)
// System.Object UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>::get_ConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_get_ConstValue_m2618265451_gshared (ComparerBaseGeneric_2_t3685577712 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_get_ConstValue_m2618265451(__this, method) ((  Il2CppObject * (*) (ComparerBaseGeneric_2_t3685577712 *, const MethodInfo*))ComparerBaseGeneric_2_get_ConstValue_m2618265451_gshared)(__this, method)
// System.Void UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>::set_ConstValue(System.Object)
extern "C"  void ComparerBaseGeneric_2_set_ConstValue_m2763752166_gshared (ComparerBaseGeneric_2_t3685577712 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ComparerBaseGeneric_2_set_ConstValue_m2763752166(__this, ___value0, method) ((  void (*) (ComparerBaseGeneric_2_t3685577712 *, Il2CppObject *, const MethodInfo*))ComparerBaseGeneric_2_set_ConstValue_m2763752166_gshared)(__this, ___value0, method)
// System.Object UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>::GetDefaultConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_GetDefaultConstValue_m28733447_gshared (ComparerBaseGeneric_2_t3685577712 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetDefaultConstValue_m28733447(__this, method) ((  Il2CppObject * (*) (ComparerBaseGeneric_2_t3685577712 *, const MethodInfo*))ComparerBaseGeneric_2_GetDefaultConstValue_m28733447_gshared)(__this, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>::IsValueType(System.Type)
extern "C"  bool ComparerBaseGeneric_2_IsValueType_m1921309075_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method);
#define ComparerBaseGeneric_2_IsValueType_m1921309075(__this /* static, unused */, ___type0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))ComparerBaseGeneric_2_IsValueType_m1921309075_gshared)(__this /* static, unused */, ___type0, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>::Compare(System.Object,System.Object)
extern "C"  bool ComparerBaseGeneric_2_Compare_m1681931688_gshared (ComparerBaseGeneric_2_t3685577712 * __this, Il2CppObject * ___a0, Il2CppObject * ___b1, const MethodInfo* method);
#define ComparerBaseGeneric_2_Compare_m1681931688(__this, ___a0, ___b1, method) ((  bool (*) (ComparerBaseGeneric_2_t3685577712 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ComparerBaseGeneric_2_Compare_m1681931688_gshared)(__this, ___a0, ___b1, method)
// System.Type[] UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>::GetAccepatbleTypesForA()
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForA_m512542484_gshared (ComparerBaseGeneric_2_t3685577712 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetAccepatbleTypesForA_m512542484(__this, method) ((  TypeU5BU5D_t1664964607* (*) (ComparerBaseGeneric_2_t3685577712 *, const MethodInfo*))ComparerBaseGeneric_2_GetAccepatbleTypesForA_m512542484_gshared)(__this, method)
// System.Type[] UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>::GetAccepatbleTypesForB()
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForB_m653704985_gshared (ComparerBaseGeneric_2_t3685577712 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetAccepatbleTypesForB_m653704985(__this, method) ((  TypeU5BU5D_t1664964607* (*) (ComparerBaseGeneric_2_t3685577712 *, const MethodInfo*))ComparerBaseGeneric_2_GetAccepatbleTypesForB_m653704985_gshared)(__this, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>::get_UseCache()
extern "C"  bool ComparerBaseGeneric_2_get_UseCache_m138643099_gshared (ComparerBaseGeneric_2_t3685577712 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_get_UseCache_m138643099(__this, method) ((  bool (*) (ComparerBaseGeneric_2_t3685577712 *, const MethodInfo*))ComparerBaseGeneric_2_get_UseCache_m138643099_gshared)(__this, method)
