﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>
struct ComparerBaseGeneric_2_t3854387844;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1664964607;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>::.ctor()
extern "C"  void ComparerBaseGeneric_2__ctor_m1420920721_gshared (ComparerBaseGeneric_2_t3854387844 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2__ctor_m1420920721(__this, method) ((  void (*) (ComparerBaseGeneric_2_t3854387844 *, const MethodInfo*))ComparerBaseGeneric_2__ctor_m1420920721_gshared)(__this, method)
// System.Object UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>::get_ConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_get_ConstValue_m2932615231_gshared (ComparerBaseGeneric_2_t3854387844 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_get_ConstValue_m2932615231(__this, method) ((  Il2CppObject * (*) (ComparerBaseGeneric_2_t3854387844 *, const MethodInfo*))ComparerBaseGeneric_2_get_ConstValue_m2932615231_gshared)(__this, method)
// System.Void UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>::set_ConstValue(System.Object)
extern "C"  void ComparerBaseGeneric_2_set_ConstValue_m2091596084_gshared (ComparerBaseGeneric_2_t3854387844 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ComparerBaseGeneric_2_set_ConstValue_m2091596084(__this, ___value0, method) ((  void (*) (ComparerBaseGeneric_2_t3854387844 *, Il2CppObject *, const MethodInfo*))ComparerBaseGeneric_2_set_ConstValue_m2091596084_gshared)(__this, ___value0, method)
// System.Object UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>::GetDefaultConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_GetDefaultConstValue_m2891031835_gshared (ComparerBaseGeneric_2_t3854387844 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetDefaultConstValue_m2891031835(__this, method) ((  Il2CppObject * (*) (ComparerBaseGeneric_2_t3854387844 *, const MethodInfo*))ComparerBaseGeneric_2_GetDefaultConstValue_m2891031835_gshared)(__this, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>::IsValueType(System.Type)
extern "C"  bool ComparerBaseGeneric_2_IsValueType_m2902129951_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method);
#define ComparerBaseGeneric_2_IsValueType_m2902129951(__this /* static, unused */, ___type0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))ComparerBaseGeneric_2_IsValueType_m2902129951_gshared)(__this /* static, unused */, ___type0, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>::Compare(System.Object,System.Object)
extern "C"  bool ComparerBaseGeneric_2_Compare_m1255210266_gshared (ComparerBaseGeneric_2_t3854387844 * __this, Il2CppObject * ___a0, Il2CppObject * ___b1, const MethodInfo* method);
#define ComparerBaseGeneric_2_Compare_m1255210266(__this, ___a0, ___b1, method) ((  bool (*) (ComparerBaseGeneric_2_t3854387844 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ComparerBaseGeneric_2_Compare_m1255210266_gshared)(__this, ___a0, ___b1, method)
// System.Type[] UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>::GetAccepatbleTypesForA()
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForA_m2700426262_gshared (ComparerBaseGeneric_2_t3854387844 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetAccepatbleTypesForA_m2700426262(__this, method) ((  TypeU5BU5D_t1664964607* (*) (ComparerBaseGeneric_2_t3854387844 *, const MethodInfo*))ComparerBaseGeneric_2_GetAccepatbleTypesForA_m2700426262_gshared)(__this, method)
// System.Type[] UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>::GetAccepatbleTypesForB()
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForB_m2559263761_gshared (ComparerBaseGeneric_2_t3854387844 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetAccepatbleTypesForB_m2559263761(__this, method) ((  TypeU5BU5D_t1664964607* (*) (ComparerBaseGeneric_2_t3854387844 *, const MethodInfo*))ComparerBaseGeneric_2_GetAccepatbleTypesForB_m2559263761_gshared)(__this, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>::get_UseCache()
extern "C"  bool ComparerBaseGeneric_2_get_UseCache_m21161807_gshared (ComparerBaseGeneric_2_t3854387844 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_get_UseCache_m21161807(__this, method) ((  bool (*) (ComparerBaseGeneric_2_t3854387844 *, const MethodInfo*))ComparerBaseGeneric_2_get_UseCache_m21161807_gshared)(__this, method)
