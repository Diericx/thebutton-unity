﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_UnityTest_ComparerBase429484414.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>
struct  ComparerBaseGeneric_2_t4171765452  : public ComparerBase_t429484414
{
public:
	// T2 UnityTest.ComparerBaseGeneric`2::constantValueGeneric
	Vector3_t2243707580  ___constantValueGeneric_13;

public:
	inline static int32_t get_offset_of_constantValueGeneric_13() { return static_cast<int32_t>(offsetof(ComparerBaseGeneric_2_t4171765452, ___constantValueGeneric_13)); }
	inline Vector3_t2243707580  get_constantValueGeneric_13() const { return ___constantValueGeneric_13; }
	inline Vector3_t2243707580 * get_address_of_constantValueGeneric_13() { return &___constantValueGeneric_13; }
	inline void set_constantValueGeneric_13(Vector3_t2243707580  value)
	{
		___constantValueGeneric_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
