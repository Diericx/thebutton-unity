﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>
struct ComparerBaseGeneric_2_t4171765452;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1664964607;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>::.ctor()
extern "C"  void ComparerBaseGeneric_2__ctor_m2686062777_gshared (ComparerBaseGeneric_2_t4171765452 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2__ctor_m2686062777(__this, method) ((  void (*) (ComparerBaseGeneric_2_t4171765452 *, const MethodInfo*))ComparerBaseGeneric_2__ctor_m2686062777_gshared)(__this, method)
// System.Object UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>::get_ConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_get_ConstValue_m759497451_gshared (ComparerBaseGeneric_2_t4171765452 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_get_ConstValue_m759497451(__this, method) ((  Il2CppObject * (*) (ComparerBaseGeneric_2_t4171765452 *, const MethodInfo*))ComparerBaseGeneric_2_get_ConstValue_m759497451_gshared)(__this, method)
// System.Void UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>::set_ConstValue(System.Object)
extern "C"  void ComparerBaseGeneric_2_set_ConstValue_m198485862_gshared (ComparerBaseGeneric_2_t4171765452 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ComparerBaseGeneric_2_set_ConstValue_m198485862(__this, ___value0, method) ((  void (*) (ComparerBaseGeneric_2_t4171765452 *, Il2CppObject *, const MethodInfo*))ComparerBaseGeneric_2_set_ConstValue_m198485862_gshared)(__this, ___value0, method)
// System.Object UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>::GetDefaultConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_GetDefaultConstValue_m1109472135_gshared (ComparerBaseGeneric_2_t4171765452 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetDefaultConstValue_m1109472135(__this, method) ((  Il2CppObject * (*) (ComparerBaseGeneric_2_t4171765452 *, const MethodInfo*))ComparerBaseGeneric_2_GetDefaultConstValue_m1109472135_gshared)(__this, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>::IsValueType(System.Type)
extern "C"  bool ComparerBaseGeneric_2_IsValueType_m3061251603_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method);
#define ComparerBaseGeneric_2_IsValueType_m3061251603(__this /* static, unused */, ___type0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))ComparerBaseGeneric_2_IsValueType_m3061251603_gshared)(__this /* static, unused */, ___type0, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>::Compare(System.Object,System.Object)
extern "C"  bool ComparerBaseGeneric_2_Compare_m56345128_gshared (ComparerBaseGeneric_2_t4171765452 * __this, Il2CppObject * ___a0, Il2CppObject * ___b1, const MethodInfo* method);
#define ComparerBaseGeneric_2_Compare_m56345128(__this, ___a0, ___b1, method) ((  bool (*) (ComparerBaseGeneric_2_t4171765452 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ComparerBaseGeneric_2_Compare_m56345128_gshared)(__this, ___a0, ___b1, method)
// System.Type[] UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>::GetAccepatbleTypesForA()
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForA_m2871835540_gshared (ComparerBaseGeneric_2_t4171765452 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetAccepatbleTypesForA_m2871835540(__this, method) ((  TypeU5BU5D_t1664964607* (*) (ComparerBaseGeneric_2_t4171765452 *, const MethodInfo*))ComparerBaseGeneric_2_GetAccepatbleTypesForA_m2871835540_gshared)(__this, method)
// System.Type[] UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>::GetAccepatbleTypesForB()
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForB_m3012998041_gshared (ComparerBaseGeneric_2_t4171765452 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetAccepatbleTypesForB_m3012998041(__this, method) ((  TypeU5BU5D_t1664964607* (*) (ComparerBaseGeneric_2_t4171765452 *, const MethodInfo*))ComparerBaseGeneric_2_GetAccepatbleTypesForB_m3012998041_gshared)(__this, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>::get_UseCache()
extern "C"  bool ComparerBaseGeneric_2_get_UseCache_m3333217051_gshared (ComparerBaseGeneric_2_t4171765452 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_get_UseCache_m3333217051(__this, method) ((  bool (*) (ComparerBaseGeneric_2_t4171765452 *, const MethodInfo*))ComparerBaseGeneric_2_get_UseCache_m3333217051_gshared)(__this, method)
