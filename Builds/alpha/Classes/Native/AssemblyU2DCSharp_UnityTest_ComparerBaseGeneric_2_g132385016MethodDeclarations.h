﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_2_2764862240MethodDeclarations.h"

// System.Void UnityTest.ComparerBaseGeneric`2<System.String,System.String>::.ctor()
#define ComparerBaseGeneric_2__ctor_m145357689(__this, method) ((  void (*) (ComparerBaseGeneric_2_t132385016 *, const MethodInfo*))ComparerBaseGeneric_2__ctor_m2944922553_gshared)(__this, method)
// System.Object UnityTest.ComparerBaseGeneric`2<System.String,System.String>::get_ConstValue()
#define ComparerBaseGeneric_2_get_ConstValue_m117201707(__this, method) ((  Il2CppObject * (*) (ComparerBaseGeneric_2_t132385016 *, const MethodInfo*))ComparerBaseGeneric_2_get_ConstValue_m3589533995_gshared)(__this, method)
// System.Void UnityTest.ComparerBaseGeneric`2<System.String,System.String>::set_ConstValue(System.Object)
#define ComparerBaseGeneric_2_set_ConstValue_m2167201382(__this, ___value0, method) ((  void (*) (ComparerBaseGeneric_2_t132385016 *, Il2CppObject *, const MethodInfo*))ComparerBaseGeneric_2_set_ConstValue_m1223600742_gshared)(__this, ___value0, method)
// System.Object UnityTest.ComparerBaseGeneric`2<System.String,System.String>::GetDefaultConstValue()
#define ComparerBaseGeneric_2_GetDefaultConstValue_m3062292999(__this, method) ((  Il2CppObject * (*) (ComparerBaseGeneric_2_t132385016 *, const MethodInfo*))ComparerBaseGeneric_2_GetDefaultConstValue_m4190662983_gshared)(__this, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.String,System.String>::IsValueType(System.Type)
#define ComparerBaseGeneric_2_IsValueType_m1513525203(__this /* static, unused */, ___type0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))ComparerBaseGeneric_2_IsValueType_m3892472595_gshared)(__this /* static, unused */, ___type0, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.String,System.String>::Compare(System.Object,System.Object)
#define ComparerBaseGeneric_2_Compare_m1131723880(__this, ___a0, ___b1, method) ((  bool (*) (ComparerBaseGeneric_2_t132385016 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ComparerBaseGeneric_2_Compare_m3999256872_gshared)(__this, ___a0, ___b1, method)
// System.Type[] UnityTest.ComparerBaseGeneric`2<System.String,System.String>::GetAccepatbleTypesForA()
#define ComparerBaseGeneric_2_GetAccepatbleTypesForA_m2383521620(__this, method) ((  TypeU5BU5D_t1664964607* (*) (ComparerBaseGeneric_2_t132385016 *, const MethodInfo*))ComparerBaseGeneric_2_GetAccepatbleTypesForA_m3612969492_gshared)(__this, method)
// System.Type[] UnityTest.ComparerBaseGeneric`2<System.String,System.String>::GetAccepatbleTypesForB()
#define ComparerBaseGeneric_2_GetAccepatbleTypesForB_m1119965657(__this, method) ((  TypeU5BU5D_t1664964607* (*) (ComparerBaseGeneric_2_t132385016 *, const MethodInfo*))ComparerBaseGeneric_2_GetAccepatbleTypesForB_m3754131993_gshared)(__this, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.String,System.String>::get_UseCache()
#define ComparerBaseGeneric_2_get_UseCache_m1000029147(__this, method) ((  bool (*) (ComparerBaseGeneric_2_t132385016 *, const MethodInfo*))ComparerBaseGeneric_2_get_UseCache_m3855553179_gshared)(__this, method)
