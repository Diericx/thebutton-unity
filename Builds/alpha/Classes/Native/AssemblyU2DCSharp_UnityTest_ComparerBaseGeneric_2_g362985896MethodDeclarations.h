﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>
struct ComparerBaseGeneric_2_t362985896;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1664964607;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>::.ctor()
extern "C"  void ComparerBaseGeneric_2__ctor_m509928505_gshared (ComparerBaseGeneric_2_t362985896 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2__ctor_m509928505(__this, method) ((  void (*) (ComparerBaseGeneric_2_t362985896 *, const MethodInfo*))ComparerBaseGeneric_2__ctor_m509928505_gshared)(__this, method)
// System.Object UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>::get_ConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_get_ConstValue_m3792050027_gshared (ComparerBaseGeneric_2_t362985896 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_get_ConstValue_m3792050027(__this, method) ((  Il2CppObject * (*) (ComparerBaseGeneric_2_t362985896 *, const MethodInfo*))ComparerBaseGeneric_2_get_ConstValue_m3792050027_gshared)(__this, method)
// System.Void UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>::set_ConstValue(System.Object)
extern "C"  void ComparerBaseGeneric_2_set_ConstValue_m3615771366_gshared (ComparerBaseGeneric_2_t362985896 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ComparerBaseGeneric_2_set_ConstValue_m3615771366(__this, ___value0, method) ((  void (*) (ComparerBaseGeneric_2_t362985896 *, Il2CppObject *, const MethodInfo*))ComparerBaseGeneric_2_set_ConstValue_m3615771366_gshared)(__this, ___value0, method)
// System.Object UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>::GetDefaultConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_GetDefaultConstValue_m1451227143_gshared (ComparerBaseGeneric_2_t362985896 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetDefaultConstValue_m1451227143(__this, method) ((  Il2CppObject * (*) (ComparerBaseGeneric_2_t362985896 *, const MethodInfo*))ComparerBaseGeneric_2_GetDefaultConstValue_m1451227143_gshared)(__this, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>::IsValueType(System.Type)
extern "C"  bool ComparerBaseGeneric_2_IsValueType_m3488305555_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method);
#define ComparerBaseGeneric_2_IsValueType_m3488305555(__this /* static, unused */, ___type0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))ComparerBaseGeneric_2_IsValueType_m3488305555_gshared)(__this /* static, unused */, ___type0, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>::Compare(System.Object,System.Object)
extern "C"  bool ComparerBaseGeneric_2_Compare_m44086696_gshared (ComparerBaseGeneric_2_t362985896 * __this, Il2CppObject * ___a0, Il2CppObject * ___b1, const MethodInfo* method);
#define ComparerBaseGeneric_2_Compare_m44086696(__this, ___a0, ___b1, method) ((  bool (*) (ComparerBaseGeneric_2_t362985896 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))ComparerBaseGeneric_2_Compare_m44086696_gshared)(__this, ___a0, ___b1, method)
// System.Type[] UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>::GetAccepatbleTypesForA()
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForA_m3986120724_gshared (ComparerBaseGeneric_2_t362985896 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetAccepatbleTypesForA_m3986120724(__this, method) ((  TypeU5BU5D_t1664964607* (*) (ComparerBaseGeneric_2_t362985896 *, const MethodInfo*))ComparerBaseGeneric_2_GetAccepatbleTypesForA_m3986120724_gshared)(__this, method)
// System.Type[] UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>::GetAccepatbleTypesForB()
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForB_m4127283225_gshared (ComparerBaseGeneric_2_t362985896 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_GetAccepatbleTypesForB_m4127283225(__this, method) ((  TypeU5BU5D_t1664964607* (*) (ComparerBaseGeneric_2_t362985896 *, const MethodInfo*))ComparerBaseGeneric_2_GetAccepatbleTypesForB_m4127283225_gshared)(__this, method)
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>::get_UseCache()
extern "C"  bool ComparerBaseGeneric_2_get_UseCache_m3586064027_gshared (ComparerBaseGeneric_2_t362985896 * __this, const MethodInfo* method);
#define ComparerBaseGeneric_2_get_UseCache_m3586064027(__this, method) ((  bool (*) (ComparerBaseGeneric_2_t362985896 *, const MethodInfo*))ComparerBaseGeneric_2_get_UseCache_m3586064027_gshared)(__this, method)
