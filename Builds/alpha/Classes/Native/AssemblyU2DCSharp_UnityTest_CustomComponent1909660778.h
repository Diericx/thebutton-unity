﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityTest.CustomComponent
struct  CustomComponent_t1909660778  : public MonoBehaviour_t1158329972
{
public:
	// System.Single UnityTest.CustomComponent::<MyFloatProp>k__BackingField
	float ___U3CMyFloatPropU3Ek__BackingField_2;
	// System.Single UnityTest.CustomComponent::MyFloatField
	float ___MyFloatField_3;

public:
	inline static int32_t get_offset_of_U3CMyFloatPropU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CustomComponent_t1909660778, ___U3CMyFloatPropU3Ek__BackingField_2)); }
	inline float get_U3CMyFloatPropU3Ek__BackingField_2() const { return ___U3CMyFloatPropU3Ek__BackingField_2; }
	inline float* get_address_of_U3CMyFloatPropU3Ek__BackingField_2() { return &___U3CMyFloatPropU3Ek__BackingField_2; }
	inline void set_U3CMyFloatPropU3Ek__BackingField_2(float value)
	{
		___U3CMyFloatPropU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_MyFloatField_3() { return static_cast<int32_t>(offsetof(CustomComponent_t1909660778, ___MyFloatField_3)); }
	inline float get_MyFloatField_3() const { return ___MyFloatField_3; }
	inline float* get_address_of_MyFloatField_3() { return &___MyFloatField_3; }
	inline void set_MyFloatField_3(float value)
	{
		___MyFloatField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
