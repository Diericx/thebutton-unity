﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.CustomComponent
struct CustomComponent_t1909660778;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityTest.CustomComponent::.ctor()
extern "C"  void CustomComponent__ctor_m3578741052 (CustomComponent_t1909660778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityTest.CustomComponent::get_MyFloatProp()
extern "C"  float CustomComponent_get_MyFloatProp_m1553565876 (CustomComponent_t1909660778 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.CustomComponent::set_MyFloatProp(System.Single)
extern "C"  void CustomComponent_set_MyFloatProp_m662839037 (CustomComponent_t1909660778 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
