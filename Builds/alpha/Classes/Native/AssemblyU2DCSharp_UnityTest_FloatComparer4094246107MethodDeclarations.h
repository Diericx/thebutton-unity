﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.FloatComparer
struct FloatComparer_t4094246107;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityTest.FloatComparer::.ctor()
extern "C"  void FloatComparer__ctor_m3775207973 (FloatComparer_t4094246107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.FloatComparer::Compare(System.Single,System.Single)
extern "C"  bool FloatComparer_Compare_m582259994 (FloatComparer_t4094246107 * __this, float ___a0, float ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityTest.FloatComparer::GetDepthOfSearch()
extern "C"  int32_t FloatComparer_GetDepthOfSearch_m629942395 (FloatComparer_t4094246107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
