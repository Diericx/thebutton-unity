﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_UnityTest_ComparerBase429484414.h"
#include "AssemblyU2DCSharp_UnityTest_GeneralComparer_Compar3001481952.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityTest.GeneralComparer
struct  GeneralComparer_t3787245533  : public ComparerBase_t429484414
{
public:
	// UnityTest.GeneralComparer/CompareType UnityTest.GeneralComparer::compareType
	int32_t ___compareType_13;

public:
	inline static int32_t get_offset_of_compareType_13() { return static_cast<int32_t>(offsetof(GeneralComparer_t3787245533, ___compareType_13)); }
	inline int32_t get_compareType_13() const { return ___compareType_13; }
	inline int32_t* get_address_of_compareType_13() { return &___compareType_13; }
	inline void set_compareType_13(int32_t value)
	{
		___compareType_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
