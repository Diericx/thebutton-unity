﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.GeneralComparer
struct GeneralComparer_t3787245533;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void UnityTest.GeneralComparer::.ctor()
extern "C"  void GeneralComparer__ctor_m678995171 (GeneralComparer_t3787245533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.GeneralComparer::Compare(System.Object,System.Object)
extern "C"  bool GeneralComparer_Compare_m2941000012 (GeneralComparer_t3787245533 * __this, Il2CppObject * ___a0, Il2CppObject * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
