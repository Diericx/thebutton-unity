﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.IntComparer
struct IntComparer_t2524366688;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityTest.IntComparer::.ctor()
extern "C"  void IntComparer__ctor_m1695062740 (IntComparer_t2524366688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.IntComparer::Compare(System.Int32,System.Int32)
extern "C"  bool IntComparer_Compare_m2438751465 (IntComparer_t2524366688 * __this, int32_t ___a0, int32_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
