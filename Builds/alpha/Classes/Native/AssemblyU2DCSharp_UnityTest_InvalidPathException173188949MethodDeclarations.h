﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.InvalidPathException
struct InvalidPathException_t173188949;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityTest.InvalidPathException::.ctor(System.String)
extern "C"  void InvalidPathException__ctor_m693152789 (InvalidPathException_t173188949 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
