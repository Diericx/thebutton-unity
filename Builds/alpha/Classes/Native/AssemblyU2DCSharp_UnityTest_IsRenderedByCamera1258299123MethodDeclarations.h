﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.IsRenderedByCamera
struct IsRenderedByCamera_t1258299123;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.Camera
struct Camera_t189460977;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"

// System.Void UnityTest.IsRenderedByCamera::.ctor()
extern "C"  void IsRenderedByCamera__ctor_m618912083 (IsRenderedByCamera_t1258299123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.IsRenderedByCamera::Compare(UnityEngine.Renderer,UnityEngine.Camera)
extern "C"  bool IsRenderedByCamera_Compare_m2087392594 (IsRenderedByCamera_t1258299123 * __this, Renderer_t257310565 * ___renderer0, Camera_t189460977 * ___camera1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
