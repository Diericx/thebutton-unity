﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.SerializableTestResult
struct SerializableTestResult_t3764870664;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UnityTest_TestResultState1620411406.h"

// System.Void UnityTest.SerializableTestResult::.ctor()
extern "C"  void SerializableTestResult__ctor_m2745568958 (SerializableTestResult_t3764870664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityTest.TestResultState UnityTest.SerializableTestResult::get_ResultState()
extern "C"  uint8_t SerializableTestResult_get_ResultState_m1796160553 (SerializableTestResult_t3764870664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityTest.SerializableTestResult::get_Message()
extern "C"  String_t* SerializableTestResult_get_Message_m3387492869 (SerializableTestResult_t3764870664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityTest.SerializableTestResult::get_Logs()
extern "C"  String_t* SerializableTestResult_get_Logs_m2350007793 (SerializableTestResult_t3764870664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.SerializableTestResult::get_Executed()
extern "C"  bool SerializableTestResult_get_Executed_m144321670 (SerializableTestResult_t3764870664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityTest.SerializableTestResult::get_Name()
extern "C"  String_t* SerializableTestResult_get_Name_m1710937633 (SerializableTestResult_t3764870664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityTest.SerializableTestResult::get_FullName()
extern "C"  String_t* SerializableTestResult_get_FullName_m3690374446 (SerializableTestResult_t3764870664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityTest.SerializableTestResult::get_Id()
extern "C"  String_t* SerializableTestResult_get_Id_m200975451 (SerializableTestResult_t3764870664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.SerializableTestResult::get_IsSuccess()
extern "C"  bool SerializableTestResult_get_IsSuccess_m746321624 (SerializableTestResult_t3764870664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double UnityTest.SerializableTestResult::get_Duration()
extern "C"  double SerializableTestResult_get_Duration_m315972244 (SerializableTestResult_t3764870664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityTest.SerializableTestResult::get_StackTrace()
extern "C"  String_t* SerializableTestResult_get_StackTrace_m3602123585 (SerializableTestResult_t3764870664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.SerializableTestResult::get_IsIgnored()
extern "C"  bool SerializableTestResult_get_IsIgnored_m2722543677 (SerializableTestResult_t3764870664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
