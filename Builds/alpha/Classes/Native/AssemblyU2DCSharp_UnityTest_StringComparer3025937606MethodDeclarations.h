﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.StringComparer
struct StringComparer_t3025937606;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityTest.StringComparer::.ctor()
extern "C"  void StringComparer__ctor_m1120170242 (StringComparer_t3025937606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.StringComparer::Compare(System.String,System.String)
extern "C"  bool StringComparer_Compare_m1216842059 (StringComparer_t3025937606 * __this, String_t* ___a0, String_t* ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
