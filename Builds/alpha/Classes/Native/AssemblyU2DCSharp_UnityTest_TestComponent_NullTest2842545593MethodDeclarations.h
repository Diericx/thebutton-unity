﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.TestComponent/NullTestComponentImpl
struct NullTestComponentImpl_t2842545593;
// UnityTest.ITestComponent
struct ITestComponent_t2920761518;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityTest.TestComponent/NullTestComponentImpl::.ctor()
extern "C"  void NullTestComponentImpl__ctor_m102519580 (NullTestComponentImpl_t2842545593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityTest.TestComponent/NullTestComponentImpl::CompareTo(UnityTest.ITestComponent)
extern "C"  int32_t NullTestComponentImpl_CompareTo_m3122035397 (NullTestComponentImpl_t2842545593 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.TestComponent/NullTestComponentImpl::EnableTest(System.Boolean)
extern "C"  void NullTestComponentImpl_EnableTest_m3025548296 (NullTestComponentImpl_t2842545593 * __this, bool ___enable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.TestComponent/NullTestComponentImpl::IsTestGroup()
extern "C"  bool NullTestComponentImpl_IsTestGroup_m1361786579 (NullTestComponentImpl_t2842545593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityTest.TestComponent/NullTestComponentImpl::get_gameObject()
extern "C"  GameObject_t1756533147 * NullTestComponentImpl_get_gameObject_m2806918845 (NullTestComponentImpl_t2842545593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.TestComponent/NullTestComponentImpl::set_gameObject(UnityEngine.GameObject)
extern "C"  void NullTestComponentImpl_set_gameObject_m427609616 (NullTestComponentImpl_t2842545593 * __this, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityTest.TestComponent/NullTestComponentImpl::get_Name()
extern "C"  String_t* NullTestComponentImpl_get_Name_m1509109323 (NullTestComponentImpl_t2842545593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityTest.ITestComponent UnityTest.TestComponent/NullTestComponentImpl::GetTestGroup()
extern "C"  Il2CppObject * NullTestComponentImpl_GetTestGroup_m3736684951 (NullTestComponentImpl_t2842545593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.TestComponent/NullTestComponentImpl::IsExceptionExpected(System.String)
extern "C"  bool NullTestComponentImpl_IsExceptionExpected_m1956661669 (NullTestComponentImpl_t2842545593 * __this, String_t* ___exceptionType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.TestComponent/NullTestComponentImpl::ShouldSucceedOnException()
extern "C"  bool NullTestComponentImpl_ShouldSucceedOnException_m3622388769 (NullTestComponentImpl_t2842545593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double UnityTest.TestComponent/NullTestComponentImpl::GetTimeout()
extern "C"  double NullTestComponentImpl_GetTimeout_m3785317274 (NullTestComponentImpl_t2842545593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.TestComponent/NullTestComponentImpl::IsIgnored()
extern "C"  bool NullTestComponentImpl_IsIgnored_m3125393572 (NullTestComponentImpl_t2842545593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.TestComponent/NullTestComponentImpl::ShouldSucceedOnAssertions()
extern "C"  bool NullTestComponentImpl_ShouldSucceedOnAssertions_m100223911 (NullTestComponentImpl_t2842545593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.TestComponent/NullTestComponentImpl::IsExludedOnThisPlatform()
extern "C"  bool NullTestComponentImpl_IsExludedOnThisPlatform_m3334361479 (NullTestComponentImpl_t2842545593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
