﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.TestComponent/<GetTypeByName>c__AnonStorey1
struct U3CGetTypeByNameU3Ec__AnonStorey1_t2811533093;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void UnityTest.TestComponent/<GetTypeByName>c__AnonStorey1::.ctor()
extern "C"  void U3CGetTypeByNameU3Ec__AnonStorey1__ctor_m3583907470 (U3CGetTypeByNameU3Ec__AnonStorey1_t2811533093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.TestComponent/<GetTypeByName>c__AnonStorey1::<>m__0(System.Type)
extern "C"  bool U3CGetTypeByNameU3Ec__AnonStorey1_U3CU3Em__0_m2219179248 (U3CGetTypeByNameU3Ec__AnonStorey1_t2811533093 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
