﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.Assembly[]
struct AssemblyU5BU5D_t1984278467;
// System.Reflection.Assembly
struct Assembly_t4268412390;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Type
struct Type_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// IntegrationTest/DynamicTestAttribute
struct DynamicTestAttribute_t2184122240;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0
struct  U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460  : public Il2CppObject
{
public:
	// System.Reflection.Assembly[] UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::$locvar0
	AssemblyU5BU5D_t1984278467* ___U24locvar0_0;
	// System.Int32 UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::$locvar1
	int32_t ___U24locvar1_1;
	// System.Reflection.Assembly UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::<assembly>__0
	Assembly_t4268412390 * ___U3CassemblyU3E__0_2;
	// System.Type[] UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::<types>__1
	TypeU5BU5D_t1664964607* ___U3CtypesU3E__1_3;
	// System.Type[] UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::$locvar4
	TypeU5BU5D_t1664964607* ___U24locvar4_4;
	// System.Int32 UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::$locvar5
	int32_t ___U24locvar5_5;
	// System.Type UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::<type>__2
	Type_t * ___U3CtypeU3E__2_6;
	// System.Object[] UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::<attributes>__3
	ObjectU5BU5D_t3614634134* ___U3CattributesU3E__3_7;
	// IntegrationTest/DynamicTestAttribute UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::<a>__4
	DynamicTestAttribute_t2184122240 * ___U3CaU3E__4_8;
	// System.String UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::sceneName
	String_t* ___sceneName_9;
	// System.Type UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::$current
	Type_t * ___U24current_10;
	// System.Boolean UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::$disposing
	bool ___U24disposing_11;
	// System.Int32 UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460, ___U24locvar0_0)); }
	inline AssemblyU5BU5D_t1984278467* get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline AssemblyU5BU5D_t1984278467** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(AssemblyU5BU5D_t1984278467* value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_0, value);
	}

	inline static int32_t get_offset_of_U24locvar1_1() { return static_cast<int32_t>(offsetof(U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460, ___U24locvar1_1)); }
	inline int32_t get_U24locvar1_1() const { return ___U24locvar1_1; }
	inline int32_t* get_address_of_U24locvar1_1() { return &___U24locvar1_1; }
	inline void set_U24locvar1_1(int32_t value)
	{
		___U24locvar1_1 = value;
	}

	inline static int32_t get_offset_of_U3CassemblyU3E__0_2() { return static_cast<int32_t>(offsetof(U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460, ___U3CassemblyU3E__0_2)); }
	inline Assembly_t4268412390 * get_U3CassemblyU3E__0_2() const { return ___U3CassemblyU3E__0_2; }
	inline Assembly_t4268412390 ** get_address_of_U3CassemblyU3E__0_2() { return &___U3CassemblyU3E__0_2; }
	inline void set_U3CassemblyU3E__0_2(Assembly_t4268412390 * value)
	{
		___U3CassemblyU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CassemblyU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3CtypesU3E__1_3() { return static_cast<int32_t>(offsetof(U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460, ___U3CtypesU3E__1_3)); }
	inline TypeU5BU5D_t1664964607* get_U3CtypesU3E__1_3() const { return ___U3CtypesU3E__1_3; }
	inline TypeU5BU5D_t1664964607** get_address_of_U3CtypesU3E__1_3() { return &___U3CtypesU3E__1_3; }
	inline void set_U3CtypesU3E__1_3(TypeU5BU5D_t1664964607* value)
	{
		___U3CtypesU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtypesU3E__1_3, value);
	}

	inline static int32_t get_offset_of_U24locvar4_4() { return static_cast<int32_t>(offsetof(U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460, ___U24locvar4_4)); }
	inline TypeU5BU5D_t1664964607* get_U24locvar4_4() const { return ___U24locvar4_4; }
	inline TypeU5BU5D_t1664964607** get_address_of_U24locvar4_4() { return &___U24locvar4_4; }
	inline void set_U24locvar4_4(TypeU5BU5D_t1664964607* value)
	{
		___U24locvar4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar4_4, value);
	}

	inline static int32_t get_offset_of_U24locvar5_5() { return static_cast<int32_t>(offsetof(U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460, ___U24locvar5_5)); }
	inline int32_t get_U24locvar5_5() const { return ___U24locvar5_5; }
	inline int32_t* get_address_of_U24locvar5_5() { return &___U24locvar5_5; }
	inline void set_U24locvar5_5(int32_t value)
	{
		___U24locvar5_5 = value;
	}

	inline static int32_t get_offset_of_U3CtypeU3E__2_6() { return static_cast<int32_t>(offsetof(U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460, ___U3CtypeU3E__2_6)); }
	inline Type_t * get_U3CtypeU3E__2_6() const { return ___U3CtypeU3E__2_6; }
	inline Type_t ** get_address_of_U3CtypeU3E__2_6() { return &___U3CtypeU3E__2_6; }
	inline void set_U3CtypeU3E__2_6(Type_t * value)
	{
		___U3CtypeU3E__2_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtypeU3E__2_6, value);
	}

	inline static int32_t get_offset_of_U3CattributesU3E__3_7() { return static_cast<int32_t>(offsetof(U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460, ___U3CattributesU3E__3_7)); }
	inline ObjectU5BU5D_t3614634134* get_U3CattributesU3E__3_7() const { return ___U3CattributesU3E__3_7; }
	inline ObjectU5BU5D_t3614634134** get_address_of_U3CattributesU3E__3_7() { return &___U3CattributesU3E__3_7; }
	inline void set_U3CattributesU3E__3_7(ObjectU5BU5D_t3614634134* value)
	{
		___U3CattributesU3E__3_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CattributesU3E__3_7, value);
	}

	inline static int32_t get_offset_of_U3CaU3E__4_8() { return static_cast<int32_t>(offsetof(U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460, ___U3CaU3E__4_8)); }
	inline DynamicTestAttribute_t2184122240 * get_U3CaU3E__4_8() const { return ___U3CaU3E__4_8; }
	inline DynamicTestAttribute_t2184122240 ** get_address_of_U3CaU3E__4_8() { return &___U3CaU3E__4_8; }
	inline void set_U3CaU3E__4_8(DynamicTestAttribute_t2184122240 * value)
	{
		___U3CaU3E__4_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CaU3E__4_8, value);
	}

	inline static int32_t get_offset_of_sceneName_9() { return static_cast<int32_t>(offsetof(U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460, ___sceneName_9)); }
	inline String_t* get_sceneName_9() const { return ___sceneName_9; }
	inline String_t** get_address_of_sceneName_9() { return &___sceneName_9; }
	inline void set_sceneName_9(String_t* value)
	{
		___sceneName_9 = value;
		Il2CppCodeGenWriteBarrier(&___sceneName_9, value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460, ___U24current_10)); }
	inline Type_t * get_U24current_10() const { return ___U24current_10; }
	inline Type_t ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(Type_t * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_10, value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
