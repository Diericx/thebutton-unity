﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0
struct U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t3074294349;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::.ctor()
extern "C"  void U3CGetTypesWithHelpAttributeU3Ec__Iterator0__ctor_m2566897407 (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::MoveNext()
extern "C"  bool U3CGetTypesWithHelpAttributeU3Ec__Iterator0_MoveNext_m3800108549 (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::System.Collections.Generic.IEnumerator<System.Type>.get_Current()
extern "C"  Type_t * U3CGetTypesWithHelpAttributeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m3074508642 (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetTypesWithHelpAttributeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2690547165 (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::Dispose()
extern "C"  void U3CGetTypesWithHelpAttributeU3Ec__Iterator0_Dispose_m3860413638 (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::Reset()
extern "C"  void U3CGetTypesWithHelpAttributeU3Ec__Iterator0_Reset_m3258660308 (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetTypesWithHelpAttributeU3Ec__Iterator0_System_Collections_IEnumerable_GetEnumerator_m1831714810 (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Type> UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::System.Collections.Generic.IEnumerable<System.Type>.GetEnumerator()
extern "C"  Il2CppObject* U3CGetTypesWithHelpAttributeU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m2870630635 (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
