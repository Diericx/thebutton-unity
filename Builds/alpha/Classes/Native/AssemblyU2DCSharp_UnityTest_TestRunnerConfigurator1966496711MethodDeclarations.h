﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.TestRunnerConfigurator
struct TestRunnerConfigurator_t1966496711;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// UnityTest.IntegrationTestRunner.ITestRunnerCallback
struct ITestRunnerCallback_t327193412;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityTest.TestRunnerConfigurator::.ctor()
extern "C"  void TestRunnerConfigurator__ctor_m705632011 (TestRunnerConfigurator_t1966496711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.TestRunnerConfigurator::get_isBatchRun()
extern "C"  bool TestRunnerConfigurator_get_isBatchRun_m4088445273 (TestRunnerConfigurator_t1966496711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.TestRunnerConfigurator::set_isBatchRun(System.Boolean)
extern "C"  void TestRunnerConfigurator_set_isBatchRun_m557229600 (TestRunnerConfigurator_t1966496711 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.TestRunnerConfigurator::get_sendResultsOverNetwork()
extern "C"  bool TestRunnerConfigurator_get_sendResultsOverNetwork_m2213479566 (TestRunnerConfigurator_t1966496711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.TestRunnerConfigurator::set_sendResultsOverNetwork(System.Boolean)
extern "C"  void TestRunnerConfigurator_set_sendResultsOverNetwork_m3490115631 (TestRunnerConfigurator_t1966496711 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityTest.TestRunnerConfigurator::GetIntegrationTestScenes(System.Int32)
extern "C"  String_t* TestRunnerConfigurator_GetIntegrationTestScenes_m1098943538 (TestRunnerConfigurator_t1966496711 * __this, int32_t ___testSceneNum0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.TestRunnerConfigurator::CheckForSendingResultsOverNetwork()
extern "C"  void TestRunnerConfigurator_CheckForSendingResultsOverNetwork_m3289821816 (TestRunnerConfigurator_t1966496711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityTest.TestRunnerConfigurator::GetTextFromTextAsset(System.String)
extern "C"  String_t* TestRunnerConfigurator_GetTextFromTextAsset_m1284058970 (Il2CppObject * __this /* static, unused */, String_t* ___fileName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityTest.TestRunnerConfigurator::GetTextFromTempFile(System.String)
extern "C"  String_t* TestRunnerConfigurator_GetTextFromTempFile_m2815795139 (Il2CppObject * __this /* static, unused */, String_t* ___fileName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.TestRunnerConfigurator::CheckForBatchMode()
extern "C"  void TestRunnerConfigurator_CheckForBatchMode_m3039033525 (TestRunnerConfigurator_t1966496711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> UnityTest.TestRunnerConfigurator::GetAvailableNetworkIPs()
extern "C"  List_1_t1398341365 * TestRunnerConfigurator_GetAvailableNetworkIPs_m2707387739 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityTest.IntegrationTestRunner.ITestRunnerCallback UnityTest.TestRunnerConfigurator::ResolveNetworkConnection()
extern "C"  Il2CppObject * TestRunnerConfigurator_ResolveNetworkConnection_m3878511651 (TestRunnerConfigurator_t1966496711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.TestRunnerConfigurator::.cctor()
extern "C"  void TestRunnerConfigurator__cctor_m4037718552 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
