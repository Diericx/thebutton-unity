﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityTest.TestComponent
struct TestComponent_t2516511601;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityTest.TestRunner/<ParseListForGroups>c__AnonStorey1
struct  U3CParseListForGroupsU3Ec__AnonStorey1_t3712145952  : public Il2CppObject
{
public:
	// UnityTest.TestComponent UnityTest.TestRunner/<ParseListForGroups>c__AnonStorey1::testResult
	TestComponent_t2516511601 * ___testResult_0;

public:
	inline static int32_t get_offset_of_testResult_0() { return static_cast<int32_t>(offsetof(U3CParseListForGroupsU3Ec__AnonStorey1_t3712145952, ___testResult_0)); }
	inline TestComponent_t2516511601 * get_testResult_0() const { return ___testResult_0; }
	inline TestComponent_t2516511601 ** get_address_of_testResult_0() { return &___testResult_0; }
	inline void set_testResult_0(TestComponent_t2516511601 * value)
	{
		___testResult_0 = value;
		Il2CppCodeGenWriteBarrier(&___testResult_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
