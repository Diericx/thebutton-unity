﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.TestRunner/<ParseListForGroups>c__AnonStorey1
struct U3CParseListForGroupsU3Ec__AnonStorey1_t3712145952;
// UnityEngine.Component
struct Component_t3819376471;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"

// System.Void UnityTest.TestRunner/<ParseListForGroups>c__AnonStorey1::.ctor()
extern "C"  void U3CParseListForGroupsU3Ec__AnonStorey1__ctor_m898455413 (U3CParseListForGroupsU3Ec__AnonStorey1_t3712145952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.TestRunner/<ParseListForGroups>c__AnonStorey1::<>m__0(UnityEngine.Component)
extern "C"  bool U3CParseListForGroupsU3Ec__AnonStorey1_U3CU3Em__0_m764506160 (U3CParseListForGroupsU3Ec__AnonStorey1_t3712145952 * __this, Component_t3819376471 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
