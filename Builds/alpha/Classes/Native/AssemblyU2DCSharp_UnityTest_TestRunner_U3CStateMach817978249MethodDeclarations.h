﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.TestRunner/<StateMachine>c__Iterator0
struct U3CStateMachineU3Ec__Iterator0_t817978249;
// System.Object
struct Il2CppObject;
// UnityTest.AssertionComponent
struct AssertionComponent_t3962419315;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UnityTest_AssertionComponent3962419315.h"

// System.Void UnityTest.TestRunner/<StateMachine>c__Iterator0::.ctor()
extern "C"  void U3CStateMachineU3Ec__Iterator0__ctor_m574500974 (U3CStateMachineU3Ec__Iterator0_t817978249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.TestRunner/<StateMachine>c__Iterator0::MoveNext()
extern "C"  bool U3CStateMachineU3Ec__Iterator0_MoveNext_m106726754 (U3CStateMachineU3Ec__Iterator0_t817978249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityTest.TestRunner/<StateMachine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStateMachineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1009509828 (U3CStateMachineU3Ec__Iterator0_t817978249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityTest.TestRunner/<StateMachine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStateMachineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3693315932 (U3CStateMachineU3Ec__Iterator0_t817978249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.TestRunner/<StateMachine>c__Iterator0::Dispose()
extern "C"  void U3CStateMachineU3Ec__Iterator0_Dispose_m692648083 (U3CStateMachineU3Ec__Iterator0_t817978249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityTest.TestRunner/<StateMachine>c__Iterator0::Reset()
extern "C"  void U3CStateMachineU3Ec__Iterator0_Reset_m3752867569 (U3CStateMachineU3Ec__Iterator0_t817978249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.TestRunner/<StateMachine>c__Iterator0::<>m__0(UnityTest.AssertionComponent)
extern "C"  bool U3CStateMachineU3Ec__Iterator0_U3CU3Em__0_m2078372045 (Il2CppObject * __this /* static, unused */, AssertionComponent_t3962419315 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.TestRunner/<StateMachine>c__Iterator0::<>m__1(UnityTest.AssertionComponent)
extern "C"  bool U3CStateMachineU3Ec__Iterator0_U3CU3Em__1_m4161423954 (Il2CppObject * __this /* static, unused */, AssertionComponent_t3962419315 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
