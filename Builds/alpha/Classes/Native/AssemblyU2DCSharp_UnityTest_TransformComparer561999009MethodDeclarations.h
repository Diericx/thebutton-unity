﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.TransformComparer
struct TransformComparer_t561999009;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void UnityTest.TransformComparer::.ctor()
extern "C"  void TransformComparer__ctor_m1537353437 (TransformComparer_t561999009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.TransformComparer::Compare(UnityEngine.Transform,UnityEngine.Transform)
extern "C"  bool TransformComparer_Compare_m2606227028 (TransformComparer_t561999009 * __this, Transform_t3275118058 * ___a0, Transform_t3275118058 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
