﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.ValueDoesNotChange
struct ValueDoesNotChange_t1999561901;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void UnityTest.ValueDoesNotChange::.ctor()
extern "C"  void ValueDoesNotChange__ctor_m1911413009 (ValueDoesNotChange_t1999561901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.ValueDoesNotChange::Compare(System.Object)
extern "C"  bool ValueDoesNotChange_Compare_m4093839954 (ValueDoesNotChange_t1999561901 * __this, Il2CppObject * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
