﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.Vector2Comparer
struct Vector2Comparer_t1532566132;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void UnityTest.Vector2Comparer::.ctor()
extern "C"  void Vector2Comparer__ctor_m37349958 (Vector2Comparer_t1532566132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.Vector2Comparer::Compare(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Vector2Comparer_Compare_m4164992529 (Vector2Comparer_t1532566132 * __this, Vector2_t2243707579  ___a0, Vector2_t2243707579  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityTest.Vector2Comparer::GetDepthOfSearch()
extern "C"  int32_t Vector2Comparer_GetDepthOfSearch_m3473229810 (Vector2Comparer_t1532566132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
