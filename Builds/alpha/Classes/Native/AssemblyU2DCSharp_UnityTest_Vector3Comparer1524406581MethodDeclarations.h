﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.Vector3Comparer
struct Vector3Comparer_t1524406581;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void UnityTest.Vector3Comparer::.ctor()
extern "C"  void Vector3Comparer__ctor_m13630023 (Vector3Comparer_t1524406581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.Vector3Comparer::Compare(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3Comparer_Compare_m3187825740 (Vector3Comparer_t1524406581 * __this, Vector3_t2243707580  ___a0, Vector3_t2243707580  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
