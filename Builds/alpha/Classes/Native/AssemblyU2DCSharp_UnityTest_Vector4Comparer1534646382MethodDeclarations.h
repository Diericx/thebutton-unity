﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.Vector4Comparer
struct Vector4Comparer_t1534646382;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"

// System.Void UnityTest.Vector4Comparer::.ctor()
extern "C"  void Vector4Comparer__ctor_m2279082948 (Vector4Comparer_t1534646382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityTest.Vector4Comparer::Compare(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  bool Vector4Comparer_Compare_m737275931 (Vector4Comparer_t1534646382 * __this, Vector4_t2243707581  ___a0, Vector4_t2243707581  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityTest.Vector4Comparer::GetDepthOfSearch()
extern "C"  int32_t Vector4Comparer_GetDepthOfSearch_m3851589052 (Vector4Comparer_t1534646382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
