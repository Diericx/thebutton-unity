﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.VectorComparerBase`1<UnityEngine.Vector3>
struct VectorComparerBase_1_t1329214031;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityTest.VectorComparerBase`1<UnityEngine.Vector3>::.ctor()
extern "C"  void VectorComparerBase_1__ctor_m2196001199_gshared (VectorComparerBase_1_t1329214031 * __this, const MethodInfo* method);
#define VectorComparerBase_1__ctor_m2196001199(__this, method) ((  void (*) (VectorComparerBase_1_t1329214031 *, const MethodInfo*))VectorComparerBase_1__ctor_m2196001199_gshared)(__this, method)
// System.Boolean UnityTest.VectorComparerBase`1<UnityEngine.Vector3>::AreVectorMagnitudeEqual(System.Single,System.Single,System.Double)
extern "C"  bool VectorComparerBase_1_AreVectorMagnitudeEqual_m290049576_gshared (VectorComparerBase_1_t1329214031 * __this, float ___a0, float ___b1, double ___floatingPointError2, const MethodInfo* method);
#define VectorComparerBase_1_AreVectorMagnitudeEqual_m290049576(__this, ___a0, ___b1, ___floatingPointError2, method) ((  bool (*) (VectorComparerBase_1_t1329214031 *, float, float, double, const MethodInfo*))VectorComparerBase_1_AreVectorMagnitudeEqual_m290049576_gshared)(__this, ___a0, ___b1, ___floatingPointError2, method)
