﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityTest.VectorComparerBase`1<System.Object>
struct VectorComparerBase_1_t1774955746;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityTest.VectorComparerBase`1<System.Object>::.ctor()
extern "C"  void VectorComparerBase_1__ctor_m691832304_gshared (VectorComparerBase_1_t1774955746 * __this, const MethodInfo* method);
#define VectorComparerBase_1__ctor_m691832304(__this, method) ((  void (*) (VectorComparerBase_1_t1774955746 *, const MethodInfo*))VectorComparerBase_1__ctor_m691832304_gshared)(__this, method)
// System.Boolean UnityTest.VectorComparerBase`1<System.Object>::AreVectorMagnitudeEqual(System.Single,System.Single,System.Double)
extern "C"  bool VectorComparerBase_1_AreVectorMagnitudeEqual_m2812616117_gshared (VectorComparerBase_1_t1774955746 * __this, float ___a0, float ___b1, double ___floatingPointError2, const MethodInfo* method);
#define VectorComparerBase_1_AreVectorMagnitudeEqual_m2812616117(__this, ___a0, ___b1, ___floatingPointError2, method) ((  bool (*) (VectorComparerBase_1_t1774955746 *, float, float, double, const MethodInfo*))VectorComparerBase_1_AreVectorMagnitudeEqual_m2812616117_gshared)(__this, ___a0, ___b1, ___floatingPointError2, method)
