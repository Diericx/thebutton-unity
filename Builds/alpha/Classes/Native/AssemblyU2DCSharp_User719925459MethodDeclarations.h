﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// User
struct User_t719925459;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void User::.ctor()
extern "C"  void User__ctor_m2333596438 (User_t719925459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void User::.ctor(System.String,System.String)
extern "C"  void User__ctor_m3286703584 (User_t719925459 * __this, String_t* ___username0, String_t* ___email1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
