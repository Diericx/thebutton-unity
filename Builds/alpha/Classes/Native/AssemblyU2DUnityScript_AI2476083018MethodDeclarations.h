﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AI
struct AI_t2476083018;
// UnityEngine.Collider
struct Collider_t3497673348;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"

// System.Void AI::.ctor()
extern "C"  void AI__ctor_m1174688884 (AI_t2476083018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AI::Awake()
extern "C"  void AI_Awake_m1198661355 (AI_t2476083018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AI::OnEnable()
extern "C"  void AI_OnEnable_m4264155440 (AI_t2476083018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AI::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void AI_OnTriggerEnter_m1092363448 (AI_t2476083018 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AI::OnEnterInterestArea()
extern "C"  void AI_OnEnterInterestArea_m3424342666 (AI_t2476083018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AI::OnExitInterestArea()
extern "C"  void AI_OnExitInterestArea_m2582640360 (AI_t2476083018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AI::OnSpotted()
extern "C"  void AI_OnSpotted_m486737262 (AI_t2476083018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AI::OnLostTrack()
extern "C"  void AI_OnLostTrack_m1813139578 (AI_t2476083018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AI::CanSeePlayer()
extern "C"  bool AI_CanSeePlayer_m2261913846 (AI_t2476083018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AI::Main()
extern "C"  void AI_Main_m3706889401 (AI_t2476083018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
