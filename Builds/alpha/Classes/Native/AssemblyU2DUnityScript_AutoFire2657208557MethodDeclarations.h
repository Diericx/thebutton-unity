﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AutoFire
struct AutoFire_t2657208557;

#include "codegen/il2cpp-codegen.h"

// System.Void AutoFire::.ctor()
extern "C"  void AutoFire__ctor_m3835423877 (AutoFire_t2657208557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoFire::Awake()
extern "C"  void AutoFire_Awake_m3641296476 (AutoFire_t2657208557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoFire::Update()
extern "C"  void AutoFire_Update_m607471546 (AutoFire_t2657208557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoFire::OnStartFire()
extern "C"  void AutoFire_OnStartFire_m2496133628 (AutoFire_t2657208557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoFire::OnStopFire()
extern "C"  void AutoFire_OnStopFire_m3525826486 (AutoFire_t2657208557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoFire::Main()
extern "C"  void AutoFire_Main_m2270067470 (AutoFire_t2657208557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
