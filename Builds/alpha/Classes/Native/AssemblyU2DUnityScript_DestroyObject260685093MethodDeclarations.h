﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DestroyObject
struct DestroyObject_t260685093;

#include "codegen/il2cpp-codegen.h"

// System.Void DestroyObject::.ctor()
extern "C"  void DestroyObject__ctor_m3787626429 (DestroyObject_t260685093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DestroyObject::OnSignal()
extern "C"  void DestroyObject_OnSignal_m1290035890 (DestroyObject_t260685093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DestroyObject::Main()
extern "C"  void DestroyObject_Main_m1532369982 (DestroyObject_t260685093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
