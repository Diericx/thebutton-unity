﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DisableOutsideRadius
struct DisableOutsideRadius_t3052300191;
// UnityEngine.Collider
struct Collider_t3497673348;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"

// System.Void DisableOutsideRadius::.ctor()
extern "C"  void DisableOutsideRadius__ctor_m3970157123 (DisableOutsideRadius_t3052300191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisableOutsideRadius::Awake()
extern "C"  void DisableOutsideRadius_Awake_m2297585978 (DisableOutsideRadius_t3052300191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisableOutsideRadius::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void DisableOutsideRadius_OnTriggerEnter_m3363212839 (DisableOutsideRadius_t3052300191 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisableOutsideRadius::OnTriggerExit(UnityEngine.Collider)
extern "C"  void DisableOutsideRadius_OnTriggerExit_m3560045963 (DisableOutsideRadius_t3052300191 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisableOutsideRadius::Disable()
extern "C"  void DisableOutsideRadius_Disable_m3496426739 (DisableOutsideRadius_t3052300191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisableOutsideRadius::Enable()
extern "C"  void DisableOutsideRadius_Enable_m3142127226 (DisableOutsideRadius_t3052300191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DisableOutsideRadius::Main()
extern "C"  void DisableOutsideRadius_Main_m1238274504 (DisableOutsideRadius_t3052300191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
