﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectSequencer
struct EffectSequencer_t194314474;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// ExplosionPart
struct ExplosionPart_t2473625634;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_ExplosionPart2473625634.h"

// System.Void EffectSequencer::.ctor()
extern "C"  void EffectSequencer__ctor_m1532800462 (EffectSequencer_t194314474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EffectSequencer::Start()
extern "C"  Il2CppObject * EffectSequencer_Start_m2608506064 (EffectSequencer_t194314474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EffectSequencer::InstantiateDelayed(ExplosionPart)
extern "C"  Il2CppObject * EffectSequencer_InstantiateDelayed_m2442955740 (EffectSequencer_t194314474 * __this, ExplosionPart_t2473625634 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectSequencer::Main()
extern "C"  void EffectSequencer_Main_m2535506055 (EffectSequencer_t194314474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
