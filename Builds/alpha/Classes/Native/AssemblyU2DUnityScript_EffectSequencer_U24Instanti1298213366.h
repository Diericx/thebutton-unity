﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExplosionPart
struct ExplosionPart_t2473625634;
// EffectSequencer
struct EffectSequencer_t194314474;

#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen4259040017.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectSequencer/$InstantiateDelayed$78
struct  U24InstantiateDelayedU2478_t1298213366  : public GenericGenerator_1_t4259040017
{
public:
	// ExplosionPart EffectSequencer/$InstantiateDelayed$78::$go$81
	ExplosionPart_t2473625634 * ___U24goU2481_0;
	// EffectSequencer EffectSequencer/$InstantiateDelayed$78::$self_$82
	EffectSequencer_t194314474 * ___U24self_U2482_1;

public:
	inline static int32_t get_offset_of_U24goU2481_0() { return static_cast<int32_t>(offsetof(U24InstantiateDelayedU2478_t1298213366, ___U24goU2481_0)); }
	inline ExplosionPart_t2473625634 * get_U24goU2481_0() const { return ___U24goU2481_0; }
	inline ExplosionPart_t2473625634 ** get_address_of_U24goU2481_0() { return &___U24goU2481_0; }
	inline void set_U24goU2481_0(ExplosionPart_t2473625634 * value)
	{
		___U24goU2481_0 = value;
		Il2CppCodeGenWriteBarrier(&___U24goU2481_0, value);
	}

	inline static int32_t get_offset_of_U24self_U2482_1() { return static_cast<int32_t>(offsetof(U24InstantiateDelayedU2478_t1298213366, ___U24self_U2482_1)); }
	inline EffectSequencer_t194314474 * get_U24self_U2482_1() const { return ___U24self_U2482_1; }
	inline EffectSequencer_t194314474 ** get_address_of_U24self_U2482_1() { return &___U24self_U2482_1; }
	inline void set_U24self_U2482_1(EffectSequencer_t194314474 * value)
	{
		___U24self_U2482_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24self_U2482_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
