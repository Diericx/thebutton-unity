﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectSequencer/$InstantiateDelayed$78
struct U24InstantiateDelayedU2478_t1298213366;
// ExplosionPart
struct ExplosionPart_t2473625634;
// EffectSequencer
struct EffectSequencer_t194314474;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds>
struct IEnumerator_1_t1315025894;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_ExplosionPart2473625634.h"
#include "AssemblyU2DUnityScript_EffectSequencer194314474.h"

// System.Void EffectSequencer/$InstantiateDelayed$78::.ctor(ExplosionPart,EffectSequencer)
extern "C"  void U24InstantiateDelayedU2478__ctor_m1575325494 (U24InstantiateDelayedU2478_t1298213366 * __this, ExplosionPart_t2473625634 * ___go0, EffectSequencer_t194314474 * ___self_1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> EffectSequencer/$InstantiateDelayed$78::GetEnumerator()
extern "C"  Il2CppObject* U24InstantiateDelayedU2478_GetEnumerator_m2327833452 (U24InstantiateDelayedU2478_t1298213366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
