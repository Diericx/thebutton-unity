﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ExplosionPart
struct ExplosionPart_t2473625634;
// EffectSequencer
struct EffectSequencer_t194314474;

#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen300505933.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectSequencer/$InstantiateDelayed$78/$
struct  U24_t814366785  : public GenericGeneratorEnumerator_1_t300505933
{
public:
	// ExplosionPart EffectSequencer/$InstantiateDelayed$78/$::$go$79
	ExplosionPart_t2473625634 * ___U24goU2479_2;
	// EffectSequencer EffectSequencer/$InstantiateDelayed$78/$::$self_$80
	EffectSequencer_t194314474 * ___U24self_U2480_3;

public:
	inline static int32_t get_offset_of_U24goU2479_2() { return static_cast<int32_t>(offsetof(U24_t814366785, ___U24goU2479_2)); }
	inline ExplosionPart_t2473625634 * get_U24goU2479_2() const { return ___U24goU2479_2; }
	inline ExplosionPart_t2473625634 ** get_address_of_U24goU2479_2() { return &___U24goU2479_2; }
	inline void set_U24goU2479_2(ExplosionPart_t2473625634 * value)
	{
		___U24goU2479_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24goU2479_2, value);
	}

	inline static int32_t get_offset_of_U24self_U2480_3() { return static_cast<int32_t>(offsetof(U24_t814366785, ___U24self_U2480_3)); }
	inline EffectSequencer_t194314474 * get_U24self_U2480_3() const { return ___U24self_U2480_3; }
	inline EffectSequencer_t194314474 ** get_address_of_U24self_U2480_3() { return &___U24self_U2480_3; }
	inline void set_U24self_U2480_3(EffectSequencer_t194314474 * value)
	{
		___U24self_U2480_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24self_U2480_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
