﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectSequencer/$InstantiateDelayed$78/$
struct U24_t814366785;
// ExplosionPart
struct ExplosionPart_t2473625634;
// EffectSequencer
struct EffectSequencer_t194314474;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_ExplosionPart2473625634.h"
#include "AssemblyU2DUnityScript_EffectSequencer194314474.h"

// System.Void EffectSequencer/$InstantiateDelayed$78/$::.ctor(ExplosionPart,EffectSequencer)
extern "C"  void U24__ctor_m3713097129 (U24_t814366785 * __this, ExplosionPart_t2473625634 * ___go0, EffectSequencer_t194314474 * ___self_1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EffectSequencer/$InstantiateDelayed$78/$::MoveNext()
extern "C"  bool U24_MoveNext_m637962735 (U24_t814366785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
