﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// EffectSequencer
struct EffectSequencer_t194314474;

#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen3108987245.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectSequencer/$Start$69
struct  U24StartU2469_t1610517444  : public GenericGenerator_1_t3108987245
{
public:
	// EffectSequencer EffectSequencer/$Start$69::$self_$77
	EffectSequencer_t194314474 * ___U24self_U2477_0;

public:
	inline static int32_t get_offset_of_U24self_U2477_0() { return static_cast<int32_t>(offsetof(U24StartU2469_t1610517444, ___U24self_U2477_0)); }
	inline EffectSequencer_t194314474 * get_U24self_U2477_0() const { return ___U24self_U2477_0; }
	inline EffectSequencer_t194314474 ** get_address_of_U24self_U2477_0() { return &___U24self_U2477_0; }
	inline void set_U24self_U2477_0(EffectSequencer_t194314474 * value)
	{
		___U24self_U2477_0 = value;
		Il2CppCodeGenWriteBarrier(&___U24self_U2477_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
