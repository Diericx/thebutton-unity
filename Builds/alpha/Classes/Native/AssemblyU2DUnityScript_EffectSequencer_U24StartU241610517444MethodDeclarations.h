﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectSequencer/$Start$69
struct U24StartU2469_t1610517444;
// EffectSequencer
struct EffectSequencer_t194314474;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_EffectSequencer194314474.h"

// System.Void EffectSequencer/$Start$69::.ctor(EffectSequencer)
extern "C"  void U24StartU2469__ctor_m1290453516 (U24StartU2469_t1610517444 * __this, EffectSequencer_t194314474 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Object> EffectSequencer/$Start$69::GetEnumerator()
extern "C"  Il2CppObject* U24StartU2469_GetEnumerator_m3845317762 (U24StartU2469_t1610517444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
