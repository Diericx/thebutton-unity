﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectSequencer/$Start$69/$
struct U24_t2409507311;
// EffectSequencer
struct EffectSequencer_t194314474;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_EffectSequencer194314474.h"

// System.Void EffectSequencer/$Start$69/$::.ctor(EffectSequencer)
extern "C"  void U24__ctor_m2054523649 (U24_t2409507311 * __this, EffectSequencer_t194314474 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EffectSequencer/$Start$69/$::MoveNext()
extern "C"  bool U24_MoveNext_m504862089 (U24_t2409507311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
