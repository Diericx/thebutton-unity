﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExplosionPart
struct ExplosionPart_t2473625634;

#include "codegen/il2cpp-codegen.h"

// System.Void ExplosionPart::.ctor()
extern "C"  void ExplosionPart__ctor_m3297066906 (ExplosionPart_t2473625634 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
