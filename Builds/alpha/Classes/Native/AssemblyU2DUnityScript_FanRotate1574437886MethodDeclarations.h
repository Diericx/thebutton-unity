﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FanRotate
struct FanRotate_t1574437886;

#include "codegen/il2cpp-codegen.h"

// System.Void FanRotate::.ctor()
extern "C"  void FanRotate__ctor_m691777854 (FanRotate_t1574437886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FanRotate::Main()
extern "C"  void FanRotate_Main_m4202370641 (FanRotate_t1574437886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
