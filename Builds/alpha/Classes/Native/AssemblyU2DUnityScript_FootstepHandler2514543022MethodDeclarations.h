﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FootstepHandler
struct FootstepHandler_t2514543022;
// UnityEngine.Collision
struct Collision_t2876846408;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"

// System.Void FootstepHandler::.ctor()
extern "C"  void FootstepHandler__ctor_m3797750694 (FootstepHandler_t2514543022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FootstepHandler::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void FootstepHandler_OnCollisionEnter_m2698422936 (FootstepHandler_t2514543022 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FootstepHandler::OnFootstep()
extern "C"  void FootstepHandler_OnFootstep_m4217666043 (FootstepHandler_t2514543022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FootstepHandler::Main()
extern "C"  void FootstepHandler_Main_m1639644865 (FootstepHandler_t2514543022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
