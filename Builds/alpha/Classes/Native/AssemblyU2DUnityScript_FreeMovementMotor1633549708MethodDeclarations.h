﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FreeMovementMotor
struct FreeMovementMotor_t1633549708;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void FreeMovementMotor::.ctor()
extern "C"  void FreeMovementMotor__ctor_m3821870680 (FreeMovementMotor_t1633549708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FreeMovementMotor::FixedUpdate()
extern "C"  void FreeMovementMotor_FixedUpdate_m2258332217 (FreeMovementMotor_t1633549708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FreeMovementMotor::AngleAroundAxis(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float FreeMovementMotor_AngleAroundAxis_m2394052890 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___dirA0, Vector3_t2243707580  ___dirB1, Vector3_t2243707580  ___axis2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FreeMovementMotor::Main()
extern "C"  void FreeMovementMotor_Main_m2384879269 (FreeMovementMotor_t1633549708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
