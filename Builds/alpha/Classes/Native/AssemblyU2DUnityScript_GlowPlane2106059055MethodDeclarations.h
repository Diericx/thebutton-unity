﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlowPlane
struct GlowPlane_t2106059055;

#include "codegen/il2cpp-codegen.h"

// System.Void GlowPlane::.ctor()
extern "C"  void GlowPlane__ctor_m1453172583 (GlowPlane_t2106059055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlowPlane::Start()
extern "C"  void GlowPlane_Start_m1406937411 (GlowPlane_t2106059055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlowPlane::OnDrawGizmos()
extern "C"  void GlowPlane_OnDrawGizmos_m3426450019 (GlowPlane_t2106059055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlowPlane::OnDrawGizmosSelected()
extern "C"  void GlowPlane_OnDrawGizmosSelected_m1583840234 (GlowPlane_t2106059055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlowPlane::OnBecameVisible()
extern "C"  void GlowPlane_OnBecameVisible_m2178474545 (GlowPlane_t2106059055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlowPlane::OnBecameInvisible()
extern "C"  void GlowPlane_OnBecameInvisible_m364750530 (GlowPlane_t2106059055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlowPlane::Update()
extern "C"  void GlowPlane_Update_m609705766 (GlowPlane_t2106059055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlowPlane::Main()
extern "C"  void GlowPlane_Main_m2806361502 (GlowPlane_t2106059055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
