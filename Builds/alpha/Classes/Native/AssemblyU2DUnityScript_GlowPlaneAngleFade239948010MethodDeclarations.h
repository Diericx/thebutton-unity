﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlowPlaneAngleFade
struct GlowPlaneAngleFade_t239948010;

#include "codegen/il2cpp-codegen.h"

// System.Void GlowPlaneAngleFade::.ctor()
extern "C"  void GlowPlaneAngleFade__ctor_m3584954580 (GlowPlaneAngleFade_t239948010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlowPlaneAngleFade::Start()
extern "C"  void GlowPlaneAngleFade_Start_m2937330716 (GlowPlaneAngleFade_t239948010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlowPlaneAngleFade::Update()
extern "C"  void GlowPlaneAngleFade_Update_m2499551425 (GlowPlaneAngleFade_t239948010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlowPlaneAngleFade::OnWillRenderObject()
extern "C"  void GlowPlaneAngleFade_OnWillRenderObject_m3987060868 (GlowPlaneAngleFade_t239948010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlowPlaneAngleFade::Main()
extern "C"  void GlowPlaneAngleFade_Main_m4071350831 (GlowPlaneAngleFade_t239948010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
