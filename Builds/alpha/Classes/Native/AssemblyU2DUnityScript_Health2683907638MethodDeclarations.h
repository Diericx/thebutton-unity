﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Health
struct Health_t2683907638;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void Health::.ctor()
extern "C"  void Health__ctor_m2434983288 (Health_t2683907638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Health::Awake()
extern "C"  void Health_Awake_m4184625751 (Health_t2683907638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Health::OnDamage(System.Single,UnityEngine.Vector3)
extern "C"  void Health_OnDamage_m2245253288 (Health_t2683907638 * __this, float ___amount0, Vector3_t2243707580  ___fromDirection1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Health::OnEnable()
extern "C"  void Health_OnEnable_m735332876 (Health_t2683907638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Health::Regenerate()
extern "C"  Il2CppObject * Health_Regenerate_m2313802398 (Health_t2683907638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Health::Main()
extern "C"  void Health_Main_m738848101 (Health_t2683907638 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
