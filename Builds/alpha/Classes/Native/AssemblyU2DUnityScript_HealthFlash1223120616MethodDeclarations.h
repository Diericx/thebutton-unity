﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HealthFlash
struct HealthFlash_t1223120616;

#include "codegen/il2cpp-codegen.h"

// System.Void HealthFlash::.ctor()
extern "C"  void HealthFlash__ctor_m635015568 (HealthFlash_t1223120616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HealthFlash::Start()
extern "C"  void HealthFlash_Start_m3491444100 (HealthFlash_t1223120616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HealthFlash::Update()
extern "C"  void HealthFlash_Update_m433886455 (HealthFlash_t1223120616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HealthFlash::Main()
extern "C"  void HealthFlash_Main_m3841114377 (HealthFlash_t1223120616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
