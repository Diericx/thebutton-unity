﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Health/$Regenerate$91
struct U24RegenerateU2491_t1949600641;
// Health
struct Health_t2683907638;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds>
struct IEnumerator_1_t1315025894;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_Health2683907638.h"

// System.Void Health/$Regenerate$91::.ctor(Health)
extern "C"  void U24RegenerateU2491__ctor_m3102807769 (U24RegenerateU2491_t1949600641 * __this, Health_t2683907638 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> Health/$Regenerate$91::GetEnumerator()
extern "C"  Il2CppObject* U24RegenerateU2491_GetEnumerator_m2761811061 (U24RegenerateU2491_t1949600641 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
