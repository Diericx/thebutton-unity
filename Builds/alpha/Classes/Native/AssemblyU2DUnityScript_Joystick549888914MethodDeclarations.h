﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Joystick
struct Joystick_t549888914;

#include "codegen/il2cpp-codegen.h"

// System.Void Joystick::.ctor()
extern "C"  void Joystick__ctor_m1335692108 (Joystick_t549888914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick::.cctor()
extern "C"  void Joystick__cctor_m1457638875 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick::Start()
extern "C"  void Joystick_Start_m3461673412 (Joystick_t549888914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick::Disable()
extern "C"  void Joystick_Disable_m2610396156 (Joystick_t549888914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick::ResetJoystick()
extern "C"  void Joystick_ResetJoystick_m2160955459 (Joystick_t549888914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Joystick::IsFingerDown()
extern "C"  bool Joystick_IsFingerDown_m535501349 (Joystick_t549888914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick::LatchedFinger(System.Int32)
extern "C"  void Joystick_LatchedFinger_m1573475483 (Joystick_t549888914 * __this, int32_t ___fingerId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick::Update()
extern "C"  void Joystick_Update_m3141343657 (Joystick_t549888914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Joystick::Main()
extern "C"  void Joystick_Main_m276376279 (Joystick_t549888914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
