﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaserScope
struct LaserScope_t1872580701;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void LaserScope::.ctor()
extern "C"  void LaserScope__ctor_m1167516545 (LaserScope_t1872580701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaserScope::Start()
extern "C"  void LaserScope_Start_m3411263089 (LaserScope_t1872580701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LaserScope::ChoseNewAnimationTargetCoroutine()
extern "C"  Il2CppObject * LaserScope_ChoseNewAnimationTargetCoroutine_m2008565246 (LaserScope_t1872580701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaserScope::Update()
extern "C"  void LaserScope_Update_m28173388 (LaserScope_t1872580701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LaserScope::Main()
extern "C"  void LaserScope_Main_m1626807512 (LaserScope_t1872580701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
