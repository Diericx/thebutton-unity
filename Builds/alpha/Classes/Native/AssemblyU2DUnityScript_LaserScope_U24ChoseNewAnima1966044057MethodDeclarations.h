﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaserScope/$ChoseNewAnimationTargetCoroutine$83/$
struct U24_t1966044057;
// LaserScope
struct LaserScope_t1872580701;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_LaserScope1872580701.h"

// System.Void LaserScope/$ChoseNewAnimationTargetCoroutine$83/$::.ctor(LaserScope)
extern "C"  void U24__ctor_m3177170082 (U24_t1966044057 * __this, LaserScope_t1872580701 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LaserScope/$ChoseNewAnimationTargetCoroutine$83/$::MoveNext()
extern "C"  bool U24_MoveNext_m152879947 (U24_t1966044057 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
