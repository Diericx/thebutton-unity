﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LaserScope/$ChoseNewAnimationTargetCoroutine$83
struct U24ChoseNewAnimationTargetCoroutineU2483_t330810268;
// LaserScope
struct LaserScope_t1872580701;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds>
struct IEnumerator_1_t1315025894;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_LaserScope1872580701.h"

// System.Void LaserScope/$ChoseNewAnimationTargetCoroutine$83::.ctor(LaserScope)
extern "C"  void U24ChoseNewAnimationTargetCoroutineU2483__ctor_m4128894609 (U24ChoseNewAnimationTargetCoroutineU2483_t330810268 * __this, LaserScope_t1872580701 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> LaserScope/$ChoseNewAnimationTargetCoroutine$83::GetEnumerator()
extern "C"  Il2CppObject* U24ChoseNewAnimationTargetCoroutineU2483_GetEnumerator_m2837683460 (U24ChoseNewAnimationTargetCoroutineU2483_t330810268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
