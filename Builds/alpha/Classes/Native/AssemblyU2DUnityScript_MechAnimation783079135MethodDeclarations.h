﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MechAnimation
struct MechAnimation_t783079135;

#include "codegen/il2cpp-codegen.h"

// System.Void MechAnimation::.ctor()
extern "C"  void MechAnimation__ctor_m3295248863 (MechAnimation_t783079135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MechAnimation::OnEnable()
extern "C"  void MechAnimation_OnEnable_m917170899 (MechAnimation_t783079135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MechAnimation::FixedUpdate()
extern "C"  void MechAnimation_FixedUpdate_m1765434400 (MechAnimation_t783079135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MechAnimation::Main()
extern "C"  void MechAnimation_Main_m1444211198 (MechAnimation_t783079135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
