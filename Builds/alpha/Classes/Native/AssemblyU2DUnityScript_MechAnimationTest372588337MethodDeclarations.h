﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MechAnimationTest
struct MechAnimationTest_t372588337;

#include "codegen/il2cpp-codegen.h"

// System.Void MechAnimationTest::.ctor()
extern "C"  void MechAnimationTest__ctor_m545631909 (MechAnimationTest_t372588337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MechAnimationTest::OnEnable()
extern "C"  void MechAnimationTest_OnEnable_m2162304133 (MechAnimationTest_t372588337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MechAnimationTest::FixedUpdate()
extern "C"  void MechAnimationTest_FixedUpdate_m3928532348 (MechAnimationTest_t372588337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MechAnimationTest::OnGUI()
extern "C"  void MechAnimationTest_OnGUI_m1660128167 (MechAnimationTest_t372588337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MechAnimationTest::Main()
extern "C"  void MechAnimationTest_Main_m3420166506 (MechAnimationTest_t372588337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
