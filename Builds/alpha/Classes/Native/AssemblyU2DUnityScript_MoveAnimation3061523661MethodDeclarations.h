﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoveAnimation
struct MoveAnimation_t3061523661;

#include "codegen/il2cpp-codegen.h"

// System.Void MoveAnimation::.ctor()
extern "C"  void MoveAnimation__ctor_m2355953137 (MoveAnimation_t3061523661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveAnimation::Init()
extern "C"  void MoveAnimation_Init_m3867678363 (MoveAnimation_t3061523661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
