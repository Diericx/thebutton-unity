﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MovementMotor
struct MovementMotor_t1612021398;

#include "codegen/il2cpp-codegen.h"

// System.Void MovementMotor::.ctor()
extern "C"  void MovementMotor__ctor_m3335335718 (MovementMotor_t1612021398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovementMotor::Main()
extern "C"  void MovementMotor_Main_m3371820361 (MovementMotor_t1612021398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
