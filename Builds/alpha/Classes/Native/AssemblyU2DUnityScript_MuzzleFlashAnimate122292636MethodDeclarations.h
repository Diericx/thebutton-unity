﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MuzzleFlashAnimate
struct MuzzleFlashAnimate_t122292636;

#include "codegen/il2cpp-codegen.h"

// System.Void MuzzleFlashAnimate::.ctor()
extern "C"  void MuzzleFlashAnimate__ctor_m3823994638 (MuzzleFlashAnimate_t122292636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MuzzleFlashAnimate::Update()
extern "C"  void MuzzleFlashAnimate_Update_m556700145 (MuzzleFlashAnimate_t122292636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MuzzleFlashAnimate::Main()
extern "C"  void MuzzleFlashAnimate_Main_m967009555 (MuzzleFlashAnimate_t122292636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
