﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PatrolPoint
struct PatrolPoint_t2056099796;

#include "codegen/il2cpp-codegen.h"

// System.Void PatrolPoint::.ctor()
extern "C"  void PatrolPoint__ctor_m3383233708 (PatrolPoint_t2056099796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PatrolPoint::Awake()
extern "C"  void PatrolPoint_Awake_m4014645279 (PatrolPoint_t2056099796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PatrolPoint::Main()
extern "C"  void PatrolPoint_Main_m1256176165 (PatrolPoint_t2056099796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
