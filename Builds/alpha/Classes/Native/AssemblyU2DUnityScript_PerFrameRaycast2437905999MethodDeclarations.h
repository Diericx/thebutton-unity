﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PerFrameRaycast
struct PerFrameRaycast_t2437905999;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"

// System.Void PerFrameRaycast::.ctor()
extern "C"  void PerFrameRaycast__ctor_m1344158147 (PerFrameRaycast_t2437905999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PerFrameRaycast::Awake()
extern "C"  void PerFrameRaycast_Awake_m3754006566 (PerFrameRaycast_t2437905999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PerFrameRaycast::Update()
extern "C"  void PerFrameRaycast_Update_m2621169380 (PerFrameRaycast_t2437905999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit PerFrameRaycast::GetHitInfo()
extern "C"  RaycastHit_t87180320  PerFrameRaycast_GetHitInfo_m4235334892 (PerFrameRaycast_t2437905999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PerFrameRaycast::Main()
extern "C"  void PerFrameRaycast_Main_m1544432620 (PerFrameRaycast_t2437905999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
