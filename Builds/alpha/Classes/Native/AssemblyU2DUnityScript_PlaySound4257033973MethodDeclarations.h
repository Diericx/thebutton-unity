﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlaySound
struct PlaySound_t4257033973;

#include "codegen/il2cpp-codegen.h"

// System.Void PlaySound::.ctor()
extern "C"  void PlaySound__ctor_m2785613205 (PlaySound_t4257033973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlaySound::Awake()
extern "C"  void PlaySound_Awake_m2154727320 (PlaySound_t4257033973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlaySound::OnSignal()
extern "C"  void PlaySound_OnSignal_m4111258042 (PlaySound_t4257033973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlaySound::Main()
extern "C"  void PlaySound_Main_m1887946366 (PlaySound_t4257033973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
