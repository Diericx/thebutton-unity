﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerAnimation
struct PlayerAnimation_t4286826603;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void PlayerAnimation::.ctor()
extern "C"  void PlayerAnimation__ctor_m3323431079 (PlayerAnimation_t4286826603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerAnimation::Awake()
extern "C"  void PlayerAnimation_Awake_m2693778044 (PlayerAnimation_t4286826603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerAnimation::OnStartFire()
extern "C"  void PlayerAnimation_OnStartFire_m2791621660 (PlayerAnimation_t4286826603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerAnimation::OnStopFire()
extern "C"  void PlayerAnimation_OnStopFire_m4018214982 (PlayerAnimation_t4286826603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerAnimation::FixedUpdate()
extern "C"  void PlayerAnimation_FixedUpdate_m2246238316 (PlayerAnimation_t4286826603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerAnimation::Update()
extern "C"  void PlayerAnimation_Update_m220504286 (PlayerAnimation_t4286826603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerAnimation::LateUpdate()
extern "C"  void PlayerAnimation_LateUpdate_m2795049138 (PlayerAnimation_t4286826603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PlayerAnimation::HorizontalAngle(UnityEngine.Vector3)
extern "C"  float PlayerAnimation_HorizontalAngle_m2440304315 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerAnimation::Main()
extern "C"  void PlayerAnimation_Main_m970525314 (PlayerAnimation_t4286826603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
