﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReceiverItem
struct ReceiverItem_t169526838;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Void ReceiverItem::.ctor()
extern "C"  void ReceiverItem__ctor_m1109735248 (ReceiverItem_t169526838 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ReceiverItem::SendWithDelay(UnityEngine.MonoBehaviour)
extern "C"  Il2CppObject * ReceiverItem_SendWithDelay_m1045708218 (ReceiverItem_t169526838 * __this, MonoBehaviour_t1158329972 * ___sender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
