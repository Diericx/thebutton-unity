﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReceiverItem/$SendWithDelay$86
struct U24SendWithDelayU2486_t3482389000;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// ReceiverItem
struct ReceiverItem_t169526838;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds>
struct IEnumerator_1_t1315025894;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DUnityScript_ReceiverItem169526838.h"

// System.Void ReceiverItem/$SendWithDelay$86::.ctor(UnityEngine.MonoBehaviour,ReceiverItem)
extern "C"  void U24SendWithDelayU2486__ctor_m2207965465 (U24SendWithDelayU2486_t3482389000 * __this, MonoBehaviour_t1158329972 * ___sender0, ReceiverItem_t169526838 * ___self_1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> ReceiverItem/$SendWithDelay$86::GetEnumerator()
extern "C"  Il2CppObject* U24SendWithDelayU2486_GetEnumerator_m2551330026 (U24SendWithDelayU2486_t3482389000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
