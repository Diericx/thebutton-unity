﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// ReceiverItem
struct ReceiverItem_t169526838;

#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen300505933.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReceiverItem/$SendWithDelay$86/$
struct  U24_t4172685971  : public GenericGeneratorEnumerator_1_t300505933
{
public:
	// UnityEngine.MonoBehaviour ReceiverItem/$SendWithDelay$86/$::$sender$87
	MonoBehaviour_t1158329972 * ___U24senderU2487_2;
	// ReceiverItem ReceiverItem/$SendWithDelay$86/$::$self_$88
	ReceiverItem_t169526838 * ___U24self_U2488_3;

public:
	inline static int32_t get_offset_of_U24senderU2487_2() { return static_cast<int32_t>(offsetof(U24_t4172685971, ___U24senderU2487_2)); }
	inline MonoBehaviour_t1158329972 * get_U24senderU2487_2() const { return ___U24senderU2487_2; }
	inline MonoBehaviour_t1158329972 ** get_address_of_U24senderU2487_2() { return &___U24senderU2487_2; }
	inline void set_U24senderU2487_2(MonoBehaviour_t1158329972 * value)
	{
		___U24senderU2487_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24senderU2487_2, value);
	}

	inline static int32_t get_offset_of_U24self_U2488_3() { return static_cast<int32_t>(offsetof(U24_t4172685971, ___U24self_U2488_3)); }
	inline ReceiverItem_t169526838 * get_U24self_U2488_3() const { return ___U24self_U2488_3; }
	inline ReceiverItem_t169526838 ** get_address_of_U24self_U2488_3() { return &___U24self_U2488_3; }
	inline void set_U24self_U2488_3(ReceiverItem_t169526838 * value)
	{
		___U24self_U2488_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24self_U2488_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
