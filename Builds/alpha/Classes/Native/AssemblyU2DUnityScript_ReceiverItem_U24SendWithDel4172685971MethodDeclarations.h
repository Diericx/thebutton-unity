﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReceiverItem/$SendWithDelay$86/$
struct U24_t4172685971;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// ReceiverItem
struct ReceiverItem_t169526838;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DUnityScript_ReceiverItem169526838.h"

// System.Void ReceiverItem/$SendWithDelay$86/$::.ctor(UnityEngine.MonoBehaviour,ReceiverItem)
extern "C"  void U24__ctor_m2301840438 (U24_t4172685971 * __this, MonoBehaviour_t1158329972 * ___sender0, ReceiverItem_t169526838 * ___self_1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ReceiverItem/$SendWithDelay$86/$::MoveNext()
extern "C"  bool U24_MoveNext_m2790908525 (U24_t4172685971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
