﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SelfIlluminationBlink
struct SelfIlluminationBlink_t1392230251;

#include "codegen/il2cpp-codegen.h"

// System.Void SelfIlluminationBlink::.ctor()
extern "C"  void SelfIlluminationBlink__ctor_m69264487 (SelfIlluminationBlink_t1392230251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelfIlluminationBlink::OnWillRenderObject()
extern "C"  void SelfIlluminationBlink_OnWillRenderObject_m1699045963 (SelfIlluminationBlink_t1392230251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelfIlluminationBlink::Blink()
extern "C"  void SelfIlluminationBlink_Blink_m2362217849 (SelfIlluminationBlink_t1392230251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelfIlluminationBlink::Main()
extern "C"  void SelfIlluminationBlink_Main_m498202930 (SelfIlluminationBlink_t1392230251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
