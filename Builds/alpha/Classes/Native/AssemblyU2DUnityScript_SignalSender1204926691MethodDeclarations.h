﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SignalSender
struct SignalSender_t1204926691;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Void SignalSender::.ctor()
extern "C"  void SignalSender__ctor_m1453183343 (SignalSender_t1204926691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SignalSender::SendSignals(UnityEngine.MonoBehaviour)
extern "C"  void SignalSender_SendSignals_m4066726171 (SignalSender_t1204926691 * __this, MonoBehaviour_t1158329972 * ___sender0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
