﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleBullet
struct SimpleBullet_t2114900010;

#include "codegen/il2cpp-codegen.h"

// System.Void SimpleBullet::.ctor()
extern "C"  void SimpleBullet__ctor_m3484848692 (SimpleBullet_t2114900010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleBullet::OnEnable()
extern "C"  void SimpleBullet_OnEnable_m967044912 (SimpleBullet_t2114900010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleBullet::Update()
extern "C"  void SimpleBullet_Update_m3908147799 (SimpleBullet_t2114900010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SimpleBullet::Main()
extern "C"  void SimpleBullet_Main_m2930733257 (SimpleBullet_t2114900010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
