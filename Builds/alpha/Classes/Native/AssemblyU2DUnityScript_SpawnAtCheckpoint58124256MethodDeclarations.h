﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpawnAtCheckpoint
struct SpawnAtCheckpoint_t58124256;

#include "codegen/il2cpp-codegen.h"

// System.Void SpawnAtCheckpoint::.ctor()
extern "C"  void SpawnAtCheckpoint__ctor_m2466345104 (SpawnAtCheckpoint_t58124256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnAtCheckpoint::OnSignal()
extern "C"  void SpawnAtCheckpoint_OnSignal_m1435128297 (SpawnAtCheckpoint_t58124256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnAtCheckpoint::ResetHealthOnAll()
extern "C"  void SpawnAtCheckpoint_ResetHealthOnAll_m1221346643 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnAtCheckpoint::Main()
extern "C"  void SpawnAtCheckpoint_Main_m3793371665 (SpawnAtCheckpoint_t58124256 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
