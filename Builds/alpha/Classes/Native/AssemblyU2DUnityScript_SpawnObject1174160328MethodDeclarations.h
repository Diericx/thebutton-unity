﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpawnObject
struct SpawnObject_t1174160328;

#include "codegen/il2cpp-codegen.h"

// System.Void SpawnObject::.ctor()
extern "C"  void SpawnObject__ctor_m1860728308 (SpawnObject_t1174160328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnObject::OnSignal()
extern "C"  void SpawnObject_OnSignal_m3589574635 (SpawnObject_t1174160328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnObject::Update()
extern "C"  void SpawnObject_Update_m339265901 (SpawnObject_t1174160328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpawnObject::Main()
extern "C"  void SpawnObject_Main_m3502971719 (SpawnObject_t1174160328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
