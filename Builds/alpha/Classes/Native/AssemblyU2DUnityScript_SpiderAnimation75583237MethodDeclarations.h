﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpiderAnimation
struct SpiderAnimation_t75583237;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void SpiderAnimation::.ctor()
extern "C"  void SpiderAnimation__ctor_m2953202753 (SpiderAnimation_t75583237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpiderAnimation::OnEnable()
extern "C"  void SpiderAnimation_OnEnable_m590839401 (SpiderAnimation_t75583237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpiderAnimation::OnDisable()
extern "C"  void SpiderAnimation_OnDisable_m85563204 (SpiderAnimation_t75583237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpiderAnimation::Update()
extern "C"  void SpiderAnimation_Update_m3015082642 (SpiderAnimation_t75583237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SpiderAnimation::HorizontalAngle(UnityEngine.Vector3)
extern "C"  float SpiderAnimation_HorizontalAngle_m4160094253 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpiderAnimation::Main()
extern "C"  void SpiderAnimation_Main_m2176370190 (SpiderAnimation_t75583237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
