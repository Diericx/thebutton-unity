﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpiderAnimationTest
struct SpiderAnimationTest_t3642224283;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void SpiderAnimationTest::.ctor()
extern "C"  void SpiderAnimationTest__ctor_m3170889203 (SpiderAnimationTest_t3642224283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpiderAnimationTest::OnEnable()
extern "C"  void SpiderAnimationTest_OnEnable_m3301736639 (SpiderAnimationTest_t3642224283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpiderAnimationTest::Update()
extern "C"  void SpiderAnimationTest_Update_m731600834 (SpiderAnimationTest_t3642224283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SpiderAnimationTest::HorizontalAngle(UnityEngine.Vector3)
extern "C"  float SpiderAnimationTest_HorizontalAngle_m3266739583 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpiderAnimationTest::OnGUI()
extern "C"  void SpiderAnimationTest_OnGUI_m2668444317 (SpiderAnimationTest_t3642224283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpiderAnimationTest::Main()
extern "C"  void SpiderAnimationTest_Main_m3355268778 (SpiderAnimationTest_t3642224283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
