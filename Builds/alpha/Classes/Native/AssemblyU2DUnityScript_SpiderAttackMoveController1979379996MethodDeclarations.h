﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpiderAttackMoveController
struct SpiderAttackMoveController_t1979379996;

#include "codegen/il2cpp-codegen.h"

// System.Void SpiderAttackMoveController::.ctor()
extern "C"  void SpiderAttackMoveController__ctor_m3546380210 (SpiderAttackMoveController_t1979379996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpiderAttackMoveController::Awake()
extern "C"  void SpiderAttackMoveController_Awake_m1099765163 (SpiderAttackMoveController_t1979379996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpiderAttackMoveController::OnEnable()
extern "C"  void SpiderAttackMoveController_OnEnable_m1357110374 (SpiderAttackMoveController_t1979379996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpiderAttackMoveController::OnDisable()
extern "C"  void SpiderAttackMoveController_OnDisable_m3921807863 (SpiderAttackMoveController_t1979379996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpiderAttackMoveController::Update()
extern "C"  void SpiderAttackMoveController_Update_m2867909263 (SpiderAttackMoveController_t1979379996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpiderAttackMoveController::Explode()
extern "C"  void SpiderAttackMoveController_Explode_m436922129 (SpiderAttackMoveController_t1979379996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpiderAttackMoveController::Main()
extern "C"  void SpiderAttackMoveController_Main_m371303609 (SpiderAttackMoveController_t1979379996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
