﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpiderReturnMoveController
struct SpiderReturnMoveController_t1990791070;

#include "codegen/il2cpp-codegen.h"

// System.Void SpiderReturnMoveController::.ctor()
extern "C"  void SpiderReturnMoveController__ctor_m3231045436 (SpiderReturnMoveController_t1990791070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpiderReturnMoveController::Awake()
extern "C"  void SpiderReturnMoveController_Awake_m1782260405 (SpiderReturnMoveController_t1990791070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpiderReturnMoveController::Update()
extern "C"  void SpiderReturnMoveController_Update_m2898212633 (SpiderReturnMoveController_t1990791070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpiderReturnMoveController::Main()
extern "C"  void SpiderReturnMoveController_Main_m1180062211 (SpiderReturnMoveController_t1990791070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
