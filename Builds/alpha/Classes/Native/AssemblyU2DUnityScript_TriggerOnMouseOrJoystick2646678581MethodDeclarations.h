﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TriggerOnMouseOrJoystick
struct TriggerOnMouseOrJoystick_t2646678581;

#include "codegen/il2cpp-codegen.h"

// System.Void TriggerOnMouseOrJoystick::.ctor()
extern "C"  void TriggerOnMouseOrJoystick__ctor_m4284235485 (TriggerOnMouseOrJoystick_t2646678581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TriggerOnMouseOrJoystick::Start()
extern "C"  void TriggerOnMouseOrJoystick_Start_m3896495933 (TriggerOnMouseOrJoystick_t2646678581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TriggerOnMouseOrJoystick::Update()
extern "C"  void TriggerOnMouseOrJoystick_Update_m1004982786 (TriggerOnMouseOrJoystick_t2646678581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TriggerOnMouseOrJoystick::Main()
extern "C"  void TriggerOnMouseOrJoystick_Main_m1799984870 (TriggerOnMouseOrJoystick_t2646678581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
