﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// conveyorBelt
struct conveyorBelt_t3030136244;

#include "codegen/il2cpp-codegen.h"

// System.Void conveyorBelt::.ctor()
extern "C"  void conveyorBelt__ctor_m3553230578 (conveyorBelt_t3030136244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void conveyorBelt::Start()
extern "C"  void conveyorBelt_Start_m1595979090 (conveyorBelt_t3030136244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void conveyorBelt::OnBecameVisible()
extern "C"  void conveyorBelt_OnBecameVisible_m3434916266 (conveyorBelt_t3030136244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void conveyorBelt::OnBecameInvisible()
extern "C"  void conveyorBelt_OnBecameInvisible_m3309334001 (conveyorBelt_t3030136244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void conveyorBelt::Update()
extern "C"  void conveyorBelt_Update_m4177102773 (conveyorBelt_t3030136244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void conveyorBelt::Main()
extern "C"  void conveyorBelt_Main_m4071695115 (conveyorBelt_t3030136244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
