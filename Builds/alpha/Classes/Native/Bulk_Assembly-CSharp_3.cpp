﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityTest.TestComponent
struct TestComponent_t2516511601;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t1989381442;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t3961629604;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Func`2<System.String,System.String>
struct Func_2_t193026957;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2825504181;
// System.Collections.Generic.IEnumerable`1<System.Type>
struct IEnumerable_1_t1595930271;
// System.Collections.Generic.IEnumerable`1<System.Reflection.Assembly>
struct IEnumerable_1_t265572139;
// System.Func`2<System.Reflection.Assembly,System.Collections.Generic.IEnumerable`1<System.Type>>
struct Func_2_t101629490;
// System.Func`2<System.Object,System.Collections.Generic.IEnumerable`1<System.Object>>
struct Func_2_t3117631226;
// System.Func`2<System.Type,System.Boolean>
struct Func_2_t2011960077;
// System.Object
struct Il2CppObject;
// UnityTest.ITestComponent
struct ITestComponent_t2920761518;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityTest.TestComponent>
struct List_1_t1885632733;
// System.Collections.Generic.IEnumerable`1<UnityTest.TestComponent>
struct IEnumerable_1_t2808638646;
// System.Collections.IEnumerable
struct IEnumerable_t2911409499;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Func`2<UnityTest.TestComponent,System.Boolean>
struct Func_2_t4279871162;
// System.Reflection.Assembly
struct Assembly_t4268412390;
// UnityTest.TestComponent/<GetTypeByName>c__AnonStorey1
struct U3CGetTypeByNameU3Ec__AnonStorey1_t2811533093;
// UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0
struct U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t3074294349;
// UnityTest.TestComponent/NullTestComponentImpl
struct NullTestComponentImpl_t2842545593;
// UnityTest.TestResult
struct TestResult_t490498461;
// UnityTest.TestRunner
struct TestRunner_t1304041832;
// System.Func`2<System.Type,System.String>
struct Func_2_t215605592;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// System.Collections.Generic.IEnumerable`1<UnityEngine.MonoBehaviour>
struct IEnumerable_1_t1450457017;
// System.Collections.Generic.IEnumerable`1<UnityTest.TestResult>
struct IEnumerable_1_t782625506;
// System.Func`2<UnityTest.TestComponent,UnityTest.TestResult>
struct Func_2_t944794905;
// System.Collections.Generic.List`1<UnityTest.TestResult>
struct List_1_t4154586889;
// System.Collections.Generic.IEnumerable`1<UnityTest.ITestComponent>
struct IEnumerable_1_t3212888563;
// System.Func`2<UnityTest.TestResult,UnityTest.ITestComponent>
struct Func_2_t1804997582;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Component>
struct IEnumerable_1_t4111503516;
// System.Func`2<UnityEngine.Component,System.Boolean>
struct Func_2_t188999804;
// UnityTest.TestComponent[]
struct TestComponentU5BU5D_t3932875692;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Func`2<UnityTest.TestResult,System.Boolean>
struct Func_2_t2709810782;
// System.Collections.Generic.List`1<UnityTest.ITestComponent>
struct List_1_t2289882650;
// System.Func`2<UnityTest.TestResult,System.String>
struct Func_2_t913456297;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Object>
struct IEnumerable_1_t1313729162;
// UnityEngine.Object
struct Object_t1021602117;
// UnityTest.TestRunner/<ParseListForGroups>c__AnonStorey1
struct U3CParseListForGroupsU3Ec__AnonStorey1_t3712145952;
// UnityEngine.Component
struct Component_t3819376471;
// UnityTest.TestRunner/<StateMachine>c__Iterator0
struct U3CStateMachineU3Ec__Iterator0_t817978249;
// UnityTest.AssertionComponent[]
struct AssertionComponentU5BU5D_t2921566306;
// System.Collections.Generic.IEnumerable`1<UnityTest.AssertionComponent>
struct IEnumerable_1_t4254546360;
// System.Func`2<UnityTest.AssertionComponent,System.Boolean>
struct Func_2_t3268561776;
// UnityTest.AssertionComponent
struct AssertionComponent_t3962419315;
// UnityTest.TestRunnerConfigurator
struct TestRunnerConfigurator_t1966496711;
// UnityTest.IntegrationTestRunner.ITestRunnerCallback
struct ITestRunnerCallback_t327193412;
// UnityTest.TransformComparer
struct TransformComparer_t561999009;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityTest.ValueDoesNotChange
struct ValueDoesNotChange_t1999561901;
// UnityTest.Vector2Comparer
struct Vector2Comparer_t1532566132;
// UnityTest.Vector3Comparer
struct Vector3Comparer_t1524406581;
// UnityTest.Vector4Comparer
struct Vector4Comparer_t1534646382;
// User
struct User_t719925459;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_UnityTest_TestComponent2516511601.h"
#include "AssemblyU2DCSharp_UnityTest_TestComponent2516511601MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_TestComponent_Included3258709055.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "System_Core_System_Func_2_gen1989381442MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable2148412300MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "System_Core_System_Func_2_gen1989381442.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "System_Core_System_Linq_Enumerable2148412300.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "System_Core_System_Func_2_gen193026957MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "System_Core_System_Func_2_gen193026957.h"
#include "mscorlib_System_Double4078015681.h"
#include "AssemblyU2DCSharp_UnityTest_TestComponent_U3CGetTy2811533093MethodDeclarations.h"
#include "mscorlib_System_AppDomain2719102437MethodDeclarations.h"
#include "System_Core_System_Func_2_gen101629490MethodDeclarations.h"
#include "System_Core_System_Func_2_gen2011960077MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_TestComponent_U3CGetTy2811533093.h"
#include "mscorlib_System_AppDomain2719102437.h"
#include "mscorlib_System_Reflection_Assembly4268412390.h"
#include "System_Core_System_Func_2_gen101629490.h"
#include "System_Core_System_Func_2_gen2011960077.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "AssemblyU2DCSharp_IntegrationTest_ExpectExceptions1126243032.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199.h"
#include "AssemblyU2DCSharp_IntegrationTest_TimeoutAttribute3752972774.h"
#include "AssemblyU2DCSharp_IntegrationTest_IgnoreAttribute323259989.h"
#include "AssemblyU2DCSharp_IntegrationTest_SucceedWithAsserti84104168.h"
#include "AssemblyU2DCSharp_IntegrationTest_ExcludePlatformA4117182796.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1885632733.h"
#include "UnityEngine_UnityEngine_Resources339470017MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "System_Core_System_Func_2_gen4279871162MethodDeclarations.h"
#include "System_Core_System_Func_2_gen4279871162.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1885632733MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1420362407.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1420362407MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_TestComponent_U3CGetTyp722069460MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_TestComponent_U3CGetTyp722069460.h"
#include "AssemblyU2DCSharp_UnityTest_TestComponent_NullTest2842545593MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_TestComponent_NullTest2842545593.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "mscorlib_System_Reflection_Assembly4268412390MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "mscorlib_System_Reflection_ReflectionTypeLoadExcep4074666396MethodDeclarations.h"
#include "AssemblyU2DCSharp_IntegrationTest_DynamicTestAttri2184122240MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_Reflection_ReflectionTypeLoadExcep4074666396.h"
#include "mscorlib_System_Exception1927440687.h"
#include "AssemblyU2DCSharp_IntegrationTest_DynamicTestAttri2184122240.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "mscorlib_System_Threading_Interlocked1625106012MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_TestComponent_Included3258709055MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException2785117854MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException2785117854.h"
#include "AssemblyU2DCSharp_UnityTest_TestResult490498461.h"
#include "AssemblyU2DCSharp_UnityTest_TestResult490498461MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_TestResult_ResultType581735912.h"
#include "AssemblyU2DCSharp_UnityTest_TestResultState1620411406.h"
#include "mscorlib_System_Exception1927440687MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_TestResult_ResultType581735912MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_TestResultState1620411406MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_TestRunner1304041832.h"
#include "AssemblyU2DCSharp_UnityTest_TestRunner1304041832MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4154586889MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_IntegrationTestRunner_3689696405MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4154586889.h"
#include "AssemblyU2DCSharp_UnityTest_IntegrationTestRunner_3689696405.h"
#include "AssemblyU2DCSharp_UnityTest_TestRunnerConfigurator1966496711MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_TestRunnerConfigurator1966496711.h"
#include "System_Core_System_Func_2_gen215605592MethodDeclarations.h"
#include "System_Core_System_Func_2_gen215605592.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "UnityEngine_UnityEngine_Application_LogCallback1867914413MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365MethodDeclarations.h"
#include "System_Core_System_Func_2_gen944794905MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1804997582MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_IntegrationTestRunner_1666533096MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat933071039.h"
#include "UnityEngine_UnityEngine_LogType1559732862.h"
#include "UnityEngine_UnityEngine_Application_LogCallback1867914413.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat933071039MethodDeclarations.h"
#include "System_Core_System_Func_2_gen944794905.h"
#include "System_Core_System_Func_2_gen1804997582.h"
#include "AssemblyU2DCSharp_UnityTest_IntegrationTestRunner_1666533096.h"
#include "System_Core_System_Collections_Generic_HashSet_1_ge849972455MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_TestRunner_U3CParseLis3712145952MethodDeclarations.h"
#include "System_Core_System_Func_2_gen188999804MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_ge849972455.h"
#include "AssemblyU2DCSharp_UnityTest_TestRunner_U3CParseLis3712145952.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "System_Core_System_Func_2_gen188999804.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "System_Core_System_Func_2_gen2709810782MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2289882650.h"
#include "System_Core_System_Func_2_gen2709810782.h"
#include "AssemblyU2DCSharp_UnityTest_TestRunner_TestState193983825.h"
#include "AssemblyU2DCSharp_UnityTest_TestRunner_U3CStateMach817978249MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_TestRunner_U3CStateMach817978249.h"
#include "System_Core_System_Func_2_gen913456297MethodDeclarations.h"
#include "System_Core_System_Func_2_gen913456297.h"
#include "mscorlib_System_IO_Path41728875MethodDeclarations.h"
#include "AssemblyU2DCSharp_TestResultRenderer1986929490MethodDeclarations.h"
#include "AssemblyU2DCSharp_TestResultRenderer1986929490.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3268561776MethodDeclarations.h"
#include "AssemblyU2DCSharp_IntegrationTest1033695346MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_AssertionComponent3962419315.h"
#include "System_Core_System_Func_2_gen3268561776.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_TestRunner_TestState193983825MethodDeclarations.h"
#include "mscorlib_System_StringSplitOptions2996162939.h"
#include "UnityEngine_UnityEngine_TextAsset3973159845MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAsset3973159845.h"
#include "AssemblyU2DCSharp_UnityTest_TransformComparer561999009.h"
#include "AssemblyU2DCSharp_UnityTest_TransformComparer561999009MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_1_g546183785MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_TransformComparer_Comp3894166528.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_UnityTest_TransformComparer_Comp3894166528MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_ValueDoesNotChange1999561901.h"
#include "AssemblyU2DCSharp_UnityTest_ValueDoesNotChange1999561901MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_ActionBase1823832315MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_Vector2Comparer1532566132.h"
#include "AssemblyU2DCSharp_UnityTest_Vector2Comparer1532566132MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_VectorComparerBase_1_g1329214030MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_UnityTest_Vector2Comparer_Compar3262516535.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_Vector2Comparer_Compar3262516535MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_Vector3Comparer1524406581.h"
#include "AssemblyU2DCSharp_UnityTest_Vector3Comparer1524406581MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_VectorComparerBase_1_g1329214031MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_Vector3Comparer_Compar1743505528.h"
#include "AssemblyU2DCSharp_UnityTest_Vector3Comparer_Compar1743505528MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_Vector4Comparer1534646382.h"
#include "AssemblyU2DCSharp_UnityTest_Vector4Comparer1534646382MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_VectorComparerBase_1_g1329214032MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "AssemblyU2DCSharp_UnityTest_Vector4Comparer_Compar1036948413.h"
#include "UnityEngine_UnityEngine_Vector42243707581MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_Vector4Comparer_Compar1036948413MethodDeclarations.h"
#include "AssemblyU2DCSharp_User719925459.h"
#include "AssemblyU2DCSharp_User719925459MethodDeclarations.h"

// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  bool Enumerable_Any_TisIl2CppObject_m3647649381_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t3961629604 * p1, const MethodInfo* method);
#define Enumerable_Any_TisIl2CppObject_m3647649381(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3961629604 *, const MethodInfo*))Enumerable_Any_TisIl2CppObject_m3647649381_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.Linq.Enumerable::Any<System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Any_TisString_t_m2705500165(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1989381442 *, const MethodInfo*))Enumerable_Any_TisIl2CppObject_m3647649381_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m3383050844_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2825504181 * p1, const MethodInfo* method);
#define Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m3383050844(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2825504181 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m3383050844_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.String,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisString_t_TisString_t_m4026255516(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t193026957 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m3383050844_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::SelectMany<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Collections.Generic.IEnumerable`1<!!1>>)
extern "C"  Il2CppObject* Enumerable_SelectMany_TisIl2CppObject_TisIl2CppObject_m1097741174_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t3117631226 * p1, const MethodInfo* method);
#define Enumerable_SelectMany_TisIl2CppObject_TisIl2CppObject_m1097741174(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3117631226 *, const MethodInfo*))Enumerable_SelectMany_TisIl2CppObject_TisIl2CppObject_m1097741174_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::SelectMany<System.Reflection.Assembly,System.Type>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Collections.Generic.IEnumerable`1<!!1>>)
#define Enumerable_SelectMany_TisAssembly_t4268412390_TisType_t_m934605457(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t101629490 *, const MethodInfo*))Enumerable_SelectMany_TisIl2CppObject_TisIl2CppObject_m1097741174_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject * Enumerable_FirstOrDefault_TisIl2CppObject_m2555526748_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t3961629604 * p1, const MethodInfo* method);
#define Enumerable_FirstOrDefault_TisIl2CppObject_m2555526748(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3961629604 *, const MethodInfo*))Enumerable_FirstOrDefault_TisIl2CppObject_m2555526748_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Linq.Enumerable::FirstOrDefault<System.Type>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_FirstOrDefault_TisType_t_m3872248161(__this /* static, unused */, p0, p1, method) ((  Type_t * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2011960077 *, const MethodInfo*))Enumerable_FirstOrDefault_TisIl2CppObject_m2555526748_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityTest.TestComponent>()
#define Component_GetComponent_TisTestComponent_t2516511601_m3603501253(__this, method) ((  TestComponent_t2516511601 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityTest.TestComponent>()
#define GameObject_GetComponent_TisTestComponent_t2516511601_m4279714025(__this, method) ((  TestComponent_t2516511601 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3175043010_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m3175043010(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3175043010_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityTest.TestComponent>()
#define GameObject_AddComponent_TisTestComponent_t2516511601_m3364043104(__this, method) ((  TestComponent_t2516511601 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3175043010_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<System.Object>(System.Collections.IEnumerable)
extern "C"  Il2CppObject* Enumerable_Cast_TisIl2CppObject_m3596005422_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Enumerable_Cast_TisIl2CppObject_m3596005422(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Enumerable_Cast_TisIl2CppObject_m3596005422_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<UnityTest.TestComponent>(System.Collections.IEnumerable)
#define Enumerable_Cast_TisTestComponent_t2516511601_m2628398324(__this /* static, unused */, p0, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Enumerable_Cast_TisIl2CppObject_m3596005422_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t2058570427 * Enumerable_ToList_TisIl2CppObject_m3160459722_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToList_TisIl2CppObject_m3160459722(__this /* static, unused */, p0, method) ((  List_1_t2058570427 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m3160459722_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<UnityTest.TestComponent>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisTestComponent_t2516511601_m1580067281(__this /* static, unused */, p0, method) ((  List_1_t1885632733 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m3160459722_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject* Enumerable_Where_TisIl2CppObject_m4266917885_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t3961629604 * p1, const MethodInfo* method);
#define Enumerable_Where_TisIl2CppObject_m4266917885(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3961629604 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m4266917885_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<UnityTest.TestComponent>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisTestComponent_t2516511601_m1029100708(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t4279871162 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m4266917885_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  bool Enumerable_Any_TisIl2CppObject_m1223591463_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_Any_TisIl2CppObject_m1223591463(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Any_TisIl2CppObject_m1223591463_gshared)(__this /* static, unused */, p0, method)
// System.Boolean System.Linq.Enumerable::Any<UnityTest.TestComponent>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Any_TisTestComponent_t2516511601_m3304620395(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Any_TisIl2CppObject_m1223591463_gshared)(__this /* static, unused */, p0, method)
// System.Boolean System.Linq.Enumerable::Any<System.Type>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Any_TisType_t_m1833564990(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Any_TisIl2CppObject_m1223591463_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::Single<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject * Enumerable_Single_TisIl2CppObject_m1522606012_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_Single_TisIl2CppObject_m1522606012(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Single_TisIl2CppObject_m1522606012_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Type,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisType_t_TisString_t_m496683439(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t215605592 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m3383050844_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisString_t_m1760476829(__this /* static, unused */, p0, method) ((  List_1_t1398341365 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m3160459722_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m1210561870_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_First_TisIl2CppObject_m1210561870(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_First_TisIl2CppObject_m1210561870_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::First<UnityEngine.MonoBehaviour>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_First_TisMonoBehaviour_t1158329972_m1779644327(__this /* static, unused */, p0, method) ((  MonoBehaviour_t1158329972 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_First_TisIl2CppObject_m1210561870_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<UnityTest.TestComponent,UnityTest.TestResult>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisTestComponent_t2516511601_TisTestResult_t490498461_m591473676(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t944794905 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m3383050844_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<UnityTest.TestResult>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisTestResult_t490498461_m2847178699(__this /* static, unused */, p0, method) ((  List_1_t4154586889 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m3160459722_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<UnityTest.TestResult,UnityTest.ITestComponent>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisTestResult_t490498461_TisITestComponent_t2920761518_m895475245(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1804997582 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m3383050844_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<UnityEngine.Component>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisComponent_t3819376471_m3579245678(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t188999804 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m4266917885_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ObjectU5BU5D_t3614634134* Enumerable_ToArray_TisIl2CppObject_m457587038_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisIl2CppObject_m457587038(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m457587038_gshared)(__this /* static, unused */, p0, method)
// !!0[] System.Linq.Enumerable::ToArray<UnityTest.TestComponent>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisTestComponent_t2516511601_m1401798956(__this /* static, unused */, p0, method) ((  TestComponentU5BU5D_t3932875692* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m457587038_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::Single<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  Il2CppObject * Enumerable_Single_TisIl2CppObject_m2550146944_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t3961629604 * p1, const MethodInfo* method);
#define Enumerable_Single_TisIl2CppObject_m2550146944(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3961629604 *, const MethodInfo*))Enumerable_Single_TisIl2CppObject_m2550146944_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Linq.Enumerable::Single<UnityTest.TestResult>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Single_TisTestResult_t490498461_m3341528866(__this /* static, unused */, p0, p1, method) ((  TestResult_t490498461 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2709810782 *, const MethodInfo*))Enumerable_Single_TisIl2CppObject_m2550146944_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<UnityTest.ITestComponent>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisITestComponent_t2920761518_m1081207268(__this /* static, unused */, p0, method) ((  List_1_t2289882650 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m3160459722_gshared)(__this /* static, unused */, p0, method)
// System.Int32 System.Linq.Enumerable::Count<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  int32_t Enumerable_Count_TisIl2CppObject_m2983004784_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t3961629604 * p1, const MethodInfo* method);
#define Enumerable_Count_TisIl2CppObject_m2983004784(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3961629604 *, const MethodInfo*))Enumerable_Count_TisIl2CppObject_m2983004784_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Linq.Enumerable::Count<UnityTest.TestResult>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Count_TisTestResult_t490498461_m938069614(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2709810782 *, const MethodInfo*))Enumerable_Count_TisIl2CppObject_m2983004784_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.Linq.Enumerable::Any<UnityTest.TestResult>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Any_TisTestResult_t490498461_m3528417887(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2709810782 *, const MethodInfo*))Enumerable_Any_TisIl2CppObject_m3647649381_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<UnityTest.TestResult>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisTestResult_t490498461_m820548660(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2709810782 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m4266917885_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<UnityTest.TestResult,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisTestResult_t490498461_TisString_t_m456196652(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t913456297 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m3383050844_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisString_t_m489990756(__this /* static, unused */, p0, method) ((  StringU5BU5D_t1642385972* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m457587038_gshared)(__this /* static, unused */, p0, method)
// System.Int32 System.Linq.Enumerable::Count<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  int32_t Enumerable_Count_TisIl2CppObject_m2161921400_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_Count_TisIl2CppObject_m2161921400(__this /* static, unused */, p0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Count_TisIl2CppObject_m2161921400_gshared)(__this /* static, unused */, p0, method)
// System.Int32 System.Linq.Enumerable::Count<UnityEngine.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Count_TisObject_t1021602117_m974259178(__this /* static, unused */, p0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Count_TisIl2CppObject_m2161921400_gshared)(__this /* static, unused */, p0, method)
// System.Boolean System.Linq.Enumerable::Any<UnityEngine.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Any_TisObject_t1021602117_m11244335(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Any_TisIl2CppObject_m1223591463_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityTest.TestRunner>()
#define GameObject_GetComponent_TisTestRunner_t1304041832_m607288562(__this, method) ((  TestRunner_t1304041832 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 System.Linq.Enumerable::Single<UnityEngine.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Single_TisObject_t1021602117_m4054968950(__this /* static, unused */, p0, method) ((  Object_t1021602117 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Single_TisIl2CppObject_m1522606012_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityTest.TestRunner>()
#define GameObject_AddComponent_TisTestRunner_t1304041832_m2820994889(__this, method) ((  TestRunner_t1304041832 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3175043010_gshared)(__this, method)
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponentsInChildren_TisIl2CppObject_m1467294482_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisIl2CppObject_m1467294482(__this, method) ((  ObjectU5BU5D_t3614634134* (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m1467294482_gshared)(__this, method)
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityTest.AssertionComponent>()
#define GameObject_GetComponentsInChildren_TisAssertionComponent_t3962419315_m3292534916(__this, method) ((  AssertionComponentU5BU5D_t2921566306* (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m1467294482_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<UnityTest.AssertionComponent>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisAssertionComponent_t3962419315_m1981650746(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3268561776 *, const MethodInfo*))Enumerable_Where_TisIl2CppObject_m4266917885_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<UnityTest.AssertionComponent>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisAssertionComponent_t3962419315_m607603754(__this /* static, unused */, p0, method) ((  AssertionComponentU5BU5D_t2921566306* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m457587038_gshared)(__this /* static, unused */, p0, method)
// System.Boolean System.Linq.Enumerable::Any<UnityTest.AssertionComponent>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Any_TisAssertionComponent_t3962419315_m3016556267(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_Any_TisIl2CppObject_m1223591463_gshared)(__this /* static, unused */, p0, method)
// System.Boolean System.Linq.Enumerable::All<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  bool Enumerable_All_TisIl2CppObject_m4276445924_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t3961629604 * p1, const MethodInfo* method);
#define Enumerable_All_TisIl2CppObject_m4276445924(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3961629604 *, const MethodInfo*))Enumerable_All_TisIl2CppObject_m4276445924_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.Linq.Enumerable::All<UnityTest.AssertionComponent>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_All_TisAssertionComponent_t3962419315_m579294480(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3268561776 *, const MethodInfo*))Enumerable_All_TisIl2CppObject_m4276445924_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Linq.Enumerable::ElementAt<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
extern "C"  Il2CppObject * Enumerable_ElementAt_TisIl2CppObject_m572762624_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, int32_t p1, const MethodInfo* method);
#define Enumerable_ElementAt_TisIl2CppObject_m572762624(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))Enumerable_ElementAt_TisIl2CppObject_m572762624_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Linq.Enumerable::ElementAt<System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
#define Enumerable_ElementAt_TisString_t_m1763072630(__this /* static, unused */, p0, p1, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, int32_t, const MethodInfo*))Enumerable_ElementAt_TisIl2CppObject_m572762624_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityTest.TestComponent::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TestComponent__ctor_m2022488661_MetadataUsageId;
extern "C"  void TestComponent__ctor_m2022488661 (TestComponent_t2516511601 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent__ctor_m2022488661_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_timeout_3((5.0f));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_expectedExceptionList_7(L_0);
		__this->set_includedPlatforms_9((-1));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityTest.TestComponent::IsExludedOnThisPlatform()
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1989381442_il2cpp_TypeInfo_var;
extern const MethodInfo* TestComponent_U3CIsExludedOnThisPlatformU3Em__0_m2322532271_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m1397517175_MethodInfo_var;
extern const MethodInfo* Enumerable_Any_TisString_t_m2705500165_MethodInfo_var;
extern const uint32_t TestComponent_IsExludedOnThisPlatform_m2715882924_MetadataUsageId;
extern "C"  bool TestComponent_IsExludedOnThisPlatform_m2715882924 (TestComponent_t2516511601 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_IsExludedOnThisPlatform_m2715882924_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1642385972* G_B3_0 = NULL;
	StringU5BU5D_t1642385972* G_B2_0 = NULL;
	int32_t G_B5_0 = 0;
	{
		StringU5BU5D_t1642385972* L_0 = __this->get_platformsToIgnore_10();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		StringU5BU5D_t1642385972* L_1 = __this->get_platformsToIgnore_10();
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		Func_2_t1989381442 * L_2 = ((TestComponent_t2516511601_StaticFields*)TestComponent_t2516511601_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_13();
		G_B2_0 = L_1;
		if (L_2)
		{
			G_B3_0 = L_1;
			goto IL_0029;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)TestComponent_U3CIsExludedOnThisPlatformU3Em__0_m2322532271_MethodInfo_var);
		Func_2_t1989381442 * L_4 = (Func_2_t1989381442 *)il2cpp_codegen_object_new(Func_2_t1989381442_il2cpp_TypeInfo_var);
		Func_2__ctor_m1397517175(L_4, NULL, L_3, /*hidden argument*/Func_2__ctor_m1397517175_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		((TestComponent_t2516511601_StaticFields*)TestComponent_t2516511601_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_13(L_4);
		G_B3_0 = G_B2_0;
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		Func_2_t1989381442 * L_5 = ((TestComponent_t2516511601_StaticFields*)TestComponent_t2516511601_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_13();
		bool L_6 = Enumerable_Any_TisString_t_m2705500165(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)G_B3_0, L_5, /*hidden argument*/Enumerable_Any_TisString_t_m2705500165_MethodInfo_var);
		G_B5_0 = ((int32_t)(L_6));
		goto IL_0036;
	}

IL_0035:
	{
		G_B5_0 = 0;
	}

IL_0036:
	{
		return (bool)G_B5_0;
	}
}
// System.Boolean UnityTest.TestComponent::IsAssignableFrom(System.Type,System.Type)
extern "C"  bool TestComponent_IsAssignableFrom_m1777167846 (Il2CppObject * __this /* static, unused */, Type_t * ___a0, Type_t * ___b1, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___a0;
		Type_t * L_1 = ___b1;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_0, L_1);
		return L_2;
	}
}
// System.Boolean UnityTest.TestComponent::IsExceptionExpected(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t193026957_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t2321347278_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t3799711356_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* TestComponent_U3CIsExceptionExpectedU3Em__1_m2337563019_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3438876856_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisString_t_TisString_t_m4026255516_MethodInfo_var;
extern const uint32_t TestComponent_IsExceptionExpected_m1446780962_MetadataUsageId;
extern "C"  bool TestComponent_IsExceptionExpected_m1446780962 (TestComponent_t2516511601 * __this, String_t* ___exception0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_IsExceptionExpected_m1446780962_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	bool V_2 = false;
	Type_t * V_3 = NULL;
	Type_t * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	StringU5BU5D_t1642385972* G_B6_0 = NULL;
	StringU5BU5D_t1642385972* G_B5_0 = NULL;
	Type_t * G_B12_0 = NULL;
	Type_t * G_B11_0 = NULL;
	Type_t * G_B14_0 = NULL;
	Type_t * G_B13_0 = NULL;
	{
		String_t* L_0 = ___exception0;
		NullCheck(L_0);
		String_t* L_1 = String_Trim_m2668767713(L_0, /*hidden argument*/NULL);
		___exception0 = L_1;
		bool L_2 = __this->get_expectException_6();
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		return (bool)0;
	}

IL_0015:
	{
		String_t* L_3 = __this->get_expectedExceptionList_7();
		NullCheck(L_3);
		String_t* L_4 = String_Trim_m2668767713(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		return (bool)1;
	}

IL_002c:
	{
		String_t* L_6 = __this->get_expectedExceptionList_7();
		CharU5BU5D_t1328083999* L_7 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		NullCheck(L_6);
		StringU5BU5D_t1642385972* L_8 = String_Split_m3326265864(L_6, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		Func_2_t193026957 * L_9 = ((TestComponent_t2516511601_StaticFields*)TestComponent_t2516511601_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_14();
		G_B5_0 = L_8;
		if (L_9)
		{
			G_B6_0 = L_8;
			goto IL_005a;
		}
	}
	{
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)TestComponent_U3CIsExceptionExpectedU3Em__1_m2337563019_MethodInfo_var);
		Func_2_t193026957 * L_11 = (Func_2_t193026957 *)il2cpp_codegen_object_new(Func_2_t193026957_il2cpp_TypeInfo_var);
		Func_2__ctor_m3438876856(L_11, NULL, L_10, /*hidden argument*/Func_2__ctor_m3438876856_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		((TestComponent_t2516511601_StaticFields*)TestComponent_t2516511601_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_14(L_11);
		G_B6_0 = G_B5_0;
	}

IL_005a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		Func_2_t193026957 * L_12 = ((TestComponent_t2516511601_StaticFields*)TestComponent_t2516511601_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_14();
		Il2CppObject* L_13 = Enumerable_Select_TisString_t_TisString_t_m4026255516(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)G_B6_0, L_12, /*hidden argument*/Enumerable_Select_TisString_t_TisString_t_m4026255516_MethodInfo_var);
		NullCheck(L_13);
		Il2CppObject* L_14 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.String>::GetEnumerator() */, IEnumerable_1_t2321347278_il2cpp_TypeInfo_var, L_13);
		V_1 = L_14;
	}

IL_006a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00d3;
		}

IL_006f:
		{
			Il2CppObject* L_15 = V_1;
			NullCheck(L_15);
			String_t* L_16 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.String>::get_Current() */, IEnumerator_1_t3799711356_il2cpp_TypeInfo_var, L_15);
			V_0 = L_16;
			String_t* L_17 = ___exception0;
			String_t* L_18 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_19 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
			if (!L_19)
			{
				goto IL_0089;
			}
		}

IL_0082:
		{
			V_2 = (bool)1;
			IL2CPP_LEAVE(0xF2, FINALLY_00e3);
		}

IL_0089:
		{
			String_t* L_20 = ___exception0;
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_21 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m773255995, L_20, "Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
			Type_t * L_22 = L_21;
			G_B11_0 = L_22;
			if (L_22)
			{
				G_B12_0 = L_22;
				goto IL_009c;
			}
		}

IL_0095:
		{
			String_t* L_23 = ___exception0;
			IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
			Type_t * L_24 = TestComponent_GetTypeByName_m524528909(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
			G_B12_0 = L_24;
		}

IL_009c:
		{
			V_3 = G_B12_0;
			String_t* L_25 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_26 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m773255995, L_25, "Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
			Type_t * L_27 = L_26;
			G_B13_0 = L_27;
			if (L_27)
			{
				G_B14_0 = L_27;
				goto IL_00b0;
			}
		}

IL_00a9:
		{
			String_t* L_28 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
			Type_t * L_29 = TestComponent_GetTypeByName_m524528909(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
			G_B14_0 = L_29;
		}

IL_00b0:
		{
			V_4 = G_B14_0;
			Type_t * L_30 = V_3;
			if (!L_30)
			{
				goto IL_00d3;
			}
		}

IL_00b8:
		{
			Type_t * L_31 = V_4;
			if (!L_31)
			{
				goto IL_00d3;
			}
		}

IL_00bf:
		{
			Type_t * L_32 = V_4;
			Type_t * L_33 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
			bool L_34 = TestComponent_IsAssignableFrom_m1777167846(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
			if (!L_34)
			{
				goto IL_00d3;
			}
		}

IL_00cc:
		{
			V_2 = (bool)1;
			IL2CPP_LEAVE(0xF2, FINALLY_00e3);
		}

IL_00d3:
		{
			Il2CppObject* L_35 = V_1;
			NullCheck(L_35);
			bool L_36 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_35);
			if (L_36)
			{
				goto IL_006f;
			}
		}

IL_00de:
		{
			IL2CPP_LEAVE(0xF0, FINALLY_00e3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00e3;
	}

FINALLY_00e3:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_37 = V_1;
			if (!L_37)
			{
				goto IL_00ef;
			}
		}

IL_00e9:
		{
			Il2CppObject* L_38 = V_1;
			NullCheck(L_38);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_38);
		}

IL_00ef:
		{
			IL2CPP_END_FINALLY(227)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(227)
	{
		IL2CPP_JUMP_TBL(0xF2, IL_00f2)
		IL2CPP_JUMP_TBL(0xF0, IL_00f0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00f0:
	{
		return (bool)0;
	}

IL_00f2:
	{
		bool L_39 = V_2;
		return L_39;
	}
}
// System.Boolean UnityTest.TestComponent::ShouldSucceedOnException()
extern "C"  bool TestComponent_ShouldSucceedOnException_m4007130788 (TestComponent_t2516511601 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_succeedWhenExceptionIsThrown_8();
		return L_0;
	}
}
// System.Double UnityTest.TestComponent::GetTimeout()
extern "C"  double TestComponent_GetTimeout_m3814449449 (TestComponent_t2516511601 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_timeout_3();
		return (((double)((double)L_0)));
	}
}
// System.Boolean UnityTest.TestComponent::IsIgnored()
extern "C"  bool TestComponent_IsIgnored_m2040452679 (TestComponent_t2516511601 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_ignored_4();
		return L_0;
	}
}
// System.Boolean UnityTest.TestComponent::ShouldSucceedOnAssertions()
extern "C"  bool TestComponent_ShouldSucceedOnAssertions_m942204676 (TestComponent_t2516511601 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_succeedAfterAllAssertionsAreExecuted_5();
		return L_0;
	}
}
// System.Type UnityTest.TestComponent::GetTypeByName(System.String)
extern Il2CppClass* U3CGetTypeByNameU3Ec__AnonStorey1_t2811533093_il2cpp_TypeInfo_var;
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t101629490_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2011960077_il2cpp_TypeInfo_var;
extern const MethodInfo* TestComponent_U3CGetTypeByNameU3Em__2_m2372521881_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3312994662_MethodInfo_var;
extern const MethodInfo* Enumerable_SelectMany_TisAssembly_t4268412390_TisType_t_m934605457_MethodInfo_var;
extern const MethodInfo* U3CGetTypeByNameU3Ec__AnonStorey1_U3CU3Em__0_m2219179248_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m182085854_MethodInfo_var;
extern const MethodInfo* Enumerable_FirstOrDefault_TisType_t_m3872248161_MethodInfo_var;
extern const uint32_t TestComponent_GetTypeByName_m524528909_MetadataUsageId;
extern "C"  Type_t * TestComponent_GetTypeByName_m524528909 (Il2CppObject * __this /* static, unused */, String_t* ___className0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_GetTypeByName_m524528909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetTypeByNameU3Ec__AnonStorey1_t2811533093 * V_0 = NULL;
	AssemblyU5BU5D_t1984278467* G_B2_0 = NULL;
	AssemblyU5BU5D_t1984278467* G_B1_0 = NULL;
	{
		U3CGetTypeByNameU3Ec__AnonStorey1_t2811533093 * L_0 = (U3CGetTypeByNameU3Ec__AnonStorey1_t2811533093 *)il2cpp_codegen_object_new(U3CGetTypeByNameU3Ec__AnonStorey1_t2811533093_il2cpp_TypeInfo_var);
		U3CGetTypeByNameU3Ec__AnonStorey1__ctor_m3583907470(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetTypeByNameU3Ec__AnonStorey1_t2811533093 * L_1 = V_0;
		String_t* L_2 = ___className0;
		NullCheck(L_1);
		L_1->set_className_0(L_2);
		AppDomain_t2719102437 * L_3 = AppDomain_get_CurrentDomain_m3432767403(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		AssemblyU5BU5D_t1984278467* L_4 = AppDomain_GetAssemblies_m2208916529(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		Func_2_t101629490 * L_5 = ((TestComponent_t2516511601_StaticFields*)TestComponent_t2516511601_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_15();
		G_B1_0 = L_4;
		if (L_5)
		{
			G_B2_0 = L_4;
			goto IL_002f;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)TestComponent_U3CGetTypeByNameU3Em__2_m2372521881_MethodInfo_var);
		Func_2_t101629490 * L_7 = (Func_2_t101629490 *)il2cpp_codegen_object_new(Func_2_t101629490_il2cpp_TypeInfo_var);
		Func_2__ctor_m3312994662(L_7, NULL, L_6, /*hidden argument*/Func_2__ctor_m3312994662_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		((TestComponent_t2516511601_StaticFields*)TestComponent_t2516511601_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_15(L_7);
		G_B2_0 = G_B1_0;
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		Func_2_t101629490 * L_8 = ((TestComponent_t2516511601_StaticFields*)TestComponent_t2516511601_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_15();
		Il2CppObject* L_9 = Enumerable_SelectMany_TisAssembly_t4268412390_TisType_t_m934605457(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)G_B2_0, L_8, /*hidden argument*/Enumerable_SelectMany_TisAssembly_t4268412390_TisType_t_m934605457_MethodInfo_var);
		U3CGetTypeByNameU3Ec__AnonStorey1_t2811533093 * L_10 = V_0;
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)U3CGetTypeByNameU3Ec__AnonStorey1_U3CU3Em__0_m2219179248_MethodInfo_var);
		Func_2_t2011960077 * L_12 = (Func_2_t2011960077 *)il2cpp_codegen_object_new(Func_2_t2011960077_il2cpp_TypeInfo_var);
		Func_2__ctor_m182085854(L_12, L_10, L_11, /*hidden argument*/Func_2__ctor_m182085854_MethodInfo_var);
		Type_t * L_13 = Enumerable_FirstOrDefault_TisType_t_m3872248161(NULL /*static, unused*/, L_9, L_12, /*hidden argument*/Enumerable_FirstOrDefault_TisType_t_m3872248161_MethodInfo_var);
		return L_13;
	}
}
// System.Void UnityTest.TestComponent::OnValidate()
extern "C"  void TestComponent_OnValidate_m252516352 (TestComponent_t2516511601 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_timeout_3();
		if ((!(((float)L_0) < ((float)(0.01f)))))
		{
			goto IL_001b;
		}
	}
	{
		__this->set_timeout_3((0.01f));
	}

IL_001b:
	{
		return;
	}
}
// System.Void UnityTest.TestComponent::EnableTest(System.Boolean)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoBehaviour_t1158329972_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t TestComponent_EnableTest_m2066779193_MetadataUsageId;
extern "C"  void TestComponent_EnableTest_m2066779193 (TestComponent_t2516511601 * __this, bool ___enable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_EnableTest_m2066779193_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	MonoBehaviour_t1158329972 * V_1 = NULL;
	{
		bool L_0 = ___enable0;
		if (!L_0)
		{
			goto IL_004e;
		}
	}
	{
		bool L_1 = __this->get_dynamic_11();
		if (!L_1)
		{
			goto IL_004e;
		}
	}
	{
		String_t* L_2 = __this->get_dynamicTypeName_12();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m773255995, L_2, "Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		V_0 = L_3;
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Type_t * L_5 = V_0;
		NullCheck(L_4);
		Component_t3819376471 * L_6 = GameObject_GetComponent_m306258075(L_4, L_5, /*hidden argument*/NULL);
		V_1 = ((MonoBehaviour_t1158329972 *)IsInstClass(L_6, MonoBehaviour_t1158329972_il2cpp_TypeInfo_var));
		MonoBehaviour_t1158329972 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_7, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0041;
		}
	}
	{
		MonoBehaviour_t1158329972 * L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0041:
	{
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Type_t * L_11 = V_0;
		NullCheck(L_10);
		GameObject_AddComponent_m3757565614(L_10, L_11, /*hidden argument*/NULL);
	}

IL_004e:
	{
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		bool L_13 = GameObject_get_activeSelf_m313590879(L_12, /*hidden argument*/NULL);
		bool L_14 = ___enable0;
		if ((((int32_t)L_13) == ((int32_t)L_14)))
		{
			goto IL_006b;
		}
	}
	{
		GameObject_t1756533147 * L_15 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		bool L_16 = ___enable0;
		NullCheck(L_15);
		GameObject_SetActive_m2887581199(L_15, L_16, /*hidden argument*/NULL);
	}

IL_006b:
	{
		return;
	}
}
// System.Int32 UnityTest.TestComponent::CompareTo(UnityTest.ITestComponent)
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern Il2CppClass* ITestComponent_t2920761518_il2cpp_TypeInfo_var;
extern const uint32_t TestComponent_CompareTo_m1478284906_MetadataUsageId;
extern "C"  int32_t TestComponent_CompareTo_m1478284906 (TestComponent_t2516511601 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_CompareTo_m1478284906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((TestComponent_t2516511601_StaticFields*)TestComponent_t2516511601_il2cpp_TypeInfo_var->static_fields)->get_NullTestComponent_2();
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return 1;
	}

IL_000d:
	{
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = Object_get_name_m2079638459(L_2, /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___obj0;
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = InterfaceFuncInvoker0< GameObject_t1756533147 * >::Invoke(2 /* UnityEngine.GameObject UnityTest.ITestComponent::get_gameObject() */, ITestComponent_t2920761518_il2cpp_TypeInfo_var, L_4);
		NullCheck(L_5);
		String_t* L_6 = Object_get_name_m2079638459(L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_7 = String_CompareTo_m3879609894(L_3, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		int32_t L_8 = V_0;
		if (L_8)
		{
			goto IL_004e;
		}
	}
	{
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = Object_GetInstanceID_m1920497914(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		Il2CppObject * L_11 = ___obj0;
		NullCheck(L_11);
		GameObject_t1756533147 * L_12 = InterfaceFuncInvoker0< GameObject_t1756533147 * >::Invoke(2 /* UnityEngine.GameObject UnityTest.ITestComponent::get_gameObject() */, ITestComponent_t2920761518_il2cpp_TypeInfo_var, L_11);
		NullCheck(L_12);
		int32_t L_13 = Object_GetInstanceID_m1920497914(L_12, /*hidden argument*/NULL);
		int32_t L_14 = Int32_CompareTo_m3808534558((&V_1), L_13, /*hidden argument*/NULL);
		V_0 = L_14;
	}

IL_004e:
	{
		int32_t L_15 = V_0;
		return L_15;
	}
}
// System.Boolean UnityTest.TestComponent::IsTestGroup()
extern const Il2CppType* TestComponent_t2516511601_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t TestComponent_IsTestGroup_m2601912502_MetadataUsageId;
extern "C"  bool TestComponent_IsTestGroup_m2601912502 (TestComponent_t2516511601 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_IsTestGroup_m2601912502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Component_t3819376471 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_003a;
	}

IL_0007:
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		Transform_t3275118058 * L_3 = Transform_GetChild_m3838588184(L_1, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(TestComponent_t2516511601_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_3);
		Component_t3819376471 * L_5 = Component_GetComponent_m4225719715(L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Component_t3819376471 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0036;
		}
	}
	{
		return (bool)1;
	}

IL_0036:
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003a:
	{
		int32_t L_9 = V_0;
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = GameObject_get_transform_m909382139(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = Transform_get_childCount_m881385315(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_9) < ((int32_t)L_12)))
		{
			goto IL_0007;
		}
	}
	{
		return (bool)0;
	}
}
// System.String UnityTest.TestComponent::get_Name()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TestComponent_get_Name_m1475120506_MetadataUsageId;
extern "C"  String_t* TestComponent_get_Name_m1475120506 (TestComponent_t2516511601 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_get_Name_m1475120506_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_2;
		goto IL_0026;
	}

IL_001b:
	{
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = Object_get_name_m2079638459(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0026:
	{
		return G_B3_0;
	}
}
// UnityTest.ITestComponent UnityTest.TestComponent::GetTestGroup()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTestComponent_t2516511601_m3603501253_MethodInfo_var;
extern const uint32_t TestComponent_GetTestGroup_m4074183410_MetadataUsageId;
extern "C"  Il2CppObject * TestComponent_GetTestGroup_m4074183410 (TestComponent_t2516511601 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_GetTestGroup_m4074183410_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = Transform_get_parent_m147407266(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Transform_t3275118058 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		Il2CppObject * L_5 = ((TestComponent_t2516511601_StaticFields*)TestComponent_t2516511601_il2cpp_TypeInfo_var->static_fields)->get_NullTestComponent_2();
		return L_5;
	}

IL_0023:
	{
		Transform_t3275118058 * L_6 = V_0;
		NullCheck(L_6);
		TestComponent_t2516511601 * L_7 = Component_GetComponent_TisTestComponent_t2516511601_m3603501253(L_6, /*hidden argument*/Component_GetComponent_TisTestComponent_t2516511601_m3603501253_MethodInfo_var);
		return L_7;
	}
}
// System.Boolean UnityTest.TestComponent::Equals(System.Object)
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern const uint32_t TestComponent_Equals_m1826577164_MetadataUsageId;
extern "C"  bool TestComponent_Equals_m1826577164 (TestComponent_t2516511601 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_Equals_m1826577164_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___o0;
		if (!((TestComponent_t2516511601 *)IsInstClass(L_0, TestComponent_t2516511601_il2cpp_TypeInfo_var)))
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_1 = ___o0;
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		bool L_2 = TestComponent_op_Equality_m920468941(NULL /*static, unused*/, __this, ((TestComponent_t2516511601 *)IsInstClass(L_1, TestComponent_t2516511601_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_2;
	}

IL_0018:
	{
		return (bool)0;
	}
}
// System.Int32 UnityTest.TestComponent::GetHashCode()
extern "C"  int32_t TestComponent_GetHashCode_m3482172304 (TestComponent_t2516511601 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Object_GetHashCode_m3431642059(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityTest.TestComponent::op_Equality(UnityTest.TestComponent,UnityTest.TestComponent)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t TestComponent_op_Equality_m920468941_MetadataUsageId;
extern "C"  bool TestComponent_op_Equality_m920468941 (Il2CppObject * __this /* static, unused */, TestComponent_t2516511601 * ___a0, TestComponent_t2516511601 * ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_op_Equality_m920468941_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TestComponent_t2516511601 * L_0 = ___a0;
		TestComponent_t2516511601 * L_1 = ___b1;
		bool L_2 = Object_ReferenceEquals_m3900584722(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)1;
	}

IL_000e:
	{
		TestComponent_t2516511601 * L_3 = ___a0;
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		TestComponent_t2516511601 * L_4 = ___b1;
		if (L_4)
		{
			goto IL_001c;
		}
	}

IL_001a:
	{
		return (bool)0;
	}

IL_001c:
	{
		TestComponent_t2516511601 * L_5 = ___a0;
		NullCheck(L_5);
		bool L_6 = L_5->get_dynamic_11();
		if (!L_6)
		{
			goto IL_0044;
		}
	}
	{
		TestComponent_t2516511601 * L_7 = ___b1;
		NullCheck(L_7);
		bool L_8 = L_7->get_dynamic_11();
		if (!L_8)
		{
			goto IL_0044;
		}
	}
	{
		TestComponent_t2516511601 * L_9 = ___a0;
		NullCheck(L_9);
		String_t* L_10 = L_9->get_dynamicTypeName_12();
		TestComponent_t2516511601 * L_11 = ___b1;
		NullCheck(L_11);
		String_t* L_12 = L_11->get_dynamicTypeName_12();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		return L_13;
	}

IL_0044:
	{
		TestComponent_t2516511601 * L_14 = ___a0;
		NullCheck(L_14);
		bool L_15 = L_14->get_dynamic_11();
		if (L_15)
		{
			goto IL_005a;
		}
	}
	{
		TestComponent_t2516511601 * L_16 = ___b1;
		NullCheck(L_16);
		bool L_17 = L_16->get_dynamic_11();
		if (!L_17)
		{
			goto IL_005c;
		}
	}

IL_005a:
	{
		return (bool)0;
	}

IL_005c:
	{
		TestComponent_t2516511601 * L_18 = ___a0;
		NullCheck(L_18);
		GameObject_t1756533147 * L_19 = Component_get_gameObject_m3105766835(L_18, /*hidden argument*/NULL);
		TestComponent_t2516511601 * L_20 = ___b1;
		NullCheck(L_20);
		GameObject_t1756533147 * L_21 = Component_get_gameObject_m3105766835(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_19, L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.Boolean UnityTest.TestComponent::op_Inequality(UnityTest.TestComponent,UnityTest.TestComponent)
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern const uint32_t TestComponent_op_Inequality_m3300184364_MetadataUsageId;
extern "C"  bool TestComponent_op_Inequality_m3300184364 (Il2CppObject * __this /* static, unused */, TestComponent_t2516511601 * ___a0, TestComponent_t2516511601 * ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_op_Inequality_m3300184364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TestComponent_t2516511601 * L_0 = ___a0;
		TestComponent_t2516511601 * L_1 = ___b1;
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		bool L_2 = TestComponent_op_Equality_m920468941(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// UnityTest.TestComponent UnityTest.TestComponent::CreateDynamicTest(System.Type)
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeoutAttribute_t3752972774_il2cpp_TypeInfo_var;
extern Il2CppClass* IgnoreAttribute_t323259989_il2cpp_TypeInfo_var;
extern Il2CppClass* SucceedWithAssertions_t84104168_il2cpp_TypeInfo_var;
extern Il2CppClass* ExcludePlatformAttribute_t4117182796_il2cpp_TypeInfo_var;
extern Il2CppClass* ExpectExceptions_t1126243032_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTestComponent_t2516511601_m4279714025_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029314;
extern const uint32_t TestComponent_CreateDynamicTest_m2215029740_MetadataUsageId;
extern "C"  TestComponent_t2516511601 * TestComponent_CreateDynamicTest_m2215029740 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_CreateDynamicTest_m2215029740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	TestComponent_t2516511601 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	ObjectU5BU5D_t3614634134* V_3 = NULL;
	int32_t V_4 = 0;
	ExpectExceptions_t1126243032 * V_5 = NULL;
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_2 = TestComponent_CreateTest_m1629125538(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		GameObject_t1756533147 * L_4 = L_3;
		NullCheck(L_4);
		int32_t L_5 = Object_get_hideFlags_m4158950869(L_4, /*hidden argument*/NULL);
		NullCheck(L_4);
		Object_set_hideFlags_m2204253440(L_4, ((int32_t)((int32_t)L_5|(int32_t)((int32_t)52))), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = V_0;
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = V_0;
		NullCheck(L_7);
		TestComponent_t2516511601 * L_8 = GameObject_GetComponent_TisTestComponent_t2516511601_m4279714025(L_7, /*hidden argument*/GameObject_GetComponent_TisTestComponent_t2516511601_m4279714025_MethodInfo_var);
		V_1 = L_8;
		TestComponent_t2516511601 * L_9 = V_1;
		NullCheck(L_9);
		L_9->set_dynamic_11((bool)1);
		TestComponent_t2516511601 * L_10 = V_1;
		Type_t * L_11 = ___type0;
		NullCheck(L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_11);
		NullCheck(L_10);
		L_10->set_dynamicTypeName_12(L_12);
		Type_t * L_13 = ___type0;
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = VirtFuncInvoker1< ObjectU5BU5D_t3614634134*, bool >::Invoke(12 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Boolean) */, L_13, (bool)0);
		V_3 = L_14;
		V_4 = 0;
		goto IL_0105;
	}

IL_004c:
	{
		ObjectU5BU5D_t3614634134* L_15 = V_3;
		int32_t L_16 = V_4;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		Il2CppObject * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_2 = L_18;
		Il2CppObject * L_19 = V_2;
		if (!((TimeoutAttribute_t3752972774 *)IsInstClass(L_19, TimeoutAttribute_t3752972774_il2cpp_TypeInfo_var)))
		{
			goto IL_0072;
		}
	}
	{
		TestComponent_t2516511601 * L_20 = V_1;
		Il2CppObject * L_21 = V_2;
		NullCheck(((TimeoutAttribute_t3752972774 *)IsInstClass(L_21, TimeoutAttribute_t3752972774_il2cpp_TypeInfo_var)));
		float L_22 = ((TimeoutAttribute_t3752972774 *)IsInstClass(L_21, TimeoutAttribute_t3752972774_il2cpp_TypeInfo_var))->get_timeout_0();
		NullCheck(L_20);
		L_20->set_timeout_3(L_22);
		goto IL_00ff;
	}

IL_0072:
	{
		Il2CppObject * L_23 = V_2;
		if (!((IgnoreAttribute_t323259989 *)IsInstClass(L_23, IgnoreAttribute_t323259989_il2cpp_TypeInfo_var)))
		{
			goto IL_0089;
		}
	}
	{
		TestComponent_t2516511601 * L_24 = V_1;
		NullCheck(L_24);
		L_24->set_ignored_4((bool)1);
		goto IL_00ff;
	}

IL_0089:
	{
		Il2CppObject * L_25 = V_2;
		if (!((SucceedWithAssertions_t84104168 *)IsInstClass(L_25, SucceedWithAssertions_t84104168_il2cpp_TypeInfo_var)))
		{
			goto IL_00a0;
		}
	}
	{
		TestComponent_t2516511601 * L_26 = V_1;
		NullCheck(L_26);
		L_26->set_succeedAfterAllAssertionsAreExecuted_5((bool)1);
		goto IL_00ff;
	}

IL_00a0:
	{
		Il2CppObject * L_27 = V_2;
		if (!((ExcludePlatformAttribute_t4117182796 *)IsInstClass(L_27, ExcludePlatformAttribute_t4117182796_il2cpp_TypeInfo_var)))
		{
			goto IL_00c1;
		}
	}
	{
		TestComponent_t2516511601 * L_28 = V_1;
		Il2CppObject * L_29 = V_2;
		NullCheck(((ExcludePlatformAttribute_t4117182796 *)IsInstClass(L_29, ExcludePlatformAttribute_t4117182796_il2cpp_TypeInfo_var)));
		StringU5BU5D_t1642385972* L_30 = ((ExcludePlatformAttribute_t4117182796 *)IsInstClass(L_29, ExcludePlatformAttribute_t4117182796_il2cpp_TypeInfo_var))->get_platformsToExclude_0();
		NullCheck(L_28);
		L_28->set_platformsToIgnore_10(L_30);
		goto IL_00ff;
	}

IL_00c1:
	{
		Il2CppObject * L_31 = V_2;
		if (!((ExpectExceptions_t1126243032 *)IsInstClass(L_31, ExpectExceptions_t1126243032_il2cpp_TypeInfo_var)))
		{
			goto IL_00ff;
		}
	}
	{
		Il2CppObject * L_32 = V_2;
		V_5 = ((ExpectExceptions_t1126243032 *)IsInstClass(L_32, ExpectExceptions_t1126243032_il2cpp_TypeInfo_var));
		TestComponent_t2516511601 * L_33 = V_1;
		NullCheck(L_33);
		L_33->set_expectException_6((bool)1);
		TestComponent_t2516511601 * L_34 = V_1;
		ExpectExceptions_t1126243032 * L_35 = V_5;
		NullCheck(L_35);
		StringU5BU5D_t1642385972* L_36 = L_35->get_exceptionTypeNames_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = String_Join_m1966872927(NULL /*static, unused*/, _stringLiteral372029314, L_36, /*hidden argument*/NULL);
		NullCheck(L_34);
		L_34->set_expectedExceptionList_7(L_37);
		TestComponent_t2516511601 * L_38 = V_1;
		ExpectExceptions_t1126243032 * L_39 = V_5;
		NullCheck(L_39);
		bool L_40 = L_39->get_succeedOnException_1();
		NullCheck(L_38);
		L_38->set_succeedWhenExceptionIsThrown_8(L_40);
	}

IL_00ff:
	{
		int32_t L_41 = V_4;
		V_4 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_0105:
	{
		int32_t L_42 = V_4;
		ObjectU5BU5D_t3614634134* L_43 = V_3;
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_43)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		GameObject_t1756533147 * L_44 = V_0;
		Type_t * L_45 = ___type0;
		NullCheck(L_44);
		GameObject_AddComponent_m3757565614(L_44, L_45, /*hidden argument*/NULL);
		TestComponent_t2516511601 * L_46 = V_1;
		return L_46;
	}
}
// UnityEngine.GameObject UnityTest.TestComponent::CreateTest()
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3777287312;
extern const uint32_t TestComponent_CreateTest_m1015745604_MetadataUsageId;
extern "C"  GameObject_t1756533147 * TestComponent_CreateTest_m1015745604 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_CreateTest_m1015745604_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_0 = TestComponent_CreateTest_m1629125538(NULL /*static, unused*/, _stringLiteral3777287312, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.GameObject UnityTest.TestComponent::CreateTest(System.String)
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisTestComponent_t2516511601_m3364043104_MethodInfo_var;
extern const uint32_t TestComponent_CreateTest_m1629125538_MetadataUsageId;
extern "C"  GameObject_t1756533147 * TestComponent_CreateTest_m1629125538 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_CreateTest_m1629125538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		GameObject_t1756533147 * L_1 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t1756533147 * L_2 = V_0;
		NullCheck(L_2);
		GameObject_AddComponent_TisTestComponent_t2516511601_m3364043104(L_2, /*hidden argument*/GameObject_AddComponent_TisTestComponent_t2516511601_m3364043104_MethodInfo_var);
		GameObject_t1756533147 * L_3 = V_0;
		return L_3;
	}
}
// System.Collections.Generic.List`1<UnityTest.TestComponent> UnityTest.TestComponent::FindAllTestsOnScene()
extern const Il2CppType* TestComponent_t2516511601_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Cast_TisTestComponent_t2516511601_m2628398324_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisTestComponent_t2516511601_m1580067281_MethodInfo_var;
extern const uint32_t TestComponent_FindAllTestsOnScene_m3397300754_MetadataUsageId;
extern "C"  List_1_t1885632733 * TestComponent_FindAllTestsOnScene_m3397300754 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_FindAllTestsOnScene_m3397300754_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(TestComponent_t2516511601_0_0_0_var), /*hidden argument*/NULL);
		ObjectU5BU5D_t4217747464* L_1 = Resources_FindObjectsOfTypeAll_m3365744935(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Il2CppObject* L_2 = Enumerable_Cast_TisTestComponent_t2516511601_m2628398324(NULL /*static, unused*/, (Il2CppObject *)(Il2CppObject *)L_1, /*hidden argument*/Enumerable_Cast_TisTestComponent_t2516511601_m2628398324_MethodInfo_var);
		V_0 = L_2;
		Il2CppObject* L_3 = V_0;
		List_1_t1885632733 * L_4 = Enumerable_ToList_TisTestComponent_t2516511601_m1580067281(NULL /*static, unused*/, L_3, /*hidden argument*/Enumerable_ToList_TisTestComponent_t2516511601_m1580067281_MethodInfo_var);
		return L_4;
	}
}
// System.Collections.Generic.List`1<UnityTest.TestComponent> UnityTest.TestComponent::FindAllTopTestsOnScene()
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t4279871162_il2cpp_TypeInfo_var;
extern const MethodInfo* TestComponent_U3CFindAllTopTestsOnSceneU3Em__3_m1172336192_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2932550383_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisTestComponent_t2516511601_m1029100708_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisTestComponent_t2516511601_m1580067281_MethodInfo_var;
extern const uint32_t TestComponent_FindAllTopTestsOnScene_m3180001167_MetadataUsageId;
extern "C"  List_1_t1885632733 * TestComponent_FindAllTopTestsOnScene_m3180001167 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_FindAllTopTestsOnScene_m3180001167_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1885632733 * G_B2_0 = NULL;
	List_1_t1885632733 * G_B1_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		List_1_t1885632733 * L_0 = TestComponent_FindAllTestsOnScene_m3397300754(NULL /*static, unused*/, /*hidden argument*/NULL);
		Func_2_t4279871162 * L_1 = ((TestComponent_t2516511601_StaticFields*)TestComponent_t2516511601_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_16();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001d;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TestComponent_U3CFindAllTopTestsOnSceneU3Em__3_m1172336192_MethodInfo_var);
		Func_2_t4279871162 * L_3 = (Func_2_t4279871162 *)il2cpp_codegen_object_new(Func_2_t4279871162_il2cpp_TypeInfo_var);
		Func_2__ctor_m2932550383(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m2932550383_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		((TestComponent_t2516511601_StaticFields*)TestComponent_t2516511601_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache3_16(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		Func_2_t4279871162 * L_4 = ((TestComponent_t2516511601_StaticFields*)TestComponent_t2516511601_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_16();
		Il2CppObject* L_5 = Enumerable_Where_TisTestComponent_t2516511601_m1029100708(NULL /*static, unused*/, G_B2_0, L_4, /*hidden argument*/Enumerable_Where_TisTestComponent_t2516511601_m1029100708_MethodInfo_var);
		List_1_t1885632733 * L_6 = Enumerable_ToList_TisTestComponent_t2516511601_m1580067281(NULL /*static, unused*/, L_5, /*hidden argument*/Enumerable_ToList_TisTestComponent_t2516511601_m1580067281_MethodInfo_var);
		return L_6;
	}
}
// System.Collections.Generic.List`1<UnityTest.TestComponent> UnityTest.TestComponent::FindAllDynamicTestsOnScene()
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t4279871162_il2cpp_TypeInfo_var;
extern const MethodInfo* TestComponent_U3CFindAllDynamicTestsOnSceneU3Em__4_m2867683043_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2932550383_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisTestComponent_t2516511601_m1029100708_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisTestComponent_t2516511601_m1580067281_MethodInfo_var;
extern const uint32_t TestComponent_FindAllDynamicTestsOnScene_m1702380637_MetadataUsageId;
extern "C"  List_1_t1885632733 * TestComponent_FindAllDynamicTestsOnScene_m1702380637 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_FindAllDynamicTestsOnScene_m1702380637_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1885632733 * G_B2_0 = NULL;
	List_1_t1885632733 * G_B1_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		List_1_t1885632733 * L_0 = TestComponent_FindAllTestsOnScene_m3397300754(NULL /*static, unused*/, /*hidden argument*/NULL);
		Func_2_t4279871162 * L_1 = ((TestComponent_t2516511601_StaticFields*)TestComponent_t2516511601_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_17();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001d;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TestComponent_U3CFindAllDynamicTestsOnSceneU3Em__4_m2867683043_MethodInfo_var);
		Func_2_t4279871162 * L_3 = (Func_2_t4279871162 *)il2cpp_codegen_object_new(Func_2_t4279871162_il2cpp_TypeInfo_var);
		Func_2__ctor_m2932550383(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m2932550383_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		((TestComponent_t2516511601_StaticFields*)TestComponent_t2516511601_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache4_17(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		Func_2_t4279871162 * L_4 = ((TestComponent_t2516511601_StaticFields*)TestComponent_t2516511601_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_17();
		Il2CppObject* L_5 = Enumerable_Where_TisTestComponent_t2516511601_m1029100708(NULL /*static, unused*/, G_B2_0, L_4, /*hidden argument*/Enumerable_Where_TisTestComponent_t2516511601_m1029100708_MethodInfo_var);
		List_1_t1885632733 * L_6 = Enumerable_ToList_TisTestComponent_t2516511601_m1580067281(NULL /*static, unused*/, L_5, /*hidden argument*/Enumerable_ToList_TisTestComponent_t2516511601_m1580067281_MethodInfo_var);
		return L_6;
	}
}
// System.Void UnityTest.TestComponent::DestroyAllDynamicTests()
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2967661240_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3211768222_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4102372260_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2189221652_MethodInfo_var;
extern const uint32_t TestComponent_DestroyAllDynamicTests_m273851988_MetadataUsageId;
extern "C"  void TestComponent_DestroyAllDynamicTests_m273851988 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_DestroyAllDynamicTests_m273851988_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TestComponent_t2516511601 * V_0 = NULL;
	Enumerator_t1420362407  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		List_1_t1885632733 * L_0 = TestComponent_FindAllDynamicTestsOnScene_m1702380637(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Enumerator_t1420362407  L_1 = List_1_GetEnumerator_m2967661240(L_0, /*hidden argument*/List_1_GetEnumerator_m2967661240_MethodInfo_var);
		V_1 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0023;
		}

IL_0010:
		{
			TestComponent_t2516511601 * L_2 = Enumerator_get_Current_m3211768222((&V_1), /*hidden argument*/Enumerator_get_Current_m3211768222_MethodInfo_var);
			V_0 = L_2;
			TestComponent_t2516511601 * L_3 = V_0;
			NullCheck(L_3);
			GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		}

IL_0023:
		{
			bool L_5 = Enumerator_MoveNext_m4102372260((&V_1), /*hidden argument*/Enumerator_MoveNext_m4102372260_MethodInfo_var);
			if (L_5)
			{
				goto IL_0010;
			}
		}

IL_002f:
		{
			IL2CPP_LEAVE(0x42, FINALLY_0034);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0034;
	}

FINALLY_0034:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2189221652((&V_1), /*hidden argument*/Enumerator_Dispose_m2189221652_MethodInfo_var);
		IL2CPP_END_FINALLY(52)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(52)
	{
		IL2CPP_JUMP_TBL(0x42, IL_0042)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0042:
	{
		return;
	}
}
// System.Void UnityTest.TestComponent::DisableAllTests()
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2967661240_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3211768222_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4102372260_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2189221652_MethodInfo_var;
extern const uint32_t TestComponent_DisableAllTests_m2617072481_MetadataUsageId;
extern "C"  void TestComponent_DisableAllTests_m2617072481 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_DisableAllTests_m2617072481_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TestComponent_t2516511601 * V_0 = NULL;
	Enumerator_t1420362407  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		List_1_t1885632733 * L_0 = TestComponent_FindAllTestsOnScene_m3397300754(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Enumerator_t1420362407  L_1 = List_1_GetEnumerator_m2967661240(L_0, /*hidden argument*/List_1_GetEnumerator_m2967661240_MethodInfo_var);
		V_1 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001f;
		}

IL_0010:
		{
			TestComponent_t2516511601 * L_2 = Enumerator_get_Current_m3211768222((&V_1), /*hidden argument*/Enumerator_get_Current_m3211768222_MethodInfo_var);
			V_0 = L_2;
			TestComponent_t2516511601 * L_3 = V_0;
			NullCheck(L_3);
			TestComponent_EnableTest_m2066779193(L_3, (bool)0, /*hidden argument*/NULL);
		}

IL_001f:
		{
			bool L_4 = Enumerator_MoveNext_m4102372260((&V_1), /*hidden argument*/Enumerator_MoveNext_m4102372260_MethodInfo_var);
			if (L_4)
			{
				goto IL_0010;
			}
		}

IL_002b:
		{
			IL2CPP_LEAVE(0x3E, FINALLY_0030);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2189221652((&V_1), /*hidden argument*/Enumerator_Dispose_m2189221652_MethodInfo_var);
		IL2CPP_END_FINALLY(48)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_JUMP_TBL(0x3E, IL_003e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003e:
	{
		return;
	}
}
// System.Boolean UnityTest.TestComponent::AnyTestsOnScene()
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Any_TisTestComponent_t2516511601_m3304620395_MethodInfo_var;
extern const uint32_t TestComponent_AnyTestsOnScene_m702308525_MetadataUsageId;
extern "C"  bool TestComponent_AnyTestsOnScene_m702308525 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_AnyTestsOnScene_m702308525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		List_1_t1885632733 * L_0 = TestComponent_FindAllTestsOnScene_m3397300754(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Enumerable_Any_TisTestComponent_t2516511601_m3304620395(NULL /*static, unused*/, L_0, /*hidden argument*/Enumerable_Any_TisTestComponent_t2516511601_m3304620395_MethodInfo_var);
		return L_1;
	}
}
// System.Boolean UnityTest.TestComponent::AnyDynamicTestForCurrentScene()
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Any_TisType_t_m1833564990_MethodInfo_var;
extern const uint32_t TestComponent_AnyDynamicTestForCurrentScene_m2386798880_MetadataUsageId;
extern "C"  bool TestComponent_AnyDynamicTestForCurrentScene_m2386798880 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_AnyDynamicTestForCurrentScene_m2386798880_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Scene_t1684909666  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Scene_t1684909666  L_0 = SceneManager_GetActiveScene_m2964039490(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = Scene_get_name_m745914591((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		Il2CppObject* L_2 = TestComponent_GetTypesWithHelpAttribute_m4123827388(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		bool L_3 = Enumerable_Any_TisType_t_m1833564990(NULL /*static, unused*/, L_2, /*hidden argument*/Enumerable_Any_TisType_t_m1833564990_MethodInfo_var);
		return L_3;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Type> UnityTest.TestComponent::GetTypesWithHelpAttribute(System.String)
extern Il2CppClass* U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460_il2cpp_TypeInfo_var;
extern const uint32_t TestComponent_GetTypesWithHelpAttribute_m4123827388_MetadataUsageId;
extern "C"  Il2CppObject* TestComponent_GetTypesWithHelpAttribute_m4123827388 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_GetTypesWithHelpAttribute_m4123827388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * V_0 = NULL;
	{
		U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * L_0 = (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 *)il2cpp_codegen_object_new(U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460_il2cpp_TypeInfo_var);
		U3CGetTypesWithHelpAttributeU3Ec__Iterator0__ctor_m2566897407(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * L_1 = V_0;
		String_t* L_2 = ___sceneName0;
		NullCheck(L_1);
		L_1->set_sceneName_9(L_2);
		U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * L_3 = V_0;
		U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * L_4 = L_3;
		NullCheck(L_4);
		L_4->set_U24PC_12(((int32_t)-2));
		return L_4;
	}
}
// System.Void UnityTest.TestComponent::.cctor()
extern Il2CppClass* NullTestComponentImpl_t2842545593_il2cpp_TypeInfo_var;
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern const uint32_t TestComponent__cctor_m2271889498_MetadataUsageId;
extern "C"  void TestComponent__cctor_m2271889498 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent__cctor_m2271889498_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullTestComponentImpl_t2842545593 * L_0 = (NullTestComponentImpl_t2842545593 *)il2cpp_codegen_object_new(NullTestComponentImpl_t2842545593_il2cpp_TypeInfo_var);
		NullTestComponentImpl__ctor_m102519580(L_0, /*hidden argument*/NULL);
		((TestComponent_t2516511601_StaticFields*)TestComponent_t2516511601_il2cpp_TypeInfo_var->static_fields)->set_NullTestComponent_2(L_0);
		return;
	}
}
// System.Boolean UnityTest.TestComponent::<IsExludedOnThisPlatform>m__0(System.String)
extern Il2CppClass* RuntimePlatform_t1869584967_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TestComponent_U3CIsExludedOnThisPlatformU3Em__0_m2322532271_MetadataUsageId;
extern "C"  bool TestComponent_U3CIsExludedOnThisPlatformU3Em__0_m2322532271 (Il2CppObject * __this /* static, unused */, String_t* ___platform0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_U3CIsExludedOnThisPlatformU3Em__0_m2322532271_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___platform0;
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppObject * L_2 = Box(RuntimePlatform_t1869584967_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String UnityTest.TestComponent::<IsExceptionExpected>m__1(System.String)
extern "C"  String_t* TestComponent_U3CIsExceptionExpectedU3Em__1_m2337563019 (Il2CppObject * __this /* static, unused */, String_t* ___e0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___e0;
		NullCheck(L_0);
		String_t* L_1 = String_Trim_m2668767713(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Type> UnityTest.TestComponent::<GetTypeByName>m__2(System.Reflection.Assembly)
extern "C"  Il2CppObject* TestComponent_U3CGetTypeByNameU3Em__2_m2372521881 (Il2CppObject * __this /* static, unused */, Assembly_t4268412390 * ___a0, const MethodInfo* method)
{
	{
		Assembly_t4268412390 * L_0 = ___a0;
		NullCheck(L_0);
		TypeU5BU5D_t1664964607* L_1 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(16 /* System.Type[] System.Reflection.Assembly::GetTypes() */, L_0);
		return (Il2CppObject*)L_1;
	}
}
// System.Boolean UnityTest.TestComponent::<FindAllTopTestsOnScene>m__3(UnityTest.TestComponent)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t TestComponent_U3CFindAllTopTestsOnSceneU3Em__3_m1172336192_MetadataUsageId;
extern "C"  bool TestComponent_U3CFindAllTopTestsOnSceneU3Em__3_m1172336192 (Il2CppObject * __this /* static, unused */, TestComponent_t2516511601 * ___component0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestComponent_U3CFindAllTopTestsOnSceneU3Em__3_m1172336192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TestComponent_t2516511601 * L_0 = ___component0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = GameObject_get_transform_m909382139(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Transform_get_parent_m147407266(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityTest.TestComponent::<FindAllDynamicTestsOnScene>m__4(UnityTest.TestComponent)
extern "C"  bool TestComponent_U3CFindAllDynamicTestsOnSceneU3Em__4_m2867683043 (Il2CppObject * __this /* static, unused */, TestComponent_t2516511601 * ___t0, const MethodInfo* method)
{
	{
		TestComponent_t2516511601 * L_0 = ___t0;
		NullCheck(L_0);
		bool L_1 = L_0->get_dynamic_11();
		return L_1;
	}
}
// UnityEngine.GameObject UnityTest.TestComponent::UnityTest.ITestComponent.get_gameObject()
extern "C"  GameObject_t1756533147 * TestComponent_UnityTest_ITestComponent_get_gameObject_m4259331441 (TestComponent_t2516511601 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityTest.TestComponent/<GetTypeByName>c__AnonStorey1::.ctor()
extern "C"  void U3CGetTypeByNameU3Ec__AnonStorey1__ctor_m3583907470 (U3CGetTypeByNameU3Ec__AnonStorey1_t2811533093 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityTest.TestComponent/<GetTypeByName>c__AnonStorey1::<>m__0(System.Type)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetTypeByNameU3Ec__AnonStorey1_U3CU3Em__0_m2219179248_MetadataUsageId;
extern "C"  bool U3CGetTypeByNameU3Ec__AnonStorey1_U3CU3Em__0_m2219179248 (U3CGetTypeByNameU3Ec__AnonStorey1_t2811533093 * __this, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetTypeByNameU3Ec__AnonStorey1_U3CU3Em__0_m2219179248_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_0);
		String_t* L_2 = __this->get_className_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::.ctor()
extern "C"  void U3CGetTypesWithHelpAttributeU3Ec__Iterator0__ctor_m2566897407 (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::MoveNext()
extern const Il2CppType* DynamicTestAttribute_t2184122240_0_0_0_var;
extern Il2CppClass* ReflectionTypeLoadException_t4074666396_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DynamicTestAttribute_t2184122240_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Single_TisIl2CppObject_m1522606012_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3523781249;
extern const uint32_t U3CGetTypesWithHelpAttributeU3Ec__Iterator0_MoveNext_m3800108549_MetadataUsageId;
extern "C"  bool U3CGetTypesWithHelpAttributeU3Ec__Iterator0_MoveNext_m3800108549 (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_MoveNext_m3800108549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	ReflectionTypeLoadException_t4074666396 * V_1 = NULL;
	Exception_t1927440687 * V_2 = NULL;
	ExceptionU5BU5D_t1780857142* V_3 = NULL;
	int32_t V_4 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_12();
		V_0 = L_0;
		__this->set_U24PC_12((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0168;
		}
	}
	{
		goto IL_01b1;
	}

IL_0021:
	{
		AppDomain_t2719102437 * L_2 = AppDomain_get_CurrentDomain_m3432767403(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		AssemblyU5BU5D_t1984278467* L_3 = AppDomain_GetAssemblies_m2208916529(L_2, /*hidden argument*/NULL);
		__this->set_U24locvar0_0(L_3);
		__this->set_U24locvar1_1(0);
		goto IL_0197;
	}

IL_003d:
	{
		AssemblyU5BU5D_t1984278467* L_4 = __this->get_U24locvar0_0();
		int32_t L_5 = __this->get_U24locvar1_1();
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Assembly_t4268412390 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		__this->set_U3CassemblyU3E__0_2(L_7);
		__this->set_U3CtypesU3E__1_3((TypeU5BU5D_t1664964607*)NULL);
	}

IL_0057:
	try
	{ // begin try (depth: 1)
		Assembly_t4268412390 * L_8 = __this->get_U3CassemblyU3E__0_2();
		NullCheck(L_8);
		TypeU5BU5D_t1664964607* L_9 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(16 /* System.Type[] System.Reflection.Assembly::GetTypes() */, L_8);
		__this->set_U3CtypesU3E__1_3(L_9);
		goto IL_00b7;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ReflectionTypeLoadException_t4074666396_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_006d;
		throw e;
	}

CATCH_006d:
	{ // begin catch(System.Reflection.ReflectionTypeLoadException)
		{
			V_1 = ((ReflectionTypeLoadException_t4074666396 *)__exception_local);
			Assembly_t4268412390 * L_10 = __this->get_U3CassemblyU3E__0_2();
			NullCheck(L_10);
			String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.Assembly::get_FullName() */, L_10);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_12 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3523781249, L_11, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_LogError_m3715728798(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
			ReflectionTypeLoadException_t4074666396 * L_13 = V_1;
			NullCheck(L_13);
			ExceptionU5BU5D_t1780857142* L_14 = ReflectionTypeLoadException_get_LoaderExceptions_m3628053757(L_13, /*hidden argument*/NULL);
			V_3 = L_14;
			V_4 = 0;
			goto IL_00a8;
		}

IL_0097:
		{
			ExceptionU5BU5D_t1780857142* L_15 = V_3;
			int32_t L_16 = V_4;
			NullCheck(L_15);
			int32_t L_17 = L_16;
			Exception_t1927440687 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
			V_2 = L_18;
			Exception_t1927440687 * L_19 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_LogException_m1861430175(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
			int32_t L_20 = V_4;
			V_4 = ((int32_t)((int32_t)L_20+(int32_t)1));
		}

IL_00a8:
		{
			int32_t L_21 = V_4;
			ExceptionU5BU5D_t1780857142* L_22 = V_3;
			NullCheck(L_22);
			if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))))
			{
				goto IL_0097;
			}
		}

IL_00b2:
		{
			goto IL_00b7;
		}
	} // end catch (depth: 1)

IL_00b7:
	{
		TypeU5BU5D_t1664964607* L_23 = __this->get_U3CtypesU3E__1_3();
		if (L_23)
		{
			goto IL_00c7;
		}
	}
	{
		goto IL_0189;
	}

IL_00c7:
	{
		TypeU5BU5D_t1664964607* L_24 = __this->get_U3CtypesU3E__1_3();
		__this->set_U24locvar4_4(L_24);
		__this->set_U24locvar5_5(0);
		goto IL_0176;
	}

IL_00df:
	{
		TypeU5BU5D_t1664964607* L_25 = __this->get_U24locvar4_4();
		int32_t L_26 = __this->get_U24locvar5_5();
		NullCheck(L_25);
		int32_t L_27 = L_26;
		Type_t * L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		__this->set_U3CtypeU3E__2_6(L_28);
		Type_t * L_29 = __this->get_U3CtypeU3E__2_6();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_30 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(DynamicTestAttribute_t2184122240_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_29);
		ObjectU5BU5D_t3614634134* L_31 = VirtFuncInvoker2< ObjectU5BU5D_t3614634134*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_29, L_30, (bool)1);
		__this->set_U3CattributesU3E__3_7(L_31);
		ObjectU5BU5D_t3614634134* L_32 = __this->get_U3CattributesU3E__3_7();
		NullCheck(L_32);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_32)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_0168;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_33 = __this->get_U3CattributesU3E__3_7();
		Il2CppObject * L_34 = Enumerable_Single_TisIl2CppObject_m1522606012(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_33, /*hidden argument*/Enumerable_Single_TisIl2CppObject_m1522606012_MethodInfo_var);
		__this->set_U3CaU3E__4_8(((DynamicTestAttribute_t2184122240 *)IsInstClass(L_34, DynamicTestAttribute_t2184122240_il2cpp_TypeInfo_var)));
		DynamicTestAttribute_t2184122240 * L_35 = __this->get_U3CaU3E__4_8();
		String_t* L_36 = __this->get_sceneName_9();
		NullCheck(L_35);
		bool L_37 = DynamicTestAttribute_IncludeOnScene_m1388801636(L_35, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0168;
		}
	}
	{
		Type_t * L_38 = __this->get_U3CtypeU3E__2_6();
		__this->set_U24current_10(L_38);
		bool L_39 = __this->get_U24disposing_11();
		if (L_39)
		{
			goto IL_0163;
		}
	}
	{
		__this->set_U24PC_12(1);
	}

IL_0163:
	{
		goto IL_01b3;
	}

IL_0168:
	{
		int32_t L_40 = __this->get_U24locvar5_5();
		__this->set_U24locvar5_5(((int32_t)((int32_t)L_40+(int32_t)1)));
	}

IL_0176:
	{
		int32_t L_41 = __this->get_U24locvar5_5();
		TypeU5BU5D_t1664964607* L_42 = __this->get_U24locvar4_4();
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_42)->max_length)))))))
		{
			goto IL_00df;
		}
	}

IL_0189:
	{
		int32_t L_43 = __this->get_U24locvar1_1();
		__this->set_U24locvar1_1(((int32_t)((int32_t)L_43+(int32_t)1)));
	}

IL_0197:
	{
		int32_t L_44 = __this->get_U24locvar1_1();
		AssemblyU5BU5D_t1984278467* L_45 = __this->get_U24locvar0_0();
		NullCheck(L_45);
		if ((((int32_t)L_44) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_45)->max_length)))))))
		{
			goto IL_003d;
		}
	}
	{
		__this->set_U24PC_12((-1));
	}

IL_01b1:
	{
		return (bool)0;
	}

IL_01b3:
	{
		return (bool)1;
	}
}
// System.Type UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::System.Collections.Generic.IEnumerator<System.Type>.get_Current()
extern "C"  Type_t * U3CGetTypesWithHelpAttributeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m3074508642 (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_U24current_10();
		return L_0;
	}
}
// System.Object UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetTypesWithHelpAttributeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2690547165 (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * __this, const MethodInfo* method)
{
	{
		Type_t * L_0 = __this->get_U24current_10();
		return L_0;
	}
}
// System.Void UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::Dispose()
extern "C"  void U3CGetTypesWithHelpAttributeU3Ec__Iterator0_Dispose_m3860413638 (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_11((bool)1);
		__this->set_U24PC_12((-1));
		return;
	}
}
// System.Void UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetTypesWithHelpAttributeU3Ec__Iterator0_Reset_m3258660308_MetadataUsageId;
extern "C"  void U3CGetTypesWithHelpAttributeU3Ec__Iterator0_Reset_m3258660308 (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_Reset_m3258660308_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.IEnumerator UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetTypesWithHelpAttributeU3Ec__Iterator0_System_Collections_IEnumerable_GetEnumerator_m1831714810 (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CGetTypesWithHelpAttributeU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m2870630635(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Type> UnityTest.TestComponent/<GetTypesWithHelpAttribute>c__Iterator0::System.Collections.Generic.IEnumerable<System.Type>.GetEnumerator()
extern Il2CppClass* U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetTypesWithHelpAttributeU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m2870630635_MetadataUsageId;
extern "C"  Il2CppObject* U3CGetTypesWithHelpAttributeU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m2870630635 (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m2870630635_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_12();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * L_2 = (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 *)il2cpp_codegen_object_new(U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460_il2cpp_TypeInfo_var);
		U3CGetTypesWithHelpAttributeU3Ec__Iterator0__ctor_m2566897407(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * L_3 = V_0;
		String_t* L_4 = __this->get_sceneName_9();
		NullCheck(L_3);
		L_3->set_sceneName_9(L_4);
		U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460 * L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityTest.TestComponent/NullTestComponentImpl::.ctor()
extern "C"  void NullTestComponentImpl__ctor_m102519580 (NullTestComponentImpl_t2842545593 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityTest.TestComponent/NullTestComponentImpl::CompareTo(UnityTest.ITestComponent)
extern "C"  int32_t NullTestComponentImpl_CompareTo_m3122035397 (NullTestComponentImpl_t2842545593 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(NullTestComponentImpl_t2842545593 *)__this))))
		{
			goto IL_0009;
		}
	}
	{
		return 0;
	}

IL_0009:
	{
		return (-1);
	}
}
// System.Void UnityTest.TestComponent/NullTestComponentImpl::EnableTest(System.Boolean)
extern "C"  void NullTestComponentImpl_EnableTest_m3025548296 (NullTestComponentImpl_t2842545593 * __this, bool ___enable0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean UnityTest.TestComponent/NullTestComponentImpl::IsTestGroup()
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t NullTestComponentImpl_IsTestGroup_m1361786579_MetadataUsageId;
extern "C"  bool NullTestComponentImpl_IsTestGroup_m1361786579 (NullTestComponentImpl_t2842545593 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NullTestComponentImpl_IsTestGroup_m1361786579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// UnityEngine.GameObject UnityTest.TestComponent/NullTestComponentImpl::get_gameObject()
extern "C"  GameObject_t1756533147 * NullTestComponentImpl_get_gameObject_m2806918845 (NullTestComponentImpl_t2842545593 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_U3CgameObjectU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UnityTest.TestComponent/NullTestComponentImpl::set_gameObject(UnityEngine.GameObject)
extern "C"  void NullTestComponentImpl_set_gameObject_m427609616 (NullTestComponentImpl_t2842545593 * __this, GameObject_t1756533147 * ___value0, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = ___value0;
		__this->set_U3CgameObjectU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String UnityTest.TestComponent/NullTestComponentImpl::get_Name()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t NullTestComponentImpl_get_Name_m1509109323_MetadataUsageId;
extern "C"  String_t* NullTestComponentImpl_get_Name_m1509109323 (NullTestComponentImpl_t2842545593 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NullTestComponentImpl_get_Name_m1509109323_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_0;
	}
}
// UnityTest.ITestComponent UnityTest.TestComponent/NullTestComponentImpl::GetTestGroup()
extern "C"  Il2CppObject * NullTestComponentImpl_GetTestGroup_m3736684951 (NullTestComponentImpl_t2842545593 * __this, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Boolean UnityTest.TestComponent/NullTestComponentImpl::IsExceptionExpected(System.String)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t NullTestComponentImpl_IsExceptionExpected_m1956661669_MetadataUsageId;
extern "C"  bool NullTestComponentImpl_IsExceptionExpected_m1956661669 (NullTestComponentImpl_t2842545593 * __this, String_t* ___exceptionType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NullTestComponentImpl_IsExceptionExpected_m1956661669_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean UnityTest.TestComponent/NullTestComponentImpl::ShouldSucceedOnException()
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t NullTestComponentImpl_ShouldSucceedOnException_m3622388769_MetadataUsageId;
extern "C"  bool NullTestComponentImpl_ShouldSucceedOnException_m3622388769 (NullTestComponentImpl_t2842545593 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NullTestComponentImpl_ShouldSucceedOnException_m3622388769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Double UnityTest.TestComponent/NullTestComponentImpl::GetTimeout()
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t NullTestComponentImpl_GetTimeout_m3785317274_MetadataUsageId;
extern "C"  double NullTestComponentImpl_GetTimeout_m3785317274 (NullTestComponentImpl_t2842545593 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NullTestComponentImpl_GetTimeout_m3785317274_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean UnityTest.TestComponent/NullTestComponentImpl::IsIgnored()
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t NullTestComponentImpl_IsIgnored_m3125393572_MetadataUsageId;
extern "C"  bool NullTestComponentImpl_IsIgnored_m3125393572 (NullTestComponentImpl_t2842545593 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NullTestComponentImpl_IsIgnored_m3125393572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean UnityTest.TestComponent/NullTestComponentImpl::ShouldSucceedOnAssertions()
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t NullTestComponentImpl_ShouldSucceedOnAssertions_m100223911_MetadataUsageId;
extern "C"  bool NullTestComponentImpl_ShouldSucceedOnAssertions_m100223911 (NullTestComponentImpl_t2842545593 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NullTestComponentImpl_ShouldSucceedOnAssertions_m100223911_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean UnityTest.TestComponent/NullTestComponentImpl::IsExludedOnThisPlatform()
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t NullTestComponentImpl_IsExludedOnThisPlatform_m3334361479_MetadataUsageId;
extern "C"  bool NullTestComponentImpl_IsExludedOnThisPlatform_m3334361479 (NullTestComponentImpl_t2842545593 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NullTestComponentImpl_IsExludedOnThisPlatform_m3334361479_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityTest.TestResult::.ctor(UnityTest.TestComponent)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t TestResult__ctor_m1646622649_MetadataUsageId;
extern "C"  void TestResult__ctor_m1646622649 (TestResult_t490498461 * __this, TestComponent_t2516511601 * ___testComponent0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestResult__ctor_m1646622649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		__this->set_resultType_2(3);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		TestComponent_t2516511601 * L_0 = ___testComponent0;
		__this->set_TestComponent_8(L_0);
		TestComponent_t2516511601 * L_1 = ___testComponent0;
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		__this->set_m_Go_0(L_2);
		TestComponent_t2516511601 * L_3 = ___testComponent0;
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = Object_GetInstanceID_m1920497914(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		String_t* L_6 = Int32_ToString_m2960866144((&V_0), /*hidden argument*/NULL);
		__this->set_id_6(L_6);
		TestComponent_t2516511601 * L_7 = ___testComponent0;
		NullCheck(L_7);
		bool L_8 = L_7->get_dynamic_11();
		__this->set_dynamicTest_7(L_8);
		GameObject_t1756533147 * L_9 = __this->get_m_Go_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_9, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		GameObject_t1756533147 * L_11 = __this->get_m_Go_0();
		NullCheck(L_11);
		String_t* L_12 = Object_get_name_m2079638459(L_11, /*hidden argument*/NULL);
		__this->set_m_Name_1(L_12);
	}

IL_006d:
	{
		bool L_13 = __this->get_dynamicTest_7();
		if (!L_13)
		{
			goto IL_0084;
		}
	}
	{
		TestComponent_t2516511601 * L_14 = ___testComponent0;
		NullCheck(L_14);
		String_t* L_15 = L_14->get_dynamicTypeName_12();
		__this->set_id_6(L_15);
	}

IL_0084:
	{
		return;
	}
}
// UnityEngine.GameObject UnityTest.TestResult::get_GameObject()
extern "C"  GameObject_t1756533147 * TestResult_get_GameObject_m1228274866 (TestResult_t490498461 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_m_Go_0();
		return L_0;
	}
}
// System.Void UnityTest.TestResult::Update(UnityTest.TestResult)
extern "C"  void TestResult_Update_m1863801978 (TestResult_t490498461 * __this, TestResult_t490498461 * ___oldResult0, const MethodInfo* method)
{
	{
		TestResult_t490498461 * L_0 = ___oldResult0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_resultType_2();
		__this->set_resultType_2(L_1);
		TestResult_t490498461 * L_2 = ___oldResult0;
		NullCheck(L_2);
		double L_3 = L_2->get_duration_3();
		__this->set_duration_3(L_3);
		TestResult_t490498461 * L_4 = ___oldResult0;
		NullCheck(L_4);
		String_t* L_5 = L_4->get_messages_4();
		__this->set_messages_4(L_5);
		TestResult_t490498461 * L_6 = ___oldResult0;
		NullCheck(L_6);
		String_t* L_7 = L_6->get_stacktrace_5();
		__this->set_stacktrace_5(L_7);
		return;
	}
}
// System.Void UnityTest.TestResult::Reset()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TestResult_Reset_m2760372268_MetadataUsageId;
extern "C"  void TestResult_Reset_m2760372268 (TestResult_t490498461 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestResult_Reset_m2760372268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_resultType_2(3);
		__this->set_duration_3((0.0));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_messages_4(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_stacktrace_5(L_1);
		return;
	}
}
// UnityTest.TestResultState UnityTest.TestResult::get_ResultState()
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern const uint32_t TestResult_get_ResultState_m2529456988_MetadataUsageId;
extern "C"  uint8_t TestResult_get_ResultState_m2529456988 (TestResult_t490498461 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestResult_get_ResultState_m2529456988_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_resultType_2();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002a;
		}
		if (L_1 == 1)
		{
			goto IL_002c;
		}
		if (L_1 == 2)
		{
			goto IL_0034;
		}
		if (L_1 == 3)
		{
			goto IL_0032;
		}
		if (L_1 == 4)
		{
			goto IL_002e;
		}
		if (L_1 == 5)
		{
			goto IL_0030;
		}
	}
	{
		goto IL_0036;
	}

IL_002a:
	{
		return (uint8_t)(4);
	}

IL_002c:
	{
		return (uint8_t)(5);
	}

IL_002e:
	{
		return (uint8_t)(6);
	}

IL_0030:
	{
		return (uint8_t)(3);
	}

IL_0032:
	{
		return (uint8_t)(2);
	}

IL_0034:
	{
		return (uint8_t)(7);
	}

IL_0036:
	{
		Exception_t1927440687 * L_2 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m3886110570(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}
}
// System.String UnityTest.TestResult::get_Message()
extern "C"  String_t* TestResult_get_Message_m1666796880 (TestResult_t490498461 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_messages_4();
		return L_0;
	}
}
// System.String UnityTest.TestResult::get_Logs()
extern "C"  String_t* TestResult_get_Logs_m1477073444 (TestResult_t490498461 * __this, const MethodInfo* method)
{
	{
		return (String_t*)NULL;
	}
}
// System.Boolean UnityTest.TestResult::get_Executed()
extern "C"  bool TestResult_get_Executed_m82902453 (TestResult_t490498461 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_resultType_2();
		return (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)3))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.String UnityTest.TestResult::get_Name()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t TestResult_get_Name_m1227207940_MetadataUsageId;
extern "C"  String_t* TestResult_get_Name_m1227207940 (TestResult_t490498461 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestResult_get_Name_m1227207940_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_m_Go_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		GameObject_t1756533147 * L_2 = __this->get_m_Go_0();
		NullCheck(L_2);
		String_t* L_3 = Object_get_name_m2079638459(L_2, /*hidden argument*/NULL);
		__this->set_m_Name_1(L_3);
	}

IL_0022:
	{
		String_t* L_4 = __this->get_m_Name_1();
		return L_4;
	}
}
// System.String UnityTest.TestResult::get_Id()
extern "C"  String_t* TestResult_get_Id_m192151902 (TestResult_t490498461 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_id_6();
		return L_0;
	}
}
// System.Boolean UnityTest.TestResult::get_IsSuccess()
extern "C"  bool TestResult_get_IsSuccess_m4143018983 (TestResult_t490498461 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_resultType_2();
		return (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityTest.TestResult::get_IsTimeout()
extern "C"  bool TestResult_get_IsTimeout_m1546464887 (TestResult_t490498461 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_resultType_2();
		return (bool)((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
	}
}
// System.Double UnityTest.TestResult::get_Duration()
extern "C"  double TestResult_get_Duration_m117948065 (TestResult_t490498461 * __this, const MethodInfo* method)
{
	{
		double L_0 = __this->get_duration_3();
		return L_0;
	}
}
// System.String UnityTest.TestResult::get_StackTrace()
extern "C"  String_t* TestResult_get_StackTrace_m4054519502 (TestResult_t490498461 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_stacktrace_5();
		return L_0;
	}
}
// System.String UnityTest.TestResult::get_FullName()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029316;
extern const uint32_t TestResult_get_FullName_m3326613931_MetadataUsageId;
extern "C"  String_t* TestResult_get_FullName_m3326613931 (TestResult_t490498461 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestResult_get_FullName_m3326613931_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Transform_t3275118058 * V_1 = NULL;
	{
		String_t* L_0 = TestResult_get_Name_m1227207940(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1756533147 * L_1 = __this->get_m_Go_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0058;
		}
	}
	{
		GameObject_t1756533147 * L_3 = __this->get_m_Go_0();
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Transform_get_parent_m147407266(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_004c;
	}

IL_002e:
	{
		Transform_t3275118058 * L_6 = V_1;
		NullCheck(L_6);
		String_t* L_7 = Object_get_name_m2079638459(L_6, /*hidden argument*/NULL);
		String_t* L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m612901809(NULL /*static, unused*/, L_7, _stringLiteral372029316, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		Transform_t3275118058 * L_10 = V_1;
		NullCheck(L_10);
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = Transform_get_parent_m147407266(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
	}

IL_004c:
	{
		Transform_t3275118058 * L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_13, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_002e;
		}
	}

IL_0058:
	{
		String_t* L_15 = V_0;
		return L_15;
	}
}
// System.Boolean UnityTest.TestResult::get_IsIgnored()
extern "C"  bool TestResult_get_IsIgnored_m638715012 (TestResult_t490498461 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_resultType_2();
		return (bool)((((int32_t)L_0) == ((int32_t)5))? 1 : 0);
	}
}
// System.Boolean UnityTest.TestResult::get_IsFailure()
extern "C"  bool TestResult_get_IsFailure_m4065885444 (TestResult_t490498461 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		int32_t L_0 = __this->get_resultType_2();
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_1 = __this->get_resultType_2();
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_2 = __this->get_resultType_2();
		G_B4_0 = ((((int32_t)L_2) == ((int32_t)2))? 1 : 0);
		goto IL_0024;
	}

IL_0023:
	{
		G_B4_0 = 1;
	}

IL_0024:
	{
		return (bool)G_B4_0;
	}
}
// System.Int32 UnityTest.TestResult::GetHashCode()
extern "C"  int32_t TestResult_GetHashCode_m2267492672 (TestResult_t490498461 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_id_6();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		return L_1;
	}
}
// System.Int32 UnityTest.TestResult::CompareTo(UnityTest.TestResult)
extern "C"  int32_t TestResult_CompareTo_m2768212981 (TestResult_t490498461 * __this, TestResult_t490498461 * ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = TestResult_get_Name_m1227207940(__this, /*hidden argument*/NULL);
		TestResult_t490498461 * L_1 = ___other0;
		NullCheck(L_1);
		String_t* L_2 = TestResult_get_Name_m1227207940(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_3 = String_CompareTo_m3879609894(L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if (L_4)
		{
			goto IL_0037;
		}
	}
	{
		GameObject_t1756533147 * L_5 = __this->get_m_Go_0();
		NullCheck(L_5);
		int32_t L_6 = Object_GetInstanceID_m1920497914(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		TestResult_t490498461 * L_7 = ___other0;
		NullCheck(L_7);
		GameObject_t1756533147 * L_8 = L_7->get_m_Go_0();
		NullCheck(L_8);
		int32_t L_9 = Object_GetInstanceID_m1920497914(L_8, /*hidden argument*/NULL);
		int32_t L_10 = Int32_CompareTo_m3808534558((&V_1), L_9, /*hidden argument*/NULL);
		V_0 = L_10;
	}

IL_0037:
	{
		int32_t L_11 = V_0;
		return L_11;
	}
}
// System.Boolean UnityTest.TestResult::Equals(System.Object)
extern Il2CppClass* TestResult_t490498461_il2cpp_TypeInfo_var;
extern const uint32_t TestResult_Equals_m1069160616_MetadataUsageId;
extern "C"  bool TestResult_Equals_m1069160616 (TestResult_t490498461 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestResult_Equals_m1069160616_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		if (!((TestResult_t490498461 *)IsInstClass(L_0, TestResult_t490498461_il2cpp_TypeInfo_var)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, __this);
		Il2CppObject * L_2 = ___obj0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_2);
		return (bool)((((int32_t)L_1) == ((int32_t)L_3))? 1 : 0);
	}

IL_001a:
	{
		Il2CppObject * L_4 = ___obj0;
		bool L_5 = Object_Equals_m753388391(__this, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void UnityTest.TestRunner::.ctor()
extern Il2CppClass* List_1_t4154586889_il2cpp_TypeInfo_var;
extern Il2CppClass* TestRunnerCallbackList_t3689696405_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1434038137_MethodInfo_var;
extern const uint32_t TestRunner__ctor_m2351895006_MetadataUsageId;
extern "C"  void TestRunner__ctor_m2351895006 (TestRunner_t1304041832 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner__ctor_m2351895006_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4154586889 * L_0 = (List_1_t4154586889 *)il2cpp_codegen_object_new(List_1_t4154586889_il2cpp_TypeInfo_var);
		List_1__ctor_m1434038137(L_0, /*hidden argument*/List_1__ctor_m1434038137_MethodInfo_var);
		__this->set_m_ResultList_5(L_0);
		TestRunnerCallbackList_t3689696405 * L_1 = (TestRunnerCallbackList_t3689696405 *)il2cpp_codegen_object_new(TestRunnerCallbackList_t3689696405_il2cpp_TypeInfo_var);
		TestRunnerCallbackList__ctor_m1494176539(L_1, /*hidden argument*/NULL);
		__this->set_TestRunnerCallback_13(L_1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityTest.TestRunner::get_isInitializedByRunner()
extern Il2CppClass* TestRunner_t1304041832_il2cpp_TypeInfo_var;
extern const uint32_t TestRunner_get_isInitializedByRunner_m3796614350_MetadataUsageId;
extern "C"  bool TestRunner_get_isInitializedByRunner_m3796614350 (TestRunner_t1304041832 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_get_isInitializedByRunner_m3796614350_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		bool L_1 = TestRunner_IsBatchMode_m926467007(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		return (bool)1;
	}

IL_0016:
	{
		return (bool)0;
	}
}
// System.Void UnityTest.TestRunner::Awake()
extern Il2CppClass* TestRunnerConfigurator_t1966496711_il2cpp_TypeInfo_var;
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern const uint32_t TestRunner_Awake_m463510683_MetadataUsageId;
extern "C"  void TestRunner_Awake_m463510683 (TestRunner_t1304041832 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_Awake_m463510683_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TestRunnerConfigurator_t1966496711 * L_0 = (TestRunnerConfigurator_t1966496711 *)il2cpp_codegen_object_new(TestRunnerConfigurator_t1966496711_il2cpp_TypeInfo_var);
		TestRunnerConfigurator__ctor_m705632011(L_0, /*hidden argument*/NULL);
		__this->set_m_Configurator_12(L_0);
		bool L_1 = TestRunner_get_isInitializedByRunner_m3796614350(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		TestComponent_DisableAllTests_m2617072481(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityTest.TestRunner::Start()
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t1595930271_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t3074294349_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* TestRunner_t1304041832_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t215605592_il2cpp_TypeInfo_var;
extern const MethodInfo* TestRunner_U3CStartU3Em__0_m1296735611_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m125760583_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisType_t_TisString_t_m496683439_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisString_t_m1760476829_MethodInfo_var;
extern const uint32_t TestRunner_Start_m1599190826_MetadataUsageId;
extern "C"  void TestRunner_Start_m1599190826 (TestRunner_t1304041832 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_Start_m1599190826_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Scene_t1684909666  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Type_t * V_3 = NULL;
	Il2CppObject* V_4 = NULL;
	List_1_t1885632733 * V_5 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Il2CppObject* G_B15_0 = NULL;
	List_1_t1885632733 * G_B15_1 = NULL;
	TestRunner_t1304041832 * G_B15_2 = NULL;
	Il2CppObject* G_B14_0 = NULL;
	List_1_t1885632733 * G_B14_1 = NULL;
	TestRunner_t1304041832 * G_B14_2 = NULL;
	{
		bool L_0 = TestRunner_get_isInitializedByRunner_m3796614350(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		TestRunnerConfigurator_t1966496711 * L_1 = __this->get_m_Configurator_12();
		NullCheck(L_1);
		bool L_2 = TestRunnerConfigurator_get_sendResultsOverNetwork_m2213479566(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003a;
		}
	}
	{
		TestRunnerConfigurator_t1966496711 * L_3 = __this->get_m_Configurator_12();
		NullCheck(L_3);
		Il2CppObject * L_4 = TestRunnerConfigurator_ResolveNetworkConnection_m3878511651(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Il2CppObject * L_5 = V_0;
		if (!L_5)
		{
			goto IL_003a;
		}
	}
	{
		TestRunnerCallbackList_t3689696405 * L_6 = __this->get_TestRunnerCallback_13();
		Il2CppObject * L_7 = V_0;
		NullCheck(L_6);
		TestRunnerCallbackList_Add_m1346713997(L_6, L_7, /*hidden argument*/NULL);
	}

IL_003a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		TestComponent_DestroyAllDynamicTests_m273851988(NULL /*static, unused*/, /*hidden argument*/NULL);
		Scene_t1684909666  L_8 = SceneManager_GetActiveScene_m2964039490(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_8;
		String_t* L_9 = Scene_get_name_m745914591((&V_2), /*hidden argument*/NULL);
		Il2CppObject* L_10 = TestComponent_GetTypesWithHelpAttribute_m4123827388(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		Il2CppObject* L_11 = V_1;
		NullCheck(L_11);
		Il2CppObject* L_12 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Type>::GetEnumerator() */, IEnumerable_1_t1595930271_il2cpp_TypeInfo_var, L_11);
		V_4 = L_12;
	}

IL_005a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006e;
		}

IL_005f:
		{
			Il2CppObject* L_13 = V_4;
			NullCheck(L_13);
			Type_t * L_14 = InterfaceFuncInvoker0< Type_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Type>::get_Current() */, IEnumerator_1_t3074294349_il2cpp_TypeInfo_var, L_13);
			V_3 = L_14;
			Type_t * L_15 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
			TestComponent_CreateDynamicTest_m2215029740(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		}

IL_006e:
		{
			Il2CppObject* L_16 = V_4;
			NullCheck(L_16);
			bool L_17 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_16);
			if (L_17)
			{
				goto IL_005f;
			}
		}

IL_007a:
		{
			IL2CPP_LEAVE(0x8E, FINALLY_007f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_007f;
	}

FINALLY_007f:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_18 = V_4;
			if (!L_18)
			{
				goto IL_008d;
			}
		}

IL_0086:
		{
			Il2CppObject* L_19 = V_4;
			NullCheck(L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_19);
		}

IL_008d:
		{
			IL2CPP_END_FINALLY(127)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(127)
	{
		IL2CPP_JUMP_TBL(0x8E, IL_008e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_008e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		List_1_t1885632733 * L_20 = TestComponent_FindAllTestsOnScene_m3397300754(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = L_20;
		List_1_t1885632733 * L_21 = V_5;
		Il2CppObject* L_22 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		Func_2_t215605592 * L_23 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_23();
		G_B14_0 = L_22;
		G_B14_1 = L_21;
		G_B14_2 = __this;
		if (L_23)
		{
			G_B15_0 = L_22;
			G_B15_1 = L_21;
			G_B15_2 = __this;
			goto IL_00b1;
		}
	}
	{
		IntPtr_t L_24;
		L_24.set_m_value_0((void*)(void*)TestRunner_U3CStartU3Em__0_m1296735611_MethodInfo_var);
		Func_2_t215605592 * L_25 = (Func_2_t215605592 *)il2cpp_codegen_object_new(Func_2_t215605592_il2cpp_TypeInfo_var);
		Func_2__ctor_m125760583(L_25, NULL, L_24, /*hidden argument*/Func_2__ctor_m125760583_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_23(L_25);
		G_B15_0 = G_B14_0;
		G_B15_1 = G_B14_1;
		G_B15_2 = G_B14_2;
	}

IL_00b1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		Func_2_t215605592 * L_26 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_23();
		Il2CppObject* L_27 = Enumerable_Select_TisType_t_TisString_t_m496683439(NULL /*static, unused*/, G_B15_0, L_26, /*hidden argument*/Enumerable_Select_TisType_t_TisString_t_m496683439_MethodInfo_var);
		List_1_t1398341365 * L_28 = Enumerable_ToList_TisString_t_m1760476829(NULL /*static, unused*/, L_27, /*hidden argument*/Enumerable_ToList_TisString_t_m1760476829_MethodInfo_var);
		NullCheck(G_B15_2);
		TestRunner_InitRunner_m1035923382(G_B15_2, G_B15_1, L_28, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityTest.TestRunner::InitRunner(System.Collections.Generic.List`1<UnityTest.TestComponent>,System.Collections.Generic.List`1<System.String>)
extern Il2CppClass* LogCallback_t1867914413_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoBehaviourU5BU5D_t3035069757_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* TestRunner_t1304041832_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t944794905_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t1804997582_il2cpp_TypeInfo_var;
extern Il2CppClass* IntegrationTestsProvider_t1666533096_il2cpp_TypeInfo_var;
extern const MethodInfo* TestRunner_LogHandler_m2956213297_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4025448196_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m501350698_MethodInfo_var;
extern const MethodInfo* Enumerable_First_TisMonoBehaviour_t1158329972_m1779644327_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisTestComponent_t2516511601_m3603501253_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3798822551_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1097710392_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2205157096_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisTestComponent_t2516511601_m1580067281_MethodInfo_var;
extern const MethodInfo* TestRunner_U3CInitRunnerU3Em__1_m3056360923_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2092387834_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisTestComponent_t2516511601_TisTestResult_t490498461_m591473676_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisTestResult_t490498461_m2847178699_MethodInfo_var;
extern const MethodInfo* TestRunner_U3CInitRunnerU3Em__2_m1385019429_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m64259293_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisTestResult_t490498461_TisITestComponent_t2920761518_m895475245_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3318475840;
extern Il2CppCodeGenString* _stringLiteral1579448933;
extern const uint32_t TestRunner_InitRunner_m1035923382_MetadataUsageId;
extern "C"  void TestRunner_InitRunner_m1035923382 (TestRunner_t1304041832 * __this, List_1_t1885632733 * ___tests0, List_1_t1398341365 * ___dynamicTestsToRun1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_InitRunner_m1035923382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Enumerator_t933071039  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Type_t * V_2 = NULL;
	MonoBehaviourU5BU5D_t3035069757* V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	List_1_t1885632733 * G_B14_0 = NULL;
	TestRunner_t1304041832 * G_B14_1 = NULL;
	List_1_t1885632733 * G_B13_0 = NULL;
	TestRunner_t1304041832 * G_B13_1 = NULL;
	List_1_t4154586889 * G_B16_0 = NULL;
	TestRunner_t1304041832 * G_B16_1 = NULL;
	List_1_t4154586889 * G_B15_0 = NULL;
	TestRunner_t1304041832 * G_B15_1 = NULL;
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TestRunner_LogHandler_m2956213297_MethodInfo_var);
		LogCallback_t1867914413 * L_1 = (LogCallback_t1867914413 *)il2cpp_codegen_object_new(LogCallback_t1867914413_il2cpp_TypeInfo_var);
		LogCallback__ctor_m898185969(L_1, __this, L_0, /*hidden argument*/NULL);
		Application_add_logMessageReceived_m2712372862(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		List_1_t1398341365 * L_2 = ___dynamicTestsToRun1;
		NullCheck(L_2);
		Enumerator_t933071039  L_3 = List_1_GetEnumerator_m4025448196(L_2, /*hidden argument*/List_1_GetEnumerator_m4025448196_MethodInfo_var);
		V_1 = L_3;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			goto IL_008a;
		}

IL_001d:
		{
			String_t* L_4 = Enumerator_get_Current_m501350698((&V_1), /*hidden argument*/Enumerator_get_Current_m501350698_MethodInfo_var);
			V_0 = L_4;
			String_t* L_5 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_6 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m773255995, L_5, "Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
			V_2 = L_6;
			Type_t * L_7 = V_2;
			if (L_7)
			{
				goto IL_0037;
			}
		}

IL_0032:
		{
			goto IL_008a;
		}

IL_0037:
		{
			Type_t * L_8 = V_2;
			ObjectU5BU5D_t4217747464* L_9 = Resources_FindObjectsOfTypeAll_m3365744935(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			V_3 = ((MonoBehaviourU5BU5D_t3035069757*)IsInst(L_9, MonoBehaviourU5BU5D_t3035069757_il2cpp_TypeInfo_var));
			MonoBehaviourU5BU5D_t3035069757* L_10 = V_3;
			NullCheck(L_10);
			if ((((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))
			{
				goto IL_0060;
			}
		}

IL_004b:
		{
			Type_t * L_11 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_12 = String_Concat_m56707527(NULL /*static, unused*/, L_11, _stringLiteral3318475840, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
			goto IL_008a;
		}

IL_0060:
		{
			MonoBehaviourU5BU5D_t3035069757* L_13 = V_3;
			NullCheck(L_13);
			if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length))))) <= ((int32_t)1)))
			{
				goto IL_0079;
			}
		}

IL_0069:
		{
			String_t* L_14 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_15 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1579448933, L_14, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		}

IL_0079:
		{
			List_1_t1885632733 * L_16 = ___tests0;
			MonoBehaviourU5BU5D_t3035069757* L_17 = V_3;
			MonoBehaviour_t1158329972 * L_18 = Enumerable_First_TisMonoBehaviour_t1158329972_m1779644327(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_17, /*hidden argument*/Enumerable_First_TisMonoBehaviour_t1158329972_m1779644327_MethodInfo_var);
			NullCheck(L_18);
			TestComponent_t2516511601 * L_19 = Component_GetComponent_TisTestComponent_t2516511601_m3603501253(L_18, /*hidden argument*/Component_GetComponent_TisTestComponent_t2516511601_m3603501253_MethodInfo_var);
			NullCheck(L_16);
			List_1_Add_m3798822551(L_16, L_19, /*hidden argument*/List_1_Add_m3798822551_MethodInfo_var);
		}

IL_008a:
		{
			bool L_20 = Enumerator_MoveNext_m1097710392((&V_1), /*hidden argument*/Enumerator_MoveNext_m1097710392_MethodInfo_var);
			if (L_20)
			{
				goto IL_001d;
			}
		}

IL_0096:
		{
			IL2CPP_LEAVE(0xA9, FINALLY_009b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_009b;
	}

FINALLY_009b:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2205157096((&V_1), /*hidden argument*/Enumerator_Dispose_m2205157096_MethodInfo_var);
		IL2CPP_END_FINALLY(155)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(155)
	{
		IL2CPP_JUMP_TBL(0xA9, IL_00a9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00a9:
	{
		List_1_t1885632733 * L_21 = ___tests0;
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		Il2CppObject* L_22 = TestRunner_ParseListForGroups_m752197201(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		List_1_t1885632733 * L_23 = Enumerable_ToList_TisTestComponent_t2516511601_m1580067281(NULL /*static, unused*/, L_22, /*hidden argument*/Enumerable_ToList_TisTestComponent_t2516511601_m1580067281_MethodInfo_var);
		__this->set_m_TestComponents_6(L_23);
		List_1_t1885632733 * L_24 = __this->get_m_TestComponents_6();
		Func_2_t944794905 * L_25 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_24();
		G_B13_0 = L_24;
		G_B13_1 = __this;
		if (L_25)
		{
			G_B14_0 = L_24;
			G_B14_1 = __this;
			goto IL_00d9;
		}
	}
	{
		IntPtr_t L_26;
		L_26.set_m_value_0((void*)(void*)TestRunner_U3CInitRunnerU3Em__1_m3056360923_MethodInfo_var);
		Func_2_t944794905 * L_27 = (Func_2_t944794905 *)il2cpp_codegen_object_new(Func_2_t944794905_il2cpp_TypeInfo_var);
		Func_2__ctor_m2092387834(L_27, NULL, L_26, /*hidden argument*/Func_2__ctor_m2092387834_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_24(L_27);
		G_B14_0 = G_B13_0;
		G_B14_1 = G_B13_1;
	}

IL_00d9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		Func_2_t944794905 * L_28 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_24();
		Il2CppObject* L_29 = Enumerable_Select_TisTestComponent_t2516511601_TisTestResult_t490498461_m591473676(NULL /*static, unused*/, G_B14_0, L_28, /*hidden argument*/Enumerable_Select_TisTestComponent_t2516511601_TisTestResult_t490498461_m591473676_MethodInfo_var);
		List_1_t4154586889 * L_30 = Enumerable_ToList_TisTestResult_t490498461_m2847178699(NULL /*static, unused*/, L_29, /*hidden argument*/Enumerable_ToList_TisTestResult_t490498461_m2847178699_MethodInfo_var);
		NullCheck(G_B14_1);
		G_B14_1->set_m_ResultList_5(L_30);
		List_1_t4154586889 * L_31 = __this->get_m_ResultList_5();
		Func_2_t1804997582 * L_32 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_25();
		G_B15_0 = L_31;
		G_B15_1 = __this;
		if (L_32)
		{
			G_B16_0 = L_31;
			G_B16_1 = __this;
			goto IL_010c;
		}
	}
	{
		IntPtr_t L_33;
		L_33.set_m_value_0((void*)(void*)TestRunner_U3CInitRunnerU3Em__2_m1385019429_MethodInfo_var);
		Func_2_t1804997582 * L_34 = (Func_2_t1804997582 *)il2cpp_codegen_object_new(Func_2_t1804997582_il2cpp_TypeInfo_var);
		Func_2__ctor_m64259293(L_34, NULL, L_33, /*hidden argument*/Func_2__ctor_m64259293_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_25(L_34);
		G_B16_0 = G_B15_0;
		G_B16_1 = G_B15_1;
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		Func_2_t1804997582 * L_35 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_25();
		Il2CppObject* L_36 = Enumerable_Select_TisTestResult_t490498461_TisITestComponent_t2920761518_m895475245(NULL /*static, unused*/, G_B16_0, L_35, /*hidden argument*/Enumerable_Select_TisTestResult_t490498461_TisITestComponent_t2920761518_m895475245_MethodInfo_var);
		IntegrationTestsProvider_t1666533096 * L_37 = (IntegrationTestsProvider_t1666533096 *)il2cpp_codegen_object_new(IntegrationTestsProvider_t1666533096_il2cpp_TypeInfo_var);
		IntegrationTestsProvider__ctor_m3921523350(L_37, L_36, /*hidden argument*/NULL);
		NullCheck(G_B16_1);
		G_B16_1->set_m_TestsProvider_14(L_37);
		__this->set_m_ReadyToRun_8((bool)1);
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<UnityTest.TestComponent> UnityTest.TestRunner::ParseListForGroups(System.Collections.Generic.IEnumerable`1<UnityTest.TestComponent>)
extern const Il2CppType* TestComponent_t2516511601_0_0_0_var;
extern Il2CppClass* HashSet_1_t849972455_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t2808638646_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CParseListForGroupsU3Ec__AnonStorey1_t3712145952_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t4287002724_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t188999804_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1__ctor_m2539085189_MethodInfo_var;
extern const MethodInfo* U3CParseListForGroupsU3Ec__AnonStorey1_U3CU3Em__0_m764506160_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m4207380717_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisComponent_t3819376471_m3579245678_MethodInfo_var;
extern const MethodInfo* Enumerable_Cast_TisTestComponent_t2516511601_m2628398324_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisTestComponent_t2516511601_m1401798956_MethodInfo_var;
extern const MethodInfo* HashSet_1_Add_m228742973_MethodInfo_var;
extern const uint32_t TestRunner_ParseListForGroups_m752197201_MetadataUsageId;
extern "C"  Il2CppObject* TestRunner_ParseListForGroups_m752197201 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___tests0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_ParseListForGroups_m752197201_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HashSet_1_t849972455 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	U3CParseListForGroupsU3Ec__AnonStorey1_t3712145952 * V_2 = NULL;
	TestComponentU5BU5D_t3932875692* V_3 = NULL;
	TestComponent_t2516511601 * V_4 = NULL;
	TestComponentU5BU5D_t3932875692* V_5 = NULL;
	int32_t V_6 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		HashSet_1_t849972455 * L_0 = (HashSet_1_t849972455 *)il2cpp_codegen_object_new(HashSet_1_t849972455_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m2539085189(L_0, /*hidden argument*/HashSet_1__ctor_m2539085189_MethodInfo_var);
		V_0 = L_0;
		Il2CppObject* L_1 = ___tests0;
		NullCheck(L_1);
		Il2CppObject* L_2 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityTest.TestComponent>::GetEnumerator() */, IEnumerable_1_t2808638646_il2cpp_TypeInfo_var, L_1);
		V_1 = L_2;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00b5;
		}

IL_0012:
		{
			U3CParseListForGroupsU3Ec__AnonStorey1_t3712145952 * L_3 = (U3CParseListForGroupsU3Ec__AnonStorey1_t3712145952 *)il2cpp_codegen_object_new(U3CParseListForGroupsU3Ec__AnonStorey1_t3712145952_il2cpp_TypeInfo_var);
			U3CParseListForGroupsU3Ec__AnonStorey1__ctor_m898455413(L_3, /*hidden argument*/NULL);
			V_2 = L_3;
			U3CParseListForGroupsU3Ec__AnonStorey1_t3712145952 * L_4 = V_2;
			Il2CppObject* L_5 = V_1;
			NullCheck(L_5);
			TestComponent_t2516511601 * L_6 = InterfaceFuncInvoker0< TestComponent_t2516511601 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityTest.TestComponent>::get_Current() */, IEnumerator_1_t4287002724_il2cpp_TypeInfo_var, L_5);
			NullCheck(L_4);
			L_4->set_testResult_0(L_6);
			U3CParseListForGroupsU3Ec__AnonStorey1_t3712145952 * L_7 = V_2;
			NullCheck(L_7);
			TestComponent_t2516511601 * L_8 = L_7->get_testResult_0();
			NullCheck(L_8);
			bool L_9 = TestComponent_IsTestGroup_m2601912502(L_8, /*hidden argument*/NULL);
			if (!L_9)
			{
				goto IL_00a8;
			}
		}

IL_0034:
		{
			U3CParseListForGroupsU3Ec__AnonStorey1_t3712145952 * L_10 = V_2;
			NullCheck(L_10);
			TestComponent_t2516511601 * L_11 = L_10->get_testResult_0();
			NullCheck(L_11);
			GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835(L_11, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_13 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(TestComponent_t2516511601_0_0_0_var), /*hidden argument*/NULL);
			NullCheck(L_12);
			ComponentU5BU5D_t4136971630* L_14 = GameObject_GetComponentsInChildren_m993725821(L_12, L_13, (bool)1, /*hidden argument*/NULL);
			U3CParseListForGroupsU3Ec__AnonStorey1_t3712145952 * L_15 = V_2;
			IntPtr_t L_16;
			L_16.set_m_value_0((void*)(void*)U3CParseListForGroupsU3Ec__AnonStorey1_U3CU3Em__0_m764506160_MethodInfo_var);
			Func_2_t188999804 * L_17 = (Func_2_t188999804 *)il2cpp_codegen_object_new(Func_2_t188999804_il2cpp_TypeInfo_var);
			Func_2__ctor_m4207380717(L_17, L_15, L_16, /*hidden argument*/Func_2__ctor_m4207380717_MethodInfo_var);
			Il2CppObject* L_18 = Enumerable_Where_TisComponent_t3819376471_m3579245678(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_14, L_17, /*hidden argument*/Enumerable_Where_TisComponent_t3819376471_m3579245678_MethodInfo_var);
			Il2CppObject* L_19 = Enumerable_Cast_TisTestComponent_t2516511601_m2628398324(NULL /*static, unused*/, L_18, /*hidden argument*/Enumerable_Cast_TisTestComponent_t2516511601_m2628398324_MethodInfo_var);
			TestComponentU5BU5D_t3932875692* L_20 = Enumerable_ToArray_TisTestComponent_t2516511601_m1401798956(NULL /*static, unused*/, L_19, /*hidden argument*/Enumerable_ToArray_TisTestComponent_t2516511601_m1401798956_MethodInfo_var);
			V_3 = L_20;
			TestComponentU5BU5D_t3932875692* L_21 = V_3;
			V_5 = L_21;
			V_6 = 0;
			goto IL_0098;
		}

IL_0076:
		{
			TestComponentU5BU5D_t3932875692* L_22 = V_5;
			int32_t L_23 = V_6;
			NullCheck(L_22);
			int32_t L_24 = L_23;
			TestComponent_t2516511601 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
			V_4 = L_25;
			TestComponent_t2516511601 * L_26 = V_4;
			NullCheck(L_26);
			bool L_27 = TestComponent_IsTestGroup_m2601912502(L_26, /*hidden argument*/NULL);
			if (L_27)
			{
				goto IL_0092;
			}
		}

IL_0089:
		{
			HashSet_1_t849972455 * L_28 = V_0;
			TestComponent_t2516511601 * L_29 = V_4;
			NullCheck(L_28);
			HashSet_1_Add_m228742973(L_28, L_29, /*hidden argument*/HashSet_1_Add_m228742973_MethodInfo_var);
		}

IL_0092:
		{
			int32_t L_30 = V_6;
			V_6 = ((int32_t)((int32_t)L_30+(int32_t)1));
		}

IL_0098:
		{
			int32_t L_31 = V_6;
			TestComponentU5BU5D_t3932875692* L_32 = V_5;
			NullCheck(L_32);
			if ((((int32_t)L_31) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_32)->max_length)))))))
			{
				goto IL_0076;
			}
		}

IL_00a3:
		{
			goto IL_00b5;
		}

IL_00a8:
		{
			HashSet_1_t849972455 * L_33 = V_0;
			U3CParseListForGroupsU3Ec__AnonStorey1_t3712145952 * L_34 = V_2;
			NullCheck(L_34);
			TestComponent_t2516511601 * L_35 = L_34->get_testResult_0();
			NullCheck(L_33);
			HashSet_1_Add_m228742973(L_33, L_35, /*hidden argument*/HashSet_1_Add_m228742973_MethodInfo_var);
		}

IL_00b5:
		{
			Il2CppObject* L_36 = V_1;
			NullCheck(L_36);
			bool L_37 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_36);
			if (L_37)
			{
				goto IL_0012;
			}
		}

IL_00c0:
		{
			IL2CPP_LEAVE(0xD2, FINALLY_00c5);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00c5;
	}

FINALLY_00c5:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_38 = V_1;
			if (!L_38)
			{
				goto IL_00d1;
			}
		}

IL_00cb:
		{
			Il2CppObject* L_39 = V_1;
			NullCheck(L_39);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_39);
		}

IL_00d1:
		{
			IL2CPP_END_FINALLY(197)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(197)
	{
		IL2CPP_JUMP_TBL(0xD2, IL_00d2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00d2:
	{
		HashSet_1_t849972455 * L_40 = V_0;
		return L_40;
	}
}
// System.Void UnityTest.TestRunner::Update()
extern Il2CppCodeGenString* _stringLiteral4188599000;
extern const uint32_t TestRunner_Update_m4091713191_MetadataUsageId;
extern "C"  void TestRunner_Update_m4091713191 (TestRunner_t1304041832 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_Update_m4091713191_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_ReadyToRun_8();
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_1 = Time_get_frameCount_m1198768813(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0029;
		}
	}
	{
		__this->set_m_ReadyToRun_8((bool)0);
		MonoBehaviour_StartCoroutine_m1399371129(__this, _stringLiteral4188599000, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Void UnityTest.TestRunner::OnDestroy()
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2709810782_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* LogCallback_t1867914413_il2cpp_TypeInfo_var;
extern const MethodInfo* TestRunner_U3COnDestroyU3Em__3_m2714852837_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m778238545_MethodInfo_var;
extern const MethodInfo* Enumerable_Single_TisTestResult_t490498461_m3341528866_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisITestComponent_t2920761518_m1081207268_MethodInfo_var;
extern const MethodInfo* TestRunner_LogHandler_m2956213297_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral589000060;
extern Il2CppCodeGenString* _stringLiteral2468663803;
extern const uint32_t TestRunner_OnDestroy_m3373558557_MetadataUsageId;
extern "C"  void TestRunner_OnDestroy_m3373558557 (TestRunner_t1304041832 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_OnDestroy_m3373558557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TestResult_t490498461 * V_0 = NULL;
	List_1_t2289882650 * V_1 = NULL;
	{
		TestComponent_t2516511601 * L_0 = __this->get_currentTest_4();
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		bool L_1 = TestComponent_op_Inequality_m3300184364(NULL /*static, unused*/, L_0, (TestComponent_t2516511601 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0051;
		}
	}
	{
		List_1_t4154586889 * L_2 = __this->get_m_ResultList_5();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)TestRunner_U3COnDestroyU3Em__3_m2714852837_MethodInfo_var);
		Func_2_t2709810782 * L_4 = (Func_2_t2709810782 *)il2cpp_codegen_object_new(Func_2_t2709810782_il2cpp_TypeInfo_var);
		Func_2__ctor_m778238545(L_4, __this, L_3, /*hidden argument*/Func_2__ctor_m778238545_MethodInfo_var);
		TestResult_t490498461 * L_5 = Enumerable_Single_TisTestResult_t490498461_m3341528866(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/Enumerable_Single_TisTestResult_t490498461_m3341528866_MethodInfo_var);
		V_0 = L_5;
		TestResult_t490498461 * L_6 = V_0;
		TestResult_t490498461 * L_7 = L_6;
		NullCheck(L_7);
		String_t* L_8 = L_7->get_messages_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m2596409543(NULL /*static, unused*/, L_8, _stringLiteral589000060, /*hidden argument*/NULL);
		NullCheck(L_7);
		L_7->set_messages_4(L_9);
		TestRunner_LogMessage_m1520221235(__this, _stringLiteral2468663803, /*hidden argument*/NULL);
		TestRunner_FinishTest_m1325069965(__this, 1, /*hidden argument*/NULL);
	}

IL_0051:
	{
		TestComponent_t2516511601 * L_10 = __this->get_currentTest_4();
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		bool L_11 = TestComponent_op_Inequality_m3300184364(NULL /*static, unused*/, L_10, (TestComponent_t2516511601 *)NULL, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_007d;
		}
	}
	{
		IntegrationTestsProvider_t1666533096 * L_12 = __this->get_m_TestsProvider_14();
		if (!L_12)
		{
			goto IL_009a;
		}
	}
	{
		IntegrationTestsProvider_t1666533096 * L_13 = __this->get_m_TestsProvider_14();
		NullCheck(L_13);
		bool L_14 = IntegrationTestsProvider_AnyTestsLeft_m3901869230(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_009a;
		}
	}

IL_007d:
	{
		IntegrationTestsProvider_t1666533096 * L_15 = __this->get_m_TestsProvider_14();
		NullCheck(L_15);
		List_1_t2289882650 * L_16 = IntegrationTestsProvider_GetRemainingTests_m1613368701(L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		TestRunnerCallbackList_t3689696405 * L_17 = __this->get_TestRunnerCallback_13();
		List_1_t2289882650 * L_18 = V_1;
		List_1_t2289882650 * L_19 = Enumerable_ToList_TisITestComponent_t2920761518_m1081207268(NULL /*static, unused*/, L_18, /*hidden argument*/Enumerable_ToList_TisITestComponent_t2920761518_m1081207268_MethodInfo_var);
		NullCheck(L_17);
		TestRunnerCallbackList_TestRunInterrupted_m960766303(L_17, L_19, /*hidden argument*/NULL);
	}

IL_009a:
	{
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)TestRunner_LogHandler_m2956213297_MethodInfo_var);
		LogCallback_t1867914413 * L_21 = (LogCallback_t1867914413 *)il2cpp_codegen_object_new(LogCallback_t1867914413_il2cpp_TypeInfo_var);
		LogCallback__ctor_m898185969(L_21, __this, L_20, /*hidden argument*/NULL);
		Application_remove_logMessageReceived_m3667144081(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityTest.TestRunner::LogHandler(System.String,System.String,UnityEngine.LogType)
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4012603225;
extern Il2CppCodeGenString* _stringLiteral168715698;
extern Il2CppCodeGenString* _stringLiteral1761773864;
extern Il2CppCodeGenString* _stringLiteral372029318;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern Il2CppCodeGenString* _stringLiteral4294932033;
extern Il2CppCodeGenString* _stringLiteral1139922469;
extern Il2CppCodeGenString* _stringLiteral1899437160;
extern const uint32_t TestRunner_LogHandler_m2956213297_MetadataUsageId;
extern "C"  void TestRunner_LogHandler_m2956213297 (TestRunner_t1304041832 * __this, String_t* ___condition0, String_t* ___stacktrace1, int32_t ___type2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_LogHandler_m2956213297_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		String_t* L_0 = ___condition0;
		NullCheck(L_0);
		bool L_1 = String_StartsWith_m1841920685(L_0, _stringLiteral4012603225, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_00a4;
		}
	}
	{
		String_t* L_2 = ___condition0;
		NullCheck(L_2);
		bool L_3 = String_StartsWith_m1841920685(L_2, _stringLiteral168715698, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_00a4;
		}
	}
	{
		String_t* L_4 = ___condition0;
		V_0 = L_4;
		String_t* L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = String_StartsWith_m1841920685(L_5, _stringLiteral1761773864, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0045;
		}
	}
	{
		String_t* L_7 = V_0;
		NullCheck(_stringLiteral1761773864);
		int32_t L_8 = String_get_Length_m1606060069(_stringLiteral1761773864, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_9 = String_Substring_m2032624251(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_9;
	}

IL_0045:
	{
		TestComponent_t2516511601 * L_10 = __this->get_currentTest_4();
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		bool L_11 = TestComponent_op_Inequality_m3300184364(NULL /*static, unused*/, L_10, (TestComponent_t2516511601 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_12 = V_0;
		TestComponent_t2516511601 * L_13 = __this->get_currentTest_4();
		NullCheck(L_13);
		String_t* L_14 = Object_get_name_m2079638459(L_13, /*hidden argument*/NULL);
		Il2CppChar L_15 = ((Il2CppChar)((int32_t)41));
		Il2CppObject * L_16 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral372029318, L_14, L_16, /*hidden argument*/NULL);
		NullCheck(L_12);
		bool L_18 = String_EndsWith_m568509976(L_12, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_19 = V_0;
		String_t* L_20 = V_0;
		NullCheck(L_20);
		int32_t L_21 = String_LastIndexOf_m3555875680(L_20, ((int32_t)40), /*hidden argument*/NULL);
		NullCheck(L_19);
		String_t* L_22 = String_Substring_m12482732(L_19, 0, L_21, /*hidden argument*/NULL);
		V_0 = L_22;
	}

IL_008d:
	{
		String_t* L_23 = __this->get_m_TestMessages_9();
		String_t* L_24 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m612901809(NULL /*static, unused*/, L_23, L_24, _stringLiteral372029352, /*hidden argument*/NULL);
		__this->set_m_TestMessages_9(L_25);
	}

IL_00a4:
	{
		int32_t L_26 = ___type2;
		if (L_26 == 0)
		{
			goto IL_013b;
		}
		if (L_26 == 1)
		{
			goto IL_013b;
		}
		if (L_26 == 2)
		{
			goto IL_018c;
		}
		if (L_26 == 3)
		{
			goto IL_014e;
		}
		if (L_26 == 4)
		{
			goto IL_00c3;
		}
	}
	{
		goto IL_018c;
	}

IL_00c3:
	{
		String_t* L_27 = ___condition0;
		String_t* L_28 = ___condition0;
		NullCheck(L_28);
		int32_t L_29 = String_IndexOf_m2358239236(L_28, ((int32_t)58), /*hidden argument*/NULL);
		NullCheck(L_27);
		String_t* L_30 = String_Substring_m12482732(L_27, 0, L_29, /*hidden argument*/NULL);
		V_1 = L_30;
		TestComponent_t2516511601 * L_31 = __this->get_currentTest_4();
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		bool L_32 = TestComponent_op_Inequality_m3300184364(NULL /*static, unused*/, L_31, (TestComponent_t2516511601 *)NULL, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0128;
		}
	}
	{
		TestComponent_t2516511601 * L_33 = __this->get_currentTest_4();
		String_t* L_34 = V_1;
		NullCheck(L_33);
		bool L_35 = TestComponent_IsExceptionExpected_m1446780962(L_33, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_0128;
		}
	}
	{
		String_t* L_36 = __this->get_m_TestMessages_9();
		String_t* L_37 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_38 = String_Concat_m612901809(NULL /*static, unused*/, L_36, L_37, _stringLiteral4294932033, /*hidden argument*/NULL);
		__this->set_m_TestMessages_9(L_38);
		TestComponent_t2516511601 * L_39 = __this->get_currentTest_4();
		NullCheck(L_39);
		bool L_40 = TestComponent_ShouldSucceedOnException_m4007130788(L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_0123;
		}
	}
	{
		__this->set_m_TestState_11(1);
	}

IL_0123:
	{
		goto IL_0136;
	}

IL_0128:
	{
		__this->set_m_TestState_11(3);
		String_t* L_41 = ___stacktrace1;
		__this->set_m_Stacktrace_10(L_41);
	}

IL_0136:
	{
		goto IL_018c;
	}

IL_013b:
	{
		__this->set_m_TestState_11(2);
		String_t* L_42 = ___stacktrace1;
		__this->set_m_Stacktrace_10(L_42);
		goto IL_018c;
	}

IL_014e:
	{
		int32_t L_43 = __this->get_m_TestState_11();
		if (L_43)
		{
			goto IL_0170;
		}
	}
	{
		String_t* L_44 = ___condition0;
		NullCheck(L_44);
		bool L_45 = String_StartsWith_m1841920685(L_44, _stringLiteral1139922469, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_0170;
		}
	}
	{
		__this->set_m_TestState_11(1);
	}

IL_0170:
	{
		String_t* L_46 = ___condition0;
		NullCheck(L_46);
		bool L_47 = String_StartsWith_m1841920685(L_46, _stringLiteral1899437160, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_0187;
		}
	}
	{
		__this->set_m_TestState_11(2);
	}

IL_0187:
	{
		goto IL_018c;
	}

IL_018c:
	{
		return;
	}
}
// System.Collections.IEnumerator UnityTest.TestRunner::StateMachine()
extern Il2CppClass* U3CStateMachineU3Ec__Iterator0_t817978249_il2cpp_TypeInfo_var;
extern const uint32_t TestRunner_StateMachine_m3541642506_MetadataUsageId;
extern "C"  Il2CppObject * TestRunner_StateMachine_m3541642506 (TestRunner_t1304041832 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_StateMachine_m3541642506_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CStateMachineU3Ec__Iterator0_t817978249 * V_0 = NULL;
	{
		U3CStateMachineU3Ec__Iterator0_t817978249 * L_0 = (U3CStateMachineU3Ec__Iterator0_t817978249 *)il2cpp_codegen_object_new(U3CStateMachineU3Ec__Iterator0_t817978249_il2cpp_TypeInfo_var);
		U3CStateMachineU3Ec__Iterator0__ctor_m574500974(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStateMachineU3Ec__Iterator0_t817978249 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CStateMachineU3Ec__Iterator0_t817978249 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityTest.TestRunner::LogMessage(System.String)
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral455075110;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TestRunner_LogMessage_m1520221235_MetadataUsageId;
extern "C"  void TestRunner_LogMessage_m1520221235 (TestRunner_t1304041832 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_LogMessage_m1520221235_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TestComponent_t2516511601 * L_0 = __this->get_currentTest_4();
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		bool L_1 = TestComponent_op_Inequality_m3300184364(NULL /*static, unused*/, L_0, (TestComponent_t2516511601 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0041;
		}
	}
	{
		String_t* L_2 = ___message0;
		TestComponent_t2516511601 * L_3 = __this->get_currentTest_4();
		NullCheck(L_3);
		String_t* L_4 = TestComponent_get_Name_m1475120506(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m1561703559(NULL /*static, unused*/, L_2, _stringLiteral455075110, L_4, _stringLiteral372029317, /*hidden argument*/NULL);
		TestComponent_t2516511601 * L_6 = __this->get_currentTest_4();
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m4271593384(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/NULL);
		goto IL_0047;
	}

IL_0041:
	{
		String_t* L_8 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0047:
	{
		return;
	}
}
// System.Void UnityTest.TestRunner::FinishTestRun()
extern "C"  void TestRunner_FinishTestRun_m2535838598 (TestRunner_t1304041832 * __this, const MethodInfo* method)
{
	{
		TestRunner_PrintResultToLog_m3202900473(__this, /*hidden argument*/NULL);
		TestRunnerCallbackList_t3689696405 * L_0 = __this->get_TestRunnerCallback_13();
		List_1_t4154586889 * L_1 = __this->get_m_ResultList_5();
		NullCheck(L_0);
		TestRunnerCallbackList_RunFinished_m4045216808(L_0, L_1, /*hidden argument*/NULL);
		TestRunner_LoadNextLevelOrQuit_m1208356473(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityTest.TestRunner::PrintResultToLog()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TestRunner_t1304041832_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2709810782_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t913456297_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* TestRunner_U3CPrintResultToLogU3Em__4_m3842018790_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m778238545_MethodInfo_var;
extern const MethodInfo* Enumerable_Count_TisTestResult_t490498461_m938069614_MethodInfo_var;
extern const MethodInfo* TestRunner_U3CPrintResultToLogU3Em__5_m4006570311_MethodInfo_var;
extern const MethodInfo* Enumerable_Any_TisTestResult_t490498461_m3528417887_MethodInfo_var;
extern const MethodInfo* TestRunner_U3CPrintResultToLogU3Em__6_m3458344612_MethodInfo_var;
extern const MethodInfo* TestRunner_U3CPrintResultToLogU3Em__7_m3926491909_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisTestResult_t490498461_m820548660_MethodInfo_var;
extern const MethodInfo* TestRunner_U3CPrintResultToLogU3Em__8_m4236607815_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m4015794938_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisTestResult_t490498461_TisString_t_m456196652_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisString_t_m489990756_MethodInfo_var;
extern const MethodInfo* TestRunner_U3CPrintResultToLogU3Em__9_m3619323859_MethodInfo_var;
extern const MethodInfo* TestRunner_U3CPrintResultToLogU3Em__A_m2707175355_MethodInfo_var;
extern const MethodInfo* TestRunner_U3CPrintResultToLogU3Em__B_m1211561240_MethodInfo_var;
extern const MethodInfo* TestRunner_U3CPrintResultToLogU3Em__C_m4240348228_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3778161144;
extern Il2CppCodeGenString* _stringLiteral3404886133;
extern Il2CppCodeGenString* _stringLiteral3808865116;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral1785173270;
extern Il2CppCodeGenString* _stringLiteral3917208303;
extern const uint32_t TestRunner_PrintResultToLog_m3202900473_MetadataUsageId;
extern "C"  void TestRunner_PrintResultToLog_m3202900473 (TestRunner_t1304041832 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_PrintResultToLog_m3202900473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	List_1_t4154586889 * G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	String_t* G_B2_2 = NULL;
	List_1_t4154586889 * G_B1_0 = NULL;
	String_t* G_B1_1 = NULL;
	String_t* G_B1_2 = NULL;
	List_1_t4154586889 * G_B4_0 = NULL;
	List_1_t4154586889 * G_B3_0 = NULL;
	List_1_t4154586889 * G_B7_0 = NULL;
	String_t* G_B7_1 = NULL;
	String_t* G_B7_2 = NULL;
	List_1_t4154586889 * G_B6_0 = NULL;
	String_t* G_B6_1 = NULL;
	String_t* G_B6_2 = NULL;
	List_1_t4154586889 * G_B9_0 = NULL;
	String_t* G_B9_1 = NULL;
	String_t* G_B9_2 = NULL;
	List_1_t4154586889 * G_B8_0 = NULL;
	String_t* G_B8_1 = NULL;
	String_t* G_B8_2 = NULL;
	Il2CppObject* G_B11_0 = NULL;
	String_t* G_B11_1 = NULL;
	String_t* G_B11_2 = NULL;
	Il2CppObject* G_B10_0 = NULL;
	String_t* G_B10_1 = NULL;
	String_t* G_B10_2 = NULL;
	List_1_t4154586889 * G_B14_0 = NULL;
	List_1_t4154586889 * G_B13_0 = NULL;
	List_1_t4154586889 * G_B17_0 = NULL;
	String_t* G_B17_1 = NULL;
	String_t* G_B17_2 = NULL;
	List_1_t4154586889 * G_B16_0 = NULL;
	String_t* G_B16_1 = NULL;
	String_t* G_B16_2 = NULL;
	List_1_t4154586889 * G_B19_0 = NULL;
	String_t* G_B19_1 = NULL;
	String_t* G_B19_2 = NULL;
	List_1_t4154586889 * G_B18_0 = NULL;
	String_t* G_B18_1 = NULL;
	String_t* G_B18_2 = NULL;
	Il2CppObject* G_B21_0 = NULL;
	String_t* G_B21_1 = NULL;
	String_t* G_B21_2 = NULL;
	Il2CppObject* G_B20_0 = NULL;
	String_t* G_B20_1 = NULL;
	String_t* G_B20_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		String_t* L_1 = V_0;
		List_1_t4154586889 * L_2 = __this->get_m_ResultList_5();
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		Func_2_t2709810782 * L_3 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_26();
		G_B1_0 = L_2;
		G_B1_1 = _stringLiteral3778161144;
		G_B1_2 = L_1;
		if (L_3)
		{
			G_B2_0 = L_2;
			G_B2_1 = _stringLiteral3778161144;
			G_B2_2 = L_1;
			goto IL_002a;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)TestRunner_U3CPrintResultToLogU3Em__4_m3842018790_MethodInfo_var);
		Func_2_t2709810782 * L_5 = (Func_2_t2709810782 *)il2cpp_codegen_object_new(Func_2_t2709810782_il2cpp_TypeInfo_var);
		Func_2__ctor_m778238545(L_5, NULL, L_4, /*hidden argument*/Func_2__ctor_m778238545_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache3_26(L_5);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		Func_2_t2709810782 * L_6 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_26();
		int32_t L_7 = Enumerable_Count_TisTestResult_t490498461_m938069614(NULL /*static, unused*/, G_B2_0, L_6, /*hidden argument*/Enumerable_Count_TisTestResult_t490498461_m938069614_MethodInfo_var);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m2000667605(NULL /*static, unused*/, G_B2_2, G_B2_1, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		List_1_t4154586889 * L_11 = __this->get_m_ResultList_5();
		Func_2_t2709810782 * L_12 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_27();
		G_B3_0 = L_11;
		if (L_12)
		{
			G_B4_0 = L_11;
			goto IL_005d;
		}
	}
	{
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)TestRunner_U3CPrintResultToLogU3Em__5_m4006570311_MethodInfo_var);
		Func_2_t2709810782 * L_14 = (Func_2_t2709810782 *)il2cpp_codegen_object_new(Func_2_t2709810782_il2cpp_TypeInfo_var);
		Func_2__ctor_m778238545(L_14, NULL, L_13, /*hidden argument*/Func_2__ctor_m778238545_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache4_27(L_14);
		G_B4_0 = G_B3_0;
	}

IL_005d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		Func_2_t2709810782 * L_15 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_27();
		bool L_16 = Enumerable_Any_TisTestResult_t490498461_m3528417887(NULL /*static, unused*/, G_B4_0, L_15, /*hidden argument*/Enumerable_Any_TisTestResult_t490498461_m3528417887_MethodInfo_var);
		if (!L_16)
		{
			goto IL_010d;
		}
	}
	{
		String_t* L_17 = V_0;
		List_1_t4154586889 * L_18 = __this->get_m_ResultList_5();
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		Func_2_t2709810782 * L_19 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_28();
		G_B6_0 = L_18;
		G_B6_1 = _stringLiteral3404886133;
		G_B6_2 = L_17;
		if (L_19)
		{
			G_B7_0 = L_18;
			G_B7_1 = _stringLiteral3404886133;
			G_B7_2 = L_17;
			goto IL_0090;
		}
	}
	{
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)TestRunner_U3CPrintResultToLogU3Em__6_m3458344612_MethodInfo_var);
		Func_2_t2709810782 * L_21 = (Func_2_t2709810782 *)il2cpp_codegen_object_new(Func_2_t2709810782_il2cpp_TypeInfo_var);
		Func_2__ctor_m778238545(L_21, NULL, L_20, /*hidden argument*/Func_2__ctor_m778238545_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache5_28(L_21);
		G_B7_0 = G_B6_0;
		G_B7_1 = G_B6_1;
		G_B7_2 = G_B6_2;
	}

IL_0090:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		Func_2_t2709810782 * L_22 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_28();
		int32_t L_23 = Enumerable_Count_TisTestResult_t490498461_m938069614(NULL /*static, unused*/, G_B7_0, L_22, /*hidden argument*/Enumerable_Count_TisTestResult_t490498461_m938069614_MethodInfo_var);
		int32_t L_24 = L_23;
		Il2CppObject * L_25 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_24);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m2000667605(NULL /*static, unused*/, G_B7_2, G_B7_1, L_25, /*hidden argument*/NULL);
		V_0 = L_26;
		List_1_t4154586889 * L_27 = __this->get_m_ResultList_5();
		Func_2_t2709810782 * L_28 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_29();
		G_B8_0 = L_27;
		G_B8_1 = _stringLiteral811305474;
		G_B8_2 = _stringLiteral3808865116;
		if (L_28)
		{
			G_B9_0 = L_27;
			G_B9_1 = _stringLiteral811305474;
			G_B9_2 = _stringLiteral3808865116;
			goto IL_00cd;
		}
	}
	{
		IntPtr_t L_29;
		L_29.set_m_value_0((void*)(void*)TestRunner_U3CPrintResultToLogU3Em__7_m3926491909_MethodInfo_var);
		Func_2_t2709810782 * L_30 = (Func_2_t2709810782 *)il2cpp_codegen_object_new(Func_2_t2709810782_il2cpp_TypeInfo_var);
		Func_2__ctor_m778238545(L_30, NULL, L_29, /*hidden argument*/Func_2__ctor_m778238545_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache6_29(L_30);
		G_B9_0 = G_B8_0;
		G_B9_1 = G_B8_1;
		G_B9_2 = G_B8_2;
	}

IL_00cd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		Func_2_t2709810782 * L_31 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_29();
		Il2CppObject* L_32 = Enumerable_Where_TisTestResult_t490498461_m820548660(NULL /*static, unused*/, G_B9_0, L_31, /*hidden argument*/Enumerable_Where_TisTestResult_t490498461_m820548660_MethodInfo_var);
		Func_2_t913456297 * L_33 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache7_30();
		G_B10_0 = L_32;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
		if (L_33)
		{
			G_B11_0 = L_32;
			G_B11_1 = G_B9_1;
			G_B11_2 = G_B9_2;
			goto IL_00ef;
		}
	}
	{
		IntPtr_t L_34;
		L_34.set_m_value_0((void*)(void*)TestRunner_U3CPrintResultToLogU3Em__8_m4236607815_MethodInfo_var);
		Func_2_t913456297 * L_35 = (Func_2_t913456297 *)il2cpp_codegen_object_new(Func_2_t913456297_il2cpp_TypeInfo_var);
		Func_2__ctor_m4015794938(L_35, NULL, L_34, /*hidden argument*/Func_2__ctor_m4015794938_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache7_30(L_35);
		G_B11_0 = G_B10_0;
		G_B11_1 = G_B10_1;
		G_B11_2 = G_B10_2;
	}

IL_00ef:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		Func_2_t913456297 * L_36 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache7_30();
		Il2CppObject* L_37 = Enumerable_Select_TisTestResult_t490498461_TisString_t_m456196652(NULL /*static, unused*/, G_B11_0, L_36, /*hidden argument*/Enumerable_Select_TisTestResult_t490498461_TisString_t_m456196652_MethodInfo_var);
		StringU5BU5D_t1642385972* L_38 = Enumerable_ToArray_TisString_t_m489990756(NULL /*static, unused*/, L_37, /*hidden argument*/Enumerable_ToArray_TisString_t_m489990756_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Join_m1966872927(NULL /*static, unused*/, G_B11_1, L_38, /*hidden argument*/NULL);
		String_t* L_40 = String_Concat_m2596409543(NULL /*static, unused*/, G_B11_2, L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
	}

IL_010d:
	{
		List_1_t4154586889 * L_41 = __this->get_m_ResultList_5();
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		Func_2_t2709810782 * L_42 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache8_31();
		G_B13_0 = L_41;
		if (L_42)
		{
			G_B14_0 = L_41;
			goto IL_012b;
		}
	}
	{
		IntPtr_t L_43;
		L_43.set_m_value_0((void*)(void*)TestRunner_U3CPrintResultToLogU3Em__9_m3619323859_MethodInfo_var);
		Func_2_t2709810782 * L_44 = (Func_2_t2709810782 *)il2cpp_codegen_object_new(Func_2_t2709810782_il2cpp_TypeInfo_var);
		Func_2__ctor_m778238545(L_44, NULL, L_43, /*hidden argument*/Func_2__ctor_m778238545_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache8_31(L_44);
		G_B14_0 = G_B13_0;
	}

IL_012b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		Func_2_t2709810782 * L_45 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache8_31();
		bool L_46 = Enumerable_Any_TisTestResult_t490498461_m3528417887(NULL /*static, unused*/, G_B14_0, L_45, /*hidden argument*/Enumerable_Any_TisTestResult_t490498461_m3528417887_MethodInfo_var);
		if (!L_46)
		{
			goto IL_01db;
		}
	}
	{
		String_t* L_47 = V_0;
		List_1_t4154586889 * L_48 = __this->get_m_ResultList_5();
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		Func_2_t2709810782 * L_49 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache9_32();
		G_B16_0 = L_48;
		G_B16_1 = _stringLiteral1785173270;
		G_B16_2 = L_47;
		if (L_49)
		{
			G_B17_0 = L_48;
			G_B17_1 = _stringLiteral1785173270;
			G_B17_2 = L_47;
			goto IL_015e;
		}
	}
	{
		IntPtr_t L_50;
		L_50.set_m_value_0((void*)(void*)TestRunner_U3CPrintResultToLogU3Em__A_m2707175355_MethodInfo_var);
		Func_2_t2709810782 * L_51 = (Func_2_t2709810782 *)il2cpp_codegen_object_new(Func_2_t2709810782_il2cpp_TypeInfo_var);
		Func_2__ctor_m778238545(L_51, NULL, L_50, /*hidden argument*/Func_2__ctor_m778238545_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache9_32(L_51);
		G_B17_0 = G_B16_0;
		G_B17_1 = G_B16_1;
		G_B17_2 = G_B16_2;
	}

IL_015e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		Func_2_t2709810782 * L_52 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache9_32();
		int32_t L_53 = Enumerable_Count_TisTestResult_t490498461_m938069614(NULL /*static, unused*/, G_B17_0, L_52, /*hidden argument*/Enumerable_Count_TisTestResult_t490498461_m938069614_MethodInfo_var);
		int32_t L_54 = L_53;
		Il2CppObject * L_55 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_54);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_56 = String_Concat_m2000667605(NULL /*static, unused*/, G_B17_2, G_B17_1, L_55, /*hidden argument*/NULL);
		V_0 = L_56;
		List_1_t4154586889 * L_57 = __this->get_m_ResultList_5();
		Func_2_t2709810782 * L_58 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheA_33();
		G_B18_0 = L_57;
		G_B18_1 = _stringLiteral811305474;
		G_B18_2 = _stringLiteral3917208303;
		if (L_58)
		{
			G_B19_0 = L_57;
			G_B19_1 = _stringLiteral811305474;
			G_B19_2 = _stringLiteral3917208303;
			goto IL_019b;
		}
	}
	{
		IntPtr_t L_59;
		L_59.set_m_value_0((void*)(void*)TestRunner_U3CPrintResultToLogU3Em__B_m1211561240_MethodInfo_var);
		Func_2_t2709810782 * L_60 = (Func_2_t2709810782 *)il2cpp_codegen_object_new(Func_2_t2709810782_il2cpp_TypeInfo_var);
		Func_2__ctor_m778238545(L_60, NULL, L_59, /*hidden argument*/Func_2__ctor_m778238545_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheA_33(L_60);
		G_B19_0 = G_B18_0;
		G_B19_1 = G_B18_1;
		G_B19_2 = G_B18_2;
	}

IL_019b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		Func_2_t2709810782 * L_61 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheA_33();
		Il2CppObject* L_62 = Enumerable_Where_TisTestResult_t490498461_m820548660(NULL /*static, unused*/, G_B19_0, L_61, /*hidden argument*/Enumerable_Where_TisTestResult_t490498461_m820548660_MethodInfo_var);
		Func_2_t913456297 * L_63 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheB_34();
		G_B20_0 = L_62;
		G_B20_1 = G_B19_1;
		G_B20_2 = G_B19_2;
		if (L_63)
		{
			G_B21_0 = L_62;
			G_B21_1 = G_B19_1;
			G_B21_2 = G_B19_2;
			goto IL_01bd;
		}
	}
	{
		IntPtr_t L_64;
		L_64.set_m_value_0((void*)(void*)TestRunner_U3CPrintResultToLogU3Em__C_m4240348228_MethodInfo_var);
		Func_2_t913456297 * L_65 = (Func_2_t913456297 *)il2cpp_codegen_object_new(Func_2_t913456297_il2cpp_TypeInfo_var);
		Func_2__ctor_m4015794938(L_65, NULL, L_64, /*hidden argument*/Func_2__ctor_m4015794938_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheB_34(L_65);
		G_B21_0 = G_B20_0;
		G_B21_1 = G_B20_1;
		G_B21_2 = G_B20_2;
	}

IL_01bd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		Func_2_t913456297 * L_66 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheB_34();
		Il2CppObject* L_67 = Enumerable_Select_TisTestResult_t490498461_TisString_t_m456196652(NULL /*static, unused*/, G_B21_0, L_66, /*hidden argument*/Enumerable_Select_TisTestResult_t490498461_TisString_t_m456196652_MethodInfo_var);
		StringU5BU5D_t1642385972* L_68 = Enumerable_ToArray_TisString_t_m489990756(NULL /*static, unused*/, L_67, /*hidden argument*/Enumerable_ToArray_TisString_t_m489990756_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_69 = String_Join_m1966872927(NULL /*static, unused*/, G_B21_1, L_68, /*hidden argument*/NULL);
		String_t* L_70 = String_Concat_m2596409543(NULL /*static, unused*/, G_B21_2, L_69, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_70, /*hidden argument*/NULL);
	}

IL_01db:
	{
		String_t* L_71 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_71, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityTest.TestRunner::LoadNextLevelOrQuit()
extern Il2CppClass* TestRunner_t1304041832_il2cpp_TypeInfo_var;
extern Il2CppClass* Path_t41728875_il2cpp_TypeInfo_var;
extern const uint32_t TestRunner_LoadNextLevelOrQuit_m1208356473_MetadataUsageId;
extern "C"  void TestRunner_LoadNextLevelOrQuit_m1208356473 (TestRunner_t1304041832 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_LoadNextLevelOrQuit_m1208356473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		bool L_0 = TestRunner_get_isInitializedByRunner_m3796614350(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		int32_t L_1 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_TestSceneNumber_2();
		((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->set_TestSceneNumber_2(((int32_t)((int32_t)L_1+(int32_t)1)));
		TestRunnerConfigurator_t1966496711 * L_2 = __this->get_m_Configurator_12();
		int32_t L_3 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_TestSceneNumber_2();
		NullCheck(L_2);
		String_t* L_4 = TestRunnerConfigurator_GetIntegrationTestScenes_m1098943538(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		if (!L_5)
		{
			goto IL_003f;
		}
	}
	{
		String_t* L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t41728875_il2cpp_TypeInfo_var);
		String_t* L_7 = Path_GetFileNameWithoutExtension_m2541641063(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		goto IL_0079;
	}

IL_003f:
	{
		TestRunnerCallbackList_t3689696405 * L_8 = __this->get_TestRunnerCallback_13();
		NullCheck(L_8);
		TestRunnerCallbackList_AllScenesFinished_m455268753(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		TestResultRenderer_t1986929490 * L_9 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_k_ResultRenderer_3();
		NullCheck(L_9);
		TestResultRenderer_ShowResults_m1265660124(L_9, /*hidden argument*/NULL);
		TestRunnerConfigurator_t1966496711 * L_10 = __this->get_m_Configurator_12();
		NullCheck(L_10);
		bool L_11 = TestRunnerConfigurator_get_isBatchRun_m4088445273(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0079;
		}
	}
	{
		TestRunnerConfigurator_t1966496711 * L_12 = __this->get_m_Configurator_12();
		NullCheck(L_12);
		bool L_13 = TestRunnerConfigurator_get_sendResultsOverNetwork_m2213479566(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0079;
		}
	}
	{
		Application_Quit_m3885595876(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0079:
	{
		return;
	}
}
// System.Void UnityTest.TestRunner::OnGUI()
extern Il2CppClass* TestRunner_t1304041832_il2cpp_TypeInfo_var;
extern const uint32_t TestRunner_OnGUI_m4164786174_MetadataUsageId;
extern "C"  void TestRunner_OnGUI_m4164786174 (TestRunner_t1304041832 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_OnGUI_m4164786174_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		TestResultRenderer_t1986929490 * L_0 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_k_ResultRenderer_3();
		NullCheck(L_0);
		TestResultRenderer_Draw_m2322615333(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityTest.TestRunner::StartNewTest()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t2709810782_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* TestRunner_U3CStartNewTestU3Em__D_m915839043_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m778238545_MethodInfo_var;
extern const MethodInfo* Enumerable_Single_TisTestResult_t490498461_m3341528866_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m1033227945_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3999888246;
extern Il2CppCodeGenString* _stringLiteral4012603225;
extern const uint32_t TestRunner_StartNewTest_m2384781446_MetadataUsageId;
extern "C"  void TestRunner_StartNewTest_m2384781446 (TestRunner_t1304041832 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_StartNewTest_m2384781446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TestResult_t490498461 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_m_TestMessages_9(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_m_Stacktrace_10(L_1);
		__this->set_m_TestState_11(0);
		float L_2 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_StartTime_7((((double)((double)L_2))));
		IntegrationTestsProvider_t1666533096 * L_3 = __this->get_m_TestsProvider_14();
		NullCheck(L_3);
		Il2CppObject * L_4 = IntegrationTestsProvider_GetNextTest_m4242984223(L_3, /*hidden argument*/NULL);
		__this->set_currentTest_4(((TestComponent_t2516511601 *)IsInstClass(L_4, TestComponent_t2516511601_il2cpp_TypeInfo_var)));
		List_1_t4154586889 * L_5 = __this->get_m_ResultList_5();
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)TestRunner_U3CStartNewTestU3Em__D_m915839043_MethodInfo_var);
		Func_2_t2709810782 * L_7 = (Func_2_t2709810782 *)il2cpp_codegen_object_new(Func_2_t2709810782_il2cpp_TypeInfo_var);
		Func_2__ctor_m778238545(L_7, __this, L_6, /*hidden argument*/Func_2__ctor_m778238545_MethodInfo_var);
		TestResult_t490498461 * L_8 = Enumerable_Single_TisTestResult_t490498461_m3341528866(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/Enumerable_Single_TisTestResult_t490498461_m3341528866_MethodInfo_var);
		V_0 = L_8;
		TestComponent_t2516511601 * L_9 = __this->get_currentTest_4();
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		bool L_10 = TestComponent_op_Inequality_m3300184364(NULL /*static, unused*/, L_9, (TestComponent_t2516511601 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_009e;
		}
	}
	{
		TestComponent_t2516511601 * L_11 = __this->get_currentTest_4();
		NullCheck(L_11);
		bool L_12 = TestComponent_IsExludedOnThisPlatform_m2715882924(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_009e;
		}
	}
	{
		__this->set_m_TestState_11(5);
		TestComponent_t2516511601 * L_13 = __this->get_currentTest_4();
		NullCheck(L_13);
		GameObject_t1756533147 * L_14 = Component_get_gameObject_m3105766835(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_15 = Object_get_name_m2079638459(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m2596409543(NULL /*static, unused*/, L_15, _stringLiteral3999888246, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
	}

IL_009e:
	{
		TestComponent_t2516511601 * L_17 = __this->get_currentTest_4();
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		bool L_18 = TestComponent_op_Inequality_m3300184364(NULL /*static, unused*/, L_17, (TestComponent_t2516511601 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00e2;
		}
	}
	{
		TestComponent_t2516511601 * L_19 = __this->get_currentTest_4();
		NullCheck(L_19);
		bool L_20 = TestComponent_IsIgnored_m2040452679(L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00e2;
		}
	}
	{
		bool L_21 = TestRunner_get_isInitializedByRunner_m3796614350(__this, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00db;
		}
	}
	{
		List_1_t4154586889 * L_22 = __this->get_m_ResultList_5();
		NullCheck(L_22);
		int32_t L_23 = List_1_get_Count_m1033227945(L_22, /*hidden argument*/List_1_get_Count_m1033227945_MethodInfo_var);
		if ((((int32_t)L_23) == ((int32_t)1)))
		{
			goto IL_00e2;
		}
	}

IL_00db:
	{
		__this->set_m_TestState_11(5);
	}

IL_00e2:
	{
		TestRunner_LogMessage_m1520221235(__this, _stringLiteral4012603225, /*hidden argument*/NULL);
		TestRunnerCallbackList_t3689696405 * L_24 = __this->get_TestRunnerCallback_13();
		TestResult_t490498461 * L_25 = V_0;
		NullCheck(L_24);
		TestRunnerCallbackList_TestStarted_m709188404(L_24, L_25, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityTest.TestRunner::FinishTest(UnityTest.TestResult/ResultType)
extern Il2CppClass* Func_2_t2709810782_il2cpp_TypeInfo_var;
extern Il2CppClass* TestRunner_t1304041832_il2cpp_TypeInfo_var;
extern const MethodInfo* TestRunner_U3CFinishTestU3Em__E_m1879346421_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m778238545_MethodInfo_var;
extern const MethodInfo* Enumerable_Single_TisTestResult_t490498461_m3341528866_MethodInfo_var;
extern const uint32_t TestRunner_FinishTest_m1325069965_MetadataUsageId;
extern "C"  void TestRunner_FinishTest_m1325069965 (TestRunner_t1304041832 * __this, int32_t ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_FinishTest_m1325069965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TestResult_t490498461 * V_0 = NULL;
	Scene_t1684909666  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IntegrationTestsProvider_t1666533096 * L_0 = __this->get_m_TestsProvider_14();
		TestComponent_t2516511601 * L_1 = __this->get_currentTest_4();
		NullCheck(L_0);
		IntegrationTestsProvider_FinishTest_m2677561504(L_0, L_1, /*hidden argument*/NULL);
		List_1_t4154586889 * L_2 = __this->get_m_ResultList_5();
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)TestRunner_U3CFinishTestU3Em__E_m1879346421_MethodInfo_var);
		Func_2_t2709810782 * L_4 = (Func_2_t2709810782 *)il2cpp_codegen_object_new(Func_2_t2709810782_il2cpp_TypeInfo_var);
		Func_2__ctor_m778238545(L_4, __this, L_3, /*hidden argument*/Func_2__ctor_m778238545_MethodInfo_var);
		TestResult_t490498461 * L_5 = Enumerable_Single_TisTestResult_t490498461_m3341528866(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/Enumerable_Single_TisTestResult_t490498461_m3341528866_MethodInfo_var);
		V_0 = L_5;
		TestResult_t490498461 * L_6 = V_0;
		int32_t L_7 = ___result0;
		NullCheck(L_6);
		L_6->set_resultType_2(L_7);
		TestResult_t490498461 * L_8 = V_0;
		float L_9 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		double L_10 = __this->get_m_StartTime_7();
		NullCheck(L_8);
		L_8->set_duration_3(((double)((double)(((double)((double)L_9)))-(double)L_10)));
		TestResult_t490498461 * L_11 = V_0;
		String_t* L_12 = __this->get_m_TestMessages_9();
		NullCheck(L_11);
		L_11->set_messages_4(L_12);
		TestResult_t490498461 * L_13 = V_0;
		String_t* L_14 = __this->get_m_Stacktrace_10();
		NullCheck(L_13);
		L_13->set_stacktrace_5(L_14);
		TestRunnerCallbackList_t3689696405 * L_15 = __this->get_TestRunnerCallback_13();
		TestResult_t490498461 * L_16 = V_0;
		NullCheck(L_15);
		TestRunnerCallbackList_TestFinished_m3043223747(L_15, L_16, /*hidden argument*/NULL);
		__this->set_currentTest_4((TestComponent_t2516511601 *)NULL);
		TestResult_t490498461 * L_17 = V_0;
		NullCheck(L_17);
		bool L_18 = TestResult_get_IsSuccess_m4143018983(L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_00a7;
		}
	}
	{
		TestResult_t490498461 * L_19 = V_0;
		NullCheck(L_19);
		bool L_20 = TestResult_get_Executed_m82902453(L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00a7;
		}
	}
	{
		TestResult_t490498461 * L_21 = V_0;
		NullCheck(L_21);
		bool L_22 = TestResult_get_IsIgnored_m638715012(L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_00a7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		TestResultRenderer_t1986929490 * L_23 = ((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->get_k_ResultRenderer_3();
		Scene_t1684909666  L_24 = SceneManager_GetActiveScene_m2964039490(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_24;
		String_t* L_25 = Scene_get_name_m745914591((&V_1), /*hidden argument*/NULL);
		TestResult_t490498461 * L_26 = V_0;
		NullCheck(L_23);
		TestResultRenderer_AddResults_m220909478(L_23, L_25, L_26, /*hidden argument*/NULL);
	}

IL_00a7:
	{
		return;
	}
}
// UnityTest.TestRunner UnityTest.TestRunner::GetTestRunner()
extern const Il2CppType* TestRunner_t1304041832_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TestRunner_t1304041832_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Count_TisObject_t1021602117_m974259178_MethodInfo_var;
extern const MethodInfo* Enumerable_Any_TisObject_t1021602117_m11244335_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTestRunner_t1304041832_m607288562_MethodInfo_var;
extern const MethodInfo* Enumerable_Single_TisObject_t1021602117_m4054968950_MethodInfo_var;
extern const uint32_t TestRunner_GetTestRunner_m2902589186_MetadataUsageId;
extern "C"  TestRunner_t1304041832 * TestRunner_GetTestRunner_m2902589186 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_GetTestRunner_m2902589186_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TestRunner_t1304041832 * V_0 = NULL;
	ObjectU5BU5D_t4217747464* V_1 = NULL;
	Object_t1021602117 * V_2 = NULL;
	ObjectU5BU5D_t4217747464* V_3 = NULL;
	int32_t V_4 = 0;
	{
		V_0 = (TestRunner_t1304041832 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(TestRunner_t1304041832_0_0_0_var), /*hidden argument*/NULL);
		ObjectU5BU5D_t4217747464* L_1 = Resources_FindObjectsOfTypeAll_m3365744935(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		ObjectU5BU5D_t4217747464* L_2 = V_1;
		int32_t L_3 = Enumerable_Count_TisObject_t1021602117_m974259178(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_2, /*hidden argument*/Enumerable_Count_TisObject_t1021602117_m974259178_MethodInfo_var);
		if ((((int32_t)L_3) <= ((int32_t)1)))
		{
			goto IL_0052;
		}
	}
	{
		ObjectU5BU5D_t4217747464* L_4 = V_1;
		V_3 = L_4;
		V_4 = 0;
		goto IL_0043;
	}

IL_0028:
	{
		ObjectU5BU5D_t4217747464* L_5 = V_3;
		int32_t L_6 = V_4;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		Object_t1021602117 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_2 = L_8;
		Object_t1021602117 * L_9 = V_2;
		NullCheck(((TestRunner_t1304041832 *)CastclassClass(L_9, TestRunner_t1304041832_il2cpp_TypeInfo_var)));
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(((TestRunner_t1304041832 *)CastclassClass(L_9, TestRunner_t1304041832_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m95027445(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_4;
		V_4 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0043:
	{
		int32_t L_12 = V_4;
		ObjectU5BU5D_t4217747464* L_13 = V_3;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_0028;
		}
	}
	{
		goto IL_0079;
	}

IL_0052:
	{
		ObjectU5BU5D_t4217747464* L_14 = V_1;
		bool L_15 = Enumerable_Any_TisObject_t1021602117_m11244335(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_14, /*hidden argument*/Enumerable_Any_TisObject_t1021602117_m11244335_MethodInfo_var);
		if (L_15)
		{
			goto IL_006d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestRunner_t1304041832_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_16 = TestRunner_Create_m2645519777(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		TestRunner_t1304041832 * L_17 = GameObject_GetComponent_TisTestRunner_t1304041832_m607288562(L_16, /*hidden argument*/GameObject_GetComponent_TisTestRunner_t1304041832_m607288562_MethodInfo_var);
		V_0 = L_17;
		goto IL_0079;
	}

IL_006d:
	{
		ObjectU5BU5D_t4217747464* L_18 = V_1;
		Object_t1021602117 * L_19 = Enumerable_Single_TisObject_t1021602117_m4054968950(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_18, /*hidden argument*/Enumerable_Single_TisObject_t1021602117_m4054968950_MethodInfo_var);
		V_0 = ((TestRunner_t1304041832 *)IsInstClass(L_19, TestRunner_t1304041832_il2cpp_TypeInfo_var));
	}

IL_0079:
	{
		TestRunner_t1304041832 * L_20 = V_0;
		return L_20;
	}
}
// UnityEngine.GameObject UnityTest.TestRunner::Create()
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisTestRunner_t1304041832_m2820994889_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3995021356;
extern Il2CppCodeGenString* _stringLiteral1823568676;
extern const uint32_t TestRunner_Create_m2645519777_MetadataUsageId;
extern "C"  GameObject_t1756533147 * TestRunner_Create_m2645519777 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_Create_m2645519777_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_0, _stringLiteral3995021356, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1756533147 * L_1 = V_0;
		NullCheck(L_1);
		GameObject_AddComponent_TisTestRunner_t1304041832_m2820994889(L_1, /*hidden argument*/GameObject_AddComponent_TisTestRunner_t1304041832_m2820994889_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1823568676, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityTest.TestRunner::IsBatchMode()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1057374825;
extern Il2CppCodeGenString* _stringLiteral816326232;
extern const uint32_t TestRunner_IsBatchMode_m926467007_MetadataUsageId;
extern "C"  bool TestRunner_IsBatchMode_m926467007 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_IsBatchMode_m926467007_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	PropertyInfo_t * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m402049910, _stringLiteral1057374825, (bool)0, "Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		V_0 = L_0;
		Type_t * L_1 = V_0;
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		Type_t * L_2 = V_0;
		NullCheck(L_2);
		PropertyInfo_t * L_3 = Type_GetProperty_m808359402(L_2, _stringLiteral816326232, /*hidden argument*/NULL);
		V_1 = L_3;
		PropertyInfo_t * L_4 = V_1;
		NullCheck(L_4);
		Il2CppObject * L_5 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t3614634134* >::Invoke(23 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_4, NULL, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL);
		return ((*(bool*)((bool*)UnBox (L_5, Boolean_t3825574718_il2cpp_TypeInfo_var))));
	}
}
// System.Void UnityTest.TestRunner::.cctor()
extern Il2CppClass* TestRunner_t1304041832_il2cpp_TypeInfo_var;
extern Il2CppClass* TestResultRenderer_t1986929490_il2cpp_TypeInfo_var;
extern const uint32_t TestRunner__cctor_m1395955665_MetadataUsageId;
extern "C"  void TestRunner__cctor_m1395955665 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner__cctor_m1395955665_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->set_TestSceneNumber_2(0);
		TestResultRenderer_t1986929490 * L_0 = (TestResultRenderer_t1986929490 *)il2cpp_codegen_object_new(TestResultRenderer_t1986929490_il2cpp_TypeInfo_var);
		TestResultRenderer__ctor_m1018687449(L_0, /*hidden argument*/NULL);
		((TestRunner_t1304041832_StaticFields*)TestRunner_t1304041832_il2cpp_TypeInfo_var->static_fields)->set_k_ResultRenderer_3(L_0);
		return;
	}
}
// System.String UnityTest.TestRunner::<Start>m__0(System.Type)
extern "C"  String_t* TestRunner_U3CStartU3Em__0_m1296735611 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_0);
		return L_1;
	}
}
// UnityTest.TestResult UnityTest.TestRunner::<InitRunner>m__1(UnityTest.TestComponent)
extern Il2CppClass* TestResult_t490498461_il2cpp_TypeInfo_var;
extern const uint32_t TestRunner_U3CInitRunnerU3Em__1_m3056360923_MetadataUsageId;
extern "C"  TestResult_t490498461 * TestRunner_U3CInitRunnerU3Em__1_m3056360923 (Il2CppObject * __this /* static, unused */, TestComponent_t2516511601 * ___component0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_U3CInitRunnerU3Em__1_m3056360923_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TestComponent_t2516511601 * L_0 = ___component0;
		TestResult_t490498461 * L_1 = (TestResult_t490498461 *)il2cpp_codegen_object_new(TestResult_t490498461_il2cpp_TypeInfo_var);
		TestResult__ctor_m1646622649(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityTest.ITestComponent UnityTest.TestRunner::<InitRunner>m__2(UnityTest.TestResult)
extern "C"  Il2CppObject * TestRunner_U3CInitRunnerU3Em__2_m1385019429 (Il2CppObject * __this /* static, unused */, TestResult_t490498461 * ___result0, const MethodInfo* method)
{
	{
		TestResult_t490498461 * L_0 = ___result0;
		NullCheck(L_0);
		TestComponent_t2516511601 * L_1 = L_0->get_TestComponent_8();
		return L_1;
	}
}
// System.Boolean UnityTest.TestRunner::<OnDestroy>m__3(UnityTest.TestResult)
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern const uint32_t TestRunner_U3COnDestroyU3Em__3_m2714852837_MetadataUsageId;
extern "C"  bool TestRunner_U3COnDestroyU3Em__3_m2714852837 (TestRunner_t1304041832 * __this, TestResult_t490498461 * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_U3COnDestroyU3Em__3_m2714852837_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TestResult_t490498461 * L_0 = ___result0;
		NullCheck(L_0);
		TestComponent_t2516511601 * L_1 = L_0->get_TestComponent_8();
		TestComponent_t2516511601 * L_2 = __this->get_currentTest_4();
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		bool L_3 = TestComponent_op_Equality_m920468941(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityTest.TestRunner::<PrintResultToLog>m__4(UnityTest.TestResult)
extern "C"  bool TestRunner_U3CPrintResultToLogU3Em__4_m3842018790 (Il2CppObject * __this /* static, unused */, TestResult_t490498461 * ___t0, const MethodInfo* method)
{
	{
		TestResult_t490498461 * L_0 = ___t0;
		NullCheck(L_0);
		bool L_1 = TestResult_get_IsSuccess_m4143018983(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityTest.TestRunner::<PrintResultToLog>m__5(UnityTest.TestResult)
extern "C"  bool TestRunner_U3CPrintResultToLogU3Em__5_m4006570311 (Il2CppObject * __this /* static, unused */, TestResult_t490498461 * ___result0, const MethodInfo* method)
{
	{
		TestResult_t490498461 * L_0 = ___result0;
		NullCheck(L_0);
		bool L_1 = TestResult_get_IsFailure_m4065885444(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityTest.TestRunner::<PrintResultToLog>m__6(UnityTest.TestResult)
extern "C"  bool TestRunner_U3CPrintResultToLogU3Em__6_m3458344612 (Il2CppObject * __this /* static, unused */, TestResult_t490498461 * ___t0, const MethodInfo* method)
{
	{
		TestResult_t490498461 * L_0 = ___t0;
		NullCheck(L_0);
		bool L_1 = TestResult_get_IsFailure_m4065885444(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityTest.TestRunner::<PrintResultToLog>m__7(UnityTest.TestResult)
extern "C"  bool TestRunner_U3CPrintResultToLogU3Em__7_m3926491909 (Il2CppObject * __this /* static, unused */, TestResult_t490498461 * ___t0, const MethodInfo* method)
{
	{
		TestResult_t490498461 * L_0 = ___t0;
		NullCheck(L_0);
		bool L_1 = TestResult_get_IsFailure_m4065885444(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String UnityTest.TestRunner::<PrintResultToLog>m__8(UnityTest.TestResult)
extern "C"  String_t* TestRunner_U3CPrintResultToLogU3Em__8_m4236607815 (Il2CppObject * __this /* static, unused */, TestResult_t490498461 * ___result0, const MethodInfo* method)
{
	{
		TestResult_t490498461 * L_0 = ___result0;
		NullCheck(L_0);
		String_t* L_1 = TestResult_get_Name_m1227207940(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityTest.TestRunner::<PrintResultToLog>m__9(UnityTest.TestResult)
extern "C"  bool TestRunner_U3CPrintResultToLogU3Em__9_m3619323859 (Il2CppObject * __this /* static, unused */, TestResult_t490498461 * ___result0, const MethodInfo* method)
{
	{
		TestResult_t490498461 * L_0 = ___result0;
		NullCheck(L_0);
		bool L_1 = TestResult_get_IsIgnored_m638715012(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityTest.TestRunner::<PrintResultToLog>m__A(UnityTest.TestResult)
extern "C"  bool TestRunner_U3CPrintResultToLogU3Em__A_m2707175355 (Il2CppObject * __this /* static, unused */, TestResult_t490498461 * ___t0, const MethodInfo* method)
{
	{
		TestResult_t490498461 * L_0 = ___t0;
		NullCheck(L_0);
		bool L_1 = TestResult_get_IsIgnored_m638715012(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityTest.TestRunner::<PrintResultToLog>m__B(UnityTest.TestResult)
extern "C"  bool TestRunner_U3CPrintResultToLogU3Em__B_m1211561240 (Il2CppObject * __this /* static, unused */, TestResult_t490498461 * ___t0, const MethodInfo* method)
{
	{
		TestResult_t490498461 * L_0 = ___t0;
		NullCheck(L_0);
		bool L_1 = TestResult_get_IsIgnored_m638715012(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String UnityTest.TestRunner::<PrintResultToLog>m__C(UnityTest.TestResult)
extern "C"  String_t* TestRunner_U3CPrintResultToLogU3Em__C_m4240348228 (Il2CppObject * __this /* static, unused */, TestResult_t490498461 * ___result0, const MethodInfo* method)
{
	{
		TestResult_t490498461 * L_0 = ___result0;
		NullCheck(L_0);
		String_t* L_1 = TestResult_get_Name_m1227207940(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityTest.TestRunner::<StartNewTest>m__D(UnityTest.TestResult)
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern const uint32_t TestRunner_U3CStartNewTestU3Em__D_m915839043_MetadataUsageId;
extern "C"  bool TestRunner_U3CStartNewTestU3Em__D_m915839043 (TestRunner_t1304041832 * __this, TestResult_t490498461 * ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_U3CStartNewTestU3Em__D_m915839043_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TestResult_t490498461 * L_0 = ___result0;
		NullCheck(L_0);
		TestComponent_t2516511601 * L_1 = L_0->get_TestComponent_8();
		TestComponent_t2516511601 * L_2 = __this->get_currentTest_4();
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		bool L_3 = TestComponent_op_Equality_m920468941(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityTest.TestRunner::<FinishTest>m__E(UnityTest.TestResult)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t TestRunner_U3CFinishTestU3Em__E_m1879346421_MetadataUsageId;
extern "C"  bool TestRunner_U3CFinishTestU3Em__E_m1879346421 (TestRunner_t1304041832 * __this, TestResult_t490498461 * ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunner_U3CFinishTestU3Em__E_m1879346421_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TestResult_t490498461 * L_0 = ___t0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = TestResult_get_GameObject_m1228274866(L_0, /*hidden argument*/NULL);
		TestComponent_t2516511601 * L_2 = __this->get_currentTest_4();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityTest.TestRunner/<ParseListForGroups>c__AnonStorey1::.ctor()
extern "C"  void U3CParseListForGroupsU3Ec__AnonStorey1__ctor_m898455413 (U3CParseListForGroupsU3Ec__AnonStorey1_t3712145952 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityTest.TestRunner/<ParseListForGroups>c__AnonStorey1::<>m__0(UnityEngine.Component)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t U3CParseListForGroupsU3Ec__AnonStorey1_U3CU3Em__0_m764506160_MetadataUsageId;
extern "C"  bool U3CParseListForGroupsU3Ec__AnonStorey1_U3CU3Em__0_m764506160 (U3CParseListForGroupsU3Ec__AnonStorey1_t3712145952 * __this, Component_t3819376471 * ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CParseListForGroupsU3Ec__AnonStorey1_U3CU3Em__0_m764506160_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Component_t3819376471 * L_0 = ___t0;
		TestComponent_t2516511601 * L_1 = __this->get_testResult_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityTest.TestRunner/<StateMachine>c__Iterator0::.ctor()
extern "C"  void U3CStateMachineU3Ec__Iterator0__ctor_m574500974 (U3CStateMachineU3Ec__Iterator0_t817978249 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityTest.TestRunner/<StateMachine>c__Iterator0::MoveNext()
extern Il2CppClass* RuntimePlatform_t1869584967_il2cpp_TypeInfo_var;
extern Il2CppClass* TestComponent_t2516511601_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CStateMachineU3Ec__Iterator0_t817978249_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3268561776_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentsInChildren_TisAssertionComponent_t3962419315_m3292534916_MethodInfo_var;
extern const MethodInfo* U3CStateMachineU3Ec__Iterator0_U3CU3Em__0_m2078372045_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m780338055_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisAssertionComponent_t3962419315_m1981650746_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisAssertionComponent_t3962419315_m607603754_MethodInfo_var;
extern const MethodInfo* Enumerable_Any_TisAssertionComponent_t3962419315_m3016556267_MethodInfo_var;
extern const MethodInfo* U3CStateMachineU3Ec__Iterator0_U3CU3Em__1_m4161423954_MethodInfo_var;
extern const MethodInfo* Enumerable_All_TisAssertionComponent_t3962419315_m579294480_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral168715698;
extern Il2CppCodeGenString* _stringLiteral3452561551;
extern Il2CppCodeGenString* _stringLiteral415804494;
extern Il2CppCodeGenString* _stringLiteral1298458407;
extern Il2CppCodeGenString* _stringLiteral2938486522;
extern const uint32_t U3CStateMachineU3Ec__Iterator0_MoveNext_m106726754_MetadataUsageId;
extern "C"  bool U3CStateMachineU3Ec__Iterator0_MoveNext_m106726754 (U3CStateMachineU3Ec__Iterator0_t817978249 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStateMachineU3Ec__Iterator0_MoveNext_m106726754_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	AssertionComponentU5BU5D_t2921566306* V_2 = NULL;
	int32_t V_3 = 0;
	AssertionComponentU5BU5D_t2921566306* G_B13_0 = NULL;
	AssertionComponentU5BU5D_t2921566306* G_B12_0 = NULL;
	AssertionComponentU5BU5D_t2921566306* G_B16_0 = NULL;
	AssertionComponentU5BU5D_t2921566306* G_B15_0 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_02ad;
		}
	}
	{
		goto IL_02b9;
	}

IL_0021:
	{
		TestRunner_t1304041832 * L_2 = __this->get_U24this_0();
		NullCheck(L_2);
		TestRunnerCallbackList_t3689696405 * L_3 = L_2->get_TestRunnerCallback_13();
		int32_t L_4 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_4;
		Il2CppObject * L_5 = Box(RuntimePlatform_t1869584967_il2cpp_TypeInfo_var, (&V_1));
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		TestRunner_t1304041832 * L_7 = __this->get_U24this_0();
		NullCheck(L_7);
		List_1_t1885632733 * L_8 = L_7->get_m_TestComponents_6();
		NullCheck(L_3);
		TestRunnerCallbackList_RunStarted_m1739761573(L_3, L_6, L_8, /*hidden argument*/NULL);
	}

IL_004f:
	{
		TestRunner_t1304041832 * L_9 = __this->get_U24this_0();
		NullCheck(L_9);
		IntegrationTestsProvider_t1666533096 * L_10 = L_9->get_m_TestsProvider_14();
		NullCheck(L_10);
		bool L_11 = IntegrationTestsProvider_AnyTestsLeft_m3901869230(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_008a;
		}
	}
	{
		TestRunner_t1304041832 * L_12 = __this->get_U24this_0();
		NullCheck(L_12);
		TestComponent_t2516511601 * L_13 = L_12->get_currentTest_4();
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		bool L_14 = TestComponent_op_Equality_m920468941(NULL /*static, unused*/, L_13, (TestComponent_t2516511601 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_008a;
		}
	}
	{
		TestRunner_t1304041832 * L_15 = __this->get_U24this_0();
		NullCheck(L_15);
		TestRunner_FinishTestRun_m2535838598(L_15, /*hidden argument*/NULL);
		goto IL_02b9;
	}

IL_008a:
	{
		TestRunner_t1304041832 * L_16 = __this->get_U24this_0();
		NullCheck(L_16);
		TestComponent_t2516511601 * L_17 = L_16->get_currentTest_4();
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		bool L_18 = TestComponent_op_Equality_m920468941(NULL /*static, unused*/, L_17, (TestComponent_t2516511601 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00ab;
		}
	}
	{
		TestRunner_t1304041832 * L_19 = __this->get_U24this_0();
		NullCheck(L_19);
		TestRunner_StartNewTest_m2384781446(L_19, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		TestRunner_t1304041832 * L_20 = __this->get_U24this_0();
		NullCheck(L_20);
		TestComponent_t2516511601 * L_21 = L_20->get_currentTest_4();
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		bool L_22 = TestComponent_op_Inequality_m3300184364(NULL /*static, unused*/, L_21, (TestComponent_t2516511601 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0292;
		}
	}
	{
		TestRunner_t1304041832 * L_23 = __this->get_U24this_0();
		NullCheck(L_23);
		int32_t L_24 = L_23->get_m_TestState_11();
		if (L_24)
		{
			goto IL_01c0;
		}
	}
	{
		TestRunner_t1304041832 * L_25 = __this->get_U24this_0();
		NullCheck(L_25);
		TestComponent_t2516511601 * L_26 = L_25->get_currentTest_4();
		NullCheck(L_26);
		bool L_27 = TestComponent_ShouldSucceedOnAssertions_m942204676(L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0177;
		}
	}
	{
		TestRunner_t1304041832 * L_28 = __this->get_U24this_0();
		NullCheck(L_28);
		TestComponent_t2516511601 * L_29 = L_28->get_currentTest_4();
		NullCheck(L_29);
		GameObject_t1756533147 * L_30 = Component_get_gameObject_m3105766835(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		AssertionComponentU5BU5D_t2921566306* L_31 = GameObject_GetComponentsInChildren_TisAssertionComponent_t3962419315_m3292534916(L_30, /*hidden argument*/GameObject_GetComponentsInChildren_TisAssertionComponent_t3962419315_m3292534916_MethodInfo_var);
		Func_2_t3268561776 * L_32 = ((U3CStateMachineU3Ec__Iterator0_t817978249_StaticFields*)U3CStateMachineU3Ec__Iterator0_t817978249_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_4();
		G_B12_0 = L_31;
		if (L_32)
		{
			G_B13_0 = L_31;
			goto IL_0113;
		}
	}
	{
		IntPtr_t L_33;
		L_33.set_m_value_0((void*)(void*)U3CStateMachineU3Ec__Iterator0_U3CU3Em__0_m2078372045_MethodInfo_var);
		Func_2_t3268561776 * L_34 = (Func_2_t3268561776 *)il2cpp_codegen_object_new(Func_2_t3268561776_il2cpp_TypeInfo_var);
		Func_2__ctor_m780338055(L_34, NULL, L_33, /*hidden argument*/Func_2__ctor_m780338055_MethodInfo_var);
		((U3CStateMachineU3Ec__Iterator0_t817978249_StaticFields*)U3CStateMachineU3Ec__Iterator0_t817978249_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_4(L_34);
		G_B13_0 = G_B12_0;
	}

IL_0113:
	{
		Func_2_t3268561776 * L_35 = ((U3CStateMachineU3Ec__Iterator0_t817978249_StaticFields*)U3CStateMachineU3Ec__Iterator0_t817978249_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_4();
		Il2CppObject* L_36 = Enumerable_Where_TisAssertionComponent_t3962419315_m1981650746(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)G_B13_0, L_35, /*hidden argument*/Enumerable_Where_TisAssertionComponent_t3962419315_m1981650746_MethodInfo_var);
		AssertionComponentU5BU5D_t2921566306* L_37 = Enumerable_ToArray_TisAssertionComponent_t3962419315_m607603754(NULL /*static, unused*/, L_36, /*hidden argument*/Enumerable_ToArray_TisAssertionComponent_t3962419315_m607603754_MethodInfo_var);
		V_2 = L_37;
		AssertionComponentU5BU5D_t2921566306* L_38 = V_2;
		bool L_39 = Enumerable_Any_TisAssertionComponent_t3962419315_m3016556267(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_38, /*hidden argument*/Enumerable_Any_TisAssertionComponent_t3962419315_m3016556267_MethodInfo_var);
		if (!L_39)
		{
			goto IL_0177;
		}
	}
	{
		AssertionComponentU5BU5D_t2921566306* L_40 = V_2;
		Func_2_t3268561776 * L_41 = ((U3CStateMachineU3Ec__Iterator0_t817978249_StaticFields*)U3CStateMachineU3Ec__Iterator0_t817978249_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_5();
		G_B15_0 = L_40;
		if (L_41)
		{
			G_B16_0 = L_40;
			goto IL_0147;
		}
	}
	{
		IntPtr_t L_42;
		L_42.set_m_value_0((void*)(void*)U3CStateMachineU3Ec__Iterator0_U3CU3Em__1_m4161423954_MethodInfo_var);
		Func_2_t3268561776 * L_43 = (Func_2_t3268561776 *)il2cpp_codegen_object_new(Func_2_t3268561776_il2cpp_TypeInfo_var);
		Func_2__ctor_m780338055(L_43, NULL, L_42, /*hidden argument*/Func_2__ctor_m780338055_MethodInfo_var);
		((U3CStateMachineU3Ec__Iterator0_t817978249_StaticFields*)U3CStateMachineU3Ec__Iterator0_t817978249_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_5(L_43);
		G_B16_0 = G_B15_0;
	}

IL_0147:
	{
		Func_2_t3268561776 * L_44 = ((U3CStateMachineU3Ec__Iterator0_t817978249_StaticFields*)U3CStateMachineU3Ec__Iterator0_t817978249_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_5();
		bool L_45 = Enumerable_All_TisAssertionComponent_t3962419315_m579294480(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)G_B16_0, L_44, /*hidden argument*/Enumerable_All_TisAssertionComponent_t3962419315_m579294480_MethodInfo_var);
		if (!L_45)
		{
			goto IL_0177;
		}
	}
	{
		TestRunner_t1304041832 * L_46 = __this->get_U24this_0();
		NullCheck(L_46);
		TestComponent_t2516511601 * L_47 = L_46->get_currentTest_4();
		NullCheck(L_47);
		GameObject_t1756533147 * L_48 = Component_get_gameObject_m3105766835(L_47, /*hidden argument*/NULL);
		IntegrationTest_Pass_m145489534(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		TestRunner_t1304041832 * L_49 = __this->get_U24this_0();
		NullCheck(L_49);
		L_49->set_m_TestState_11(1);
	}

IL_0177:
	{
		TestRunner_t1304041832 * L_50 = __this->get_U24this_0();
		NullCheck(L_50);
		TestComponent_t2516511601 * L_51 = L_50->get_currentTest_4();
		IL2CPP_RUNTIME_CLASS_INIT(TestComponent_t2516511601_il2cpp_TypeInfo_var);
		bool L_52 = TestComponent_op_Inequality_m3300184364(NULL /*static, unused*/, L_51, (TestComponent_t2516511601 *)NULL, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_01c0;
		}
	}
	{
		float L_53 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		TestRunner_t1304041832 * L_54 = __this->get_U24this_0();
		NullCheck(L_54);
		double L_55 = L_54->get_m_StartTime_7();
		TestRunner_t1304041832 * L_56 = __this->get_U24this_0();
		NullCheck(L_56);
		TestComponent_t2516511601 * L_57 = L_56->get_currentTest_4();
		NullCheck(L_57);
		double L_58 = TestComponent_GetTimeout_m3814449449(L_57, /*hidden argument*/NULL);
		if ((!(((double)(((double)((double)L_53)))) > ((double)((double)((double)L_55+(double)L_58))))))
		{
			goto IL_01c0;
		}
	}
	{
		TestRunner_t1304041832 * L_59 = __this->get_U24this_0();
		NullCheck(L_59);
		L_59->set_m_TestState_11(4);
	}

IL_01c0:
	{
		TestRunner_t1304041832 * L_60 = __this->get_U24this_0();
		NullCheck(L_60);
		int32_t L_61 = L_60->get_m_TestState_11();
		V_3 = L_61;
		int32_t L_62 = V_3;
		if (((int32_t)((int32_t)L_62-(int32_t)1)) == 0)
		{
			goto IL_01ed;
		}
		if (((int32_t)((int32_t)L_62-(int32_t)1)) == 1)
		{
			goto IL_020e;
		}
		if (((int32_t)((int32_t)L_62-(int32_t)1)) == 2)
		{
			goto IL_022f;
		}
		if (((int32_t)((int32_t)L_62-(int32_t)1)) == 3)
		{
			goto IL_0250;
		}
		if (((int32_t)((int32_t)L_62-(int32_t)1)) == 4)
		{
			goto IL_0271;
		}
	}
	{
		goto IL_0292;
	}

IL_01ed:
	{
		TestRunner_t1304041832 * L_63 = __this->get_U24this_0();
		NullCheck(L_63);
		TestRunner_LogMessage_m1520221235(L_63, _stringLiteral168715698, /*hidden argument*/NULL);
		TestRunner_t1304041832 * L_64 = __this->get_U24this_0();
		NullCheck(L_64);
		TestRunner_FinishTest_m1325069965(L_64, 0, /*hidden argument*/NULL);
		goto IL_0292;
	}

IL_020e:
	{
		TestRunner_t1304041832 * L_65 = __this->get_U24this_0();
		NullCheck(L_65);
		TestRunner_LogMessage_m1520221235(L_65, _stringLiteral3452561551, /*hidden argument*/NULL);
		TestRunner_t1304041832 * L_66 = __this->get_U24this_0();
		NullCheck(L_66);
		TestRunner_FinishTest_m1325069965(L_66, 1, /*hidden argument*/NULL);
		goto IL_0292;
	}

IL_022f:
	{
		TestRunner_t1304041832 * L_67 = __this->get_U24this_0();
		NullCheck(L_67);
		TestRunner_LogMessage_m1520221235(L_67, _stringLiteral415804494, /*hidden argument*/NULL);
		TestRunner_t1304041832 * L_68 = __this->get_U24this_0();
		NullCheck(L_68);
		TestRunner_FinishTest_m1325069965(L_68, 4, /*hidden argument*/NULL);
		goto IL_0292;
	}

IL_0250:
	{
		TestRunner_t1304041832 * L_69 = __this->get_U24this_0();
		NullCheck(L_69);
		TestRunner_LogMessage_m1520221235(L_69, _stringLiteral1298458407, /*hidden argument*/NULL);
		TestRunner_t1304041832 * L_70 = __this->get_U24this_0();
		NullCheck(L_70);
		TestRunner_FinishTest_m1325069965(L_70, 2, /*hidden argument*/NULL);
		goto IL_0292;
	}

IL_0271:
	{
		TestRunner_t1304041832 * L_71 = __this->get_U24this_0();
		NullCheck(L_71);
		TestRunner_LogMessage_m1520221235(L_71, _stringLiteral2938486522, /*hidden argument*/NULL);
		TestRunner_t1304041832 * L_72 = __this->get_U24this_0();
		NullCheck(L_72);
		TestRunner_FinishTest_m1325069965(L_72, 5, /*hidden argument*/NULL);
		goto IL_0292;
	}

IL_0292:
	{
		__this->set_U24current_1(NULL);
		bool L_73 = __this->get_U24disposing_2();
		if (L_73)
		{
			goto IL_02a8;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_02a8:
	{
		goto IL_02bb;
	}

IL_02ad:
	{
		goto IL_004f;
	}
	// Dead block : IL_02b2: ldarg.0

IL_02b9:
	{
		return (bool)0;
	}

IL_02bb:
	{
		return (bool)1;
	}
}
// System.Object UnityTest.TestRunner/<StateMachine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStateMachineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1009509828 (U3CStateMachineU3Ec__Iterator0_t817978249 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object UnityTest.TestRunner/<StateMachine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStateMachineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3693315932 (U3CStateMachineU3Ec__Iterator0_t817978249 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void UnityTest.TestRunner/<StateMachine>c__Iterator0::Dispose()
extern "C"  void U3CStateMachineU3Ec__Iterator0_Dispose_m692648083 (U3CStateMachineU3Ec__Iterator0_t817978249 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void UnityTest.TestRunner/<StateMachine>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CStateMachineU3Ec__Iterator0_Reset_m3752867569_MetadataUsageId;
extern "C"  void U3CStateMachineU3Ec__Iterator0_Reset_m3752867569 (U3CStateMachineU3Ec__Iterator0_t817978249 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStateMachineU3Ec__Iterator0_Reset_m3752867569_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean UnityTest.TestRunner/<StateMachine>c__Iterator0::<>m__0(UnityTest.AssertionComponent)
extern "C"  bool U3CStateMachineU3Ec__Iterator0_U3CU3Em__0_m2078372045 (Il2CppObject * __this /* static, unused */, AssertionComponent_t3962419315 * ___a0, const MethodInfo* method)
{
	{
		AssertionComponent_t3962419315 * L_0 = ___a0;
		NullCheck(L_0);
		bool L_1 = Behaviour_get_enabled_m4079055610(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityTest.TestRunner/<StateMachine>c__Iterator0::<>m__1(UnityTest.AssertionComponent)
extern "C"  bool U3CStateMachineU3Ec__Iterator0_U3CU3Em__1_m4161423954 (Il2CppObject * __this /* static, unused */, AssertionComponent_t3962419315 * ___a0, const MethodInfo* method)
{
	{
		AssertionComponent_t3962419315 * L_0 = ___a0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_checksPerformed_11();
		return (bool)((((int32_t)L_1) > ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityTest.TestRunnerConfigurator::.ctor()
extern "C"  void TestRunnerConfigurator__ctor_m705632011 (TestRunnerConfigurator_t1966496711 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		TestRunnerConfigurator_CheckForBatchMode_m3039033525(__this, /*hidden argument*/NULL);
		TestRunnerConfigurator_CheckForSendingResultsOverNetwork_m3289821816(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityTest.TestRunnerConfigurator::get_isBatchRun()
extern "C"  bool TestRunnerConfigurator_get_isBatchRun_m4088445273 (TestRunnerConfigurator_t1966496711 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CisBatchRunU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void UnityTest.TestRunnerConfigurator::set_isBatchRun(System.Boolean)
extern "C"  void TestRunnerConfigurator_set_isBatchRun_m557229600 (TestRunnerConfigurator_t1966496711 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CisBatchRunU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Boolean UnityTest.TestRunnerConfigurator::get_sendResultsOverNetwork()
extern "C"  bool TestRunnerConfigurator_get_sendResultsOverNetwork_m2213479566 (TestRunnerConfigurator_t1966496711 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CsendResultsOverNetworkU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void UnityTest.TestRunnerConfigurator::set_sendResultsOverNetwork(System.Boolean)
extern "C"  void TestRunnerConfigurator_set_sendResultsOverNetwork_m3490115631 (TestRunnerConfigurator_t1966496711 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CsendResultsOverNetworkU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.String UnityTest.TestRunnerConfigurator::GetIntegrationTestScenes(System.Int32)
extern Il2CppClass* TestRunnerConfigurator_t1966496711_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m780127360_MethodInfo_var;
extern const MethodInfo* Enumerable_ElementAt_TisString_t_m1763072630_MethodInfo_var;
extern const uint32_t TestRunnerConfigurator_GetIntegrationTestScenes_m1098943538_MetadataUsageId;
extern "C"  String_t* TestRunnerConfigurator_GetIntegrationTestScenes_m1098943538 (TestRunnerConfigurator_t1966496711 * __this, int32_t ___testSceneNum0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunnerConfigurator_GetIntegrationTestScenes_m1098943538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	List_1_t1398341365 * V_1 = NULL;
	String_t* V_2 = NULL;
	StringU5BU5D_t1642385972* V_3 = NULL;
	int32_t V_4 = 0;
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestRunnerConfigurator_t1966496711_il2cpp_TypeInfo_var);
		String_t* L_1 = ((TestRunnerConfigurator_t1966496711_StaticFields*)TestRunnerConfigurator_t1966496711_il2cpp_TypeInfo_var->static_fields)->get_testScenesToRun_2();
		String_t* L_2 = TestRunnerConfigurator_GetTextFromTempFile_m2815795139(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0025;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestRunnerConfigurator_t1966496711_il2cpp_TypeInfo_var);
		String_t* L_3 = ((TestRunnerConfigurator_t1966496711_StaticFields*)TestRunnerConfigurator_t1966496711_il2cpp_TypeInfo_var->static_fields)->get_testScenesToRun_2();
		String_t* L_4 = TestRunnerConfigurator_GetTextFromTextAsset_m1284058970(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_0025:
	{
		List_1_t1398341365 * L_5 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_5, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		V_1 = L_5;
		String_t* L_6 = V_0;
		CharU5BU5D_t1328083999* L_7 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)10));
		NullCheck(L_6);
		StringU5BU5D_t1642385972* L_8 = String_Split_m631480578(L_6, L_7, 1, /*hidden argument*/NULL);
		V_3 = L_8;
		V_4 = 0;
		goto IL_005d;
	}

IL_0046:
	{
		StringU5BU5D_t1642385972* L_9 = V_3;
		int32_t L_10 = V_4;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		String_t* L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		V_2 = L_12;
		List_1_t1398341365 * L_13 = V_1;
		String_t* L_14 = V_2;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
		NullCheck(L_13);
		List_1_Add_m4061286785(L_13, L_15, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		int32_t L_16 = V_4;
		V_4 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005d:
	{
		int32_t L_17 = V_4;
		StringU5BU5D_t1642385972* L_18 = V_3;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_19 = ___testSceneNum0;
		List_1_t1398341365 * L_20 = V_1;
		NullCheck(L_20);
		int32_t L_21 = List_1_get_Count_m780127360(L_20, /*hidden argument*/List_1_get_Count_m780127360_MethodInfo_var);
		if ((((int32_t)L_19) >= ((int32_t)L_21)))
		{
			goto IL_007b;
		}
	}
	{
		List_1_t1398341365 * L_22 = V_1;
		int32_t L_23 = ___testSceneNum0;
		String_t* L_24 = Enumerable_ElementAt_TisString_t_m1763072630(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/Enumerable_ElementAt_TisString_t_m1763072630_MethodInfo_var);
		return L_24;
	}

IL_007b:
	{
		return (String_t*)NULL;
	}
}
// System.Void UnityTest.TestRunnerConfigurator::CheckForSendingResultsOverNetwork()
extern "C"  void TestRunnerConfigurator_CheckForSendingResultsOverNetwork_m3289821816 (TestRunnerConfigurator_t1966496711 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.String UnityTest.TestRunnerConfigurator::GetTextFromTextAsset(System.String)
extern Il2CppClass* TextAsset_t3973159845_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t TestRunnerConfigurator_GetTextFromTextAsset_m1284058970_MetadataUsageId;
extern "C"  String_t* TestRunnerConfigurator_GetTextFromTextAsset_m1284058970 (Il2CppObject * __this /* static, unused */, String_t* ___fileName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunnerConfigurator_GetTextFromTextAsset_m1284058970_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	TextAsset_t3973159845 * V_1 = NULL;
	String_t* G_B3_0 = NULL;
	{
		String_t* L_0 = ___fileName0;
		String_t* L_1 = ___fileName0;
		NullCheck(L_1);
		int32_t L_2 = String_LastIndexOf_m3555875680(L_1, ((int32_t)46), /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_3 = String_Substring_m12482732(L_0, 0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = V_0;
		Object_t1021602117 * L_5 = Resources_Load_m2041782325(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_1 = ((TextAsset_t3973159845 *)IsInstClass(L_5, TextAsset_t3973159845_il2cpp_TypeInfo_var));
		TextAsset_t3973159845 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0033;
		}
	}
	{
		TextAsset_t3973159845 * L_8 = V_1;
		NullCheck(L_8);
		String_t* L_9 = TextAsset_get_text_m2589865997(L_8, /*hidden argument*/NULL);
		G_B3_0 = L_9;
		goto IL_0034;
	}

IL_0033:
	{
		G_B3_0 = ((String_t*)(NULL));
	}

IL_0034:
	{
		return G_B3_0;
	}
}
// System.String UnityTest.TestRunnerConfigurator::GetTextFromTempFile(System.String)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TestRunnerConfigurator_GetTextFromTempFile_m2815795139_MetadataUsageId;
extern "C"  String_t* TestRunnerConfigurator_GetTextFromTempFile_m2815795139 (Il2CppObject * __this /* static, unused */, String_t* ___fileName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunnerConfigurator_GetTextFromTempFile_m2815795139_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (String_t*)NULL;
	}

IL_0002:
	try
	{ // begin try (depth: 1)
		goto IL_000f;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0007;
		throw e;
	}

CATCH_0007:
	{ // begin catch(System.Object)
		V_1 = (String_t*)NULL;
		goto IL_0011;
	} // end catch (depth: 1)

IL_000f:
	{
		String_t* L_0 = V_0;
		return L_0;
	}

IL_0011:
	{
		String_t* L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityTest.TestRunnerConfigurator::CheckForBatchMode()
extern Il2CppClass* TestRunnerConfigurator_t1966496711_il2cpp_TypeInfo_var;
extern const uint32_t TestRunnerConfigurator_CheckForBatchMode_m3039033525_MetadataUsageId;
extern "C"  void TestRunnerConfigurator_CheckForBatchMode_m3039033525 (TestRunnerConfigurator_t1966496711 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunnerConfigurator_CheckForBatchMode_m3039033525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TestRunnerConfigurator_t1966496711_il2cpp_TypeInfo_var);
		String_t* L_0 = ((TestRunnerConfigurator_t1966496711_StaticFields*)TestRunnerConfigurator_t1966496711_il2cpp_TypeInfo_var->static_fields)->get_batchRunFileMarker_1();
		String_t* L_1 = TestRunnerConfigurator_GetTextFromTextAsset_m1284058970(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		TestRunnerConfigurator_set_isBatchRun_m557229600(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Collections.Generic.List`1<System.String> UnityTest.TestRunnerConfigurator::GetAvailableNetworkIPs()
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const uint32_t TestRunnerConfigurator_GetAvailableNetworkIPs_m2707387739_MetadataUsageId;
extern "C"  List_1_t1398341365 * TestRunnerConfigurator_GetAvailableNetworkIPs_m2707387739 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunnerConfigurator_GetAvailableNetworkIPs_m2707387739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		return L_0;
	}
}
// UnityTest.IntegrationTestRunner.ITestRunnerCallback UnityTest.TestRunnerConfigurator::ResolveNetworkConnection()
extern "C"  Il2CppObject * TestRunnerConfigurator_ResolveNetworkConnection_m3878511651 (TestRunnerConfigurator_t1966496711 * __this, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Void UnityTest.TestRunnerConfigurator::.cctor()
extern Il2CppClass* TestRunnerConfigurator_t1966496711_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3159226856;
extern Il2CppCodeGenString* _stringLiteral1766715871;
extern Il2CppCodeGenString* _stringLiteral1571655865;
extern Il2CppCodeGenString* _stringLiteral2362111320;
extern const uint32_t TestRunnerConfigurator__cctor_m4037718552_MetadataUsageId;
extern "C"  void TestRunnerConfigurator__cctor_m4037718552 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestRunnerConfigurator__cctor_m4037718552_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((TestRunnerConfigurator_t1966496711_StaticFields*)TestRunnerConfigurator_t1966496711_il2cpp_TypeInfo_var->static_fields)->set_integrationTestsNetwork_0(_stringLiteral3159226856);
		((TestRunnerConfigurator_t1966496711_StaticFields*)TestRunnerConfigurator_t1966496711_il2cpp_TypeInfo_var->static_fields)->set_batchRunFileMarker_1(_stringLiteral1766715871);
		((TestRunnerConfigurator_t1966496711_StaticFields*)TestRunnerConfigurator_t1966496711_il2cpp_TypeInfo_var->static_fields)->set_testScenesToRun_2(_stringLiteral1571655865);
		((TestRunnerConfigurator_t1966496711_StaticFields*)TestRunnerConfigurator_t1966496711_il2cpp_TypeInfo_var->static_fields)->set_previousScenes_3(_stringLiteral2362111320);
		return;
	}
}
// System.Void UnityTest.TransformComparer::.ctor()
extern const MethodInfo* ComparerBaseGeneric_1__ctor_m2761158373_MethodInfo_var;
extern const uint32_t TransformComparer__ctor_m1537353437_MetadataUsageId;
extern "C"  void TransformComparer__ctor_m1537353437 (TransformComparer_t561999009 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransformComparer__ctor_m1537353437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComparerBaseGeneric_1__ctor_m2761158373(__this, /*hidden argument*/ComparerBaseGeneric_1__ctor_m2761158373_MethodInfo_var);
		return;
	}
}
// System.Boolean UnityTest.TransformComparer::Compare(UnityEngine.Transform,UnityEngine.Transform)
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern const uint32_t TransformComparer_Compare_m2606227028_MetadataUsageId;
extern "C"  bool TransformComparer_Compare_m2606227028 (TransformComparer_t561999009 * __this, Transform_t3275118058 * ___a0, Transform_t3275118058 * ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TransformComparer_Compare_m2606227028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_compareType_14();
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		Transform_t3275118058 * L_1 = ___a0;
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = ___b1;
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_position_m1104419803(L_3, /*hidden argument*/NULL);
		bool L_5 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_001d:
	{
		int32_t L_6 = __this->get_compareType_14();
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_003b;
		}
	}
	{
		Transform_t3275118058 * L_7 = ___a0;
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = Transform_get_position_m1104419803(L_7, /*hidden argument*/NULL);
		Transform_t3275118058 * L_9 = ___b1;
		NullCheck(L_9);
		Vector3_t2243707580  L_10 = Transform_get_position_m1104419803(L_9, /*hidden argument*/NULL);
		bool L_11 = Vector3_op_Inequality_m799191452(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_003b:
	{
		Exception_t1927440687 * L_12 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m3886110570(L_12, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}
}
// System.Void UnityTest.ValueDoesNotChange::.ctor()
extern "C"  void ValueDoesNotChange__ctor_m1911413009 (ValueDoesNotChange_t1999561901 * __this, const MethodInfo* method)
{
	{
		ActionBase__ctor_m4102037675(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityTest.ValueDoesNotChange::Compare(System.Object)
extern "C"  bool ValueDoesNotChange_Compare_m4093839954 (ValueDoesNotChange_t1999561901 * __this, Il2CppObject * ___a0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_m_Value_7();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppObject * L_1 = ___a0;
		__this->set_m_Value_7(L_1);
	}

IL_0012:
	{
		Il2CppObject * L_2 = __this->get_m_Value_7();
		Il2CppObject * L_3 = ___a0;
		NullCheck(L_2);
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_2, L_3);
		if (L_4)
		{
			goto IL_0025;
		}
	}
	{
		return (bool)0;
	}

IL_0025:
	{
		return (bool)1;
	}
}
// System.Void UnityTest.Vector2Comparer::.ctor()
extern const MethodInfo* VectorComparerBase_1__ctor_m1399026100_MethodInfo_var;
extern const uint32_t Vector2Comparer__ctor_m37349958_MetadataUsageId;
extern "C"  void Vector2Comparer__ctor_m37349958 (Vector2Comparer_t1532566132 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2Comparer__ctor_m37349958_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_floatingPointError_15((0.0001f));
		VectorComparerBase_1__ctor_m1399026100(__this, /*hidden argument*/VectorComparerBase_1__ctor_m1399026100_MethodInfo_var);
		return;
	}
}
// System.Boolean UnityTest.Vector2Comparer::Compare(UnityEngine.Vector2,UnityEngine.Vector2)
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern const MethodInfo* VectorComparerBase_1_AreVectorMagnitudeEqual_m1374791335_MethodInfo_var;
extern const uint32_t Vector2Comparer_Compare_m4164992529_MetadataUsageId;
extern "C"  bool Vector2Comparer_Compare_m4164992529 (Vector2Comparer_t1532566132 * __this, Vector2_t2243707579  ___a0, Vector2_t2243707579  ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2Comparer_Compare_m4164992529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_compareType_14();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0035;
		}
	}
	{
		goto IL_0054;
	}

IL_0019:
	{
		float L_3 = Vector2_get_magnitude_m33802565((&___a0), /*hidden argument*/NULL);
		float L_4 = Vector2_get_magnitude_m33802565((&___b1), /*hidden argument*/NULL);
		float L_5 = __this->get_floatingPointError_15();
		bool L_6 = VectorComparerBase_1_AreVectorMagnitudeEqual_m1374791335(__this, L_3, L_4, (((double)((double)L_5))), /*hidden argument*/VectorComparerBase_1_AreVectorMagnitudeEqual_m1374791335_MethodInfo_var);
		return L_6;
	}

IL_0035:
	{
		float L_7 = Vector2_get_magnitude_m33802565((&___a0), /*hidden argument*/NULL);
		float L_8 = Vector2_get_magnitude_m33802565((&___b1), /*hidden argument*/NULL);
		float L_9 = __this->get_floatingPointError_15();
		bool L_10 = VectorComparerBase_1_AreVectorMagnitudeEqual_m1374791335(__this, L_7, L_8, (((double)((double)L_9))), /*hidden argument*/VectorComparerBase_1_AreVectorMagnitudeEqual_m1374791335_MethodInfo_var);
		return (bool)((((int32_t)L_10) == ((int32_t)0))? 1 : 0);
	}

IL_0054:
	{
		Exception_t1927440687 * L_11 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m3886110570(L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}
}
// System.Int32 UnityTest.Vector2Comparer::GetDepthOfSearch()
extern "C"  int32_t Vector2Comparer_GetDepthOfSearch_m3473229810 (Vector2Comparer_t1532566132 * __this, const MethodInfo* method)
{
	{
		return 3;
	}
}
// System.Void UnityTest.Vector3Comparer::.ctor()
extern const MethodInfo* VectorComparerBase_1__ctor_m2196001199_MethodInfo_var;
extern const uint32_t Vector3Comparer__ctor_m13630023_MetadataUsageId;
extern "C"  void Vector3Comparer__ctor_m13630023 (Vector3Comparer_t1524406581 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3Comparer__ctor_m13630023_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_floatingPointError_15((9.9999997473787516E-05));
		VectorComparerBase_1__ctor_m2196001199(__this, /*hidden argument*/VectorComparerBase_1__ctor_m2196001199_MethodInfo_var);
		return;
	}
}
// System.Boolean UnityTest.Vector3Comparer::Compare(UnityEngine.Vector3,UnityEngine.Vector3)
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern const MethodInfo* VectorComparerBase_1_AreVectorMagnitudeEqual_m290049576_MethodInfo_var;
extern const uint32_t Vector3Comparer_Compare_m3187825740_MetadataUsageId;
extern "C"  bool Vector3Comparer_Compare_m3187825740 (Vector3Comparer_t1524406581 * __this, Vector3_t2243707580  ___a0, Vector3_t2243707580  ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3Comparer_Compare_m3187825740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_compareType_14();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0034;
		}
	}
	{
		goto IL_0052;
	}

IL_0019:
	{
		float L_3 = Vector3_get_magnitude_m860342598((&___a0), /*hidden argument*/NULL);
		float L_4 = Vector3_get_magnitude_m860342598((&___b1), /*hidden argument*/NULL);
		double L_5 = __this->get_floatingPointError_15();
		bool L_6 = VectorComparerBase_1_AreVectorMagnitudeEqual_m290049576(__this, L_3, L_4, L_5, /*hidden argument*/VectorComparerBase_1_AreVectorMagnitudeEqual_m290049576_MethodInfo_var);
		return L_6;
	}

IL_0034:
	{
		float L_7 = Vector3_get_magnitude_m860342598((&___a0), /*hidden argument*/NULL);
		float L_8 = Vector3_get_magnitude_m860342598((&___b1), /*hidden argument*/NULL);
		double L_9 = __this->get_floatingPointError_15();
		bool L_10 = VectorComparerBase_1_AreVectorMagnitudeEqual_m290049576(__this, L_7, L_8, L_9, /*hidden argument*/VectorComparerBase_1_AreVectorMagnitudeEqual_m290049576_MethodInfo_var);
		return (bool)((((int32_t)L_10) == ((int32_t)0))? 1 : 0);
	}

IL_0052:
	{
		Exception_t1927440687 * L_11 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m3886110570(L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}
}
// System.Void UnityTest.Vector4Comparer::.ctor()
extern const MethodInfo* VectorComparerBase_1__ctor_m2827238570_MethodInfo_var;
extern const uint32_t Vector4Comparer__ctor_m2279082948_MetadataUsageId;
extern "C"  void Vector4Comparer__ctor_m2279082948 (Vector4Comparer_t1534646382 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4Comparer__ctor_m2279082948_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VectorComparerBase_1__ctor_m2827238570(__this, /*hidden argument*/VectorComparerBase_1__ctor_m2827238570_MethodInfo_var);
		return;
	}
}
// System.Boolean UnityTest.Vector4Comparer::Compare(UnityEngine.Vector4,UnityEngine.Vector4)
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern const MethodInfo* VectorComparerBase_1_AreVectorMagnitudeEqual_m1121930541_MethodInfo_var;
extern const uint32_t Vector4Comparer_Compare_m737275931_MetadataUsageId;
extern "C"  bool Vector4Comparer_Compare_m737275931 (Vector4Comparer_t1534646382 * __this, Vector4_t2243707581  ___a0, Vector4_t2243707581  ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4Comparer_Compare_m737275931_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_compareType_14();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0034;
		}
	}
	{
		goto IL_0052;
	}

IL_0019:
	{
		float L_3 = Vector4_get_magnitude_m4049434951((&___a0), /*hidden argument*/NULL);
		float L_4 = Vector4_get_magnitude_m4049434951((&___b1), /*hidden argument*/NULL);
		double L_5 = __this->get_floatingPointError_15();
		bool L_6 = VectorComparerBase_1_AreVectorMagnitudeEqual_m1121930541(__this, L_3, L_4, L_5, /*hidden argument*/VectorComparerBase_1_AreVectorMagnitudeEqual_m1121930541_MethodInfo_var);
		return L_6;
	}

IL_0034:
	{
		float L_7 = Vector4_get_magnitude_m4049434951((&___a0), /*hidden argument*/NULL);
		float L_8 = Vector4_get_magnitude_m4049434951((&___b1), /*hidden argument*/NULL);
		double L_9 = __this->get_floatingPointError_15();
		bool L_10 = VectorComparerBase_1_AreVectorMagnitudeEqual_m1121930541(__this, L_7, L_8, L_9, /*hidden argument*/VectorComparerBase_1_AreVectorMagnitudeEqual_m1121930541_MethodInfo_var);
		return (bool)((((int32_t)L_10) == ((int32_t)0))? 1 : 0);
	}

IL_0052:
	{
		Exception_t1927440687 * L_11 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m3886110570(L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}
}
// System.Int32 UnityTest.Vector4Comparer::GetDepthOfSearch()
extern "C"  int32_t Vector4Comparer_GetDepthOfSearch_m3851589052 (Vector4Comparer_t1534646382 * __this, const MethodInfo* method)
{
	{
		return 3;
	}
}
// System.Void User::.ctor()
extern "C"  void User__ctor_m2333596438 (User_t719925459 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void User::.ctor(System.String,System.String)
extern "C"  void User__ctor_m3286703584 (User_t719925459 * __this, String_t* ___username0, String_t* ___email1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___username0;
		__this->set_username_0(L_0);
		String_t* L_1 = ___email1;
		__this->set_email_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
