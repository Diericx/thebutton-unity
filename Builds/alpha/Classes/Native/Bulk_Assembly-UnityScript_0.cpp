﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// AI
struct AI_t2476083018;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// System.Object
struct Il2CppObject;
// AutoFire
struct AutoFire_t2657208557;
// PerFrameRaycast
struct PerFrameRaycast_t2437905999;
// SimpleBullet
struct SimpleBullet_t2114900010;
// Health
struct Health_t2683907638;
// Boundary
struct Boundary_t1794889402;
// conveyorBelt
struct conveyorBelt_t3030136244;
// DestroyObject
struct DestroyObject_t260685093;
// DisableOutsideRadius
struct DisableOutsideRadius_t3052300191;
// UnityEngine.SphereCollider
struct SphereCollider_t1662511355;
// EffectSequencer
struct EffectSequencer_t194314474;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// ExplosionPart
struct ExplosionPart_t2473625634;
// EffectSequencer/$InstantiateDelayed$78
struct U24InstantiateDelayedU2478_t1298213366;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds>
struct IEnumerator_1_t1315025894;
// EffectSequencer/$InstantiateDelayed$78/$
struct U24_t814366785;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// EffectSequencer/$Start$69
struct U24StartU2469_t1610517444;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// EffectSequencer/$Start$69/$
struct U24_t2409507311;
// UnityEngine.ParticleEmitter
struct ParticleEmitter_t4099167268;
// FanRotate
struct FanRotate_t1574437886;
// FootstepHandler
struct FootstepHandler_t2514543022;
// UnityEngine.Collision
struct Collision_t2876846408;
// FreeMovementMotor
struct FreeMovementMotor_t1633549708;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// GlowPlane
struct GlowPlane_t2106059055;
// UnityEngine.Renderer
struct Renderer_t257310565;
// GlowPlaneAngleFade
struct GlowPlaneAngleFade_t239948010;
// Health/$Regenerate$91
struct U24RegenerateU2491_t1949600641;
// Health/$Regenerate$91/$
struct U24_t3039949752;
// HealthFlash
struct HealthFlash_t1223120616;
// Joystick
struct Joystick_t549888914;
// UnityEngine.GUITexture
struct GUITexture_t1909122990;
// LaserScope
struct LaserScope_t1872580701;
// LaserScope/$ChoseNewAnimationTargetCoroutine$83
struct U24ChoseNewAnimationTargetCoroutineU2483_t330810268;
// LaserScope/$ChoseNewAnimationTargetCoroutine$83/$
struct U24_t1966044057;
// MechAnimation
struct MechAnimation_t783079135;
// UnityEngine.Animation
struct Animation_t2068071072;
// MechAnimationTest
struct MechAnimationTest_t372588337;
// MoveAnimation
struct MoveAnimation_t3061523661;
// MovementMotor
struct MovementMotor_t1612021398;
// MuzzleFlashAnimate
struct MuzzleFlashAnimate_t122292636;
// ObjectCache
struct ObjectCache_t960934699;
// PatrolPoint
struct PatrolPoint_t2056099796;
// PlayerAnimation
struct PlayerAnimation_t4286826603;
// PlayerMoveController
struct PlayerMoveController_t2811056080;
// UnityEngine.Camera
struct Camera_t189460977;
// PlaySound
struct PlaySound_t4257033973;
// ReceiverItem
struct ReceiverItem_t169526838;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// ReceiverItem/$SendWithDelay$86
struct U24SendWithDelayU2486_t3482389000;
// ReceiverItem/$SendWithDelay$86/$
struct U24_t4172685971;
// SelfIlluminationBlink
struct SelfIlluminationBlink_t1392230251;
// SignalSender
struct SignalSender_t1204926691;
// SpawnAtCheckpoint
struct SpawnAtCheckpoint_t58124256;
// Spawner
struct Spawner_t534830648;
// SpawnObject
struct SpawnObject_t1174160328;
// SpiderAnimation
struct SpiderAnimation_t75583237;
// SpiderAnimationTest
struct SpiderAnimationTest_t3642224283;
// SpiderAttackMoveController
struct SpiderAttackMoveController_t1979379996;
// SelfIlluminationBlink[]
struct SelfIlluminationBlinkU5BU5D_t4009522826;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// SpiderReturnMoveController
struct SpiderReturnMoveController_t1990791070;
// TriggerOnMouseOrJoystick
struct TriggerOnMouseOrJoystick_t2646678581;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DUnityScript_AI2476083018.h"
#include "AssemblyU2DUnityScript_AI2476083018MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics634932869MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320MethodDeclarations.h"
#include "AssemblyU2DUnityScript_AutoFire2657208557.h"
#include "AssemblyU2DUnityScript_AutoFire2657208557MethodDeclarations.h"
#include "AssemblyU2DUnityScript_PerFrameRaycast2437905999.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random1170710517MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Spawner534830648MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DUnityScript_SimpleBullet2114900010.h"
#include "AssemblyU2DUnityScript_Health2683907638.h"
#include "AssemblyU2DUnityScript_PerFrameRaycast2437905999MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Health2683907638MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "UnityEngine_UnityEngine_ForceMode1856518252.h"
#include "AssemblyU2DUnityScript_Boundary1794889402.h"
#include "AssemblyU2DUnityScript_Boundary1794889402MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DUnityScript_conveyorBelt3030136244.h"
#include "AssemblyU2DUnityScript_conveyorBelt3030136244MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material193706927MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "AssemblyU2DUnityScript_DestroyObject260685093.h"
#include "AssemblyU2DUnityScript_DestroyObject260685093MethodDeclarations.h"
#include "AssemblyU2DUnityScript_DisableOutsideRadius3052300191.h"
#include "AssemblyU2DUnityScript_DisableOutsideRadius3052300191MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SphereCollider1662511355MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SphereCollider1662511355.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "AssemblyU2DUnityScript_EffectSequencer194314474.h"
#include "AssemblyU2DUnityScript_EffectSequencer194314474MethodDeclarations.h"
#include "AssemblyU2DUnityScript_EffectSequencer_U24StartU241610517444MethodDeclarations.h"
#include "AssemblyU2DUnityScript_EffectSequencer_U24StartU241610517444.h"
#include "AssemblyU2DUnityScript_ExplosionPart2473625634.h"
#include "AssemblyU2DUnityScript_EffectSequencer_U24Instanti1298213366MethodDeclarations.h"
#include "AssemblyU2DUnityScript_EffectSequencer_U24Instanti1298213366.h"
#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen4259040017MethodDeclarations.h"
#include "AssemblyU2DUnityScript_EffectSequencer_U24Instantia814366785MethodDeclarations.h"
#include "AssemblyU2DUnityScript_EffectSequencer_U24Instantia814366785.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen300505933MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen300505933.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen3108987245MethodDeclarations.h"
#include "AssemblyU2DUnityScript_EffectSequencer_U24StartU242409507311MethodDeclarations.h"
#include "AssemblyU2DUnityScript_EffectSequencer_U24StartU242409507311.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen3445420457MethodDeclarations.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices1910041954MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleEmitter4099167268MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen3445420457.h"
#include "Assembly-UnityScript_ArrayTypes.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "UnityEngine_UnityEngine_ParticleEmitter4099167268.h"
#include "AssemblyU2DUnityScript_ExplosionPart2473625634MethodDeclarations.h"
#include "AssemblyU2DUnityScript_FanRotate1574437886.h"
#include "AssemblyU2DUnityScript_FanRotate1574437886MethodDeclarations.h"
#include "AssemblyU2DUnityScript_FootstepHandler2514543022.h"
#include "AssemblyU2DUnityScript_FootstepHandler2514543022MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "UnityEngine_UnityEngine_Collision2876846408MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider3497673348MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PhysicMaterial578636151.h"
#include "AssemblyU2DUnityScript_FootType3548411356.h"
#include "AssemblyU2DUnityScript_FootType3548411356MethodDeclarations.h"
#include "AssemblyU2DUnityScript_FreeMovementMotor1633549708.h"
#include "AssemblyU2DUnityScript_FreeMovementMotor1633549708MethodDeclarations.h"
#include "AssemblyU2DUnityScript_MovementMotor1612021398MethodDeclarations.h"
#include "AssemblyU2DUnityScript_MovementMotor1612021398.h"
#include "AssemblyU2DUnityScript_GlowPlane2106059055.h"
#include "AssemblyU2DUnityScript_GlowPlane2106059055MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Renderer257310565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_Gizmos2256232573MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "AssemblyU2DUnityScript_GlowPlaneAngleFade239948010.h"
#include "AssemblyU2DUnityScript_GlowPlaneAngleFade239948010MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "UnityEngine_UnityEngine_Bounds3033363703MethodDeclarations.h"
#include "AssemblyU2DUnityScript_SignalSender1204926691.h"
#include "AssemblyU2DUnityScript_SignalSender1204926691MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Health_U24RegenerateU24911949600641MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Health_U24RegenerateU24911949600641.h"
#include "AssemblyU2DUnityScript_Health_U24RegenerateU2491_U3039949752MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Health_U24RegenerateU2491_U3039949752.h"
#include "AssemblyU2DUnityScript_HealthFlash1223120616.h"
#include "AssemblyU2DUnityScript_HealthFlash1223120616MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Joystick549888914.h"
#include "AssemblyU2DUnityScript_Joystick549888914MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUITexture1909122990MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUITexture1909122990.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIElement3381083099MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch407273883.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Touch407273883MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchPhase2458120420.h"
#include "AssemblyU2DUnityScript_LaserScope1872580701.h"
#include "AssemblyU2DUnityScript_LaserScope1872580701MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LineRenderer849157671.h"
#include "AssemblyU2DUnityScript_LaserScope_U24ChoseNewAnimat330810268MethodDeclarations.h"
#include "AssemblyU2DUnityScript_LaserScope_U24ChoseNewAnimat330810268.h"
#include "UnityEngine_UnityEngine_LineRenderer849157671MethodDeclarations.h"
#include "AssemblyU2DUnityScript_LaserScope_U24ChoseNewAnima1966044057MethodDeclarations.h"
#include "AssemblyU2DUnityScript_LaserScope_U24ChoseNewAnima1966044057.h"
#include "AssemblyU2DUnityScript_MechAnimation783079135.h"
#include "AssemblyU2DUnityScript_MechAnimation783079135MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animation2068071072MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationState1303741697MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animation2068071072.h"
#include "UnityEngine_UnityEngine_AnimationClip3510324950.h"
#include "UnityEngine_UnityEngine_AnimationState1303741697.h"
#include "AssemblyU2DUnityScript_MechAnimationTest372588337.h"
#include "AssemblyU2DUnityScript_MechAnimationTest372588337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayout2579273657MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI4082743951MethodDeclarations.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayoutOption4183744904.h"
#include "AssemblyU2DUnityScript_MoveAnimation3061523661.h"
#include "AssemblyU2DUnityScript_MoveAnimation3061523661MethodDeclarations.h"
#include "AssemblyU2DUnityScript_PlayerAnimation4286826603MethodDeclarations.h"
#include "AssemblyU2DUnityScript_MuzzleFlashAnimate122292636.h"
#include "AssemblyU2DUnityScript_MuzzleFlashAnimate122292636MethodDeclarations.h"
#include "AssemblyU2DUnityScript_ObjectCache960934699.h"
#include "AssemblyU2DUnityScript_ObjectCache960934699MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "AssemblyU2DUnityScript_PatrolPoint2056099796.h"
#include "AssemblyU2DUnityScript_PatrolPoint2056099796MethodDeclarations.h"
#include "AssemblyU2DUnityScript_PlayerAnimation4286826603.h"
#include "UnityEngine_UnityEngine_AnimationBlendMode3402232318.h"
#include "AssemblyU2DUnityScript_PlayerMoveController2811056080.h"
#include "AssemblyU2DUnityScript_PlayerMoveController2811056080MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Plane3727654732.h"
#include "UnityEngine_UnityEngine_Plane3727654732MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "UnityEngine_UnityEngine_Ray2469606224MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Cursor873194084MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "AssemblyU2DUnityScript_PlaySound4257033973.h"
#include "AssemblyU2DUnityScript_PlaySound4257033973MethodDeclarations.h"
#include "AssemblyU2DUnityScript_ReceiverItem169526838.h"
#include "AssemblyU2DUnityScript_ReceiverItem169526838MethodDeclarations.h"
#include "AssemblyU2DUnityScript_ReceiverItem_U24SendWithDel3482389000MethodDeclarations.h"
#include "AssemblyU2DUnityScript_ReceiverItem_U24SendWithDel3482389000.h"
#include "AssemblyU2DUnityScript_ReceiverItem_U24SendWithDel4172685971MethodDeclarations.h"
#include "AssemblyU2DUnityScript_ReceiverItem_U24SendWithDel4172685971.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "AssemblyU2DUnityScript_SelfIlluminationBlink1392230251.h"
#include "AssemblyU2DUnityScript_SelfIlluminationBlink1392230251MethodDeclarations.h"
#include "UnityScript_Lang_UnityScript_Lang_Extensions2406697300MethodDeclarations.h"
#include "AssemblyU2DUnityScript_SimpleBullet2114900010MethodDeclarations.h"
#include "AssemblyU2DUnityScript_SpawnAtCheckpoint58124256.h"
#include "AssemblyU2DUnityScript_SpawnAtCheckpoint58124256MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Spawner534830648.h"
#include "mscorlib_System_Collections_Hashtable909839986MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "AssemblyU2DUnityScript_SpawnObject1174160328.h"
#include "AssemblyU2DUnityScript_SpawnObject1174160328MethodDeclarations.h"
#include "AssemblyU2DUnityScript_SpiderAnimation75583237.h"
#include "AssemblyU2DUnityScript_SpiderAnimation75583237MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayMode1184682879.h"
#include "AssemblyU2DUnityScript_SpiderAnimationTest3642224283.h"
#include "AssemblyU2DUnityScript_SpiderAnimationTest3642224283MethodDeclarations.h"
#include "AssemblyU2DUnityScript_SpiderAttackMoveController1979379996.h"
#include "AssemblyU2DUnityScript_SpiderAttackMoveController1979379996MethodDeclarations.h"
#include "AssemblyU2DUnityScript_SpiderReturnMoveController1990791070.h"
#include "AssemblyU2DUnityScript_SpiderReturnMoveController1990791070MethodDeclarations.h"
#include "AssemblyU2DUnityScript_TriggerOnMouseOrJoystick2646678581.h"
#include "AssemblyU2DUnityScript_TriggerOnMouseOrJoystick2646678581MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
#define Component_GetComponent_TisAudioSource_t1135106623_m1132397112(__this, method) ((  AudioSource_t1135106623 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<PerFrameRaycast>()
#define Component_GetComponent_TisPerFrameRaycast_t2437905999_m1230835351(__this, method) ((  PerFrameRaycast_t2437905999 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<SimpleBullet>()
#define GameObject_GetComponent_TisSimpleBullet_t2114900010_m3080042914(__this, method) ((  SimpleBullet_t2114900010 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Health>()
#define Component_GetComponent_TisHealth_t2683907638_m697466234(__this, method) ((  Health_t2683907638 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SphereCollider>()
#define Component_GetComponent_TisSphereCollider_t1662511355_m471828246(__this, method) ((  SphereCollider_t1662511355 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m2929390107_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Vector3_t2243707580  p1, Quaternion_t4030073918  p2, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m2929390107(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m2929390107_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1756533147_m1577880517(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m2929390107_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.ParticleEmitter>()
#define GameObject_GetComponent_TisParticleEmitter_t4099167268_m2707952219(__this, method) ((  ParticleEmitter_t4099167268 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t4233889191_m1908970916(__this, method) ((  Rigidbody_t4233889191 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t257310565_m692268576(__this, method) ((  Renderer_t257310565 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider>()
#define Component_GetComponent_TisCollider_t3497673348_m319016787(__this, method) ((  Collider_t3497673348 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUITexture>()
#define Component_GetComponent_TisGUITexture_t1909122990_m368557725(__this, method) ((  GUITexture_t1909122990 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t257310565_m2926323388(__this, method) ((  Renderer_t257310565 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animation>()
#define Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, method) ((  Animation_t2068071072 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m447919519_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m447919519(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t1756533147_m3664764861(__this /* static, unused */, p0, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<Joystick>()
#define GameObject_GetComponent_TisJoystick_t549888914_m2668756518(__this, method) ((  Joystick_t549888914 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.GUITexture>()
#define GameObject_GetComponent_TisGUITexture_t1909122990_m2982615765(__this, method) ((  GUITexture_t1909122990 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m2461586036_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m2461586036(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m2461586036_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<AI>()
#define Component_GetComponentInChildren_TisAI_t2476083018_m1309876514(__this, method) ((  AI_t2476083018 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m2461586036_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponentsInChildren_TisIl2CppObject_m126455769_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m126455769(__this, method) ((  ObjectU5BU5D_t3614634134* (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m126455769_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<SelfIlluminationBlink>()
#define Component_GetComponentsInChildren_TisSelfIlluminationBlink_t1392230251_m1406606728(__this, method) ((  SelfIlluminationBlinkU5BU5D_t4009522826* (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m126455769_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AI::.ctor()
extern "C"  void AI__ctor_m1174688884 (AI_t2476083018 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_insideInterestArea_7((bool)1);
		return;
	}
}
// System.Void AI::Awake()
extern Il2CppCodeGenString* _stringLiteral1875862075;
extern const uint32_t AI_Awake_m1198661355_MetadataUsageId;
extern "C"  void AI_Awake_m1198661355 (AI_t2476083018 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AI_Awake_m1198661355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		__this->set_character_5(L_0);
		GameObject_t1756533147 * L_1 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = GameObject_get_transform_m909382139(L_1, /*hidden argument*/NULL);
		__this->set_player_6(L_2);
		return;
	}
}
// System.Void AI::OnEnable()
extern "C"  void AI_OnEnable_m4264155440 (AI_t2476083018 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour_t1158329972 * L_0 = __this->get_behaviourOnLostTrack_4();
		NullCheck(L_0);
		Behaviour_set_enabled_m1796096907(L_0, (bool)1, /*hidden argument*/NULL);
		MonoBehaviour_t1158329972 * L_1 = __this->get_behaviourOnSpotted_2();
		NullCheck(L_1);
		Behaviour_set_enabled_m1796096907(L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AI::OnTriggerEnter(UnityEngine.Collider)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t AI_OnTriggerEnter_m1092363448_MetadataUsageId;
extern "C"  void AI_OnTriggerEnter_m1092363448 (AI_t2476083018 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AI_OnTriggerEnter_m1092363448_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider_t3497673348 * L_0 = ___other0;
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = __this->get_player_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean AI::CanSeePlayer() */, __this);
		if (!L_4)
		{
			goto IL_0027;
		}
	}
	{
		VirtActionInvoker0::Invoke(9 /* System.Void AI::OnSpotted() */, __this);
	}

IL_0027:
	{
		return;
	}
}
// System.Void AI::OnEnterInterestArea()
extern "C"  void AI_OnEnterInterestArea_m3424342666 (AI_t2476083018 * __this, const MethodInfo* method)
{
	{
		__this->set_insideInterestArea_7((bool)1);
		return;
	}
}
// System.Void AI::OnExitInterestArea()
extern "C"  void AI_OnExitInterestArea_m2582640360 (AI_t2476083018 * __this, const MethodInfo* method)
{
	{
		__this->set_insideInterestArea_7((bool)0);
		VirtActionInvoker0::Invoke(10 /* System.Void AI::OnLostTrack() */, __this);
		return;
	}
}
// System.Void AI::OnSpotted()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAudioSource_t1135106623_m1132397112_MethodInfo_var;
extern const uint32_t AI_OnSpotted_m486737262_MetadataUsageId;
extern "C"  void AI_OnSpotted_m486737262 (AI_t2476083018 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AI_OnSpotted_m486737262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_insideInterestArea_7();
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		goto IL_0074;
	}

IL_0010:
	{
		MonoBehaviour_t1158329972 * L_1 = __this->get_behaviourOnSpotted_2();
		NullCheck(L_1);
		bool L_2 = Behaviour_get_enabled_m4079055610(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0074;
		}
	}
	{
		MonoBehaviour_t1158329972 * L_3 = __this->get_behaviourOnSpotted_2();
		NullCheck(L_3);
		Behaviour_set_enabled_m1796096907(L_3, (bool)1, /*hidden argument*/NULL);
		MonoBehaviour_t1158329972 * L_4 = __this->get_behaviourOnLostTrack_4();
		NullCheck(L_4);
		Behaviour_set_enabled_m1796096907(L_4, (bool)0, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_5 = Component_GetComponent_TisAudioSource_t1135106623_m1132397112(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m1132397112_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0074;
		}
	}
	{
		AudioClip_t1932558630 * L_7 = __this->get_soundOnSpotted_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0074;
		}
	}
	{
		AudioSource_t1135106623 * L_9 = Component_GetComponent_TisAudioSource_t1135106623_m1132397112(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m1132397112_MethodInfo_var);
		AudioClip_t1932558630 * L_10 = __this->get_soundOnSpotted_3();
		NullCheck(L_9);
		AudioSource_set_clip_m738814682(L_9, L_10, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_11 = Component_GetComponent_TisAudioSource_t1135106623_m1132397112(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m1132397112_MethodInfo_var);
		NullCheck(L_11);
		AudioSource_Play_m353744792(L_11, /*hidden argument*/NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Void AI::OnLostTrack()
extern "C"  void AI_OnLostTrack_m1813139578 (AI_t2476083018 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour_t1158329972 * L_0 = __this->get_behaviourOnLostTrack_4();
		NullCheck(L_0);
		bool L_1 = Behaviour_get_enabled_m4079055610(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0028;
		}
	}
	{
		MonoBehaviour_t1158329972 * L_2 = __this->get_behaviourOnLostTrack_4();
		NullCheck(L_2);
		Behaviour_set_enabled_m1796096907(L_2, (bool)1, /*hidden argument*/NULL);
		MonoBehaviour_t1158329972 * L_3 = __this->get_behaviourOnSpotted_2();
		NullCheck(L_3);
		Behaviour_set_enabled_m1796096907(L_3, (bool)0, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean AI::CanSeePlayer()
extern Il2CppClass* RaycastHit_t87180320_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t AI_CanSeePlayer_m2261913846_MetadataUsageId;
extern "C"  bool AI_CanSeePlayer_m2261913846 (AI_t2476083018 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AI_CanSeePlayer_m2261913846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit_t87180320  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B4_0 = 0;
	{
		Transform_t3275118058 * L_0 = __this->get_player_6();
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = __this->get_character_5();
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Initobj (RaycastHit_t87180320_il2cpp_TypeInfo_var, (&V_1));
		Transform_t3275118058 * L_5 = __this->get_character_5();
		NullCheck(L_5);
		Vector3_t2243707580  L_6 = Transform_get_position_m1104419803(L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = V_0;
		float L_8 = Vector3_get_magnitude_m860342598((&V_0), /*hidden argument*/NULL);
		Physics_Raycast_m2994111303(NULL /*static, unused*/, L_6, L_7, (&V_1), L_8, /*hidden argument*/NULL);
		Collider_t3497673348 * L_9 = RaycastHit_get_collider_m301198172((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0072;
		}
	}
	{
		Collider_t3497673348 * L_11 = RaycastHit_get_collider_m301198172((&V_1), /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(L_11, /*hidden argument*/NULL);
		Transform_t3275118058 * L_13 = __this->get_player_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0072;
		}
	}
	{
		G_B4_0 = 1;
		goto IL_0073;
	}

IL_0072:
	{
		G_B4_0 = 0;
	}

IL_0073:
	{
		return (bool)G_B4_0;
	}
}
// System.Void AI::Main()
extern "C"  void AI_Main_m3706889401 (AI_t2476083018 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void AutoFire::.ctor()
extern "C"  void AutoFire__ctor_m3835423877 (AutoFire_t2657208557 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_frequency_4((((float)((float)((int32_t)10)))));
		__this->set_coneAngle_5((1.5f));
		__this->set_damagePerSecond_7((20.0f));
		__this->set_forcePerSecond_8((20.0f));
		__this->set_hitSoundVolume_9((0.5f));
		__this->set_lastFireTime_11((((float)((float)(-1)))));
		return;
	}
}
// System.Void AutoFire::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisPerFrameRaycast_t2437905999_m1230835351_MethodInfo_var;
extern const uint32_t AutoFire_Awake_m3641296476_MetadataUsageId;
extern "C"  void AutoFire_Awake_m3641296476 (AutoFire_t2657208557 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AutoFire_Awake_m3641296476_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_muzzleFlashFront_10();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		PerFrameRaycast_t2437905999 * L_1 = Component_GetComponent_TisPerFrameRaycast_t2437905999_m1230835351(__this, /*hidden argument*/Component_GetComponent_TisPerFrameRaycast_t2437905999_m1230835351_MethodInfo_var);
		__this->set_raycast_12(L_1);
		Transform_t3275118058 * L_2 = __this->get_spawnPoint_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		__this->set_spawnPoint_3(L_4);
	}

IL_0035:
	{
		return;
	}
}
// System.Void AutoFire::Update()
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSimpleBullet_t2114900010_m3080042914_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisHealth_t2683907638_m697466234_MethodInfo_var;
extern const uint32_t AutoFire_Update_m607471546_MetadataUsageId;
extern "C"  void AutoFire_Update_m607471546 (AutoFire_t2657208557 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AutoFire_Update_m607471546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	GameObject_t1756533147 * V_1 = NULL;
	SimpleBullet_t2114900010 * V_2 = NULL;
	RaycastHit_t87180320  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Health_t2683907638 * V_4 = NULL;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		bool L_0 = __this->get_firing_6();
		if (!L_0)
		{
			goto IL_014f;
		}
	}
	{
		float L_1 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = __this->get_lastFireTime_11();
		float L_3 = __this->get_frequency_4();
		if ((((float)L_1) <= ((float)((float)((float)L_2+(float)((float)((float)(((float)((float)1)))/(float)L_3)))))))
		{
			goto IL_014f;
		}
	}
	{
		float L_4 = __this->get_coneAngle_5();
		float L_5 = __this->get_coneAngle_5();
		float L_6 = Random_Range_m2884721203(NULL /*static, unused*/, ((-L_4)), L_5, /*hidden argument*/NULL);
		float L_7 = __this->get_coneAngle_5();
		float L_8 = __this->get_coneAngle_5();
		float L_9 = Random_Range_m2884721203(NULL /*static, unused*/, ((-L_7)), L_8, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_10 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, L_6, L_9, (((float)((float)0))), /*hidden argument*/NULL);
		V_0 = L_10;
		GameObject_t1756533147 * L_11 = __this->get_bulletPrefab_2();
		Transform_t3275118058 * L_12 = __this->get_spawnPoint_3();
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_position_m1104419803(L_12, /*hidden argument*/NULL);
		Transform_t3275118058 * L_14 = __this->get_spawnPoint_3();
		NullCheck(L_14);
		Quaternion_t4030073918  L_15 = Transform_get_rotation_m1033555130(L_14, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_16 = V_0;
		Quaternion_t4030073918  L_17 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_18 = Spawner_Spawn_m4269243264(NULL /*static, unused*/, L_11, L_13, L_17, /*hidden argument*/NULL);
		V_1 = ((GameObject_t1756533147 *)IsInstSealed(L_18, GameObject_t1756533147_il2cpp_TypeInfo_var));
		GameObject_t1756533147 * L_19 = V_1;
		NullCheck(L_19);
		SimpleBullet_t2114900010 * L_20 = GameObject_GetComponent_TisSimpleBullet_t2114900010_m3080042914(L_19, /*hidden argument*/GameObject_GetComponent_TisSimpleBullet_t2114900010_m3080042914_MethodInfo_var);
		V_2 = L_20;
		float L_21 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastFireTime_11(L_21);
		PerFrameRaycast_t2437905999 * L_22 = __this->get_raycast_12();
		NullCheck(L_22);
		RaycastHit_t87180320  L_23 = VirtFuncInvoker0< RaycastHit_t87180320  >::Invoke(6 /* UnityEngine.RaycastHit PerFrameRaycast::GetHitInfo() */, L_22);
		V_3 = L_23;
		Transform_t3275118058 * L_24 = RaycastHit_get_transform_m3290290036((&V_3), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_25 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_0143;
		}
	}
	{
		Transform_t3275118058 * L_26 = RaycastHit_get_transform_m3290290036((&V_3), /*hidden argument*/NULL);
		NullCheck(L_26);
		Health_t2683907638 * L_27 = Component_GetComponent_TisHealth_t2683907638_m697466234(L_26, /*hidden argument*/Component_GetComponent_TisHealth_t2683907638_m697466234_MethodInfo_var);
		V_4 = L_27;
		Health_t2683907638 * L_28 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_29 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00eb;
		}
	}
	{
		Health_t2683907638 * L_30 = V_4;
		float L_31 = __this->get_damagePerSecond_7();
		float L_32 = __this->get_frequency_4();
		Transform_t3275118058 * L_33 = __this->get_spawnPoint_3();
		NullCheck(L_33);
		Vector3_t2243707580  L_34 = Transform_get_forward_m1833488937(L_33, /*hidden argument*/NULL);
		Vector3_t2243707580  L_35 = Vector3_op_UnaryNegation_m3383802608(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		NullCheck(L_30);
		VirtActionInvoker2< float, Vector3_t2243707580  >::Invoke(5 /* System.Void Health::OnDamage(System.Single,UnityEngine.Vector3) */, L_30, ((float)((float)L_31/(float)L_32)), L_35);
	}

IL_00eb:
	{
		Rigidbody_t4233889191 * L_36 = RaycastHit_get_rigidbody_m480380820((&V_3), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_37 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0131;
		}
	}
	{
		Transform_t3275118058 * L_38 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_38);
		Vector3_t2243707580  L_39 = Transform_get_forward_m1833488937(L_38, /*hidden argument*/NULL);
		float L_40 = __this->get_forcePerSecond_8();
		float L_41 = __this->get_frequency_4();
		Vector3_t2243707580  L_42 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_39, ((float)((float)L_40/(float)L_41)), /*hidden argument*/NULL);
		V_5 = L_42;
		Rigidbody_t4233889191 * L_43 = RaycastHit_get_rigidbody_m480380820((&V_3), /*hidden argument*/NULL);
		Vector3_t2243707580  L_44 = V_5;
		Vector3_t2243707580  L_45 = RaycastHit_get_point_m326143462((&V_3), /*hidden argument*/NULL);
		NullCheck(L_43);
		Rigidbody_AddForceAtPosition_m4129134921(L_43, L_44, L_45, 1, /*hidden argument*/NULL);
	}

IL_0131:
	{
		SimpleBullet_t2114900010 * L_46 = V_2;
		float L_47 = RaycastHit_get_distance_m1178709367((&V_3), /*hidden argument*/NULL);
		NullCheck(L_46);
		L_46->set_dist_4(L_47);
		goto IL_014f;
	}

IL_0143:
	{
		SimpleBullet_t2114900010 * L_48 = V_2;
		NullCheck(L_48);
		L_48->set_dist_4((((float)((float)((int32_t)1000)))));
	}

IL_014f:
	{
		return;
	}
}
// System.Void AutoFire::OnStartFire()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAudioSource_t1135106623_m1132397112_MethodInfo_var;
extern const uint32_t AutoFire_OnStartFire_m2496133628_MetadataUsageId;
extern "C"  void AutoFire_OnStartFire_m2496133628 (AutoFire_t2657208557 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AutoFire_OnStartFire_m2496133628_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Time_get_timeScale_m3151482970(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)(((float)((float)0)))))))
		{
			goto IL_0011;
		}
	}
	{
		goto IL_003f;
	}

IL_0011:
	{
		__this->set_firing_6((bool)1);
		GameObject_t1756533147 * L_1 = __this->get_muzzleFlashFront_10();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_2 = Component_GetComponent_TisAudioSource_t1135106623_m1132397112(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m1132397112_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		AudioSource_t1135106623 * L_4 = Component_GetComponent_TisAudioSource_t1135106623_m1132397112(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m1132397112_MethodInfo_var);
		NullCheck(L_4);
		AudioSource_Play_m353744792(L_4, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void AutoFire::OnStopFire()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAudioSource_t1135106623_m1132397112_MethodInfo_var;
extern const uint32_t AutoFire_OnStopFire_m3525826486_MetadataUsageId;
extern "C"  void AutoFire_OnStopFire_m3525826486 (AutoFire_t2657208557 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AutoFire_OnStopFire_m3525826486_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_firing_6((bool)0);
		GameObject_t1756533147 * L_0 = __this->get_muzzleFlashFront_10();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_1 = Component_GetComponent_TisAudioSource_t1135106623_m1132397112(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m1132397112_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		AudioSource_t1135106623 * L_3 = Component_GetComponent_TisAudioSource_t1135106623_m1132397112(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m1132397112_MethodInfo_var);
		NullCheck(L_3);
		AudioSource_Stop_m3452679614(L_3, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void AutoFire::Main()
extern "C"  void AutoFire_Main_m2270067470 (AutoFire_t2657208557 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Boundary::.ctor()
extern "C"  void Boundary__ctor_m1977119940 (Boundary_t1794889402 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_min_0(L_0);
		Vector2_t2243707579  L_1 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_max_1(L_1);
		return;
	}
}
// System.Void conveyorBelt::.ctor()
extern "C"  void conveyorBelt__ctor_m3553230578 (conveyorBelt_t3030136244 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_scrollSpeed_2((0.1f));
		return;
	}
}
// System.Void conveyorBelt::Start()
extern "C"  void conveyorBelt_Start_m1595979090 (conveyorBelt_t3030136244 * __this, const MethodInfo* method)
{
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void conveyorBelt::OnBecameVisible()
extern "C"  void conveyorBelt_OnBecameVisible_m3434916266 (conveyorBelt_t3030136244 * __this, const MethodInfo* method)
{
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void conveyorBelt::OnBecameInvisible()
extern "C"  void conveyorBelt_OnBecameInvisible_m3309334001 (conveyorBelt_t3030136244 * __this, const MethodInfo* method)
{
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void conveyorBelt::Update()
extern Il2CppCodeGenString* _stringLiteral4026354833;
extern Il2CppCodeGenString* _stringLiteral836285905;
extern const uint32_t conveyorBelt_Update_m4177102773_MetadataUsageId;
extern "C"  void conveyorBelt_Update_m4177102773 (conveyorBelt_t3030136244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (conveyorBelt_Update_m4177102773_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_scrollSpeed_2();
		V_0 = (fmodf(((float)((float)L_0*(float)L_1)), (1.0f)));
		Material_t193706927 * L_2 = __this->get_mat_3();
		float L_3 = V_0;
		Vector2_t2243707579  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3067419446(&L_4, (((float)((float)0))), ((-L_3)), /*hidden argument*/NULL);
		NullCheck(L_2);
		Material_SetTextureOffset_m3084369360(L_2, _stringLiteral4026354833, L_4, /*hidden argument*/NULL);
		Material_t193706927 * L_5 = __this->get_mat_3();
		float L_6 = V_0;
		Vector2_t2243707579  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector2__ctor_m3067419446(&L_7, (((float)((float)0))), ((-L_6)), /*hidden argument*/NULL);
		NullCheck(L_5);
		Material_SetTextureOffset_m3084369360(L_5, _stringLiteral836285905, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void conveyorBelt::Main()
extern "C"  void conveyorBelt_Main_m4071695115 (conveyorBelt_t3030136244 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DestroyObject::.ctor()
extern "C"  void DestroyObject__ctor_m3787626429 (DestroyObject_t260685093 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DestroyObject::OnSignal()
extern "C"  void DestroyObject_OnSignal_m1290035890 (DestroyObject_t260685093 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_objectToDestroy_2();
		Spawner_Destroy_m590425740(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DestroyObject::Main()
extern "C"  void DestroyObject_Main_m1532369982 (DestroyObject_t260685093 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DisableOutsideRadius::.ctor()
extern "C"  void DisableOutsideRadius__ctor_m3970157123 (DisableOutsideRadius_t3052300191 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DisableOutsideRadius::Awake()
extern const MethodInfo* Component_GetComponent_TisSphereCollider_t1662511355_m471828246_MethodInfo_var;
extern const uint32_t DisableOutsideRadius_Awake_m2297585978_MetadataUsageId;
extern "C"  void DisableOutsideRadius_Awake_m2297585978 (DisableOutsideRadius_t3052300191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DisableOutsideRadius_Awake_m2297585978_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Transform_get_parent_m147407266(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(L_1, /*hidden argument*/NULL);
		__this->set_target_2(L_2);
		SphereCollider_t1662511355 * L_3 = Component_GetComponent_TisSphereCollider_t1662511355_m471828246(__this, /*hidden argument*/Component_GetComponent_TisSphereCollider_t1662511355_m471828246_MethodInfo_var);
		__this->set_sphereCollider_3(L_3);
		SphereCollider_t1662511355 * L_4 = __this->get_sphereCollider_3();
		NullCheck(L_4);
		float L_5 = SphereCollider_get_radius_m271100661(L_4, /*hidden argument*/NULL);
		__this->set_activeRadius_4(L_5);
		VirtActionInvoker0::Invoke(7 /* System.Void DisableOutsideRadius::Disable() */, __this);
		return;
	}
}
// System.Void DisableOutsideRadius::OnTriggerEnter(UnityEngine.Collider)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1875862075;
extern const uint32_t DisableOutsideRadius_OnTriggerEnter_m3363212839_MetadataUsageId;
extern "C"  void DisableOutsideRadius_OnTriggerEnter_m3363212839 (DisableOutsideRadius_t3052300191 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DisableOutsideRadius_OnTriggerEnter_m3363212839_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider_t3497673348 * L_0 = ___other0;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m357168014(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, _stringLiteral1875862075, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003b;
		}
	}
	{
		GameObject_t1756533147 * L_3 = __this->get_target_2();
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Transform_get_parent_m147407266(L_4, /*hidden argument*/NULL);
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003b;
		}
	}
	{
		VirtActionInvoker0::Invoke(8 /* System.Void DisableOutsideRadius::Enable() */, __this);
	}

IL_003b:
	{
		return;
	}
}
// System.Void DisableOutsideRadius::OnTriggerExit(UnityEngine.Collider)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1875862075;
extern const uint32_t DisableOutsideRadius_OnTriggerExit_m3560045963_MetadataUsageId;
extern "C"  void DisableOutsideRadius_OnTriggerExit_m3560045963 (DisableOutsideRadius_t3052300191 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DisableOutsideRadius_OnTriggerExit_m3560045963_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider_t3497673348 * L_0 = ___other0;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m357168014(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, _stringLiteral1875862075, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		VirtActionInvoker0::Invoke(7 /* System.Void DisableOutsideRadius::Disable() */, __this);
	}

IL_001b:
	{
		return;
	}
}
// System.Void DisableOutsideRadius::Disable()
extern "C"  void DisableOutsideRadius_Disable_m3496426739 (DisableOutsideRadius_t3052300191 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_target_2();
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = GameObject_get_transform_m909382139(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Transform_get_parent_m147407266(L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_parent_m3281327839(L_0, L_3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_target_2();
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_parent_m3281327839(L_5, L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = __this->get_target_2();
		NullCheck(L_7);
		GameObject_SetActive_m2887581199(L_7, (bool)0, /*hidden argument*/NULL);
		SphereCollider_t1662511355 * L_8 = __this->get_sphereCollider_3();
		float L_9 = __this->get_activeRadius_4();
		NullCheck(L_8);
		SphereCollider_set_radius_m369924668(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DisableOutsideRadius::Enable()
extern "C"  void DisableOutsideRadius_Enable_m3142127226 (DisableOutsideRadius_t3052300191 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_target_2();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Transform_get_parent_m147407266(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_parent_m3281327839(L_1, L_3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_target_2();
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = __this->get_target_2();
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = GameObject_get_transform_m909382139(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_parent_m3281327839(L_5, L_7, /*hidden argument*/NULL);
		SphereCollider_t1662511355 * L_8 = __this->get_sphereCollider_3();
		float L_9 = __this->get_activeRadius_4();
		NullCheck(L_8);
		SphereCollider_set_radius_m369924668(L_8, ((float)((float)L_9*(float)(1.1f))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void DisableOutsideRadius::Main()
extern "C"  void DisableOutsideRadius_Main_m1238274504 (DisableOutsideRadius_t3052300191 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void EffectSequencer::.ctor()
extern "C"  void EffectSequencer__ctor_m1532800462 (EffectSequencer_t194314474 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator EffectSequencer::Start()
extern Il2CppClass* U24StartU2469_t1610517444_il2cpp_TypeInfo_var;
extern const uint32_t EffectSequencer_Start_m2608506064_MetadataUsageId;
extern "C"  Il2CppObject * EffectSequencer_Start_m2608506064 (EffectSequencer_t194314474 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EffectSequencer_Start_m2608506064_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U24StartU2469_t1610517444 * L_0 = (U24StartU2469_t1610517444 *)il2cpp_codegen_object_new(U24StartU2469_t1610517444_il2cpp_TypeInfo_var);
		U24StartU2469__ctor_m1290453516(L_0, __this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject* L_1 = U24StartU2469_GetEnumerator_m3845317762(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.IEnumerator EffectSequencer::InstantiateDelayed(ExplosionPart)
extern Il2CppClass* U24InstantiateDelayedU2478_t1298213366_il2cpp_TypeInfo_var;
extern const uint32_t EffectSequencer_InstantiateDelayed_m2442955740_MetadataUsageId;
extern "C"  Il2CppObject * EffectSequencer_InstantiateDelayed_m2442955740 (EffectSequencer_t194314474 * __this, ExplosionPart_t2473625634 * ___go0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EffectSequencer_InstantiateDelayed_m2442955740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExplosionPart_t2473625634 * L_0 = ___go0;
		U24InstantiateDelayedU2478_t1298213366 * L_1 = (U24InstantiateDelayedU2478_t1298213366 *)il2cpp_codegen_object_new(U24InstantiateDelayedU2478_t1298213366_il2cpp_TypeInfo_var);
		U24InstantiateDelayedU2478__ctor_m1575325494(L_1, L_0, __this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Il2CppObject* L_2 = U24InstantiateDelayedU2478_GetEnumerator_m2327833452(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void EffectSequencer::Main()
extern "C"  void EffectSequencer_Main_m2535506055 (EffectSequencer_t194314474 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void EffectSequencer/$InstantiateDelayed$78::.ctor(ExplosionPart,EffectSequencer)
extern const MethodInfo* GenericGenerator_1__ctor_m1542724045_MethodInfo_var;
extern const uint32_t U24InstantiateDelayedU2478__ctor_m1575325494_MetadataUsageId;
extern "C"  void U24InstantiateDelayedU2478__ctor_m1575325494 (U24InstantiateDelayedU2478_t1298213366 * __this, ExplosionPart_t2473625634 * ___go0, EffectSequencer_t194314474 * ___self_1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24InstantiateDelayedU2478__ctor_m1575325494_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GenericGenerator_1__ctor_m1542724045(__this, /*hidden argument*/GenericGenerator_1__ctor_m1542724045_MethodInfo_var);
		ExplosionPart_t2473625634 * L_0 = ___go0;
		__this->set_U24goU2481_0(L_0);
		EffectSequencer_t194314474 * L_1 = ___self_1;
		__this->set_U24self_U2482_1(L_1);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> EffectSequencer/$InstantiateDelayed$78::GetEnumerator()
extern Il2CppClass* U24_t814366785_il2cpp_TypeInfo_var;
extern const uint32_t U24InstantiateDelayedU2478_GetEnumerator_m2327833452_MetadataUsageId;
extern "C"  Il2CppObject* U24InstantiateDelayedU2478_GetEnumerator_m2327833452 (U24InstantiateDelayedU2478_t1298213366 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24InstantiateDelayedU2478_GetEnumerator_m2327833452_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExplosionPart_t2473625634 * L_0 = __this->get_U24goU2481_0();
		EffectSequencer_t194314474 * L_1 = __this->get_U24self_U2482_1();
		U24_t814366785 * L_2 = (U24_t814366785 *)il2cpp_codegen_object_new(U24_t814366785_il2cpp_TypeInfo_var);
		U24__ctor_m3713097129(L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void EffectSequencer/$InstantiateDelayed$78/$::.ctor(ExplosionPart,EffectSequencer)
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m3451731617_MethodInfo_var;
extern const uint32_t U24__ctor_m3713097129_MetadataUsageId;
extern "C"  void U24__ctor_m3713097129 (U24_t814366785 * __this, ExplosionPart_t2473625634 * ___go0, EffectSequencer_t194314474 * ___self_1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m3713097129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m3451731617(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m3451731617_MethodInfo_var);
		ExplosionPart_t2473625634 * L_0 = ___go0;
		__this->set_U24goU2479_2(L_0);
		EffectSequencer_t194314474 * L_1 = ___self_1;
		__this->set_U24self_U2480_3(L_1);
		return;
	}
}
// System.Boolean EffectSequencer/$InstantiateDelayed$78/$::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_Yield_m1415833518_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m1577880517_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m3507348778_MethodInfo_var;
extern const uint32_t U24_MoveNext_m637962735_MetadataUsageId;
extern "C"  bool U24_MoveNext_m637962735 (U24_t814366785 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m637962735_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t300505933 *)__this)->get__state_1();
		if (L_0 == 0)
		{
			goto IL_0017;
		}
		if (L_0 == 1)
		{
			goto IL_0086;
		}
		if (L_0 == 2)
		{
			goto IL_0033;
		}
	}

IL_0017:
	{
		ExplosionPart_t2473625634 * L_1 = __this->get_U24goU2479_2();
		NullCheck(L_1);
		float L_2 = L_1->get_delay_1();
		WaitForSeconds_t3839502067 * L_3 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_3, L_2, /*hidden argument*/NULL);
		bool L_4 = GenericGeneratorEnumerator_1_Yield_m1415833518(__this, 2, L_3, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m1415833518_MethodInfo_var);
		G_B4_0 = ((int32_t)(L_4));
		goto IL_0087;
	}

IL_0033:
	{
		ExplosionPart_t2473625634 * L_5 = __this->get_U24goU2479_2();
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = L_5->get_gameObject_0();
		EffectSequencer_t194314474 * L_7 = __this->get_U24self_U2480_3();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t2243707580  L_9 = Transform_get_position_m1104419803(L_8, /*hidden argument*/NULL);
		Vector3_t2243707580  L_10 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		ExplosionPart_t2473625634 * L_11 = __this->get_U24goU2479_2();
		NullCheck(L_11);
		float L_12 = L_11->get_yOffset_3();
		Vector3_t2243707580  L_13 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_9, L_13, /*hidden argument*/NULL);
		EffectSequencer_t194314474 * L_15 = __this->get_U24self_U2480_3();
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = Component_get_transform_m2697483695(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Quaternion_t4030073918  L_17 = Transform_get_rotation_m1033555130(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m1577880517(NULL /*static, unused*/, L_6, L_14, L_17, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m1577880517_MethodInfo_var);
		GenericGeneratorEnumerator_1_YieldDefault_m3507348778(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m3507348778_MethodInfo_var);
	}

IL_0086:
	{
		G_B4_0 = 0;
	}

IL_0087:
	{
		return (bool)G_B4_0;
	}
}
// System.Void EffectSequencer/$Start$69::.ctor(EffectSequencer)
extern const MethodInfo* GenericGenerator_1__ctor_m409073663_MethodInfo_var;
extern const uint32_t U24StartU2469__ctor_m1290453516_MetadataUsageId;
extern "C"  void U24StartU2469__ctor_m1290453516 (U24StartU2469_t1610517444 * __this, EffectSequencer_t194314474 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24StartU2469__ctor_m1290453516_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GenericGenerator_1__ctor_m409073663(__this, /*hidden argument*/GenericGenerator_1__ctor_m409073663_MethodInfo_var);
		EffectSequencer_t194314474 * L_0 = ___self_0;
		__this->set_U24self_U2477_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Object> EffectSequencer/$Start$69::GetEnumerator()
extern Il2CppClass* U24_t2409507311_il2cpp_TypeInfo_var;
extern const uint32_t U24StartU2469_GetEnumerator_m3845317762_MetadataUsageId;
extern "C"  Il2CppObject* U24StartU2469_GetEnumerator_m3845317762 (U24StartU2469_t1610517444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24StartU2469_GetEnumerator_m3845317762_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EffectSequencer_t194314474 * L_0 = __this->get_U24self_U2477_0();
		U24_t2409507311 * L_1 = (U24_t2409507311 *)il2cpp_codegen_object_new(U24_t2409507311_il2cpp_TypeInfo_var);
		U24__ctor_m2054523649(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void EffectSequencer/$Start$69/$::.ctor(EffectSequencer)
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m509214543_MethodInfo_var;
extern const uint32_t U24__ctor_m2054523649_MetadataUsageId;
extern "C"  void U24__ctor_m2054523649 (U24_t2409507311 * __this, EffectSequencer_t194314474 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m2054523649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m509214543(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m509214543_MethodInfo_var);
		EffectSequencer_t194314474 * L_0 = ___self_0;
		__this->set_U24self_U2476_8(L_0);
		return;
	}
}
// System.Boolean EffectSequencer/$Start$69/$::MoveNext()
extern const Il2CppType* ExplosionPart_t2473625634_0_0_0_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* ExplosionPart_t2473625634_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimeServices_t1910041954_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisParticleEmitter_t4099167268_m2707952219_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisAudioSource_t1135106623_m1132397112_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m799132706_MethodInfo_var;
extern const uint32_t U24_MoveNext_m504862089_MetadataUsageId;
extern "C"  bool U24_MoveNext_m504862089 (U24_t2409507311 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m504862089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * G_B4_0 = NULL;
	U24_t2409507311 * G_B4_1 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	U24_t2409507311 * G_B3_1 = NULL;
	Il2CppObject * G_B10_0 = NULL;
	U24_t2409507311 * G_B10_1 = NULL;
	Il2CppObject * G_B9_0 = NULL;
	U24_t2409507311 * G_B9_1 = NULL;
	Il2CppObject * G_B16_0 = NULL;
	U24_t2409507311 * G_B16_1 = NULL;
	Il2CppObject * G_B15_0 = NULL;
	U24_t2409507311 * G_B15_1 = NULL;
	int32_t G_B31_0 = 0;
	Il2CppObject * G_B26_0 = NULL;
	U24_t2409507311 * G_B26_1 = NULL;
	Il2CppObject * G_B25_0 = NULL;
	U24_t2409507311 * G_B25_1 = NULL;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t3445420457 *)__this)->get__state_1();
		if (L_0 == 0)
		{
			goto IL_0017;
		}
		if (L_0 == 1)
		{
			goto IL_03bb;
		}
		if (L_0 == 2)
		{
			goto IL_02d3;
		}
	}

IL_0017:
	{
		__this->set_U24goU2470_2((ExplosionPart_t2473625634 *)NULL);
		__this->set_U24maxTimeU2471_3((((float)((float)0))));
		EffectSequencer_t194314474 * L_1 = __this->get_U24self_U2476_8();
		NullCheck(L_1);
		ExplosionPartU5BU5D_t10352919* L_2 = L_1->get_ambientEmitters_2();
		NullCheck((Il2CppArray *)(Il2CppArray *)L_2);
		Il2CppObject * L_3 = Array_GetEnumerator_m2284404958((Il2CppArray *)(Il2CppArray *)L_2, /*hidden argument*/NULL);
		__this->set_U24U24iteratorU2465U2472_4(L_3);
		goto IL_00da;
	}

IL_0041:
	{
		Il2CppObject * L_4 = __this->get_U24U24iteratorU2465U2472_4();
		NullCheck(L_4);
		Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_4);
		Il2CppObject * L_6 = L_5;
		G_B3_0 = L_6;
		G_B3_1 = __this;
		if (((ExplosionPart_t2473625634 *)IsInstClass(L_6, ExplosionPart_t2473625634_il2cpp_TypeInfo_var)))
		{
			G_B4_0 = L_6;
			G_B4_1 = __this;
			goto IL_0067;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ExplosionPart_t2473625634_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = RuntimeServices_Coerce_m43784504(NULL /*static, unused*/, G_B3_0, L_7, /*hidden argument*/NULL);
		G_B4_0 = L_8;
		G_B4_1 = G_B3_1;
	}

IL_0067:
	{
		NullCheck(G_B4_1);
		G_B4_1->set_U24goU2470_2(((ExplosionPart_t2473625634 *)CastclassClass(G_B4_0, ExplosionPart_t2473625634_il2cpp_TypeInfo_var)));
		EffectSequencer_t194314474 * L_9 = __this->get_U24self_U2476_8();
		EffectSequencer_t194314474 * L_10 = __this->get_U24self_U2476_8();
		ExplosionPart_t2473625634 * L_11 = __this->get_U24goU2470_2();
		NullCheck(L_10);
		Il2CppObject * L_12 = VirtFuncInvoker1< Il2CppObject *, ExplosionPart_t2473625634 * >::Invoke(5 /* System.Collections.IEnumerator EffectSequencer::InstantiateDelayed(ExplosionPart) */, L_10, L_11);
		NullCheck(L_9);
		MonoBehaviour_StartCoroutine_m2470621050(L_9, L_12, /*hidden argument*/NULL);
		ExplosionPart_t2473625634 * L_13 = __this->get_U24goU2470_2();
		NullCheck(L_13);
		GameObject_t1756533147 * L_14 = L_13->get_gameObject_0();
		NullCheck(L_14);
		ParticleEmitter_t4099167268 * L_15 = GameObject_GetComponent_TisParticleEmitter_t4099167268_m2707952219(L_14, /*hidden argument*/GameObject_GetComponent_TisParticleEmitter_t4099167268_m2707952219_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00da;
		}
	}
	{
		float L_17 = __this->get_U24maxTimeU2471_3();
		ExplosionPart_t2473625634 * L_18 = __this->get_U24goU2470_2();
		NullCheck(L_18);
		float L_19 = L_18->get_delay_1();
		ExplosionPart_t2473625634 * L_20 = __this->get_U24goU2470_2();
		NullCheck(L_20);
		GameObject_t1756533147 * L_21 = L_20->get_gameObject_0();
		NullCheck(L_21);
		ParticleEmitter_t4099167268 * L_22 = GameObject_GetComponent_TisParticleEmitter_t4099167268_m2707952219(L_21, /*hidden argument*/GameObject_GetComponent_TisParticleEmitter_t4099167268_m2707952219_MethodInfo_var);
		NullCheck(L_22);
		float L_23 = ParticleEmitter_get_maxEnergy_m4065204998(L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_24 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_17, ((float)((float)L_19+(float)L_23)), /*hidden argument*/NULL);
		__this->set_U24maxTimeU2471_3(L_24);
	}

IL_00da:
	{
		Il2CppObject * L_25 = __this->get_U24U24iteratorU2465U2472_4();
		NullCheck(L_25);
		bool L_26 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_25);
		if (L_26)
		{
			goto IL_0041;
		}
	}
	{
		EffectSequencer_t194314474 * L_27 = __this->get_U24self_U2476_8();
		NullCheck(L_27);
		ExplosionPartU5BU5D_t10352919* L_28 = L_27->get_explosionEmitters_3();
		NullCheck((Il2CppArray *)(Il2CppArray *)L_28);
		Il2CppObject * L_29 = Array_GetEnumerator_m2284404958((Il2CppArray *)(Il2CppArray *)L_28, /*hidden argument*/NULL);
		__this->set_U24U24iteratorU2466U2473_5(L_29);
		goto IL_019e;
	}

IL_0105:
	{
		Il2CppObject * L_30 = __this->get_U24U24iteratorU2466U2473_5();
		NullCheck(L_30);
		Il2CppObject * L_31 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_30);
		Il2CppObject * L_32 = L_31;
		G_B9_0 = L_32;
		G_B9_1 = __this;
		if (((ExplosionPart_t2473625634 *)IsInstClass(L_32, ExplosionPart_t2473625634_il2cpp_TypeInfo_var)))
		{
			G_B10_0 = L_32;
			G_B10_1 = __this;
			goto IL_012b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_33 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ExplosionPart_t2473625634_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		Il2CppObject * L_34 = RuntimeServices_Coerce_m43784504(NULL /*static, unused*/, G_B9_0, L_33, /*hidden argument*/NULL);
		G_B10_0 = L_34;
		G_B10_1 = G_B9_1;
	}

IL_012b:
	{
		NullCheck(G_B10_1);
		G_B10_1->set_U24goU2470_2(((ExplosionPart_t2473625634 *)CastclassClass(G_B10_0, ExplosionPart_t2473625634_il2cpp_TypeInfo_var)));
		EffectSequencer_t194314474 * L_35 = __this->get_U24self_U2476_8();
		EffectSequencer_t194314474 * L_36 = __this->get_U24self_U2476_8();
		ExplosionPart_t2473625634 * L_37 = __this->get_U24goU2470_2();
		NullCheck(L_36);
		Il2CppObject * L_38 = VirtFuncInvoker1< Il2CppObject *, ExplosionPart_t2473625634 * >::Invoke(5 /* System.Collections.IEnumerator EffectSequencer::InstantiateDelayed(ExplosionPart) */, L_36, L_37);
		NullCheck(L_35);
		MonoBehaviour_StartCoroutine_m2470621050(L_35, L_38, /*hidden argument*/NULL);
		ExplosionPart_t2473625634 * L_39 = __this->get_U24goU2470_2();
		NullCheck(L_39);
		GameObject_t1756533147 * L_40 = L_39->get_gameObject_0();
		NullCheck(L_40);
		ParticleEmitter_t4099167268 * L_41 = GameObject_GetComponent_TisParticleEmitter_t4099167268_m2707952219(L_40, /*hidden argument*/GameObject_GetComponent_TisParticleEmitter_t4099167268_m2707952219_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_42 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_019e;
		}
	}
	{
		float L_43 = __this->get_U24maxTimeU2471_3();
		ExplosionPart_t2473625634 * L_44 = __this->get_U24goU2470_2();
		NullCheck(L_44);
		float L_45 = L_44->get_delay_1();
		ExplosionPart_t2473625634 * L_46 = __this->get_U24goU2470_2();
		NullCheck(L_46);
		GameObject_t1756533147 * L_47 = L_46->get_gameObject_0();
		NullCheck(L_47);
		ParticleEmitter_t4099167268 * L_48 = GameObject_GetComponent_TisParticleEmitter_t4099167268_m2707952219(L_47, /*hidden argument*/GameObject_GetComponent_TisParticleEmitter_t4099167268_m2707952219_MethodInfo_var);
		NullCheck(L_48);
		float L_49 = ParticleEmitter_get_maxEnergy_m4065204998(L_48, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_50 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_43, ((float)((float)L_45+(float)L_49)), /*hidden argument*/NULL);
		__this->set_U24maxTimeU2471_3(L_50);
	}

IL_019e:
	{
		Il2CppObject * L_51 = __this->get_U24U24iteratorU2466U2473_5();
		NullCheck(L_51);
		bool L_52 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_51);
		if (L_52)
		{
			goto IL_0105;
		}
	}
	{
		EffectSequencer_t194314474 * L_53 = __this->get_U24self_U2476_8();
		NullCheck(L_53);
		ExplosionPartU5BU5D_t10352919* L_54 = L_53->get_smokeEmitters_4();
		NullCheck((Il2CppArray *)(Il2CppArray *)L_54);
		Il2CppObject * L_55 = Array_GetEnumerator_m2284404958((Il2CppArray *)(Il2CppArray *)L_54, /*hidden argument*/NULL);
		__this->set_U24U24iteratorU2467U2474_6(L_55);
		goto IL_0262;
	}

IL_01c9:
	{
		Il2CppObject * L_56 = __this->get_U24U24iteratorU2467U2474_6();
		NullCheck(L_56);
		Il2CppObject * L_57 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_56);
		Il2CppObject * L_58 = L_57;
		G_B15_0 = L_58;
		G_B15_1 = __this;
		if (((ExplosionPart_t2473625634 *)IsInstClass(L_58, ExplosionPart_t2473625634_il2cpp_TypeInfo_var)))
		{
			G_B16_0 = L_58;
			G_B16_1 = __this;
			goto IL_01ef;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_59 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ExplosionPart_t2473625634_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		Il2CppObject * L_60 = RuntimeServices_Coerce_m43784504(NULL /*static, unused*/, G_B15_0, L_59, /*hidden argument*/NULL);
		G_B16_0 = L_60;
		G_B16_1 = G_B15_1;
	}

IL_01ef:
	{
		NullCheck(G_B16_1);
		G_B16_1->set_U24goU2470_2(((ExplosionPart_t2473625634 *)CastclassClass(G_B16_0, ExplosionPart_t2473625634_il2cpp_TypeInfo_var)));
		EffectSequencer_t194314474 * L_61 = __this->get_U24self_U2476_8();
		EffectSequencer_t194314474 * L_62 = __this->get_U24self_U2476_8();
		ExplosionPart_t2473625634 * L_63 = __this->get_U24goU2470_2();
		NullCheck(L_62);
		Il2CppObject * L_64 = VirtFuncInvoker1< Il2CppObject *, ExplosionPart_t2473625634 * >::Invoke(5 /* System.Collections.IEnumerator EffectSequencer::InstantiateDelayed(ExplosionPart) */, L_62, L_63);
		NullCheck(L_61);
		MonoBehaviour_StartCoroutine_m2470621050(L_61, L_64, /*hidden argument*/NULL);
		ExplosionPart_t2473625634 * L_65 = __this->get_U24goU2470_2();
		NullCheck(L_65);
		GameObject_t1756533147 * L_66 = L_65->get_gameObject_0();
		NullCheck(L_66);
		ParticleEmitter_t4099167268 * L_67 = GameObject_GetComponent_TisParticleEmitter_t4099167268_m2707952219(L_66, /*hidden argument*/GameObject_GetComponent_TisParticleEmitter_t4099167268_m2707952219_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_68 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_0262;
		}
	}
	{
		float L_69 = __this->get_U24maxTimeU2471_3();
		ExplosionPart_t2473625634 * L_70 = __this->get_U24goU2470_2();
		NullCheck(L_70);
		float L_71 = L_70->get_delay_1();
		ExplosionPart_t2473625634 * L_72 = __this->get_U24goU2470_2();
		NullCheck(L_72);
		GameObject_t1756533147 * L_73 = L_72->get_gameObject_0();
		NullCheck(L_73);
		ParticleEmitter_t4099167268 * L_74 = GameObject_GetComponent_TisParticleEmitter_t4099167268_m2707952219(L_73, /*hidden argument*/GameObject_GetComponent_TisParticleEmitter_t4099167268_m2707952219_MethodInfo_var);
		NullCheck(L_74);
		float L_75 = ParticleEmitter_get_maxEnergy_m4065204998(L_74, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_76 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_69, ((float)((float)L_71+(float)L_75)), /*hidden argument*/NULL);
		__this->set_U24maxTimeU2471_3(L_76);
	}

IL_0262:
	{
		Il2CppObject * L_77 = __this->get_U24U24iteratorU2467U2474_6();
		NullCheck(L_77);
		bool L_78 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_77);
		if (L_78)
		{
			goto IL_01c9;
		}
	}
	{
		EffectSequencer_t194314474 * L_79 = __this->get_U24self_U2476_8();
		NullCheck(L_79);
		AudioSource_t1135106623 * L_80 = Component_GetComponent_TisAudioSource_t1135106623_m1132397112(L_79, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m1132397112_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_81 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_80, /*hidden argument*/NULL);
		if (!L_81)
		{
			goto IL_02c7;
		}
	}
	{
		EffectSequencer_t194314474 * L_82 = __this->get_U24self_U2476_8();
		NullCheck(L_82);
		AudioSource_t1135106623 * L_83 = Component_GetComponent_TisAudioSource_t1135106623_m1132397112(L_82, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m1132397112_MethodInfo_var);
		NullCheck(L_83);
		AudioClip_t1932558630 * L_84 = AudioSource_get_clip_m2127996365(L_83, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_85 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_84, /*hidden argument*/NULL);
		if (!L_85)
		{
			goto IL_02c7;
		}
	}
	{
		float L_86 = __this->get_U24maxTimeU2471_3();
		EffectSequencer_t194314474 * L_87 = __this->get_U24self_U2476_8();
		NullCheck(L_87);
		AudioSource_t1135106623 * L_88 = Component_GetComponent_TisAudioSource_t1135106623_m1132397112(L_87, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m1132397112_MethodInfo_var);
		NullCheck(L_88);
		AudioClip_t1932558630 * L_89 = AudioSource_get_clip_m2127996365(L_88, /*hidden argument*/NULL);
		NullCheck(L_89);
		float L_90 = AudioClip_get_length_m3881628918(L_89, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_91 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_86, L_90, /*hidden argument*/NULL);
		__this->set_U24maxTimeU2471_3(L_91);
	}

IL_02c7:
	{
		bool L_92 = GenericGeneratorEnumerator_1_YieldDefault_m799132706(__this, 2, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m799132706_MethodInfo_var);
		G_B31_0 = ((int32_t)(L_92));
		goto IL_03bc;
	}

IL_02d3:
	{
		EffectSequencer_t194314474 * L_93 = __this->get_U24self_U2476_8();
		NullCheck(L_93);
		ExplosionPartU5BU5D_t10352919* L_94 = L_93->get_miscSpecialEffects_5();
		NullCheck((Il2CppArray *)(Il2CppArray *)L_94);
		Il2CppObject * L_95 = Array_GetEnumerator_m2284404958((Il2CppArray *)(Il2CppArray *)L_94, /*hidden argument*/NULL);
		__this->set_U24U24iteratorU2468U2475_7(L_95);
		goto IL_0387;
	}

IL_02ee:
	{
		Il2CppObject * L_96 = __this->get_U24U24iteratorU2468U2475_7();
		NullCheck(L_96);
		Il2CppObject * L_97 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_96);
		Il2CppObject * L_98 = L_97;
		G_B25_0 = L_98;
		G_B25_1 = __this;
		if (((ExplosionPart_t2473625634 *)IsInstClass(L_98, ExplosionPart_t2473625634_il2cpp_TypeInfo_var)))
		{
			G_B26_0 = L_98;
			G_B26_1 = __this;
			goto IL_0314;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_99 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ExplosionPart_t2473625634_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		Il2CppObject * L_100 = RuntimeServices_Coerce_m43784504(NULL /*static, unused*/, G_B25_0, L_99, /*hidden argument*/NULL);
		G_B26_0 = L_100;
		G_B26_1 = G_B25_1;
	}

IL_0314:
	{
		NullCheck(G_B26_1);
		G_B26_1->set_U24goU2470_2(((ExplosionPart_t2473625634 *)CastclassClass(G_B26_0, ExplosionPart_t2473625634_il2cpp_TypeInfo_var)));
		EffectSequencer_t194314474 * L_101 = __this->get_U24self_U2476_8();
		EffectSequencer_t194314474 * L_102 = __this->get_U24self_U2476_8();
		ExplosionPart_t2473625634 * L_103 = __this->get_U24goU2470_2();
		NullCheck(L_102);
		Il2CppObject * L_104 = VirtFuncInvoker1< Il2CppObject *, ExplosionPart_t2473625634 * >::Invoke(5 /* System.Collections.IEnumerator EffectSequencer::InstantiateDelayed(ExplosionPart) */, L_102, L_103);
		NullCheck(L_101);
		MonoBehaviour_StartCoroutine_m2470621050(L_101, L_104, /*hidden argument*/NULL);
		ExplosionPart_t2473625634 * L_105 = __this->get_U24goU2470_2();
		NullCheck(L_105);
		GameObject_t1756533147 * L_106 = L_105->get_gameObject_0();
		NullCheck(L_106);
		ParticleEmitter_t4099167268 * L_107 = GameObject_GetComponent_TisParticleEmitter_t4099167268_m2707952219(L_106, /*hidden argument*/GameObject_GetComponent_TisParticleEmitter_t4099167268_m2707952219_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_108 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_107, /*hidden argument*/NULL);
		if (!L_108)
		{
			goto IL_0387;
		}
	}
	{
		float L_109 = __this->get_U24maxTimeU2471_3();
		ExplosionPart_t2473625634 * L_110 = __this->get_U24goU2470_2();
		NullCheck(L_110);
		float L_111 = L_110->get_delay_1();
		ExplosionPart_t2473625634 * L_112 = __this->get_U24goU2470_2();
		NullCheck(L_112);
		GameObject_t1756533147 * L_113 = L_112->get_gameObject_0();
		NullCheck(L_113);
		ParticleEmitter_t4099167268 * L_114 = GameObject_GetComponent_TisParticleEmitter_t4099167268_m2707952219(L_113, /*hidden argument*/GameObject_GetComponent_TisParticleEmitter_t4099167268_m2707952219_MethodInfo_var);
		NullCheck(L_114);
		float L_115 = ParticleEmitter_get_maxEnergy_m4065204998(L_114, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_116 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_109, ((float)((float)L_111+(float)L_115)), /*hidden argument*/NULL);
		__this->set_U24maxTimeU2471_3(L_116);
	}

IL_0387:
	{
		Il2CppObject * L_117 = __this->get_U24U24iteratorU2468U2475_7();
		NullCheck(L_117);
		bool L_118 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_117);
		if (L_118)
		{
			goto IL_02ee;
		}
	}
	{
		EffectSequencer_t194314474 * L_119 = __this->get_U24self_U2476_8();
		NullCheck(L_119);
		GameObject_t1756533147 * L_120 = Component_get_gameObject_m3105766835(L_119, /*hidden argument*/NULL);
		float L_121 = __this->get_U24maxTimeU2471_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4279412553(NULL /*static, unused*/, L_120, ((float)((float)L_121+(float)(0.5f))), /*hidden argument*/NULL);
		GenericGeneratorEnumerator_1_YieldDefault_m799132706(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m799132706_MethodInfo_var);
	}

IL_03bb:
	{
		G_B31_0 = 0;
	}

IL_03bc:
	{
		return (bool)G_B31_0;
	}
}
// System.Void ExplosionPart::.ctor()
extern "C"  void ExplosionPart__ctor_m3297066906 (ExplosionPart_t2473625634 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FanRotate::.ctor()
extern "C"  void FanRotate__ctor_m691777854 (FanRotate_t1574437886 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FanRotate::Main()
extern "C"  void FanRotate_Main_m4202370641 (FanRotate_t1574437886 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FootstepHandler::.ctor()
extern "C"  void FootstepHandler__ctor_m3797750694 (FootstepHandler_t2514543022 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FootstepHandler::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void FootstepHandler_OnCollisionEnter_m2698422936 (FootstepHandler_t2514543022 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method)
{
	{
		Collision_t2876846408 * L_0 = ___collisionInfo0;
		NullCheck(L_0);
		Collider_t3497673348 * L_1 = Collision_get_collider_m3340328360(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		PhysicMaterial_t578636151 * L_2 = Collider_get_sharedMaterial_m2977820719(L_1, /*hidden argument*/NULL);
		__this->set_physicMaterial_4(L_2);
		return;
	}
}
// System.Void FootstepHandler::OnFootstep()
extern "C"  void FootstepHandler_OnFootstep_m4217666043 (FootstepHandler_t2514543022 * __this, const MethodInfo* method)
{
	AudioClip_t1932558630 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		AudioSource_t1135106623 * L_0 = __this->get_audioSource_2();
		NullCheck(L_0);
		bool L_1 = Behaviour_get_enabled_m4079055610(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		goto IL_0077;
	}

IL_0015:
	{
		V_0 = (AudioClip_t1932558630 *)NULL;
		int32_t L_2 = __this->get_footType_3();
		V_1 = L_2;
		int32_t L_3 = V_1;
		if ((!(((uint32_t)L_3) == ((uint32_t)0))))
		{
			goto IL_002a;
		}
	}
	{
		goto IL_0042;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0036;
		}
	}
	{
		goto IL_0042;
	}

IL_0036:
	{
		int32_t L_5 = V_1;
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_0042;
		}
	}
	{
		goto IL_0042;
	}

IL_0042:
	{
		AudioSource_t1135106623 * L_6 = __this->get_audioSource_2();
		float L_7 = Random_Range_m2884721203(NULL /*static, unused*/, (0.98f), (1.02f), /*hidden argument*/NULL);
		NullCheck(L_6);
		AudioSource_set_pitch_m3064416458(L_6, L_7, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_8 = __this->get_audioSource_2();
		AudioClip_t1932558630 * L_9 = V_0;
		float L_10 = Random_Range_m2884721203(NULL /*static, unused*/, (0.8f), (1.2f), /*hidden argument*/NULL);
		NullCheck(L_8);
		AudioSource_PlayOneShot_m4118899740(L_8, L_9, L_10, /*hidden argument*/NULL);
	}

IL_0077:
	{
		return;
	}
}
// System.Void FootstepHandler::Main()
extern "C"  void FootstepHandler_Main_m1639644865 (FootstepHandler_t2514543022 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FreeMovementMotor::.ctor()
extern "C"  void FreeMovementMotor__ctor_m3821870680 (FreeMovementMotor_t1633549708 * __this, const MethodInfo* method)
{
	{
		MovementMotor__ctor_m3335335718(__this, /*hidden argument*/NULL);
		__this->set_walkingSpeed_5((5.0f));
		__this->set_walkingSnappyness_6((((float)((float)((int32_t)50)))));
		__this->set_turningSmoothing_7((0.3f));
		return;
	}
}
// System.Void FreeMovementMotor::FixedUpdate()
extern const MethodInfo* Component_GetComponent_TisRigidbody_t4233889191_m1908970916_MethodInfo_var;
extern const uint32_t FreeMovementMotor_FixedUpdate_m2258332217_MetadataUsageId;
extern "C"  void FreeMovementMotor_FixedUpdate_m2258332217 (FreeMovementMotor_t1633549708 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FreeMovementMotor_FixedUpdate_m2258332217_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	{
		Vector3_t2243707580  L_0 = ((MovementMotor_t1612021398 *)__this)->get_movementDirection_2();
		float L_1 = __this->get_walkingSpeed_5();
		Vector3_t2243707580  L_2 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t2243707580  L_3 = V_0;
		Rigidbody_t4233889191 * L_4 = Component_GetComponent_TisRigidbody_t4233889191_m1908970916(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m1908970916_MethodInfo_var);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Rigidbody_get_velocity_m2022666970(L_4, /*hidden argument*/NULL);
		Vector3_t2243707580  L_6 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Rigidbody_t4233889191 * L_7 = Component_GetComponent_TisRigidbody_t4233889191_m1908970916(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m1908970916_MethodInfo_var);
		NullCheck(L_7);
		bool L_8 = Rigidbody_get_useGravity_m1380955568(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_003d;
		}
	}
	{
		(&V_1)->set_y_2((((float)((float)0))));
	}

IL_003d:
	{
		Rigidbody_t4233889191 * L_9 = Component_GetComponent_TisRigidbody_t4233889191_m1908970916(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m1908970916_MethodInfo_var);
		Vector3_t2243707580  L_10 = V_1;
		float L_11 = __this->get_walkingSnappyness_6();
		Vector3_t2243707580  L_12 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		Rigidbody_AddForce_m3219459786(L_9, L_12, 5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_13 = ((MovementMotor_t1612021398 *)__this)->get_facingDirection_4();
		V_2 = L_13;
		Vector3_t2243707580  L_14 = V_2;
		Vector3_t2243707580  L_15 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_16 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0073;
		}
	}
	{
		Vector3_t2243707580  L_17 = ((MovementMotor_t1612021398 *)__this)->get_movementDirection_2();
		V_2 = L_17;
	}

IL_0073:
	{
		Vector3_t2243707580  L_18 = V_2;
		Vector3_t2243707580  L_19 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_20 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0098;
		}
	}
	{
		Rigidbody_t4233889191 * L_21 = Component_GetComponent_TisRigidbody_t4233889191_m1908970916(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m1908970916_MethodInfo_var);
		Vector3_t2243707580  L_22 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		Rigidbody_set_angularVelocity_m824394045(L_21, L_22, /*hidden argument*/NULL);
		goto IL_00d0;
	}

IL_0098:
	{
		Transform_t3275118058 * L_23 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector3_t2243707580  L_24 = Transform_get_forward_m1833488937(L_23, /*hidden argument*/NULL);
		Vector3_t2243707580  L_25 = V_2;
		Vector3_t2243707580  L_26 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_27 = FreeMovementMotor_AngleAroundAxis_m2394052890(NULL /*static, unused*/, L_24, L_25, L_26, /*hidden argument*/NULL);
		V_3 = L_27;
		Rigidbody_t4233889191 * L_28 = Component_GetComponent_TisRigidbody_t4233889191_m1908970916(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m1908970916_MethodInfo_var);
		Vector3_t2243707580  L_29 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_30 = V_3;
		Vector3_t2243707580  L_31 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		float L_32 = __this->get_turningSmoothing_7();
		Vector3_t2243707580  L_33 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		NullCheck(L_28);
		Rigidbody_set_angularVelocity_m824394045(L_28, L_33, /*hidden argument*/NULL);
	}

IL_00d0:
	{
		return;
	}
}
// System.Single FreeMovementMotor::AngleAroundAxis(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float FreeMovementMotor_AngleAroundAxis_m2394052890 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___dirA0, Vector3_t2243707580  ___dirB1, Vector3_t2243707580  ___axis2, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float G_B2_0 = 0.0f;
	float G_B1_0 = 0.0f;
	int32_t G_B3_0 = 0;
	float G_B3_1 = 0.0f;
	{
		Vector3_t2243707580  L_0 = ___dirA0;
		Vector3_t2243707580  L_1 = ___dirA0;
		Vector3_t2243707580  L_2 = ___axis2;
		Vector3_t2243707580  L_3 = Vector3_Project_m1396027688(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		___dirA0 = L_4;
		Vector3_t2243707580  L_5 = ___dirB1;
		Vector3_t2243707580  L_6 = ___dirB1;
		Vector3_t2243707580  L_7 = ___axis2;
		Vector3_t2243707580  L_8 = Vector3_Project_m1396027688(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_5, L_8, /*hidden argument*/NULL);
		___dirB1 = L_9;
		Vector3_t2243707580  L_10 = ___dirA0;
		Vector3_t2243707580  L_11 = ___dirB1;
		float L_12 = Vector3_Angle_m2552334978(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		float L_13 = V_0;
		Vector3_t2243707580  L_14 = ___axis2;
		Vector3_t2243707580  L_15 = ___dirA0;
		Vector3_t2243707580  L_16 = ___dirB1;
		Vector3_t2243707580  L_17 = Vector3_Cross_m4149044051(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		float L_18 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_14, L_17, /*hidden argument*/NULL);
		G_B1_0 = L_13;
		if ((((float)L_18) >= ((float)(((float)((float)0))))))
		{
			G_B2_0 = L_13;
			goto IL_0049;
		}
	}
	{
		G_B3_0 = (-1);
		G_B3_1 = G_B1_0;
		goto IL_004a;
	}

IL_0049:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_004a:
	{
		return ((float)((float)G_B3_1*(float)(((float)((float)G_B3_0)))));
	}
}
// System.Void FreeMovementMotor::Main()
extern "C"  void FreeMovementMotor_Main_m2384879269 (FreeMovementMotor_t1633549708 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GlowPlane::.ctor()
extern "C"  void GlowPlane__ctor_m1453172583 (GlowPlane_t2106059055 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_minGlow_5((0.2f));
		__this->set_maxGlow_6((0.5f));
		Color_t2020392075  L_0 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_glowColor_7(L_0);
		return;
	}
}
// System.Void GlowPlane::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1875862075;
extern const uint32_t GlowPlane_Start_m1406937411_MetadataUsageId;
extern "C"  void GlowPlane_Start_m1406937411 (GlowPlane_t2106059055 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GlowPlane_Start_m1406937411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = __this->get_playerTransform_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		GameObject_t1756533147 * L_2 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139(L_2, /*hidden argument*/NULL);
		__this->set_playerTransform_2(L_3);
	}

IL_0025:
	{
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		__this->set_pos_3(L_5);
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_localScale_m3074381503(L_6, /*hidden argument*/NULL);
		__this->set_scale_4(L_7);
		Renderer_t257310565 * L_8 = Component_GetComponent_TisRenderer_t257310565_m692268576(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var);
		NullCheck(L_8);
		Material_t193706927 * L_9 = Renderer_get_material_m2553789785(L_8, /*hidden argument*/NULL);
		__this->set_mat_8(L_9);
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GlowPlane::OnDrawGizmos()
extern "C"  void GlowPlane_OnDrawGizmos_m3426450019 (GlowPlane_t2106059055 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Color_t2020392075  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Color_t2020392075  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Color_t2020392075  L_0 = __this->get_glowColor_7();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_1 = __this->get_maxGlow_6();
		float L_2 = ((float)((float)L_1*(float)(0.25f)));
		V_1 = L_2;
		Color_t2020392075  L_3 = Gizmos_get_color_m837177613(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t2020392075  L_4 = L_3;
		V_2 = L_4;
		float L_5 = V_1;
		float L_6 = L_5;
		V_3 = L_6;
		(&V_2)->set_a_3(L_6);
		float L_7 = V_3;
		Color_t2020392075  L_8 = V_2;
		Color_t2020392075  L_9 = L_8;
		V_4 = L_9;
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Color_t2020392075  L_10 = V_4;
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Matrix4x4_t2933234003  L_12 = Transform_get_localToWorldMatrix_m2868579006(L_11, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		Vector3_t2243707580  L_13 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2638739322(&L_14, (((float)((float)1))), (((float)((float)0))), (((float)((float)1))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_15 = Vector3_Scale_m1087116865(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, (5.0f), L_15, /*hidden argument*/NULL);
		V_0 = L_16;
		Vector3_t2243707580  L_17 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_18 = V_0;
		Gizmos_DrawCube_m452336961(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_19 = Matrix4x4_get_identity_m3039560904(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GlowPlane::OnDrawGizmosSelected()
extern "C"  void GlowPlane_OnDrawGizmosSelected_m1583840234 (GlowPlane_t2106059055 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Color_t2020392075  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Color_t2020392075  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Color_t2020392075  L_0 = __this->get_glowColor_7();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_1 = __this->get_maxGlow_6();
		float L_2 = L_1;
		V_1 = L_2;
		Color_t2020392075  L_3 = Gizmos_get_color_m837177613(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t2020392075  L_4 = L_3;
		V_2 = L_4;
		float L_5 = V_1;
		float L_6 = L_5;
		V_3 = L_6;
		(&V_2)->set_a_3(L_6);
		float L_7 = V_3;
		Color_t2020392075  L_8 = V_2;
		Color_t2020392075  L_9 = L_8;
		V_4 = L_9;
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Color_t2020392075  L_10 = V_4;
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Matrix4x4_t2933234003  L_12 = Transform_get_localToWorldMatrix_m2868579006(L_11, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		Vector3_t2243707580  L_13 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2638739322(&L_14, (((float)((float)1))), (((float)((float)0))), (((float)((float)1))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_15 = Vector3_Scale_m1087116865(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, (5.0f), L_15, /*hidden argument*/NULL);
		V_0 = L_16;
		Vector3_t2243707580  L_17 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_18 = V_0;
		Gizmos_DrawCube_m452336961(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_19 = Matrix4x4_get_identity_m3039560904(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_matrix_m1590313986(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GlowPlane::OnBecameVisible()
extern "C"  void GlowPlane_OnBecameVisible_m2178474545 (GlowPlane_t2106059055 * __this, const MethodInfo* method)
{
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GlowPlane::OnBecameInvisible()
extern "C"  void GlowPlane_OnBecameInvisible_m364750530 (GlowPlane_t2106059055 * __this, const MethodInfo* method)
{
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GlowPlane::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3855512561;
extern const uint32_t GlowPlane_Update_m609705766_MetadataUsageId;
extern "C"  void GlowPlane_Update_m609705766 (GlowPlane_t2106059055 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GlowPlane_Update_m609705766_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		Vector3_t2243707580  L_0 = __this->get_pos_3();
		Transform_t3275118058 * L_1 = __this->get_playerTransform_2();
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		(&V_0)->set_y_2((((float)((float)0))));
		float L_4 = Vector3_get_magnitude_m860342598((&V_0), /*hidden argument*/NULL);
		V_1 = L_4;
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_6 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = __this->get_minGlow_5();
		Vector3_t2243707580  L_8 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = __this->get_scale_4();
		float L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_11 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, ((float)((float)L_10*(float)(0.35f))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_12 = Vector3_Lerp_m2935648359(NULL /*static, unused*/, L_8, L_9, L_11, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_localScale_m2325460848(L_5, L_12, /*hidden argument*/NULL);
		Material_t193706927 * L_13 = __this->get_mat_8();
		Color_t2020392075  L_14 = __this->get_glowColor_7();
		float L_15 = V_1;
		float L_16 = __this->get_minGlow_5();
		float L_17 = __this->get_maxGlow_6();
		float L_18 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, ((float)((float)L_15*(float)(0.1f))), L_16, L_17, /*hidden argument*/NULL);
		Color_t2020392075  L_19 = Color_op_Multiply_m325555950(NULL /*static, unused*/, L_14, L_18, /*hidden argument*/NULL);
		NullCheck(L_13);
		Material_SetColor_m650857509(L_13, _stringLiteral3855512561, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GlowPlane::Main()
extern "C"  void GlowPlane_Main_m2806361502 (GlowPlane_t2106059055 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GlowPlaneAngleFade::.ctor()
extern "C"  void GlowPlaneAngleFade__ctor_m3584954580 (GlowPlaneAngleFade_t239948010 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		Color_t2020392075  L_0 = Color_get_grey_m1961362537(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_glowColor_3(L_0);
		__this->set_dot_4((0.5f));
		return;
	}
}
// System.Void GlowPlaneAngleFade::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t GlowPlaneAngleFade_Start_m2937330716_MetadataUsageId;
extern "C"  void GlowPlaneAngleFade_Start_m2937330716 (GlowPlaneAngleFade_t239948010 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GlowPlaneAngleFade_Start_m2937330716_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = __this->get_cameraTransform_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		Camera_t189460977 * L_2 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(L_2, /*hidden argument*/NULL);
		__this->set_cameraTransform_2(L_3);
	}

IL_0020:
	{
		return;
	}
}
// System.Void GlowPlaneAngleFade::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t GlowPlaneAngleFade_Update_m2499551425_MetadataUsageId;
extern "C"  void GlowPlaneAngleFade_Update_m2499551425 (GlowPlaneAngleFade_t239948010 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GlowPlaneAngleFade_Update_m2499551425_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = __this->get_cameraTransform_2();
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_forward_m1833488937(L_0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_up_m1603627763(L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector3_op_UnaryNegation_m3383802608(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		float L_5 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, ((float)((float)L_5-(float)(0.25f))), /*hidden argument*/NULL);
		__this->set_dot_4(((float)((float)(1.5f)*(float)L_6)));
		return;
	}
}
// System.Void GlowPlaneAngleFade::OnWillRenderObject()
extern const MethodInfo* Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3855512561;
extern const uint32_t GlowPlaneAngleFade_OnWillRenderObject_m3987060868_MetadataUsageId;
extern "C"  void GlowPlaneAngleFade_OnWillRenderObject_m3987060868 (GlowPlaneAngleFade_t239948010 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GlowPlaneAngleFade_OnWillRenderObject_m3987060868_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Renderer_t257310565 * L_0 = Component_GetComponent_TisRenderer_t257310565_m692268576(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var);
		NullCheck(L_0);
		Material_t193706927 * L_1 = Renderer_get_sharedMaterial_m155010392(L_0, /*hidden argument*/NULL);
		Color_t2020392075  L_2 = __this->get_glowColor_3();
		float L_3 = __this->get_dot_4();
		Color_t2020392075  L_4 = Color_op_Multiply_m325555950(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		Material_SetColor_m650857509(L_1, _stringLiteral3855512561, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GlowPlaneAngleFade::Main()
extern "C"  void GlowPlaneAngleFade_Main_m4071350831 (GlowPlaneAngleFade_t239948010 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Health::.ctor()
extern "C"  void Health__ctor_m2434983288 (Health_t2683907638 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_maxHealth_2((100.0f));
		__this->set_health_3((100.0f));
		__this->set_damageEffectMultiplier_9((1.0f));
		__this->set_damageEffectCentered_10((bool)1);
		__this->set_colliderRadiusHeuristic_17((1.0f));
		return;
	}
}
// System.Void Health::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCollider_t3497673348_m319016787_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m1577880517_MethodInfo_var;
extern const uint32_t Health_Awake_m4184625751_MetadataUsageId;
extern "C"  void Health_Awake_m4184625751 (Health_t2683907638 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Health_Awake_m4184625751_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Bounds_t3033363703  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Bounds_t3033363703  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Bounds_t3033363703  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_0 = __this->get_damagePrefab_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00df;
		}
	}
	{
		Transform_t3275118058 * L_2 = __this->get_damageEffectTransform_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0034;
		}
	}
	{
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		__this->set_damageEffectTransform_8(L_4);
	}

IL_0034:
	{
		GameObject_t1756533147 * L_5 = __this->get_damagePrefab_7();
		Vector3_t2243707580  L_6 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_7 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Spawner_Spawn_m4269243264(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		GameObject_t1756533147 * L_9 = V_0;
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = GameObject_get_transform_m909382139(L_9, /*hidden argument*/NULL);
		Transform_t3275118058 * L_11 = __this->get_damageEffectTransform_8();
		NullCheck(L_10);
		Transform_set_parent_m3281327839(L_10, L_11, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_12 = V_0;
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = GameObject_get_transform_m909382139(L_12, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_localPosition_m1026930133(L_13, L_14, /*hidden argument*/NULL);
		Collider_t3497673348 * L_15 = Component_GetComponent_TisCollider_t3497673348_m319016787(__this, /*hidden argument*/Component_GetComponent_TisCollider_t3497673348_m319016787_MethodInfo_var);
		NullCheck(L_15);
		Bounds_t3033363703  L_16 = Collider_get_bounds_m3534458178(L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		Vector3_t2243707580  L_17 = Bounds_get_extents_m4077324178((&V_2), /*hidden argument*/NULL);
		V_3 = L_17;
		float L_18 = (&V_3)->get_x_1();
		Collider_t3497673348 * L_19 = Component_GetComponent_TisCollider_t3497673348_m319016787(__this, /*hidden argument*/Component_GetComponent_TisCollider_t3497673348_m319016787_MethodInfo_var);
		NullCheck(L_19);
		Bounds_t3033363703  L_20 = Collider_get_bounds_m3534458178(L_19, /*hidden argument*/NULL);
		V_4 = L_20;
		Vector3_t2243707580  L_21 = Bounds_get_extents_m4077324178((&V_4), /*hidden argument*/NULL);
		V_5 = L_21;
		float L_22 = (&V_5)->get_z_3();
		Vector2_t2243707579  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Vector2__ctor_m3067419446(&L_23, L_18, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		float L_24 = Vector2_get_magnitude_m33802565((&V_1), /*hidden argument*/NULL);
		__this->set_colliderRadiusHeuristic_17(((float)((float)L_24*(float)(0.5f))));
		Collider_t3497673348 * L_25 = Component_GetComponent_TisCollider_t3497673348_m319016787(__this, /*hidden argument*/Component_GetComponent_TisCollider_t3497673348_m319016787_MethodInfo_var);
		NullCheck(L_25);
		Bounds_t3033363703  L_26 = Collider_get_bounds_m3534458178(L_25, /*hidden argument*/NULL);
		V_6 = L_26;
		Vector3_t2243707580  L_27 = Bounds_get_extents_m4077324178((&V_6), /*hidden argument*/NULL);
		V_7 = L_27;
		float L_28 = (&V_7)->get_y_2();
		__this->set_damageEffectCenterYOffset_16(L_28);
	}

IL_00df:
	{
		GameObject_t1756533147 * L_29 = __this->get_scorchMarkPrefab_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_30 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0116;
		}
	}
	{
		GameObject_t1756533147 * L_31 = __this->get_scorchMarkPrefab_11();
		Vector3_t2243707580  L_32 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_33 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_34 = Object_Instantiate_TisGameObject_t1756533147_m1577880517(NULL /*static, unused*/, L_31, L_32, L_33, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m1577880517_MethodInfo_var);
		__this->set_scorchMark_12(L_34);
		GameObject_t1756533147 * L_35 = __this->get_scorchMark_12();
		NullCheck(L_35);
		GameObject_SetActive_m2887581199(L_35, (bool)0, /*hidden argument*/NULL);
	}

IL_0116:
	{
		return;
	}
}
// System.Void Health::OnDamage(System.Single,UnityEngine.Vector3)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCollider_t3497673348_m319016787_MethodInfo_var;
extern const uint32_t Health_OnDamage_m2245253288_MetadataUsageId;
extern "C"  void Health_OnDamage_m2245253288 (Health_t2683907638 * __this, float ___amount0, Vector3_t2243707580  ___fromDirection1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Health_OnDamage_m2245253288_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		bool L_0 = __this->get_invincible_5();
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		goto IL_0144;
	}

IL_0010:
	{
		bool L_1 = __this->get_dead_6();
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		goto IL_0144;
	}

IL_0020:
	{
		float L_2 = ___amount0;
		if ((((float)L_2) > ((float)(((float)((float)0))))))
		{
			goto IL_002d;
		}
	}
	{
		goto IL_0144;
	}

IL_002d:
	{
		float L_3 = __this->get_health_3();
		float L_4 = ___amount0;
		__this->set_health_3(((float)((float)L_3-(float)L_4)));
		SignalSender_t1204926691 * L_5 = __this->get_damageSignals_13();
		NullCheck(L_5);
		VirtActionInvoker1< MonoBehaviour_t1158329972 * >::Invoke(4 /* System.Void SignalSender::SendSignals(UnityEngine.MonoBehaviour) */, L_5, __this);
		float L_6 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastDamageTime_15(L_6);
		float L_7 = __this->get_regenerateSpeed_4();
		if ((((float)L_7) <= ((float)(((float)((float)0))))))
		{
			goto IL_0066;
		}
	}
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0066:
	{
		float L_8 = __this->get_health_3();
		if ((((float)L_8) > ((float)(((float)((float)0))))))
		{
			goto IL_0144;
		}
	}
	{
		__this->set_health_3((((float)((float)0))));
		__this->set_dead_6((bool)1);
		SignalSender_t1204926691 * L_9 = __this->get_dieSignals_14();
		NullCheck(L_9);
		VirtActionInvoker1< MonoBehaviour_t1158329972 * >::Invoke(4 /* System.Void SignalSender::SendSignals(UnityEngine.MonoBehaviour) */, L_9, __this);
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = __this->get_scorchMark_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0144;
		}
	}
	{
		GameObject_t1756533147 * L_12 = __this->get_scorchMark_12();
		NullCheck(L_12);
		GameObject_SetActive_m2887581199(L_12, (bool)1, /*hidden argument*/NULL);
		Collider_t3497673348 * L_13 = Component_GetComponent_TisCollider_t3497673348_m319016787(__this, /*hidden argument*/Component_GetComponent_TisCollider_t3497673348_m319016787_MethodInfo_var);
		Transform_t3275118058 * L_14 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t2243707580  L_15 = Transform_get_position_m1104419803(L_14, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_17 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_16, (((float)((float)((int32_t)100)))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_18 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t2243707580  L_19 = Collider_ClosestPointOnBounds_m4211637205(L_13, L_18, /*hidden argument*/NULL);
		V_0 = L_19;
		GameObject_t1756533147 * L_20 = __this->get_scorchMark_12();
		NullCheck(L_20);
		Transform_t3275118058 * L_21 = GameObject_get_transform_m909382139(L_20, /*hidden argument*/NULL);
		Vector3_t2243707580  L_22 = V_0;
		Vector3_t2243707580  L_23 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_24 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_23, (0.1f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_25 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_22, L_24, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_set_position_m2469242620(L_21, L_25, /*hidden argument*/NULL);
		float L_26 = Random_Range_m2884721203(NULL /*static, unused*/, (((float)((float)0))), (90.0f), /*hidden argument*/NULL);
		float L_27 = L_26;
		V_1 = L_27;
		GameObject_t1756533147 * L_28 = __this->get_scorchMark_12();
		NullCheck(L_28);
		Transform_t3275118058 * L_29 = GameObject_get_transform_m909382139(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_t2243707580  L_30 = Transform_get_eulerAngles_m4066505159(L_29, /*hidden argument*/NULL);
		Vector3_t2243707580  L_31 = L_30;
		V_2 = L_31;
		float L_32 = V_1;
		float L_33 = L_32;
		V_3 = L_33;
		(&V_2)->set_y_2(L_33);
		float L_34 = V_3;
		GameObject_t1756533147 * L_35 = __this->get_scorchMark_12();
		NullCheck(L_35);
		Transform_t3275118058 * L_36 = GameObject_get_transform_m909382139(L_35, /*hidden argument*/NULL);
		Vector3_t2243707580  L_37 = V_2;
		Vector3_t2243707580  L_38 = L_37;
		V_4 = L_38;
		NullCheck(L_36);
		Transform_set_eulerAngles_m2881310872(L_36, L_38, /*hidden argument*/NULL);
		Vector3_t2243707580  L_39 = V_4;
	}

IL_0144:
	{
		return;
	}
}
// System.Void Health::OnEnable()
extern "C"  void Health_OnEnable_m735332876 (Health_t2683907638 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = VirtFuncInvoker0< Il2CppObject * >::Invoke(7 /* System.Collections.IEnumerator Health::Regenerate() */, __this);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Health::Regenerate()
extern Il2CppClass* U24RegenerateU2491_t1949600641_il2cpp_TypeInfo_var;
extern const uint32_t Health_Regenerate_m2313802398_MetadataUsageId;
extern "C"  Il2CppObject * Health_Regenerate_m2313802398 (Health_t2683907638 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Health_Regenerate_m2313802398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U24RegenerateU2491_t1949600641 * L_0 = (U24RegenerateU2491_t1949600641 *)il2cpp_codegen_object_new(U24RegenerateU2491_t1949600641_il2cpp_TypeInfo_var);
		U24RegenerateU2491__ctor_m3102807769(L_0, __this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject* L_1 = U24RegenerateU2491_GetEnumerator_m2761811061(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Health::Main()
extern "C"  void Health_Main_m738848101 (Health_t2683907638 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Health/$Regenerate$91::.ctor(Health)
extern const MethodInfo* GenericGenerator_1__ctor_m1542724045_MethodInfo_var;
extern const uint32_t U24RegenerateU2491__ctor_m3102807769_MetadataUsageId;
extern "C"  void U24RegenerateU2491__ctor_m3102807769 (U24RegenerateU2491_t1949600641 * __this, Health_t2683907638 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24RegenerateU2491__ctor_m3102807769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GenericGenerator_1__ctor_m1542724045(__this, /*hidden argument*/GenericGenerator_1__ctor_m1542724045_MethodInfo_var);
		Health_t2683907638 * L_0 = ___self_0;
		__this->set_U24self_U2493_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> Health/$Regenerate$91::GetEnumerator()
extern Il2CppClass* U24_t3039949752_il2cpp_TypeInfo_var;
extern const uint32_t U24RegenerateU2491_GetEnumerator_m2761811061_MetadataUsageId;
extern "C"  Il2CppObject* U24RegenerateU2491_GetEnumerator_m2761811061 (U24RegenerateU2491_t1949600641 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24RegenerateU2491_GetEnumerator_m2761811061_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Health_t2683907638 * L_0 = __this->get_U24self_U2493_0();
		U24_t3039949752 * L_1 = (U24_t3039949752 *)il2cpp_codegen_object_new(U24_t3039949752_il2cpp_TypeInfo_var);
		U24__ctor_m1085135550(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Health/$Regenerate$91/$::.ctor(Health)
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m3451731617_MethodInfo_var;
extern const uint32_t U24__ctor_m1085135550_MetadataUsageId;
extern "C"  void U24__ctor_m1085135550 (U24_t3039949752 * __this, Health_t2683907638 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m1085135550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m3451731617(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m3451731617_MethodInfo_var);
		Health_t2683907638 * L_0 = ___self_0;
		__this->set_U24self_U2492_2(L_0);
		return;
	}
}
// System.Boolean Health/$Regenerate$91/$::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m3507348778_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_Yield_m1415833518_MethodInfo_var;
extern const uint32_t U24_MoveNext_m3520769554_MetadataUsageId;
extern "C"  bool U24_MoveNext_m3520769554 (U24_t3039949752 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m3520769554_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B11_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t300505933 *)__this)->get__state_1();
		if (L_0 == 0)
		{
			goto IL_001b;
		}
		if (L_0 == 1)
		{
			goto IL_00e3;
		}
		if (L_0 == 2)
		{
			goto IL_0078;
		}
		if (L_0 == 3)
		{
			goto IL_00cb;
		}
	}

IL_001b:
	{
		Health_t2683907638 * L_1 = __this->get_U24self_U2492_2();
		NullCheck(L_1);
		float L_2 = L_1->get_regenerateSpeed_4();
		if ((((float)L_2) <= ((float)(((float)((float)0))))))
		{
			goto IL_00db;
		}
	}
	{
		goto IL_00cb;
	}

IL_0032:
	{
		float L_3 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Health_t2683907638 * L_4 = __this->get_U24self_U2492_2();
		NullCheck(L_4);
		float L_5 = L_4->get_lastDamageTime_15();
		if ((((float)L_3) <= ((float)((float)((float)L_5+(float)(((float)((float)3))))))))
		{
			goto IL_00b5;
		}
	}
	{
		Health_t2683907638 * L_6 = __this->get_U24self_U2492_2();
		Health_t2683907638 * L_7 = __this->get_U24self_U2492_2();
		NullCheck(L_7);
		float L_8 = L_7->get_health_3();
		Health_t2683907638 * L_9 = __this->get_U24self_U2492_2();
		NullCheck(L_9);
		float L_10 = L_9->get_regenerateSpeed_4();
		NullCheck(L_6);
		L_6->set_health_3(((float)((float)L_8+(float)L_10)));
		bool L_11 = GenericGeneratorEnumerator_1_YieldDefault_m3507348778(__this, 2, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m3507348778_MethodInfo_var);
		G_B11_0 = ((int32_t)(L_11));
		goto IL_00e4;
	}

IL_0078:
	{
		Health_t2683907638 * L_12 = __this->get_U24self_U2492_2();
		NullCheck(L_12);
		float L_13 = L_12->get_health_3();
		Health_t2683907638 * L_14 = __this->get_U24self_U2492_2();
		NullCheck(L_14);
		float L_15 = L_14->get_maxHealth_2();
		if ((((float)L_13) < ((float)L_15)))
		{
			goto IL_00b5;
		}
	}
	{
		Health_t2683907638 * L_16 = __this->get_U24self_U2492_2();
		Health_t2683907638 * L_17 = __this->get_U24self_U2492_2();
		NullCheck(L_17);
		float L_18 = L_17->get_maxHealth_2();
		NullCheck(L_16);
		L_16->set_health_3(L_18);
		Health_t2683907638 * L_19 = __this->get_U24self_U2492_2();
		NullCheck(L_19);
		Behaviour_set_enabled_m1796096907(L_19, (bool)0, /*hidden argument*/NULL);
	}

IL_00b5:
	{
		WaitForSeconds_t3839502067 * L_20 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_20, (1.0f), /*hidden argument*/NULL);
		bool L_21 = GenericGeneratorEnumerator_1_Yield_m1415833518(__this, 3, L_20, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m1415833518_MethodInfo_var);
		G_B11_0 = ((int32_t)(L_21));
		goto IL_00e4;
	}

IL_00cb:
	{
		Health_t2683907638 * L_22 = __this->get_U24self_U2492_2();
		NullCheck(L_22);
		bool L_23 = Behaviour_get_enabled_m4079055610(L_22, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_0032;
		}
	}

IL_00db:
	{
		GenericGeneratorEnumerator_1_YieldDefault_m3507348778(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m3507348778_MethodInfo_var);
	}

IL_00e3:
	{
		G_B11_0 = 0;
	}

IL_00e4:
	{
		return (bool)G_B11_0;
	}
}
// System.Void HealthFlash::.ctor()
extern "C"  void HealthFlash__ctor_m635015568 (HealthFlash_t1223120616 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_healthBlink_4((1.0f));
		__this->set_oneOverMaxHealth_5((0.5f));
		return;
	}
}
// System.Void HealthFlash::Start()
extern "C"  void HealthFlash_Start_m3491444100 (HealthFlash_t1223120616 * __this, const MethodInfo* method)
{
	{
		Health_t2683907638 * L_0 = __this->get_playerHealth_2();
		NullCheck(L_0);
		float L_1 = L_0->get_maxHealth_2();
		__this->set_oneOverMaxHealth_5(((float)((float)(1.0f)/(float)L_1)));
		return;
	}
}
// System.Void HealthFlash::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral689571250;
extern const uint32_t HealthFlash_Update_m433886455_MetadataUsageId;
extern "C"  void HealthFlash_Update_m433886455 (HealthFlash_t1223120616 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthFlash_Update_m433886455_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Health_t2683907638 * L_0 = __this->get_playerHealth_2();
		NullCheck(L_0);
		float L_1 = L_0->get_health_3();
		float L_2 = __this->get_oneOverMaxHealth_5();
		V_0 = ((float)((float)L_1*(float)L_2));
		Material_t193706927 * L_3 = __this->get_healthMaterial_3();
		float L_4 = V_0;
		float L_5 = __this->get_healthBlink_4();
		NullCheck(L_3);
		Material_SetFloat_m1926275467(L_3, _stringLiteral689571250, ((float)((float)((float)((float)L_4*(float)(2.0f)))*(float)L_5)), /*hidden argument*/NULL);
		float L_6 = V_0;
		if ((((float)L_6) >= ((float)(0.45f))))
		{
			goto IL_005c;
		}
	}
	{
		float L_7 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_8 = Mathf_PingPong_m2539699755(NULL /*static, unused*/, ((float)((float)L_7*(float)(6.0f))), (2.0f), /*hidden argument*/NULL);
		__this->set_healthBlink_4(L_8);
		goto IL_0067;
	}

IL_005c:
	{
		__this->set_healthBlink_4((1.0f));
	}

IL_0067:
	{
		return;
	}
}
// System.Void HealthFlash::Main()
extern "C"  void HealthFlash_Main_m3841114377 (HealthFlash_t1223120616 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Joystick::.ctor()
extern Il2CppClass* Boundary_t1794889402_il2cpp_TypeInfo_var;
extern const uint32_t Joystick__ctor_m1335692108_MetadataUsageId;
extern "C"  void Joystick__ctor_m1335692108 (Joystick_t549888914 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick__ctor_m1335692108_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_lastFingerId_11((-1));
		__this->set_firstDeltaTime_15((0.5f));
		Boundary_t1794889402 * L_0 = (Boundary_t1794889402 *)il2cpp_codegen_object_new(Boundary_t1794889402_il2cpp_TypeInfo_var);
		Boundary__ctor_m1977119940(L_0, /*hidden argument*/NULL);
		__this->set_guiBoundary_18(L_0);
		return;
	}
}
// System.Void Joystick::.cctor()
extern Il2CppClass* Joystick_t549888914_il2cpp_TypeInfo_var;
extern const uint32_t Joystick__cctor_m1457638875_MetadataUsageId;
extern "C"  void Joystick__cctor_m1457638875 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick__cctor_m1457638875_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Joystick_t549888914_StaticFields*)Joystick_t549888914_il2cpp_TypeInfo_var->static_fields)->set_tapTimeDelta_4((0.3f));
		return;
	}
}
// System.Void Joystick::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisGUITexture_t1909122990_m368557725_MethodInfo_var;
extern const uint32_t Joystick_Start_m3461673412_MetadataUsageId;
extern "C"  void Joystick_Start_m3461673412 (Joystick_t549888914 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick_Start_m3461673412_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float V_8 = 0.0f;
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	{
		GUITexture_t1909122990 * L_0 = Component_GetComponent_TisGUITexture_t1909122990_m368557725(__this, /*hidden argument*/Component_GetComponent_TisGUITexture_t1909122990_m368557725_MethodInfo_var);
		__this->set_gui_16(L_0);
		GUITexture_t1909122990 * L_1 = __this->get_gui_16();
		NullCheck(L_1);
		Rect_t3681755626  L_2 = GUITexture_get_pixelInset_m1273695445(L_1, /*hidden argument*/NULL);
		__this->set_defaultRect_17(L_2);
		Rect_t3681755626 * L_3 = __this->get_address_of_defaultRect_17();
		Rect_t3681755626 * L_4 = __this->get_address_of_defaultRect_17();
		float L_5 = Rect_get_x_m1393582490(L_4, /*hidden argument*/NULL);
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		V_4 = L_7;
		float L_8 = (&V_4)->get_x_1();
		int32_t L_9 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_x_m3783700513(L_3, ((float)((float)L_5+(float)((float)((float)L_8*(float)(((float)((float)L_9))))))), /*hidden argument*/NULL);
		Rect_t3681755626 * L_10 = __this->get_address_of_defaultRect_17();
		Rect_t3681755626 * L_11 = __this->get_address_of_defaultRect_17();
		float L_12 = Rect_get_y_m1393582395(L_11, /*hidden argument*/NULL);
		Transform_t3275118058 * L_13 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t2243707580  L_14 = Transform_get_position_m1104419803(L_13, /*hidden argument*/NULL);
		V_5 = L_14;
		float L_15 = (&V_5)->get_y_2();
		int32_t L_16 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_y_m4294916608(L_10, ((float)((float)L_12+(float)((float)((float)L_15*(float)(((float)((float)L_16))))))), /*hidden argument*/NULL);
		float L_17 = (((float)((float)0)));
		V_0 = L_17;
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t2243707580  L_19 = Transform_get_position_m1104419803(L_18, /*hidden argument*/NULL);
		Vector3_t2243707580  L_20 = L_19;
		V_1 = L_20;
		float L_21 = V_0;
		float L_22 = L_21;
		V_6 = L_22;
		(&V_1)->set_x_1(L_22);
		float L_23 = V_6;
		Transform_t3275118058 * L_24 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_25 = V_1;
		Vector3_t2243707580  L_26 = L_25;
		V_7 = L_26;
		NullCheck(L_24);
		Transform_set_position_m2469242620(L_24, L_26, /*hidden argument*/NULL);
		Vector3_t2243707580  L_27 = V_7;
		float L_28 = (((float)((float)0)));
		V_2 = L_28;
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_t2243707580  L_30 = Transform_get_position_m1104419803(L_29, /*hidden argument*/NULL);
		Vector3_t2243707580  L_31 = L_30;
		V_3 = L_31;
		float L_32 = V_2;
		float L_33 = L_32;
		V_8 = L_33;
		(&V_3)->set_y_2(L_33);
		float L_34 = V_8;
		Transform_t3275118058 * L_35 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_36 = V_3;
		Vector3_t2243707580  L_37 = L_36;
		V_9 = L_37;
		NullCheck(L_35);
		Transform_set_position_m2469242620(L_35, L_37, /*hidden argument*/NULL);
		Vector3_t2243707580  L_38 = V_9;
		bool L_39 = __this->get_touchPad_5();
		if (!L_39)
		{
			goto IL_0118;
		}
	}
	{
		GUITexture_t1909122990 * L_40 = __this->get_gui_16();
		NullCheck(L_40);
		Texture_t2243626319 * L_41 = GUITexture_get_texture_m538118770(L_40, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_42 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0113;
		}
	}
	{
		Rect_t3681755626  L_43 = __this->get_defaultRect_17();
		__this->set_touchZone_6(L_43);
	}

IL_0113:
	{
		goto IL_0230;
	}

IL_0118:
	{
		Vector2_t2243707579 * L_44 = __this->get_address_of_guiTouchOffset_19();
		Rect_t3681755626 * L_45 = __this->get_address_of_defaultRect_17();
		float L_46 = Rect_get_width_m1138015702(L_45, /*hidden argument*/NULL);
		L_44->set_x_0(((float)((float)L_46*(float)(0.5f))));
		Vector2_t2243707579 * L_47 = __this->get_address_of_guiTouchOffset_19();
		Rect_t3681755626 * L_48 = __this->get_address_of_defaultRect_17();
		float L_49 = Rect_get_height_m3128694305(L_48, /*hidden argument*/NULL);
		L_47->set_y_1(((float)((float)L_49*(float)(0.5f))));
		Vector2_t2243707579 * L_50 = __this->get_address_of_guiCenter_20();
		Rect_t3681755626 * L_51 = __this->get_address_of_defaultRect_17();
		float L_52 = Rect_get_x_m1393582490(L_51, /*hidden argument*/NULL);
		Vector2_t2243707579 * L_53 = __this->get_address_of_guiTouchOffset_19();
		float L_54 = L_53->get_x_0();
		L_50->set_x_0(((float)((float)L_52+(float)L_54)));
		Vector2_t2243707579 * L_55 = __this->get_address_of_guiCenter_20();
		Rect_t3681755626 * L_56 = __this->get_address_of_defaultRect_17();
		float L_57 = Rect_get_y_m1393582395(L_56, /*hidden argument*/NULL);
		Vector2_t2243707579 * L_58 = __this->get_address_of_guiTouchOffset_19();
		float L_59 = L_58->get_y_1();
		L_55->set_y_1(((float)((float)L_57+(float)L_59)));
		Boundary_t1794889402 * L_60 = __this->get_guiBoundary_18();
		NullCheck(L_60);
		Vector2_t2243707579 * L_61 = L_60->get_address_of_min_0();
		Rect_t3681755626 * L_62 = __this->get_address_of_defaultRect_17();
		float L_63 = Rect_get_x_m1393582490(L_62, /*hidden argument*/NULL);
		Vector2_t2243707579 * L_64 = __this->get_address_of_guiTouchOffset_19();
		float L_65 = L_64->get_x_0();
		L_61->set_x_0(((float)((float)L_63-(float)L_65)));
		Boundary_t1794889402 * L_66 = __this->get_guiBoundary_18();
		NullCheck(L_66);
		Vector2_t2243707579 * L_67 = L_66->get_address_of_max_1();
		Rect_t3681755626 * L_68 = __this->get_address_of_defaultRect_17();
		float L_69 = Rect_get_x_m1393582490(L_68, /*hidden argument*/NULL);
		Vector2_t2243707579 * L_70 = __this->get_address_of_guiTouchOffset_19();
		float L_71 = L_70->get_x_0();
		L_67->set_x_0(((float)((float)L_69+(float)L_71)));
		Boundary_t1794889402 * L_72 = __this->get_guiBoundary_18();
		NullCheck(L_72);
		Vector2_t2243707579 * L_73 = L_72->get_address_of_min_0();
		Rect_t3681755626 * L_74 = __this->get_address_of_defaultRect_17();
		float L_75 = Rect_get_y_m1393582395(L_74, /*hidden argument*/NULL);
		Vector2_t2243707579 * L_76 = __this->get_address_of_guiTouchOffset_19();
		float L_77 = L_76->get_y_1();
		L_73->set_y_1(((float)((float)L_75-(float)L_77)));
		Boundary_t1794889402 * L_78 = __this->get_guiBoundary_18();
		NullCheck(L_78);
		Vector2_t2243707579 * L_79 = L_78->get_address_of_max_1();
		Rect_t3681755626 * L_80 = __this->get_address_of_defaultRect_17();
		float L_81 = Rect_get_y_m1393582395(L_80, /*hidden argument*/NULL);
		Vector2_t2243707579 * L_82 = __this->get_address_of_guiTouchOffset_19();
		float L_83 = L_82->get_y_1();
		L_79->set_y_1(((float)((float)L_81+(float)L_83)));
	}

IL_0230:
	{
		return;
	}
}
// System.Void Joystick::Disable()
extern Il2CppClass* Joystick_t549888914_il2cpp_TypeInfo_var;
extern const uint32_t Joystick_Disable_m2610396156_MetadataUsageId;
extern "C"  void Joystick_Disable_m2610396156 (Joystick_t549888914 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick_Disable_m2610396156_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Joystick_t549888914_il2cpp_TypeInfo_var);
		((Joystick_t549888914_StaticFields*)Joystick_t549888914_il2cpp_TypeInfo_var->static_fields)->set_enumeratedJoysticks_3((bool)0);
		return;
	}
}
// System.Void Joystick::ResetJoystick()
extern "C"  void Joystick_ResetJoystick_m2160955459 (Joystick_t549888914 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Color_t2020392075  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Color_t2020392075  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		GUITexture_t1909122990 * L_0 = __this->get_gui_16();
		Rect_t3681755626  L_1 = __this->get_defaultRect_17();
		NullCheck(L_0);
		GUITexture_set_pixelInset_m2054734980(L_0, L_1, /*hidden argument*/NULL);
		__this->set_lastFingerId_11((-1));
		Vector2_t2243707579  L_2 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_position_9(L_2);
		Vector2_t2243707579  L_3 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_fingerDownPos_13(L_3);
		bool L_4 = __this->get_touchPad_5();
		if (!L_4)
		{
			goto IL_006b;
		}
	}
	{
		float L_5 = (0.025f);
		V_0 = L_5;
		GUITexture_t1909122990 * L_6 = __this->get_gui_16();
		NullCheck(L_6);
		Color_t2020392075  L_7 = GUITexture_get_color_m3560963858(L_6, /*hidden argument*/NULL);
		Color_t2020392075  L_8 = L_7;
		V_1 = L_8;
		float L_9 = V_0;
		float L_10 = L_9;
		V_2 = L_10;
		(&V_1)->set_a_3(L_10);
		float L_11 = V_2;
		GUITexture_t1909122990 * L_12 = __this->get_gui_16();
		Color_t2020392075  L_13 = V_1;
		Color_t2020392075  L_14 = L_13;
		V_3 = L_14;
		NullCheck(L_12);
		GUITexture_set_color_m2687737111(L_12, L_14, /*hidden argument*/NULL);
		Color_t2020392075  L_15 = V_3;
	}

IL_006b:
	{
		return;
	}
}
// System.Boolean Joystick::IsFingerDown()
extern "C"  bool Joystick_IsFingerDown_m535501349 (Joystick_t549888914 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_lastFingerId_11();
		return (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Joystick::LatchedFinger(System.Int32)
extern "C"  void Joystick_LatchedFinger_m1573475483 (Joystick_t549888914 * __this, int32_t ___fingerId0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_lastFingerId_11();
		int32_t L_1 = ___fingerId0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void Joystick::ResetJoystick() */, __this);
	}

IL_0012:
	{
		return;
	}
}
// System.Void Joystick::Update()
extern const Il2CppType* Joystick_t549888914_0_0_0_var;
extern Il2CppClass* Joystick_t549888914_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* JoystickU5BU5D_t1912885735_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Joystick_Update_m3141343657_MetadataUsageId;
extern "C"  void Joystick_Update_m3141343657 (Joystick_t549888914 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick_Update_m3141343657_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Touch_t407273883  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	bool V_4 = false;
	Joystick_t549888914 * V_5 = NULL;
	float V_6 = 0.0f;
	int32_t V_7 = 0;
	JoystickU5BU5D_t1912885735* V_8 = NULL;
	int32_t V_9 = 0;
	float V_10 = 0.0f;
	Color_t2020392075  V_11;
	memset(&V_11, 0, sizeof(V_11));
	float V_12 = 0.0f;
	Rect_t3681755626  V_13;
	memset(&V_13, 0, sizeof(V_13));
	float V_14 = 0.0f;
	Rect_t3681755626  V_15;
	memset(&V_15, 0, sizeof(V_15));
	float V_16 = 0.0f;
	Color_t2020392075  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Vector2_t2243707579  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Vector2_t2243707579  V_19;
	memset(&V_19, 0, sizeof(V_19));
	Vector2_t2243707579  V_20;
	memset(&V_20, 0, sizeof(V_20));
	Vector2_t2243707579  V_21;
	memset(&V_21, 0, sizeof(V_21));
	float V_22 = 0.0f;
	Rect_t3681755626  V_23;
	memset(&V_23, 0, sizeof(V_23));
	float V_24 = 0.0f;
	Rect_t3681755626  V_25;
	memset(&V_25, 0, sizeof(V_25));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Joystick_t549888914_il2cpp_TypeInfo_var);
		bool L_0 = ((Joystick_t549888914_StaticFields*)Joystick_t549888914_il2cpp_TypeInfo_var->static_fields)->get_enumeratedJoysticks_3();
		if (L_0)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Joystick_t549888914_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_2 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Joystick_t549888914_il2cpp_TypeInfo_var);
		((Joystick_t549888914_StaticFields*)Joystick_t549888914_il2cpp_TypeInfo_var->static_fields)->set_joysticks_2(((JoystickU5BU5D_t1912885735*)IsInst(((JoystickU5BU5D_t1912885735*)Castclass(L_2, JoystickU5BU5D_t1912885735_il2cpp_TypeInfo_var)), JoystickU5BU5D_t1912885735_il2cpp_TypeInfo_var)));
		((Joystick_t549888914_StaticFields*)Joystick_t549888914_il2cpp_TypeInfo_var->static_fields)->set_enumeratedJoysticks_3((bool)1);
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_3 = Input_get_touchCount_m2050827666(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = __this->get_tapTimeWindow_12();
		if ((((float)L_4) <= ((float)(((float)((float)0))))))
		{
			goto IL_0058;
		}
	}
	{
		float L_5 = __this->get_tapTimeWindow_12();
		float L_6 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_tapTimeWindow_12(((float)((float)L_5-(float)L_6)));
		goto IL_005f;
	}

IL_0058:
	{
		__this->set_tapCount_10(0);
	}

IL_005f:
	{
		int32_t L_7 = V_0;
		if (L_7)
		{
			goto IL_0070;
		}
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void Joystick::ResetJoystick() */, __this);
		goto IL_0353;
	}

IL_0070:
	{
		V_1 = 0;
		goto IL_034c;
	}

IL_0077:
	{
		int32_t L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Touch_t407273883  L_9 = Input_GetTouch_m1463942798(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		Vector2_t2243707579  L_10 = Touch_get_position_m2079703643((&V_2), /*hidden argument*/NULL);
		Vector2_t2243707579  L_11 = __this->get_guiTouchOffset_19();
		Vector2_t2243707579  L_12 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		V_4 = (bool)0;
		bool L_13 = __this->get_touchPad_5();
		if (!L_13)
		{
			goto IL_00be;
		}
	}
	{
		Rect_t3681755626 * L_14 = __this->get_address_of_touchZone_6();
		Vector2_t2243707579  L_15 = Touch_get_position_m2079703643((&V_2), /*hidden argument*/NULL);
		bool L_16 = Rect_Contains_m1334685290(L_14, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00b9;
		}
	}
	{
		V_4 = (bool)1;
	}

IL_00b9:
	{
		goto IL_00dd;
	}

IL_00be:
	{
		GUITexture_t1909122990 * L_17 = __this->get_gui_16();
		Vector2_t2243707579  L_18 = Touch_get_position_m2079703643((&V_2), /*hidden argument*/NULL);
		Vector3_t2243707580  L_19 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		bool L_20 = GUIElement_HitTest_m3066714724(L_17, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00dd;
		}
	}
	{
		V_4 = (bool)1;
	}

IL_00dd:
	{
		bool L_21 = V_4;
		if (!L_21)
		{
			goto IL_0203;
		}
	}
	{
		int32_t L_22 = __this->get_lastFingerId_11();
		if ((((int32_t)L_22) == ((int32_t)(-1))))
		{
			goto IL_0102;
		}
	}
	{
		int32_t L_23 = __this->get_lastFingerId_11();
		int32_t L_24 = Touch_get_fingerId_m4109475843((&V_2), /*hidden argument*/NULL);
		if ((((int32_t)L_23) == ((int32_t)L_24)))
		{
			goto IL_0203;
		}
	}

IL_0102:
	{
		bool L_25 = __this->get_touchPad_5();
		if (!L_25)
		{
			goto IL_016c;
		}
	}
	{
		float L_26 = (0.15f);
		V_10 = L_26;
		GUITexture_t1909122990 * L_27 = __this->get_gui_16();
		NullCheck(L_27);
		Color_t2020392075  L_28 = GUITexture_get_color_m3560963858(L_27, /*hidden argument*/NULL);
		Color_t2020392075  L_29 = L_28;
		V_11 = L_29;
		float L_30 = V_10;
		float L_31 = L_30;
		V_16 = L_31;
		(&V_11)->set_a_3(L_31);
		float L_32 = V_16;
		GUITexture_t1909122990 * L_33 = __this->get_gui_16();
		Color_t2020392075  L_34 = V_11;
		Color_t2020392075  L_35 = L_34;
		V_17 = L_35;
		NullCheck(L_33);
		GUITexture_set_color_m2687737111(L_33, L_35, /*hidden argument*/NULL);
		Color_t2020392075  L_36 = V_17;
		int32_t L_37 = Touch_get_fingerId_m4109475843((&V_2), /*hidden argument*/NULL);
		__this->set_lastFingerId_11(L_37);
		Vector2_t2243707579  L_38 = Touch_get_position_m2079703643((&V_2), /*hidden argument*/NULL);
		__this->set_fingerDownPos_13(L_38);
		float L_39 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_fingerDownTime_14(L_39);
	}

IL_016c:
	{
		int32_t L_40 = Touch_get_fingerId_m4109475843((&V_2), /*hidden argument*/NULL);
		__this->set_lastFingerId_11(L_40);
		float L_41 = __this->get_tapTimeWindow_12();
		if ((((float)L_41) <= ((float)(((float)((float)0))))))
		{
			goto IL_0199;
		}
	}
	{
		int32_t L_42 = __this->get_tapCount_10();
		__this->set_tapCount_10(((int32_t)((int32_t)L_42+(int32_t)1)));
		goto IL_01ab;
	}

IL_0199:
	{
		__this->set_tapCount_10(1);
		IL2CPP_RUNTIME_CLASS_INIT(Joystick_t549888914_il2cpp_TypeInfo_var);
		float L_43 = ((Joystick_t549888914_StaticFields*)Joystick_t549888914_il2cpp_TypeInfo_var->static_fields)->get_tapTimeDelta_4();
		__this->set_tapTimeWindow_12(L_43);
	}

IL_01ab:
	{
		V_7 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Joystick_t549888914_il2cpp_TypeInfo_var);
		JoystickU5BU5D_t1912885735* L_44 = ((Joystick_t549888914_StaticFields*)Joystick_t549888914_il2cpp_TypeInfo_var->static_fields)->get_joysticks_2();
		V_8 = L_44;
		JoystickU5BU5D_t1912885735* L_45 = V_8;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_45);
		int32_t L_46 = Array_get_Length_m1498215565((Il2CppArray *)(Il2CppArray *)L_45, /*hidden argument*/NULL);
		V_9 = L_46;
		goto IL_01fa;
	}

IL_01c3:
	{
		JoystickU5BU5D_t1912885735* L_47 = V_8;
		int32_t L_48 = V_7;
		NullCheck(L_47);
		int32_t L_49 = L_48;
		Joystick_t549888914 * L_50 = (L_47)->GetAt(static_cast<il2cpp_array_size_t>(L_49));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_51 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_50, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_51)
		{
			goto IL_01f4;
		}
	}
	{
		JoystickU5BU5D_t1912885735* L_52 = V_8;
		int32_t L_53 = V_7;
		NullCheck(L_52);
		int32_t L_54 = L_53;
		Joystick_t549888914 * L_55 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_56 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_55, __this, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_01f4;
		}
	}
	{
		JoystickU5BU5D_t1912885735* L_57 = V_8;
		int32_t L_58 = V_7;
		NullCheck(L_57);
		int32_t L_59 = L_58;
		Joystick_t549888914 * L_60 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_59));
		int32_t L_61 = Touch_get_fingerId_m4109475843((&V_2), /*hidden argument*/NULL);
		NullCheck(L_60);
		VirtActionInvoker1< int32_t >::Invoke(8 /* System.Void Joystick::LatchedFinger(System.Int32) */, L_60, L_61);
	}

IL_01f4:
	{
		int32_t L_62 = V_7;
		V_7 = ((int32_t)((int32_t)L_62+(int32_t)1));
	}

IL_01fa:
	{
		int32_t L_63 = V_7;
		int32_t L_64 = V_9;
		if ((((int32_t)L_63) < ((int32_t)L_64)))
		{
			goto IL_01c3;
		}
	}

IL_0203:
	{
		int32_t L_65 = __this->get_lastFingerId_11();
		int32_t L_66 = Touch_get_fingerId_m4109475843((&V_2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_65) == ((uint32_t)L_66))))
		{
			goto IL_0348;
		}
	}
	{
		int32_t L_67 = Touch_get_tapCount_m4090741061((&V_2), /*hidden argument*/NULL);
		int32_t L_68 = __this->get_tapCount_10();
		if ((((int32_t)L_67) <= ((int32_t)L_68)))
		{
			goto IL_0234;
		}
	}
	{
		int32_t L_69 = Touch_get_tapCount_m4090741061((&V_2), /*hidden argument*/NULL);
		__this->set_tapCount_10(L_69);
	}

IL_0234:
	{
		bool L_70 = __this->get_touchPad_5();
		if (!L_70)
		{
			goto IL_02c2;
		}
	}
	{
		Vector2_t2243707579 * L_71 = __this->get_address_of_position_9();
		Vector2_t2243707579  L_72 = Touch_get_position_m2079703643((&V_2), /*hidden argument*/NULL);
		V_18 = L_72;
		float L_73 = (&V_18)->get_x_0();
		Vector2_t2243707579 * L_74 = __this->get_address_of_fingerDownPos_13();
		float L_75 = L_74->get_x_0();
		Rect_t3681755626 * L_76 = __this->get_address_of_touchZone_6();
		float L_77 = Rect_get_width_m1138015702(L_76, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_78 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, ((float)((float)((float)((float)L_73-(float)L_75))/(float)((float)((float)L_77/(float)(((float)((float)2))))))), (((float)((float)(-1)))), (((float)((float)1))), /*hidden argument*/NULL);
		L_71->set_x_0(L_78);
		Vector2_t2243707579 * L_79 = __this->get_address_of_position_9();
		Vector2_t2243707579  L_80 = Touch_get_position_m2079703643((&V_2), /*hidden argument*/NULL);
		V_19 = L_80;
		float L_81 = (&V_19)->get_y_1();
		Vector2_t2243707579 * L_82 = __this->get_address_of_fingerDownPos_13();
		float L_83 = L_82->get_y_1();
		Rect_t3681755626 * L_84 = __this->get_address_of_touchZone_6();
		float L_85 = Rect_get_height_m3128694305(L_84, /*hidden argument*/NULL);
		float L_86 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, ((float)((float)((float)((float)L_81-(float)L_83))/(float)((float)((float)L_85/(float)(((float)((float)2))))))), (((float)((float)(-1)))), (((float)((float)1))), /*hidden argument*/NULL);
		L_79->set_y_1(L_86);
		goto IL_0328;
	}

IL_02c2:
	{
		Vector2_t2243707579 * L_87 = __this->get_address_of_position_9();
		Vector2_t2243707579  L_88 = Touch_get_position_m2079703643((&V_2), /*hidden argument*/NULL);
		V_20 = L_88;
		float L_89 = (&V_20)->get_x_0();
		Vector2_t2243707579 * L_90 = __this->get_address_of_guiCenter_20();
		float L_91 = L_90->get_x_0();
		Vector2_t2243707579 * L_92 = __this->get_address_of_guiTouchOffset_19();
		float L_93 = L_92->get_x_0();
		L_87->set_x_0(((float)((float)((float)((float)L_89-(float)L_91))/(float)L_93)));
		Vector2_t2243707579 * L_94 = __this->get_address_of_position_9();
		Vector2_t2243707579  L_95 = Touch_get_position_m2079703643((&V_2), /*hidden argument*/NULL);
		V_21 = L_95;
		float L_96 = (&V_21)->get_y_1();
		Vector2_t2243707579 * L_97 = __this->get_address_of_guiCenter_20();
		float L_98 = L_97->get_y_1();
		Vector2_t2243707579 * L_99 = __this->get_address_of_guiTouchOffset_19();
		float L_100 = L_99->get_y_1();
		L_94->set_y_1(((float)((float)((float)((float)L_96-(float)L_98))/(float)L_100)));
	}

IL_0328:
	{
		int32_t L_101 = Touch_get_phase_m196706494((&V_2), /*hidden argument*/NULL);
		if ((((int32_t)L_101) == ((int32_t)3)))
		{
			goto IL_0342;
		}
	}
	{
		int32_t L_102 = Touch_get_phase_m196706494((&V_2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_102) == ((uint32_t)4))))
		{
			goto IL_0348;
		}
	}

IL_0342:
	{
		VirtActionInvoker0::Invoke(6 /* System.Void Joystick::ResetJoystick() */, __this);
	}

IL_0348:
	{
		int32_t L_103 = V_1;
		V_1 = ((int32_t)((int32_t)L_103+(int32_t)1));
	}

IL_034c:
	{
		int32_t L_104 = V_1;
		int32_t L_105 = V_0;
		if ((((int32_t)L_104) < ((int32_t)L_105)))
		{
			goto IL_0077;
		}
	}

IL_0353:
	{
		Vector2_t2243707579 * L_106 = __this->get_address_of_position_9();
		float L_107 = Vector2_get_magnitude_m33802565(L_106, /*hidden argument*/NULL);
		V_6 = L_107;
		float L_108 = V_6;
		float L_109 = __this->get_deadZone_7();
		if ((((float)L_108) >= ((float)L_109)))
		{
			goto IL_037d;
		}
	}
	{
		Vector2_t2243707579  L_110 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_position_9(L_110);
		goto IL_03d0;
	}

IL_037d:
	{
		float L_111 = V_6;
		if ((((float)L_111) <= ((float)(((float)((float)1))))))
		{
			goto IL_039e;
		}
	}
	{
		Vector2_t2243707579  L_112 = __this->get_position_9();
		float L_113 = V_6;
		Vector2_t2243707579  L_114 = Vector2_op_Division_m96580069(NULL /*static, unused*/, L_112, L_113, /*hidden argument*/NULL);
		__this->set_position_9(L_114);
		goto IL_03d0;
	}

IL_039e:
	{
		bool L_115 = __this->get_normalize_8();
		if (!L_115)
		{
			goto IL_03d0;
		}
	}
	{
		Vector2_t2243707579  L_116 = __this->get_position_9();
		float L_117 = V_6;
		Vector2_t2243707579  L_118 = Vector2_op_Division_m96580069(NULL /*static, unused*/, L_116, L_117, /*hidden argument*/NULL);
		float L_119 = V_6;
		float L_120 = __this->get_deadZone_7();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_121 = Mathf_InverseLerp_m55890283(NULL /*static, unused*/, L_119, L_120, (((float)((float)1))), /*hidden argument*/NULL);
		Vector2_t2243707579  L_122 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, L_118, L_121, /*hidden argument*/NULL);
		__this->set_position_9(L_122);
	}

IL_03d0:
	{
		bool L_123 = __this->get_touchPad_5();
		if (L_123)
		{
			goto IL_0491;
		}
	}
	{
		Vector2_t2243707579 * L_124 = __this->get_address_of_position_9();
		float L_125 = L_124->get_x_0();
		Vector2_t2243707579 * L_126 = __this->get_address_of_guiTouchOffset_19();
		float L_127 = L_126->get_x_0();
		Vector2_t2243707579 * L_128 = __this->get_address_of_guiCenter_20();
		float L_129 = L_128->get_x_0();
		float L_130 = ((float)((float)((float)((float)((float)((float)L_125-(float)(((float)((float)1)))))*(float)L_127))+(float)L_129));
		V_12 = L_130;
		GUITexture_t1909122990 * L_131 = __this->get_gui_16();
		NullCheck(L_131);
		Rect_t3681755626  L_132 = GUITexture_get_pixelInset_m1273695445(L_131, /*hidden argument*/NULL);
		Rect_t3681755626  L_133 = L_132;
		V_13 = L_133;
		float L_134 = V_12;
		float L_135 = L_134;
		V_22 = L_135;
		Rect_set_x_m3783700513((&V_13), L_135, /*hidden argument*/NULL);
		float L_136 = V_22;
		GUITexture_t1909122990 * L_137 = __this->get_gui_16();
		Rect_t3681755626  L_138 = V_13;
		Rect_t3681755626  L_139 = L_138;
		V_23 = L_139;
		NullCheck(L_137);
		GUITexture_set_pixelInset_m2054734980(L_137, L_139, /*hidden argument*/NULL);
		Rect_t3681755626  L_140 = V_23;
		Vector2_t2243707579 * L_141 = __this->get_address_of_position_9();
		float L_142 = L_141->get_y_1();
		Vector2_t2243707579 * L_143 = __this->get_address_of_guiTouchOffset_19();
		float L_144 = L_143->get_y_1();
		Vector2_t2243707579 * L_145 = __this->get_address_of_guiCenter_20();
		float L_146 = L_145->get_y_1();
		float L_147 = ((float)((float)((float)((float)((float)((float)L_142-(float)(((float)((float)1)))))*(float)L_144))+(float)L_146));
		V_14 = L_147;
		GUITexture_t1909122990 * L_148 = __this->get_gui_16();
		NullCheck(L_148);
		Rect_t3681755626  L_149 = GUITexture_get_pixelInset_m1273695445(L_148, /*hidden argument*/NULL);
		Rect_t3681755626  L_150 = L_149;
		V_15 = L_150;
		float L_151 = V_14;
		float L_152 = L_151;
		V_24 = L_152;
		Rect_set_y_m4294916608((&V_15), L_152, /*hidden argument*/NULL);
		float L_153 = V_24;
		GUITexture_t1909122990 * L_154 = __this->get_gui_16();
		Rect_t3681755626  L_155 = V_15;
		Rect_t3681755626  L_156 = L_155;
		V_25 = L_156;
		NullCheck(L_154);
		GUITexture_set_pixelInset_m2054734980(L_154, L_156, /*hidden argument*/NULL);
		Rect_t3681755626  L_157 = V_25;
	}

IL_0491:
	{
		return;
	}
}
// System.Void Joystick::Main()
extern "C"  void Joystick_Main_m276376279 (Joystick_t549888914 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LaserScope::.ctor()
extern "C"  void LaserScope__ctor_m1167516545 (LaserScope_t1872580701 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_scrollSpeed_2((0.5f));
		__this->set_pulseSpeed_3((1.5f));
		__this->set_noiseSize_4((1.0f));
		__this->set_maxWidth_5((0.5f));
		__this->set_minWidth_6((0.2f));
		__this->set_aniDir_10((1.0f));
		return;
	}
}
// System.Void LaserScope::Start()
extern const Il2CppType* LineRenderer_t849157671_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* LineRenderer_t849157671_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisPerFrameRaycast_t2437905999_m1230835351_MethodInfo_var;
extern const uint32_t LaserScope_Start_m3411263089_MetadataUsageId;
extern "C"  void LaserScope_Start_m3411263089 (LaserScope_t1872580701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LaserScope_Start_m3411263089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(LineRenderer_t849157671_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3819376471 * L_2 = GameObject_GetComponent_m306258075(L_0, L_1, /*hidden argument*/NULL);
		__this->set_lRenderer_8(((LineRenderer_t849157671 *)IsInstSealed(((LineRenderer_t849157671 *)CastclassSealed(L_2, LineRenderer_t849157671_il2cpp_TypeInfo_var)), LineRenderer_t849157671_il2cpp_TypeInfo_var)));
		__this->set_aniTime_9((((float)((float)0))));
		Il2CppObject * L_3 = VirtFuncInvoker0< Il2CppObject * >::Invoke(5 /* System.Collections.IEnumerator LaserScope::ChoseNewAnimationTargetCoroutine() */, __this);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_3, /*hidden argument*/NULL);
		PerFrameRaycast_t2437905999 * L_4 = Component_GetComponent_TisPerFrameRaycast_t2437905999_m1230835351(__this, /*hidden argument*/Component_GetComponent_TisPerFrameRaycast_t2437905999_m1230835351_MethodInfo_var);
		__this->set_raycast_11(L_4);
		return;
	}
}
// System.Collections.IEnumerator LaserScope::ChoseNewAnimationTargetCoroutine()
extern Il2CppClass* U24ChoseNewAnimationTargetCoroutineU2483_t330810268_il2cpp_TypeInfo_var;
extern const uint32_t LaserScope_ChoseNewAnimationTargetCoroutine_m2008565246_MetadataUsageId;
extern "C"  Il2CppObject * LaserScope_ChoseNewAnimationTargetCoroutine_m2008565246 (LaserScope_t1872580701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LaserScope_ChoseNewAnimationTargetCoroutine_m2008565246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U24ChoseNewAnimationTargetCoroutineU2483_t330810268 * L_0 = (U24ChoseNewAnimationTargetCoroutineU2483_t330810268 *)il2cpp_codegen_object_new(U24ChoseNewAnimationTargetCoroutineU2483_t330810268_il2cpp_TypeInfo_var);
		U24ChoseNewAnimationTargetCoroutineU2483__ctor_m4128894609(L_0, __this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject* L_1 = U24ChoseNewAnimationTargetCoroutineU2483_GetEnumerator_m2837683460(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void LaserScope::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t257310565_m2926323388_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1978928756;
extern const uint32_t LaserScope_Update_m28173388_MetadataUsageId;
extern "C"  void LaserScope_Update_m28173388 (LaserScope_t1872580701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LaserScope_Update_m28173388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	RaycastHit_t87180320  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector2_t2243707579  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	Vector2_t2243707579  V_6;
	memset(&V_6, 0, sizeof(V_6));
	float V_7 = 0.0f;
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	float V_9 = 0.0f;
	Vector2_t2243707579  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector2_t2243707579  V_11;
	memset(&V_11, 0, sizeof(V_11));
	float V_12 = 0.0f;
	Vector2_t2243707579  V_13;
	memset(&V_13, 0, sizeof(V_13));
	float V_14 = 0.0f;
	Vector2_t2243707579  V_15;
	memset(&V_15, 0, sizeof(V_15));
	float V_16 = 0.0f;
	Vector3_t2243707580  V_17;
	memset(&V_17, 0, sizeof(V_17));
	float V_18 = 0.0f;
	Vector2_t2243707579  V_19;
	memset(&V_19, 0, sizeof(V_19));
	{
		Renderer_t257310565 * L_0 = Component_GetComponent_TisRenderer_t257310565_m692268576(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var);
		NullCheck(L_0);
		Material_t193706927 * L_1 = Renderer_get_material_m2553789785(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t2243707579  L_2 = Material_get_mainTextureOffset_m786294629(L_1, /*hidden argument*/NULL);
		V_11 = L_2;
		float L_3 = (&V_11)->get_x_0();
		float L_4 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = __this->get_aniDir_10();
		float L_6 = __this->get_scrollSpeed_2();
		float L_7 = ((float)((float)L_3+(float)((float)((float)((float)((float)L_4*(float)L_5))*(float)L_6))));
		V_3 = L_7;
		Renderer_t257310565 * L_8 = Component_GetComponent_TisRenderer_t257310565_m692268576(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var);
		NullCheck(L_8);
		Material_t193706927 * L_9 = Renderer_get_material_m2553789785(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector2_t2243707579  L_10 = Material_get_mainTextureOffset_m786294629(L_9, /*hidden argument*/NULL);
		Vector2_t2243707579  L_11 = L_10;
		V_4 = L_11;
		float L_12 = V_3;
		float L_13 = L_12;
		V_12 = L_13;
		(&V_4)->set_x_0(L_13);
		float L_14 = V_12;
		Renderer_t257310565 * L_15 = Component_GetComponent_TisRenderer_t257310565_m692268576(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var);
		NullCheck(L_15);
		Material_t193706927 * L_16 = Renderer_get_material_m2553789785(L_15, /*hidden argument*/NULL);
		Vector2_t2243707579  L_17 = V_4;
		Vector2_t2243707579  L_18 = L_17;
		V_13 = L_18;
		NullCheck(L_16);
		Material_set_mainTextureOffset_m3533368774(L_16, L_18, /*hidden argument*/NULL);
		Vector2_t2243707579  L_19 = V_13;
		Renderer_t257310565 * L_20 = Component_GetComponent_TisRenderer_t257310565_m692268576(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var);
		NullCheck(L_20);
		Material_t193706927 * L_21 = Renderer_get_material_m2553789785(L_20, /*hidden argument*/NULL);
		float L_22 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_23 = __this->get_aniDir_10();
		float L_24 = __this->get_scrollSpeed_2();
		Vector2_t2243707579  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Vector2__ctor_m3067419446(&L_25, ((float)((float)((float)((float)((-L_22))*(float)L_23))*(float)L_24)), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_21);
		Material_SetTextureOffset_m3084369360(L_21, _stringLiteral1978928756, L_25, /*hidden argument*/NULL);
		float L_26 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_27 = __this->get_pulseSpeed_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_28 = Mathf_PingPong_m2539699755(NULL /*static, unused*/, ((float)((float)L_26*(float)L_27)), (1.0f), /*hidden argument*/NULL);
		V_0 = L_28;
		float L_29 = __this->get_minWidth_6();
		float L_30 = V_0;
		float L_31 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		float L_32 = __this->get_maxWidth_5();
		V_0 = ((float)((float)L_31*(float)L_32));
		LineRenderer_t849157671 * L_33 = __this->get_lRenderer_8();
		float L_34 = V_0;
		float L_35 = V_0;
		NullCheck(L_33);
		LineRenderer_SetWidth_m178226110(L_33, L_34, L_35, /*hidden argument*/NULL);
		PerFrameRaycast_t2437905999 * L_36 = __this->get_raycast_11();
		NullCheck(L_36);
		RaycastHit_t87180320  L_37 = VirtFuncInvoker0< RaycastHit_t87180320  >::Invoke(6 /* UnityEngine.RaycastHit PerFrameRaycast::GetHitInfo() */, L_36);
		V_1 = L_37;
		Transform_t3275118058 * L_38 = RaycastHit_get_transform_m3290290036((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_39 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_025a;
		}
	}
	{
		LineRenderer_t849157671 * L_40 = __this->get_lRenderer_8();
		float L_41 = RaycastHit_get_distance_m1178709367((&V_1), /*hidden argument*/NULL);
		Vector3_t2243707580  L_42 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		NullCheck(L_40);
		LineRenderer_SetPosition_m4048451705(L_40, 1, L_43, /*hidden argument*/NULL);
		float L_44 = RaycastHit_get_distance_m1178709367((&V_1), /*hidden argument*/NULL);
		float L_45 = ((float)((float)(0.1f)*(float)L_44));
		V_5 = L_45;
		Renderer_t257310565 * L_46 = Component_GetComponent_TisRenderer_t257310565_m692268576(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var);
		NullCheck(L_46);
		Material_t193706927 * L_47 = Renderer_get_material_m2553789785(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		Vector2_t2243707579  L_48 = Material_get_mainTextureScale_m2123078452(L_47, /*hidden argument*/NULL);
		Vector2_t2243707579  L_49 = L_48;
		V_6 = L_49;
		float L_50 = V_5;
		float L_51 = L_50;
		V_14 = L_51;
		(&V_6)->set_x_0(L_51);
		float L_52 = V_14;
		Renderer_t257310565 * L_53 = Component_GetComponent_TisRenderer_t257310565_m692268576(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var);
		NullCheck(L_53);
		Material_t193706927 * L_54 = Renderer_get_material_m2553789785(L_53, /*hidden argument*/NULL);
		Vector2_t2243707579  L_55 = V_6;
		Vector2_t2243707579  L_56 = L_55;
		V_15 = L_56;
		NullCheck(L_54);
		Material_set_mainTextureScale_m723074403(L_54, L_56, /*hidden argument*/NULL);
		Vector2_t2243707579  L_57 = V_15;
		Renderer_t257310565 * L_58 = Component_GetComponent_TisRenderer_t257310565_m692268576(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var);
		NullCheck(L_58);
		Material_t193706927 * L_59 = Renderer_get_material_m2553789785(L_58, /*hidden argument*/NULL);
		float L_60 = RaycastHit_get_distance_m1178709367((&V_1), /*hidden argument*/NULL);
		float L_61 = __this->get_noiseSize_4();
		float L_62 = __this->get_noiseSize_4();
		Vector2_t2243707579  L_63;
		memset(&L_63, 0, sizeof(L_63));
		Vector2__ctor_m3067419446(&L_63, ((float)((float)((float)((float)(0.1f)*(float)L_60))*(float)L_61)), L_62, /*hidden argument*/NULL);
		NullCheck(L_59);
		Material_SetTextureScale_m1622979841(L_59, _stringLiteral1978928756, L_63, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_64 = __this->get_pointer_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_65 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_64, /*hidden argument*/NULL);
		if (!L_65)
		{
			goto IL_0255;
		}
	}
	{
		GameObject_t1756533147 * L_66 = __this->get_pointer_7();
		NullCheck(L_66);
		Renderer_t257310565 * L_67 = GameObject_GetComponent_TisRenderer_t257310565_m2926323388(L_66, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m2926323388_MethodInfo_var);
		NullCheck(L_67);
		Renderer_set_enabled_m142717579(L_67, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_68 = __this->get_pointer_7();
		NullCheck(L_68);
		Transform_t3275118058 * L_69 = GameObject_get_transform_m909382139(L_68, /*hidden argument*/NULL);
		Vector3_t2243707580  L_70 = RaycastHit_get_point_m326143462((&V_1), /*hidden argument*/NULL);
		Transform_t3275118058 * L_71 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_71);
		Vector3_t2243707580  L_72 = Transform_get_position_m1104419803(L_71, /*hidden argument*/NULL);
		Vector3_t2243707580  L_73 = RaycastHit_get_point_m326143462((&V_1), /*hidden argument*/NULL);
		Vector3_t2243707580  L_74 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_72, L_73, /*hidden argument*/NULL);
		Vector3_t2243707580  L_75 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_74, (0.01f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_76 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_70, L_75, /*hidden argument*/NULL);
		NullCheck(L_69);
		Transform_set_position_m2469242620(L_69, L_76, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_77 = __this->get_pointer_7();
		NullCheck(L_77);
		Transform_t3275118058 * L_78 = GameObject_get_transform_m909382139(L_77, /*hidden argument*/NULL);
		Vector3_t2243707580  L_79 = RaycastHit_get_normal_m817665579((&V_1), /*hidden argument*/NULL);
		Transform_t3275118058 * L_80 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_80);
		Vector3_t2243707580  L_81 = Transform_get_up_m1603627763(L_80, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_82 = Quaternion_LookRotation_m700700634(NULL /*static, unused*/, L_79, L_81, /*hidden argument*/NULL);
		NullCheck(L_78);
		Transform_set_rotation_m3411284563(L_78, L_82, /*hidden argument*/NULL);
		float L_83 = (90.0f);
		V_7 = L_83;
		GameObject_t1756533147 * L_84 = __this->get_pointer_7();
		NullCheck(L_84);
		Transform_t3275118058 * L_85 = GameObject_get_transform_m909382139(L_84, /*hidden argument*/NULL);
		NullCheck(L_85);
		Vector3_t2243707580  L_86 = Transform_get_eulerAngles_m4066505159(L_85, /*hidden argument*/NULL);
		Vector3_t2243707580  L_87 = L_86;
		V_8 = L_87;
		float L_88 = V_7;
		float L_89 = L_88;
		V_16 = L_89;
		(&V_8)->set_x_1(L_89);
		float L_90 = V_16;
		GameObject_t1756533147 * L_91 = __this->get_pointer_7();
		NullCheck(L_91);
		Transform_t3275118058 * L_92 = GameObject_get_transform_m909382139(L_91, /*hidden argument*/NULL);
		Vector3_t2243707580  L_93 = V_8;
		Vector3_t2243707580  L_94 = L_93;
		V_17 = L_94;
		NullCheck(L_92);
		Transform_set_eulerAngles_m2881310872(L_92, L_94, /*hidden argument*/NULL);
		Vector3_t2243707580  L_95 = V_17;
	}

IL_0255:
	{
		goto IL_030c;
	}

IL_025a:
	{
		GameObject_t1756533147 * L_96 = __this->get_pointer_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_97 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_96, /*hidden argument*/NULL);
		if (!L_97)
		{
			goto IL_027b;
		}
	}
	{
		GameObject_t1756533147 * L_98 = __this->get_pointer_7();
		NullCheck(L_98);
		Renderer_t257310565 * L_99 = GameObject_GetComponent_TisRenderer_t257310565_m2926323388(L_98, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m2926323388_MethodInfo_var);
		NullCheck(L_99);
		Renderer_set_enabled_m142717579(L_99, (bool)0, /*hidden argument*/NULL);
	}

IL_027b:
	{
		V_2 = (200.0f);
		LineRenderer_t849157671 * L_100 = __this->get_lRenderer_8();
		float L_101 = V_2;
		Vector3_t2243707580  L_102 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_103 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, L_101, L_102, /*hidden argument*/NULL);
		NullCheck(L_100);
		LineRenderer_SetPosition_m4048451705(L_100, 1, L_103, /*hidden argument*/NULL);
		float L_104 = V_2;
		float L_105 = ((float)((float)(0.1f)*(float)L_104));
		V_9 = L_105;
		Renderer_t257310565 * L_106 = Component_GetComponent_TisRenderer_t257310565_m692268576(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var);
		NullCheck(L_106);
		Material_t193706927 * L_107 = Renderer_get_material_m2553789785(L_106, /*hidden argument*/NULL);
		NullCheck(L_107);
		Vector2_t2243707579  L_108 = Material_get_mainTextureScale_m2123078452(L_107, /*hidden argument*/NULL);
		Vector2_t2243707579  L_109 = L_108;
		V_10 = L_109;
		float L_110 = V_9;
		float L_111 = L_110;
		V_18 = L_111;
		(&V_10)->set_x_0(L_111);
		float L_112 = V_18;
		Renderer_t257310565 * L_113 = Component_GetComponent_TisRenderer_t257310565_m692268576(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var);
		NullCheck(L_113);
		Material_t193706927 * L_114 = Renderer_get_material_m2553789785(L_113, /*hidden argument*/NULL);
		Vector2_t2243707579  L_115 = V_10;
		Vector2_t2243707579  L_116 = L_115;
		V_19 = L_116;
		NullCheck(L_114);
		Material_set_mainTextureScale_m723074403(L_114, L_116, /*hidden argument*/NULL);
		Vector2_t2243707579  L_117 = V_19;
		Renderer_t257310565 * L_118 = Component_GetComponent_TisRenderer_t257310565_m692268576(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var);
		NullCheck(L_118);
		Material_t193706927 * L_119 = Renderer_get_material_m2553789785(L_118, /*hidden argument*/NULL);
		float L_120 = V_2;
		float L_121 = __this->get_noiseSize_4();
		float L_122 = __this->get_noiseSize_4();
		Vector2_t2243707579  L_123;
		memset(&L_123, 0, sizeof(L_123));
		Vector2__ctor_m3067419446(&L_123, ((float)((float)((float)((float)(0.1f)*(float)L_120))*(float)L_121)), L_122, /*hidden argument*/NULL);
		NullCheck(L_119);
		Material_SetTextureScale_m1622979841(L_119, _stringLiteral1978928756, L_123, /*hidden argument*/NULL);
	}

IL_030c:
	{
		return;
	}
}
// System.Void LaserScope::Main()
extern "C"  void LaserScope_Main_m1626807512 (LaserScope_t1872580701 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LaserScope/$ChoseNewAnimationTargetCoroutine$83::.ctor(LaserScope)
extern const MethodInfo* GenericGenerator_1__ctor_m1542724045_MethodInfo_var;
extern const uint32_t U24ChoseNewAnimationTargetCoroutineU2483__ctor_m4128894609_MetadataUsageId;
extern "C"  void U24ChoseNewAnimationTargetCoroutineU2483__ctor_m4128894609 (U24ChoseNewAnimationTargetCoroutineU2483_t330810268 * __this, LaserScope_t1872580701 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24ChoseNewAnimationTargetCoroutineU2483__ctor_m4128894609_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GenericGenerator_1__ctor_m1542724045(__this, /*hidden argument*/GenericGenerator_1__ctor_m1542724045_MethodInfo_var);
		LaserScope_t1872580701 * L_0 = ___self_0;
		__this->set_U24self_U2485_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> LaserScope/$ChoseNewAnimationTargetCoroutine$83::GetEnumerator()
extern Il2CppClass* U24_t1966044057_il2cpp_TypeInfo_var;
extern const uint32_t U24ChoseNewAnimationTargetCoroutineU2483_GetEnumerator_m2837683460_MetadataUsageId;
extern "C"  Il2CppObject* U24ChoseNewAnimationTargetCoroutineU2483_GetEnumerator_m2837683460 (U24ChoseNewAnimationTargetCoroutineU2483_t330810268 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24ChoseNewAnimationTargetCoroutineU2483_GetEnumerator_m2837683460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LaserScope_t1872580701 * L_0 = __this->get_U24self_U2485_0();
		U24_t1966044057 * L_1 = (U24_t1966044057 *)il2cpp_codegen_object_new(U24_t1966044057_il2cpp_TypeInfo_var);
		U24__ctor_m3177170082(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void LaserScope/$ChoseNewAnimationTargetCoroutine$83/$::.ctor(LaserScope)
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m3451731617_MethodInfo_var;
extern const uint32_t U24__ctor_m3177170082_MetadataUsageId;
extern "C"  void U24__ctor_m3177170082 (U24_t1966044057 * __this, LaserScope_t1872580701 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m3177170082_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m3451731617(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m3451731617_MethodInfo_var);
		LaserScope_t1872580701 * L_0 = ___self_0;
		__this->set_U24self_U2484_2(L_0);
		return;
	}
}
// System.Boolean LaserScope/$ChoseNewAnimationTargetCoroutine$83/$::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m3507348778_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_Yield_m1415833518_MethodInfo_var;
extern const uint32_t U24_MoveNext_m152879947_MetadataUsageId;
extern "C"  bool U24_MoveNext_m152879947 (U24_t1966044057 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m152879947_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B7_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t300505933 *)__this)->get__state_1();
		if (L_0 == 0)
		{
			goto IL_001b;
		}
		if (L_0 == 1)
		{
			goto IL_00c5;
		}
		if (L_0 == 2)
		{
			goto IL_005e;
		}
		if (L_0 == 3)
		{
			goto IL_00b8;
		}
	}

IL_001b:
	{
		goto IL_00b8;
	}

IL_0020:
	{
		LaserScope_t1872580701 * L_1 = __this->get_U24self_U2484_2();
		LaserScope_t1872580701 * L_2 = __this->get_U24self_U2484_2();
		NullCheck(L_2);
		float L_3 = L_2->get_aniDir_10();
		float L_4 = Random_Range_m2884721203(NULL /*static, unused*/, (0.5f), (1.5f), /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_aniDir_10(((float)((float)((float)((float)L_3*(float)(0.9f)))+(float)((float)((float)L_4*(float)(0.1f))))));
		bool L_5 = GenericGeneratorEnumerator_1_YieldDefault_m3507348778(__this, 2, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m3507348778_MethodInfo_var);
		G_B7_0 = ((int32_t)(L_5));
		goto IL_00c6;
	}

IL_005e:
	{
		LaserScope_t1872580701 * L_6 = __this->get_U24self_U2484_2();
		LaserScope_t1872580701 * L_7 = __this->get_U24self_U2484_2();
		NullCheck(L_7);
		float L_8 = L_7->get_minWidth_6();
		float L_9 = Random_Range_m2884721203(NULL /*static, unused*/, (0.1f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_6);
		L_6->set_minWidth_6(((float)((float)((float)((float)L_8*(float)(0.8f)))+(float)((float)((float)L_9*(float)(0.2f))))));
		float L_10 = Random_get_value_m976649312(NULL /*static, unused*/, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_11 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_11, ((float)((float)((float)((float)(1.0f)+(float)((float)((float)L_10*(float)(2.0f)))))-(float)(1.0f))), /*hidden argument*/NULL);
		bool L_12 = GenericGeneratorEnumerator_1_Yield_m1415833518(__this, 3, L_11, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m1415833518_MethodInfo_var);
		G_B7_0 = ((int32_t)(L_12));
		goto IL_00c6;
	}

IL_00b8:
	{
		goto IL_0020;
	}
	// Dead block : IL_00bd: ldarg.0

IL_00c5:
	{
		G_B7_0 = 0;
	}

IL_00c6:
	{
		return (bool)G_B7_0;
	}
}
// System.Void MechAnimation::.ctor()
extern "C"  void MechAnimation__ctor_m3295248863 (MechAnimation_t783079135 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MechAnimation::OnEnable()
extern const MethodInfo* Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var;
extern const uint32_t MechAnimation_OnEnable_m917170899_MetadataUsageId;
extern "C"  void MechAnimation_OnEnable_m917170899 (MechAnimation_t783079135 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MechAnimation_OnEnable_m917170899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody_t4233889191 * L_0 = __this->get_rigid_2();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		__this->set_tr_8(L_1);
		Animation_t2068071072 * L_2 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_3 = __this->get_idle_3();
		NullCheck(L_3);
		String_t* L_4 = Object_get_name_m2079638459(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		AnimationState_t1303741697 * L_5 = Animation_get_Item_m4198128320(L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		AnimationState_set_layer_m139053567(L_5, 0, /*hidden argument*/NULL);
		Animation_t2068071072 * L_6 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_7 = __this->get_idle_3();
		NullCheck(L_7);
		String_t* L_8 = Object_get_name_m2079638459(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		AnimationState_t1303741697 * L_9 = Animation_get_Item_m4198128320(L_6, L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		AnimationState_set_weight_m2370306600(L_9, (((float)((float)1))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_10 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_11 = __this->get_idle_3();
		NullCheck(L_11);
		String_t* L_12 = Object_get_name_m2079638459(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		AnimationState_t1303741697 * L_13 = Animation_get_Item_m4198128320(L_10, L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		AnimationState_set_enabled_m2079619927(L_13, (bool)1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_14 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_15 = __this->get_walk_4();
		NullCheck(L_15);
		String_t* L_16 = Object_get_name_m2079638459(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		AnimationState_t1303741697 * L_17 = Animation_get_Item_m4198128320(L_14, L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		AnimationState_set_layer_m139053567(L_17, 1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_18 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_19 = __this->get_turnLeft_5();
		NullCheck(L_19);
		String_t* L_20 = Object_get_name_m2079638459(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		AnimationState_t1303741697 * L_21 = Animation_get_Item_m4198128320(L_18, L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		AnimationState_set_layer_m139053567(L_21, 1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_22 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_23 = __this->get_turnRight_6();
		NullCheck(L_23);
		String_t* L_24 = Object_get_name_m2079638459(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		AnimationState_t1303741697 * L_25 = Animation_get_Item_m4198128320(L_22, L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		AnimationState_set_layer_m139053567(L_25, 1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_26 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_27 = __this->get_walk_4();
		NullCheck(L_27);
		String_t* L_28 = Object_get_name_m2079638459(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		AnimationState_t1303741697 * L_29 = Animation_get_Item_m4198128320(L_26, L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		AnimationState_set_weight_m2370306600(L_29, (((float)((float)1))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_30 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_31 = __this->get_turnLeft_5();
		NullCheck(L_31);
		String_t* L_32 = Object_get_name_m2079638459(L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		AnimationState_t1303741697 * L_33 = Animation_get_Item_m4198128320(L_30, L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		AnimationState_set_weight_m2370306600(L_33, (((float)((float)0))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_34 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_35 = __this->get_turnRight_6();
		NullCheck(L_35);
		String_t* L_36 = Object_get_name_m2079638459(L_35, /*hidden argument*/NULL);
		NullCheck(L_34);
		AnimationState_t1303741697 * L_37 = Animation_get_Item_m4198128320(L_34, L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		AnimationState_set_weight_m2370306600(L_37, (((float)((float)0))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_38 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_39 = __this->get_walk_4();
		NullCheck(L_39);
		String_t* L_40 = Object_get_name_m2079638459(L_39, /*hidden argument*/NULL);
		NullCheck(L_38);
		AnimationState_t1303741697 * L_41 = Animation_get_Item_m4198128320(L_38, L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		AnimationState_set_enabled_m2079619927(L_41, (bool)1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_42 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_43 = __this->get_turnLeft_5();
		NullCheck(L_43);
		String_t* L_44 = Object_get_name_m2079638459(L_43, /*hidden argument*/NULL);
		NullCheck(L_42);
		AnimationState_t1303741697 * L_45 = Animation_get_Item_m4198128320(L_42, L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		AnimationState_set_enabled_m2079619927(L_45, (bool)1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_46 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_47 = __this->get_turnRight_6();
		NullCheck(L_47);
		String_t* L_48 = Object_get_name_m2079638459(L_47, /*hidden argument*/NULL);
		NullCheck(L_46);
		AnimationState_t1303741697 * L_49 = Animation_get_Item_m4198128320(L_46, L_48, /*hidden argument*/NULL);
		NullCheck(L_49);
		AnimationState_set_enabled_m2079619927(L_49, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MechAnimation::FixedUpdate()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var;
extern const uint32_t MechAnimation_FixedUpdate_m1765434400_MetadataUsageId;
extern "C"  void MechAnimation_FixedUpdate_m1765434400 (MechAnimation_t783079135 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MechAnimation_FixedUpdate_m1765434400_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		Rigidbody_t4233889191 * L_0 = __this->get_rigid_2();
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Rigidbody_get_angularVelocity_m3820121000(L_0, /*hidden argument*/NULL);
		V_4 = L_1;
		float L_2 = (&V_4)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = fabsf(L_2);
		V_0 = ((float)((float)((float)((float)L_3*(float)(57.29578f)))/(float)(100.0f)));
		Rigidbody_t4233889191 * L_4 = __this->get_rigid_2();
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Rigidbody_get_velocity_m2022666970(L_4, /*hidden argument*/NULL);
		V_5 = L_5;
		float L_6 = Vector3_get_magnitude_m860342598((&V_5), /*hidden argument*/NULL);
		V_1 = ((float)((float)L_6/(float)(2.5f)));
		Rigidbody_t4233889191 * L_7 = __this->get_rigid_2();
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = Rigidbody_get_angularVelocity_m3820121000(L_7, /*hidden argument*/NULL);
		V_6 = L_8;
		float L_9 = (&V_6)->get_y_2();
		float L_10 = Mathf_Sign_m2039143327(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		Animation_t2068071072 * L_11 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_12 = __this->get_walk_4();
		NullCheck(L_12);
		String_t* L_13 = Object_get_name_m2079638459(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		AnimationState_t1303741697 * L_14 = Animation_get_Item_m4198128320(L_11, L_13, /*hidden argument*/NULL);
		Animation_t2068071072 * L_15 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_16 = __this->get_walk_4();
		NullCheck(L_16);
		String_t* L_17 = Object_get_name_m2079638459(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		AnimationState_t1303741697 * L_18 = Animation_get_Item_m4198128320(L_15, L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		float L_19 = AnimationState_get_length_m2895238571(L_18, /*hidden argument*/NULL);
		Animation_t2068071072 * L_20 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_21 = __this->get_turnLeft_5();
		NullCheck(L_21);
		String_t* L_22 = Object_get_name_m2079638459(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		AnimationState_t1303741697 * L_23 = Animation_get_Item_m4198128320(L_20, L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		float L_24 = AnimationState_get_length_m2895238571(L_23, /*hidden argument*/NULL);
		float L_25 = V_0;
		float L_26 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, (1.0f), ((float)((float)((float)((float)L_19/(float)L_24))*(float)(1.33f))), L_25, /*hidden argument*/NULL);
		NullCheck(L_14);
		AnimationState_set_speed_m465014523(L_14, L_26, /*hidden argument*/NULL);
		Animation_t2068071072 * L_27 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_28 = __this->get_turnLeft_5();
		NullCheck(L_28);
		String_t* L_29 = Object_get_name_m2079638459(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		AnimationState_t1303741697 * L_30 = Animation_get_Item_m4198128320(L_27, L_29, /*hidden argument*/NULL);
		Animation_t2068071072 * L_31 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_32 = __this->get_walk_4();
		NullCheck(L_32);
		String_t* L_33 = Object_get_name_m2079638459(L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		AnimationState_t1303741697 * L_34 = Animation_get_Item_m4198128320(L_31, L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		float L_35 = AnimationState_get_time_m2280025052(L_34, /*hidden argument*/NULL);
		NullCheck(L_30);
		AnimationState_set_time_m1882411177(L_30, L_35, /*hidden argument*/NULL);
		Animation_t2068071072 * L_36 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_37 = __this->get_turnRight_6();
		NullCheck(L_37);
		String_t* L_38 = Object_get_name_m2079638459(L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		AnimationState_t1303741697 * L_39 = Animation_get_Item_m4198128320(L_36, L_38, /*hidden argument*/NULL);
		Animation_t2068071072 * L_40 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_41 = __this->get_walk_4();
		NullCheck(L_41);
		String_t* L_42 = Object_get_name_m2079638459(L_41, /*hidden argument*/NULL);
		NullCheck(L_40);
		AnimationState_t1303741697 * L_43 = Animation_get_Item_m4198128320(L_40, L_42, /*hidden argument*/NULL);
		NullCheck(L_43);
		float L_44 = AnimationState_get_time_m2280025052(L_43, /*hidden argument*/NULL);
		NullCheck(L_39);
		AnimationState_set_time_m1882411177(L_39, L_44, /*hidden argument*/NULL);
		Animation_t2068071072 * L_45 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_46 = __this->get_turnLeft_5();
		NullCheck(L_46);
		String_t* L_47 = Object_get_name_m2079638459(L_46, /*hidden argument*/NULL);
		NullCheck(L_45);
		AnimationState_t1303741697 * L_48 = Animation_get_Item_m4198128320(L_45, L_47, /*hidden argument*/NULL);
		float L_49 = V_0;
		float L_50 = V_2;
		float L_51 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, ((float)((float)((-L_49))*(float)L_50)), /*hidden argument*/NULL);
		NullCheck(L_48);
		AnimationState_set_weight_m2370306600(L_48, L_51, /*hidden argument*/NULL);
		Animation_t2068071072 * L_52 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_53 = __this->get_turnRight_6();
		NullCheck(L_53);
		String_t* L_54 = Object_get_name_m2079638459(L_53, /*hidden argument*/NULL);
		NullCheck(L_52);
		AnimationState_t1303741697 * L_55 = Animation_get_Item_m4198128320(L_52, L_54, /*hidden argument*/NULL);
		float L_56 = V_0;
		float L_57 = V_2;
		float L_58 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, ((float)((float)L_56*(float)L_57)), /*hidden argument*/NULL);
		NullCheck(L_55);
		AnimationState_set_weight_m2370306600(L_55, L_58, /*hidden argument*/NULL);
		Animation_t2068071072 * L_59 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_60 = __this->get_walk_4();
		NullCheck(L_60);
		String_t* L_61 = Object_get_name_m2079638459(L_60, /*hidden argument*/NULL);
		NullCheck(L_59);
		AnimationState_t1303741697 * L_62 = Animation_get_Item_m4198128320(L_59, L_61, /*hidden argument*/NULL);
		float L_63 = V_1;
		float L_64 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, L_63, /*hidden argument*/NULL);
		NullCheck(L_62);
		AnimationState_set_weight_m2370306600(L_62, L_64, /*hidden argument*/NULL);
		float L_65 = V_1;
		float L_66 = V_0;
		if ((((float)((float)((float)L_65+(float)L_66))) <= ((float)(0.1f))))
		{
			goto IL_020b;
		}
	}
	{
		Animation_t2068071072 * L_67 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_68 = __this->get_walk_4();
		NullCheck(L_68);
		String_t* L_69 = Object_get_name_m2079638459(L_68, /*hidden argument*/NULL);
		NullCheck(L_67);
		AnimationState_t1303741697 * L_70 = Animation_get_Item_m4198128320(L_67, L_69, /*hidden argument*/NULL);
		NullCheck(L_70);
		float L_71 = AnimationState_get_normalizedTime_m3003675227(L_70, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_72 = Mathf_Repeat_m943844734(NULL /*static, unused*/, ((float)((float)((float)((float)L_71*(float)(((float)((float)2)))))+(float)(0.1f))), (((float)((float)1))), /*hidden argument*/NULL);
		V_3 = L_72;
		float L_73 = V_3;
		float L_74 = __this->get_lastAnimTime_10();
		if ((((float)L_73) >= ((float)L_74)))
		{
			goto IL_0204;
		}
	}
	{
		float L_75 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_76 = __this->get_lastFootstepTime_9();
		if ((((float)L_75) <= ((float)((float)((float)L_76+(float)(0.1f))))))
		{
			goto IL_0204;
		}
	}
	{
		SignalSender_t1204926691 * L_77 = __this->get_footstepSignals_7();
		NullCheck(L_77);
		VirtActionInvoker1< MonoBehaviour_t1158329972 * >::Invoke(4 /* System.Void SignalSender::SendSignals(UnityEngine.MonoBehaviour) */, L_77, __this);
		float L_78 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastFootstepTime_9(L_78);
	}

IL_0204:
	{
		float L_79 = V_3;
		__this->set_lastAnimTime_10(L_79);
	}

IL_020b:
	{
		return;
	}
}
// System.Void MechAnimation::Main()
extern "C"  void MechAnimation_Main_m1444211198 (MechAnimation_t783079135 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MechAnimationTest::.ctor()
extern "C"  void MechAnimationTest__ctor_m545631909 (MechAnimationTest_t372588337 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MechAnimationTest::OnEnable()
extern const MethodInfo* Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var;
extern const uint32_t MechAnimationTest_OnEnable_m2162304133_MetadataUsageId;
extern "C"  void MechAnimationTest_OnEnable_m2162304133 (MechAnimationTest_t372588337 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MechAnimationTest_OnEnable_m2162304133_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animation_t2068071072 * L_0 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_1 = __this->get_idle_6();
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m2079638459(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		AnimationState_t1303741697 * L_3 = Animation_get_Item_m4198128320(L_0, L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		AnimationState_set_layer_m139053567(L_3, 0, /*hidden argument*/NULL);
		Animation_t2068071072 * L_4 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_5 = __this->get_idle_6();
		NullCheck(L_5);
		String_t* L_6 = Object_get_name_m2079638459(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		AnimationState_t1303741697 * L_7 = Animation_get_Item_m4198128320(L_4, L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		AnimationState_set_weight_m2370306600(L_7, (((float)((float)1))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_8 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_9 = __this->get_idle_6();
		NullCheck(L_9);
		String_t* L_10 = Object_get_name_m2079638459(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		AnimationState_t1303741697 * L_11 = Animation_get_Item_m4198128320(L_8, L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		AnimationState_set_enabled_m2079619927(L_11, (bool)1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_12 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_13 = __this->get_walk_7();
		NullCheck(L_13);
		String_t* L_14 = Object_get_name_m2079638459(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		AnimationState_t1303741697 * L_15 = Animation_get_Item_m4198128320(L_12, L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		AnimationState_set_layer_m139053567(L_15, 1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_16 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_17 = __this->get_turnLeft_8();
		NullCheck(L_17);
		String_t* L_18 = Object_get_name_m2079638459(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		AnimationState_t1303741697 * L_19 = Animation_get_Item_m4198128320(L_16, L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		AnimationState_set_layer_m139053567(L_19, 1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_20 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_21 = __this->get_turnRight_9();
		NullCheck(L_21);
		String_t* L_22 = Object_get_name_m2079638459(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		AnimationState_t1303741697 * L_23 = Animation_get_Item_m4198128320(L_20, L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		AnimationState_set_layer_m139053567(L_23, 1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_24 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_25 = __this->get_walk_7();
		NullCheck(L_25);
		String_t* L_26 = Object_get_name_m2079638459(L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		AnimationState_t1303741697 * L_27 = Animation_get_Item_m4198128320(L_24, L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		AnimationState_set_weight_m2370306600(L_27, (((float)((float)1))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_28 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_29 = __this->get_turnLeft_8();
		NullCheck(L_29);
		String_t* L_30 = Object_get_name_m2079638459(L_29, /*hidden argument*/NULL);
		NullCheck(L_28);
		AnimationState_t1303741697 * L_31 = Animation_get_Item_m4198128320(L_28, L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		AnimationState_set_weight_m2370306600(L_31, (((float)((float)0))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_32 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_33 = __this->get_turnRight_9();
		NullCheck(L_33);
		String_t* L_34 = Object_get_name_m2079638459(L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		AnimationState_t1303741697 * L_35 = Animation_get_Item_m4198128320(L_32, L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		AnimationState_set_weight_m2370306600(L_35, (((float)((float)0))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_36 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_37 = __this->get_walk_7();
		NullCheck(L_37);
		String_t* L_38 = Object_get_name_m2079638459(L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		AnimationState_t1303741697 * L_39 = Animation_get_Item_m4198128320(L_36, L_38, /*hidden argument*/NULL);
		NullCheck(L_39);
		AnimationState_set_enabled_m2079619927(L_39, (bool)1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_40 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_41 = __this->get_turnLeft_8();
		NullCheck(L_41);
		String_t* L_42 = Object_get_name_m2079638459(L_41, /*hidden argument*/NULL);
		NullCheck(L_40);
		AnimationState_t1303741697 * L_43 = Animation_get_Item_m4198128320(L_40, L_42, /*hidden argument*/NULL);
		NullCheck(L_43);
		AnimationState_set_enabled_m2079619927(L_43, (bool)1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_44 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_45 = __this->get_turnRight_9();
		NullCheck(L_45);
		String_t* L_46 = Object_get_name_m2079638459(L_45, /*hidden argument*/NULL);
		NullCheck(L_44);
		AnimationState_t1303741697 * L_47 = Animation_get_Item_m4198128320(L_44, L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		AnimationState_set_enabled_m2079619927(L_47, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MechAnimationTest::FixedUpdate()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var;
extern const uint32_t MechAnimationTest_FixedUpdate_m3928532348_MetadataUsageId;
extern "C"  void MechAnimationTest_FixedUpdate_m3928532348 (MechAnimationTest_t372588337 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MechAnimationTest_FixedUpdate_m3928532348_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Animation_t2068071072 * L_0 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_1 = __this->get_walk_7();
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m2079638459(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		AnimationState_t1303741697 * L_3 = Animation_get_Item_m4198128320(L_0, L_2, /*hidden argument*/NULL);
		Animation_t2068071072 * L_4 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_5 = __this->get_walk_7();
		NullCheck(L_5);
		String_t* L_6 = Object_get_name_m2079638459(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		AnimationState_t1303741697 * L_7 = Animation_get_Item_m4198128320(L_4, L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		float L_8 = AnimationState_get_length_m2895238571(L_7, /*hidden argument*/NULL);
		Animation_t2068071072 * L_9 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_10 = __this->get_turnLeft_8();
		NullCheck(L_10);
		String_t* L_11 = Object_get_name_m2079638459(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		AnimationState_t1303741697 * L_12 = Animation_get_Item_m4198128320(L_9, L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		float L_13 = AnimationState_get_length_m2895238571(L_12, /*hidden argument*/NULL);
		float L_14 = __this->get_turning_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_15 = fabsf(L_14);
		float L_16 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, (((float)((float)1))), ((float)((float)L_8/(float)L_13)), L_15, /*hidden argument*/NULL);
		NullCheck(L_3);
		AnimationState_set_speed_m465014523(L_3, L_16, /*hidden argument*/NULL);
		Animation_t2068071072 * L_17 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_18 = __this->get_turnLeft_8();
		NullCheck(L_18);
		String_t* L_19 = Object_get_name_m2079638459(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		AnimationState_t1303741697 * L_20 = Animation_get_Item_m4198128320(L_17, L_19, /*hidden argument*/NULL);
		Animation_t2068071072 * L_21 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_22 = __this->get_walk_7();
		NullCheck(L_22);
		String_t* L_23 = Object_get_name_m2079638459(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		AnimationState_t1303741697 * L_24 = Animation_get_Item_m4198128320(L_21, L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		float L_25 = AnimationState_get_time_m2280025052(L_24, /*hidden argument*/NULL);
		float L_26 = __this->get_turnOffset_4();
		NullCheck(L_20);
		AnimationState_set_time_m1882411177(L_20, ((float)((float)L_25+(float)L_26)), /*hidden argument*/NULL);
		Animation_t2068071072 * L_27 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_28 = __this->get_turnRight_9();
		NullCheck(L_28);
		String_t* L_29 = Object_get_name_m2079638459(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		AnimationState_t1303741697 * L_30 = Animation_get_Item_m4198128320(L_27, L_29, /*hidden argument*/NULL);
		Animation_t2068071072 * L_31 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_32 = __this->get_walk_7();
		NullCheck(L_32);
		String_t* L_33 = Object_get_name_m2079638459(L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		AnimationState_t1303741697 * L_34 = Animation_get_Item_m4198128320(L_31, L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		float L_35 = AnimationState_get_time_m2280025052(L_34, /*hidden argument*/NULL);
		float L_36 = __this->get_turnOffset_4();
		NullCheck(L_30);
		AnimationState_set_time_m1882411177(L_30, ((float)((float)L_35+(float)L_36)), /*hidden argument*/NULL);
		Rigidbody_t4233889191 * L_37 = __this->get_rigid_5();
		Rigidbody_t4233889191 * L_38 = __this->get_rigid_5();
		NullCheck(L_38);
		Transform_t3275118058 * L_39 = Component_get_transform_m2697483695(L_38, /*hidden argument*/NULL);
		NullCheck(L_39);
		Vector3_t2243707580  L_40 = Transform_get_forward_m1833488937(L_39, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_40, (2.5f), /*hidden argument*/NULL);
		float L_42 = __this->get_walking_3();
		Vector3_t2243707580  L_43 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		NullCheck(L_37);
		Rigidbody_set_velocity_m2514070071(L_37, L_43, /*hidden argument*/NULL);
		Rigidbody_t4233889191 * L_44 = __this->get_rigid_5();
		Vector3_t2243707580  L_45 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_46 = __this->get_turning_2();
		Vector3_t2243707580  L_47 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
		Vector3_t2243707580  L_48 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_47, (((float)((float)((int32_t)100)))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_49 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_48, (0.0174532924f), /*hidden argument*/NULL);
		NullCheck(L_44);
		Rigidbody_set_angularVelocity_m824394045(L_44, L_49, /*hidden argument*/NULL);
		Rigidbody_t4233889191 * L_50 = __this->get_rigid_5();
		NullCheck(L_50);
		Vector3_t2243707580  L_51 = Rigidbody_get_angularVelocity_m3820121000(L_50, /*hidden argument*/NULL);
		V_2 = L_51;
		float L_52 = (&V_2)->get_y_2();
		V_0 = ((float)((float)((float)((float)L_52*(float)(57.29578f)))/(float)(100.0f)));
		Rigidbody_t4233889191 * L_53 = __this->get_rigid_5();
		NullCheck(L_53);
		Vector3_t2243707580  L_54 = Rigidbody_get_velocity_m2022666970(L_53, /*hidden argument*/NULL);
		V_3 = L_54;
		float L_55 = Vector3_get_magnitude_m860342598((&V_3), /*hidden argument*/NULL);
		V_1 = ((float)((float)L_55/(float)(2.5f)));
		Animation_t2068071072 * L_56 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_57 = __this->get_turnLeft_8();
		NullCheck(L_57);
		String_t* L_58 = Object_get_name_m2079638459(L_57, /*hidden argument*/NULL);
		NullCheck(L_56);
		AnimationState_t1303741697 * L_59 = Animation_get_Item_m4198128320(L_56, L_58, /*hidden argument*/NULL);
		float L_60 = V_0;
		float L_61 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, ((-L_60)), /*hidden argument*/NULL);
		NullCheck(L_59);
		AnimationState_set_weight_m2370306600(L_59, L_61, /*hidden argument*/NULL);
		Animation_t2068071072 * L_62 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_63 = __this->get_turnRight_9();
		NullCheck(L_63);
		String_t* L_64 = Object_get_name_m2079638459(L_63, /*hidden argument*/NULL);
		NullCheck(L_62);
		AnimationState_t1303741697 * L_65 = Animation_get_Item_m4198128320(L_62, L_64, /*hidden argument*/NULL);
		float L_66 = V_0;
		float L_67 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
		NullCheck(L_65);
		AnimationState_set_weight_m2370306600(L_65, L_67, /*hidden argument*/NULL);
		Animation_t2068071072 * L_68 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_69 = __this->get_walk_7();
		NullCheck(L_69);
		String_t* L_70 = Object_get_name_m2079638459(L_69, /*hidden argument*/NULL);
		NullCheck(L_68);
		AnimationState_t1303741697 * L_71 = Animation_get_Item_m4198128320(L_68, L_70, /*hidden argument*/NULL);
		float L_72 = V_1;
		float L_73 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, L_72, /*hidden argument*/NULL);
		NullCheck(L_71);
		AnimationState_set_weight_m2370306600(L_71, L_73, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MechAnimationTest::OnGUI()
extern Il2CppClass* RuntimeServices_t1910041954_il2cpp_TypeInfo_var;
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3501618936;
extern Il2CppCodeGenString* _stringLiteral2562712940;
extern Il2CppCodeGenString* _stringLiteral932773414;
extern Il2CppCodeGenString* _stringLiteral2991604180;
extern const uint32_t MechAnimationTest_OnGUI_m1660128167_MetadataUsageId;
extern "C"  void MechAnimationTest_OnGUI_m1660128167 (MechAnimationTest_t372588337 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MechAnimationTest_OnGUI_m1660128167_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float* L_0 = __this->get_address_of_walking_3();
		String_t* L_1 = Single_ToString_m2359963436(L_0, _stringLiteral2562712940, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		String_t* L_2 = RuntimeServices_op_Addition_m1630013314(NULL /*static, unused*/, _stringLiteral3501618936, L_1, /*hidden argument*/NULL);
		GUILayout_Label_m3466110979(NULL /*static, unused*/, L_2, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		float L_3 = __this->get_walking_3();
		GUILayoutOptionU5BU5D_t2108882777* L_4 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)1));
		GUILayoutOption_t4183744904 * L_5 = GUILayout_Width_m261136689(NULL /*static, unused*/, (((float)((float)((int32_t)100)))), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_5);
		float L_6 = GUILayout_HorizontalSlider_m2178689167(NULL /*static, unused*/, L_3, (((float)((float)0))), (((float)((float)1))), L_4, /*hidden argument*/NULL);
		__this->set_walking_3(L_6);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_7 = GUI_get_changed_m296124577(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0088;
		}
	}
	{
		float L_8 = __this->get_turning_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_9 = fabsf(L_8);
		float L_10 = __this->get_walking_3();
		float L_11 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_9, (((float)((float)0))), ((float)((float)(((float)((float)1)))-(float)L_10)), /*hidden argument*/NULL);
		float L_12 = __this->get_turning_2();
		float L_13 = Mathf_Sign_m2039143327(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		__this->set_turning_2(((float)((float)L_11*(float)L_13)));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_changed_m470833806(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
	}

IL_0088:
	{
		float* L_14 = __this->get_address_of_turning_2();
		String_t* L_15 = Single_ToString_m2359963436(L_14, _stringLiteral2562712940, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		String_t* L_16 = RuntimeServices_op_Addition_m1630013314(NULL /*static, unused*/, _stringLiteral932773414, L_15, /*hidden argument*/NULL);
		GUILayout_Label_m3466110979(NULL /*static, unused*/, L_16, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		float L_17 = __this->get_turning_2();
		GUILayoutOptionU5BU5D_t2108882777* L_18 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)1));
		GUILayoutOption_t4183744904 * L_19 = GUILayout_Width_m261136689(NULL /*static, unused*/, (((float)((float)((int32_t)100)))), /*hidden argument*/NULL);
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_19);
		float L_20 = GUILayout_HorizontalSlider_m2178689167(NULL /*static, unused*/, L_17, (((float)((float)(-1)))), (((float)((float)1))), L_18, /*hidden argument*/NULL);
		__this->set_turning_2(L_20);
		float L_21 = __this->get_turning_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_22 = fabsf(L_21);
		if ((((float)L_22) >= ((float)(0.1f))))
		{
			goto IL_00f0;
		}
	}
	{
		__this->set_turning_2((((float)((float)0))));
	}

IL_00f0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_23 = GUI_get_changed_m296124577(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0121;
		}
	}
	{
		float L_24 = __this->get_walking_3();
		float L_25 = __this->get_turning_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_26 = fabsf(L_25);
		float L_27 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_24, (((float)((float)0))), ((float)((float)(((float)((float)1)))-(float)L_26)), /*hidden argument*/NULL);
		__this->set_walking_3(L_27);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_changed_m470833806(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
	}

IL_0121:
	{
		float* L_28 = __this->get_address_of_turnOffset_4();
		String_t* L_29 = Single_ToString_m2359963436(L_28, _stringLiteral2562712940, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		String_t* L_30 = RuntimeServices_op_Addition_m1630013314(NULL /*static, unused*/, _stringLiteral2991604180, L_29, /*hidden argument*/NULL);
		GUILayout_Label_m3466110979(NULL /*static, unused*/, L_30, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		float L_31 = __this->get_turnOffset_4();
		GUILayoutOptionU5BU5D_t2108882777* L_32 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)1));
		GUILayoutOption_t4183744904 * L_33 = GUILayout_Width_m261136689(NULL /*static, unused*/, (((float)((float)((int32_t)100)))), /*hidden argument*/NULL);
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, L_33);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_33);
		float L_34 = GUILayout_HorizontalSlider_m2178689167(NULL /*static, unused*/, L_31, (-0.5f), (0.5f), L_32, /*hidden argument*/NULL);
		__this->set_turnOffset_4(L_34);
		float L_35 = __this->get_turnOffset_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_36 = fabsf(L_35);
		if ((((float)L_36) >= ((float)(0.05f))))
		{
			goto IL_018f;
		}
	}
	{
		__this->set_turnOffset_4((((float)((float)0))));
	}

IL_018f:
	{
		return;
	}
}
// System.Void MechAnimationTest::Main()
extern "C"  void MechAnimationTest_Main_m3420166506 (MechAnimationTest_t372588337 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MoveAnimation::.ctor()
extern "C"  void MoveAnimation__ctor_m2355953137 (MoveAnimation_t3061523661 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoveAnimation::Init()
extern "C"  void MoveAnimation_Init_m3867678363 (MoveAnimation_t3061523661 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580 * L_0 = __this->get_address_of_velocity_1();
		L_0->set_y_2((((float)((float)0))));
		Vector3_t2243707580 * L_1 = __this->get_address_of_velocity_1();
		float L_2 = Vector3_get_magnitude_m860342598(L_1, /*hidden argument*/NULL);
		__this->set_speed_4(L_2);
		Vector3_t2243707580  L_3 = __this->get_velocity_1();
		float L_4 = PlayerAnimation_HorizontalAngle_m2440304315(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->set_angle_5(L_4);
		return;
	}
}
// System.Void MovementMotor::.ctor()
extern "C"  void MovementMotor__ctor_m3335335718 (MovementMotor_t1612021398 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MovementMotor::Main()
extern "C"  void MovementMotor_Main_m3371820361 (MovementMotor_t1612021398 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MuzzleFlashAnimate::.ctor()
extern "C"  void MuzzleFlashAnimate__ctor_m3823994638 (MuzzleFlashAnimate_t122292636 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MuzzleFlashAnimate::Update()
extern "C"  void MuzzleFlashAnimate_Update_m556700145 (MuzzleFlashAnimate_t122292636 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = Random_Range_m2884721203(NULL /*static, unused*/, (0.5f), (1.5f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_localScale_m2325460848(L_0, L_3, /*hidden argument*/NULL);
		float L_4 = Random_Range_m2884721203(NULL /*static, unused*/, (((float)((float)0))), (90.0f), /*hidden argument*/NULL);
		float L_5 = L_4;
		V_0 = L_5;
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_localEulerAngles_m4231787854(L_6, /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = L_7;
		V_1 = L_8;
		float L_9 = V_0;
		float L_10 = L_9;
		V_2 = L_10;
		(&V_1)->set_z_3(L_10);
		float L_11 = V_2;
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_13 = V_1;
		Vector3_t2243707580  L_14 = L_13;
		V_3 = L_14;
		NullCheck(L_12);
		Transform_set_localEulerAngles_m2927195985(L_12, L_14, /*hidden argument*/NULL);
		Vector3_t2243707580  L_15 = V_3;
		return;
	}
}
// System.Void MuzzleFlashAnimate::Main()
extern "C"  void MuzzleFlashAnimate_Main_m967009555 (MuzzleFlashAnimate_t122292636 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ObjectCache::.ctor()
extern "C"  void ObjectCache__ctor_m2281047 (ObjectCache_t960934699 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_cacheSize_1(((int32_t)10));
		return;
	}
}
// System.Void ObjectCache::Initialize()
extern Il2CppClass* GameObjectU5BU5D_t3057952154_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimeServices_t1910041954_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern const uint32_t ObjectCache_Initialize_m278030859_MetadataUsageId;
extern "C"  void ObjectCache_Initialize_m278030859 (ObjectCache_t960934699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectCache_Initialize_m278030859_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_cacheSize_1();
		__this->set_objects_2(((GameObjectU5BU5D_t3057952154*)SZArrayNew(GameObjectU5BU5D_t3057952154_il2cpp_TypeInfo_var, (uint32_t)L_0)));
		V_0 = 0;
		goto IL_0067;
	}

IL_0018:
	{
		GameObjectU5BU5D_t3057952154* L_1 = __this->get_objects_2();
		int32_t L_2 = V_0;
		GameObject_t1756533147 * L_3 = __this->get_prefab_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_4 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_3, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, ((GameObject_t1756533147 *)IsInstSealed(L_4, GameObject_t1756533147_il2cpp_TypeInfo_var)));
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (GameObject_t1756533147 *)((GameObject_t1756533147 *)IsInstSealed(L_4, GameObject_t1756533147_il2cpp_TypeInfo_var)));
		GameObjectU5BU5D_t3057952154* L_5 = __this->get_objects_2();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		GameObject_t1756533147 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		GameObject_SetActive_m2887581199(L_8, (bool)0, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3057952154* L_9 = __this->get_objects_2();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		GameObject_t1756533147 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		GameObjectU5BU5D_t3057952154* L_13 = __this->get_objects_2();
		int32_t L_14 = V_0;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		GameObject_t1756533147 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		String_t* L_17 = Object_get_name_m2079638459(L_16, /*hidden argument*/NULL);
		int32_t L_18 = V_0;
		int32_t L_19 = L_18;
		Il2CppObject * L_20 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_19);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		String_t* L_21 = RuntimeServices_op_Addition_m3403021532(NULL /*static, unused*/, L_17, L_20, /*hidden argument*/NULL);
		NullCheck(L_12);
		Object_set_name_m4157836998(L_12, L_21, /*hidden argument*/NULL);
		int32_t L_22 = V_0;
		V_0 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0067:
	{
		int32_t L_23 = V_0;
		int32_t L_24 = __this->get_cacheSize_1();
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0018;
		}
	}
	{
		return;
	}
}
// UnityEngine.GameObject ObjectCache::GetNextObjectInCache()
extern Il2CppClass* RuntimeServices_t1910041954_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral172229322;
extern Il2CppCodeGenString* _stringLiteral274058865;
extern Il2CppCodeGenString* _stringLiteral3863905929;
extern const uint32_t ObjectCache_GetNextObjectInCache_m1909089351_MetadataUsageId;
extern "C"  GameObject_t1756533147 * ObjectCache_GetNextObjectInCache_m1909089351 (ObjectCache_t960934699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectCache_GetNextObjectInCache_m1909089351_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		V_0 = (GameObject_t1756533147 *)NULL;
		V_1 = 0;
		goto IL_0040;
	}

IL_0009:
	{
		GameObjectU5BU5D_t3057952154* L_0 = __this->get_objects_2();
		int32_t L_1 = __this->get_cacheIndex_3();
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_t1756533147 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_0 = L_3;
		GameObject_t1756533147 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = GameObject_get_activeSelf_m313590879(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0027;
		}
	}
	{
		goto IL_004c;
	}

IL_0027:
	{
		int32_t L_6 = __this->get_cacheIndex_3();
		int32_t L_7 = __this->get_cacheSize_1();
		__this->set_cacheIndex_3(((int32_t)((int32_t)((int32_t)((int32_t)L_6+(int32_t)1))%(int32_t)L_7)));
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = __this->get_cacheSize_1();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0009;
		}
	}

IL_004c:
	{
		GameObject_t1756533147 * L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = GameObject_get_activeSelf_m313590879(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_009c;
		}
	}
	{
		GameObject_t1756533147 * L_13 = __this->get_prefab_0();
		NullCheck(L_13);
		String_t* L_14 = Object_get_name_m2079638459(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		String_t* L_15 = RuntimeServices_op_Addition_m1630013314(NULL /*static, unused*/, _stringLiteral172229322, L_14, /*hidden argument*/NULL);
		String_t* L_16 = RuntimeServices_op_Addition_m1630013314(NULL /*static, unused*/, L_15, _stringLiteral274058865, /*hidden argument*/NULL);
		int32_t L_17 = __this->get_cacheSize_1();
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_18);
		String_t* L_20 = RuntimeServices_op_Addition_m3403021532(NULL /*static, unused*/, L_16, L_19, /*hidden argument*/NULL);
		String_t* L_21 = RuntimeServices_op_Addition_m1630013314(NULL /*static, unused*/, L_20, _stringLiteral3863905929, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_22 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m1280021602(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_23 = V_0;
		Spawner_Destroy_m590425740(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
	}

IL_009c:
	{
		int32_t L_24 = __this->get_cacheIndex_3();
		int32_t L_25 = __this->get_cacheSize_1();
		__this->set_cacheIndex_3(((int32_t)((int32_t)((int32_t)((int32_t)L_24+(int32_t)1))%(int32_t)L_25)));
		GameObject_t1756533147 * L_26 = V_0;
		return L_26;
	}
}
// System.Void PatrolPoint::.ctor()
extern "C"  void PatrolPoint__ctor_m3383233708 (PatrolPoint_t2056099796 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PatrolPoint::Awake()
extern "C"  void PatrolPoint_Awake_m4014645279 (PatrolPoint_t2056099796 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		__this->set_position_2(L_1);
		return;
	}
}
// System.Void PatrolPoint::Main()
extern "C"  void PatrolPoint_Main_m1256176165 (PatrolPoint_t2056099796 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PerFrameRaycast::.ctor()
extern "C"  void PerFrameRaycast__ctor_m1344158147 (PerFrameRaycast_t2437905999 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PerFrameRaycast::Awake()
extern "C"  void PerFrameRaycast_Awake_m3754006566 (PerFrameRaycast_t2437905999 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		__this->set_tr_3(L_0);
		return;
	}
}
// System.Void PerFrameRaycast::Update()
extern Il2CppClass* RaycastHit_t87180320_il2cpp_TypeInfo_var;
extern const uint32_t PerFrameRaycast_Update_m2621169380_MetadataUsageId;
extern "C"  void PerFrameRaycast_Update_m2621169380 (PerFrameRaycast_t2437905999 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PerFrameRaycast_Update_m2621169380_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit_t87180320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (RaycastHit_t87180320_il2cpp_TypeInfo_var, (&V_0));
		RaycastHit_t87180320  L_0 = V_0;
		__this->set_hitInfo_2(L_0);
		Transform_t3275118058 * L_1 = __this->get_tr_3();
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = __this->get_tr_3();
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_forward_m1833488937(L_3, /*hidden argument*/NULL);
		RaycastHit_t87180320 * L_5 = __this->get_address_of_hitInfo_2();
		Physics_Raycast_m4027183840(NULL /*static, unused*/, L_2, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RaycastHit PerFrameRaycast::GetHitInfo()
extern "C"  RaycastHit_t87180320  PerFrameRaycast_GetHitInfo_m4235334892 (PerFrameRaycast_t2437905999 * __this, const MethodInfo* method)
{
	{
		RaycastHit_t87180320  L_0 = __this->get_hitInfo_2();
		return L_0;
	}
}
// System.Void PerFrameRaycast::Main()
extern "C"  void PerFrameRaycast_Main_m1544432620 (PerFrameRaycast_t2437905999 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PlayerAnimation::.ctor()
extern "C"  void PlayerAnimation__ctor_m3323431079 (PlayerAnimation_t4286826603 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_maxIdleSpeed_5((0.5f));
		__this->set_minWalkSpeed_6((2.0f));
		Vector3_t2243707580  L_0 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastPosition_13(L_0);
		Vector3_t2243707580  L_1 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_velocity_14(L_1);
		Vector3_t2243707580  L_2 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_localVelocity_15(L_2);
		Vector3_t2243707580  L_3 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lowerBodyForwardTarget_20(L_3);
		Vector3_t2243707580  L_4 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lowerBodyForward_21(L_4);
		return;
	}
}
// System.Void PlayerAnimation::Awake()
extern "C"  void PlayerAnimation_Awake_m2693778044 (PlayerAnimation_t4286826603 * __this, const MethodInfo* method)
{
	MoveAnimation_t3061523661 * V_0 = NULL;
	int32_t V_1 = 0;
	MoveAnimationU5BU5D_t1691628896* V_2 = NULL;
	int32_t V_3 = 0;
	{
		Rigidbody_t4233889191 * L_0 = __this->get_rigid_2();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		__this->set_tr_12(L_1);
		Transform_t3275118058 * L_2 = __this->get_tr_12();
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		__this->set_lastPosition_13(L_3);
		V_1 = 0;
		MoveAnimationU5BU5D_t1691628896* L_4 = __this->get_moveAnimations_10();
		V_2 = L_4;
		MoveAnimationU5BU5D_t1691628896* L_5 = V_2;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)(Il2CppArray *)L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		goto IL_007f;
	}

IL_0037:
	{
		MoveAnimationU5BU5D_t1691628896* L_7 = V_2;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		MoveAnimation_t3061523661 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(4 /* System.Void MoveAnimation::Init() */, L_10);
		Animation_t2068071072 * L_11 = __this->get_animationComponent_25();
		MoveAnimationU5BU5D_t1691628896* L_12 = V_2;
		int32_t L_13 = V_1;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		MoveAnimation_t3061523661 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_15);
		AnimationClip_t3510324950 * L_16 = L_15->get_clip_0();
		NullCheck(L_16);
		String_t* L_17 = Object_get_name_m2079638459(L_16, /*hidden argument*/NULL);
		NullCheck(L_11);
		AnimationState_t1303741697 * L_18 = Animation_get_Item_m4198128320(L_11, L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		AnimationState_set_layer_m139053567(L_18, 1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_19 = __this->get_animationComponent_25();
		MoveAnimationU5BU5D_t1691628896* L_20 = V_2;
		int32_t L_21 = V_1;
		NullCheck(L_20);
		int32_t L_22 = L_21;
		MoveAnimation_t3061523661 * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_23);
		AnimationClip_t3510324950 * L_24 = L_23->get_clip_0();
		NullCheck(L_24);
		String_t* L_25 = Object_get_name_m2079638459(L_24, /*hidden argument*/NULL);
		NullCheck(L_19);
		AnimationState_t1303741697 * L_26 = Animation_get_Item_m4198128320(L_19, L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		AnimationState_set_enabled_m2079619927(L_26, (bool)1, /*hidden argument*/NULL);
		int32_t L_27 = V_1;
		V_1 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_007f:
	{
		int32_t L_28 = V_1;
		int32_t L_29 = V_3;
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0037;
		}
	}
	{
		Animation_t2068071072 * L_30 = __this->get_animationComponent_25();
		NullCheck(L_30);
		Animation_SyncLayer_m3054866342(L_30, 1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_31 = __this->get_animationComponent_25();
		AnimationClip_t3510324950 * L_32 = __this->get_idle_7();
		NullCheck(L_32);
		String_t* L_33 = Object_get_name_m2079638459(L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		AnimationState_t1303741697 * L_34 = Animation_get_Item_m4198128320(L_31, L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		AnimationState_set_layer_m139053567(L_34, 2, /*hidden argument*/NULL);
		Animation_t2068071072 * L_35 = __this->get_animationComponent_25();
		AnimationClip_t3510324950 * L_36 = __this->get_turn_8();
		NullCheck(L_36);
		String_t* L_37 = Object_get_name_m2079638459(L_36, /*hidden argument*/NULL);
		NullCheck(L_35);
		AnimationState_t1303741697 * L_38 = Animation_get_Item_m4198128320(L_35, L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		AnimationState_set_layer_m139053567(L_38, 3, /*hidden argument*/NULL);
		Animation_t2068071072 * L_39 = __this->get_animationComponent_25();
		AnimationClip_t3510324950 * L_40 = __this->get_idle_7();
		NullCheck(L_40);
		String_t* L_41 = Object_get_name_m2079638459(L_40, /*hidden argument*/NULL);
		NullCheck(L_39);
		AnimationState_t1303741697 * L_42 = Animation_get_Item_m4198128320(L_39, L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		AnimationState_set_enabled_m2079619927(L_42, (bool)1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_43 = __this->get_animationComponent_25();
		AnimationClip_t3510324950 * L_44 = __this->get_shootAdditive_9();
		NullCheck(L_44);
		String_t* L_45 = Object_get_name_m2079638459(L_44, /*hidden argument*/NULL);
		NullCheck(L_43);
		AnimationState_t1303741697 * L_46 = Animation_get_Item_m4198128320(L_43, L_45, /*hidden argument*/NULL);
		NullCheck(L_46);
		AnimationState_set_layer_m139053567(L_46, 4, /*hidden argument*/NULL);
		Animation_t2068071072 * L_47 = __this->get_animationComponent_25();
		AnimationClip_t3510324950 * L_48 = __this->get_shootAdditive_9();
		NullCheck(L_48);
		String_t* L_49 = Object_get_name_m2079638459(L_48, /*hidden argument*/NULL);
		NullCheck(L_47);
		AnimationState_t1303741697 * L_50 = Animation_get_Item_m4198128320(L_47, L_49, /*hidden argument*/NULL);
		NullCheck(L_50);
		AnimationState_set_weight_m2370306600(L_50, (((float)((float)1))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_51 = __this->get_animationComponent_25();
		AnimationClip_t3510324950 * L_52 = __this->get_shootAdditive_9();
		NullCheck(L_52);
		String_t* L_53 = Object_get_name_m2079638459(L_52, /*hidden argument*/NULL);
		NullCheck(L_51);
		AnimationState_t1303741697 * L_54 = Animation_get_Item_m4198128320(L_51, L_53, /*hidden argument*/NULL);
		NullCheck(L_54);
		AnimationState_set_speed_m465014523(L_54, (0.6f), /*hidden argument*/NULL);
		Animation_t2068071072 * L_55 = __this->get_animationComponent_25();
		AnimationClip_t3510324950 * L_56 = __this->get_shootAdditive_9();
		NullCheck(L_56);
		String_t* L_57 = Object_get_name_m2079638459(L_56, /*hidden argument*/NULL);
		NullCheck(L_55);
		AnimationState_t1303741697 * L_58 = Animation_get_Item_m4198128320(L_55, L_57, /*hidden argument*/NULL);
		NullCheck(L_58);
		AnimationState_set_blendMode_m3997078040(L_58, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerAnimation::OnStartFire()
extern "C"  void PlayerAnimation_OnStartFire_m2791621660 (PlayerAnimation_t4286826603 * __this, const MethodInfo* method)
{
	{
		float L_0 = Time_get_timeScale_m3151482970(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)(((float)((float)0)))))))
		{
			goto IL_0011;
		}
	}
	{
		goto IL_002d;
	}

IL_0011:
	{
		Animation_t2068071072 * L_1 = __this->get_animationComponent_25();
		AnimationClip_t3510324950 * L_2 = __this->get_shootAdditive_9();
		NullCheck(L_2);
		String_t* L_3 = Object_get_name_m2079638459(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		AnimationState_t1303741697 * L_4 = Animation_get_Item_m4198128320(L_1, L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		AnimationState_set_enabled_m2079619927(L_4, (bool)1, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void PlayerAnimation::OnStopFire()
extern "C"  void PlayerAnimation_OnStopFire_m4018214982 (PlayerAnimation_t4286826603 * __this, const MethodInfo* method)
{
	{
		Animation_t2068071072 * L_0 = __this->get_animationComponent_25();
		AnimationClip_t3510324950 * L_1 = __this->get_shootAdditive_9();
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m2079638459(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		AnimationState_t1303741697 * L_3 = Animation_get_Item_m4198128320(L_0, L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		AnimationState_set_enabled_m2079619927(L_3, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerAnimation::FixedUpdate()
extern "C"  void PlayerAnimation_FixedUpdate_m2246238316 (PlayerAnimation_t4286826603 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = __this->get_tr_12();
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = __this->get_lastPosition_13();
		Vector3_t2243707580  L_3 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		float L_4 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = Vector3_op_Division_m3315615850(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		__this->set_velocity_14(L_5);
		Transform_t3275118058 * L_6 = __this->get_tr_12();
		Vector3_t2243707580  L_7 = __this->get_velocity_14();
		NullCheck(L_6);
		Vector3_t2243707580  L_8 = Transform_InverseTransformDirection_m3595190459(L_6, L_7, /*hidden argument*/NULL);
		__this->set_localVelocity_15(L_8);
		Vector3_t2243707580 * L_9 = __this->get_address_of_localVelocity_15();
		L_9->set_y_2((((float)((float)0))));
		Vector3_t2243707580 * L_10 = __this->get_address_of_localVelocity_15();
		float L_11 = Vector3_get_magnitude_m860342598(L_10, /*hidden argument*/NULL);
		__this->set_speed_16(L_11);
		Vector3_t2243707580  L_12 = __this->get_localVelocity_15();
		float L_13 = PlayerAnimation_HorizontalAngle_m2440304315(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		__this->set_angle_17(L_13);
		Transform_t3275118058 * L_14 = __this->get_tr_12();
		NullCheck(L_14);
		Vector3_t2243707580  L_15 = Transform_get_position_m1104419803(L_14, /*hidden argument*/NULL);
		__this->set_lastPosition_13(L_15);
		return;
	}
}
// System.Void PlayerAnimation::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimeServices_t1910041954_il2cpp_TypeInfo_var;
extern const uint32_t PlayerAnimation_Update_m220504286_MetadataUsageId;
extern "C"  void PlayerAnimation_Update_m220504286 (PlayerAnimation_t4286826603 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerAnimation_Update_m220504286_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	MoveAnimation_t3061523661 * V_1 = NULL;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t V_6 = 0;
	MoveAnimationU5BU5D_t1691628896* V_7 = NULL;
	int32_t V_8 = 0;
	{
		float L_0 = __this->get_idleWeight_19();
		float L_1 = __this->get_minWalkSpeed_6();
		float L_2 = __this->get_maxIdleSpeed_5();
		float L_3 = __this->get_speed_16();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = Mathf_InverseLerp_m55890283(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		float L_5 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_0, L_4, ((float)((float)L_5*(float)(((float)((float)((int32_t)10)))))), /*hidden argument*/NULL);
		__this->set_idleWeight_19(L_6);
		Animation_t2068071072 * L_7 = __this->get_animationComponent_25();
		AnimationClip_t3510324950 * L_8 = __this->get_idle_7();
		NullCheck(L_8);
		String_t* L_9 = Object_get_name_m2079638459(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		AnimationState_t1303741697 * L_10 = Animation_get_Item_m4198128320(L_7, L_9, /*hidden argument*/NULL);
		float L_11 = __this->get_idleWeight_19();
		NullCheck(L_10);
		AnimationState_set_weight_m2370306600(L_10, L_11, /*hidden argument*/NULL);
		float L_12 = __this->get_speed_16();
		if ((((float)L_12) <= ((float)(((float)((float)0))))))
		{
			goto IL_0119;
		}
	}
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_6 = 0;
		MoveAnimationU5BU5D_t1691628896* L_13 = __this->get_moveAnimations_10();
		V_7 = L_13;
		MoveAnimationU5BU5D_t1691628896* L_14 = V_7;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_14);
		int32_t L_15 = Array_get_Length_m1498215565((Il2CppArray *)(Il2CppArray *)L_14, /*hidden argument*/NULL);
		V_8 = L_15;
		goto IL_00f0;
	}

IL_007e:
	{
		float L_16 = __this->get_angle_17();
		MoveAnimationU5BU5D_t1691628896* L_17 = V_7;
		int32_t L_18 = V_6;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		MoveAnimation_t3061523661 * L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		NullCheck(L_20);
		float L_21 = L_20->get_angle_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_22 = Mathf_DeltaAngle_m1024687732(NULL /*static, unused*/, L_16, L_21, /*hidden argument*/NULL);
		float L_23 = fabsf(L_22);
		V_2 = L_23;
		float L_24 = __this->get_speed_16();
		MoveAnimationU5BU5D_t1691628896* L_25 = V_7;
		int32_t L_26 = V_6;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		MoveAnimation_t3061523661 * L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_28);
		float L_29 = L_28->get_speed_4();
		float L_30 = fabsf(((float)((float)L_24-(float)L_29)));
		V_3 = L_30;
		float L_31 = V_2;
		float L_32 = V_3;
		V_4 = ((float)((float)L_31+(float)L_32));
		MoveAnimationU5BU5D_t1691628896* L_33 = V_7;
		int32_t L_34 = V_6;
		NullCheck(L_33);
		int32_t L_35 = L_34;
		MoveAnimation_t3061523661 * L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		MoveAnimation_t3061523661 * L_37 = __this->get_bestAnimation_22();
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		bool L_38 = RuntimeServices_EqualityOperator_m2233200645(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_00d4;
		}
	}
	{
		float L_39 = V_4;
		V_4 = ((float)((float)L_39*(float)(0.9f)));
	}

IL_00d4:
	{
		float L_40 = V_4;
		float L_41 = V_0;
		if ((((float)L_40) >= ((float)L_41)))
		{
			goto IL_00ea;
		}
	}
	{
		MoveAnimationU5BU5D_t1691628896* L_42 = V_7;
		int32_t L_43 = V_6;
		NullCheck(L_42);
		int32_t L_44 = L_43;
		MoveAnimation_t3061523661 * L_45 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		__this->set_bestAnimation_22(L_45);
		float L_46 = V_4;
		V_0 = L_46;
	}

IL_00ea:
	{
		int32_t L_47 = V_6;
		V_6 = ((int32_t)((int32_t)L_47+(int32_t)1));
	}

IL_00f0:
	{
		int32_t L_48 = V_6;
		int32_t L_49 = V_8;
		if ((((int32_t)L_48) < ((int32_t)L_49)))
		{
			goto IL_007e;
		}
	}
	{
		Animation_t2068071072 * L_50 = __this->get_animationComponent_25();
		MoveAnimation_t3061523661 * L_51 = __this->get_bestAnimation_22();
		NullCheck(L_51);
		AnimationClip_t3510324950 * L_52 = L_51->get_clip_0();
		NullCheck(L_52);
		String_t* L_53 = Object_get_name_m2079638459(L_52, /*hidden argument*/NULL);
		NullCheck(L_50);
		Animation_CrossFade_m3878519673(L_50, L_53, /*hidden argument*/NULL);
		goto IL_0120;
	}

IL_0119:
	{
		__this->set_bestAnimation_22((MoveAnimation_t3061523661 *)NULL);
	}

IL_0120:
	{
		Vector3_t2243707580  L_54 = __this->get_lowerBodyForward_21();
		Vector3_t2243707580  L_55 = __this->get_lowerBodyForwardTarget_20();
		bool L_56 = Vector3_op_Inequality_m799191452(NULL /*static, unused*/, L_54, L_55, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_0161;
		}
	}
	{
		float L_57 = __this->get_idleWeight_19();
		if ((((float)L_57) < ((float)(0.9f))))
		{
			goto IL_0161;
		}
	}
	{
		Animation_t2068071072 * L_58 = __this->get_animationComponent_25();
		AnimationClip_t3510324950 * L_59 = __this->get_turn_8();
		NullCheck(L_59);
		String_t* L_60 = Object_get_name_m2079638459(L_59, /*hidden argument*/NULL);
		NullCheck(L_58);
		Animation_CrossFade_m1757328910(L_58, L_60, (0.05f), /*hidden argument*/NULL);
	}

IL_0161:
	{
		MoveAnimation_t3061523661 * L_61 = __this->get_bestAnimation_22();
		if (!((!(((Il2CppObject*)(MoveAnimation_t3061523661 *)L_61) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0))
		{
			goto IL_01f3;
		}
	}
	{
		float L_62 = __this->get_idleWeight_19();
		if ((((float)L_62) >= ((float)(0.9f))))
		{
			goto IL_01f3;
		}
	}
	{
		Animation_t2068071072 * L_63 = __this->get_animationComponent_25();
		MoveAnimation_t3061523661 * L_64 = __this->get_bestAnimation_22();
		NullCheck(L_64);
		AnimationClip_t3510324950 * L_65 = L_64->get_clip_0();
		NullCheck(L_65);
		String_t* L_66 = Object_get_name_m2079638459(L_65, /*hidden argument*/NULL);
		NullCheck(L_63);
		AnimationState_t1303741697 * L_67 = Animation_get_Item_m4198128320(L_63, L_66, /*hidden argument*/NULL);
		NullCheck(L_67);
		float L_68 = AnimationState_get_normalizedTime_m3003675227(L_67, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_69 = Mathf_Repeat_m943844734(NULL /*static, unused*/, ((float)((float)((float)((float)L_68*(float)(((float)((float)2)))))+(float)(0.1f))), (((float)((float)1))), /*hidden argument*/NULL);
		V_5 = L_69;
		float L_70 = V_5;
		float L_71 = __this->get_lastAnimTime_24();
		if ((((float)L_70) >= ((float)L_71)))
		{
			goto IL_01eb;
		}
	}
	{
		float L_72 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_73 = __this->get_lastFootstepTime_23();
		if ((((float)L_72) <= ((float)((float)((float)L_73+(float)(0.1f))))))
		{
			goto IL_01eb;
		}
	}
	{
		SignalSender_t1204926691 * L_74 = __this->get_footstepSignals_11();
		NullCheck(L_74);
		VirtActionInvoker1< MonoBehaviour_t1158329972 * >::Invoke(4 /* System.Void SignalSender::SendSignals(UnityEngine.MonoBehaviour) */, L_74, __this);
		float L_75 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastFootstepTime_23(L_75);
	}

IL_01eb:
	{
		float L_76 = V_5;
		__this->set_lastAnimTime_24(L_76);
	}

IL_01f3:
	{
		return;
	}
}
// System.Void PlayerAnimation::LateUpdate()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t PlayerAnimation_LateUpdate_m2795049138_MetadataUsageId;
extern "C"  void PlayerAnimation_LateUpdate_m2795049138 (PlayerAnimation_t4286826603 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerAnimation_LateUpdate_m2795049138_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	MoveAnimation_t3061523661 * V_2 = NULL;
	float V_3 = 0.0f;
	Quaternion_t4030073918  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t V_5 = 0;
	MoveAnimationU5BU5D_t1691628896* V_6 = NULL;
	int32_t V_7 = 0;
	{
		float L_0 = __this->get_minWalkSpeed_6();
		float L_1 = __this->get_maxIdleSpeed_5();
		float L_2 = __this->get_speed_16();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = Mathf_InverseLerp_m55890283(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = V_0;
		if ((((float)L_4) >= ((float)(((float)((float)1))))))
		{
			goto IL_0147;
		}
	}
	{
		Vector3_t2243707580  L_5 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_5;
		V_5 = 0;
		MoveAnimationU5BU5D_t1691628896* L_6 = __this->get_moveAnimations_10();
		V_6 = L_6;
		MoveAnimationU5BU5D_t1691628896* L_7 = V_6;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_7);
		int32_t L_8 = Array_get_Length_m1498215565((Il2CppArray *)(Il2CppArray *)L_7, /*hidden argument*/NULL);
		V_7 = L_8;
		goto IL_00c6;
	}

IL_003f:
	{
		Animation_t2068071072 * L_9 = __this->get_animationComponent_25();
		MoveAnimationU5BU5D_t1691628896* L_10 = V_6;
		int32_t L_11 = V_5;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		MoveAnimation_t3061523661 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		AnimationClip_t3510324950 * L_14 = L_13->get_clip_0();
		NullCheck(L_14);
		String_t* L_15 = Object_get_name_m2079638459(L_14, /*hidden argument*/NULL);
		NullCheck(L_9);
		AnimationState_t1303741697 * L_16 = Animation_get_Item_m4198128320(L_9, L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		float L_17 = AnimationState_get_weight_m3266163847(L_16, /*hidden argument*/NULL);
		if ((!(((float)L_17) == ((float)(((float)((float)0)))))))
		{
			goto IL_006a;
		}
	}
	{
		goto IL_00c0;
	}

IL_006a:
	{
		MoveAnimationU5BU5D_t1691628896* L_18 = V_6;
		int32_t L_19 = V_5;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		MoveAnimation_t3061523661 * L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_21);
		Vector3_t2243707580  L_22 = L_21->get_velocity_1();
		Vector3_t2243707580  L_23 = __this->get_localVelocity_15();
		float L_24 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		if ((((float)L_24) > ((float)(((float)((float)0))))))
		{
			goto IL_008b;
		}
	}
	{
		goto IL_00c0;
	}

IL_008b:
	{
		Vector3_t2243707580  L_25 = V_1;
		MoveAnimationU5BU5D_t1691628896* L_26 = V_6;
		int32_t L_27 = V_5;
		NullCheck(L_26);
		int32_t L_28 = L_27;
		MoveAnimation_t3061523661 * L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_29);
		Vector3_t2243707580  L_30 = L_29->get_velocity_1();
		Animation_t2068071072 * L_31 = __this->get_animationComponent_25();
		MoveAnimationU5BU5D_t1691628896* L_32 = V_6;
		int32_t L_33 = V_5;
		NullCheck(L_32);
		int32_t L_34 = L_33;
		MoveAnimation_t3061523661 * L_35 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		NullCheck(L_35);
		AnimationClip_t3510324950 * L_36 = L_35->get_clip_0();
		NullCheck(L_36);
		String_t* L_37 = Object_get_name_m2079638459(L_36, /*hidden argument*/NULL);
		NullCheck(L_31);
		AnimationState_t1303741697 * L_38 = Animation_get_Item_m4198128320(L_31, L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		float L_39 = AnimationState_get_weight_m3266163847(L_38, /*hidden argument*/NULL);
		Vector3_t2243707580  L_40 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_30, L_39, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_25, L_40, /*hidden argument*/NULL);
		V_1 = L_41;
	}

IL_00c0:
	{
		int32_t L_42 = V_5;
		V_5 = ((int32_t)((int32_t)L_42+(int32_t)1));
	}

IL_00c6:
	{
		int32_t L_43 = V_5;
		int32_t L_44 = V_7;
		if ((((int32_t)L_43) < ((int32_t)L_44)))
		{
			goto IL_003f;
		}
	}
	{
		Transform_t3275118058 * L_45 = __this->get_tr_12();
		NullCheck(L_45);
		Quaternion_t4030073918  L_46 = Transform_get_rotation_m1033555130(L_45, /*hidden argument*/NULL);
		Vector3_t2243707580  L_47 = V_1;
		Vector3_t2243707580  L_48 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_46, L_47, /*hidden argument*/NULL);
		float L_49 = PlayerAnimation_HorizontalAngle_m2440304315(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		Vector3_t2243707580  L_50 = __this->get_velocity_14();
		float L_51 = PlayerAnimation_HorizontalAngle_m2440304315(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_52 = Mathf_DeltaAngle_m1024687732(NULL /*static, unused*/, L_49, L_51, /*hidden argument*/NULL);
		V_3 = L_52;
		float L_53 = __this->get_lowerBodyDeltaAngle_18();
		float L_54 = V_3;
		float L_55 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_56 = Mathf_LerpAngle_m3501252860(NULL /*static, unused*/, L_53, L_54, ((float)((float)L_55*(float)(((float)((float)((int32_t)10)))))), /*hidden argument*/NULL);
		__this->set_lowerBodyDeltaAngle_18(L_56);
		Transform_t3275118058 * L_57 = __this->get_tr_12();
		NullCheck(L_57);
		Vector3_t2243707580  L_58 = Transform_get_forward_m1833488937(L_57, /*hidden argument*/NULL);
		__this->set_lowerBodyForwardTarget_20(L_58);
		float L_59 = __this->get_lowerBodyDeltaAngle_18();
		Quaternion_t4030073918  L_60 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (((float)((float)0))), L_59, (((float)((float)0))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_61 = __this->get_lowerBodyForwardTarget_20();
		Vector3_t2243707580  L_62 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_60, L_61, /*hidden argument*/NULL);
		__this->set_lowerBodyForward_21(L_62);
		goto IL_01bc;
	}

IL_0147:
	{
		Vector3_t2243707580  L_63 = __this->get_lowerBodyForward_21();
		Vector3_t2243707580  L_64 = __this->get_lowerBodyForwardTarget_20();
		float L_65 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_66 = Vector3_RotateTowards_m4046679056(NULL /*static, unused*/, L_63, L_64, ((float)((float)((float)((float)L_65*(float)(((float)((float)((int32_t)520))))))*(float)(0.0174532924f))), (((float)((float)1))), /*hidden argument*/NULL);
		__this->set_lowerBodyForward_21(L_66);
		Transform_t3275118058 * L_67 = __this->get_tr_12();
		NullCheck(L_67);
		Vector3_t2243707580  L_68 = Transform_get_forward_m1833488937(L_67, /*hidden argument*/NULL);
		float L_69 = PlayerAnimation_HorizontalAngle_m2440304315(NULL /*static, unused*/, L_68, /*hidden argument*/NULL);
		Vector3_t2243707580  L_70 = __this->get_lowerBodyForward_21();
		float L_71 = PlayerAnimation_HorizontalAngle_m2440304315(NULL /*static, unused*/, L_70, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_72 = Mathf_DeltaAngle_m1024687732(NULL /*static, unused*/, L_69, L_71, /*hidden argument*/NULL);
		__this->set_lowerBodyDeltaAngle_18(L_72);
		float L_73 = __this->get_lowerBodyDeltaAngle_18();
		float L_74 = fabsf(L_73);
		if ((((float)L_74) <= ((float)(((float)((float)((int32_t)80)))))))
		{
			goto IL_01bc;
		}
	}
	{
		Transform_t3275118058 * L_75 = __this->get_tr_12();
		NullCheck(L_75);
		Vector3_t2243707580  L_76 = Transform_get_forward_m1833488937(L_75, /*hidden argument*/NULL);
		__this->set_lowerBodyForwardTarget_20(L_76);
	}

IL_01bc:
	{
		float L_77 = __this->get_lowerBodyDeltaAngle_18();
		Quaternion_t4030073918  L_78 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (((float)((float)0))), L_77, (((float)((float)0))), /*hidden argument*/NULL);
		V_4 = L_78;
		Transform_t3275118058 * L_79 = __this->get_rootBone_3();
		Quaternion_t4030073918  L_80 = V_4;
		Transform_t3275118058 * L_81 = __this->get_rootBone_3();
		NullCheck(L_81);
		Quaternion_t4030073918  L_82 = Transform_get_rotation_m1033555130(L_81, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_83 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_80, L_82, /*hidden argument*/NULL);
		NullCheck(L_79);
		Transform_set_rotation_m3411284563(L_79, L_83, /*hidden argument*/NULL);
		Transform_t3275118058 * L_84 = __this->get_upperBodyBone_4();
		Quaternion_t4030073918  L_85 = V_4;
		Quaternion_t4030073918  L_86 = Quaternion_Inverse_m3931399088(NULL /*static, unused*/, L_85, /*hidden argument*/NULL);
		Transform_t3275118058 * L_87 = __this->get_upperBodyBone_4();
		NullCheck(L_87);
		Quaternion_t4030073918  L_88 = Transform_get_rotation_m1033555130(L_87, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_89 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_86, L_88, /*hidden argument*/NULL);
		NullCheck(L_84);
		Transform_set_rotation_m3411284563(L_84, L_89, /*hidden argument*/NULL);
		return;
	}
}
// System.Single PlayerAnimation::HorizontalAngle(UnityEngine.Vector3)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t PlayerAnimation_HorizontalAngle_m2440304315_MetadataUsageId;
extern "C"  float PlayerAnimation_HorizontalAngle_m2440304315 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___direction0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerAnimation_HorizontalAngle_m2440304315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = (&___direction0)->get_x_1();
		float L_1 = (&___direction0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = atan2f(L_0, L_1);
		return ((float)((float)L_2*(float)(57.29578f)));
	}
}
// System.Void PlayerAnimation::Main()
extern "C"  void PlayerAnimation_Main_m970525314 (PlayerAnimation_t4286826603 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PlayerMoveController::.ctor()
extern "C"  void PlayerMoveController__ctor_m1137400478 (PlayerMoveController_t2811056080 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_cameraSmoothing_6((0.01f));
		__this->set_cameraPreview_7((2.0f));
		__this->set_cursorSmallerWhenClose_11((((float)((float)1))));
		Vector3_t2243707580  L_0 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_cameraVelocity_17(L_0);
		Vector3_t2243707580  L_1 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_cameraOffset_18(L_1);
		return;
	}
}
// System.Void PlayerMoveController::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisJoystick_t549888914_m2668756518_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3962498853;
extern Il2CppCodeGenString* _stringLiteral2573222338;
extern const uint32_t PlayerMoveController_Awake_m2020416037_MetadataUsageId;
extern "C"  void PlayerMoveController_Awake_m2020416037 (PlayerMoveController_t2811056080 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerMoveController_Awake_m2020416037_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		MovementMotor_t1612021398 * L_0 = __this->get_motor_2();
		Vector2_t2243707579  L_1 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		L_0->set_movementDirection_2(L_2);
		MovementMotor_t1612021398 * L_3 = __this->get_motor_2();
		Vector2_t2243707579  L_4 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_facingDirection_4(L_5);
		Camera_t189460977 * L_6 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mainCamera_12(L_6);
		Camera_t189460977 * L_7 = __this->get_mainCamera_12();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(L_7, /*hidden argument*/NULL);
		__this->set_mainCameraTransform_16(L_8);
		Transform_t3275118058 * L_9 = __this->get_character_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0062;
		}
	}
	{
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		__this->set_character_3(L_11);
	}

IL_0062:
	{
		Transform_t3275118058 * L_12 = __this->get_mainCameraTransform_16();
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_position_m1104419803(L_12, /*hidden argument*/NULL);
		Transform_t3275118058 * L_14 = __this->get_character_3();
		NullCheck(L_14);
		Vector3_t2243707580  L_15 = Transform_get_position_m1104419803(L_14, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		__this->set_initOffsetToPlayer_19(L_16);
		GameObject_t1756533147 * L_17 = __this->get_joystickPrefab_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00f2;
		}
	}
	{
		GameObject_t1756533147 * L_19 = __this->get_joystickPrefab_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_20 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_19, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		V_0 = ((GameObject_t1756533147 *)IsInstSealed(L_20, GameObject_t1756533147_il2cpp_TypeInfo_var));
		GameObject_t1756533147 * L_21 = V_0;
		NullCheck(L_21);
		Object_set_name_m4157836998(L_21, _stringLiteral3962498853, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_22 = V_0;
		NullCheck(L_22);
		Joystick_t549888914 * L_23 = GameObject_GetComponent_TisJoystick_t549888914_m2668756518(L_22, /*hidden argument*/GameObject_GetComponent_TisJoystick_t549888914_m2668756518_MethodInfo_var);
		__this->set_joystickLeft_14(L_23);
		GameObject_t1756533147 * L_24 = __this->get_joystickPrefab_5();
		GameObject_t1756533147 * L_25 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_24, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		__this->set_joystickRightGO_22(((GameObject_t1756533147 *)IsInstSealed(L_25, GameObject_t1756533147_il2cpp_TypeInfo_var)));
		GameObject_t1756533147 * L_26 = __this->get_joystickRightGO_22();
		NullCheck(L_26);
		Object_set_name_m4157836998(L_26, _stringLiteral2573222338, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_27 = __this->get_joystickRightGO_22();
		NullCheck(L_27);
		Joystick_t549888914 * L_28 = GameObject_GetComponent_TisJoystick_t549888914_m2668756518(L_27, /*hidden argument*/GameObject_GetComponent_TisJoystick_t549888914_m2668756518_MethodInfo_var);
		__this->set_joystickRight_15(L_28);
	}

IL_00f2:
	{
		Transform_t3275118058 * L_29 = __this->get_mainCameraTransform_16();
		NullCheck(L_29);
		Vector3_t2243707580  L_30 = Transform_get_position_m1104419803(L_29, /*hidden argument*/NULL);
		Transform_t3275118058 * L_31 = __this->get_character_3();
		NullCheck(L_31);
		Vector3_t2243707580  L_32 = Transform_get_position_m1104419803(L_31, /*hidden argument*/NULL);
		Vector3_t2243707580  L_33 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_30, L_32, /*hidden argument*/NULL);
		__this->set_cameraOffset_18(L_33);
		int32_t L_34 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_35 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_36;
		memset(&L_36, 0, sizeof(L_36));
		Vector3__ctor_m2638739322(&L_36, ((float)((float)(0.5f)*(float)(((float)((float)L_34))))), ((float)((float)(0.5f)*(float)(((float)((float)L_35))))), (((float)((float)0))), /*hidden argument*/NULL);
		__this->set_cursorScreenPosition_20(L_36);
		Transform_t3275118058 * L_37 = __this->get_character_3();
		NullCheck(L_37);
		Vector3_t2243707580  L_38 = Transform_get_up_m1603627763(L_37, /*hidden argument*/NULL);
		Transform_t3275118058 * L_39 = __this->get_character_3();
		NullCheck(L_39);
		Vector3_t2243707580  L_40 = Transform_get_position_m1104419803(L_39, /*hidden argument*/NULL);
		Transform_t3275118058 * L_41 = __this->get_character_3();
		NullCheck(L_41);
		Vector3_t2243707580  L_42 = Transform_get_up_m1603627763(L_41, /*hidden argument*/NULL);
		float L_43 = __this->get_cursorPlaneHeight_8();
		Vector3_t2243707580  L_44 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_42, L_43, /*hidden argument*/NULL);
		Vector3_t2243707580  L_45 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_40, L_44, /*hidden argument*/NULL);
		Plane_t3727654732  L_46;
		memset(&L_46, 0, sizeof(L_46));
		Plane__ctor_m3187718367(&L_46, L_38, L_45, /*hidden argument*/NULL);
		__this->set_playerMovementPlane_21(L_46);
		return;
	}
}
// System.Void PlayerMoveController::Start()
extern const MethodInfo* GameObject_GetComponent_TisGUITexture_t1909122990_m2982615765_MethodInfo_var;
extern const uint32_t PlayerMoveController_Start_m1875097022_MetadataUsageId;
extern "C"  void PlayerMoveController_Start_m1875097022 (PlayerMoveController_t2811056080 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerMoveController_Start_m1875097022_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GUITexture_t1909122990 * V_0 = NULL;
	float V_1 = 0.0f;
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t3681755626  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t3681755626  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	Rect_t3681755626  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		GameObject_t1756533147 * L_0 = __this->get_joystickRightGO_22();
		NullCheck(L_0);
		GUITexture_t1909122990 * L_1 = GameObject_GetComponent_TisGUITexture_t1909122990_m2982615765(L_0, /*hidden argument*/GameObject_GetComponent_TisGUITexture_t1909122990_m2982615765_MethodInfo_var);
		V_0 = L_1;
		int32_t L_2 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUITexture_t1909122990 * L_3 = V_0;
		NullCheck(L_3);
		Rect_t3681755626  L_4 = GUITexture_get_pixelInset_m1273695445(L_3, /*hidden argument*/NULL);
		V_3 = L_4;
		float L_5 = Rect_get_x_m1393582490((&V_3), /*hidden argument*/NULL);
		GUITexture_t1909122990 * L_6 = V_0;
		NullCheck(L_6);
		Rect_t3681755626  L_7 = GUITexture_get_pixelInset_m1273695445(L_6, /*hidden argument*/NULL);
		V_4 = L_7;
		float L_8 = Rect_get_width_m1138015702((&V_4), /*hidden argument*/NULL);
		float L_9 = ((float)((float)((float)((float)(((float)((float)L_2)))-(float)L_5))-(float)L_8));
		V_1 = L_9;
		GUITexture_t1909122990 * L_10 = V_0;
		NullCheck(L_10);
		Rect_t3681755626  L_11 = GUITexture_get_pixelInset_m1273695445(L_10, /*hidden argument*/NULL);
		Rect_t3681755626  L_12 = L_11;
		V_2 = L_12;
		float L_13 = V_1;
		float L_14 = L_13;
		V_5 = L_14;
		Rect_set_x_m3783700513((&V_2), L_14, /*hidden argument*/NULL);
		float L_15 = V_5;
		GUITexture_t1909122990 * L_16 = V_0;
		Rect_t3681755626  L_17 = V_2;
		Rect_t3681755626  L_18 = L_17;
		V_6 = L_18;
		NullCheck(L_16);
		GUITexture_set_pixelInset_m2054734980(L_16, L_18, /*hidden argument*/NULL);
		Rect_t3681755626  L_19 = V_6;
		Transform_t3275118058 * L_20 = __this->get_mainCameraTransform_16();
		NullCheck(L_20);
		Vector3_t2243707580  L_21 = Transform_get_eulerAngles_m4066505159(L_20, /*hidden argument*/NULL);
		V_7 = L_21;
		float L_22 = (&V_7)->get_y_2();
		Quaternion_t4030073918  L_23 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (((float)((float)0))), L_22, (((float)((float)0))), /*hidden argument*/NULL);
		__this->set_screenMovementSpace_23(L_23);
		Quaternion_t4030073918  L_24 = __this->get_screenMovementSpace_23();
		Vector3_t2243707580  L_25 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_26 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		__this->set_screenMovementForward_24(L_26);
		Quaternion_t4030073918  L_27 = __this->get_screenMovementSpace_23();
		Vector3_t2243707580  L_28 = Vector3_get_right_m1884123822(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_29 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
		__this->set_screenMovementRight_25(L_29);
		return;
	}
}
// System.Void PlayerMoveController::OnDisable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t PlayerMoveController_OnDisable_m2779455277_MetadataUsageId;
extern "C"  void PlayerMoveController_OnDisable_m2779455277 (PlayerMoveController_t2811056080 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerMoveController_OnDisable_m2779455277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Joystick_t549888914 * L_0 = __this->get_joystickLeft_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Joystick_t549888914 * L_2 = __this->get_joystickLeft_14();
		NullCheck(L_2);
		Behaviour_set_enabled_m1796096907(L_2, (bool)0, /*hidden argument*/NULL);
	}

IL_001c:
	{
		Joystick_t549888914 * L_3 = __this->get_joystickRight_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		Joystick_t549888914 * L_5 = __this->get_joystickRight_15();
		NullCheck(L_5);
		Behaviour_set_enabled_m1796096907(L_5, (bool)0, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void PlayerMoveController::OnEnable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t PlayerMoveController_OnEnable_m2956590558_MetadataUsageId;
extern "C"  void PlayerMoveController_OnEnable_m2956590558 (PlayerMoveController_t2811056080 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerMoveController_OnEnable_m2956590558_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Joystick_t549888914 * L_0 = __this->get_joystickLeft_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Joystick_t549888914 * L_2 = __this->get_joystickLeft_14();
		NullCheck(L_2);
		Behaviour_set_enabled_m1796096907(L_2, (bool)1, /*hidden argument*/NULL);
	}

IL_001c:
	{
		Joystick_t549888914 * L_3 = __this->get_joystickRight_15();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		Joystick_t549888914 * L_5 = __this->get_joystickRight_15();
		NullCheck(L_5);
		Behaviour_set_enabled_m1796096907(L_5, (bool)1, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void PlayerMoveController::Update()
extern "C"  void PlayerMoveController_Update_m2700824225 (PlayerMoveController_t2811056080 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		MovementMotor_t1612021398 * L_0 = __this->get_motor_2();
		Joystick_t549888914 * L_1 = __this->get_joystickLeft_14();
		NullCheck(L_1);
		Vector2_t2243707579 * L_2 = L_1->get_address_of_position_9();
		float L_3 = L_2->get_x_0();
		Vector3_t2243707580  L_4 = __this->get_screenMovementRight_25();
		Vector3_t2243707580  L_5 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Joystick_t549888914 * L_6 = __this->get_joystickLeft_14();
		NullCheck(L_6);
		Vector2_t2243707579 * L_7 = L_6->get_address_of_position_9();
		float L_8 = L_7->get_y_1();
		Vector3_t2243707580  L_9 = __this->get_screenMovementForward_24();
		Vector3_t2243707580  L_10 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		Vector3_t2243707580  L_11 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_5, L_10, /*hidden argument*/NULL);
		NullCheck(L_0);
		L_0->set_movementDirection_2(L_11);
		MovementMotor_t1612021398 * L_12 = __this->get_motor_2();
		NullCheck(L_12);
		Vector3_t2243707580 * L_13 = L_12->get_address_of_movementDirection_2();
		float L_14 = Vector3_get_sqrMagnitude_m1814096310(L_13, /*hidden argument*/NULL);
		if ((((float)L_14) <= ((float)(((float)((float)1))))))
		{
			goto IL_006d;
		}
	}
	{
		MovementMotor_t1612021398 * L_15 = __this->get_motor_2();
		NullCheck(L_15);
		Vector3_t2243707580 * L_16 = L_15->get_address_of_movementDirection_2();
		Vector3_Normalize_m3679112426(L_16, /*hidden argument*/NULL);
	}

IL_006d:
	{
		Plane_t3727654732 * L_17 = __this->get_address_of_playerMovementPlane_21();
		Transform_t3275118058 * L_18 = __this->get_character_3();
		NullCheck(L_18);
		Vector3_t2243707580  L_19 = Transform_get_up_m1603627763(L_18, /*hidden argument*/NULL);
		Plane_set_normal_m2698708846(L_17, L_19, /*hidden argument*/NULL);
		Plane_t3727654732 * L_20 = __this->get_address_of_playerMovementPlane_21();
		Transform_t3275118058 * L_21 = __this->get_character_3();
		NullCheck(L_21);
		Vector3_t2243707580  L_22 = Transform_get_position_m1104419803(L_21, /*hidden argument*/NULL);
		V_2 = L_22;
		float L_23 = (&V_2)->get_y_2();
		float L_24 = __this->get_cursorPlaneHeight_8();
		Plane_set_distance_m1726772526(L_20, ((float)((float)((-L_23))+(float)L_24)), /*hidden argument*/NULL);
		Vector3_t2243707580  L_25 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_25;
		MovementMotor_t1612021398 * L_26 = __this->get_motor_2();
		Joystick_t549888914 * L_27 = __this->get_joystickRight_15();
		NullCheck(L_27);
		Vector2_t2243707579 * L_28 = L_27->get_address_of_position_9();
		float L_29 = L_28->get_x_0();
		Vector3_t2243707580  L_30 = __this->get_screenMovementRight_25();
		Vector3_t2243707580  L_31 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		Joystick_t549888914 * L_32 = __this->get_joystickRight_15();
		NullCheck(L_32);
		Vector2_t2243707579 * L_33 = L_32->get_address_of_position_9();
		float L_34 = L_33->get_y_1();
		Vector3_t2243707580  L_35 = __this->get_screenMovementForward_24();
		Vector3_t2243707580  L_36 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/NULL);
		Vector3_t2243707580  L_37 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_31, L_36, /*hidden argument*/NULL);
		NullCheck(L_26);
		L_26->set_facingDirection_4(L_37);
		MovementMotor_t1612021398 * L_38 = __this->get_motor_2();
		NullCheck(L_38);
		Vector3_t2243707580  L_39 = L_38->get_facingDirection_4();
		V_0 = L_39;
		Transform_t3275118058 * L_40 = __this->get_character_3();
		NullCheck(L_40);
		Vector3_t2243707580  L_41 = Transform_get_position_m1104419803(L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_42 = __this->get_initOffsetToPlayer_19();
		Vector3_t2243707580  L_43 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_44 = V_0;
		float L_45 = __this->get_cameraPreview_7();
		Vector3_t2243707580  L_46 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_44, L_45, /*hidden argument*/NULL);
		Vector3_t2243707580  L_47 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_43, L_46, /*hidden argument*/NULL);
		V_1 = L_47;
		Transform_t3275118058 * L_48 = __this->get_mainCameraTransform_16();
		Transform_t3275118058 * L_49 = __this->get_mainCameraTransform_16();
		NullCheck(L_49);
		Vector3_t2243707580  L_50 = Transform_get_position_m1104419803(L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = V_1;
		Vector3_t2243707580 * L_52 = __this->get_address_of_cameraVelocity_17();
		float L_53 = __this->get_cameraSmoothing_6();
		Vector3_t2243707580  L_54 = Vector3_SmoothDamp_m3087890513(NULL /*static, unused*/, L_50, L_51, L_52, L_53, /*hidden argument*/NULL);
		NullCheck(L_48);
		Transform_set_position_m2469242620(L_48, L_54, /*hidden argument*/NULL);
		Transform_t3275118058 * L_55 = __this->get_mainCameraTransform_16();
		NullCheck(L_55);
		Vector3_t2243707580  L_56 = Transform_get_position_m1104419803(L_55, /*hidden argument*/NULL);
		Transform_t3275118058 * L_57 = __this->get_character_3();
		NullCheck(L_57);
		Vector3_t2243707580  L_58 = Transform_get_position_m1104419803(L_57, /*hidden argument*/NULL);
		Vector3_t2243707580  L_59 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_56, L_58, /*hidden argument*/NULL);
		__this->set_cameraOffset_18(L_59);
		return;
	}
}
// UnityEngine.Vector3 PlayerMoveController::PlaneRayIntersection(UnityEngine.Plane,UnityEngine.Ray)
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t PlayerMoveController_PlaneRayIntersection_m2000116583_MetadataUsageId;
extern "C"  Vector3_t2243707580  PlayerMoveController_PlaneRayIntersection_m2000116583 (Il2CppObject * __this /* static, unused */, Plane_t3727654732  ___plane0, Ray_t2469606224  ___ray1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerMoveController_PlaneRayIntersection_m2000116583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_0));
		Ray_t2469606224  L_0 = ___ray1;
		Plane_Raycast_m2870142810((&___plane0), L_0, (&V_0), /*hidden argument*/NULL);
		float L_1 = V_0;
		Vector3_t2243707580  L_2 = Ray_GetPoint_m1353702366((&___ray1), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 PlayerMoveController::ScreenPointToWorldPointOnPlane(UnityEngine.Vector3,UnityEngine.Plane,UnityEngine.Camera)
extern "C"  Vector3_t2243707580  PlayerMoveController_ScreenPointToWorldPointOnPlane_m1526574694 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___screenPoint0, Plane_t3727654732  ___plane1, Camera_t189460977 * ___camera2, const MethodInfo* method)
{
	Ray_t2469606224  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_t189460977 * L_0 = ___camera2;
		Vector3_t2243707580  L_1 = ___screenPoint0;
		NullCheck(L_0);
		Ray_t2469606224  L_2 = Camera_ScreenPointToRay_m614889538(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Plane_t3727654732  L_3 = ___plane1;
		Ray_t2469606224  L_4 = V_0;
		Vector3_t2243707580  L_5 = PlayerMoveController_PlaneRayIntersection_m2000116583(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void PlayerMoveController::HandleCursorAlignment(UnityEngine.Vector3)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t PlayerMoveController_HandleCursorAlignment_m937938588_MetadataUsageId;
extern "C"  void PlayerMoveController_HandleCursorAlignment_m937938588 (PlayerMoveController_t2811056080 * __this, Vector3_t2243707580  ___cursorWorldPosition0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerMoveController_HandleCursorAlignment_m937938588_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Quaternion_t4030073918  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	int32_t G_B4_0 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B8_0 = 0;
	int32_t G_B7_0 = 0;
	{
		Transform_t3275118058 * L_0 = __this->get_cursorObject_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		goto IL_0274;
	}

IL_0015:
	{
		Transform_t3275118058 * L_2 = __this->get_cursorObject_13();
		Vector3_t2243707580  L_3 = ___cursorWorldPosition0;
		NullCheck(L_2);
		Transform_set_position_m2469242620(L_2, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_4 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = L_4;
		float L_5 = (&V_5)->get_x_1();
		int32_t L_6 = ((((float)L_5) < ((float)(((float)((float)0)))))? 1 : 0);
		G_B3_0 = L_6;
		if (L_6)
		{
			G_B4_0 = L_6;
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_7 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_6 = L_7;
		float L_8 = (&V_6)->get_x_1();
		int32_t L_9 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B4_0 = ((((float)L_8) > ((float)(((float)((float)L_9)))))? 1 : 0);
	}

IL_0050:
	{
		int32_t L_10 = G_B4_0;
		G_B5_0 = L_10;
		if (L_10)
		{
			G_B6_0 = L_10;
			goto IL_0069;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_11 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_7 = L_11;
		float L_12 = (&V_7)->get_y_2();
		G_B6_0 = ((((float)L_12) < ((float)(((float)((float)0)))))? 1 : 0);
	}

IL_0069:
	{
		int32_t L_13 = G_B6_0;
		G_B7_0 = L_13;
		if (L_13)
		{
			G_B8_0 = L_13;
			goto IL_0086;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_14 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_8 = L_14;
		float L_15 = (&V_8)->get_y_2();
		int32_t L_16 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = ((((float)L_15) > ((float)(((float)((float)L_16)))))? 1 : 0);
	}

IL_0086:
	{
		Cursor_set_visible_m860533511(NULL /*static, unused*/, (bool)G_B8_0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_17 = __this->get_cursorObject_13();
		NullCheck(L_17);
		Quaternion_t4030073918  L_18 = Transform_get_rotation_m1033555130(L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		MovementMotor_t1612021398 * L_19 = __this->get_motor_2();
		NullCheck(L_19);
		Vector3_t2243707580  L_20 = L_19->get_facingDirection_4();
		Vector3_t2243707580  L_21 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_22 = Vector3_op_Inequality_m799191452(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00c2;
		}
	}
	{
		MovementMotor_t1612021398 * L_23 = __this->get_motor_2();
		NullCheck(L_23);
		Vector3_t2243707580  L_24 = L_23->get_facingDirection_4();
		Quaternion_t4030073918  L_25 = Quaternion_LookRotation_m633695927(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		V_0 = L_25;
	}

IL_00c2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_26 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t189460977 * L_27 = __this->get_mainCamera_12();
		Transform_t3275118058 * L_28 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t2243707580  L_29 = Transform_get_position_m1104419803(L_28, /*hidden argument*/NULL);
		Transform_t3275118058 * L_30 = __this->get_character_3();
		NullCheck(L_30);
		Vector3_t2243707580  L_31 = Transform_get_up_m1603627763(L_30, /*hidden argument*/NULL);
		float L_32 = __this->get_cursorPlaneHeight_8();
		Vector3_t2243707580  L_33 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		Vector3_t2243707580  L_34 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_29, L_33, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector3_t2243707580  L_35 = Camera_WorldToScreenPoint_m638747266(L_27, L_34, /*hidden argument*/NULL);
		Vector3_t2243707580  L_36 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_26, L_35, /*hidden argument*/NULL);
		V_1 = L_36;
		(&V_1)->set_z_3((((float)((float)0))));
		Transform_t3275118058 * L_37 = __this->get_mainCameraTransform_16();
		NullCheck(L_37);
		Quaternion_t4030073918  L_38 = Transform_get_rotation_m1033555130(L_37, /*hidden argument*/NULL);
		Vector3_t2243707580  L_39 = V_1;
		Vector3_t2243707580  L_40 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = Vector3_op_UnaryNegation_m3383802608(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_42 = Quaternion_LookRotation_m700700634(NULL /*static, unused*/, L_39, L_41, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_43 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_38, L_42, /*hidden argument*/NULL);
		V_2 = L_43;
		Transform_t3275118058 * L_44 = __this->get_cursorObject_13();
		Quaternion_t4030073918  L_45 = V_0;
		Quaternion_t4030073918  L_46 = V_2;
		float L_47 = __this->get_cursorFacingCamera_9();
		Quaternion_t4030073918  L_48 = Quaternion_Slerp_m1992855400(NULL /*static, unused*/, L_45, L_46, L_47, /*hidden argument*/NULL);
		NullCheck(L_44);
		Transform_set_rotation_m3411284563(L_44, L_48, /*hidden argument*/NULL);
		Vector3_t2243707580  L_49 = ___cursorWorldPosition0;
		Transform_t3275118058 * L_50 = __this->get_mainCameraTransform_16();
		NullCheck(L_50);
		Vector3_t2243707580  L_51 = Transform_get_position_m1104419803(L_50, /*hidden argument*/NULL);
		Vector3_t2243707580  L_52 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_49, L_51, /*hidden argument*/NULL);
		Transform_t3275118058 * L_53 = __this->get_mainCameraTransform_16();
		NullCheck(L_53);
		Vector3_t2243707580  L_54 = Transform_get_forward_m1833488937(L_53, /*hidden argument*/NULL);
		float L_55 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_52, L_54, /*hidden argument*/NULL);
		V_3 = ((float)((float)(0.1f)*(float)L_55));
		MovementMotor_t1612021398 * L_56 = __this->get_motor_2();
		NullCheck(L_56);
		Vector3_t2243707580 * L_57 = L_56->get_address_of_facingDirection_4();
		float L_58 = Vector3_get_magnitude_m860342598(L_57, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_59 = Mathf_InverseLerp_m55890283(NULL /*static, unused*/, (0.5f), (4.0f), L_58, /*hidden argument*/NULL);
		float L_60 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, (0.7f), (1.0f), L_59, /*hidden argument*/NULL);
		V_4 = L_60;
		Transform_t3275118058 * L_61 = __this->get_cursorObject_13();
		Vector3_t2243707580  L_62 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_63 = V_3;
		float L_64 = __this->get_cursorSmallerWithDistance_10();
		float L_65 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_63, (((float)((float)1))), L_64, /*hidden argument*/NULL);
		Vector3_t2243707580  L_66 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_62, L_65, /*hidden argument*/NULL);
		float L_67 = V_4;
		Vector3_t2243707580  L_68 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_66, L_67, /*hidden argument*/NULL);
		NullCheck(L_61);
		Transform_set_localScale_m2325460848(L_61, L_68, /*hidden argument*/NULL);
		bool L_69 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)111), /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_01e6;
		}
	}
	{
		float L_70 = __this->get_cursorFacingCamera_9();
		float L_71 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_cursorFacingCamera_9(((float)((float)L_70+(float)((float)((float)L_71*(float)(0.5f))))));
	}

IL_01e6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_72 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)112), /*hidden argument*/NULL);
		if (!L_72)
		{
			goto IL_020a;
		}
	}
	{
		float L_73 = __this->get_cursorFacingCamera_9();
		float L_74 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_cursorFacingCamera_9(((float)((float)L_73-(float)((float)((float)L_74*(float)(0.5f))))));
	}

IL_020a:
	{
		float L_75 = __this->get_cursorFacingCamera_9();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_76 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, L_75, /*hidden argument*/NULL);
		__this->set_cursorFacingCamera_9(L_76);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_77 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)107), /*hidden argument*/NULL);
		if (!L_77)
		{
			goto IL_023f;
		}
	}
	{
		float L_78 = __this->get_cursorSmallerWithDistance_10();
		float L_79 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_cursorSmallerWithDistance_10(((float)((float)L_78+(float)((float)((float)L_79*(float)(0.5f))))));
	}

IL_023f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_80 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)108), /*hidden argument*/NULL);
		if (!L_80)
		{
			goto IL_0263;
		}
	}
	{
		float L_81 = __this->get_cursorSmallerWithDistance_10();
		float L_82 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_cursorSmallerWithDistance_10(((float)((float)L_81-(float)((float)((float)L_82*(float)(0.5f))))));
	}

IL_0263:
	{
		float L_83 = __this->get_cursorSmallerWithDistance_10();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_84 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, L_83, /*hidden argument*/NULL);
		__this->set_cursorSmallerWithDistance_10(L_84);
	}

IL_0274:
	{
		return;
	}
}
// System.Void PlayerMoveController::Main()
extern "C"  void PlayerMoveController_Main_m3485452087 (PlayerMoveController_t2811056080 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PlaySound::.ctor()
extern "C"  void PlaySound__ctor_m2785613205 (PlaySound_t4257033973 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlaySound::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAudioSource_t1135106623_m1132397112_MethodInfo_var;
extern const uint32_t PlaySound_Awake_m2154727320_MetadataUsageId;
extern "C"  void PlaySound_Awake_m2154727320 (PlaySound_t4257033973 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlaySound_Awake_m2154727320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_t1135106623 * L_0 = __this->get_audioSource_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_002c;
		}
	}
	{
		AudioSource_t1135106623 * L_2 = Component_GetComponent_TisAudioSource_t1135106623_m1132397112(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m1132397112_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		AudioSource_t1135106623 * L_4 = Component_GetComponent_TisAudioSource_t1135106623_m1132397112(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m1132397112_MethodInfo_var);
		__this->set_audioSource_2(L_4);
	}

IL_002c:
	{
		return;
	}
}
// System.Void PlaySound::OnSignal()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t PlaySound_OnSignal_m4111258042_MetadataUsageId;
extern "C"  void PlaySound_OnSignal_m4111258042 (PlaySound_t4257033973 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlaySound_OnSignal_m4111258042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioClip_t1932558630 * L_0 = __this->get_sound_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		AudioSource_t1135106623 * L_2 = __this->get_audioSource_2();
		AudioClip_t1932558630 * L_3 = __this->get_sound_3();
		NullCheck(L_2);
		AudioSource_set_clip_m738814682(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0021:
	{
		AudioSource_t1135106623 * L_4 = __this->get_audioSource_2();
		NullCheck(L_4);
		AudioSource_Play_m353744792(L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlaySound::Main()
extern "C"  void PlaySound_Main_m1887946366 (PlaySound_t4257033973 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ReceiverItem::.ctor()
extern Il2CppCodeGenString* _stringLiteral3787901919;
extern const uint32_t ReceiverItem__ctor_m1109735248_MetadataUsageId;
extern "C"  void ReceiverItem__ctor_m1109735248 (ReceiverItem_t169526838 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReceiverItem__ctor_m1109735248_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_action_1(_stringLiteral3787901919);
		return;
	}
}
// System.Collections.IEnumerator ReceiverItem::SendWithDelay(UnityEngine.MonoBehaviour)
extern Il2CppClass* U24SendWithDelayU2486_t3482389000_il2cpp_TypeInfo_var;
extern const uint32_t ReceiverItem_SendWithDelay_m1045708218_MetadataUsageId;
extern "C"  Il2CppObject * ReceiverItem_SendWithDelay_m1045708218 (ReceiverItem_t169526838 * __this, MonoBehaviour_t1158329972 * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReceiverItem_SendWithDelay_m1045708218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_t1158329972 * L_0 = ___sender0;
		U24SendWithDelayU2486_t3482389000 * L_1 = (U24SendWithDelayU2486_t3482389000 *)il2cpp_codegen_object_new(U24SendWithDelayU2486_t3482389000_il2cpp_TypeInfo_var);
		U24SendWithDelayU2486__ctor_m2207965465(L_1, L_0, __this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Il2CppObject* L_2 = U24SendWithDelayU2486_GetEnumerator_m2551330026(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void ReceiverItem/$SendWithDelay$86::.ctor(UnityEngine.MonoBehaviour,ReceiverItem)
extern const MethodInfo* GenericGenerator_1__ctor_m1542724045_MethodInfo_var;
extern const uint32_t U24SendWithDelayU2486__ctor_m2207965465_MetadataUsageId;
extern "C"  void U24SendWithDelayU2486__ctor_m2207965465 (U24SendWithDelayU2486_t3482389000 * __this, MonoBehaviour_t1158329972 * ___sender0, ReceiverItem_t169526838 * ___self_1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24SendWithDelayU2486__ctor_m2207965465_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GenericGenerator_1__ctor_m1542724045(__this, /*hidden argument*/GenericGenerator_1__ctor_m1542724045_MethodInfo_var);
		MonoBehaviour_t1158329972 * L_0 = ___sender0;
		__this->set_U24senderU2489_0(L_0);
		ReceiverItem_t169526838 * L_1 = ___self_1;
		__this->set_U24self_U2490_1(L_1);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> ReceiverItem/$SendWithDelay$86::GetEnumerator()
extern Il2CppClass* U24_t4172685971_il2cpp_TypeInfo_var;
extern const uint32_t U24SendWithDelayU2486_GetEnumerator_m2551330026_MetadataUsageId;
extern "C"  Il2CppObject* U24SendWithDelayU2486_GetEnumerator_m2551330026 (U24SendWithDelayU2486_t3482389000 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24SendWithDelayU2486_GetEnumerator_m2551330026_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_t1158329972 * L_0 = __this->get_U24senderU2489_0();
		ReceiverItem_t169526838 * L_1 = __this->get_U24self_U2490_1();
		U24_t4172685971 * L_2 = (U24_t4172685971 *)il2cpp_codegen_object_new(U24_t4172685971_il2cpp_TypeInfo_var);
		U24__ctor_m2301840438(L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void ReceiverItem/$SendWithDelay$86/$::.ctor(UnityEngine.MonoBehaviour,ReceiverItem)
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m3451731617_MethodInfo_var;
extern const uint32_t U24__ctor_m2301840438_MetadataUsageId;
extern "C"  void U24__ctor_m2301840438 (U24_t4172685971 * __this, MonoBehaviour_t1158329972 * ___sender0, ReceiverItem_t169526838 * ___self_1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m2301840438_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m3451731617(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m3451731617_MethodInfo_var);
		MonoBehaviour_t1158329972 * L_0 = ___sender0;
		__this->set_U24senderU2487_2(L_0);
		ReceiverItem_t169526838 * L_1 = ___self_1;
		__this->set_U24self_U2488_3(L_1);
		return;
	}
}
// System.Boolean ReceiverItem/$SendWithDelay$86/$::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimeServices_t1910041954_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_Yield_m1415833518_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m3507348778_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2639417149;
extern Il2CppCodeGenString* _stringLiteral3896074972;
extern Il2CppCodeGenString* _stringLiteral455075110;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t U24_MoveNext_m2790908525_MetadataUsageId;
extern "C"  bool U24_MoveNext_m2790908525 (U24_t4172685971 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m2790908525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B7_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t300505933 *)__this)->get__state_1();
		if (L_0 == 0)
		{
			goto IL_0017;
		}
		if (L_0 == 1)
		{
			goto IL_00d3;
		}
		if (L_0 == 2)
		{
			goto IL_0033;
		}
	}

IL_0017:
	{
		ReceiverItem_t169526838 * L_1 = __this->get_U24self_U2488_3();
		NullCheck(L_1);
		float L_2 = L_1->get_delay_2();
		WaitForSeconds_t3839502067 * L_3 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_3, L_2, /*hidden argument*/NULL);
		bool L_4 = GenericGeneratorEnumerator_1_Yield_m1415833518(__this, 2, L_3, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m1415833518_MethodInfo_var);
		G_B7_0 = ((int32_t)(L_4));
		goto IL_00d4;
	}

IL_0033:
	{
		ReceiverItem_t169526838 * L_5 = __this->get_U24self_U2488_3();
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = L_5->get_receiver_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0068;
		}
	}
	{
		ReceiverItem_t169526838 * L_8 = __this->get_U24self_U2488_3();
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = L_8->get_receiver_0();
		ReceiverItem_t169526838 * L_10 = __this->get_U24self_U2488_3();
		NullCheck(L_10);
		String_t* L_11 = L_10->get_action_1();
		NullCheck(L_9);
		GameObject_SendMessage_m1177535567(L_9, L_11, /*hidden argument*/NULL);
		goto IL_00cb;
	}

IL_0068:
	{
		ReceiverItem_t169526838 * L_12 = __this->get_U24self_U2488_3();
		NullCheck(L_12);
		String_t* L_13 = L_12->get_action_1();
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		String_t* L_14 = RuntimeServices_op_Addition_m1630013314(NULL /*static, unused*/, _stringLiteral2639417149, L_13, /*hidden argument*/NULL);
		String_t* L_15 = RuntimeServices_op_Addition_m1630013314(NULL /*static, unused*/, L_14, _stringLiteral3896074972, /*hidden argument*/NULL);
		MonoBehaviour_t1158329972 * L_16 = __this->get_U24senderU2487_2();
		NullCheck(L_16);
		String_t* L_17 = Object_get_name_m2079638459(L_16, /*hidden argument*/NULL);
		String_t* L_18 = RuntimeServices_op_Addition_m1630013314(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		String_t* L_19 = RuntimeServices_op_Addition_m1630013314(NULL /*static, unused*/, L_18, _stringLiteral455075110, /*hidden argument*/NULL);
		MonoBehaviour_t1158329972 * L_20 = __this->get_U24senderU2487_2();
		NullCheck(L_20);
		Type_t * L_21 = Object_GetType_m191970594(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_21);
		String_t* L_23 = RuntimeServices_op_Addition_m1630013314(NULL /*static, unused*/, L_19, L_22, /*hidden argument*/NULL);
		String_t* L_24 = RuntimeServices_op_Addition_m1630013314(NULL /*static, unused*/, L_23, _stringLiteral372029317, /*hidden argument*/NULL);
		MonoBehaviour_t1158329972 * L_25 = __this->get_U24senderU2487_2();
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m1280021602(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00cb:
	{
		GenericGeneratorEnumerator_1_YieldDefault_m3507348778(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m3507348778_MethodInfo_var);
	}

IL_00d3:
	{
		G_B7_0 = 0;
	}

IL_00d4:
	{
		return (bool)G_B7_0;
	}
}
// System.Void SelfIlluminationBlink::.ctor()
extern "C"  void SelfIlluminationBlink__ctor_m69264487 (SelfIlluminationBlink_t1392230251 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SelfIlluminationBlink::OnWillRenderObject()
extern const MethodInfo* Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2677592887;
extern const uint32_t SelfIlluminationBlink_OnWillRenderObject_m1699045963_MetadataUsageId;
extern "C"  void SelfIlluminationBlink_OnWillRenderObject_m1699045963 (SelfIlluminationBlink_t1392230251 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelfIlluminationBlink_OnWillRenderObject_m1699045963_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Renderer_t257310565 * L_0 = Component_GetComponent_TisRenderer_t257310565_m692268576(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var);
		NullCheck(L_0);
		Material_t193706927 * L_1 = Renderer_get_sharedMaterial_m155010392(L_0, /*hidden argument*/NULL);
		float L_2 = __this->get_blink_2();
		NullCheck(L_1);
		Material_SetFloat_m1926275467(L_1, _stringLiteral2677592887, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SelfIlluminationBlink::Blink()
extern "C"  void SelfIlluminationBlink_Blink_m2362217849 (SelfIlluminationBlink_t1392230251 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_blink_2();
		__this->set_blink_2(((float)((float)(1.0f)-(float)L_0)));
		return;
	}
}
// System.Void SelfIlluminationBlink::Main()
extern "C"  void SelfIlluminationBlink_Main_m498202930 (SelfIlluminationBlink_t1392230251 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SignalSender::.ctor()
extern "C"  void SignalSender__ctor_m1453183343 (SignalSender_t1204926691 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SignalSender::SendSignals(UnityEngine.MonoBehaviour)
extern "C"  void SignalSender_SendSignals_m4066726171 (SignalSender_t1204926691 * __this, MonoBehaviour_t1158329972 * ___sender0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_hasFired_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = __this->get_onlyOnce_0();
		if (L_1)
		{
			goto IL_004e;
		}
	}

IL_0016:
	{
		V_0 = 0;
		goto IL_0036;
	}

IL_001d:
	{
		MonoBehaviour_t1158329972 * L_2 = ___sender0;
		ReceiverItemU5BU5D_t252820531* L_3 = __this->get_receivers_1();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		ReceiverItem_t169526838 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		MonoBehaviour_t1158329972 * L_7 = ___sender0;
		NullCheck(L_6);
		Il2CppObject * L_8 = VirtFuncInvoker1< Il2CppObject *, MonoBehaviour_t1158329972 * >::Invoke(4 /* System.Collections.IEnumerator ReceiverItem::SendWithDelay(UnityEngine.MonoBehaviour) */, L_6, L_7);
		NullCheck(L_2);
		MonoBehaviour_StartCoroutine_m2470621050(L_2, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_0;
		ReceiverItemU5BU5D_t252820531* L_11 = __this->get_receivers_1();
		int32_t L_12 = Extensions_get_length_m2475420192(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_001d;
		}
	}
	{
		__this->set_hasFired_2((bool)1);
	}

IL_004e:
	{
		return;
	}
}
// System.Void SimpleBullet::.ctor()
extern "C"  void SimpleBullet__ctor_m3484848692 (SimpleBullet_t2114900010 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_speed_2((((float)((float)((int32_t)10)))));
		__this->set_lifeTime_3((0.5f));
		__this->set_dist_4((((float)((float)((int32_t)10000)))));
		return;
	}
}
// System.Void SimpleBullet::OnEnable()
extern "C"  void SimpleBullet_OnEnable_m967044912 (SimpleBullet_t2114900010 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		__this->set_tr_6(L_0);
		float L_1 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_spawnTime_5(L_1);
		return;
	}
}
// System.Void SimpleBullet::Update()
extern "C"  void SimpleBullet_Update_m3908147799 (SimpleBullet_t2114900010 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = __this->get_tr_6();
		Transform_t3275118058 * L_1 = __this->get_tr_6();
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = __this->get_tr_6();
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_forward_m1833488937(L_3, /*hidden argument*/NULL);
		float L_5 = __this->get_speed_2();
		Vector3_t2243707580  L_6 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		float L_7 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_2, L_8, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m2469242620(L_0, L_9, /*hidden argument*/NULL);
		float L_10 = __this->get_dist_4();
		float L_11 = __this->get_speed_2();
		float L_12 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_dist_4(((float)((float)L_10-(float)((float)((float)L_11*(float)L_12)))));
		float L_13 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_14 = __this->get_spawnTime_5();
		float L_15 = __this->get_lifeTime_3();
		if ((((float)L_13) > ((float)((float)((float)L_14+(float)L_15)))))
		{
			goto IL_0078;
		}
	}
	{
		float L_16 = __this->get_dist_4();
		if ((((float)L_16) >= ((float)(((float)((float)0))))))
		{
			goto IL_0083;
		}
	}

IL_0078:
	{
		GameObject_t1756533147 * L_17 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Spawner_Destroy_m590425740(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
	}

IL_0083:
	{
		return;
	}
}
// System.Void SimpleBullet::Main()
extern "C"  void SimpleBullet_Main_m2930733257 (SimpleBullet_t2114900010 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SpawnAtCheckpoint::.ctor()
extern "C"  void SpawnAtCheckpoint__ctor_m2466345104 (SpawnAtCheckpoint_t58124256 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnAtCheckpoint::OnSignal()
extern "C"  void SpawnAtCheckpoint_OnSignal_m1435128297 (SpawnAtCheckpoint_t58124256 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_1 = __this->get_checkpoint_2();
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m2469242620(L_0, L_2, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = __this->get_checkpoint_2();
		NullCheck(L_4);
		Quaternion_t4030073918  L_5 = Transform_get_rotation_m1033555130(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_rotation_m3411284563(L_3, L_5, /*hidden argument*/NULL);
		SpawnAtCheckpoint_ResetHealthOnAll_m1221346643(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnAtCheckpoint::ResetHealthOnAll()
extern const Il2CppType* Health_t2683907638_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* HealthU5BU5D_t2451878963_il2cpp_TypeInfo_var;
extern const uint32_t SpawnAtCheckpoint_ResetHealthOnAll_m1221346643_MetadataUsageId;
extern "C"  void SpawnAtCheckpoint_ResetHealthOnAll_m1221346643 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnAtCheckpoint_ResetHealthOnAll_m1221346643_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HealthU5BU5D_t2451878963* V_0 = NULL;
	Health_t2683907638 * V_1 = NULL;
	int32_t V_2 = 0;
	HealthU5BU5D_t2451878963* V_3 = NULL;
	int32_t V_4 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Health_t2683907638_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_1 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((HealthU5BU5D_t2451878963*)Castclass(L_1, HealthU5BU5D_t2451878963_il2cpp_TypeInfo_var));
		V_2 = 0;
		HealthU5BU5D_t2451878963* L_2 = V_0;
		V_3 = L_2;
		HealthU5BU5D_t2451878963* L_3 = V_3;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_3);
		int32_t L_4 = Array_get_Length_m1498215565((Il2CppArray *)(Il2CppArray *)L_3, /*hidden argument*/NULL);
		V_4 = L_4;
		goto IL_0043;
	}

IL_0026:
	{
		HealthU5BU5D_t2451878963* L_5 = V_3;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		Health_t2683907638 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		L_8->set_dead_6((bool)0);
		HealthU5BU5D_t2451878963* L_9 = V_3;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Health_t2683907638 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		HealthU5BU5D_t2451878963* L_13 = V_3;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Health_t2683907638 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_16);
		float L_17 = L_16->get_maxHealth_2();
		NullCheck(L_12);
		L_12->set_health_3(L_17);
		int32_t L_18 = V_2;
		V_2 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0043:
	{
		int32_t L_19 = V_2;
		int32_t L_20 = V_4;
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0026;
		}
	}
	{
		return;
	}
}
// System.Void SpawnAtCheckpoint::Main()
extern "C"  void SpawnAtCheckpoint_Main_m3793371665 (SpawnAtCheckpoint_t58124256 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Spawner::.ctor()
extern "C"  void Spawner__ctor_m929082488 (Spawner_t534830648 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spawner::Awake()
extern Il2CppClass* Spawner_t534830648_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern const uint32_t Spawner_Awake_m1560756707_MetadataUsageId;
extern "C"  void Spawner_Awake_m1560756707 (Spawner_t534830648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Spawner_Awake_m1560756707_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		((Spawner_t534830648_StaticFields*)Spawner_t534830648_il2cpp_TypeInfo_var->static_fields)->set_spawner_2(__this);
		V_0 = 0;
		V_1 = 0;
		goto IL_0030;
	}

IL_000f:
	{
		ObjectCacheU5BU5D_t1759149002* L_0 = __this->get_caches_3();
		int32_t L_1 = V_1;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		ObjectCache_t960934699 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(4 /* System.Void ObjectCache::Initialize() */, L_3);
		int32_t L_4 = V_0;
		ObjectCacheU5BU5D_t1759149002* L_5 = __this->get_caches_3();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		ObjectCache_t960934699 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		int32_t L_9 = L_8->get_cacheSize_1();
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)L_9));
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0030:
	{
		int32_t L_11 = V_1;
		ObjectCacheU5BU5D_t1759149002* L_12 = __this->get_caches_3();
		int32_t L_13 = Extensions_get_length_m2475420192(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_14 = V_0;
		Hashtable_t909839986 * L_15 = (Hashtable_t909839986 *)il2cpp_codegen_object_new(Hashtable_t909839986_il2cpp_TypeInfo_var);
		Hashtable__ctor_m2702360557(L_15, L_14, /*hidden argument*/NULL);
		__this->set_activeCachedObjects_4(L_15);
		return;
	}
}
// UnityEngine.GameObject Spawner::Spawn(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern Il2CppClass* Spawner_t534830648_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimeServices_t1910041954_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m1577880517_MethodInfo_var;
extern const uint32_t Spawner_Spawn_m4269243264_MetadataUsageId;
extern "C"  GameObject_t1756533147 * Spawner_Spawn_m4269243264 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___prefab0, Vector3_t2243707580  ___position1, Quaternion_t4030073918  ___rotation2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Spawner_Spawn_m4269243264_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectCache_t960934699 * V_0 = NULL;
	int32_t V_1 = 0;
	GameObject_t1756533147 * V_2 = NULL;
	GameObject_t1756533147 * G_B9_0 = NULL;
	{
		V_0 = (ObjectCache_t960934699 *)NULL;
		Spawner_t534830648 * L_0 = ((Spawner_t534830648_StaticFields*)Spawner_t534830648_il2cpp_TypeInfo_var->static_fields)->get_spawner_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005a;
		}
	}
	{
		V_1 = 0;
		goto IL_0045;
	}

IL_0018:
	{
		Spawner_t534830648 * L_2 = ((Spawner_t534830648_StaticFields*)Spawner_t534830648_il2cpp_TypeInfo_var->static_fields)->get_spawner_2();
		NullCheck(L_2);
		ObjectCacheU5BU5D_t1759149002* L_3 = L_2->get_caches_3();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		ObjectCache_t960934699 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = L_6->get_prefab_0();
		GameObject_t1756533147 * L_8 = ___prefab0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0041;
		}
	}
	{
		Spawner_t534830648 * L_10 = ((Spawner_t534830648_StaticFields*)Spawner_t534830648_il2cpp_TypeInfo_var->static_fields)->get_spawner_2();
		NullCheck(L_10);
		ObjectCacheU5BU5D_t1759149002* L_11 = L_10->get_caches_3();
		int32_t L_12 = V_1;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		ObjectCache_t960934699 * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		V_0 = L_14;
	}

IL_0041:
	{
		int32_t L_15 = V_1;
		V_1 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0045:
	{
		int32_t L_16 = V_1;
		Spawner_t534830648 * L_17 = ((Spawner_t534830648_StaticFields*)Spawner_t534830648_il2cpp_TypeInfo_var->static_fields)->get_spawner_2();
		NullCheck(L_17);
		ObjectCacheU5BU5D_t1759149002* L_18 = L_17->get_caches_3();
		int32_t L_19 = Extensions_get_length_m2475420192(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_18, /*hidden argument*/NULL);
		if ((((int32_t)L_16) < ((int32_t)L_19)))
		{
			goto IL_0018;
		}
	}

IL_005a:
	{
		ObjectCache_t960934699 * L_20 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		bool L_21 = RuntimeServices_EqualityOperator_m2233200645(NULL /*static, unused*/, L_20, NULL, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0078;
		}
	}
	{
		GameObject_t1756533147 * L_22 = ___prefab0;
		Vector3_t2243707580  L_23 = ___position1;
		Quaternion_t4030073918  L_24 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_25 = Object_Instantiate_TisGameObject_t1756533147_m1577880517(NULL /*static, unused*/, L_22, L_23, L_24, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m1577880517_MethodInfo_var);
		G_B9_0 = ((GameObject_t1756533147 *)IsInstSealed(L_25, GameObject_t1756533147_il2cpp_TypeInfo_var));
		goto IL_00b5;
	}

IL_0078:
	{
		ObjectCache_t960934699 * L_26 = V_0;
		NullCheck(L_26);
		GameObject_t1756533147 * L_27 = VirtFuncInvoker0< GameObject_t1756533147 * >::Invoke(5 /* UnityEngine.GameObject ObjectCache::GetNextObjectInCache() */, L_26);
		V_2 = L_27;
		GameObject_t1756533147 * L_28 = V_2;
		NullCheck(L_28);
		Transform_t3275118058 * L_29 = GameObject_get_transform_m909382139(L_28, /*hidden argument*/NULL);
		Vector3_t2243707580  L_30 = ___position1;
		NullCheck(L_29);
		Transform_set_position_m2469242620(L_29, L_30, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_31 = V_2;
		NullCheck(L_31);
		Transform_t3275118058 * L_32 = GameObject_get_transform_m909382139(L_31, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_33 = ___rotation2;
		NullCheck(L_32);
		Transform_set_rotation_m3411284563(L_32, L_33, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_34 = V_2;
		NullCheck(L_34);
		GameObject_SetActive_m2887581199(L_34, (bool)1, /*hidden argument*/NULL);
		Spawner_t534830648 * L_35 = ((Spawner_t534830648_StaticFields*)Spawner_t534830648_il2cpp_TypeInfo_var->static_fields)->get_spawner_2();
		NullCheck(L_35);
		Hashtable_t909839986 * L_36 = L_35->get_activeCachedObjects_4();
		GameObject_t1756533147 * L_37 = V_2;
		bool L_38 = ((bool)1);
		Il2CppObject * L_39 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_38);
		NullCheck(L_36);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(27 /* System.Void System.Collections.Hashtable::set_Item(System.Object,System.Object) */, L_36, L_37, L_39);
		GameObject_t1756533147 * L_40 = V_2;
		G_B9_0 = L_40;
	}

IL_00b5:
	{
		return G_B9_0;
	}
}
// System.Void Spawner::Destroy(UnityEngine.GameObject)
extern Il2CppClass* Spawner_t534830648_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Spawner_Destroy_m590425740_MetadataUsageId;
extern "C"  void Spawner_Destroy_m590425740 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___objectToDestroy0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Spawner_Destroy_m590425740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Spawner_t534830648 * L_0 = ((Spawner_t534830648_StaticFields*)Spawner_t534830648_il2cpp_TypeInfo_var->static_fields)->get_spawner_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0046;
		}
	}
	{
		Spawner_t534830648 * L_2 = ((Spawner_t534830648_StaticFields*)Spawner_t534830648_il2cpp_TypeInfo_var->static_fields)->get_spawner_2();
		NullCheck(L_2);
		Hashtable_t909839986 * L_3 = L_2->get_activeCachedObjects_4();
		GameObject_t1756533147 * L_4 = ___objectToDestroy0;
		NullCheck(L_3);
		bool L_5 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(34 /* System.Boolean System.Collections.Hashtable::ContainsKey(System.Object) */, L_3, L_4);
		if (!L_5)
		{
			goto IL_0046;
		}
	}
	{
		GameObject_t1756533147 * L_6 = ___objectToDestroy0;
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)0, /*hidden argument*/NULL);
		Spawner_t534830648 * L_7 = ((Spawner_t534830648_StaticFields*)Spawner_t534830648_il2cpp_TypeInfo_var->static_fields)->get_spawner_2();
		NullCheck(L_7);
		Hashtable_t909839986 * L_8 = L_7->get_activeCachedObjects_4();
		GameObject_t1756533147 * L_9 = ___objectToDestroy0;
		bool L_10 = ((bool)0);
		Il2CppObject * L_11 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(27 /* System.Void System.Collections.Hashtable::set_Item(System.Object,System.Object) */, L_8, L_9, L_11);
		goto IL_004c;
	}

IL_0046:
	{
		GameObject_t1756533147 * L_12 = ___objectToDestroy0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
	}

IL_004c:
	{
		return;
	}
}
// System.Void Spawner::Main()
extern "C"  void Spawner_Main_m985549929 (Spawner_t534830648 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SpawnObject::.ctor()
extern "C"  void SpawnObject__ctor_m1860728308 (SpawnObject_t1174160328 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpawnObject::OnSignal()
extern "C"  void SpawnObject_OnSignal_m3589574635 (SpawnObject_t1174160328 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_objectToSpawn_2();
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Quaternion_t4030073918  L_4 = Transform_get_rotation_m1033555130(L_3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = Spawner_Spawn_m4269243264(NULL /*static, unused*/, L_0, L_2, L_4, /*hidden argument*/NULL);
		__this->set_spawned_4(L_5);
		SignalSender_t1204926691 * L_6 = __this->get_onDestroyedSignals_3();
		NullCheck(L_6);
		ReceiverItemU5BU5D_t252820531* L_7 = L_6->get_receivers_1();
		NullCheck((Il2CppArray *)(Il2CppArray *)L_7);
		int32_t L_8 = Array_get_Length_m1498215565((Il2CppArray *)(Il2CppArray *)L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_0044;
		}
	}
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0044:
	{
		return;
	}
}
// System.Void SpawnObject::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t SpawnObject_Update_m339265901_MetadataUsageId;
extern "C"  void SpawnObject_Update_m339265901 (SpawnObject_t1174160328 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnObject_Update_m339265901_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_spawned_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		GameObject_t1756533147 * L_2 = __this->get_spawned_4();
		NullCheck(L_2);
		bool L_3 = GameObject_get_activeInHierarchy_m4242915935(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0034;
		}
	}

IL_0021:
	{
		SignalSender_t1204926691 * L_4 = __this->get_onDestroyedSignals_3();
		NullCheck(L_4);
		VirtActionInvoker1< MonoBehaviour_t1158329972 * >::Invoke(4 /* System.Void SignalSender::SendSignals(UnityEngine.MonoBehaviour) */, L_4, __this);
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void SpawnObject::Main()
extern "C"  void SpawnObject_Main_m3502971719 (SpawnObject_t1174160328 * __this, const MethodInfo* method)
{
	{
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpiderAnimation::.ctor()
extern "C"  void SpiderAnimation__ctor_m2953202753 (SpiderAnimation_t75583237 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpiderAnimation::OnEnable()
extern const MethodInfo* Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var;
extern const uint32_t SpiderAnimation_OnEnable_m590839401_MetadataUsageId;
extern "C"  void SpiderAnimation_OnEnable_m590839401 (SpiderAnimation_t75583237 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpiderAnimation_OnEnable_m590839401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MovementMotor_t1612021398 * L_0 = __this->get_motor_2();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		__this->set_tr_12(L_1);
		Animation_t2068071072 * L_2 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_3 = __this->get_activateAnim_3();
		NullCheck(L_3);
		String_t* L_4 = Object_get_name_m2079638459(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		AnimationState_t1303741697 * L_5 = Animation_get_Item_m4198128320(L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		AnimationState_set_enabled_m2079619927(L_5, (bool)1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_6 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_7 = __this->get_activateAnim_3();
		NullCheck(L_7);
		String_t* L_8 = Object_get_name_m2079638459(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		AnimationState_t1303741697 * L_9 = Animation_get_Item_m4198128320(L_6, L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		AnimationState_set_weight_m2370306600(L_9, (((float)((float)1))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_10 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_11 = __this->get_activateAnim_3();
		NullCheck(L_11);
		String_t* L_12 = Object_get_name_m2079638459(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		AnimationState_t1303741697 * L_13 = Animation_get_Item_m4198128320(L_10, L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		AnimationState_set_time_m1882411177(L_13, (((float)((float)0))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_14 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_15 = __this->get_activateAnim_3();
		NullCheck(L_15);
		String_t* L_16 = Object_get_name_m2079638459(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		AnimationState_t1303741697 * L_17 = Animation_get_Item_m4198128320(L_14, L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		AnimationState_set_speed_m465014523(L_17, (((float)((float)1))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_18 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_19 = __this->get_forwardAnim_4();
		NullCheck(L_19);
		String_t* L_20 = Object_get_name_m2079638459(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		AnimationState_t1303741697 * L_21 = Animation_get_Item_m4198128320(L_18, L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		AnimationState_set_layer_m139053567(L_21, 1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_22 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_23 = __this->get_forwardAnim_4();
		NullCheck(L_23);
		String_t* L_24 = Object_get_name_m2079638459(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		AnimationState_t1303741697 * L_25 = Animation_get_Item_m4198128320(L_22, L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		AnimationState_set_enabled_m2079619927(L_25, (bool)1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_26 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_27 = __this->get_forwardAnim_4();
		NullCheck(L_27);
		String_t* L_28 = Object_get_name_m2079638459(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		AnimationState_t1303741697 * L_29 = Animation_get_Item_m4198128320(L_26, L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		AnimationState_set_weight_m2370306600(L_29, (((float)((float)0))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_30 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_31 = __this->get_backAnim_5();
		NullCheck(L_31);
		String_t* L_32 = Object_get_name_m2079638459(L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		AnimationState_t1303741697 * L_33 = Animation_get_Item_m4198128320(L_30, L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		AnimationState_set_layer_m139053567(L_33, 1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_34 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_35 = __this->get_backAnim_5();
		NullCheck(L_35);
		String_t* L_36 = Object_get_name_m2079638459(L_35, /*hidden argument*/NULL);
		NullCheck(L_34);
		AnimationState_t1303741697 * L_37 = Animation_get_Item_m4198128320(L_34, L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		AnimationState_set_enabled_m2079619927(L_37, (bool)1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_38 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_39 = __this->get_backAnim_5();
		NullCheck(L_39);
		String_t* L_40 = Object_get_name_m2079638459(L_39, /*hidden argument*/NULL);
		NullCheck(L_38);
		AnimationState_t1303741697 * L_41 = Animation_get_Item_m4198128320(L_38, L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		AnimationState_set_weight_m2370306600(L_41, (((float)((float)0))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_42 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_43 = __this->get_leftAnim_6();
		NullCheck(L_43);
		String_t* L_44 = Object_get_name_m2079638459(L_43, /*hidden argument*/NULL);
		NullCheck(L_42);
		AnimationState_t1303741697 * L_45 = Animation_get_Item_m4198128320(L_42, L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		AnimationState_set_layer_m139053567(L_45, 1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_46 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_47 = __this->get_leftAnim_6();
		NullCheck(L_47);
		String_t* L_48 = Object_get_name_m2079638459(L_47, /*hidden argument*/NULL);
		NullCheck(L_46);
		AnimationState_t1303741697 * L_49 = Animation_get_Item_m4198128320(L_46, L_48, /*hidden argument*/NULL);
		NullCheck(L_49);
		AnimationState_set_enabled_m2079619927(L_49, (bool)1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_50 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_51 = __this->get_leftAnim_6();
		NullCheck(L_51);
		String_t* L_52 = Object_get_name_m2079638459(L_51, /*hidden argument*/NULL);
		NullCheck(L_50);
		AnimationState_t1303741697 * L_53 = Animation_get_Item_m4198128320(L_50, L_52, /*hidden argument*/NULL);
		NullCheck(L_53);
		AnimationState_set_weight_m2370306600(L_53, (((float)((float)0))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_54 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_55 = __this->get_rightAnim_7();
		NullCheck(L_55);
		String_t* L_56 = Object_get_name_m2079638459(L_55, /*hidden argument*/NULL);
		NullCheck(L_54);
		AnimationState_t1303741697 * L_57 = Animation_get_Item_m4198128320(L_54, L_56, /*hidden argument*/NULL);
		NullCheck(L_57);
		AnimationState_set_layer_m139053567(L_57, 1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_58 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_59 = __this->get_rightAnim_7();
		NullCheck(L_59);
		String_t* L_60 = Object_get_name_m2079638459(L_59, /*hidden argument*/NULL);
		NullCheck(L_58);
		AnimationState_t1303741697 * L_61 = Animation_get_Item_m4198128320(L_58, L_60, /*hidden argument*/NULL);
		NullCheck(L_61);
		AnimationState_set_enabled_m2079619927(L_61, (bool)1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_62 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_63 = __this->get_rightAnim_7();
		NullCheck(L_63);
		String_t* L_64 = Object_get_name_m2079638459(L_63, /*hidden argument*/NULL);
		NullCheck(L_62);
		AnimationState_t1303741697 * L_65 = Animation_get_Item_m4198128320(L_62, L_64, /*hidden argument*/NULL);
		NullCheck(L_65);
		AnimationState_set_weight_m2370306600(L_65, (((float)((float)0))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpiderAnimation::OnDisable()
extern const MethodInfo* Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var;
extern const uint32_t SpiderAnimation_OnDisable_m85563204_MetadataUsageId;
extern "C"  void SpiderAnimation_OnDisable_m85563204 (SpiderAnimation_t75583237 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpiderAnimation_OnDisable_m85563204_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animation_t2068071072 * L_0 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_1 = __this->get_activateAnim_3();
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m2079638459(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		AnimationState_t1303741697 * L_3 = Animation_get_Item_m4198128320(L_0, L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		AnimationState_set_enabled_m2079619927(L_3, (bool)1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_4 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_5 = __this->get_activateAnim_3();
		NullCheck(L_5);
		String_t* L_6 = Object_get_name_m2079638459(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		AnimationState_t1303741697 * L_7 = Animation_get_Item_m4198128320(L_4, L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		AnimationState_set_weight_m2370306600(L_7, (((float)((float)1))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_8 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_9 = __this->get_activateAnim_3();
		NullCheck(L_9);
		String_t* L_10 = Object_get_name_m2079638459(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		AnimationState_t1303741697 * L_11 = Animation_get_Item_m4198128320(L_8, L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		AnimationState_set_normalizedTime_m3942659976(L_11, (((float)((float)1))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_12 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_13 = __this->get_activateAnim_3();
		NullCheck(L_13);
		String_t* L_14 = Object_get_name_m2079638459(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		AnimationState_t1303741697 * L_15 = Animation_get_Item_m4198128320(L_12, L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		AnimationState_set_speed_m465014523(L_15, (((float)((float)(-1)))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_16 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_17 = __this->get_activateAnim_3();
		NullCheck(L_17);
		String_t* L_18 = Object_get_name_m2079638459(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		Animation_CrossFade_m3167311838(L_16, L_18, (0.3f), 4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpiderAnimation::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var;
extern const uint32_t SpiderAnimation_Update_m3015082642_MetadataUsageId;
extern "C"  void SpiderAnimation_Update_m3015082642 (SpiderAnimation_t75583237 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpiderAnimation_Update_m3015082642_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		MovementMotor_t1612021398 * L_0 = __this->get_motor_2();
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = L_0->get_movementDirection_2();
		V_0 = L_1;
		(&V_0)->set_y_2((((float)((float)0))));
		float L_2 = Vector3_get_magnitude_m860342598((&V_0), /*hidden argument*/NULL);
		V_1 = L_2;
		Animation_t2068071072 * L_3 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_4 = __this->get_forwardAnim_4();
		NullCheck(L_4);
		String_t* L_5 = Object_get_name_m2079638459(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		AnimationState_t1303741697 * L_6 = Animation_get_Item_m4198128320(L_3, L_5, /*hidden argument*/NULL);
		float L_7 = V_1;
		NullCheck(L_6);
		AnimationState_set_speed_m465014523(L_6, L_7, /*hidden argument*/NULL);
		Animation_t2068071072 * L_8 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_9 = __this->get_rightAnim_7();
		NullCheck(L_9);
		String_t* L_10 = Object_get_name_m2079638459(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		AnimationState_t1303741697 * L_11 = Animation_get_Item_m4198128320(L_8, L_10, /*hidden argument*/NULL);
		float L_12 = V_1;
		NullCheck(L_11);
		AnimationState_set_speed_m465014523(L_11, L_12, /*hidden argument*/NULL);
		Animation_t2068071072 * L_13 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_14 = __this->get_backAnim_5();
		NullCheck(L_14);
		String_t* L_15 = Object_get_name_m2079638459(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		AnimationState_t1303741697 * L_16 = Animation_get_Item_m4198128320(L_13, L_15, /*hidden argument*/NULL);
		float L_17 = V_1;
		NullCheck(L_16);
		AnimationState_set_speed_m465014523(L_16, L_17, /*hidden argument*/NULL);
		Animation_t2068071072 * L_18 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_19 = __this->get_leftAnim_6();
		NullCheck(L_19);
		String_t* L_20 = Object_get_name_m2079638459(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		AnimationState_t1303741697 * L_21 = Animation_get_Item_m4198128320(L_18, L_20, /*hidden argument*/NULL);
		float L_22 = V_1;
		NullCheck(L_21);
		AnimationState_set_speed_m465014523(L_21, L_22, /*hidden argument*/NULL);
		Transform_t3275118058 * L_23 = __this->get_tr_12();
		NullCheck(L_23);
		Vector3_t2243707580  L_24 = Transform_get_forward_m1833488937(L_23, /*hidden argument*/NULL);
		float L_25 = SpiderAnimation_HorizontalAngle_m4160094253(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		Vector3_t2243707580  L_26 = V_0;
		float L_27 = SpiderAnimation_HorizontalAngle_m4160094253(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_28 = Mathf_DeltaAngle_m1024687732(NULL /*static, unused*/, L_25, L_27, /*hidden argument*/NULL);
		V_2 = L_28;
		float L_29 = V_1;
		if ((((float)L_29) <= ((float)(0.01f))))
		{
			goto IL_02f2;
		}
	}
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_3));
		float L_30 = V_2;
		if ((((float)L_30) >= ((float)(((float)((float)((int32_t)-90)))))))
		{
			goto IL_0150;
		}
	}
	{
		float L_31 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_32 = Mathf_InverseLerp_m55890283(NULL /*static, unused*/, (((float)((float)((int32_t)-180)))), (((float)((float)((int32_t)-90)))), L_31, /*hidden argument*/NULL);
		V_3 = L_32;
		Animation_t2068071072 * L_33 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_34 = __this->get_forwardAnim_4();
		NullCheck(L_34);
		String_t* L_35 = Object_get_name_m2079638459(L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		AnimationState_t1303741697 * L_36 = Animation_get_Item_m4198128320(L_33, L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		AnimationState_set_weight_m2370306600(L_36, (((float)((float)0))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_37 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_38 = __this->get_rightAnim_7();
		NullCheck(L_38);
		String_t* L_39 = Object_get_name_m2079638459(L_38, /*hidden argument*/NULL);
		NullCheck(L_37);
		AnimationState_t1303741697 * L_40 = Animation_get_Item_m4198128320(L_37, L_39, /*hidden argument*/NULL);
		NullCheck(L_40);
		AnimationState_set_weight_m2370306600(L_40, (((float)((float)0))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_41 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_42 = __this->get_backAnim_5();
		NullCheck(L_42);
		String_t* L_43 = Object_get_name_m2079638459(L_42, /*hidden argument*/NULL);
		NullCheck(L_41);
		AnimationState_t1303741697 * L_44 = Animation_get_Item_m4198128320(L_41, L_43, /*hidden argument*/NULL);
		float L_45 = V_3;
		NullCheck(L_44);
		AnimationState_set_weight_m2370306600(L_44, ((float)((float)(((float)((float)1)))-(float)L_45)), /*hidden argument*/NULL);
		Animation_t2068071072 * L_46 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_47 = __this->get_leftAnim_6();
		NullCheck(L_47);
		String_t* L_48 = Object_get_name_m2079638459(L_47, /*hidden argument*/NULL);
		NullCheck(L_46);
		AnimationState_t1303741697 * L_49 = Animation_get_Item_m4198128320(L_46, L_48, /*hidden argument*/NULL);
		NullCheck(L_49);
		AnimationState_set_weight_m2370306600(L_49, (((float)((float)1))), /*hidden argument*/NULL);
		goto IL_02f2;
	}

IL_0150:
	{
		float L_50 = V_2;
		if ((((float)L_50) >= ((float)(((float)((float)0))))))
		{
			goto IL_01de;
		}
	}
	{
		float L_51 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_52 = Mathf_InverseLerp_m55890283(NULL /*static, unused*/, (((float)((float)((int32_t)-90)))), (((float)((float)0))), L_51, /*hidden argument*/NULL);
		V_3 = L_52;
		Animation_t2068071072 * L_53 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_54 = __this->get_forwardAnim_4();
		NullCheck(L_54);
		String_t* L_55 = Object_get_name_m2079638459(L_54, /*hidden argument*/NULL);
		NullCheck(L_53);
		AnimationState_t1303741697 * L_56 = Animation_get_Item_m4198128320(L_53, L_55, /*hidden argument*/NULL);
		float L_57 = V_3;
		NullCheck(L_56);
		AnimationState_set_weight_m2370306600(L_56, L_57, /*hidden argument*/NULL);
		Animation_t2068071072 * L_58 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_59 = __this->get_rightAnim_7();
		NullCheck(L_59);
		String_t* L_60 = Object_get_name_m2079638459(L_59, /*hidden argument*/NULL);
		NullCheck(L_58);
		AnimationState_t1303741697 * L_61 = Animation_get_Item_m4198128320(L_58, L_60, /*hidden argument*/NULL);
		NullCheck(L_61);
		AnimationState_set_weight_m2370306600(L_61, (((float)((float)0))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_62 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_63 = __this->get_backAnim_5();
		NullCheck(L_63);
		String_t* L_64 = Object_get_name_m2079638459(L_63, /*hidden argument*/NULL);
		NullCheck(L_62);
		AnimationState_t1303741697 * L_65 = Animation_get_Item_m4198128320(L_62, L_64, /*hidden argument*/NULL);
		NullCheck(L_65);
		AnimationState_set_weight_m2370306600(L_65, (((float)((float)0))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_66 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_67 = __this->get_leftAnim_6();
		NullCheck(L_67);
		String_t* L_68 = Object_get_name_m2079638459(L_67, /*hidden argument*/NULL);
		NullCheck(L_66);
		AnimationState_t1303741697 * L_69 = Animation_get_Item_m4198128320(L_66, L_68, /*hidden argument*/NULL);
		float L_70 = V_3;
		NullCheck(L_69);
		AnimationState_set_weight_m2370306600(L_69, ((float)((float)(((float)((float)1)))-(float)L_70)), /*hidden argument*/NULL);
		goto IL_02f2;
	}

IL_01de:
	{
		float L_71 = V_2;
		if ((((float)L_71) >= ((float)(((float)((float)((int32_t)90)))))))
		{
			goto IL_026d;
		}
	}
	{
		float L_72 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_73 = Mathf_InverseLerp_m55890283(NULL /*static, unused*/, (((float)((float)0))), (((float)((float)((int32_t)90)))), L_72, /*hidden argument*/NULL);
		V_3 = L_73;
		Animation_t2068071072 * L_74 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_75 = __this->get_forwardAnim_4();
		NullCheck(L_75);
		String_t* L_76 = Object_get_name_m2079638459(L_75, /*hidden argument*/NULL);
		NullCheck(L_74);
		AnimationState_t1303741697 * L_77 = Animation_get_Item_m4198128320(L_74, L_76, /*hidden argument*/NULL);
		float L_78 = V_3;
		NullCheck(L_77);
		AnimationState_set_weight_m2370306600(L_77, ((float)((float)(((float)((float)1)))-(float)L_78)), /*hidden argument*/NULL);
		Animation_t2068071072 * L_79 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_80 = __this->get_rightAnim_7();
		NullCheck(L_80);
		String_t* L_81 = Object_get_name_m2079638459(L_80, /*hidden argument*/NULL);
		NullCheck(L_79);
		AnimationState_t1303741697 * L_82 = Animation_get_Item_m4198128320(L_79, L_81, /*hidden argument*/NULL);
		float L_83 = V_3;
		NullCheck(L_82);
		AnimationState_set_weight_m2370306600(L_82, L_83, /*hidden argument*/NULL);
		Animation_t2068071072 * L_84 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_85 = __this->get_backAnim_5();
		NullCheck(L_85);
		String_t* L_86 = Object_get_name_m2079638459(L_85, /*hidden argument*/NULL);
		NullCheck(L_84);
		AnimationState_t1303741697 * L_87 = Animation_get_Item_m4198128320(L_84, L_86, /*hidden argument*/NULL);
		NullCheck(L_87);
		AnimationState_set_weight_m2370306600(L_87, (((float)((float)0))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_88 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_89 = __this->get_leftAnim_6();
		NullCheck(L_89);
		String_t* L_90 = Object_get_name_m2079638459(L_89, /*hidden argument*/NULL);
		NullCheck(L_88);
		AnimationState_t1303741697 * L_91 = Animation_get_Item_m4198128320(L_88, L_90, /*hidden argument*/NULL);
		NullCheck(L_91);
		AnimationState_set_weight_m2370306600(L_91, (((float)((float)0))), /*hidden argument*/NULL);
		goto IL_02f2;
	}

IL_026d:
	{
		float L_92 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_93 = Mathf_InverseLerp_m55890283(NULL /*static, unused*/, (((float)((float)((int32_t)90)))), (((float)((float)((int32_t)180)))), L_92, /*hidden argument*/NULL);
		V_3 = L_93;
		Animation_t2068071072 * L_94 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_95 = __this->get_forwardAnim_4();
		NullCheck(L_95);
		String_t* L_96 = Object_get_name_m2079638459(L_95, /*hidden argument*/NULL);
		NullCheck(L_94);
		AnimationState_t1303741697 * L_97 = Animation_get_Item_m4198128320(L_94, L_96, /*hidden argument*/NULL);
		NullCheck(L_97);
		AnimationState_set_weight_m2370306600(L_97, (((float)((float)0))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_98 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_99 = __this->get_rightAnim_7();
		NullCheck(L_99);
		String_t* L_100 = Object_get_name_m2079638459(L_99, /*hidden argument*/NULL);
		NullCheck(L_98);
		AnimationState_t1303741697 * L_101 = Animation_get_Item_m4198128320(L_98, L_100, /*hidden argument*/NULL);
		float L_102 = V_3;
		NullCheck(L_101);
		AnimationState_set_weight_m2370306600(L_101, ((float)((float)(((float)((float)1)))-(float)L_102)), /*hidden argument*/NULL);
		Animation_t2068071072 * L_103 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_104 = __this->get_backAnim_5();
		NullCheck(L_104);
		String_t* L_105 = Object_get_name_m2079638459(L_104, /*hidden argument*/NULL);
		NullCheck(L_103);
		AnimationState_t1303741697 * L_106 = Animation_get_Item_m4198128320(L_103, L_105, /*hidden argument*/NULL);
		float L_107 = V_3;
		NullCheck(L_106);
		AnimationState_set_weight_m2370306600(L_106, L_107, /*hidden argument*/NULL);
		Animation_t2068071072 * L_108 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_109 = __this->get_leftAnim_6();
		NullCheck(L_109);
		String_t* L_110 = Object_get_name_m2079638459(L_109, /*hidden argument*/NULL);
		NullCheck(L_108);
		AnimationState_t1303741697 * L_111 = Animation_get_Item_m4198128320(L_108, L_110, /*hidden argument*/NULL);
		NullCheck(L_111);
		AnimationState_set_weight_m2370306600(L_111, (((float)((float)0))), /*hidden argument*/NULL);
	}

IL_02f2:
	{
		bool L_112 = __this->get_skiddingSounds_10();
		if (!L_112)
		{
			goto IL_034e;
		}
	}
	{
		float L_113 = V_1;
		if ((((float)L_113) <= ((float)(0.2f))))
		{
			goto IL_0328;
		}
	}
	{
		AudioSource_t1135106623 * L_114 = __this->get_audioSource_8();
		NullCheck(L_114);
		bool L_115 = AudioSource_get_isPlaying_m3677592677(L_114, /*hidden argument*/NULL);
		if (L_115)
		{
			goto IL_0328;
		}
	}
	{
		AudioSource_t1135106623 * L_116 = __this->get_audioSource_8();
		NullCheck(L_116);
		AudioSource_Play_m353744792(L_116, /*hidden argument*/NULL);
		goto IL_034e;
	}

IL_0328:
	{
		float L_117 = V_1;
		if ((((float)L_117) >= ((float)(0.2f))))
		{
			goto IL_034e;
		}
	}
	{
		AudioSource_t1135106623 * L_118 = __this->get_audioSource_8();
		NullCheck(L_118);
		bool L_119 = AudioSource_get_isPlaying_m3677592677(L_118, /*hidden argument*/NULL);
		if (!L_119)
		{
			goto IL_034e;
		}
	}
	{
		AudioSource_t1135106623 * L_120 = __this->get_audioSource_8();
		NullCheck(L_120);
		AudioSource_Pause_m71375470(L_120, /*hidden argument*/NULL);
	}

IL_034e:
	{
		bool L_121 = __this->get_footstepSounds_11();
		if (!L_121)
		{
			goto IL_03d3;
		}
	}
	{
		float L_122 = V_1;
		if ((((float)L_122) <= ((float)(0.2f))))
		{
			goto IL_03d3;
		}
	}
	{
		Animation_t2068071072 * L_123 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_124 = __this->get_forwardAnim_4();
		NullCheck(L_124);
		String_t* L_125 = Object_get_name_m2079638459(L_124, /*hidden argument*/NULL);
		NullCheck(L_123);
		AnimationState_t1303741697 * L_126 = Animation_get_Item_m4198128320(L_123, L_125, /*hidden argument*/NULL);
		NullCheck(L_126);
		float L_127 = AnimationState_get_normalizedTime_m3003675227(L_126, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_128 = Mathf_Repeat_m943844734(NULL /*static, unused*/, ((float)((float)((float)((float)L_127*(float)(((float)((float)4)))))+(float)(0.1f))), (((float)((float)1))), /*hidden argument*/NULL);
		V_4 = L_128;
		float L_129 = V_4;
		float L_130 = __this->get_lastAnimTime_14();
		if ((((float)L_129) >= ((float)L_130)))
		{
			goto IL_03cb;
		}
	}
	{
		float L_131 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_132 = __this->get_lastFootstepTime_13();
		if ((((float)L_131) <= ((float)((float)((float)L_132+(float)(0.1f))))))
		{
			goto IL_03cb;
		}
	}
	{
		SignalSender_t1204926691 * L_133 = __this->get_footstepSignals_9();
		NullCheck(L_133);
		VirtActionInvoker1< MonoBehaviour_t1158329972 * >::Invoke(4 /* System.Void SignalSender::SendSignals(UnityEngine.MonoBehaviour) */, L_133, __this);
		float L_134 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastFootstepTime_13(L_134);
	}

IL_03cb:
	{
		float L_135 = V_4;
		__this->set_lastAnimTime_14(L_135);
	}

IL_03d3:
	{
		return;
	}
}
// System.Single SpiderAnimation::HorizontalAngle(UnityEngine.Vector3)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t SpiderAnimation_HorizontalAngle_m4160094253_MetadataUsageId;
extern "C"  float SpiderAnimation_HorizontalAngle_m4160094253 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___direction0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpiderAnimation_HorizontalAngle_m4160094253_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = (&___direction0)->get_x_1();
		float L_1 = (&___direction0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = atan2f(L_0, L_1);
		return ((float)((float)L_2*(float)(57.29578f)));
	}
}
// System.Void SpiderAnimation::Main()
extern "C"  void SpiderAnimation_Main_m2176370190 (SpiderAnimation_t75583237 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SpiderAnimationTest::.ctor()
extern "C"  void SpiderAnimationTest__ctor_m3170889203 (SpiderAnimationTest_t3642224283 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpiderAnimationTest::OnEnable()
extern const MethodInfo* Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var;
extern const uint32_t SpiderAnimationTest_OnEnable_m3301736639_MetadataUsageId;
extern "C"  void SpiderAnimationTest_OnEnable_m3301736639 (SpiderAnimationTest_t3642224283 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpiderAnimationTest_OnEnable_m3301736639_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody_t4233889191 * L_0 = __this->get_rigid_2();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		__this->set_tr_9(L_1);
		Animation_t2068071072 * L_2 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_3 = __this->get_forwardAnim_3();
		NullCheck(L_3);
		String_t* L_4 = Object_get_name_m2079638459(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		AnimationState_t1303741697 * L_5 = Animation_get_Item_m4198128320(L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		AnimationState_set_layer_m139053567(L_5, 1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_6 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_7 = __this->get_forwardAnim_3();
		NullCheck(L_7);
		String_t* L_8 = Object_get_name_m2079638459(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		AnimationState_t1303741697 * L_9 = Animation_get_Item_m4198128320(L_6, L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		AnimationState_set_enabled_m2079619927(L_9, (bool)1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_10 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_11 = __this->get_backAnim_4();
		NullCheck(L_11);
		String_t* L_12 = Object_get_name_m2079638459(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		AnimationState_t1303741697 * L_13 = Animation_get_Item_m4198128320(L_10, L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		AnimationState_set_layer_m139053567(L_13, 1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_14 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_15 = __this->get_backAnim_4();
		NullCheck(L_15);
		String_t* L_16 = Object_get_name_m2079638459(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		AnimationState_t1303741697 * L_17 = Animation_get_Item_m4198128320(L_14, L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		AnimationState_set_enabled_m2079619927(L_17, (bool)1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_18 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_19 = __this->get_leftAnim_5();
		NullCheck(L_19);
		String_t* L_20 = Object_get_name_m2079638459(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		AnimationState_t1303741697 * L_21 = Animation_get_Item_m4198128320(L_18, L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		AnimationState_set_layer_m139053567(L_21, 1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_22 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_23 = __this->get_leftAnim_5();
		NullCheck(L_23);
		String_t* L_24 = Object_get_name_m2079638459(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		AnimationState_t1303741697 * L_25 = Animation_get_Item_m4198128320(L_22, L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		AnimationState_set_enabled_m2079619927(L_25, (bool)1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_26 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_27 = __this->get_rightAnim_6();
		NullCheck(L_27);
		String_t* L_28 = Object_get_name_m2079638459(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		AnimationState_t1303741697 * L_29 = Animation_get_Item_m4198128320(L_26, L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		AnimationState_set_layer_m139053567(L_29, 1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_30 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_31 = __this->get_rightAnim_6();
		NullCheck(L_31);
		String_t* L_32 = Object_get_name_m2079638459(L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		AnimationState_t1303741697 * L_33 = Animation_get_Item_m4198128320(L_30, L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		AnimationState_set_enabled_m2079619927(L_33, (bool)1, /*hidden argument*/NULL);
		Animation_t2068071072 * L_34 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		NullCheck(L_34);
		Animation_SyncLayer_m3054866342(L_34, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpiderAnimationTest::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var;
extern const uint32_t SpiderAnimationTest_Update_m731600834_MetadataUsageId;
extern "C"  void SpiderAnimationTest_Update_m731600834 (SpiderAnimationTest_t3642224283 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpiderAnimationTest_Update_m731600834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		Rigidbody_t4233889191 * L_0 = __this->get_rigid_2();
		float L_1 = __this->get_angle_8();
		Quaternion_t4030073918  L_2 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (((float)((float)0))), L_1, (((float)((float)0))), /*hidden argument*/NULL);
		Rigidbody_t4233889191 * L_3 = __this->get_rigid_2();
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_forward_m1833488937(L_4, /*hidden argument*/NULL);
		Vector3_t2243707580  L_6 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_6, (2.4f), /*hidden argument*/NULL);
		float L_8 = __this->get_walking_7();
		Vector3_t2243707580  L_9 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rigidbody_set_velocity_m2514070071(L_0, L_9, /*hidden argument*/NULL);
		Rigidbody_t4233889191 * L_10 = __this->get_rigid_2();
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Rigidbody_get_velocity_m2022666970(L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		(&V_0)->set_y_2((((float)((float)0))));
		float L_12 = Vector3_get_magnitude_m860342598((&V_0), /*hidden argument*/NULL);
		V_1 = ((float)((float)L_12/(float)(2.4f)));
		Animation_t2068071072 * L_13 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_14 = __this->get_forwardAnim_3();
		NullCheck(L_14);
		String_t* L_15 = Object_get_name_m2079638459(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		AnimationState_t1303741697 * L_16 = Animation_get_Item_m4198128320(L_13, L_15, /*hidden argument*/NULL);
		float L_17 = V_1;
		NullCheck(L_16);
		AnimationState_set_speed_m465014523(L_16, L_17, /*hidden argument*/NULL);
		Animation_t2068071072 * L_18 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_19 = __this->get_rightAnim_6();
		NullCheck(L_19);
		String_t* L_20 = Object_get_name_m2079638459(L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		AnimationState_t1303741697 * L_21 = Animation_get_Item_m4198128320(L_18, L_20, /*hidden argument*/NULL);
		float L_22 = V_1;
		NullCheck(L_21);
		AnimationState_set_speed_m465014523(L_21, L_22, /*hidden argument*/NULL);
		Animation_t2068071072 * L_23 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_24 = __this->get_backAnim_4();
		NullCheck(L_24);
		String_t* L_25 = Object_get_name_m2079638459(L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		AnimationState_t1303741697 * L_26 = Animation_get_Item_m4198128320(L_23, L_25, /*hidden argument*/NULL);
		float L_27 = V_1;
		NullCheck(L_26);
		AnimationState_set_speed_m465014523(L_26, L_27, /*hidden argument*/NULL);
		Animation_t2068071072 * L_28 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_29 = __this->get_leftAnim_5();
		NullCheck(L_29);
		String_t* L_30 = Object_get_name_m2079638459(L_29, /*hidden argument*/NULL);
		NullCheck(L_28);
		AnimationState_t1303741697 * L_31 = Animation_get_Item_m4198128320(L_28, L_30, /*hidden argument*/NULL);
		float L_32 = V_1;
		NullCheck(L_31);
		AnimationState_set_speed_m465014523(L_31, L_32, /*hidden argument*/NULL);
		Vector3_t2243707580  L_33 = V_0;
		Vector3_t2243707580  L_34 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_35 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_00ec;
		}
	}
	{
		goto IL_0350;
	}

IL_00ec:
	{
		Transform_t3275118058 * L_36 = __this->get_tr_9();
		NullCheck(L_36);
		Vector3_t2243707580  L_37 = Transform_get_forward_m1833488937(L_36, /*hidden argument*/NULL);
		float L_38 = SpiderAnimationTest_HorizontalAngle_m3266739583(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		Rigidbody_t4233889191 * L_39 = __this->get_rigid_2();
		NullCheck(L_39);
		Vector3_t2243707580  L_40 = Rigidbody_get_velocity_m2022666970(L_39, /*hidden argument*/NULL);
		float L_41 = SpiderAnimationTest_HorizontalAngle_m3266739583(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_42 = Mathf_DeltaAngle_m1024687732(NULL /*static, unused*/, L_38, L_41, /*hidden argument*/NULL);
		V_2 = L_42;
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_3));
		float L_43 = V_2;
		if ((((float)L_43) >= ((float)(((float)((float)((int32_t)-90)))))))
		{
			goto IL_01ae;
		}
	}
	{
		float L_44 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_45 = Mathf_InverseLerp_m55890283(NULL /*static, unused*/, (((float)((float)((int32_t)-180)))), (((float)((float)((int32_t)-90)))), L_44, /*hidden argument*/NULL);
		V_3 = L_45;
		Animation_t2068071072 * L_46 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_47 = __this->get_forwardAnim_3();
		NullCheck(L_47);
		String_t* L_48 = Object_get_name_m2079638459(L_47, /*hidden argument*/NULL);
		NullCheck(L_46);
		AnimationState_t1303741697 * L_49 = Animation_get_Item_m4198128320(L_46, L_48, /*hidden argument*/NULL);
		NullCheck(L_49);
		AnimationState_set_weight_m2370306600(L_49, (((float)((float)0))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_50 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_51 = __this->get_rightAnim_6();
		NullCheck(L_51);
		String_t* L_52 = Object_get_name_m2079638459(L_51, /*hidden argument*/NULL);
		NullCheck(L_50);
		AnimationState_t1303741697 * L_53 = Animation_get_Item_m4198128320(L_50, L_52, /*hidden argument*/NULL);
		NullCheck(L_53);
		AnimationState_set_weight_m2370306600(L_53, (((float)((float)0))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_54 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_55 = __this->get_backAnim_4();
		NullCheck(L_55);
		String_t* L_56 = Object_get_name_m2079638459(L_55, /*hidden argument*/NULL);
		NullCheck(L_54);
		AnimationState_t1303741697 * L_57 = Animation_get_Item_m4198128320(L_54, L_56, /*hidden argument*/NULL);
		float L_58 = V_3;
		NullCheck(L_57);
		AnimationState_set_weight_m2370306600(L_57, ((float)((float)(((float)((float)1)))-(float)L_58)), /*hidden argument*/NULL);
		Animation_t2068071072 * L_59 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_60 = __this->get_leftAnim_5();
		NullCheck(L_60);
		String_t* L_61 = Object_get_name_m2079638459(L_60, /*hidden argument*/NULL);
		NullCheck(L_59);
		AnimationState_t1303741697 * L_62 = Animation_get_Item_m4198128320(L_59, L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		AnimationState_set_weight_m2370306600(L_62, (((float)((float)1))), /*hidden argument*/NULL);
		goto IL_0350;
	}

IL_01ae:
	{
		float L_63 = V_2;
		if ((((float)L_63) >= ((float)(((float)((float)0))))))
		{
			goto IL_023c;
		}
	}
	{
		float L_64 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_65 = Mathf_InverseLerp_m55890283(NULL /*static, unused*/, (((float)((float)((int32_t)-90)))), (((float)((float)0))), L_64, /*hidden argument*/NULL);
		V_3 = L_65;
		Animation_t2068071072 * L_66 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_67 = __this->get_forwardAnim_3();
		NullCheck(L_67);
		String_t* L_68 = Object_get_name_m2079638459(L_67, /*hidden argument*/NULL);
		NullCheck(L_66);
		AnimationState_t1303741697 * L_69 = Animation_get_Item_m4198128320(L_66, L_68, /*hidden argument*/NULL);
		float L_70 = V_3;
		NullCheck(L_69);
		AnimationState_set_weight_m2370306600(L_69, L_70, /*hidden argument*/NULL);
		Animation_t2068071072 * L_71 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_72 = __this->get_rightAnim_6();
		NullCheck(L_72);
		String_t* L_73 = Object_get_name_m2079638459(L_72, /*hidden argument*/NULL);
		NullCheck(L_71);
		AnimationState_t1303741697 * L_74 = Animation_get_Item_m4198128320(L_71, L_73, /*hidden argument*/NULL);
		NullCheck(L_74);
		AnimationState_set_weight_m2370306600(L_74, (((float)((float)0))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_75 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_76 = __this->get_backAnim_4();
		NullCheck(L_76);
		String_t* L_77 = Object_get_name_m2079638459(L_76, /*hidden argument*/NULL);
		NullCheck(L_75);
		AnimationState_t1303741697 * L_78 = Animation_get_Item_m4198128320(L_75, L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		AnimationState_set_weight_m2370306600(L_78, (((float)((float)0))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_79 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_80 = __this->get_leftAnim_5();
		NullCheck(L_80);
		String_t* L_81 = Object_get_name_m2079638459(L_80, /*hidden argument*/NULL);
		NullCheck(L_79);
		AnimationState_t1303741697 * L_82 = Animation_get_Item_m4198128320(L_79, L_81, /*hidden argument*/NULL);
		float L_83 = V_3;
		NullCheck(L_82);
		AnimationState_set_weight_m2370306600(L_82, ((float)((float)(((float)((float)1)))-(float)L_83)), /*hidden argument*/NULL);
		goto IL_0350;
	}

IL_023c:
	{
		float L_84 = V_2;
		if ((((float)L_84) >= ((float)(((float)((float)((int32_t)90)))))))
		{
			goto IL_02cb;
		}
	}
	{
		float L_85 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_86 = Mathf_InverseLerp_m55890283(NULL /*static, unused*/, (((float)((float)0))), (((float)((float)((int32_t)90)))), L_85, /*hidden argument*/NULL);
		V_3 = L_86;
		Animation_t2068071072 * L_87 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_88 = __this->get_forwardAnim_3();
		NullCheck(L_88);
		String_t* L_89 = Object_get_name_m2079638459(L_88, /*hidden argument*/NULL);
		NullCheck(L_87);
		AnimationState_t1303741697 * L_90 = Animation_get_Item_m4198128320(L_87, L_89, /*hidden argument*/NULL);
		float L_91 = V_3;
		NullCheck(L_90);
		AnimationState_set_weight_m2370306600(L_90, ((float)((float)(((float)((float)1)))-(float)L_91)), /*hidden argument*/NULL);
		Animation_t2068071072 * L_92 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_93 = __this->get_rightAnim_6();
		NullCheck(L_93);
		String_t* L_94 = Object_get_name_m2079638459(L_93, /*hidden argument*/NULL);
		NullCheck(L_92);
		AnimationState_t1303741697 * L_95 = Animation_get_Item_m4198128320(L_92, L_94, /*hidden argument*/NULL);
		float L_96 = V_3;
		NullCheck(L_95);
		AnimationState_set_weight_m2370306600(L_95, L_96, /*hidden argument*/NULL);
		Animation_t2068071072 * L_97 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_98 = __this->get_backAnim_4();
		NullCheck(L_98);
		String_t* L_99 = Object_get_name_m2079638459(L_98, /*hidden argument*/NULL);
		NullCheck(L_97);
		AnimationState_t1303741697 * L_100 = Animation_get_Item_m4198128320(L_97, L_99, /*hidden argument*/NULL);
		NullCheck(L_100);
		AnimationState_set_weight_m2370306600(L_100, (((float)((float)0))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_101 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_102 = __this->get_leftAnim_5();
		NullCheck(L_102);
		String_t* L_103 = Object_get_name_m2079638459(L_102, /*hidden argument*/NULL);
		NullCheck(L_101);
		AnimationState_t1303741697 * L_104 = Animation_get_Item_m4198128320(L_101, L_103, /*hidden argument*/NULL);
		NullCheck(L_104);
		AnimationState_set_weight_m2370306600(L_104, (((float)((float)0))), /*hidden argument*/NULL);
		goto IL_0350;
	}

IL_02cb:
	{
		float L_105 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_106 = Mathf_InverseLerp_m55890283(NULL /*static, unused*/, (((float)((float)((int32_t)90)))), (((float)((float)((int32_t)180)))), L_105, /*hidden argument*/NULL);
		V_3 = L_106;
		Animation_t2068071072 * L_107 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_108 = __this->get_forwardAnim_3();
		NullCheck(L_108);
		String_t* L_109 = Object_get_name_m2079638459(L_108, /*hidden argument*/NULL);
		NullCheck(L_107);
		AnimationState_t1303741697 * L_110 = Animation_get_Item_m4198128320(L_107, L_109, /*hidden argument*/NULL);
		NullCheck(L_110);
		AnimationState_set_weight_m2370306600(L_110, (((float)((float)0))), /*hidden argument*/NULL);
		Animation_t2068071072 * L_111 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_112 = __this->get_rightAnim_6();
		NullCheck(L_112);
		String_t* L_113 = Object_get_name_m2079638459(L_112, /*hidden argument*/NULL);
		NullCheck(L_111);
		AnimationState_t1303741697 * L_114 = Animation_get_Item_m4198128320(L_111, L_113, /*hidden argument*/NULL);
		float L_115 = V_3;
		NullCheck(L_114);
		AnimationState_set_weight_m2370306600(L_114, ((float)((float)(((float)((float)1)))-(float)L_115)), /*hidden argument*/NULL);
		Animation_t2068071072 * L_116 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_117 = __this->get_backAnim_4();
		NullCheck(L_117);
		String_t* L_118 = Object_get_name_m2079638459(L_117, /*hidden argument*/NULL);
		NullCheck(L_116);
		AnimationState_t1303741697 * L_119 = Animation_get_Item_m4198128320(L_116, L_118, /*hidden argument*/NULL);
		float L_120 = V_3;
		NullCheck(L_119);
		AnimationState_set_weight_m2370306600(L_119, L_120, /*hidden argument*/NULL);
		Animation_t2068071072 * L_121 = Component_GetComponent_TisAnimation_t2068071072_m911931835(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t2068071072_m911931835_MethodInfo_var);
		AnimationClip_t3510324950 * L_122 = __this->get_leftAnim_5();
		NullCheck(L_122);
		String_t* L_123 = Object_get_name_m2079638459(L_122, /*hidden argument*/NULL);
		NullCheck(L_121);
		AnimationState_t1303741697 * L_124 = Animation_get_Item_m4198128320(L_121, L_123, /*hidden argument*/NULL);
		NullCheck(L_124);
		AnimationState_set_weight_m2370306600(L_124, (((float)((float)0))), /*hidden argument*/NULL);
	}

IL_0350:
	{
		return;
	}
}
// System.Single SpiderAnimationTest::HorizontalAngle(UnityEngine.Vector3)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t SpiderAnimationTest_HorizontalAngle_m3266739583_MetadataUsageId;
extern "C"  float SpiderAnimationTest_HorizontalAngle_m3266739583 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___direction0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpiderAnimationTest_HorizontalAngle_m3266739583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = (&___direction0)->get_x_1();
		float L_1 = (&___direction0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = atan2f(L_0, L_1);
		return ((float)((float)L_2*(float)(57.29578f)));
	}
}
// System.Void SpiderAnimationTest::OnGUI()
extern Il2CppClass* RuntimeServices_t1910041954_il2cpp_TypeInfo_var;
extern Il2CppClass* GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3887817700;
extern Il2CppCodeGenString* _stringLiteral2562712940;
extern Il2CppCodeGenString* _stringLiteral3501618936;
extern const uint32_t SpiderAnimationTest_OnGUI_m2668444317_MetadataUsageId;
extern "C"  void SpiderAnimationTest_OnGUI_m2668444317 (SpiderAnimationTest_t3642224283 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpiderAnimationTest_OnGUI_m2668444317_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		float* L_0 = __this->get_address_of_angle_8();
		String_t* L_1 = Single_ToString_m2359963436(L_0, _stringLiteral2562712940, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		String_t* L_2 = RuntimeServices_op_Addition_m1630013314(NULL /*static, unused*/, _stringLiteral3887817700, L_1, /*hidden argument*/NULL);
		GUILayout_Label_m3466110979(NULL /*static, unused*/, L_2, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		float L_3 = __this->get_angle_8();
		GUILayoutOptionU5BU5D_t2108882777* L_4 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)1));
		GUILayoutOption_t4183744904 * L_5 = GUILayout_Width_m261136689(NULL /*static, unused*/, (((float)((float)((int32_t)200)))), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_5);
		float L_6 = GUILayout_HorizontalSlider_m2178689167(NULL /*static, unused*/, L_3, (((float)((float)0))), (((float)((float)((int32_t)360)))), L_4, /*hidden argument*/NULL);
		__this->set_angle_8(L_6);
		V_0 = 0;
		goto IL_007c;
	}

IL_0059:
	{
		float L_7 = __this->get_angle_8();
		int32_t L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_9 = fabsf(((float)((float)L_7-(float)(((float)((float)L_8))))));
		if ((((float)L_9) >= ((float)(((float)((float)((int32_t)10)))))))
		{
			goto IL_0077;
		}
	}
	{
		int32_t L_10 = V_0;
		__this->set_angle_8((((float)((float)L_10))));
	}

IL_0077:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)((int32_t)45)));
	}

IL_007c:
	{
		int32_t L_12 = V_0;
		if ((((int32_t)L_12) <= ((int32_t)((int32_t)360))))
		{
			goto IL_0059;
		}
	}
	{
		float* L_13 = __this->get_address_of_walking_7();
		String_t* L_14 = Single_ToString_m2359963436(L_13, _stringLiteral2562712940, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		String_t* L_15 = RuntimeServices_op_Addition_m1630013314(NULL /*static, unused*/, _stringLiteral3501618936, L_14, /*hidden argument*/NULL);
		GUILayout_Label_m3466110979(NULL /*static, unused*/, L_15, ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		float L_16 = __this->get_walking_7();
		GUILayoutOptionU5BU5D_t2108882777* L_17 = ((GUILayoutOptionU5BU5D_t2108882777*)SZArrayNew(GUILayoutOptionU5BU5D_t2108882777_il2cpp_TypeInfo_var, (uint32_t)1));
		GUILayoutOption_t4183744904 * L_18 = GUILayout_Width_m261136689(NULL /*static, unused*/, (((float)((float)((int32_t)100)))), /*hidden argument*/NULL);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t4183744904 *)L_18);
		float L_19 = GUILayout_HorizontalSlider_m2178689167(NULL /*static, unused*/, L_16, (((float)((float)0))), (((float)((float)1))), L_17, /*hidden argument*/NULL);
		__this->set_walking_7(L_19);
		return;
	}
}
// System.Void SpiderAnimationTest::Main()
extern "C"  void SpiderAnimationTest_Main_m3355268778 (SpiderAnimationTest_t3642224283 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SpiderAttackMoveController::.ctor()
extern "C"  void SpiderAttackMoveController__ctor_m3546380210 (SpiderAttackMoveController_t1979379996 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		__this->set_targetDistanceMin_3((2.0f));
		__this->set_targetDistanceMax_4((3.0f));
		__this->set_proximityDistance_5((4.0f));
		__this->set_damageRadius_6((5.0f));
		__this->set_proximityBuildupTime_7((2.0f));
		__this->set_proximityOfNoReturn_8((0.6f));
		__this->set_damageAmount_9((30.0f));
		return;
	}
}
// System.Void SpiderAttackMoveController::Awake()
extern const MethodInfo* Component_GetComponentInChildren_TisAI_t2476083018_m1309876514_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisSelfIlluminationBlink_t1392230251_m1406606728_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1875862075;
extern const uint32_t SpiderAttackMoveController_Awake_m1099765163_MetadataUsageId;
extern "C"  void SpiderAttackMoveController_Awake_m1099765163 (SpiderAttackMoveController_t1979379996 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpiderAttackMoveController_Awake_m1099765163_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MovementMotor_t1612021398 * L_0 = __this->get_motor_2();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		__this->set_character_17(L_1);
		GameObject_t1756533147 * L_2 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139(L_2, /*hidden argument*/NULL);
		__this->set_player_18(L_3);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Transform_get_parent_m147407266(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		AI_t2476083018 * L_6 = Component_GetComponentInChildren_TisAI_t2476083018_m1309876514(L_5, /*hidden argument*/Component_GetComponentInChildren_TisAI_t2476083018_m1309876514_MethodInfo_var);
		__this->set_ai_16(L_6);
		SelfIlluminationBlinkU5BU5D_t4009522826* L_7 = __this->get_blinkComponents_12();
		NullCheck((Il2CppArray *)(Il2CppArray *)L_7);
		int32_t L_8 = Array_get_Length_m1498215565((Il2CppArray *)(Il2CppArray *)L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0062;
		}
	}
	{
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_t3275118058 * L_10 = Transform_get_parent_m147407266(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		SelfIlluminationBlinkU5BU5D_t4009522826* L_11 = Component_GetComponentsInChildren_TisSelfIlluminationBlink_t1392230251_m1406606728(L_10, /*hidden argument*/Component_GetComponentsInChildren_TisSelfIlluminationBlink_t1392230251_m1406606728_MethodInfo_var);
		__this->set_blinkComponents_12(L_11);
	}

IL_0062:
	{
		return;
	}
}
// System.Void SpiderAttackMoveController::OnEnable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var;
extern const uint32_t SpiderAttackMoveController_OnEnable_m1357110374_MetadataUsageId;
extern "C"  void SpiderAttackMoveController_OnEnable_m1357110374 (SpiderAttackMoveController_t1979379996 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpiderAttackMoveController_OnEnable_m1357110374_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_inRange_19((bool)0);
		float L_0 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_nextRaycastTime_20(L_0);
		float L_1 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastRaycastSuccessfulTime_21(L_1);
		float L_2 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_noticeTime_24(L_2);
		MonoBehaviour_t1158329972 * L_3 = __this->get_animationBehaviour_15();
		NullCheck(L_3);
		Behaviour_set_enabled_m1796096907(L_3, (bool)1, /*hidden argument*/NULL);
		GlowPlane_t2106059055 * L_4 = __this->get_blinkPlane_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0055;
		}
	}
	{
		GlowPlane_t2106059055 * L_6 = __this->get_blinkPlane_13();
		NullCheck(L_6);
		Renderer_t257310565 * L_7 = Component_GetComponent_TisRenderer_t257310565_m692268576(L_6, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var);
		NullCheck(L_7);
		Renderer_set_enabled_m142717579(L_7, (bool)0, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void SpiderAttackMoveController::OnDisable()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2852667257;
extern Il2CppCodeGenString* _stringLiteral3971956130;
extern const uint32_t SpiderAttackMoveController_OnDisable_m3921807863_MetadataUsageId;
extern "C"  void SpiderAttackMoveController_OnDisable_m3921807863 (SpiderAttackMoveController_t1979379996 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpiderAttackMoveController_OnDisable_m3921807863_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Renderer_t257310565 * L_0 = __this->get_proximityRenderer_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m865553560(NULL /*static, unused*/, _stringLiteral2852667257, __this, /*hidden argument*/NULL);
		goto IL_005c;
	}

IL_0021:
	{
		Renderer_t257310565 * L_2 = __this->get_proximityRenderer_10();
		NullCheck(L_2);
		Material_t193706927 * L_3 = Renderer_get_material_m2553789785(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0047;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m865553560(NULL /*static, unused*/, _stringLiteral3971956130, __this, /*hidden argument*/NULL);
		goto IL_005c;
	}

IL_0047:
	{
		Renderer_t257310565 * L_5 = __this->get_proximityRenderer_10();
		NullCheck(L_5);
		Material_t193706927 * L_6 = Renderer_get_material_m2553789785(L_5, /*hidden argument*/NULL);
		Color_t2020392075  L_7 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		Material_set_color_m577844242(L_6, L_7, /*hidden argument*/NULL);
	}

IL_005c:
	{
		GlowPlane_t2106059055 * L_8 = __this->get_blinkPlane_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007d;
		}
	}
	{
		GlowPlane_t2106059055 * L_10 = __this->get_blinkPlane_13();
		NullCheck(L_10);
		Renderer_t257310565 * L_11 = Component_GetComponent_TisRenderer_t257310565_m692268576(L_10, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var);
		NullCheck(L_11);
		Renderer_set_enabled_m142717579(L_11, (bool)0, /*hidden argument*/NULL);
	}

IL_007d:
	{
		return;
	}
}
// System.Void SpiderAttackMoveController::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var;
extern const uint32_t SpiderAttackMoveController_Update_m2867909263_MetadataUsageId;
extern "C"  void SpiderAttackMoveController_Update_m2867909263 (SpiderAttackMoveController_t1979379996 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpiderAttackMoveController_Update_m2867909263_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	SelfIlluminationBlink_t1392230251 * V_3 = NULL;
	int32_t V_4 = 0;
	SelfIlluminationBlinkU5BU5D_t4009522826* V_5 = NULL;
	int32_t V_6 = 0;
	{
		float L_0 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_noticeTime_24();
		if ((((float)L_0) >= ((float)((float)((float)L_1+(float)(0.7f))))))
		{
			goto IL_002b;
		}
	}
	{
		MovementMotor_t1612021398 * L_2 = __this->get_motor_2();
		Vector3_t2243707580  L_3 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_movementDirection_2(L_3);
		goto IL_02a0;
	}

IL_002b:
	{
		Transform_t3275118058 * L_4 = __this->get_player_18();
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		Transform_t3275118058 * L_6 = __this->get_character_17();
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		(&V_0)->set_y_2((((float)((float)0))));
		float L_9 = Vector3_get_magnitude_m860342598((&V_0), /*hidden argument*/NULL);
		V_1 = L_9;
		Vector3_t2243707580  L_10 = V_0;
		float L_11 = V_1;
		Vector3_t2243707580  L_12 = Vector3_op_Division_m3315615850(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		bool L_13 = __this->get_inRange_19();
		if (!L_13)
		{
			goto IL_007e;
		}
	}
	{
		float L_14 = V_1;
		float L_15 = __this->get_targetDistanceMax_4();
		if ((((float)L_14) <= ((float)L_15)))
		{
			goto IL_007e;
		}
	}
	{
		__this->set_inRange_19((bool)0);
	}

IL_007e:
	{
		bool L_16 = __this->get_inRange_19();
		if (L_16)
		{
			goto IL_009c;
		}
	}
	{
		float L_17 = V_1;
		float L_18 = __this->get_targetDistanceMin_3();
		if ((((float)L_17) >= ((float)L_18)))
		{
			goto IL_009c;
		}
	}
	{
		__this->set_inRange_19((bool)1);
	}

IL_009c:
	{
		bool L_19 = __this->get_inRange_19();
		if (!L_19)
		{
			goto IL_00bc;
		}
	}
	{
		MovementMotor_t1612021398 * L_20 = __this->get_motor_2();
		Vector3_t2243707580  L_21 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_20);
		L_20->set_movementDirection_2(L_21);
		goto IL_00c8;
	}

IL_00bc:
	{
		MovementMotor_t1612021398 * L_22 = __this->get_motor_2();
		Vector3_t2243707580  L_23 = V_0;
		NullCheck(L_22);
		L_22->set_movementDirection_2(L_23);
	}

IL_00c8:
	{
		float L_24 = V_1;
		float L_25 = __this->get_proximityDistance_5();
		if ((((float)L_24) >= ((float)L_25)))
		{
			goto IL_00e7;
		}
	}
	{
		float L_26 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_27 = __this->get_lastRaycastSuccessfulTime_21();
		if ((((float)L_26) < ((float)((float)((float)L_27+(float)(((float)((float)1))))))))
		{
			goto IL_00f8;
		}
	}

IL_00e7:
	{
		float L_28 = __this->get_proximityLevel_22();
		float L_29 = __this->get_proximityOfNoReturn_8();
		if ((((float)L_28) <= ((float)L_29)))
		{
			goto IL_0116;
		}
	}

IL_00f8:
	{
		float L_30 = __this->get_proximityLevel_22();
		float L_31 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_32 = __this->get_proximityBuildupTime_7();
		__this->set_proximityLevel_22(((float)((float)L_30+(float)((float)((float)L_31/(float)L_32)))));
		goto IL_012f;
	}

IL_0116:
	{
		float L_33 = __this->get_proximityLevel_22();
		float L_34 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_35 = __this->get_proximityBuildupTime_7();
		__this->set_proximityLevel_22(((float)((float)L_33-(float)((float)((float)L_34/(float)L_35)))));
	}

IL_012f:
	{
		float L_36 = __this->get_proximityLevel_22();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_37 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		__this->set_proximityLevel_22(L_37);
		float L_38 = __this->get_proximityLevel_22();
		if ((!(((float)L_38) == ((float)(((float)((float)1)))))))
		{
			goto IL_0153;
		}
	}
	{
		VirtActionInvoker0::Invoke(8 /* System.Void SpiderAttackMoveController::Explode() */, __this);
	}

IL_0153:
	{
		float L_39 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_40 = __this->get_nextRaycastTime_20();
		if ((((float)L_39) <= ((float)L_40)))
		{
			goto IL_01af;
		}
	}
	{
		float L_41 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_nextRaycastTime_20(((float)((float)L_41+(float)(((float)((float)1))))));
		AI_t2476083018 * L_42 = __this->get_ai_16();
		NullCheck(L_42);
		bool L_43 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean AI::CanSeePlayer() */, L_42);
		if (!L_43)
		{
			goto IL_0191;
		}
	}
	{
		float L_44 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastRaycastSuccessfulTime_21(L_44);
		goto IL_01af;
	}

IL_0191:
	{
		float L_45 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_46 = __this->get_lastRaycastSuccessfulTime_21();
		if ((((float)L_45) <= ((float)((float)((float)L_46+(float)(((float)((float)2))))))))
		{
			goto IL_01af;
		}
	}
	{
		AI_t2476083018 * L_47 = __this->get_ai_16();
		NullCheck(L_47);
		VirtActionInvoker0::Invoke(10 /* System.Void AI::OnLostTrack() */, L_47);
	}

IL_01af:
	{
		float L_48 = __this->get_proximityLevel_22();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_49 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, (((float)((float)2))), (((float)((float)((int32_t)15)))), L_48, /*hidden argument*/NULL);
		V_2 = ((float)((float)(((float)((float)1)))/(float)L_49));
		float L_50 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_51 = __this->get_lastBlinkTime_23();
		float L_52 = V_2;
		if ((((float)L_50) <= ((float)((float)((float)L_51+(float)L_52)))))
		{
			goto IL_0275;
		}
	}
	{
		float L_53 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_lastBlinkTime_23(L_53);
		Renderer_t257310565 * L_54 = __this->get_proximityRenderer_10();
		NullCheck(L_54);
		Material_t193706927 * L_55 = Renderer_get_material_m2553789785(L_54, /*hidden argument*/NULL);
		Color_t2020392075  L_56 = Color_get_red_m2410286591(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_55);
		Material_set_color_m577844242(L_55, L_56, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_57 = __this->get_audioSource_11();
		NullCheck(L_57);
		bool L_58 = Behaviour_get_enabled_m4079055610(L_57, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_0210;
		}
	}
	{
		AudioSource_t1135106623 * L_59 = __this->get_audioSource_11();
		NullCheck(L_59);
		AudioSource_Play_m353744792(L_59, /*hidden argument*/NULL);
	}

IL_0210:
	{
		V_4 = 0;
		SelfIlluminationBlinkU5BU5D_t4009522826* L_60 = __this->get_blinkComponents_12();
		V_5 = L_60;
		SelfIlluminationBlinkU5BU5D_t4009522826* L_61 = V_5;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_61);
		int32_t L_62 = Array_get_Length_m1498215565((Il2CppArray *)(Il2CppArray *)L_61, /*hidden argument*/NULL);
		V_6 = L_62;
		goto IL_0239;
	}

IL_0229:
	{
		SelfIlluminationBlinkU5BU5D_t4009522826* L_63 = V_5;
		int32_t L_64 = V_4;
		NullCheck(L_63);
		int32_t L_65 = L_64;
		SelfIlluminationBlink_t1392230251 * L_66 = (L_63)->GetAt(static_cast<il2cpp_array_size_t>(L_65));
		NullCheck(L_66);
		VirtActionInvoker0::Invoke(5 /* System.Void SelfIlluminationBlink::Blink() */, L_66);
		int32_t L_67 = V_4;
		V_4 = ((int32_t)((int32_t)L_67+(int32_t)1));
	}

IL_0239:
	{
		int32_t L_68 = V_4;
		int32_t L_69 = V_6;
		if ((((int32_t)L_68) < ((int32_t)L_69)))
		{
			goto IL_0229;
		}
	}
	{
		GlowPlane_t2106059055 * L_70 = __this->get_blinkPlane_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_71 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_70, /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_0275;
		}
	}
	{
		GlowPlane_t2106059055 * L_72 = __this->get_blinkPlane_13();
		NullCheck(L_72);
		Renderer_t257310565 * L_73 = Component_GetComponent_TisRenderer_t257310565_m692268576(L_72, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var);
		GlowPlane_t2106059055 * L_74 = __this->get_blinkPlane_13();
		NullCheck(L_74);
		Renderer_t257310565 * L_75 = Component_GetComponent_TisRenderer_t257310565_m692268576(L_74, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m692268576_MethodInfo_var);
		NullCheck(L_75);
		bool L_76 = Renderer_get_enabled_m2362836534(L_75, /*hidden argument*/NULL);
		NullCheck(L_73);
		Renderer_set_enabled_m142717579(L_73, (bool)((((int32_t)L_76) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_0275:
	{
		float L_77 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_78 = __this->get_lastBlinkTime_23();
		if ((((float)L_77) <= ((float)((float)((float)L_78+(float)(0.04f))))))
		{
			goto IL_02a0;
		}
	}
	{
		Renderer_t257310565 * L_79 = __this->get_proximityRenderer_10();
		NullCheck(L_79);
		Material_t193706927 * L_80 = Renderer_get_material_m2553789785(L_79, /*hidden argument*/NULL);
		Color_t2020392075  L_81 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_80);
		Material_set_color_m577844242(L_80, L_81, /*hidden argument*/NULL);
	}

IL_02a0:
	{
		return;
	}
}
// System.Void SpiderAttackMoveController::Explode()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisHealth_t2683907638_m697466234_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody_t4233889191_m1908970916_MethodInfo_var;
extern const uint32_t SpiderAttackMoveController_Explode_m436922129_MetadataUsageId;
extern "C"  void SpiderAttackMoveController_Explode_m436922129 (SpiderAttackMoveController_t1979379996 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpiderAttackMoveController_Explode_m436922129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Health_t2683907638 * V_1 = NULL;
	{
		Transform_t3275118058 * L_0 = __this->get_player_18();
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = __this->get_character_17();
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		float L_4 = Vector3_Distance_m1859670022(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		float L_5 = __this->get_damageRadius_6();
		V_0 = ((float)((float)(((float)((float)1)))-(float)((float)((float)L_4/(float)L_5))));
		Transform_t3275118058 * L_6 = __this->get_player_18();
		NullCheck(L_6);
		Health_t2683907638 * L_7 = Component_GetComponent_TisHealth_t2683907638_m697466234(L_6, /*hidden argument*/Component_GetComponent_TisHealth_t2683907638_m697466234_MethodInfo_var);
		V_1 = L_7;
		Health_t2683907638 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0066;
		}
	}
	{
		Health_t2683907638 * L_10 = V_1;
		float L_11 = __this->get_damageAmount_9();
		float L_12 = V_0;
		Transform_t3275118058 * L_13 = __this->get_character_17();
		NullCheck(L_13);
		Vector3_t2243707580  L_14 = Transform_get_position_m1104419803(L_13, /*hidden argument*/NULL);
		Transform_t3275118058 * L_15 = __this->get_player_18();
		NullCheck(L_15);
		Vector3_t2243707580  L_16 = Transform_get_position_m1104419803(L_15, /*hidden argument*/NULL);
		Vector3_t2243707580  L_17 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker2< float, Vector3_t2243707580  >::Invoke(5 /* System.Void Health::OnDamage(System.Single,UnityEngine.Vector3) */, L_10, ((float)((float)L_11*(float)L_12)), L_17);
	}

IL_0066:
	{
		Transform_t3275118058 * L_18 = __this->get_player_18();
		NullCheck(L_18);
		Rigidbody_t4233889191 * L_19 = Component_GetComponent_TisRigidbody_t4233889191_m1908970916(L_18, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m1908970916_MethodInfo_var);
		Transform_t3275118058 * L_20 = __this->get_character_17();
		NullCheck(L_20);
		Vector3_t2243707580  L_21 = Transform_get_position_m1104419803(L_20, /*hidden argument*/NULL);
		float L_22 = __this->get_damageRadius_6();
		NullCheck(L_19);
		Rigidbody_AddExplosionForce_m1666381818(L_19, (((float)((float)((int32_t)10)))), L_21, L_22, (((float)((float)0))), 1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_23 = __this->get_intentionalExplosion_14();
		Transform_t3275118058 * L_24 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t2243707580  L_25 = Transform_get_position_m1104419803(L_24, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_26 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Spawner_Spawn_m4269243264(NULL /*static, unused*/, L_23, L_25, L_26, /*hidden argument*/NULL);
		Transform_t3275118058 * L_27 = __this->get_character_17();
		NullCheck(L_27);
		GameObject_t1756533147 * L_28 = Component_get_gameObject_m3105766835(L_27, /*hidden argument*/NULL);
		Spawner_Destroy_m590425740(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpiderAttackMoveController::Main()
extern "C"  void SpiderAttackMoveController_Main_m371303609 (SpiderAttackMoveController_t1979379996 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SpiderReturnMoveController::.ctor()
extern "C"  void SpiderReturnMoveController__ctor_m3231045436 (SpiderReturnMoveController_t1990791070 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpiderReturnMoveController::Awake()
extern const MethodInfo* Component_GetComponentInChildren_TisAI_t2476083018_m1309876514_MethodInfo_var;
extern const uint32_t SpiderReturnMoveController_Awake_m1782260405_MetadataUsageId;
extern "C"  void SpiderReturnMoveController_Awake_m1782260405 (SpiderReturnMoveController_t1990791070 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpiderReturnMoveController_Awake_m1782260405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MovementMotor_t1612021398 * L_0 = __this->get_motor_2();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		__this->set_character_4(L_1);
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Transform_get_parent_m147407266(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		AI_t2476083018 * L_4 = Component_GetComponentInChildren_TisAI_t2476083018_m1309876514(L_3, /*hidden argument*/Component_GetComponentInChildren_TisAI_t2476083018_m1309876514_MethodInfo_var);
		__this->set_ai_3(L_4);
		Transform_t3275118058 * L_5 = __this->get_character_4();
		NullCheck(L_5);
		Vector3_t2243707580  L_6 = Transform_get_position_m1104419803(L_5, /*hidden argument*/NULL);
		__this->set_spawnPos_5(L_6);
		return;
	}
}
// System.Void SpiderReturnMoveController::Update()
extern const MethodInfo* Component_GetComponent_TisRigidbody_t4233889191_m1908970916_MethodInfo_var;
extern const uint32_t SpiderReturnMoveController_Update_m2898212633_MetadataUsageId;
extern "C"  void SpiderReturnMoveController_Update_m2898212633 (SpiderReturnMoveController_t1990791070 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpiderReturnMoveController_Update_m2898212633_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		MovementMotor_t1612021398 * L_0 = __this->get_motor_2();
		Vector3_t2243707580  L_1 = __this->get_spawnPos_5();
		Transform_t3275118058 * L_2 = __this->get_character_4();
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		L_0->set_movementDirection_2(L_4);
		MovementMotor_t1612021398 * L_5 = __this->get_motor_2();
		NullCheck(L_5);
		Vector3_t2243707580 * L_6 = L_5->get_address_of_movementDirection_2();
		L_6->set_y_2((((float)((float)0))));
		MovementMotor_t1612021398 * L_7 = __this->get_motor_2();
		NullCheck(L_7);
		Vector3_t2243707580 * L_8 = L_7->get_address_of_movementDirection_2();
		float L_9 = Vector3_get_sqrMagnitude_m1814096310(L_8, /*hidden argument*/NULL);
		if ((((float)L_9) <= ((float)(((float)((float)1))))))
		{
			goto IL_0065;
		}
	}
	{
		MovementMotor_t1612021398 * L_10 = __this->get_motor_2();
		MovementMotor_t1612021398 * L_11 = __this->get_motor_2();
		NullCheck(L_11);
		Vector3_t2243707580 * L_12 = L_11->get_address_of_movementDirection_2();
		Vector3_t2243707580  L_13 = Vector3_get_normalized_m936072361(L_12, /*hidden argument*/NULL);
		NullCheck(L_10);
		L_10->set_movementDirection_2(L_13);
	}

IL_0065:
	{
		MovementMotor_t1612021398 * L_14 = __this->get_motor_2();
		NullCheck(L_14);
		Vector3_t2243707580 * L_15 = L_14->get_address_of_movementDirection_2();
		float L_16 = Vector3_get_sqrMagnitude_m1814096310(L_15, /*hidden argument*/NULL);
		if ((((float)L_16) >= ((float)(0.01f))))
		{
			goto IL_0105;
		}
	}
	{
		Transform_t3275118058 * L_17 = __this->get_character_4();
		Vector3_t2243707580 * L_18 = __this->get_address_of_spawnPos_5();
		float L_19 = L_18->get_x_1();
		Transform_t3275118058 * L_20 = __this->get_character_4();
		NullCheck(L_20);
		Vector3_t2243707580  L_21 = Transform_get_position_m1104419803(L_20, /*hidden argument*/NULL);
		V_0 = L_21;
		float L_22 = (&V_0)->get_y_2();
		Vector3_t2243707580 * L_23 = __this->get_address_of_spawnPos_5();
		float L_24 = L_23->get_z_3();
		Vector3_t2243707580  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Vector3__ctor_m2638739322(&L_25, L_19, L_22, L_24, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_position_m2469242620(L_17, L_25, /*hidden argument*/NULL);
		MovementMotor_t1612021398 * L_26 = __this->get_motor_2();
		NullCheck(L_26);
		Rigidbody_t4233889191 * L_27 = Component_GetComponent_TisRigidbody_t4233889191_m1908970916(L_26, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m1908970916_MethodInfo_var);
		Vector3_t2243707580  L_28 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_27);
		Rigidbody_set_velocity_m2514070071(L_27, L_28, /*hidden argument*/NULL);
		MovementMotor_t1612021398 * L_29 = __this->get_motor_2();
		NullCheck(L_29);
		Rigidbody_t4233889191 * L_30 = Component_GetComponent_TisRigidbody_t4233889191_m1908970916(L_29, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m1908970916_MethodInfo_var);
		Vector3_t2243707580  L_31 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_30);
		Rigidbody_set_angularVelocity_m824394045(L_30, L_31, /*hidden argument*/NULL);
		MovementMotor_t1612021398 * L_32 = __this->get_motor_2();
		Vector3_t2243707580  L_33 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_32);
		L_32->set_movementDirection_2(L_33);
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
		MonoBehaviour_t1158329972 * L_34 = __this->get_animationBehaviour_6();
		NullCheck(L_34);
		Behaviour_set_enabled_m1796096907(L_34, (bool)0, /*hidden argument*/NULL);
	}

IL_0105:
	{
		return;
	}
}
// System.Void SpiderReturnMoveController::Main()
extern "C"  void SpiderReturnMoveController_Main_m1180062211 (SpiderReturnMoveController_t1990791070 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void TriggerOnMouseOrJoystick::.ctor()
extern "C"  void TriggerOnMouseOrJoystick__ctor_m4284235485 (TriggerOnMouseOrJoystick_t2646678581 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TriggerOnMouseOrJoystick::Start()
extern const Il2CppType* Joystick_t549888914_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* JoystickU5BU5D_t1912885735_il2cpp_TypeInfo_var;
extern const uint32_t TriggerOnMouseOrJoystick_Start_m3896495933_MetadataUsageId;
extern "C"  void TriggerOnMouseOrJoystick_Start_m3896495933 (TriggerOnMouseOrJoystick_t2646678581 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TriggerOnMouseOrJoystick_Start_m3896495933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Joystick_t549888914_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_1 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_joysticks_5(((JoystickU5BU5D_t1912885735*)IsInst(((JoystickU5BU5D_t1912885735*)Castclass(L_1, JoystickU5BU5D_t1912885735_il2cpp_TypeInfo_var)), JoystickU5BU5D_t1912885735_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void TriggerOnMouseOrJoystick::Update()
extern "C"  void TriggerOnMouseOrJoystick_Update_m1004982786 (TriggerOnMouseOrJoystick_t2646678581 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_state_4();
		if (L_0)
		{
			goto IL_0036;
		}
	}
	{
		JoystickU5BU5D_t1912885735* L_1 = __this->get_joysticks_5();
		NullCheck(L_1);
		int32_t L_2 = 0;
		Joystick_t549888914 * L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		int32_t L_4 = L_3->get_tapCount_10();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		SignalSender_t1204926691 * L_5 = __this->get_mouseDownSignals_2();
		NullCheck(L_5);
		VirtActionInvoker1< MonoBehaviour_t1158329972 * >::Invoke(4 /* System.Void SignalSender::SendSignals(UnityEngine.MonoBehaviour) */, L_5, __this);
		__this->set_state_4((bool)1);
		goto IL_005c;
	}

IL_0036:
	{
		JoystickU5BU5D_t1912885735* L_6 = __this->get_joysticks_5();
		NullCheck(L_6);
		int32_t L_7 = 0;
		Joystick_t549888914 * L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		int32_t L_9 = L_8->get_tapCount_10();
		if ((((int32_t)L_9) > ((int32_t)0)))
		{
			goto IL_005c;
		}
	}
	{
		SignalSender_t1204926691 * L_10 = __this->get_mouseUpSignals_3();
		NullCheck(L_10);
		VirtActionInvoker1< MonoBehaviour_t1158329972 * >::Invoke(4 /* System.Void SignalSender::SendSignals(UnityEngine.MonoBehaviour) */, L_10, __this);
		__this->set_state_4((bool)0);
	}

IL_005c:
	{
		return;
	}
}
// System.Void TriggerOnMouseOrJoystick::Main()
extern "C"  void TriggerOnMouseOrJoystick_Main_m1799984870 (TriggerOnMouseOrJoystick_t2646678581 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
