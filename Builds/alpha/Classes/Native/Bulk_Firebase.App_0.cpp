﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Firebase.AppOptions
struct AppOptions_t1641189195;
// System.Uri
struct Uri_t19570940;
// System.String
struct String_t;
// Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate
struct LogMessageDelegate_t1988210674;
// Firebase.FutureString/SWIG_CompletionDelegate
struct SWIG_CompletionDelegate_t1819656562;
// Firebase.AppUtilPINVOKE/SWIGExceptionHelper
struct SWIGExceptionHelper_t1412130252;
// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate
struct ExceptionDelegate_t2876249339;
// Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate
struct ExceptionArgumentDelegate_t2443153790;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Exception
struct Exception_t1927440687;
// Firebase.AppUtilPINVOKE/SWIGStringHelper
struct SWIGStringHelper_t549488518;
// Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate
struct SWIGStringDelegate_t18001273;
// Firebase.FirebaseApp
struct FirebaseApp_t210707726;
// Firebase.FirebaseApp/CreateDelegate
struct CreateDelegate_t413676709;
// Firebase.FirebaseApp/DestroyDelegate
struct DestroyDelegate_t3635929227;
// Firebase.FirebaseApp/FirebaseHandler
struct FirebaseHandler_t2907300047;
// Firebase.FirebaseApp/FirebaseHandler/MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t323097478;
// System.Type
struct Type_t;
// Firebase.FirebaseException
struct FirebaseException_t2567272216;
// Firebase.FutureBase
struct FutureBase_t2698306134;
// Firebase.FutureString
struct FutureString_t4225986006;
// System.Threading.Tasks.Task`1<System.String>
struct Task_1_t1149249240;
// Firebase.FutureString/Action
struct Action_t3062064994;
// Firebase.FutureString/<GetTask>c__AnonStorey0
struct U3CGetTaskU3Ec__AnonStorey0_t4099292678;
// Firebase.FutureString/MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t507919426;
// Firebase.InitializationException
struct InitializationException_t1438959983;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct Dictionary_2_t2951825130;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct Dictionary_2_t3612054192;
// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t2766455145;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t1197680765;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t4056456767;
// System.Security.Cryptography.X509Certificates.X509ChainStatus[]
struct X509ChainStatusU5BU5D_t830390908;
// Firebase.Internal.InstallRootCerts/<Process>c__AnonStorey0
struct U3CProcessU3Ec__AnonStorey0_t1447134549;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t283079845;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t777637347;
// Firebase.UnitySynchronizationContext
struct UnitySynchronizationContext_t1051733884;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir
struct SynchronizationContextBehavoir_t692674473;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// System.Func`1<System.Collections.IEnumerator>
struct Func_1_t3420419431;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t296893742;
// Firebase.UnitySynchronizationContext/<Send>c__AnonStorey3
struct U3CSendU3Ec__AnonStorey3_t1465756544;
// Firebase.UnitySynchronizationContext/<Send>c__AnonStorey4
struct U3CSendU3Ec__AnonStorey4_t1465756541;
// Firebase.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey1
struct U3CSendCoroutineU3Ec__AnonStorey1_t3854140196;
// Firebase.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey2
struct U3CSendCoroutineU3Ec__AnonStorey2_t3450855669;
// Firebase.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0
struct U3CSignaledCoroutineU3Ec__Iterator0_t3693910080;
// System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>>
struct Queue_1_t279829387;
// Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t1310454857;
// MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t1970456718;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "Firebase_App_U3CModuleU3E3783534214.h"
#include "Firebase_App_U3CModuleU3E3783534214MethodDeclarations.h"
#include "Firebase_App_Firebase_AppOptions1641189195.h"
#include "Firebase_App_Firebase_AppOptions1641189195MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef2419939847.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef2419939847MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Threading_Monitor3228523394MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609MethodDeclarations.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE3515465473MethodDeclarations.h"
#include "mscorlib_System_GC2902933594MethodDeclarations.h"
#include "System_System_Uri19570940.h"
#include "Firebase_App_Firebase_FirebaseApp210707726MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGPendingEx4193433529MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687.h"
#include "Firebase_App_Firebase_AppUtil2859790353.h"
#include "Firebase_App_Firebase_AppUtil2859790353MethodDeclarations.h"
#include "Firebase_App_Firebase_LogLevel543421840.h"
#include "mscorlib_System_Int322071877448.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler_1988210674.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE3515465473.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGException1412130252MethodDeclarations.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGStringHelp549488518MethodDeclarations.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGException1412130252.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGStringHelp549488518.h"
#include "Firebase_App_Firebase_FutureString_SWIG_Completion1819656562.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGException2876249339MethodDeclarations.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGException2443153790MethodDeclarations.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGException2876249339.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGException2443153790.h"
#include "mscorlib_System_ApplicationException474868623MethodDeclarations.h"
#include "mscorlib_System_ApplicationException474868623.h"
#include "mscorlib_System_ArithmeticException3261462543MethodDeclarations.h"
#include "mscorlib_System_ArithmeticException3261462543.h"
#include "mscorlib_System_DivideByZeroException1660837001MethodDeclarations.h"
#include "mscorlib_System_DivideByZeroException1660837001.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107.h"
#include "mscorlib_System_InvalidCastException3625212209MethodDeclarations.h"
#include "mscorlib_System_InvalidCastException3625212209.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_IO_IOException2458421087MethodDeclarations.h"
#include "mscorlib_System_IO_IOException2458421087.h"
#include "mscorlib_System_NullReferenceException3156209119MethodDeclarations.h"
#include "mscorlib_System_NullReferenceException3156209119.h"
#include "mscorlib_System_OutOfMemoryException1181064283MethodDeclarations.h"
#include "mscorlib_System_OutOfMemoryException1181064283.h"
#include "mscorlib_System_OverflowException1075868493MethodDeclarations.h"
#include "mscorlib_System_OverflowException1075868493.h"
#include "mscorlib_System_SystemException3877406272MethodDeclarations.h"
#include "mscorlib_System_SystemException3877406272.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGPendingEx4193433529.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGStringHelpe18001273MethodDeclarations.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGStringHelpe18001273.h"
#include "Firebase_App_Firebase_FirebaseApp210707726.h"
#include "Firebase_App_Firebase_FirebaseApp_DestroyDelegate3635929227MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge652733044MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2125486988MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2125486988.h"
#include "Firebase_App_Firebase_FirebaseApp_DestroyDelegate3635929227.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge652733044.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler2907300047MethodDeclarations.h"
#include "Firebase_App_Firebase_FirebaseApp_CreateDelegate413676709MethodDeclarations.h"
#include "Firebase_App_Firebase_FirebaseApp_CreateDelegate413676709.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2513902766MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2513902766.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3874796154MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3874796154.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3409525828.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va828546831.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3409525828MethodDeclarations.h"
#include "System_System_Uri19570940MethodDeclarations.h"
#include "System_System_UriFormatException3682083048.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "Firebase_App_Firebase_InitializationException1438959983MethodDeclarations.h"
#include "Firebase_App_Firebase_Internal_InstallRootCerts1106242208MethodDeclarations.h"
#include "Firebase_App_Firebase_InitializationException1438959983.h"
#include "Firebase_App_Firebase_InitResult105293995.h"
#include "System_System_Security_Cryptography_X509Certificat1197680765.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler2907300047.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler_1988210674MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext1051733884MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler_M323097478.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler_M323097478MethodDeclarations.h"
#include "mscorlib_System_Attribute542643598MethodDeclarations.h"
#include "Firebase_App_Firebase_FirebaseException2567272216.h"
#include "Firebase_App_Firebase_FirebaseException2567272216MethodDeclarations.h"
#include "Firebase_App_Firebase_FutureBase2698306134.h"
#include "Firebase_App_Firebase_FutureBase2698306134MethodDeclarations.h"
#include "Firebase_App_Firebase_FutureStatus4011176069.h"
#include "Firebase_App_Firebase_FutureStatus4011176069MethodDeclarations.h"
#include "Firebase_App_Firebase_FutureString4225986006.h"
#include "Firebase_App_Firebase_FutureString4225986006MethodDeclarations.h"
#include "Unity_Tasks_System_Threading_Tasks_Task_1_gen1149249240.h"
#include "Firebase_App_Firebase_FutureString_U3CGetTaskU3Ec_4099292678MethodDeclarations.h"
#include "Unity_Tasks_System_Threading_Tasks_TaskCompletionSo911654313MethodDeclarations.h"
#include "Firebase_App_Firebase_FutureString_Action3062064994MethodDeclarations.h"
#include "Firebase_App_Firebase_FutureString_U3CGetTaskU3Ec_4099292678.h"
#include "Unity_Tasks_System_Threading_Tasks_TaskCompletionSo911654313.h"
#include "Firebase_App_Firebase_FutureString_Action3062064994.h"
#include "Firebase_App_Firebase_FutureString_SWIG_Completion1819656562MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2069890629MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2069890629.h"
#include "Firebase_App_Firebase_FutureString_MonoPInvokeCallb507919426.h"
#include "Firebase_App_Firebase_FutureString_MonoPInvokeCallb507919426MethodDeclarations.h"
#include "Firebase_App_Firebase_InitResult105293995MethodDeclarations.h"
#include "Firebase_App_Firebase_Internal_FirebaseConfigExtens617662701.h"
#include "Firebase_App_Firebase_Internal_FirebaseConfigExtens617662701MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2951825130.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2951825130MethodDeclarations.h"
#include "Firebase_App_Firebase_Internal_FirebaseEditorExten1010167188.h"
#include "Firebase_App_Firebase_Internal_FirebaseEditorExten1010167188MethodDeclarations.h"
#include "Firebase_App_Firebase_Internal_InstallRootCerts1106242208.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2766455145.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2766455145MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Regex1803876613MethodDeclarations.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte3683104436.h"
#include "System_System_Security_Cryptography_X509Certificat1197680765MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificat4056456767MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2301184819.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2301184819MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificat4056456767.h"
#include "mscorlib_System_Security_Cryptography_X509Certifica283079845.h"
#include "mscorlib_System_IO_StreamReader2360341767MethodDeclarations.h"
#include "mscorlib_System_IO_StreamReader2360341767.h"
#include "mscorlib_System_Reflection_Assembly4268412390.h"
#include "mscorlib_System_Reflection_Assembly4268412390MethodDeclarations.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_IO_TextReader1561828458MethodDeclarations.h"
#include "mscorlib_System_IO_TextReader1561828458.h"
#include "UnityEngine_UnityEngine_Resources339470017MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAsset3973159845MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAsset3973159845.h"
#include "mscorlib_System_IO_Directory3318511961MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_MD51507972490MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_HashAlgorith2624936259MethodDeclarations.h"
#include "mscorlib_System_BitConverter3195628829MethodDeclarations.h"
#include "mscorlib_System_IO_Path41728875MethodDeclarations.h"
#include "mscorlib_System_IO_File1930543328MethodDeclarations.h"
#include "mscorlib_System_IO_FileStream1695958676MethodDeclarations.h"
#include "mscorlib_System_IO_BinaryWriter3179371318MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_MD51507972490.h"
#include "mscorlib_System_IO_BinaryWriter3179371318.h"
#include "mscorlib_System_IO_DirectoryInfo1934446453.h"
#include "mscorlib_System_IO_FileStream1695958676.h"
#include "mscorlib_System_IO_FileMode236403845.h"
#include "System_ArrayTypes.h"
#include "System_System_Security_Cryptography_X509Certificat4278378721.h"
#include "mscorlib_System_Security_Cryptography_X509Certifica283079845MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificate650873211MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificat1038124237MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificat1038124237.h"
#include "mscorlib_System_DateTime693205669.h"
#include "System_System_Security_Cryptography_X509Certificate650873211.h"
#include "System_System_Security_Cryptography_X509Certificat1320896183.h"
#include "System_System_Security_Cryptography_X509Certificat2461349531.h"
#include "System_System_Security_Cryptography_X509Certificat4278378721MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificate480677120.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1664293826MethodDeclarations.h"
#include "mscorlib_System_Environment3662374671MethodDeclarations.h"
#include "mscorlib_System_Collections_CollectionBase1101587467MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificat1617430119MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificat1208230922MethodDeclarations.h"
#include "System_System_Net_ServicePointManager745663000MethodDeclarations.h"
#include "Firebase_App_Firebase_Internal_InstallRootCerts_U31447134549MethodDeclarations.h"
#include "System_System_Net_Security_RemoteCertificateValida2756269959MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificat1617430119.h"
#include "System_System_Security_Cryptography_X509Certificat1208230922.h"
#include "Firebase_App_Firebase_Internal_InstallRootCerts_U31447134549.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1664293826.h"
#include "mscorlib_System_Environment_SpecialFolder1519540278.h"
#include "mscorlib_System_Collections_CollectionBase1101587467.h"
#include "System_System_Security_Cryptography_X509Certificat2370524385.h"
#include "System_System_Security_Cryptography_X509Certificat1108969367.h"
#include "System_System_Net_Security_RemoteCertificateValida2756269959.h"
#include "System_System_Security_Cryptography_X509Certificate777637347.h"
#include "System_System_Net_Security_SslPolicyErrors1928581431.h"
#include "System_System_Security_Cryptography_X509Certificate777637347MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificat2081831987MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificat3304975821MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificate528874471MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificate528874471.h"
#include "System_System_Security_Cryptography_X509Certificat3304975821.h"
#include "System_System_Security_Cryptography_X509Certificat2081831987.h"
#include "Firebase_App_Firebase_LogLevel543421840MethodDeclarations.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext1051733884.h"
#include "mscorlib_System_Threading_SynchronizationContext3857790437MethodDeclarations.h"
#include "mscorlib_System_Threading_Thread241561612MethodDeclarations.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_S692674473MethodDeclarations.h"
#include "mscorlib_System_Threading_Thread241561612.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_S692674473.h"
#include "System_System_Collections_Generic_Queue_1_gen279829387.h"
#include "mscorlib_System_Threading_SynchronizationContext3857790437.h"
#include "mscorlib_System_Threading_ManualResetEvent926074657.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4228867588MethodDeclarations.h"
#include "mscorlib_System_Threading_ManualResetEvent926074657MethodDeclarations.h"
#include "mscorlib_System_Threading_EventWaitHandle2091316307MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4228867588.h"
#include "System_Core_System_Func_1_gen3420419431.h"
#include "mscorlib_System_Threading_SendOrPostCallback296893742MethodDeclarations.h"
#include "mscorlib_System_Threading_SendOrPostCallback296893742.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_3693910080MethodDeclarations.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_3693910080.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_3854140196MethodDeclarations.h"
#include "System_Core_System_Func_1_gen3420419431MethodDeclarations.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_3450855669MethodDeclarations.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_3854140196.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_3450855669.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "mscorlib_System_Threading_WaitHandle677569169MethodDeclarations.h"
#include "mscorlib_System_Threading_WaitHandle677569169.h"
#include "Unity_Compat_System_Tuple_2_gen460172552MethodDeclarations.h"
#include "System_System_Collections_Generic_Queue_1_gen279829387MethodDeclarations.h"
#include "Unity_Compat_System_Tuple_2_gen460172552.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_1465756544.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_1465756544MethodDeclarations.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_1465756541.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_1465756541MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_1310454857MethodDeclarations.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_1310454857.h"
#include "Firebase_App_MonoPInvokeCallbackAttribute1970456718.h"
#include "Firebase_App_MonoPInvokeCallbackAttribute1970456718MethodDeclarations.h"

// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3175043010_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m3175043010(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3175043010_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Firebase.FirebaseApp/FirebaseHandler>()
#define GameObject_AddComponent_TisFirebaseHandler_t2907300047_m3196704967(__this, method) ((  FirebaseHandler_t2907300047 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3175043010_gshared)(__this, method)
// System.Void Firebase.Internal.FirebaseConfigExtensions::SetState<System.Object>(Firebase.FirebaseApp,System.Int32,T,System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,T>>)
extern "C"  void FirebaseConfigExtensions_SetState_TisIl2CppObject_m2070781272_gshared (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, int32_t ___state1, Il2CppObject * ___value2, Dictionary_2_t3612054192 * ___store3, const MethodInfo* method);
#define FirebaseConfigExtensions_SetState_TisIl2CppObject_m2070781272(__this /* static, unused */, ___app0, ___state1, ___value2, ___store3, method) ((  void (*) (Il2CppObject * /* static, unused */, FirebaseApp_t210707726 *, int32_t, Il2CppObject *, Dictionary_2_t3612054192 *, const MethodInfo*))FirebaseConfigExtensions_SetState_TisIl2CppObject_m2070781272_gshared)(__this /* static, unused */, ___app0, ___state1, ___value2, ___store3, method)
// System.Void Firebase.Internal.FirebaseConfigExtensions::SetState<System.String>(Firebase.FirebaseApp,System.Int32,T,System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,T>>)
#define FirebaseConfigExtensions_SetState_TisString_t_m1566626226(__this /* static, unused */, ___app0, ___state1, ___value2, ___store3, method) ((  void (*) (Il2CppObject * /* static, unused */, FirebaseApp_t210707726 *, int32_t, String_t*, Dictionary_2_t2951825130 *, const MethodInfo*))FirebaseConfigExtensions_SetState_TisIl2CppObject_m2070781272_gshared)(__this /* static, unused */, ___app0, ___state1, ___value2, ___store3, method)
// T Firebase.Internal.FirebaseConfigExtensions::GetState<System.Object>(Firebase.FirebaseApp,System.Int32,System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,T>>)
extern "C"  Il2CppObject * FirebaseConfigExtensions_GetState_TisIl2CppObject_m4276357729_gshared (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, int32_t ___state1, Dictionary_2_t3612054192 * ___store2, const MethodInfo* method);
#define FirebaseConfigExtensions_GetState_TisIl2CppObject_m4276357729(__this /* static, unused */, ___app0, ___state1, ___store2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, FirebaseApp_t210707726 *, int32_t, Dictionary_2_t3612054192 *, const MethodInfo*))FirebaseConfigExtensions_GetState_TisIl2CppObject_m4276357729_gshared)(__this /* static, unused */, ___app0, ___state1, ___store2, method)
// T Firebase.Internal.FirebaseConfigExtensions::GetState<System.String>(Firebase.FirebaseApp,System.Int32,System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,T>>)
#define FirebaseConfigExtensions_GetState_TisString_t_m1312439933(__this /* static, unused */, ___app0, ___state1, ___store2, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, FirebaseApp_t210707726 *, int32_t, Dictionary_2_t2951825130 *, const MethodInfo*))FirebaseConfigExtensions_GetState_TisIl2CppObject_m4276357729_gshared)(__this /* static, unused */, ___app0, ___state1, ___store2, method)
// !!0 UnityEngine.GameObject::AddComponent<Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir>()
#define GameObject_AddComponent_TisSynchronizationContextBehavoir_t692674473_m2263356785(__this, method) ((  SynchronizationContextBehavoir_t692674473 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3175043010_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.AppOptions::.ctor(System.IntPtr,System.Boolean)
extern "C"  void AppOptions__ctor_m1733148272 (AppOptions_t1641189195 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		bool L_0 = ___cMemoryOwn1;
		__this->set_swigCMemOwn_1(L_0);
		IntPtr_t L_1 = ___cPtr0;
		HandleRef_t2419939847  L_2;
		memset(&L_2, 0, sizeof(L_2));
		HandleRef__ctor_m1370162289(&L_2, __this, L_1, /*hidden argument*/NULL);
		__this->set_swigCPtr_0(L_2);
		return;
	}
}
// System.Void Firebase.AppOptions::Finalize()
extern "C"  void AppOptions_Finalize_m285105145 (AppOptions_t1641189195 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		AppOptions_Dispose_m287632940(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void Firebase.AppOptions::Dispose()
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern const uint32_t AppOptions_Dispose_m287632940_MetadataUsageId;
extern "C"  void AppOptions_Dispose_m287632940 (AppOptions_t1641189195 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppOptions_Dispose_m287632940_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = __this;
		Il2CppObject * L_0 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			HandleRef_t2419939847 * L_1 = __this->get_address_of_swigCPtr_0();
			IntPtr_t L_2 = HandleRef_get_Handle_m769981143(L_1, /*hidden argument*/NULL);
			IntPtr_t L_3 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_4 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
			if (!L_4)
			{
				goto IL_0050;
			}
		}

IL_0022:
		{
			bool L_5 = __this->get_swigCMemOwn_1();
			if (!L_5)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			__this->set_swigCMemOwn_1((bool)0);
			HandleRef_t2419939847  L_6 = __this->get_swigCPtr_0();
			IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
			AppUtilPINVOKE_Firebase_App_delete_AppOptions_m4039302585(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		}

IL_003f:
		{
			IntPtr_t L_7 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			HandleRef_t2419939847  L_8;
			memset(&L_8, 0, sizeof(L_8));
			HandleRef__ctor_m1370162289(&L_8, NULL, L_7, /*hidden argument*/NULL);
			__this->set_swigCPtr_0(L_8);
		}

IL_0050:
		{
			GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x62, FINALLY_005b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_005b;
	}

FINALLY_005b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_9 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(91)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(91)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0062:
	{
		return;
	}
}
// System.Uri Firebase.AppOptions::get_DatabaseUrl()
extern Il2CppClass* FirebaseApp_t210707726_il2cpp_TypeInfo_var;
extern const uint32_t AppOptions_get_DatabaseUrl_m241783396_MetadataUsageId;
extern "C"  Uri_t19570940 * AppOptions_get_DatabaseUrl_m241783396 (AppOptions_t1641189195 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppOptions_get_DatabaseUrl_m241783396_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = AppOptions_GetDatabaseUrlInternal_m3701450289(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		Uri_t19570940 * L_1 = FirebaseApp_UrlStringToUri_m3126757169(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String Firebase.AppOptions::GetDatabaseUrlInternal()
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t AppOptions_GetDatabaseUrlInternal_m3701450289_MetadataUsageId;
extern "C"  String_t* AppOptions_GetDatabaseUrlInternal_m3701450289 (AppOptions_t1641189195 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppOptions_GetDatabaseUrlInternal_m3701450289_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		String_t* L_1 = AppUtilPINVOKE_Firebase_App_AppOptions_GetDatabaseUrlInternal_m2442888338(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_3 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.Void Firebase.AppUtil::PollCallbacks()
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t AppUtil_PollCallbacks_m1948862416_MetadataUsageId;
extern "C"  void AppUtil_PollCallbacks_m1948862416 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppUtil_PollCallbacks_m1948862416_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		AppUtilPINVOKE_Firebase_App_PollCallbacks_m784944262(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_0 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		return;
	}
}
// System.Void Firebase.AppUtil::AppEnableLogCallback(System.Boolean)
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t AppUtil_AppEnableLogCallback_m1418645513_MetadataUsageId;
extern "C"  void AppUtil_AppEnableLogCallback_m1418645513 (Il2CppObject * __this /* static, unused */, bool ___arg00, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppUtil_AppEnableLogCallback_m1418645513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___arg00;
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		AppUtilPINVOKE_Firebase_App_AppEnableLogCallback_m3057939313(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_1 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_2 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		return;
	}
}
// Firebase.LogLevel Firebase.AppUtil::AppGetLogLevel()
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t AppUtil_AppGetLogLevel_m2540188292_MetadataUsageId;
extern "C"  int32_t AppUtil_AppGetLogLevel_m2540188292 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppUtil_AppGetLogLevel_m2540188292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		int32_t L_0 = AppUtilPINVOKE_Firebase_App_AppGetLogLevel_m1131543554(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_1 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_2 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Void Firebase.AppUtil::SetEnabledAllAppCallbacks(System.Boolean)
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t AppUtil_SetEnabledAllAppCallbacks_m2417423293_MetadataUsageId;
extern "C"  void AppUtil_SetEnabledAllAppCallbacks_m2417423293 (Il2CppObject * __this /* static, unused */, bool ___arg00, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppUtil_SetEnabledAllAppCallbacks_m2417423293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___arg00;
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		AppUtilPINVOKE_Firebase_App_SetEnabledAllAppCallbacks_m3339389901(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_1 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_2 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Firebase.AppUtil::SetLogFunction(Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate)
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t AppUtil_SetLogFunction_m1947305073_MetadataUsageId;
extern "C"  void AppUtil_SetLogFunction_m1947305073 (Il2CppObject * __this /* static, unused */, LogMessageDelegate_t1988210674 * ___arg00, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppUtil_SetLogFunction_m1947305073_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LogMessageDelegate_t1988210674 * L_0 = ___arg00;
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		AppUtilPINVOKE_Firebase_App_SetLogFunction_m3733654825(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_1 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_2 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Firebase.AppUtilPINVOKE::.cctor()
extern Il2CppClass* SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var;
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGStringHelper_t549488518_il2cpp_TypeInfo_var;
extern const uint32_t AppUtilPINVOKE__cctor_m1477430606_MetadataUsageId;
extern "C"  void AppUtilPINVOKE__cctor_m1477430606 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppUtilPINVOKE__cctor_m1477430606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SWIGExceptionHelper_t1412130252 * L_0 = (SWIGExceptionHelper_t1412130252 *)il2cpp_codegen_object_new(SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var);
		SWIGExceptionHelper__ctor_m432919667(L_0, /*hidden argument*/NULL);
		((AppUtilPINVOKE_t3515465473_StaticFields*)AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var->static_fields)->set_swigExceptionHelper_0(L_0);
		SWIGStringHelper_t549488518 * L_1 = (SWIGStringHelper_t549488518 *)il2cpp_codegen_object_new(SWIGStringHelper_t549488518_il2cpp_TypeInfo_var);
		SWIGStringHelper__ctor_m1408369181(L_1, /*hidden argument*/NULL);
		((AppUtilPINVOKE_t3515465473_StaticFields*)AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var->static_fields)->set_swigStringHelper_1(L_1);
		return;
	}
}
extern "C" void DEFAULT_CALL Firebase_App_delete_FutureBase(void*);
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_FutureBase(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_FutureBase_m3328440028 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_App_delete_FutureBase)(____jarg10_marshaled);

}
extern "C" int32_t DEFAULT_CALL Firebase_App_FutureBase_Status(void*);
// System.Int32 Firebase.AppUtilPINVOKE::Firebase_App_FutureBase_Status(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t AppUtilPINVOKE_Firebase_App_FutureBase_Status_m3094756663 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_App_FutureBase_Status)(____jarg10_marshaled);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL Firebase_App_FutureBase_Error(void*);
// System.Int32 Firebase.AppUtilPINVOKE::Firebase_App_FutureBase_Error(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t AppUtilPINVOKE_Firebase_App_FutureBase_Error_m1475679819 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_App_FutureBase_Error)(____jarg10_marshaled);

	return returnValue;
}
extern "C" char* DEFAULT_CALL Firebase_App_FutureBase_ErrorMessage(void*);
// System.String Firebase.AppUtilPINVOKE::Firebase_App_FutureBase_ErrorMessage(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_FutureBase_ErrorMessage_m1466285231 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_App_FutureBase_ErrorMessage)(____jarg10_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL Firebase_App_FutureString_SWIG_OnCompletion(void*, Il2CppMethodPointer, int32_t);
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FutureString_SWIG_OnCompletion(System.Runtime.InteropServices.HandleRef,Firebase.FutureString/SWIG_CompletionDelegate,System.Int32)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_FutureString_SWIG_OnCompletion_m2011264479 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, SWIG_CompletionDelegate_t1819656562 * ___jarg21, int32_t ___jarg32, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, int32_t);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___jarg21' to native representation
	Il2CppMethodPointer ____jarg21_marshaled = NULL;
	____jarg21_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___jarg21));

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_App_FutureString_SWIG_OnCompletion)(____jarg10_marshaled, ____jarg21_marshaled, ___jarg32);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL Firebase_App_FutureString_SWIG_FreeCompletionData(void*, intptr_t);
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_FutureString_SWIG_FreeCompletionData(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C"  void AppUtilPINVOKE_Firebase_App_FutureString_SWIG_FreeCompletionData_m628326608 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, IntPtr_t ___jarg21, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_App_FutureString_SWIG_FreeCompletionData)(____jarg10_marshaled, reinterpret_cast<intptr_t>((___jarg21).get_m_value_0()));

}
extern "C" char* DEFAULT_CALL Firebase_App_FutureString_Result(void*);
// System.String Firebase.AppUtilPINVOKE::Firebase_App_FutureString_Result(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_FutureString_Result_m1458338797 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_App_FutureString_Result)(____jarg10_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL Firebase_App_delete_FutureString(void*);
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_FutureString(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_FutureString_m298034936 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_App_delete_FutureString)(____jarg10_marshaled);

}
extern "C" char* DEFAULT_CALL Firebase_App_AppOptions_GetDatabaseUrlInternal(void*);
// System.String Firebase.AppUtilPINVOKE::Firebase_App_AppOptions_GetDatabaseUrlInternal(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_AppOptions_GetDatabaseUrlInternal_m2442888338 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_App_AppOptions_GetDatabaseUrlInternal)(____jarg10_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL Firebase_App_delete_AppOptions(void*);
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_AppOptions(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_AppOptions_m4039302585 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_App_delete_AppOptions)(____jarg10_marshaled);

}
extern "C" void DEFAULT_CALL Firebase_App_delete_FirebaseApp(void*);
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_FirebaseApp(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_FirebaseApp_m1137114670 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_App_delete_FirebaseApp)(____jarg10_marshaled);

}
extern "C" char* DEFAULT_CALL Firebase_App_FirebaseApp_Name_get(void*);
// System.String Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_Name_get(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_FirebaseApp_Name_get_m2545680430 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_App_FirebaseApp_Name_get)(____jarg10_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL Firebase_App_FirebaseApp_Options_get(void*);
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_Options_get(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_FirebaseApp_Options_get_m2245867877 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_App_FirebaseApp_Options_get)(____jarg10_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL Firebase_App_FirebaseApp_CreateInternal__SWIG_0();
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_CreateInternal__SWIG_0()
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_FirebaseApp_CreateInternal__SWIG_0_m2787922779 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_App_FirebaseApp_CreateInternal__SWIG_0)();

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
extern "C" char* DEFAULT_CALL Firebase_App_FirebaseApp_DefaultName_get();
// System.String Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_DefaultName_get()
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_FirebaseApp_DefaultName_get_m4097276820 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_App_FirebaseApp_DefaultName_get)();

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL Firebase_App_PollCallbacks();
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_PollCallbacks()
extern "C"  void AppUtilPINVOKE_Firebase_App_PollCallbacks_m784944262 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_App_PollCallbacks)();

}
extern "C" void DEFAULT_CALL Firebase_App_AppEnableLogCallback(int32_t);
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_AppEnableLogCallback(System.Boolean)
extern "C"  void AppUtilPINVOKE_Firebase_App_AppEnableLogCallback_m3057939313 (Il2CppObject * __this /* static, unused */, bool ___jarg10, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_App_AppEnableLogCallback)(static_cast<int32_t>(___jarg10));

}
extern "C" int32_t DEFAULT_CALL Firebase_App_AppGetLogLevel();
// System.Int32 Firebase.AppUtilPINVOKE::Firebase_App_AppGetLogLevel()
extern "C"  int32_t AppUtilPINVOKE_Firebase_App_AppGetLogLevel_m1131543554 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_App_AppGetLogLevel)();

	return returnValue;
}
extern "C" void DEFAULT_CALL Firebase_App_SetEnabledAllAppCallbacks(int32_t);
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_SetEnabledAllAppCallbacks(System.Boolean)
extern "C"  void AppUtilPINVOKE_Firebase_App_SetEnabledAllAppCallbacks_m3339389901 (Il2CppObject * __this /* static, unused */, bool ___jarg10, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_App_SetEnabledAllAppCallbacks)(static_cast<int32_t>(___jarg10));

}
extern "C" void DEFAULT_CALL Firebase_App_SetLogFunction(Il2CppMethodPointer);
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_SetLogFunction(Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate)
extern "C"  void AppUtilPINVOKE_Firebase_App_SetLogFunction_m3733654825 (Il2CppObject * __this /* static, unused */, LogMessageDelegate_t1988210674 * ___jarg10, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer);

	// Marshaling of parameter '___jarg10' to native representation
	Il2CppMethodPointer ____jarg10_marshaled = NULL;
	____jarg10_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___jarg10));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_App_SetLogFunction)(____jarg10_marshaled);

}
extern "C" intptr_t DEFAULT_CALL Firebase_App_FutureString_SWIGUpcast(intptr_t);
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FutureString_SWIGUpcast(System.IntPtr)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_FutureString_SWIGUpcast_m1098833757 (Il2CppObject * __this /* static, unused */, IntPtr_t ___jarg10, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_App_FutureString_SWIGUpcast)(reinterpret_cast<intptr_t>((___jarg10).get_m_value_0()));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::.cctor()
extern Il2CppClass* ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var;
extern Il2CppClass* ExceptionArgumentDelegate_t2443153790_il2cpp_TypeInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingApplicationException_m1534282613_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingArithmeticException_m4135332127_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingDivideByZeroException_m2259111009_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m540342305_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingInvalidCastException_m3135880131_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingInvalidOperationException_m2034725867_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingIOException_m2228906713_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingNullReferenceException_m1175983179_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingOutOfMemoryException_m2908735873_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingOverflowException_m3154125439_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingSystemException_m2519873314_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingArgumentException_m3648622388_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingArgumentNullException_m1938756533_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3427245934_MethodInfo_var;
extern const uint32_t SWIGExceptionHelper__cctor_m2709485170_MetadataUsageId;
extern "C"  void SWIGExceptionHelper__cctor_m2709485170 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper__cctor_m2709485170_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingApplicationException_m1534282613_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_1 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_1, NULL, L_0, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_applicationDelegate_0(L_1);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingArithmeticException_m4135332127_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_3 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_3, NULL, L_2, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_arithmeticDelegate_1(L_3);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingDivideByZeroException_m2259111009_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_5 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_5, NULL, L_4, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_divideByZeroDelegate_2(L_5);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m540342305_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_7 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_7, NULL, L_6, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_indexOutOfRangeDelegate_3(L_7);
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingInvalidCastException_m3135880131_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_9 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_9, NULL, L_8, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_invalidCastDelegate_4(L_9);
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingInvalidOperationException_m2034725867_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_11 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_11, NULL, L_10, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_invalidOperationDelegate_5(L_11);
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingIOException_m2228906713_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_13 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_13, NULL, L_12, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_ioDelegate_6(L_13);
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingNullReferenceException_m1175983179_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_15 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_15, NULL, L_14, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_nullReferenceDelegate_7(L_15);
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingOutOfMemoryException_m2908735873_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_17 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_17, NULL, L_16, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_outOfMemoryDelegate_8(L_17);
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingOverflowException_m3154125439_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_19 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_19, NULL, L_18, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_overflowDelegate_9(L_19);
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingSystemException_m2519873314_MethodInfo_var);
		ExceptionDelegate_t2876249339 * L_21 = (ExceptionDelegate_t2876249339 *)il2cpp_codegen_object_new(ExceptionDelegate_t2876249339_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1540989664(L_21, NULL, L_20, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_systemDelegate_10(L_21);
		IntPtr_t L_22;
		L_22.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingArgumentException_m3648622388_MethodInfo_var);
		ExceptionArgumentDelegate_t2443153790 * L_23 = (ExceptionArgumentDelegate_t2443153790 *)il2cpp_codegen_object_new(ExceptionArgumentDelegate_t2443153790_il2cpp_TypeInfo_var);
		ExceptionArgumentDelegate__ctor_m2902104247(L_23, NULL, L_22, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_argumentDelegate_11(L_23);
		IntPtr_t L_24;
		L_24.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingArgumentNullException_m1938756533_MethodInfo_var);
		ExceptionArgumentDelegate_t2443153790 * L_25 = (ExceptionArgumentDelegate_t2443153790 *)il2cpp_codegen_object_new(ExceptionArgumentDelegate_t2443153790_il2cpp_TypeInfo_var);
		ExceptionArgumentDelegate__ctor_m2902104247(L_25, NULL, L_24, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_argumentNullDelegate_12(L_25);
		IntPtr_t L_26;
		L_26.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3427245934_MethodInfo_var);
		ExceptionArgumentDelegate_t2443153790 * L_27 = (ExceptionArgumentDelegate_t2443153790 *)il2cpp_codegen_object_new(ExceptionArgumentDelegate_t2443153790_il2cpp_TypeInfo_var);
		ExceptionArgumentDelegate__ctor_m2902104247(L_27, NULL, L_26, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->set_argumentOutOfRangeDelegate_13(L_27);
		ExceptionDelegate_t2876249339 * L_28 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_applicationDelegate_0();
		ExceptionDelegate_t2876249339 * L_29 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_arithmeticDelegate_1();
		ExceptionDelegate_t2876249339 * L_30 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_divideByZeroDelegate_2();
		ExceptionDelegate_t2876249339 * L_31 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_indexOutOfRangeDelegate_3();
		ExceptionDelegate_t2876249339 * L_32 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_invalidCastDelegate_4();
		ExceptionDelegate_t2876249339 * L_33 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_invalidOperationDelegate_5();
		ExceptionDelegate_t2876249339 * L_34 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_ioDelegate_6();
		ExceptionDelegate_t2876249339 * L_35 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_nullReferenceDelegate_7();
		ExceptionDelegate_t2876249339 * L_36 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_outOfMemoryDelegate_8();
		ExceptionDelegate_t2876249339 * L_37 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_overflowDelegate_9();
		ExceptionDelegate_t2876249339 * L_38 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_systemDelegate_10();
		SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_AppUtil_m1809271624(NULL /*static, unused*/, L_28, L_29, L_30, L_31, L_32, L_33, L_34, L_35, L_36, L_37, L_38, /*hidden argument*/NULL);
		ExceptionArgumentDelegate_t2443153790 * L_39 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_argumentDelegate_11();
		ExceptionArgumentDelegate_t2443153790 * L_40 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_argumentNullDelegate_12();
		ExceptionArgumentDelegate_t2443153790 * L_41 = ((SWIGExceptionHelper_t1412130252_StaticFields*)SWIGExceptionHelper_t1412130252_il2cpp_TypeInfo_var->static_fields)->get_argumentOutOfRangeDelegate_13();
		SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_AppUtil_m386473544(NULL /*static, unused*/, L_39, L_40, L_41, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::.ctor()
extern "C"  void SWIGExceptionHelper__ctor_m432919667 (SWIGExceptionHelper_t1412130252 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL SWIGRegisterExceptionCallbacks_AppUtil(Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacks_AppUtil(Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate)
extern "C"  void SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_AppUtil_m1809271624 (Il2CppObject * __this /* static, unused */, ExceptionDelegate_t2876249339 * ___applicationDelegate0, ExceptionDelegate_t2876249339 * ___arithmeticDelegate1, ExceptionDelegate_t2876249339 * ___divideByZeroDelegate2, ExceptionDelegate_t2876249339 * ___indexOutOfRangeDelegate3, ExceptionDelegate_t2876249339 * ___invalidCastDelegate4, ExceptionDelegate_t2876249339 * ___invalidOperationDelegate5, ExceptionDelegate_t2876249339 * ___ioDelegate6, ExceptionDelegate_t2876249339 * ___nullReferenceDelegate7, ExceptionDelegate_t2876249339 * ___outOfMemoryDelegate8, ExceptionDelegate_t2876249339 * ___overflowDelegate9, ExceptionDelegate_t2876249339 * ___systemExceptionDelegate10, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);

	// Marshaling of parameter '___applicationDelegate0' to native representation
	Il2CppMethodPointer ____applicationDelegate0_marshaled = NULL;
	____applicationDelegate0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___applicationDelegate0));

	// Marshaling of parameter '___arithmeticDelegate1' to native representation
	Il2CppMethodPointer ____arithmeticDelegate1_marshaled = NULL;
	____arithmeticDelegate1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___arithmeticDelegate1));

	// Marshaling of parameter '___divideByZeroDelegate2' to native representation
	Il2CppMethodPointer ____divideByZeroDelegate2_marshaled = NULL;
	____divideByZeroDelegate2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___divideByZeroDelegate2));

	// Marshaling of parameter '___indexOutOfRangeDelegate3' to native representation
	Il2CppMethodPointer ____indexOutOfRangeDelegate3_marshaled = NULL;
	____indexOutOfRangeDelegate3_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___indexOutOfRangeDelegate3));

	// Marshaling of parameter '___invalidCastDelegate4' to native representation
	Il2CppMethodPointer ____invalidCastDelegate4_marshaled = NULL;
	____invalidCastDelegate4_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___invalidCastDelegate4));

	// Marshaling of parameter '___invalidOperationDelegate5' to native representation
	Il2CppMethodPointer ____invalidOperationDelegate5_marshaled = NULL;
	____invalidOperationDelegate5_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___invalidOperationDelegate5));

	// Marshaling of parameter '___ioDelegate6' to native representation
	Il2CppMethodPointer ____ioDelegate6_marshaled = NULL;
	____ioDelegate6_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___ioDelegate6));

	// Marshaling of parameter '___nullReferenceDelegate7' to native representation
	Il2CppMethodPointer ____nullReferenceDelegate7_marshaled = NULL;
	____nullReferenceDelegate7_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___nullReferenceDelegate7));

	// Marshaling of parameter '___outOfMemoryDelegate8' to native representation
	Il2CppMethodPointer ____outOfMemoryDelegate8_marshaled = NULL;
	____outOfMemoryDelegate8_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___outOfMemoryDelegate8));

	// Marshaling of parameter '___overflowDelegate9' to native representation
	Il2CppMethodPointer ____overflowDelegate9_marshaled = NULL;
	____overflowDelegate9_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___overflowDelegate9));

	// Marshaling of parameter '___systemExceptionDelegate10' to native representation
	Il2CppMethodPointer ____systemExceptionDelegate10_marshaled = NULL;
	____systemExceptionDelegate10_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___systemExceptionDelegate10));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SWIGRegisterExceptionCallbacks_AppUtil)(____applicationDelegate0_marshaled, ____arithmeticDelegate1_marshaled, ____divideByZeroDelegate2_marshaled, ____indexOutOfRangeDelegate3_marshaled, ____invalidCastDelegate4_marshaled, ____invalidOperationDelegate5_marshaled, ____ioDelegate6_marshaled, ____nullReferenceDelegate7_marshaled, ____outOfMemoryDelegate8_marshaled, ____overflowDelegate9_marshaled, ____systemExceptionDelegate10_marshaled);

}
extern "C" void DEFAULT_CALL SWIGRegisterExceptionCallbacksArgument_AppUtil(Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacksArgument_AppUtil(Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate)
extern "C"  void SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_AppUtil_m386473544 (Il2CppObject * __this /* static, unused */, ExceptionArgumentDelegate_t2443153790 * ___argumentDelegate0, ExceptionArgumentDelegate_t2443153790 * ___argumentNullDelegate1, ExceptionArgumentDelegate_t2443153790 * ___argumentOutOfRangeDelegate2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);

	// Marshaling of parameter '___argumentDelegate0' to native representation
	Il2CppMethodPointer ____argumentDelegate0_marshaled = NULL;
	____argumentDelegate0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___argumentDelegate0));

	// Marshaling of parameter '___argumentNullDelegate1' to native representation
	Il2CppMethodPointer ____argumentNullDelegate1_marshaled = NULL;
	____argumentNullDelegate1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___argumentNullDelegate1));

	// Marshaling of parameter '___argumentOutOfRangeDelegate2' to native representation
	Il2CppMethodPointer ____argumentOutOfRangeDelegate2_marshaled = NULL;
	____argumentOutOfRangeDelegate2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___argumentOutOfRangeDelegate2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SWIGRegisterExceptionCallbacksArgument_AppUtil)(____argumentDelegate0_marshaled, ____argumentNullDelegate1_marshaled, ____argumentOutOfRangeDelegate2_marshaled);

}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingApplicationException(System.String)
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern Il2CppClass* ApplicationException_t474868623_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingApplicationException_m1534282613_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingApplicationException_m1534282613 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingApplicationException_m1534282613_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		ApplicationException_t474868623 * L_2 = (ApplicationException_t474868623 *)il2cpp_codegen_object_new(ApplicationException_t474868623_il2cpp_TypeInfo_var);
		ApplicationException__ctor_m856993678(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingApplicationException_m1534282613(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingApplicationException_m1534282613(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArithmeticException(System.String)
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern Il2CppClass* ArithmeticException_t3261462543_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingArithmeticException_m4135332127_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingArithmeticException_m4135332127 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingArithmeticException_m4135332127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		ArithmeticException_t3261462543 * L_2 = (ArithmeticException_t3261462543 *)il2cpp_codegen_object_new(ArithmeticException_t3261462543_il2cpp_TypeInfo_var);
		ArithmeticException__ctor_m2264388592(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArithmeticException_m4135332127(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingArithmeticException_m4135332127(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingDivideByZeroException(System.String)
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern Il2CppClass* DivideByZeroException_t1660837001_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingDivideByZeroException_m2259111009_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingDivideByZeroException_m2259111009 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingDivideByZeroException_m2259111009_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		DivideByZeroException_t1660837001 * L_2 = (DivideByZeroException_t1660837001 *)il2cpp_codegen_object_new(DivideByZeroException_t1660837001_il2cpp_TypeInfo_var);
		DivideByZeroException__ctor_m4006599682(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingDivideByZeroException_m2259111009(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingDivideByZeroException_m2259111009(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingIndexOutOfRangeException(System.String)
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern Il2CppClass* IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m540342305_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m540342305 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m540342305_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IndexOutOfRangeException_t3527622107 * L_2 = (IndexOutOfRangeException_t3527622107 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m667958210(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m540342305(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m540342305(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingInvalidCastException(System.String)
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingInvalidCastException_m3135880131_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingInvalidCastException_m3135880131 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingInvalidCastException_m3135880131_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		InvalidCastException_t3625212209 * L_2 = (InvalidCastException_t3625212209 *)il2cpp_codegen_object_new(InvalidCastException_t3625212209_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m3097122796(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingInvalidCastException_m3135880131(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingInvalidCastException_m3135880131(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingInvalidOperationException(System.String)
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingInvalidOperationException_m2034725867_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingInvalidOperationException_m2034725867 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingInvalidOperationException_m2034725867_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		InvalidOperationException_t721527559 * L_2 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m725121084(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingInvalidOperationException_m2034725867(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingInvalidOperationException_m2034725867(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingIOException(System.String)
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern Il2CppClass* IOException_t2458421087_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingIOException_m2228906713_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingIOException_m2228906713 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingIOException_m2228906713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IOException_t2458421087 * L_2 = (IOException_t2458421087 *)il2cpp_codegen_object_new(IOException_t2458421087_il2cpp_TypeInfo_var);
		IOException__ctor_m847281350(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingIOException_m2228906713(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingIOException_m2228906713(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingNullReferenceException(System.String)
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingNullReferenceException_m1175983179_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingNullReferenceException_m1175983179 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingNullReferenceException_m1175983179_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullReferenceException_t3156209119 * L_2 = (NullReferenceException_t3156209119 *)il2cpp_codegen_object_new(NullReferenceException_t3156209119_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m4167886794(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingNullReferenceException_m1175983179(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingNullReferenceException_m1175983179(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingOutOfMemoryException(System.String)
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern Il2CppClass* OutOfMemoryException_t1181064283_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingOutOfMemoryException_m2908735873_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingOutOfMemoryException_m2908735873 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingOutOfMemoryException_m2908735873_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		OutOfMemoryException_t1181064283 * L_2 = (OutOfMemoryException_t1181064283 *)il2cpp_codegen_object_new(OutOfMemoryException_t1181064283_il2cpp_TypeInfo_var);
		OutOfMemoryException__ctor_m2926800194(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingOutOfMemoryException_m2908735873(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingOutOfMemoryException_m2908735873(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingOverflowException(System.String)
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern Il2CppClass* OverflowException_t1075868493_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingOverflowException_m3154125439_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingOverflowException_m3154125439 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingOverflowException_m3154125439_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		OverflowException_t1075868493 * L_2 = (OverflowException_t1075868493 *)il2cpp_codegen_object_new(OverflowException_t1075868493_il2cpp_TypeInfo_var);
		OverflowException__ctor_m579335998(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingOverflowException_m3154125439(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingOverflowException_m3154125439(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingSystemException(System.String)
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern Il2CppClass* SystemException_t3877406272_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingSystemException_m2519873314_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingSystemException_m2519873314 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingSystemException_m2519873314_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		SystemException_t3877406272 * L_2 = (SystemException_t3877406272 *)il2cpp_codegen_object_new(SystemException_t3877406272_il2cpp_TypeInfo_var);
		SystemException__ctor_m3356086419(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingSystemException_m2519873314(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingSystemException_m2519873314(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentException(System.String,System.String)
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingArgumentException_m3648622388_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingArgumentException_m3648622388 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingArgumentException_m3648622388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		String_t* L_1 = ___paramName1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_2 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3312963299(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentException_m3648622388(char* ___message0, char* ___paramName1)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Marshaling of parameter '___paramName1' to managed representation
	String_t* ____paramName1_unmarshaled = NULL;
	____paramName1_unmarshaled = il2cpp_codegen_marshal_string_result(___paramName1);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingArgumentException_m3648622388(NULL, ____message0_unmarshaled, ____paramName1_unmarshaled, NULL);

}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentNullException(System.String,System.String)
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4083594583;
extern const uint32_t SWIGExceptionHelper_SetPendingArgumentNullException_m1938756533_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingArgumentNullException_m1938756533 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingArgumentNullException_m1938756533_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_0 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Exception_t1927440687 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_2 = ___message0;
		Exception_t1927440687 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m612901809(NULL /*static, unused*/, L_2, _stringLiteral4083594583, L_4, /*hidden argument*/NULL);
		___message0 = L_5;
	}

IL_001f:
	{
		String_t* L_6 = ___paramName1;
		String_t* L_7 = ___message0;
		ArgumentNullException_t628810857 * L_8 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2624491786(L_8, L_6, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentNullException_m1938756533(char* ___message0, char* ___paramName1)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Marshaling of parameter '___paramName1' to managed representation
	String_t* ____paramName1_unmarshaled = NULL;
	____paramName1_unmarshaled = il2cpp_codegen_marshal_string_result(___paramName1);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingArgumentNullException_m1938756533(NULL, ____message0_unmarshaled, ____paramName1_unmarshaled, NULL);

}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentOutOfRangeException(System.String,System.String)
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4083594583;
extern const uint32_t SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3427245934_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3427245934 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3427245934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_0 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Exception_t1927440687 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_2 = ___message0;
		Exception_t1927440687 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m612901809(NULL /*static, unused*/, L_2, _stringLiteral4083594583, L_4, /*hidden argument*/NULL);
		___message0 = L_5;
	}

IL_001f:
	{
		String_t* L_6 = ___paramName1;
		String_t* L_7 = ___message0;
		ArgumentOutOfRangeException_t279959794 * L_8 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_8, L_6, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		SWIGPendingException_Set_m944203304(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3427245934(char* ___message0, char* ___paramName1)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Marshaling of parameter '___paramName1' to managed representation
	String_t* ____paramName1_unmarshaled = NULL;
	____paramName1_unmarshaled = il2cpp_codegen_marshal_string_result(___paramName1);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m3427245934(NULL, ____message0_unmarshaled, ____paramName1_unmarshaled, NULL);

}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ExceptionArgumentDelegate__ctor_m2902104247 (ExceptionArgumentDelegate_t2443153790 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::Invoke(System.String,System.String)
extern "C"  void ExceptionArgumentDelegate_Invoke_m3328736243 (ExceptionArgumentDelegate_t2443153790 * __this, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExceptionArgumentDelegate_Invoke_m3328736243((ExceptionArgumentDelegate_t2443153790 *)__this->get_prev_9(),___message0, ___paramName1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___message0, ___paramName1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___message0, ___paramName1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___paramName1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___message0, ___paramName1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_ExceptionArgumentDelegate_t2443153790 (ExceptionArgumentDelegate_t2443153790 * __this, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Marshaling of parameter '___paramName1' to native representation
	char* ____paramName1_marshaled = NULL;
	____paramName1_marshaled = il2cpp_codegen_marshal_string(___paramName1);

	// Native function invocation
	il2cppPInvokeFunc(____message0_marshaled, ____paramName1_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

	// Marshaling cleanup of parameter '___paramName1' native representation
	il2cpp_codegen_marshal_free(____paramName1_marshaled);
	____paramName1_marshaled = NULL;

}
// System.IAsyncResult Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExceptionArgumentDelegate_BeginInvoke_m1234147944 (ExceptionArgumentDelegate_t2443153790 * __this, String_t* ___message0, String_t* ___paramName1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___message0;
	__d_args[1] = ___paramName1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ExceptionArgumentDelegate_EndInvoke_m2954088285 (ExceptionArgumentDelegate_t2443153790 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ExceptionDelegate__ctor_m1540989664 (ExceptionDelegate_t2876249339 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::Invoke(System.String)
extern "C"  void ExceptionDelegate_Invoke_m4439704 (ExceptionDelegate_t2876249339 * __this, String_t* ___message0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExceptionDelegate_Invoke_m4439704((ExceptionDelegate_t2876249339 *)__this->get_prev_9(),___message0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___message0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___message0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_ExceptionDelegate_t2876249339 (ExceptionDelegate_t2876249339 * __this, String_t* ___message0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	il2cppPInvokeFunc(____message0_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

}
// System.IAsyncResult Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExceptionDelegate_BeginInvoke_m979767613 (ExceptionDelegate_t2876249339 * __this, String_t* ___message0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___message0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ExceptionDelegate_EndInvoke_m405205414 (ExceptionDelegate_t2876249339 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Boolean Firebase.AppUtilPINVOKE/SWIGPendingException::get_Pending()
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t SWIGPendingException_get_Pending_m1368465104_MetadataUsageId;
extern "C"  bool SWIGPendingException_get_Pending_m1368465104 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGPendingException_get_Pending_m1368465104_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		int32_t L_0 = ((SWIGPendingException_t4193433529_StaticFields*)SWIGPendingException_t4193433529_il2cpp_TypeInfo_var->static_fields)->get_numExceptionsPending_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = ((SWIGPendingException_t4193433529_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var))->get_pendingException_0();
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_0019:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGPendingException::Set(System.Exception)
extern const Il2CppType* AppUtilPINVOKE_t3515465473_0_0_0_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ApplicationException_t474868623_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1125746324;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t SWIGPendingException_Set_m944203304_MetadataUsageId;
extern "C"  void SWIGPendingException_Set_m944203304 (Il2CppObject * __this /* static, unused */, Exception_t1927440687 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGPendingException_Set_m944203304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_0 = ((SWIGPendingException_t4193433529_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var))->get_pendingException_0();
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = ((SWIGPendingException_t4193433529_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var))->get_pendingException_0();
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1125746324, L_2, _stringLiteral372029317, /*hidden argument*/NULL);
		Exception_t1927440687 * L_4 = ___e0;
		ApplicationException_t474868623 * L_5 = (ApplicationException_t474868623 *)il2cpp_codegen_object_new(ApplicationException_t474868623_il2cpp_TypeInfo_var);
		ApplicationException__ctor_m856993678(L_5, L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002a:
	{
		Exception_t1927440687 * L_6 = ___e0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		((SWIGPendingException_t4193433529_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var))->set_pendingException_0(L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(AppUtilPINVOKE_t3515465473_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_7;
		Il2CppObject * L_8 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0041:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		int32_t L_9 = ((SWIGPendingException_t4193433529_StaticFields*)SWIGPendingException_t4193433529_il2cpp_TypeInfo_var->static_fields)->get_numExceptionsPending_1();
		((SWIGPendingException_t4193433529_StaticFields*)SWIGPendingException_t4193433529_il2cpp_TypeInfo_var->static_fields)->set_numExceptionsPending_1(((int32_t)((int32_t)L_9+(int32_t)1)));
		IL2CPP_LEAVE(0x59, FINALLY_0052);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0052;
	}

FINALLY_0052:
	{ // begin finally (depth: 1)
		Il2CppObject * L_10 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(82)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(82)
	{
		IL2CPP_JUMP_TBL(0x59, IL_0059)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0059:
	{
		return;
	}
}
// System.Exception Firebase.AppUtilPINVOKE/SWIGPendingException::Retrieve()
extern const Il2CppType* AppUtilPINVOKE_t3515465473_0_0_0_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t SWIGPendingException_Retrieve_m197352211_MetadataUsageId;
extern "C"  Exception_t1927440687 * SWIGPendingException_Retrieve_m197352211 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGPendingException_Retrieve_m197352211_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (Exception_t1927440687 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		int32_t L_0 = ((SWIGPendingException_t4193433529_StaticFields*)SWIGPendingException_t4193433529_il2cpp_TypeInfo_var->static_fields)->get_numExceptionsPending_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = ((SWIGPendingException_t4193433529_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var))->get_pendingException_0();
		if (!L_1)
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_2 = ((SWIGPendingException_t4193433529_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var))->get_pendingException_0();
		V_0 = L_2;
		((SWIGPendingException_t4193433529_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var))->set_pendingException_0((Exception_t1927440687 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(AppUtilPINVOKE_t3515465473_0_0_0_var), /*hidden argument*/NULL);
		V_1 = L_3;
		Il2CppObject * L_4 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0034:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		int32_t L_5 = ((SWIGPendingException_t4193433529_StaticFields*)SWIGPendingException_t4193433529_il2cpp_TypeInfo_var->static_fields)->get_numExceptionsPending_1();
		((SWIGPendingException_t4193433529_StaticFields*)SWIGPendingException_t4193433529_il2cpp_TypeInfo_var->static_fields)->set_numExceptionsPending_1(((int32_t)((int32_t)L_5-(int32_t)1)));
		IL2CPP_LEAVE(0x4C, FINALLY_0045);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(69)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x4C, IL_004c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_004c:
	{
		Exception_t1927440687 * L_7 = V_0;
		return L_7;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGPendingException::.cctor()
extern "C"  void SWIGPendingException__cctor_m4006952997 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::.cctor()
extern Il2CppClass* SWIGStringDelegate_t18001273_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGStringHelper_t549488518_il2cpp_TypeInfo_var;
extern const MethodInfo* SWIGStringHelper_CreateString_m687016595_MethodInfo_var;
extern const uint32_t SWIGStringHelper__cctor_m3676208588_MetadataUsageId;
extern "C"  void SWIGStringHelper__cctor_m3676208588 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGStringHelper__cctor_m3676208588_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)SWIGStringHelper_CreateString_m687016595_MethodInfo_var);
		SWIGStringDelegate_t18001273 * L_1 = (SWIGStringDelegate_t18001273 *)il2cpp_codegen_object_new(SWIGStringDelegate_t18001273_il2cpp_TypeInfo_var);
		SWIGStringDelegate__ctor_m579429766(L_1, NULL, L_0, /*hidden argument*/NULL);
		((SWIGStringHelper_t549488518_StaticFields*)SWIGStringHelper_t549488518_il2cpp_TypeInfo_var->static_fields)->set_stringDelegate_0(L_1);
		SWIGStringDelegate_t18001273 * L_2 = ((SWIGStringHelper_t549488518_StaticFields*)SWIGStringHelper_t549488518_il2cpp_TypeInfo_var->static_fields)->get_stringDelegate_0();
		SWIGStringHelper_SWIGRegisterStringCallback_AppUtil_m4196108411(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::.ctor()
extern "C"  void SWIGStringHelper__ctor_m1408369181 (SWIGStringHelper_t549488518 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL SWIGRegisterStringCallback_AppUtil(Il2CppMethodPointer);
// System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::SWIGRegisterStringCallback_AppUtil(Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate)
extern "C"  void SWIGStringHelper_SWIGRegisterStringCallback_AppUtil_m4196108411 (Il2CppObject * __this /* static, unused */, SWIGStringDelegate_t18001273 * ___stringDelegate0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer);

	// Marshaling of parameter '___stringDelegate0' to native representation
	Il2CppMethodPointer ____stringDelegate0_marshaled = NULL;
	____stringDelegate0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___stringDelegate0));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SWIGRegisterStringCallback_AppUtil)(____stringDelegate0_marshaled);

}
// System.String Firebase.AppUtilPINVOKE/SWIGStringHelper::CreateString(System.String)
extern "C"  String_t* SWIGStringHelper_CreateString_m687016595 (Il2CppObject * __this /* static, unused */, String_t* ___cString0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___cString0;
		return L_0;
	}
}
extern "C" char* DEFAULT_CALL ReversePInvokeWrapper_SWIGStringHelper_CreateString_m687016595(char* ___cString0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___cString0' to managed representation
	String_t* ____cString0_unmarshaled = NULL;
	____cString0_unmarshaled = il2cpp_codegen_marshal_string_result(___cString0);

	// Managed method invocation
	String_t* returnValue = ::SWIGStringHelper_CreateString_m687016595(NULL, ____cString0_unmarshaled, NULL);

	// Marshaling of return value back from managed representation
	char* _returnValue_marshaled = NULL;
	_returnValue_marshaled = il2cpp_codegen_marshal_string(returnValue);

	return _returnValue_marshaled;
}
// System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void SWIGStringDelegate__ctor_m579429766 (SWIGStringDelegate_t18001273 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.String Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::Invoke(System.String)
extern "C"  String_t* SWIGStringDelegate_Invoke_m1812140305 (SWIGStringDelegate_t18001273 * __this, String_t* ___message0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		SWIGStringDelegate_Invoke_m1812140305((SWIGStringDelegate_t18001273 *)__this->get_prev_9(),___message0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef String_t* (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___message0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef String_t* (*FunctionPointerType) (void* __this, String_t* ___message0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef String_t* (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  String_t* DelegatePInvokeWrapper_SWIGStringDelegate_t18001273 (SWIGStringDelegate_t18001273 * __this, String_t* ___message0, const MethodInfo* method)
{
	typedef char* (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	char* returnValue = il2cppPInvokeFunc(____message0_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

	return _returnValue_unmarshaled;
}
// System.IAsyncResult Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SWIGStringDelegate_BeginInvoke_m1506460307 (SWIGStringDelegate_t18001273 * __this, String_t* ___message0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___message0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.String Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::EndInvoke(System.IAsyncResult)
extern "C"  String_t* SWIGStringDelegate_EndInvoke_m3347905093 (SWIGStringDelegate_t18001273 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (String_t*)__result;
}
// System.Void Firebase.FirebaseApp::.ctor(System.IntPtr,System.Boolean)
extern "C"  void FirebaseApp__ctor_m3662482775 (FirebaseApp_t210707726 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		bool L_0 = ___cMemoryOwn1;
		__this->set_swigCMemOwn_1(L_0);
		IntPtr_t L_1 = ___cPtr0;
		HandleRef_t2419939847  L_2;
		memset(&L_2, 0, sizeof(L_2));
		HandleRef__ctor_m1370162289(&L_2, __this, L_1, /*hidden argument*/NULL);
		__this->set_swigCPtr_0(L_2);
		return;
	}
}
// System.Runtime.InteropServices.HandleRef Firebase.FirebaseApp::getCPtr(Firebase.FirebaseApp)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseApp_getCPtr_m4255293038_MetadataUsageId;
extern "C"  HandleRef_t2419939847  FirebaseApp_getCPtr_m4255293038 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_getCPtr_m4255293038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HandleRef_t2419939847  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		FirebaseApp_t210707726 * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		HandleRef_t2419939847  L_2;
		memset(&L_2, 0, sizeof(L_2));
		HandleRef__ctor_m1370162289(&L_2, NULL, L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_001c;
	}

IL_0016:
	{
		FirebaseApp_t210707726 * L_3 = ___obj0;
		NullCheck(L_3);
		HandleRef_t2419939847  L_4 = L_3->get_swigCPtr_0();
		G_B3_0 = L_4;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Void Firebase.FirebaseApp::Finalize()
extern Il2CppClass* FirebaseApp_t210707726_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseApp_Finalize_m3651549934_MetadataUsageId;
extern "C"  void FirebaseApp_Finalize_m3651549934 (FirebaseApp_t210707726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_Finalize_m3651549934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		FirebaseApp_Dispose_m3098692549(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		FirebaseApp_RemoveReference_m1475253910(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x18, FINALLY_0011);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0011;
	}

FINALLY_0011:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(17)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(17)
	{
		IL2CPP_JUMP_TBL(0x18, IL_0018)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0018:
	{
		return;
	}
}
// System.Void Firebase.FirebaseApp::Dispose()
extern Il2CppClass* FirebaseApp_t210707726_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DestroyDelegate_t3635929227_il2cpp_TypeInfo_var;
extern const MethodInfo* FirebaseApp_U3CDisposeU3Em__0_m3004522492_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Remove_m196626582_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Remove_m2831909098_MethodInfo_var;
extern const uint32_t FirebaseApp_Dispose_m3098692549_MetadataUsageId;
extern "C"  void FirebaseApp_Dispose_m3098692549 (FirebaseApp_t210707726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_Dispose_m3098692549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		Dictionary_2_t2125486988 * L_0 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_nameToProxy_2();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			HandleRef_t2419939847 * L_2 = __this->get_address_of_swigCPtr_0();
			IntPtr_t L_3 = HandleRef_get_Handle_m769981143(L_2, /*hidden argument*/NULL);
			IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_5 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
			if (!L_5)
			{
				goto IL_005f;
			}
		}

IL_0026:
		{
			IntPtr_t L_6;
			L_6.set_m_value_0((void*)(void*)FirebaseApp_U3CDisposeU3Em__0_m3004522492_MethodInfo_var);
			DestroyDelegate_t3635929227 * L_7 = (DestroyDelegate_t3635929227 *)il2cpp_codegen_object_new(DestroyDelegate_t3635929227_il2cpp_TypeInfo_var);
			DestroyDelegate__ctor_m984049708(L_7, __this, L_6, /*hidden argument*/NULL);
			__this->set_destroy_5(L_7);
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
			Dictionary_2_t652733044 * L_8 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_cPtrToProxy_3();
			HandleRef_t2419939847 * L_9 = __this->get_address_of_swigCPtr_0();
			IntPtr_t L_10 = HandleRef_get_Handle_m769981143(L_9, /*hidden argument*/NULL);
			NullCheck(L_8);
			Dictionary_2_Remove_m196626582(L_8, L_10, /*hidden argument*/Dictionary_2_Remove_m196626582_MethodInfo_var);
			Dictionary_2_t2125486988 * L_11 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_nameToProxy_2();
			String_t* L_12 = FirebaseApp_get_Name_m3737532545(__this, /*hidden argument*/NULL);
			NullCheck(L_11);
			Dictionary_2_Remove_m2831909098(L_11, L_12, /*hidden argument*/Dictionary_2_Remove_m2831909098_MethodInfo_var);
		}

IL_005f:
		{
			IL2CPP_LEAVE(0x6B, FINALLY_0064);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0064;
	}

FINALLY_0064:
	{ // begin finally (depth: 1)
		Il2CppObject * L_13 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(100)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(100)
	{
		IL2CPP_JUMP_TBL(0x6B, IL_006b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_006b:
	{
		return;
	}
}
// Firebase.FirebaseApp Firebase.FirebaseApp::get_DefaultInstance()
extern Il2CppClass* FirebaseHandler_t2907300047_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseApp_t210707726_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseApp_get_DefaultInstance_m465202029_MetadataUsageId;
extern "C"  FirebaseApp_t210707726 * FirebaseApp_get_DefaultInstance_m465202029 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_get_DefaultInstance_m465202029_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FirebaseApp_t210707726 * V_0 = NULL;
	FirebaseApp_t210707726 * G_B3_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
		FirebaseHandler_Create_m3248912600(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		String_t* L_0 = FirebaseApp_get_DefaultName_m2031394344(NULL /*static, unused*/, /*hidden argument*/NULL);
		FirebaseApp_t210707726 * L_1 = FirebaseApp_GetInstance_m4058350271(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		FirebaseApp_t210707726 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		FirebaseApp_t210707726 * L_3 = V_0;
		G_B3_0 = L_3;
		goto IL_0021;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		FirebaseApp_t210707726 * L_4 = FirebaseApp_Create_m4111948162(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0021:
	{
		return G_B3_0;
	}
}
// Firebase.FirebaseApp Firebase.FirebaseApp::GetInstance(System.String)
extern Il2CppClass* FirebaseApp_t210707726_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseHandler_t2907300047_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m3167158394_MethodInfo_var;
extern const uint32_t FirebaseApp_GetInstance_m4058350271_MetadataUsageId;
extern "C"  FirebaseApp_t210707726 * FirebaseApp_GetInstance_m4058350271 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_GetInstance_m4058350271_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	FirebaseApp_t210707726 * V_1 = NULL;
	FirebaseApp_t210707726 * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		Dictionary_2_t2125486988 * L_0 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_nameToProxy_2();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
			FirebaseHandler_Create_m3248912600(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
			Dictionary_2_t2125486988 * L_2 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_nameToProxy_2();
			String_t* L_3 = ___name0;
			NullCheck(L_2);
			bool L_4 = Dictionary_2_TryGetValue_m3167158394(L_2, L_3, (&V_1), /*hidden argument*/Dictionary_2_TryGetValue_m3167158394_MethodInfo_var);
			if (!L_4)
			{
				goto IL_002a;
			}
		}

IL_0023:
		{
			FirebaseApp_t210707726 * L_5 = V_1;
			V_2 = L_5;
			IL2CPP_LEAVE(0x38, FINALLY_0031);
		}

IL_002a:
		{
			V_2 = (FirebaseApp_t210707726 *)NULL;
			IL2CPP_LEAVE(0x38, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(49)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x38, IL_0038)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0038:
	{
		FirebaseApp_t210707726 * L_7 = V_2;
		return L_7;
	}
}
// Firebase.FirebaseApp Firebase.FirebaseApp::Create()
extern Il2CppClass* FirebaseApp_t210707726_il2cpp_TypeInfo_var;
extern Il2CppClass* CreateDelegate_t413676709_il2cpp_TypeInfo_var;
extern const MethodInfo* FirebaseApp_U3CCreateU3Em__1_m64761912_MethodInfo_var;
extern const uint32_t FirebaseApp_Create_m4111948162_MetadataUsageId;
extern "C"  FirebaseApp_t210707726 * FirebaseApp_Create_m4111948162 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_Create_m4111948162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		CreateDelegate_t413676709 * L_0 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_6();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)FirebaseApp_U3CCreateU3Em__1_m64761912_MethodInfo_var);
		CreateDelegate_t413676709 * L_2 = (CreateDelegate_t413676709 *)il2cpp_codegen_object_new(CreateDelegate_t413676709_il2cpp_TypeInfo_var);
		CreateDelegate__ctor_m3440641418(L_2, NULL, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_6(L_2);
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		CreateDelegate_t413676709 * L_3 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_6();
		FirebaseApp_t210707726 * L_4 = FirebaseApp_CreateAndTrack_m2952140369(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void Firebase.FirebaseApp::RemoveReference(Firebase.FirebaseApp)
extern Il2CppClass* FirebaseApp_t210707726_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m3177056351_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m1952206578_MethodInfo_var;
extern const uint32_t FirebaseApp_RemoveReference_m1475253910_MetadataUsageId;
extern "C"  void FirebaseApp_RemoveReference_m1475253910 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_RemoveReference_m1475253910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	IntPtr_t V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		Dictionary_2_t2125486988 * L_0 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_nameToProxy_2();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			FirebaseApp_t210707726 * L_2 = ___app0;
			NullCheck(L_2);
			HandleRef_t2419939847 * L_3 = L_2->get_address_of_swigCPtr_0();
			IntPtr_t L_4 = HandleRef_get_Handle_m769981143(L_3, /*hidden argument*/NULL);
			V_1 = L_4;
			IntPtr_t L_5 = V_1;
			IntPtr_t L_6 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_7 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_0071;
			}
		}

IL_0028:
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
			Dictionary_2_t2513902766 * L_8 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_cPtrRefCount_4();
			IntPtr_t L_9 = V_1;
			Dictionary_2_t2513902766 * L_10 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_cPtrRefCount_4();
			IntPtr_t L_11 = V_1;
			NullCheck(L_10);
			int32_t L_12 = Dictionary_2_get_Item_m3177056351(L_10, L_11, /*hidden argument*/Dictionary_2_get_Item_m3177056351_MethodInfo_var);
			NullCheck(L_8);
			Dictionary_2_set_Item_m1952206578(L_8, L_9, ((int32_t)((int32_t)L_12-(int32_t)1)), /*hidden argument*/Dictionary_2_set_Item_m1952206578_MethodInfo_var);
			Dictionary_2_t2513902766 * L_13 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_cPtrRefCount_4();
			IntPtr_t L_14 = V_1;
			NullCheck(L_13);
			int32_t L_15 = Dictionary_2_get_Item_m3177056351(L_13, L_14, /*hidden argument*/Dictionary_2_get_Item_m3177056351_MethodInfo_var);
			if (L_15)
			{
				goto IL_0071;
			}
		}

IL_0050:
		{
			FirebaseApp_t210707726 * L_16 = ___app0;
			NullCheck(L_16);
			bool L_17 = L_16->get_swigCMemOwn_1();
			if (!L_17)
			{
				goto IL_0071;
			}
		}

IL_005b:
		{
			FirebaseApp_t210707726 * L_18 = ___app0;
			NullCheck(L_18);
			DestroyDelegate_t3635929227 * L_19 = L_18->get_destroy_5();
			if (!L_19)
			{
				goto IL_0071;
			}
		}

IL_0066:
		{
			FirebaseApp_t210707726 * L_20 = ___app0;
			NullCheck(L_20);
			DestroyDelegate_t3635929227 * L_21 = L_20->get_destroy_5();
			NullCheck(L_21);
			DestroyDelegate_Invoke_m3785470502(L_21, /*hidden argument*/NULL);
		}

IL_0071:
		{
			FirebaseApp_t210707726 * L_22 = ___app0;
			NullCheck(L_22);
			L_22->set_swigCMemOwn_1((bool)0);
			FirebaseApp_t210707726 * L_23 = ___app0;
			IntPtr_t L_24 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			HandleRef_t2419939847  L_25;
			memset(&L_25, 0, sizeof(L_25));
			HandleRef__ctor_m1370162289(&L_25, NULL, L_24, /*hidden argument*/NULL);
			NullCheck(L_23);
			L_23->set_swigCPtr_0(L_25);
			IL2CPP_LEAVE(0x95, FINALLY_008e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_008e;
	}

FINALLY_008e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_26 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(142)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(142)
	{
		IL2CPP_JUMP_TBL(0x95, IL_0095)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0095:
	{
		return;
	}
}
// System.Void Firebase.FirebaseApp::EmptyAppDictionaries()
extern Il2CppClass* FirebaseApp_t210707726_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3874796154_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m373261557_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3695390092_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Clear_m1059231906_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Clear_m3658164290_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m3866234593_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3438154773_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m112670625_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1826530985_MethodInfo_var;
extern const uint32_t FirebaseApp_EmptyAppDictionaries_m4102170024_MetadataUsageId;
extern "C"  void FirebaseApp_EmptyAppDictionaries_m4102170024 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_EmptyAppDictionaries_m4102170024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	List_1_t3874796154 * V_1 = NULL;
	FirebaseApp_t210707726 * V_2 = NULL;
	Enumerator_t3409525828  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		Dictionary_2_t2125486988 * L_0 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_nameToProxy_2();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
			Dictionary_2_t2125486988 * L_2 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_nameToProxy_2();
			NullCheck(L_2);
			ValueCollection_t828546831 * L_3 = Dictionary_2_get_Values_m373261557(L_2, /*hidden argument*/Dictionary_2_get_Values_m373261557_MethodInfo_var);
			List_1_t3874796154 * L_4 = (List_1_t3874796154 *)il2cpp_codegen_object_new(List_1_t3874796154_il2cpp_TypeInfo_var);
			List_1__ctor_m3695390092(L_4, L_3, /*hidden argument*/List_1__ctor_m3695390092_MethodInfo_var);
			V_1 = L_4;
			Dictionary_2_t652733044 * L_5 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_cPtrToProxy_3();
			NullCheck(L_5);
			Dictionary_2_Clear_m1059231906(L_5, /*hidden argument*/Dictionary_2_Clear_m1059231906_MethodInfo_var);
			Dictionary_2_t2125486988 * L_6 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_nameToProxy_2();
			NullCheck(L_6);
			Dictionary_2_Clear_m3658164290(L_6, /*hidden argument*/Dictionary_2_Clear_m3658164290_MethodInfo_var);
			List_1_t3874796154 * L_7 = V_1;
			NullCheck(L_7);
			Enumerator_t3409525828  L_8 = List_1_GetEnumerator_m3866234593(L_7, /*hidden argument*/List_1_GetEnumerator_m3866234593_MethodInfo_var);
			V_3 = L_8;
		}

IL_0037:
		try
		{ // begin try (depth: 2)
			{
				goto IL_004a;
			}

IL_003c:
			{
				FirebaseApp_t210707726 * L_9 = Enumerator_get_Current_m3438154773((&V_3), /*hidden argument*/Enumerator_get_Current_m3438154773_MethodInfo_var);
				V_2 = L_9;
				FirebaseApp_t210707726 * L_10 = V_2;
				NullCheck(L_10);
				FirebaseApp_Dispose_m3098692549(L_10, /*hidden argument*/NULL);
			}

IL_004a:
			{
				bool L_11 = Enumerator_MoveNext_m112670625((&V_3), /*hidden argument*/Enumerator_MoveNext_m112670625_MethodInfo_var);
				if (L_11)
				{
					goto IL_003c;
				}
			}

IL_0056:
			{
				IL2CPP_LEAVE(0x69, FINALLY_005b);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_005b;
		}

FINALLY_005b:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m1826530985((&V_3), /*hidden argument*/Enumerator_Dispose_m1826530985_MethodInfo_var);
			IL2CPP_END_FINALLY(91)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(91)
		{
			IL2CPP_JUMP_TBL(0x69, IL_0069)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_0069:
		{
			IL2CPP_LEAVE(0x75, FINALLY_006e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_006e;
	}

FINALLY_006e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_12 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(110)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(110)
	{
		IL2CPP_JUMP_TBL(0x75, IL_0075)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0075:
	{
		return;
	}
}
// System.Uri Firebase.FirebaseApp::UrlStringToUri(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* UriFormatException_t3682083048_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseApp_UrlStringToUri_m3126757169_MetadataUsageId;
extern "C"  Uri_t19570940 * FirebaseApp_UrlStringToUri_m3126757169 (Il2CppObject * __this /* static, unused */, String_t* ___urlString0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_UrlStringToUri_m3126757169_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Uri_t19570940 * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___urlString0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (Uri_t19570940 *)NULL;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		String_t* L_2 = ___urlString0;
		Uri_t19570940 * L_3 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_3, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0021;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (UriFormatException_t3682083048_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0019;
		throw e;
	}

CATCH_0019:
	{ // begin catch(System.UriFormatException)
		V_0 = (Uri_t19570940 *)NULL;
		goto IL_0021;
	} // end catch (depth: 1)

IL_0021:
	{
		Uri_t19570940 * L_4 = V_0;
		return L_4;
	}
}
// Firebase.FirebaseApp Firebase.FirebaseApp::CreateAndTrack(Firebase.FirebaseApp/CreateDelegate)
extern Il2CppClass* FirebaseHandler_t2907300047_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseApp_t210707726_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern Il2CppClass* ApplicationException_t474868623_il2cpp_TypeInfo_var;
extern Il2CppClass* InitializationException_t1438959983_il2cpp_TypeInfo_var;
extern Il2CppClass* InstallRootCerts_t1106242208_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m3198068170_MethodInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m4186983950_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m2164619886_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m1617273894_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m1952206578_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029336;
extern const uint32_t FirebaseApp_CreateAndTrack_m2952140369_MetadataUsageId;
extern "C"  FirebaseApp_t210707726 * FirebaseApp_CreateAndTrack_m2952140369 (Il2CppObject * __this /* static, unused */, CreateDelegate_t413676709 * ___createDelegate0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_CreateAndTrack_m2952140369_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	FirebaseApp_t210707726 * V_1 = NULL;
	HandleRef_t2419939847  V_2;
	memset(&V_2, 0, sizeof(V_2));
	ApplicationException_t474868623 * V_3 = NULL;
	String_t* V_4 = NULL;
	int32_t V_5 = 0;
	FirebaseApp_t210707726 * V_6 = NULL;
	FirebaseApp_t210707726 * V_7 = NULL;
	int32_t V_8 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
		FirebaseHandler_Create_m3248912600(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		Dictionary_2_t2125486988 * L_0 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_nameToProxy_2();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			HandleRef__ctor_m1370162289((&V_2), NULL, L_2, /*hidden argument*/NULL);
		}

IL_001e:
		try
		{ // begin try (depth: 2)
			{
				CreateDelegate_t413676709 * L_3 = ___createDelegate0;
				NullCheck(L_3);
				FirebaseApp_t210707726 * L_4 = CreateDelegate_Invoke_m3558955498(L_3, /*hidden argument*/NULL);
				V_1 = L_4;
				IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
				bool L_5 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
				if (!L_5)
				{
					goto IL_0035;
				}
			}

IL_002f:
			{
				IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
				Exception_t1927440687 * L_6 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
				IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
			}

IL_0035:
			{
				FirebaseApp_t210707726 * L_7 = V_1;
				if (!L_7)
				{
					goto IL_0042;
				}
			}

IL_003b:
			{
				FirebaseApp_t210707726 * L_8 = V_1;
				IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
				HandleRef_t2419939847  L_9 = FirebaseApp_getCPtr_m4255293038(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
				V_2 = L_9;
			}

IL_0042:
			{
				goto IL_008f;
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1927440687 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (ApplicationException_t474868623_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0047;
			throw e;
		}

CATCH_0047:
		{ // begin catch(System.ApplicationException)
			{
				V_3 = ((ApplicationException_t474868623 *)__exception_local);
				ApplicationException_t474868623 * L_10 = V_3;
				NullCheck(L_10);
				String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_10);
				V_4 = L_11;
				String_t* L_12 = V_4;
				NullCheck(L_12);
				int32_t L_13 = String_IndexOf_m4251815737(L_12, _stringLiteral372029336, /*hidden argument*/NULL);
				V_5 = L_13;
				int32_t L_14 = V_5;
				if ((((int32_t)L_14) < ((int32_t)0)))
				{
					goto IL_0086;
				}
			}

IL_0066:
			{
				String_t* L_15 = V_4;
				int32_t L_16 = V_5;
				NullCheck(L_15);
				String_t* L_17 = String_Substring_m12482732(L_15, 0, L_16, /*hidden argument*/NULL);
				int32_t L_18 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
				String_t* L_19 = V_4;
				int32_t L_20 = V_5;
				NullCheck(L_19);
				String_t* L_21 = String_Substring_m2032624251(L_19, ((int32_t)((int32_t)L_20+(int32_t)1)), /*hidden argument*/NULL);
				InitializationException_t1438959983 * L_22 = (InitializationException_t1438959983 *)il2cpp_codegen_object_new(InitializationException_t1438959983_il2cpp_TypeInfo_var);
				InitializationException__ctor_m1927152695(L_22, L_18, L_21, /*hidden argument*/NULL);
				IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
			}

IL_0086:
			{
				String_t* L_23 = V_4;
				InitializationException_t1438959983 * L_24 = (InitializationException_t1438959983 *)il2cpp_codegen_object_new(InitializationException_t1438959983_il2cpp_TypeInfo_var);
				InitializationException__ctor_m1927152695(L_24, 1, L_23, /*hidden argument*/NULL);
				IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
			}
		} // end catch (depth: 2)

IL_008f:
		{
			IntPtr_t L_25 = HandleRef_get_Handle_m769981143((&V_2), /*hidden argument*/NULL);
			IntPtr_t L_26 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_27 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
			if (!L_27)
			{
				goto IL_00ad;
			}
		}

IL_00a5:
		{
			V_6 = (FirebaseApp_t210707726 *)NULL;
			IL2CPP_LEAVE(0x15E, FINALLY_0157);
		}

IL_00ad:
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
			Dictionary_2_t652733044 * L_28 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_cPtrToProxy_3();
			IntPtr_t L_29 = HandleRef_get_Handle_m769981143((&V_2), /*hidden argument*/NULL);
			NullCheck(L_28);
			bool L_30 = Dictionary_2_TryGetValue_m3198068170(L_28, L_29, (&V_7), /*hidden argument*/Dictionary_2_TryGetValue_m3198068170_MethodInfo_var);
			if (!L_30)
			{
				goto IL_00e5;
			}
		}

IL_00c5:
		{
			FirebaseApp_t210707726 * L_31 = V_1;
			IntPtr_t L_32 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			HandleRef_t2419939847  L_33;
			memset(&L_33, 0, sizeof(L_33));
			HandleRef__ctor_m1370162289(&L_33, NULL, L_32, /*hidden argument*/NULL);
			NullCheck(L_31);
			L_31->set_swigCPtr_0(L_33);
			FirebaseApp_t210707726 * L_34 = V_1;
			NullCheck(L_34);
			FirebaseApp_Dispose_m3098692549(L_34, /*hidden argument*/NULL);
			FirebaseApp_t210707726 * L_35 = V_7;
			V_6 = L_35;
			IL2CPP_LEAVE(0x15E, FINALLY_0157);
		}

IL_00e5:
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
			Dictionary_2_t2513902766 * L_36 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_cPtrRefCount_4();
			IntPtr_t L_37 = HandleRef_get_Handle_m769981143((&V_2), /*hidden argument*/NULL);
			NullCheck(L_36);
			bool L_38 = Dictionary_2_TryGetValue_m4186983950(L_36, L_37, (&V_8), /*hidden argument*/Dictionary_2_TryGetValue_m4186983950_MethodInfo_var);
			if (!L_38)
			{
				goto IL_0108;
			}
		}

IL_00fd:
		{
			int32_t L_39 = V_8;
			V_8 = ((int32_t)((int32_t)L_39+(int32_t)1));
			goto IL_010b;
		}

IL_0108:
		{
			V_8 = 1;
		}

IL_010b:
		{
			FirebaseApp_t210707726 * L_40 = V_1;
			NullCheck(L_40);
			L_40->set_swigCMemOwn_1((bool)1);
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
			Dictionary_2_t652733044 * L_41 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_cPtrToProxy_3();
			IntPtr_t L_42 = HandleRef_get_Handle_m769981143((&V_2), /*hidden argument*/NULL);
			FirebaseApp_t210707726 * L_43 = V_1;
			NullCheck(L_41);
			Dictionary_2_set_Item_m2164619886(L_41, L_42, L_43, /*hidden argument*/Dictionary_2_set_Item_m2164619886_MethodInfo_var);
			Dictionary_2_t2125486988 * L_44 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_nameToProxy_2();
			FirebaseApp_t210707726 * L_45 = V_1;
			NullCheck(L_45);
			String_t* L_46 = FirebaseApp_get_Name_m3737532545(L_45, /*hidden argument*/NULL);
			FirebaseApp_t210707726 * L_47 = V_1;
			NullCheck(L_44);
			Dictionary_2_set_Item_m1617273894(L_44, L_46, L_47, /*hidden argument*/Dictionary_2_set_Item_m1617273894_MethodInfo_var);
			Dictionary_2_t2513902766 * L_48 = ((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->get_cPtrRefCount_4();
			IntPtr_t L_49 = HandleRef_get_Handle_m769981143((&V_2), /*hidden argument*/NULL);
			int32_t L_50 = V_8;
			NullCheck(L_48);
			Dictionary_2_set_Item_m1952206578(L_48, L_49, L_50, /*hidden argument*/Dictionary_2_set_Item_m1952206578_MethodInfo_var);
			FirebaseApp_t210707726 * L_51 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(InstallRootCerts_t1106242208_il2cpp_TypeInfo_var);
			InstallRootCerts_Process_m1398406616(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
			FirebaseApp_t210707726 * L_52 = V_1;
			V_6 = L_52;
			IL2CPP_LEAVE(0x15E, FINALLY_0157);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0157;
	}

FINALLY_0157:
	{ // begin finally (depth: 1)
		Il2CppObject * L_53 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(343)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(343)
	{
		IL2CPP_JUMP_TBL(0x15E, IL_015e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_015e:
	{
		FirebaseApp_t210707726 * L_54 = V_6;
		return L_54;
	}
}
// System.String Firebase.FirebaseApp::get_Name()
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseApp_get_Name_m3737532545_MetadataUsageId;
extern "C"  String_t* FirebaseApp_get_Name_m3737532545 (FirebaseApp_t210707726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_get_Name_m3737532545_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		String_t* L_1 = AppUtilPINVOKE_Firebase_App_FirebaseApp_Name_get_m2545680430(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_3 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// Firebase.AppOptions Firebase.FirebaseApp::get_Options()
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern Il2CppClass* AppOptions_t1641189195_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseApp_get_Options_m763126714_MetadataUsageId;
extern "C"  AppOptions_t1641189195 * FirebaseApp_get_Options_m763126714 (FirebaseApp_t210707726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_get_Options_m763126714_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AppOptions_t1641189195 * V_0 = NULL;
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		IntPtr_t L_1 = AppUtilPINVOKE_Firebase_App_FirebaseApp_Options_get_m2245867877(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		AppOptions_t1641189195 * L_2 = (AppOptions_t1641189195 *)il2cpp_codegen_object_new(AppOptions_t1641189195_il2cpp_TypeInfo_var);
		AppOptions__ctor_m1733148272(L_2, L_1, (bool)0, /*hidden argument*/NULL);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_3 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_4 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0022:
	{
		AppOptions_t1641189195 * L_5 = V_0;
		return L_5;
	}
}
// Firebase.FirebaseApp Firebase.FirebaseApp::CreateInternal()
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseApp_t210707726_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseApp_CreateInternal_m3582107925_MetadataUsageId;
extern "C"  FirebaseApp_t210707726 * FirebaseApp_CreateInternal_m3582107925 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_CreateInternal_m3582107925_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	FirebaseApp_t210707726 * V_1 = NULL;
	FirebaseApp_t210707726 * G_B3_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		IntPtr_t L_0 = AppUtilPINVOKE_Firebase_App_FirebaseApp_CreateInternal__SWIG_0_m2787922779(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		IntPtr_t L_1 = V_0;
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_3 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		G_B3_0 = ((FirebaseApp_t210707726 *)(NULL));
		goto IL_0023;
	}

IL_001c:
	{
		IntPtr_t L_4 = V_0;
		FirebaseApp_t210707726 * L_5 = (FirebaseApp_t210707726 *)il2cpp_codegen_object_new(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		FirebaseApp__ctor_m3662482775(L_5, L_4, (bool)0, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_0023:
	{
		V_1 = G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_6 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0034;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_7 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0034:
	{
		FirebaseApp_t210707726 * L_8 = V_1;
		return L_8;
	}
}
// System.String Firebase.FirebaseApp::get_DefaultName()
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseApp_get_DefaultName_m2031394344_MetadataUsageId;
extern "C"  String_t* FirebaseApp_get_DefaultName_m2031394344 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_get_DefaultName_m2031394344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		String_t* L_0 = AppUtilPINVOKE_Firebase_App_FirebaseApp_DefaultName_get_m4097276820(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_1 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_2 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		String_t* L_3 = V_0;
		return L_3;
	}
}
// System.Void Firebase.FirebaseApp::.cctor()
extern Il2CppClass* Dictionary_2_t2125486988_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseApp_t210707726_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t652733044_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2513902766_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2836329251_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1865841403_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m186159185_MethodInfo_var;
extern const uint32_t FirebaseApp__cctor_m2391020073_MetadataUsageId;
extern "C"  void FirebaseApp__cctor_m2391020073 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp__cctor_m2391020073_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2125486988 * L_0 = (Dictionary_2_t2125486988 *)il2cpp_codegen_object_new(Dictionary_2_t2125486988_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2836329251(L_0, /*hidden argument*/Dictionary_2__ctor_m2836329251_MethodInfo_var);
		((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->set_nameToProxy_2(L_0);
		Dictionary_2_t652733044 * L_1 = (Dictionary_2_t652733044 *)il2cpp_codegen_object_new(Dictionary_2_t652733044_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1865841403(L_1, /*hidden argument*/Dictionary_2__ctor_m1865841403_MethodInfo_var);
		((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->set_cPtrToProxy_3(L_1);
		Dictionary_2_t2513902766 * L_2 = (Dictionary_2_t2513902766 *)il2cpp_codegen_object_new(Dictionary_2_t2513902766_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m186159185(L_2, /*hidden argument*/Dictionary_2__ctor_m186159185_MethodInfo_var);
		((FirebaseApp_t210707726_StaticFields*)FirebaseApp_t210707726_il2cpp_TypeInfo_var->static_fields)->set_cPtrRefCount_4(L_2);
		return;
	}
}
// System.Void Firebase.FirebaseApp::<Dispose>m__0()
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseApp_U3CDisposeU3Em__0_m3004522492_MetadataUsageId;
extern "C"  void FirebaseApp_U3CDisposeU3Em__0_m3004522492 (FirebaseApp_t210707726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_U3CDisposeU3Em__0_m3004522492_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		AppUtilPINVOKE_Firebase_App_delete_FirebaseApp_m1137114670(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// Firebase.FirebaseApp Firebase.FirebaseApp::<Create>m__1()
extern Il2CppClass* FirebaseApp_t210707726_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseApp_U3CCreateU3Em__1_m64761912_MetadataUsageId;
extern "C"  FirebaseApp_t210707726 * FirebaseApp_U3CCreateU3Em__1_m64761912 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseApp_U3CCreateU3Em__1_m64761912_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		FirebaseApp_t210707726 * L_0 = FirebaseApp_CreateInternal_m3582107925(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Firebase.FirebaseApp/CreateDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void CreateDelegate__ctor_m3440641418 (CreateDelegate_t413676709 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// Firebase.FirebaseApp Firebase.FirebaseApp/CreateDelegate::Invoke()
extern "C"  FirebaseApp_t210707726 * CreateDelegate_Invoke_m3558955498 (CreateDelegate_t413676709 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CreateDelegate_Invoke_m3558955498((CreateDelegate_t413676709 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef FirebaseApp_t210707726 * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef FirebaseApp_t210707726 * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Firebase.FirebaseApp/CreateDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CreateDelegate_BeginInvoke_m3861139009 (CreateDelegate_t413676709 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// Firebase.FirebaseApp Firebase.FirebaseApp/CreateDelegate::EndInvoke(System.IAsyncResult)
extern "C"  FirebaseApp_t210707726 * CreateDelegate_EndInvoke_m3008445990 (CreateDelegate_t413676709 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (FirebaseApp_t210707726 *)__result;
}
// System.Void Firebase.FirebaseApp/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void DestroyDelegate__ctor_m984049708 (DestroyDelegate_t3635929227 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.FirebaseApp/DestroyDelegate::Invoke()
extern "C"  void DestroyDelegate_Invoke_m3785470502 (DestroyDelegate_t3635929227 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DestroyDelegate_Invoke_m3785470502((DestroyDelegate_t3635929227 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_DestroyDelegate_t3635929227 (DestroyDelegate_t3635929227 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult Firebase.FirebaseApp/DestroyDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DestroyDelegate_BeginInvoke_m3002495555 (DestroyDelegate_t3635929227 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void Firebase.FirebaseApp/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void DestroyDelegate_EndInvoke_m1272627102 (DestroyDelegate_t3635929227 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Firebase.FirebaseApp/FirebaseHandler::.ctor()
extern "C"  void FirebaseHandler__ctor_m1949563562 (FirebaseHandler_t2907300047 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.FirebaseApp/FirebaseHandler::Create()
extern const Il2CppType* FirebaseHandler_t2907300047_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseHandler_t2907300047_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* LogMessageDelegate_t1988210674_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var;
extern const MethodInfo* FirebaseHandler_LogMessage_m1734897910_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisFirebaseHandler_t2907300047_m3196704967_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1742625763;
extern const uint32_t FirebaseHandler_Create_m3248912600_MetadataUsageId;
extern "C"  void FirebaseHandler_Create_m3248912600 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseHandler_Create_m3248912600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(FirebaseHandler_t2907300047_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
			GameObject_t1756533147 * L_2 = ((FirebaseHandler_t2907300047_StaticFields*)FirebaseHandler_t2907300047_il2cpp_TypeInfo_var->static_fields)->get_firebaseHandler_2();
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
			if (!L_3)
			{
				goto IL_0026;
			}
		}

IL_0021:
		{
			IL2CPP_LEAVE(0x9A, FINALLY_0093);
		}

IL_0026:
		{
			int32_t L_4 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((int32_t)L_4) == ((int32_t)((int32_t)11))))
			{
				goto IL_0038;
			}
		}

IL_0032:
		{
			AppUtil_SetEnabledAllAppCallbacks_m2417423293(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		}

IL_0038:
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
			LogMessageDelegate_t1988210674 * L_5 = ((FirebaseHandler_t2907300047_StaticFields*)FirebaseHandler_t2907300047_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_3();
			if (L_5)
			{
				goto IL_0050;
			}
		}

IL_003f:
		{
			IntPtr_t L_6;
			L_6.set_m_value_0((void*)(void*)FirebaseHandler_LogMessage_m1734897910_MethodInfo_var);
			LogMessageDelegate_t1988210674 * L_7 = (LogMessageDelegate_t1988210674 *)il2cpp_codegen_object_new(LogMessageDelegate_t1988210674_il2cpp_TypeInfo_var);
			LogMessageDelegate__ctor_m1146820131(L_7, NULL, L_6, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
			((FirebaseHandler_t2907300047_StaticFields*)FirebaseHandler_t2907300047_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache0_3(L_7);
		}

IL_0050:
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
			LogMessageDelegate_t1988210674 * L_8 = ((FirebaseHandler_t2907300047_StaticFields*)FirebaseHandler_t2907300047_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_3();
			AppUtil_SetLogFunction_m1947305073(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			AppUtil_AppEnableLogCallback_m1418645513(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_9 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
			GameObject__ctor_m962601984(L_9, _stringLiteral1742625763, /*hidden argument*/NULL);
			((FirebaseHandler_t2907300047_StaticFields*)FirebaseHandler_t2907300047_il2cpp_TypeInfo_var->static_fields)->set_firebaseHandler_2(L_9);
			GameObject_t1756533147 * L_10 = ((FirebaseHandler_t2907300047_StaticFields*)FirebaseHandler_t2907300047_il2cpp_TypeInfo_var->static_fields)->get_firebaseHandler_2();
			NullCheck(L_10);
			GameObject_AddComponent_TisFirebaseHandler_t2907300047_m3196704967(L_10, /*hidden argument*/GameObject_AddComponent_TisFirebaseHandler_t2907300047_m3196704967_MethodInfo_var);
			GameObject_t1756533147 * L_11 = ((FirebaseHandler_t2907300047_StaticFields*)FirebaseHandler_t2907300047_il2cpp_TypeInfo_var->static_fields)->get_firebaseHandler_2();
			IL2CPP_RUNTIME_CLASS_INIT(UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var);
			UnitySynchronizationContext_Install_m3862032013(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_12 = ((FirebaseHandler_t2907300047_StaticFields*)FirebaseHandler_t2907300047_il2cpp_TypeInfo_var->static_fields)->get_firebaseHandler_2();
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x9A, FINALLY_0093);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0093;
	}

FINALLY_0093:
	{ // begin finally (depth: 1)
		Il2CppObject * L_13 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(147)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(147)
	{
		IL2CPP_JUMP_TBL(0x9A, IL_009a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_009a:
	{
		return;
	}
}
// System.Void Firebase.FirebaseApp/FirebaseHandler::LogMessage(Firebase.LogLevel,System.String)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseHandler_LogMessage_m1734897910_MetadataUsageId;
extern "C"  void FirebaseHandler_LogMessage_m1734897910 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, String_t* ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseHandler_LogMessage_m1734897910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = AppUtil_AppGetLogLevel_m2540188292(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = ___logLevel0;
		int32_t L_2 = V_0;
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = ___logLevel0;
		if (L_3 == 0)
		{
			goto IL_0031;
		}
		if (L_3 == 1)
		{
			goto IL_0031;
		}
		if (L_3 == 2)
		{
			goto IL_0031;
		}
		if (L_3 == 3)
		{
			goto IL_003c;
		}
		if (L_3 == 4)
		{
			goto IL_0047;
		}
		if (L_3 == 5)
		{
			goto IL_0052;
		}
	}
	{
		goto IL_0057;
	}

IL_0031:
	{
		String_t* L_4 = ___message1;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_003c:
	{
		String_t* L_5 = ___message1;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_0047:
	{
		String_t* L_6 = ___message1;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_0052:
	{
		goto IL_0057;
	}

IL_0057:
	{
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_FirebaseHandler_LogMessage_m1734897910(int32_t ___logLevel0, char* ___message1)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message1' to managed representation
	String_t* ____message1_unmarshaled = NULL;
	____message1_unmarshaled = il2cpp_codegen_marshal_string_result(___message1);

	// Managed method invocation
	::FirebaseHandler_LogMessage_m1734897910(NULL, ___logLevel0, ____message1_unmarshaled, NULL);

}
// System.Void Firebase.FirebaseApp/FirebaseHandler::Update()
extern "C"  void FirebaseHandler_Update_m164025069 (FirebaseHandler_t2907300047 * __this, const MethodInfo* method)
{
	{
		AppUtil_PollCallbacks_m1948862416(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.FirebaseApp/FirebaseHandler::OnApplicationQuit()
extern Il2CppClass* FirebaseHandler_t2907300047_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseApp_t210707726_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseHandler_OnApplicationQuit_m2676139308_MetadataUsageId;
extern "C"  void FirebaseHandler_OnApplicationQuit_m2676139308 (FirebaseHandler_t2907300047 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseHandler_OnApplicationQuit_m2676139308_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
		((FirebaseHandler_t2907300047_StaticFields*)FirebaseHandler_t2907300047_il2cpp_TypeInfo_var->static_fields)->set_firebaseHandler_2((GameObject_t1756533147 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		FirebaseApp_EmptyAppDictionaries_m4102170024(NULL /*static, unused*/, /*hidden argument*/NULL);
		AppUtil_AppEnableLogCallback_m1418645513(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		AppUtil_SetLogFunction_m1947305073(NULL /*static, unused*/, (LogMessageDelegate_t1988210674 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.FirebaseApp/FirebaseHandler::.cctor()
extern "C"  void FirebaseHandler__cctor_m901621883 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void LogMessageDelegate__ctor_m1146820131 (LogMessageDelegate_t1988210674 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate::Invoke(Firebase.LogLevel,System.String)
extern "C"  void LogMessageDelegate_Invoke_m495697698 (LogMessageDelegate_t1988210674 * __this, int32_t ___log_level0, String_t* ___message1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LogMessageDelegate_Invoke_m495697698((LogMessageDelegate_t1988210674 *)__this->get_prev_9(),___log_level0, ___message1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___log_level0, String_t* ___message1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___log_level0, ___message1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___log_level0, String_t* ___message1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___log_level0, ___message1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_LogMessageDelegate_t1988210674 (LogMessageDelegate_t1988210674 * __this, int32_t ___log_level0, String_t* ___message1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___message1' to native representation
	char* ____message1_marshaled = NULL;
	____message1_marshaled = il2cpp_codegen_marshal_string(___message1);

	// Native function invocation
	il2cppPInvokeFunc(___log_level0, ____message1_marshaled);

	// Marshaling cleanup of parameter '___message1' native representation
	il2cpp_codegen_marshal_free(____message1_marshaled);
	____message1_marshaled = NULL;

}
// System.IAsyncResult Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate::BeginInvoke(Firebase.LogLevel,System.String,System.AsyncCallback,System.Object)
extern Il2CppClass* LogLevel_t543421840_il2cpp_TypeInfo_var;
extern const uint32_t LogMessageDelegate_BeginInvoke_m71829407_MetadataUsageId;
extern "C"  Il2CppObject * LogMessageDelegate_BeginInvoke_m71829407 (LogMessageDelegate_t1988210674 * __this, int32_t ___log_level0, String_t* ___message1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LogMessageDelegate_BeginInvoke_m71829407_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(LogLevel_t543421840_il2cpp_TypeInfo_var, &___log_level0);
	__d_args[1] = ___message1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void LogMessageDelegate_EndInvoke_m368326581 (LogMessageDelegate_t1988210674 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Firebase.FirebaseApp/FirebaseHandler/MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern "C"  void MonoPInvokeCallbackAttribute__ctor_m287189802 (MonoPInvokeCallbackAttribute_t323097478 * __this, Type_t * ___t0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.FirebaseException::.ctor(System.Int32,System.String)
extern "C"  void FirebaseException__ctor_m3734642555 (FirebaseException_t2567272216 * __this, int32_t ___errorCode0, String_t* ___message1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message1;
		Exception__ctor_m485833136(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___errorCode0;
		FirebaseException_set_ErrorCode_m1261295849(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.FirebaseException::set_ErrorCode(System.Int32)
extern "C"  void FirebaseException_set_ErrorCode_m1261295849 (FirebaseException_t2567272216 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CErrorCodeU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Void Firebase.FutureBase::.ctor(System.IntPtr,System.Boolean)
extern "C"  void FutureBase__ctor_m3054944159 (FutureBase_t2698306134 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		bool L_0 = ___cMemoryOwn1;
		__this->set_swigCMemOwn_1(L_0);
		IntPtr_t L_1 = ___cPtr0;
		HandleRef_t2419939847  L_2;
		memset(&L_2, 0, sizeof(L_2));
		HandleRef__ctor_m1370162289(&L_2, __this, L_1, /*hidden argument*/NULL);
		__this->set_swigCPtr_0(L_2);
		return;
	}
}
// System.Void Firebase.FutureBase::Finalize()
extern "C"  void FutureBase_Finalize_m1464906966 (FutureBase_t2698306134 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker0::Invoke(5 /* System.Void Firebase.FutureBase::Dispose() */, __this);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void Firebase.FutureBase::Dispose()
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern const uint32_t FutureBase_Dispose_m1042966601_MetadataUsageId;
extern "C"  void FutureBase_Dispose_m1042966601 (FutureBase_t2698306134 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FutureBase_Dispose_m1042966601_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = __this;
		Il2CppObject * L_0 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			HandleRef_t2419939847 * L_1 = __this->get_address_of_swigCPtr_0();
			IntPtr_t L_2 = HandleRef_get_Handle_m769981143(L_1, /*hidden argument*/NULL);
			IntPtr_t L_3 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_4 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
			if (!L_4)
			{
				goto IL_0050;
			}
		}

IL_0022:
		{
			bool L_5 = __this->get_swigCMemOwn_1();
			if (!L_5)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			__this->set_swigCMemOwn_1((bool)0);
			HandleRef_t2419939847  L_6 = __this->get_swigCPtr_0();
			IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
			AppUtilPINVOKE_Firebase_App_delete_FutureBase_m3328440028(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		}

IL_003f:
		{
			IntPtr_t L_7 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			HandleRef_t2419939847  L_8;
			memset(&L_8, 0, sizeof(L_8));
			HandleRef__ctor_m1370162289(&L_8, NULL, L_7, /*hidden argument*/NULL);
			__this->set_swigCPtr_0(L_8);
		}

IL_0050:
		{
			GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x62, FINALLY_005b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_005b;
	}

FINALLY_005b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_9 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(91)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(91)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0062:
	{
		return;
	}
}
// Firebase.FutureStatus Firebase.FutureBase::Status()
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t FutureBase_Status_m543620527_MetadataUsageId;
extern "C"  int32_t FutureBase_Status_m543620527 (FutureBase_t2698306134 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FutureBase_Status_m543620527_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		int32_t L_1 = AppUtilPINVOKE_Firebase_App_FutureBase_Status_m3094756663(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_3 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.Int32 Firebase.FutureBase::Error()
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t FutureBase_Error_m954039370_MetadataUsageId;
extern "C"  int32_t FutureBase_Error_m954039370 (FutureBase_t2698306134 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FutureBase_Error_m954039370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		int32_t L_1 = AppUtilPINVOKE_Firebase_App_FutureBase_Error_m1475679819(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_3 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.String Firebase.FutureBase::ErrorMessage()
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t FutureBase_ErrorMessage_m11232826_MetadataUsageId;
extern "C"  String_t* FutureBase_ErrorMessage_m11232826 (FutureBase_t2698306134 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FutureBase_ErrorMessage_m11232826_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		String_t* L_1 = AppUtilPINVOKE_Firebase_App_FutureBase_ErrorMessage_m1466285231(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_3 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.Void Firebase.FutureString::.ctor(System.IntPtr,System.Boolean)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern const uint32_t FutureString__ctor_m1515554591_MetadataUsageId;
extern "C"  void FutureString__ctor_m1515554591 (FutureString_t4225986006 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FutureString__ctor_m1515554591_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_callbackData_6(L_0);
		IntPtr_t L_1 = ___cPtr0;
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		IntPtr_t L_2 = AppUtilPINVOKE_Firebase_App_FutureString_SWIGUpcast_m1098833757(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		bool L_3 = ___cMemoryOwn1;
		FutureBase__ctor_m3054944159(__this, L_2, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4 = ___cPtr0;
		HandleRef_t2419939847  L_5;
		memset(&L_5, 0, sizeof(L_5));
		HandleRef__ctor_m1370162289(&L_5, __this, L_4, /*hidden argument*/NULL);
		__this->set_swigCPtr_2(L_5);
		return;
	}
}
// System.Void Firebase.FutureString::Finalize()
extern "C"  void FutureString_Finalize_m4198862634 (FutureString_t4225986006 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker0::Invoke(5 /* System.Void Firebase.FutureBase::Dispose() */, __this);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		FutureBase_Finalize_m1464906966(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void Firebase.FutureString::Dispose()
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern const uint32_t FutureString_Dispose_m37735597_MetadataUsageId;
extern "C"  void FutureString_Dispose_m37735597 (FutureString_t4225986006 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FutureString_Dispose_m37735597_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = __this;
		Il2CppObject * L_0 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			HandleRef_t2419939847 * L_1 = __this->get_address_of_swigCPtr_2();
			IntPtr_t L_2 = HandleRef_get_Handle_m769981143(L_1, /*hidden argument*/NULL);
			IntPtr_t L_3 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_4 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
			if (!L_4)
			{
				goto IL_0050;
			}
		}

IL_0022:
		{
			bool L_5 = ((FutureBase_t2698306134 *)__this)->get_swigCMemOwn_1();
			if (!L_5)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			((FutureBase_t2698306134 *)__this)->set_swigCMemOwn_1((bool)0);
			HandleRef_t2419939847  L_6 = __this->get_swigCPtr_2();
			IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
			AppUtilPINVOKE_Firebase_App_delete_FutureString_m298034936(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		}

IL_003f:
		{
			IntPtr_t L_7 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			HandleRef_t2419939847  L_8;
			memset(&L_8, 0, sizeof(L_8));
			HandleRef__ctor_m1370162289(&L_8, NULL, L_7, /*hidden argument*/NULL);
			__this->set_swigCPtr_2(L_8);
		}

IL_0050:
		{
			IntPtr_t L_9 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			FutureString_SetCompletionData_m3159764978(__this, L_9, /*hidden argument*/NULL);
			GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
			FutureBase_Dispose_m1042966601(__this, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x73, FINALLY_006c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_006c;
	}

FINALLY_006c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_10 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(108)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(108)
	{
		IL2CPP_JUMP_TBL(0x73, IL_0073)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0073:
	{
		return;
	}
}
// System.Threading.Tasks.Task`1<System.String> Firebase.FutureString::GetTask(Firebase.FutureString)
extern Il2CppClass* U3CGetTaskU3Ec__AnonStorey0_t4099292678_il2cpp_TypeInfo_var;
extern Il2CppClass* TaskCompletionSource_1_t911654313_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseException_t2567272216_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3062064994_il2cpp_TypeInfo_var;
extern const MethodInfo* TaskCompletionSource_1__ctor_m2461165479_MethodInfo_var;
extern const MethodInfo* TaskCompletionSource_1_SetException_m3451240888_MethodInfo_var;
extern const MethodInfo* TaskCompletionSource_1_get_Task_m3400202330_MethodInfo_var;
extern const MethodInfo* U3CGetTaskU3Ec__AnonStorey0_U3CU3Em__0_m4067005652_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1074014380;
extern const uint32_t FutureString_GetTask_m1388189510_MetadataUsageId;
extern "C"  Task_1_t1149249240 * FutureString_GetTask_m1388189510 (Il2CppObject * __this /* static, unused */, FutureString_t4225986006 * ___fu0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FutureString_GetTask_m1388189510_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetTaskU3Ec__AnonStorey0_t4099292678 * V_0 = NULL;
	{
		U3CGetTaskU3Ec__AnonStorey0_t4099292678 * L_0 = (U3CGetTaskU3Ec__AnonStorey0_t4099292678 *)il2cpp_codegen_object_new(U3CGetTaskU3Ec__AnonStorey0_t4099292678_il2cpp_TypeInfo_var);
		U3CGetTaskU3Ec__AnonStorey0__ctor_m3043421459(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetTaskU3Ec__AnonStorey0_t4099292678 * L_1 = V_0;
		FutureString_t4225986006 * L_2 = ___fu0;
		NullCheck(L_1);
		L_1->set_fu_0(L_2);
		U3CGetTaskU3Ec__AnonStorey0_t4099292678 * L_3 = V_0;
		TaskCompletionSource_1_t911654313 * L_4 = (TaskCompletionSource_1_t911654313 *)il2cpp_codegen_object_new(TaskCompletionSource_1_t911654313_il2cpp_TypeInfo_var);
		TaskCompletionSource_1__ctor_m2461165479(L_4, /*hidden argument*/TaskCompletionSource_1__ctor_m2461165479_MethodInfo_var);
		NullCheck(L_3);
		L_3->set_tcs_1(L_4);
		U3CGetTaskU3Ec__AnonStorey0_t4099292678 * L_5 = V_0;
		NullCheck(L_5);
		FutureString_t4225986006 * L_6 = L_5->get_fu_0();
		NullCheck(L_6);
		int32_t L_7 = FutureBase_Status_m543620527(L_6, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)2))))
		{
			goto IL_004b;
		}
	}
	{
		U3CGetTaskU3Ec__AnonStorey0_t4099292678 * L_8 = V_0;
		NullCheck(L_8);
		TaskCompletionSource_1_t911654313 * L_9 = L_8->get_tcs_1();
		FirebaseException_t2567272216 * L_10 = (FirebaseException_t2567272216 *)il2cpp_codegen_object_new(FirebaseException_t2567272216_il2cpp_TypeInfo_var);
		FirebaseException__ctor_m3734642555(L_10, 0, _stringLiteral1074014380, /*hidden argument*/NULL);
		NullCheck(L_9);
		TaskCompletionSource_1_SetException_m3451240888(L_9, L_10, /*hidden argument*/TaskCompletionSource_1_SetException_m3451240888_MethodInfo_var);
		U3CGetTaskU3Ec__AnonStorey0_t4099292678 * L_11 = V_0;
		NullCheck(L_11);
		TaskCompletionSource_1_t911654313 * L_12 = L_11->get_tcs_1();
		NullCheck(L_12);
		Task_1_t1149249240 * L_13 = TaskCompletionSource_1_get_Task_m3400202330(L_12, /*hidden argument*/TaskCompletionSource_1_get_Task_m3400202330_MethodInfo_var);
		return L_13;
	}

IL_004b:
	{
		U3CGetTaskU3Ec__AnonStorey0_t4099292678 * L_14 = V_0;
		NullCheck(L_14);
		FutureString_t4225986006 * L_15 = L_14->get_fu_0();
		U3CGetTaskU3Ec__AnonStorey0_t4099292678 * L_16 = V_0;
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)U3CGetTaskU3Ec__AnonStorey0_U3CU3Em__0_m4067005652_MethodInfo_var);
		Action_t3062064994 * L_18 = (Action_t3062064994 *)il2cpp_codegen_object_new(Action_t3062064994_il2cpp_TypeInfo_var);
		Action__ctor_m3716776567(L_18, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_15);
		FutureString_SetOnCompletionCallback_m1709809508(L_15, L_18, /*hidden argument*/NULL);
		U3CGetTaskU3Ec__AnonStorey0_t4099292678 * L_19 = V_0;
		NullCheck(L_19);
		TaskCompletionSource_1_t911654313 * L_20 = L_19->get_tcs_1();
		NullCheck(L_20);
		Task_1_t1149249240 * L_21 = TaskCompletionSource_1_get_Task_m3400202330(L_20, /*hidden argument*/TaskCompletionSource_1_get_Task_m3400202330_MethodInfo_var);
		return L_21;
	}
}
// System.Void Firebase.FutureString::SetOnCompletionCallback(Firebase.FutureString/Action)
extern Il2CppClass* SWIG_CompletionDelegate_t1819656562_il2cpp_TypeInfo_var;
extern Il2CppClass* FutureString_t4225986006_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2069890629_il2cpp_TypeInfo_var;
extern const MethodInfo* FutureString_SWIG_CompletionDispatcher_m2853657061_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m426178845_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m2114566632_MethodInfo_var;
extern const uint32_t FutureString_SetOnCompletionCallback_m1709809508_MetadataUsageId;
extern "C"  void FutureString_SetOnCompletionCallback_m1709809508 (FutureString_t4225986006 * __this, Action_t3062064994 * ___userCompletionCallback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FutureString_SetOnCompletionCallback_m1709809508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SWIG_CompletionDelegate_t1819656562 * L_0 = __this->get_SWIG_CompletionCB_7();
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)FutureString_SWIG_CompletionDispatcher_m2853657061_MethodInfo_var);
		SWIG_CompletionDelegate_t1819656562 * L_2 = (SWIG_CompletionDelegate_t1819656562 *)il2cpp_codegen_object_new(SWIG_CompletionDelegate_t1819656562_il2cpp_TypeInfo_var);
		SWIG_CompletionDelegate__ctor_m3991997779(L_2, NULL, L_1, /*hidden argument*/NULL);
		__this->set_SWIG_CompletionCB_7(L_2);
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FutureString_t4225986006_il2cpp_TypeInfo_var);
		Il2CppObject * L_3 = ((FutureString_t4225986006_StaticFields*)FutureString_t4225986006_il2cpp_TypeInfo_var->static_fields)->get_CallbackLock_5();
		V_1 = L_3;
		Il2CppObject * L_4 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(FutureString_t4225986006_il2cpp_TypeInfo_var);
			Dictionary_2_t2069890629 * L_5 = ((FutureString_t4225986006_StaticFields*)FutureString_t4225986006_il2cpp_TypeInfo_var->static_fields)->get_Callbacks_3();
			if (L_5)
			{
				goto IL_003d;
			}
		}

IL_0033:
		{
			Dictionary_2_t2069890629 * L_6 = (Dictionary_2_t2069890629 *)il2cpp_codegen_object_new(Dictionary_2_t2069890629_il2cpp_TypeInfo_var);
			Dictionary_2__ctor_m426178845(L_6, /*hidden argument*/Dictionary_2__ctor_m426178845_MethodInfo_var);
			IL2CPP_RUNTIME_CLASS_INIT(FutureString_t4225986006_il2cpp_TypeInfo_var);
			((FutureString_t4225986006_StaticFields*)FutureString_t4225986006_il2cpp_TypeInfo_var->static_fields)->set_Callbacks_3(L_6);
		}

IL_003d:
		{
			IL2CPP_RUNTIME_CLASS_INIT(FutureString_t4225986006_il2cpp_TypeInfo_var);
			int32_t L_7 = ((FutureString_t4225986006_StaticFields*)FutureString_t4225986006_il2cpp_TypeInfo_var->static_fields)->get_CallbackIndex_4();
			int32_t L_8 = ((int32_t)((int32_t)L_7+(int32_t)1));
			((FutureString_t4225986006_StaticFields*)FutureString_t4225986006_il2cpp_TypeInfo_var->static_fields)->set_CallbackIndex_4(L_8);
			V_0 = L_8;
			Dictionary_2_t2069890629 * L_9 = ((FutureString_t4225986006_StaticFields*)FutureString_t4225986006_il2cpp_TypeInfo_var->static_fields)->get_Callbacks_3();
			int32_t L_10 = V_0;
			Action_t3062064994 * L_11 = ___userCompletionCallback0;
			NullCheck(L_9);
			Dictionary_2_set_Item_m2114566632(L_9, L_10, L_11, /*hidden argument*/Dictionary_2_set_Item_m2114566632_MethodInfo_var);
			IL2CPP_LEAVE(0x63, FINALLY_005c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_005c;
	}

FINALLY_005c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_12 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(92)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(92)
	{
		IL2CPP_JUMP_TBL(0x63, IL_0063)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0063:
	{
		SWIG_CompletionDelegate_t1819656562 * L_13 = __this->get_SWIG_CompletionCB_7();
		int32_t L_14 = V_0;
		IntPtr_t L_15 = FutureString_SWIG_OnCompletion_m3612574184(__this, L_13, L_14, /*hidden argument*/NULL);
		FutureString_SetCompletionData_m3159764978(__this, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.FutureString::SetCompletionData(System.IntPtr)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t FutureString_SetCompletionData_m3159764978_MetadataUsageId;
extern "C"  void FutureString_SetCompletionData_m3159764978 (FutureString_t4225986006 * __this, IntPtr_t ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FutureString_SetCompletionData_m3159764978_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_callbackData_6();
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		IntPtr_t L_3 = __this->get_callbackData_6();
		FutureString_SWIG_FreeCompletionData_m279119313(__this, L_3, /*hidden argument*/NULL);
	}

IL_0021:
	{
		IntPtr_t L_4 = ___data0;
		__this->set_callbackData_6(L_4);
		return;
	}
}
// System.Void Firebase.FutureString::SWIG_CompletionDispatcher(System.Int32)
extern Il2CppClass* FutureString_t4225986006_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m2508335028_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Remove_m956931612_MethodInfo_var;
extern const uint32_t FutureString_SWIG_CompletionDispatcher_m2853657061_MetadataUsageId;
extern "C"  void FutureString_SWIG_CompletionDispatcher_m2853657061 (Il2CppObject * __this /* static, unused */, int32_t ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FutureString_SWIG_CompletionDispatcher_m2853657061_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_t3062064994 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (Action_t3062064994 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(FutureString_t4225986006_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((FutureString_t4225986006_StaticFields*)FutureString_t4225986006_il2cpp_TypeInfo_var->static_fields)->get_CallbackLock_5();
		V_1 = L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(FutureString_t4225986006_il2cpp_TypeInfo_var);
			Dictionary_2_t2069890629 * L_2 = ((FutureString_t4225986006_StaticFields*)FutureString_t4225986006_il2cpp_TypeInfo_var->static_fields)->get_Callbacks_3();
			if (!L_2)
			{
				goto IL_0036;
			}
		}

IL_0018:
		{
			IL2CPP_RUNTIME_CLASS_INIT(FutureString_t4225986006_il2cpp_TypeInfo_var);
			Dictionary_2_t2069890629 * L_3 = ((FutureString_t4225986006_StaticFields*)FutureString_t4225986006_il2cpp_TypeInfo_var->static_fields)->get_Callbacks_3();
			int32_t L_4 = ___key0;
			NullCheck(L_3);
			bool L_5 = Dictionary_2_TryGetValue_m2508335028(L_3, L_4, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m2508335028_MethodInfo_var);
			if (!L_5)
			{
				goto IL_0036;
			}
		}

IL_002a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(FutureString_t4225986006_il2cpp_TypeInfo_var);
			Dictionary_2_t2069890629 * L_6 = ((FutureString_t4225986006_StaticFields*)FutureString_t4225986006_il2cpp_TypeInfo_var->static_fields)->get_Callbacks_3();
			int32_t L_7 = ___key0;
			NullCheck(L_6);
			Dictionary_2_Remove_m956931612(L_6, L_7, /*hidden argument*/Dictionary_2_Remove_m956931612_MethodInfo_var);
		}

IL_0036:
		{
			IL2CPP_LEAVE(0x42, FINALLY_003b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_003b;
	}

FINALLY_003b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(59)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(59)
	{
		IL2CPP_JUMP_TBL(0x42, IL_0042)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0042:
	{
		Action_t3062064994 * L_9 = V_0;
		if (!L_9)
		{
			goto IL_004e;
		}
	}
	{
		Action_t3062064994 * L_10 = V_0;
		NullCheck(L_10);
		Action_Invoke_m4105640499(L_10, /*hidden argument*/NULL);
	}

IL_004e:
	{
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_FutureString_SWIG_CompletionDispatcher_m2853657061(int32_t ___key0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Managed method invocation
	::FutureString_SWIG_CompletionDispatcher_m2853657061(NULL, ___key0, NULL);

}
// System.IntPtr Firebase.FutureString::SWIG_OnCompletion(Firebase.FutureString/SWIG_CompletionDelegate,System.Int32)
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t FutureString_SWIG_OnCompletion_m3612574184_MetadataUsageId;
extern "C"  IntPtr_t FutureString_SWIG_OnCompletion_m3612574184 (FutureString_t4225986006 * __this, SWIG_CompletionDelegate_t1819656562 * ___cs_callback0, int32_t ___cs_key1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FutureString_SWIG_OnCompletion_m3612574184_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_2();
		SWIG_CompletionDelegate_t1819656562 * L_1 = ___cs_callback0;
		int32_t L_2 = ___cs_key1;
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		IntPtr_t L_3 = AppUtilPINVOKE_Firebase_App_FutureString_SWIG_OnCompletion_m2011264479(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_4 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_5 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_001e:
	{
		IntPtr_t L_6 = V_0;
		return L_6;
	}
}
// System.Void Firebase.FutureString::SWIG_FreeCompletionData(System.IntPtr)
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t FutureString_SWIG_FreeCompletionData_m279119313_MetadataUsageId;
extern "C"  void FutureString_SWIG_FreeCompletionData_m279119313 (FutureString_t4225986006 * __this, IntPtr_t ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FutureString_SWIG_FreeCompletionData_m279119313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_2();
		IntPtr_t L_1 = ___data0;
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		AppUtilPINVOKE_Firebase_App_FutureString_SWIG_FreeCompletionData_m628326608(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_3 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		return;
	}
}
// System.String Firebase.FutureString::Result()
extern Il2CppClass* AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t FutureString_Result_m2875035570_MetadataUsageId;
extern "C"  String_t* FutureString_Result_m2875035570 (FutureString_t4225986006 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FutureString_Result_m2875035570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_2();
		IL2CPP_RUNTIME_CLASS_INIT(AppUtilPINVOKE_t3515465473_il2cpp_TypeInfo_var);
		String_t* L_1 = AppUtilPINVOKE_Firebase_App_FutureString_Result_m1458338797(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_3 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.Void Firebase.FutureString::.cctor()
extern Il2CppClass* FutureString_t4225986006_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t FutureString__cctor_m3703236793_MetadataUsageId;
extern "C"  void FutureString__cctor_m3703236793 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FutureString__cctor_m3703236793_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((FutureString_t4225986006_StaticFields*)FutureString_t4225986006_il2cpp_TypeInfo_var->static_fields)->set_CallbackIndex_4(0);
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		((FutureString_t4225986006_StaticFields*)FutureString_t4225986006_il2cpp_TypeInfo_var->static_fields)->set_CallbackLock_5(L_0);
		return;
	}
}
// System.Void Firebase.FutureString/<GetTask>c__AnonStorey0::.ctor()
extern "C"  void U3CGetTaskU3Ec__AnonStorey0__ctor_m3043421459 (U3CGetTaskU3Ec__AnonStorey0_t4099292678 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.FutureString/<GetTask>c__AnonStorey0::<>m__0()
extern Il2CppClass* FirebaseException_t2567272216_il2cpp_TypeInfo_var;
extern const MethodInfo* TaskCompletionSource_1_SetCanceled_m2886845820_MethodInfo_var;
extern const MethodInfo* TaskCompletionSource_1_SetException_m3451240888_MethodInfo_var;
extern const MethodInfo* TaskCompletionSource_1_SetResult_m539871673_MethodInfo_var;
extern const uint32_t U3CGetTaskU3Ec__AnonStorey0_U3CU3Em__0_m4067005652_MetadataUsageId;
extern "C"  void U3CGetTaskU3Ec__AnonStorey0_U3CU3Em__0_m4067005652 (U3CGetTaskU3Ec__AnonStorey0_t4099292678 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetTaskU3Ec__AnonStorey0_U3CU3Em__0_m4067005652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		FutureString_t4225986006 * L_0 = __this->get_fu_0();
		NullCheck(L_0);
		int32_t L_1 = FutureBase_Status_m543620527(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0021;
		}
	}
	{
		TaskCompletionSource_1_t911654313 * L_2 = __this->get_tcs_1();
		NullCheck(L_2);
		TaskCompletionSource_1_SetCanceled_m2886845820(L_2, /*hidden argument*/TaskCompletionSource_1_SetCanceled_m2886845820_MethodInfo_var);
		goto IL_006a;
	}

IL_0021:
	{
		FutureString_t4225986006 * L_3 = __this->get_fu_0();
		NullCheck(L_3);
		int32_t L_4 = FutureBase_Error_m954039370(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if (!L_5)
		{
			goto IL_0054;
		}
	}
	{
		TaskCompletionSource_1_t911654313 * L_6 = __this->get_tcs_1();
		int32_t L_7 = V_0;
		FutureString_t4225986006 * L_8 = __this->get_fu_0();
		NullCheck(L_8);
		String_t* L_9 = FutureBase_ErrorMessage_m11232826(L_8, /*hidden argument*/NULL);
		FirebaseException_t2567272216 * L_10 = (FirebaseException_t2567272216 *)il2cpp_codegen_object_new(FirebaseException_t2567272216_il2cpp_TypeInfo_var);
		FirebaseException__ctor_m3734642555(L_10, L_7, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		TaskCompletionSource_1_SetException_m3451240888(L_6, L_10, /*hidden argument*/TaskCompletionSource_1_SetException_m3451240888_MethodInfo_var);
		goto IL_006a;
	}

IL_0054:
	{
		TaskCompletionSource_1_t911654313 * L_11 = __this->get_tcs_1();
		FutureString_t4225986006 * L_12 = __this->get_fu_0();
		NullCheck(L_12);
		String_t* L_13 = FutureString_Result_m2875035570(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		TaskCompletionSource_1_SetResult_m539871673(L_11, L_13, /*hidden argument*/TaskCompletionSource_1_SetResult_m539871673_MethodInfo_var);
	}

IL_006a:
	{
		FutureString_t4225986006 * L_14 = __this->get_fu_0();
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(5 /* System.Void Firebase.FutureBase::Dispose() */, L_14);
		return;
	}
}
// System.Void Firebase.FutureString/Action::.ctor(System.Object,System.IntPtr)
extern "C"  void Action__ctor_m3716776567 (Action_t3062064994 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.FutureString/Action::Invoke()
extern "C"  void Action_Invoke_m4105640499 (Action_t3062064994 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_Invoke_m4105640499((Action_t3062064994 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_Action_t3062064994 (Action_t3062064994 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult Firebase.FutureString/Action::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_BeginInvoke_m1000576792 (Action_t3062064994 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void Firebase.FutureString/Action::EndInvoke(System.IAsyncResult)
extern "C"  void Action_EndInvoke_m2605602173 (Action_t3062064994 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Firebase.FutureString/MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern "C"  void MonoPInvokeCallbackAttribute__ctor_m158176042 (MonoPInvokeCallbackAttribute_t507919426 * __this, Type_t * ___t0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.FutureString/SWIG_CompletionDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void SWIG_CompletionDelegate__ctor_m3991997779 (SWIG_CompletionDelegate_t1819656562 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.FutureString/SWIG_CompletionDelegate::Invoke(System.Int32)
extern "C"  void SWIG_CompletionDelegate_Invoke_m2502924064 (SWIG_CompletionDelegate_t1819656562 * __this, int32_t ___index0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		SWIG_CompletionDelegate_Invoke_m2502924064((SWIG_CompletionDelegate_t1819656562 *)__this->get_prev_9(),___index0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___index0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___index0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___index0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___index0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_SWIG_CompletionDelegate_t1819656562 (SWIG_CompletionDelegate_t1819656562 * __this, int32_t ___index0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___index0);

}
// System.IAsyncResult Firebase.FutureString/SWIG_CompletionDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t SWIG_CompletionDelegate_BeginInvoke_m1619116057_MetadataUsageId;
extern "C"  Il2CppObject * SWIG_CompletionDelegate_BeginInvoke_m1619116057 (SWIG_CompletionDelegate_t1819656562 * __this, int32_t ___index0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIG_CompletionDelegate_BeginInvoke_m1619116057_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___index0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Firebase.FutureString/SWIG_CompletionDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void SWIG_CompletionDelegate_EndInvoke_m2088723553 (SWIG_CompletionDelegate_t1819656562 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Firebase.InitializationException::.ctor(Firebase.InitResult)
extern "C"  void InitializationException__ctor_m6220901 (InitializationException_t1438959983 * __this, int32_t ___result0, const MethodInfo* method)
{
	{
		Exception__ctor_m3886110570(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___result0;
		InitializationException_set_InitResult_m394481415(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.InitializationException::.ctor(Firebase.InitResult,System.String)
extern "C"  void InitializationException__ctor_m1927152695 (InitializationException_t1438959983 * __this, int32_t ___result0, String_t* ___message1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message1;
		Exception__ctor_m485833136(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___result0;
		InitializationException_set_InitResult_m394481415(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.InitializationException::set_InitResult(Firebase.InitResult)
extern "C"  void InitializationException_set_InitResult_m394481415 (InitializationException_t1438959983 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CInitResultU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Void Firebase.Internal.FirebaseConfigExtensions::SetDatabaseUrl(Firebase.FirebaseApp,System.String)
extern Il2CppClass* FirebaseConfigExtensions_t617662701_il2cpp_TypeInfo_var;
extern const MethodInfo* FirebaseConfigExtensions_SetState_TisString_t_m1566626226_MethodInfo_var;
extern const uint32_t FirebaseConfigExtensions_SetDatabaseUrl_m1055815427_MetadataUsageId;
extern "C"  void FirebaseConfigExtensions_SetDatabaseUrl_m1055815427 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, String_t* ___databaseUrl1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseConfigExtensions_SetDatabaseUrl_m1055815427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FirebaseApp_t210707726 * L_0 = ___app0;
		String_t* L_1 = ___databaseUrl1;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseConfigExtensions_t617662701_il2cpp_TypeInfo_var);
		Dictionary_2_t2951825130 * L_2 = ((FirebaseConfigExtensions_t617662701_StaticFields*)FirebaseConfigExtensions_t617662701_il2cpp_TypeInfo_var->static_fields)->get_SStringState_2();
		FirebaseConfigExtensions_SetState_TisString_t_m1566626226(NULL /*static, unused*/, L_0, 0, L_1, L_2, /*hidden argument*/FirebaseConfigExtensions_SetState_TisString_t_m1566626226_MethodInfo_var);
		return;
	}
}
// System.String Firebase.Internal.FirebaseConfigExtensions::GetDatabaseUrl(Firebase.FirebaseApp)
extern Il2CppClass* FirebaseConfigExtensions_t617662701_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* FirebaseConfigExtensions_GetState_TisString_t_m1312439933_MethodInfo_var;
extern const uint32_t FirebaseConfigExtensions_GetDatabaseUrl_m3883777974_MetadataUsageId;
extern "C"  String_t* FirebaseConfigExtensions_GetDatabaseUrl_m3883777974 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseConfigExtensions_GetDatabaseUrl_m3883777974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		FirebaseApp_t210707726 * L_0 = ___app0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseConfigExtensions_t617662701_il2cpp_TypeInfo_var);
		Dictionary_2_t2951825130 * L_1 = ((FirebaseConfigExtensions_t617662701_StaticFields*)FirebaseConfigExtensions_t617662701_il2cpp_TypeInfo_var->static_fields)->get_SStringState_2();
		String_t* L_2 = FirebaseConfigExtensions_GetState_TisString_t_m1312439933(NULL /*static, unused*/, L_0, 0, L_1, /*hidden argument*/FirebaseConfigExtensions_GetState_TisString_t_m1312439933_MethodInfo_var);
		V_0 = L_2;
		String_t* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002f;
		}
	}
	{
		FirebaseApp_t210707726 * L_5 = ___app0;
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		FirebaseApp_t210707726 * L_6 = ___app0;
		NullCheck(L_6);
		AppOptions_t1641189195 * L_7 = FirebaseApp_get_Options_m763126714(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Uri_t19570940 * L_8 = AppOptions_get_DatabaseUrl_m241783396(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
		V_0 = L_9;
	}

IL_002f:
	{
		String_t* L_10 = V_0;
		return L_10;
	}
}
// System.Void Firebase.Internal.FirebaseConfigExtensions::.cctor()
extern Il2CppClass* FirebaseConfigExtensions_t617662701_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2951825130_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2994107520_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3512666503;
extern const uint32_t FirebaseConfigExtensions__cctor_m2295918737_MetadataUsageId;
extern "C"  void FirebaseConfigExtensions__cctor_m2295918737 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseConfigExtensions__cctor_m2295918737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((FirebaseConfigExtensions_t617662701_StaticFields*)FirebaseConfigExtensions_t617662701_il2cpp_TypeInfo_var->static_fields)->set_Default_0(_stringLiteral3512666503);
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		((FirebaseConfigExtensions_t617662701_StaticFields*)FirebaseConfigExtensions_t617662701_il2cpp_TypeInfo_var->static_fields)->set_Sync_1(L_0);
		Dictionary_2_t2951825130 * L_1 = (Dictionary_2_t2951825130 *)il2cpp_codegen_object_new(Dictionary_2_t2951825130_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2994107520(L_1, /*hidden argument*/Dictionary_2__ctor_m2994107520_MethodInfo_var);
		((FirebaseConfigExtensions_t617662701_StaticFields*)FirebaseConfigExtensions_t617662701_il2cpp_TypeInfo_var->static_fields)->set_SStringState_2(L_1);
		return;
	}
}
// System.String Firebase.Internal.FirebaseEditorExtensions::GetEditorP12Password(Firebase.FirebaseApp)
extern Il2CppClass* FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseConfigExtensions_t617662701_il2cpp_TypeInfo_var;
extern const MethodInfo* FirebaseConfigExtensions_GetState_TisString_t_m1312439933_MethodInfo_var;
extern const uint32_t FirebaseEditorExtensions_GetEditorP12Password_m873176310_MetadataUsageId;
extern "C"  String_t* FirebaseEditorExtensions_GetEditorP12Password_m873176310 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseEditorExtensions_GetEditorP12Password_m873176310_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FirebaseApp_t210707726 * L_0 = ___app0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var);
		Dictionary_2_t2951825130 * L_1 = ((FirebaseEditorExtensions_t1010167188_StaticFields*)FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var->static_fields)->get_SStringState_0();
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseConfigExtensions_t617662701_il2cpp_TypeInfo_var);
		String_t* L_2 = FirebaseConfigExtensions_GetState_TisString_t_m1312439933(NULL /*static, unused*/, L_0, 1, L_1, /*hidden argument*/FirebaseConfigExtensions_GetState_TisString_t_m1312439933_MethodInfo_var);
		return L_2;
	}
}
// System.String Firebase.Internal.FirebaseEditorExtensions::GetEditorP12FileName(Firebase.FirebaseApp)
extern Il2CppClass* FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseConfigExtensions_t617662701_il2cpp_TypeInfo_var;
extern const MethodInfo* FirebaseConfigExtensions_GetState_TisString_t_m1312439933_MethodInfo_var;
extern const uint32_t FirebaseEditorExtensions_GetEditorP12FileName_m620685606_MetadataUsageId;
extern "C"  String_t* FirebaseEditorExtensions_GetEditorP12FileName_m620685606 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseEditorExtensions_GetEditorP12FileName_m620685606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FirebaseApp_t210707726 * L_0 = ___app0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var);
		Dictionary_2_t2951825130 * L_1 = ((FirebaseEditorExtensions_t1010167188_StaticFields*)FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var->static_fields)->get_SStringState_0();
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseConfigExtensions_t617662701_il2cpp_TypeInfo_var);
		String_t* L_2 = FirebaseConfigExtensions_GetState_TisString_t_m1312439933(NULL /*static, unused*/, L_0, 0, L_1, /*hidden argument*/FirebaseConfigExtensions_GetState_TisString_t_m1312439933_MethodInfo_var);
		return L_2;
	}
}
// System.String Firebase.Internal.FirebaseEditorExtensions::GetEditorServiceAccountEmail(Firebase.FirebaseApp)
extern Il2CppClass* FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseConfigExtensions_t617662701_il2cpp_TypeInfo_var;
extern const MethodInfo* FirebaseConfigExtensions_GetState_TisString_t_m1312439933_MethodInfo_var;
extern const uint32_t FirebaseEditorExtensions_GetEditorServiceAccountEmail_m460851250_MetadataUsageId;
extern "C"  String_t* FirebaseEditorExtensions_GetEditorServiceAccountEmail_m460851250 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseEditorExtensions_GetEditorServiceAccountEmail_m460851250_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FirebaseApp_t210707726 * L_0 = ___app0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var);
		Dictionary_2_t2951825130 * L_1 = ((FirebaseEditorExtensions_t1010167188_StaticFields*)FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var->static_fields)->get_SStringState_0();
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseConfigExtensions_t617662701_il2cpp_TypeInfo_var);
		String_t* L_2 = FirebaseConfigExtensions_GetState_TisString_t_m1312439933(NULL /*static, unused*/, L_0, 2, L_1, /*hidden argument*/FirebaseConfigExtensions_GetState_TisString_t_m1312439933_MethodInfo_var);
		return L_2;
	}
}
// System.String Firebase.Internal.FirebaseEditorExtensions::GetEditorAuthUserId(Firebase.FirebaseApp)
extern Il2CppClass* FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseConfigExtensions_t617662701_il2cpp_TypeInfo_var;
extern const MethodInfo* FirebaseConfigExtensions_GetState_TisString_t_m1312439933_MethodInfo_var;
extern const uint32_t FirebaseEditorExtensions_GetEditorAuthUserId_m2961892456_MetadataUsageId;
extern "C"  String_t* FirebaseEditorExtensions_GetEditorAuthUserId_m2961892456 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseEditorExtensions_GetEditorAuthUserId_m2961892456_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FirebaseApp_t210707726 * L_0 = ___app0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var);
		Dictionary_2_t2951825130 * L_1 = ((FirebaseEditorExtensions_t1010167188_StaticFields*)FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var->static_fields)->get_SStringState_0();
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseConfigExtensions_t617662701_il2cpp_TypeInfo_var);
		String_t* L_2 = FirebaseConfigExtensions_GetState_TisString_t_m1312439933(NULL /*static, unused*/, L_0, 3, L_1, /*hidden argument*/FirebaseConfigExtensions_GetState_TisString_t_m1312439933_MethodInfo_var);
		return L_2;
	}
}
// System.String Firebase.Internal.FirebaseEditorExtensions::GetCertPemFile(Firebase.FirebaseApp)
extern Il2CppClass* FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseConfigExtensions_t617662701_il2cpp_TypeInfo_var;
extern const MethodInfo* FirebaseConfigExtensions_GetState_TisString_t_m1312439933_MethodInfo_var;
extern const uint32_t FirebaseEditorExtensions_GetCertPemFile_m1950008923_MetadataUsageId;
extern "C"  String_t* FirebaseEditorExtensions_GetCertPemFile_m1950008923 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseEditorExtensions_GetCertPemFile_m1950008923_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FirebaseApp_t210707726 * L_0 = ___app0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var);
		Dictionary_2_t2951825130 * L_1 = ((FirebaseEditorExtensions_t1010167188_StaticFields*)FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var->static_fields)->get_SStringState_0();
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseConfigExtensions_t617662701_il2cpp_TypeInfo_var);
		String_t* L_2 = FirebaseConfigExtensions_GetState_TisString_t_m1312439933(NULL /*static, unused*/, L_0, 4, L_1, /*hidden argument*/FirebaseConfigExtensions_GetState_TisString_t_m1312439933_MethodInfo_var);
		return L_2;
	}
}
// System.Void Firebase.Internal.FirebaseEditorExtensions::.cctor()
extern Il2CppClass* Dictionary_2_t2951825130_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2994107520_MethodInfo_var;
extern const uint32_t FirebaseEditorExtensions__cctor_m722228402_MetadataUsageId;
extern "C"  void FirebaseEditorExtensions__cctor_m722228402 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseEditorExtensions__cctor_m722228402_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2951825130 * L_0 = (Dictionary_2_t2951825130 *)il2cpp_codegen_object_new(Dictionary_2_t2951825130_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2994107520(L_0, /*hidden argument*/Dictionary_2__ctor_m2994107520_MethodInfo_var);
		((FirebaseEditorExtensions_t1010167188_StaticFields*)FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var->static_fields)->set_SStringState_0(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.Byte[]> Firebase.Internal.InstallRootCerts::DecodeBase64Blobs(System.String,System.String,System.String)
extern Il2CppClass* List_1_t2766455145_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* Regex_t1803876613_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1332851588_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1633283408_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2789824602;
extern const uint32_t InstallRootCerts_DecodeBase64Blobs_m4010299014_MetadataUsageId;
extern "C"  List_1_t2766455145 * InstallRootCerts_DecodeBase64Blobs_m4010299014 (Il2CppObject * __this /* static, unused */, String_t* ___base64BlobList0, String_t* ___startLine1, String_t* ___endLine2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InstallRootCerts_DecodeBase64Blobs_m4010299014_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2766455145 * V_0 = NULL;
	StringBuilder_t1221177846 * V_1 = NULL;
	bool V_2 = false;
	StringU5BU5D_t1642385972* V_3 = NULL;
	String_t* V_4 = NULL;
	StringU5BU5D_t1642385972* V_5 = NULL;
	int32_t V_6 = 0;
	{
		List_1_t2766455145 * L_0 = (List_1_t2766455145 *)il2cpp_codegen_object_new(List_1_t2766455145_il2cpp_TypeInfo_var);
		List_1__ctor_m1332851588(L_0, /*hidden argument*/List_1__ctor_m1332851588_MethodInfo_var);
		V_0 = L_0;
		StringBuilder_t1221177846 * L_1 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_1, /*hidden argument*/NULL);
		V_1 = L_1;
		V_2 = (bool)0;
		String_t* L_2 = ___base64BlobList0;
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t1803876613_il2cpp_TypeInfo_var);
		StringU5BU5D_t1642385972* L_3 = Regex_Split_m4283382107(NULL /*static, unused*/, L_2, _stringLiteral2789824602, /*hidden argument*/NULL);
		V_3 = L_3;
		StringU5BU5D_t1642385972* L_4 = V_3;
		V_5 = L_4;
		V_6 = 0;
		goto IL_007a;
	}

IL_0025:
	{
		StringU5BU5D_t1642385972* L_5 = V_5;
		int32_t L_6 = V_6;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		String_t* L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_4 = L_8;
		bool L_9 = V_2;
		if (!L_9)
		{
			goto IL_006b;
		}
	}
	{
		String_t* L_10 = V_4;
		String_t* L_11 = ___endLine2;
		NullCheck(L_10);
		bool L_12 = String_StartsWith_m1841920685(L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_005d;
		}
	}
	{
		V_2 = (bool)0;
		List_1_t2766455145 * L_13 = V_0;
		StringBuilder_t1221177846 * L_14 = V_1;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_16 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		List_1_Add_m1633283408(L_13, L_16, /*hidden argument*/List_1_Add_m1633283408_MethodInfo_var);
		StringBuilder_t1221177846 * L_17 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_17, /*hidden argument*/NULL);
		V_1 = L_17;
		goto IL_0074;
	}

IL_005d:
	{
		StringBuilder_t1221177846 * L_18 = V_1;
		String_t* L_19 = V_4;
		NullCheck(L_18);
		StringBuilder_Append_m3636508479(L_18, L_19, /*hidden argument*/NULL);
		goto IL_0074;
	}

IL_006b:
	{
		String_t* L_20 = V_4;
		String_t* L_21 = ___startLine1;
		NullCheck(L_20);
		bool L_22 = String_StartsWith_m1841920685(L_20, L_21, /*hidden argument*/NULL);
		V_2 = L_22;
	}

IL_0074:
	{
		int32_t L_23 = V_6;
		V_6 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_007a:
	{
		int32_t L_24 = V_6;
		StringU5BU5D_t1642385972* L_25 = V_5;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length)))))))
		{
			goto IL_0025;
		}
	}
	{
		List_1_t2766455145 * L_26 = V_0;
		return L_26;
	}
}
// System.Security.Cryptography.X509Certificates.X509CertificateCollection Firebase.Internal.InstallRootCerts::DecodeCertificateCollectionFromString(System.String)
extern Il2CppClass* X509CertificateCollection_t1197680765_il2cpp_TypeInfo_var;
extern Il2CppClass* InstallRootCerts_t1106242208_il2cpp_TypeInfo_var;
extern Il2CppClass* X509Certificate2_t4056456767_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1015989329_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2519597945_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2488397049_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2453997997_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral756475024;
extern Il2CppCodeGenString* _stringLiteral282734432;
extern const uint32_t InstallRootCerts_DecodeCertificateCollectionFromString_m2530752882_MetadataUsageId;
extern "C"  X509CertificateCollection_t1197680765 * InstallRootCerts_DecodeCertificateCollectionFromString_m2530752882 (Il2CppObject * __this /* static, unused */, String_t* ___certString0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InstallRootCerts_DecodeCertificateCollectionFromString_m2530752882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	X509CertificateCollection_t1197680765 * V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	Enumerator_t2301184819  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		X509CertificateCollection_t1197680765 * L_0 = (X509CertificateCollection_t1197680765 *)il2cpp_codegen_object_new(X509CertificateCollection_t1197680765_il2cpp_TypeInfo_var);
		X509CertificateCollection__ctor_m1497182392(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = ___certString0;
		IL2CPP_RUNTIME_CLASS_INIT(InstallRootCerts_t1106242208_il2cpp_TypeInfo_var);
		List_1_t2766455145 * L_2 = InstallRootCerts_DecodeBase64Blobs_m4010299014(NULL /*static, unused*/, L_1, _stringLiteral756475024, _stringLiteral282734432, /*hidden argument*/NULL);
		NullCheck(L_2);
		Enumerator_t2301184819  L_3 = List_1_GetEnumerator_m1015989329(L_2, /*hidden argument*/List_1_GetEnumerator_m1015989329_MethodInfo_var);
		V_2 = L_3;
	}

IL_001c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0036;
		}

IL_0021:
		{
			ByteU5BU5D_t3397334013* L_4 = Enumerator_get_Current_m2519597945((&V_2), /*hidden argument*/Enumerator_get_Current_m2519597945_MethodInfo_var);
			V_1 = L_4;
			X509CertificateCollection_t1197680765 * L_5 = V_0;
			ByteU5BU5D_t3397334013* L_6 = V_1;
			X509Certificate2_t4056456767 * L_7 = (X509Certificate2_t4056456767 *)il2cpp_codegen_object_new(X509Certificate2_t4056456767_il2cpp_TypeInfo_var);
			X509Certificate2__ctor_m4067762773(L_7, L_6, /*hidden argument*/NULL);
			NullCheck(L_5);
			X509CertificateCollection_Add_m3582592011(L_5, L_7, /*hidden argument*/NULL);
		}

IL_0036:
		{
			bool L_8 = Enumerator_MoveNext_m2488397049((&V_2), /*hidden argument*/Enumerator_MoveNext_m2488397049_MethodInfo_var);
			if (L_8)
			{
				goto IL_0021;
			}
		}

IL_0042:
		{
			IL2CPP_LEAVE(0x55, FINALLY_0047);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0047;
	}

FINALLY_0047:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2453997997((&V_2), /*hidden argument*/Enumerator_Dispose_m2453997997_MethodInfo_var);
		IL2CPP_END_FINALLY(71)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(71)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0055:
	{
		X509CertificateCollection_t1197680765 * L_9 = V_0;
		return L_9;
	}
}
// System.Security.Cryptography.X509Certificates.X509CertificateCollection Firebase.Internal.InstallRootCerts::DecodeDefaultCollection()
extern const Il2CppType* InstallRootCerts_t1106242208_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StreamReader_t2360341767_il2cpp_TypeInfo_var;
extern Il2CppClass* InstallRootCerts_t1106242208_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* X509CertificateCollection_t1197680765_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1762187431;
extern const uint32_t InstallRootCerts_DecodeDefaultCollection_m56704705_MetadataUsageId;
extern "C"  X509CertificateCollection_t1197680765 * InstallRootCerts_DecodeDefaultCollection_m56704705 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InstallRootCerts_DecodeDefaultCollection_m56704705_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StreamReader_t2360341767 * V_0 = NULL;
	X509CertificateCollection_t1197680765 * V_1 = NULL;
	Exception_t1927440687 * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(InstallRootCerts_t1106242208_0_0_0_var), /*hidden argument*/NULL);
			NullCheck(L_0);
			Assembly_t4268412390 * L_1 = VirtFuncInvoker0< Assembly_t4268412390 * >::Invoke(14 /* System.Reflection.Assembly System.Type::get_Assembly() */, L_0);
			NullCheck(L_1);
			Stream_t3255436806 * L_2 = VirtFuncInvoker1< Stream_t3255436806 *, String_t* >::Invoke(14 /* System.IO.Stream System.Reflection.Assembly::GetManifestResourceStream(System.String) */, L_1, _stringLiteral1762187431);
			StreamReader_t2360341767 * L_3 = (StreamReader_t2360341767 *)il2cpp_codegen_object_new(StreamReader_t2360341767_il2cpp_TypeInfo_var);
			StreamReader__ctor_m1780435609(L_3, L_2, /*hidden argument*/NULL);
			V_0 = L_3;
		}

IL_001f:
		try
		{ // begin try (depth: 2)
			StreamReader_t2360341767 * L_4 = V_0;
			NullCheck(L_4);
			String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(13 /* System.String System.IO.TextReader::ReadToEnd() */, L_4);
			IL2CPP_RUNTIME_CLASS_INIT(InstallRootCerts_t1106242208_il2cpp_TypeInfo_var);
			X509CertificateCollection_t1197680765 * L_6 = InstallRootCerts_DecodeCertificateCollectionFromString_m2530752882(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
			V_1 = L_6;
			IL2CPP_LEAVE(0x54, FINALLY_0030);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_0030;
		}

FINALLY_0030:
		{ // begin finally (depth: 2)
			{
				StreamReader_t2360341767 * L_7 = V_0;
				if (!L_7)
				{
					goto IL_003c;
				}
			}

IL_0036:
			{
				StreamReader_t2360341767 * L_8 = V_0;
				NullCheck(L_8);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_8);
			}

IL_003c:
			{
				IL2CPP_END_FINALLY(48)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(48)
		{
			IL2CPP_JUMP_TBL(0x54, IL_0054)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_003d;
		throw e;
	}

CATCH_003d:
	{ // begin catch(System.Exception)
		V_2 = ((Exception_t1927440687 *)__exception_local);
		Exception_t1927440687 * L_9 = V_2;
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		goto IL_004e;
	} // end catch (depth: 1)

IL_004e:
	{
		X509CertificateCollection_t1197680765 * L_11 = (X509CertificateCollection_t1197680765 *)il2cpp_codegen_object_new(X509CertificateCollection_t1197680765_il2cpp_TypeInfo_var);
		X509CertificateCollection__ctor_m1497182392(L_11, /*hidden argument*/NULL);
		return L_11;
	}

IL_0054:
	{
		X509CertificateCollection_t1197680765 * L_12 = V_1;
		return L_12;
	}
}
// System.Security.Cryptography.X509Certificates.X509CertificateCollection Firebase.Internal.InstallRootCerts::DecodeCollection(Firebase.FirebaseApp)
extern Il2CppClass* FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TextAsset_t3973159845_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* InstallRootCerts_t1106242208_il2cpp_TypeInfo_var;
extern Il2CppClass* X509CertificateCollection_t1197680765_il2cpp_TypeInfo_var;
extern const uint32_t InstallRootCerts_DecodeCollection_m3158849309_MetadataUsageId;
extern "C"  X509CertificateCollection_t1197680765 * InstallRootCerts_DecodeCollection_m3158849309 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InstallRootCerts_DecodeCollection_m3158849309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	TextAsset_t3973159845 * V_1 = NULL;
	{
		FirebaseApp_t210707726 * L_0 = ___app0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var);
		String_t* L_1 = FirebaseEditorExtensions_GetCertPemFile_m1950008923(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0036;
		}
	}
	{
		String_t* L_4 = V_0;
		Object_t1021602117 * L_5 = Resources_Load_m2041782325(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_1 = ((TextAsset_t3973159845 *)IsInstClass(L_5, TextAsset_t3973159845_il2cpp_TypeInfo_var));
		TextAsset_t3973159845 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0036;
		}
	}
	{
		TextAsset_t3973159845 * L_8 = V_1;
		NullCheck(L_8);
		String_t* L_9 = TextAsset_get_text_m2589865997(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(InstallRootCerts_t1106242208_il2cpp_TypeInfo_var);
		X509CertificateCollection_t1197680765 * L_10 = InstallRootCerts_DecodeCertificateCollectionFromString_m2530752882(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return L_10;
	}

IL_0036:
	{
		X509CertificateCollection_t1197680765 * L_11 = (X509CertificateCollection_t1197680765 *)il2cpp_codegen_object_new(X509CertificateCollection_t1197680765_il2cpp_TypeInfo_var);
		X509CertificateCollection__ctor_m1497182392(L_11, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Void Firebase.Internal.InstallRootCerts::InstallDefaultCRLs(System.String,System.String)
extern const Il2CppType* InstallRootCerts_t1106242208_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseHandler_t2907300047_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StreamReader_t2360341767_il2cpp_TypeInfo_var;
extern Il2CppClass* InstallRootCerts_t1106242208_il2cpp_TypeInfo_var;
extern Il2CppClass* BitConverter_t3195628829_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* Path_t41728875_il2cpp_TypeInfo_var;
extern Il2CppClass* FileStream_t1695958676_il2cpp_TypeInfo_var;
extern Il2CppClass* BinaryWriter_t3179371318_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1015989329_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2519597945_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2488397049_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2453997997_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2790840768;
extern Il2CppCodeGenString* _stringLiteral917176900;
extern Il2CppCodeGenString* _stringLiteral3832406372;
extern Il2CppCodeGenString* _stringLiteral372029313;
extern Il2CppCodeGenString* _stringLiteral3578476267;
extern Il2CppCodeGenString* _stringLiteral1094699609;
extern const uint32_t InstallRootCerts_InstallDefaultCRLs_m2824657295_MetadataUsageId;
extern "C"  void InstallRootCerts_InstallDefaultCRLs_m2824657295 (Il2CppObject * __this /* static, unused */, String_t* ___resource_name0, String_t* ___directory1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InstallRootCerts_InstallDefaultCRLs_m2824657295_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	StreamReader_t2360341767 * V_1 = NULL;
	ByteU5BU5D_t3397334013* V_2 = NULL;
	Enumerator_t2301184819  V_3;
	memset(&V_3, 0, sizeof(V_3));
	String_t* V_4 = NULL;
	MD5_t1507972490 * V_5 = NULL;
	String_t* V_6 = NULL;
	BinaryWriter_t3179371318 * V_7 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___directory1;
		Directory_CreateDirectory_m3561886598(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = 0;
		String_t* L_1 = ___directory1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2790840768, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
		FirebaseHandler_LogMessage_m1734897910(NULL /*static, unused*/, 1, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(InstallRootCerts_t1106242208_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_3);
		Assembly_t4268412390 * L_4 = VirtFuncInvoker0< Assembly_t4268412390 * >::Invoke(14 /* System.Reflection.Assembly System.Type::get_Assembly() */, L_3);
		String_t* L_5 = ___resource_name0;
		NullCheck(L_4);
		Stream_t3255436806 * L_6 = VirtFuncInvoker1< Stream_t3255436806 *, String_t* >::Invoke(14 /* System.IO.Stream System.Reflection.Assembly::GetManifestResourceStream(System.String) */, L_4, L_5);
		StreamReader_t2360341767 * L_7 = (StreamReader_t2360341767 *)il2cpp_codegen_object_new(StreamReader_t2360341767_il2cpp_TypeInfo_var);
		StreamReader__ctor_m1780435609(L_7, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
	}

IL_0035:
	try
	{ // begin try (depth: 1)
		{
			StreamReader_t2360341767 * L_8 = V_1;
			NullCheck(L_8);
			String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(13 /* System.String System.IO.TextReader::ReadToEnd() */, L_8);
			IL2CPP_RUNTIME_CLASS_INIT(InstallRootCerts_t1106242208_il2cpp_TypeInfo_var);
			List_1_t2766455145 * L_10 = InstallRootCerts_DecodeBase64Blobs_m4010299014(NULL /*static, unused*/, L_9, _stringLiteral917176900, _stringLiteral3832406372, /*hidden argument*/NULL);
			NullCheck(L_10);
			Enumerator_t2301184819  L_11 = List_1_GetEnumerator_m1015989329(L_10, /*hidden argument*/List_1_GetEnumerator_m1015989329_MethodInfo_var);
			V_3 = L_11;
		}

IL_0050:
		try
		{ // begin try (depth: 2)
			{
				goto IL_00ea;
			}

IL_0055:
			{
				ByteU5BU5D_t3397334013* L_12 = Enumerator_get_Current_m2519597945((&V_3), /*hidden argument*/Enumerator_get_Current_m2519597945_MethodInfo_var);
				V_2 = L_12;
				MD5_t1507972490 * L_13 = MD5_Create_m1572997499(NULL /*static, unused*/, /*hidden argument*/NULL);
				V_5 = L_13;
			}

IL_0064:
			try
			{ // begin try (depth: 3)
				MD5_t1507972490 * L_14 = V_5;
				ByteU5BU5D_t3397334013* L_15 = V_2;
				NullCheck(L_14);
				ByteU5BU5D_t3397334013* L_16 = HashAlgorithm_ComputeHash_m3637856778(L_14, L_15, /*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
				String_t* L_17 = BitConverter_ToString_m927173850(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_18 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
				NullCheck(L_17);
				String_t* L_19 = String_Replace_m1941156251(L_17, _stringLiteral372029313, L_18, /*hidden argument*/NULL);
				V_4 = L_19;
				IL2CPP_LEAVE(0x96, FINALLY_0087);
			} // end try (depth: 3)
			catch(Il2CppExceptionWrapper& e)
			{
				__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
				goto FINALLY_0087;
			}

FINALLY_0087:
			{ // begin finally (depth: 3)
				{
					MD5_t1507972490 * L_20 = V_5;
					if (!L_20)
					{
						goto IL_0095;
					}
				}

IL_008e:
				{
					MD5_t1507972490 * L_21 = V_5;
					NullCheck(L_21);
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_21);
				}

IL_0095:
				{
					IL2CPP_END_FINALLY(135)
				}
			} // end finally (depth: 3)
			IL2CPP_CLEANUP(135)
			{
				IL2CPP_JUMP_TBL(0x96, IL_0096)
				IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
			}

IL_0096:
			{
				String_t* L_22 = ___directory1;
				String_t* L_23 = V_4;
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_24 = String_Concat_m2596409543(NULL /*static, unused*/, L_23, _stringLiteral3578476267, /*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(Path_t41728875_il2cpp_TypeInfo_var);
				String_t* L_25 = Path_Combine_m3185811654(NULL /*static, unused*/, L_22, L_24, /*hidden argument*/NULL);
				V_6 = L_25;
				String_t* L_26 = V_6;
				bool L_27 = File_Exists_m1685968367(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
				if (!L_27)
				{
					goto IL_00bb;
				}
			}

IL_00b6:
			{
				goto IL_00ea;
			}

IL_00bb:
			{
				String_t* L_28 = V_6;
				FileStream_t1695958676 * L_29 = (FileStream_t1695958676 *)il2cpp_codegen_object_new(FileStream_t1695958676_il2cpp_TypeInfo_var);
				FileStream__ctor_m785772645(L_29, L_28, 1, /*hidden argument*/NULL);
				BinaryWriter_t3179371318 * L_30 = (BinaryWriter_t3179371318 *)il2cpp_codegen_object_new(BinaryWriter_t3179371318_il2cpp_TypeInfo_var);
				BinaryWriter__ctor_m828435354(L_30, L_29, /*hidden argument*/NULL);
				V_7 = L_30;
			}

IL_00ca:
			try
			{ // begin try (depth: 3)
				BinaryWriter_t3179371318 * L_31 = V_7;
				ByteU5BU5D_t3397334013* L_32 = V_2;
				NullCheck(L_31);
				VirtActionInvoker1< ByteU5BU5D_t3397334013* >::Invoke(9 /* System.Void System.IO.BinaryWriter::Write(System.Byte[]) */, L_31, L_32);
				IL2CPP_LEAVE(0xE6, FINALLY_00d7);
			} // end try (depth: 3)
			catch(Il2CppExceptionWrapper& e)
			{
				__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
				goto FINALLY_00d7;
			}

FINALLY_00d7:
			{ // begin finally (depth: 3)
				{
					BinaryWriter_t3179371318 * L_33 = V_7;
					if (!L_33)
					{
						goto IL_00e5;
					}
				}

IL_00de:
				{
					BinaryWriter_t3179371318 * L_34 = V_7;
					NullCheck(L_34);
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_34);
				}

IL_00e5:
				{
					IL2CPP_END_FINALLY(215)
				}
			} // end finally (depth: 3)
			IL2CPP_CLEANUP(215)
			{
				IL2CPP_JUMP_TBL(0xE6, IL_00e6)
				IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
			}

IL_00e6:
			{
				int32_t L_35 = V_0;
				V_0 = ((int32_t)((int32_t)L_35+(int32_t)1));
			}

IL_00ea:
			{
				bool L_36 = Enumerator_MoveNext_m2488397049((&V_3), /*hidden argument*/Enumerator_MoveNext_m2488397049_MethodInfo_var);
				if (L_36)
				{
					goto IL_0055;
				}
			}

IL_00f6:
			{
				IL2CPP_LEAVE(0x109, FINALLY_00fb);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_00fb;
		}

FINALLY_00fb:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m2453997997((&V_3), /*hidden argument*/Enumerator_Dispose_m2453997997_MethodInfo_var);
			IL2CPP_END_FINALLY(251)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(251)
		{
			IL2CPP_JUMP_TBL(0x109, IL_0109)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_0109:
		{
			IL2CPP_LEAVE(0x11B, FINALLY_010e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_010e;
	}

FINALLY_010e:
	{ // begin finally (depth: 1)
		{
			StreamReader_t2360341767 * L_37 = V_1;
			if (!L_37)
			{
				goto IL_011a;
			}
		}

IL_0114:
		{
			StreamReader_t2360341767 * L_38 = V_1;
			NullCheck(L_38);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_38);
		}

IL_011a:
		{
			IL2CPP_END_FINALLY(270)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(270)
	{
		IL2CPP_JUMP_TBL(0x11B, IL_011b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_011b:
	{
		int32_t L_39 = V_0;
		int32_t L_40 = L_39;
		Il2CppObject * L_41 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_40);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_42 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral1094699609, L_41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
		FirebaseHandler_LogMessage_m1734897910(NULL /*static, unused*/, 1, L_42, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Internal.InstallRootCerts::PrintCert(Firebase.LogLevel,System.Security.Cryptography.X509Certificates.X509Certificate2,System.Security.Cryptography.X509Certificates.X509ChainStatus[],System.String)
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* X509KeyUsageExtension_t1038124237_il2cpp_TypeInfo_var;
extern Il2CppClass* X509KeyUsageFlags_t2461349531_il2cpp_TypeInfo_var;
extern Il2CppClass* X509ChainStatusFlags_t480677120_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseHandler_t2907300047_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral971389738;
extern Il2CppCodeGenString* _stringLiteral2323966648;
extern Il2CppCodeGenString* _stringLiteral4014466388;
extern Il2CppCodeGenString* _stringLiteral2516192615;
extern Il2CppCodeGenString* _stringLiteral3008066940;
extern Il2CppCodeGenString* _stringLiteral4220031635;
extern Il2CppCodeGenString* _stringLiteral1568864492;
extern Il2CppCodeGenString* _stringLiteral3416795099;
extern Il2CppCodeGenString* _stringLiteral889234647;
extern const uint32_t InstallRootCerts_PrintCert_m2117415721_MetadataUsageId;
extern "C"  void InstallRootCerts_PrintCert_m2117415721 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, X509Certificate2_t4056456767 * ___cert1, X509ChainStatusU5BU5D_t830390908* ___chainElementStatus2, String_t* ___chainElementInformation3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InstallRootCerts_PrintCert_m2117415721_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	X509KeyUsageExtension_t1038124237 * V_1 = NULL;
	X509ChainStatus_t4278378721  V_2;
	memset(&V_2, 0, sizeof(V_2));
	X509ChainStatusU5BU5D_t830390908* V_3 = NULL;
	int32_t V_4 = 0;
	String_t* G_B4_0 = NULL;
	String_t* G_B4_1 = NULL;
	StringBuilder_t1221177846 * G_B4_2 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B3_1 = NULL;
	StringBuilder_t1221177846 * G_B3_2 = NULL;
	String_t* G_B6_0 = NULL;
	String_t* G_B6_1 = NULL;
	StringBuilder_t1221177846 * G_B6_2 = NULL;
	String_t* G_B5_0 = NULL;
	String_t* G_B5_1 = NULL;
	StringBuilder_t1221177846 * G_B5_2 = NULL;
	String_t* G_B8_0 = NULL;
	String_t* G_B8_1 = NULL;
	StringBuilder_t1221177846 * G_B8_2 = NULL;
	String_t* G_B7_0 = NULL;
	String_t* G_B7_1 = NULL;
	StringBuilder_t1221177846 * G_B7_2 = NULL;
	String_t* G_B10_0 = NULL;
	StringBuilder_t1221177846 * G_B10_1 = NULL;
	String_t* G_B9_0 = NULL;
	StringBuilder_t1221177846 * G_B9_1 = NULL;
	int32_t G_B11_0 = 0;
	String_t* G_B11_1 = NULL;
	StringBuilder_t1221177846 * G_B11_2 = NULL;
	String_t* G_B13_0 = NULL;
	String_t* G_B13_1 = NULL;
	StringBuilder_t1221177846 * G_B13_2 = NULL;
	String_t* G_B12_0 = NULL;
	String_t* G_B12_1 = NULL;
	StringBuilder_t1221177846 * G_B12_2 = NULL;
	X509KeyUsageExtension_t1038124237 * G_B16_0 = NULL;
	String_t* G_B22_0 = NULL;
	Il2CppObject * G_B22_1 = NULL;
	String_t* G_B22_2 = NULL;
	StringBuilder_t1221177846 * G_B22_3 = NULL;
	String_t* G_B21_0 = NULL;
	Il2CppObject * G_B21_1 = NULL;
	String_t* G_B21_2 = NULL;
	StringBuilder_t1221177846 * G_B21_3 = NULL;
	{
		X509Certificate2_t4056456767 * L_0 = ___cert1;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		StringBuilder_t1221177846 * L_1 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		StringBuilder_t1221177846 * L_2 = V_0;
		X509Certificate2_t4056456767 * L_3 = ___cert1;
		NullCheck(L_3);
		String_t* L_4 = X509Certificate_get_Issuer_m3750321873(L_3, /*hidden argument*/NULL);
		String_t* L_5 = L_4;
		G_B3_0 = L_5;
		G_B3_1 = _stringLiteral971389738;
		G_B3_2 = L_2;
		if (L_5)
		{
			G_B4_0 = L_5;
			G_B4_1 = _stringLiteral971389738;
			G_B4_2 = L_2;
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B4_0 = L_6;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m2024975688(NULL /*static, unused*/, G_B4_1, G_B4_0, /*hidden argument*/NULL);
		NullCheck(G_B4_2);
		StringBuilder_Append_m3636508479(G_B4_2, L_7, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_8 = V_0;
		X509Certificate2_t4056456767 * L_9 = ___cert1;
		NullCheck(L_9);
		String_t* L_10 = X509Certificate_get_Subject_m3123435748(L_9, /*hidden argument*/NULL);
		String_t* L_11 = L_10;
		G_B5_0 = L_11;
		G_B5_1 = _stringLiteral2323966648;
		G_B5_2 = L_8;
		if (L_11)
		{
			G_B6_0 = L_11;
			G_B6_1 = _stringLiteral2323966648;
			G_B6_2 = L_8;
			goto IL_0048;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_1;
		G_B6_2 = G_B5_2;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Format_m2024975688(NULL /*static, unused*/, G_B6_1, G_B6_0, /*hidden argument*/NULL);
		NullCheck(G_B6_2);
		StringBuilder_Append_m3636508479(G_B6_2, L_13, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_14 = V_0;
		X509Certificate2_t4056456767 * L_15 = ___cert1;
		NullCheck(L_15);
		String_t* L_16 = X509Certificate2_get_SerialNumber_m1521011849(L_15, /*hidden argument*/NULL);
		String_t* L_17 = L_16;
		G_B7_0 = L_17;
		G_B7_1 = _stringLiteral4014466388;
		G_B7_2 = L_14;
		if (L_17)
		{
			G_B8_0 = L_17;
			G_B8_1 = _stringLiteral4014466388;
			G_B8_2 = L_14;
			goto IL_006b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B8_0 = L_18;
		G_B8_1 = G_B7_1;
		G_B8_2 = G_B7_2;
	}

IL_006b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Format_m2024975688(NULL /*static, unused*/, G_B8_1, G_B8_0, /*hidden argument*/NULL);
		NullCheck(G_B8_2);
		StringBuilder_Append_m3636508479(G_B8_2, L_19, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_20 = V_0;
		X509Certificate2_t4056456767 * L_21 = ___cert1;
		NullCheck(L_21);
		DateTime_t693205669  L_22 = X509Certificate2_get_NotAfter_m3430171627(L_21, /*hidden argument*/NULL);
		DateTime_t693205669  L_23 = L_22;
		Il2CppObject * L_24 = Box(DateTime_t693205669_il2cpp_TypeInfo_var, &L_23);
		String_t* L_25 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral2516192615, L_24, /*hidden argument*/NULL);
		NullCheck(L_20);
		StringBuilder_Append_m3636508479(L_20, L_25, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_26 = V_0;
		X509ChainStatusU5BU5D_t830390908* L_27 = ___chainElementStatus2;
		G_B9_0 = _stringLiteral3008066940;
		G_B9_1 = L_26;
		if (!L_27)
		{
			G_B10_0 = _stringLiteral3008066940;
			G_B10_1 = L_26;
			goto IL_00a6;
		}
	}
	{
		X509ChainStatusU5BU5D_t830390908* L_28 = ___chainElementStatus2;
		NullCheck(L_28);
		G_B11_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length))));
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		goto IL_00a7;
	}

IL_00a6:
	{
		G_B11_0 = 0;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
	}

IL_00a7:
	{
		int32_t L_29 = G_B11_0;
		Il2CppObject * L_30 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_29);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Format_m2024975688(NULL /*static, unused*/, G_B11_1, L_30, /*hidden argument*/NULL);
		NullCheck(G_B11_2);
		StringBuilder_Append_m3636508479(G_B11_2, L_31, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_32 = V_0;
		String_t* L_33 = ___chainElementInformation3;
		String_t* L_34 = L_33;
		G_B12_0 = L_34;
		G_B12_1 = _stringLiteral4220031635;
		G_B12_2 = L_32;
		if (L_34)
		{
			G_B13_0 = L_34;
			G_B13_1 = _stringLiteral4220031635;
			G_B13_2 = L_32;
			goto IL_00ca;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B13_0 = L_35;
		G_B13_1 = G_B12_1;
		G_B13_2 = G_B12_2;
	}

IL_00ca:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_36 = String_Format_m2024975688(NULL /*static, unused*/, G_B13_1, G_B13_0, /*hidden argument*/NULL);
		NullCheck(G_B13_2);
		StringBuilder_Append_m3636508479(G_B13_2, L_36, /*hidden argument*/NULL);
		X509Certificate2_t4056456767 * L_37 = ___cert1;
		NullCheck(L_37);
		X509ExtensionCollection_t650873211 * L_38 = X509Certificate2_get_Extensions_m3374377562(L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_00fa;
		}
	}
	{
		X509Certificate2_t4056456767 * L_39 = ___cert1;
		NullCheck(L_39);
		X509ExtensionCollection_t650873211 * L_40 = X509Certificate2_get_Extensions_m3374377562(L_39, /*hidden argument*/NULL);
		NullCheck(L_40);
		X509Extension_t1320896183 * L_41 = X509ExtensionCollection_get_Item_m2392275793(L_40, _stringLiteral1568864492, /*hidden argument*/NULL);
		G_B16_0 = ((X509KeyUsageExtension_t1038124237 *)IsInstSealed(L_41, X509KeyUsageExtension_t1038124237_il2cpp_TypeInfo_var));
		goto IL_00fb;
	}

IL_00fa:
	{
		G_B16_0 = ((X509KeyUsageExtension_t1038124237 *)(NULL));
	}

IL_00fb:
	{
		V_1 = G_B16_0;
		X509KeyUsageExtension_t1038124237 * L_42 = V_1;
		if (!L_42)
		{
			goto IL_011e;
		}
	}
	{
		StringBuilder_t1221177846 * L_43 = V_0;
		X509KeyUsageExtension_t1038124237 * L_44 = V_1;
		NullCheck(L_44);
		int32_t L_45 = X509KeyUsageExtension_get_KeyUsages_m4014806163(L_44, /*hidden argument*/NULL);
		int32_t L_46 = L_45;
		Il2CppObject * L_47 = Box(X509KeyUsageFlags_t2461349531_il2cpp_TypeInfo_var, &L_46);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral3416795099, L_47, /*hidden argument*/NULL);
		NullCheck(L_43);
		StringBuilder_Append_m3636508479(L_43, L_48, /*hidden argument*/NULL);
	}

IL_011e:
	{
		X509ChainStatusU5BU5D_t830390908* L_49 = ___chainElementStatus2;
		if (!L_49)
		{
			goto IL_017c;
		}
	}
	{
		X509ChainStatusU5BU5D_t830390908* L_50 = ___chainElementStatus2;
		V_3 = L_50;
		V_4 = 0;
		goto IL_0172;
	}

IL_012e:
	{
		X509ChainStatusU5BU5D_t830390908* L_51 = V_3;
		int32_t L_52 = V_4;
		NullCheck(L_51);
		V_2 = (*(X509ChainStatus_t4278378721 *)((L_51)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_52))));
		StringBuilder_t1221177846 * L_53 = V_0;
		int32_t L_54 = X509ChainStatus_get_Status_m3251168011((&V_2), /*hidden argument*/NULL);
		int32_t L_55 = L_54;
		Il2CppObject * L_56 = Box(X509ChainStatusFlags_t480677120_il2cpp_TypeInfo_var, &L_55);
		String_t* L_57 = X509ChainStatus_get_StatusInformation_m3046569674((&V_2), /*hidden argument*/NULL);
		String_t* L_58 = L_57;
		G_B21_0 = L_58;
		G_B21_1 = L_56;
		G_B21_2 = _stringLiteral889234647;
		G_B21_3 = L_53;
		if (L_58)
		{
			G_B22_0 = L_58;
			G_B22_1 = L_56;
			G_B22_2 = _stringLiteral889234647;
			G_B22_3 = L_53;
			goto IL_0161;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_59 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B22_0 = L_59;
		G_B22_1 = G_B21_1;
		G_B22_2 = G_B21_2;
		G_B22_3 = G_B21_3;
	}

IL_0161:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_60 = String_Format_m1811873526(NULL /*static, unused*/, G_B22_2, G_B22_1, G_B22_0, /*hidden argument*/NULL);
		NullCheck(G_B22_3);
		StringBuilder_Append_m3636508479(G_B22_3, L_60, /*hidden argument*/NULL);
		int32_t L_61 = V_4;
		V_4 = ((int32_t)((int32_t)L_61+(int32_t)1));
	}

IL_0172:
	{
		int32_t L_62 = V_4;
		X509ChainStatusU5BU5D_t830390908* L_63 = V_3;
		NullCheck(L_63);
		if ((((int32_t)L_62) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_63)->max_length)))))))
		{
			goto IL_012e;
		}
	}

IL_017c:
	{
		int32_t L_64 = ___logLevel0;
		StringBuilder_t1221177846 * L_65 = V_0;
		NullCheck(L_65);
		String_t* L_66 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_65);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
		FirebaseHandler_LogMessage_m1734897910(NULL /*static, unused*/, L_64, L_66, /*hidden argument*/NULL);
		return;
	}
}
// System.Security.Cryptography.X509Certificates.X509CertificateCollection Firebase.Internal.InstallRootCerts::Process(Firebase.FirebaseApp)
extern Il2CppClass* InstallRootCerts_t1106242208_il2cpp_TypeInfo_var;
extern Il2CppClass* X509CertificateCollection_t1197680765_il2cpp_TypeInfo_var;
extern Il2CppClass* Path_t41728875_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseHandler_t2907300047_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseApp_t210707726_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* X509Store_t1617430119_il2cpp_TypeInfo_var;
extern Il2CppClass* X509Certificate2_t4056456767_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* ServicePointManager_t745663000_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CProcessU3Ec__AnonStorey0_t1447134549_il2cpp_TypeInfo_var;
extern Il2CppClass* RemoteCertificateValidationCallback_t2756269959_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m1424368394_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m1470897782_MethodInfo_var;
extern const MethodInfo* U3CProcessU3Ec__AnonStorey0_U3CU3Em__0_m2591264586_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral351469801;
extern Il2CppCodeGenString* _stringLiteral3611316491;
extern Il2CppCodeGenString* _stringLiteral2093571363;
extern Il2CppCodeGenString* _stringLiteral2872308020;
extern Il2CppCodeGenString* _stringLiteral3762375568;
extern Il2CppCodeGenString* _stringLiteral3610790251;
extern Il2CppCodeGenString* _stringLiteral1093711738;
extern const uint32_t InstallRootCerts_Process_m1398406616_MetadataUsageId;
extern "C"  X509CertificateCollection_t1197680765 * InstallRootCerts_Process_m1398406616 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InstallRootCerts_Process_m1398406616_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	X509CertificateCollection_t1197680765 * V_1 = NULL;
	X509CertificateCollection_t1197680765 * V_2 = NULL;
	String_t* V_3 = NULL;
	bool V_4 = false;
	X509CertificateCollection_t1197680765 * V_5 = NULL;
	X509CertificateCollection_t1197680765 * V_6 = NULL;
	X509Store_t1617430119 * V_7 = NULL;
	X509CertificateCollection_t1197680765 * V_8 = NULL;
	X509Certificate_t283079845 * V_9 = NULL;
	X509CertificateEnumerator_t1208230922 * V_10 = NULL;
	Exception_t1927440687 * V_11 = NULL;
	Il2CppObject * V_12 = NULL;
	U3CProcessU3Ec__AnonStorey0_t1447134549 * V_13 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B7_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InstallRootCerts_t1106242208_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((InstallRootCerts_t1106242208_StaticFields*)InstallRootCerts_t1106242208_il2cpp_TypeInfo_var->static_fields)->get_Sync_0();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(InstallRootCerts_t1106242208_il2cpp_TypeInfo_var);
			Dictionary_2_t1664293826 * L_2 = ((InstallRootCerts_t1106242208_StaticFields*)InstallRootCerts_t1106242208_il2cpp_TypeInfo_var->static_fields)->get__installedRoots_1();
			FirebaseApp_t210707726 * L_3 = ___app0;
			NullCheck(L_2);
			bool L_4 = Dictionary_2_TryGetValue_m1424368394(L_2, L_3, (&V_1), /*hidden argument*/Dictionary_2_TryGetValue_m1424368394_MethodInfo_var);
			if (!L_4)
			{
				goto IL_0025;
			}
		}

IL_001e:
		{
			X509CertificateCollection_t1197680765 * L_5 = V_1;
			V_2 = L_5;
			IL2CPP_LEAVE(0x219, FINALLY_0212);
		}

IL_0025:
		{
			X509CertificateCollection_t1197680765 * L_6 = (X509CertificateCollection_t1197680765 *)il2cpp_codegen_object_new(X509CertificateCollection_t1197680765_il2cpp_TypeInfo_var);
			X509CertificateCollection__ctor_m1497182392(L_6, /*hidden argument*/NULL);
			V_1 = L_6;
			String_t* L_7 = Environment_GetFolderPath_m1849163024(NULL /*static, unused*/, ((int32_t)26), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Path_t41728875_il2cpp_TypeInfo_var);
			String_t* L_8 = Path_Combine_m3185811654(NULL /*static, unused*/, L_7, _stringLiteral351469801, /*hidden argument*/NULL);
			String_t* L_9 = Path_Combine_m3185811654(NULL /*static, unused*/, L_8, _stringLiteral3611316491, /*hidden argument*/NULL);
			V_3 = L_9;
			V_4 = (bool)0;
		}

IL_004a:
		try
		{ // begin try (depth: 2)
			{
				String_t* L_10 = V_3;
				bool L_11 = Directory_Exists_m2663601042(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
				if (L_11)
				{
					goto IL_0063;
				}
			}

IL_0055:
			{
				String_t* L_12 = V_3;
				DirectoryInfo_t1934446453 * L_13 = Directory_CreateDirectory_m3561886598(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
				G_B7_0 = ((((int32_t)((((Il2CppObject*)(DirectoryInfo_t1934446453 *)L_13) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
				goto IL_0064;
			}

IL_0063:
			{
				G_B7_0 = 1;
			}

IL_0064:
			{
				V_4 = (bool)G_B7_0;
				goto IL_0087;
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1927440687 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_006b;
			throw e;
		}

CATCH_006b:
		{ // begin catch(System.Exception)
			String_t* L_14 = Application_get_persistentDataPath_m3129298355(NULL /*static, unused*/, /*hidden argument*/NULL);
			String_t* L_15 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_16 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral2093571363, L_14, L_15, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
			FirebaseHandler_LogMessage_m1734897910(NULL /*static, unused*/, 1, L_16, /*hidden argument*/NULL);
			goto IL_0087;
		} // end catch (depth: 2)

IL_0087:
		{
			bool L_17 = V_4;
			if (L_17)
			{
				goto IL_00a3;
			}
		}

IL_008e:
		{
			String_t* L_18 = Application_get_persistentDataPath_m3129298355(NULL /*static, unused*/, /*hidden argument*/NULL);
			Environment_SetEnvironmentVariable_m3906641507(NULL /*static, unused*/, _stringLiteral2872308020, L_18, /*hidden argument*/NULL);
			String_t* L_19 = Application_get_persistentDataPath_m3129298355(NULL /*static, unused*/, /*hidden argument*/NULL);
			V_3 = L_19;
		}

IL_00a3:
		{
			IL2CPP_RUNTIME_CLASS_INIT(InstallRootCerts_t1106242208_il2cpp_TypeInfo_var);
			X509CertificateCollection_t1197680765 * L_20 = InstallRootCerts_DecodeDefaultCollection_m56704705(NULL /*static, unused*/, /*hidden argument*/NULL);
			V_5 = L_20;
			FirebaseApp_t210707726 * L_21 = ___app0;
			X509CertificateCollection_t1197680765 * L_22 = InstallRootCerts_DecodeCollection_m3158849309(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
			V_6 = L_22;
			FirebaseApp_t210707726 * L_23 = ___app0;
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
			FirebaseApp_t210707726 * L_24 = FirebaseApp_get_DefaultInstance_m465202029(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((!(((Il2CppObject*)(FirebaseApp_t210707726 *)L_23) == ((Il2CppObject*)(FirebaseApp_t210707726 *)L_24))))
			{
				goto IL_00ce;
			}
		}

IL_00bd:
		{
			X509CertificateCollection_t1197680765 * L_25 = V_6;
			X509CertificateCollection_t1197680765 * L_26 = V_5;
			NullCheck(L_25);
			X509CertificateCollection_AddRange_m2499603020(L_25, L_26, /*hidden argument*/NULL);
			X509CertificateCollection_t1197680765 * L_27 = V_6;
			V_1 = L_27;
			goto IL_00d6;
		}

IL_00ce:
		{
			X509CertificateCollection_t1197680765 * L_28 = V_1;
			X509CertificateCollection_t1197680765 * L_29 = V_5;
			NullCheck(L_28);
			X509CertificateCollection_AddRange_m2499603020(L_28, L_29, /*hidden argument*/NULL);
		}

IL_00d6:
		{
			IL2CPP_RUNTIME_CLASS_INIT(InstallRootCerts_t1106242208_il2cpp_TypeInfo_var);
			Dictionary_2_t1664293826 * L_30 = ((InstallRootCerts_t1106242208_StaticFields*)InstallRootCerts_t1106242208_il2cpp_TypeInfo_var->static_fields)->get__installedRoots_1();
			FirebaseApp_t210707726 * L_31 = ___app0;
			X509CertificateCollection_t1197680765 * L_32 = V_6;
			NullCheck(L_30);
			Dictionary_2_set_Item_m1470897782(L_30, L_31, L_32, /*hidden argument*/Dictionary_2_set_Item_m1470897782_MethodInfo_var);
			X509CertificateCollection_t1197680765 * L_33 = V_1;
			NullCheck(L_33);
			int32_t L_34 = CollectionBase_get_Count_m740218359(L_33, /*hidden argument*/NULL);
			if (L_34)
			{
				goto IL_00f5;
			}
		}

IL_00ee:
		{
			X509CertificateCollection_t1197680765 * L_35 = V_1;
			V_2 = L_35;
			IL2CPP_LEAVE(0x219, FINALLY_0212);
		}

IL_00f5:
		{
			String_t* L_36 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(InstallRootCerts_t1106242208_il2cpp_TypeInfo_var);
			String_t* L_37 = ((InstallRootCerts_t1106242208_StaticFields*)InstallRootCerts_t1106242208_il2cpp_TypeInfo_var->static_fields)->get_TrustedRoot_2();
			IL2CPP_RUNTIME_CLASS_INIT(Path_t41728875_il2cpp_TypeInfo_var);
			String_t* L_38 = Path_Combine_m3185811654(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/NULL);
			InstallRootCerts_InstallDefaultCRLs_m2824657295(NULL /*static, unused*/, _stringLiteral3762375568, L_38, /*hidden argument*/NULL);
			String_t* L_39 = V_3;
			String_t* L_40 = ((InstallRootCerts_t1106242208_StaticFields*)InstallRootCerts_t1106242208_il2cpp_TypeInfo_var->static_fields)->get_IntermediateCA_3();
			String_t* L_41 = Path_Combine_m3185811654(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
			InstallRootCerts_InstallDefaultCRLs_m2824657295(NULL /*static, unused*/, _stringLiteral3610790251, L_41, /*hidden argument*/NULL);
			X509CertificateCollection_t1197680765 * L_42 = V_6;
			NullCheck(L_42);
			int32_t L_43 = CollectionBase_get_Count_m740218359(L_42, /*hidden argument*/NULL);
			int32_t L_44 = L_43;
			Il2CppObject * L_45 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_44);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_46 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral1093711738, L_45, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
			FirebaseHandler_LogMessage_m1734897910(NULL /*static, unused*/, 1, L_46, /*hidden argument*/NULL);
			String_t* L_47 = ((InstallRootCerts_t1106242208_StaticFields*)InstallRootCerts_t1106242208_il2cpp_TypeInfo_var->static_fields)->get_TrustedRoot_2();
			X509Store_t1617430119 * L_48 = (X509Store_t1617430119 *)il2cpp_codegen_object_new(X509Store_t1617430119_il2cpp_TypeInfo_var);
			X509Store__ctor_m325509164(L_48, L_47, /*hidden argument*/NULL);
			V_7 = L_48;
			X509Store_t1617430119 * L_49 = V_7;
			NullCheck(L_49);
			X509Store_Open_m3416485546(L_49, 1, /*hidden argument*/NULL);
			X509Store_t1617430119 * L_50 = V_7;
			NullCheck(L_50);
			X509Certificate2Collection_t1108969367 * L_51 = X509Store_get_Certificates_m329642048(L_50, /*hidden argument*/NULL);
			V_8 = L_51;
			X509CertificateCollection_t1197680765 * L_52 = V_1;
			NullCheck(L_52);
			X509CertificateEnumerator_t1208230922 * L_53 = X509CertificateCollection_GetEnumerator_m1618085929(L_52, /*hidden argument*/NULL);
			V_10 = L_53;
		}

IL_0160:
		try
		{ // begin try (depth: 2)
			{
				goto IL_01a2;
			}

IL_0165:
			{
				X509CertificateEnumerator_t1208230922 * L_54 = V_10;
				NullCheck(L_54);
				X509Certificate_t283079845 * L_55 = X509CertificateEnumerator_get_Current_m3202551321(L_54, /*hidden argument*/NULL);
				V_9 = L_55;
				X509CertificateCollection_t1197680765 * L_56 = V_8;
				X509Certificate_t283079845 * L_57 = V_9;
				NullCheck(L_56);
				bool L_58 = X509CertificateCollection_Contains_m470372483(L_56, L_57, /*hidden argument*/NULL);
				if (L_58)
				{
					goto IL_01a2;
				}
			}

IL_017c:
			try
			{ // begin try (depth: 3)
				X509Store_t1617430119 * L_59 = V_7;
				X509Certificate_t283079845 * L_60 = V_9;
				NullCheck(L_59);
				X509Store_Add_m11201059(L_59, ((X509Certificate2_t4056456767 *)CastclassClass(L_60, X509Certificate2_t4056456767_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
				goto IL_01a2;
			} // end try (depth: 3)
			catch(Il2CppExceptionWrapper& e)
			{
				__exception_local = (Exception_t1927440687 *)e.ex;
				if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
					goto CATCH_018f;
				throw e;
			}

CATCH_018f:
			{ // begin catch(System.Exception)
				V_11 = ((Exception_t1927440687 *)__exception_local);
				Exception_t1927440687 * L_61 = V_11;
				NullCheck(L_61);
				String_t* L_62 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_61);
				IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
				Debug_LogError_m3715728798(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
				goto IL_01a2;
			} // end catch (depth: 3)

IL_01a2:
			{
				X509CertificateEnumerator_t1208230922 * L_63 = V_10;
				NullCheck(L_63);
				bool L_64 = X509CertificateEnumerator_MoveNext_m1691973768(L_63, /*hidden argument*/NULL);
				if (L_64)
				{
					goto IL_0165;
				}
			}

IL_01ae:
			{
				IL2CPP_LEAVE(0x1CA, FINALLY_01b3);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_01b3;
		}

FINALLY_01b3:
		{ // begin finally (depth: 2)
			{
				X509CertificateEnumerator_t1208230922 * L_65 = V_10;
				Il2CppObject * L_66 = ((Il2CppObject *)IsInst(L_65, IDisposable_t2427283555_il2cpp_TypeInfo_var));
				V_12 = L_66;
				if (!L_66)
				{
					goto IL_01c9;
				}
			}

IL_01c2:
			{
				Il2CppObject * L_67 = V_12;
				NullCheck(L_67);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_67);
			}

IL_01c9:
			{
				IL2CPP_END_FINALLY(435)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(435)
		{
			IL2CPP_JUMP_TBL(0x1CA, IL_01ca)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_01ca:
		{
			X509Store_t1617430119 * L_68 = V_7;
			NullCheck(L_68);
			X509Store_Close_m2412711366(L_68, /*hidden argument*/NULL);
			int32_t L_69 = AppUtil_AppGetLogLevel_m2540188292(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((int32_t)L_69) > ((int32_t)1)))
			{
				goto IL_020b;
			}
		}

IL_01dc:
		{
			IL2CPP_RUNTIME_CLASS_INIT(ServicePointManager_t745663000_il2cpp_TypeInfo_var);
			RemoteCertificateValidationCallback_t2756269959 * L_70 = ServicePointManager_get_ServerCertificateValidationCallback_m1419353403(NULL /*static, unused*/, /*hidden argument*/NULL);
			if (L_70)
			{
				goto IL_020b;
			}
		}

IL_01e6:
		{
			U3CProcessU3Ec__AnonStorey0_t1447134549 * L_71 = (U3CProcessU3Ec__AnonStorey0_t1447134549 *)il2cpp_codegen_object_new(U3CProcessU3Ec__AnonStorey0_t1447134549_il2cpp_TypeInfo_var);
			U3CProcessU3Ec__AnonStorey0__ctor_m1025331492(L_71, /*hidden argument*/NULL);
			V_13 = L_71;
			U3CProcessU3Ec__AnonStorey0_t1447134549 * L_72 = V_13;
			IL2CPP_RUNTIME_CLASS_INIT(ServicePointManager_t745663000_il2cpp_TypeInfo_var);
			RemoteCertificateValidationCallback_t2756269959 * L_73 = ServicePointManager_get_ServerCertificateValidationCallback_m1419353403(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck(L_72);
			L_72->set_originalCallback_0(L_73);
			U3CProcessU3Ec__AnonStorey0_t1447134549 * L_74 = V_13;
			IntPtr_t L_75;
			L_75.set_m_value_0((void*)(void*)U3CProcessU3Ec__AnonStorey0_U3CU3Em__0_m2591264586_MethodInfo_var);
			RemoteCertificateValidationCallback_t2756269959 * L_76 = (RemoteCertificateValidationCallback_t2756269959 *)il2cpp_codegen_object_new(RemoteCertificateValidationCallback_t2756269959_il2cpp_TypeInfo_var);
			RemoteCertificateValidationCallback__ctor_m2946714095(L_76, L_74, L_75, /*hidden argument*/NULL);
			ServicePointManager_set_ServerCertificateValidationCallback_m1450099506(NULL /*static, unused*/, L_76, /*hidden argument*/NULL);
		}

IL_020b:
		{
			X509CertificateCollection_t1197680765 * L_77 = V_1;
			V_2 = L_77;
			IL2CPP_LEAVE(0x219, FINALLY_0212);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0212;
	}

FINALLY_0212:
	{ // begin finally (depth: 1)
		Il2CppObject * L_78 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_78, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(530)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(530)
	{
		IL2CPP_JUMP_TBL(0x219, IL_0219)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0219:
	{
		X509CertificateCollection_t1197680765 * L_79 = V_2;
		return L_79;
	}
}
// System.Void Firebase.Internal.InstallRootCerts::.cctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* InstallRootCerts_t1106242208_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1664293826_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2981090045_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3060525540;
extern Il2CppCodeGenString* _stringLiteral3560391898;
extern const uint32_t InstallRootCerts__cctor_m516906450_MetadataUsageId;
extern "C"  void InstallRootCerts__cctor_m516906450 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InstallRootCerts__cctor_m516906450_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		((InstallRootCerts_t1106242208_StaticFields*)InstallRootCerts_t1106242208_il2cpp_TypeInfo_var->static_fields)->set_Sync_0(L_0);
		Dictionary_2_t1664293826 * L_1 = (Dictionary_2_t1664293826 *)il2cpp_codegen_object_new(Dictionary_2_t1664293826_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2981090045(L_1, /*hidden argument*/Dictionary_2__ctor_m2981090045_MethodInfo_var);
		((InstallRootCerts_t1106242208_StaticFields*)InstallRootCerts_t1106242208_il2cpp_TypeInfo_var->static_fields)->set__installedRoots_1(L_1);
		((InstallRootCerts_t1106242208_StaticFields*)InstallRootCerts_t1106242208_il2cpp_TypeInfo_var->static_fields)->set_TrustedRoot_2(_stringLiteral3060525540);
		((InstallRootCerts_t1106242208_StaticFields*)InstallRootCerts_t1106242208_il2cpp_TypeInfo_var->static_fields)->set_IntermediateCA_3(_stringLiteral3560391898);
		return;
	}
}
// System.Void Firebase.Internal.InstallRootCerts/<Process>c__AnonStorey0::.ctor()
extern "C"  void U3CProcessU3Ec__AnonStorey0__ctor_m1025331492 (U3CProcessU3Ec__AnonStorey0_t1447134549 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Firebase.Internal.InstallRootCerts/<Process>c__AnonStorey0::<>m__0(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern Il2CppClass* SslPolicyErrors_t1928581431_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseHandler_t2907300047_il2cpp_TypeInfo_var;
extern Il2CppClass* X509ChainStatusFlags_t480677120_il2cpp_TypeInfo_var;
extern Il2CppClass* InstallRootCerts_t1106242208_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral957560885;
extern Il2CppCodeGenString* _stringLiteral1331448826;
extern const uint32_t U3CProcessU3Ec__AnonStorey0_U3CU3Em__0_m2591264586_MetadataUsageId;
extern "C"  bool U3CProcessU3Ec__AnonStorey0_U3CU3Em__0_m2591264586 (U3CProcessU3Ec__AnonStorey0_t1447134549 * __this, Il2CppObject * ___sender0, X509Certificate_t283079845 * ___certificate1, X509Chain_t777637347 * ___chain2, int32_t ___sslPolicyErrors3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CProcessU3Ec__AnonStorey0_U3CU3Em__0_m2591264586_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	X509ChainElement_t528874471 * V_1 = NULL;
	X509ChainElementEnumerator_t3304975821 * V_2 = NULL;
	X509ChainStatus_t4278378721  V_3;
	memset(&V_3, 0, sizeof(V_3));
	X509ChainStatusU5BU5D_t830390908* V_4 = NULL;
	int32_t V_5 = 0;
	X509ChainStatus_t4278378721  V_6;
	memset(&V_6, 0, sizeof(V_6));
	X509ChainStatusU5BU5D_t830390908* V_7 = NULL;
	int32_t V_8 = 0;
	X509ChainElement_t528874471 * V_9 = NULL;
	X509ChainElementEnumerator_t3304975821 * V_10 = NULL;
	bool G_B2_0 = false;
	bool G_B1_0 = false;
	int32_t G_B3_0 = 0;
	bool G_B3_1 = false;
	String_t* G_B15_0 = NULL;
	Il2CppObject * G_B15_1 = NULL;
	String_t* G_B15_2 = NULL;
	int32_t G_B15_3 = 0;
	String_t* G_B14_0 = NULL;
	Il2CppObject * G_B14_1 = NULL;
	String_t* G_B14_2 = NULL;
	int32_t G_B14_3 = 0;
	{
		int32_t L_0 = ___sslPolicyErrors3;
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_0;
		int32_t L_2 = ___sslPolicyErrors3;
		G_B1_0 = L_1;
		if ((!(((uint32_t)L_2) == ((uint32_t)4))))
		{
			G_B2_0 = L_1;
			goto IL_001a;
		}
	}
	{
		X509Chain_t777637347 * L_3 = ___chain2;
		NullCheck(L_3);
		X509ChainStatusU5BU5D_t830390908* L_4 = X509Chain_get_ChainStatus_m1863492671(L_3, /*hidden argument*/NULL);
		G_B3_0 = ((((Il2CppObject*)(X509ChainStatusU5BU5D_t830390908*)L_4) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		G_B3_1 = G_B1_0;
		goto IL_001b;
	}

IL_001a:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_001b:
	{
		V_0 = (bool)((int32_t)((int32_t)G_B3_1|(int32_t)G_B3_0));
		X509Chain_t777637347 * L_5 = ___chain2;
		NullCheck(L_5);
		X509ChainElementCollection_t2081831987 * L_6 = X509Chain_get_ChainElements_m2489870846(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		X509ChainElementEnumerator_t3304975821 * L_7 = X509ChainElementCollection_GetEnumerator_m3367250365(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		goto IL_007d;
	}

IL_002e:
	{
		X509ChainElementEnumerator_t3304975821 * L_8 = V_2;
		NullCheck(L_8);
		X509ChainElement_t528874471 * L_9 = X509ChainElementEnumerator_get_Current_m2232618221(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		X509ChainElement_t528874471 * L_10 = V_1;
		NullCheck(L_10);
		X509ChainStatusU5BU5D_t830390908* L_11 = X509ChainElement_get_ChainElementStatus_m2778866439(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_007d;
		}
	}
	{
		X509ChainElement_t528874471 * L_12 = V_1;
		NullCheck(L_12);
		X509ChainStatusU5BU5D_t830390908* L_13 = X509ChainElement_get_ChainElementStatus_m2778866439(L_12, /*hidden argument*/NULL);
		V_4 = L_13;
		V_5 = 0;
		goto IL_0072;
	}

IL_0050:
	{
		X509ChainStatusU5BU5D_t830390908* L_14 = V_4;
		int32_t L_15 = V_5;
		NullCheck(L_14);
		V_3 = (*(X509ChainStatus_t4278378721 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_15))));
		bool L_16 = V_0;
		int32_t L_17 = X509ChainStatus_get_Status_m3251168011((&V_3), /*hidden argument*/NULL);
		V_0 = (bool)((int32_t)((int32_t)L_16&(int32_t)((((int32_t)L_17) == ((int32_t)0))? 1 : 0)));
		int32_t L_18 = V_5;
		V_5 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_19 = V_5;
		X509ChainStatusU5BU5D_t830390908* L_20 = V_4;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_0050;
		}
	}

IL_007d:
	{
		X509ChainElementEnumerator_t3304975821 * L_21 = V_2;
		NullCheck(L_21);
		bool L_22 = X509ChainElementEnumerator_MoveNext_m3157555152(L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_002e;
		}
	}
	{
		bool L_23 = V_0;
		if (L_23)
		{
			goto IL_0167;
		}
	}
	{
		int32_t L_24 = ___sslPolicyErrors3;
		int32_t L_25 = L_24;
		Il2CppObject * L_26 = Box(SslPolicyErrors_t1928581431_il2cpp_TypeInfo_var, &L_25);
		Il2CppObject * L_27 = Box(SslPolicyErrors_t1928581431_il2cpp_TypeInfo_var, (&___sslPolicyErrors3));
		NullCheck(L_27);
		String_t* L_28 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral957560885, L_26, L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
		FirebaseHandler_LogMessage_m1734897910(NULL /*static, unused*/, 1, L_29, /*hidden argument*/NULL);
		int32_t L_30 = ___sslPolicyErrors3;
		if ((!(((uint32_t)L_30) == ((uint32_t)4))))
		{
			goto IL_0125;
		}
	}
	{
		X509Chain_t777637347 * L_31 = ___chain2;
		NullCheck(L_31);
		X509ChainStatusU5BU5D_t830390908* L_32 = X509Chain_get_ChainStatus_m1863492671(L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0125;
		}
	}
	{
		X509Chain_t777637347 * L_33 = ___chain2;
		NullCheck(L_33);
		X509ChainStatusU5BU5D_t830390908* L_34 = X509Chain_get_ChainStatus_m1863492671(L_33, /*hidden argument*/NULL);
		V_7 = L_34;
		V_8 = 0;
		goto IL_011a;
	}

IL_00d5:
	{
		X509ChainStatusU5BU5D_t830390908* L_35 = V_7;
		int32_t L_36 = V_8;
		NullCheck(L_35);
		V_6 = (*(X509ChainStatus_t4278378721 *)((L_35)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_36))));
		int32_t L_37 = X509ChainStatus_get_Status_m3251168011((&V_6), /*hidden argument*/NULL);
		int32_t L_38 = L_37;
		Il2CppObject * L_39 = Box(X509ChainStatusFlags_t480677120_il2cpp_TypeInfo_var, &L_38);
		String_t* L_40 = X509ChainStatus_get_StatusInformation_m3046569674((&V_6), /*hidden argument*/NULL);
		String_t* L_41 = L_40;
		G_B14_0 = L_41;
		G_B14_1 = L_39;
		G_B14_2 = _stringLiteral1331448826;
		G_B14_3 = 1;
		if (L_41)
		{
			G_B15_0 = L_41;
			G_B15_1 = L_39;
			G_B15_2 = _stringLiteral1331448826;
			G_B15_3 = 1;
			goto IL_010a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_42 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B15_0 = L_42;
		G_B15_1 = G_B14_1;
		G_B15_2 = G_B14_2;
		G_B15_3 = G_B14_3;
	}

IL_010a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_43 = String_Format_m1811873526(NULL /*static, unused*/, G_B15_2, G_B15_1, G_B15_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
		FirebaseHandler_LogMessage_m1734897910(NULL /*static, unused*/, G_B15_3, L_43, /*hidden argument*/NULL);
		int32_t L_44 = V_8;
		V_8 = ((int32_t)((int32_t)L_44+(int32_t)1));
	}

IL_011a:
	{
		int32_t L_45 = V_8;
		X509ChainStatusU5BU5D_t830390908* L_46 = V_7;
		NullCheck(L_46);
		if ((((int32_t)L_45) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_46)->max_length)))))))
		{
			goto IL_00d5;
		}
	}

IL_0125:
	{
		X509Chain_t777637347 * L_47 = ___chain2;
		NullCheck(L_47);
		X509ChainElementCollection_t2081831987 * L_48 = X509Chain_get_ChainElements_m2489870846(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		X509ChainElementEnumerator_t3304975821 * L_49 = X509ChainElementCollection_GetEnumerator_m3367250365(L_48, /*hidden argument*/NULL);
		V_10 = L_49;
		goto IL_015b;
	}

IL_0137:
	{
		X509ChainElementEnumerator_t3304975821 * L_50 = V_10;
		NullCheck(L_50);
		X509ChainElement_t528874471 * L_51 = X509ChainElementEnumerator_get_Current_m2232618221(L_50, /*hidden argument*/NULL);
		V_9 = L_51;
		X509ChainElement_t528874471 * L_52 = V_9;
		NullCheck(L_52);
		X509Certificate2_t4056456767 * L_53 = X509ChainElement_get_Certificate_m3929662793(L_52, /*hidden argument*/NULL);
		X509ChainElement_t528874471 * L_54 = V_9;
		NullCheck(L_54);
		X509ChainStatusU5BU5D_t830390908* L_55 = X509ChainElement_get_ChainElementStatus_m2778866439(L_54, /*hidden argument*/NULL);
		X509ChainElement_t528874471 * L_56 = V_9;
		NullCheck(L_56);
		String_t* L_57 = X509ChainElement_get_Information_m2538337562(L_56, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(InstallRootCerts_t1106242208_il2cpp_TypeInfo_var);
		InstallRootCerts_PrintCert_m2117415721(NULL /*static, unused*/, 1, L_53, L_55, L_57, /*hidden argument*/NULL);
	}

IL_015b:
	{
		X509ChainElementEnumerator_t3304975821 * L_58 = V_10;
		NullCheck(L_58);
		bool L_59 = X509ChainElementEnumerator_MoveNext_m3157555152(L_58, /*hidden argument*/NULL);
		if (L_59)
		{
			goto IL_0137;
		}
	}

IL_0167:
	{
		bool L_60 = V_0;
		if (L_60)
		{
			goto IL_0189;
		}
	}
	{
		RemoteCertificateValidationCallback_t2756269959 * L_61 = __this->get_originalCallback_0();
		if (!L_61)
		{
			goto IL_0189;
		}
	}
	{
		RemoteCertificateValidationCallback_t2756269959 * L_62 = __this->get_originalCallback_0();
		Il2CppObject * L_63 = ___sender0;
		X509Certificate_t283079845 * L_64 = ___certificate1;
		X509Chain_t777637347 * L_65 = ___chain2;
		int32_t L_66 = ___sslPolicyErrors3;
		NullCheck(L_62);
		bool L_67 = RemoteCertificateValidationCallback_Invoke_m3011066238(L_62, L_63, L_64, L_65, L_66, /*hidden argument*/NULL);
		return L_67;
	}

IL_0189:
	{
		bool L_68 = V_0;
		return L_68;
	}
}
// System.Void Firebase.UnitySynchronizationContext::.ctor(UnityEngine.GameObject)
extern Il2CppClass* Thread_t241561612_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisSynchronizationContextBehavoir_t692674473_m2263356785_MethodInfo_var;
extern const uint32_t UnitySynchronizationContext__ctor_m1343069354_MetadataUsageId;
extern "C"  void UnitySynchronizationContext__ctor_m1343069354 (UnitySynchronizationContext_t1051733884 * __this, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext__ctor_m1343069354_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SynchronizationContext__ctor_m1576930414(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t241561612_il2cpp_TypeInfo_var);
		Thread_t241561612 * L_0 = Thread_get_CurrentThread_m3667342817(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Thread_get_ManagedThreadId_m1995754972(L_0, /*hidden argument*/NULL);
		__this->set_mainThreadId_4(L_1);
		GameObject_t1756533147 * L_2 = ___gameObject0;
		NullCheck(L_2);
		SynchronizationContextBehavoir_t692674473 * L_3 = GameObject_AddComponent_TisSynchronizationContextBehavoir_t692674473_m2263356785(L_2, /*hidden argument*/GameObject_AddComponent_TisSynchronizationContextBehavoir_t692674473_m2263356785_MethodInfo_var);
		__this->set_behavior_3(L_3);
		SynchronizationContextBehavoir_t692674473 * L_4 = __this->get_behavior_3();
		NullCheck(L_4);
		Queue_1_t279829387 * L_5 = SynchronizationContextBehavoir_get_CallbackQueue_m2312663759(L_4, /*hidden argument*/NULL);
		__this->set_queue_2(L_5);
		return;
	}
}
// Firebase.UnitySynchronizationContext Firebase.UnitySynchronizationContext::get_Instance()
extern Il2CppClass* UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3728509879;
extern const uint32_t UnitySynchronizationContext_get_Instance_m3676127900_MetadataUsageId;
extern "C"  UnitySynchronizationContext_t1051733884 * UnitySynchronizationContext_get_Instance_m3676127900 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext_get_Instance_m3676127900_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var);
		UnitySynchronizationContext_t1051733884 * L_0 = ((UnitySynchronizationContext_t1051733884_StaticFields*)UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var->static_fields)->get__instance_1();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, _stringLiteral3728509879, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var);
		UnitySynchronizationContext_t1051733884 * L_2 = ((UnitySynchronizationContext_t1051733884_StaticFields*)UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var->static_fields)->get__instance_1();
		return L_2;
	}
}
// System.Void Firebase.UnitySynchronizationContext::Install(UnityEngine.GameObject)
extern Il2CppClass* UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var;
extern const uint32_t UnitySynchronizationContext_Install_m3862032013_MetadataUsageId;
extern "C"  void UnitySynchronizationContext_Install_m3862032013 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext_Install_m3862032013_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var);
		UnitySynchronizationContext_t1051733884 * L_0 = ((UnitySynchronizationContext_t1051733884_StaticFields*)UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var->static_fields)->get__instance_1();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		GameObject_t1756533147 * L_1 = ___gameObject0;
		UnitySynchronizationContext_t1051733884 * L_2 = (UnitySynchronizationContext_t1051733884 *)il2cpp_codegen_object_new(UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var);
		UnitySynchronizationContext__ctor_m1343069354(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var);
		((UnitySynchronizationContext_t1051733884_StaticFields*)UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var->static_fields)->set__instance_1(L_2);
	}

IL_0015:
	{
		SynchronizationContext_t3857790437 * L_3 = SynchronizationContext_get_Current_m2617498083(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var);
		UnitySynchronizationContext_t1051733884 * L_4 = ((UnitySynchronizationContext_t1051733884_StaticFields*)UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var->static_fields)->get__instance_1();
		SynchronizationContext_SetSynchronizationContext_m951246343(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Threading.ManualResetEvent Firebase.UnitySynchronizationContext::GetThreadEvent()
extern Il2CppClass* UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var;
extern Il2CppClass* Thread_t241561612_il2cpp_TypeInfo_var;
extern Il2CppClass* ManualResetEvent_t926074657_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m3408234746_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m2770759310_MethodInfo_var;
extern const uint32_t UnitySynchronizationContext_GetThreadEvent_m2649321943_MetadataUsageId;
extern "C"  ManualResetEvent_t926074657 * UnitySynchronizationContext_GetThreadEvent_m2649321943 (UnitySynchronizationContext_t1051733884 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext_GetThreadEvent_m2649321943_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ManualResetEvent_t926074657 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var);
		Dictionary_2_t4228867588 * L_0 = ((UnitySynchronizationContext_t1051733884_StaticFields*)UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var->static_fields)->get_signalDictionary_5();
		V_1 = L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var);
			Dictionary_2_t4228867588 * L_2 = ((UnitySynchronizationContext_t1051733884_StaticFields*)UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var->static_fields)->get_signalDictionary_5();
			IL2CPP_RUNTIME_CLASS_INIT(Thread_t241561612_il2cpp_TypeInfo_var);
			Thread_t241561612 * L_3 = Thread_get_CurrentThread_m3667342817(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck(L_3);
			int32_t L_4 = Thread_get_ManagedThreadId_m1995754972(L_3, /*hidden argument*/NULL);
			NullCheck(L_2);
			bool L_5 = Dictionary_2_TryGetValue_m3408234746(L_2, L_4, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m3408234746_MethodInfo_var);
			if (L_5)
			{
				goto IL_0043;
			}
		}

IL_0027:
		{
			ManualResetEvent_t926074657 * L_6 = (ManualResetEvent_t926074657 *)il2cpp_codegen_object_new(ManualResetEvent_t926074657_il2cpp_TypeInfo_var);
			ManualResetEvent__ctor_m3470249043(L_6, (bool)0, /*hidden argument*/NULL);
			V_0 = L_6;
			IL2CPP_RUNTIME_CLASS_INIT(UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var);
			Dictionary_2_t4228867588 * L_7 = ((UnitySynchronizationContext_t1051733884_StaticFields*)UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var->static_fields)->get_signalDictionary_5();
			IL2CPP_RUNTIME_CLASS_INIT(Thread_t241561612_il2cpp_TypeInfo_var);
			Thread_t241561612 * L_8 = Thread_get_CurrentThread_m3667342817(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck(L_8);
			int32_t L_9 = Thread_get_ManagedThreadId_m1995754972(L_8, /*hidden argument*/NULL);
			ManualResetEvent_t926074657 * L_10 = V_0;
			NullCheck(L_7);
			Dictionary_2_set_Item_m2770759310(L_7, L_9, L_10, /*hidden argument*/Dictionary_2_set_Item_m2770759310_MethodInfo_var);
		}

IL_0043:
		{
			IL2CPP_LEAVE(0x4F, FINALLY_0048);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0048;
	}

FINALLY_0048:
	{ // begin finally (depth: 1)
		Il2CppObject * L_11 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(72)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(72)
	{
		IL2CPP_JUMP_TBL(0x4F, IL_004f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_004f:
	{
		ManualResetEvent_t926074657 * L_12 = V_0;
		NullCheck(L_12);
		EventWaitHandle_Reset_m779895263(L_12, /*hidden argument*/NULL);
		ManualResetEvent_t926074657 * L_13 = V_0;
		return L_13;
	}
}
// System.Void Firebase.UnitySynchronizationContext::PostCoroutine(System.Func`1<System.Collections.IEnumerator>)
extern Il2CppClass* SendOrPostCallback_t296893742_il2cpp_TypeInfo_var;
extern const MethodInfo* UnitySynchronizationContext_U3CPostCoroutineU3Em__0_m2310698975_MethodInfo_var;
extern const uint32_t UnitySynchronizationContext_PostCoroutine_m567443941_MetadataUsageId;
extern "C"  void UnitySynchronizationContext_PostCoroutine_m567443941 (UnitySynchronizationContext_t1051733884 * __this, Func_1_t3420419431 * ___coroutine0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext_PostCoroutine_m567443941_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)UnitySynchronizationContext_U3CPostCoroutineU3Em__0_m2310698975_MethodInfo_var);
		SendOrPostCallback_t296893742 * L_1 = (SendOrPostCallback_t296893742 *)il2cpp_codegen_object_new(SendOrPostCallback_t296893742_il2cpp_TypeInfo_var);
		SendOrPostCallback__ctor_m107067079(L_1, __this, L_0, /*hidden argument*/NULL);
		Func_1_t3420419431 * L_2 = ___coroutine0;
		VirtActionInvoker2< SendOrPostCallback_t296893742 *, Il2CppObject * >::Invoke(4 /* System.Void System.Threading.SynchronizationContext::Post(System.Threading.SendOrPostCallback,System.Object) */, __this, L_1, L_2);
		return;
	}
}
// System.Collections.IEnumerator Firebase.UnitySynchronizationContext::SignaledCoroutine(System.Func`1<System.Collections.IEnumerator>,System.Threading.ManualResetEvent)
extern Il2CppClass* U3CSignaledCoroutineU3Ec__Iterator0_t3693910080_il2cpp_TypeInfo_var;
extern const uint32_t UnitySynchronizationContext_SignaledCoroutine_m2712439634_MetadataUsageId;
extern "C"  Il2CppObject * UnitySynchronizationContext_SignaledCoroutine_m2712439634 (UnitySynchronizationContext_t1051733884 * __this, Func_1_t3420419431 * ___coroutine0, ManualResetEvent_t926074657 * ___newSignal1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext_SignaledCoroutine_m2712439634_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSignaledCoroutineU3Ec__Iterator0_t3693910080 * V_0 = NULL;
	{
		U3CSignaledCoroutineU3Ec__Iterator0_t3693910080 * L_0 = (U3CSignaledCoroutineU3Ec__Iterator0_t3693910080 *)il2cpp_codegen_object_new(U3CSignaledCoroutineU3Ec__Iterator0_t3693910080_il2cpp_TypeInfo_var);
		U3CSignaledCoroutineU3Ec__Iterator0__ctor_m3075902269(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSignaledCoroutineU3Ec__Iterator0_t3693910080 * L_1 = V_0;
		Func_1_t3420419431 * L_2 = ___coroutine0;
		NullCheck(L_1);
		L_1->set_coroutine_0(L_2);
		U3CSignaledCoroutineU3Ec__Iterator0_t3693910080 * L_3 = V_0;
		ManualResetEvent_t926074657 * L_4 = ___newSignal1;
		NullCheck(L_3);
		L_3->set_newSignal_1(L_4);
		U3CSignaledCoroutineU3Ec__Iterator0_t3693910080 * L_5 = V_0;
		return L_5;
	}
}
// System.Void Firebase.UnitySynchronizationContext::SendCoroutine(System.Func`1<System.Collections.IEnumerator>)
extern Il2CppClass* U3CSendCoroutineU3Ec__AnonStorey1_t3854140196_il2cpp_TypeInfo_var;
extern Il2CppClass* Thread_t241561612_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CSendCoroutineU3Ec__AnonStorey2_t3450855669_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_1_t3420419431_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_1_Invoke_m3170726294_MethodInfo_var;
extern const MethodInfo* U3CSendCoroutineU3Ec__AnonStorey2_U3CU3Em__0_m2167246975_MethodInfo_var;
extern const MethodInfo* Func_1__ctor_m3632084784_MethodInfo_var;
extern const uint32_t UnitySynchronizationContext_SendCoroutine_m3987642997_MetadataUsageId;
extern "C"  void UnitySynchronizationContext_SendCoroutine_m3987642997 (UnitySynchronizationContext_t1051733884 * __this, Func_1_t3420419431 * ___coroutine0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext_SendCoroutine_m3987642997_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSendCoroutineU3Ec__AnonStorey1_t3854140196 * V_0 = NULL;
	U3CSendCoroutineU3Ec__AnonStorey2_t3450855669 * V_1 = NULL;
	{
		U3CSendCoroutineU3Ec__AnonStorey1_t3854140196 * L_0 = (U3CSendCoroutineU3Ec__AnonStorey1_t3854140196 *)il2cpp_codegen_object_new(U3CSendCoroutineU3Ec__AnonStorey1_t3854140196_il2cpp_TypeInfo_var);
		U3CSendCoroutineU3Ec__AnonStorey1__ctor_m567649193(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSendCoroutineU3Ec__AnonStorey1_t3854140196 * L_1 = V_0;
		Func_1_t3420419431 * L_2 = ___coroutine0;
		NullCheck(L_1);
		L_1->set_coroutine_0(L_2);
		U3CSendCoroutineU3Ec__AnonStorey1_t3854140196 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_1(__this);
		int32_t L_4 = __this->get_mainThreadId_4();
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t241561612_il2cpp_TypeInfo_var);
		Thread_t241561612 * L_5 = Thread_get_CurrentThread_m3667342817(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = Thread_get_ManagedThreadId_m1995754972(L_5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)L_6))))
		{
			goto IL_0045;
		}
	}
	{
		SynchronizationContextBehavoir_t692674473 * L_7 = __this->get_behavior_3();
		U3CSendCoroutineU3Ec__AnonStorey1_t3854140196 * L_8 = V_0;
		NullCheck(L_8);
		Func_1_t3420419431 * L_9 = L_8->get_coroutine_0();
		NullCheck(L_9);
		Il2CppObject * L_10 = Func_1_Invoke_m3170726294(L_9, /*hidden argument*/Func_1_Invoke_m3170726294_MethodInfo_var);
		NullCheck(L_7);
		MonoBehaviour_StartCoroutine_m2470621050(L_7, L_10, /*hidden argument*/NULL);
		goto IL_0081;
	}

IL_0045:
	{
		U3CSendCoroutineU3Ec__AnonStorey2_t3450855669 * L_11 = (U3CSendCoroutineU3Ec__AnonStorey2_t3450855669 *)il2cpp_codegen_object_new(U3CSendCoroutineU3Ec__AnonStorey2_t3450855669_il2cpp_TypeInfo_var);
		U3CSendCoroutineU3Ec__AnonStorey2__ctor_m568898540(L_11, /*hidden argument*/NULL);
		V_1 = L_11;
		U3CSendCoroutineU3Ec__AnonStorey2_t3450855669 * L_12 = V_1;
		U3CSendCoroutineU3Ec__AnonStorey1_t3854140196 * L_13 = V_0;
		NullCheck(L_12);
		L_12->set_U3CU3Ef__refU241_1(L_13);
		U3CSendCoroutineU3Ec__AnonStorey2_t3450855669 * L_14 = V_1;
		ManualResetEvent_t926074657 * L_15 = UnitySynchronizationContext_GetThreadEvent_m2649321943(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		L_14->set_newSignal_0(L_15);
		U3CSendCoroutineU3Ec__AnonStorey2_t3450855669 * L_16 = V_1;
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)U3CSendCoroutineU3Ec__AnonStorey2_U3CU3Em__0_m2167246975_MethodInfo_var);
		Func_1_t3420419431 * L_18 = (Func_1_t3420419431 *)il2cpp_codegen_object_new(Func_1_t3420419431_il2cpp_TypeInfo_var);
		Func_1__ctor_m3632084784(L_18, L_16, L_17, /*hidden argument*/Func_1__ctor_m3632084784_MethodInfo_var);
		UnitySynchronizationContext_PostCoroutine_m567443941(__this, L_18, /*hidden argument*/NULL);
		U3CSendCoroutineU3Ec__AnonStorey2_t3450855669 * L_19 = V_1;
		NullCheck(L_19);
		ManualResetEvent_t926074657 * L_20 = L_19->get_newSignal_0();
		NullCheck(L_20);
		VirtFuncInvoker1< bool, int32_t >::Invoke(13 /* System.Boolean System.Threading.WaitHandle::WaitOne(System.Int32) */, L_20, ((int32_t)15000));
	}

IL_0081:
	{
		return;
	}
}
// System.Void Firebase.UnitySynchronizationContext::Post(System.Threading.SendOrPostCallback,System.Object)
extern Il2CppClass* Tuple_2_t460172552_il2cpp_TypeInfo_var;
extern const MethodInfo* Tuple_2__ctor_m4133476842_MethodInfo_var;
extern const MethodInfo* Queue_1_Enqueue_m2589358569_MethodInfo_var;
extern const uint32_t UnitySynchronizationContext_Post_m4267436977_MetadataUsageId;
extern "C"  void UnitySynchronizationContext_Post_m4267436977 (UnitySynchronizationContext_t1051733884 * __this, SendOrPostCallback_t296893742 * ___d0, Il2CppObject * ___state1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext_Post_m4267436977_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Queue_1_t279829387 * L_0 = __this->get_queue_2();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Queue_1_t279829387 * L_2 = __this->get_queue_2();
		SendOrPostCallback_t296893742 * L_3 = ___d0;
		Il2CppObject * L_4 = ___state1;
		Tuple_2_t460172552 * L_5 = (Tuple_2_t460172552 *)il2cpp_codegen_object_new(Tuple_2_t460172552_il2cpp_TypeInfo_var);
		Tuple_2__ctor_m4133476842(L_5, L_3, L_4, /*hidden argument*/Tuple_2__ctor_m4133476842_MethodInfo_var);
		NullCheck(L_2);
		Queue_1_Enqueue_m2589358569(L_2, L_5, /*hidden argument*/Queue_1_Enqueue_m2589358569_MethodInfo_var);
		IL2CPP_LEAVE(0x2B, FINALLY_0024);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0024;
	}

FINALLY_0024:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(36)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(36)
	{
		IL2CPP_JUMP_TBL(0x2B, IL_002b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_002b:
	{
		return;
	}
}
// System.Void Firebase.UnitySynchronizationContext::.cctor()
extern Il2CppClass* UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t4228867588_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m446622171_MethodInfo_var;
extern const uint32_t UnitySynchronizationContext__cctor_m3960838443_MetadataUsageId;
extern "C"  void UnitySynchronizationContext__cctor_m3960838443 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext__cctor_m3960838443_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((UnitySynchronizationContext_t1051733884_StaticFields*)UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var->static_fields)->set__instance_1((UnitySynchronizationContext_t1051733884 *)NULL);
		Dictionary_2_t4228867588 * L_0 = (Dictionary_2_t4228867588 *)il2cpp_codegen_object_new(Dictionary_2_t4228867588_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m446622171(L_0, /*hidden argument*/Dictionary_2__ctor_m446622171_MethodInfo_var);
		((UnitySynchronizationContext_t1051733884_StaticFields*)UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var->static_fields)->set_signalDictionary_5(L_0);
		return;
	}
}
// System.Void Firebase.UnitySynchronizationContext::<PostCoroutine>m__0(System.Object)
extern Il2CppClass* Func_1_t3420419431_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_1_Invoke_m3170726294_MethodInfo_var;
extern const uint32_t UnitySynchronizationContext_U3CPostCoroutineU3Em__0_m2310698975_MetadataUsageId;
extern "C"  void UnitySynchronizationContext_U3CPostCoroutineU3Em__0_m2310698975 (UnitySynchronizationContext_t1051733884 * __this, Il2CppObject * ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext_U3CPostCoroutineU3Em__0_m2310698975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Func_1_t3420419431 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___x0;
		V_0 = ((Func_1_t3420419431 *)CastclassSealed(L_0, Func_1_t3420419431_il2cpp_TypeInfo_var));
		SynchronizationContextBehavoir_t692674473 * L_1 = __this->get_behavior_3();
		Func_1_t3420419431 * L_2 = V_0;
		NullCheck(L_2);
		Il2CppObject * L_3 = Func_1_Invoke_m3170726294(L_2, /*hidden argument*/Func_1_Invoke_m3170726294_MethodInfo_var);
		NullCheck(L_1);
		MonoBehaviour_StartCoroutine_m2470621050(L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.UnitySynchronizationContext/<Send>c__AnonStorey3::.ctor()
extern "C"  void U3CSendU3Ec__AnonStorey3__ctor_m1310654673 (U3CSendU3Ec__AnonStorey3_t1465756544 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.UnitySynchronizationContext/<Send>c__AnonStorey4::.ctor()
extern "C"  void U3CSendU3Ec__AnonStorey4__ctor_m2424407232 (U3CSendU3Ec__AnonStorey4_t1465756541 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey1::.ctor()
extern "C"  void U3CSendCoroutineU3Ec__AnonStorey1__ctor_m567649193 (U3CSendCoroutineU3Ec__AnonStorey1_t3854140196 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey2::.ctor()
extern "C"  void U3CSendCoroutineU3Ec__AnonStorey2__ctor_m568898540 (U3CSendCoroutineU3Ec__AnonStorey2_t3450855669 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Firebase.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey2::<>m__0()
extern "C"  Il2CppObject * U3CSendCoroutineU3Ec__AnonStorey2_U3CU3Em__0_m2167246975 (U3CSendCoroutineU3Ec__AnonStorey2_t3450855669 * __this, const MethodInfo* method)
{
	{
		U3CSendCoroutineU3Ec__AnonStorey1_t3854140196 * L_0 = __this->get_U3CU3Ef__refU241_1();
		NullCheck(L_0);
		UnitySynchronizationContext_t1051733884 * L_1 = L_0->get_U24this_1();
		U3CSendCoroutineU3Ec__AnonStorey1_t3854140196 * L_2 = __this->get_U3CU3Ef__refU241_1();
		NullCheck(L_2);
		Func_1_t3420419431 * L_3 = L_2->get_coroutine_0();
		ManualResetEvent_t926074657 * L_4 = __this->get_newSignal_0();
		NullCheck(L_1);
		Il2CppObject * L_5 = UnitySynchronizationContext_SignaledCoroutine_m2712439634(L_1, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void Firebase.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::.ctor()
extern "C"  void U3CSignaledCoroutineU3Ec__Iterator0__ctor_m3075902269 (U3CSignaledCoroutineU3Ec__Iterator0_t3693910080 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Firebase.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::MoveNext()
extern const MethodInfo* Func_1_Invoke_m3170726294_MethodInfo_var;
extern const uint32_t U3CSignaledCoroutineU3Ec__Iterator0_MoveNext_m53105779_MetadataUsageId;
extern "C"  bool U3CSignaledCoroutineU3Ec__Iterator0_MoveNext_m53105779 (U3CSignaledCoroutineU3Ec__Iterator0_t3693910080 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSignaledCoroutineU3Ec__Iterator0_MoveNext_m53105779_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0046;
		}
	}
	{
		goto IL_0059;
	}

IL_0021:
	{
		Func_1_t3420419431 * L_2 = __this->get_coroutine_0();
		NullCheck(L_2);
		Il2CppObject * L_3 = Func_1_Invoke_m3170726294(L_2, /*hidden argument*/Func_1_Invoke_m3170726294_MethodInfo_var);
		__this->set_U24current_2(L_3);
		bool L_4 = __this->get_U24disposing_3();
		if (L_4)
		{
			goto IL_0041;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0041:
	{
		goto IL_005b;
	}

IL_0046:
	{
		ManualResetEvent_t926074657 * L_5 = __this->get_newSignal_1();
		NullCheck(L_5);
		EventWaitHandle_Set_m2975776670(L_5, /*hidden argument*/NULL);
		__this->set_U24PC_4((-1));
	}

IL_0059:
	{
		return (bool)0;
	}

IL_005b:
	{
		return (bool)1;
	}
}
// System.Object Firebase.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSignaledCoroutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4036536747 (U3CSignaledCoroutineU3Ec__Iterator0_t3693910080 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object Firebase.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSignaledCoroutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3163236739 (U3CSignaledCoroutineU3Ec__Iterator0_t3693910080 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void Firebase.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::Dispose()
extern "C"  void U3CSignaledCoroutineU3Ec__Iterator0_Dispose_m777660278 (U3CSignaledCoroutineU3Ec__Iterator0_t3693910080 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void Firebase.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CSignaledCoroutineU3Ec__Iterator0_Reset_m1946727928_MetadataUsageId;
extern "C"  void U3CSignaledCoroutineU3Ec__Iterator0_Reset_m1946727928 (U3CSignaledCoroutineU3Ec__Iterator0_t3693910080 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSignaledCoroutineU3Ec__Iterator0_Reset_m1946727928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir::.ctor()
extern "C"  void SynchronizationContextBehavoir__ctor_m3448616242 (SynchronizationContextBehavoir_t692674473 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>> Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir::get_CallbackQueue()
extern Il2CppClass* Queue_1_t279829387_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1__ctor_m4231618664_MethodInfo_var;
extern const uint32_t SynchronizationContextBehavoir_get_CallbackQueue_m2312663759_MetadataUsageId;
extern "C"  Queue_1_t279829387 * SynchronizationContextBehavoir_get_CallbackQueue_m2312663759 (SynchronizationContextBehavoir_t692674473 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SynchronizationContextBehavoir_get_CallbackQueue_m2312663759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Queue_1_t279829387 * L_0 = __this->get_callbackQueue_2();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Queue_1_t279829387 * L_1 = (Queue_1_t279829387 *)il2cpp_codegen_object_new(Queue_1_t279829387_il2cpp_TypeInfo_var);
		Queue_1__ctor_m4231618664(L_1, /*hidden argument*/Queue_1__ctor_m4231618664_MethodInfo_var);
		__this->set_callbackQueue_2(L_1);
	}

IL_0016:
	{
		Queue_1_t279829387 * L_2 = __this->get_callbackQueue_2();
		return L_2;
	}
}
// System.Collections.IEnumerator Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir::Start()
extern Il2CppClass* U3CStartU3Ec__Iterator0_t1310454857_il2cpp_TypeInfo_var;
extern const uint32_t SynchronizationContextBehavoir_Start_m3359373602_MetadataUsageId;
extern "C"  Il2CppObject * SynchronizationContextBehavoir_Start_m3359373602 (SynchronizationContextBehavoir_t692674473 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SynchronizationContextBehavoir_Start_m3359373602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CStartU3Ec__Iterator0_t1310454857 * V_0 = NULL;
	{
		U3CStartU3Ec__Iterator0_t1310454857 * L_0 = (U3CStartU3Ec__Iterator0_t1310454857 *)il2cpp_codegen_object_new(U3CStartU3Ec__Iterator0_t1310454857_il2cpp_TypeInfo_var);
		U3CStartU3Ec__Iterator0__ctor_m3829161492(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartU3Ec__Iterator0_t1310454857 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_2(__this);
		U3CStartU3Ec__Iterator0_t1310454857 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m3829161492 (U3CStartU3Ec__Iterator0_t1310454857 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::MoveNext()
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1_get_Count_m177438226_MethodInfo_var;
extern const MethodInfo* Queue_1_Dequeue_m1588528382_MethodInfo_var;
extern const MethodInfo* Tuple_2_get_Item1_m2647242292_MethodInfo_var;
extern const MethodInfo* Tuple_2_get_Item2_m3295131920_MethodInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m4183249272_MetadataUsageId;
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m4183249272 (U3CStartU3Ec__Iterator0_t1310454857 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m4183249272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00e8;
		}
	}
	{
		goto IL_00f4;
	}

IL_0021:
	{
		__this->set_U3CentryU3E__0_0((Tuple_2_t460172552 *)NULL);
		SynchronizationContextBehavoir_t692674473 * L_2 = __this->get_U24this_2();
		NullCheck(L_2);
		Queue_1_t279829387 * L_3 = SynchronizationContextBehavoir_get_CallbackQueue_m2312663759(L_2, /*hidden argument*/NULL);
		__this->set_U24locvar0_1(L_3);
		Il2CppObject * L_4 = __this->get_U24locvar0_1();
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0044:
	try
	{ // begin try (depth: 1)
		{
			SynchronizationContextBehavoir_t692674473 * L_5 = __this->get_U24this_2();
			NullCheck(L_5);
			Queue_1_t279829387 * L_6 = SynchronizationContextBehavoir_get_CallbackQueue_m2312663759(L_5, /*hidden argument*/NULL);
			NullCheck(L_6);
			int32_t L_7 = Queue_1_get_Count_m177438226(L_6, /*hidden argument*/Queue_1_get_Count_m177438226_MethodInfo_var);
			if ((((int32_t)L_7) <= ((int32_t)0)))
			{
				goto IL_0070;
			}
		}

IL_005a:
		{
			SynchronizationContextBehavoir_t692674473 * L_8 = __this->get_U24this_2();
			NullCheck(L_8);
			Queue_1_t279829387 * L_9 = SynchronizationContextBehavoir_get_CallbackQueue_m2312663759(L_8, /*hidden argument*/NULL);
			NullCheck(L_9);
			Tuple_2_t460172552 * L_10 = Queue_1_Dequeue_m1588528382(L_9, /*hidden argument*/Queue_1_Dequeue_m1588528382_MethodInfo_var);
			__this->set_U3CentryU3E__0_0(L_10);
		}

IL_0070:
		{
			IL2CPP_LEAVE(0x81, FINALLY_0075);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0075;
	}

FINALLY_0075:
	{ // begin finally (depth: 1)
		Il2CppObject * L_11 = __this->get_U24locvar0_1();
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(117)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(117)
	{
		IL2CPP_JUMP_TBL(0x81, IL_0081)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0081:
	{
		Tuple_2_t460172552 * L_12 = __this->get_U3CentryU3E__0_0();
		if (!L_12)
		{
			goto IL_00cd;
		}
	}
	{
		Tuple_2_t460172552 * L_13 = __this->get_U3CentryU3E__0_0();
		NullCheck(L_13);
		SendOrPostCallback_t296893742 * L_14 = Tuple_2_get_Item1_m2647242292(L_13, /*hidden argument*/Tuple_2_get_Item1_m2647242292_MethodInfo_var);
		if (!L_14)
		{
			goto IL_00cd;
		}
	}

IL_009c:
	try
	{ // begin try (depth: 1)
		Tuple_2_t460172552 * L_15 = __this->get_U3CentryU3E__0_0();
		NullCheck(L_15);
		SendOrPostCallback_t296893742 * L_16 = Tuple_2_get_Item1_m2647242292(L_15, /*hidden argument*/Tuple_2_get_Item1_m2647242292_MethodInfo_var);
		Tuple_2_t460172552 * L_17 = __this->get_U3CentryU3E__0_0();
		NullCheck(L_17);
		Il2CppObject * L_18 = Tuple_2_get_Item2_m3295131920(L_17, /*hidden argument*/Tuple_2_get_Item2_m3295131920_MethodInfo_var);
		NullCheck(L_16);
		SendOrPostCallback_Invoke_m4266976241(L_16, L_18, /*hidden argument*/NULL);
		goto IL_00cd;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00bc;
		throw e;
	}

CATCH_00bc:
	{ // begin catch(System.Exception)
		V_1 = ((Exception_t1927440687 *)__exception_local);
		Exception_t1927440687 * L_19 = V_1;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_19);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		goto IL_00cd;
	} // end catch (depth: 1)

IL_00cd:
	{
		__this->set_U24current_3(NULL);
		bool L_21 = __this->get_U24disposing_4();
		if (L_21)
		{
			goto IL_00e3;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_00e3:
	{
		goto IL_00f6;
	}

IL_00e8:
	{
		goto IL_0021;
	}
	// Dead block : IL_00ed: ldarg.0

IL_00f4:
	{
		return (bool)0;
	}

IL_00f6:
	{
		return (bool)1;
	}
}
// System.Object Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m222512858 (U3CStartU3Ec__Iterator0_t1310454857 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m225009554 (U3CStartU3Ec__Iterator0_t1310454857 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m3034070173 (U3CStartU3Ec__Iterator0_t1310454857 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir/<Start>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m4016881843_MetadataUsageId;
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m4016881843 (U3CStartU3Ec__Iterator0_t1310454857 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m4016881843_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern "C"  void MonoPInvokeCallbackAttribute__ctor_m3537801838 (MonoPInvokeCallbackAttribute_t1970456718 * __this, Type_t * ___t0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
