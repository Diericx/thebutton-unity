﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Firebase.Auth.FirebaseAuth
struct FirebaseAuth_t3105883899;
// Firebase.Auth.FirebaseAuth/StateChangedDelegate
struct StateChangedDelegate_t3080659151;
// Firebase.Auth.Future_User/SWIG_CompletionDelegate
struct SWIG_CompletionDelegate_t2830873745;
// System.String
struct String_t;
// Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper
struct SWIGExceptionHelper_t326262631;
// Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate
struct ExceptionDelegate_t4033223266;
// Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate
struct ExceptionArgumentDelegate_t1735378451;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Exception
struct Exception_t1927440687;
// Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper
struct SWIGStringHelper_t1725176893;
// Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate
struct SWIGStringDelegate_t553428412;
// Firebase.FirebaseApp
struct FirebaseApp_t210707726;
// System.EventHandler
struct EventHandler_t277755526;
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>
struct Task_1_t3166995609;
// Firebase.Auth.FirebaseUser
struct FirebaseUser_t4046966602;
// Firebase.Auth.FirebaseAuth/MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t403298259;
// System.Type
struct Type_t;
// System.Threading.Tasks.Task`1<System.String>
struct Task_1_t1149249240;
// Firebase.Auth.Future_User
struct Future_User_t3746551525;
// Firebase.Auth.Future_User/Action
struct Action_t1614918345;
// Firebase.Auth.Future_User/<GetTask>c__AnonStorey0
struct U3CGetTaskU3Ec__AnonStorey0_t2356161773;
// Firebase.Auth.Future_User/MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t629937593;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "Firebase_Auth_U3CModuleU3E3783534214.h"
#include "Firebase_Auth_U3CModuleU3E3783534214MethodDeclarations.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtil2343916418.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtil2343916418MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "Firebase_Auth_Firebase_Auth_FirebaseAuth3105883899.h"
#include "Firebase_Auth_Firebase_Auth_FirebaseAuth_StateChan3080659151.h"
#include "Firebase_Auth_Firebase_Auth_FirebaseAuth3105883899MethodDeclarations.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE2761888122MethodDeclarations.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGPendingEx4193433529MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef2419939847.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Void1841601450.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE2761888122.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE_SWIGExc326262631MethodDeclarations.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE_SWIGSt1725176893MethodDeclarations.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE_SWIGExc326262631.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE_SWIGSt1725176893.h"
#include "Firebase_Auth_Firebase_Auth_Future_User_SWIG_Compl2830873745.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_String2029220233.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE_SWIGEx4033223266MethodDeclarations.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE_SWIGEx1735378451MethodDeclarations.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE_SWIGEx4033223266.h"
#include "mscorlib_System_Object2689449295.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE_SWIGEx1735378451.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE_SWIGPe3156587018MethodDeclarations.h"
#include "mscorlib_System_ApplicationException474868623MethodDeclarations.h"
#include "mscorlib_System_ApplicationException474868623.h"
#include "mscorlib_System_ArithmeticException3261462543MethodDeclarations.h"
#include "mscorlib_System_ArithmeticException3261462543.h"
#include "mscorlib_System_DivideByZeroException1660837001MethodDeclarations.h"
#include "mscorlib_System_DivideByZeroException1660837001.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107.h"
#include "mscorlib_System_InvalidCastException3625212209MethodDeclarations.h"
#include "mscorlib_System_InvalidCastException3625212209.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_IO_IOException2458421087MethodDeclarations.h"
#include "mscorlib_System_IO_IOException2458421087.h"
#include "mscorlib_System_NullReferenceException3156209119MethodDeclarations.h"
#include "mscorlib_System_NullReferenceException3156209119.h"
#include "mscorlib_System_OutOfMemoryException1181064283MethodDeclarations.h"
#include "mscorlib_System_OutOfMemoryException1181064283.h"
#include "mscorlib_System_OverflowException1075868493MethodDeclarations.h"
#include "mscorlib_System_OverflowException1075868493.h"
#include "mscorlib_System_SystemException3877406272MethodDeclarations.h"
#include "mscorlib_System_SystemException3877406272.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE_SWIGPe3156587018.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Threading_Monitor3228523394MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE_SWIGStr553428412MethodDeclarations.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE_SWIGStr553428412.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef2419939847MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3547909217MethodDeclarations.h"
#include "Firebase_Auth_Firebase_Auth_FirebaseUser4046966602MethodDeclarations.h"
#include "mscorlib_System_GC2902933594MethodDeclarations.h"
#include "Firebase_Auth_Firebase_Auth_FirebaseUser4046966602.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3547909217.h"
#include "Firebase_App_Firebase_FirebaseApp210707726.h"
#include "Firebase_App_Firebase_FirebaseApp210707726MethodDeclarations.h"
#include "Firebase_App_Firebase_InitializationException1438959983MethodDeclarations.h"
#include "Firebase_Auth_Firebase_Auth_FirebaseAuth_StateChan3080659151MethodDeclarations.h"
#include "Firebase_App_Firebase_InitResult105293995.h"
#include "Firebase_App_Firebase_InitializationException1438959983.h"
#include "mscorlib_System_EventHandler277755526MethodDeclarations.h"
#include "mscorlib_System_EventHandler277755526.h"
#include "mscorlib_System_EventArgs3289624707.h"
#include "mscorlib_System_EventArgs3289624707MethodDeclarations.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "mscorlib_System_Threading_Interlocked1625106012MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked1625106012.h"
#include "Unity_Tasks_System_Threading_Tasks_Task_1_gen3166995609.h"
#include "Firebase_Auth_Firebase_Auth_Future_User3746551525MethodDeclarations.h"
#include "Firebase_Auth_Firebase_Auth_Future_User3746551525.h"
#include "Firebase_Auth_Firebase_Auth_FirebaseAuth_MonoPInvok403298259.h"
#include "Firebase_Auth_Firebase_Auth_FirebaseAuth_MonoPInvok403298259MethodDeclarations.h"
#include "mscorlib_System_Attribute542643598MethodDeclarations.h"
#include "Unity_Tasks_System_Threading_Tasks_Task_1_gen1149249240.h"
#include "Firebase_App_Firebase_FutureString4225986006MethodDeclarations.h"
#include "Firebase_App_Firebase_FutureString4225986006.h"
#include "Firebase_App_Firebase_FutureBase2698306134MethodDeclarations.h"
#include "Firebase_App_Firebase_FutureBase2698306134.h"
#include "Firebase_Auth_Firebase_Auth_Future_User_U3CGetTask2356161773MethodDeclarations.h"
#include "Unity_Tasks_System_Threading_Tasks_TaskCompletionS2929400682MethodDeclarations.h"
#include "Firebase_App_Firebase_FirebaseException2567272216MethodDeclarations.h"
#include "Firebase_Auth_Firebase_Auth_Future_User_Action1614918345MethodDeclarations.h"
#include "Firebase_Auth_Firebase_Auth_Future_User_U3CGetTask2356161773.h"
#include "Unity_Tasks_System_Threading_Tasks_TaskCompletionS2929400682.h"
#include "Firebase_App_Firebase_FutureStatus4011176069.h"
#include "Firebase_App_Firebase_FirebaseException2567272216.h"
#include "Firebase_Auth_Firebase_Auth_Future_User_Action1614918345.h"
#include "Firebase_Auth_Firebase_Auth_Future_User_SWIG_Compl2830873745MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge622743980MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge622743980.h"
#include "Firebase_Auth_Firebase_Auth_Future_User_MonoPInvoke629937593.h"
#include "Firebase_Auth_Firebase_Auth_Future_User_MonoPInvoke629937593MethodDeclarations.h"

// !!0 System.Threading.Interlocked::CompareExchange<System.Object>(!!0&,!!0,!!0)
extern "C"  Il2CppObject * Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject ** p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method);
#define Interlocked_CompareExchange_TisIl2CppObject_m2145889806(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject **, Il2CppObject *, Il2CppObject *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<System.EventHandler>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisEventHandler_t277755526_m4284642413(__this /* static, unused */, p0, p1, p2, method) ((  EventHandler_t277755526 * (*) (Il2CppObject * /* static, unused */, EventHandler_t277755526 **, EventHandler_t277755526 *, EventHandler_t277755526 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.IntPtr Firebase.Auth.AuthUtil::CreateAuthStateChangedListener(Firebase.Auth.FirebaseAuth,Firebase.Auth.FirebaseAuth/StateChangedDelegate)
extern Il2CppClass* FirebaseAuth_t3105883899_il2cpp_TypeInfo_var;
extern Il2CppClass* AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t AuthUtil_CreateAuthStateChangedListener_m1484217948_MetadataUsageId;
extern "C"  IntPtr_t AuthUtil_CreateAuthStateChangedListener_m1484217948 (Il2CppObject * __this /* static, unused */, FirebaseAuth_t3105883899 * ___auth0, StateChangedDelegate_t3080659151 * ___stateChangedDelegate1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthUtil_CreateAuthStateChangedListener_m1484217948_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		FirebaseAuth_t3105883899 * L_0 = ___auth0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t3105883899_il2cpp_TypeInfo_var);
		HandleRef_t2419939847  L_1 = FirebaseAuth_getCPtr_m1394702201(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		StateChangedDelegate_t3080659151 * L_2 = ___stateChangedDelegate1;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var);
		IntPtr_t L_3 = AuthUtilPINVOKE_Firebase_Auth_CreateAuthStateChangedListener_m1488217552(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_4 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_5 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_001d:
	{
		IntPtr_t L_6 = V_0;
		return L_6;
	}
}
// System.Void Firebase.Auth.AuthUtil::DestroyAuthStateChangedListener(System.IntPtr)
extern Il2CppClass* AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t AuthUtil_DestroyAuthStateChangedListener_m2167235822_MetadataUsageId;
extern "C"  void AuthUtil_DestroyAuthStateChangedListener_m2167235822 (Il2CppObject * __this /* static, unused */, IntPtr_t ___listener0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthUtil_DestroyAuthStateChangedListener_m2167235822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ___listener0;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var);
		AuthUtilPINVOKE_Firebase_Auth_DestroyAuthStateChangedListener_m1829959023(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_1 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_2 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE::.cctor()
extern Il2CppClass* SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var;
extern Il2CppClass* AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGStringHelper_t1725176893_il2cpp_TypeInfo_var;
extern const uint32_t AuthUtilPINVOKE__cctor_m659306542_MetadataUsageId;
extern "C"  void AuthUtilPINVOKE__cctor_m659306542 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthUtilPINVOKE__cctor_m659306542_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SWIGExceptionHelper_t326262631 * L_0 = (SWIGExceptionHelper_t326262631 *)il2cpp_codegen_object_new(SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var);
		SWIGExceptionHelper__ctor_m2301782569(L_0, /*hidden argument*/NULL);
		((AuthUtilPINVOKE_t2761888122_StaticFields*)AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var->static_fields)->set_swigExceptionHelper_0(L_0);
		SWIGStringHelper_t1725176893 * L_1 = (SWIGStringHelper_t1725176893 *)il2cpp_codegen_object_new(SWIGStringHelper_t1725176893_il2cpp_TypeInfo_var);
		SWIGStringHelper__ctor_m1377274003(L_1, /*hidden argument*/NULL);
		((AuthUtilPINVOKE_t2761888122_StaticFields*)AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var->static_fields)->set_swigStringHelper_1(L_1);
		return;
	}
}
extern "C" intptr_t DEFAULT_CALL Firebase_Auth_Future_User_SWIG_OnCompletion(void*, Il2CppMethodPointer, int32_t);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::Firebase_Auth_Future_User_SWIG_OnCompletion(System.Runtime.InteropServices.HandleRef,Firebase.Auth.Future_User/SWIG_CompletionDelegate,System.Int32)
extern "C"  IntPtr_t AuthUtilPINVOKE_Firebase_Auth_Future_User_SWIG_OnCompletion_m420864614 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, SWIG_CompletionDelegate_t2830873745 * ___jarg21, int32_t ___jarg32, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, int32_t);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___jarg21' to native representation
	Il2CppMethodPointer ____jarg21_marshaled = NULL;
	____jarg21_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___jarg21));

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_Future_User_SWIG_OnCompletion)(____jarg10_marshaled, ____jarg21_marshaled, ___jarg32);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL Firebase_Auth_Future_User_SWIG_FreeCompletionData(void*, intptr_t);
// System.Void Firebase.Auth.AuthUtilPINVOKE::Firebase_Auth_Future_User_SWIG_FreeCompletionData(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C"  void AuthUtilPINVOKE_Firebase_Auth_Future_User_SWIG_FreeCompletionData_m4173504260 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, IntPtr_t ___jarg21, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_Future_User_SWIG_FreeCompletionData)(____jarg10_marshaled, reinterpret_cast<intptr_t>((___jarg21).get_m_value_0()));

}
extern "C" intptr_t DEFAULT_CALL Firebase_Auth_Future_User_Result(void*);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::Firebase_Auth_Future_User_Result(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t AuthUtilPINVOKE_Firebase_Auth_Future_User_Result_m2641877733 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_Future_User_Result)(____jarg10_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL Firebase_Auth_delete_Future_User(void*);
// System.Void Firebase.Auth.AuthUtilPINVOKE::Firebase_Auth_delete_Future_User(System.Runtime.InteropServices.HandleRef)
extern "C"  void AuthUtilPINVOKE_Firebase_Auth_delete_Future_User_m2507266046 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_delete_Future_User)(____jarg10_marshaled);

}
extern "C" intptr_t DEFAULT_CALL Firebase_Auth_FirebaseUser_Token(void*, int32_t);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::Firebase_Auth_FirebaseUser_Token(System.Runtime.InteropServices.HandleRef,System.Boolean)
extern "C"  IntPtr_t AuthUtilPINVOKE_Firebase_Auth_FirebaseUser_Token_m4057723011 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, bool ___jarg21, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, int32_t);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_FirebaseUser_Token)(____jarg10_marshaled, static_cast<int32_t>(___jarg21));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
extern "C" char* DEFAULT_CALL Firebase_Auth_FirebaseUser_UserId_get(void*);
// System.String Firebase.Auth.AuthUtilPINVOKE::Firebase_Auth_FirebaseUser_UserId_get(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AuthUtilPINVOKE_Firebase_Auth_FirebaseUser_UserId_get_m572498592 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_FirebaseUser_UserId_get)(____jarg10_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
extern "C" char* DEFAULT_CALL Firebase_Auth_FirebaseUser_DisplayName_get(void*);
// System.String Firebase.Auth.AuthUtilPINVOKE::Firebase_Auth_FirebaseUser_DisplayName_get(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AuthUtilPINVOKE_Firebase_Auth_FirebaseUser_DisplayName_get_m594667867 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_FirebaseUser_DisplayName_get)(____jarg10_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL Firebase_Auth_delete_FirebaseUser(void*);
// System.Void Firebase.Auth.AuthUtilPINVOKE::Firebase_Auth_delete_FirebaseUser(System.Runtime.InteropServices.HandleRef)
extern "C"  void AuthUtilPINVOKE_Firebase_Auth_delete_FirebaseUser_m2178992323 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_delete_FirebaseUser)(____jarg10_marshaled);

}
extern "C" void DEFAULT_CALL Firebase_Auth_delete_FirebaseAuth(void*);
// System.Void Firebase.Auth.AuthUtilPINVOKE::Firebase_Auth_delete_FirebaseAuth(System.Runtime.InteropServices.HandleRef)
extern "C"  void AuthUtilPINVOKE_Firebase_Auth_delete_FirebaseAuth_m1391292 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_delete_FirebaseAuth)(____jarg10_marshaled);

}
extern "C" intptr_t DEFAULT_CALL Firebase_Auth_FirebaseAuth_SignInWithEmailAndPassword(void*, char*, char*);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::Firebase_Auth_FirebaseAuth_SignInWithEmailAndPassword(System.Runtime.InteropServices.HandleRef,System.String,System.String)
extern "C"  IntPtr_t AuthUtilPINVOKE_Firebase_Auth_FirebaseAuth_SignInWithEmailAndPassword_m475468804 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, String_t* ___jarg21, String_t* ___jarg32, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, char*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___jarg21' to native representation
	char* ____jarg21_marshaled = NULL;
	____jarg21_marshaled = il2cpp_codegen_marshal_string(___jarg21);

	// Marshaling of parameter '___jarg32' to native representation
	char* ____jarg32_marshaled = NULL;
	____jarg32_marshaled = il2cpp_codegen_marshal_string(___jarg32);

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_FirebaseAuth_SignInWithEmailAndPassword)(____jarg10_marshaled, ____jarg21_marshaled, ____jarg32_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	// Marshaling cleanup of parameter '___jarg21' native representation
	il2cpp_codegen_marshal_free(____jarg21_marshaled);
	____jarg21_marshaled = NULL;

	// Marshaling cleanup of parameter '___jarg32' native representation
	il2cpp_codegen_marshal_free(____jarg32_marshaled);
	____jarg32_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL Firebase_Auth_FirebaseAuth_CreateUserWithEmailAndPassword(void*, char*, char*);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::Firebase_Auth_FirebaseAuth_CreateUserWithEmailAndPassword(System.Runtime.InteropServices.HandleRef,System.String,System.String)
extern "C"  IntPtr_t AuthUtilPINVOKE_Firebase_Auth_FirebaseAuth_CreateUserWithEmailAndPassword_m2159767857 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, String_t* ___jarg21, String_t* ___jarg32, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*, char*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___jarg21' to native representation
	char* ____jarg21_marshaled = NULL;
	____jarg21_marshaled = il2cpp_codegen_marshal_string(___jarg21);

	// Marshaling of parameter '___jarg32' to native representation
	char* ____jarg32_marshaled = NULL;
	____jarg32_marshaled = il2cpp_codegen_marshal_string(___jarg32);

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_FirebaseAuth_CreateUserWithEmailAndPassword)(____jarg10_marshaled, ____jarg21_marshaled, ____jarg32_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	// Marshaling cleanup of parameter '___jarg21' native representation
	il2cpp_codegen_marshal_free(____jarg21_marshaled);
	____jarg21_marshaled = NULL;

	// Marshaling cleanup of parameter '___jarg32' native representation
	il2cpp_codegen_marshal_free(____jarg32_marshaled);
	____jarg32_marshaled = NULL;

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL Firebase_Auth_FirebaseAuth_GetAuthInternal(void*, int32_t*);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::Firebase_Auth_FirebaseAuth_GetAuthInternal(System.Runtime.InteropServices.HandleRef,System.Int32&)
extern "C"  IntPtr_t AuthUtilPINVOKE_Firebase_Auth_FirebaseAuth_GetAuthInternal_m6131012 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, int32_t* ___jarg21, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, int32_t*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___jarg21' to native representation
	int32_t ____jarg21_empty = 0;

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_FirebaseAuth_GetAuthInternal)(____jarg10_marshaled, &____jarg21_empty);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	// Marshaling of parameter '___jarg21' back from native representation
	*___jarg21 = ____jarg21_empty;

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL Firebase_Auth_FirebaseAuth_CurrentUser_get(void*);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::Firebase_Auth_FirebaseAuth_CurrentUser_get(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t AuthUtilPINVOKE_Firebase_Auth_FirebaseAuth_CurrentUser_get_m4035012579 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_FirebaseAuth_CurrentUser_get)(____jarg10_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
extern "C" intptr_t DEFAULT_CALL Firebase_Auth_CreateAuthStateChangedListener(void*, Il2CppMethodPointer);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::Firebase_Auth_CreateAuthStateChangedListener(System.Runtime.InteropServices.HandleRef,Firebase.Auth.FirebaseAuth/StateChangedDelegate)
extern "C"  IntPtr_t AuthUtilPINVOKE_Firebase_Auth_CreateAuthStateChangedListener_m1488217552 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, StateChangedDelegate_t3080659151 * ___jarg21, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = ___jarg10.get_handle_1().get_m_value_0();

	// Marshaling of parameter '___jarg21' to native representation
	Il2CppMethodPointer ____jarg21_marshaled = NULL;
	____jarg21_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___jarg21));

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CreateAuthStateChangedListener)(____jarg10_marshaled, ____jarg21_marshaled);

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL Firebase_Auth_DestroyAuthStateChangedListener(intptr_t);
// System.Void Firebase.Auth.AuthUtilPINVOKE::Firebase_Auth_DestroyAuthStateChangedListener(System.IntPtr)
extern "C"  void AuthUtilPINVOKE_Firebase_Auth_DestroyAuthStateChangedListener_m1829959023 (Il2CppObject * __this /* static, unused */, IntPtr_t ___jarg10, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_DestroyAuthStateChangedListener)(reinterpret_cast<intptr_t>((___jarg10).get_m_value_0()));

}
extern "C" intptr_t DEFAULT_CALL Firebase_Auth_Future_User_SWIGUpcast(intptr_t);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::Firebase_Auth_Future_User_SWIGUpcast(System.IntPtr)
extern "C"  IntPtr_t AuthUtilPINVOKE_Firebase_Auth_Future_User_SWIGUpcast_m327913273 (Il2CppObject * __this /* static, unused */, IntPtr_t ___jarg10, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_Future_User_SWIGUpcast)(reinterpret_cast<intptr_t>((___jarg10).get_m_value_0()));

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::.cctor()
extern Il2CppClass* ExceptionDelegate_t4033223266_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var;
extern Il2CppClass* ExceptionArgumentDelegate_t1735378451_il2cpp_TypeInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingApplicationException_m3419465515_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingArithmeticException_m2597371933_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingDivideByZeroException_m1514435475_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m1375230975_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingInvalidCastException_m2684782141_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingInvalidOperationException_m1635284189_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingIOException_m3688406363_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingNullReferenceException_m2580883413_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingOutOfMemoryException_m3100436703_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingOverflowException_m1180163321_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingSystemException_m3121931598_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingArgumentException_m1413553452_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingArgumentNullException_m2790842443_MethodInfo_var;
extern const MethodInfo* SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m1328423326_MethodInfo_var;
extern const uint32_t SWIGExceptionHelper__cctor_m116731802_MetadataUsageId;
extern "C"  void SWIGExceptionHelper__cctor_m116731802 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper__cctor_m116731802_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingApplicationException_m3419465515_MethodInfo_var);
		ExceptionDelegate_t4033223266 * L_1 = (ExceptionDelegate_t4033223266 *)il2cpp_codegen_object_new(ExceptionDelegate_t4033223266_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1273916664(L_1, NULL, L_0, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->set_applicationDelegate_0(L_1);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingArithmeticException_m2597371933_MethodInfo_var);
		ExceptionDelegate_t4033223266 * L_3 = (ExceptionDelegate_t4033223266 *)il2cpp_codegen_object_new(ExceptionDelegate_t4033223266_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1273916664(L_3, NULL, L_2, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->set_arithmeticDelegate_1(L_3);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingDivideByZeroException_m1514435475_MethodInfo_var);
		ExceptionDelegate_t4033223266 * L_5 = (ExceptionDelegate_t4033223266 *)il2cpp_codegen_object_new(ExceptionDelegate_t4033223266_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1273916664(L_5, NULL, L_4, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->set_divideByZeroDelegate_2(L_5);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m1375230975_MethodInfo_var);
		ExceptionDelegate_t4033223266 * L_7 = (ExceptionDelegate_t4033223266 *)il2cpp_codegen_object_new(ExceptionDelegate_t4033223266_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1273916664(L_7, NULL, L_6, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->set_indexOutOfRangeDelegate_3(L_7);
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingInvalidCastException_m2684782141_MethodInfo_var);
		ExceptionDelegate_t4033223266 * L_9 = (ExceptionDelegate_t4033223266 *)il2cpp_codegen_object_new(ExceptionDelegate_t4033223266_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1273916664(L_9, NULL, L_8, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->set_invalidCastDelegate_4(L_9);
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingInvalidOperationException_m1635284189_MethodInfo_var);
		ExceptionDelegate_t4033223266 * L_11 = (ExceptionDelegate_t4033223266 *)il2cpp_codegen_object_new(ExceptionDelegate_t4033223266_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1273916664(L_11, NULL, L_10, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->set_invalidOperationDelegate_5(L_11);
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingIOException_m3688406363_MethodInfo_var);
		ExceptionDelegate_t4033223266 * L_13 = (ExceptionDelegate_t4033223266 *)il2cpp_codegen_object_new(ExceptionDelegate_t4033223266_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1273916664(L_13, NULL, L_12, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->set_ioDelegate_6(L_13);
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingNullReferenceException_m2580883413_MethodInfo_var);
		ExceptionDelegate_t4033223266 * L_15 = (ExceptionDelegate_t4033223266 *)il2cpp_codegen_object_new(ExceptionDelegate_t4033223266_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1273916664(L_15, NULL, L_14, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->set_nullReferenceDelegate_7(L_15);
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingOutOfMemoryException_m3100436703_MethodInfo_var);
		ExceptionDelegate_t4033223266 * L_17 = (ExceptionDelegate_t4033223266 *)il2cpp_codegen_object_new(ExceptionDelegate_t4033223266_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1273916664(L_17, NULL, L_16, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->set_outOfMemoryDelegate_8(L_17);
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingOverflowException_m1180163321_MethodInfo_var);
		ExceptionDelegate_t4033223266 * L_19 = (ExceptionDelegate_t4033223266 *)il2cpp_codegen_object_new(ExceptionDelegate_t4033223266_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1273916664(L_19, NULL, L_18, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->set_overflowDelegate_9(L_19);
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingSystemException_m3121931598_MethodInfo_var);
		ExceptionDelegate_t4033223266 * L_21 = (ExceptionDelegate_t4033223266 *)il2cpp_codegen_object_new(ExceptionDelegate_t4033223266_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m1273916664(L_21, NULL, L_20, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->set_systemDelegate_10(L_21);
		IntPtr_t L_22;
		L_22.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingArgumentException_m1413553452_MethodInfo_var);
		ExceptionArgumentDelegate_t1735378451 * L_23 = (ExceptionArgumentDelegate_t1735378451 *)il2cpp_codegen_object_new(ExceptionArgumentDelegate_t1735378451_il2cpp_TypeInfo_var);
		ExceptionArgumentDelegate__ctor_m2513469229(L_23, NULL, L_22, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->set_argumentDelegate_11(L_23);
		IntPtr_t L_24;
		L_24.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingArgumentNullException_m2790842443_MethodInfo_var);
		ExceptionArgumentDelegate_t1735378451 * L_25 = (ExceptionArgumentDelegate_t1735378451 *)il2cpp_codegen_object_new(ExceptionArgumentDelegate_t1735378451_il2cpp_TypeInfo_var);
		ExceptionArgumentDelegate__ctor_m2513469229(L_25, NULL, L_24, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->set_argumentNullDelegate_12(L_25);
		IntPtr_t L_26;
		L_26.set_m_value_0((void*)(void*)SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m1328423326_MethodInfo_var);
		ExceptionArgumentDelegate_t1735378451 * L_27 = (ExceptionArgumentDelegate_t1735378451 *)il2cpp_codegen_object_new(ExceptionArgumentDelegate_t1735378451_il2cpp_TypeInfo_var);
		ExceptionArgumentDelegate__ctor_m2513469229(L_27, NULL, L_26, /*hidden argument*/NULL);
		((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->set_argumentOutOfRangeDelegate_13(L_27);
		ExceptionDelegate_t4033223266 * L_28 = ((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->get_applicationDelegate_0();
		ExceptionDelegate_t4033223266 * L_29 = ((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->get_arithmeticDelegate_1();
		ExceptionDelegate_t4033223266 * L_30 = ((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->get_divideByZeroDelegate_2();
		ExceptionDelegate_t4033223266 * L_31 = ((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->get_indexOutOfRangeDelegate_3();
		ExceptionDelegate_t4033223266 * L_32 = ((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->get_invalidCastDelegate_4();
		ExceptionDelegate_t4033223266 * L_33 = ((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->get_invalidOperationDelegate_5();
		ExceptionDelegate_t4033223266 * L_34 = ((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->get_ioDelegate_6();
		ExceptionDelegate_t4033223266 * L_35 = ((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->get_nullReferenceDelegate_7();
		ExceptionDelegate_t4033223266 * L_36 = ((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->get_outOfMemoryDelegate_8();
		ExceptionDelegate_t4033223266 * L_37 = ((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->get_overflowDelegate_9();
		ExceptionDelegate_t4033223266 * L_38 = ((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->get_systemDelegate_10();
		SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_AuthUtil_m256439704(NULL /*static, unused*/, L_28, L_29, L_30, L_31, L_32, L_33, L_34, L_35, L_36, L_37, L_38, /*hidden argument*/NULL);
		ExceptionArgumentDelegate_t1735378451 * L_39 = ((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->get_argumentDelegate_11();
		ExceptionArgumentDelegate_t1735378451 * L_40 = ((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->get_argumentNullDelegate_12();
		ExceptionArgumentDelegate_t1735378451 * L_41 = ((SWIGExceptionHelper_t326262631_StaticFields*)SWIGExceptionHelper_t326262631_il2cpp_TypeInfo_var->static_fields)->get_argumentOutOfRangeDelegate_13();
		SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_AuthUtil_m3003604090(NULL /*static, unused*/, L_39, L_40, L_41, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::.ctor()
extern "C"  void SWIGExceptionHelper__ctor_m2301782569 (SWIGExceptionHelper_t326262631 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL SWIGRegisterExceptionCallbacks_AuthUtil(Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacks_AuthUtil(Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate)
extern "C"  void SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_AuthUtil_m256439704 (Il2CppObject * __this /* static, unused */, ExceptionDelegate_t4033223266 * ___applicationDelegate0, ExceptionDelegate_t4033223266 * ___arithmeticDelegate1, ExceptionDelegate_t4033223266 * ___divideByZeroDelegate2, ExceptionDelegate_t4033223266 * ___indexOutOfRangeDelegate3, ExceptionDelegate_t4033223266 * ___invalidCastDelegate4, ExceptionDelegate_t4033223266 * ___invalidOperationDelegate5, ExceptionDelegate_t4033223266 * ___ioDelegate6, ExceptionDelegate_t4033223266 * ___nullReferenceDelegate7, ExceptionDelegate_t4033223266 * ___outOfMemoryDelegate8, ExceptionDelegate_t4033223266 * ___overflowDelegate9, ExceptionDelegate_t4033223266 * ___systemExceptionDelegate10, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);

	// Marshaling of parameter '___applicationDelegate0' to native representation
	Il2CppMethodPointer ____applicationDelegate0_marshaled = NULL;
	____applicationDelegate0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___applicationDelegate0));

	// Marshaling of parameter '___arithmeticDelegate1' to native representation
	Il2CppMethodPointer ____arithmeticDelegate1_marshaled = NULL;
	____arithmeticDelegate1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___arithmeticDelegate1));

	// Marshaling of parameter '___divideByZeroDelegate2' to native representation
	Il2CppMethodPointer ____divideByZeroDelegate2_marshaled = NULL;
	____divideByZeroDelegate2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___divideByZeroDelegate2));

	// Marshaling of parameter '___indexOutOfRangeDelegate3' to native representation
	Il2CppMethodPointer ____indexOutOfRangeDelegate3_marshaled = NULL;
	____indexOutOfRangeDelegate3_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___indexOutOfRangeDelegate3));

	// Marshaling of parameter '___invalidCastDelegate4' to native representation
	Il2CppMethodPointer ____invalidCastDelegate4_marshaled = NULL;
	____invalidCastDelegate4_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___invalidCastDelegate4));

	// Marshaling of parameter '___invalidOperationDelegate5' to native representation
	Il2CppMethodPointer ____invalidOperationDelegate5_marshaled = NULL;
	____invalidOperationDelegate5_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___invalidOperationDelegate5));

	// Marshaling of parameter '___ioDelegate6' to native representation
	Il2CppMethodPointer ____ioDelegate6_marshaled = NULL;
	____ioDelegate6_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___ioDelegate6));

	// Marshaling of parameter '___nullReferenceDelegate7' to native representation
	Il2CppMethodPointer ____nullReferenceDelegate7_marshaled = NULL;
	____nullReferenceDelegate7_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___nullReferenceDelegate7));

	// Marshaling of parameter '___outOfMemoryDelegate8' to native representation
	Il2CppMethodPointer ____outOfMemoryDelegate8_marshaled = NULL;
	____outOfMemoryDelegate8_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___outOfMemoryDelegate8));

	// Marshaling of parameter '___overflowDelegate9' to native representation
	Il2CppMethodPointer ____overflowDelegate9_marshaled = NULL;
	____overflowDelegate9_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___overflowDelegate9));

	// Marshaling of parameter '___systemExceptionDelegate10' to native representation
	Il2CppMethodPointer ____systemExceptionDelegate10_marshaled = NULL;
	____systemExceptionDelegate10_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___systemExceptionDelegate10));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SWIGRegisterExceptionCallbacks_AuthUtil)(____applicationDelegate0_marshaled, ____arithmeticDelegate1_marshaled, ____divideByZeroDelegate2_marshaled, ____indexOutOfRangeDelegate3_marshaled, ____invalidCastDelegate4_marshaled, ____invalidOperationDelegate5_marshaled, ____ioDelegate6_marshaled, ____nullReferenceDelegate7_marshaled, ____outOfMemoryDelegate8_marshaled, ____overflowDelegate9_marshaled, ____systemExceptionDelegate10_marshaled);

}
extern "C" void DEFAULT_CALL SWIGRegisterExceptionCallbacksArgument_AuthUtil(Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacksArgument_AuthUtil(Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate)
extern "C"  void SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_AuthUtil_m3003604090 (Il2CppObject * __this /* static, unused */, ExceptionArgumentDelegate_t1735378451 * ___argumentDelegate0, ExceptionArgumentDelegate_t1735378451 * ___argumentNullDelegate1, ExceptionArgumentDelegate_t1735378451 * ___argumentOutOfRangeDelegate2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);

	// Marshaling of parameter '___argumentDelegate0' to native representation
	Il2CppMethodPointer ____argumentDelegate0_marshaled = NULL;
	____argumentDelegate0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___argumentDelegate0));

	// Marshaling of parameter '___argumentNullDelegate1' to native representation
	Il2CppMethodPointer ____argumentNullDelegate1_marshaled = NULL;
	____argumentNullDelegate1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___argumentNullDelegate1));

	// Marshaling of parameter '___argumentOutOfRangeDelegate2' to native representation
	Il2CppMethodPointer ____argumentOutOfRangeDelegate2_marshaled = NULL;
	____argumentOutOfRangeDelegate2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___argumentOutOfRangeDelegate2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SWIGRegisterExceptionCallbacksArgument_AuthUtil)(____argumentDelegate0_marshaled, ____argumentNullDelegate1_marshaled, ____argumentOutOfRangeDelegate2_marshaled);

}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingApplicationException(System.String)
extern Il2CppClass* SWIGPendingException_t3156587018_il2cpp_TypeInfo_var;
extern Il2CppClass* ApplicationException_t474868623_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingApplicationException_m3419465515_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingApplicationException_m3419465515 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingApplicationException_m3419465515_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m1366816809(NULL /*static, unused*/, /*hidden argument*/NULL);
		ApplicationException_t474868623 * L_2 = (ApplicationException_t474868623 *)il2cpp_codegen_object_new(ApplicationException_t474868623_il2cpp_TypeInfo_var);
		ApplicationException__ctor_m856993678(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2147645632(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingApplicationException_m3419465515(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingApplicationException_m3419465515(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingArithmeticException(System.String)
extern Il2CppClass* SWIGPendingException_t3156587018_il2cpp_TypeInfo_var;
extern Il2CppClass* ArithmeticException_t3261462543_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingArithmeticException_m2597371933_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingArithmeticException_m2597371933 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingArithmeticException_m2597371933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m1366816809(NULL /*static, unused*/, /*hidden argument*/NULL);
		ArithmeticException_t3261462543 * L_2 = (ArithmeticException_t3261462543 *)il2cpp_codegen_object_new(ArithmeticException_t3261462543_il2cpp_TypeInfo_var);
		ArithmeticException__ctor_m2264388592(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2147645632(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArithmeticException_m2597371933(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingArithmeticException_m2597371933(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingDivideByZeroException(System.String)
extern Il2CppClass* SWIGPendingException_t3156587018_il2cpp_TypeInfo_var;
extern Il2CppClass* DivideByZeroException_t1660837001_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingDivideByZeroException_m1514435475_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingDivideByZeroException_m1514435475 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingDivideByZeroException_m1514435475_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m1366816809(NULL /*static, unused*/, /*hidden argument*/NULL);
		DivideByZeroException_t1660837001 * L_2 = (DivideByZeroException_t1660837001 *)il2cpp_codegen_object_new(DivideByZeroException_t1660837001_il2cpp_TypeInfo_var);
		DivideByZeroException__ctor_m4006599682(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2147645632(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingDivideByZeroException_m1514435475(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingDivideByZeroException_m1514435475(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingIndexOutOfRangeException(System.String)
extern Il2CppClass* SWIGPendingException_t3156587018_il2cpp_TypeInfo_var;
extern Il2CppClass* IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m1375230975_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m1375230975 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m1375230975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m1366816809(NULL /*static, unused*/, /*hidden argument*/NULL);
		IndexOutOfRangeException_t3527622107 * L_2 = (IndexOutOfRangeException_t3527622107 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m667958210(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2147645632(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m1375230975(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m1375230975(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingInvalidCastException(System.String)
extern Il2CppClass* SWIGPendingException_t3156587018_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingInvalidCastException_m2684782141_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingInvalidCastException_m2684782141 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingInvalidCastException_m2684782141_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m1366816809(NULL /*static, unused*/, /*hidden argument*/NULL);
		InvalidCastException_t3625212209 * L_2 = (InvalidCastException_t3625212209 *)il2cpp_codegen_object_new(InvalidCastException_t3625212209_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m3097122796(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2147645632(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingInvalidCastException_m2684782141(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingInvalidCastException_m2684782141(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingInvalidOperationException(System.String)
extern Il2CppClass* SWIGPendingException_t3156587018_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingInvalidOperationException_m1635284189_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingInvalidOperationException_m1635284189 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingInvalidOperationException_m1635284189_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m1366816809(NULL /*static, unused*/, /*hidden argument*/NULL);
		InvalidOperationException_t721527559 * L_2 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m725121084(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2147645632(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingInvalidOperationException_m1635284189(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingInvalidOperationException_m1635284189(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingIOException(System.String)
extern Il2CppClass* SWIGPendingException_t3156587018_il2cpp_TypeInfo_var;
extern Il2CppClass* IOException_t2458421087_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingIOException_m3688406363_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingIOException_m3688406363 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingIOException_m3688406363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m1366816809(NULL /*static, unused*/, /*hidden argument*/NULL);
		IOException_t2458421087 * L_2 = (IOException_t2458421087 *)il2cpp_codegen_object_new(IOException_t2458421087_il2cpp_TypeInfo_var);
		IOException__ctor_m847281350(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2147645632(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingIOException_m3688406363(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingIOException_m3688406363(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingNullReferenceException(System.String)
extern Il2CppClass* SWIGPendingException_t3156587018_il2cpp_TypeInfo_var;
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingNullReferenceException_m2580883413_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingNullReferenceException_m2580883413 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingNullReferenceException_m2580883413_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m1366816809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullReferenceException_t3156209119 * L_2 = (NullReferenceException_t3156209119 *)il2cpp_codegen_object_new(NullReferenceException_t3156209119_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m4167886794(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2147645632(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingNullReferenceException_m2580883413(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingNullReferenceException_m2580883413(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingOutOfMemoryException(System.String)
extern Il2CppClass* SWIGPendingException_t3156587018_il2cpp_TypeInfo_var;
extern Il2CppClass* OutOfMemoryException_t1181064283_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingOutOfMemoryException_m3100436703_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingOutOfMemoryException_m3100436703 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingOutOfMemoryException_m3100436703_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m1366816809(NULL /*static, unused*/, /*hidden argument*/NULL);
		OutOfMemoryException_t1181064283 * L_2 = (OutOfMemoryException_t1181064283 *)il2cpp_codegen_object_new(OutOfMemoryException_t1181064283_il2cpp_TypeInfo_var);
		OutOfMemoryException__ctor_m2926800194(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2147645632(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingOutOfMemoryException_m3100436703(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingOutOfMemoryException_m3100436703(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingOverflowException(System.String)
extern Il2CppClass* SWIGPendingException_t3156587018_il2cpp_TypeInfo_var;
extern Il2CppClass* OverflowException_t1075868493_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingOverflowException_m1180163321_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingOverflowException_m1180163321 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingOverflowException_m1180163321_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m1366816809(NULL /*static, unused*/, /*hidden argument*/NULL);
		OverflowException_t1075868493 * L_2 = (OverflowException_t1075868493 *)il2cpp_codegen_object_new(OverflowException_t1075868493_il2cpp_TypeInfo_var);
		OverflowException__ctor_m579335998(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2147645632(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingOverflowException_m1180163321(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingOverflowException_m1180163321(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingSystemException(System.String)
extern Il2CppClass* SWIGPendingException_t3156587018_il2cpp_TypeInfo_var;
extern Il2CppClass* SystemException_t3877406272_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingSystemException_m3121931598_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingSystemException_m3121931598 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingSystemException_m3121931598_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = SWIGPendingException_Retrieve_m1366816809(NULL /*static, unused*/, /*hidden argument*/NULL);
		SystemException_t3877406272 * L_2 = (SystemException_t3877406272 *)il2cpp_codegen_object_new(SystemException_t3877406272_il2cpp_TypeInfo_var);
		SystemException__ctor_m3356086419(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2147645632(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingSystemException_m3121931598(char* ___message0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingSystemException_m3121931598(NULL, ____message0_unmarshaled, NULL);

}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentException(System.String,System.String)
extern Il2CppClass* SWIGPendingException_t3156587018_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t SWIGExceptionHelper_SetPendingArgumentException_m1413553452_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingArgumentException_m1413553452 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingArgumentException_m1413553452_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		String_t* L_1 = ___paramName1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_2 = SWIGPendingException_Retrieve_m1366816809(NULL /*static, unused*/, /*hidden argument*/NULL);
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3312963299(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2147645632(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentException_m1413553452(char* ___message0, char* ___paramName1)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Marshaling of parameter '___paramName1' to managed representation
	String_t* ____paramName1_unmarshaled = NULL;
	____paramName1_unmarshaled = il2cpp_codegen_marshal_string_result(___paramName1);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingArgumentException_m1413553452(NULL, ____message0_unmarshaled, ____paramName1_unmarshaled, NULL);

}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentNullException(System.String,System.String)
extern Il2CppClass* SWIGPendingException_t3156587018_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4083594583;
extern const uint32_t SWIGExceptionHelper_SetPendingArgumentNullException_m2790842443_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingArgumentNullException_m2790842443 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingArgumentNullException_m2790842443_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_0 = SWIGPendingException_Retrieve_m1366816809(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Exception_t1927440687 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_2 = ___message0;
		Exception_t1927440687 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m612901809(NULL /*static, unused*/, L_2, _stringLiteral4083594583, L_4, /*hidden argument*/NULL);
		___message0 = L_5;
	}

IL_001f:
	{
		String_t* L_6 = ___paramName1;
		String_t* L_7 = ___message0;
		ArgumentNullException_t628810857 * L_8 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m2624491786(L_8, L_6, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		SWIGPendingException_Set_m2147645632(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentNullException_m2790842443(char* ___message0, char* ___paramName1)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Marshaling of parameter '___paramName1' to managed representation
	String_t* ____paramName1_unmarshaled = NULL;
	____paramName1_unmarshaled = il2cpp_codegen_marshal_string_result(___paramName1);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingArgumentNullException_m2790842443(NULL, ____message0_unmarshaled, ____paramName1_unmarshaled, NULL);

}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentOutOfRangeException(System.String,System.String)
extern Il2CppClass* SWIGPendingException_t3156587018_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4083594583;
extern const uint32_t SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m1328423326_MetadataUsageId;
extern "C"  void SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m1328423326 (Il2CppObject * __this /* static, unused */, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m1328423326_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_0 = SWIGPendingException_Retrieve_m1366816809(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Exception_t1927440687 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_2 = ___message0;
		Exception_t1927440687 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m612901809(NULL /*static, unused*/, L_2, _stringLiteral4083594583, L_4, /*hidden argument*/NULL);
		___message0 = L_5;
	}

IL_001f:
	{
		String_t* L_6 = ___paramName1;
		String_t* L_7 = ___message0;
		ArgumentOutOfRangeException_t279959794 * L_8 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_8, L_6, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		SWIGPendingException_Set_m2147645632(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m1328423326(char* ___message0, char* ___paramName1)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Marshaling of parameter '___paramName1' to managed representation
	String_t* ____paramName1_unmarshaled = NULL;
	____paramName1_unmarshaled = il2cpp_codegen_marshal_string_result(___paramName1);

	// Managed method invocation
	::SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m1328423326(NULL, ____message0_unmarshaled, ____paramName1_unmarshaled, NULL);

}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ExceptionArgumentDelegate__ctor_m2513469229 (ExceptionArgumentDelegate_t1735378451 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::Invoke(System.String,System.String)
extern "C"  void ExceptionArgumentDelegate_Invoke_m936504709 (ExceptionArgumentDelegate_t1735378451 * __this, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExceptionArgumentDelegate_Invoke_m936504709((ExceptionArgumentDelegate_t1735378451 *)__this->get_prev_9(),___message0, ___paramName1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___message0, ___paramName1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___message0, ___paramName1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___paramName1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___message0, ___paramName1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_ExceptionArgumentDelegate_t1735378451 (ExceptionArgumentDelegate_t1735378451 * __this, String_t* ___message0, String_t* ___paramName1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Marshaling of parameter '___paramName1' to native representation
	char* ____paramName1_marshaled = NULL;
	____paramName1_marshaled = il2cpp_codegen_marshal_string(___paramName1);

	// Native function invocation
	il2cppPInvokeFunc(____message0_marshaled, ____paramName1_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

	// Marshaling cleanup of parameter '___paramName1' native representation
	il2cpp_codegen_marshal_free(____paramName1_marshaled);
	____paramName1_marshaled = NULL;

}
// System.IAsyncResult Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExceptionArgumentDelegate_BeginInvoke_m284475872 (ExceptionArgumentDelegate_t1735378451 * __this, String_t* ___message0, String_t* ___paramName1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___message0;
	__d_args[1] = ___paramName1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ExceptionArgumentDelegate_EndInvoke_m259331091 (ExceptionArgumentDelegate_t1735378451 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ExceptionDelegate__ctor_m1273916664 (ExceptionDelegate_t4033223266 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::Invoke(System.String)
extern "C"  void ExceptionDelegate_Invoke_m1118456928 (ExceptionDelegate_t4033223266 * __this, String_t* ___message0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExceptionDelegate_Invoke_m1118456928((ExceptionDelegate_t4033223266 *)__this->get_prev_9(),___message0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___message0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___message0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_ExceptionDelegate_t4033223266 (ExceptionDelegate_t4033223266 * __this, String_t* ___message0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	il2cppPInvokeFunc(____message0_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

}
// System.IAsyncResult Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExceptionDelegate_BeginInvoke_m2419099175 (ExceptionDelegate_t4033223266 * __this, String_t* ___message0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___message0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ExceptionDelegate_EndInvoke_m1379692334 (ExceptionDelegate_t4033223266 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGPendingException::Set(System.Exception)
extern const Il2CppType* AuthUtilPINVOKE_t2761888122_0_0_0_var;
extern Il2CppClass* SWIGPendingException_t3156587018_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ApplicationException_t474868623_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1125746324;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t SWIGPendingException_Set_m2147645632_MetadataUsageId;
extern "C"  void SWIGPendingException_Set_m2147645632 (Il2CppObject * __this /* static, unused */, Exception_t1927440687 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGPendingException_Set_m2147645632_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_0 = ((SWIGPendingException_t3156587018_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var))->get_pendingException_0();
		if (!L_0)
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = ((SWIGPendingException_t3156587018_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var))->get_pendingException_0();
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1125746324, L_2, _stringLiteral372029317, /*hidden argument*/NULL);
		Exception_t1927440687 * L_4 = ___e0;
		ApplicationException_t474868623 * L_5 = (ApplicationException_t474868623 *)il2cpp_codegen_object_new(ApplicationException_t474868623_il2cpp_TypeInfo_var);
		ApplicationException__ctor_m856993678(L_5, L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002a:
	{
		Exception_t1927440687 * L_6 = ___e0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		((SWIGPendingException_t3156587018_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var))->set_pendingException_0(L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(AuthUtilPINVOKE_t2761888122_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_7;
		Il2CppObject * L_8 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0041:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		int32_t L_9 = ((SWIGPendingException_t3156587018_StaticFields*)SWIGPendingException_t3156587018_il2cpp_TypeInfo_var->static_fields)->get_numExceptionsPending_1();
		((SWIGPendingException_t3156587018_StaticFields*)SWIGPendingException_t3156587018_il2cpp_TypeInfo_var->static_fields)->set_numExceptionsPending_1(((int32_t)((int32_t)L_9+(int32_t)1)));
		IL2CPP_LEAVE(0x59, FINALLY_0052);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0052;
	}

FINALLY_0052:
	{ // begin finally (depth: 1)
		Il2CppObject * L_10 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(82)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(82)
	{
		IL2CPP_JUMP_TBL(0x59, IL_0059)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0059:
	{
		return;
	}
}
// System.Exception Firebase.Auth.AuthUtilPINVOKE/SWIGPendingException::Retrieve()
extern const Il2CppType* AuthUtilPINVOKE_t2761888122_0_0_0_var;
extern Il2CppClass* SWIGPendingException_t3156587018_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t SWIGPendingException_Retrieve_m1366816809_MetadataUsageId;
extern "C"  Exception_t1927440687 * SWIGPendingException_Retrieve_m1366816809 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGPendingException_Retrieve_m1366816809_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (Exception_t1927440687 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		int32_t L_0 = ((SWIGPendingException_t3156587018_StaticFields*)SWIGPendingException_t3156587018_il2cpp_TypeInfo_var->static_fields)->get_numExceptionsPending_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_1 = ((SWIGPendingException_t3156587018_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var))->get_pendingException_0();
		if (!L_1)
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_2 = ((SWIGPendingException_t3156587018_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var))->get_pendingException_0();
		V_0 = L_2;
		((SWIGPendingException_t3156587018_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var))->set_pendingException_0((Exception_t1927440687 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(AuthUtilPINVOKE_t2761888122_0_0_0_var), /*hidden argument*/NULL);
		V_1 = L_3;
		Il2CppObject * L_4 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0034:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t3156587018_il2cpp_TypeInfo_var);
		int32_t L_5 = ((SWIGPendingException_t3156587018_StaticFields*)SWIGPendingException_t3156587018_il2cpp_TypeInfo_var->static_fields)->get_numExceptionsPending_1();
		((SWIGPendingException_t3156587018_StaticFields*)SWIGPendingException_t3156587018_il2cpp_TypeInfo_var->static_fields)->set_numExceptionsPending_1(((int32_t)((int32_t)L_5-(int32_t)1)));
		IL2CPP_LEAVE(0x4C, FINALLY_0045);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(69)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x4C, IL_004c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_004c:
	{
		Exception_t1927440687 * L_7 = V_0;
		return L_7;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGPendingException::.cctor()
extern "C"  void SWIGPendingException__cctor_m2296785719 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper::.cctor()
extern Il2CppClass* SWIGStringDelegate_t553428412_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGStringHelper_t1725176893_il2cpp_TypeInfo_var;
extern const MethodInfo* SWIGStringHelper_CreateString_m881484445_MethodInfo_var;
extern const uint32_t SWIGStringHelper__cctor_m1662264752_MetadataUsageId;
extern "C"  void SWIGStringHelper__cctor_m1662264752 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGStringHelper__cctor_m1662264752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)SWIGStringHelper_CreateString_m881484445_MethodInfo_var);
		SWIGStringDelegate_t553428412 * L_1 = (SWIGStringDelegate_t553428412 *)il2cpp_codegen_object_new(SWIGStringDelegate_t553428412_il2cpp_TypeInfo_var);
		SWIGStringDelegate__ctor_m4244385166(L_1, NULL, L_0, /*hidden argument*/NULL);
		((SWIGStringHelper_t1725176893_StaticFields*)SWIGStringHelper_t1725176893_il2cpp_TypeInfo_var->static_fields)->set_stringDelegate_0(L_1);
		SWIGStringDelegate_t553428412 * L_2 = ((SWIGStringHelper_t1725176893_StaticFields*)SWIGStringHelper_t1725176893_il2cpp_TypeInfo_var->static_fields)->get_stringDelegate_0();
		SWIGStringHelper_SWIGRegisterStringCallback_AuthUtil_m2804163847(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper::.ctor()
extern "C"  void SWIGStringHelper__ctor_m1377274003 (SWIGStringHelper_t1725176893 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL SWIGRegisterStringCallback_AuthUtil(Il2CppMethodPointer);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper::SWIGRegisterStringCallback_AuthUtil(Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate)
extern "C"  void SWIGStringHelper_SWIGRegisterStringCallback_AuthUtil_m2804163847 (Il2CppObject * __this /* static, unused */, SWIGStringDelegate_t553428412 * ___stringDelegate0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer);

	// Marshaling of parameter '___stringDelegate0' to native representation
	Il2CppMethodPointer ____stringDelegate0_marshaled = NULL;
	____stringDelegate0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___stringDelegate0));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SWIGRegisterStringCallback_AuthUtil)(____stringDelegate0_marshaled);

}
// System.String Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper::CreateString(System.String)
extern "C"  String_t* SWIGStringHelper_CreateString_m881484445 (Il2CppObject * __this /* static, unused */, String_t* ___cString0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___cString0;
		return L_0;
	}
}
extern "C" char* DEFAULT_CALL ReversePInvokeWrapper_SWIGStringHelper_CreateString_m881484445(char* ___cString0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___cString0' to managed representation
	String_t* ____cString0_unmarshaled = NULL;
	____cString0_unmarshaled = il2cpp_codegen_marshal_string_result(___cString0);

	// Managed method invocation
	String_t* returnValue = ::SWIGStringHelper_CreateString_m881484445(NULL, ____cString0_unmarshaled, NULL);

	// Marshaling of return value back from managed representation
	char* _returnValue_marshaled = NULL;
	_returnValue_marshaled = il2cpp_codegen_marshal_string(returnValue);

	return _returnValue_marshaled;
}
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void SWIGStringDelegate__ctor_m4244385166 (SWIGStringDelegate_t553428412 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.String Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::Invoke(System.String)
extern "C"  String_t* SWIGStringDelegate_Invoke_m3892666191 (SWIGStringDelegate_t553428412 * __this, String_t* ___message0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		SWIGStringDelegate_Invoke_m3892666191((SWIGStringDelegate_t553428412 *)__this->get_prev_9(),___message0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef String_t* (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___message0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef String_t* (*FunctionPointerType) (void* __this, String_t* ___message0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef String_t* (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___message0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  String_t* DelegatePInvokeWrapper_SWIGStringDelegate_t553428412 (SWIGStringDelegate_t553428412 * __this, String_t* ___message0, const MethodInfo* method)
{
	typedef char* (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	char* returnValue = il2cppPInvokeFunc(____message0_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

	return _returnValue_unmarshaled;
}
// System.IAsyncResult Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SWIGStringDelegate_BeginInvoke_m4283854525 (SWIGStringDelegate_t553428412 * __this, String_t* ___message0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___message0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.String Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::EndInvoke(System.IAsyncResult)
extern "C"  String_t* SWIGStringDelegate_EndInvoke_m701155767 (SWIGStringDelegate_t553428412 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (String_t*)__result;
}
// System.Void Firebase.Auth.FirebaseAuth::.ctor(System.IntPtr,System.Boolean)
extern "C"  void FirebaseAuth__ctor_m3346144045 (FirebaseAuth_t3105883899 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		bool L_0 = ___cMemoryOwn1;
		__this->set_swigCMemOwn_1(L_0);
		IntPtr_t L_1 = ___cPtr0;
		HandleRef_t2419939847  L_2;
		memset(&L_2, 0, sizeof(L_2));
		HandleRef__ctor_m1370162289(&L_2, __this, L_1, /*hidden argument*/NULL);
		__this->set_swigCPtr_0(L_2);
		return;
	}
}
// System.Runtime.InteropServices.HandleRef Firebase.Auth.FirebaseAuth::getCPtr(Firebase.Auth.FirebaseAuth)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseAuth_getCPtr_m1394702201_MetadataUsageId;
extern "C"  HandleRef_t2419939847  FirebaseAuth_getCPtr_m1394702201 (Il2CppObject * __this /* static, unused */, FirebaseAuth_t3105883899 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_getCPtr_m1394702201_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HandleRef_t2419939847  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		FirebaseAuth_t3105883899 * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		HandleRef_t2419939847  L_2;
		memset(&L_2, 0, sizeof(L_2));
		HandleRef__ctor_m1370162289(&L_2, NULL, L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_001c;
	}

IL_0016:
	{
		FirebaseAuth_t3105883899 * L_3 = ___obj0;
		NullCheck(L_3);
		HandleRef_t2419939847  L_4 = L_3->get_swigCPtr_0();
		G_B3_0 = L_4;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::Finalize()
extern "C"  void FirebaseAuth_Finalize_m316336200 (FirebaseAuth_t3105883899 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		FirebaseAuth_Dispose_m3120571751(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::Dispose()
extern Il2CppClass* FirebaseAuth_t3105883899_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_Remove_m3089939258_MethodInfo_var;
extern const uint32_t FirebaseAuth_Dispose_m3120571751_MetadataUsageId;
extern "C"  void FirebaseAuth_Dispose_m3120571751 (FirebaseAuth_t3105883899 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_Dispose_m3120571751_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	FirebaseUser_t4046966602 * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t3105883899_il2cpp_TypeInfo_var);
		Dictionary_2_t3547909217 * L_0 = ((FirebaseAuth_t3105883899_StaticFields*)FirebaseAuth_t3105883899_il2cpp_TypeInfo_var->static_fields)->get_appCPtrToAuth_6();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			HandleRef_t2419939847 * L_2 = __this->get_address_of_swigCPtr_0();
			IntPtr_t L_3 = HandleRef_get_Handle_m769981143(L_2, /*hidden argument*/NULL);
			IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_5 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
			if (!L_5)
			{
				goto IL_0095;
			}
		}

IL_0026:
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t3105883899_il2cpp_TypeInfo_var);
			Dictionary_2_t3547909217 * L_6 = ((FirebaseAuth_t3105883899_StaticFields*)FirebaseAuth_t3105883899_il2cpp_TypeInfo_var->static_fields)->get_appCPtrToAuth_6();
			IntPtr_t L_7 = __this->get_appCPtr_3();
			NullCheck(L_6);
			Dictionary_2_Remove_m3089939258(L_6, L_7, /*hidden argument*/Dictionary_2_Remove_m3089939258_MethodInfo_var);
			__this->set_appProxy_2((FirebaseApp_t210707726 *)NULL);
			IntPtr_t L_8 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			__this->set_appCPtr_3(L_8);
			bool L_9 = __this->get_swigCMemOwn_1();
			if (!L_9)
			{
				goto IL_0084;
			}
		}

IL_0054:
		{
			__this->set_swigCMemOwn_1((bool)0);
			IntPtr_t L_10 = __this->get_authStateListener_4();
			AuthUtil_DestroyAuthStateChangedListener_m2167235822(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
			FirebaseUser_t4046966602 * L_11 = FirebaseAuth_get_CurrentUser_m3290820871(__this, /*hidden argument*/NULL);
			V_1 = L_11;
			FirebaseUser_t4046966602 * L_12 = V_1;
			if (!L_12)
			{
				goto IL_0079;
			}
		}

IL_0073:
		{
			FirebaseUser_t4046966602 * L_13 = V_1;
			NullCheck(L_13);
			FirebaseUser_Dispose_m632137998(L_13, /*hidden argument*/NULL);
		}

IL_0079:
		{
			HandleRef_t2419939847  L_14 = __this->get_swigCPtr_0();
			IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var);
			AuthUtilPINVOKE_Firebase_Auth_delete_FirebaseAuth_m1391292(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		}

IL_0084:
		{
			IntPtr_t L_15 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			HandleRef_t2419939847  L_16;
			memset(&L_16, 0, sizeof(L_16));
			HandleRef__ctor_m1370162289(&L_16, NULL, L_15, /*hidden argument*/NULL);
			__this->set_swigCPtr_0(L_16);
		}

IL_0095:
		{
			GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0xA7, FINALLY_00a0);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00a0;
	}

FINALLY_00a0:
	{ // begin finally (depth: 1)
		Il2CppObject * L_17 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(160)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(160)
	{
		IL2CPP_JUMP_TBL(0xA7, IL_00a7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00a7:
	{
		return;
	}
}
// Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseAuth::GetAuth(Firebase.FirebaseApp)
extern Il2CppClass* FirebaseAuth_t3105883899_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseApp_t210707726_il2cpp_TypeInfo_var;
extern Il2CppClass* InitializationException_t1438959983_il2cpp_TypeInfo_var;
extern Il2CppClass* StateChangedDelegate_t3080659151_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m1381016338_MethodInfo_var;
extern const MethodInfo* FirebaseAuth_StateChangedFunction_m1288632637_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m1351210872_MethodInfo_var;
extern const uint32_t FirebaseAuth_GetAuth_m1881142076_MetadataUsageId;
extern "C"  FirebaseAuth_t3105883899 * FirebaseAuth_GetAuth_m1881142076 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_GetAuth_m1881142076_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FirebaseAuth_t3105883899 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	IntPtr_t V_2;
	memset(&V_2, 0, sizeof(V_2));
	HandleRef_t2419939847  V_3;
	memset(&V_3, 0, sizeof(V_3));
	FirebaseAuth_t3105883899 * V_4 = NULL;
	int32_t V_5 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	FirebaseAuth_t3105883899 * G_B7_0 = NULL;
	FirebaseAuth_t3105883899 * G_B7_1 = NULL;
	FirebaseAuth_t3105883899 * G_B6_0 = NULL;
	FirebaseAuth_t3105883899 * G_B6_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t3105883899_il2cpp_TypeInfo_var);
		Dictionary_2_t3547909217 * L_0 = ((FirebaseAuth_t3105883899_StaticFields*)FirebaseAuth_t3105883899_il2cpp_TypeInfo_var->static_fields)->get_appCPtrToAuth_6();
		V_1 = L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			FirebaseApp_t210707726 * L_2 = ___app0;
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
			HandleRef_t2419939847  L_3 = FirebaseApp_getCPtr_m4255293038(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
			V_3 = L_3;
			IntPtr_t L_4 = HandleRef_get_Handle_m769981143((&V_3), /*hidden argument*/NULL);
			V_2 = L_4;
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t3105883899_il2cpp_TypeInfo_var);
			Dictionary_2_t3547909217 * L_5 = ((FirebaseAuth_t3105883899_StaticFields*)FirebaseAuth_t3105883899_il2cpp_TypeInfo_var->static_fields)->get_appCPtrToAuth_6();
			IntPtr_t L_6 = V_2;
			NullCheck(L_5);
			bool L_7 = Dictionary_2_TryGetValue_m1381016338(L_5, L_6, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1381016338_MethodInfo_var);
			if (!L_7)
			{
				goto IL_0035;
			}
		}

IL_002d:
		{
			FirebaseAuth_t3105883899 * L_8 = V_0;
			V_4 = L_8;
			IL2CPP_LEAVE(0x9E, FINALLY_0095);
		}

IL_0035:
		{
			FirebaseApp_t210707726 * L_9 = ___app0;
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t3105883899_il2cpp_TypeInfo_var);
			FirebaseAuth_t3105883899 * L_10 = FirebaseAuth_GetAuthInternal_m794040299(NULL /*static, unused*/, L_9, (&V_5), /*hidden argument*/NULL);
			V_0 = L_10;
			int32_t L_11 = V_5;
			if (!L_11)
			{
				goto IL_004d;
			}
		}

IL_0045:
		{
			int32_t L_12 = V_5;
			InitializationException_t1438959983 * L_13 = (InitializationException_t1438959983 *)il2cpp_codegen_object_new(InitializationException_t1438959983_il2cpp_TypeInfo_var);
			InitializationException__ctor_m6220901(L_13, L_12, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
		}

IL_004d:
		{
			FirebaseAuth_t3105883899 * L_14 = V_0;
			FirebaseAuth_t3105883899 * L_15 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t3105883899_il2cpp_TypeInfo_var);
			StateChangedDelegate_t3080659151 * L_16 = ((FirebaseAuth_t3105883899_StaticFields*)FirebaseAuth_t3105883899_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_7();
			G_B6_0 = L_15;
			G_B6_1 = L_14;
			if (L_16)
			{
				G_B7_0 = L_15;
				G_B7_1 = L_14;
				goto IL_0067;
			}
		}

IL_0056:
		{
			IntPtr_t L_17;
			L_17.set_m_value_0((void*)(void*)FirebaseAuth_StateChangedFunction_m1288632637_MethodInfo_var);
			StateChangedDelegate_t3080659151 * L_18 = (StateChangedDelegate_t3080659151 *)il2cpp_codegen_object_new(StateChangedDelegate_t3080659151_il2cpp_TypeInfo_var);
			StateChangedDelegate__ctor_m1800607001(L_18, NULL, L_17, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t3105883899_il2cpp_TypeInfo_var);
			((FirebaseAuth_t3105883899_StaticFields*)FirebaseAuth_t3105883899_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache0_7(L_18);
			G_B7_0 = G_B6_0;
			G_B7_1 = G_B6_1;
		}

IL_0067:
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t3105883899_il2cpp_TypeInfo_var);
			StateChangedDelegate_t3080659151 * L_19 = ((FirebaseAuth_t3105883899_StaticFields*)FirebaseAuth_t3105883899_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_7();
			IntPtr_t L_20 = AuthUtil_CreateAuthStateChangedListener_m1484217948(NULL /*static, unused*/, G_B7_0, L_19, /*hidden argument*/NULL);
			NullCheck(G_B7_1);
			G_B7_1->set_authStateListener_4(L_20);
			FirebaseAuth_t3105883899 * L_21 = V_0;
			FirebaseApp_t210707726 * L_22 = ___app0;
			NullCheck(L_21);
			L_21->set_appProxy_2(L_22);
			FirebaseAuth_t3105883899 * L_23 = V_0;
			IntPtr_t L_24 = V_2;
			NullCheck(L_23);
			L_23->set_appCPtr_3(L_24);
			Dictionary_2_t3547909217 * L_25 = ((FirebaseAuth_t3105883899_StaticFields*)FirebaseAuth_t3105883899_il2cpp_TypeInfo_var->static_fields)->get_appCPtrToAuth_6();
			IntPtr_t L_26 = V_2;
			FirebaseAuth_t3105883899 * L_27 = V_0;
			NullCheck(L_25);
			Dictionary_2_set_Item_m1351210872(L_25, L_26, L_27, /*hidden argument*/Dictionary_2_set_Item_m1351210872_MethodInfo_var);
			IL2CPP_LEAVE(0x9C, FINALLY_0095);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0095;
	}

FINALLY_0095:
	{ // begin finally (depth: 1)
		Il2CppObject * L_28 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(149)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(149)
	{
		IL2CPP_JUMP_TBL(0x9E, IL_009e)
		IL2CPP_JUMP_TBL(0x9C, IL_009c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_009c:
	{
		FirebaseAuth_t3105883899 * L_29 = V_0;
		return L_29;
	}

IL_009e:
	{
		FirebaseAuth_t3105883899 * L_30 = V_4;
		return L_30;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::StateChangedFunction(System.IntPtr)
extern Il2CppClass* FirebaseAuth_t3105883899_il2cpp_TypeInfo_var;
extern Il2CppClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m1381016338_MethodInfo_var;
extern const uint32_t FirebaseAuth_StateChangedFunction_m1288632637_MetadataUsageId;
extern "C"  void FirebaseAuth_StateChangedFunction_m1288632637 (Il2CppObject * __this /* static, unused */, IntPtr_t ___appCPtr0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_StateChangedFunction_m1288632637_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	FirebaseAuth_t3105883899 * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t3105883899_il2cpp_TypeInfo_var);
		Dictionary_2_t3547909217 * L_0 = ((FirebaseAuth_t3105883899_StaticFields*)FirebaseAuth_t3105883899_il2cpp_TypeInfo_var->static_fields)->get_appCPtrToAuth_6();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t3105883899_il2cpp_TypeInfo_var);
			Dictionary_2_t3547909217 * L_2 = ((FirebaseAuth_t3105883899_StaticFields*)FirebaseAuth_t3105883899_il2cpp_TypeInfo_var->static_fields)->get_appCPtrToAuth_6();
			IntPtr_t L_3 = ___appCPtr0;
			NullCheck(L_2);
			bool L_4 = Dictionary_2_TryGetValue_m1381016338(L_2, L_3, (&V_1), /*hidden argument*/Dictionary_2_TryGetValue_m1381016338_MethodInfo_var);
			if (!L_4)
			{
				goto IL_003a;
			}
		}

IL_001e:
		{
			FirebaseAuth_t3105883899 * L_5 = V_1;
			NullCheck(L_5);
			EventHandler_t277755526 * L_6 = L_5->get_StateChanged_5();
			if (!L_6)
			{
				goto IL_003a;
			}
		}

IL_0029:
		{
			FirebaseAuth_t3105883899 * L_7 = V_1;
			NullCheck(L_7);
			EventHandler_t277755526 * L_8 = L_7->get_StateChanged_5();
			FirebaseAuth_t3105883899 * L_9 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
			EventArgs_t3289624707 * L_10 = ((EventArgs_t3289624707_StaticFields*)EventArgs_t3289624707_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
			NullCheck(L_8);
			EventHandler_Invoke_m1137722757(L_8, L_9, L_10, /*hidden argument*/NULL);
		}

IL_003a:
		{
			IL2CPP_LEAVE(0x46, FINALLY_003f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_003f;
	}

FINALLY_003f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_11 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(63)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(63)
	{
		IL2CPP_JUMP_TBL(0x46, IL_0046)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0046:
	{
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_FirebaseAuth_StateChangedFunction_m1288632637(intptr_t ___appCPtr0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___appCPtr0' to managed representation
	IntPtr_t ____appCPtr0_unmarshaled;
	____appCPtr0_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(___appCPtr0)));

	// Managed method invocation
	::FirebaseAuth_StateChangedFunction_m1288632637(NULL, ____appCPtr0_unmarshaled, NULL);

}
// Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseAuth::get_DefaultInstance()
extern Il2CppClass* FirebaseApp_t210707726_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseAuth_t3105883899_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseAuth_get_DefaultInstance_m399894646_MetadataUsageId;
extern "C"  FirebaseAuth_t3105883899 * FirebaseAuth_get_DefaultInstance_m399894646 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_get_DefaultInstance_m399894646_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		FirebaseApp_t210707726 * L_0 = FirebaseApp_get_DefaultInstance_m465202029(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t3105883899_il2cpp_TypeInfo_var);
		FirebaseAuth_t3105883899 * L_1 = FirebaseAuth_GetAuth_m1881142076(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::add_StateChanged(System.EventHandler)
extern Il2CppClass* EventHandler_t277755526_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseAuth_add_StateChanged_m2504269526_MetadataUsageId;
extern "C"  void FirebaseAuth_add_StateChanged_m2504269526 (FirebaseAuth_t3105883899 * __this, EventHandler_t277755526 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_add_StateChanged_m2504269526_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t277755526 * V_0 = NULL;
	EventHandler_t277755526 * V_1 = NULL;
	{
		EventHandler_t277755526 * L_0 = __this->get_StateChanged_5();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t277755526 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t277755526 ** L_2 = __this->get_address_of_StateChanged_5();
		EventHandler_t277755526 * L_3 = V_1;
		EventHandler_t277755526 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_t277755526 * L_6 = V_0;
		EventHandler_t277755526 * L_7 = InterlockedCompareExchangeImpl<EventHandler_t277755526 *>(L_2, ((EventHandler_t277755526 *)CastclassSealed(L_5, EventHandler_t277755526_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_t277755526 * L_8 = V_0;
		EventHandler_t277755526 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_t277755526 *)L_8) == ((Il2CppObject*)(EventHandler_t277755526 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::remove_StateChanged(System.EventHandler)
extern Il2CppClass* EventHandler_t277755526_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseAuth_remove_StateChanged_m3358990019_MetadataUsageId;
extern "C"  void FirebaseAuth_remove_StateChanged_m3358990019 (FirebaseAuth_t3105883899 * __this, EventHandler_t277755526 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_remove_StateChanged_m3358990019_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t277755526 * V_0 = NULL;
	EventHandler_t277755526 * V_1 = NULL;
	{
		EventHandler_t277755526 * L_0 = __this->get_StateChanged_5();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t277755526 * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t277755526 ** L_2 = __this->get_address_of_StateChanged_5();
		EventHandler_t277755526 * L_3 = V_1;
		EventHandler_t277755526 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		EventHandler_t277755526 * L_6 = V_0;
		EventHandler_t277755526 * L_7 = InterlockedCompareExchangeImpl<EventHandler_t277755526 *>(L_2, ((EventHandler_t277755526 *)CastclassSealed(L_5, EventHandler_t277755526_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		EventHandler_t277755526 * L_8 = V_0;
		EventHandler_t277755526 * L_9 = V_1;
		if ((!(((Il2CppObject*)(EventHandler_t277755526 *)L_8) == ((Il2CppObject*)(EventHandler_t277755526 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.FirebaseAuth::SignInWithEmailAndPasswordAsync(System.String,System.String)
extern Il2CppClass* AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var;
extern Il2CppClass* Future_User_t3746551525_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseAuth_SignInWithEmailAndPasswordAsync_m358762043_MetadataUsageId;
extern "C"  Task_1_t3166995609 * FirebaseAuth_SignInWithEmailAndPasswordAsync_m358762043 (FirebaseAuth_t3105883899 * __this, String_t* ___email0, String_t* ___password1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_SignInWithEmailAndPasswordAsync_m358762043_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Task_1_t3166995609 * V_0 = NULL;
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_0();
		String_t* L_1 = ___email0;
		String_t* L_2 = ___password1;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var);
		IntPtr_t L_3 = AuthUtilPINVOKE_Firebase_Auth_FirebaseAuth_SignInWithEmailAndPassword_m475468804(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		Future_User_t3746551525 * L_4 = (Future_User_t3746551525 *)il2cpp_codegen_object_new(Future_User_t3746551525_il2cpp_TypeInfo_var);
		Future_User__ctor_m341523963(L_4, L_3, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Future_User_t3746551525_il2cpp_TypeInfo_var);
		Task_1_t3166995609 * L_5 = Future_User_GetTask_m1069196464(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Task_1_t3166995609 * L_6 = V_0;
		return L_6;
	}
}
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.FirebaseAuth::CreateUserWithEmailAndPasswordAsync(System.String,System.String)
extern Il2CppClass* AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var;
extern Il2CppClass* Future_User_t3746551525_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseAuth_CreateUserWithEmailAndPasswordAsync_m1849539524_MetadataUsageId;
extern "C"  Task_1_t3166995609 * FirebaseAuth_CreateUserWithEmailAndPasswordAsync_m1849539524 (FirebaseAuth_t3105883899 * __this, String_t* ___email0, String_t* ___password1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_CreateUserWithEmailAndPasswordAsync_m1849539524_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Task_1_t3166995609 * V_0 = NULL;
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_0();
		String_t* L_1 = ___email0;
		String_t* L_2 = ___password1;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var);
		IntPtr_t L_3 = AuthUtilPINVOKE_Firebase_Auth_FirebaseAuth_CreateUserWithEmailAndPassword_m2159767857(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		Future_User_t3746551525 * L_4 = (Future_User_t3746551525 *)il2cpp_codegen_object_new(Future_User_t3746551525_il2cpp_TypeInfo_var);
		Future_User__ctor_m341523963(L_4, L_3, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Future_User_t3746551525_il2cpp_TypeInfo_var);
		Task_1_t3166995609 * L_5 = Future_User_GetTask_m1069196464(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Task_1_t3166995609 * L_6 = V_0;
		return L_6;
	}
}
// Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseAuth::GetAuthInternal(Firebase.FirebaseApp,Firebase.InitResult&)
extern Il2CppClass* FirebaseApp_t210707726_il2cpp_TypeInfo_var;
extern Il2CppClass* AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseAuth_t3105883899_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseAuth_GetAuthInternal_m794040299_MetadataUsageId;
extern "C"  FirebaseAuth_t3105883899 * FirebaseAuth_GetAuthInternal_m794040299 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, int32_t* ___init_result_out1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_GetAuthInternal_m794040299_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1;
	memset(&V_1, 0, sizeof(V_1));
	FirebaseAuth_t3105883899 * V_2 = NULL;
	FirebaseAuth_t3105883899 * V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	FirebaseAuth_t3105883899 * G_B4_0 = NULL;
	{
		V_0 = 0;
	}

IL_0002:
	try
	{ // begin try (depth: 1)
		{
			FirebaseApp_t210707726 * L_0 = ___app0;
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
			HandleRef_t2419939847  L_1 = FirebaseApp_getCPtr_m4255293038(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var);
			IntPtr_t L_2 = AuthUtilPINVOKE_Firebase_Auth_FirebaseAuth_GetAuthInternal_m6131012(NULL /*static, unused*/, L_1, (&V_0), /*hidden argument*/NULL);
			V_1 = L_2;
			IntPtr_t L_3 = V_1;
			IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_5 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
			if (!L_5)
			{
				goto IL_0026;
			}
		}

IL_0020:
		{
			G_B4_0 = ((FirebaseAuth_t3105883899 *)(NULL));
			goto IL_002d;
		}

IL_0026:
		{
			IntPtr_t L_6 = V_1;
			FirebaseAuth_t3105883899 * L_7 = (FirebaseAuth_t3105883899 *)il2cpp_codegen_object_new(FirebaseAuth_t3105883899_il2cpp_TypeInfo_var);
			FirebaseAuth__ctor_m3346144045(L_7, L_6, (bool)0, /*hidden argument*/NULL);
			G_B4_0 = L_7;
		}

IL_002d:
		{
			V_2 = G_B4_0;
			IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
			bool L_8 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
			if (!L_8)
			{
				goto IL_003e;
			}
		}

IL_0038:
		{
			IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
			Exception_t1927440687 * L_9 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
		}

IL_003e:
		{
			FirebaseAuth_t3105883899 * L_10 = V_2;
			V_3 = L_10;
			IL2CPP_LEAVE(0x49, FINALLY_0045);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		int32_t* L_11 = ___init_result_out1;
		int32_t L_12 = V_0;
		*((int32_t*)(L_11)) = (int32_t)L_12;
		IL2CPP_END_FINALLY(69)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x49, IL_0049)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0049:
	{
		FirebaseAuth_t3105883899 * L_13 = V_3;
		return L_13;
	}
}
// Firebase.Auth.FirebaseUser Firebase.Auth.FirebaseAuth::get_CurrentUser()
extern Il2CppClass* AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseUser_t4046966602_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseAuth_get_CurrentUser_m3290820871_MetadataUsageId;
extern "C"  FirebaseUser_t4046966602 * FirebaseAuth_get_CurrentUser_m3290820871 (FirebaseAuth_t3105883899 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_get_CurrentUser_m3290820871_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	FirebaseUser_t4046966602 * V_1 = NULL;
	FirebaseUser_t4046966602 * G_B3_0 = NULL;
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var);
		IntPtr_t L_1 = AuthUtilPINVOKE_Firebase_Auth_FirebaseAuth_CurrentUser_get_m4035012579(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IntPtr_t L_2 = V_0;
		IntPtr_t L_3 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_4 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0022;
		}
	}
	{
		G_B3_0 = ((FirebaseUser_t4046966602 *)(NULL));
		goto IL_0029;
	}

IL_0022:
	{
		IntPtr_t L_5 = V_0;
		FirebaseUser_t4046966602 * L_6 = (FirebaseUser_t4046966602 *)il2cpp_codegen_object_new(FirebaseUser_t4046966602_il2cpp_TypeInfo_var);
		FirebaseUser__ctor_m47287326(L_6, L_5, (bool)0, /*hidden argument*/NULL);
		G_B3_0 = L_6;
	}

IL_0029:
	{
		V_1 = G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_7 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_8 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_003a:
	{
		FirebaseUser_t4046966602 * L_9 = V_1;
		return L_9;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::.cctor()
extern Il2CppClass* Dictionary_2_t3547909217_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseAuth_t3105883899_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m4016654485_MethodInfo_var;
extern const uint32_t FirebaseAuth__cctor_m2218910303_MetadataUsageId;
extern "C"  void FirebaseAuth__cctor_m2218910303 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth__cctor_m2218910303_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3547909217 * L_0 = (Dictionary_2_t3547909217 *)il2cpp_codegen_object_new(Dictionary_2_t3547909217_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4016654485(L_0, /*hidden argument*/Dictionary_2__ctor_m4016654485_MethodInfo_var);
		((FirebaseAuth_t3105883899_StaticFields*)FirebaseAuth_t3105883899_il2cpp_TypeInfo_var->static_fields)->set_appCPtrToAuth_6(L_0);
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth/MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern "C"  void MonoPInvokeCallbackAttribute__ctor_m3782692084 (MonoPInvokeCallbackAttribute_t403298259 * __this, Type_t * ___t0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth/StateChangedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void StateChangedDelegate__ctor_m1800607001 (StateChangedDelegate_t3080659151 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.Auth.FirebaseAuth/StateChangedDelegate::Invoke(System.IntPtr)
extern "C"  void StateChangedDelegate_Invoke_m436547703 (StateChangedDelegate_t3080659151 * __this, IntPtr_t ___authCPtr0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		StateChangedDelegate_Invoke_m436547703((StateChangedDelegate_t3080659151 *)__this->get_prev_9(),___authCPtr0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___authCPtr0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___authCPtr0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, IntPtr_t ___authCPtr0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___authCPtr0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_StateChangedDelegate_t3080659151 (StateChangedDelegate_t3080659151 * __this, IntPtr_t ___authCPtr0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(reinterpret_cast<intptr_t>((___authCPtr0).get_m_value_0()));

}
// System.IAsyncResult Firebase.Auth.FirebaseAuth/StateChangedDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t StateChangedDelegate_BeginInvoke_m2514628660_MetadataUsageId;
extern "C"  Il2CppObject * StateChangedDelegate_BeginInvoke_m2514628660 (StateChangedDelegate_t3080659151 * __this, IntPtr_t ___authCPtr0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StateChangedDelegate_BeginInvoke_m2514628660_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___authCPtr0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Firebase.Auth.FirebaseAuth/StateChangedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void StateChangedDelegate_EndInvoke_m3910520615 (StateChangedDelegate_t3080659151 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Firebase.Auth.FirebaseUser::.ctor(System.IntPtr,System.Boolean)
extern "C"  void FirebaseUser__ctor_m47287326 (FirebaseUser_t4046966602 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___cPtr0;
		HandleRef_t2419939847  L_1;
		memset(&L_1, 0, sizeof(L_1));
		HandleRef__ctor_m1370162289(&L_1, __this, L_0, /*hidden argument*/NULL);
		__this->set_swigCPtr_1(L_1);
		return;
	}
}
// System.Void Firebase.Auth.FirebaseUser::Finalize()
extern "C"  void FirebaseUser_Finalize_m2016751943 (FirebaseUser_t4046966602 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		FirebaseUser_Dispose_m632137998(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void Firebase.Auth.FirebaseUser::Dispose()
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseUser_Dispose_m632137998_MetadataUsageId;
extern "C"  void FirebaseUser_Dispose_m632137998 (FirebaseUser_t4046966602 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseUser_Dispose_m632137998_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = __this;
		Il2CppObject * L_0 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			HandleRef_t2419939847 * L_1 = __this->get_address_of_swigCPtr_1();
			IntPtr_t L_2 = HandleRef_get_Handle_m769981143(L_1, /*hidden argument*/NULL);
			IntPtr_t L_3 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_4 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
			if (!L_4)
			{
				goto IL_0050;
			}
		}

IL_0022:
		{
			bool L_5 = __this->get_swigCMemOwn_0();
			if (!L_5)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			__this->set_swigCMemOwn_0((bool)0);
			HandleRef_t2419939847  L_6 = __this->get_swigCPtr_1();
			IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var);
			AuthUtilPINVOKE_Firebase_Auth_delete_FirebaseUser_m2178992323(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		}

IL_003f:
		{
			IntPtr_t L_7 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			HandleRef_t2419939847  L_8;
			memset(&L_8, 0, sizeof(L_8));
			HandleRef__ctor_m1370162289(&L_8, NULL, L_7, /*hidden argument*/NULL);
			__this->set_swigCPtr_1(L_8);
		}

IL_0050:
		{
			GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x62, FINALLY_005b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_005b;
	}

FINALLY_005b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_9 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(91)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(91)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0062:
	{
		return;
	}
}
// System.Threading.Tasks.Task`1<System.String> Firebase.Auth.FirebaseUser::TokenAsync(System.Boolean)
extern Il2CppClass* AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var;
extern Il2CppClass* FutureString_t4225986006_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseUser_TokenAsync_m3595996755_MetadataUsageId;
extern "C"  Task_1_t1149249240 * FirebaseUser_TokenAsync_m3595996755 (FirebaseUser_t4046966602 * __this, bool ___forceRefresh0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseUser_TokenAsync_m3595996755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Task_1_t1149249240 * V_0 = NULL;
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_1();
		bool L_1 = ___forceRefresh0;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var);
		IntPtr_t L_2 = AuthUtilPINVOKE_Firebase_Auth_FirebaseUser_Token_m4057723011(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		FutureString_t4225986006 * L_3 = (FutureString_t4225986006 *)il2cpp_codegen_object_new(FutureString_t4225986006_il2cpp_TypeInfo_var);
		FutureString__ctor_m1515554591(L_3, L_2, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FutureString_t4225986006_il2cpp_TypeInfo_var);
		Task_1_t1149249240 * L_4 = FutureString_GetTask_m1388189510(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Task_1_t1149249240 * L_5 = V_0;
		return L_5;
	}
}
// System.String Firebase.Auth.FirebaseUser::get_UserId()
extern Il2CppClass* AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseUser_get_UserId_m3056192957_MetadataUsageId;
extern "C"  String_t* FirebaseUser_get_UserId_m3056192957 (FirebaseUser_t4046966602 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseUser_get_UserId_m3056192957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_1();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var);
		String_t* L_1 = AuthUtilPINVOKE_Firebase_Auth_FirebaseUser_UserId_get_m572498592(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_3 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.String Firebase.Auth.FirebaseUser::get_DisplayName()
extern Il2CppClass* AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseUser_get_DisplayName_m1649983710_MetadataUsageId;
extern "C"  String_t* FirebaseUser_get_DisplayName_m1649983710 (FirebaseUser_t4046966602 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseUser_get_DisplayName_m1649983710_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_1();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var);
		String_t* L_1 = AuthUtilPINVOKE_Firebase_Auth_FirebaseUser_DisplayName_get_m594667867(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_3 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.Void Firebase.Auth.Future_User::.ctor(System.IntPtr,System.Boolean)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var;
extern const uint32_t Future_User__ctor_m341523963_MetadataUsageId;
extern "C"  void Future_User__ctor_m341523963 (Future_User_t3746551525 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Future_User__ctor_m341523963_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_callbackData_6(L_0);
		IntPtr_t L_1 = ___cPtr0;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var);
		IntPtr_t L_2 = AuthUtilPINVOKE_Firebase_Auth_Future_User_SWIGUpcast_m327913273(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		bool L_3 = ___cMemoryOwn1;
		FutureBase__ctor_m3054944159(__this, L_2, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4 = ___cPtr0;
		HandleRef_t2419939847  L_5;
		memset(&L_5, 0, sizeof(L_5));
		HandleRef__ctor_m1370162289(&L_5, __this, L_4, /*hidden argument*/NULL);
		__this->set_swigCPtr_2(L_5);
		return;
	}
}
// System.Void Firebase.Auth.Future_User::Finalize()
extern "C"  void Future_User_Finalize_m2443298420 (Future_User_t3746551525 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker0::Invoke(5 /* System.Void Firebase.FutureBase::Dispose() */, __this);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		FutureBase_Finalize_m1464906966(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void Firebase.Auth.Future_User::Dispose()
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var;
extern const uint32_t Future_User_Dispose_m3386840133_MetadataUsageId;
extern "C"  void Future_User_Dispose_m3386840133 (Future_User_t3746551525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Future_User_Dispose_m3386840133_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = __this;
		Il2CppObject * L_0 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			HandleRef_t2419939847 * L_1 = __this->get_address_of_swigCPtr_2();
			IntPtr_t L_2 = HandleRef_get_Handle_m769981143(L_1, /*hidden argument*/NULL);
			IntPtr_t L_3 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_4 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
			if (!L_4)
			{
				goto IL_0050;
			}
		}

IL_0022:
		{
			bool L_5 = ((FutureBase_t2698306134 *)__this)->get_swigCMemOwn_1();
			if (!L_5)
			{
				goto IL_003f;
			}
		}

IL_002d:
		{
			((FutureBase_t2698306134 *)__this)->set_swigCMemOwn_1((bool)0);
			HandleRef_t2419939847  L_6 = __this->get_swigCPtr_2();
			IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var);
			AuthUtilPINVOKE_Firebase_Auth_delete_Future_User_m2507266046(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		}

IL_003f:
		{
			IntPtr_t L_7 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			HandleRef_t2419939847  L_8;
			memset(&L_8, 0, sizeof(L_8));
			HandleRef__ctor_m1370162289(&L_8, NULL, L_7, /*hidden argument*/NULL);
			__this->set_swigCPtr_2(L_8);
		}

IL_0050:
		{
			IntPtr_t L_9 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			Future_User_SetCompletionData_m2358975060(__this, L_9, /*hidden argument*/NULL);
			GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
			FutureBase_Dispose_m1042966601(__this, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x73, FINALLY_006c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_006c;
	}

FINALLY_006c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_10 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(108)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(108)
	{
		IL2CPP_JUMP_TBL(0x73, IL_0073)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0073:
	{
		return;
	}
}
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.Future_User::GetTask(Firebase.Auth.Future_User)
extern Il2CppClass* U3CGetTaskU3Ec__AnonStorey0_t2356161773_il2cpp_TypeInfo_var;
extern Il2CppClass* TaskCompletionSource_1_t2929400682_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseException_t2567272216_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t1614918345_il2cpp_TypeInfo_var;
extern const MethodInfo* TaskCompletionSource_1__ctor_m1654415365_MethodInfo_var;
extern const MethodInfo* TaskCompletionSource_1_SetException_m872764268_MethodInfo_var;
extern const MethodInfo* TaskCompletionSource_1_get_Task_m601916200_MethodInfo_var;
extern const MethodInfo* U3CGetTaskU3Ec__AnonStorey0_U3CU3Em__0_m618574514_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1074014380;
extern const uint32_t Future_User_GetTask_m1069196464_MetadataUsageId;
extern "C"  Task_1_t3166995609 * Future_User_GetTask_m1069196464 (Il2CppObject * __this /* static, unused */, Future_User_t3746551525 * ___fu0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Future_User_GetTask_m1069196464_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetTaskU3Ec__AnonStorey0_t2356161773 * V_0 = NULL;
	{
		U3CGetTaskU3Ec__AnonStorey0_t2356161773 * L_0 = (U3CGetTaskU3Ec__AnonStorey0_t2356161773 *)il2cpp_codegen_object_new(U3CGetTaskU3Ec__AnonStorey0_t2356161773_il2cpp_TypeInfo_var);
		U3CGetTaskU3Ec__AnonStorey0__ctor_m3797510095(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetTaskU3Ec__AnonStorey0_t2356161773 * L_1 = V_0;
		Future_User_t3746551525 * L_2 = ___fu0;
		NullCheck(L_1);
		L_1->set_fu_0(L_2);
		U3CGetTaskU3Ec__AnonStorey0_t2356161773 * L_3 = V_0;
		TaskCompletionSource_1_t2929400682 * L_4 = (TaskCompletionSource_1_t2929400682 *)il2cpp_codegen_object_new(TaskCompletionSource_1_t2929400682_il2cpp_TypeInfo_var);
		TaskCompletionSource_1__ctor_m1654415365(L_4, /*hidden argument*/TaskCompletionSource_1__ctor_m1654415365_MethodInfo_var);
		NullCheck(L_3);
		L_3->set_tcs_1(L_4);
		U3CGetTaskU3Ec__AnonStorey0_t2356161773 * L_5 = V_0;
		NullCheck(L_5);
		Future_User_t3746551525 * L_6 = L_5->get_fu_0();
		NullCheck(L_6);
		int32_t L_7 = FutureBase_Status_m543620527(L_6, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)2))))
		{
			goto IL_004b;
		}
	}
	{
		U3CGetTaskU3Ec__AnonStorey0_t2356161773 * L_8 = V_0;
		NullCheck(L_8);
		TaskCompletionSource_1_t2929400682 * L_9 = L_8->get_tcs_1();
		FirebaseException_t2567272216 * L_10 = (FirebaseException_t2567272216 *)il2cpp_codegen_object_new(FirebaseException_t2567272216_il2cpp_TypeInfo_var);
		FirebaseException__ctor_m3734642555(L_10, 0, _stringLiteral1074014380, /*hidden argument*/NULL);
		NullCheck(L_9);
		TaskCompletionSource_1_SetException_m872764268(L_9, L_10, /*hidden argument*/TaskCompletionSource_1_SetException_m872764268_MethodInfo_var);
		U3CGetTaskU3Ec__AnonStorey0_t2356161773 * L_11 = V_0;
		NullCheck(L_11);
		TaskCompletionSource_1_t2929400682 * L_12 = L_11->get_tcs_1();
		NullCheck(L_12);
		Task_1_t3166995609 * L_13 = TaskCompletionSource_1_get_Task_m601916200(L_12, /*hidden argument*/TaskCompletionSource_1_get_Task_m601916200_MethodInfo_var);
		return L_13;
	}

IL_004b:
	{
		U3CGetTaskU3Ec__AnonStorey0_t2356161773 * L_14 = V_0;
		NullCheck(L_14);
		Future_User_t3746551525 * L_15 = L_14->get_fu_0();
		U3CGetTaskU3Ec__AnonStorey0_t2356161773 * L_16 = V_0;
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)U3CGetTaskU3Ec__AnonStorey0_U3CU3Em__0_m618574514_MethodInfo_var);
		Action_t1614918345 * L_18 = (Action_t1614918345 *)il2cpp_codegen_object_new(Action_t1614918345_il2cpp_TypeInfo_var);
		Action__ctor_m1412355475(L_18, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_15);
		Future_User_SetOnCompletionCallback_m2991677111(L_15, L_18, /*hidden argument*/NULL);
		U3CGetTaskU3Ec__AnonStorey0_t2356161773 * L_19 = V_0;
		NullCheck(L_19);
		TaskCompletionSource_1_t2929400682 * L_20 = L_19->get_tcs_1();
		NullCheck(L_20);
		Task_1_t3166995609 * L_21 = TaskCompletionSource_1_get_Task_m601916200(L_20, /*hidden argument*/TaskCompletionSource_1_get_Task_m601916200_MethodInfo_var);
		return L_21;
	}
}
// System.Void Firebase.Auth.Future_User::SetOnCompletionCallback(Firebase.Auth.Future_User/Action)
extern Il2CppClass* SWIG_CompletionDelegate_t2830873745_il2cpp_TypeInfo_var;
extern Il2CppClass* Future_User_t3746551525_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t622743980_il2cpp_TypeInfo_var;
extern const MethodInfo* Future_User_SWIG_CompletionDispatcher_m1755402693_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1928557225_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m3243981592_MethodInfo_var;
extern const uint32_t Future_User_SetOnCompletionCallback_m2991677111_MetadataUsageId;
extern "C"  void Future_User_SetOnCompletionCallback_m2991677111 (Future_User_t3746551525 * __this, Action_t1614918345 * ___userCompletionCallback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Future_User_SetOnCompletionCallback_m2991677111_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		SWIG_CompletionDelegate_t2830873745 * L_0 = __this->get_SWIG_CompletionCB_7();
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)Future_User_SWIG_CompletionDispatcher_m1755402693_MethodInfo_var);
		SWIG_CompletionDelegate_t2830873745 * L_2 = (SWIG_CompletionDelegate_t2830873745 *)il2cpp_codegen_object_new(SWIG_CompletionDelegate_t2830873745_il2cpp_TypeInfo_var);
		SWIG_CompletionDelegate__ctor_m944757911(L_2, NULL, L_1, /*hidden argument*/NULL);
		__this->set_SWIG_CompletionCB_7(L_2);
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Future_User_t3746551525_il2cpp_TypeInfo_var);
		Il2CppObject * L_3 = ((Future_User_t3746551525_StaticFields*)Future_User_t3746551525_il2cpp_TypeInfo_var->static_fields)->get_CallbackLock_5();
		V_1 = L_3;
		Il2CppObject * L_4 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(Future_User_t3746551525_il2cpp_TypeInfo_var);
			Dictionary_2_t622743980 * L_5 = ((Future_User_t3746551525_StaticFields*)Future_User_t3746551525_il2cpp_TypeInfo_var->static_fields)->get_Callbacks_3();
			if (L_5)
			{
				goto IL_003d;
			}
		}

IL_0033:
		{
			Dictionary_2_t622743980 * L_6 = (Dictionary_2_t622743980 *)il2cpp_codegen_object_new(Dictionary_2_t622743980_il2cpp_TypeInfo_var);
			Dictionary_2__ctor_m1928557225(L_6, /*hidden argument*/Dictionary_2__ctor_m1928557225_MethodInfo_var);
			IL2CPP_RUNTIME_CLASS_INIT(Future_User_t3746551525_il2cpp_TypeInfo_var);
			((Future_User_t3746551525_StaticFields*)Future_User_t3746551525_il2cpp_TypeInfo_var->static_fields)->set_Callbacks_3(L_6);
		}

IL_003d:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Future_User_t3746551525_il2cpp_TypeInfo_var);
			int32_t L_7 = ((Future_User_t3746551525_StaticFields*)Future_User_t3746551525_il2cpp_TypeInfo_var->static_fields)->get_CallbackIndex_4();
			int32_t L_8 = ((int32_t)((int32_t)L_7+(int32_t)1));
			((Future_User_t3746551525_StaticFields*)Future_User_t3746551525_il2cpp_TypeInfo_var->static_fields)->set_CallbackIndex_4(L_8);
			V_0 = L_8;
			Dictionary_2_t622743980 * L_9 = ((Future_User_t3746551525_StaticFields*)Future_User_t3746551525_il2cpp_TypeInfo_var->static_fields)->get_Callbacks_3();
			int32_t L_10 = V_0;
			Action_t1614918345 * L_11 = ___userCompletionCallback0;
			NullCheck(L_9);
			Dictionary_2_set_Item_m3243981592(L_9, L_10, L_11, /*hidden argument*/Dictionary_2_set_Item_m3243981592_MethodInfo_var);
			IL2CPP_LEAVE(0x63, FINALLY_005c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_005c;
	}

FINALLY_005c:
	{ // begin finally (depth: 1)
		Il2CppObject * L_12 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(92)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(92)
	{
		IL2CPP_JUMP_TBL(0x63, IL_0063)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0063:
	{
		SWIG_CompletionDelegate_t2830873745 * L_13 = __this->get_SWIG_CompletionCB_7();
		int32_t L_14 = V_0;
		IntPtr_t L_15 = Future_User_SWIG_OnCompletion_m2671901145(__this, L_13, L_14, /*hidden argument*/NULL);
		Future_User_SetCompletionData_m2358975060(__this, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.Future_User::SetCompletionData(System.IntPtr)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Future_User_SetCompletionData_m2358975060_MetadataUsageId;
extern "C"  void Future_User_SetCompletionData_m2358975060 (Future_User_t3746551525 * __this, IntPtr_t ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Future_User_SetCompletionData_m2358975060_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_callbackData_6();
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		IntPtr_t L_3 = __this->get_callbackData_6();
		Future_User_SWIG_FreeCompletionData_m2921042785(__this, L_3, /*hidden argument*/NULL);
	}

IL_0021:
	{
		IntPtr_t L_4 = ___data0;
		__this->set_callbackData_6(L_4);
		return;
	}
}
// System.Void Firebase.Auth.Future_User::SWIG_CompletionDispatcher(System.Int32)
extern Il2CppClass* Future_User_t3746551525_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m1413651722_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Remove_m3017600130_MethodInfo_var;
extern const uint32_t Future_User_SWIG_CompletionDispatcher_m1755402693_MetadataUsageId;
extern "C"  void Future_User_SWIG_CompletionDispatcher_m1755402693 (Il2CppObject * __this /* static, unused */, int32_t ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Future_User_SWIG_CompletionDispatcher_m1755402693_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_t1614918345 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (Action_t1614918345 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(Future_User_t3746551525_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((Future_User_t3746551525_StaticFields*)Future_User_t3746551525_il2cpp_TypeInfo_var->static_fields)->get_CallbackLock_5();
		V_1 = L_0;
		Il2CppObject * L_1 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(Future_User_t3746551525_il2cpp_TypeInfo_var);
			Dictionary_2_t622743980 * L_2 = ((Future_User_t3746551525_StaticFields*)Future_User_t3746551525_il2cpp_TypeInfo_var->static_fields)->get_Callbacks_3();
			if (!L_2)
			{
				goto IL_0036;
			}
		}

IL_0018:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Future_User_t3746551525_il2cpp_TypeInfo_var);
			Dictionary_2_t622743980 * L_3 = ((Future_User_t3746551525_StaticFields*)Future_User_t3746551525_il2cpp_TypeInfo_var->static_fields)->get_Callbacks_3();
			int32_t L_4 = ___key0;
			NullCheck(L_3);
			bool L_5 = Dictionary_2_TryGetValue_m1413651722(L_3, L_4, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1413651722_MethodInfo_var);
			if (!L_5)
			{
				goto IL_0036;
			}
		}

IL_002a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Future_User_t3746551525_il2cpp_TypeInfo_var);
			Dictionary_2_t622743980 * L_6 = ((Future_User_t3746551525_StaticFields*)Future_User_t3746551525_il2cpp_TypeInfo_var->static_fields)->get_Callbacks_3();
			int32_t L_7 = ___key0;
			NullCheck(L_6);
			Dictionary_2_Remove_m3017600130(L_6, L_7, /*hidden argument*/Dictionary_2_Remove_m3017600130_MethodInfo_var);
		}

IL_0036:
		{
			IL2CPP_LEAVE(0x42, FINALLY_003b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_003b;
	}

FINALLY_003b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(59)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(59)
	{
		IL2CPP_JUMP_TBL(0x42, IL_0042)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0042:
	{
		Action_t1614918345 * L_9 = V_0;
		if (!L_9)
		{
			goto IL_004e;
		}
	}
	{
		Action_t1614918345 * L_10 = V_0;
		NullCheck(L_10);
		Action_Invoke_m739197615(L_10, /*hidden argument*/NULL);
	}

IL_004e:
	{
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Future_User_SWIG_CompletionDispatcher_m1755402693(int32_t ___key0)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Managed method invocation
	::Future_User_SWIG_CompletionDispatcher_m1755402693(NULL, ___key0, NULL);

}
// System.IntPtr Firebase.Auth.Future_User::SWIG_OnCompletion(Firebase.Auth.Future_User/SWIG_CompletionDelegate,System.Int32)
extern Il2CppClass* AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t Future_User_SWIG_OnCompletion_m2671901145_MetadataUsageId;
extern "C"  IntPtr_t Future_User_SWIG_OnCompletion_m2671901145 (Future_User_t3746551525 * __this, SWIG_CompletionDelegate_t2830873745 * ___cs_callback0, int32_t ___cs_key1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Future_User_SWIG_OnCompletion_m2671901145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_2();
		SWIG_CompletionDelegate_t2830873745 * L_1 = ___cs_callback0;
		int32_t L_2 = ___cs_key1;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var);
		IntPtr_t L_3 = AuthUtilPINVOKE_Firebase_Auth_Future_User_SWIG_OnCompletion_m420864614(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_4 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_5 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_001e:
	{
		IntPtr_t L_6 = V_0;
		return L_6;
	}
}
// System.Void Firebase.Auth.Future_User::SWIG_FreeCompletionData(System.IntPtr)
extern Il2CppClass* AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t Future_User_SWIG_FreeCompletionData_m2921042785_MetadataUsageId;
extern "C"  void Future_User_SWIG_FreeCompletionData_m2921042785 (Future_User_t3746551525 * __this, IntPtr_t ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Future_User_SWIG_FreeCompletionData_m2921042785_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_2();
		IntPtr_t L_1 = ___data0;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var);
		AuthUtilPINVOKE_Firebase_Auth_Future_User_SWIG_FreeCompletionData_m4173504260(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_3 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		return;
	}
}
// Firebase.Auth.FirebaseUser Firebase.Auth.Future_User::Result()
extern Il2CppClass* AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseUser_t4046966602_il2cpp_TypeInfo_var;
extern Il2CppClass* SWIGPendingException_t4193433529_il2cpp_TypeInfo_var;
extern const uint32_t Future_User_Result_m1430738177_MetadataUsageId;
extern "C"  FirebaseUser_t4046966602 * Future_User_Result_m1430738177 (Future_User_t3746551525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Future_User_Result_m1430738177_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	FirebaseUser_t4046966602 * V_1 = NULL;
	FirebaseUser_t4046966602 * G_B3_0 = NULL;
	{
		HandleRef_t2419939847  L_0 = __this->get_swigCPtr_2();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t2761888122_il2cpp_TypeInfo_var);
		IntPtr_t L_1 = AuthUtilPINVOKE_Firebase_Auth_Future_User_Result_m2641877733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IntPtr_t L_2 = V_0;
		IntPtr_t L_3 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_4 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0022;
		}
	}
	{
		G_B3_0 = ((FirebaseUser_t4046966602 *)(NULL));
		goto IL_0029;
	}

IL_0022:
	{
		IntPtr_t L_5 = V_0;
		FirebaseUser_t4046966602 * L_6 = (FirebaseUser_t4046966602 *)il2cpp_codegen_object_new(FirebaseUser_t4046966602_il2cpp_TypeInfo_var);
		FirebaseUser__ctor_m47287326(L_6, L_5, (bool)0, /*hidden argument*/NULL);
		G_B3_0 = L_6;
	}

IL_0029:
	{
		V_1 = G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		bool L_7 = SWIGPendingException_get_Pending_m1368465104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t4193433529_il2cpp_TypeInfo_var);
		Exception_t1927440687 * L_8 = SWIGPendingException_Retrieve_m197352211(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_003a:
	{
		FirebaseUser_t4046966602 * L_9 = V_1;
		return L_9;
	}
}
// System.Void Firebase.Auth.Future_User::.cctor()
extern Il2CppClass* Future_User_t3746551525_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Future_User__cctor_m3764872609_MetadataUsageId;
extern "C"  void Future_User__cctor_m3764872609 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Future_User__cctor_m3764872609_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Future_User_t3746551525_StaticFields*)Future_User_t3746551525_il2cpp_TypeInfo_var->static_fields)->set_CallbackIndex_4(0);
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		((Future_User_t3746551525_StaticFields*)Future_User_t3746551525_il2cpp_TypeInfo_var->static_fields)->set_CallbackLock_5(L_0);
		return;
	}
}
// System.Void Firebase.Auth.Future_User/<GetTask>c__AnonStorey0::.ctor()
extern "C"  void U3CGetTaskU3Ec__AnonStorey0__ctor_m3797510095 (U3CGetTaskU3Ec__AnonStorey0_t2356161773 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.Future_User/<GetTask>c__AnonStorey0::<>m__0()
extern Il2CppClass* FirebaseException_t2567272216_il2cpp_TypeInfo_var;
extern const MethodInfo* TaskCompletionSource_1_SetCanceled_m340886328_MethodInfo_var;
extern const MethodInfo* TaskCompletionSource_1_SetException_m872764268_MethodInfo_var;
extern const MethodInfo* TaskCompletionSource_1_SetResult_m210845803_MethodInfo_var;
extern const uint32_t U3CGetTaskU3Ec__AnonStorey0_U3CU3Em__0_m618574514_MetadataUsageId;
extern "C"  void U3CGetTaskU3Ec__AnonStorey0_U3CU3Em__0_m618574514 (U3CGetTaskU3Ec__AnonStorey0_t2356161773 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetTaskU3Ec__AnonStorey0_U3CU3Em__0_m618574514_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Future_User_t3746551525 * L_0 = __this->get_fu_0();
		NullCheck(L_0);
		int32_t L_1 = FutureBase_Status_m543620527(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0021;
		}
	}
	{
		TaskCompletionSource_1_t2929400682 * L_2 = __this->get_tcs_1();
		NullCheck(L_2);
		TaskCompletionSource_1_SetCanceled_m340886328(L_2, /*hidden argument*/TaskCompletionSource_1_SetCanceled_m340886328_MethodInfo_var);
		goto IL_006a;
	}

IL_0021:
	{
		Future_User_t3746551525 * L_3 = __this->get_fu_0();
		NullCheck(L_3);
		int32_t L_4 = FutureBase_Error_m954039370(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if (!L_5)
		{
			goto IL_0054;
		}
	}
	{
		TaskCompletionSource_1_t2929400682 * L_6 = __this->get_tcs_1();
		int32_t L_7 = V_0;
		Future_User_t3746551525 * L_8 = __this->get_fu_0();
		NullCheck(L_8);
		String_t* L_9 = FutureBase_ErrorMessage_m11232826(L_8, /*hidden argument*/NULL);
		FirebaseException_t2567272216 * L_10 = (FirebaseException_t2567272216 *)il2cpp_codegen_object_new(FirebaseException_t2567272216_il2cpp_TypeInfo_var);
		FirebaseException__ctor_m3734642555(L_10, L_7, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		TaskCompletionSource_1_SetException_m872764268(L_6, L_10, /*hidden argument*/TaskCompletionSource_1_SetException_m872764268_MethodInfo_var);
		goto IL_006a;
	}

IL_0054:
	{
		TaskCompletionSource_1_t2929400682 * L_11 = __this->get_tcs_1();
		Future_User_t3746551525 * L_12 = __this->get_fu_0();
		NullCheck(L_12);
		FirebaseUser_t4046966602 * L_13 = Future_User_Result_m1430738177(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		TaskCompletionSource_1_SetResult_m210845803(L_11, L_13, /*hidden argument*/TaskCompletionSource_1_SetResult_m210845803_MethodInfo_var);
	}

IL_006a:
	{
		Future_User_t3746551525 * L_14 = __this->get_fu_0();
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(5 /* System.Void Firebase.FutureBase::Dispose() */, L_14);
		return;
	}
}
// System.Void Firebase.Auth.Future_User/Action::.ctor(System.Object,System.IntPtr)
extern "C"  void Action__ctor_m1412355475 (Action_t1614918345 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.Auth.Future_User/Action::Invoke()
extern "C"  void Action_Invoke_m739197615 (Action_t1614918345 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_Invoke_m739197615((Action_t1614918345 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_Action_t1614918345 (Action_t1614918345 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult Firebase.Auth.Future_User/Action::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_BeginInvoke_m2335366190 (Action_t1614918345 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void Firebase.Auth.Future_User/Action::EndInvoke(System.IAsyncResult)
extern "C"  void Action_EndInvoke_m3042904045 (Action_t1614918345 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Firebase.Auth.Future_User/MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern "C"  void MonoPInvokeCallbackAttribute__ctor_m1256182244 (MonoPInvokeCallbackAttribute_t629937593 * __this, Type_t * ___t0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.Future_User/SWIG_CompletionDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void SWIG_CompletionDelegate__ctor_m944757911 (SWIG_CompletionDelegate_t2830873745 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.Auth.Future_User/SWIG_CompletionDelegate::Invoke(System.Int32)
extern "C"  void SWIG_CompletionDelegate_Invoke_m4095112822 (SWIG_CompletionDelegate_t2830873745 * __this, int32_t ___index0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		SWIG_CompletionDelegate_Invoke_m4095112822((SWIG_CompletionDelegate_t2830873745 *)__this->get_prev_9(),___index0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___index0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___index0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___index0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___index0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_SWIG_CompletionDelegate_t2830873745 (SWIG_CompletionDelegate_t2830873745 * __this, int32_t ___index0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___index0);

}
// System.IAsyncResult Firebase.Auth.Future_User/SWIG_CompletionDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t SWIG_CompletionDelegate_BeginInvoke_m3385373825_MetadataUsageId;
extern "C"  Il2CppObject * SWIG_CompletionDelegate_BeginInvoke_m3385373825 (SWIG_CompletionDelegate_t2830873745 * __this, int32_t ___index0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIG_CompletionDelegate_BeginInvoke_m3385373825_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___index0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Firebase.Auth.Future_User/SWIG_CompletionDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void SWIG_CompletionDelegate_EndInvoke_m2520415465 (SWIG_CompletionDelegate_t2830873745 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
