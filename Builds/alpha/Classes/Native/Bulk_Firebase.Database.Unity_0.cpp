﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Firebase.Database.Unity.PreserveAttribute
struct PreserveAttribute_t2023989938;
// Firebase.Database.Unity.ServiceAccountCredential
struct ServiceAccountCredential_t3398163339;
// Firebase.Database.Unity.ServiceAccountCredential/Initializer
struct Initializer_t1955556897;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Security.Cryptography.RSACryptoServiceProvider
struct RSACryptoServiceProvider_t4229286967;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Firebase.Database.Unity.ServiceAccountCredential/OAuthRequest
struct OAuthRequest_t3497136277;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Firebase.Database.Unity.ServiceAccountCredential/<GetAccessTokenForRequestSync>c__AnonStorey2
struct U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_t3624071070;
// Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0
struct U3CSendOAuthU3Ec__Iterator0_t1559836771;
// System.Object
struct Il2CppObject;
// Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0/<SendOAuth>c__AnonStorey1
struct U3CSendOAuthU3Ec__AnonStorey1_t520895791;
// System.Net.UploadValuesCompletedEventArgs
struct UploadValuesCompletedEventArgs_t3564452537;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t4056456767;
// Firebase.Database.Unity.ServiceAccountCredential/UploadCompleted
struct UploadCompleted_t2145858625;
// Firebase.Database.Unity.ServiceCredential
struct ServiceCredential_t2286218796;
// Firebase.Database.Unity.ServiceCredential/Initializer
struct Initializer_t2915825156;
// Firebase.Database.Unity.IClock
struct IClock_t1761322331;
// Firebase.Database.Unity.SystemClock
struct SystemClock_t3351332359;
// Firebase.Database.Unity.TreeNodeIListTransactionData
struct TreeNodeIListTransactionData_t390449921;
// Firebase.Database.Unity.UnityAuthTokenProvider
struct UnityAuthTokenProvider_t371495853;
// Firebase.FirebaseApp
struct FirebaseApp_t210707726;
// Firebase.Database.Core.AuthTokenProvider/IGetTokenCompletionListener
struct IGetTokenCompletionListener_t2927602790;
// System.Threading.Tasks.Task`1<System.String>
struct Task_1_t1149249240;
// System.Func`2<System.Threading.Tasks.Task`1<System.String>,System.String>
struct Func_2_t3938612258;
// System.Threading.Tasks.Task`1<System.Object>
struct Task_1_t1809478302;
// System.Func`2<System.Threading.Tasks.Task`1<System.Object>,System.Object>
struct Func_2_t2276122186;
// Firebase.Database.Core.AuthTokenProvider/ITokenChangeListener
struct ITokenChangeListener_t4180538330;
// Firebase.Database.Unity.UnityAuthTokenProvider/<AddTokenChangeListener>c__AnonStorey1
struct U3CAddTokenChangeListenerU3Ec__AnonStorey1_t3054348951;
// System.EventArgs
struct EventArgs_t3289624707;
// Firebase.Database.Unity.UnityAuthTokenProvider/<GetToken>c__AnonStorey0
struct U3CGetTokenU3Ec__AnonStorey0_t4052243837;
// Firebase.Database.Unity.UnityPlatform
struct UnityPlatform_t3623770554;
// Firebase.Database.Logger
struct Logger_t225270238;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_t2570160834;
// Firebase.Database.Core.AuthTokenProvider
struct AuthTokenProvider_t3681374264;
// Firebase.Database.DotNet.DotNetPlatform
struct DotNetPlatform_t2951135975;
// Firebase.Database.Unity.UnityPlatform/UnityLogger
struct UnityLogger_t2258131283;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "Firebase_Database_Unity_U3CModuleU3E3783534214.h"
#include "Firebase_Database_Unity_U3CModuleU3E3783534214MethodDeclarations.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Go3945198801.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Go3945198801MethodDeclarations.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Pr2023989938.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Pr2023989938MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Attribute542643598MethodDeclarations.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se3398163339.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se3398163339MethodDeclarations.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se1955556897.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se2286218796MethodDeclarations.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se1955556897MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365MethodDeclarations.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se2915825156.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Security_Cryptography_RSACryptoSer4229286967.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se3497136277.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se1559836771MethodDeclarations.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se1559836771.h"
#include "Unity_Tasks_System_Threading_CancellationToken1851405782.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se3624071070MethodDeclarations.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se3497136277MethodDeclarations.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext1051733884MethodDeclarations.h"
#include "System_Core_System_Func_1_gen3420419431MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "Google_MiniJson_Google_MiniJSON_Json3279401392MethodDeclarations.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se3624071070.h"
#include "mscorlib_System_Object2689449295.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext1051733884.h"
#include "System_Core_System_Func_1_gen3420419431.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding663144255MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_RSACryptoSer4229286967MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_Text_Encoding663144255.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_DateTime693205669MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_TimeSpan3430258949MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_DateTimeKind2186819611.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler2907300047MethodDeclarations.h"
#include "System_System_Net_WebClient1432723993MethodDeclarations.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Ser520895791MethodDeclarations.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564MethodDeclarations.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se2145858625MethodDeclarations.h"
#include "System_System_Net_UploadValuesCompletedEventHandler563858374MethodDeclarations.h"
#include "System_System_Uri19570940MethodDeclarations.h"
#include "System_System_ComponentModel_AsyncCompletedEventArgs83270938MethodDeclarations.h"
#include "System_System_Net_UploadValuesCompletedEventArgs3564452537MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "Firebase_App_Firebase_LogLevel543421840.h"
#include "System_System_Net_WebClient1432723993.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Ser520895791.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se2145858625.h"
#include "System_System_Net_UploadValuesCompletedEventArgs3564452537.h"
#include "System_System_Net_UploadValuesCompletedEventHandler563858374.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se2915825156MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificat4056456767.h"
#include "System_System_Security_Cryptography_X509Certificat4056456767MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricAlg784058677.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se2286218796.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Sy3351332359.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Sy3351332359MethodDeclarations.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Tre390449921.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Tre390449921MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3824902566MethodDeclarations.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Uni371495853.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Uni371495853MethodDeclarations.h"
#include "Firebase_App_Firebase_FirebaseApp210707726.h"
#include "Firebase_Database_Firebase_Database_Core_AuthToken3681374264MethodDeclarations.h"
#include "Firebase_App_Firebase_FirebaseApp210707726MethodDeclarations.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Un4052243837MethodDeclarations.h"
#include "Firebase_Database_Unity_Firebase_Unity_Editor_Fireba83728876MethodDeclarations.h"
#include "mscorlib_System_IO_File1930543328MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Core_GAuthToke2345784186MethodDeclarations.h"
#include "Firebase_Auth_Firebase_Auth_FirebaseAuth3105883899MethodDeclarations.h"
#include "Firebase_Auth_Firebase_Auth_FirebaseUser4046966602MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3938612258MethodDeclarations.h"
#include "Unity_Tasks_System_Threading_Tasks_Task_1_gen1149249240MethodDeclarations.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Un4052243837.h"
#include "Firebase_Database_Firebase_Database_Core_GAuthToke2345784186.h"
#include "Firebase_Auth_Firebase_Auth_FirebaseAuth3105883899.h"
#include "Firebase_Auth_Firebase_Auth_FirebaseUser4046966602.h"
#include "mscorlib_System_Security_Cryptography_X509Certific1216946873.h"
#include "Unity_Tasks_System_Threading_Tasks_Task_1_gen1149249240.h"
#include "System_Core_System_Func_2_gen3938612258.h"
#include "Unity_Tasks_System_Threading_Tasks_Task_1_gen1809478302.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Un3054348951MethodDeclarations.h"
#include "mscorlib_System_EventHandler277755526MethodDeclarations.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Un3054348951.h"
#include "mscorlib_System_EventArgs3289624707.h"
#include "mscorlib_System_EventHandler277755526.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Un3623770554.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Un3623770554MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_DotNet_DotNetP2951135975MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3961743794MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Core_FirebaseC1046005831MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3961743794.h"
#include "Firebase_Database_Firebase_Database_DotNet_DotNetP2951135975.h"
#include "Firebase_Database_Firebase_Database_Logger_Level2798387899.h"
#include "Firebase_Database_Firebase_Database_Logger225270238.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Un2258131283MethodDeclarations.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Un2258131283.h"
#include "Firebase_Database_Firebase_Database_Core_AuthToken3681374264.h"
#include "Firebase_Database_Firebase_Database_Logger225270238MethodDeclarations.h"
#include "mscorlib_System_Int64909078037.h"
#include "Firebase_Database_Unity_Firebase_Unity_Editor_Fireba83728876.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "Firebase_App_Firebase_Internal_FirebaseEditorExten1010167188MethodDeclarations.h"

// System.Threading.Tasks.Task`1<!!0> System.Threading.Tasks.Task`1<System.Object>::ContinueWith<System.Object>(System.Func`2<System.Threading.Tasks.Task`1<!0>,!!0>)
extern "C"  Task_1_t1809478302 * Task_1_ContinueWith_TisIl2CppObject_m524392371_gshared (Task_1_t1809478302 * __this, Func_2_t2276122186 * p0, const MethodInfo* method);
#define Task_1_ContinueWith_TisIl2CppObject_m524392371(__this, p0, method) ((  Task_1_t1809478302 * (*) (Task_1_t1809478302 *, Func_2_t2276122186 *, const MethodInfo*))Task_1_ContinueWith_TisIl2CppObject_m524392371_gshared)(__this, p0, method)
// System.Threading.Tasks.Task`1<!!0> System.Threading.Tasks.Task`1<System.String>::ContinueWith<System.String>(System.Func`2<System.Threading.Tasks.Task`1<!0>,!!0>)
#define Task_1_ContinueWith_TisString_t_m2972546687(__this, p0, method) ((  Task_1_t1149249240 * (*) (Task_1_t1149249240 *, Func_2_t3938612258 *, const MethodInfo*))Task_1_ContinueWith_TisIl2CppObject_m524392371_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Database.Unity.PreserveAttribute::.ctor()
extern "C"  void PreserveAttribute__ctor_m94944701 (PreserveAttribute_t2023989938 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential::.ctor(Firebase.Database.Unity.ServiceAccountCredential/Initializer)
extern const MethodInfo* List_1_ToArray_m948919263_MethodInfo_var;
extern const uint32_t ServiceAccountCredential__ctor_m1185135217_MetadataUsageId;
extern "C"  void ServiceAccountCredential__ctor_m1185135217 (ServiceAccountCredential_t3398163339 * __this, Initializer_t1955556897 * ___initializer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServiceAccountCredential__ctor_m1185135217_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Initializer_t1955556897 * L_0 = ___initializer0;
		ServiceCredential__ctor_m3146366905(__this, L_0, /*hidden argument*/NULL);
		Initializer_t1955556897 * L_1 = ___initializer0;
		NullCheck(L_1);
		String_t* L_2 = Initializer_get_Id_m701527080(L_1, /*hidden argument*/NULL);
		ServiceAccountCredential_set_Id_m518344680(__this, L_2, /*hidden argument*/NULL);
		Initializer_t1955556897 * L_3 = ___initializer0;
		NullCheck(L_3);
		String_t* L_4 = Initializer_get_User_m122414950(L_3, /*hidden argument*/NULL);
		ServiceAccountCredential_set_User_m222781194(__this, L_4, /*hidden argument*/NULL);
		Initializer_t1955556897 * L_5 = ___initializer0;
		NullCheck(L_5);
		List_1_t1398341365 * L_6 = Initializer_get_Scopes_m3715438940(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		StringU5BU5D_t1642385972* L_7 = List_1_ToArray_m948919263(L_6, /*hidden argument*/List_1_ToArray_m948919263_MethodInfo_var);
		ServiceAccountCredential_set_Scopes_m2209060156(__this, L_7, /*hidden argument*/NULL);
		Initializer_t1955556897 * L_8 = ___initializer0;
		NullCheck(L_8);
		RSACryptoServiceProvider_t4229286967 * L_9 = Initializer_get_Key_m3005707888(L_8, /*hidden argument*/NULL);
		ServiceAccountCredential_set_Key_m825919564(__this, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.String Firebase.Database.Unity.ServiceAccountCredential::get_Id()
extern "C"  String_t* ServiceAccountCredential_get_Id_m2088493583 (ServiceAccountCredential_t3398163339 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential::set_Id(System.String)
extern "C"  void ServiceAccountCredential_set_Id_m518344680 (ServiceAccountCredential_t3398163339 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String Firebase.Database.Unity.ServiceAccountCredential::get_User()
extern "C"  String_t* ServiceAccountCredential_get_User_m3232432303 (ServiceAccountCredential_t3398163339 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CUserU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential::set_User(System.String)
extern "C"  void ServiceAccountCredential_set_User_m222781194 (ServiceAccountCredential_t3398163339 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUserU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.String[] Firebase.Database.Unity.ServiceAccountCredential::get_Scopes()
extern "C"  StringU5BU5D_t1642385972* ServiceAccountCredential_get_Scopes_m2586408377 (ServiceAccountCredential_t3398163339 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = __this->get_U3CScopesU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential::set_Scopes(System.String[])
extern "C"  void ServiceAccountCredential_set_Scopes_m2209060156 (ServiceAccountCredential_t3398163339 * __this, StringU5BU5D_t1642385972* ___value0, const MethodInfo* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = ___value0;
		__this->set_U3CScopesU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Security.Cryptography.RSACryptoServiceProvider Firebase.Database.Unity.ServiceAccountCredential::get_Key()
extern "C"  RSACryptoServiceProvider_t4229286967 * ServiceAccountCredential_get_Key_m4018384159 (ServiceAccountCredential_t3398163339 * __this, const MethodInfo* method)
{
	{
		RSACryptoServiceProvider_t4229286967 * L_0 = __this->get_U3CKeyU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential::set_Key(System.Security.Cryptography.RSACryptoServiceProvider)
extern "C"  void ServiceAccountCredential_set_Key_m825919564 (ServiceAccountCredential_t3398163339 * __this, RSACryptoServiceProvider_t4229286967 * ___value0, const MethodInfo* method)
{
	{
		RSACryptoServiceProvider_t4229286967 * L_0 = ___value0;
		__this->set_U3CKeyU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Collections.IEnumerator Firebase.Database.Unity.ServiceAccountCredential::SendOAuth(Firebase.Database.Unity.ServiceAccountCredential/OAuthRequest)
extern Il2CppClass* U3CSendOAuthU3Ec__Iterator0_t1559836771_il2cpp_TypeInfo_var;
extern const uint32_t ServiceAccountCredential_SendOAuth_m4019051956_MetadataUsageId;
extern "C"  Il2CppObject * ServiceAccountCredential_SendOAuth_m4019051956 (ServiceAccountCredential_t3398163339 * __this, OAuthRequest_t3497136277 * ___request0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServiceAccountCredential_SendOAuth_m4019051956_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSendOAuthU3Ec__Iterator0_t1559836771 * V_0 = NULL;
	{
		U3CSendOAuthU3Ec__Iterator0_t1559836771 * L_0 = (U3CSendOAuthU3Ec__Iterator0_t1559836771 *)il2cpp_codegen_object_new(U3CSendOAuthU3Ec__Iterator0_t1559836771_il2cpp_TypeInfo_var);
		U3CSendOAuthU3Ec__Iterator0__ctor_m1996092573(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSendOAuthU3Ec__Iterator0_t1559836771 * L_1 = V_0;
		OAuthRequest_t3497136277 * L_2 = ___request0;
		NullCheck(L_1);
		L_1->set_request_2(L_2);
		U3CSendOAuthU3Ec__Iterator0_t1559836771 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_3(__this);
		U3CSendOAuthU3Ec__Iterator0_t1559836771 * L_4 = V_0;
		return L_4;
	}
}
// System.String Firebase.Database.Unity.ServiceAccountCredential::GetAccessTokenForRequestSync(System.Threading.CancellationToken)
extern Il2CppClass* U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_t3624071070_il2cpp_TypeInfo_var;
extern Il2CppClass* OAuthRequest_t3497136277_il2cpp_TypeInfo_var;
extern Il2CppClass* UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_1_t3420419431_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2603311978_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_U3CU3Em__0_m991289755_MethodInfo_var;
extern const MethodInfo* Func_1__ctor_m3632084784_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3439604294;
extern const uint32_t ServiceAccountCredential_GetAccessTokenForRequestSync_m3174721268_MetadataUsageId;
extern "C"  String_t* ServiceAccountCredential_GetAccessTokenForRequestSync_m3174721268 (ServiceAccountCredential_t3398163339 * __this, CancellationToken_t1851405782  ___taskCancellationToken0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServiceAccountCredential_GetAccessTokenForRequestSync_m3174721268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_t3624071070 * V_0 = NULL;
	String_t* V_1 = NULL;
	OAuthRequest_t3497136277 * V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	String_t* G_B7_0 = NULL;
	{
		U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_t3624071070 * L_0 = (U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_t3624071070 *)il2cpp_codegen_object_new(U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_t3624071070_il2cpp_TypeInfo_var);
		U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2__ctor_m3845533516(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_t3624071070 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		String_t* L_2 = ServiceAccountCredential_CreatePayload_m2766755585(__this, /*hidden argument*/NULL);
		String_t* L_3 = ServiceAccountCredential_CreateAssertionFromPayload_m2311819027(__this, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_t3624071070 * L_4 = V_0;
		OAuthRequest_t3497136277 * L_5 = (OAuthRequest_t3497136277 *)il2cpp_codegen_object_new(OAuthRequest_t3497136277_il2cpp_TypeInfo_var);
		OAuthRequest__ctor_m398011043(L_5, /*hidden argument*/NULL);
		V_2 = L_5;
		OAuthRequest_t3497136277 * L_6 = V_2;
		String_t* L_7 = V_1;
		NullCheck(L_6);
		L_6->set_Assertion_0(L_7);
		OAuthRequest_t3497136277 * L_8 = V_2;
		NullCheck(L_4);
		L_4->set_request_0(L_8);
		IL2CPP_RUNTIME_CLASS_INIT(UnitySynchronizationContext_t1051733884_il2cpp_TypeInfo_var);
		UnitySynchronizationContext_t1051733884 * L_9 = UnitySynchronizationContext_get_Instance_m3676127900(NULL /*static, unused*/, /*hidden argument*/NULL);
		U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_t3624071070 * L_10 = V_0;
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_U3CU3Em__0_m991289755_MethodInfo_var);
		Func_1_t3420419431 * L_12 = (Func_1_t3420419431 *)il2cpp_codegen_object_new(Func_1_t3420419431_il2cpp_TypeInfo_var);
		Func_1__ctor_m3632084784(L_12, L_10, L_11, /*hidden argument*/Func_1__ctor_m3632084784_MethodInfo_var);
		NullCheck(L_9);
		UnitySynchronizationContext_SendCoroutine_m3987642997(L_9, L_12, /*hidden argument*/NULL);
		U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_t3624071070 * L_13 = V_0;
		NullCheck(L_13);
		OAuthRequest_t3497136277 * L_14 = L_13->get_request_0();
		NullCheck(L_14);
		String_t* L_15 = L_14->get_ResponseBody_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_16 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_005b;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_005b:
	{
		U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_t3624071070 * L_17 = V_0;
		NullCheck(L_17);
		OAuthRequest_t3497136277 * L_18 = L_17->get_request_0();
		NullCheck(L_18);
		String_t* L_19 = L_18->get_ResponseBody_1();
		Il2CppObject * L_20 = Json_Deserialize_m1368408977(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		V_3 = ((Il2CppObject*)Castclass(L_20, IDictionary_2_t2603311978_il2cpp_TypeInfo_var));
		V_4 = NULL;
		Il2CppObject* L_21 = V_3;
		if (!L_21)
		{
			goto IL_0088;
		}
	}
	{
		Il2CppObject* L_22 = V_3;
		NullCheck(L_22);
		InterfaceFuncInvoker2< bool, String_t*, Il2CppObject ** >::Invoke(2 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::TryGetValue(!0,!1&) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_22, _stringLiteral3439604294, (&V_4));
	}

IL_0088:
	{
		Il2CppObject * L_23 = V_4;
		if (!L_23)
		{
			goto IL_009b;
		}
	}
	{
		Il2CppObject * L_24 = V_4;
		NullCheck(L_24);
		String_t* L_25 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_24);
		G_B7_0 = L_25;
		goto IL_009c;
	}

IL_009b:
	{
		G_B7_0 = ((String_t*)(NULL));
	}

IL_009c:
	{
		return G_B7_0;
	}
}
// System.String Firebase.Database.Unity.ServiceAccountCredential::CreateAssertionFromPayload(System.String)
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral55936074;
extern Il2CppCodeGenString* _stringLiteral372029316;
extern Il2CppCodeGenString* _stringLiteral3426013327;
extern const uint32_t ServiceAccountCredential_CreateAssertionFromPayload_m2311819027_MetadataUsageId;
extern "C"  String_t* ServiceAccountCredential_CreateAssertionFromPayload_m2311819027 (ServiceAccountCredential_t3398163339 * __this, String_t* ___serializedPayload0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServiceAccountCredential_CreateAssertionFromPayload_m2311819027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	StringBuilder_t1221177846 * V_1 = NULL;
	String_t* V_2 = NULL;
	{
		V_0 = _stringLiteral55936074;
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		StringBuilder_t1221177846 * L_1 = V_1;
		String_t* L_2 = V_0;
		String_t* L_3 = ServiceAccountCredential_UrlSafeBase64Encode_m3896127820(__this, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_t1221177846 * L_4 = StringBuilder_Append_m3636508479(L_1, L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		StringBuilder_t1221177846 * L_5 = StringBuilder_Append_m3636508479(L_4, _stringLiteral372029316, /*hidden argument*/NULL);
		String_t* L_6 = ___serializedPayload0;
		String_t* L_7 = ServiceAccountCredential_UrlSafeBase64Encode_m3896127820(__this, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		StringBuilder_Append_m3636508479(L_5, L_7, /*hidden argument*/NULL);
		RSACryptoServiceProvider_t4229286967 * L_8 = ServiceAccountCredential_get_Key_m4018384159(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_9 = Encoding_get_ASCII_m2727409419(NULL /*static, unused*/, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_10 = V_1;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		NullCheck(L_9);
		ByteU5BU5D_t3397334013* L_12 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_9, L_11);
		NullCheck(L_8);
		ByteU5BU5D_t3397334013* L_13 = RSACryptoServiceProvider_SignData_m1177968540(L_8, L_12, _stringLiteral3426013327, /*hidden argument*/NULL);
		String_t* L_14 = ServiceAccountCredential_UrlSafeBase64Encode_m97698283(__this, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		StringBuilder_t1221177846 * L_15 = V_1;
		NullCheck(L_15);
		StringBuilder_t1221177846 * L_16 = StringBuilder_Append_m3636508479(L_15, _stringLiteral372029316, /*hidden argument*/NULL);
		String_t* L_17 = V_2;
		NullCheck(L_16);
		StringBuilder_Append_m3636508479(L_16, L_17, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_18 = V_1;
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_18);
		return L_19;
	}
}
// System.String Firebase.Database.Unity.ServiceAccountCredential::CreatePayload()
extern Il2CppClass* IClock_t1761322331_il2cpp_TypeInfo_var;
extern Il2CppClass* ServiceAccountCredential_t3398163339_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2199215334_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m1358196493_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3115736749;
extern Il2CppCodeGenString* _stringLiteral1084221834;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern Il2CppCodeGenString* _stringLiteral2309167408;
extern Il2CppCodeGenString* _stringLiteral3260010625;
extern Il2CppCodeGenString* _stringLiteral1193422049;
extern Il2CppCodeGenString* _stringLiteral696029580;
extern const uint32_t ServiceAccountCredential_CreatePayload_m2766755585_MetadataUsageId;
extern "C"  String_t* ServiceAccountCredential_CreatePayload_m2766755585 (ServiceAccountCredential_t3398163339 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServiceAccountCredential_CreatePayload_m2766755585_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	TimeSpan_t3430258949  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Dictionary_2_t309261261 * V_2 = NULL;
	{
		Il2CppObject * L_0 = ServiceCredential_get_Clock_m4062955925(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		DateTime_t693205669  L_1 = InterfaceFuncInvoker0< DateTime_t693205669  >::Invoke(1 /* System.DateTime Firebase.Database.Unity.IClock::get_UtcNow() */, IClock_t1761322331_il2cpp_TypeInfo_var, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(ServiceAccountCredential_t3398163339_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_2 = ((ServiceAccountCredential_t3398163339_StaticFields*)ServiceAccountCredential_t3398163339_il2cpp_TypeInfo_var->static_fields)->get_UnixEpoch_6();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		TimeSpan_t3430258949  L_3 = DateTime_op_Subtraction_m3246456251(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		double L_4 = TimeSpan_get_TotalSeconds_m1295026915((&V_1), /*hidden argument*/NULL);
		V_0 = (((int32_t)((int32_t)L_4)));
		Dictionary_2_t309261261 * L_5 = (Dictionary_2_t309261261 *)il2cpp_codegen_object_new(Dictionary_2_t309261261_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2199215334(L_5, /*hidden argument*/Dictionary_2__ctor_m2199215334_MethodInfo_var);
		V_2 = L_5;
		Dictionary_2_t309261261 * L_6 = V_2;
		String_t* L_7 = ServiceAccountCredential_get_Id_m2088493583(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Dictionary_2_set_Item_m1358196493(L_6, _stringLiteral3115736749, L_7, /*hidden argument*/Dictionary_2_set_Item_m1358196493_MethodInfo_var);
		Dictionary_2_t309261261 * L_8 = V_2;
		StringU5BU5D_t1642385972* L_9 = ServiceAccountCredential_get_Scopes_m2586408377(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Join_m1966872927(NULL /*static, unused*/, _stringLiteral372029310, L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		Dictionary_2_set_Item_m1358196493(L_8, _stringLiteral1084221834, L_10, /*hidden argument*/Dictionary_2_set_Item_m1358196493_MethodInfo_var);
		Dictionary_2_t309261261 * L_11 = V_2;
		NullCheck(L_11);
		Dictionary_2_set_Item_m1358196493(L_11, _stringLiteral2309167408, _stringLiteral3260010625, /*hidden argument*/Dictionary_2_set_Item_m1358196493_MethodInfo_var);
		Dictionary_2_t309261261 * L_12 = V_2;
		int32_t L_13 = V_0;
		int32_t L_14 = ((int32_t)((int32_t)L_13+(int32_t)((int32_t)3600)));
		Il2CppObject * L_15 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		Dictionary_2_set_Item_m1358196493(L_12, _stringLiteral1193422049, L_15, /*hidden argument*/Dictionary_2_set_Item_m1358196493_MethodInfo_var);
		Dictionary_2_t309261261 * L_16 = V_2;
		int32_t L_17 = V_0;
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		Dictionary_2_set_Item_m1358196493(L_16, _stringLiteral696029580, L_19, /*hidden argument*/Dictionary_2_set_Item_m1358196493_MethodInfo_var);
		Dictionary_2_t309261261 * L_20 = V_2;
		String_t* L_21 = Json_Serialize_m172045766(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.String Firebase.Database.Unity.ServiceAccountCredential::UrlSafeBase64Encode(System.String)
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern const uint32_t ServiceAccountCredential_UrlSafeBase64Encode_m3896127820_MetadataUsageId;
extern "C"  String_t* ServiceAccountCredential_UrlSafeBase64Encode_m3896127820 (ServiceAccountCredential_t3398163339 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServiceAccountCredential_UrlSafeBase64Encode_m3896127820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_0 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___value0;
		NullCheck(L_0);
		ByteU5BU5D_t3397334013* L_2 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		String_t* L_3 = ServiceAccountCredential_UrlSafeBase64Encode_m97698283(__this, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String Firebase.Database.Unity.ServiceAccountCredential::UrlSafeBase64Encode(System.Byte[])
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029329;
extern const uint32_t ServiceAccountCredential_UrlSafeBase64Encode_m97698283_MetadataUsageId;
extern "C"  String_t* ServiceAccountCredential_UrlSafeBase64Encode_m97698283 (ServiceAccountCredential_t3398163339 * __this, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServiceAccountCredential_UrlSafeBase64Encode_m97698283_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___bytes0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_1 = Convert_ToBase64String_m1936815455(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_1);
		String_t* L_3 = String_Replace_m1941156251(L_1, _stringLiteral372029329, L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = String_Replace_m534438427(L_3, ((int32_t)43), ((int32_t)45), /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = String_Replace_m534438427(L_4, ((int32_t)47), ((int32_t)95), /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential::.cctor()
extern Il2CppClass* ServiceAccountCredential_t3398163339_il2cpp_TypeInfo_var;
extern const uint32_t ServiceAccountCredential__cctor_m1374164505_MetadataUsageId;
extern "C"  void ServiceAccountCredential__cctor_m1374164505 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServiceAccountCredential__cctor_m1374164505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DateTime_t693205669  L_0;
		memset(&L_0, 0, sizeof(L_0));
		DateTime__ctor_m3270618252(&L_0, ((int32_t)1970), 1, 1, 0, 0, 0, 1, /*hidden argument*/NULL);
		((ServiceAccountCredential_t3398163339_StaticFields*)ServiceAccountCredential_t3398163339_il2cpp_TypeInfo_var->static_fields)->set_UnixEpoch_6(L_0);
		return;
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential/<GetAccessTokenForRequestSync>c__AnonStorey2::.ctor()
extern "C"  void U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2__ctor_m3845533516 (U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_t3624071070 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Firebase.Database.Unity.ServiceAccountCredential/<GetAccessTokenForRequestSync>c__AnonStorey2::<>m__0()
extern "C"  Il2CppObject * U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_U3CU3Em__0_m991289755 (U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_t3624071070 * __this, const MethodInfo* method)
{
	{
		ServiceAccountCredential_t3398163339 * L_0 = __this->get_U24this_1();
		OAuthRequest_t3497136277 * L_1 = __this->get_request_0();
		NullCheck(L_0);
		Il2CppObject * L_2 = ServiceAccountCredential_SendOAuth_m4019051956(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::.ctor()
extern "C"  void U3CSendOAuthU3Ec__Iterator0__ctor_m1996092573 (U3CSendOAuthU3Ec__Iterator0_t1559836771 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::MoveNext()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseHandler_t2907300047_il2cpp_TypeInfo_var;
extern Il2CppClass* WebClient_t1432723993_il2cpp_TypeInfo_var;
extern Il2CppClass* U3CSendOAuthU3Ec__AnonStorey1_t520895791_il2cpp_TypeInfo_var;
extern Il2CppClass* NameValueCollection_t3047564564_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadCompleted_t2145858625_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadValuesCompletedEventHandler_t563858374_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CSendOAuthU3Ec__AnonStorey1_U3CU3Em__0_m3378572595_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral715233275;
extern Il2CppCodeGenString* _stringLiteral3783514101;
extern Il2CppCodeGenString* _stringLiteral2816545808;
extern Il2CppCodeGenString* _stringLiteral3825833098;
extern Il2CppCodeGenString* _stringLiteral782856060;
extern Il2CppCodeGenString* _stringLiteral107080876;
extern Il2CppCodeGenString* _stringLiteral1905454349;
extern Il2CppCodeGenString* _stringLiteral868258363;
extern const uint32_t U3CSendOAuthU3Ec__Iterator0_MoveNext_m4174874303_MetadataUsageId;
extern "C"  bool U3CSendOAuthU3Ec__Iterator0_MoveNext_m4174874303 (U3CSendOAuthU3Ec__Iterator0_t1559836771 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSendOAuthU3Ec__Iterator0_MoveNext_m4174874303_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_004c;
		}
	}
	{
		goto IL_0234;
	}

IL_0023:
	{
		ServiceAccountCredential_t3398163339 * L_2 = __this->get_U24this_3();
		NullCheck(L_2);
		String_t* L_3 = ServiceCredential_get_TokenServerUrl_m591785886(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral715233275, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
		FirebaseHandler_LogMessage_m1734897910(NULL /*static, unused*/, 1, L_4, /*hidden argument*/NULL);
		WebClient_t1432723993 * L_5 = (WebClient_t1432723993 *)il2cpp_codegen_object_new(WebClient_t1432723993_il2cpp_TypeInfo_var);
		WebClient__ctor_m660733025(L_5, /*hidden argument*/NULL);
		__this->set_U3CclientU3E__0_0(L_5);
		V_0 = ((int32_t)-3);
	}

IL_004c:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_6 = V_0;
			if (((int32_t)((int32_t)L_6-(int32_t)1)) == 0)
			{
				goto IL_011e;
			}
		}

IL_0058:
		{
			U3CSendOAuthU3Ec__AnonStorey1_t520895791 * L_7 = (U3CSendOAuthU3Ec__AnonStorey1_t520895791 *)il2cpp_codegen_object_new(U3CSendOAuthU3Ec__AnonStorey1_t520895791_il2cpp_TypeInfo_var);
			U3CSendOAuthU3Ec__AnonStorey1__ctor_m1929189873(L_7, /*hidden argument*/NULL);
			__this->set_U24locvar0_7(L_7);
			U3CSendOAuthU3Ec__AnonStorey1_t520895791 * L_8 = __this->get_U24locvar0_7();
			NullCheck(L_8);
			L_8->set_U3CU3Ef__refU240_1(__this);
			NameValueCollection_t3047564564 * L_9 = (NameValueCollection_t3047564564 *)il2cpp_codegen_object_new(NameValueCollection_t3047564564_il2cpp_TypeInfo_var);
			NameValueCollection__ctor_m1767369537(L_9, /*hidden argument*/NULL);
			__this->set_U3CreqparmU3E__1_1(L_9);
			NameValueCollection_t3047564564 * L_10 = __this->get_U3CreqparmU3E__1_1();
			NullCheck(L_10);
			VirtActionInvoker2< String_t*, String_t* >::Invoke(16 /* System.Void System.Collections.Specialized.NameValueCollection::Add(System.String,System.String) */, L_10, _stringLiteral3783514101, _stringLiteral2816545808);
			NameValueCollection_t3047564564 * L_11 = __this->get_U3CreqparmU3E__1_1();
			OAuthRequest_t3497136277 * L_12 = __this->get_request_2();
			NullCheck(L_12);
			String_t* L_13 = L_12->get_Assertion_0();
			NullCheck(L_11);
			VirtActionInvoker2< String_t*, String_t* >::Invoke(16 /* System.Void System.Collections.Specialized.NameValueCollection::Add(System.String,System.String) */, L_11, _stringLiteral3825833098, L_13);
			U3CSendOAuthU3Ec__AnonStorey1_t520895791 * L_14 = __this->get_U24locvar0_7();
			UploadCompleted_t2145858625 * L_15 = (UploadCompleted_t2145858625 *)il2cpp_codegen_object_new(UploadCompleted_t2145858625_il2cpp_TypeInfo_var);
			UploadCompleted__ctor_m182388035(L_15, /*hidden argument*/NULL);
			NullCheck(L_14);
			L_14->set_uploadCompleted_0(L_15);
			WebClient_t1432723993 * L_16 = __this->get_U3CclientU3E__0_0();
			U3CSendOAuthU3Ec__AnonStorey1_t520895791 * L_17 = __this->get_U24locvar0_7();
			IntPtr_t L_18;
			L_18.set_m_value_0((void*)(void*)U3CSendOAuthU3Ec__AnonStorey1_U3CU3Em__0_m3378572595_MethodInfo_var);
			UploadValuesCompletedEventHandler_t563858374 * L_19 = (UploadValuesCompletedEventHandler_t563858374 *)il2cpp_codegen_object_new(UploadValuesCompletedEventHandler_t563858374_il2cpp_TypeInfo_var);
			UploadValuesCompletedEventHandler__ctor_m143504748(L_19, L_17, L_18, /*hidden argument*/NULL);
			NullCheck(L_16);
			WebClient_add_UploadValuesCompleted_m3981342317(L_16, L_19, /*hidden argument*/NULL);
			WebClient_t1432723993 * L_20 = __this->get_U3CclientU3E__0_0();
			ServiceAccountCredential_t3398163339 * L_21 = __this->get_U24this_3();
			NullCheck(L_21);
			String_t* L_22 = ServiceCredential_get_TokenServerUrl_m591785886(L_21, /*hidden argument*/NULL);
			Uri_t19570940 * L_23 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
			Uri__ctor_m3927533881(L_23, L_22, /*hidden argument*/NULL);
			NameValueCollection_t3047564564 * L_24 = __this->get_U3CreqparmU3E__1_1();
			NullCheck(L_20);
			WebClient_UploadValuesAsync_m1764200028(L_20, L_23, _stringLiteral782856060, L_24, /*hidden argument*/NULL);
			goto IL_011e;
		}

IL_0101:
		{
			__this->set_U24current_4(NULL);
			bool L_25 = __this->get_U24disposing_5();
			if (L_25)
			{
				goto IL_0117;
			}
		}

IL_0110:
		{
			__this->set_U24PC_6(1);
		}

IL_0117:
		{
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x236, FINALLY_0222);
		}

IL_011e:
		{
			U3CSendOAuthU3Ec__AnonStorey1_t520895791 * L_26 = __this->get_U24locvar0_7();
			NullCheck(L_26);
			UploadCompleted_t2145858625 * L_27 = L_26->get_uploadCompleted_0();
			NullCheck(L_27);
			UploadValuesCompletedEventArgs_t3564452537 * L_28 = L_27->get_args_0();
			if (!L_28)
			{
				goto IL_0101;
			}
		}

IL_0133:
		{
			U3CSendOAuthU3Ec__AnonStorey1_t520895791 * L_29 = __this->get_U24locvar0_7();
			NullCheck(L_29);
			UploadCompleted_t2145858625 * L_30 = L_29->get_uploadCompleted_0();
			NullCheck(L_30);
			UploadValuesCompletedEventArgs_t3564452537 * L_31 = L_30->get_args_0();
			NullCheck(L_31);
			bool L_32 = AsyncCompletedEventArgs_get_Cancelled_m1739331837(L_31, /*hidden argument*/NULL);
			if (!L_32)
			{
				goto IL_0179;
			}
		}

IL_014d:
		{
			ServiceAccountCredential_t3398163339 * L_33 = __this->get_U24this_3();
			NullCheck(L_33);
			String_t* L_34 = ServiceCredential_get_TokenServerUrl_m591785886(L_33, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_35 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral107080876, L_34, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
			FirebaseHandler_LogMessage_m1734897910(NULL /*static, unused*/, 1, L_35, /*hidden argument*/NULL);
			OAuthRequest_t3497136277 * L_36 = __this->get_request_2();
			NullCheck(L_36);
			L_36->set_ResponseBody_1((String_t*)NULL);
			goto IL_021d;
		}

IL_0179:
		{
			U3CSendOAuthU3Ec__AnonStorey1_t520895791 * L_37 = __this->get_U24locvar0_7();
			NullCheck(L_37);
			UploadCompleted_t2145858625 * L_38 = L_37->get_uploadCompleted_0();
			NullCheck(L_38);
			UploadValuesCompletedEventArgs_t3564452537 * L_39 = L_38->get_args_0();
			NullCheck(L_39);
			Exception_t1927440687 * L_40 = AsyncCompletedEventArgs_get_Error_m3178619961(L_39, /*hidden argument*/NULL);
			if (!L_40)
			{
				goto IL_01cd;
			}
		}

IL_0193:
		{
			ServiceAccountCredential_t3398163339 * L_41 = __this->get_U24this_3();
			NullCheck(L_41);
			String_t* L_42 = ServiceCredential_get_TokenServerUrl_m591785886(L_41, /*hidden argument*/NULL);
			U3CSendOAuthU3Ec__AnonStorey1_t520895791 * L_43 = __this->get_U24locvar0_7();
			NullCheck(L_43);
			UploadCompleted_t2145858625 * L_44 = L_43->get_uploadCompleted_0();
			NullCheck(L_44);
			UploadValuesCompletedEventArgs_t3564452537 * L_45 = L_44->get_args_0();
			NullCheck(L_45);
			Exception_t1927440687 * L_46 = AsyncCompletedEventArgs_get_Error_m3178619961(L_45, /*hidden argument*/NULL);
			NullCheck(L_46);
			String_t* L_47 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_46);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_48 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral1905454349, L_42, L_47, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
			FirebaseHandler_LogMessage_m1734897910(NULL /*static, unused*/, 4, L_48, /*hidden argument*/NULL);
			goto IL_021d;
		}

IL_01cd:
		{
			OAuthRequest_t3497136277 * L_49 = __this->get_request_2();
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_50 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			U3CSendOAuthU3Ec__AnonStorey1_t520895791 * L_51 = __this->get_U24locvar0_7();
			NullCheck(L_51);
			UploadCompleted_t2145858625 * L_52 = L_51->get_uploadCompleted_0();
			NullCheck(L_52);
			UploadValuesCompletedEventArgs_t3564452537 * L_53 = L_52->get_args_0();
			NullCheck(L_53);
			ByteU5BU5D_t3397334013* L_54 = UploadValuesCompletedEventArgs_get_Result_m3852338329(L_53, /*hidden argument*/NULL);
			NullCheck(L_50);
			String_t* L_55 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_50, L_54);
			NullCheck(L_49);
			L_49->set_ResponseBody_1(L_55);
			ServiceAccountCredential_t3398163339 * L_56 = __this->get_U24this_3();
			NullCheck(L_56);
			String_t* L_57 = ServiceCredential_get_TokenServerUrl_m591785886(L_56, /*hidden argument*/NULL);
			OAuthRequest_t3497136277 * L_58 = __this->get_request_2();
			NullCheck(L_58);
			String_t* L_59 = L_58->get_ResponseBody_1();
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_60 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral868258363, L_57, L_59, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseHandler_t2907300047_il2cpp_TypeInfo_var);
			FirebaseHandler_LogMessage_m1734897910(NULL /*static, unused*/, 1, L_60, /*hidden argument*/NULL);
		}

IL_021d:
		{
			IL2CPP_LEAVE(0x22D, FINALLY_0222);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0222;
	}

FINALLY_0222:
	{ // begin finally (depth: 1)
		{
			bool L_61 = V_1;
			if (!L_61)
			{
				goto IL_0226;
			}
		}

IL_0225:
		{
			IL2CPP_END_FINALLY(546)
		}

IL_0226:
		{
			U3CSendOAuthU3Ec__Iterator0_U3CU3E__Finally0_m788646374(__this, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(546)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(546)
	{
		IL2CPP_JUMP_TBL(0x236, IL_0236)
		IL2CPP_JUMP_TBL(0x22D, IL_022d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_022d:
	{
		__this->set_U24PC_6((-1));
	}

IL_0234:
	{
		return (bool)0;
	}

IL_0236:
	{
		return (bool)1;
	}
}
// System.Object Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendOAuthU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2896288715 (U3CSendOAuthU3Ec__Iterator0_t1559836771 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendOAuthU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1171917123 (U3CSendOAuthU3Ec__Iterator0_t1559836771 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::Dispose()
extern "C"  void U3CSendOAuthU3Ec__Iterator0_Dispose_m1858233370 (U3CSendOAuthU3Ec__Iterator0_t1559836771 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0034;
		}
		if (L_1 == 1)
		{
			goto IL_0028;
		}
	}
	{
		goto IL_0034;
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x34, FINALLY_002d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002d;
	}

FINALLY_002d:
	{ // begin finally (depth: 1)
		U3CSendOAuthU3Ec__Iterator0_U3CU3E__Finally0_m788646374(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(45)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(45)
	{
		IL2CPP_JUMP_TBL(0x34, IL_0034)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0034:
	{
		return;
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CSendOAuthU3Ec__Iterator0_Reset_m1165723380_MetadataUsageId;
extern "C"  void U3CSendOAuthU3Ec__Iterator0_Reset_m1165723380 (U3CSendOAuthU3Ec__Iterator0_t1559836771 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSendOAuthU3Ec__Iterator0_Reset_m1165723380_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::<>__Finally0()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CSendOAuthU3Ec__Iterator0_U3CU3E__Finally0_m788646374_MetadataUsageId;
extern "C"  void U3CSendOAuthU3Ec__Iterator0_U3CU3E__Finally0_m788646374 (U3CSendOAuthU3Ec__Iterator0_t1559836771 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSendOAuthU3Ec__Iterator0_U3CU3E__Finally0_m788646374_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WebClient_t1432723993 * L_0 = __this->get_U3CclientU3E__0_0();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		WebClient_t1432723993 * L_1 = __this->get_U3CclientU3E__0_0();
		NullCheck(L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0/<SendOAuth>c__AnonStorey1::.ctor()
extern "C"  void U3CSendOAuthU3Ec__AnonStorey1__ctor_m1929189873 (U3CSendOAuthU3Ec__AnonStorey1_t520895791 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0/<SendOAuth>c__AnonStorey1::<>m__0(System.Object,System.Net.UploadValuesCompletedEventArgs)
extern "C"  void U3CSendOAuthU3Ec__AnonStorey1_U3CU3Em__0_m3378572595 (U3CSendOAuthU3Ec__AnonStorey1_t520895791 * __this, Il2CppObject * ___sender0, UploadValuesCompletedEventArgs_t3564452537 * ___e1, const MethodInfo* method)
{
	{
		UploadCompleted_t2145858625 * L_0 = __this->get_uploadCompleted_0();
		UploadValuesCompletedEventArgs_t3564452537 * L_1 = ___e1;
		NullCheck(L_0);
		L_0->set_args_0(L_1);
		return;
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential/Initializer::.ctor(System.String)
extern Il2CppCodeGenString* _stringLiteral3260010625;
extern const uint32_t Initializer__ctor_m1336621065_MetadataUsageId;
extern "C"  void Initializer__ctor_m1336621065 (Initializer_t1955556897 * __this, String_t* ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Initializer__ctor_m1336621065_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___id0;
		Initializer__ctor_m3585247251(__this, L_0, _stringLiteral3260010625, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential/Initializer::.ctor(System.String,System.String)
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const uint32_t Initializer__ctor_m3585247251_MetadataUsageId;
extern "C"  void Initializer__ctor_m3585247251 (Initializer_t1955556897 * __this, String_t* ___id0, String_t* ___tokenServerUrl1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Initializer__ctor_m3585247251_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___tokenServerUrl1;
		Initializer__ctor_m2870913516(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___id0;
		Initializer_set_Id_m1034028241(__this, L_1, /*hidden argument*/NULL);
		List_1_t1398341365 * L_2 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_2, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		Initializer_set_Scopes_m2350759887(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.String Firebase.Database.Unity.ServiceAccountCredential/Initializer::get_Id()
extern "C"  String_t* Initializer_get_Id_m701527080 (Initializer_t1955556897 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential/Initializer::set_Id(System.String)
extern "C"  void Initializer_set_Id_m1034028241 (Initializer_t1955556897 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CIdU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String Firebase.Database.Unity.ServiceAccountCredential/Initializer::get_User()
extern "C"  String_t* Initializer_get_User_m122414950 (Initializer_t1955556897 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CUserU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential/Initializer::set_User(System.String)
extern "C"  void Initializer_set_User_m2701204433 (Initializer_t1955556897 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUserU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> Firebase.Database.Unity.ServiceAccountCredential/Initializer::get_Scopes()
extern "C"  List_1_t1398341365 * Initializer_get_Scopes_m3715438940 (Initializer_t1955556897 * __this, const MethodInfo* method)
{
	{
		List_1_t1398341365 * L_0 = __this->get_U3CScopesU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential/Initializer::set_Scopes(System.Collections.Generic.List`1<System.String>)
extern "C"  void Initializer_set_Scopes_m2350759887 (Initializer_t1955556897 * __this, List_1_t1398341365 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1398341365 * L_0 = ___value0;
		__this->set_U3CScopesU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Security.Cryptography.RSACryptoServiceProvider Firebase.Database.Unity.ServiceAccountCredential/Initializer::get_Key()
extern "C"  RSACryptoServiceProvider_t4229286967 * Initializer_get_Key_m3005707888 (Initializer_t1955556897 * __this, const MethodInfo* method)
{
	{
		RSACryptoServiceProvider_t4229286967 * L_0 = __this->get_U3CKeyU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential/Initializer::set_Key(System.Security.Cryptography.RSACryptoServiceProvider)
extern "C"  void Initializer_set_Key_m2410272885 (Initializer_t1955556897 * __this, RSACryptoServiceProvider_t4229286967 * ___value0, const MethodInfo* method)
{
	{
		RSACryptoServiceProvider_t4229286967 * L_0 = ___value0;
		__this->set_U3CKeyU3Ek__BackingField_5(L_0);
		return;
	}
}
// Firebase.Database.Unity.ServiceAccountCredential/Initializer Firebase.Database.Unity.ServiceAccountCredential/Initializer::FromCertificate(System.Security.Cryptography.X509Certificates.X509Certificate2)
extern Il2CppClass* RSACryptoServiceProvider_t4229286967_il2cpp_TypeInfo_var;
extern const uint32_t Initializer_FromCertificate_m3970222400_MetadataUsageId;
extern "C"  Initializer_t1955556897 * Initializer_FromCertificate_m3970222400 (Initializer_t1955556897 * __this, X509Certificate2_t4056456767 * ___certificate0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Initializer_FromCertificate_m3970222400_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RSACryptoServiceProvider_t4229286967 * V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	{
		X509Certificate2_t4056456767 * L_0 = ___certificate0;
		NullCheck(L_0);
		AsymmetricAlgorithm_t784058677 * L_1 = X509Certificate2_get_PrivateKey_m2896208208(L_0, /*hidden argument*/NULL);
		V_0 = ((RSACryptoServiceProvider_t4229286967 *)CastclassSealed(L_1, RSACryptoServiceProvider_t4229286967_il2cpp_TypeInfo_var));
		RSACryptoServiceProvider_t4229286967 * L_2 = V_0;
		NullCheck(L_2);
		ByteU5BU5D_t3397334013* L_3 = RSACryptoServiceProvider_ExportCspBlob_m353047322(L_2, (bool)1, /*hidden argument*/NULL);
		V_1 = L_3;
		RSACryptoServiceProvider_t4229286967 * L_4 = (RSACryptoServiceProvider_t4229286967 *)il2cpp_codegen_object_new(RSACryptoServiceProvider_t4229286967_il2cpp_TypeInfo_var);
		RSACryptoServiceProvider__ctor_m1532797528(L_4, /*hidden argument*/NULL);
		Initializer_set_Key_m2410272885(__this, L_4, /*hidden argument*/NULL);
		RSACryptoServiceProvider_t4229286967 * L_5 = Initializer_get_Key_m3005707888(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_6 = V_1;
		NullCheck(L_5);
		RSACryptoServiceProvider_ImportCspBlob_m992237701(L_5, L_6, /*hidden argument*/NULL);
		return __this;
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential/OAuthRequest::.ctor()
extern "C"  void OAuthRequest__ctor_m398011043 (OAuthRequest_t3497136277 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Database.Unity.ServiceAccountCredential/UploadCompleted::.ctor()
extern "C"  void UploadCompleted__ctor_m182388035 (UploadCompleted_t2145858625 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Database.Unity.ServiceCredential::.ctor(Firebase.Database.Unity.ServiceCredential/Initializer)
extern "C"  void ServiceCredential__ctor_m3146366905 (ServiceCredential_t2286218796 * __this, Initializer_t2915825156 * ___initializer0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Initializer_t2915825156 * L_0 = ___initializer0;
		NullCheck(L_0);
		String_t* L_1 = Initializer_get_TokenServerUrl_m1876759603(L_0, /*hidden argument*/NULL);
		ServiceCredential_set_TokenServerUrl_m637697169(__this, L_1, /*hidden argument*/NULL);
		Initializer_t2915825156 * L_2 = ___initializer0;
		NullCheck(L_2);
		Il2CppObject * L_3 = Initializer_get_Clock_m3276295110(L_2, /*hidden argument*/NULL);
		ServiceCredential_set_Clock_m1636754526(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.String Firebase.Database.Unity.ServiceCredential::get_TokenServerUrl()
extern "C"  String_t* ServiceCredential_get_TokenServerUrl_m591785886 (ServiceCredential_t2286218796 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTokenServerUrlU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void Firebase.Database.Unity.ServiceCredential::set_TokenServerUrl(System.String)
extern "C"  void ServiceCredential_set_TokenServerUrl_m637697169 (ServiceCredential_t2286218796 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTokenServerUrlU3Ek__BackingField_0(L_0);
		return;
	}
}
// Firebase.Database.Unity.IClock Firebase.Database.Unity.ServiceCredential::get_Clock()
extern "C"  Il2CppObject * ServiceCredential_get_Clock_m4062955925 (ServiceCredential_t2286218796 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U3CClockU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void Firebase.Database.Unity.ServiceCredential::set_Clock(Firebase.Database.Unity.IClock)
extern "C"  void ServiceCredential_set_Clock_m1636754526 (ServiceCredential_t2286218796 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CClockU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String Firebase.Database.Unity.ServiceCredential::GetAccessTokenForRequest()
extern Il2CppClass* CancellationToken_t1851405782_il2cpp_TypeInfo_var;
extern const uint32_t ServiceCredential_GetAccessTokenForRequest_m57314549_MetadataUsageId;
extern "C"  String_t* ServiceCredential_GetAccessTokenForRequest_m57314549 (ServiceCredential_t2286218796 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ServiceCredential_GetAccessTokenForRequest_m57314549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CancellationToken_t1851405782  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (CancellationToken_t1851405782_il2cpp_TypeInfo_var, (&V_0));
		CancellationToken_t1851405782  L_0 = V_0;
		String_t* L_1 = VirtFuncInvoker1< String_t*, CancellationToken_t1851405782  >::Invoke(5 /* System.String Firebase.Database.Unity.ServiceCredential::GetAccessTokenForRequestSync(System.Threading.CancellationToken) */, __this, L_0);
		return L_1;
	}
}
// System.Void Firebase.Database.Unity.ServiceCredential/Initializer::.ctor(System.String)
extern Il2CppClass* SystemClock_t3351332359_il2cpp_TypeInfo_var;
extern const uint32_t Initializer__ctor_m2870913516_MetadataUsageId;
extern "C"  void Initializer__ctor_m2870913516 (Initializer_t2915825156 * __this, String_t* ___tokenServerUrl0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Initializer__ctor_m2870913516_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___tokenServerUrl0;
		Initializer_set_TokenServerUrl_m210258324(__this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t3351332359_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((SystemClock_t3351332359_StaticFields*)SystemClock_t3351332359_il2cpp_TypeInfo_var->static_fields)->get_Default_0();
		Initializer_set_Clock_m1598159275(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String Firebase.Database.Unity.ServiceCredential/Initializer::get_TokenServerUrl()
extern "C"  String_t* Initializer_get_TokenServerUrl_m1876759603 (Initializer_t2915825156 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CTokenServerUrlU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void Firebase.Database.Unity.ServiceCredential/Initializer::set_TokenServerUrl(System.String)
extern "C"  void Initializer_set_TokenServerUrl_m210258324 (Initializer_t2915825156 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CTokenServerUrlU3Ek__BackingField_0(L_0);
		return;
	}
}
// Firebase.Database.Unity.IClock Firebase.Database.Unity.ServiceCredential/Initializer::get_Clock()
extern "C"  Il2CppObject * Initializer_get_Clock_m3276295110 (Initializer_t2915825156 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U3CClockU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void Firebase.Database.Unity.ServiceCredential/Initializer::set_Clock(Firebase.Database.Unity.IClock)
extern "C"  void Initializer_set_Clock_m1598159275 (Initializer_t2915825156 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CClockU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void Firebase.Database.Unity.SystemClock::.ctor()
extern "C"  void SystemClock__ctor_m2043626562 (SystemClock_t3351332359 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.DateTime Firebase.Database.Unity.SystemClock::get_Now()
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t SystemClock_get_Now_m797233368_MetadataUsageId;
extern "C"  DateTime_t693205669  SystemClock_get_Now_m797233368 (SystemClock_t3351332359 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SystemClock_get_Now_m797233368_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_0 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.DateTime Firebase.Database.Unity.SystemClock::get_UtcNow()
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t SystemClock_get_UtcNow_m3188797936_MetadataUsageId;
extern "C"  DateTime_t693205669  SystemClock_get_UtcNow_m3188797936 (SystemClock_t3351332359 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SystemClock_get_UtcNow_m3188797936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_0 = DateTime_get_UtcNow_m1309841468(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Firebase.Database.Unity.SystemClock::.cctor()
extern Il2CppClass* SystemClock_t3351332359_il2cpp_TypeInfo_var;
extern const uint32_t SystemClock__cctor_m529698727_MetadataUsageId;
extern "C"  void SystemClock__cctor_m529698727 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SystemClock__cctor_m529698727_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SystemClock_t3351332359 * L_0 = (SystemClock_t3351332359 *)il2cpp_codegen_object_new(SystemClock_t3351332359_il2cpp_TypeInfo_var);
		SystemClock__ctor_m2043626562(L_0, /*hidden argument*/NULL);
		((SystemClock_t3351332359_StaticFields*)SystemClock_t3351332359_il2cpp_TypeInfo_var->static_fields)->set_Default_0(L_0);
		return;
	}
}
// System.Void Firebase.Database.Unity.TreeNodeIListTransactionData::.ctor()
extern const MethodInfo* TreeNode_1__ctor_m578994537_MethodInfo_var;
extern const uint32_t TreeNodeIListTransactionData__ctor_m3485790242_MetadataUsageId;
extern "C"  void TreeNodeIListTransactionData__ctor_m3485790242 (TreeNodeIListTransactionData_t390449921 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TreeNodeIListTransactionData__ctor_m3485790242_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TreeNode_1__ctor_m578994537(__this, /*hidden argument*/TreeNode_1__ctor_m578994537_MethodInfo_var);
		return;
	}
}
// System.Void Firebase.Database.Unity.UnityAuthTokenProvider::.ctor(Firebase.FirebaseApp)
extern Il2CppClass* FirebaseApp_t210707726_il2cpp_TypeInfo_var;
extern const uint32_t UnityAuthTokenProvider__ctor_m3982661543_MetadataUsageId;
extern "C"  void UnityAuthTokenProvider__ctor_m3982661543 (UnityAuthTokenProvider_t371495853 * __this, FirebaseApp_t210707726 * ___firebaseApp0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAuthTokenProvider__ctor_m3982661543_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AuthTokenProvider__ctor_m3484254436(__this, /*hidden argument*/NULL);
		FirebaseApp_t210707726 * L_0 = ___firebaseApp0;
		__this->set__firebaseApp_0(L_0);
		FirebaseApp_t210707726 * L_1 = __this->get__firebaseApp_0();
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t210707726_il2cpp_TypeInfo_var);
		FirebaseApp_t210707726 * L_2 = FirebaseApp_get_DefaultInstance_m465202029(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__firebaseApp_0(L_2);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Firebase.Database.Unity.UnityAuthTokenProvider::GetToken(System.Boolean,Firebase.Database.Core.AuthTokenProvider/IGetTokenCompletionListener)
extern Il2CppClass* U3CGetTokenU3Ec__AnonStorey0_t4052243837_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* X509Certificate2_t4056456767_il2cpp_TypeInfo_var;
extern Il2CppClass* Initializer_t1955556897_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* ServiceAccountCredential_t3398163339_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* IGetTokenCompletionListener_t2927602790_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern Il2CppClass* GAuthToken_t2345784186_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseAuth_t3105883899_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3938612258_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2052238296_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2199215334_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m1358196493_MethodInfo_var;
extern const MethodInfo* U3CGetTokenU3Ec__AnonStorey0_U3CU3Em__0_m1504785580_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m1446518598_MethodInfo_var;
extern const MethodInfo* Task_1_ContinueWith_TisString_t_m2972546687_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3076167538;
extern Il2CppCodeGenString* _stringLiteral1624823179;
extern Il2CppCodeGenString* _stringLiteral2667433418;
extern Il2CppCodeGenString* _stringLiteral812380654;
extern Il2CppCodeGenString* _stringLiteral339799344;
extern const uint32_t UnityAuthTokenProvider_GetToken_m3335724944_MetadataUsageId;
extern "C"  void UnityAuthTokenProvider_GetToken_m3335724944 (UnityAuthTokenProvider_t371495853 * __this, bool ___forceRefresh0, Il2CppObject * ___listener1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAuthTokenProvider_GetToken_m3335724944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetTokenU3Ec__AnonStorey0_t4052243837 * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	X509Certificate2_t4056456767 * V_4 = NULL;
	ServiceAccountCredential_t3398163339 * V_5 = NULL;
	Initializer_t1955556897 * V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	Dictionary_2_t309261261 * V_9 = NULL;
	GAuthToken_t2345784186 * V_10 = NULL;
	FirebaseAuth_t3105883899 * V_11 = NULL;
	FirebaseUser_t4046966602 * V_12 = NULL;
	{
		U3CGetTokenU3Ec__AnonStorey0_t4052243837 * L_0 = (U3CGetTokenU3Ec__AnonStorey0_t4052243837 *)il2cpp_codegen_object_new(U3CGetTokenU3Ec__AnonStorey0_t4052243837_il2cpp_TypeInfo_var);
		U3CGetTokenU3Ec__AnonStorey0__ctor_m1307976171(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetTokenU3Ec__AnonStorey0_t4052243837 * L_1 = V_0;
		Il2CppObject * L_2 = ___listener1;
		NullCheck(L_1);
		L_1->set_listener_0(L_2);
		FirebaseApp_t210707726 * L_3 = __this->get__firebaseApp_0();
		String_t* L_4 = FirebaseEditorExtensions_GetEditorP12FileName_m1066698760(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		FirebaseApp_t210707726 * L_5 = __this->get__firebaseApp_0();
		String_t* L_6 = FirebaseEditorExtensions_GetEditorP12Password_m3642378088(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		FirebaseApp_t210707726 * L_7 = __this->get__firebaseApp_0();
		String_t* L_8 = FirebaseEditorExtensions_GetEditorServiceAccountEmail_m2686895772(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_3 = L_8;
		String_t* L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_012e;
		}
	}
	{
		String_t* L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_012e;
		}
	}
	{
		String_t* L_13 = V_1;
		bool L_14 = File_Exists_m1685968367(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_012e;
		}
	}
	{
		String_t* L_15 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_16 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0063;
		}
	}
	{
		V_2 = _stringLiteral3076167538;
	}

IL_0063:
	{
		String_t* L_17 = V_1;
		String_t* L_18 = V_2;
		X509Certificate2_t4056456767 * L_19 = (X509Certificate2_t4056456767 *)il2cpp_codegen_object_new(X509Certificate2_t4056456767_il2cpp_TypeInfo_var);
		X509Certificate2__ctor_m1013923890(L_19, L_17, L_18, 4, /*hidden argument*/NULL);
		V_4 = L_19;
		String_t* L_20 = V_3;
		Initializer_t1955556897 * L_21 = (Initializer_t1955556897 *)il2cpp_codegen_object_new(Initializer_t1955556897_il2cpp_TypeInfo_var);
		Initializer__ctor_m1336621065(L_21, L_20, /*hidden argument*/NULL);
		V_6 = L_21;
		Initializer_t1955556897 * L_22 = V_6;
		StringU5BU5D_t1642385972* L_23 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral1624823179);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1624823179);
		StringU5BU5D_t1642385972* L_24 = L_23;
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, _stringLiteral2667433418);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2667433418);
		List_1_t1398341365 * L_25 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m2052238296(L_25, (Il2CppObject*)(Il2CppObject*)L_24, /*hidden argument*/List_1__ctor_m2052238296_MethodInfo_var);
		NullCheck(L_22);
		Initializer_set_Scopes_m2350759887(L_22, L_25, /*hidden argument*/NULL);
		Initializer_t1955556897 * L_26 = V_6;
		X509Certificate2_t4056456767 * L_27 = V_4;
		NullCheck(L_26);
		Initializer_t1955556897 * L_28 = Initializer_FromCertificate_m3970222400(L_26, L_27, /*hidden argument*/NULL);
		ServiceAccountCredential_t3398163339 * L_29 = (ServiceAccountCredential_t3398163339 *)il2cpp_codegen_object_new(ServiceAccountCredential_t3398163339_il2cpp_TypeInfo_var);
		ServiceAccountCredential__ctor_m1185135217(L_29, L_28, /*hidden argument*/NULL);
		V_5 = L_29;
		ServiceAccountCredential_t3398163339 * L_30 = V_5;
		NullCheck(L_30);
		String_t* L_31 = ServiceCredential_GetAccessTokenForRequest_m57314549(L_30, /*hidden argument*/NULL);
		V_7 = L_31;
		String_t* L_32 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_33 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_00db;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral812380654, /*hidden argument*/NULL);
		U3CGetTokenU3Ec__AnonStorey0_t4052243837 * L_34 = V_0;
		NullCheck(L_34);
		Il2CppObject * L_35 = L_34->get_listener_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_36 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_35);
		InterfaceActionInvoker1< String_t* >::Invoke(0 /* System.Void Firebase.Database.Core.AuthTokenProvider/IGetTokenCompletionListener::OnSuccess(System.String) */, IGetTokenCompletionListener_t2927602790_il2cpp_TypeInfo_var, L_35, L_36);
		goto IL_0129;
	}

IL_00db:
	{
		FirebaseApp_t210707726 * L_37 = __this->get__firebaseApp_0();
		String_t* L_38 = FirebaseEditorExtensions_GetEditorAuthUserId_m1692323114(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		V_8 = L_38;
		V_9 = (Dictionary_2_t309261261 *)NULL;
		String_t* L_39 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_40 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		if (L_40)
		{
			goto IL_010c;
		}
	}
	{
		Dictionary_2_t309261261 * L_41 = (Dictionary_2_t309261261 *)il2cpp_codegen_object_new(Dictionary_2_t309261261_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2199215334(L_41, /*hidden argument*/Dictionary_2__ctor_m2199215334_MethodInfo_var);
		V_9 = L_41;
		Dictionary_2_t309261261 * L_42 = V_9;
		String_t* L_43 = V_8;
		NullCheck(L_42);
		Dictionary_2_set_Item_m1358196493(L_42, _stringLiteral339799344, L_43, /*hidden argument*/Dictionary_2_set_Item_m1358196493_MethodInfo_var);
	}

IL_010c:
	{
		String_t* L_44 = V_7;
		Dictionary_2_t309261261 * L_45 = V_9;
		GAuthToken_t2345784186 * L_46 = (GAuthToken_t2345784186 *)il2cpp_codegen_object_new(GAuthToken_t2345784186_il2cpp_TypeInfo_var);
		GAuthToken__ctor_m2026883488(L_46, L_44, L_45, /*hidden argument*/NULL);
		V_10 = L_46;
		U3CGetTokenU3Ec__AnonStorey0_t4052243837 * L_47 = V_0;
		NullCheck(L_47);
		Il2CppObject * L_48 = L_47->get_listener_0();
		GAuthToken_t2345784186 * L_49 = V_10;
		NullCheck(L_49);
		String_t* L_50 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Firebase.Database.Core.GAuthToken::SerializeToString() */, L_49);
		NullCheck(L_48);
		InterfaceActionInvoker1< String_t* >::Invoke(0 /* System.Void Firebase.Database.Core.AuthTokenProvider/IGetTokenCompletionListener::OnSuccess(System.String) */, IGetTokenCompletionListener_t2927602790_il2cpp_TypeInfo_var, L_48, L_50);
	}

IL_0129:
	{
		goto IL_017d;
	}

IL_012e:
	{
		FirebaseApp_t210707726 * L_51 = __this->get__firebaseApp_0();
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t3105883899_il2cpp_TypeInfo_var);
		FirebaseAuth_t3105883899 * L_52 = FirebaseAuth_GetAuth_m1881142076(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		V_11 = L_52;
		FirebaseAuth_t3105883899 * L_53 = V_11;
		if (!L_53)
		{
			goto IL_016d;
		}
	}
	{
		FirebaseAuth_t3105883899 * L_54 = V_11;
		NullCheck(L_54);
		FirebaseUser_t4046966602 * L_55 = FirebaseAuth_get_CurrentUser_m3290820871(L_54, /*hidden argument*/NULL);
		V_12 = L_55;
		FirebaseUser_t4046966602 * L_56 = V_12;
		if (!L_56)
		{
			goto IL_016d;
		}
	}
	{
		FirebaseUser_t4046966602 * L_57 = V_12;
		NullCheck(L_57);
		Task_1_t1149249240 * L_58 = FirebaseUser_TokenAsync_m3595996755(L_57, (bool)0, /*hidden argument*/NULL);
		U3CGetTokenU3Ec__AnonStorey0_t4052243837 * L_59 = V_0;
		IntPtr_t L_60;
		L_60.set_m_value_0((void*)(void*)U3CGetTokenU3Ec__AnonStorey0_U3CU3Em__0_m1504785580_MethodInfo_var);
		Func_2_t3938612258 * L_61 = (Func_2_t3938612258 *)il2cpp_codegen_object_new(Func_2_t3938612258_il2cpp_TypeInfo_var);
		Func_2__ctor_m1446518598(L_61, L_59, L_60, /*hidden argument*/Func_2__ctor_m1446518598_MethodInfo_var);
		NullCheck(L_58);
		Task_1_ContinueWith_TisString_t_m2972546687(L_58, L_61, /*hidden argument*/Task_1_ContinueWith_TisString_t_m2972546687_MethodInfo_var);
		return;
	}

IL_016d:
	{
		U3CGetTokenU3Ec__AnonStorey0_t4052243837 * L_62 = V_0;
		NullCheck(L_62);
		Il2CppObject * L_63 = L_62->get_listener_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_64 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_63);
		InterfaceActionInvoker1< String_t* >::Invoke(0 /* System.Void Firebase.Database.Core.AuthTokenProvider/IGetTokenCompletionListener::OnSuccess(System.String) */, IGetTokenCompletionListener_t2927602790_il2cpp_TypeInfo_var, L_63, L_64);
	}

IL_017d:
	{
		return;
	}
}
// System.Void Firebase.Database.Unity.UnityAuthTokenProvider::AddTokenChangeListener(Firebase.Database.Core.AuthTokenProvider/ITokenChangeListener)
extern Il2CppClass* U3CAddTokenChangeListenerU3Ec__AnonStorey1_t3054348951_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseAuth_t3105883899_il2cpp_TypeInfo_var;
extern Il2CppClass* EventHandler_t277755526_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CAddTokenChangeListenerU3Ec__AnonStorey1_U3CU3Em__0_m364880740_MethodInfo_var;
extern const uint32_t UnityAuthTokenProvider_AddTokenChangeListener_m3234711904_MetadataUsageId;
extern "C"  void UnityAuthTokenProvider_AddTokenChangeListener_m3234711904 (UnityAuthTokenProvider_t371495853 * __this, Il2CppObject * ___listener0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAuthTokenProvider_AddTokenChangeListener_m3234711904_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CAddTokenChangeListenerU3Ec__AnonStorey1_t3054348951 * V_0 = NULL;
	FirebaseAuth_t3105883899 * V_1 = NULL;
	{
		U3CAddTokenChangeListenerU3Ec__AnonStorey1_t3054348951 * L_0 = (U3CAddTokenChangeListenerU3Ec__AnonStorey1_t3054348951 *)il2cpp_codegen_object_new(U3CAddTokenChangeListenerU3Ec__AnonStorey1_t3054348951_il2cpp_TypeInfo_var);
		U3CAddTokenChangeListenerU3Ec__AnonStorey1__ctor_m2451425437(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAddTokenChangeListenerU3Ec__AnonStorey1_t3054348951 * L_1 = V_0;
		Il2CppObject * L_2 = ___listener0;
		NullCheck(L_1);
		L_1->set_listener_0(L_2);
		FirebaseApp_t210707726 * L_3 = __this->get__firebaseApp_0();
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t3105883899_il2cpp_TypeInfo_var);
		FirebaseAuth_t3105883899 * L_4 = FirebaseAuth_GetAuth_m1881142076(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		FirebaseAuth_t3105883899 * L_5 = V_1;
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		FirebaseAuth_t3105883899 * L_6 = V_1;
		U3CAddTokenChangeListenerU3Ec__AnonStorey1_t3054348951 * L_7 = V_0;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)U3CAddTokenChangeListenerU3Ec__AnonStorey1_U3CU3Em__0_m364880740_MethodInfo_var);
		EventHandler_t277755526 * L_9 = (EventHandler_t277755526 *)il2cpp_codegen_object_new(EventHandler_t277755526_il2cpp_TypeInfo_var);
		EventHandler__ctor_m3447735595(L_9, L_7, L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		FirebaseAuth_add_StateChanged_m2504269526(L_6, L_9, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Firebase.Database.Unity.UnityAuthTokenProvider/<AddTokenChangeListener>c__AnonStorey1::.ctor()
extern "C"  void U3CAddTokenChangeListenerU3Ec__AnonStorey1__ctor_m2451425437 (U3CAddTokenChangeListenerU3Ec__AnonStorey1_t3054348951 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Database.Unity.UnityAuthTokenProvider/<AddTokenChangeListener>c__AnonStorey1::<>m__0(System.Object,System.EventArgs)
extern Il2CppClass* ITokenChangeListener_t4180538330_il2cpp_TypeInfo_var;
extern const uint32_t U3CAddTokenChangeListenerU3Ec__AnonStorey1_U3CU3Em__0_m364880740_MetadataUsageId;
extern "C"  void U3CAddTokenChangeListenerU3Ec__AnonStorey1_U3CU3Em__0_m364880740 (U3CAddTokenChangeListenerU3Ec__AnonStorey1_t3054348951 * __this, Il2CppObject * ___sender0, EventArgs_t3289624707 * ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CAddTokenChangeListenerU3Ec__AnonStorey1_U3CU3Em__0_m364880740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_listener_0();
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void Firebase.Database.Core.AuthTokenProvider/ITokenChangeListener::OnTokenChange() */, ITokenChangeListener_t4180538330_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void Firebase.Database.Unity.UnityAuthTokenProvider/<GetToken>c__AnonStorey0::.ctor()
extern "C"  void U3CGetTokenU3Ec__AnonStorey0__ctor_m1307976171 (U3CGetTokenU3Ec__AnonStorey0_t4052243837 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String Firebase.Database.Unity.UnityAuthTokenProvider/<GetToken>c__AnonStorey0::<>m__0(System.Threading.Tasks.Task`1<System.String>)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IGetTokenCompletionListener_t2927602790_il2cpp_TypeInfo_var;
extern const MethodInfo* Task_1_get_Result_m1972456293_MethodInfo_var;
extern const uint32_t U3CGetTokenU3Ec__AnonStorey0_U3CU3Em__0_m1504785580_MetadataUsageId;
extern "C"  String_t* U3CGetTokenU3Ec__AnonStorey0_U3CU3Em__0_m1504785580 (U3CGetTokenU3Ec__AnonStorey0_t4052243837 * __this, Task_1_t1149249240 * ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetTokenU3Ec__AnonStorey0_U3CU3Em__0_m1504785580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Task_1_t1149249240 * L_0 = ___x0;
		NullCheck(L_0);
		String_t* L_1 = Task_1_get_Result_m1972456293(L_0, /*hidden argument*/Task_1_get_Result_m1972456293_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject * L_3 = __this->get_listener_0();
		Task_1_t1149249240 * L_4 = ___x0;
		NullCheck(L_4);
		String_t* L_5 = Task_1_get_Result_m1972456293(L_4, /*hidden argument*/Task_1_get_Result_m1972456293_MethodInfo_var);
		NullCheck(L_3);
		InterfaceActionInvoker1< String_t* >::Invoke(0 /* System.Void Firebase.Database.Core.AuthTokenProvider/IGetTokenCompletionListener::OnSuccess(System.String) */, IGetTokenCompletionListener_t2927602790_il2cpp_TypeInfo_var, L_3, L_5);
		goto IL_0036;
	}

IL_0026:
	{
		Il2CppObject * L_6 = __this->get_listener_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_6);
		InterfaceActionInvoker1< String_t* >::Invoke(0 /* System.Void Firebase.Database.Core.AuthTokenProvider/IGetTokenCompletionListener::OnSuccess(System.String) */, IGetTokenCompletionListener_t2927602790_il2cpp_TypeInfo_var, L_6, L_7);
	}

IL_0036:
	{
		return (String_t*)NULL;
	}
}
// System.Void Firebase.Database.Unity.UnityPlatform::.cctor()
extern Il2CppClass* UnityPlatform_t3623770554_il2cpp_TypeInfo_var;
extern const uint32_t UnityPlatform__cctor_m1339425218_MetadataUsageId;
extern "C"  void UnityPlatform__cctor_m1339425218 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityPlatform__cctor_m1339425218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityPlatform_SetPlatformFactory_m1143030984(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Database.Unity.UnityPlatform::.ctor(Firebase.FirebaseApp)
extern "C"  void UnityPlatform__ctor_m2132707302 (UnityPlatform_t3623770554 * __this, FirebaseApp_t210707726 * ___firebaseApp0, const MethodInfo* method)
{
	{
		FirebaseApp_t210707726 * L_0 = ___firebaseApp0;
		DotNetPlatform__ctor_m3903981007(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Database.Unity.UnityPlatform::SetPlatformFactory()
extern Il2CppClass* UnityPlatform_t3623770554_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3961743794_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityPlatform_U3CSetPlatformFactoryU3Em__0_m801103290_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m568761322_MethodInfo_var;
extern const uint32_t UnityPlatform_SetPlatformFactory_m1143030984_MetadataUsageId;
extern "C"  void UnityPlatform_SetPlatformFactory_m1143030984 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityPlatform_SetPlatformFactory_m1143030984_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityPlatform_t3623770554_il2cpp_TypeInfo_var);
		Func_2_t3961743794 * L_0 = ((UnityPlatform_t3623770554_StaticFields*)UnityPlatform_t3623770554_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_1();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UnityPlatform_U3CSetPlatformFactoryU3Em__0_m801103290_MethodInfo_var);
		Func_2_t3961743794 * L_2 = (Func_2_t3961743794 *)il2cpp_codegen_object_new(Func_2_t3961743794_il2cpp_TypeInfo_var);
		Func_2__ctor_m568761322(L_2, NULL, L_1, /*hidden argument*/Func_2__ctor_m568761322_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(UnityPlatform_t3623770554_il2cpp_TypeInfo_var);
		((UnityPlatform_t3623770554_StaticFields*)UnityPlatform_t3623770554_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_1(L_2);
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityPlatform_t3623770554_il2cpp_TypeInfo_var);
		Func_2_t3961743794 * L_3 = ((UnityPlatform_t3623770554_StaticFields*)UnityPlatform_t3623770554_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_1();
		FirebaseConfigExtensions_SetPlatformFactory_m4072611435(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// Firebase.Database.Logger Firebase.Database.Unity.UnityPlatform::NewLogger(Firebase.Database.Logger/Level,System.Collections.Generic.IList`1<System.String>)
extern Il2CppClass* UnityLogger_t2258131283_il2cpp_TypeInfo_var;
extern const uint32_t UnityPlatform_NewLogger_m1758199710_MetadataUsageId;
extern "C"  Logger_t225270238 * UnityPlatform_NewLogger_m1758199710 (UnityPlatform_t3623770554 * __this, int32_t ___level0, Il2CppObject* ___components1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityPlatform_NewLogger_m1758199710_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___level0;
		UnityLogger_t2258131283 * L_1 = (UnityLogger_t2258131283 *)il2cpp_codegen_object_new(UnityLogger_t2258131283_il2cpp_TypeInfo_var);
		UnityLogger__ctor_m530628304(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Firebase.Database.Core.AuthTokenProvider Firebase.Database.Unity.UnityPlatform::NewAuthTokenProvider()
extern Il2CppClass* UnityAuthTokenProvider_t371495853_il2cpp_TypeInfo_var;
extern const uint32_t UnityPlatform_NewAuthTokenProvider_m4255098023_MetadataUsageId;
extern "C"  AuthTokenProvider_t3681374264 * UnityPlatform_NewAuthTokenProvider_m4255098023 (UnityPlatform_t3623770554 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityPlatform_NewAuthTokenProvider_m4255098023_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FirebaseApp_t210707726 * L_0 = DotNetPlatform_get_FirebaseApp_m465321925(__this, /*hidden argument*/NULL);
		UnityAuthTokenProvider_t371495853 * L_1 = (UnityAuthTokenProvider_t371495853 *)il2cpp_codegen_object_new(UnityAuthTokenProvider_t371495853_il2cpp_TypeInfo_var);
		UnityAuthTokenProvider__ctor_m3982661543(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Firebase.Database.DotNet.DotNetPlatform Firebase.Database.Unity.UnityPlatform::<SetPlatformFactory>m__0(Firebase.FirebaseApp)
extern Il2CppClass* UnityPlatform_t3623770554_il2cpp_TypeInfo_var;
extern const uint32_t UnityPlatform_U3CSetPlatformFactoryU3Em__0_m801103290_MetadataUsageId;
extern "C"  DotNetPlatform_t2951135975 * UnityPlatform_U3CSetPlatformFactoryU3Em__0_m801103290 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityPlatform_U3CSetPlatformFactoryU3Em__0_m801103290_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FirebaseApp_t210707726 * L_0 = ___x0;
		UnityPlatform_t3623770554 * L_1 = (UnityPlatform_t3623770554 *)il2cpp_codegen_object_new(UnityPlatform_t3623770554_il2cpp_TypeInfo_var);
		UnityPlatform__ctor_m2132707302(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Firebase.Database.Unity.UnityPlatform/UnityLogger::.ctor(Firebase.Database.Logger/Level)
extern "C"  void UnityLogger__ctor_m530628304 (UnityLogger_t2258131283 * __this, int32_t ___level0, const MethodInfo* method)
{
	{
		Logger__ctor_m3319557657(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___level0;
		__this->set__loglevel_0(L_0);
		return;
	}
}
// System.Void Firebase.Database.Unity.UnityPlatform/UnityLogger::OnLogMessage(Firebase.Database.Logger/Level,System.String,System.String,System.Int64)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3605534298;
extern Il2CppCodeGenString* _stringLiteral372029336;
extern const uint32_t UnityLogger_OnLogMessage_m3482070510_MetadataUsageId;
extern "C"  void UnityLogger_OnLogMessage_m3482070510 (UnityLogger_t2258131283 * __this, int32_t ___level0, String_t* ___tag1, String_t* ___message2, int64_t ___msTimestamp3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityLogger_OnLogMessage_m3482070510_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___tag1;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		___tag1 = _stringLiteral3605534298;
	}

IL_000d:
	{
		int32_t L_1 = ___level0;
		int32_t L_2 = __this->get__loglevel_0();
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_3 = ___level0;
		if ((((int32_t)L_3) < ((int32_t)3)))
		{
			goto IL_0036;
		}
	}
	{
		String_t* L_4 = ___tag1;
		String_t* L_5 = ___message2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m612901809(NULL /*static, unused*/, L_4, _stringLiteral372029336, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		goto IL_0047;
	}

IL_0036:
	{
		String_t* L_7 = ___tag1;
		String_t* L_8 = ___message2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m612901809(NULL /*static, unused*/, L_7, _stringLiteral372029336, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0047:
	{
		return;
	}
}
// Firebase.Database.Logger/Level Firebase.Database.Unity.UnityPlatform/UnityLogger::GetLogLevel()
extern "C"  int32_t UnityLogger_GetLogLevel_m3316149087 (UnityLogger_t2258131283 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get__loglevel_0();
		return L_0;
	}
}
// System.Void Firebase.Unity.Editor.FirebaseEditorExtensions::SetEditorDatabaseUrl(Firebase.FirebaseApp,System.String)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2715004041;
extern const uint32_t FirebaseEditorExtensions_SetEditorDatabaseUrl_m3984445699_MetadataUsageId;
extern "C"  void FirebaseEditorExtensions_SetEditorDatabaseUrl_m3984445699 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, String_t* ___databaseUrl1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseEditorExtensions_SetEditorDatabaseUrl_m3984445699_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FirebaseApp_t210707726 * L_0 = ___app0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, _stringLiteral2715004041, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		FirebaseApp_t210707726 * L_2 = ___app0;
		String_t* L_3 = ___databaseUrl1;
		FirebaseConfigExtensions_SetDatabaseUrl_m4171544188(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.String Firebase.Unity.Editor.FirebaseEditorExtensions::GetEditorP12Password(Firebase.FirebaseApp)
extern Il2CppClass* FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseEditorExtensions_GetEditorP12Password_m3642378088_MetadataUsageId;
extern "C"  String_t* FirebaseEditorExtensions_GetEditorP12Password_m3642378088 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseEditorExtensions_GetEditorP12Password_m3642378088_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FirebaseApp_t210707726 * L_0 = ___app0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var);
		String_t* L_1 = FirebaseEditorExtensions_GetEditorP12Password_m873176310(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String Firebase.Unity.Editor.FirebaseEditorExtensions::GetEditorP12FileName(Firebase.FirebaseApp)
extern Il2CppClass* FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseEditorExtensions_GetEditorP12FileName_m1066698760_MetadataUsageId;
extern "C"  String_t* FirebaseEditorExtensions_GetEditorP12FileName_m1066698760 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseEditorExtensions_GetEditorP12FileName_m1066698760_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FirebaseApp_t210707726 * L_0 = ___app0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var);
		String_t* L_1 = FirebaseEditorExtensions_GetEditorP12FileName_m620685606(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String Firebase.Unity.Editor.FirebaseEditorExtensions::GetEditorServiceAccountEmail(Firebase.FirebaseApp)
extern Il2CppClass* FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseEditorExtensions_GetEditorServiceAccountEmail_m2686895772_MetadataUsageId;
extern "C"  String_t* FirebaseEditorExtensions_GetEditorServiceAccountEmail_m2686895772 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseEditorExtensions_GetEditorServiceAccountEmail_m2686895772_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FirebaseApp_t210707726 * L_0 = ___app0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var);
		String_t* L_1 = FirebaseEditorExtensions_GetEditorServiceAccountEmail_m460851250(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String Firebase.Unity.Editor.FirebaseEditorExtensions::GetEditorAuthUserId(Firebase.FirebaseApp)
extern Il2CppClass* FirebaseAuth_t3105883899_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var;
extern const uint32_t FirebaseEditorExtensions_GetEditorAuthUserId_m1692323114_MetadataUsageId;
extern "C"  String_t* FirebaseEditorExtensions_GetEditorAuthUserId_m1692323114 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseEditorExtensions_GetEditorAuthUserId_m1692323114_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	FirebaseAuth_t3105883899 * V_1 = NULL;
	{
		V_0 = (String_t*)NULL;
		FirebaseApp_t210707726 * L_0 = ___app0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t3105883899_il2cpp_TypeInfo_var);
		FirebaseAuth_t3105883899 * L_1 = FirebaseAuth_GetAuth_m1881142076(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		FirebaseAuth_t3105883899 * L_2 = V_1;
		NullCheck(L_2);
		FirebaseUser_t4046966602 * L_3 = FirebaseAuth_get_CurrentUser_m3290820871(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0020;
		}
	}
	{
		FirebaseAuth_t3105883899 * L_4 = V_1;
		NullCheck(L_4);
		FirebaseUser_t4046966602 * L_5 = FirebaseAuth_get_CurrentUser_m3290820871(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = FirebaseUser_get_UserId_m3056192957(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_0020:
	{
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0032;
		}
	}
	{
		FirebaseApp_t210707726 * L_9 = ___app0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseEditorExtensions_t1010167188_il2cpp_TypeInfo_var);
		String_t* L_10 = FirebaseEditorExtensions_GetEditorAuthUserId_m2961892456(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
	}

IL_0032:
	{
		String_t* L_11 = V_0;
		return L_11;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
