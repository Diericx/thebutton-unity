﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Boo.Lang.GenericGenerator`1<System.Object>
struct GenericGenerator_1_t3108987245;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// Boo.Lang.GenericGeneratorEnumerator`1<System.Object>
struct GenericGeneratorEnumerator_1_t3445420457;
// System.Object
struct Il2CppObject;
// Boo.Lang.List`1/<GetEnumerator>c__Iterator6<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator6_t3055131479;
// Boo.Lang.List`1<System.Object>
struct List_1_t61287617;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// Firebase.Database.Internal.Collection.ArraySortedMap`2/Enumerator134<System.Object,System.Object>
struct Enumerator134_t1479555021;
// Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>
struct ArraySortedMap_2_t3398066313;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t643912417;
// System.Collections.Generic.IDictionary`2<System.Object,System.Object>
struct IDictionary_2_t280592844;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3230389896;
// Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder/IKeyTranslator<System.Object,System.Object>
struct IKeyTranslator_t1428849306;
// Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>
struct ImmutableSortedMap_2_t3217094540;
// Firebase.Database.Internal.Collection.RbTreeSortedMap`2<System.Object,System.Object>
struct RbTreeSortedMap_2_t791914602;
// Firebase.Database.Internal.Collection.LlrbNode`2/NodeVisitor<System.Object,System.Object>
struct NodeVisitor_t2944196615;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t1809345768;
// Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder/KeyTranslator109<System.Object,System.Object>
struct KeyTranslator109_t2828396717;
// Firebase.Database.Internal.Collection.ImmutableSortedMapIterator`2<System.Object,System.Object>
struct ImmutableSortedMapIterator_2_t2384854130;
// Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>
struct LlrbNode_2_t4262869811;
// Firebase.Database.Internal.Collection.ImmutableSortedSet`1/WrappedEntryIterator<System.Object>
struct WrappedEntryIterator_t3190181025;
// Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>
struct ImmutableSortedSet_1_t2348759144;
// Firebase.Database.Internal.Collection.LlrbBlackValueNode`2<System.Object,System.Object>
struct LlrbBlackValueNode_2_t3839725159;
// Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>
struct LlrbValueNode_2_t1296857666;
// Firebase.Database.Internal.Collection.LlrbEmptyNode`2<System.Object,System.Object>
struct LlrbEmptyNode_2_t3030218886;
// Firebase.Database.Internal.Collection.LlrbRedValueNode`2<System.Object,System.Object>
struct LlrbRedValueNode_2_t3628144835;
// Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12/Enumerator216<System.Object,System.Object,System.Object,System.Object,System.Object>
struct Enumerator216_t3810636662;
// Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12<System.Object,System.Object,System.Object,System.Object,System.Object>
struct Base12_t2988184810;
// Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/BooleanChunk<System.Object,System.Object,System.Object,System.Object,System.Object>
struct BooleanChunk_t3036858313;
// System.Collections.Generic.IEnumerator`1<Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/BooleanChunk<System.Object,System.Object,System.Object,System.Object,System.Object>>
struct IEnumerator_1_t512382140;
// Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3<System.Object,System.Object,System.Object,System.Object,System.Object>
struct BuilderAbc_3_t1611920761;
// Firebase.Database.Internal.Collection.StandardComparator`1<System.Object>
struct StandardComparator_1_t2182977910;
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1/TreeVisitor278<System.Nullable`1<System.Boolean>>
struct TreeVisitor278_t934374925;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>>
struct IList_1_t3991400828;
// Firebase.Database.Internal.Core.Path
struct Path_t2568473163;
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1/TreeVisitor278<System.Object>
struct TreeVisitor278_t1535183187;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Object>>
struct IList_1_t297241794;
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>
struct ImmutableTree_1_t368550071;
// Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>
struct ImmutableSortedMap_2_t94277708;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>>
struct IEnumerator_1_t925984054;
// Firebase.Database.Internal.Core.Utilities.Predicate`1<System.Nullable`1<System.Boolean>>
struct Predicate_1_t605805710;
// Firebase.Database.Internal.Snapshot.ChildKey
struct ChildKey_t1197802383;
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1/ITreeVisitor`1<System.Nullable`1<System.Boolean>,System.Object>
struct ITreeVisitor_1_t2066911337;
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>
struct ImmutableTree_1_t969358333;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Object>>
struct IEnumerator_1_t1526792316;
// Firebase.Database.Internal.Core.Utilities.Predicate`1<System.Object>
struct Predicate_1_t1206613972;
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1/ITreeVisitor`1<System.Object,System.Object>
struct ITreeVisitor_1_t3362435403;
// Firebase.Database.Internal.Core.Utilities.Predicate`1/Predicate6<System.Nullable`1<System.Boolean>>
struct Predicate6_t842365816;
// Firebase.Database.Internal.Core.Utilities.Predicate`1/Predicate6<System.Object>
struct Predicate6_t1443174078;
// Firebase.Database.Internal.Core.Utilities.Tree`1/TreeVisitor111<System.Object>
struct TreeVisitor111_t1617732397;
// Firebase.Database.Internal.Core.Utilities.Tree`1/ITreeVisitor<System.Object>
struct ITreeVisitor_t512772653;
// Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>
struct Tree_1_t3114744003;
// Firebase.Database.Internal.Core.Utilities.TreeNode`1<System.Object>
struct TreeNode_1_t3829898795;
// System.Collections.Generic.IDictionary`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>
struct IDictionary_2_t1452743308;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>>
struct ICollection_1_t2163080414;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ICollection_1_t990929950;
// Firebase.Database.Internal.Core.Utilities.Tree`1/ITreeFilter<System.Object>
struct ITreeFilter_t3174189785;
// Firebase.Database.Internal.Utilities.Pair`2<System.Object,System.Object>
struct Pair_2_t1259711689;
// Google.Sharpen.BlockingCollection`1<System.Object>
struct BlockingCollection_1_t3061287978;
// System.Collections.Generic.IList`1<System.Int64>
struct IList_1_t1450018638;
// Google.Sharpen.MessageDigest`1<System.Object>
struct MessageDigest_1_t1512921346;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Google.Sharpen.ScheduledThreadPoolExecutor/Task`1<System.Object>
struct Task_1_t2765902707;
// Google.Sharpen.ScheduledThreadPoolExecutor
struct ScheduledThreadPoolExecutor_t2537379786;
// Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime>
struct WriteDelegate_1_t415313529;
// Pathfinding.Serialization.JsonFx.JsonWriter
struct JsonWriter_t446744171;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.Object>
struct WriteDelegate_1_t2411557155;
// PubNubMessaging.Core.CoroutineClass/<CheckTimeoutHeartbeat>c__Iterator8`1<System.Object>
struct U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t2997308713;
// PubNubMessaging.Core.CoroutineParams`1<System.Object>
struct CoroutineParams_1_t3789821357;
// PubNubMessaging.Core.CoroutineClass/<CheckTimeoutNonSub>c__Iterator6`1<System.Object>
struct U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296;
// PubNubMessaging.Core.CoroutineClass/<CheckTimeoutPresenceHeartbeat>c__Iterator7`1<System.Object>
struct U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t163451287;
// PubNubMessaging.Core.CoroutineClass/<CheckTimeoutSub>c__Iterator5`1<System.Object>
struct U3CCheckTimeoutSubU3Ec__Iterator5_1_t3644537212;
// PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1<System.Object>
struct U3CDelayRequestU3Ec__Iterator0_1_t1605486090;
// PubNubMessaging.Core.RequestState`1<System.Object>
struct RequestState_1_t8940997;
// PubNubMessaging.Core.CoroutineClass/<SendRequestHeartbeat>c__Iterator4`1<System.Object>
struct U3CSendRequestHeartbeatU3Ec__Iterator4_1_t1669783977;
// UnityEngine.WWW
struct WWW_t2919945039;
// PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1<System.Object>
struct U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748;
// PubNubMessaging.Core.CoroutineClass/<SendRequestPresenceHeartbeat>c__Iterator3`1<System.Object>
struct U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1006883627;
// PubNubMessaging.Core.CoroutineClass/<SendRequestSub>c__Iterator1`1<System.Object>
struct U3CSendRequestSubU3Ec__Iterator1_1_t4220044048;
// System.Type
struct Type_t;
// PubNubMessaging.Core.CustomEventArgs`1<System.Object>
struct CustomEventArgs_1_t1034156509;
// PubNubMessaging.Core.Helpers/<FindChannelEntityAndCallback>c__AnonStorey1`1<System.Object>
struct U3CFindChannelEntityAndCallbackU3Ec__AnonStorey1_1_t242888489;
// PubNubMessaging.Core.ChannelEntity
struct ChannelEntity_t3266154606;
// PubNubMessaging.Core.InternetState`1<System.Object>
struct InternetState_1_t2062608855;
// PubNubMessaging.Core.MultiplexExceptionEventArgs`1<System.Object>
struct MultiplexExceptionEventArgs_1_t2662502103;
// PubNubMessaging.Core.PubnubChannelCallback`1<System.Object>
struct PubnubChannelCallback_1_t3499039769;
// PubNubMessaging.Core.ReconnectState`1<System.Object>
struct ReconnectState_1_t3075722957;
// PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>
struct SafeDictionary_2_t537630836;
// System.Func`3<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Object>
struct Func_3_t916471171;
// System.Collections.Generic.ICollection`1<PubNubMessaging.Core.ChannelIdentity>
struct ICollection_1_t2099237572;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>[]
struct KeyValuePair_2U5BU5D_t402044932;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>
struct IEnumerator_1_t3152335084;
// PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>
struct SafeDictionary_2_t2260115521;
// System.Func`3<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Object>
struct Func_3_t1343333346;
// System.Collections.Generic.ICollection`1<PubNubMessaging.Core.CurrentRequestType>
struct ICollection_1_t4202303291;
// System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>[]
struct KeyValuePair_2U5BU5D_t828907107;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>
struct IEnumerator_1_t579852473;
// PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>
struct SafeDictionary_2_t3489608816;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t3369346583;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2854920344;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action`2<System.Boolean,System.Object>
struct Action_2_t2525452034;
// System.Action`2<System.Object,System.Object>
struct Action_2_t2572051853;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t2445488949;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t4145164493;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t1254237568;
// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t2471096271;
// System.Exception
struct Exception_t1927440687;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>
struct ArrayReadOnlyList_1_t4170771815;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3304067486;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t1864648666;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>
struct ArrayReadOnlyList_1_t1279844890;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1075686591;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t3268689037;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen3108987245.h"
#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen3108987245MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Object2689449295.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen3445420457.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen3445420457MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "Boo_Lang_Boo_Lang_List_1_U3CGetEnumeratorU3Ec__Ite3055131479.h"
#include "Boo_Lang_Boo_Lang_List_1_U3CGetEnumeratorU3Ec__Ite3055131479MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "Boo_Lang_Boo_Lang_List_1_gen61287617.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "Boo_Lang_Boo_Lang_List_1_gen61287617MethodDeclarations.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_Builtins3763248930MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices1910041954MethodDeclarations.h"
#include "mscorlib_System_Math2022911894MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle1479555021.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle1479555021MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle3398066313.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle3398066313MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle3217094540MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle1529578535MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle3217094540.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Collec791914602MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Collec791914602.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle2944196615.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle2944196615MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle2828396717.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle2828396717MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle1529578535.h"
#include "mscorlib_System_Text_StringBuilder1221177846MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle2384854130.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle2384854130MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle4262869811.h"
#include "System_System_Collections_Generic_Stack_1_gen2384585820.h"
#include "System_System_Collections_Generic_Stack_1_gen2384585820MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle4262869811MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle1296857666.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle3190181025.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle3190181025MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle2348759144.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle2348759144MethodDeclarations.h"
#include "Google_Sharpen_Google_Sharpen_Collections4125780067MethodDeclarations.h"
#include "Google_Sharpen_Google_Sharpen_Collections4125780067.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle3839725159.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle3839725159MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle1296857666MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle1714721308.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle3030218886.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle3030218886MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle3628144835.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle3628144835MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle1714721308MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle3810636662.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle3810636662MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle2988184810.h"
#include "mscorlib_System_Int64909078037.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle3036858313.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle3036858313MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle2988184810MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle1611920761.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle1611920761MethodDeclarations.h"
#include "Google_Sharpen_Google_Sharpen_Extensions996338116MethodDeclarations.h"
#include "Google_Sharpen_Google_Sharpen_Extensions996338116.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle2182977910.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle2182977910MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_U934374925.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_U934374925MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2568473163.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23450460227.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23450460227MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1535183187.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1535183187MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24051268489.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24051268489MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_U368550071.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_U368550071MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Collect94277708.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2819581359.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2819581359MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Collect94277708MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_U605805710.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2568473163MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps1197802383.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_U605805710MethodDeclarations.h"
#include "Firebase.Database_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21211005109.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21211005109MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps1197802383MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Collec691330998MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle2701728999MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Collec691330998.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_U969358333.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_U969358333MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3420389621.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3420389621MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1206613972.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1206613972MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_U842365816.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_U842365816MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1443174078.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1443174078MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1617732397.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1617732397MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3114744003.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3114744003MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3829898795.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3829898795MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3453659887MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3453659887.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En478717293.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En478717293MethodDeclarations.h"
#include "Firebase_Database_Firebase_Database_Internal_Utili1259711689.h"
#include "Firebase_Database_Firebase_Database_Internal_Utili1259711689MethodDeclarations.h"
#include "Google_Sharpen_Google_Sharpen_BlockingCollection_13061287978.h"
#include "Google_Sharpen_Google_Sharpen_BlockingCollection_13061287978MethodDeclarations.h"
#include "System_System_Threading_Semaphore159839144MethodDeclarations.h"
#include "System_System_Threading_Semaphore159839144.h"
#include "System_System_Collections_Generic_Queue_1_gen2509106130.h"
#include "System_System_Collections_Generic_Queue_1_gen2509106130MethodDeclarations.h"
#include "mscorlib_System_Threading_Monitor3228523394MethodDeclarations.h"
#include "mscorlib_System_Threading_WaitHandle677569169MethodDeclarations.h"
#include "mscorlib_System_Threading_WaitHandle677569169.h"
#include "mscorlib_System_ObjectDisposedException2695136451.h"
#include "Google_Sharpen_Google_Sharpen_Collections_1_gen957031954.h"
#include "Google_Sharpen_Google_Sharpen_Collections_1_gen957031954MethodDeclarations.h"
#include "Google_Sharpen_Google_Sharpen_Collections_1_gen2737403212.h"
#include "Google_Sharpen_Google_Sharpen_Collections_1_gen2737403212MethodDeclarations.h"
#include "Google_Sharpen_Google_Sharpen_MessageDigest_1_gen1512921346.h"
#include "Google_Sharpen_Google_Sharpen_MessageDigest_1_gen1512921346MethodDeclarations.h"
#include "Google_Sharpen_Google_Sharpen_MessageDigest1820469897MethodDeclarations.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Security_Cryptography_CryptoStream3531341937MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_CryptoStream3531341937.h"
#include "mscorlib_System_Security_Cryptography_HashAlgorith2624936259.h"
#include "mscorlib_System_Security_Cryptography_HashAlgorith2624936259MethodDeclarations.h"
#include "Google_Sharpen_Google_Sharpen_MessageDigest1820469897.h"
#include "mscorlib_System_IO_Stream3255436806MethodDeclarations.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "mscorlib_System_Activator1850728717MethodDeclarations.h"
#include "mscorlib_System_Activator1850728717.h"
#include "mscorlib_System_Security_Cryptography_CryptoStream1337713182.h"
#include "Google_Sharpen_Google_Sharpen_ScheduledThreadPoolE2765902707.h"
#include "Google_Sharpen_Google_Sharpen_ScheduledThreadPoolE2765902707MethodDeclarations.h"
#include "mscorlib_System_Threading_ManualResetEvent926074657MethodDeclarations.h"
#include "mscorlib_System_Threading_ManualResetEvent926074657.h"
#include "mscorlib_System_DateTime693205669.h"
#include "Google_Sharpen_Google_Sharpen_ScheduledThreadPoolE2537379786.h"
#include "Google_Sharpen_Google_Sharpen_ThreadPoolExecutor376308723MethodDeclarations.h"
#include "Google_Sharpen_Google_Sharpen_Thread1322377586MethodDeclarations.h"
#include "mscorlib_System_Threading_EventWaitHandle2091316307MethodDeclarations.h"
#include "Google_Sharpen_Google_Sharpen_Thread1322377586.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_WriteD415313529.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_WriteD415313529MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonWr446744171.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_Write2411557155.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_Write2411557155MethodDeclarations.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCl2997308713.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCl2997308713MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067MethodDeclarations.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutinePa3789821357.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "mscorlib_System_Single2076509932.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCl2476503358.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCl2476503358MethodDeclarations.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCl1941009296.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCl1941009296MethodDeclarations.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCla163451287.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCla163451287MethodDeclarations.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCl3644537212.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCl3644537212MethodDeclarations.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCl1605486090.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCl1605486090MethodDeclarations.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_RequestState_18940997.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCl1669783977.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCl1669783977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW2919945039MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCl4127451748.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCl4127451748MethodDeclarations.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCl1006883627.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCl1006883627MethodDeclarations.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCl4220044048.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCl4220044048MethodDeclarations.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutinePa3789821357MethodDeclarations.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CustomEvent1034156509.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CustomEvent1034156509MethodDeclarations.h"
#include "mscorlib_System_EventArgs3289624707MethodDeclarations.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_Helpers_U3CF242888489.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_Helpers_U3CF242888489MethodDeclarations.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelEnti3266154606.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelIden1147162267.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_InternetSta2062608855.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_InternetSta2062608855MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3627374100.h"
#include "mscorlib_System_Action_1_gen4207084599.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_MultiplexEx2662502103.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_MultiplexEx2662502103MethodDeclarations.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubChann3499039769.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubChann3499039769MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2491248677.h"
#include "mscorlib_System_Action_1_gen557919624.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ReconnectSt3075722957.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ReconnectSt3075722957MethodDeclarations.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_RequestState_18940997MethodDeclarations.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubWebRe3863823607.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubWebRes647984363.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2635275738.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ResponseTyp1248099179.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_SafeDictiona537630836.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_SafeDictiona537630836MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3624498739.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3624498739MethodDeclarations.h"
#include "System_Core_System_Func_3_gen916471171.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1813029214.h"
#include "mscorlib_System_NotImplementedException2785117854MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException2785117854.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2327558582.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21381843961.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_SafeDiction2260115521.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_SafeDiction2260115521MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1052016128.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1052016128MethodDeclarations.h"
#include "System_Core_System_Func_3_gen1343333346.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3535513899.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4050043267.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23104328646.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_SafeDiction3489608816.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_SafeDiction3489608816MethodDeclarations.h"
#include "System_Core_System_Func_3_gen3369346583.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke470039898.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va984569266.h"
#include "mscorlib_System_Action_1_gen3627374100MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2491248677MethodDeclarations.h"
#include "System_Core_System_Action_2_gen2525452034.h"
#include "System_Core_System_Action_2_gen2525452034MethodDeclarations.h"
#include "System_Core_System_Action_2_gen2572051853.h"
#include "System_Core_System_Action_2_gen2572051853MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2445488949.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2445488949MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen2471096271.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn4145164493.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn4145164493MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen4170771815.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn1254237568.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn1254237568MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen1279844890.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen2471096271MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen4170771815MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen1279844890MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2870158877.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2870158877MethodDeclarations.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndex2011406615.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen565169432.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen565169432MethodDeclarations.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake4001384466.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1542250127.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1542250127MethodDeclarations.h"
#include "mscorlib_Mono_Security_Uri_UriScheme683497865.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3199726719.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3199726719MethodDeclarations.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName2340974457.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2005914529.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2005914529MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4108980248.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4108980248MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3452969744.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3452969744MethodDeclarations.h"
#include "mscorlib_System_ArraySegment_1_gen2594217482.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen389359684.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen389359684MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen246889402.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen246889402MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen18266304.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen18266304MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3907627660.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3907627660MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4238481571.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4238481571MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L3379729309.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1723885533.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1723885533MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li865133271.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2240596223.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2240596223MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3963080908.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3963080908MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3990767863.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3990767863MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen313372414.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen313372414MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1867125779.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1867125779MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21008373517.h"

// Firebase.Database.Internal.Collection.ArraySortedMap`2<!!0,!!2> Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::BuildFrom<System.Object,System.Object,System.Object>(System.Collections.Generic.IList`1<!!0>,System.Collections.Generic.IDictionary`2<!!1,!!2>,Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder/IKeyTranslator<!!1,!!2>,System.Collections.Generic.IComparer`1<!!0>)
extern "C"  ArraySortedMap_2_t3398066313 * ArraySortedMap_2_BuildFrom_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m1911200854_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject* p1, Il2CppObject* p2, Il2CppObject* p3, const MethodInfo* method);
#define ArraySortedMap_2_BuildFrom_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m1911200854(__this /* static, unused */, p0, p1, p2, p3, method) ((  ArraySortedMap_2_t3398066313 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, Il2CppObject*, Il2CppObject*, const MethodInfo*))ArraySortedMap_2_BuildFrom_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m1911200854_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// !!0[] Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::RemoveFromArray<System.Object>(!!0[],System.Int32)
extern "C"  ObjectU5BU5D_t3614634134* ArraySortedMap_2_RemoveFromArray_TisIl2CppObject_m3037648441_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, int32_t p1, const MethodInfo* method);
#define ArraySortedMap_2_RemoveFromArray_TisIl2CppObject_m3037648441(__this /* static, unused */, p0, p1, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ArraySortedMap_2_RemoveFromArray_TisIl2CppObject_m3037648441_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::ReplaceInArray<System.Object>(!!0[],System.Int32,!!0)
extern "C"  ObjectU5BU5D_t3614634134* ArraySortedMap_2_ReplaceInArray_TisIl2CppObject_m3204988386_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, int32_t p1, Il2CppObject * p2, const MethodInfo* method);
#define ArraySortedMap_2_ReplaceInArray_TisIl2CppObject_m3204988386(__this /* static, unused */, p0, p1, p2, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, int32_t, Il2CppObject *, const MethodInfo*))ArraySortedMap_2_ReplaceInArray_TisIl2CppObject_m3204988386_gshared)(__this /* static, unused */, p0, p1, p2, method)
// Firebase.Database.Internal.Collection.RbTreeSortedMap`2<!!0,!!1> Firebase.Database.Internal.Collection.RbTreeSortedMap`2<System.Object,System.Object>::FromMap<System.Object,System.Object>(System.Collections.Generic.IDictionary`2<!!0,!!1>,System.Collections.Generic.IComparer`1<!!0>)
extern "C"  RbTreeSortedMap_2_t791914602 * RbTreeSortedMap_2_FromMap_TisIl2CppObject_TisIl2CppObject_m2089470893_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject* p1, const MethodInfo* method);
#define RbTreeSortedMap_2_FromMap_TisIl2CppObject_TisIl2CppObject_m2089470893(__this /* static, unused */, p0, p1, method) ((  RbTreeSortedMap_2_t791914602 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, const MethodInfo*))RbTreeSortedMap_2_FromMap_TisIl2CppObject_TisIl2CppObject_m2089470893_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::AddToArray<System.Object>(!!0[],System.Int32,!!0)
extern "C"  ObjectU5BU5D_t3614634134* ArraySortedMap_2_AddToArray_TisIl2CppObject_m3642001339_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, int32_t p1, Il2CppObject * p2, const MethodInfo* method);
#define ArraySortedMap_2_AddToArray_TisIl2CppObject_m3642001339(__this /* static, unused */, p0, p1, p2, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, int32_t, Il2CppObject *, const MethodInfo*))ArraySortedMap_2_AddToArray_TisIl2CppObject_m3642001339_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Collections.Generic.IDictionary`2<!!0,!!1> Google.Sharpen.Collections::EmptyMap<System.Object,System.Object>()
extern "C"  Il2CppObject* Collections_EmptyMap_TisIl2CppObject_TisIl2CppObject_m1534446411_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Collections_EmptyMap_TisIl2CppObject_TisIl2CppObject_m1534446411(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Collections_EmptyMap_TisIl2CppObject_TisIl2CppObject_m1534446411_gshared)(__this /* static, unused */, method)
// Firebase.Database.Internal.Collection.ImmutableSortedMap`2<!!0,!!2> Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder<System.Object,System.Object>::BuildFrom<System.Object,System.Object,System.Object>(System.Collections.Generic.IList`1<!!0>,System.Collections.Generic.IDictionary`2<!!1,!!2>,Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder/IKeyTranslator<!!1,!!2>,System.Collections.Generic.IComparer`1<!!0>)
extern "C"  ImmutableSortedMap_2_t3217094540 * Builder_BuildFrom_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m2528018_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject* p1, Il2CppObject* p2, Il2CppObject* p3, const MethodInfo* method);
#define Builder_BuildFrom_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m2528018(__this /* static, unused */, p0, p1, p2, p3, method) ((  ImmutableSortedMap_2_t3217094540 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, Il2CppObject*, Il2CppObject*, const MethodInfo*))Builder_BuildFrom_TisIl2CppObject_TisIl2CppObject_TisIl2CppObject_m2528018_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void Google.Sharpen.Extensions::Sort<System.Object>(System.Collections.Generic.IList`1<!!0>,System.Collections.Generic.IComparer`1<!!0>)
extern "C"  void Extensions_Sort_TisIl2CppObject_m4256495022_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject* p1, const MethodInfo* method);
#define Extensions_Sort_TisIl2CppObject_m4256495022(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, const MethodInfo*))Extensions_Sort_TisIl2CppObject_m4256495022_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::Fold<System.Object>(Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.Utilities.ImmutableTree`1/ITreeVisitor`1<T,!!0>,!!0)
extern "C"  Il2CppObject * ImmutableTree_1_Fold_TisIl2CppObject_m733258567_gshared (ImmutableTree_1_t368550071 * __this, Path_t2568473163 * p0, Il2CppObject* p1, Il2CppObject * p2, const MethodInfo* method);
#define ImmutableTree_1_Fold_TisIl2CppObject_m733258567(__this, p0, p1, p2, method) ((  Il2CppObject * (*) (ImmutableTree_1_t368550071 *, Path_t2568473163 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ImmutableTree_1_Fold_TisIl2CppObject_m733258567_gshared)(__this, p0, p1, p2, method)
// !!0 Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::Fold<System.Object>(Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.Utilities.ImmutableTree`1/ITreeVisitor`1<T,!!0>,!!0)
extern "C"  Il2CppObject * ImmutableTree_1_Fold_TisIl2CppObject_m1150095747_gshared (ImmutableTree_1_t969358333 * __this, Path_t2568473163 * p0, Il2CppObject* p1, Il2CppObject * p2, const MethodInfo* method);
#define ImmutableTree_1_Fold_TisIl2CppObject_m1150095747(__this, p0, p1, p2, method) ((  Il2CppObject * (*) (ImmutableTree_1_t969358333 *, Path_t2568473163 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))ImmutableTree_1_Fold_TisIl2CppObject_m1150095747_gshared)(__this, p0, p1, p2, method)
// System.Boolean Google.Sharpen.Extensions::Contains<System.Object,System.Object>(System.Collections.Generic.IDictionary`2<!!0,!!1>,!!0)
extern "C"  bool Extensions_Contains_TisIl2CppObject_TisIl2CppObject_m3895620891_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject * p1, const MethodInfo* method);
#define Extensions_Contains_TisIl2CppObject_TisIl2CppObject_m3895620891(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject *, const MethodInfo*))Extensions_Contains_TisIl2CppObject_TisIl2CppObject_m3895620891_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean Google.Sharpen.Extensions::Contains<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>(System.Collections.Generic.IDictionary`2<!!0,!!1>,!!0)
#define Extensions_Contains_TisChildKey_t1197802383_TisIl2CppObject_m1879317483(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, ChildKey_t1197802383 *, const MethodInfo*))Extensions_Contains_TisIl2CppObject_TisIl2CppObject_m3895620891_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean Google.Sharpen.Extensions::IsEmpty<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Collections.Generic.ICollection`1<!!0>)
extern "C"  bool Extensions_IsEmpty_TisKeyValuePair_2_t38854645_m3362125878_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Extensions_IsEmpty_TisKeyValuePair_2_t38854645_m3362125878(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Extensions_IsEmpty_TisKeyValuePair_2_t38854645_m3362125878_gshared)(__this /* static, unused */, p0, method)
// System.Boolean Google.Sharpen.Extensions::IsEmpty<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>>(System.Collections.Generic.ICollection`1<!!0>)
#define Extensions_IsEmpty_TisKeyValuePair_2_t1211005109_m2908331326(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Extensions_IsEmpty_TisKeyValuePair_2_t38854645_m3362125878_gshared)(__this /* static, unused */, p0, method)
// !!1 Google.Sharpen.Collections::Remove<System.Object,System.Object>(System.Collections.Generic.IDictionary`2<!!0,!!1>,!!0)
extern "C"  Il2CppObject * Collections_Remove_TisIl2CppObject_TisIl2CppObject_m4254658075_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Il2CppObject * p1, const MethodInfo* method);
#define Collections_Remove_TisIl2CppObject_TisIl2CppObject_m4254658075(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject *, const MethodInfo*))Collections_Remove_TisIl2CppObject_TisIl2CppObject_m4254658075_gshared)(__this /* static, unused */, p0, p1, method)
// !!1 Google.Sharpen.Collections::Remove<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>(System.Collections.Generic.IDictionary`2<!!0,!!1>,!!0)
#define Collections_Remove_TisChildKey_t1197802383_TisIl2CppObject_m2048268851(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, ChildKey_t1197802383 *, const MethodInfo*))Collections_Remove_TisIl2CppObject_TisIl2CppObject_m4254658075_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Activator::CreateInstance<System.Object>()
extern "C"  Il2CppObject * Activator_CreateInstance_TisIl2CppObject_m1022768098_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisIl2CppObject_m1022768098(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisIl2CppObject_m1022768098_gshared)(__this /* static, unused */, method)
// System.Void PubNubMessaging.Core.CoroutineClass::ProcessTimeout<System.Object>(PubNubMessaging.Core.CoroutineParams`1<T>)
extern "C"  void CoroutineClass_ProcessTimeout_TisIl2CppObject_m3255883322_gshared (CoroutineClass_t2476503358 * __this, CoroutineParams_1_t3789821357 * ___cp0, const MethodInfo* method);
#define CoroutineClass_ProcessTimeout_TisIl2CppObject_m3255883322(__this, ___cp0, method) ((  void (*) (CoroutineClass_t2476503358 *, CoroutineParams_1_t3789821357 *, const MethodInfo*))CoroutineClass_ProcessTimeout_TisIl2CppObject_m3255883322_gshared)(__this, ___cp0, method)
// System.Void PubNubMessaging.Core.CoroutineClass::StartCoroutinesByName<System.Object>(System.String,PubNubMessaging.Core.RequestState`1<T>,System.Int32,System.Int32,PubNubMessaging.Core.CurrentRequestType)
extern "C"  void CoroutineClass_StartCoroutinesByName_TisIl2CppObject_m1312232994_gshared (CoroutineClass_t2476503358 * __this, String_t* ___url0, RequestState_1_t8940997 * ___pubnubRequestState1, int32_t ___timeout2, int32_t ___pause3, int32_t ___crt4, const MethodInfo* method);
#define CoroutineClass_StartCoroutinesByName_TisIl2CppObject_m1312232994(__this, ___url0, ___pubnubRequestState1, ___timeout2, ___pause3, ___crt4, method) ((  void (*) (CoroutineClass_t2476503358 *, String_t*, RequestState_1_t8940997 *, int32_t, int32_t, int32_t, const MethodInfo*))CoroutineClass_StartCoroutinesByName_TisIl2CppObject_m1312232994_gshared)(__this, ___url0, ___pubnubRequestState1, ___timeout2, ___pause3, ___crt4, method)
// System.Void PubNubMessaging.Core.CoroutineClass::ProcessResponse<System.Object>(UnityEngine.WWW,PubNubMessaging.Core.CoroutineParams`1<T>)
extern "C"  void CoroutineClass_ProcessResponse_TisIl2CppObject_m4237048716_gshared (CoroutineClass_t2476503358 * __this, WWW_t2919945039 * ___www0, CoroutineParams_1_t3789821357 * ___cp1, const MethodInfo* method);
#define CoroutineClass_ProcessResponse_TisIl2CppObject_m4237048716(__this, ___www0, ___cp1, method) ((  void (*) (CoroutineClass_t2476503358 *, WWW_t2919945039 *, CoroutineParams_1_t3789821357 *, const MethodInfo*))CoroutineClass_ProcessResponse_TisIl2CppObject_m4237048716_gshared)(__this, ___www0, ___cp1, method)
// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisIl2CppObject_m2032877681_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, Il2CppObject * p1, const MethodInfo* method);
#define Array_IndexOf_TisIl2CppObject_m2032877681(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2032877681_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m745056346_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486* p0, CustomAttributeNamedArgument_t94157543  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m745056346(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m745056346_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m3666284377_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591* p0, CustomAttributeTypedArgument_t1498197914  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m3666284377(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m3666284377_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32)
extern "C"  TableRange_t2011406615  Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977(__this, p0, method) ((  TableRange_t2011406615  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Uri/UriScheme>(System.Int32)
extern "C"  UriScheme_t683497865  Array_InternalArray__get_Item_TisUriScheme_t683497865_m1381537299_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUriScheme_t683497865_m1381537299(__this, p0, method) ((  UriScheme_t683497865  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUriScheme_t683497865_m1381537299_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Xml2.XmlTextReader/TagName>(System.Int32)
extern "C"  TagName_t2340974457  Array_InternalArray__get_Item_TisTagName_t2340974457_m451755763_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTagName_t2340974457_m451755763(__this, p0, method) ((  TagName_t2340974457  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTagName_t2340974457_m451755763_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<PubNubMessaging.Core.ChannelIdentity>(System.Int32)
extern "C"  ChannelIdentity_t1147162267  Array_InternalArray__get_Item_TisChannelIdentity_t1147162267_m2880911186_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisChannelIdentity_t1147162267_m2880911186(__this, p0, method) ((  ChannelIdentity_t1147162267  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisChannelIdentity_t1147162267_m2880911186_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<PubNubMessaging.Core.CurrentRequestType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisCurrentRequestType_t3250227986_m779693847_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCurrentRequestType_t3250227986_m779693847(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCurrentRequestType_t3250227986_m779693847_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.ArraySegment`1<System.Byte>>(System.Int32)
extern "C"  ArraySegment_1_t2594217482  Array_InternalArray__get_Item_TisArraySegment_1_t2594217482_m983042683_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisArraySegment_1_t2594217482_m983042683(__this, p0, method) ((  ArraySegment_1_t2594217482  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisArraySegment_1_t2594217482_m983042683_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Boolean>(System.Int32)
extern "C"  bool Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639(__this, p0, method) ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Byte>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisByte_t3683104436_m635665873_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisByte_t3683104436_m635665873(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisByte_t3683104436_m635665873_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Char>(System.Int32)
extern "C"  Il2CppChar Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547(__this, p0, method) ((  Il2CppChar (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.DictionaryEntry>(System.Int32)
extern "C"  DictionaryEntry_t3048875398  Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320(__this, p0, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Int64>>(System.Int32)
extern "C"  Link_t3379729309  Array_InternalArray__get_Item_TisLink_t3379729309_m3341250869_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t3379729309_m3341250869(__this, p0, method) ((  Link_t3379729309  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t3379729309_m3341250869_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Object>>(System.Int32)
extern "C"  Link_t865133271  Array_InternalArray__get_Item_TisLink_t865133271_m2489845481_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t865133271_m2489845481(__this, p0, method) ((  Link_t865133271  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t865133271_m2489845481_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t1381843961  Array_InternalArray__get_Item_TisKeyValuePair_2_t1381843961_m1978754885_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1381843961_m1978754885(__this, p0, method) ((  KeyValuePair_2_t1381843961  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1381843961_m1978754885_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t3104328646  Array_InternalArray__get_Item_TisKeyValuePair_2_t3104328646_m3381025132_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3104328646_m3381025132(__this, p0, method) ((  KeyValuePair_2_t3104328646  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3104328646_m3381025132_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t3132015601  Array_InternalArray__get_Item_TisKeyValuePair_2_t3132015601_m2931568841_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3132015601_m2931568841(__this, p0, method) ((  KeyValuePair_2_t3132015601  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3132015601_m2931568841_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t3749587448  Array_InternalArray__get_Item_TisKeyValuePair_2_t3749587448_m833470118_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3749587448_m833470118(__this, p0, method) ((  KeyValuePair_2_t3749587448  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3749587448_m833470118_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>(System.Int32)
extern "C"  KeyValuePair_2_t1008373517  Array_InternalArray__get_Item_TisKeyValuePair_2_t1008373517_m3251731979_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1008373517_m3251731979(__this, p0, method) ((  KeyValuePair_2_t1008373517  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1008373517_m3251731979_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Boo.Lang.GenericGenerator`1<System.Object>::.ctor()
extern "C"  void GenericGenerator_1__ctor_m409073663_gshared (GenericGenerator_1_t3108987245 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Boo.Lang.GenericGenerator`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * GenericGenerator_1_System_Collections_IEnumerable_GetEnumerator_m3032861675_gshared (GenericGenerator_1_t3108987245 * __this, const MethodInfo* method)
{
	{
		NullCheck((GenericGenerator_1_t3108987245 *)__this);
		Il2CppObject* L_0 = VirtFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.IEnumerator`1<T> Boo.Lang.GenericGenerator`1<System.Object>::GetEnumerator() */, (GenericGenerator_1_t3108987245 *)__this);
		return L_0;
	}
}
// System.String Boo.Lang.GenericGenerator`1<System.Object>::ToString()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral550572160;
extern const uint32_t GenericGenerator_1_ToString_m1222804785_MetadataUsageId;
extern "C"  String_t* GenericGenerator_1_ToString_m1222804785_gshared (GenericGenerator_1_t3108987245 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GenericGenerator_1_ToString_m1222804785_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Format_m2024975688(NULL /*static, unused*/, (String_t*)_stringLiteral550572160, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::.ctor()
extern "C"  void GenericGeneratorEnumerator_1__ctor_m509214543_gshared (GenericGeneratorEnumerator_1_t3445420457 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set__state_1(0);
		return;
	}
}
// System.Object Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * GenericGeneratorEnumerator_1_System_Collections_IEnumerator_get_Current_m1921234798_gshared (GenericGeneratorEnumerator_1_t3445420457 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__current_0();
		return L_0;
	}
}
// T Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::get_Current()
extern "C"  Il2CppObject * GenericGeneratorEnumerator_1_get_Current_m3586948289_gshared (GenericGeneratorEnumerator_1_t3445420457 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__current_0();
		return L_0;
	}
}
// System.Void Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::Dispose()
extern "C"  void GenericGeneratorEnumerator_1_Dispose_m1715637737_gshared (GenericGeneratorEnumerator_1_t3445420457 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t GenericGeneratorEnumerator_1_Reset_m4255715191_MetadataUsageId;
extern "C"  void GenericGeneratorEnumerator_1_Reset_m4255715191_gshared (GenericGeneratorEnumerator_1_t3445420457 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GenericGeneratorEnumerator_1_Reset_m4255715191_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::Yield(System.Int32,T)
extern "C"  bool GenericGeneratorEnumerator_1_Yield_m557996368_gshared (GenericGeneratorEnumerator_1_t3445420457 * __this, int32_t ___state0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___state0;
		__this->set__state_1(L_0);
		Il2CppObject * L_1 = ___value1;
		__this->set__current_0(L_1);
		return (bool)1;
	}
}
// System.Boolean Boo.Lang.GenericGeneratorEnumerator`1<System.Object>::YieldDefault(System.Int32)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t GenericGeneratorEnumerator_1_YieldDefault_m799132706_MetadataUsageId;
extern "C"  bool GenericGeneratorEnumerator_1_YieldDefault_m799132706_gshared (GenericGeneratorEnumerator_1_t3445420457 * __this, int32_t ___state0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GenericGeneratorEnumerator_1_YieldDefault_m799132706_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		int32_t L_0 = ___state0;
		__this->set__state_1(L_0);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_1 = V_0;
		__this->set__current_0(L_1);
		return (bool)1;
	}
}
// System.Void Boo.Lang.List`1/<GetEnumerator>c__Iterator6<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator6__ctor_m963943484_gshared (U3CGetEnumeratorU3Ec__Iterator6_t3055131479 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T Boo.Lang.List`1/<GetEnumerator>c__Iterator6<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m925963075_gshared (U3CGetEnumeratorU3Ec__Iterator6_t3055131479 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Object Boo.Lang.List`1/<GetEnumerator>c__Iterator6<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m1245731780_gshared (U3CGetEnumeratorU3Ec__Iterator6_t3055131479 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Boolean Boo.Lang.List`1/<GetEnumerator>c__Iterator6<System.Object>::MoveNext()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral850795601;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator6_MoveNext_m3471029548_MetadataUsageId;
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator6_MoveNext_m3471029548_gshared (U3CGetEnumeratorU3Ec__Iterator6_t3055131479 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator6_MoveNext_m3471029548_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00ae;
		}
	}
	{
		goto IL_00d9;
	}

IL_0021:
	{
		List_1_t61287617 * L_2 = (List_1_t61287617 *)__this->get_U3CU3Ef__this_5();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__count_2();
		__this->set_U3CoriginalCountU3E__0_0(L_3);
		List_1_t61287617 * L_4 = (List_1_t61287617 *)__this->get_U3CU3Ef__this_5();
		NullCheck(L_4);
		ObjectU5BU5D_t3614634134* L_5 = (ObjectU5BU5D_t3614634134*)L_4->get__items_1();
		__this->set_U3CoriginalItemsU3E__1_1(L_5);
		__this->set_U3CiU3E__2_2(0);
		goto IL_00bc;
	}

IL_004f:
	{
		int32_t L_6 = (int32_t)__this->get_U3CoriginalCountU3E__0_0();
		List_1_t61287617 * L_7 = (List_1_t61287617 *)__this->get_U3CU3Ef__this_5();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__count_2();
		if ((!(((uint32_t)L_6) == ((uint32_t)L_8))))
		{
			goto IL_007b;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)__this->get_U3CoriginalItemsU3E__1_1();
		List_1_t61287617 * L_10 = (List_1_t61287617 *)__this->get_U3CU3Ef__this_5();
		NullCheck(L_10);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10->get__items_1();
		if ((((Il2CppObject*)(ObjectU5BU5D_t3614634134*)L_9) == ((Il2CppObject*)(ObjectU5BU5D_t3614634134*)L_11)))
		{
			goto IL_0086;
		}
	}

IL_007b:
	{
		InvalidOperationException_t721527559 * L_12 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_12, (String_t*)_stringLiteral850795601, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0086:
	{
		List_1_t61287617 * L_13 = (List_1_t61287617 *)__this->get_U3CU3Ef__this_5();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get__items_1();
		int32_t L_15 = (int32_t)__this->get_U3CiU3E__2_2();
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		__this->set_U24current_4(L_17);
		__this->set_U24PC_3(1);
		goto IL_00db;
	}

IL_00ae:
	{
		int32_t L_18 = (int32_t)__this->get_U3CiU3E__2_2();
		__this->set_U3CiU3E__2_2(((int32_t)((int32_t)L_18+(int32_t)1)));
	}

IL_00bc:
	{
		int32_t L_19 = (int32_t)__this->get_U3CiU3E__2_2();
		List_1_t61287617 * L_20 = (List_1_t61287617 *)__this->get_U3CU3Ef__this_5();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get__count_2();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_004f;
		}
	}
	{
		__this->set_U24PC_3((-1));
	}

IL_00d9:
	{
		return (bool)0;
	}

IL_00db:
	{
		return (bool)1;
	}
	// Dead block : IL_00dd: ldloc.1
}
// System.Void Boo.Lang.List`1/<GetEnumerator>c__Iterator6<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator6_Dispose_m1678474553_gshared (U3CGetEnumeratorU3Ec__Iterator6_t3055131479 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Boo.Lang.List`1/<GetEnumerator>c__Iterator6<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator6_Reset_m4037020951_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator6_Reset_m4037020951_gshared (U3CGetEnumeratorU3Ec__Iterator6_t3055131479 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator6_Reset_m4037020951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Boo.Lang.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2786657214_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3614634134* L_0 = ((List_1_t61287617_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_0();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::.cctor()
extern "C"  void List_1__cctor_m926686541_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t61287617_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_0(((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (uint32_t)0)));
		return;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void List_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m217229263_gshared (List_1_t61287617 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___item0;
		NullCheck((List_1_t61287617 *)__this);
		((  List_1_t61287617 * (*) (List_1_t61287617 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((List_1_t61287617 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Collections.IEnumerator Boo.Lang.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3060334751_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject*)__this);
		Il2CppObject* L_0 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)__this);
		return L_0;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void List_1_System_Collections_Generic_IListU3CTU3E_Insert_m2474991156_gshared (List_1_t61287617 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___item1;
		NullCheck((List_1_t61287617 *)__this);
		((  List_1_t61287617 * (*) (List_1_t61287617 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void List_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1444197752_gshared (List_1_t61287617 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_2 = ((  int32_t (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t61287617 *)__this);
		((  void (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Boolean Boo.Lang.List`1<System.Object>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3517243408_gshared (List_1_t61287617 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___item0;
		NullCheck((List_1_t61287617 *)__this);
		bool L_1 = ((  bool (*) (List_1_t61287617 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t61287617 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Int32 Boo.Lang.List`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m3135844156_gshared (List_1_t61287617 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((List_1_t61287617 *)__this);
		VirtFuncInvoker1< List_1_t61287617 *, Il2CppObject * >::Invoke(34 /* Boo.Lang.List`1<T> Boo.Lang.List`1<System.Object>::Add(T) */, (List_1_t61287617 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9))));
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t61287617 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t61287617 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return ((int32_t)((int32_t)L_1-(int32_t)1));
	}
}
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2107483803_gshared (List_1_t61287617 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((List_1_t61287617 *)__this);
		((  List_1_t61287617 * (*) (List_1_t61287617 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_0, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m187661787_gshared (List_1_t61287617 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((List_1_t61287617 *)__this);
		((  List_1_t61287617 * (*) (List_1_t61287617 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t61287617 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return;
	}
}
// System.Int32 Boo.Lang.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3492045770_gshared (List_1_t61287617 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_2 = ((  int32_t (*) (List_1_t61287617 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t61287617 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_2;
	}
}
// System.Boolean Boo.Lang.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2033159042_gshared (List_1_t61287617 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((List_1_t61287617 *)__this);
		bool L_2 = ((  bool (*) (List_1_t61287617 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t61287617 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return L_2;
	}
}
// System.Object Boo.Lang.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m936735699_gshared (List_1_t61287617 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t61287617 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		return L_1;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m611384076_gshared (List_1_t61287617 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((List_1_t61287617 *)__this);
		((  void (*) (List_1_t61287617 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_0, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		return;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void List_1_System_Collections_IList_RemoveAt_m1396596113_gshared (List_1_t61287617 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t61287617 *)__this);
		((  List_1_t61287617 * (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		return;
	}
}
// System.Boolean Boo.Lang.List`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1643189175_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m92731834_gshared (List_1_t61287617 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		int32_t L_3 = (int32_t)__this->get__count_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Boo.Lang.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m4001062748_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<T> Boo.Lang.List`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* List_1_GetEnumerator_m1902460426_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator6_t3055131479 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator6_t3055131479 * L_0 = (U3CGetEnumeratorU3Ec__Iterator6_t3055131479 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 19));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator6_t3055131479 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator6_t3055131479 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator6_t3055131479 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_5(__this);
		U3CGetEnumeratorU3Ec__Iterator6_t3055131479 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1360236987_gshared (List_1_t61287617 * __this, ObjectU5BU5D_t3614634134* ___target0, int32_t ___index1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		ObjectU5BU5D_t3614634134* L_1 = ___target0;
		int32_t L_2 = ___index1;
		int32_t L_3 = (int32_t)__this->get__count_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Boo.Lang.List`1<System.Object>::get_IsSynchronized()
extern "C"  bool List_1_get_IsSynchronized_m4219972939_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object Boo.Lang.List`1<System.Object>::get_SyncRoot()
extern "C"  Il2CppObject * List_1_get_SyncRoot_m2491646691_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		return (Il2CppObject *)L_0;
	}
}
// System.Boolean Boo.Lang.List`1<System.Object>::get_IsReadOnly()
extern "C"  bool List_1_get_IsReadOnly_m2003327643_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// T Boo.Lang.List`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_get_Item_m3413770970_gshared (List_1_t61287617 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_2 = ((  int32_t (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_3 = ((  int32_t (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck(L_0);
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		return L_5;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m2985722127_gshared (List_1_t61287617 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_2 = ((  int32_t (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_3 = ((  int32_t (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Il2CppObject * L_4 = ___value1;
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Il2CppObject *)L_4);
		return;
	}
}
// Boo.Lang.List`1<T> Boo.Lang.List`1<System.Object>::Push(T)
extern "C"  List_1_t61287617 * List_1_Push_m2231078188_gshared (List_1_t61287617 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___item0;
		NullCheck((List_1_t61287617 *)__this);
		List_1_t61287617 * L_1 = VirtFuncInvoker1< List_1_t61287617 *, Il2CppObject * >::Invoke(34 /* Boo.Lang.List`1<T> Boo.Lang.List`1<System.Object>::Add(T) */, (List_1_t61287617 *)__this, (Il2CppObject *)L_0);
		return L_1;
	}
}
// Boo.Lang.List`1<T> Boo.Lang.List`1<System.Object>::Add(T)
extern "C"  List_1_t61287617 * List_1_Add_m1792075939_gshared (List_1_t61287617 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		NullCheck((List_1_t61287617 *)__this);
		((  void (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((List_1_t61287617 *)__this, (int32_t)((int32_t)((int32_t)L_0+(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_2 = (int32_t)__this->get__count_2();
		Il2CppObject * L_3 = ___item0;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Il2CppObject *)L_3);
		int32_t L_4 = (int32_t)__this->get__count_2();
		__this->set__count_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		return __this;
	}
}
// System.String Boo.Lang.List`1<System.Object>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t List_1_ToString_m3353273637_MetadataUsageId;
extern "C"  String_t* List_1_ToString_m3353273637_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_ToString_m3353273637_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((List_1_t61287617 *)__this);
		String_t* L_0 = ((  String_t* (*) (List_1_t61287617 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)((List_1_t61287617 *)__this, (String_t*)_stringLiteral811305474, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m612901809(NULL /*static, unused*/, (String_t*)_stringLiteral372029431, (String_t*)L_0, (String_t*)_stringLiteral372029425, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String Boo.Lang.List`1<System.Object>::Join(System.String)
extern "C"  String_t* List_1_Join_m2084635115_gshared (List_1_t61287617 * __this, String_t* ___separator0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___separator0;
		String_t* L_1 = Builtins_join_m2036613869(NULL /*static, unused*/, (Il2CppObject *)__this, (String_t*)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Boo.Lang.List`1<System.Object>::GetHashCode()
extern "C"  int32_t List_1_GetHashCode_m1730152805_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		V_0 = (int32_t)L_0;
		V_1 = (int32_t)0;
		goto IL_003a;
	}

IL_000e:
	{
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = (Il2CppObject *)L_4;
		Il2CppObject * L_5 = V_2;
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_6 = V_0;
		NullCheck((Il2CppObject *)(*(&V_2)));
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)(*(&V_2)));
		V_0 = (int32_t)((int32_t)((int32_t)L_6^(int32_t)L_7));
	}

IL_0036:
	{
		int32_t L_8 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003a:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = (int32_t)__this->get__count_2();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_11 = V_0;
		return L_11;
	}
}
// System.Boolean Boo.Lang.List`1<System.Object>::Equals(System.Object)
extern "C"  bool List_1_Equals_m34533115_gshared (List_1_t61287617 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if ((((Il2CppObject*)(List_1_t61287617 *)__this) == ((Il2CppObject*)(Il2CppObject *)L_0)))
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject * L_1 = ___other0;
		NullCheck((List_1_t61287617 *)__this);
		bool L_2 = ((  bool (*) (List_1_t61287617 *, List_1_t61287617 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((List_1_t61287617 *)__this, (List_1_t61287617 *)((List_1_t61287617 *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 23))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0016;
	}

IL_0015:
	{
		G_B3_0 = 1;
	}

IL_0016:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean Boo.Lang.List`1<System.Object>::Equals(Boo.Lang.List`1<T>)
extern Il2CppClass* RuntimeServices_t1910041954_il2cpp_TypeInfo_var;
extern const uint32_t List_1_Equals_m2416030142_MetadataUsageId;
extern "C"  bool List_1_Equals_m2416030142_gshared (List_1_t61287617 * __this, List_1_t61287617 * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_Equals_m2416030142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_t61287617 * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		List_1_t61287617 * L_1 = ___other0;
		bool L_2 = Object_ReferenceEquals_m3900584722(NULL /*static, unused*/, (Il2CppObject *)__this, (Il2CppObject *)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		return (bool)1;
	}

IL_0016:
	{
		int32_t L_3 = (int32_t)__this->get__count_2();
		List_1_t61287617 * L_4 = ___other0;
		NullCheck((List_1_t61287617 *)L_4);
		int32_t L_5 = ((  int32_t (*) (List_1_t61287617 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t61287617 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if ((((int32_t)L_3) == ((int32_t)L_5)))
		{
			goto IL_0029;
		}
	}
	{
		return (bool)0;
	}

IL_0029:
	{
		V_0 = (int32_t)0;
		goto IL_005d;
	}

IL_0030:
	{
		ObjectU5BU5D_t3614634134* L_6 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		List_1_t61287617 * L_10 = ___other0;
		int32_t L_11 = V_0;
		NullCheck((List_1_t61287617 *)L_10);
		Il2CppObject * L_12 = ((  Il2CppObject * (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t61287617 *)L_10, (int32_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		bool L_13 = RuntimeServices_EqualityOperator_m2233200645(NULL /*static, unused*/, (Il2CppObject *)L_9, (Il2CppObject *)L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0059;
		}
	}
	{
		return (bool)0;
	}

IL_0059:
	{
		int32_t L_14 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_005d:
	{
		int32_t L_15 = V_0;
		int32_t L_16 = (int32_t)__this->get__count_2();
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0030;
		}
	}
	{
		return (bool)1;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::Clear()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t List_1_Clear_m2512712227_MetadataUsageId;
extern "C"  void List_1_Clear_m2512712227_gshared (List_1_t61287617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_Clear_m2512712227_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = (int32_t)0;
		goto IL_0020;
	}

IL_0007:
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_1 = V_0;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_2 = V_1;
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (Il2CppObject *)L_2);
		int32_t L_3 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_4 = V_0;
		int32_t L_5 = (int32_t)__this->get__count_2();
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_0007;
		}
	}
	{
		__this->set__count_2(0);
		return;
	}
}
// System.Boolean Boo.Lang.List`1<System.Object>::Contains(T)
extern "C"  bool List_1_Contains_m872023821_gshared (List_1_t61287617 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___item0;
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t61287617 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t61287617 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return (bool)((((int32_t)((((int32_t)(-1)) == ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 Boo.Lang.List`1<System.Object>::IndexOf(T)
extern Il2CppClass* RuntimeServices_t1910041954_il2cpp_TypeInfo_var;
extern const uint32_t List_1_IndexOf_m131628331_MetadataUsageId;
extern "C"  int32_t List_1_IndexOf_m131628331_gshared (List_1_t61287617 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_IndexOf_m131628331_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_002e;
	}

IL_0007:
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		Il2CppObject * L_4 = ___item0;
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		bool L_5 = RuntimeServices_EqualityOperator_m2233200645(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_6 = V_0;
		return L_6;
	}

IL_002a:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get__count_2();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0007;
		}
	}
	{
		return (-1);
	}
}
// Boo.Lang.List`1<T> Boo.Lang.List`1<System.Object>::Insert(System.Int32,T)
extern "C"  List_1_t61287617 * List_1_Insert_m1948090966_gshared (List_1_t61287617 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (int32_t)L_1;
		int32_t L_2 = (int32_t)__this->get__count_2();
		int32_t L_3 = V_0;
		int32_t L_4 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		NullCheck((List_1_t61287617 *)__this);
		((  void (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((List_1_t61287617 *)__this, (int32_t)((int32_t)((int32_t)L_4+(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		int32_t L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get__count_2();
		if ((((int32_t)L_5) >= ((int32_t)L_6)))
		{
			goto IL_0045;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_8 = V_0;
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_10 = V_0;
		int32_t L_11 = (int32_t)__this->get__count_2();
		int32_t L_12 = V_0;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)L_8, (Il2CppArray *)(Il2CppArray *)L_9, (int32_t)((int32_t)((int32_t)L_10+(int32_t)1)), (int32_t)((int32_t)((int32_t)L_11-(int32_t)L_12)), /*hidden argument*/NULL);
	}

IL_0045:
	{
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_14 = V_0;
		Il2CppObject * L_15 = ___item1;
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Il2CppObject *)L_15);
		int32_t L_16 = (int32_t)__this->get__count_2();
		__this->set__count_2(((int32_t)((int32_t)L_16+(int32_t)1)));
		return __this;
	}
}
// Boo.Lang.List`1<T> Boo.Lang.List`1<System.Object>::Remove(T)
extern "C"  List_1_t61287617 * List_1_Remove_m3021996592_gshared (List_1_t61287617 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___item0;
		NullCheck((List_1_t61287617 *)__this);
		((  bool (*) (List_1_t61287617 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t61287617 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return __this;
	}
}
// Boo.Lang.List`1<T> Boo.Lang.List`1<System.Object>::RemoveAt(System.Int32)
extern "C"  List_1_t61287617 * List_1_RemoveAt_m3714963554_gshared (List_1_t61287617 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_2 = ((  int32_t (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		NullCheck((List_1_t61287617 *)__this);
		((  void (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return __this;
	}
}
// System.Void Boo.Lang.List`1<System.Object>::EnsureCapacity(System.Int32)
extern "C"  void List_1_EnsureCapacity_m3110424457_gshared (List_1_t61287617 * __this, int32_t ___minCapacity0, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		int32_t L_0 = ___minCapacity0;
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		NullCheck(L_1);
		if ((((int32_t)L_0) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		int32_t L_2 = ___minCapacity0;
		NullCheck((List_1_t61287617 *)__this);
		ObjectU5BU5D_t3614634134* L_3 = ((  ObjectU5BU5D_t3614634134* (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		V_0 = (ObjectU5BU5D_t3614634134*)L_3;
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		ObjectU5BU5D_t3614634134* L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get__count_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)0, (int32_t)L_6, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_7 = V_0;
		__this->set__items_1(L_7);
	}

IL_0031:
	{
		return;
	}
}
// T[] Boo.Lang.List`1<System.Object>::NewArray(System.Int32)
extern "C"  ObjectU5BU5D_t3614634134* List_1_NewArray_m742551263_gshared (List_1_t61287617 * __this, int32_t ___minCapacity0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		NullCheck(L_0);
		int32_t L_1 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)1, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))), /*hidden argument*/NULL);
		V_0 = (int32_t)((int32_t)((int32_t)L_1*(int32_t)2));
		int32_t L_2 = V_0;
		int32_t L_3 = ___minCapacity0;
		int32_t L_4 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return ((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), (uint32_t)L_4));
	}
}
// System.Void Boo.Lang.List`1<System.Object>::InnerRemoveAt(System.Int32)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t List_1_InnerRemoveAt_m467546350_MetadataUsageId;
extern "C"  void List_1_InnerRemoveAt_m467546350_gshared (List_1_t61287617 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_InnerRemoveAt_m467546350_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		__this->set__count_2(((int32_t)((int32_t)L_0-(int32_t)1)));
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_2 = ___index0;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_3 = V_0;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Il2CppObject *)L_3);
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get__count_2();
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_004c;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_6 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_7 = ___index0;
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_9 = ___index0;
		int32_t L_10 = (int32_t)__this->get__count_2();
		int32_t L_11 = ___index0;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)((int32_t)((int32_t)L_7+(int32_t)1)), (Il2CppArray *)(Il2CppArray *)L_8, (int32_t)L_9, (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_004c:
	{
		return;
	}
}
// System.Boolean Boo.Lang.List`1<System.Object>::InnerRemove(T)
extern "C"  bool List_1_InnerRemove_m845891310_gshared (List_1_t61287617 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = ___item0;
		NullCheck((List_1_t61287617 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t61287617 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t61287617 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t61287617 *)__this);
		((  void (*) (List_1_t61287617 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t61287617 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return (bool)1;
	}

IL_0018:
	{
		return (bool)0;
	}
}
// System.Int32 Boo.Lang.List`1<System.Object>::CheckIndex(System.Int32)
extern Il2CppClass* IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var;
extern const uint32_t List_1_CheckIndex_m2149663805_MetadataUsageId;
extern "C"  int32_t List_1_CheckIndex_m2149663805_gshared (List_1_t61287617 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m2149663805_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__count_2();
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0012;
		}
	}
	{
		IndexOutOfRangeException_t3527622107 * L_2 = (IndexOutOfRangeException_t3527622107 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m3497760912(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		int32_t L_3 = ___index0;
		return L_3;
	}
}
// System.Int32 Boo.Lang.List`1<System.Object>::NormalizeIndex(System.Int32)
extern "C"  int32_t List_1_NormalizeIndex_m2608178430_gshared (List_1_t61287617 * __this, int32_t ___index0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__count_2();
		G_B3_0 = ((int32_t)((int32_t)L_1+(int32_t)L_2));
		goto IL_0015;
	}

IL_0014:
	{
		int32_t L_3 = ___index0;
		G_B3_0 = L_3;
	}

IL_0015:
	{
		return G_B3_0;
	}
}
// T Boo.Lang.List`1<System.Object>::Coerce(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimeServices_t1910041954_il2cpp_TypeInfo_var;
extern const uint32_t List_1_Coerce_m3843713328_MetadataUsageId;
extern "C"  Il2CppObject * List_1_Coerce_m3843713328_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_Coerce_m3843713328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		if (!((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9))))
		{
			goto IL_0012;
		}
	}
	{
		Il2CppObject * L_1 = ___value0;
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)));
	}

IL_0012:
	{
		Il2CppObject * L_2 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t1910041954_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = RuntimeServices_Coerce_m43784504(NULL /*static, unused*/, (Il2CppObject *)L_2, (Type_t *)L_3, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)));
	}
}
// System.Void Firebase.Database.Internal.Collection.ArraySortedMap`2/Enumerator134<System.Object,System.Object>::.ctor(Firebase.Database.Internal.Collection.ArraySortedMap`2<TK,TV>,System.Int32,System.Boolean)
extern "C"  void Enumerator134__ctor_m2329147759_gshared (Enumerator134_t1479555021 * __this, ArraySortedMap_2_t3398066313 * ___enclosing0, int32_t ___pos1, bool ___reverse2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		ArraySortedMap_2_t3398066313 * L_0 = ___enclosing0;
		__this->set__enclosing_0(L_0);
		bool L_1 = ___reverse2;
		__this->set__reverse_1(L_1);
		int32_t L_2 = ___pos1;
		__this->set__currentPos_2(L_2);
		return;
	}
}
// System.Boolean Firebase.Database.Internal.Collection.ArraySortedMap`2/Enumerator134<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator134_MoveNext_m3794421025_gshared (Enumerator134_t1479555021 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	{
		bool L_0 = (bool)__this->get__reverse_1();
		if (!L_0)
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get__currentPos_2();
		if ((((int32_t)L_1) < ((int32_t)0)))
		{
			goto IL_0062;
		}
	}
	{
		ArraySortedMap_2_t3398066313 * L_2 = (ArraySortedMap_2_t3398066313 *)__this->get__enclosing_0();
		NullCheck(L_2);
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)L_2->get__keys_1();
		int32_t L_4 = (int32_t)__this->get__currentPos_2();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_0 = (Il2CppObject *)L_6;
		ArraySortedMap_2_t3398066313 * L_7 = (ArraySortedMap_2_t3398066313 *)__this->get__enclosing_0();
		NullCheck(L_7);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7->get__values_2();
		int32_t L_9 = (int32_t)__this->get__currentPos_2();
		NullCheck(L_8);
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_1 = (Il2CppObject *)L_11;
		Il2CppObject * L_12 = V_0;
		Il2CppObject * L_13 = V_1;
		KeyValuePair_2_t38854645  L_14;
		memset(&L_14, 0, sizeof(L_14));
		KeyValuePair_2__ctor_m1640124561(&L_14, (Il2CppObject *)L_12, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		NullCheck((Enumerator134_t1479555021 *)__this);
		((  void (*) (Enumerator134_t1479555021 *, KeyValuePair_2_t38854645 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Enumerator134_t1479555021 *)__this, (KeyValuePair_2_t38854645 )L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		int32_t L_15 = (int32_t)__this->get__currentPos_2();
		__this->set__currentPos_2(((int32_t)((int32_t)L_15-(int32_t)1)));
		return (bool)1;
	}

IL_0062:
	{
		bool L_16 = (bool)__this->get__reverse_1();
		if (L_16)
		{
			goto IL_00d0;
		}
	}
	{
		int32_t L_17 = (int32_t)__this->get__currentPos_2();
		ArraySortedMap_2_t3398066313 * L_18 = (ArraySortedMap_2_t3398066313 *)__this->get__enclosing_0();
		NullCheck(L_18);
		ObjectU5BU5D_t3614634134* L_19 = (ObjectU5BU5D_t3614634134*)L_18->get__keys_1();
		NullCheck(L_19);
		if ((((int32_t)L_17) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))))))
		{
			goto IL_00d0;
		}
	}
	{
		ArraySortedMap_2_t3398066313 * L_20 = (ArraySortedMap_2_t3398066313 *)__this->get__enclosing_0();
		NullCheck(L_20);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_20->get__keys_1();
		int32_t L_22 = (int32_t)__this->get__currentPos_2();
		NullCheck(L_21);
		int32_t L_23 = L_22;
		Il2CppObject * L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		V_2 = (Il2CppObject *)L_24;
		ArraySortedMap_2_t3398066313 * L_25 = (ArraySortedMap_2_t3398066313 *)__this->get__enclosing_0();
		NullCheck(L_25);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_25->get__values_2();
		int32_t L_27 = (int32_t)__this->get__currentPos_2();
		NullCheck(L_26);
		int32_t L_28 = L_27;
		Il2CppObject * L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		V_3 = (Il2CppObject *)L_29;
		Il2CppObject * L_30 = V_2;
		Il2CppObject * L_31 = V_3;
		KeyValuePair_2_t38854645  L_32;
		memset(&L_32, 0, sizeof(L_32));
		KeyValuePair_2__ctor_m1640124561(&L_32, (Il2CppObject *)L_30, (Il2CppObject *)L_31, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		NullCheck((Enumerator134_t1479555021 *)__this);
		((  void (*) (Enumerator134_t1479555021 *, KeyValuePair_2_t38854645 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Enumerator134_t1479555021 *)__this, (KeyValuePair_2_t38854645 )L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		int32_t L_33 = (int32_t)__this->get__currentPos_2();
		__this->set__currentPos_2(((int32_t)((int32_t)L_33+(int32_t)1)));
		return (bool)1;
	}

IL_00d0:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.KeyValuePair`2<TK,TV> Firebase.Database.Internal.Collection.ArraySortedMap`2/Enumerator134<System.Object,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t38854645  Enumerator134_get_Current_m751914458_gshared (Enumerator134_t1479555021 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = (KeyValuePair_2_t38854645 )__this->get_U3CCurrentU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void Firebase.Database.Internal.Collection.ArraySortedMap`2/Enumerator134<System.Object,System.Object>::set_Current(System.Collections.Generic.KeyValuePair`2<TK,TV>)
extern "C"  void Enumerator134_set_Current_m627319475_gshared (Enumerator134_t1479555021 * __this, KeyValuePair_2_t38854645  ___value0, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = ___value0;
		__this->set_U3CCurrentU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Object Firebase.Database.Internal.Collection.ArraySortedMap`2/Enumerator134<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator134_System_Collections_IEnumerator_get_Current_m3695877741_gshared (Enumerator134_t1479555021 * __this, const MethodInfo* method)
{
	{
		NullCheck((Enumerator134_t1479555021 *)__this);
		KeyValuePair_2_t38854645  L_0 = ((  KeyValuePair_2_t38854645  (*) (Enumerator134_t1479555021 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Enumerator134_t1479555021 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		KeyValuePair_2_t38854645  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Void Firebase.Database.Internal.Collection.ArraySortedMap`2/Enumerator134<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator134_Dispose_m3049192278_gshared (Enumerator134_t1479555021 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Firebase.Database.Internal.Collection.ArraySortedMap`2/Enumerator134<System.Object,System.Object>::Reset()
extern "C"  void Enumerator134_Reset_m2928477348_gshared (Enumerator134_t1479555021 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IComparer`1<TK>)
extern "C"  void ArraySortedMap_2__ctor_m1213294893_gshared (ArraySortedMap_2_t3398066313 * __this, Il2CppObject* ___comparator0, const MethodInfo* method)
{
	{
		NullCheck((ImmutableSortedMap_2_t3217094540 *)__this);
		((  void (*) (ImmutableSortedMap_2_t3217094540 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ImmutableSortedMap_2_t3217094540 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set__keys_1(((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (uint32_t)0)));
		__this->set__values_2(((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (uint32_t)0)));
		Il2CppObject* L_0 = ___comparator0;
		__this->set__comparator_0(L_0);
		return;
	}
}
// System.Void Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IComparer`1<TK>,TK[],TV[])
extern "C"  void ArraySortedMap_2__ctor_m459065254_gshared (ArraySortedMap_2_t3398066313 * __this, Il2CppObject* ___comparator0, ObjectU5BU5D_t3614634134* ___keys1, ObjectU5BU5D_t3614634134* ___values2, const MethodInfo* method)
{
	{
		NullCheck((ImmutableSortedMap_2_t3217094540 *)__this);
		((  void (*) (ImmutableSortedMap_2_t3217094540 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ImmutableSortedMap_2_t3217094540 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3614634134* L_0 = ___keys1;
		__this->set__keys_1(L_0);
		ObjectU5BU5D_t3614634134* L_1 = ___values2;
		__this->set__values_2(L_1);
		Il2CppObject* L_2 = ___comparator0;
		__this->set__comparator_0(L_2);
		return;
	}
}
// Firebase.Database.Internal.Collection.ArraySortedMap`2<TK,TV> Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::FromMap(System.Collections.Generic.IDictionary`2<TK,TV>,System.Collections.Generic.IComparer`1<TK>)
extern "C"  ArraySortedMap_2_t3398066313 * ArraySortedMap_2_FromMap_m850096373_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___map0, Il2CppObject* ___comparator1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___map0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.ICollection`1<!0> System.Collections.Generic.IDictionary`2<System.Object,System.Object>::get_Keys() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0);
		List_1_t2058570427 * L_2 = (List_1_t2058570427 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (List_1_t2058570427 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_2, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Il2CppObject* L_3 = ___map0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Il2CppObject* L_4 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		Il2CppObject* L_5 = ___comparator1;
		ArraySortedMap_2_t3398066313 * L_6 = ((  ArraySortedMap_2_t3398066313 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, Il2CppObject*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_2, (Il2CppObject*)L_3, (Il2CppObject*)L_4, (Il2CppObject*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return L_6;
	}
}
// System.Boolean Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::ContainsKey(TK)
extern "C"  bool ArraySortedMap_2_ContainsKey_m4035413709_gshared (ArraySortedMap_2_t3398066313 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((ArraySortedMap_2_t3398066313 *)__this);
		int32_t L_1 = ((  int32_t (*) (ArraySortedMap_2_t3398066313 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((ArraySortedMap_2_t3398066313 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return (bool)((((int32_t)((((int32_t)L_1) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// TV Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::Get(TK)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ArraySortedMap_2_Get_m3150758520_MetadataUsageId;
extern "C"  Il2CppObject * ArraySortedMap_2_Get_m3150758520_gshared (ArraySortedMap_2_t3398066313 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArraySortedMap_2_Get_m3150758520_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((ArraySortedMap_2_t3398066313 *)__this);
		int32_t L_1 = ((  int32_t (*) (ArraySortedMap_2_t3398066313 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((ArraySortedMap_2_t3398066313 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0020;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)__this->get__values_2();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		G_B3_0 = L_6;
		goto IL_0029;
	}

IL_0020:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_7 = V_1;
		G_B3_0 = L_7;
	}

IL_0029:
	{
		return G_B3_0;
	}
}
// Firebase.Database.Internal.Collection.ImmutableSortedMap`2<TK,TV> Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::Remove(TK)
extern "C"  ImmutableSortedMap_2_t3217094540 * ArraySortedMap_2_Remove_m3965377617_gshared (ArraySortedMap_2_t3398066313 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	ObjectU5BU5D_t3614634134* V_1 = NULL;
	ObjectU5BU5D_t3614634134* V_2 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((ArraySortedMap_2_t3398066313 *)__this);
		int32_t L_1 = ((  int32_t (*) (ArraySortedMap_2_t3398066313 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((ArraySortedMap_2_t3398066313 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0011;
		}
	}
	{
		return __this;
	}

IL_0011:
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		int32_t L_4 = V_0;
		ObjectU5BU5D_t3614634134* L_5 = ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		V_1 = (ObjectU5BU5D_t3614634134*)L_5;
		ObjectU5BU5D_t3614634134* L_6 = (ObjectU5BU5D_t3614634134*)__this->get__values_2();
		int32_t L_7 = V_0;
		ObjectU5BU5D_t3614634134* L_8 = ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		V_2 = (ObjectU5BU5D_t3614634134*)L_8;
		Il2CppObject* L_9 = (Il2CppObject*)__this->get__comparator_0();
		ObjectU5BU5D_t3614634134* L_10 = V_1;
		ObjectU5BU5D_t3614634134* L_11 = V_2;
		ArraySortedMap_2_t3398066313 * L_12 = (ArraySortedMap_2_t3398066313 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14));
		((  void (*) (ArraySortedMap_2_t3398066313 *, Il2CppObject*, ObjectU5BU5D_t3614634134*, ObjectU5BU5D_t3614634134*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)(L_12, (Il2CppObject*)L_9, (ObjectU5BU5D_t3614634134*)L_10, (ObjectU5BU5D_t3614634134*)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return L_12;
	}
}
// Firebase.Database.Internal.Collection.ImmutableSortedMap`2<TK,TV> Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::Insert(TK,TV)
extern "C"  ImmutableSortedMap_2_t3217094540 * ArraySortedMap_2_Insert_m797993744_gshared (ArraySortedMap_2_t3398066313 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	ObjectU5BU5D_t3614634134* V_1 = NULL;
	ObjectU5BU5D_t3614634134* V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	ObjectU5BU5D_t3614634134* V_6 = NULL;
	ObjectU5BU5D_t3614634134* V_7 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((ArraySortedMap_2_t3398066313 *)__this);
		int32_t L_1 = ((  int32_t (*) (ArraySortedMap_2_t3398066313 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((ArraySortedMap_2_t3398066313 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_007d;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Il2CppObject * L_7 = ___key0;
		bool L_8 = Object_Equals_m969736273(NULL /*static, unused*/, (Il2CppObject *)L_6, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0053;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)__this->get__values_2();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Il2CppObject * L_13 = ___value1;
		bool L_14 = Object_Equals_m969736273(NULL /*static, unused*/, (Il2CppObject *)L_12, (Il2CppObject *)L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0053;
		}
	}
	{
		return __this;
	}

IL_0053:
	{
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		int32_t L_16 = V_0;
		Il2CppObject * L_17 = ___key0;
		ObjectU5BU5D_t3614634134* L_18 = ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_15, (int32_t)L_16, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		V_1 = (ObjectU5BU5D_t3614634134*)L_18;
		ObjectU5BU5D_t3614634134* L_19 = (ObjectU5BU5D_t3614634134*)__this->get__values_2();
		int32_t L_20 = V_0;
		Il2CppObject * L_21 = ___value1;
		ObjectU5BU5D_t3614634134* L_22 = ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_19, (int32_t)L_20, (Il2CppObject *)L_21, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		V_2 = (ObjectU5BU5D_t3614634134*)L_22;
		Il2CppObject* L_23 = (Il2CppObject*)__this->get__comparator_0();
		ObjectU5BU5D_t3614634134* L_24 = V_1;
		ObjectU5BU5D_t3614634134* L_25 = V_2;
		ArraySortedMap_2_t3398066313 * L_26 = (ArraySortedMap_2_t3398066313 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14));
		((  void (*) (ArraySortedMap_2_t3398066313 *, Il2CppObject*, ObjectU5BU5D_t3614634134*, ObjectU5BU5D_t3614634134*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)(L_26, (Il2CppObject*)L_23, (ObjectU5BU5D_t3614634134*)L_24, (ObjectU5BU5D_t3614634134*)L_25, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return L_26;
	}

IL_007d:
	{
		ObjectU5BU5D_t3614634134* L_27 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		NullCheck(L_27);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_27)->max_length))))) <= ((int32_t)((int32_t)25))))
		{
			goto IL_00ee;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_28 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		NullCheck(L_28);
		Dictionary_2_t2281509423 * L_29 = (Dictionary_2_t2281509423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20));
		((  void (*) (Dictionary_2_t2281509423 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)(L_29, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length))))+(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		V_3 = (Il2CppObject*)L_29;
		V_4 = (int32_t)0;
		goto IL_00ca;
	}

IL_00a4:
	{
		Il2CppObject* L_30 = V_3;
		ObjectU5BU5D_t3614634134* L_31 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		int32_t L_32 = V_4;
		NullCheck(L_31);
		int32_t L_33 = L_32;
		Il2CppObject * L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		ObjectU5BU5D_t3614634134* L_35 = (ObjectU5BU5D_t3614634134*)__this->get__values_2();
		int32_t L_36 = V_4;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		Il2CppObject * L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck((Il2CppObject*)L_30);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(4 /* System.Void System.Collections.Generic.IDictionary`2<System.Object,System.Object>::set_Item(!0,!1) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_30, (Il2CppObject *)L_34, (Il2CppObject *)L_38);
		int32_t L_39 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_00ca:
	{
		int32_t L_40 = V_4;
		ObjectU5BU5D_t3614634134* L_41 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		NullCheck(L_41);
		if ((((int32_t)L_40) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_41)->max_length)))))))
		{
			goto IL_00a4;
		}
	}
	{
		Il2CppObject* L_42 = V_3;
		Il2CppObject * L_43 = ___key0;
		Il2CppObject * L_44 = ___value1;
		NullCheck((Il2CppObject*)L_42);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(4 /* System.Void System.Collections.Generic.IDictionary`2<System.Object,System.Object>::set_Item(!0,!1) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_42, (Il2CppObject *)L_43, (Il2CppObject *)L_44);
		Il2CppObject* L_45 = V_3;
		Il2CppObject* L_46 = (Il2CppObject*)__this->get__comparator_0();
		RbTreeSortedMap_2_t791914602 * L_47 = ((  RbTreeSortedMap_2_t791914602 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_45, (Il2CppObject*)L_46, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		return L_47;
	}

IL_00ee:
	{
		Il2CppObject * L_48 = ___key0;
		NullCheck((ArraySortedMap_2_t3398066313 *)__this);
		int32_t L_49 = ((  int32_t (*) (ArraySortedMap_2_t3398066313 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((ArraySortedMap_2_t3398066313 *)__this, (Il2CppObject *)L_48, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		V_5 = (int32_t)L_49;
		ObjectU5BU5D_t3614634134* L_50 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		int32_t L_51 = V_5;
		Il2CppObject * L_52 = ___key0;
		ObjectU5BU5D_t3614634134* L_53 = ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_50, (int32_t)L_51, (Il2CppObject *)L_52, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		V_6 = (ObjectU5BU5D_t3614634134*)L_53;
		ObjectU5BU5D_t3614634134* L_54 = (ObjectU5BU5D_t3614634134*)__this->get__values_2();
		int32_t L_55 = V_5;
		Il2CppObject * L_56 = ___value1;
		ObjectU5BU5D_t3614634134* L_57 = ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_54, (int32_t)L_55, (Il2CppObject *)L_56, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		V_7 = (ObjectU5BU5D_t3614634134*)L_57;
		Il2CppObject* L_58 = (Il2CppObject*)__this->get__comparator_0();
		ObjectU5BU5D_t3614634134* L_59 = V_6;
		ObjectU5BU5D_t3614634134* L_60 = V_7;
		ArraySortedMap_2_t3398066313 * L_61 = (ArraySortedMap_2_t3398066313 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14));
		((  void (*) (ArraySortedMap_2_t3398066313 *, Il2CppObject*, ObjectU5BU5D_t3614634134*, ObjectU5BU5D_t3614634134*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)(L_61, (Il2CppObject*)L_58, (ObjectU5BU5D_t3614634134*)L_59, (ObjectU5BU5D_t3614634134*)L_60, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return L_61;
	}
}
// TK Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::GetMinKey()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ArraySortedMap_2_GetMinKey_m3154603777_MetadataUsageId;
extern "C"  Il2CppObject * ArraySortedMap_2_GetMinKey_m3154603777_gshared (ArraySortedMap_2_t3398066313 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArraySortedMap_2_GetMinKey_m3154603777_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_001f;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		NullCheck(L_1);
		int32_t L_2 = 0;
		Il2CppObject * L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		G_B3_0 = L_3;
		goto IL_0028;
	}

IL_001f:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_4 = V_0;
		G_B3_0 = L_4;
	}

IL_0028:
	{
		return G_B3_0;
	}
}
// TK Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::GetMaxKey()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ArraySortedMap_2_GetMaxKey_m2926417175_MetadataUsageId;
extern "C"  Il2CppObject * ArraySortedMap_2_GetMaxKey_m2926417175_gshared (ArraySortedMap_2_t3398066313 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArraySortedMap_2_GetMaxKey_m2926417175_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0028;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		NullCheck(L_2);
		NullCheck(L_1);
		int32_t L_3 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))-(int32_t)1));
		Il2CppObject * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		G_B3_0 = L_4;
		goto IL_0031;
	}

IL_0028:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_5 = V_0;
		G_B3_0 = L_5;
	}

IL_0031:
	{
		return G_B3_0;
	}
}
// System.Int32 Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::Size()
extern "C"  int32_t ArraySortedMap_2_Size_m2956848197_gshared (ArraySortedMap_2_t3398066313 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::IsEmpty()
extern "C"  bool ArraySortedMap_2_IsEmpty_m2644371141_gshared (ArraySortedMap_2_t3398066313 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		NullCheck(L_0);
		return (bool)((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::InOrderTraversal(Firebase.Database.Internal.Collection.LlrbNode`2/NodeVisitor<TK,TV>)
extern "C"  void ArraySortedMap_2_InOrderTraversal_m2890703614_gshared (ArraySortedMap_2_t3398066313 * __this, NodeVisitor_t2944196615 * ___visitor0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_0029;
	}

IL_0007:
	{
		NodeVisitor_t2944196615 * L_0 = ___visitor0;
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		ObjectU5BU5D_t3614634134* L_5 = (ObjectU5BU5D_t3614634134*)__this->get__values_2();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck((NodeVisitor_t2944196615 *)L_0);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(4 /* System.Void Firebase.Database.Internal.Collection.LlrbNode`2/NodeVisitor<System.Object,System.Object>::VisitEntry(TK,TV) */, (NodeVisitor_t2944196615 *)L_0, (Il2CppObject *)L_4, (Il2CppObject *)L_8);
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0029:
	{
		int32_t L_10 = V_0;
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TK,TV>> Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::Iterator(System.Int32,System.Boolean)
extern "C"  Il2CppObject* ArraySortedMap_2_Iterator_m320963300_gshared (ArraySortedMap_2_t3398066313 * __this, int32_t ___pos0, bool ___reverse1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___pos0;
		bool L_1 = ___reverse1;
		Enumerator134_t1479555021 * L_2 = (Enumerator134_t1479555021 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 28));
		((  void (*) (Enumerator134_t1479555021 *, ArraySortedMap_2_t3398066313 *, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)(L_2, (ArraySortedMap_2_t3398066313 *)__this, (int32_t)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		return L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TK,TV>> Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ArraySortedMap_2_GetEnumerator_m780148810_gshared (ArraySortedMap_2_t3398066313 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArraySortedMap_2_t3398066313 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArraySortedMap_2_t3398066313 *, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((ArraySortedMap_2_t3398066313 *)__this, (int32_t)0, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TK,TV>> Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::ReverseIterator()
extern "C"  Il2CppObject* ArraySortedMap_2_ReverseIterator_m2182111192_gshared (ArraySortedMap_2_t3398066313 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		NullCheck(L_0);
		NullCheck((ArraySortedMap_2_t3398066313 *)__this);
		Il2CppObject* L_1 = ((  Il2CppObject* (*) (ArraySortedMap_2_t3398066313 *, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((ArraySortedMap_2_t3398066313 *)__this, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))-(int32_t)1)), (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		return L_1;
	}
}
// TK Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::GetPredecessorKey(TK)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2254513248;
extern const uint32_t ArraySortedMap_2_GetPredecessorKey_m371765065_MetadataUsageId;
extern "C"  Il2CppObject * ArraySortedMap_2_GetPredecessorKey_m371765065_gshared (ArraySortedMap_2_t3398066313 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArraySortedMap_2_GetPredecessorKey_m371765065_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B5_0 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((ArraySortedMap_2_t3398066313 *)__this);
		int32_t L_1 = ((  int32_t (*) (ArraySortedMap_2_t3398066313 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((ArraySortedMap_2_t3398066313 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_001a;
		}
	}
	{
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_3, (String_t*)_stringLiteral2254513248, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001a:
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_5 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = ((int32_t)((int32_t)L_6-(int32_t)1));
		Il2CppObject * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		G_B5_0 = L_8;
		goto IL_003d;
	}

IL_0034:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_9 = V_1;
		G_B5_0 = L_9;
	}

IL_003d:
	{
		return G_B5_0;
	}
}
// System.Collections.Generic.IComparer`1<TK> Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::GetComparator()
extern "C"  Il2CppObject* ArraySortedMap_2_GetComparator_m1626728834_gshared (ArraySortedMap_2_t3398066313 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__comparator_0();
		return L_0;
	}
}
// System.Int32 Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::FindKeyOrInsertPosition(TK)
extern "C"  int32_t ArraySortedMap_2_FindKeyOrInsertPosition_m2031308278_gshared (ArraySortedMap_2_t3398066313 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_000b;
	}

IL_0007:
	{
		int32_t L_0 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)1));
	}

IL_000b:
	{
		int32_t L_1 = V_0;
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_0037;
		}
	}
	{
		Il2CppObject* L_3 = (Il2CppObject*)__this->get__comparator_0();
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		Il2CppObject * L_8 = ___key0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_9 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Object>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 31), (Il2CppObject*)L_3, (Il2CppObject *)L_7, (Il2CppObject *)L_8);
		if ((((int32_t)L_9) < ((int32_t)0)))
		{
			goto IL_0007;
		}
	}

IL_0037:
	{
		int32_t L_10 = V_0;
		return L_10;
	}
}
// System.Int32 Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>::FindKey(TK)
extern "C"  int32_t ArraySortedMap_2_FindKey_m2334624529_gshared (ArraySortedMap_2_t3398066313 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	ObjectU5BU5D_t3614634134* V_2 = NULL;
	int32_t V_3 = 0;
	{
		V_0 = (int32_t)0;
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__keys_1();
		V_2 = (ObjectU5BU5D_t3614634134*)L_0;
		V_3 = (int32_t)0;
		goto IL_0034;
	}

IL_0010:
	{
		ObjectU5BU5D_t3614634134* L_1 = V_2;
		int32_t L_2 = V_3;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_1 = (Il2CppObject *)L_4;
		Il2CppObject* L_5 = (Il2CppObject*)__this->get__comparator_0();
		Il2CppObject * L_6 = ___key0;
		Il2CppObject * L_7 = V_1;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_8 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Object>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 31), (Il2CppObject*)L_5, (Il2CppObject *)L_6, (Il2CppObject *)L_7);
		if (L_8)
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_9 = V_0;
		return L_9;
	}

IL_002c:
	{
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
		int32_t L_11 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_12 = V_3;
		ObjectU5BU5D_t3614634134* L_13 = V_2;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		return (-1);
	}
}
// System.Void Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder/KeyTranslator109<System.Object,System.Object>::.ctor()
extern "C"  void KeyTranslator109__ctor_m3806668953_gshared (KeyTranslator109_t2828396717 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder/KeyTranslator109<System.Object,System.Object>::Translate(System.Object)
extern "C"  Il2CppObject * KeyTranslator109_Translate_m4195723552_gshared (KeyTranslator109_t2828396717 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		return L_0;
	}
}
// Firebase.Database.Internal.Collection.ImmutableSortedMap`2<TK,TV> Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder<System.Object,System.Object>::EmptyMap(System.Collections.Generic.IComparer`1<TK>)
extern "C"  ImmutableSortedMap_2_t3217094540 * Builder_EmptyMap_m3930589567_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___comparator0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___comparator0;
		ArraySortedMap_2_t3398066313 * L_1 = (ArraySortedMap_2_t3398066313 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (ArraySortedMap_2_t3398066313 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_1, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
// Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder/IKeyTranslator<TK,TV> Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder<System.Object,System.Object>::IdentityTranslator()
extern "C"  Il2CppObject* Builder_IdentityTranslator_m224979022_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Il2CppObject* L_0 = ((Builder_t1529578535_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_IdentityTranslatorImpl_0();
		return L_0;
	}
}
// System.Void Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder<System.Object,System.Object>::.cctor()
extern "C"  void Builder__cctor_m1907675492_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		KeyTranslator109_t2828396717 * L_0 = (KeyTranslator109_t2828396717 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (KeyTranslator109_t2828396717 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((Builder_t1529578535_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_IdentityTranslatorImpl_0(L_0);
		return;
	}
}
// System.Void Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::.ctor()
extern "C"  void ImmutableSortedMap_2__ctor_m2147415669_gshared (ImmutableSortedMap_2_t3217094540 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ImmutableSortedMap_2_System_Collections_IEnumerable_GetEnumerator_m1935600564_gshared (ImmutableSortedMap_2_t3217094540 * __this, const MethodInfo* method)
{
	{
		NullCheck((ImmutableSortedMap_2_t3217094540 *)__this);
		Il2CppObject* L_0 = VirtFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TK,TV>> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::GetEnumerator() */, (ImmutableSortedMap_2_t3217094540 *)__this);
		return L_0;
	}
}
// System.Boolean Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::Equals(System.Object)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern const uint32_t ImmutableSortedMap_2_Equals_m525750466_MetadataUsageId;
extern "C"  bool ImmutableSortedMap_2_Equals_m525750466_gshared (ImmutableSortedMap_2_t3217094540 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImmutableSortedMap_2_Equals_m525750466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ImmutableSortedMap_2_t3217094540 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	KeyValuePair_2_t38854645  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Il2CppObject * L_0 = ___o0;
		if ((!(((Il2CppObject*)(ImmutableSortedMap_2_t3217094540 *)__this) == ((Il2CppObject*)(Il2CppObject *)L_0))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_1 = ___o0;
		if (((ImmutableSortedMap_2_t3217094540 *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))))
		{
			goto IL_0016;
		}
	}
	{
		return (bool)0;
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___o0;
		V_0 = (ImmutableSortedMap_2_t3217094540 *)((ImmutableSortedMap_2_t3217094540 *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1)));
		NullCheck((ImmutableSortedMap_2_t3217094540 *)__this);
		Il2CppObject* L_3 = VirtFuncInvoker0< Il2CppObject* >::Invoke(18 /* System.Collections.Generic.IComparer`1<TK> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::GetComparator() */, (ImmutableSortedMap_2_t3217094540 *)__this);
		ImmutableSortedMap_2_t3217094540 * L_4 = V_0;
		NullCheck((ImmutableSortedMap_2_t3217094540 *)L_4);
		Il2CppObject* L_5 = VirtFuncInvoker0< Il2CppObject* >::Invoke(18 /* System.Collections.Generic.IComparer`1<TK> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::GetComparator() */, (ImmutableSortedMap_2_t3217094540 *)L_4);
		NullCheck((Il2CppObject *)L_3);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_3, (Il2CppObject *)L_5);
		if (L_6)
		{
			goto IL_0035;
		}
	}
	{
		return (bool)0;
	}

IL_0035:
	{
		NullCheck((ImmutableSortedMap_2_t3217094540 *)__this);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(13 /* System.Int32 Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::Size() */, (ImmutableSortedMap_2_t3217094540 *)__this);
		ImmutableSortedMap_2_t3217094540 * L_8 = V_0;
		NullCheck((ImmutableSortedMap_2_t3217094540 *)L_8);
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(13 /* System.Int32 Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::Size() */, (ImmutableSortedMap_2_t3217094540 *)L_8);
		if ((((int32_t)L_7) == ((int32_t)L_9)))
		{
			goto IL_0048;
		}
	}
	{
		return (bool)0;
	}

IL_0048:
	{
		NullCheck((ImmutableSortedMap_2_t3217094540 *)__this);
		Il2CppObject* L_10 = VirtFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TK,TV>> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::GetEnumerator() */, (ImmutableSortedMap_2_t3217094540 *)__this);
		V_1 = (Il2CppObject*)L_10;
		ImmutableSortedMap_2_t3217094540 * L_11 = V_0;
		NullCheck((ImmutableSortedMap_2_t3217094540 *)L_11);
		Il2CppObject* L_12 = VirtFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TK,TV>> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::GetEnumerator() */, (ImmutableSortedMap_2_t3217094540 *)L_11);
		V_2 = (Il2CppObject*)L_12;
		goto IL_008e;
	}

IL_005b:
	{
		Il2CppObject* L_13 = V_2;
		NullCheck((Il2CppObject *)L_13);
		bool L_14 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_13);
		if (L_14)
		{
			goto IL_0068;
		}
	}
	{
		return (bool)0;
	}

IL_0068:
	{
		Il2CppObject* L_15 = V_1;
		NullCheck((Il2CppObject*)L_15);
		KeyValuePair_2_t38854645  L_16 = InterfaceFuncInvoker0< KeyValuePair_2_t38854645  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_15);
		V_3 = (KeyValuePair_2_t38854645 )L_16;
		Il2CppObject* L_17 = V_2;
		NullCheck((Il2CppObject*)L_17);
		KeyValuePair_2_t38854645  L_18 = InterfaceFuncInvoker0< KeyValuePair_2_t38854645  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_17);
		KeyValuePair_2_t38854645  L_19 = L_18;
		Il2CppObject * L_20 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_19);
		Il2CppObject * L_21 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (&V_3));
		NullCheck((Il2CppObject *)L_21);
		bool L_22 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_21, (Il2CppObject *)L_20);
		if (L_22)
		{
			goto IL_008e;
		}
	}
	{
		return (bool)0;
	}

IL_008e:
	{
		Il2CppObject* L_23 = V_1;
		NullCheck((Il2CppObject *)L_23);
		bool L_24 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_23);
		if (L_24)
		{
			goto IL_005b;
		}
	}
	{
		return (bool)1;
	}
}
// System.Int32 Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::GetHashCode()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t ImmutableSortedMap_2_GetHashCode_m567452986_MetadataUsageId;
extern "C"  int32_t ImmutableSortedMap_2_GetHashCode_m567452986_gshared (ImmutableSortedMap_2_t3217094540 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImmutableSortedMap_2_GetHashCode_m567452986_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t38854645  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Il2CppObject* V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		NullCheck((ImmutableSortedMap_2_t3217094540 *)__this);
		Il2CppObject* L_0 = VirtFuncInvoker0< Il2CppObject* >::Invoke(18 /* System.Collections.Generic.IComparer`1<TK> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::GetComparator() */, (ImmutableSortedMap_2_t3217094540 *)__this);
		NullCheck((Il2CppObject *)L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_0);
		V_0 = (int32_t)L_1;
		NullCheck((ImmutableSortedMap_2_t3217094540 *)__this);
		Il2CppObject* L_2 = VirtFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TK,TV>> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::GetEnumerator() */, (ImmutableSortedMap_2_t3217094540 *)__this);
		V_2 = (Il2CppObject*)L_2;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0032;
		}

IL_0018:
		{
			Il2CppObject* L_3 = V_2;
			NullCheck((Il2CppObject*)L_3);
			KeyValuePair_2_t38854645  L_4 = InterfaceFuncInvoker0< KeyValuePair_2_t38854645  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_3);
			V_1 = (KeyValuePair_2_t38854645 )L_4;
			int32_t L_5 = V_0;
			Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (&V_1));
			NullCheck((Il2CppObject *)L_6);
			int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_6);
			V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)31)*(int32_t)L_5))+(int32_t)L_7));
		}

IL_0032:
		{
			Il2CppObject* L_8 = V_2;
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_0018;
			}
		}

IL_003d:
		{
			IL2CPP_LEAVE(0x4F, FINALLY_0042);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0042;
	}

FINALLY_0042:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_10 = V_2;
			if (!L_10)
			{
				goto IL_004e;
			}
		}

IL_0048:
		{
			Il2CppObject* L_11 = V_2;
			NullCheck((Il2CppObject *)L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
		}

IL_004e:
		{
			IL2CPP_END_FINALLY(66)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(66)
	{
		IL2CPP_JUMP_TBL(0x4F, IL_004f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_004f:
	{
		int32_t L_12 = V_0;
		return L_12;
	}
}
// System.String Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::ToString()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029399;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029318;
extern Il2CppCodeGenString* _stringLiteral905413823;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern Il2CppCodeGenString* _stringLiteral1308698414;
extern const uint32_t ImmutableSortedMap_2_ToString_m1520722834_MetadataUsageId;
extern "C"  String_t* ImmutableSortedMap_2_ToString_m1520722834_gshared (ImmutableSortedMap_2_t3217094540 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImmutableSortedMap_2_ToString_m1520722834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	bool V_1 = false;
	KeyValuePair_2_t38854645  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Il2CppObject* V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1221177846 *)L_0;
		StringBuilder_t1221177846 * L_1 = V_0;
		NullCheck((Il2CppObject *)__this);
		Type_t * L_2 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_2);
		NullCheck((StringBuilder_t1221177846 *)L_1);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_1, (String_t*)L_3, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_4 = V_0;
		NullCheck((StringBuilder_t1221177846 *)L_4);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_4, (String_t*)_stringLiteral372029399, /*hidden argument*/NULL);
		V_1 = (bool)1;
		NullCheck((ImmutableSortedMap_2_t3217094540 *)__this);
		Il2CppObject* L_5 = VirtFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TK,TV>> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::GetEnumerator() */, (ImmutableSortedMap_2_t3217094540 *)__this);
		V_3 = (Il2CppObject*)L_5;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_009c;
		}

IL_0032:
		{
			Il2CppObject* L_6 = V_3;
			NullCheck((Il2CppObject*)L_6);
			KeyValuePair_2_t38854645  L_7 = InterfaceFuncInvoker0< KeyValuePair_2_t38854645  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_6);
			V_2 = (KeyValuePair_2_t38854645 )L_7;
			bool L_8 = V_1;
			if (!L_8)
			{
				goto IL_0046;
			}
		}

IL_003f:
		{
			V_1 = (bool)0;
			goto IL_0052;
		}

IL_0046:
		{
			StringBuilder_t1221177846 * L_9 = V_0;
			NullCheck((StringBuilder_t1221177846 *)L_9);
			StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_9, (String_t*)_stringLiteral811305474, /*hidden argument*/NULL);
		}

IL_0052:
		{
			StringBuilder_t1221177846 * L_10 = V_0;
			NullCheck((StringBuilder_t1221177846 *)L_10);
			StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_10, (String_t*)_stringLiteral372029318, /*hidden argument*/NULL);
			StringBuilder_t1221177846 * L_11 = V_0;
			Il2CppObject * L_12 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			NullCheck((StringBuilder_t1221177846 *)L_11);
			StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_11, (Il2CppObject *)L_12, /*hidden argument*/NULL);
			StringBuilder_t1221177846 * L_13 = V_0;
			NullCheck((StringBuilder_t1221177846 *)L_13);
			StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_13, (String_t*)_stringLiteral905413823, /*hidden argument*/NULL);
			StringBuilder_t1221177846 * L_14 = V_0;
			Il2CppObject * L_15 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
			NullCheck((StringBuilder_t1221177846 *)L_14);
			StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_14, (Il2CppObject *)L_15, /*hidden argument*/NULL);
			StringBuilder_t1221177846 * L_16 = V_0;
			NullCheck((StringBuilder_t1221177846 *)L_16);
			StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_16, (String_t*)_stringLiteral372029317, /*hidden argument*/NULL);
		}

IL_009c:
		{
			Il2CppObject* L_17 = V_3;
			NullCheck((Il2CppObject *)L_17);
			bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_17);
			if (L_18)
			{
				goto IL_0032;
			}
		}

IL_00a7:
		{
			IL2CPP_LEAVE(0xB9, FINALLY_00ac);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00ac;
	}

FINALLY_00ac:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_19 = V_3;
			if (!L_19)
			{
				goto IL_00b8;
			}
		}

IL_00b2:
		{
			Il2CppObject* L_20 = V_3;
			NullCheck((Il2CppObject *)L_20);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_20);
		}

IL_00b8:
		{
			IL2CPP_END_FINALLY(172)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(172)
	{
		IL2CPP_JUMP_TBL(0xB9, IL_00b9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b9:
	{
		StringBuilder_t1221177846 * L_21 = V_0;
		NullCheck((StringBuilder_t1221177846 *)L_21);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_21, (String_t*)_stringLiteral1308698414, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_22 = V_0;
		NullCheck((Il2CppObject *)L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_22);
		return L_23;
	}
}
// System.Void Firebase.Database.Internal.Collection.ImmutableSortedMapIterator`2<System.Object,System.Object>::.ctor(Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,TK,System.Collections.Generic.IComparer`1<TK>,System.Boolean)
extern "C"  void ImmutableSortedMapIterator_2__ctor_m4098854814_gshared (ImmutableSortedMapIterator_2_t2384854130 * __this, LlrbNode_2_t4262869811 * ___root0, Il2CppObject * ___startKey1, Il2CppObject* ___comparator2, bool ___isReverse3, const MethodInfo* method)
{
	LlrbNode_2_t4262869811 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B5_0 = 0;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Stack_1_t2384585820 * L_0 = (Stack_1_t2384585820 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Stack_1_t2384585820 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set__nodeStack_1(L_0);
		bool L_1 = ___isReverse3;
		__this->set__isReverse_0(L_1);
		LlrbNode_2_t4262869811 * L_2 = ___root0;
		V_0 = (LlrbNode_2_t4262869811 *)L_2;
		goto IL_00c6;
	}

IL_0020:
	{
		Il2CppObject * L_3 = ___startKey1;
		if (!L_3)
		{
			goto IL_0057;
		}
	}
	{
		bool L_4 = ___isReverse3;
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		Il2CppObject* L_5 = ___comparator2;
		Il2CppObject * L_6 = ___startKey1;
		LlrbNode_2_t4262869811 * L_7 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_7);
		Il2CppObject * L_8 = VirtFuncInvoker0< Il2CppObject * >::Invoke(9 /* TK Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetKey() */, (LlrbNode_2_t4262869811 *)L_7);
		NullCheck((Il2CppObject*)L_5);
		int32_t L_9 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Object>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_5, (Il2CppObject *)L_6, (Il2CppObject *)L_8);
		G_B5_0 = L_9;
		goto IL_0051;
	}

IL_0044:
	{
		Il2CppObject* L_10 = ___comparator2;
		LlrbNode_2_t4262869811 * L_11 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_11);
		Il2CppObject * L_12 = VirtFuncInvoker0< Il2CppObject * >::Invoke(9 /* TK Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetKey() */, (LlrbNode_2_t4262869811 *)L_11);
		Il2CppObject * L_13 = ___startKey1;
		NullCheck((Il2CppObject*)L_10);
		int32_t L_14 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Object>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_10, (Il2CppObject *)L_12, (Il2CppObject *)L_13);
		G_B5_0 = L_14;
	}

IL_0051:
	{
		V_1 = (int32_t)G_B5_0;
		goto IL_0059;
	}

IL_0057:
	{
		V_1 = (int32_t)1;
	}

IL_0059:
	{
		int32_t L_15 = V_1;
		if ((((int32_t)L_15) >= ((int32_t)0)))
		{
			goto IL_007f;
		}
	}
	{
		bool L_16 = ___isReverse3;
		if (!L_16)
		{
			goto IL_0073;
		}
	}
	{
		LlrbNode_2_t4262869811 * L_17 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_17);
		LlrbNode_2_t4262869811 * L_18 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(11 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetLeft() */, (LlrbNode_2_t4262869811 *)L_17);
		V_0 = (LlrbNode_2_t4262869811 *)L_18;
		goto IL_007a;
	}

IL_0073:
	{
		LlrbNode_2_t4262869811 * L_19 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_19);
		LlrbNode_2_t4262869811 * L_20 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(12 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetRight() */, (LlrbNode_2_t4262869811 *)L_19);
		V_0 = (LlrbNode_2_t4262869811 *)L_20;
	}

IL_007a:
	{
		goto IL_00c6;
	}

IL_007f:
	{
		int32_t L_21 = V_1;
		if (L_21)
		{
			goto IL_009b;
		}
	}
	{
		Stack_1_t2384585820 * L_22 = (Stack_1_t2384585820 *)__this->get__nodeStack_1();
		LlrbNode_2_t4262869811 * L_23 = V_0;
		NullCheck((Stack_1_t2384585820 *)L_22);
		((  void (*) (Stack_1_t2384585820 *, LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Stack_1_t2384585820 *)L_22, (LlrbValueNode_2_t1296857666 *)((LlrbValueNode_2_t1296857666 *)Castclass(L_23, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		goto IL_00d1;
	}

IL_009b:
	{
		Stack_1_t2384585820 * L_24 = (Stack_1_t2384585820 *)__this->get__nodeStack_1();
		LlrbNode_2_t4262869811 * L_25 = V_0;
		NullCheck((Stack_1_t2384585820 *)L_24);
		((  void (*) (Stack_1_t2384585820 *, LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Stack_1_t2384585820 *)L_24, (LlrbValueNode_2_t1296857666 *)((LlrbValueNode_2_t1296857666 *)Castclass(L_25, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		bool L_26 = ___isReverse3;
		if (!L_26)
		{
			goto IL_00bf;
		}
	}
	{
		LlrbNode_2_t4262869811 * L_27 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_27);
		LlrbNode_2_t4262869811 * L_28 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(12 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetRight() */, (LlrbNode_2_t4262869811 *)L_27);
		V_0 = (LlrbNode_2_t4262869811 *)L_28;
		goto IL_00c6;
	}

IL_00bf:
	{
		LlrbNode_2_t4262869811 * L_29 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_29);
		LlrbNode_2_t4262869811 * L_30 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(11 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetLeft() */, (LlrbNode_2_t4262869811 *)L_29);
		V_0 = (LlrbNode_2_t4262869811 *)L_30;
	}

IL_00c6:
	{
		LlrbNode_2_t4262869811 * L_31 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_31);
		bool L_32 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsEmpty() */, (LlrbNode_2_t4262869811 *)L_31);
		if (!L_32)
		{
			goto IL_0020;
		}
	}

IL_00d1:
	{
		return;
	}
}
// System.Boolean Firebase.Database.Internal.Collection.ImmutableSortedMapIterator`2<System.Object,System.Object>::MoveNext()
extern "C"  bool ImmutableSortedMapIterator_2_MoveNext_m314133173_gshared (ImmutableSortedMapIterator_2_t2384854130 * __this, const MethodInfo* method)
{
	LlrbValueNode_2_t1296857666 * V_0 = NULL;
	LlrbNode_2_t4262869811 * V_1 = NULL;
	LlrbNode_2_t4262869811 * V_2 = NULL;
	{
		Stack_1_t2384585820 * L_0 = (Stack_1_t2384585820 *)__this->get__nodeStack_1();
		NullCheck((Stack_1_t2384585820 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t2384585820 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Stack_1_t2384585820 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_00a4;
		}
	}
	{
		Stack_1_t2384585820 * L_2 = (Stack_1_t2384585820 *)__this->get__nodeStack_1();
		NullCheck((Stack_1_t2384585820 *)L_2);
		LlrbValueNode_2_t1296857666 * L_3 = ((  LlrbValueNode_2_t1296857666 * (*) (Stack_1_t2384585820 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((Stack_1_t2384585820 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (LlrbValueNode_2_t1296857666 *)L_3;
		LlrbValueNode_2_t1296857666 * L_4 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_4);
		Il2CppObject * L_5 = VirtFuncInvoker0< Il2CppObject * >::Invoke(9 /* TK Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetKey() */, (LlrbNode_2_t4262869811 *)L_4);
		LlrbValueNode_2_t1296857666 * L_6 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_6);
		Il2CppObject * L_7 = VirtFuncInvoker0< Il2CppObject * >::Invoke(10 /* TV Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetValue() */, (LlrbNode_2_t4262869811 *)L_6);
		KeyValuePair_2_t38854645  L_8;
		memset(&L_8, 0, sizeof(L_8));
		KeyValuePair_2__ctor_m1640124561(&L_8, (Il2CppObject *)L_5, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		NullCheck((ImmutableSortedMapIterator_2_t2384854130 *)__this);
		((  void (*) (ImmutableSortedMapIterator_2_t2384854130 *, KeyValuePair_2_t38854645 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((ImmutableSortedMapIterator_2_t2384854130 *)__this, (KeyValuePair_2_t38854645 )L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		bool L_9 = (bool)__this->get__isReverse_0();
		if (!L_9)
		{
			goto IL_0073;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_10 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_10);
		LlrbNode_2_t4262869811 * L_11 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(11 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetLeft() */, (LlrbNode_2_t4262869811 *)L_10);
		V_1 = (LlrbNode_2_t4262869811 *)L_11;
		goto IL_0063;
	}

IL_004b:
	{
		Stack_1_t2384585820 * L_12 = (Stack_1_t2384585820 *)__this->get__nodeStack_1();
		LlrbNode_2_t4262869811 * L_13 = V_1;
		NullCheck((Stack_1_t2384585820 *)L_12);
		((  void (*) (Stack_1_t2384585820 *, LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Stack_1_t2384585820 *)L_12, (LlrbValueNode_2_t1296857666 *)((LlrbValueNode_2_t1296857666 *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		LlrbNode_2_t4262869811 * L_14 = V_1;
		NullCheck((LlrbNode_2_t4262869811 *)L_14);
		LlrbNode_2_t4262869811 * L_15 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(12 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetRight() */, (LlrbNode_2_t4262869811 *)L_14);
		V_1 = (LlrbNode_2_t4262869811 *)L_15;
	}

IL_0063:
	{
		LlrbNode_2_t4262869811 * L_16 = V_1;
		NullCheck((LlrbNode_2_t4262869811 *)L_16);
		bool L_17 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsEmpty() */, (LlrbNode_2_t4262869811 *)L_16);
		if (!L_17)
		{
			goto IL_004b;
		}
	}
	{
		goto IL_00a2;
	}

IL_0073:
	{
		LlrbValueNode_2_t1296857666 * L_18 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_18);
		LlrbNode_2_t4262869811 * L_19 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(12 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetRight() */, (LlrbNode_2_t4262869811 *)L_18);
		V_2 = (LlrbNode_2_t4262869811 *)L_19;
		goto IL_0097;
	}

IL_007f:
	{
		Stack_1_t2384585820 * L_20 = (Stack_1_t2384585820 *)__this->get__nodeStack_1();
		LlrbNode_2_t4262869811 * L_21 = V_2;
		NullCheck((Stack_1_t2384585820 *)L_20);
		((  void (*) (Stack_1_t2384585820 *, LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Stack_1_t2384585820 *)L_20, (LlrbValueNode_2_t1296857666 *)((LlrbValueNode_2_t1296857666 *)Castclass(L_21, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		LlrbNode_2_t4262869811 * L_22 = V_2;
		NullCheck((LlrbNode_2_t4262869811 *)L_22);
		LlrbNode_2_t4262869811 * L_23 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(11 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetLeft() */, (LlrbNode_2_t4262869811 *)L_22);
		V_2 = (LlrbNode_2_t4262869811 *)L_23;
	}

IL_0097:
	{
		LlrbNode_2_t4262869811 * L_24 = V_2;
		NullCheck((LlrbNode_2_t4262869811 *)L_24);
		bool L_25 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsEmpty() */, (LlrbNode_2_t4262869811 *)L_24);
		if (!L_25)
		{
			goto IL_007f;
		}
	}

IL_00a2:
	{
		return (bool)1;
	}

IL_00a4:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.KeyValuePair`2<TK,TV> Firebase.Database.Internal.Collection.ImmutableSortedMapIterator`2<System.Object,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t38854645  ImmutableSortedMapIterator_2_get_Current_m2768638934_gshared (ImmutableSortedMapIterator_2_t2384854130 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = (KeyValuePair_2_t38854645 )__this->get_U3CCurrentU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void Firebase.Database.Internal.Collection.ImmutableSortedMapIterator`2<System.Object,System.Object>::set_Current(System.Collections.Generic.KeyValuePair`2<TK,TV>)
extern "C"  void ImmutableSortedMapIterator_2_set_Current_m307565391_gshared (ImmutableSortedMapIterator_2_t2384854130 * __this, KeyValuePair_2_t38854645  ___value0, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = ___value0;
		__this->set_U3CCurrentU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Object Firebase.Database.Internal.Collection.ImmutableSortedMapIterator`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * ImmutableSortedMapIterator_2_System_Collections_IEnumerator_get_Current_m1907867665_gshared (ImmutableSortedMapIterator_2_t2384854130 * __this, const MethodInfo* method)
{
	{
		NullCheck((ImmutableSortedMapIterator_2_t2384854130 *)__this);
		KeyValuePair_2_t38854645  L_0 = ((  KeyValuePair_2_t38854645  (*) (ImmutableSortedMapIterator_2_t2384854130 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((ImmutableSortedMapIterator_2_t2384854130 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		KeyValuePair_2_t38854645  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13), &L_1);
		return L_2;
	}
}
// System.Void Firebase.Database.Internal.Collection.ImmutableSortedMapIterator`2<System.Object,System.Object>::Dispose()
extern "C"  void ImmutableSortedMapIterator_2_Dispose_m1803640002_gshared (ImmutableSortedMapIterator_2_t2384854130 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Firebase.Database.Internal.Collection.ImmutableSortedMapIterator`2<System.Object,System.Object>::Reset()
extern "C"  void ImmutableSortedMapIterator_2_Reset_m3402692160_gshared (ImmutableSortedMapIterator_2_t2384854130 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Firebase.Database.Internal.Collection.ImmutableSortedSet`1/WrappedEntryIterator<System.Object>::.ctor(System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<T,System.Object>>)
extern "C"  void WrappedEntryIterator__ctor_m2946463310_gshared (WrappedEntryIterator_t3190181025 * __this, Il2CppObject* ___iterator0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___iterator0;
		__this->set__iterator_0(L_0);
		return;
	}
}
// System.Boolean Firebase.Database.Internal.Collection.ImmutableSortedSet`1/WrappedEntryIterator<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern const uint32_t WrappedEntryIterator_MoveNext_m1318140214_MetadataUsageId;
extern "C"  bool WrappedEntryIterator_MoveNext_m1318140214_gshared (WrappedEntryIterator_t3190181025 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WrappedEntryIterator_MoveNext_m1318140214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__iterator_0();
		NullCheck((Il2CppObject *)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// T Firebase.Database.Internal.Collection.ImmutableSortedSet`1/WrappedEntryIterator<System.Object>::get_Current()
extern "C"  Il2CppObject * WrappedEntryIterator_get_Current_m2584468257_gshared (WrappedEntryIterator_t3190181025 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__iterator_0();
		NullCheck((Il2CppObject*)L_0);
		KeyValuePair_2_t38854645  L_1 = InterfaceFuncInvoker0< KeyValuePair_2_t38854645  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_0);
		V_0 = (KeyValuePair_2_t38854645 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_2;
	}
}
// System.Object Firebase.Database.Internal.Collection.ImmutableSortedSet`1/WrappedEntryIterator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * WrappedEntryIterator_System_Collections_IEnumerator_get_Current_m3843867690_gshared (WrappedEntryIterator_t3190181025 * __this, const MethodInfo* method)
{
	{
		NullCheck((WrappedEntryIterator_t3190181025 *)__this);
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (WrappedEntryIterator_t3190181025 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((WrappedEntryIterator_t3190181025 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_0;
	}
}
// System.Void Firebase.Database.Internal.Collection.ImmutableSortedSet`1/WrappedEntryIterator<System.Object>::Dispose()
extern "C"  void WrappedEntryIterator_Dispose_m1881631145_gshared (WrappedEntryIterator_t3190181025 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Firebase.Database.Internal.Collection.ImmutableSortedSet`1/WrappedEntryIterator<System.Object>::Reset()
extern "C"  void WrappedEntryIterator_Reset_m2058005523_gshared (WrappedEntryIterator_t3190181025 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>::.ctor(System.Collections.Generic.IList`1<T>,System.Collections.Generic.IComparer`1<T>)
extern Il2CppClass* Collections_t4125780067_il2cpp_TypeInfo_var;
extern const uint32_t ImmutableSortedSet_1__ctor_m1945539485_MetadataUsageId;
extern "C"  void ImmutableSortedSet_1__ctor_m1945539485_gshared (ImmutableSortedSet_1_t2348759144 * __this, Il2CppObject* ___elems0, Il2CppObject* ___comparator1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImmutableSortedSet_1__ctor_m1945539485_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___elems0;
		IL2CPP_RUNTIME_CLASS_INIT(Collections_t4125780067_il2cpp_TypeInfo_var);
		Il2CppObject* L_1 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		Il2CppObject* L_2 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_3 = ___comparator1;
		ImmutableSortedMap_2_t3217094540 * L_4 = ((  ImmutableSortedMap_2_t3217094540 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, Il2CppObject*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_0, (Il2CppObject*)L_1, (Il2CppObject*)L_2, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		__this->set__map_0(L_4);
		return;
	}
}
// System.Void Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>::.ctor(Firebase.Database.Internal.Collection.ImmutableSortedMap`2<T,System.Object>)
extern "C"  void ImmutableSortedSet_1__ctor_m1960942718_gshared (ImmutableSortedSet_1_t2348759144 * __this, ImmutableSortedMap_2_t3217094540 * ___map0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		ImmutableSortedMap_2_t3217094540 * L_0 = ___map0;
		__this->set__map_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ImmutableSortedSet_1_GetEnumerator_m3228214528_gshared (ImmutableSortedSet_1_t2348759144 * __this, const MethodInfo* method)
{
	{
		ImmutableSortedMap_2_t3217094540 * L_0 = (ImmutableSortedMap_2_t3217094540 *)__this->get__map_0();
		NullCheck((ImmutableSortedMap_2_t3217094540 *)L_0);
		Il2CppObject* L_1 = VirtFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TK,TV>> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::GetEnumerator() */, (ImmutableSortedMap_2_t3217094540 *)L_0);
		WrappedEntryIterator_t3190181025 * L_2 = (WrappedEntryIterator_t3190181025 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		((  void (*) (WrappedEntryIterator_t3190181025 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_2, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Collections.IEnumerator Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ImmutableSortedSet_1_System_Collections_IEnumerable_GetEnumerator_m16219723_gshared (ImmutableSortedSet_1_t2348759144 * __this, const MethodInfo* method)
{
	{
		NullCheck((ImmutableSortedSet_1_t2348759144 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ImmutableSortedSet_1_t2348759144 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((ImmutableSortedSet_1_t2348759144 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return L_0;
	}
}
// Firebase.Database.Internal.Collection.ImmutableSortedSet`1<T> Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>::Remove(T)
extern "C"  ImmutableSortedSet_1_t2348759144 * ImmutableSortedSet_1_Remove_m3750122640_gshared (ImmutableSortedSet_1_t2348759144 * __this, Il2CppObject * ___entry0, const MethodInfo* method)
{
	ImmutableSortedMap_2_t3217094540 * V_0 = NULL;
	ImmutableSortedSet_1_t2348759144 * G_B3_0 = NULL;
	{
		ImmutableSortedMap_2_t3217094540 * L_0 = (ImmutableSortedMap_2_t3217094540 *)__this->get__map_0();
		Il2CppObject * L_1 = ___entry0;
		NullCheck((ImmutableSortedMap_2_t3217094540 *)L_0);
		ImmutableSortedMap_2_t3217094540 * L_2 = VirtFuncInvoker1< ImmutableSortedMap_2_t3217094540 *, Il2CppObject * >::Invoke(9 /* Firebase.Database.Internal.Collection.ImmutableSortedMap`2<TK,TV> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::Remove(TK) */, (ImmutableSortedMap_2_t3217094540 *)L_0, (Il2CppObject *)L_1);
		V_0 = (ImmutableSortedMap_2_t3217094540 *)L_2;
		ImmutableSortedMap_2_t3217094540 * L_3 = V_0;
		ImmutableSortedMap_2_t3217094540 * L_4 = (ImmutableSortedMap_2_t3217094540 *)__this->get__map_0();
		bool L_5 = Object_ReferenceEquals_m3900584722(NULL /*static, unused*/, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0024;
		}
	}
	{
		G_B3_0 = ((ImmutableSortedSet_1_t2348759144 *)(__this));
		goto IL_002a;
	}

IL_0024:
	{
		ImmutableSortedMap_2_t3217094540 * L_6 = V_0;
		ImmutableSortedSet_1_t2348759144 * L_7 = (ImmutableSortedSet_1_t2348759144 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9));
		((  void (*) (ImmutableSortedSet_1_t2348759144 *, ImmutableSortedMap_2_t3217094540 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)(L_7, (ImmutableSortedMap_2_t3217094540 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		G_B3_0 = ((ImmutableSortedSet_1_t2348759144 *)(L_7));
	}

IL_002a:
	{
		return G_B3_0;
	}
}
// Firebase.Database.Internal.Collection.ImmutableSortedSet`1<T> Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>::Insert(T)
extern "C"  ImmutableSortedSet_1_t2348759144 * ImmutableSortedSet_1_Insert_m3179399611_gshared (ImmutableSortedSet_1_t2348759144 * __this, Il2CppObject * ___entry0, const MethodInfo* method)
{
	{
		ImmutableSortedMap_2_t3217094540 * L_0 = (ImmutableSortedMap_2_t3217094540 *)__this->get__map_0();
		Il2CppObject * L_1 = ___entry0;
		NullCheck((ImmutableSortedMap_2_t3217094540 *)L_0);
		ImmutableSortedMap_2_t3217094540 * L_2 = VirtFuncInvoker2< ImmutableSortedMap_2_t3217094540 *, Il2CppObject *, Il2CppObject * >::Invoke(10 /* Firebase.Database.Internal.Collection.ImmutableSortedMap`2<TK,TV> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::Insert(TK,TV) */, (ImmutableSortedMap_2_t3217094540 *)L_0, (Il2CppObject *)L_1, (Il2CppObject *)NULL);
		ImmutableSortedSet_1_t2348759144 * L_3 = (ImmutableSortedSet_1_t2348759144 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9));
		((  void (*) (ImmutableSortedSet_1_t2348759144 *, ImmutableSortedMap_2_t3217094540 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)(L_3, (ImmutableSortedMap_2_t3217094540 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return L_3;
	}
}
// T Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>::GetMinEntry()
extern "C"  Il2CppObject * ImmutableSortedSet_1_GetMinEntry_m4219155093_gshared (ImmutableSortedSet_1_t2348759144 * __this, const MethodInfo* method)
{
	{
		ImmutableSortedMap_2_t3217094540 * L_0 = (ImmutableSortedMap_2_t3217094540 *)__this->get__map_0();
		NullCheck((ImmutableSortedMap_2_t3217094540 *)L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(11 /* TK Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::GetMinKey() */, (ImmutableSortedMap_2_t3217094540 *)L_0);
		return L_1;
	}
}
// T Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>::GetMaxEntry()
extern "C"  Il2CppObject * ImmutableSortedSet_1_GetMaxEntry_m2387621655_gshared (ImmutableSortedSet_1_t2348759144 * __this, const MethodInfo* method)
{
	{
		ImmutableSortedMap_2_t3217094540 * L_0 = (ImmutableSortedMap_2_t3217094540 *)__this->get__map_0();
		NullCheck((ImmutableSortedMap_2_t3217094540 *)L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(12 /* TK Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::GetMaxKey() */, (ImmutableSortedMap_2_t3217094540 *)L_0);
		return L_1;
	}
}
// System.Collections.Generic.IEnumerator`1<T> Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>::ReverseIterator()
extern "C"  Il2CppObject* ImmutableSortedSet_1_ReverseIterator_m1480254674_gshared (ImmutableSortedSet_1_t2348759144 * __this, const MethodInfo* method)
{
	{
		ImmutableSortedMap_2_t3217094540 * L_0 = (ImmutableSortedMap_2_t3217094540 *)__this->get__map_0();
		NullCheck((ImmutableSortedMap_2_t3217094540 *)L_0);
		Il2CppObject* L_1 = VirtFuncInvoker0< Il2CppObject* >::Invoke(16 /* System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TK,TV>> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::ReverseIterator() */, (ImmutableSortedMap_2_t3217094540 *)L_0);
		WrappedEntryIterator_t3190181025 * L_2 = (WrappedEntryIterator_t3190181025 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		((  void (*) (WrappedEntryIterator_t3190181025 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_2, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// T Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>::GetPredecessorEntry(T)
extern "C"  Il2CppObject * ImmutableSortedSet_1_GetPredecessorEntry_m1208315696_gshared (ImmutableSortedSet_1_t2348759144 * __this, Il2CppObject * ___entry0, const MethodInfo* method)
{
	{
		ImmutableSortedMap_2_t3217094540 * L_0 = (ImmutableSortedMap_2_t3217094540 *)__this->get__map_0();
		Il2CppObject * L_1 = ___entry0;
		NullCheck((ImmutableSortedMap_2_t3217094540 *)L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(17 /* TK Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::GetPredecessorKey(TK) */, (ImmutableSortedMap_2_t3217094540 *)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void Firebase.Database.Internal.Collection.LlrbBlackValueNode`2<System.Object,System.Object>::.ctor(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>)
extern "C"  void LlrbBlackValueNode_2__ctor_m2300250781_gshared (LlrbBlackValueNode_2_t3839725159 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, LlrbNode_2_t4262869811 * ___left2, LlrbNode_2_t4262869811 * ___right3, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		Il2CppObject * L_1 = ___value1;
		LlrbNode_2_t4262869811 * L_2 = ___left2;
		LlrbNode_2_t4262869811 * L_3 = ___right3;
		NullCheck((LlrbValueNode_2_t1296857666 *)__this);
		((  void (*) (LlrbValueNode_2_t1296857666 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((LlrbValueNode_2_t1296857666 *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1, (LlrbNode_2_t4262869811 *)L_2, (LlrbNode_2_t4262869811 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// Firebase.Database.Internal.Collection.LlrbNode`2/Color<TK,TV> Firebase.Database.Internal.Collection.LlrbBlackValueNode`2<System.Object,System.Object>::GetColor()
extern "C"  int32_t LlrbBlackValueNode_2_GetColor_m422302376_gshared (LlrbBlackValueNode_2_t3839725159 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(1);
	}
}
// System.Boolean Firebase.Database.Internal.Collection.LlrbBlackValueNode`2<System.Object,System.Object>::IsRed()
extern "C"  bool LlrbBlackValueNode_2_IsRed_m1597543809_gshared (LlrbBlackValueNode_2_t3839725159 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// Firebase.Database.Internal.Collection.LlrbValueNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbBlackValueNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>)
extern "C"  LlrbValueNode_2_t1296857666 * LlrbBlackValueNode_2_Copy_m4069924962_gshared (LlrbBlackValueNode_2_t3839725159 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, LlrbNode_2_t4262869811 * ___left2, LlrbNode_2_t4262869811 * ___right3, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	LlrbNode_2_t4262869811 * V_2 = NULL;
	LlrbNode_2_t4262869811 * V_3 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	Il2CppObject * G_B6_0 = NULL;
	LlrbNode_2_t4262869811 * G_B9_0 = NULL;
	LlrbNode_2_t4262869811 * G_B12_0 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		NullCheck((LlrbNode_2_t4262869811 *)__this);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(9 /* TK Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetKey() */, (LlrbNode_2_t4262869811 *)__this);
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___key0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		V_0 = (Il2CppObject *)G_B3_0;
		Il2CppObject * L_3 = ___value1;
		if (L_3)
		{
			goto IL_002e;
		}
	}
	{
		NullCheck((LlrbNode_2_t4262869811 *)__this);
		Il2CppObject * L_4 = VirtFuncInvoker0< Il2CppObject * >::Invoke(10 /* TV Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetValue() */, (LlrbNode_2_t4262869811 *)__this);
		G_B6_0 = L_4;
		goto IL_002f;
	}

IL_002e:
	{
		Il2CppObject * L_5 = ___value1;
		G_B6_0 = L_5;
	}

IL_002f:
	{
		V_1 = (Il2CppObject *)G_B6_0;
		LlrbNode_2_t4262869811 * L_6 = ___left2;
		if (L_6)
		{
			goto IL_0041;
		}
	}
	{
		NullCheck((LlrbNode_2_t4262869811 *)__this);
		LlrbNode_2_t4262869811 * L_7 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(11 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetLeft() */, (LlrbNode_2_t4262869811 *)__this);
		G_B9_0 = L_7;
		goto IL_0042;
	}

IL_0041:
	{
		LlrbNode_2_t4262869811 * L_8 = ___left2;
		G_B9_0 = L_8;
	}

IL_0042:
	{
		V_2 = (LlrbNode_2_t4262869811 *)G_B9_0;
		LlrbNode_2_t4262869811 * L_9 = ___right3;
		if (L_9)
		{
			goto IL_0055;
		}
	}
	{
		NullCheck((LlrbNode_2_t4262869811 *)__this);
		LlrbNode_2_t4262869811 * L_10 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(12 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetRight() */, (LlrbNode_2_t4262869811 *)__this);
		G_B12_0 = L_10;
		goto IL_0057;
	}

IL_0055:
	{
		LlrbNode_2_t4262869811 * L_11 = ___right3;
		G_B12_0 = L_11;
	}

IL_0057:
	{
		V_3 = (LlrbNode_2_t4262869811 *)G_B12_0;
		Il2CppObject * L_12 = V_0;
		Il2CppObject * L_13 = V_1;
		LlrbNode_2_t4262869811 * L_14 = V_2;
		LlrbNode_2_t4262869811 * L_15 = V_3;
		LlrbBlackValueNode_2_t3839725159 * L_16 = (LlrbBlackValueNode_2_t3839725159 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9));
		((  void (*) (LlrbBlackValueNode_2_t3839725159 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)(L_16, (Il2CppObject *)L_12, (Il2CppObject *)L_13, (LlrbNode_2_t4262869811 *)L_14, (LlrbNode_2_t4262869811 *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return L_16;
	}
}
// System.Void Firebase.Database.Internal.Collection.LlrbEmptyNode`2<System.Object,System.Object>::.ctor()
extern "C"  void LlrbEmptyNode_2__ctor_m1454818453_gshared (LlrbEmptyNode_2_t3030218886 * __this, const MethodInfo* method)
{
	{
		NullCheck((LlrbNode_2_t4262869811 *)__this);
		((  void (*) (LlrbNode_2_t4262869811 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((LlrbNode_2_t4262869811 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// Firebase.Database.Internal.Collection.LlrbEmptyNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbEmptyNode`2<System.Object,System.Object>::GetInstance()
extern "C"  LlrbEmptyNode_2_t3030218886 * LlrbEmptyNode_2_GetInstance_m1228740834_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		LlrbEmptyNode_2_t3030218886 * L_0 = ((LlrbEmptyNode_2_t3030218886_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->get_Instance_0();
		return L_0;
	}
}
// Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbEmptyNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2/Color<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>)
extern "C"  LlrbNode_2_t4262869811 * LlrbEmptyNode_2_Copy_m400410332_gshared (LlrbEmptyNode_2_t3030218886 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, int32_t ___color2, LlrbNode_2_t4262869811 * ___left3, LlrbNode_2_t4262869811 * ___right4, const MethodInfo* method)
{
	{
		return __this;
	}
}
// Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbEmptyNode`2<System.Object,System.Object>::Insert(TK,TV,System.Collections.Generic.IComparer`1<TK>)
extern "C"  LlrbNode_2_t4262869811 * LlrbEmptyNode_2_Insert_m2992281951_gshared (LlrbEmptyNode_2_t3030218886 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, Il2CppObject* ___comparator2, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		Il2CppObject * L_1 = ___value1;
		LlrbRedValueNode_2_t3628144835 * L_2 = (LlrbRedValueNode_2_t3628144835 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (LlrbRedValueNode_2_t3628144835 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbEmptyNode`2<System.Object,System.Object>::Remove(TK,System.Collections.Generic.IComparer`1<TK>)
extern "C"  LlrbNode_2_t4262869811 * LlrbEmptyNode_2_Remove_m3398061600_gshared (LlrbEmptyNode_2_t3030218886 * __this, Il2CppObject * ___key0, Il2CppObject* ___comparator1, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean Firebase.Database.Internal.Collection.LlrbEmptyNode`2<System.Object,System.Object>::IsEmpty()
extern "C"  bool LlrbEmptyNode_2_IsEmpty_m2232198176_gshared (LlrbEmptyNode_2_t3030218886 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean Firebase.Database.Internal.Collection.LlrbEmptyNode`2<System.Object,System.Object>::IsRed()
extern "C"  bool LlrbEmptyNode_2_IsRed_m4049998314_gshared (LlrbEmptyNode_2_t3030218886 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// TK Firebase.Database.Internal.Collection.LlrbEmptyNode`2<System.Object,System.Object>::GetKey()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LlrbEmptyNode_2_GetKey_m3180626044_MetadataUsageId;
extern "C"  Il2CppObject * LlrbEmptyNode_2_GetKey_m3180626044_gshared (LlrbEmptyNode_2_t3030218886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LlrbEmptyNode_2_GetKey_m3180626044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		return L_0;
	}
}
// TV Firebase.Database.Internal.Collection.LlrbEmptyNode`2<System.Object,System.Object>::GetValue()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LlrbEmptyNode_2_GetValue_m503547629_MetadataUsageId;
extern "C"  Il2CppObject * LlrbEmptyNode_2_GetValue_m503547629_gshared (LlrbEmptyNode_2_t3030218886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LlrbEmptyNode_2_GetValue_m503547629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		return L_0;
	}
}
// Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbEmptyNode`2<System.Object,System.Object>::GetLeft()
extern "C"  LlrbNode_2_t4262869811 * LlrbEmptyNode_2_GetLeft_m1071281139_gshared (LlrbEmptyNode_2_t3030218886 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbEmptyNode`2<System.Object,System.Object>::GetRight()
extern "C"  LlrbNode_2_t4262869811 * LlrbEmptyNode_2_GetRight_m2591289026_gshared (LlrbEmptyNode_2_t3030218886 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbEmptyNode`2<System.Object,System.Object>::GetMin()
extern "C"  LlrbNode_2_t4262869811 * LlrbEmptyNode_2_GetMin_m4225977428_gshared (LlrbEmptyNode_2_t3030218886 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbEmptyNode`2<System.Object,System.Object>::GetMax()
extern "C"  LlrbNode_2_t4262869811 * LlrbEmptyNode_2_GetMax_m1342635390_gshared (LlrbEmptyNode_2_t3030218886 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Int32 Firebase.Database.Internal.Collection.LlrbEmptyNode`2<System.Object,System.Object>::Count()
extern "C"  int32_t LlrbEmptyNode_2_Count_m2193555718_gshared (LlrbEmptyNode_2_t3030218886 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void Firebase.Database.Internal.Collection.LlrbEmptyNode`2<System.Object,System.Object>::InOrderTraversal(Firebase.Database.Internal.Collection.LlrbNode`2/NodeVisitor<TK,TV>)
extern "C"  void LlrbEmptyNode_2_InOrderTraversal_m486308083_gshared (LlrbEmptyNode_2_t3030218886 * __this, NodeVisitor_t2944196615 * ___visitor0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Firebase.Database.Internal.Collection.LlrbEmptyNode`2<System.Object,System.Object>::.cctor()
extern "C"  void LlrbEmptyNode_2__cctor_m2089748082_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		LlrbEmptyNode_2_t3030218886 * L_0 = (LlrbEmptyNode_2_t3030218886 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((  void (*) (LlrbEmptyNode_2_t3030218886 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((LlrbEmptyNode_2_t3030218886_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void Firebase.Database.Internal.Collection.LlrbNode`2/NodeVisitor<System.Object,System.Object>::.ctor()
extern "C"  void NodeVisitor__ctor_m3743962795_gshared (NodeVisitor_t2944196615 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::.ctor()
extern "C"  void LlrbNode_2__ctor_m2809819688_gshared (LlrbNode_2_t4262869811 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Database.Internal.Collection.LlrbRedValueNode`2<System.Object,System.Object>::.ctor(TK,TV)
extern "C"  void LlrbRedValueNode_2__ctor_m2387469861_gshared (LlrbRedValueNode_2_t3628144835 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		Il2CppObject * L_1 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		LlrbEmptyNode_2_t3030218886 * L_2 = ((  LlrbEmptyNode_2_t3030218886 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		LlrbEmptyNode_2_t3030218886 * L_3 = ((  LlrbEmptyNode_2_t3030218886 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((LlrbValueNode_2_t1296857666 *)__this);
		((  void (*) (LlrbValueNode_2_t1296857666 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LlrbValueNode_2_t1296857666 *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1, (LlrbNode_2_t4262869811 *)L_2, (LlrbNode_2_t4262869811 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void Firebase.Database.Internal.Collection.LlrbRedValueNode`2<System.Object,System.Object>::.ctor(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>)
extern "C"  void LlrbRedValueNode_2__ctor_m1169553381_gshared (LlrbRedValueNode_2_t3628144835 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, LlrbNode_2_t4262869811 * ___left2, LlrbNode_2_t4262869811 * ___right3, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		Il2CppObject * L_1 = ___value1;
		LlrbNode_2_t4262869811 * L_2 = ___left2;
		LlrbNode_2_t4262869811 * L_3 = ___right3;
		NullCheck((LlrbValueNode_2_t1296857666 *)__this);
		((  void (*) (LlrbValueNode_2_t1296857666 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LlrbValueNode_2_t1296857666 *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1, (LlrbNode_2_t4262869811 *)L_2, (LlrbNode_2_t4262869811 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// Firebase.Database.Internal.Collection.LlrbNode`2/Color<TK,TV> Firebase.Database.Internal.Collection.LlrbRedValueNode`2<System.Object,System.Object>::GetColor()
extern "C"  int32_t LlrbRedValueNode_2_GetColor_m2296677748_gshared (LlrbRedValueNode_2_t3628144835 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Boolean Firebase.Database.Internal.Collection.LlrbRedValueNode`2<System.Object,System.Object>::IsRed()
extern "C"  bool LlrbRedValueNode_2_IsRed_m4110529481_gshared (LlrbRedValueNode_2_t3628144835 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// Firebase.Database.Internal.Collection.LlrbValueNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbRedValueNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>)
extern "C"  LlrbValueNode_2_t1296857666 * LlrbRedValueNode_2_Copy_m554805106_gshared (LlrbRedValueNode_2_t3628144835 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, LlrbNode_2_t4262869811 * ___left2, LlrbNode_2_t4262869811 * ___right3, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	LlrbNode_2_t4262869811 * V_2 = NULL;
	LlrbNode_2_t4262869811 * V_3 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	Il2CppObject * G_B6_0 = NULL;
	LlrbNode_2_t4262869811 * G_B9_0 = NULL;
	LlrbNode_2_t4262869811 * G_B12_0 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		NullCheck((LlrbNode_2_t4262869811 *)__this);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(9 /* TK Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetKey() */, (LlrbNode_2_t4262869811 *)__this);
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___key0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		V_0 = (Il2CppObject *)G_B3_0;
		Il2CppObject * L_3 = ___value1;
		if (L_3)
		{
			goto IL_002e;
		}
	}
	{
		NullCheck((LlrbNode_2_t4262869811 *)__this);
		Il2CppObject * L_4 = VirtFuncInvoker0< Il2CppObject * >::Invoke(10 /* TV Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetValue() */, (LlrbNode_2_t4262869811 *)__this);
		G_B6_0 = L_4;
		goto IL_002f;
	}

IL_002e:
	{
		Il2CppObject * L_5 = ___value1;
		G_B6_0 = L_5;
	}

IL_002f:
	{
		V_1 = (Il2CppObject *)G_B6_0;
		LlrbNode_2_t4262869811 * L_6 = ___left2;
		if (L_6)
		{
			goto IL_0041;
		}
	}
	{
		NullCheck((LlrbNode_2_t4262869811 *)__this);
		LlrbNode_2_t4262869811 * L_7 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(11 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetLeft() */, (LlrbNode_2_t4262869811 *)__this);
		G_B9_0 = L_7;
		goto IL_0042;
	}

IL_0041:
	{
		LlrbNode_2_t4262869811 * L_8 = ___left2;
		G_B9_0 = L_8;
	}

IL_0042:
	{
		V_2 = (LlrbNode_2_t4262869811 *)G_B9_0;
		LlrbNode_2_t4262869811 * L_9 = ___right3;
		if (L_9)
		{
			goto IL_0055;
		}
	}
	{
		NullCheck((LlrbNode_2_t4262869811 *)__this);
		LlrbNode_2_t4262869811 * L_10 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(12 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetRight() */, (LlrbNode_2_t4262869811 *)__this);
		G_B12_0 = L_10;
		goto IL_0057;
	}

IL_0055:
	{
		LlrbNode_2_t4262869811 * L_11 = ___right3;
		G_B12_0 = L_11;
	}

IL_0057:
	{
		V_3 = (LlrbNode_2_t4262869811 *)G_B12_0;
		Il2CppObject * L_12 = V_0;
		Il2CppObject * L_13 = V_1;
		LlrbNode_2_t4262869811 * L_14 = V_2;
		LlrbNode_2_t4262869811 * L_15 = V_3;
		LlrbRedValueNode_2_t3628144835 * L_16 = (LlrbRedValueNode_2_t3628144835 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		((  void (*) (LlrbRedValueNode_2_t3628144835 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(L_16, (Il2CppObject *)L_12, (Il2CppObject *)L_13, (LlrbNode_2_t4262869811 *)L_14, (LlrbNode_2_t4262869811 *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_16;
	}
}
// System.Void Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::.ctor(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>)
extern "C"  void LlrbValueNode_2__ctor_m3789885180_gshared (LlrbValueNode_2_t1296857666 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, LlrbNode_2_t4262869811 * ___left2, LlrbNode_2_t4262869811 * ___right3, const MethodInfo* method)
{
	LlrbValueNode_2_t1296857666 * G_B2_0 = NULL;
	LlrbValueNode_2_t1296857666 * G_B1_0 = NULL;
	LlrbEmptyNode_2_t3030218886 * G_B3_0 = NULL;
	LlrbValueNode_2_t1296857666 * G_B3_1 = NULL;
	LlrbValueNode_2_t1296857666 * G_B5_0 = NULL;
	LlrbValueNode_2_t1296857666 * G_B4_0 = NULL;
	LlrbEmptyNode_2_t3030218886 * G_B6_0 = NULL;
	LlrbValueNode_2_t1296857666 * G_B6_1 = NULL;
	{
		NullCheck((LlrbNode_2_t4262869811 *)__this);
		((  void (*) (LlrbNode_2_t4262869811 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((LlrbNode_2_t4262869811 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Il2CppObject * L_0 = ___key0;
		__this->set__key_0(L_0);
		Il2CppObject * L_1 = ___value1;
		__this->set__value_2(L_1);
		LlrbNode_2_t4262869811 * L_2 = ___left2;
		G_B1_0 = ((LlrbValueNode_2_t1296857666 *)(__this));
		if (L_2)
		{
			G_B2_0 = ((LlrbValueNode_2_t1296857666 *)(__this));
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		LlrbEmptyNode_2_t3030218886 * L_3 = ((  LlrbEmptyNode_2_t3030218886 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		G_B3_0 = L_3;
		G_B3_1 = ((LlrbValueNode_2_t1296857666 *)(G_B1_0));
		goto IL_0026;
	}

IL_0025:
	{
		LlrbNode_2_t4262869811 * L_4 = ___left2;
		G_B3_0 = ((LlrbEmptyNode_2_t3030218886 *)(L_4));
		G_B3_1 = ((LlrbValueNode_2_t1296857666 *)(G_B2_0));
	}

IL_0026:
	{
		NullCheck(G_B3_1);
		G_B3_1->set__left_3(G_B3_0);
		LlrbNode_2_t4262869811 * L_5 = ___right3;
		G_B4_0 = ((LlrbValueNode_2_t1296857666 *)(__this));
		if (L_5)
		{
			G_B5_0 = ((LlrbValueNode_2_t1296857666 *)(__this));
			goto IL_003d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		LlrbEmptyNode_2_t3030218886 * L_6 = ((  LlrbEmptyNode_2_t3030218886 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		G_B6_0 = L_6;
		G_B6_1 = ((LlrbValueNode_2_t1296857666 *)(G_B4_0));
		goto IL_003f;
	}

IL_003d:
	{
		LlrbNode_2_t4262869811 * L_7 = ___right3;
		G_B6_0 = ((LlrbEmptyNode_2_t3030218886 *)(L_7));
		G_B6_1 = ((LlrbValueNode_2_t1296857666 *)(G_B5_0));
	}

IL_003f:
	{
		NullCheck(G_B6_1);
		G_B6_1->set__right_1(G_B6_0);
		return;
	}
}
// Firebase.Database.Internal.Collection.LlrbNode`2/Color<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::OppositeColor(Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>)
extern "C"  int32_t LlrbValueNode_2_OppositeColor_m356183322_gshared (Il2CppObject * __this /* static, unused */, LlrbNode_2_t4262869811 * ___node0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		LlrbNode_2_t4262869811 * L_0 = ___node0;
		NullCheck((LlrbNode_2_t4262869811 *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsRed() */, (LlrbNode_2_t4262869811 *)L_0);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 1;
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 0;
	}

IL_0012:
	{
		return (int32_t)(G_B3_0);
	}
}
// Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::GetLeft()
extern "C"  LlrbNode_2_t4262869811 * LlrbValueNode_2_GetLeft_m3193603093_gshared (LlrbValueNode_2_t1296857666 * __this, const MethodInfo* method)
{
	{
		LlrbNode_2_t4262869811 * L_0 = (LlrbNode_2_t4262869811 *)__this->get__left_3();
		return L_0;
	}
}
// Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::GetRight()
extern "C"  LlrbNode_2_t4262869811 * LlrbValueNode_2_GetRight_m1105278098_gshared (LlrbValueNode_2_t1296857666 * __this, const MethodInfo* method)
{
	{
		LlrbNode_2_t4262869811 * L_0 = (LlrbNode_2_t4262869811 *)__this->get__right_1();
		return L_0;
	}
}
// TK Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::GetKey()
extern "C"  Il2CppObject * LlrbValueNode_2_GetKey_m2081196488_gshared (LlrbValueNode_2_t1296857666 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__key_0();
		return L_0;
	}
}
// TV Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::GetValue()
extern "C"  Il2CppObject * LlrbValueNode_2_GetValue_m252497311_gshared (LlrbValueNode_2_t1296857666 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__value_2();
		return L_0;
	}
}
// Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2/Color<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>)
extern "C"  LlrbNode_2_t4262869811 * LlrbValueNode_2_Copy_m160077796_gshared (LlrbValueNode_2_t1296857666 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, int32_t ___color2, LlrbNode_2_t4262869811 * ___left3, LlrbNode_2_t4262869811 * ___right4, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	LlrbNode_2_t4262869811 * V_2 = NULL;
	LlrbNode_2_t4262869811 * V_3 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	Il2CppObject * G_B6_0 = NULL;
	LlrbNode_2_t4262869811 * G_B9_0 = NULL;
	LlrbNode_2_t4262869811 * G_B12_0 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__key_0();
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___key0;
		G_B3_0 = L_2;
	}

IL_0017:
	{
		V_0 = (Il2CppObject *)G_B3_0;
		Il2CppObject * L_3 = ___value1;
		if (L_3)
		{
			goto IL_002e;
		}
	}
	{
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__value_2();
		G_B6_0 = L_4;
		goto IL_002f;
	}

IL_002e:
	{
		Il2CppObject * L_5 = ___value1;
		G_B6_0 = L_5;
	}

IL_002f:
	{
		V_1 = (Il2CppObject *)G_B6_0;
		LlrbNode_2_t4262869811 * L_6 = ___left3;
		if (L_6)
		{
			goto IL_0042;
		}
	}
	{
		LlrbNode_2_t4262869811 * L_7 = (LlrbNode_2_t4262869811 *)__this->get__left_3();
		G_B9_0 = L_7;
		goto IL_0044;
	}

IL_0042:
	{
		LlrbNode_2_t4262869811 * L_8 = ___left3;
		G_B9_0 = L_8;
	}

IL_0044:
	{
		V_2 = (LlrbNode_2_t4262869811 *)G_B9_0;
		LlrbNode_2_t4262869811 * L_9 = ___right4;
		if (L_9)
		{
			goto IL_0057;
		}
	}
	{
		LlrbNode_2_t4262869811 * L_10 = (LlrbNode_2_t4262869811 *)__this->get__right_1();
		G_B12_0 = L_10;
		goto IL_0059;
	}

IL_0057:
	{
		LlrbNode_2_t4262869811 * L_11 = ___right4;
		G_B12_0 = L_11;
	}

IL_0059:
	{
		V_3 = (LlrbNode_2_t4262869811 *)G_B12_0;
		int32_t L_12 = ___color2;
		if (L_12)
		{
			goto IL_006a;
		}
	}
	{
		Il2CppObject * L_13 = V_0;
		Il2CppObject * L_14 = V_1;
		LlrbNode_2_t4262869811 * L_15 = V_2;
		LlrbNode_2_t4262869811 * L_16 = V_3;
		LlrbRedValueNode_2_t3628144835 * L_17 = (LlrbRedValueNode_2_t3628144835 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LlrbRedValueNode_2_t3628144835 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_17, (Il2CppObject *)L_13, (Il2CppObject *)L_14, (LlrbNode_2_t4262869811 *)L_15, (LlrbNode_2_t4262869811 *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_17;
	}

IL_006a:
	{
		Il2CppObject * L_18 = V_0;
		Il2CppObject * L_19 = V_1;
		LlrbNode_2_t4262869811 * L_20 = V_2;
		LlrbNode_2_t4262869811 * L_21 = V_3;
		LlrbBlackValueNode_2_t3839725159 * L_22 = (LlrbBlackValueNode_2_t3839725159 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9));
		((  void (*) (LlrbBlackValueNode_2_t3839725159 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)(L_22, (Il2CppObject *)L_18, (Il2CppObject *)L_19, (LlrbNode_2_t4262869811 *)L_20, (LlrbNode_2_t4262869811 *)L_21, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return L_22;
	}
}
// Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::Insert(TK,TV,System.Collections.Generic.IComparer`1<TK>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LlrbValueNode_2_Insert_m781426253_MetadataUsageId;
extern "C"  LlrbNode_2_t4262869811 * LlrbValueNode_2_Insert_m781426253_gshared (LlrbValueNode_2_t1296857666 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, Il2CppObject* ___comparator2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LlrbValueNode_2_Insert_m781426253_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	LlrbValueNode_2_t1296857666 * V_1 = NULL;
	LlrbNode_2_t4262869811 * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	LlrbNode_2_t4262869811 * V_5 = NULL;
	{
		Il2CppObject* L_0 = ___comparator2;
		Il2CppObject * L_1 = ___key0;
		Il2CppObject * L_2 = (Il2CppObject *)__this->get__key_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_3 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Object>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (Il2CppObject*)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2);
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_0045;
		}
	}
	{
		LlrbNode_2_t4262869811 * L_5 = (LlrbNode_2_t4262869811 *)__this->get__left_3();
		Il2CppObject * L_6 = ___key0;
		Il2CppObject * L_7 = ___value1;
		Il2CppObject* L_8 = ___comparator2;
		NullCheck((LlrbNode_2_t4262869811 *)L_5);
		LlrbNode_2_t4262869811 * L_9 = VirtFuncInvoker3< LlrbNode_2_t4262869811 *, Il2CppObject *, Il2CppObject *, Il2CppObject* >::Invoke(5 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::Insert(TK,TV,System.Collections.Generic.IComparer`1<TK>) */, (LlrbNode_2_t4262869811 *)L_5, (Il2CppObject *)L_6, (Il2CppObject *)L_7, (Il2CppObject*)L_8);
		V_2 = (LlrbNode_2_t4262869811 *)L_9;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_10 = V_3;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_4));
		Il2CppObject * L_11 = V_4;
		LlrbNode_2_t4262869811 * L_12 = V_2;
		NullCheck((LlrbValueNode_2_t1296857666 *)__this);
		LlrbValueNode_2_t1296857666 * L_13 = VirtFuncInvoker4< LlrbValueNode_2_t1296857666 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 * >::Invoke(18 /* Firebase.Database.Internal.Collection.LlrbValueNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>) */, (LlrbValueNode_2_t1296857666 *)__this, (Il2CppObject *)L_10, (Il2CppObject *)L_11, (LlrbNode_2_t4262869811 *)L_12, (LlrbNode_2_t4262869811 *)NULL);
		V_1 = (LlrbValueNode_2_t1296857666 *)L_13;
		goto IL_0088;
	}

IL_0045:
	{
		int32_t L_14 = V_0;
		if (L_14)
		{
			goto IL_005b;
		}
	}
	{
		Il2CppObject * L_15 = ___key0;
		Il2CppObject * L_16 = ___value1;
		NullCheck((LlrbValueNode_2_t1296857666 *)__this);
		LlrbValueNode_2_t1296857666 * L_17 = VirtFuncInvoker4< LlrbValueNode_2_t1296857666 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 * >::Invoke(18 /* Firebase.Database.Internal.Collection.LlrbValueNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>) */, (LlrbValueNode_2_t1296857666 *)__this, (Il2CppObject *)L_15, (Il2CppObject *)L_16, (LlrbNode_2_t4262869811 *)NULL, (LlrbNode_2_t4262869811 *)NULL);
		V_1 = (LlrbValueNode_2_t1296857666 *)L_17;
		goto IL_0088;
	}

IL_005b:
	{
		LlrbNode_2_t4262869811 * L_18 = (LlrbNode_2_t4262869811 *)__this->get__right_1();
		Il2CppObject * L_19 = ___key0;
		Il2CppObject * L_20 = ___value1;
		Il2CppObject* L_21 = ___comparator2;
		NullCheck((LlrbNode_2_t4262869811 *)L_18);
		LlrbNode_2_t4262869811 * L_22 = VirtFuncInvoker3< LlrbNode_2_t4262869811 *, Il2CppObject *, Il2CppObject *, Il2CppObject* >::Invoke(5 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::Insert(TK,TV,System.Collections.Generic.IComparer`1<TK>) */, (LlrbNode_2_t4262869811 *)L_18, (Il2CppObject *)L_19, (Il2CppObject *)L_20, (Il2CppObject*)L_21);
		V_5 = (LlrbNode_2_t4262869811 *)L_22;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_23 = V_3;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_4));
		Il2CppObject * L_24 = V_4;
		LlrbNode_2_t4262869811 * L_25 = V_5;
		NullCheck((LlrbValueNode_2_t1296857666 *)__this);
		LlrbValueNode_2_t1296857666 * L_26 = VirtFuncInvoker4< LlrbValueNode_2_t1296857666 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 * >::Invoke(18 /* Firebase.Database.Internal.Collection.LlrbValueNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>) */, (LlrbValueNode_2_t1296857666 *)__this, (Il2CppObject *)L_23, (Il2CppObject *)L_24, (LlrbNode_2_t4262869811 *)NULL, (LlrbNode_2_t4262869811 *)L_25);
		V_1 = (LlrbValueNode_2_t1296857666 *)L_26;
	}

IL_0088:
	{
		LlrbValueNode_2_t1296857666 * L_27 = V_1;
		NullCheck((LlrbValueNode_2_t1296857666 *)L_27);
		LlrbValueNode_2_t1296857666 * L_28 = ((  LlrbValueNode_2_t1296857666 * (*) (LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((LlrbValueNode_2_t1296857666 *)L_27, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_28;
	}
}
// Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::Remove(TK,System.Collections.Generic.IComparer`1<TK>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LlrbValueNode_2_Remove_m3581654292_MetadataUsageId;
extern "C"  LlrbNode_2_t4262869811 * LlrbValueNode_2_Remove_m3581654292_gshared (LlrbValueNode_2_t1296857666 * __this, Il2CppObject * ___key0, Il2CppObject* ___comparator1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LlrbValueNode_2_Remove_m3581654292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LlrbValueNode_2_t1296857666 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	LlrbNode_2_t4262869811 * V_3 = NULL;
	{
		V_0 = (LlrbValueNode_2_t1296857666 *)__this;
		Il2CppObject* L_0 = ___comparator1;
		Il2CppObject * L_1 = ___key0;
		LlrbValueNode_2_t1296857666 * L_2 = V_0;
		NullCheck(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)L_2->get__key_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_4 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Object>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (Il2CppObject*)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_3);
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_0082;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_5 = V_0;
		NullCheck(L_5);
		LlrbNode_2_t4262869811 * L_6 = (LlrbNode_2_t4262869811 *)L_5->get__left_3();
		NullCheck((LlrbNode_2_t4262869811 *)L_6);
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsEmpty() */, (LlrbNode_2_t4262869811 *)L_6);
		if (L_7)
		{
			goto IL_0056;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_8 = V_0;
		NullCheck(L_8);
		LlrbNode_2_t4262869811 * L_9 = (LlrbNode_2_t4262869811 *)L_8->get__left_3();
		NullCheck((LlrbNode_2_t4262869811 *)L_9);
		bool L_10 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsRed() */, (LlrbNode_2_t4262869811 *)L_9);
		if (L_10)
		{
			goto IL_0056;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_11 = V_0;
		NullCheck(L_11);
		LlrbNode_2_t4262869811 * L_12 = (LlrbNode_2_t4262869811 *)L_11->get__left_3();
		NullCheck(((LlrbValueNode_2_t1296857666 *)Castclass(L_12, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16))));
		LlrbNode_2_t4262869811 * L_13 = (LlrbNode_2_t4262869811 *)((LlrbValueNode_2_t1296857666 *)Castclass(L_12, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)))->get__left_3();
		NullCheck((LlrbNode_2_t4262869811 *)L_13);
		bool L_14 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsRed() */, (LlrbNode_2_t4262869811 *)L_13);
		if (L_14)
		{
			goto IL_0056;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_15 = V_0;
		NullCheck((LlrbValueNode_2_t1296857666 *)L_15);
		LlrbValueNode_2_t1296857666 * L_16 = ((  LlrbValueNode_2_t1296857666 * (*) (LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LlrbValueNode_2_t1296857666 *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		V_0 = (LlrbValueNode_2_t1296857666 *)L_16;
	}

IL_0056:
	{
		LlrbValueNode_2_t1296857666 * L_17 = V_0;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_18 = V_1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_19 = V_2;
		LlrbValueNode_2_t1296857666 * L_20 = V_0;
		NullCheck(L_20);
		LlrbNode_2_t4262869811 * L_21 = (LlrbNode_2_t4262869811 *)L_20->get__left_3();
		Il2CppObject * L_22 = ___key0;
		Il2CppObject* L_23 = ___comparator1;
		NullCheck((LlrbNode_2_t4262869811 *)L_21);
		LlrbNode_2_t4262869811 * L_24 = VirtFuncInvoker2< LlrbNode_2_t4262869811 *, Il2CppObject *, Il2CppObject* >::Invoke(6 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::Remove(TK,System.Collections.Generic.IComparer`1<TK>) */, (LlrbNode_2_t4262869811 *)L_21, (Il2CppObject *)L_22, (Il2CppObject*)L_23);
		NullCheck((LlrbValueNode_2_t1296857666 *)L_17);
		LlrbValueNode_2_t1296857666 * L_25 = VirtFuncInvoker4< LlrbValueNode_2_t1296857666 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 * >::Invoke(18 /* Firebase.Database.Internal.Collection.LlrbValueNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>) */, (LlrbValueNode_2_t1296857666 *)L_17, (Il2CppObject *)L_18, (Il2CppObject *)L_19, (LlrbNode_2_t4262869811 *)L_24, (LlrbNode_2_t4262869811 *)NULL);
		V_0 = (LlrbValueNode_2_t1296857666 *)L_25;
		goto IL_0159;
	}

IL_0082:
	{
		LlrbValueNode_2_t1296857666 * L_26 = V_0;
		NullCheck(L_26);
		LlrbNode_2_t4262869811 * L_27 = (LlrbNode_2_t4262869811 *)L_26->get__left_3();
		NullCheck((LlrbNode_2_t4262869811 *)L_27);
		bool L_28 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsRed() */, (LlrbNode_2_t4262869811 *)L_27);
		if (!L_28)
		{
			goto IL_0099;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_29 = V_0;
		NullCheck((LlrbValueNode_2_t1296857666 *)L_29);
		LlrbValueNode_2_t1296857666 * L_30 = ((  LlrbValueNode_2_t1296857666 * (*) (LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((LlrbValueNode_2_t1296857666 *)L_29, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		V_0 = (LlrbValueNode_2_t1296857666 *)L_30;
	}

IL_0099:
	{
		LlrbValueNode_2_t1296857666 * L_31 = V_0;
		NullCheck(L_31);
		LlrbNode_2_t4262869811 * L_32 = (LlrbNode_2_t4262869811 *)L_31->get__right_1();
		NullCheck((LlrbNode_2_t4262869811 *)L_32);
		bool L_33 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsEmpty() */, (LlrbNode_2_t4262869811 *)L_32);
		if (L_33)
		{
			goto IL_00da;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_34 = V_0;
		NullCheck(L_34);
		LlrbNode_2_t4262869811 * L_35 = (LlrbNode_2_t4262869811 *)L_34->get__right_1();
		NullCheck((LlrbNode_2_t4262869811 *)L_35);
		bool L_36 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsRed() */, (LlrbNode_2_t4262869811 *)L_35);
		if (L_36)
		{
			goto IL_00da;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_37 = V_0;
		NullCheck(L_37);
		LlrbNode_2_t4262869811 * L_38 = (LlrbNode_2_t4262869811 *)L_37->get__right_1();
		NullCheck(((LlrbValueNode_2_t1296857666 *)Castclass(L_38, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16))));
		LlrbNode_2_t4262869811 * L_39 = (LlrbNode_2_t4262869811 *)((LlrbValueNode_2_t1296857666 *)Castclass(L_38, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)))->get__left_3();
		NullCheck((LlrbNode_2_t4262869811 *)L_39);
		bool L_40 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsRed() */, (LlrbNode_2_t4262869811 *)L_39);
		if (L_40)
		{
			goto IL_00da;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_41 = V_0;
		NullCheck((LlrbValueNode_2_t1296857666 *)L_41);
		LlrbValueNode_2_t1296857666 * L_42 = ((  LlrbValueNode_2_t1296857666 * (*) (LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((LlrbValueNode_2_t1296857666 *)L_41, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		V_0 = (LlrbValueNode_2_t1296857666 *)L_42;
	}

IL_00da:
	{
		Il2CppObject* L_43 = ___comparator1;
		Il2CppObject * L_44 = ___key0;
		LlrbValueNode_2_t1296857666 * L_45 = V_0;
		NullCheck(L_45);
		Il2CppObject * L_46 = (Il2CppObject *)L_45->get__key_0();
		NullCheck((Il2CppObject*)L_43);
		int32_t L_47 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Object>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (Il2CppObject*)L_43, (Il2CppObject *)L_44, (Il2CppObject *)L_46);
		if (L_47)
		{
			goto IL_0132;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_48 = V_0;
		NullCheck(L_48);
		LlrbNode_2_t4262869811 * L_49 = (LlrbNode_2_t4262869811 *)L_48->get__right_1();
		NullCheck((LlrbNode_2_t4262869811 *)L_49);
		bool L_50 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsEmpty() */, (LlrbNode_2_t4262869811 *)L_49);
		if (!L_50)
		{
			goto IL_0102;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		LlrbEmptyNode_2_t3030218886 * L_51 = ((  LlrbEmptyNode_2_t3030218886 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_51;
	}

IL_0102:
	{
		LlrbValueNode_2_t1296857666 * L_52 = V_0;
		NullCheck(L_52);
		LlrbNode_2_t4262869811 * L_53 = (LlrbNode_2_t4262869811 *)L_52->get__right_1();
		NullCheck((LlrbNode_2_t4262869811 *)L_53);
		LlrbNode_2_t4262869811 * L_54 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(13 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetMin() */, (LlrbNode_2_t4262869811 *)L_53);
		V_3 = (LlrbNode_2_t4262869811 *)L_54;
		LlrbValueNode_2_t1296857666 * L_55 = V_0;
		LlrbNode_2_t4262869811 * L_56 = V_3;
		NullCheck((LlrbNode_2_t4262869811 *)L_56);
		Il2CppObject * L_57 = VirtFuncInvoker0< Il2CppObject * >::Invoke(9 /* TK Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetKey() */, (LlrbNode_2_t4262869811 *)L_56);
		LlrbNode_2_t4262869811 * L_58 = V_3;
		NullCheck((LlrbNode_2_t4262869811 *)L_58);
		Il2CppObject * L_59 = VirtFuncInvoker0< Il2CppObject * >::Invoke(10 /* TV Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetValue() */, (LlrbNode_2_t4262869811 *)L_58);
		LlrbValueNode_2_t1296857666 * L_60 = V_0;
		NullCheck(L_60);
		LlrbNode_2_t4262869811 * L_61 = (LlrbNode_2_t4262869811 *)L_60->get__right_1();
		NullCheck((LlrbValueNode_2_t1296857666 *)((LlrbValueNode_2_t1296857666 *)Castclass(L_61, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16))));
		LlrbNode_2_t4262869811 * L_62 = ((  LlrbNode_2_t4262869811 * (*) (LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((LlrbValueNode_2_t1296857666 *)((LlrbValueNode_2_t1296857666 *)Castclass(L_61, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		NullCheck((LlrbValueNode_2_t1296857666 *)L_55);
		LlrbValueNode_2_t1296857666 * L_63 = VirtFuncInvoker4< LlrbValueNode_2_t1296857666 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 * >::Invoke(18 /* Firebase.Database.Internal.Collection.LlrbValueNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>) */, (LlrbValueNode_2_t1296857666 *)L_55, (Il2CppObject *)L_57, (Il2CppObject *)L_59, (LlrbNode_2_t4262869811 *)NULL, (LlrbNode_2_t4262869811 *)L_62);
		V_0 = (LlrbValueNode_2_t1296857666 *)L_63;
	}

IL_0132:
	{
		LlrbValueNode_2_t1296857666 * L_64 = V_0;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_65 = V_1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_66 = V_2;
		LlrbValueNode_2_t1296857666 * L_67 = V_0;
		NullCheck(L_67);
		LlrbNode_2_t4262869811 * L_68 = (LlrbNode_2_t4262869811 *)L_67->get__right_1();
		Il2CppObject * L_69 = ___key0;
		Il2CppObject* L_70 = ___comparator1;
		NullCheck((LlrbNode_2_t4262869811 *)L_68);
		LlrbNode_2_t4262869811 * L_71 = VirtFuncInvoker2< LlrbNode_2_t4262869811 *, Il2CppObject *, Il2CppObject* >::Invoke(6 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::Remove(TK,System.Collections.Generic.IComparer`1<TK>) */, (LlrbNode_2_t4262869811 *)L_68, (Il2CppObject *)L_69, (Il2CppObject*)L_70);
		NullCheck((LlrbValueNode_2_t1296857666 *)L_64);
		LlrbValueNode_2_t1296857666 * L_72 = VirtFuncInvoker4< LlrbValueNode_2_t1296857666 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 * >::Invoke(18 /* Firebase.Database.Internal.Collection.LlrbValueNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>) */, (LlrbValueNode_2_t1296857666 *)L_64, (Il2CppObject *)L_65, (Il2CppObject *)L_66, (LlrbNode_2_t4262869811 *)NULL, (LlrbNode_2_t4262869811 *)L_71);
		V_0 = (LlrbValueNode_2_t1296857666 *)L_72;
	}

IL_0159:
	{
		LlrbValueNode_2_t1296857666 * L_73 = V_0;
		NullCheck((LlrbValueNode_2_t1296857666 *)L_73);
		LlrbValueNode_2_t1296857666 * L_74 = ((  LlrbValueNode_2_t1296857666 * (*) (LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((LlrbValueNode_2_t1296857666 *)L_73, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_74;
	}
}
// System.Boolean Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::IsEmpty()
extern "C"  bool LlrbValueNode_2_IsEmpty_m1001034316_gshared (LlrbValueNode_2_t1296857666 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::GetMin()
extern "C"  LlrbNode_2_t4262869811 * LlrbValueNode_2_GetMin_m2394736484_gshared (LlrbValueNode_2_t1296857666 * __this, const MethodInfo* method)
{
	{
		LlrbNode_2_t4262869811 * L_0 = (LlrbNode_2_t4262869811 *)__this->get__left_3();
		NullCheck((LlrbNode_2_t4262869811 *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsEmpty() */, (LlrbNode_2_t4262869811 *)L_0);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return __this;
	}

IL_0012:
	{
		LlrbNode_2_t4262869811 * L_2 = (LlrbNode_2_t4262869811 *)__this->get__left_3();
		NullCheck((LlrbNode_2_t4262869811 *)L_2);
		LlrbNode_2_t4262869811 * L_3 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(13 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetMin() */, (LlrbNode_2_t4262869811 *)L_2);
		return L_3;
	}
}
// Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::GetMax()
extern "C"  LlrbNode_2_t4262869811 * LlrbValueNode_2_GetMax_m640694454_gshared (LlrbValueNode_2_t1296857666 * __this, const MethodInfo* method)
{
	{
		LlrbNode_2_t4262869811 * L_0 = (LlrbNode_2_t4262869811 *)__this->get__right_1();
		NullCheck((LlrbNode_2_t4262869811 *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsEmpty() */, (LlrbNode_2_t4262869811 *)L_0);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return __this;
	}

IL_0012:
	{
		LlrbNode_2_t4262869811 * L_2 = (LlrbNode_2_t4262869811 *)__this->get__right_1();
		NullCheck((LlrbNode_2_t4262869811 *)L_2);
		LlrbNode_2_t4262869811 * L_3 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(14 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetMax() */, (LlrbNode_2_t4262869811 *)L_2);
		return L_3;
	}
}
// System.Int32 Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::Count()
extern "C"  int32_t LlrbValueNode_2_Count_m3245830974_gshared (LlrbValueNode_2_t1296857666 * __this, const MethodInfo* method)
{
	{
		LlrbNode_2_t4262869811 * L_0 = (LlrbNode_2_t4262869811 *)__this->get__left_3();
		NullCheck((LlrbNode_2_t4262869811 *)L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::Count() */, (LlrbNode_2_t4262869811 *)L_0);
		LlrbNode_2_t4262869811 * L_2 = (LlrbNode_2_t4262869811 *)__this->get__right_1();
		NullCheck((LlrbNode_2_t4262869811 *)L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::Count() */, (LlrbNode_2_t4262869811 *)L_2);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1+(int32_t)1))+(int32_t)L_3));
	}
}
// System.Void Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::InOrderTraversal(Firebase.Database.Internal.Collection.LlrbNode`2/NodeVisitor<TK,TV>)
extern "C"  void LlrbValueNode_2_InOrderTraversal_m1563838317_gshared (LlrbValueNode_2_t1296857666 * __this, NodeVisitor_t2944196615 * ___visitor0, const MethodInfo* method)
{
	{
		LlrbNode_2_t4262869811 * L_0 = (LlrbNode_2_t4262869811 *)__this->get__left_3();
		NodeVisitor_t2944196615 * L_1 = ___visitor0;
		NullCheck((LlrbNode_2_t4262869811 *)L_0);
		VirtActionInvoker1< NodeVisitor_t2944196615 * >::Invoke(16 /* System.Void Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::InOrderTraversal(Firebase.Database.Internal.Collection.LlrbNode`2/NodeVisitor<TK,TV>) */, (LlrbNode_2_t4262869811 *)L_0, (NodeVisitor_t2944196615 *)L_1);
		NodeVisitor_t2944196615 * L_2 = ___visitor0;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__key_0();
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__value_2();
		NullCheck((NodeVisitor_t2944196615 *)L_2);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(4 /* System.Void Firebase.Database.Internal.Collection.LlrbNode`2/NodeVisitor<System.Object,System.Object>::VisitEntry(TK,TV) */, (NodeVisitor_t2944196615 *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4);
		LlrbNode_2_t4262869811 * L_5 = (LlrbNode_2_t4262869811 *)__this->get__right_1();
		NodeVisitor_t2944196615 * L_6 = ___visitor0;
		NullCheck((LlrbNode_2_t4262869811 *)L_5);
		VirtActionInvoker1< NodeVisitor_t2944196615 * >::Invoke(16 /* System.Void Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::InOrderTraversal(Firebase.Database.Internal.Collection.LlrbNode`2/NodeVisitor<TK,TV>) */, (LlrbNode_2_t4262869811 *)L_5, (NodeVisitor_t2944196615 *)L_6);
		return;
	}
}
// System.Void Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::SetLeft(Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>)
extern "C"  void LlrbValueNode_2_SetLeft_m3306826474_gshared (LlrbValueNode_2_t1296857666 * __this, LlrbNode_2_t4262869811 * ___left0, const MethodInfo* method)
{
	{
		LlrbNode_2_t4262869811 * L_0 = ___left0;
		__this->set__left_3(L_0);
		return;
	}
}
// Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::RemoveMin()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LlrbValueNode_2_RemoveMin_m1694062810_MetadataUsageId;
extern "C"  LlrbNode_2_t4262869811 * LlrbValueNode_2_RemoveMin_m1694062810_gshared (LlrbValueNode_2_t1296857666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LlrbValueNode_2_RemoveMin_m1694062810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LlrbValueNode_2_t1296857666 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		LlrbNode_2_t4262869811 * L_0 = (LlrbNode_2_t4262869811 *)__this->get__left_3();
		NullCheck((LlrbNode_2_t4262869811 *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsEmpty() */, (LlrbNode_2_t4262869811 *)L_0);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		LlrbEmptyNode_2_t3030218886 * L_2 = ((  LlrbEmptyNode_2_t3030218886 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_2;
	}

IL_0016:
	{
		V_0 = (LlrbValueNode_2_t1296857666 *)__this;
		LlrbValueNode_2_t1296857666 * L_3 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_3);
		LlrbNode_2_t4262869811 * L_4 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(11 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetLeft() */, (LlrbNode_2_t4262869811 *)L_3);
		NullCheck((LlrbNode_2_t4262869811 *)L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsRed() */, (LlrbNode_2_t4262869811 *)L_4);
		if (L_5)
		{
			goto IL_0044;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_6 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_6);
		LlrbNode_2_t4262869811 * L_7 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(11 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetLeft() */, (LlrbNode_2_t4262869811 *)L_6);
		NullCheck((LlrbNode_2_t4262869811 *)L_7);
		LlrbNode_2_t4262869811 * L_8 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(11 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetLeft() */, (LlrbNode_2_t4262869811 *)L_7);
		NullCheck((LlrbNode_2_t4262869811 *)L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsRed() */, (LlrbNode_2_t4262869811 *)L_8);
		if (L_9)
		{
			goto IL_0044;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_10 = V_0;
		NullCheck((LlrbValueNode_2_t1296857666 *)L_10);
		LlrbValueNode_2_t1296857666 * L_11 = ((  LlrbValueNode_2_t1296857666 * (*) (LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LlrbValueNode_2_t1296857666 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		V_0 = (LlrbValueNode_2_t1296857666 *)L_11;
	}

IL_0044:
	{
		LlrbValueNode_2_t1296857666 * L_12 = V_0;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_13 = V_1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_14 = V_2;
		LlrbValueNode_2_t1296857666 * L_15 = V_0;
		NullCheck(L_15);
		LlrbNode_2_t4262869811 * L_16 = (LlrbNode_2_t4262869811 *)L_15->get__left_3();
		NullCheck((LlrbValueNode_2_t1296857666 *)((LlrbValueNode_2_t1296857666 *)Castclass(L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16))));
		LlrbNode_2_t4262869811 * L_17 = ((  LlrbNode_2_t4262869811 * (*) (LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)((LlrbValueNode_2_t1296857666 *)((LlrbValueNode_2_t1296857666 *)Castclass(L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		NullCheck((LlrbValueNode_2_t1296857666 *)L_12);
		LlrbValueNode_2_t1296857666 * L_18 = VirtFuncInvoker4< LlrbValueNode_2_t1296857666 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 * >::Invoke(18 /* Firebase.Database.Internal.Collection.LlrbValueNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>) */, (LlrbValueNode_2_t1296857666 *)L_12, (Il2CppObject *)L_13, (Il2CppObject *)L_14, (LlrbNode_2_t4262869811 *)L_17, (LlrbNode_2_t4262869811 *)NULL);
		V_0 = (LlrbValueNode_2_t1296857666 *)L_18;
		LlrbValueNode_2_t1296857666 * L_19 = V_0;
		NullCheck((LlrbValueNode_2_t1296857666 *)L_19);
		LlrbValueNode_2_t1296857666 * L_20 = ((  LlrbValueNode_2_t1296857666 * (*) (LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((LlrbValueNode_2_t1296857666 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_20;
	}
}
// Firebase.Database.Internal.Collection.LlrbValueNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::MoveRedLeft()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LlrbValueNode_2_MoveRedLeft_m2581967588_MetadataUsageId;
extern "C"  LlrbValueNode_2_t1296857666 * LlrbValueNode_2_MoveRedLeft_m2581967588_gshared (LlrbValueNode_2_t1296857666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LlrbValueNode_2_MoveRedLeft_m2581967588_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LlrbValueNode_2_t1296857666 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		NullCheck((LlrbValueNode_2_t1296857666 *)__this);
		LlrbValueNode_2_t1296857666 * L_0 = ((  LlrbValueNode_2_t1296857666 * (*) (LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((LlrbValueNode_2_t1296857666 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		V_0 = (LlrbValueNode_2_t1296857666 *)L_0;
		LlrbValueNode_2_t1296857666 * L_1 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_1);
		LlrbNode_2_t4262869811 * L_2 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(12 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetRight() */, (LlrbNode_2_t4262869811 *)L_1);
		NullCheck((LlrbNode_2_t4262869811 *)L_2);
		LlrbNode_2_t4262869811 * L_3 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(11 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetLeft() */, (LlrbNode_2_t4262869811 *)L_2);
		NullCheck((LlrbNode_2_t4262869811 *)L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsRed() */, (LlrbNode_2_t4262869811 *)L_3);
		if (!L_4)
		{
			goto IL_0054;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_5 = V_0;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_6 = V_1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_7 = V_2;
		LlrbValueNode_2_t1296857666 * L_8 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_8);
		LlrbNode_2_t4262869811 * L_9 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(12 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetRight() */, (LlrbNode_2_t4262869811 *)L_8);
		NullCheck((LlrbValueNode_2_t1296857666 *)((LlrbValueNode_2_t1296857666 *)Castclass(L_9, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16))));
		LlrbValueNode_2_t1296857666 * L_10 = ((  LlrbValueNode_2_t1296857666 * (*) (LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((LlrbValueNode_2_t1296857666 *)((LlrbValueNode_2_t1296857666 *)Castclass(L_9, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		NullCheck((LlrbValueNode_2_t1296857666 *)L_5);
		LlrbValueNode_2_t1296857666 * L_11 = VirtFuncInvoker4< LlrbValueNode_2_t1296857666 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 * >::Invoke(18 /* Firebase.Database.Internal.Collection.LlrbValueNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>) */, (LlrbValueNode_2_t1296857666 *)L_5, (Il2CppObject *)L_6, (Il2CppObject *)L_7, (LlrbNode_2_t4262869811 *)NULL, (LlrbNode_2_t4262869811 *)L_10);
		V_0 = (LlrbValueNode_2_t1296857666 *)L_11;
		LlrbValueNode_2_t1296857666 * L_12 = V_0;
		NullCheck((LlrbValueNode_2_t1296857666 *)L_12);
		LlrbValueNode_2_t1296857666 * L_13 = ((  LlrbValueNode_2_t1296857666 * (*) (LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32)->methodPointer)((LlrbValueNode_2_t1296857666 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32));
		V_0 = (LlrbValueNode_2_t1296857666 *)L_13;
		LlrbValueNode_2_t1296857666 * L_14 = V_0;
		NullCheck((LlrbValueNode_2_t1296857666 *)L_14);
		LlrbValueNode_2_t1296857666 * L_15 = ((  LlrbValueNode_2_t1296857666 * (*) (LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((LlrbValueNode_2_t1296857666 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		V_0 = (LlrbValueNode_2_t1296857666 *)L_15;
	}

IL_0054:
	{
		LlrbValueNode_2_t1296857666 * L_16 = V_0;
		return L_16;
	}
}
// Firebase.Database.Internal.Collection.LlrbValueNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::MoveRedRight()
extern "C"  LlrbValueNode_2_t1296857666 * LlrbValueNode_2_MoveRedRight_m1531981241_gshared (LlrbValueNode_2_t1296857666 * __this, const MethodInfo* method)
{
	LlrbValueNode_2_t1296857666 * V_0 = NULL;
	{
		NullCheck((LlrbValueNode_2_t1296857666 *)__this);
		LlrbValueNode_2_t1296857666 * L_0 = ((  LlrbValueNode_2_t1296857666 * (*) (LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((LlrbValueNode_2_t1296857666 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		V_0 = (LlrbValueNode_2_t1296857666 *)L_0;
		LlrbValueNode_2_t1296857666 * L_1 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_1);
		LlrbNode_2_t4262869811 * L_2 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(11 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetLeft() */, (LlrbNode_2_t4262869811 *)L_1);
		NullCheck((LlrbNode_2_t4262869811 *)L_2);
		LlrbNode_2_t4262869811 * L_3 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(11 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetLeft() */, (LlrbNode_2_t4262869811 *)L_2);
		NullCheck((LlrbNode_2_t4262869811 *)L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsRed() */, (LlrbNode_2_t4262869811 *)L_3);
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_5 = V_0;
		NullCheck((LlrbValueNode_2_t1296857666 *)L_5);
		LlrbValueNode_2_t1296857666 * L_6 = ((  LlrbValueNode_2_t1296857666 * (*) (LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((LlrbValueNode_2_t1296857666 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		V_0 = (LlrbValueNode_2_t1296857666 *)L_6;
		LlrbValueNode_2_t1296857666 * L_7 = V_0;
		NullCheck((LlrbValueNode_2_t1296857666 *)L_7);
		LlrbValueNode_2_t1296857666 * L_8 = ((  LlrbValueNode_2_t1296857666 * (*) (LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((LlrbValueNode_2_t1296857666 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		V_0 = (LlrbValueNode_2_t1296857666 *)L_8;
	}

IL_002a:
	{
		LlrbValueNode_2_t1296857666 * L_9 = V_0;
		return L_9;
	}
}
// Firebase.Database.Internal.Collection.LlrbValueNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::FixUp()
extern "C"  LlrbValueNode_2_t1296857666 * LlrbValueNode_2_FixUp_m2907309153_gshared (LlrbValueNode_2_t1296857666 * __this, const MethodInfo* method)
{
	LlrbValueNode_2_t1296857666 * V_0 = NULL;
	{
		V_0 = (LlrbValueNode_2_t1296857666 *)__this;
		LlrbValueNode_2_t1296857666 * L_0 = V_0;
		NullCheck(L_0);
		LlrbNode_2_t4262869811 * L_1 = (LlrbNode_2_t4262869811 *)L_0->get__right_1();
		NullCheck((LlrbNode_2_t4262869811 *)L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsRed() */, (LlrbNode_2_t4262869811 *)L_1);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_3 = V_0;
		NullCheck(L_3);
		LlrbNode_2_t4262869811 * L_4 = (LlrbNode_2_t4262869811 *)L_3->get__left_3();
		NullCheck((LlrbNode_2_t4262869811 *)L_4);
		bool L_5 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsRed() */, (LlrbNode_2_t4262869811 *)L_4);
		if (L_5)
		{
			goto IL_0029;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_6 = V_0;
		NullCheck((LlrbValueNode_2_t1296857666 *)L_6);
		LlrbValueNode_2_t1296857666 * L_7 = ((  LlrbValueNode_2_t1296857666 * (*) (LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32)->methodPointer)((LlrbValueNode_2_t1296857666 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32));
		V_0 = (LlrbValueNode_2_t1296857666 *)L_7;
	}

IL_0029:
	{
		LlrbValueNode_2_t1296857666 * L_8 = V_0;
		NullCheck(L_8);
		LlrbNode_2_t4262869811 * L_9 = (LlrbNode_2_t4262869811 *)L_8->get__left_3();
		NullCheck((LlrbNode_2_t4262869811 *)L_9);
		bool L_10 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsRed() */, (LlrbNode_2_t4262869811 *)L_9);
		if (!L_10)
		{
			goto IL_005a;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_11 = V_0;
		NullCheck(L_11);
		LlrbNode_2_t4262869811 * L_12 = (LlrbNode_2_t4262869811 *)L_11->get__left_3();
		NullCheck(((LlrbValueNode_2_t1296857666 *)Castclass(L_12, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16))));
		LlrbNode_2_t4262869811 * L_13 = (LlrbNode_2_t4262869811 *)((LlrbValueNode_2_t1296857666 *)Castclass(L_12, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)))->get__left_3();
		NullCheck((LlrbNode_2_t4262869811 *)L_13);
		bool L_14 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsRed() */, (LlrbNode_2_t4262869811 *)L_13);
		if (!L_14)
		{
			goto IL_005a;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_15 = V_0;
		NullCheck((LlrbValueNode_2_t1296857666 *)L_15);
		LlrbValueNode_2_t1296857666 * L_16 = ((  LlrbValueNode_2_t1296857666 * (*) (LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((LlrbValueNode_2_t1296857666 *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		V_0 = (LlrbValueNode_2_t1296857666 *)L_16;
	}

IL_005a:
	{
		LlrbValueNode_2_t1296857666 * L_17 = V_0;
		NullCheck(L_17);
		LlrbNode_2_t4262869811 * L_18 = (LlrbNode_2_t4262869811 *)L_17->get__left_3();
		NullCheck((LlrbNode_2_t4262869811 *)L_18);
		bool L_19 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsRed() */, (LlrbNode_2_t4262869811 *)L_18);
		if (!L_19)
		{
			goto IL_0081;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_20 = V_0;
		NullCheck(L_20);
		LlrbNode_2_t4262869811 * L_21 = (LlrbNode_2_t4262869811 *)L_20->get__right_1();
		NullCheck((LlrbNode_2_t4262869811 *)L_21);
		bool L_22 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsRed() */, (LlrbNode_2_t4262869811 *)L_21);
		if (!L_22)
		{
			goto IL_0081;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_23 = V_0;
		NullCheck((LlrbValueNode_2_t1296857666 *)L_23);
		LlrbValueNode_2_t1296857666 * L_24 = ((  LlrbValueNode_2_t1296857666 * (*) (LlrbValueNode_2_t1296857666 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((LlrbValueNode_2_t1296857666 *)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		V_0 = (LlrbValueNode_2_t1296857666 *)L_24;
	}

IL_0081:
	{
		LlrbValueNode_2_t1296857666 * L_25 = V_0;
		return L_25;
	}
}
// Firebase.Database.Internal.Collection.LlrbValueNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::RotateLeft()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LlrbValueNode_2_RotateLeft_m4139860009_MetadataUsageId;
extern "C"  LlrbValueNode_2_t1296857666 * LlrbValueNode_2_RotateLeft_m4139860009_gshared (LlrbValueNode_2_t1296857666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LlrbValueNode_2_RotateLeft_m4139860009_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LlrbValueNode_2_t1296857666 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_0 = V_1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_1 = V_2;
		LlrbNode_2_t4262869811 * L_2 = (LlrbNode_2_t4262869811 *)__this->get__right_1();
		NullCheck(((LlrbValueNode_2_t1296857666 *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16))));
		LlrbNode_2_t4262869811 * L_3 = (LlrbNode_2_t4262869811 *)((LlrbValueNode_2_t1296857666 *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)))->get__left_3();
		NullCheck((LlrbNode_2_t4262869811 *)__this);
		LlrbNode_2_t4262869811 * L_4 = VirtFuncInvoker5< LlrbNode_2_t4262869811 *, Il2CppObject *, Il2CppObject *, int32_t, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 * >::Invoke(4 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2/Color<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>) */, (LlrbNode_2_t4262869811 *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1, (int32_t)0, (LlrbNode_2_t4262869811 *)NULL, (LlrbNode_2_t4262869811 *)L_3);
		V_0 = (LlrbValueNode_2_t1296857666 *)((LlrbValueNode_2_t1296857666 *)Castclass(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)));
		LlrbNode_2_t4262869811 * L_5 = (LlrbNode_2_t4262869811 *)__this->get__right_1();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_6 = V_1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_7 = V_2;
		NullCheck((LlrbValueNode_2_t1296857666 *)__this);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(17 /* Firebase.Database.Internal.Collection.LlrbNode`2/Color<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::GetColor() */, (LlrbValueNode_2_t1296857666 *)__this);
		LlrbValueNode_2_t1296857666 * L_9 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_5);
		LlrbNode_2_t4262869811 * L_10 = VirtFuncInvoker5< LlrbNode_2_t4262869811 *, Il2CppObject *, Il2CppObject *, int32_t, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 * >::Invoke(4 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2/Color<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>) */, (LlrbNode_2_t4262869811 *)L_5, (Il2CppObject *)L_6, (Il2CppObject *)L_7, (int32_t)L_8, (LlrbNode_2_t4262869811 *)L_9, (LlrbNode_2_t4262869811 *)NULL);
		return ((LlrbValueNode_2_t1296857666 *)Castclass(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)));
	}
}
// Firebase.Database.Internal.Collection.LlrbValueNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::RotateRight()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LlrbValueNode_2_RotateRight_m2134806614_MetadataUsageId;
extern "C"  LlrbValueNode_2_t1296857666 * LlrbValueNode_2_RotateRight_m2134806614_gshared (LlrbValueNode_2_t1296857666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LlrbValueNode_2_RotateRight_m2134806614_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LlrbValueNode_2_t1296857666 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_0 = V_1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_1 = V_2;
		LlrbNode_2_t4262869811 * L_2 = (LlrbNode_2_t4262869811 *)__this->get__left_3();
		NullCheck(((LlrbValueNode_2_t1296857666 *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16))));
		LlrbNode_2_t4262869811 * L_3 = (LlrbNode_2_t4262869811 *)((LlrbValueNode_2_t1296857666 *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)))->get__right_1();
		NullCheck((LlrbNode_2_t4262869811 *)__this);
		LlrbNode_2_t4262869811 * L_4 = VirtFuncInvoker5< LlrbNode_2_t4262869811 *, Il2CppObject *, Il2CppObject *, int32_t, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 * >::Invoke(4 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2/Color<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>) */, (LlrbNode_2_t4262869811 *)__this, (Il2CppObject *)L_0, (Il2CppObject *)L_1, (int32_t)0, (LlrbNode_2_t4262869811 *)L_3, (LlrbNode_2_t4262869811 *)NULL);
		V_0 = (LlrbValueNode_2_t1296857666 *)((LlrbValueNode_2_t1296857666 *)Castclass(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)));
		LlrbNode_2_t4262869811 * L_5 = (LlrbNode_2_t4262869811 *)__this->get__left_3();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_6 = V_1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_7 = V_2;
		NullCheck((LlrbValueNode_2_t1296857666 *)__this);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(17 /* Firebase.Database.Internal.Collection.LlrbNode`2/Color<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::GetColor() */, (LlrbValueNode_2_t1296857666 *)__this);
		LlrbValueNode_2_t1296857666 * L_9 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_5);
		LlrbNode_2_t4262869811 * L_10 = VirtFuncInvoker5< LlrbNode_2_t4262869811 *, Il2CppObject *, Il2CppObject *, int32_t, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 * >::Invoke(4 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2/Color<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>) */, (LlrbNode_2_t4262869811 *)L_5, (Il2CppObject *)L_6, (Il2CppObject *)L_7, (int32_t)L_8, (LlrbNode_2_t4262869811 *)NULL, (LlrbNode_2_t4262869811 *)L_9);
		return ((LlrbValueNode_2_t1296857666 *)Castclass(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)));
	}
}
// Firebase.Database.Internal.Collection.LlrbValueNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::ColorFlip()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LlrbValueNode_2_ColorFlip_m164232373_MetadataUsageId;
extern "C"  LlrbValueNode_2_t1296857666 * LlrbValueNode_2_ColorFlip_m164232373_gshared (LlrbValueNode_2_t1296857666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LlrbValueNode_2_ColorFlip_m164232373_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LlrbNode_2_t4262869811 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	LlrbNode_2_t4262869811 * V_3 = NULL;
	{
		LlrbNode_2_t4262869811 * L_0 = (LlrbNode_2_t4262869811 *)__this->get__left_3();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_1 = V_1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_2 = V_2;
		LlrbNode_2_t4262869811 * L_3 = (LlrbNode_2_t4262869811 *)__this->get__left_3();
		int32_t L_4 = ((  int32_t (*) (Il2CppObject * /* static, unused */, LlrbNode_2_t4262869811 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35)->methodPointer)(NULL /*static, unused*/, (LlrbNode_2_t4262869811 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35));
		NullCheck((LlrbNode_2_t4262869811 *)L_0);
		LlrbNode_2_t4262869811 * L_5 = VirtFuncInvoker5< LlrbNode_2_t4262869811 *, Il2CppObject *, Il2CppObject *, int32_t, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 * >::Invoke(4 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2/Color<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>) */, (LlrbNode_2_t4262869811 *)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2, (int32_t)L_4, (LlrbNode_2_t4262869811 *)NULL, (LlrbNode_2_t4262869811 *)NULL);
		V_0 = (LlrbNode_2_t4262869811 *)L_5;
		LlrbNode_2_t4262869811 * L_6 = (LlrbNode_2_t4262869811 *)__this->get__right_1();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_7 = V_1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_8 = V_2;
		LlrbNode_2_t4262869811 * L_9 = (LlrbNode_2_t4262869811 *)__this->get__right_1();
		int32_t L_10 = ((  int32_t (*) (Il2CppObject * /* static, unused */, LlrbNode_2_t4262869811 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35)->methodPointer)(NULL /*static, unused*/, (LlrbNode_2_t4262869811 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35));
		NullCheck((LlrbNode_2_t4262869811 *)L_6);
		LlrbNode_2_t4262869811 * L_11 = VirtFuncInvoker5< LlrbNode_2_t4262869811 *, Il2CppObject *, Il2CppObject *, int32_t, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 * >::Invoke(4 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2/Color<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>) */, (LlrbNode_2_t4262869811 *)L_6, (Il2CppObject *)L_7, (Il2CppObject *)L_8, (int32_t)L_10, (LlrbNode_2_t4262869811 *)NULL, (LlrbNode_2_t4262869811 *)NULL);
		V_3 = (LlrbNode_2_t4262869811 *)L_11;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_12 = V_1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_13 = V_2;
		int32_t L_14 = ((  int32_t (*) (Il2CppObject * /* static, unused */, LlrbNode_2_t4262869811 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35)->methodPointer)(NULL /*static, unused*/, (LlrbNode_2_t4262869811 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35));
		LlrbNode_2_t4262869811 * L_15 = V_0;
		LlrbNode_2_t4262869811 * L_16 = V_3;
		NullCheck((LlrbNode_2_t4262869811 *)__this);
		LlrbNode_2_t4262869811 * L_17 = VirtFuncInvoker5< LlrbNode_2_t4262869811 *, Il2CppObject *, Il2CppObject *, int32_t, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 * >::Invoke(4 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2/Color<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>) */, (LlrbNode_2_t4262869811 *)__this, (Il2CppObject *)L_12, (Il2CppObject *)L_13, (int32_t)L_14, (LlrbNode_2_t4262869811 *)L_15, (LlrbNode_2_t4262869811 *)L_16);
		return ((LlrbValueNode_2_t1296857666 *)Castclass(L_17, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16)));
	}
}
// System.Void Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12/Enumerator216<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12<TK,TV,TA,TB,TC>)
extern "C"  void Enumerator216__ctor_m3035470894_gshared (Enumerator216_t3810636662 * __this, Base12_t2988184810 * ___enclosing0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Base12_t2988184810 * L_0 = ___enclosing0;
		__this->set__enclosing_0(L_0);
		Base12_t2988184810 * L_1 = (Base12_t2988184810 *)__this->get__enclosing_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__length_0();
		__this->set__current_1(((int32_t)((int32_t)L_2-(int32_t)1)));
		return;
	}
}
// System.Boolean Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12/Enumerator216<System.Object,System.Object,System.Object,System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator216_MoveNext_m1132866179_gshared (Enumerator216_t3810636662 * __this, const MethodInfo* method)
{
	int64_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__current_1();
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0071;
		}
	}
	{
		Base12_t2988184810 * L_1 = (Base12_t2988184810 *)__this->get__enclosing_0();
		NullCheck(L_1);
		int64_t L_2 = (int64_t)L_1->get__value_1();
		int32_t L_3 = (int32_t)__this->get__current_1();
		V_0 = (int64_t)((int64_t)((int64_t)L_2&(int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_3&(int32_t)((int32_t)31))))))))));
		BooleanChunk_t3036858313 * L_4 = (BooleanChunk_t3036858313 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (BooleanChunk_t3036858313 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set__currentItem_2(L_4);
		BooleanChunk_t3036858313 * L_5 = (BooleanChunk_t3036858313 *)__this->get__currentItem_2();
		int64_t L_6 = V_0;
		NullCheck(L_5);
		L_5->set_IsOne_1((bool)((((int64_t)L_6) == ((int64_t)(((int64_t)((int64_t)0)))))? 1 : 0));
		BooleanChunk_t3036858313 * L_7 = (BooleanChunk_t3036858313 *)__this->get__currentItem_2();
		int32_t L_8 = (int32_t)__this->get__current_1();
		double L_9 = pow((double)(2.0), (double)(((double)((double)L_8))));
		NullCheck(L_7);
		L_7->set_ChunkSize_0((((int32_t)((int32_t)L_9))));
		int32_t L_10 = (int32_t)__this->get__current_1();
		__this->set__current_1(((int32_t)((int32_t)L_10-(int32_t)1)));
		return (bool)1;
	}

IL_0071:
	{
		return (bool)0;
	}
}
// Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/BooleanChunk<TK,TV,TA,TB,TC> Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12/Enumerator216<System.Object,System.Object,System.Object,System.Object,System.Object>::System.Collections.Generic.IEnumerator<Firebase.Database.Internal.Collection.RbTreeSortedMap<TK,TV>.BuilderAbc<TA,TB,TC>.BooleanChunk>.get_Current()
extern "C"  BooleanChunk_t3036858313 * Enumerator216_System_Collections_Generic_IEnumeratorU3CFirebase_Database_Internal_Collection_RbTreeSortedMapU3CTKU2CTVU3E_BuilderAbcU3CTAU2CTBU2CTCU3E_BooleanChunkU3E_get_Current_m4007318139_gshared (Enumerator216_t3810636662 * __this, const MethodInfo* method)
{
	{
		BooleanChunk_t3036858313 * L_0 = (BooleanChunk_t3036858313 *)__this->get__currentItem_2();
		return L_0;
	}
}
// System.Object Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12/Enumerator216<System.Object,System.Object,System.Object,System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator216_System_Collections_IEnumerator_get_Current_m1327421699_gshared (Enumerator216_t3810636662 * __this, const MethodInfo* method)
{
	{
		BooleanChunk_t3036858313 * L_0 = (BooleanChunk_t3036858313 *)__this->get__currentItem_2();
		return L_0;
	}
}
// System.Void Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12/Enumerator216<System.Object,System.Object,System.Object,System.Object,System.Object>::Dispose()
extern "C"  void Enumerator216_Dispose_m3119943996_gshared (Enumerator216_t3810636662 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12/Enumerator216<System.Object,System.Object,System.Object,System.Object,System.Object>::Reset()
extern "C"  void Enumerator216_Reset_m679673586_gshared (Enumerator216_t3810636662 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Int32)
extern "C"  void Base12__ctor_m440061024_gshared (Base12_t2988184810 * __this, int32_t ___size0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int64_t V_1 = 0;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___size0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)1));
		int32_t L_1 = V_0;
		double L_2 = log((double)(((double)((double)L_1))));
		double L_3 = log((double)(2.0));
		double L_4 = floor((double)((double)((double)L_2/(double)L_3)));
		__this->set__length_0((((int32_t)((int32_t)L_4))));
		int32_t L_5 = (int32_t)__this->get__length_0();
		double L_6 = pow((double)(2.0), (double)(((double)((double)L_5))));
		V_1 = (int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)L_6)))-(int64_t)(((int64_t)((int64_t)1)))));
		int32_t L_7 = V_0;
		int64_t L_8 = V_1;
		__this->set__value_1(((int64_t)((int64_t)(((int64_t)((int64_t)L_7)))&(int64_t)L_8)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/BooleanChunk<TK,TV,TA,TB,TC>> Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12<System.Object,System.Object,System.Object,System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* Base12_GetEnumerator_m3539400435_gshared (Base12_t2988184810 * __this, const MethodInfo* method)
{
	{
		Enumerator216_t3810636662 * L_0 = (Enumerator216_t3810636662 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Enumerator216_t3810636662 *, Base12_t2988184810 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, (Base12_t2988184810 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.IEnumerator Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12<System.Object,System.Object,System.Object,System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Base12_System_Collections_IEnumerable_GetEnumerator_m158844324_gshared (Base12_t2988184810 * __this, const MethodInfo* method)
{
	{
		Enumerator216_t3810636662 * L_0 = (Enumerator216_t3810636662 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Enumerator216_t3810636662 *, Base12_t2988184810 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, (Base12_t2988184810 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Void Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/BooleanChunk<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void BooleanChunk__ctor_m1163001482_gshared (BooleanChunk_t3036858313 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Collections.Generic.IList`1<TA>,System.Collections.Generic.IDictionary`2<TB,TC>,Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder/IKeyTranslator<TB,TC>)
extern "C"  void BuilderAbc_3__ctor_m2884185703_gshared (BuilderAbc_3_t1611920761 * __this, Il2CppObject* ___keys0, Il2CppObject* ___values1, Il2CppObject* ___translator2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___keys0;
		__this->set__keys_0(L_0);
		Il2CppObject* L_1 = ___values1;
		__this->set__values_2(L_1);
		Il2CppObject* L_2 = ___translator2;
		__this->set__keyTranslator_1(L_2);
		return;
	}
}
// TC Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3<System.Object,System.Object,System.Object,System.Object,System.Object>::GetValue(TA)
extern "C"  Il2CppObject * BuilderAbc_3_GetValue_m4036173926_gshared (BuilderAbc_3_t1611920761 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__values_2();
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__keyTranslator_1();
		Il2CppObject * L_2 = ___key0;
		NullCheck((Il2CppObject*)L_1);
		Il2CppObject * L_3 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Object Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder/IKeyTranslator<System.Object,System.Object>::Translate(System.Object) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), (Il2CppObject*)L_1, (Il2CppObject *)L_2);
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(3 /* !1 System.Collections.Generic.IDictionary`2<System.Object,System.Object>::get_Item(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_0, (Il2CppObject *)((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2))));
		return L_4;
	}
}
// Firebase.Database.Internal.Collection.LlrbNode`2<TA,TC> Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3<System.Object,System.Object,System.Object,System.Object,System.Object>::BuildBalancedTree(System.Int32,System.Int32)
extern "C"  LlrbNode_2_t4262869811 * BuilderAbc_3_BuildBalancedTree_m3112072366_gshared (BuilderAbc_3_t1611920761 * __this, int32_t ___start0, int32_t ___size1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	LlrbNode_2_t4262869811 * V_3 = NULL;
	LlrbNode_2_t4262869811 * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	{
		int32_t L_0 = ___size1;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		LlrbEmptyNode_2_t3030218886 * L_1 = ((  LlrbEmptyNode_2_t3030218886 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_1;
	}

IL_000c:
	{
		int32_t L_2 = ___size1;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0030;
		}
	}
	{
		Il2CppObject* L_3 = (Il2CppObject*)__this->get__keys_0();
		int32_t L_4 = ___start0;
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), (Il2CppObject*)L_3, (int32_t)L_4);
		V_0 = (Il2CppObject *)L_5;
		Il2CppObject * L_6 = V_0;
		Il2CppObject * L_7 = V_0;
		NullCheck((BuilderAbc_3_t1611920761 *)__this);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (BuilderAbc_3_t1611920761 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((BuilderAbc_3_t1611920761 *)__this, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		LlrbBlackValueNode_2_t3839725159 * L_9 = (LlrbBlackValueNode_2_t3839725159 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9));
		((  void (*) (LlrbBlackValueNode_2_t3839725159 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)(L_9, (Il2CppObject *)L_6, (Il2CppObject *)L_8, (LlrbNode_2_t4262869811 *)NULL, (LlrbNode_2_t4262869811 *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return L_9;
	}

IL_0030:
	{
		int32_t L_10 = ___size1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10/(int32_t)2));
		int32_t L_11 = ___start0;
		int32_t L_12 = V_1;
		V_2 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)L_12));
		int32_t L_13 = ___start0;
		int32_t L_14 = V_1;
		NullCheck((BuilderAbc_3_t1611920761 *)__this);
		LlrbNode_2_t4262869811 * L_15 = ((  LlrbNode_2_t4262869811 * (*) (BuilderAbc_3_t1611920761 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((BuilderAbc_3_t1611920761 *)__this, (int32_t)L_13, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_3 = (LlrbNode_2_t4262869811 *)L_15;
		int32_t L_16 = V_2;
		int32_t L_17 = V_1;
		NullCheck((BuilderAbc_3_t1611920761 *)__this);
		LlrbNode_2_t4262869811 * L_18 = ((  LlrbNode_2_t4262869811 * (*) (BuilderAbc_3_t1611920761 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((BuilderAbc_3_t1611920761 *)__this, (int32_t)((int32_t)((int32_t)L_16+(int32_t)1)), (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_4 = (LlrbNode_2_t4262869811 *)L_18;
		Il2CppObject* L_19 = (Il2CppObject*)__this->get__keys_0();
		int32_t L_20 = V_2;
		NullCheck((Il2CppObject*)L_19);
		Il2CppObject * L_21 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), (Il2CppObject*)L_19, (int32_t)L_20);
		V_5 = (Il2CppObject *)L_21;
		Il2CppObject * L_22 = V_5;
		Il2CppObject * L_23 = V_5;
		NullCheck((BuilderAbc_3_t1611920761 *)__this);
		Il2CppObject * L_24 = ((  Il2CppObject * (*) (BuilderAbc_3_t1611920761 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((BuilderAbc_3_t1611920761 *)__this, (Il2CppObject *)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		LlrbNode_2_t4262869811 * L_25 = V_3;
		LlrbNode_2_t4262869811 * L_26 = V_4;
		LlrbBlackValueNode_2_t3839725159 * L_27 = (LlrbBlackValueNode_2_t3839725159 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9));
		((  void (*) (LlrbBlackValueNode_2_t3839725159 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)(L_27, (Il2CppObject *)L_22, (Il2CppObject *)L_24, (LlrbNode_2_t4262869811 *)L_25, (LlrbNode_2_t4262869811 *)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return L_27;
	}
}
// System.Void Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3<System.Object,System.Object,System.Object,System.Object,System.Object>::BuildPennant(Firebase.Database.Internal.Collection.LlrbNode`2/Color<TA,TC>,System.Int32,System.Int32)
extern "C"  void BuilderAbc_3_BuildPennant_m4132420023_gshared (BuilderAbc_3_t1611920761 * __this, int32_t ___color0, int32_t ___chunkSize1, int32_t ___start2, const MethodInfo* method)
{
	LlrbNode_2_t4262869811 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	LlrbValueNode_2_t1296857666 * V_2 = NULL;
	{
		int32_t L_0 = ___start2;
		int32_t L_1 = ___chunkSize1;
		NullCheck((BuilderAbc_3_t1611920761 *)__this);
		LlrbNode_2_t4262869811 * L_2 = ((  LlrbNode_2_t4262869811 * (*) (BuilderAbc_3_t1611920761 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((BuilderAbc_3_t1611920761 *)__this, (int32_t)((int32_t)((int32_t)L_0+(int32_t)1)), (int32_t)((int32_t)((int32_t)L_1-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (LlrbNode_2_t4262869811 *)L_2;
		Il2CppObject* L_3 = (Il2CppObject*)__this->get__keys_0();
		int32_t L_4 = ___start2;
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject * L_5 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), (Il2CppObject*)L_3, (int32_t)L_4);
		V_1 = (Il2CppObject *)L_5;
		int32_t L_6 = ___color0;
		if (L_6)
		{
			goto IL_0035;
		}
	}
	{
		Il2CppObject * L_7 = V_1;
		Il2CppObject * L_8 = V_1;
		NullCheck((BuilderAbc_3_t1611920761 *)__this);
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (BuilderAbc_3_t1611920761 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((BuilderAbc_3_t1611920761 *)__this, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		LlrbNode_2_t4262869811 * L_10 = V_0;
		LlrbRedValueNode_2_t3628144835 * L_11 = (LlrbRedValueNode_2_t3628144835 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 12));
		((  void (*) (LlrbRedValueNode_2_t3628144835 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)(L_11, (Il2CppObject *)L_7, (Il2CppObject *)L_9, (LlrbNode_2_t4262869811 *)NULL, (LlrbNode_2_t4262869811 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		V_2 = (LlrbValueNode_2_t1296857666 *)L_11;
		goto IL_0045;
	}

IL_0035:
	{
		Il2CppObject * L_12 = V_1;
		Il2CppObject * L_13 = V_1;
		NullCheck((BuilderAbc_3_t1611920761 *)__this);
		Il2CppObject * L_14 = ((  Il2CppObject * (*) (BuilderAbc_3_t1611920761 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((BuilderAbc_3_t1611920761 *)__this, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		LlrbNode_2_t4262869811 * L_15 = V_0;
		LlrbBlackValueNode_2_t3839725159 * L_16 = (LlrbBlackValueNode_2_t3839725159 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9));
		((  void (*) (LlrbBlackValueNode_2_t3839725159 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)(L_16, (Il2CppObject *)L_12, (Il2CppObject *)L_14, (LlrbNode_2_t4262869811 *)NULL, (LlrbNode_2_t4262869811 *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		V_2 = (LlrbValueNode_2_t1296857666 *)L_16;
	}

IL_0045:
	{
		LlrbValueNode_2_t1296857666 * L_17 = (LlrbValueNode_2_t1296857666 *)__this->get__root_4();
		if (L_17)
		{
			goto IL_0063;
		}
	}
	{
		LlrbValueNode_2_t1296857666 * L_18 = V_2;
		__this->set__root_4(L_18);
		LlrbValueNode_2_t1296857666 * L_19 = V_2;
		__this->set__leaf_3(L_19);
		goto IL_0076;
	}

IL_0063:
	{
		LlrbValueNode_2_t1296857666 * L_20 = (LlrbValueNode_2_t1296857666 *)__this->get__leaf_3();
		LlrbValueNode_2_t1296857666 * L_21 = V_2;
		NullCheck((LlrbValueNode_2_t1296857666 *)L_20);
		VirtActionInvoker1< LlrbNode_2_t4262869811 * >::Invoke(19 /* System.Void Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>::SetLeft(Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>) */, (LlrbValueNode_2_t1296857666 *)L_20, (LlrbNode_2_t4262869811 *)L_21);
		LlrbValueNode_2_t1296857666 * L_22 = V_2;
		__this->set__leaf_3(L_22);
	}

IL_0076:
	{
		return;
	}
}
// Firebase.Database.Internal.Collection.RbTreeSortedMap`2<TA,TC> Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3<System.Object,System.Object,System.Object,System.Object,System.Object>::BuildFrom(System.Collections.Generic.IList`1<TA>,System.Collections.Generic.IDictionary`2<TB,TC>,Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder/IKeyTranslator<TB,TC>,System.Collections.Generic.IComparer`1<TA>)
extern Il2CppClass* Extensions_t996338116_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern const uint32_t BuilderAbc_3_BuildFrom_m3513364847_MetadataUsageId;
extern "C"  RbTreeSortedMap_2_t791914602 * BuilderAbc_3_BuildFrom_m3513364847_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___keys0, Il2CppObject* ___values1, Il2CppObject* ___translator2, Il2CppObject* ___comparator3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BuilderAbc_3_BuildFrom_m3513364847_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BuilderAbc_3_t1611920761 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	int32_t V_2 = 0;
	BooleanChunk_t3036858313 * V_3 = NULL;
	LlrbNode_2_t4262869811 * V_4 = NULL;
	{
		Il2CppObject* L_0 = ___keys0;
		Il2CppObject* L_1 = ___values1;
		Il2CppObject* L_2 = ___translator2;
		BuilderAbc_3_t1611920761 * L_3 = (BuilderAbc_3_t1611920761 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		((  void (*) (BuilderAbc_3_t1611920761 *, Il2CppObject*, Il2CppObject*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->methodPointer)(L_3, (Il2CppObject*)L_0, (Il2CppObject*)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		V_0 = (BuilderAbc_3_t1611920761 *)L_3;
		Il2CppObject* L_4 = ___keys0;
		Il2CppObject* L_5 = ___comparator3;
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t996338116_il2cpp_TypeInfo_var);
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_4, (Il2CppObject*)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		Il2CppObject* L_6 = ___keys0;
		NullCheck((Il2CppObject*)L_6);
		int32_t L_7 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18), (Il2CppObject*)L_6);
		Base12_t2988184810 * L_8 = (Base12_t2988184810 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		((  void (*) (Base12_t2988184810 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20)->methodPointer)(L_8, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20));
		NullCheck((Base12_t2988184810 *)L_8);
		Il2CppObject* L_9 = ((  Il2CppObject* (*) (Base12_t2988184810 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21)->methodPointer)((Base12_t2988184810 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21));
		V_1 = (Il2CppObject*)L_9;
		Il2CppObject* L_10 = ___keys0;
		NullCheck((Il2CppObject*)L_10);
		int32_t L_11 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18), (Il2CppObject*)L_10);
		V_2 = (int32_t)L_11;
		goto IL_0080;
	}

IL_002d:
	{
		Il2CppObject* L_12 = V_1;
		NullCheck((Il2CppObject*)L_12);
		BooleanChunk_t3036858313 * L_13 = InterfaceFuncInvoker0< BooleanChunk_t3036858313 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/BooleanChunk<System.Object,System.Object,System.Object,System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22), (Il2CppObject*)L_12);
		V_3 = (BooleanChunk_t3036858313 *)L_13;
		int32_t L_14 = V_2;
		BooleanChunk_t3036858313 * L_15 = V_3;
		NullCheck(L_15);
		int32_t L_16 = (int32_t)L_15->get_ChunkSize_0();
		V_2 = (int32_t)((int32_t)((int32_t)L_14-(int32_t)L_16));
		BooleanChunk_t3036858313 * L_17 = V_3;
		NullCheck(L_17);
		bool L_18 = (bool)L_17->get_IsOne_1();
		if (!L_18)
		{
			goto IL_005b;
		}
	}
	{
		BuilderAbc_3_t1611920761 * L_19 = V_0;
		BooleanChunk_t3036858313 * L_20 = V_3;
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_ChunkSize_0();
		int32_t L_22 = V_2;
		NullCheck((BuilderAbc_3_t1611920761 *)L_19);
		((  void (*) (BuilderAbc_3_t1611920761 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->methodPointer)((BuilderAbc_3_t1611920761 *)L_19, (int32_t)1, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		goto IL_0080;
	}

IL_005b:
	{
		BuilderAbc_3_t1611920761 * L_23 = V_0;
		BooleanChunk_t3036858313 * L_24 = V_3;
		NullCheck(L_24);
		int32_t L_25 = (int32_t)L_24->get_ChunkSize_0();
		int32_t L_26 = V_2;
		NullCheck((BuilderAbc_3_t1611920761 *)L_23);
		((  void (*) (BuilderAbc_3_t1611920761 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->methodPointer)((BuilderAbc_3_t1611920761 *)L_23, (int32_t)1, (int32_t)L_25, (int32_t)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		int32_t L_27 = V_2;
		BooleanChunk_t3036858313 * L_28 = V_3;
		NullCheck(L_28);
		int32_t L_29 = (int32_t)L_28->get_ChunkSize_0();
		V_2 = (int32_t)((int32_t)((int32_t)L_27-(int32_t)L_29));
		BuilderAbc_3_t1611920761 * L_30 = V_0;
		BooleanChunk_t3036858313 * L_31 = V_3;
		NullCheck(L_31);
		int32_t L_32 = (int32_t)L_31->get_ChunkSize_0();
		int32_t L_33 = V_2;
		NullCheck((BuilderAbc_3_t1611920761 *)L_30);
		((  void (*) (BuilderAbc_3_t1611920761 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->methodPointer)((BuilderAbc_3_t1611920761 *)L_30, (int32_t)0, (int32_t)L_32, (int32_t)L_33, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
	}

IL_0080:
	{
		Il2CppObject* L_34 = V_1;
		NullCheck((Il2CppObject *)L_34);
		bool L_35 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_34);
		if (L_35)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		LlrbEmptyNode_2_t3030218886 * L_36 = ((  LlrbEmptyNode_2_t3030218886 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_4 = (LlrbNode_2_t4262869811 *)L_36;
		BuilderAbc_3_t1611920761 * L_37 = V_0;
		NullCheck(L_37);
		LlrbValueNode_2_t1296857666 * L_38 = (LlrbValueNode_2_t1296857666 *)L_37->get__root_4();
		if (!L_38)
		{
			goto IL_00a5;
		}
	}
	{
		BuilderAbc_3_t1611920761 * L_39 = V_0;
		NullCheck(L_39);
		LlrbValueNode_2_t1296857666 * L_40 = (LlrbValueNode_2_t1296857666 *)L_39->get__root_4();
		V_4 = (LlrbNode_2_t4262869811 *)L_40;
	}

IL_00a5:
	{
		LlrbNode_2_t4262869811 * L_41 = V_4;
		Il2CppObject* L_42 = ___comparator3;
		RbTreeSortedMap_2_t791914602 * L_43 = (RbTreeSortedMap_2_t791914602 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		((  void (*) (RbTreeSortedMap_2_t791914602 *, LlrbNode_2_t4262869811 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->methodPointer)(L_43, (LlrbNode_2_t4262869811 *)L_41, (Il2CppObject*)L_42, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		return L_43;
	}
}
// System.Void Firebase.Database.Internal.Collection.RbTreeSortedMap`2<System.Object,System.Object>::.ctor(Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,System.Collections.Generic.IComparer`1<TK>)
extern "C"  void RbTreeSortedMap_2__ctor_m3316159118_gshared (RbTreeSortedMap_2_t791914602 * __this, LlrbNode_2_t4262869811 * ___root0, Il2CppObject* ___comparator1, const MethodInfo* method)
{
	{
		NullCheck((ImmutableSortedMap_2_t3217094540 *)__this);
		((  void (*) (ImmutableSortedMap_2_t3217094540 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ImmutableSortedMap_2_t3217094540 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		LlrbNode_2_t4262869811 * L_0 = ___root0;
		__this->set__root_1(L_0);
		Il2CppObject* L_1 = ___comparator1;
		__this->set__comparator_0(L_1);
		return;
	}
}
// Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.RbTreeSortedMap`2<System.Object,System.Object>::GetNode(TK)
extern "C"  LlrbNode_2_t4262869811 * RbTreeSortedMap_2_GetNode_m726917759_gshared (RbTreeSortedMap_2_t791914602 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	LlrbNode_2_t4262869811 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		LlrbNode_2_t4262869811 * L_0 = (LlrbNode_2_t4262869811 *)__this->get__root_1();
		V_0 = (LlrbNode_2_t4262869811 *)L_0;
		goto IL_0041;
	}

IL_000c:
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__comparator_0();
		Il2CppObject * L_2 = ___key0;
		LlrbNode_2_t4262869811 * L_3 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_3);
		Il2CppObject * L_4 = VirtFuncInvoker0< Il2CppObject * >::Invoke(9 /* TK Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetKey() */, (LlrbNode_2_t4262869811 *)L_3);
		NullCheck((Il2CppObject*)L_1);
		int32_t L_5 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Object>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_4);
		V_1 = (int32_t)L_5;
		int32_t L_6 = V_1;
		if ((((int32_t)L_6) >= ((int32_t)0)))
		{
			goto IL_0032;
		}
	}
	{
		LlrbNode_2_t4262869811 * L_7 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_7);
		LlrbNode_2_t4262869811 * L_8 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(11 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetLeft() */, (LlrbNode_2_t4262869811 *)L_7);
		V_0 = (LlrbNode_2_t4262869811 *)L_8;
		goto IL_0041;
	}

IL_0032:
	{
		int32_t L_9 = V_1;
		if (L_9)
		{
			goto IL_003a;
		}
	}
	{
		LlrbNode_2_t4262869811 * L_10 = V_0;
		return L_10;
	}

IL_003a:
	{
		LlrbNode_2_t4262869811 * L_11 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_11);
		LlrbNode_2_t4262869811 * L_12 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(12 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetRight() */, (LlrbNode_2_t4262869811 *)L_11);
		V_0 = (LlrbNode_2_t4262869811 *)L_12;
	}

IL_0041:
	{
		LlrbNode_2_t4262869811 * L_13 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_13);
		bool L_14 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsEmpty() */, (LlrbNode_2_t4262869811 *)L_13);
		if (!L_14)
		{
			goto IL_000c;
		}
	}
	{
		return (LlrbNode_2_t4262869811 *)NULL;
	}
}
// System.Boolean Firebase.Database.Internal.Collection.RbTreeSortedMap`2<System.Object,System.Object>::ContainsKey(TK)
extern "C"  bool RbTreeSortedMap_2_ContainsKey_m2568327266_gshared (RbTreeSortedMap_2_t791914602 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((RbTreeSortedMap_2_t791914602 *)__this);
		LlrbNode_2_t4262869811 * L_1 = ((  LlrbNode_2_t4262869811 * (*) (RbTreeSortedMap_2_t791914602 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((RbTreeSortedMap_2_t791914602 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return (bool)((((int32_t)((((Il2CppObject*)(LlrbNode_2_t4262869811 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// TV Firebase.Database.Internal.Collection.RbTreeSortedMap`2<System.Object,System.Object>::Get(TK)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t RbTreeSortedMap_2_Get_m1997454941_MetadataUsageId;
extern "C"  Il2CppObject * RbTreeSortedMap_2_Get_m1997454941_gshared (RbTreeSortedMap_2_t791914602 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RbTreeSortedMap_2_Get_m1997454941_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LlrbNode_2_t4262869811 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((RbTreeSortedMap_2_t791914602 *)__this);
		LlrbNode_2_t4262869811 * L_1 = ((  LlrbNode_2_t4262869811 * (*) (RbTreeSortedMap_2_t791914602 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((RbTreeSortedMap_2_t791914602 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		V_0 = (LlrbNode_2_t4262869811 *)L_1;
		LlrbNode_2_t4262869811 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		LlrbNode_2_t4262869811 * L_3 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_3);
		Il2CppObject * L_4 = VirtFuncInvoker0< Il2CppObject * >::Invoke(10 /* TV Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetValue() */, (LlrbNode_2_t4262869811 *)L_3);
		G_B3_0 = L_4;
		goto IL_0022;
	}

IL_0019:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_5 = V_1;
		G_B3_0 = L_5;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// Firebase.Database.Internal.Collection.ImmutableSortedMap`2<TK,TV> Firebase.Database.Internal.Collection.RbTreeSortedMap`2<System.Object,System.Object>::Remove(TK)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t RbTreeSortedMap_2_Remove_m3907863786_MetadataUsageId;
extern "C"  ImmutableSortedMap_2_t3217094540 * RbTreeSortedMap_2_Remove_m3907863786_gshared (RbTreeSortedMap_2_t791914602 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RbTreeSortedMap_2_Remove_m3907863786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LlrbNode_2_t4262869811 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((ImmutableSortedMap_2_t3217094540 *)__this);
		bool L_1 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(7 /* System.Boolean Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::ContainsKey(TK) */, (ImmutableSortedMap_2_t3217094540 *)__this, (Il2CppObject *)L_0);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return __this;
	}

IL_000e:
	{
		LlrbNode_2_t4262869811 * L_2 = (LlrbNode_2_t4262869811 *)__this->get__root_1();
		Il2CppObject * L_3 = ___key0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get__comparator_0();
		NullCheck((LlrbNode_2_t4262869811 *)L_2);
		LlrbNode_2_t4262869811 * L_5 = VirtFuncInvoker2< LlrbNode_2_t4262869811 *, Il2CppObject *, Il2CppObject* >::Invoke(6 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::Remove(TK,System.Collections.Generic.IComparer`1<TK>) */, (LlrbNode_2_t4262869811 *)L_2, (Il2CppObject *)L_3, (Il2CppObject*)L_4);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_6 = V_1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_7 = V_2;
		NullCheck((LlrbNode_2_t4262869811 *)L_5);
		LlrbNode_2_t4262869811 * L_8 = VirtFuncInvoker5< LlrbNode_2_t4262869811 *, Il2CppObject *, Il2CppObject *, int32_t, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 * >::Invoke(4 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2/Color<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>) */, (LlrbNode_2_t4262869811 *)L_5, (Il2CppObject *)L_6, (Il2CppObject *)L_7, (int32_t)1, (LlrbNode_2_t4262869811 *)NULL, (LlrbNode_2_t4262869811 *)NULL);
		V_0 = (LlrbNode_2_t4262869811 *)L_8;
		LlrbNode_2_t4262869811 * L_9 = V_0;
		Il2CppObject* L_10 = (Il2CppObject*)__this->get__comparator_0();
		RbTreeSortedMap_2_t791914602 * L_11 = (RbTreeSortedMap_2_t791914602 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 12));
		((  void (*) (RbTreeSortedMap_2_t791914602 *, LlrbNode_2_t4262869811 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)(L_11, (LlrbNode_2_t4262869811 *)L_9, (Il2CppObject*)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_11;
	}
}
// Firebase.Database.Internal.Collection.ImmutableSortedMap`2<TK,TV> Firebase.Database.Internal.Collection.RbTreeSortedMap`2<System.Object,System.Object>::Insert(TK,TV)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t RbTreeSortedMap_2_Insert_m691565085_MetadataUsageId;
extern "C"  ImmutableSortedMap_2_t3217094540 * RbTreeSortedMap_2_Insert_m691565085_gshared (RbTreeSortedMap_2_t791914602 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RbTreeSortedMap_2_Insert_m691565085_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LlrbNode_2_t4262869811 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		LlrbNode_2_t4262869811 * L_0 = (LlrbNode_2_t4262869811 *)__this->get__root_1();
		Il2CppObject * L_1 = ___key0;
		Il2CppObject * L_2 = ___value1;
		Il2CppObject* L_3 = (Il2CppObject*)__this->get__comparator_0();
		NullCheck((LlrbNode_2_t4262869811 *)L_0);
		LlrbNode_2_t4262869811 * L_4 = VirtFuncInvoker3< LlrbNode_2_t4262869811 *, Il2CppObject *, Il2CppObject *, Il2CppObject* >::Invoke(5 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::Insert(TK,TV,System.Collections.Generic.IComparer`1<TK>) */, (LlrbNode_2_t4262869811 *)L_0, (Il2CppObject *)L_1, (Il2CppObject *)L_2, (Il2CppObject*)L_3);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_5 = V_1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_6 = V_2;
		NullCheck((LlrbNode_2_t4262869811 *)L_4);
		LlrbNode_2_t4262869811 * L_7 = VirtFuncInvoker5< LlrbNode_2_t4262869811 *, Il2CppObject *, Il2CppObject *, int32_t, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 * >::Invoke(4 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2/Color<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>) */, (LlrbNode_2_t4262869811 *)L_4, (Il2CppObject *)L_5, (Il2CppObject *)L_6, (int32_t)1, (LlrbNode_2_t4262869811 *)NULL, (LlrbNode_2_t4262869811 *)NULL);
		V_0 = (LlrbNode_2_t4262869811 *)L_7;
		LlrbNode_2_t4262869811 * L_8 = V_0;
		Il2CppObject* L_9 = (Il2CppObject*)__this->get__comparator_0();
		RbTreeSortedMap_2_t791914602 * L_10 = (RbTreeSortedMap_2_t791914602 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 12));
		((  void (*) (RbTreeSortedMap_2_t791914602 *, LlrbNode_2_t4262869811 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)(L_10, (LlrbNode_2_t4262869811 *)L_8, (Il2CppObject*)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_10;
	}
}
// TK Firebase.Database.Internal.Collection.RbTreeSortedMap`2<System.Object,System.Object>::GetMinKey()
extern "C"  Il2CppObject * RbTreeSortedMap_2_GetMinKey_m337601112_gshared (RbTreeSortedMap_2_t791914602 * __this, const MethodInfo* method)
{
	{
		LlrbNode_2_t4262869811 * L_0 = (LlrbNode_2_t4262869811 *)__this->get__root_1();
		NullCheck((LlrbNode_2_t4262869811 *)L_0);
		LlrbNode_2_t4262869811 * L_1 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(13 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetMin() */, (LlrbNode_2_t4262869811 *)L_0);
		NullCheck((LlrbNode_2_t4262869811 *)L_1);
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(9 /* TK Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetKey() */, (LlrbNode_2_t4262869811 *)L_1);
		return L_2;
	}
}
// TK Firebase.Database.Internal.Collection.RbTreeSortedMap`2<System.Object,System.Object>::GetMaxKey()
extern "C"  Il2CppObject * RbTreeSortedMap_2_GetMaxKey_m1733774194_gshared (RbTreeSortedMap_2_t791914602 * __this, const MethodInfo* method)
{
	{
		LlrbNode_2_t4262869811 * L_0 = (LlrbNode_2_t4262869811 *)__this->get__root_1();
		NullCheck((LlrbNode_2_t4262869811 *)L_0);
		LlrbNode_2_t4262869811 * L_1 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(14 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetMax() */, (LlrbNode_2_t4262869811 *)L_0);
		NullCheck((LlrbNode_2_t4262869811 *)L_1);
		Il2CppObject * L_2 = VirtFuncInvoker0< Il2CppObject * >::Invoke(9 /* TK Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetKey() */, (LlrbNode_2_t4262869811 *)L_1);
		return L_2;
	}
}
// System.Int32 Firebase.Database.Internal.Collection.RbTreeSortedMap`2<System.Object,System.Object>::Size()
extern "C"  int32_t RbTreeSortedMap_2_Size_m4289517240_gshared (RbTreeSortedMap_2_t791914602 * __this, const MethodInfo* method)
{
	{
		LlrbNode_2_t4262869811 * L_0 = (LlrbNode_2_t4262869811 *)__this->get__root_1();
		NullCheck((LlrbNode_2_t4262869811 *)L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::Count() */, (LlrbNode_2_t4262869811 *)L_0);
		return L_1;
	}
}
// System.Boolean Firebase.Database.Internal.Collection.RbTreeSortedMap`2<System.Object,System.Object>::IsEmpty()
extern "C"  bool RbTreeSortedMap_2_IsEmpty_m3650965036_gshared (RbTreeSortedMap_2_t791914602 * __this, const MethodInfo* method)
{
	{
		LlrbNode_2_t4262869811 * L_0 = (LlrbNode_2_t4262869811 *)__this->get__root_1();
		NullCheck((LlrbNode_2_t4262869811 *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsEmpty() */, (LlrbNode_2_t4262869811 *)L_0);
		return L_1;
	}
}
// System.Void Firebase.Database.Internal.Collection.RbTreeSortedMap`2<System.Object,System.Object>::InOrderTraversal(Firebase.Database.Internal.Collection.LlrbNode`2/NodeVisitor<TK,TV>)
extern "C"  void RbTreeSortedMap_2_InOrderTraversal_m4262549487_gshared (RbTreeSortedMap_2_t791914602 * __this, NodeVisitor_t2944196615 * ___visitor0, const MethodInfo* method)
{
	{
		LlrbNode_2_t4262869811 * L_0 = (LlrbNode_2_t4262869811 *)__this->get__root_1();
		NodeVisitor_t2944196615 * L_1 = ___visitor0;
		NullCheck((LlrbNode_2_t4262869811 *)L_0);
		VirtActionInvoker1< NodeVisitor_t2944196615 * >::Invoke(16 /* System.Void Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::InOrderTraversal(Firebase.Database.Internal.Collection.LlrbNode`2/NodeVisitor<TK,TV>) */, (LlrbNode_2_t4262869811 *)L_0, (NodeVisitor_t2944196615 *)L_1);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TK,TV>> Firebase.Database.Internal.Collection.RbTreeSortedMap`2<System.Object,System.Object>::GetEnumerator()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t RbTreeSortedMap_2_GetEnumerator_m4061730193_MetadataUsageId;
extern "C"  Il2CppObject* RbTreeSortedMap_2_GetEnumerator_m4061730193_gshared (RbTreeSortedMap_2_t791914602 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RbTreeSortedMap_2_GetEnumerator_m4061730193_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		LlrbNode_2_t4262869811 * L_0 = (LlrbNode_2_t4262869811 *)__this->get__root_1();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_1 = V_0;
		Il2CppObject* L_2 = (Il2CppObject*)__this->get__comparator_0();
		ImmutableSortedMapIterator_2_t2384854130 * L_3 = (ImmutableSortedMapIterator_2_t2384854130 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 19));
		((  void (*) (ImmutableSortedMapIterator_2_t2384854130 *, LlrbNode_2_t4262869811 *, Il2CppObject *, Il2CppObject*, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)(L_3, (LlrbNode_2_t4262869811 *)L_0, (Il2CppObject *)L_1, (Il2CppObject*)L_2, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TK,TV>> Firebase.Database.Internal.Collection.RbTreeSortedMap`2<System.Object,System.Object>::ReverseIterator()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t RbTreeSortedMap_2_ReverseIterator_m3656546497_MetadataUsageId;
extern "C"  Il2CppObject* RbTreeSortedMap_2_ReverseIterator_m3656546497_gshared (RbTreeSortedMap_2_t791914602 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RbTreeSortedMap_2_ReverseIterator_m3656546497_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		LlrbNode_2_t4262869811 * L_0 = (LlrbNode_2_t4262869811 *)__this->get__root_1();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_1 = V_0;
		Il2CppObject* L_2 = (Il2CppObject*)__this->get__comparator_0();
		ImmutableSortedMapIterator_2_t2384854130 * L_3 = (ImmutableSortedMapIterator_2_t2384854130 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 19));
		((  void (*) (ImmutableSortedMapIterator_2_t2384854130 *, LlrbNode_2_t4262869811 *, Il2CppObject *, Il2CppObject*, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)(L_3, (LlrbNode_2_t4262869811 *)L_0, (Il2CppObject *)L_1, (Il2CppObject*)L_2, (bool)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		return L_3;
	}
}
// TK Firebase.Database.Internal.Collection.RbTreeSortedMap`2<System.Object,System.Object>::GetPredecessorKey(TK)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1808017102;
extern const uint32_t RbTreeSortedMap_2_GetPredecessorKey_m1189796600_MetadataUsageId;
extern "C"  Il2CppObject * RbTreeSortedMap_2_GetPredecessorKey_m1189796600_gshared (RbTreeSortedMap_2_t791914602 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RbTreeSortedMap_2_GetPredecessorKey_m1189796600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LlrbNode_2_t4262869811 * V_0 = NULL;
	LlrbNode_2_t4262869811 * V_1 = NULL;
	int32_t V_2 = 0;
	Il2CppObject * V_3 = NULL;
	{
		LlrbNode_2_t4262869811 * L_0 = (LlrbNode_2_t4262869811 *)__this->get__root_1();
		V_0 = (LlrbNode_2_t4262869811 *)L_0;
		V_1 = (LlrbNode_2_t4262869811 *)NULL;
		goto IL_0094;
	}

IL_000e:
	{
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__comparator_0();
		Il2CppObject * L_2 = ___key0;
		LlrbNode_2_t4262869811 * L_3 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_3);
		Il2CppObject * L_4 = VirtFuncInvoker0< Il2CppObject * >::Invoke(9 /* TK Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetKey() */, (LlrbNode_2_t4262869811 *)L_3);
		NullCheck((Il2CppObject*)L_1);
		int32_t L_5 = InterfaceFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IComparer`1<System.Object>::Compare(!0,!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3), (Il2CppObject*)L_1, (Il2CppObject *)L_2, (Il2CppObject *)L_4);
		V_2 = (int32_t)L_5;
		int32_t L_6 = V_2;
		if (L_6)
		{
			goto IL_0078;
		}
	}
	{
		LlrbNode_2_t4262869811 * L_7 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_7);
		LlrbNode_2_t4262869811 * L_8 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(11 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetLeft() */, (LlrbNode_2_t4262869811 *)L_7);
		NullCheck((LlrbNode_2_t4262869811 *)L_8);
		bool L_9 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsEmpty() */, (LlrbNode_2_t4262869811 *)L_8);
		if (L_9)
		{
			goto IL_0061;
		}
	}
	{
		LlrbNode_2_t4262869811 * L_10 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_10);
		LlrbNode_2_t4262869811 * L_11 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(11 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetLeft() */, (LlrbNode_2_t4262869811 *)L_10);
		V_0 = (LlrbNode_2_t4262869811 *)L_11;
		goto IL_004a;
	}

IL_0043:
	{
		LlrbNode_2_t4262869811 * L_12 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_12);
		LlrbNode_2_t4262869811 * L_13 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(12 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetRight() */, (LlrbNode_2_t4262869811 *)L_12);
		V_0 = (LlrbNode_2_t4262869811 *)L_13;
	}

IL_004a:
	{
		LlrbNode_2_t4262869811 * L_14 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_14);
		LlrbNode_2_t4262869811 * L_15 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(12 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetRight() */, (LlrbNode_2_t4262869811 *)L_14);
		NullCheck((LlrbNode_2_t4262869811 *)L_15);
		bool L_16 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsEmpty() */, (LlrbNode_2_t4262869811 *)L_15);
		if (!L_16)
		{
			goto IL_0043;
		}
	}
	{
		LlrbNode_2_t4262869811 * L_17 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_17);
		Il2CppObject * L_18 = VirtFuncInvoker0< Il2CppObject * >::Invoke(9 /* TK Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetKey() */, (LlrbNode_2_t4262869811 *)L_17);
		return L_18;
	}

IL_0061:
	{
		LlrbNode_2_t4262869811 * L_19 = V_1;
		if (!L_19)
		{
			goto IL_006e;
		}
	}
	{
		LlrbNode_2_t4262869811 * L_20 = V_1;
		NullCheck((LlrbNode_2_t4262869811 *)L_20);
		Il2CppObject * L_21 = VirtFuncInvoker0< Il2CppObject * >::Invoke(9 /* TK Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetKey() */, (LlrbNode_2_t4262869811 *)L_20);
		return L_21;
	}

IL_006e:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_22 = V_3;
		return L_22;
	}

IL_0078:
	{
		int32_t L_23 = V_2;
		if ((((int32_t)L_23) >= ((int32_t)0)))
		{
			goto IL_008b;
		}
	}
	{
		LlrbNode_2_t4262869811 * L_24 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_24);
		LlrbNode_2_t4262869811 * L_25 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(11 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetLeft() */, (LlrbNode_2_t4262869811 *)L_24);
		V_0 = (LlrbNode_2_t4262869811 *)L_25;
		goto IL_0094;
	}

IL_008b:
	{
		LlrbNode_2_t4262869811 * L_26 = V_0;
		V_1 = (LlrbNode_2_t4262869811 *)L_26;
		LlrbNode_2_t4262869811 * L_27 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_27);
		LlrbNode_2_t4262869811 * L_28 = VirtFuncInvoker0< LlrbNode_2_t4262869811 * >::Invoke(12 /* Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::GetRight() */, (LlrbNode_2_t4262869811 *)L_27);
		V_0 = (LlrbNode_2_t4262869811 *)L_28;
	}

IL_0094:
	{
		LlrbNode_2_t4262869811 * L_29 = V_0;
		NullCheck((LlrbNode_2_t4262869811 *)L_29);
		bool L_30 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::IsEmpty() */, (LlrbNode_2_t4262869811 *)L_29);
		if (!L_30)
		{
			goto IL_000e;
		}
	}
	{
		Il2CppObject * L_31 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m56707527(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1808017102, (Il2CppObject *)L_31, /*hidden argument*/NULL);
		ArgumentException_t3259014390 * L_33 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_33, (String_t*)L_32, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_33);
	}
}
// System.Collections.Generic.IComparer`1<TK> Firebase.Database.Internal.Collection.RbTreeSortedMap`2<System.Object,System.Object>::GetComparator()
extern "C"  Il2CppObject* RbTreeSortedMap_2_GetComparator_m2623139801_gshared (RbTreeSortedMap_2_t791914602 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__comparator_0();
		return L_0;
	}
}
// System.Void Firebase.Database.Internal.Collection.StandardComparator`1<System.Object>::.ctor()
extern "C"  void StandardComparator_1__ctor_m3122475602_gshared (StandardComparator_1_t2182977910 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Firebase.Database.Internal.Collection.StandardComparator`1<System.Object>::Compare(TA,TA)
extern "C"  int32_t StandardComparator_1_Compare_m517984725_gshared (StandardComparator_1_t2182977910 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___y1;
		NullCheck((Il2CppObject*)(*(&___x0)));
		int32_t L_1 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable`1<System.Object>::CompareTo(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), (Il2CppObject*)(*(&___x0)), (Il2CppObject *)L_0);
		return L_1;
	}
}
// Firebase.Database.Internal.Collection.StandardComparator`1<TA> Firebase.Database.Internal.Collection.StandardComparator`1<System.Object>::GetComparator()
extern "C"  StandardComparator_1_t2182977910 * StandardComparator_1_GetComparator_m3420190789_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		StandardComparator_1_t2182977910 * L_0 = ((StandardComparator_1_t2182977910_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get_Instance_0();
		return L_0;
	}
}
// System.Void Firebase.Database.Internal.Collection.StandardComparator`1<System.Object>::.cctor()
extern "C"  void StandardComparator_1__cctor_m3768252471_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		StandardComparator_1_t2182977910 * L_0 = (StandardComparator_1_t2182977910 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (StandardComparator_1_t2182977910 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((StandardComparator_1_t2182977910_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set_Instance_0(L_0);
		return;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.ImmutableTree`1/TreeVisitor278<System.Nullable`1<System.Boolean>>::.ctor(System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,T>>)
extern "C"  void TreeVisitor278__ctor_m12229935_gshared (TreeVisitor278_t934374925 * __this, Il2CppObject* ___list0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___list0;
		__this->set__list_0(L_0);
		return;
	}
}
// System.Object Firebase.Database.Internal.Core.Utilities.ImmutableTree`1/TreeVisitor278<System.Nullable`1<System.Boolean>>::OnNodeValue(Firebase.Database.Internal.Core.Path,T,System.Object)
extern "C"  Il2CppObject * TreeVisitor278_OnNodeValue_m2796073726_gshared (TreeVisitor278_t934374925 * __this, Path_t2568473163 * ___relativePath0, Nullable_1_t2088641033  ___value1, Il2CppObject * ___accum2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__list_0();
		Path_t2568473163 * L_1 = ___relativePath0;
		Nullable_1_t2088641033  L_2 = ___value1;
		KeyValuePair_2_t3450460227  L_3;
		memset(&L_3, 0, sizeof(L_3));
		KeyValuePair_2__ctor_m3805376634(&L_3, (Path_t2568473163 *)L_1, (Nullable_1_t2088641033 )L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< KeyValuePair_2_t3450460227  >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>>::Add(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0, (KeyValuePair_2_t3450460227 )L_3);
		return NULL;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.ImmutableTree`1/TreeVisitor278<System.Object>::.ctor(System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,T>>)
extern "C"  void TreeVisitor278__ctor_m1845275067_gshared (TreeVisitor278_t1535183187 * __this, Il2CppObject* ___list0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___list0;
		__this->set__list_0(L_0);
		return;
	}
}
// System.Object Firebase.Database.Internal.Core.Utilities.ImmutableTree`1/TreeVisitor278<System.Object>::OnNodeValue(Firebase.Database.Internal.Core.Path,T,System.Object)
extern "C"  Il2CppObject * TreeVisitor278_OnNodeValue_m2174579068_gshared (TreeVisitor278_t1535183187 * __this, Path_t2568473163 * ___relativePath0, Il2CppObject * ___value1, Il2CppObject * ___accum2, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get__list_0();
		Path_t2568473163 * L_1 = ___relativePath0;
		Il2CppObject * L_2 = ___value1;
		KeyValuePair_2_t4051268489  L_3;
		memset(&L_3, 0, sizeof(L_3));
		KeyValuePair_2__ctor_m21461360(&L_3, (Path_t2568473163 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< KeyValuePair_2_t4051268489  >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Object>>::Add(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0, (KeyValuePair_2_t4051268489 )L_3);
		return NULL;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::.ctor(T,Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>)
extern "C"  void ImmutableTree_1__ctor_m1801583884_gshared (ImmutableTree_1_t368550071 * __this, Nullable_1_t2088641033  ___value0, ImmutableSortedMap_2_t94277708 * ___children1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Nullable_1_t2088641033  L_0 = ___value0;
		__this->set__value_3(L_0);
		ImmutableSortedMap_2_t94277708 * L_1 = ___children1;
		__this->set__children_2(L_1);
		return;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::.ctor(T)
extern "C"  void ImmutableTree_1__ctor_m1738413240_gshared (ImmutableTree_1_t368550071 * __this, Nullable_1_t2088641033  ___value0, const MethodInfo* method)
{
	{
		Nullable_1_t2088641033  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ImmutableSortedMap_2_t94277708 * L_1 = ((ImmutableTree_1_t368550071_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyChildren_0();
		NullCheck((ImmutableTree_1_t368550071 *)__this);
		((  void (*) (ImmutableTree_1_t368550071 *, Nullable_1_t2088641033 , ImmutableSortedMap_2_t94277708 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ImmutableTree_1_t368550071 *)__this, (Nullable_1_t2088641033 )L_0, (ImmutableSortedMap_2_t94277708 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,T>> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::GetEnumerator()
extern "C"  Il2CppObject* ImmutableTree_1_GetEnumerator_m1799115700_gshared (ImmutableTree_1_t368550071 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		List_1_t2819581359 * L_0 = (List_1_t2819581359 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (List_1_t2819581359 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (Il2CppObject*)L_0;
		Il2CppObject* L_1 = V_0;
		TreeVisitor278_t934374925 * L_2 = (TreeVisitor278_t934374925 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (TreeVisitor278_t934374925 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_2, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((ImmutableTree_1_t368550071 *)__this);
		VirtActionInvoker1< Il2CppObject* >::Invoke(17 /* System.Void Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::Foreach(Firebase.Database.Internal.Core.Utilities.ImmutableTree`1/ITreeVisitor`1<T,System.Object>) */, (ImmutableTree_1_t368550071 *)__this, (Il2CppObject*)L_2);
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Collections.IEnumerator Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ImmutableTree_1_System_Collections_IEnumerable_GetEnumerator_m2023205285_gshared (ImmutableTree_1_t368550071 * __this, const MethodInfo* method)
{
	{
		NullCheck((ImmutableTree_1_t368550071 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ImmutableTree_1_t368550071 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ImmutableTree_1_t368550071 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_0;
	}
}
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::EmptyInstance()
extern "C"  ImmutableTree_1_t368550071 * ImmutableTree_1_EmptyInstance_m2573230590_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ImmutableTree_1_t368550071 * L_0 = ((ImmutableTree_1_t368550071_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Empty_1();
		return L_0;
	}
}
// T Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::GetValue()
extern "C"  Nullable_1_t2088641033  ImmutableTree_1_GetValue_m2767026044_gshared (ImmutableTree_1_t368550071 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t2088641033  L_0 = (Nullable_1_t2088641033 )__this->get__value_3();
		return L_0;
	}
}
// Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::GetChildren()
extern "C"  ImmutableSortedMap_2_t94277708 * ImmutableTree_1_GetChildren_m1070085586_gshared (ImmutableTree_1_t368550071 * __this, const MethodInfo* method)
{
	{
		ImmutableSortedMap_2_t94277708 * L_0 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		return L_0;
	}
}
// System.Boolean Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::IsEmpty()
extern "C"  bool ImmutableTree_1_IsEmpty_m2012344195_gshared (ImmutableTree_1_t368550071 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Nullable_1_t2088641033  L_0 = (Nullable_1_t2088641033 )__this->get__value_3();
		Nullable_1_t2088641033  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), &L_1);
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		ImmutableSortedMap_2_t94277708 * L_3 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_3);
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::IsEmpty() */, (ImmutableSortedMap_2_t94277708 *)L_3);
		G_B3_0 = ((int32_t)(L_4));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::FindRootMostMatchingPath(Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.Utilities.Predicate`1<T>)
extern Il2CppClass* Path_t2568473163_il2cpp_TypeInfo_var;
extern Il2CppClass* ChildKeyU5BU5D_t491817302_il2cpp_TypeInfo_var;
extern const uint32_t ImmutableTree_1_FindRootMostMatchingPath_m411676773_MetadataUsageId;
extern "C"  Path_t2568473163 * ImmutableTree_1_FindRootMostMatchingPath_m411676773_gshared (ImmutableTree_1_t368550071 * __this, Path_t2568473163 * ___relativePath0, Predicate_1_t605805710 * ___predicate1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImmutableTree_1_FindRootMostMatchingPath_m411676773_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ChildKey_t1197802383 * V_0 = NULL;
	ImmutableTree_1_t368550071 * V_1 = NULL;
	Path_t2568473163 * V_2 = NULL;
	{
		Nullable_1_t2088641033  L_0 = (Nullable_1_t2088641033 )__this->get__value_3();
		Nullable_1_t2088641033  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), &L_1);
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		Predicate_1_t605805710 * L_3 = ___predicate1;
		Nullable_1_t2088641033  L_4 = (Nullable_1_t2088641033 )__this->get__value_3();
		NullCheck((Predicate_1_t605805710 *)L_3);
		bool L_5 = VirtFuncInvoker1< bool, Nullable_1_t2088641033  >::Invoke(4 /* System.Boolean Firebase.Database.Internal.Core.Utilities.Predicate`1<System.Nullable`1<System.Boolean>>::Evaluate(T) */, (Predicate_1_t605805710 *)L_3, (Nullable_1_t2088641033 )L_4);
		if (!L_5)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Path_t2568473163_il2cpp_TypeInfo_var);
		Path_t2568473163 * L_6 = Path_GetEmptyPath_m3969996076(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_6;
	}

IL_0027:
	{
		Path_t2568473163 * L_7 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_7);
		bool L_8 = VirtFuncInvoker0< bool >::Invoke(17 /* System.Boolean Firebase.Database.Internal.Core.Path::IsEmpty() */, (Path_t2568473163 *)L_7);
		if (!L_8)
		{
			goto IL_0034;
		}
	}
	{
		return (Path_t2568473163 *)NULL;
	}

IL_0034:
	{
		Path_t2568473163 * L_9 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_9);
		ChildKey_t1197802383 * L_10 = VirtFuncInvoker0< ChildKey_t1197802383 * >::Invoke(13 /* Firebase.Database.Internal.Snapshot.ChildKey Firebase.Database.Internal.Core.Path::GetFront() */, (Path_t2568473163 *)L_9);
		V_0 = (ChildKey_t1197802383 *)L_10;
		ImmutableSortedMap_2_t94277708 * L_11 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_12 = V_0;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_11);
		Il2CppObject * L_13 = VirtFuncInvoker1< Il2CppObject *, ChildKey_t1197802383 * >::Invoke(8 /* TV Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Get(TK) */, (ImmutableSortedMap_2_t94277708 *)L_11, (ChildKey_t1197802383 *)L_12);
		V_1 = (ImmutableTree_1_t368550071 *)((ImmutableTree_1_t368550071 *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)));
		ImmutableTree_1_t368550071 * L_14 = V_1;
		if (!L_14)
		{
			goto IL_007f;
		}
	}
	{
		ImmutableTree_1_t368550071 * L_15 = V_1;
		Path_t2568473163 * L_16 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_16);
		Path_t2568473163 * L_17 = VirtFuncInvoker0< Path_t2568473163 * >::Invoke(14 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Path::PopFront() */, (Path_t2568473163 *)L_16);
		Predicate_1_t605805710 * L_18 = ___predicate1;
		NullCheck((ImmutableTree_1_t368550071 *)L_15);
		Path_t2568473163 * L_19 = VirtFuncInvoker2< Path_t2568473163 *, Path_t2568473163 *, Predicate_1_t605805710 * >::Invoke(9 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::FindRootMostMatchingPath(Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.Utilities.Predicate`1<T>) */, (ImmutableTree_1_t368550071 *)L_15, (Path_t2568473163 *)L_17, (Predicate_1_t605805710 *)L_18);
		V_2 = (Path_t2568473163 *)L_19;
		Path_t2568473163 * L_20 = V_2;
		if (!L_20)
		{
			goto IL_007d;
		}
	}
	{
		ChildKeyU5BU5D_t491817302* L_21 = (ChildKeyU5BU5D_t491817302*)((ChildKeyU5BU5D_t491817302*)SZArrayNew(ChildKeyU5BU5D_t491817302_il2cpp_TypeInfo_var, (uint32_t)1));
		ChildKey_t1197802383 * L_22 = V_0;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (ChildKey_t1197802383 *)L_22);
		Path_t2568473163 * L_23 = (Path_t2568473163 *)il2cpp_codegen_object_new(Path_t2568473163_il2cpp_TypeInfo_var);
		Path__ctor_m2358381126(L_23, (ChildKeyU5BU5D_t491817302*)L_21, /*hidden argument*/NULL);
		Path_t2568473163 * L_24 = V_2;
		NullCheck((Path_t2568473163 *)L_23);
		Path_t2568473163 * L_25 = VirtFuncInvoker1< Path_t2568473163 *, Path_t2568473163 * >::Invoke(9 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Path::Child(Firebase.Database.Internal.Core.Path) */, (Path_t2568473163 *)L_23, (Path_t2568473163 *)L_24);
		return L_25;
	}

IL_007d:
	{
		return (Path_t2568473163 *)NULL;
	}

IL_007f:
	{
		return (Path_t2568473163 *)NULL;
	}
}
// Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::FindRootMostPathWithValue(Firebase.Database.Internal.Core.Path)
extern "C"  Path_t2568473163 * ImmutableTree_1_FindRootMostPathWithValue_m1449986173_gshared (ImmutableTree_1_t368550071 * __this, Path_t2568473163 * ___relativePath0, const MethodInfo* method)
{
	{
		Path_t2568473163 * L_0 = ___relativePath0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13));
		Predicate_1_t605805710 * L_1 = ((Predicate_1_t605805710_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13)->static_fields)->get_True_0();
		NullCheck((ImmutableTree_1_t368550071 *)__this);
		Path_t2568473163 * L_2 = VirtFuncInvoker2< Path_t2568473163 *, Path_t2568473163 *, Predicate_1_t605805710 * >::Invoke(9 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::FindRootMostMatchingPath(Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.Utilities.Predicate`1<T>) */, (ImmutableTree_1_t368550071 *)__this, (Path_t2568473163 *)L_0, (Predicate_1_t605805710 *)L_1);
		return L_2;
	}
}
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::GetChild(Firebase.Database.Internal.Snapshot.ChildKey)
extern "C"  ImmutableTree_1_t368550071 * ImmutableTree_1_GetChild_m3783161626_gshared (ImmutableTree_1_t368550071 * __this, ChildKey_t1197802383 * ___child0, const MethodInfo* method)
{
	ImmutableTree_1_t368550071 * V_0 = NULL;
	{
		ImmutableSortedMap_2_t94277708 * L_0 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_1 = ___child0;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, ChildKey_t1197802383 * >::Invoke(8 /* TV Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Get(TK) */, (ImmutableSortedMap_2_t94277708 *)L_0, (ChildKey_t1197802383 *)L_1);
		V_0 = (ImmutableTree_1_t368550071 *)((ImmutableTree_1_t368550071 *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)));
		ImmutableTree_1_t368550071 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		ImmutableTree_1_t368550071 * L_4 = V_0;
		return L_4;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ImmutableTree_1_t368550071 * L_5 = ((  ImmutableTree_1_t368550071 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_5;
	}
}
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::Subtree(Firebase.Database.Internal.Core.Path)
extern "C"  ImmutableTree_1_t368550071 * ImmutableTree_1_Subtree_m206536087_gshared (ImmutableTree_1_t368550071 * __this, Path_t2568473163 * ___relativePath0, const MethodInfo* method)
{
	ChildKey_t1197802383 * V_0 = NULL;
	ImmutableTree_1_t368550071 * V_1 = NULL;
	{
		Path_t2568473163 * L_0 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(17 /* System.Boolean Firebase.Database.Internal.Core.Path::IsEmpty() */, (Path_t2568473163 *)L_0);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return __this;
	}

IL_000d:
	{
		Path_t2568473163 * L_2 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_2);
		ChildKey_t1197802383 * L_3 = VirtFuncInvoker0< ChildKey_t1197802383 * >::Invoke(13 /* Firebase.Database.Internal.Snapshot.ChildKey Firebase.Database.Internal.Core.Path::GetFront() */, (Path_t2568473163 *)L_2);
		V_0 = (ChildKey_t1197802383 *)L_3;
		ImmutableSortedMap_2_t94277708 * L_4 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_5 = V_0;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_4);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, ChildKey_t1197802383 * >::Invoke(8 /* TV Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Get(TK) */, (ImmutableSortedMap_2_t94277708 *)L_4, (ChildKey_t1197802383 *)L_5);
		V_1 = (ImmutableTree_1_t368550071 *)((ImmutableTree_1_t368550071 *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)));
		ImmutableTree_1_t368550071 * L_7 = V_1;
		if (!L_7)
		{
			goto IL_0039;
		}
	}
	{
		ImmutableTree_1_t368550071 * L_8 = V_1;
		Path_t2568473163 * L_9 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_9);
		Path_t2568473163 * L_10 = VirtFuncInvoker0< Path_t2568473163 * >::Invoke(14 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Path::PopFront() */, (Path_t2568473163 *)L_9);
		NullCheck((ImmutableTree_1_t368550071 *)L_8);
		ImmutableTree_1_t368550071 * L_11 = VirtFuncInvoker1< ImmutableTree_1_t368550071 *, Path_t2568473163 * >::Invoke(12 /* Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::Subtree(Firebase.Database.Internal.Core.Path) */, (ImmutableTree_1_t368550071 *)L_8, (Path_t2568473163 *)L_10);
		return L_11;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ImmutableTree_1_t368550071 * L_12 = ((  ImmutableTree_1_t368550071 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_12;
	}
}
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::Set(Firebase.Database.Internal.Core.Path,T)
extern "C"  ImmutableTree_1_t368550071 * ImmutableTree_1_Set_m495016611_gshared (ImmutableTree_1_t368550071 * __this, Path_t2568473163 * ___relativePath0, Nullable_1_t2088641033  ___value1, const MethodInfo* method)
{
	ChildKey_t1197802383 * V_0 = NULL;
	ImmutableTree_1_t368550071 * V_1 = NULL;
	ImmutableTree_1_t368550071 * V_2 = NULL;
	ImmutableSortedMap_2_t94277708 * V_3 = NULL;
	{
		Path_t2568473163 * L_0 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(17 /* System.Boolean Firebase.Database.Internal.Core.Path::IsEmpty() */, (Path_t2568473163 *)L_0);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		Nullable_1_t2088641033  L_2 = ___value1;
		ImmutableSortedMap_2_t94277708 * L_3 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ImmutableTree_1_t368550071 * L_4 = (ImmutableTree_1_t368550071 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		((  void (*) (ImmutableTree_1_t368550071 *, Nullable_1_t2088641033 , ImmutableSortedMap_2_t94277708 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_4, (Nullable_1_t2088641033 )L_2, (ImmutableSortedMap_2_t94277708 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_4;
	}

IL_0018:
	{
		Path_t2568473163 * L_5 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_5);
		ChildKey_t1197802383 * L_6 = VirtFuncInvoker0< ChildKey_t1197802383 * >::Invoke(13 /* Firebase.Database.Internal.Snapshot.ChildKey Firebase.Database.Internal.Core.Path::GetFront() */, (Path_t2568473163 *)L_5);
		V_0 = (ChildKey_t1197802383 *)L_6;
		ImmutableSortedMap_2_t94277708 * L_7 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_8 = V_0;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_7);
		Il2CppObject * L_9 = VirtFuncInvoker1< Il2CppObject *, ChildKey_t1197802383 * >::Invoke(8 /* TV Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Get(TK) */, (ImmutableSortedMap_2_t94277708 *)L_7, (ChildKey_t1197802383 *)L_8);
		V_1 = (ImmutableTree_1_t368550071 *)((ImmutableTree_1_t368550071 *)Castclass(L_9, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)));
		ImmutableTree_1_t368550071 * L_10 = V_1;
		if (L_10)
		{
			goto IL_003d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ImmutableTree_1_t368550071 * L_11 = ((  ImmutableTree_1_t368550071 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_1 = (ImmutableTree_1_t368550071 *)L_11;
	}

IL_003d:
	{
		ImmutableTree_1_t368550071 * L_12 = V_1;
		Path_t2568473163 * L_13 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_13);
		Path_t2568473163 * L_14 = VirtFuncInvoker0< Path_t2568473163 * >::Invoke(14 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Path::PopFront() */, (Path_t2568473163 *)L_13);
		Nullable_1_t2088641033  L_15 = ___value1;
		NullCheck((ImmutableTree_1_t368550071 *)L_12);
		ImmutableTree_1_t368550071 * L_16 = VirtFuncInvoker2< ImmutableTree_1_t368550071 *, Path_t2568473163 *, Nullable_1_t2088641033  >::Invoke(13 /* Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::Set(Firebase.Database.Internal.Core.Path,T) */, (ImmutableTree_1_t368550071 *)L_12, (Path_t2568473163 *)L_14, (Nullable_1_t2088641033 )L_15);
		V_2 = (ImmutableTree_1_t368550071 *)L_16;
		ImmutableSortedMap_2_t94277708 * L_17 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_18 = V_0;
		ImmutableTree_1_t368550071 * L_19 = V_2;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_17);
		ImmutableSortedMap_2_t94277708 * L_20 = VirtFuncInvoker2< ImmutableSortedMap_2_t94277708 *, ChildKey_t1197802383 *, Il2CppObject * >::Invoke(10 /* Firebase.Database.Internal.Collection.ImmutableSortedMap`2<TK,TV> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Insert(TK,TV) */, (ImmutableSortedMap_2_t94277708 *)L_17, (ChildKey_t1197802383 *)L_18, (Il2CppObject *)L_19);
		V_3 = (ImmutableSortedMap_2_t94277708 *)L_20;
		Nullable_1_t2088641033  L_21 = (Nullable_1_t2088641033 )__this->get__value_3();
		ImmutableSortedMap_2_t94277708 * L_22 = V_3;
		ImmutableTree_1_t368550071 * L_23 = (ImmutableTree_1_t368550071 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		((  void (*) (ImmutableTree_1_t368550071 *, Nullable_1_t2088641033 , ImmutableSortedMap_2_t94277708 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_23, (Nullable_1_t2088641033 )L_21, (ImmutableSortedMap_2_t94277708 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_23;
	}
}
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::Remove(Firebase.Database.Internal.Core.Path)
extern Il2CppClass* Nullable_1_t2088641033_il2cpp_TypeInfo_var;
extern const uint32_t ImmutableTree_1_Remove_m3151072967_MetadataUsageId;
extern "C"  ImmutableTree_1_t368550071 * ImmutableTree_1_Remove_m3151072967_gshared (ImmutableTree_1_t368550071 * __this, Path_t2568473163 * ___relativePath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImmutableTree_1_Remove_m3151072967_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_t2088641033  V_0;
	memset(&V_0, 0, sizeof(V_0));
	ChildKey_t1197802383 * V_1 = NULL;
	ImmutableTree_1_t368550071 * V_2 = NULL;
	ImmutableTree_1_t368550071 * V_3 = NULL;
	ImmutableSortedMap_2_t94277708 * V_4 = NULL;
	{
		Path_t2568473163 * L_0 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(17 /* System.Boolean Firebase.Database.Internal.Core.Path::IsEmpty() */, (Path_t2568473163 *)L_0);
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		ImmutableSortedMap_2_t94277708 * L_2 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::IsEmpty() */, (ImmutableSortedMap_2_t94277708 *)L_2);
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ImmutableTree_1_t368550071 * L_4 = ((  ImmutableTree_1_t368550071 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_4;
	}

IL_0021:
	{
		Initobj (Nullable_1_t2088641033_il2cpp_TypeInfo_var, (&V_0));
		Nullable_1_t2088641033  L_5 = V_0;
		ImmutableSortedMap_2_t94277708 * L_6 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ImmutableTree_1_t368550071 * L_7 = (ImmutableTree_1_t368550071 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		((  void (*) (ImmutableTree_1_t368550071 *, Nullable_1_t2088641033 , ImmutableSortedMap_2_t94277708 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_7, (Nullable_1_t2088641033 )L_5, (ImmutableSortedMap_2_t94277708 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_7;
	}

IL_0036:
	{
		Path_t2568473163 * L_8 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_8);
		ChildKey_t1197802383 * L_9 = VirtFuncInvoker0< ChildKey_t1197802383 * >::Invoke(13 /* Firebase.Database.Internal.Snapshot.ChildKey Firebase.Database.Internal.Core.Path::GetFront() */, (Path_t2568473163 *)L_8);
		V_1 = (ChildKey_t1197802383 *)L_9;
		ImmutableSortedMap_2_t94277708 * L_10 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_11 = V_1;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_10);
		Il2CppObject * L_12 = VirtFuncInvoker1< Il2CppObject *, ChildKey_t1197802383 * >::Invoke(8 /* TV Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Get(TK) */, (ImmutableSortedMap_2_t94277708 *)L_10, (ChildKey_t1197802383 *)L_11);
		V_2 = (ImmutableTree_1_t368550071 *)((ImmutableTree_1_t368550071 *)Castclass(L_12, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)));
		ImmutableTree_1_t368550071 * L_13 = V_2;
		if (!L_13)
		{
			goto IL_00bf;
		}
	}
	{
		ImmutableTree_1_t368550071 * L_14 = V_2;
		Path_t2568473163 * L_15 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_15);
		Path_t2568473163 * L_16 = VirtFuncInvoker0< Path_t2568473163 * >::Invoke(14 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Path::PopFront() */, (Path_t2568473163 *)L_15);
		NullCheck((ImmutableTree_1_t368550071 *)L_14);
		ImmutableTree_1_t368550071 * L_17 = VirtFuncInvoker1< ImmutableTree_1_t368550071 *, Path_t2568473163 * >::Invoke(14 /* Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::Remove(Firebase.Database.Internal.Core.Path) */, (ImmutableTree_1_t368550071 *)L_14, (Path_t2568473163 *)L_16);
		V_3 = (ImmutableTree_1_t368550071 *)L_17;
		ImmutableTree_1_t368550071 * L_18 = V_3;
		NullCheck((ImmutableTree_1_t368550071 *)L_18);
		bool L_19 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::IsEmpty() */, (ImmutableTree_1_t368550071 *)L_18);
		if (!L_19)
		{
			goto IL_0080;
		}
	}
	{
		ImmutableSortedMap_2_t94277708 * L_20 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_21 = V_1;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_20);
		ImmutableSortedMap_2_t94277708 * L_22 = VirtFuncInvoker1< ImmutableSortedMap_2_t94277708 *, ChildKey_t1197802383 * >::Invoke(9 /* Firebase.Database.Internal.Collection.ImmutableSortedMap`2<TK,TV> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Remove(TK) */, (ImmutableSortedMap_2_t94277708 *)L_20, (ChildKey_t1197802383 *)L_21);
		V_4 = (ImmutableSortedMap_2_t94277708 *)L_22;
		goto IL_008f;
	}

IL_0080:
	{
		ImmutableSortedMap_2_t94277708 * L_23 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_24 = V_1;
		ImmutableTree_1_t368550071 * L_25 = V_3;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_23);
		ImmutableSortedMap_2_t94277708 * L_26 = VirtFuncInvoker2< ImmutableSortedMap_2_t94277708 *, ChildKey_t1197802383 *, Il2CppObject * >::Invoke(10 /* Firebase.Database.Internal.Collection.ImmutableSortedMap`2<TK,TV> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Insert(TK,TV) */, (ImmutableSortedMap_2_t94277708 *)L_23, (ChildKey_t1197802383 *)L_24, (Il2CppObject *)L_25);
		V_4 = (ImmutableSortedMap_2_t94277708 *)L_26;
	}

IL_008f:
	{
		Nullable_1_t2088641033  L_27 = (Nullable_1_t2088641033 )__this->get__value_3();
		Nullable_1_t2088641033  L_28 = L_27;
		Il2CppObject * L_29 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), &L_28);
		if (L_29)
		{
			goto IL_00b1;
		}
	}
	{
		ImmutableSortedMap_2_t94277708 * L_30 = V_4;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_30);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::IsEmpty() */, (ImmutableSortedMap_2_t94277708 *)L_30);
		if (!L_31)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ImmutableTree_1_t368550071 * L_32 = ((  ImmutableTree_1_t368550071 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_32;
	}

IL_00b1:
	{
		Nullable_1_t2088641033  L_33 = (Nullable_1_t2088641033 )__this->get__value_3();
		ImmutableSortedMap_2_t94277708 * L_34 = V_4;
		ImmutableTree_1_t368550071 * L_35 = (ImmutableTree_1_t368550071 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		((  void (*) (ImmutableTree_1_t368550071 *, Nullable_1_t2088641033 , ImmutableSortedMap_2_t94277708 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_35, (Nullable_1_t2088641033 )L_33, (ImmutableSortedMap_2_t94277708 *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_35;
	}

IL_00bf:
	{
		return __this;
	}
}
// T Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::Get(Firebase.Database.Internal.Core.Path)
extern Il2CppClass* Nullable_1_t2088641033_il2cpp_TypeInfo_var;
extern const uint32_t ImmutableTree_1_Get_m1924369382_MetadataUsageId;
extern "C"  Nullable_1_t2088641033  ImmutableTree_1_Get_m1924369382_gshared (ImmutableTree_1_t368550071 * __this, Path_t2568473163 * ___relativePath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImmutableTree_1_Get_m1924369382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ChildKey_t1197802383 * V_0 = NULL;
	ImmutableTree_1_t368550071 * V_1 = NULL;
	Nullable_1_t2088641033  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Path_t2568473163 * L_0 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(17 /* System.Boolean Firebase.Database.Internal.Core.Path::IsEmpty() */, (Path_t2568473163 *)L_0);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		Nullable_1_t2088641033  L_2 = (Nullable_1_t2088641033 )__this->get__value_3();
		return L_2;
	}

IL_0012:
	{
		Path_t2568473163 * L_3 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_3);
		ChildKey_t1197802383 * L_4 = VirtFuncInvoker0< ChildKey_t1197802383 * >::Invoke(13 /* Firebase.Database.Internal.Snapshot.ChildKey Firebase.Database.Internal.Core.Path::GetFront() */, (Path_t2568473163 *)L_3);
		V_0 = (ChildKey_t1197802383 *)L_4;
		ImmutableSortedMap_2_t94277708 * L_5 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_6 = V_0;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_5);
		Il2CppObject * L_7 = VirtFuncInvoker1< Il2CppObject *, ChildKey_t1197802383 * >::Invoke(8 /* TV Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Get(TK) */, (ImmutableSortedMap_2_t94277708 *)L_5, (ChildKey_t1197802383 *)L_6);
		V_1 = (ImmutableTree_1_t368550071 *)((ImmutableTree_1_t368550071 *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)));
		ImmutableTree_1_t368550071 * L_8 = V_1;
		if (!L_8)
		{
			goto IL_003e;
		}
	}
	{
		ImmutableTree_1_t368550071 * L_9 = V_1;
		Path_t2568473163 * L_10 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_10);
		Path_t2568473163 * L_11 = VirtFuncInvoker0< Path_t2568473163 * >::Invoke(14 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Path::PopFront() */, (Path_t2568473163 *)L_10);
		NullCheck((ImmutableTree_1_t368550071 *)L_9);
		Nullable_1_t2088641033  L_12 = VirtFuncInvoker1< Nullable_1_t2088641033 , Path_t2568473163 * >::Invoke(15 /* T Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::Get(Firebase.Database.Internal.Core.Path) */, (ImmutableTree_1_t368550071 *)L_9, (Path_t2568473163 *)L_11);
		return L_12;
	}

IL_003e:
	{
		Initobj (Nullable_1_t2088641033_il2cpp_TypeInfo_var, (&V_2));
		Nullable_1_t2088641033  L_13 = V_2;
		return L_13;
	}
}
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::SetTree(Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T>)
extern "C"  ImmutableTree_1_t368550071 * ImmutableTree_1_SetTree_m902619558_gshared (ImmutableTree_1_t368550071 * __this, Path_t2568473163 * ___relativePath0, ImmutableTree_1_t368550071 * ___newTree1, const MethodInfo* method)
{
	ChildKey_t1197802383 * V_0 = NULL;
	ImmutableTree_1_t368550071 * V_1 = NULL;
	ImmutableTree_1_t368550071 * V_2 = NULL;
	ImmutableSortedMap_2_t94277708 * V_3 = NULL;
	{
		Path_t2568473163 * L_0 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(17 /* System.Boolean Firebase.Database.Internal.Core.Path::IsEmpty() */, (Path_t2568473163 *)L_0);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		ImmutableTree_1_t368550071 * L_2 = ___newTree1;
		return L_2;
	}

IL_000d:
	{
		Path_t2568473163 * L_3 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_3);
		ChildKey_t1197802383 * L_4 = VirtFuncInvoker0< ChildKey_t1197802383 * >::Invoke(13 /* Firebase.Database.Internal.Snapshot.ChildKey Firebase.Database.Internal.Core.Path::GetFront() */, (Path_t2568473163 *)L_3);
		V_0 = (ChildKey_t1197802383 *)L_4;
		ImmutableSortedMap_2_t94277708 * L_5 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_6 = V_0;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_5);
		Il2CppObject * L_7 = VirtFuncInvoker1< Il2CppObject *, ChildKey_t1197802383 * >::Invoke(8 /* TV Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Get(TK) */, (ImmutableSortedMap_2_t94277708 *)L_5, (ChildKey_t1197802383 *)L_6);
		V_1 = (ImmutableTree_1_t368550071 *)((ImmutableTree_1_t368550071 *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)));
		ImmutableTree_1_t368550071 * L_8 = V_1;
		if (L_8)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ImmutableTree_1_t368550071 * L_9 = ((  ImmutableTree_1_t368550071 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_1 = (ImmutableTree_1_t368550071 *)L_9;
	}

IL_0032:
	{
		ImmutableTree_1_t368550071 * L_10 = V_1;
		Path_t2568473163 * L_11 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_11);
		Path_t2568473163 * L_12 = VirtFuncInvoker0< Path_t2568473163 * >::Invoke(14 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Path::PopFront() */, (Path_t2568473163 *)L_11);
		ImmutableTree_1_t368550071 * L_13 = ___newTree1;
		NullCheck((ImmutableTree_1_t368550071 *)L_10);
		ImmutableTree_1_t368550071 * L_14 = VirtFuncInvoker2< ImmutableTree_1_t368550071 *, Path_t2568473163 *, ImmutableTree_1_t368550071 * >::Invoke(16 /* Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::SetTree(Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T>) */, (ImmutableTree_1_t368550071 *)L_10, (Path_t2568473163 *)L_12, (ImmutableTree_1_t368550071 *)L_13);
		V_2 = (ImmutableTree_1_t368550071 *)L_14;
		ImmutableTree_1_t368550071 * L_15 = V_2;
		NullCheck((ImmutableTree_1_t368550071 *)L_15);
		bool L_16 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::IsEmpty() */, (ImmutableTree_1_t368550071 *)L_15);
		if (!L_16)
		{
			goto IL_005d;
		}
	}
	{
		ImmutableSortedMap_2_t94277708 * L_17 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_18 = V_0;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_17);
		ImmutableSortedMap_2_t94277708 * L_19 = VirtFuncInvoker1< ImmutableSortedMap_2_t94277708 *, ChildKey_t1197802383 * >::Invoke(9 /* Firebase.Database.Internal.Collection.ImmutableSortedMap`2<TK,TV> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Remove(TK) */, (ImmutableSortedMap_2_t94277708 *)L_17, (ChildKey_t1197802383 *)L_18);
		V_3 = (ImmutableSortedMap_2_t94277708 *)L_19;
		goto IL_006b;
	}

IL_005d:
	{
		ImmutableSortedMap_2_t94277708 * L_20 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_21 = V_0;
		ImmutableTree_1_t368550071 * L_22 = V_2;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_20);
		ImmutableSortedMap_2_t94277708 * L_23 = VirtFuncInvoker2< ImmutableSortedMap_2_t94277708 *, ChildKey_t1197802383 *, Il2CppObject * >::Invoke(10 /* Firebase.Database.Internal.Collection.ImmutableSortedMap`2<TK,TV> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Insert(TK,TV) */, (ImmutableSortedMap_2_t94277708 *)L_20, (ChildKey_t1197802383 *)L_21, (Il2CppObject *)L_22);
		V_3 = (ImmutableSortedMap_2_t94277708 *)L_23;
	}

IL_006b:
	{
		Nullable_1_t2088641033  L_24 = (Nullable_1_t2088641033 )__this->get__value_3();
		ImmutableSortedMap_2_t94277708 * L_25 = V_3;
		ImmutableTree_1_t368550071 * L_26 = (ImmutableTree_1_t368550071 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		((  void (*) (ImmutableTree_1_t368550071 *, Nullable_1_t2088641033 , ImmutableSortedMap_2_t94277708 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_26, (Nullable_1_t2088641033 )L_24, (ImmutableSortedMap_2_t94277708 *)L_25, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_26;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::Foreach(Firebase.Database.Internal.Core.Utilities.ImmutableTree`1/ITreeVisitor`1<T,System.Object>)
extern Il2CppClass* Path_t2568473163_il2cpp_TypeInfo_var;
extern const uint32_t ImmutableTree_1_Foreach_m555067238_MetadataUsageId;
extern "C"  void ImmutableTree_1_Foreach_m555067238_gshared (ImmutableTree_1_t368550071 * __this, Il2CppObject* ___visitor0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImmutableTree_1_Foreach_m555067238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Path_t2568473163_il2cpp_TypeInfo_var);
		Path_t2568473163 * L_0 = Path_GetEmptyPath_m3969996076(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___visitor0;
		NullCheck((ImmutableTree_1_t368550071 *)__this);
		((  Il2CppObject * (*) (ImmutableTree_1_t368550071 *, Path_t2568473163 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((ImmutableTree_1_t368550071 *)__this, (Path_t2568473163 *)L_0, (Il2CppObject*)L_1, (Il2CppObject *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		return;
	}
}
// System.String Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::ToString()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2981496232_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m182894088_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m689905909_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3758586953;
extern Il2CppCodeGenString* _stringLiteral3551194621;
extern Il2CppCodeGenString* _stringLiteral372029329;
extern Il2CppCodeGenString* _stringLiteral816989470;
extern const uint32_t ImmutableTree_1_ToString_m450004739_MetadataUsageId;
extern "C"  String_t* ImmutableTree_1_ToString_m450004739_gshared (ImmutableTree_1_t368550071 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImmutableTree_1_ToString_m450004739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	KeyValuePair_2_t1211005109  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Il2CppObject* V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1221177846 *)L_0;
		StringBuilder_t1221177846 * L_1 = V_0;
		NullCheck((StringBuilder_t1221177846 *)L_1);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_1, (String_t*)_stringLiteral3758586953, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_2 = V_0;
		NullCheck((ImmutableTree_1_t368550071 *)__this);
		Nullable_1_t2088641033  L_3 = VirtFuncInvoker0< Nullable_1_t2088641033  >::Invoke(6 /* T Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::GetValue() */, (ImmutableTree_1_t368550071 *)__this);
		Nullable_1_t2088641033  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), &L_4);
		NullCheck((StringBuilder_t1221177846 *)L_2);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_2, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_6 = V_0;
		NullCheck((StringBuilder_t1221177846 *)L_6);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_6, (String_t*)_stringLiteral3551194621, /*hidden argument*/NULL);
		ImmutableSortedMap_2_t94277708 * L_7 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_7);
		Il2CppObject* L_8 = VirtFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TK,TV>> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::GetEnumerator() */, (ImmutableSortedMap_2_t94277708 *)L_7);
		V_2 = (Il2CppObject*)L_8;
	}

IL_003c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0075;
		}

IL_0041:
		{
			Il2CppObject* L_9 = V_2;
			NullCheck((Il2CppObject*)L_9);
			KeyValuePair_2_t1211005109  L_10 = InterfaceFuncInvoker0< KeyValuePair_2_t1211005109  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>>::get_Current() */, IEnumerator_1_t2981496232_il2cpp_TypeInfo_var, (Il2CppObject*)L_9);
			V_1 = (KeyValuePair_2_t1211005109 )L_10;
			StringBuilder_t1221177846 * L_11 = V_0;
			ChildKey_t1197802383 * L_12 = KeyValuePair_2_get_Key_m182894088((KeyValuePair_2_t1211005109 *)(&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m182894088_MethodInfo_var);
			NullCheck((ChildKey_t1197802383 *)L_12);
			String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Firebase.Database.Internal.Snapshot.ChildKey::AsString() */, (ChildKey_t1197802383 *)L_12);
			NullCheck((StringBuilder_t1221177846 *)L_11);
			StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_11, (String_t*)L_13, /*hidden argument*/NULL);
			StringBuilder_t1221177846 * L_14 = V_0;
			NullCheck((StringBuilder_t1221177846 *)L_14);
			StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_14, (String_t*)_stringLiteral372029329, /*hidden argument*/NULL);
			StringBuilder_t1221177846 * L_15 = V_0;
			Il2CppObject * L_16 = KeyValuePair_2_get_Value_m689905909((KeyValuePair_2_t1211005109 *)(&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m689905909_MethodInfo_var);
			NullCheck((StringBuilder_t1221177846 *)L_15);
			StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_15, (Il2CppObject *)L_16, /*hidden argument*/NULL);
		}

IL_0075:
		{
			Il2CppObject* L_17 = V_2;
			NullCheck((Il2CppObject *)L_17);
			bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_17);
			if (L_18)
			{
				goto IL_0041;
			}
		}

IL_0080:
		{
			IL2CPP_LEAVE(0x92, FINALLY_0085);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0085;
	}

FINALLY_0085:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_19 = V_2;
			if (!L_19)
			{
				goto IL_0091;
			}
		}

IL_008b:
		{
			Il2CppObject* L_20 = V_2;
			NullCheck((Il2CppObject *)L_20);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_20);
		}

IL_0091:
		{
			IL2CPP_END_FINALLY(133)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(133)
	{
		IL2CPP_JUMP_TBL(0x92, IL_0092)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0092:
	{
		StringBuilder_t1221177846 * L_21 = V_0;
		NullCheck((StringBuilder_t1221177846 *)L_21);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_21, (String_t*)_stringLiteral816989470, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_22 = V_0;
		NullCheck((Il2CppObject *)L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_22);
		return L_23;
	}
}
// System.Boolean Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::Equals(System.Object)
extern "C"  bool ImmutableTree_1_Equals_m1063714981_gshared (ImmutableTree_1_t368550071 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	ImmutableTree_1_t368550071 * V_0 = NULL;
	Nullable_1_t2088641033  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B8_0 = 0;
	int32_t G_B13_0 = 0;
	{
		Il2CppObject * L_0 = ___o0;
		if ((!(((Il2CppObject*)(ImmutableTree_1_t368550071 *)__this) == ((Il2CppObject*)(Il2CppObject *)L_0))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_1 = ___o0;
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_2 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_3 = ___o0;
		NullCheck((Il2CppObject *)L_3);
		Type_t * L_4 = Object_GetType_m191970594((Il2CppObject *)L_3, /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_2) == ((Il2CppObject*)(Type_t *)L_4)))
		{
			goto IL_0022;
		}
	}

IL_0020:
	{
		return (bool)0;
	}

IL_0022:
	{
		Il2CppObject * L_5 = ___o0;
		V_0 = (ImmutableTree_1_t368550071 *)((ImmutableTree_1_t368550071 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)));
		ImmutableSortedMap_2_t94277708 * L_6 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		if (!L_6)
		{
			goto IL_004d;
		}
	}
	{
		ImmutableSortedMap_2_t94277708 * L_7 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ImmutableTree_1_t368550071 * L_8 = V_0;
		NullCheck(L_8);
		ImmutableSortedMap_2_t94277708 * L_9 = (ImmutableSortedMap_2_t94277708 *)L_8->get__children_2();
		NullCheck((Il2CppObject *)L_7);
		bool L_10 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_9);
		G_B8_0 = ((((int32_t)L_10) == ((int32_t)0))? 1 : 0);
		goto IL_0059;
	}

IL_004d:
	{
		ImmutableTree_1_t368550071 * L_11 = V_0;
		NullCheck(L_11);
		ImmutableSortedMap_2_t94277708 * L_12 = (ImmutableSortedMap_2_t94277708 *)L_11->get__children_2();
		G_B8_0 = ((((int32_t)((((Il2CppObject*)(ImmutableSortedMap_2_t94277708 *)L_12) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_0059:
	{
		if (!G_B8_0)
		{
			goto IL_0060;
		}
	}
	{
		return (bool)0;
	}

IL_0060:
	{
		Nullable_1_t2088641033  L_13 = (Nullable_1_t2088641033 )__this->get__value_3();
		Nullable_1_t2088641033  L_14 = L_13;
		Il2CppObject * L_15 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), &L_14);
		if (!L_15)
		{
			goto IL_0097;
		}
	}
	{
		Nullable_1_t2088641033  L_16 = (Nullable_1_t2088641033 )__this->get__value_3();
		V_1 = (Nullable_1_t2088641033 )L_16;
		ImmutableTree_1_t368550071 * L_17 = V_0;
		NullCheck(L_17);
		Nullable_1_t2088641033  L_18 = (Nullable_1_t2088641033 )L_17->get__value_3();
		Nullable_1_t2088641033  L_19 = L_18;
		Il2CppObject * L_20 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), &L_19);
		Il2CppClass* il2cpp_this_typeinfo_138 = IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9);
		bool L_21 = ((  bool (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))il2cpp_this_typeinfo_138->vtable[0].methodPtr)((Il2CppObject *)il2cpp_codegen_fake_box((&V_1)), (Il2CppObject *)L_20, /*hidden argument*/il2cpp_this_typeinfo_138->vtable[0].method);
		G_B13_0 = ((((int32_t)L_21) == ((int32_t)0))? 1 : 0);
		goto IL_00a8;
	}

IL_0097:
	{
		ImmutableTree_1_t368550071 * L_22 = V_0;
		NullCheck(L_22);
		Nullable_1_t2088641033  L_23 = (Nullable_1_t2088641033 )L_22->get__value_3();
		Nullable_1_t2088641033  L_24 = L_23;
		Il2CppObject * L_25 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), &L_24);
		G_B13_0 = ((((int32_t)((((Il2CppObject*)(Il2CppObject *)L_25) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_00a8:
	{
		if (!G_B13_0)
		{
			goto IL_00af;
		}
	}
	{
		return (bool)0;
	}

IL_00af:
	{
		return (bool)1;
	}
}
// System.Int32 Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::GetHashCode()
extern "C"  int32_t ImmutableTree_1_GetHashCode_m3650643267_gshared (ImmutableTree_1_t368550071 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Nullable_1_t2088641033  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B3_0 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	{
		Nullable_1_t2088641033  L_0 = (Nullable_1_t2088641033 )__this->get__value_3();
		Nullable_1_t2088641033  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9), &L_1);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Nullable_1_t2088641033  L_3 = (Nullable_1_t2088641033 )__this->get__value_3();
		V_1 = (Nullable_1_t2088641033 )L_3;
		Il2CppClass* il2cpp_this_typeinfo_31 = IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9);
		int32_t L_4 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))il2cpp_this_typeinfo_31->vtable[2].methodPtr)((Il2CppObject *)il2cpp_codegen_fake_box((&V_1)), /*hidden argument*/il2cpp_this_typeinfo_31->vtable[2].method);
		G_B3_0 = L_4;
		goto IL_002a;
	}

IL_0029:
	{
		G_B3_0 = 0;
	}

IL_002a:
	{
		V_0 = (int32_t)G_B3_0;
		int32_t L_5 = V_0;
		ImmutableSortedMap_2_t94277708 * L_6 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		G_B4_0 = ((int32_t)((int32_t)((int32_t)31)*(int32_t)L_5));
		if (!L_6)
		{
			G_B5_0 = ((int32_t)((int32_t)((int32_t)31)*(int32_t)L_5));
			goto IL_004a;
		}
	}
	{
		ImmutableSortedMap_2_t94277708 * L_7 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		NullCheck((Il2CppObject *)L_7);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_7);
		G_B6_0 = L_8;
		G_B6_1 = G_B4_0;
		goto IL_004b;
	}

IL_004a:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_004b:
	{
		V_0 = (int32_t)((int32_t)((int32_t)G_B6_1+(int32_t)G_B6_0));
		int32_t L_9 = V_0;
		return L_9;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Nullable`1<System.Boolean>>::.cctor()
extern Il2CppClass* StandardComparator_1_t691330998_il2cpp_TypeInfo_var;
extern Il2CppClass* Builder_t2701728999_il2cpp_TypeInfo_var;
extern Il2CppClass* Nullable_1_t2088641033_il2cpp_TypeInfo_var;
extern const MethodInfo* StandardComparator_1_GetComparator_m2621741865_MethodInfo_var;
extern const MethodInfo* Builder_EmptyMap_m3858564147_MethodInfo_var;
extern const uint32_t ImmutableTree_1__cctor_m507018603_MetadataUsageId;
extern "C"  void ImmutableTree_1__cctor_m507018603_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImmutableTree_1__cctor_m507018603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_t2088641033  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(StandardComparator_1_t691330998_il2cpp_TypeInfo_var);
		StandardComparator_1_t691330998 * L_0 = StandardComparator_1_GetComparator_m2621741865(NULL /*static, unused*/, /*hidden argument*/StandardComparator_1_GetComparator_m2621741865_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Builder_t2701728999_il2cpp_TypeInfo_var);
		ImmutableSortedMap_2_t94277708 * L_1 = Builder_EmptyMap_m3858564147(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/Builder_EmptyMap_m3858564147_MethodInfo_var);
		((ImmutableTree_1_t368550071_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyChildren_0(L_1);
		Initobj (Nullable_1_t2088641033_il2cpp_TypeInfo_var, (&V_0));
		Nullable_1_t2088641033  L_2 = V_0;
		ImmutableSortedMap_2_t94277708 * L_3 = ((ImmutableTree_1_t368550071_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_EmptyChildren_0();
		ImmutableTree_1_t368550071 * L_4 = (ImmutableTree_1_t368550071 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		((  void (*) (ImmutableTree_1_t368550071 *, Nullable_1_t2088641033 , ImmutableSortedMap_2_t94277708 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_4, (Nullable_1_t2088641033 )L_2, (ImmutableSortedMap_2_t94277708 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((ImmutableTree_1_t368550071_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Empty_1(L_4);
		return;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::.ctor(T,Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>)
extern "C"  void ImmutableTree_1__ctor_m4028204070_gshared (ImmutableTree_1_t969358333 * __this, Il2CppObject * ___value0, ImmutableSortedMap_2_t94277708 * ___children1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___value0;
		__this->set__value_3(L_0);
		ImmutableSortedMap_2_t94277708 * L_1 = ___children1;
		__this->set__children_2(L_1);
		return;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::.ctor(T)
extern "C"  void ImmutableTree_1__ctor_m2496271442_gshared (ImmutableTree_1_t969358333 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ImmutableSortedMap_2_t94277708 * L_1 = ((ImmutableTree_1_t969358333_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyChildren_0();
		NullCheck((ImmutableTree_1_t969358333 *)__this);
		((  void (*) (ImmutableTree_1_t969358333 *, Il2CppObject *, ImmutableSortedMap_2_t94277708 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ImmutableTree_1_t969358333 *)__this, (Il2CppObject *)L_0, (ImmutableSortedMap_2_t94277708 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,T>> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ImmutableTree_1_GetEnumerator_m1374165662_gshared (ImmutableTree_1_t969358333 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		List_1_t3420389621 * L_0 = (List_1_t3420389621 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (List_1_t3420389621 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (Il2CppObject*)L_0;
		Il2CppObject* L_1 = V_0;
		TreeVisitor278_t1535183187 * L_2 = (TreeVisitor278_t1535183187 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (TreeVisitor278_t1535183187 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_2, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((ImmutableTree_1_t969358333 *)__this);
		VirtActionInvoker1< Il2CppObject* >::Invoke(17 /* System.Void Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::Foreach(Firebase.Database.Internal.Core.Utilities.ImmutableTree`1/ITreeVisitor`1<T,System.Object>) */, (ImmutableTree_1_t969358333 *)__this, (Il2CppObject*)L_2);
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), (Il2CppObject*)L_3);
		return L_4;
	}
}
// System.Collections.IEnumerator Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ImmutableTree_1_System_Collections_IEnumerable_GetEnumerator_m1780813397_gshared (ImmutableTree_1_t969358333 * __this, const MethodInfo* method)
{
	{
		NullCheck((ImmutableTree_1_t969358333 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ImmutableTree_1_t969358333 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ImmutableTree_1_t969358333 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_0;
	}
}
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::EmptyInstance()
extern "C"  ImmutableTree_1_t969358333 * ImmutableTree_1_EmptyInstance_m3642173572_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ImmutableTree_1_t969358333 * L_0 = ((ImmutableTree_1_t969358333_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_Empty_1();
		return L_0;
	}
}
// T Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::GetValue()
extern "C"  Il2CppObject * ImmutableTree_1_GetValue_m3705018054_gshared (ImmutableTree_1_t969358333 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__value_3();
		return L_0;
	}
}
// Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::GetChildren()
extern "C"  ImmutableSortedMap_2_t94277708 * ImmutableTree_1_GetChildren_m4256937032_gshared (ImmutableTree_1_t969358333 * __this, const MethodInfo* method)
{
	{
		ImmutableSortedMap_2_t94277708 * L_0 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		return L_0;
	}
}
// System.Boolean Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::IsEmpty()
extern "C"  bool ImmutableTree_1_IsEmpty_m4143570191_gshared (ImmutableTree_1_t969358333 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__value_3();
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		ImmutableSortedMap_2_t94277708 * L_1 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::IsEmpty() */, (ImmutableSortedMap_2_t94277708 *)L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::FindRootMostMatchingPath(Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.Utilities.Predicate`1<T>)
extern Il2CppClass* Path_t2568473163_il2cpp_TypeInfo_var;
extern Il2CppClass* ChildKeyU5BU5D_t491817302_il2cpp_TypeInfo_var;
extern const uint32_t ImmutableTree_1_FindRootMostMatchingPath_m1844098277_MetadataUsageId;
extern "C"  Path_t2568473163 * ImmutableTree_1_FindRootMostMatchingPath_m1844098277_gshared (ImmutableTree_1_t969358333 * __this, Path_t2568473163 * ___relativePath0, Predicate_1_t1206613972 * ___predicate1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImmutableTree_1_FindRootMostMatchingPath_m1844098277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ChildKey_t1197802383 * V_0 = NULL;
	ImmutableTree_1_t969358333 * V_1 = NULL;
	Path_t2568473163 * V_2 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__value_3();
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		Predicate_1_t1206613972 * L_1 = ___predicate1;
		Il2CppObject * L_2 = (Il2CppObject *)__this->get__value_3();
		NullCheck((Predicate_1_t1206613972 *)L_1);
		bool L_3 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(4 /* System.Boolean Firebase.Database.Internal.Core.Utilities.Predicate`1<System.Object>::Evaluate(T) */, (Predicate_1_t1206613972 *)L_1, (Il2CppObject *)L_2);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Path_t2568473163_il2cpp_TypeInfo_var);
		Path_t2568473163 * L_4 = Path_GetEmptyPath_m3969996076(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_4;
	}

IL_0027:
	{
		Path_t2568473163 * L_5 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(17 /* System.Boolean Firebase.Database.Internal.Core.Path::IsEmpty() */, (Path_t2568473163 *)L_5);
		if (!L_6)
		{
			goto IL_0034;
		}
	}
	{
		return (Path_t2568473163 *)NULL;
	}

IL_0034:
	{
		Path_t2568473163 * L_7 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_7);
		ChildKey_t1197802383 * L_8 = VirtFuncInvoker0< ChildKey_t1197802383 * >::Invoke(13 /* Firebase.Database.Internal.Snapshot.ChildKey Firebase.Database.Internal.Core.Path::GetFront() */, (Path_t2568473163 *)L_7);
		V_0 = (ChildKey_t1197802383 *)L_8;
		ImmutableSortedMap_2_t94277708 * L_9 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_10 = V_0;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_9);
		Il2CppObject * L_11 = VirtFuncInvoker1< Il2CppObject *, ChildKey_t1197802383 * >::Invoke(8 /* TV Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Get(TK) */, (ImmutableSortedMap_2_t94277708 *)L_9, (ChildKey_t1197802383 *)L_10);
		V_1 = (ImmutableTree_1_t969358333 *)((ImmutableTree_1_t969358333 *)Castclass(L_11, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)));
		ImmutableTree_1_t969358333 * L_12 = V_1;
		if (!L_12)
		{
			goto IL_007f;
		}
	}
	{
		ImmutableTree_1_t969358333 * L_13 = V_1;
		Path_t2568473163 * L_14 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_14);
		Path_t2568473163 * L_15 = VirtFuncInvoker0< Path_t2568473163 * >::Invoke(14 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Path::PopFront() */, (Path_t2568473163 *)L_14);
		Predicate_1_t1206613972 * L_16 = ___predicate1;
		NullCheck((ImmutableTree_1_t969358333 *)L_13);
		Path_t2568473163 * L_17 = VirtFuncInvoker2< Path_t2568473163 *, Path_t2568473163 *, Predicate_1_t1206613972 * >::Invoke(9 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::FindRootMostMatchingPath(Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.Utilities.Predicate`1<T>) */, (ImmutableTree_1_t969358333 *)L_13, (Path_t2568473163 *)L_15, (Predicate_1_t1206613972 *)L_16);
		V_2 = (Path_t2568473163 *)L_17;
		Path_t2568473163 * L_18 = V_2;
		if (!L_18)
		{
			goto IL_007d;
		}
	}
	{
		ChildKeyU5BU5D_t491817302* L_19 = (ChildKeyU5BU5D_t491817302*)((ChildKeyU5BU5D_t491817302*)SZArrayNew(ChildKeyU5BU5D_t491817302_il2cpp_TypeInfo_var, (uint32_t)1));
		ChildKey_t1197802383 * L_20 = V_0;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (ChildKey_t1197802383 *)L_20);
		Path_t2568473163 * L_21 = (Path_t2568473163 *)il2cpp_codegen_object_new(Path_t2568473163_il2cpp_TypeInfo_var);
		Path__ctor_m2358381126(L_21, (ChildKeyU5BU5D_t491817302*)L_19, /*hidden argument*/NULL);
		Path_t2568473163 * L_22 = V_2;
		NullCheck((Path_t2568473163 *)L_21);
		Path_t2568473163 * L_23 = VirtFuncInvoker1< Path_t2568473163 *, Path_t2568473163 * >::Invoke(9 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Path::Child(Firebase.Database.Internal.Core.Path) */, (Path_t2568473163 *)L_21, (Path_t2568473163 *)L_22);
		return L_23;
	}

IL_007d:
	{
		return (Path_t2568473163 *)NULL;
	}

IL_007f:
	{
		return (Path_t2568473163 *)NULL;
	}
}
// Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::FindRootMostPathWithValue(Firebase.Database.Internal.Core.Path)
extern "C"  Path_t2568473163 * ImmutableTree_1_FindRootMostPathWithValue_m318551461_gshared (ImmutableTree_1_t969358333 * __this, Path_t2568473163 * ___relativePath0, const MethodInfo* method)
{
	{
		Path_t2568473163 * L_0 = ___relativePath0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13));
		Predicate_1_t1206613972 * L_1 = ((Predicate_1_t1206613972_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13)->static_fields)->get_True_0();
		NullCheck((ImmutableTree_1_t969358333 *)__this);
		Path_t2568473163 * L_2 = VirtFuncInvoker2< Path_t2568473163 *, Path_t2568473163 *, Predicate_1_t1206613972 * >::Invoke(9 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::FindRootMostMatchingPath(Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.Utilities.Predicate`1<T>) */, (ImmutableTree_1_t969358333 *)__this, (Path_t2568473163 *)L_0, (Predicate_1_t1206613972 *)L_1);
		return L_2;
	}
}
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::GetChild(Firebase.Database.Internal.Snapshot.ChildKey)
extern "C"  ImmutableTree_1_t969358333 * ImmutableTree_1_GetChild_m4207950176_gshared (ImmutableTree_1_t969358333 * __this, ChildKey_t1197802383 * ___child0, const MethodInfo* method)
{
	ImmutableTree_1_t969358333 * V_0 = NULL;
	{
		ImmutableSortedMap_2_t94277708 * L_0 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_1 = ___child0;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, ChildKey_t1197802383 * >::Invoke(8 /* TV Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Get(TK) */, (ImmutableSortedMap_2_t94277708 *)L_0, (ChildKey_t1197802383 *)L_1);
		V_0 = (ImmutableTree_1_t969358333 *)((ImmutableTree_1_t969358333 *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)));
		ImmutableTree_1_t969358333 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		ImmutableTree_1_t969358333 * L_4 = V_0;
		return L_4;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ImmutableTree_1_t969358333 * L_5 = ((  ImmutableTree_1_t969358333 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_5;
	}
}
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::Subtree(Firebase.Database.Internal.Core.Path)
extern "C"  ImmutableTree_1_t969358333 * ImmutableTree_1_Subtree_m2292476291_gshared (ImmutableTree_1_t969358333 * __this, Path_t2568473163 * ___relativePath0, const MethodInfo* method)
{
	ChildKey_t1197802383 * V_0 = NULL;
	ImmutableTree_1_t969358333 * V_1 = NULL;
	{
		Path_t2568473163 * L_0 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(17 /* System.Boolean Firebase.Database.Internal.Core.Path::IsEmpty() */, (Path_t2568473163 *)L_0);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return __this;
	}

IL_000d:
	{
		Path_t2568473163 * L_2 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_2);
		ChildKey_t1197802383 * L_3 = VirtFuncInvoker0< ChildKey_t1197802383 * >::Invoke(13 /* Firebase.Database.Internal.Snapshot.ChildKey Firebase.Database.Internal.Core.Path::GetFront() */, (Path_t2568473163 *)L_2);
		V_0 = (ChildKey_t1197802383 *)L_3;
		ImmutableSortedMap_2_t94277708 * L_4 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_5 = V_0;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_4);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, ChildKey_t1197802383 * >::Invoke(8 /* TV Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Get(TK) */, (ImmutableSortedMap_2_t94277708 *)L_4, (ChildKey_t1197802383 *)L_5);
		V_1 = (ImmutableTree_1_t969358333 *)((ImmutableTree_1_t969358333 *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)));
		ImmutableTree_1_t969358333 * L_7 = V_1;
		if (!L_7)
		{
			goto IL_0039;
		}
	}
	{
		ImmutableTree_1_t969358333 * L_8 = V_1;
		Path_t2568473163 * L_9 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_9);
		Path_t2568473163 * L_10 = VirtFuncInvoker0< Path_t2568473163 * >::Invoke(14 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Path::PopFront() */, (Path_t2568473163 *)L_9);
		NullCheck((ImmutableTree_1_t969358333 *)L_8);
		ImmutableTree_1_t969358333 * L_11 = VirtFuncInvoker1< ImmutableTree_1_t969358333 *, Path_t2568473163 * >::Invoke(12 /* Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::Subtree(Firebase.Database.Internal.Core.Path) */, (ImmutableTree_1_t969358333 *)L_8, (Path_t2568473163 *)L_10);
		return L_11;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ImmutableTree_1_t969358333 * L_12 = ((  ImmutableTree_1_t969358333 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_12;
	}
}
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::Set(Firebase.Database.Internal.Core.Path,T)
extern "C"  ImmutableTree_1_t969358333 * ImmutableTree_1_Set_m1462385863_gshared (ImmutableTree_1_t969358333 * __this, Path_t2568473163 * ___relativePath0, Il2CppObject * ___value1, const MethodInfo* method)
{
	ChildKey_t1197802383 * V_0 = NULL;
	ImmutableTree_1_t969358333 * V_1 = NULL;
	ImmutableTree_1_t969358333 * V_2 = NULL;
	ImmutableSortedMap_2_t94277708 * V_3 = NULL;
	{
		Path_t2568473163 * L_0 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(17 /* System.Boolean Firebase.Database.Internal.Core.Path::IsEmpty() */, (Path_t2568473163 *)L_0);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_2 = ___value1;
		ImmutableSortedMap_2_t94277708 * L_3 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ImmutableTree_1_t969358333 * L_4 = (ImmutableTree_1_t969358333 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		((  void (*) (ImmutableTree_1_t969358333 *, Il2CppObject *, ImmutableSortedMap_2_t94277708 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_4, (Il2CppObject *)L_2, (ImmutableSortedMap_2_t94277708 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_4;
	}

IL_0018:
	{
		Path_t2568473163 * L_5 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_5);
		ChildKey_t1197802383 * L_6 = VirtFuncInvoker0< ChildKey_t1197802383 * >::Invoke(13 /* Firebase.Database.Internal.Snapshot.ChildKey Firebase.Database.Internal.Core.Path::GetFront() */, (Path_t2568473163 *)L_5);
		V_0 = (ChildKey_t1197802383 *)L_6;
		ImmutableSortedMap_2_t94277708 * L_7 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_8 = V_0;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_7);
		Il2CppObject * L_9 = VirtFuncInvoker1< Il2CppObject *, ChildKey_t1197802383 * >::Invoke(8 /* TV Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Get(TK) */, (ImmutableSortedMap_2_t94277708 *)L_7, (ChildKey_t1197802383 *)L_8);
		V_1 = (ImmutableTree_1_t969358333 *)((ImmutableTree_1_t969358333 *)Castclass(L_9, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)));
		ImmutableTree_1_t969358333 * L_10 = V_1;
		if (L_10)
		{
			goto IL_003d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ImmutableTree_1_t969358333 * L_11 = ((  ImmutableTree_1_t969358333 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_1 = (ImmutableTree_1_t969358333 *)L_11;
	}

IL_003d:
	{
		ImmutableTree_1_t969358333 * L_12 = V_1;
		Path_t2568473163 * L_13 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_13);
		Path_t2568473163 * L_14 = VirtFuncInvoker0< Path_t2568473163 * >::Invoke(14 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Path::PopFront() */, (Path_t2568473163 *)L_13);
		Il2CppObject * L_15 = ___value1;
		NullCheck((ImmutableTree_1_t969358333 *)L_12);
		ImmutableTree_1_t969358333 * L_16 = VirtFuncInvoker2< ImmutableTree_1_t969358333 *, Path_t2568473163 *, Il2CppObject * >::Invoke(13 /* Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::Set(Firebase.Database.Internal.Core.Path,T) */, (ImmutableTree_1_t969358333 *)L_12, (Path_t2568473163 *)L_14, (Il2CppObject *)L_15);
		V_2 = (ImmutableTree_1_t969358333 *)L_16;
		ImmutableSortedMap_2_t94277708 * L_17 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_18 = V_0;
		ImmutableTree_1_t969358333 * L_19 = V_2;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_17);
		ImmutableSortedMap_2_t94277708 * L_20 = VirtFuncInvoker2< ImmutableSortedMap_2_t94277708 *, ChildKey_t1197802383 *, Il2CppObject * >::Invoke(10 /* Firebase.Database.Internal.Collection.ImmutableSortedMap`2<TK,TV> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Insert(TK,TV) */, (ImmutableSortedMap_2_t94277708 *)L_17, (ChildKey_t1197802383 *)L_18, (Il2CppObject *)L_19);
		V_3 = (ImmutableSortedMap_2_t94277708 *)L_20;
		Il2CppObject * L_21 = (Il2CppObject *)__this->get__value_3();
		ImmutableSortedMap_2_t94277708 * L_22 = V_3;
		ImmutableTree_1_t969358333 * L_23 = (ImmutableTree_1_t969358333 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		((  void (*) (ImmutableTree_1_t969358333 *, Il2CppObject *, ImmutableSortedMap_2_t94277708 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_23, (Il2CppObject *)L_21, (ImmutableSortedMap_2_t94277708 *)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_23;
	}
}
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::Remove(Firebase.Database.Internal.Core.Path)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ImmutableTree_1_Remove_m3691997035_MetadataUsageId;
extern "C"  ImmutableTree_1_t969358333 * ImmutableTree_1_Remove_m3691997035_gshared (ImmutableTree_1_t969358333 * __this, Path_t2568473163 * ___relativePath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImmutableTree_1_Remove_m3691997035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	ChildKey_t1197802383 * V_1 = NULL;
	ImmutableTree_1_t969358333 * V_2 = NULL;
	ImmutableTree_1_t969358333 * V_3 = NULL;
	ImmutableSortedMap_2_t94277708 * V_4 = NULL;
	{
		Path_t2568473163 * L_0 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(17 /* System.Boolean Firebase.Database.Internal.Core.Path::IsEmpty() */, (Path_t2568473163 *)L_0);
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		ImmutableSortedMap_2_t94277708 * L_2 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::IsEmpty() */, (ImmutableSortedMap_2_t94277708 *)L_2);
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ImmutableTree_1_t969358333 * L_4 = ((  ImmutableTree_1_t969358333 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_4;
	}

IL_0021:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_5 = V_0;
		ImmutableSortedMap_2_t94277708 * L_6 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ImmutableTree_1_t969358333 * L_7 = (ImmutableTree_1_t969358333 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		((  void (*) (ImmutableTree_1_t969358333 *, Il2CppObject *, ImmutableSortedMap_2_t94277708 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_7, (Il2CppObject *)L_5, (ImmutableSortedMap_2_t94277708 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_7;
	}

IL_0036:
	{
		Path_t2568473163 * L_8 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_8);
		ChildKey_t1197802383 * L_9 = VirtFuncInvoker0< ChildKey_t1197802383 * >::Invoke(13 /* Firebase.Database.Internal.Snapshot.ChildKey Firebase.Database.Internal.Core.Path::GetFront() */, (Path_t2568473163 *)L_8);
		V_1 = (ChildKey_t1197802383 *)L_9;
		ImmutableSortedMap_2_t94277708 * L_10 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_11 = V_1;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_10);
		Il2CppObject * L_12 = VirtFuncInvoker1< Il2CppObject *, ChildKey_t1197802383 * >::Invoke(8 /* TV Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Get(TK) */, (ImmutableSortedMap_2_t94277708 *)L_10, (ChildKey_t1197802383 *)L_11);
		V_2 = (ImmutableTree_1_t969358333 *)((ImmutableTree_1_t969358333 *)Castclass(L_12, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)));
		ImmutableTree_1_t969358333 * L_13 = V_2;
		if (!L_13)
		{
			goto IL_00bf;
		}
	}
	{
		ImmutableTree_1_t969358333 * L_14 = V_2;
		Path_t2568473163 * L_15 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_15);
		Path_t2568473163 * L_16 = VirtFuncInvoker0< Path_t2568473163 * >::Invoke(14 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Path::PopFront() */, (Path_t2568473163 *)L_15);
		NullCheck((ImmutableTree_1_t969358333 *)L_14);
		ImmutableTree_1_t969358333 * L_17 = VirtFuncInvoker1< ImmutableTree_1_t969358333 *, Path_t2568473163 * >::Invoke(14 /* Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::Remove(Firebase.Database.Internal.Core.Path) */, (ImmutableTree_1_t969358333 *)L_14, (Path_t2568473163 *)L_16);
		V_3 = (ImmutableTree_1_t969358333 *)L_17;
		ImmutableTree_1_t969358333 * L_18 = V_3;
		NullCheck((ImmutableTree_1_t969358333 *)L_18);
		bool L_19 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::IsEmpty() */, (ImmutableTree_1_t969358333 *)L_18);
		if (!L_19)
		{
			goto IL_0080;
		}
	}
	{
		ImmutableSortedMap_2_t94277708 * L_20 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_21 = V_1;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_20);
		ImmutableSortedMap_2_t94277708 * L_22 = VirtFuncInvoker1< ImmutableSortedMap_2_t94277708 *, ChildKey_t1197802383 * >::Invoke(9 /* Firebase.Database.Internal.Collection.ImmutableSortedMap`2<TK,TV> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Remove(TK) */, (ImmutableSortedMap_2_t94277708 *)L_20, (ChildKey_t1197802383 *)L_21);
		V_4 = (ImmutableSortedMap_2_t94277708 *)L_22;
		goto IL_008f;
	}

IL_0080:
	{
		ImmutableSortedMap_2_t94277708 * L_23 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_24 = V_1;
		ImmutableTree_1_t969358333 * L_25 = V_3;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_23);
		ImmutableSortedMap_2_t94277708 * L_26 = VirtFuncInvoker2< ImmutableSortedMap_2_t94277708 *, ChildKey_t1197802383 *, Il2CppObject * >::Invoke(10 /* Firebase.Database.Internal.Collection.ImmutableSortedMap`2<TK,TV> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Insert(TK,TV) */, (ImmutableSortedMap_2_t94277708 *)L_23, (ChildKey_t1197802383 *)L_24, (Il2CppObject *)L_25);
		V_4 = (ImmutableSortedMap_2_t94277708 *)L_26;
	}

IL_008f:
	{
		Il2CppObject * L_27 = (Il2CppObject *)__this->get__value_3();
		if (L_27)
		{
			goto IL_00b1;
		}
	}
	{
		ImmutableSortedMap_2_t94277708 * L_28 = V_4;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_28);
		bool L_29 = VirtFuncInvoker0< bool >::Invoke(14 /* System.Boolean Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::IsEmpty() */, (ImmutableSortedMap_2_t94277708 *)L_28);
		if (!L_29)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ImmutableTree_1_t969358333 * L_30 = ((  ImmutableTree_1_t969358333 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_30;
	}

IL_00b1:
	{
		Il2CppObject * L_31 = (Il2CppObject *)__this->get__value_3();
		ImmutableSortedMap_2_t94277708 * L_32 = V_4;
		ImmutableTree_1_t969358333 * L_33 = (ImmutableTree_1_t969358333 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		((  void (*) (ImmutableTree_1_t969358333 *, Il2CppObject *, ImmutableSortedMap_2_t94277708 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_33, (Il2CppObject *)L_31, (ImmutableSortedMap_2_t94277708 *)L_32, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_33;
	}

IL_00bf:
	{
		return __this;
	}
}
// T Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::Get(Firebase.Database.Internal.Core.Path)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ImmutableTree_1_Get_m3285009340_MetadataUsageId;
extern "C"  Il2CppObject * ImmutableTree_1_Get_m3285009340_gshared (ImmutableTree_1_t969358333 * __this, Path_t2568473163 * ___relativePath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImmutableTree_1_Get_m3285009340_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ChildKey_t1197802383 * V_0 = NULL;
	ImmutableTree_1_t969358333 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		Path_t2568473163 * L_0 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(17 /* System.Boolean Firebase.Database.Internal.Core.Path::IsEmpty() */, (Path_t2568473163 *)L_0);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get__value_3();
		return L_2;
	}

IL_0012:
	{
		Path_t2568473163 * L_3 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_3);
		ChildKey_t1197802383 * L_4 = VirtFuncInvoker0< ChildKey_t1197802383 * >::Invoke(13 /* Firebase.Database.Internal.Snapshot.ChildKey Firebase.Database.Internal.Core.Path::GetFront() */, (Path_t2568473163 *)L_3);
		V_0 = (ChildKey_t1197802383 *)L_4;
		ImmutableSortedMap_2_t94277708 * L_5 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_6 = V_0;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_5);
		Il2CppObject * L_7 = VirtFuncInvoker1< Il2CppObject *, ChildKey_t1197802383 * >::Invoke(8 /* TV Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Get(TK) */, (ImmutableSortedMap_2_t94277708 *)L_5, (ChildKey_t1197802383 *)L_6);
		V_1 = (ImmutableTree_1_t969358333 *)((ImmutableTree_1_t969358333 *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)));
		ImmutableTree_1_t969358333 * L_8 = V_1;
		if (!L_8)
		{
			goto IL_003e;
		}
	}
	{
		ImmutableTree_1_t969358333 * L_9 = V_1;
		Path_t2568473163 * L_10 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_10);
		Path_t2568473163 * L_11 = VirtFuncInvoker0< Path_t2568473163 * >::Invoke(14 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Path::PopFront() */, (Path_t2568473163 *)L_10);
		NullCheck((ImmutableTree_1_t969358333 *)L_9);
		Il2CppObject * L_12 = VirtFuncInvoker1< Il2CppObject *, Path_t2568473163 * >::Invoke(15 /* T Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::Get(Firebase.Database.Internal.Core.Path) */, (ImmutableTree_1_t969358333 *)L_9, (Path_t2568473163 *)L_11);
		return L_12;
	}

IL_003e:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_13 = V_2;
		return L_13;
	}
}
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::SetTree(Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T>)
extern "C"  ImmutableTree_1_t969358333 * ImmutableTree_1_SetTree_m730507020_gshared (ImmutableTree_1_t969358333 * __this, Path_t2568473163 * ___relativePath0, ImmutableTree_1_t969358333 * ___newTree1, const MethodInfo* method)
{
	ChildKey_t1197802383 * V_0 = NULL;
	ImmutableTree_1_t969358333 * V_1 = NULL;
	ImmutableTree_1_t969358333 * V_2 = NULL;
	ImmutableSortedMap_2_t94277708 * V_3 = NULL;
	{
		Path_t2568473163 * L_0 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(17 /* System.Boolean Firebase.Database.Internal.Core.Path::IsEmpty() */, (Path_t2568473163 *)L_0);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		ImmutableTree_1_t969358333 * L_2 = ___newTree1;
		return L_2;
	}

IL_000d:
	{
		Path_t2568473163 * L_3 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_3);
		ChildKey_t1197802383 * L_4 = VirtFuncInvoker0< ChildKey_t1197802383 * >::Invoke(13 /* Firebase.Database.Internal.Snapshot.ChildKey Firebase.Database.Internal.Core.Path::GetFront() */, (Path_t2568473163 *)L_3);
		V_0 = (ChildKey_t1197802383 *)L_4;
		ImmutableSortedMap_2_t94277708 * L_5 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_6 = V_0;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_5);
		Il2CppObject * L_7 = VirtFuncInvoker1< Il2CppObject *, ChildKey_t1197802383 * >::Invoke(8 /* TV Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Get(TK) */, (ImmutableSortedMap_2_t94277708 *)L_5, (ChildKey_t1197802383 *)L_6);
		V_1 = (ImmutableTree_1_t969358333 *)((ImmutableTree_1_t969358333 *)Castclass(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)));
		ImmutableTree_1_t969358333 * L_8 = V_1;
		if (L_8)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ImmutableTree_1_t969358333 * L_9 = ((  ImmutableTree_1_t969358333 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_1 = (ImmutableTree_1_t969358333 *)L_9;
	}

IL_0032:
	{
		ImmutableTree_1_t969358333 * L_10 = V_1;
		Path_t2568473163 * L_11 = ___relativePath0;
		NullCheck((Path_t2568473163 *)L_11);
		Path_t2568473163 * L_12 = VirtFuncInvoker0< Path_t2568473163 * >::Invoke(14 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Path::PopFront() */, (Path_t2568473163 *)L_11);
		ImmutableTree_1_t969358333 * L_13 = ___newTree1;
		NullCheck((ImmutableTree_1_t969358333 *)L_10);
		ImmutableTree_1_t969358333 * L_14 = VirtFuncInvoker2< ImmutableTree_1_t969358333 *, Path_t2568473163 *, ImmutableTree_1_t969358333 * >::Invoke(16 /* Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::SetTree(Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T>) */, (ImmutableTree_1_t969358333 *)L_10, (Path_t2568473163 *)L_12, (ImmutableTree_1_t969358333 *)L_13);
		V_2 = (ImmutableTree_1_t969358333 *)L_14;
		ImmutableTree_1_t969358333 * L_15 = V_2;
		NullCheck((ImmutableTree_1_t969358333 *)L_15);
		bool L_16 = VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::IsEmpty() */, (ImmutableTree_1_t969358333 *)L_15);
		if (!L_16)
		{
			goto IL_005d;
		}
	}
	{
		ImmutableSortedMap_2_t94277708 * L_17 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_18 = V_0;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_17);
		ImmutableSortedMap_2_t94277708 * L_19 = VirtFuncInvoker1< ImmutableSortedMap_2_t94277708 *, ChildKey_t1197802383 * >::Invoke(9 /* Firebase.Database.Internal.Collection.ImmutableSortedMap`2<TK,TV> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Remove(TK) */, (ImmutableSortedMap_2_t94277708 *)L_17, (ChildKey_t1197802383 *)L_18);
		V_3 = (ImmutableSortedMap_2_t94277708 *)L_19;
		goto IL_006b;
	}

IL_005d:
	{
		ImmutableSortedMap_2_t94277708 * L_20 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ChildKey_t1197802383 * L_21 = V_0;
		ImmutableTree_1_t969358333 * L_22 = V_2;
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_20);
		ImmutableSortedMap_2_t94277708 * L_23 = VirtFuncInvoker2< ImmutableSortedMap_2_t94277708 *, ChildKey_t1197802383 *, Il2CppObject * >::Invoke(10 /* Firebase.Database.Internal.Collection.ImmutableSortedMap`2<TK,TV> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Insert(TK,TV) */, (ImmutableSortedMap_2_t94277708 *)L_20, (ChildKey_t1197802383 *)L_21, (Il2CppObject *)L_22);
		V_3 = (ImmutableSortedMap_2_t94277708 *)L_23;
	}

IL_006b:
	{
		Il2CppObject * L_24 = (Il2CppObject *)__this->get__value_3();
		ImmutableSortedMap_2_t94277708 * L_25 = V_3;
		ImmutableTree_1_t969358333 * L_26 = (ImmutableTree_1_t969358333 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11));
		((  void (*) (ImmutableTree_1_t969358333 *, Il2CppObject *, ImmutableSortedMap_2_t94277708 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_26, (Il2CppObject *)L_24, (ImmutableSortedMap_2_t94277708 *)L_25, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_26;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::Foreach(Firebase.Database.Internal.Core.Utilities.ImmutableTree`1/ITreeVisitor`1<T,System.Object>)
extern Il2CppClass* Path_t2568473163_il2cpp_TypeInfo_var;
extern const uint32_t ImmutableTree_1_Foreach_m2205888316_MetadataUsageId;
extern "C"  void ImmutableTree_1_Foreach_m2205888316_gshared (ImmutableTree_1_t969358333 * __this, Il2CppObject* ___visitor0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImmutableTree_1_Foreach_m2205888316_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Path_t2568473163_il2cpp_TypeInfo_var);
		Path_t2568473163 * L_0 = Path_GetEmptyPath_m3969996076(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___visitor0;
		NullCheck((ImmutableTree_1_t969358333 *)__this);
		((  Il2CppObject * (*) (ImmutableTree_1_t969358333 *, Path_t2568473163 *, Il2CppObject*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((ImmutableTree_1_t969358333 *)__this, (Path_t2568473163 *)L_0, (Il2CppObject*)L_1, (Il2CppObject *)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		return;
	}
}
// System.String Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::ToString()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2981496232_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m182894088_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m689905909_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3758586953;
extern Il2CppCodeGenString* _stringLiteral3551194621;
extern Il2CppCodeGenString* _stringLiteral372029329;
extern Il2CppCodeGenString* _stringLiteral816989470;
extern const uint32_t ImmutableTree_1_ToString_m1893070879_MetadataUsageId;
extern "C"  String_t* ImmutableTree_1_ToString_m1893070879_gshared (ImmutableTree_1_t969358333 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImmutableTree_1_ToString_m1893070879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	KeyValuePair_2_t1211005109  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Il2CppObject* V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		V_0 = (StringBuilder_t1221177846 *)L_0;
		StringBuilder_t1221177846 * L_1 = V_0;
		NullCheck((StringBuilder_t1221177846 *)L_1);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_1, (String_t*)_stringLiteral3758586953, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_2 = V_0;
		NullCheck((ImmutableTree_1_t969358333 *)__this);
		Il2CppObject * L_3 = VirtFuncInvoker0< Il2CppObject * >::Invoke(6 /* T Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::GetValue() */, (ImmutableTree_1_t969358333 *)__this);
		NullCheck((StringBuilder_t1221177846 *)L_2);
		StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_4 = V_0;
		NullCheck((StringBuilder_t1221177846 *)L_4);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_4, (String_t*)_stringLiteral3551194621, /*hidden argument*/NULL);
		ImmutableSortedMap_2_t94277708 * L_5 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		NullCheck((ImmutableSortedMap_2_t94277708 *)L_5);
		Il2CppObject* L_6 = VirtFuncInvoker0< Il2CppObject* >::Invoke(6 /* System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TK,TV>> Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::GetEnumerator() */, (ImmutableSortedMap_2_t94277708 *)L_5);
		V_2 = (Il2CppObject*)L_6;
	}

IL_003c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0075;
		}

IL_0041:
		{
			Il2CppObject* L_7 = V_2;
			NullCheck((Il2CppObject*)L_7);
			KeyValuePair_2_t1211005109  L_8 = InterfaceFuncInvoker0< KeyValuePair_2_t1211005109  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>>::get_Current() */, IEnumerator_1_t2981496232_il2cpp_TypeInfo_var, (Il2CppObject*)L_7);
			V_1 = (KeyValuePair_2_t1211005109 )L_8;
			StringBuilder_t1221177846 * L_9 = V_0;
			ChildKey_t1197802383 * L_10 = KeyValuePair_2_get_Key_m182894088((KeyValuePair_2_t1211005109 *)(&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m182894088_MethodInfo_var);
			NullCheck((ChildKey_t1197802383 *)L_10);
			String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Firebase.Database.Internal.Snapshot.ChildKey::AsString() */, (ChildKey_t1197802383 *)L_10);
			NullCheck((StringBuilder_t1221177846 *)L_9);
			StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_9, (String_t*)L_11, /*hidden argument*/NULL);
			StringBuilder_t1221177846 * L_12 = V_0;
			NullCheck((StringBuilder_t1221177846 *)L_12);
			StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_12, (String_t*)_stringLiteral372029329, /*hidden argument*/NULL);
			StringBuilder_t1221177846 * L_13 = V_0;
			Il2CppObject * L_14 = KeyValuePair_2_get_Value_m689905909((KeyValuePair_2_t1211005109 *)(&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m689905909_MethodInfo_var);
			NullCheck((StringBuilder_t1221177846 *)L_13);
			StringBuilder_Append_m3541816491((StringBuilder_t1221177846 *)L_13, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		}

IL_0075:
		{
			Il2CppObject* L_15 = V_2;
			NullCheck((Il2CppObject *)L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			if (L_16)
			{
				goto IL_0041;
			}
		}

IL_0080:
		{
			IL2CPP_LEAVE(0x92, FINALLY_0085);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0085;
	}

FINALLY_0085:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_17 = V_2;
			if (!L_17)
			{
				goto IL_0091;
			}
		}

IL_008b:
		{
			Il2CppObject* L_18 = V_2;
			NullCheck((Il2CppObject *)L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_18);
		}

IL_0091:
		{
			IL2CPP_END_FINALLY(133)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(133)
	{
		IL2CPP_JUMP_TBL(0x92, IL_0092)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0092:
	{
		StringBuilder_t1221177846 * L_19 = V_0;
		NullCheck((StringBuilder_t1221177846 *)L_19);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_19, (String_t*)_stringLiteral816989470, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_20 = V_0;
		NullCheck((Il2CppObject *)L_20);
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_20);
		return L_21;
	}
}
// System.Boolean Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::Equals(System.Object)
extern "C"  bool ImmutableTree_1_Equals_m2384379909_gshared (ImmutableTree_1_t969358333 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	ImmutableTree_1_t969358333 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B8_0 = 0;
	int32_t G_B13_0 = 0;
	{
		Il2CppObject * L_0 = ___o0;
		if ((!(((Il2CppObject*)(ImmutableTree_1_t969358333 *)__this) == ((Il2CppObject*)(Il2CppObject *)L_0))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_1 = ___o0;
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_2 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_3 = ___o0;
		NullCheck((Il2CppObject *)L_3);
		Type_t * L_4 = Object_GetType_m191970594((Il2CppObject *)L_3, /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_2) == ((Il2CppObject*)(Type_t *)L_4)))
		{
			goto IL_0022;
		}
	}

IL_0020:
	{
		return (bool)0;
	}

IL_0022:
	{
		Il2CppObject * L_5 = ___o0;
		V_0 = (ImmutableTree_1_t969358333 *)((ImmutableTree_1_t969358333 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11)));
		ImmutableSortedMap_2_t94277708 * L_6 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		if (!L_6)
		{
			goto IL_004d;
		}
	}
	{
		ImmutableSortedMap_2_t94277708 * L_7 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		ImmutableTree_1_t969358333 * L_8 = V_0;
		NullCheck(L_8);
		ImmutableSortedMap_2_t94277708 * L_9 = (ImmutableSortedMap_2_t94277708 *)L_8->get__children_2();
		NullCheck((Il2CppObject *)L_7);
		bool L_10 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_7, (Il2CppObject *)L_9);
		G_B8_0 = ((((int32_t)L_10) == ((int32_t)0))? 1 : 0);
		goto IL_0059;
	}

IL_004d:
	{
		ImmutableTree_1_t969358333 * L_11 = V_0;
		NullCheck(L_11);
		ImmutableSortedMap_2_t94277708 * L_12 = (ImmutableSortedMap_2_t94277708 *)L_11->get__children_2();
		G_B8_0 = ((((int32_t)((((Il2CppObject*)(ImmutableSortedMap_2_t94277708 *)L_12) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_0059:
	{
		if (!G_B8_0)
		{
			goto IL_0060;
		}
	}
	{
		return (bool)0;
	}

IL_0060:
	{
		Il2CppObject * L_13 = (Il2CppObject *)__this->get__value_3();
		if (!L_13)
		{
			goto IL_0097;
		}
	}
	{
		Il2CppObject * L_14 = (Il2CppObject *)__this->get__value_3();
		V_1 = (Il2CppObject *)L_14;
		ImmutableTree_1_t969358333 * L_15 = V_0;
		NullCheck(L_15);
		Il2CppObject * L_16 = (Il2CppObject *)L_15->get__value_3();
		NullCheck((Il2CppObject *)(*(&V_1)));
		bool L_17 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&V_1)), (Il2CppObject *)L_16);
		G_B13_0 = ((((int32_t)L_17) == ((int32_t)0))? 1 : 0);
		goto IL_00a8;
	}

IL_0097:
	{
		ImmutableTree_1_t969358333 * L_18 = V_0;
		NullCheck(L_18);
		Il2CppObject * L_19 = (Il2CppObject *)L_18->get__value_3();
		G_B13_0 = ((((int32_t)((((Il2CppObject*)(Il2CppObject *)L_19) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_00a8:
	{
		if (!G_B13_0)
		{
			goto IL_00af;
		}
	}
	{
		return (bool)0;
	}

IL_00af:
	{
		return (bool)1;
	}
}
// System.Int32 Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::GetHashCode()
extern "C"  int32_t ImmutableTree_1_GetHashCode_m2582451103_gshared (ImmutableTree_1_t969358333 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__value_3();
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__value_3();
		V_1 = (Il2CppObject *)L_1;
		NullCheck((Il2CppObject *)(*(&V_1)));
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)(*(&V_1)));
		G_B3_0 = L_2;
		goto IL_002a;
	}

IL_0029:
	{
		G_B3_0 = 0;
	}

IL_002a:
	{
		V_0 = (int32_t)G_B3_0;
		int32_t L_3 = V_0;
		ImmutableSortedMap_2_t94277708 * L_4 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		G_B4_0 = ((int32_t)((int32_t)((int32_t)31)*(int32_t)L_3));
		if (!L_4)
		{
			G_B5_0 = ((int32_t)((int32_t)((int32_t)31)*(int32_t)L_3));
			goto IL_004a;
		}
	}
	{
		ImmutableSortedMap_2_t94277708 * L_5 = (ImmutableSortedMap_2_t94277708 *)__this->get__children_2();
		NullCheck((Il2CppObject *)L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)L_5);
		G_B6_0 = L_6;
		G_B6_1 = G_B4_0;
		goto IL_004b;
	}

IL_004a:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_004b:
	{
		V_0 = (int32_t)((int32_t)((int32_t)G_B6_1+(int32_t)G_B6_0));
		int32_t L_7 = V_0;
		return L_7;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<System.Object>::.cctor()
extern Il2CppClass* StandardComparator_1_t691330998_il2cpp_TypeInfo_var;
extern Il2CppClass* Builder_t2701728999_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const MethodInfo* StandardComparator_1_GetComparator_m2621741865_MethodInfo_var;
extern const MethodInfo* Builder_EmptyMap_m3858564147_MethodInfo_var;
extern const uint32_t ImmutableTree_1__cctor_m171993079_MetadataUsageId;
extern "C"  void ImmutableTree_1__cctor_m171993079_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImmutableTree_1__cctor_m171993079_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(StandardComparator_1_t691330998_il2cpp_TypeInfo_var);
		StandardComparator_1_t691330998 * L_0 = StandardComparator_1_GetComparator_m2621741865(NULL /*static, unused*/, /*hidden argument*/StandardComparator_1_GetComparator_m2621741865_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Builder_t2701728999_il2cpp_TypeInfo_var);
		ImmutableSortedMap_2_t94277708 * L_1 = Builder_EmptyMap_m3858564147(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/Builder_EmptyMap_m3858564147_MethodInfo_var);
		((ImmutableTree_1_t969358333_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyChildren_0(L_1);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_2 = V_0;
		ImmutableSortedMap_2_t94277708 * L_3 = ((ImmutableTree_1_t969358333_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_EmptyChildren_0();
		ImmutableTree_1_t969358333 * L_4 = (ImmutableTree_1_t969358333 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		((  void (*) (ImmutableTree_1_t969358333 *, Il2CppObject *, ImmutableSortedMap_2_t94277708 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_4, (Il2CppObject *)L_2, (ImmutableSortedMap_2_t94277708 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((ImmutableTree_1_t969358333_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_Empty_1(L_4);
		return;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.Predicate`1/Predicate6<System.Nullable`1<System.Boolean>>::.ctor()
extern "C"  void Predicate6__ctor_m3289419165_gshared (Predicate6_t842365816 * __this, const MethodInfo* method)
{
	{
		NullCheck((Predicate_1_t605805710 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Predicate_1_t605805710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Predicate_1_t605805710 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean Firebase.Database.Internal.Core.Utilities.Predicate`1/Predicate6<System.Nullable`1<System.Boolean>>::Evaluate(T)
extern "C"  bool Predicate6_Evaluate_m2929706496_gshared (Predicate6_t842365816 * __this, Nullable_1_t2088641033  ___object0, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.Predicate`1/Predicate6<System.Object>::.ctor()
extern "C"  void Predicate6__ctor_m2242370669_gshared (Predicate6_t1443174078 * __this, const MethodInfo* method)
{
	{
		NullCheck((Predicate_1_t1206613972 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (Predicate_1_t1206613972 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Predicate_1_t1206613972 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean Firebase.Database.Internal.Core.Utilities.Predicate`1/Predicate6<System.Object>::Evaluate(T)
extern "C"  bool Predicate6_Evaluate_m1553685562_gshared (Predicate6_t1443174078 * __this, Il2CppObject * ___object0, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.Predicate`1<System.Nullable`1<System.Boolean>>::.ctor()
extern "C"  void Predicate_1__ctor_m3549290457_gshared (Predicate_1_t605805710 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.Predicate`1<System.Nullable`1<System.Boolean>>::.cctor()
extern "C"  void Predicate_1__cctor_m612371706_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Predicate6_t842365816 * L_0 = (Predicate6_t842365816 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Predicate6_t842365816 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((Predicate_1_t605805710_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_True_0(L_0);
		return;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.Predicate`1<System.Object>::.ctor()
extern "C"  void Predicate_1__ctor_m2995668689_gshared (Predicate_1_t1206613972 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.Predicate`1<System.Object>::.cctor()
extern "C"  void Predicate_1__cctor_m1504777504_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Predicate6_t1443174078 * L_0 = (Predicate6_t1443174078 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (Predicate6_t1443174078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((Predicate_1_t1206613972_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->static_fields)->set_True_0(L_0);
		return;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.Tree`1/TreeVisitor111<System.Object>::.ctor(Firebase.Database.Internal.Core.Utilities.Tree`1/ITreeVisitor<T>,System.Boolean)
extern "C"  void TreeVisitor111__ctor_m2621602448_gshared (TreeVisitor111_t1617732397 * __this, Il2CppObject* ___visitor0, bool ___childrenFirst1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___visitor0;
		__this->set__visitor_1(L_0);
		bool L_1 = ___childrenFirst1;
		__this->set__childrenFirst_0(L_1);
		return;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.Tree`1/TreeVisitor111<System.Object>::VisitTree(Firebase.Database.Internal.Core.Utilities.Tree`1<T>)
extern "C"  void TreeVisitor111_VisitTree_m3204516422_gshared (TreeVisitor111_t1617732397 * __this, Tree_1_t3114744003 * ___tree0, const MethodInfo* method)
{
	{
		Tree_1_t3114744003 * L_0 = ___tree0;
		Il2CppObject* L_1 = (Il2CppObject*)__this->get__visitor_1();
		bool L_2 = (bool)__this->get__childrenFirst_0();
		NullCheck((Tree_1_t3114744003 *)L_0);
		VirtActionInvoker3< Il2CppObject*, bool, bool >::Invoke(11 /* System.Void Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::ForEachDescendant(Firebase.Database.Internal.Core.Utilities.Tree`1/ITreeVisitor<T>,System.Boolean,System.Boolean) */, (Tree_1_t3114744003 *)L_0, (Il2CppObject*)L_1, (bool)1, (bool)L_2);
		return;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::.ctor(Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.Utilities.Tree`1<T>,Firebase.Database.Internal.Core.Utilities.TreeNode`1<T>)
extern "C"  void Tree_1__ctor_m3666705606_gshared (Tree_1_t3114744003 * __this, ChildKey_t1197802383 * ___name0, Tree_1_t3114744003 * ___parent1, TreeNode_1_t3829898795 * ___node2, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		ChildKey_t1197802383 * L_0 = ___name0;
		__this->set__name_0(L_0);
		Tree_1_t3114744003 * L_1 = ___parent1;
		__this->set__parent_2(L_1);
		TreeNode_1_t3829898795 * L_2 = ___node2;
		__this->set__node_1(L_2);
		return;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::.ctor()
extern "C"  void Tree_1__ctor_m2140393204_gshared (Tree_1_t3114744003 * __this, const MethodInfo* method)
{
	{
		TreeNode_1_t3829898795 * L_0 = (TreeNode_1_t3829898795 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (TreeNode_1_t3829898795 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		NullCheck((Tree_1_t3114744003 *)__this);
		((  void (*) (Tree_1_t3114744003 *, ChildKey_t1197802383 *, Tree_1_t3114744003 *, TreeNode_1_t3829898795 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Tree_1_t3114744003 *)__this, (ChildKey_t1197802383 *)NULL, (Tree_1_t3114744003 *)NULL, (TreeNode_1_t3829898795 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// Firebase.Database.Internal.Core.Utilities.Tree`1<T> Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::SubTree(Firebase.Database.Internal.Core.Path)
extern Il2CppClass* Extensions_t996338116_il2cpp_TypeInfo_var;
extern const MethodInfo* Extensions_Contains_TisChildKey_t1197802383_TisIl2CppObject_m1879317483_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m1780405376_MethodInfo_var;
extern const uint32_t Tree_1_SubTree_m3478601559_MetadataUsageId;
extern "C"  Tree_1_t3114744003 * Tree_1_SubTree_m3478601559_gshared (Tree_1_t3114744003 * __this, Path_t2568473163 * ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tree_1_SubTree_m3478601559_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Tree_1_t3114744003 * V_0 = NULL;
	ChildKey_t1197802383 * V_1 = NULL;
	TreeNode_1_t3829898795 * V_2 = NULL;
	TreeNode_1_t3829898795 * G_B4_0 = NULL;
	{
		V_0 = (Tree_1_t3114744003 *)__this;
		Path_t2568473163 * L_0 = ___path0;
		NullCheck((Path_t2568473163 *)L_0);
		ChildKey_t1197802383 * L_1 = VirtFuncInvoker0< ChildKey_t1197802383 * >::Invoke(13 /* Firebase.Database.Internal.Snapshot.ChildKey Firebase.Database.Internal.Core.Path::GetFront() */, (Path_t2568473163 *)L_0);
		V_1 = (ChildKey_t1197802383 *)L_1;
		goto IL_005d;
	}

IL_000e:
	{
		Tree_1_t3114744003 * L_2 = V_0;
		NullCheck(L_2);
		TreeNode_1_t3829898795 * L_3 = (TreeNode_1_t3829898795 *)L_2->get__node_1();
		NullCheck(L_3);
		Dictionary_2_t3453659887 * L_4 = (Dictionary_2_t3453659887 *)L_3->get_Children_0();
		ChildKey_t1197802383 * L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t996338116_il2cpp_TypeInfo_var);
		bool L_6 = Extensions_Contains_TisChildKey_t1197802383_TisIl2CppObject_m1879317483(NULL /*static, unused*/, (Il2CppObject*)L_4, (ChildKey_t1197802383 *)L_5, /*hidden argument*/Extensions_Contains_TisChildKey_t1197802383_TisIl2CppObject_m1879317483_MethodInfo_var);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		Tree_1_t3114744003 * L_7 = V_0;
		NullCheck(L_7);
		TreeNode_1_t3829898795 * L_8 = (TreeNode_1_t3829898795 *)L_7->get__node_1();
		NullCheck(L_8);
		Dictionary_2_t3453659887 * L_9 = (Dictionary_2_t3453659887 *)L_8->get_Children_0();
		ChildKey_t1197802383 * L_10 = V_1;
		NullCheck((Dictionary_2_t3453659887 *)L_9);
		Il2CppObject * L_11 = Dictionary_2_get_Item_m1780405376((Dictionary_2_t3453659887 *)L_9, (ChildKey_t1197802383 *)L_10, /*hidden argument*/Dictionary_2_get_Item_m1780405376_MethodInfo_var);
		G_B4_0 = ((TreeNode_1_t3829898795 *)Castclass(L_11, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		goto IL_0044;
	}

IL_003f:
	{
		TreeNode_1_t3829898795 * L_12 = (TreeNode_1_t3829898795 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (TreeNode_1_t3829898795 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		G_B4_0 = L_12;
	}

IL_0044:
	{
		V_2 = (TreeNode_1_t3829898795 *)G_B4_0;
		ChildKey_t1197802383 * L_13 = V_1;
		Tree_1_t3114744003 * L_14 = V_0;
		TreeNode_1_t3829898795 * L_15 = V_2;
		Tree_1_t3114744003 * L_16 = (Tree_1_t3114744003 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (Tree_1_t3114744003 *, ChildKey_t1197802383 *, Tree_1_t3114744003 *, TreeNode_1_t3829898795 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_16, (ChildKey_t1197802383 *)L_13, (Tree_1_t3114744003 *)L_14, (TreeNode_1_t3829898795 *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (Tree_1_t3114744003 *)L_16;
		Path_t2568473163 * L_17 = ___path0;
		NullCheck((Path_t2568473163 *)L_17);
		Path_t2568473163 * L_18 = VirtFuncInvoker0< Path_t2568473163 * >::Invoke(14 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Path::PopFront() */, (Path_t2568473163 *)L_17);
		___path0 = (Path_t2568473163 *)L_18;
		Path_t2568473163 * L_19 = ___path0;
		NullCheck((Path_t2568473163 *)L_19);
		ChildKey_t1197802383 * L_20 = VirtFuncInvoker0< ChildKey_t1197802383 * >::Invoke(13 /* Firebase.Database.Internal.Snapshot.ChildKey Firebase.Database.Internal.Core.Path::GetFront() */, (Path_t2568473163 *)L_19);
		V_1 = (ChildKey_t1197802383 *)L_20;
	}

IL_005d:
	{
		ChildKey_t1197802383 * L_21 = V_1;
		if (L_21)
		{
			goto IL_000e;
		}
	}
	{
		Tree_1_t3114744003 * L_22 = V_0;
		return L_22;
	}
}
// T Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::GetValue()
extern "C"  Il2CppObject * Tree_1_GetValue_m3153563740_gshared (Tree_1_t3114744003 * __this, const MethodInfo* method)
{
	{
		TreeNode_1_t3829898795 * L_0 = (TreeNode_1_t3829898795 *)__this->get__node_1();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_Value_1();
		return L_1;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::SetValue(T)
extern "C"  void Tree_1_SetValue_m3428075119_gshared (Tree_1_t3114744003 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		TreeNode_1_t3829898795 * L_0 = (TreeNode_1_t3829898795 *)__this->get__node_1();
		Il2CppObject * L_1 = ___value0;
		NullCheck(L_0);
		L_0->set_Value_1(L_1);
		NullCheck((Tree_1_t3114744003 *)__this);
		((  void (*) (Tree_1_t3114744003 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Tree_1_t3114744003 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}
}
// Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::GetPath()
extern Il2CppClass* ChildKeyU5BU5D_t491817302_il2cpp_TypeInfo_var;
extern Il2CppClass* Path_t2568473163_il2cpp_TypeInfo_var;
extern const uint32_t Tree_1_GetPath_m2527014843_MetadataUsageId;
extern "C"  Path_t2568473163 * Tree_1_GetPath_m2527014843_gshared (Tree_1_t3114744003 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tree_1_GetPath_m2527014843_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Path_t2568473163 * G_B5_0 = NULL;
	{
		Tree_1_t3114744003 * L_0 = (Tree_1_t3114744003 *)__this->get__parent_2();
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		Tree_1_t3114744003 * L_1 = (Tree_1_t3114744003 *)__this->get__parent_2();
		NullCheck((Tree_1_t3114744003 *)L_1);
		Path_t2568473163 * L_2 = VirtFuncInvoker0< Path_t2568473163 * >::Invoke(7 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::GetPath() */, (Tree_1_t3114744003 *)L_1);
		ChildKey_t1197802383 * L_3 = (ChildKey_t1197802383 *)__this->get__name_0();
		NullCheck((Path_t2568473163 *)L_2);
		Path_t2568473163 * L_4 = VirtFuncInvoker1< Path_t2568473163 *, ChildKey_t1197802383 * >::Invoke(10 /* Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Path::Child(Firebase.Database.Internal.Snapshot.ChildKey) */, (Path_t2568473163 *)L_2, (ChildKey_t1197802383 *)L_3);
		return L_4;
	}

IL_0022:
	{
		ChildKey_t1197802383 * L_5 = (ChildKey_t1197802383 *)__this->get__name_0();
		if (!L_5)
		{
			goto IL_0046;
		}
	}
	{
		ChildKeyU5BU5D_t491817302* L_6 = (ChildKeyU5BU5D_t491817302*)((ChildKeyU5BU5D_t491817302*)SZArrayNew(ChildKeyU5BU5D_t491817302_il2cpp_TypeInfo_var, (uint32_t)1));
		ChildKey_t1197802383 * L_7 = (ChildKey_t1197802383 *)__this->get__name_0();
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (ChildKey_t1197802383 *)L_7);
		Path_t2568473163 * L_8 = (Path_t2568473163 *)il2cpp_codegen_object_new(Path_t2568473163_il2cpp_TypeInfo_var);
		Path__ctor_m2358381126(L_8, (ChildKeyU5BU5D_t491817302*)L_6, /*hidden argument*/NULL);
		G_B5_0 = L_8;
		goto IL_004b;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Path_t2568473163_il2cpp_TypeInfo_var);
		Path_t2568473163 * L_9 = Path_GetEmptyPath_m3969996076(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B5_0 = L_9;
	}

IL_004b:
	{
		return G_B5_0;
	}
}
// System.Boolean Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::HasChildren()
extern Il2CppClass* Extensions_t996338116_il2cpp_TypeInfo_var;
extern const MethodInfo* Extensions_IsEmpty_TisKeyValuePair_2_t1211005109_m2908331326_MethodInfo_var;
extern const uint32_t Tree_1_HasChildren_m2129150403_MetadataUsageId;
extern "C"  bool Tree_1_HasChildren_m2129150403_gshared (Tree_1_t3114744003 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tree_1_HasChildren_m2129150403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TreeNode_1_t3829898795 * L_0 = (TreeNode_1_t3829898795 *)__this->get__node_1();
		NullCheck(L_0);
		Dictionary_2_t3453659887 * L_1 = (Dictionary_2_t3453659887 *)L_0->get_Children_0();
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t996338116_il2cpp_TypeInfo_var);
		bool L_2 = Extensions_IsEmpty_TisKeyValuePair_2_t1211005109_m2908331326(NULL /*static, unused*/, (Il2CppObject*)L_1, /*hidden argument*/Extensions_IsEmpty_TisKeyValuePair_2_t1211005109_m2908331326_MethodInfo_var);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::IsEmpty()
extern Il2CppClass* Extensions_t996338116_il2cpp_TypeInfo_var;
extern const MethodInfo* Extensions_IsEmpty_TisKeyValuePair_2_t1211005109_m2908331326_MethodInfo_var;
extern const uint32_t Tree_1_IsEmpty_m3123790885_MetadataUsageId;
extern "C"  bool Tree_1_IsEmpty_m3123790885_gshared (Tree_1_t3114744003 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tree_1_IsEmpty_m3123790885_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		TreeNode_1_t3829898795 * L_0 = (TreeNode_1_t3829898795 *)__this->get__node_1();
		NullCheck(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)L_0->get_Value_1();
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		TreeNode_1_t3829898795 * L_2 = (TreeNode_1_t3829898795 *)__this->get__node_1();
		NullCheck(L_2);
		Dictionary_2_t3453659887 * L_3 = (Dictionary_2_t3453659887 *)L_2->get_Children_0();
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t996338116_il2cpp_TypeInfo_var);
		bool L_4 = Extensions_IsEmpty_TisKeyValuePair_2_t1211005109_m2908331326(NULL /*static, unused*/, (Il2CppObject*)L_3, /*hidden argument*/Extensions_IsEmpty_TisKeyValuePair_2_t1211005109_m2908331326_MethodInfo_var);
		G_B3_0 = ((int32_t)(L_4));
		goto IL_0028;
	}

IL_0027:
	{
		G_B3_0 = 0;
	}

IL_0028:
	{
		return (bool)G_B3_0;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::ForEachDescendant(Firebase.Database.Internal.Core.Utilities.Tree`1/ITreeVisitor<T>)
extern "C"  void Tree_1_ForEachDescendant_m2029243360_gshared (Tree_1_t3114744003 * __this, Il2CppObject* ___visitor0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___visitor0;
		NullCheck((Tree_1_t3114744003 *)__this);
		VirtActionInvoker3< Il2CppObject*, bool, bool >::Invoke(11 /* System.Void Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::ForEachDescendant(Firebase.Database.Internal.Core.Utilities.Tree`1/ITreeVisitor<T>,System.Boolean,System.Boolean) */, (Tree_1_t3114744003 *)__this, (Il2CppObject*)L_0, (bool)0, (bool)0);
		return;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::ForEachDescendant(Firebase.Database.Internal.Core.Utilities.Tree`1/ITreeVisitor<T>,System.Boolean,System.Boolean)
extern "C"  void Tree_1_ForEachDescendant_m897105138_gshared (Tree_1_t3114744003 * __this, Il2CppObject* ___visitor0, bool ___includeSelf1, bool ___childrenFirst2, const MethodInfo* method)
{
	{
		bool L_0 = ___includeSelf1;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		bool L_1 = ___childrenFirst2;
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		Il2CppObject* L_2 = ___visitor0;
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker1< Tree_1_t3114744003 * >::Invoke(0 /* System.Void Firebase.Database.Internal.Core.Utilities.Tree`1/ITreeVisitor<System.Object>::VisitTree(Firebase.Database.Internal.Core.Utilities.Tree`1<T>) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (Il2CppObject*)L_2, (Tree_1_t3114744003 *)__this);
	}

IL_0013:
	{
		Il2CppObject* L_3 = ___visitor0;
		bool L_4 = ___childrenFirst2;
		TreeVisitor111_t1617732397 * L_5 = (TreeVisitor111_t1617732397 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9));
		((  void (*) (TreeVisitor111_t1617732397 *, Il2CppObject*, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)(L_5, (Il2CppObject*)L_3, (bool)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		NullCheck((Tree_1_t3114744003 *)__this);
		VirtActionInvoker1< Il2CppObject* >::Invoke(14 /* System.Void Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::ForEachChild(Firebase.Database.Internal.Core.Utilities.Tree`1/ITreeVisitor<T>) */, (Tree_1_t3114744003 *)__this, (Il2CppObject*)L_5);
		bool L_6 = ___includeSelf1;
		if (!L_6)
		{
			goto IL_0033;
		}
	}
	{
		bool L_7 = ___childrenFirst2;
		if (!L_7)
		{
			goto IL_0033;
		}
	}
	{
		Il2CppObject* L_8 = ___visitor0;
		NullCheck((Il2CppObject*)L_8);
		InterfaceActionInvoker1< Tree_1_t3114744003 * >::Invoke(0 /* System.Void Firebase.Database.Internal.Core.Utilities.Tree`1/ITreeVisitor<System.Object>::VisitTree(Firebase.Database.Internal.Core.Utilities.Tree`1<T>) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (Il2CppObject*)L_8, (Tree_1_t3114744003 *)__this);
	}

IL_0033:
	{
		return;
	}
}
// System.Boolean Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::ForEachAncestor(Firebase.Database.Internal.Core.Utilities.Tree`1/ITreeFilter<T>)
extern "C"  bool Tree_1_ForEachAncestor_m3932405846_gshared (Tree_1_t3114744003 * __this, Il2CppObject* ___filter0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___filter0;
		NullCheck((Tree_1_t3114744003 *)__this);
		bool L_1 = VirtFuncInvoker2< bool, Il2CppObject*, bool >::Invoke(13 /* System.Boolean Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::ForEachAncestor(Firebase.Database.Internal.Core.Utilities.Tree`1/ITreeFilter<T>,System.Boolean) */, (Tree_1_t3114744003 *)__this, (Il2CppObject*)L_0, (bool)0);
		return L_1;
	}
}
// System.Boolean Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::ForEachAncestor(Firebase.Database.Internal.Core.Utilities.Tree`1/ITreeFilter<T>,System.Boolean)
extern "C"  bool Tree_1_ForEachAncestor_m1658107771_gshared (Tree_1_t3114744003 * __this, Il2CppObject* ___filter0, bool ___includeSelf1, const MethodInfo* method)
{
	Tree_1_t3114744003 * V_0 = NULL;
	Tree_1_t3114744003 * G_B3_0 = NULL;
	{
		bool L_0 = ___includeSelf1;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		G_B3_0 = ((Tree_1_t3114744003 *)(__this));
		goto IL_0012;
	}

IL_000c:
	{
		Tree_1_t3114744003 * L_1 = (Tree_1_t3114744003 *)__this->get__parent_2();
		G_B3_0 = ((Tree_1_t3114744003 *)(L_1));
	}

IL_0012:
	{
		V_0 = (Tree_1_t3114744003 *)G_B3_0;
		goto IL_002d;
	}

IL_0018:
	{
		Il2CppObject* L_2 = ___filter0;
		Tree_1_t3114744003 * L_3 = V_0;
		NullCheck((Il2CppObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, Tree_1_t3114744003 * >::Invoke(0 /* System.Boolean Firebase.Database.Internal.Core.Utilities.Tree`1/ITreeFilter<System.Object>::FilterTreeNode(Firebase.Database.Internal.Core.Utilities.Tree`1<T>) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13), (Il2CppObject*)L_2, (Tree_1_t3114744003 *)L_3);
		if (!L_4)
		{
			goto IL_0026;
		}
	}
	{
		return (bool)1;
	}

IL_0026:
	{
		Tree_1_t3114744003 * L_5 = V_0;
		NullCheck(L_5);
		Tree_1_t3114744003 * L_6 = (Tree_1_t3114744003 *)L_5->get__parent_2();
		V_0 = (Tree_1_t3114744003 *)L_6;
	}

IL_002d:
	{
		Tree_1_t3114744003 * L_7 = V_0;
		if (L_7)
		{
			goto IL_0018;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::ForEachChild(Firebase.Database.Internal.Core.Utilities.Tree`1/ITreeVisitor<T>)
extern Il2CppClass* KeyValuePair_2U5BU5D_t3571829976_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t478717293_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Count_m2261478175_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m129487310_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1368213154_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1256938234_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m182894088_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m689905909_MethodInfo_var;
extern const uint32_t Tree_1_ForEachChild_m1884352577_MetadataUsageId;
extern "C"  void Tree_1_ForEachChild_m1884352577_gshared (Tree_1_t3114744003 * __this, Il2CppObject* ___visitor0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tree_1_ForEachChild_m1884352577_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2U5BU5D_t3571829976* V_0 = NULL;
	int32_t V_1 = 0;
	KeyValuePair_2_t1211005109  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Enumerator_t478717293  V_3;
	memset(&V_3, 0, sizeof(V_3));
	KeyValuePair_2_t1211005109  V_4;
	memset(&V_4, 0, sizeof(V_4));
	KeyValuePair_2U5BU5D_t3571829976* V_5 = NULL;
	int32_t V_6 = 0;
	Tree_1_t3114744003 * V_7 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		TreeNode_1_t3829898795 * L_0 = (TreeNode_1_t3829898795 *)__this->get__node_1();
		NullCheck(L_0);
		Dictionary_2_t3453659887 * L_1 = (Dictionary_2_t3453659887 *)L_0->get_Children_0();
		NullCheck((Dictionary_2_t3453659887 *)L_1);
		int32_t L_2 = Dictionary_2_get_Count_m2261478175((Dictionary_2_t3453659887 *)L_1, /*hidden argument*/Dictionary_2_get_Count_m2261478175_MethodInfo_var);
		V_0 = (KeyValuePair_2U5BU5D_t3571829976*)((KeyValuePair_2U5BU5D_t3571829976*)SZArrayNew(KeyValuePair_2U5BU5D_t3571829976_il2cpp_TypeInfo_var, (uint32_t)L_2));
		V_1 = (int32_t)0;
		TreeNode_1_t3829898795 * L_3 = (TreeNode_1_t3829898795 *)__this->get__node_1();
		NullCheck(L_3);
		Dictionary_2_t3453659887 * L_4 = (Dictionary_2_t3453659887 *)L_3->get_Children_0();
		NullCheck((Dictionary_2_t3453659887 *)L_4);
		Enumerator_t478717293  L_5 = Dictionary_2_GetEnumerator_m129487310((Dictionary_2_t3453659887 *)L_4, /*hidden argument*/Dictionary_2_GetEnumerator_m129487310_MethodInfo_var);
		V_3 = (Enumerator_t478717293 )L_5;
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0047;
		}

IL_002e:
		{
			KeyValuePair_2_t1211005109  L_6 = Enumerator_get_Current_m1368213154((Enumerator_t478717293 *)(&V_3), /*hidden argument*/Enumerator_get_Current_m1368213154_MethodInfo_var);
			V_2 = (KeyValuePair_2_t1211005109 )L_6;
			KeyValuePair_2U5BU5D_t3571829976* L_7 = V_0;
			int32_t L_8 = V_1;
			NullCheck(L_7);
			KeyValuePair_2_t1211005109  L_9 = V_2;
			(*(KeyValuePair_2_t1211005109 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))) = L_9;
			int32_t L_10 = V_1;
			V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
		}

IL_0047:
		{
			bool L_11 = Enumerator_MoveNext_m1256938234((Enumerator_t478717293 *)(&V_3), /*hidden argument*/Enumerator_MoveNext_m1256938234_MethodInfo_var);
			if (L_11)
			{
				goto IL_002e;
			}
		}

IL_0053:
		{
			IL2CPP_LEAVE(0x66, FINALLY_0058);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0058;
	}

FINALLY_0058:
	{ // begin finally (depth: 1)
		Il2CppClass* il2cpp_this_typeinfo_96 = Enumerator_t478717293_il2cpp_TypeInfo_var;
		int32_t il2cpp_interface_offset__96 = il2cpp_codegen_class_interface_offset(il2cpp_this_typeinfo_96, IDisposable_t2427283555_il2cpp_TypeInfo_var);
		((  void (*) (Il2CppObject *, const MethodInfo*))il2cpp_this_typeinfo_96->vtable[0 + il2cpp_interface_offset__96].methodPtr)((Il2CppObject *)il2cpp_codegen_fake_box((&V_3)), /*hidden argument*/il2cpp_this_typeinfo_96->vtable[0 + il2cpp_interface_offset__96].method);
		IL2CPP_END_FINALLY(88)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(88)
	{
		IL2CPP_JUMP_TBL(0x66, IL_0066)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0066:
	{
		KeyValuePair_2U5BU5D_t3571829976* L_12 = V_0;
		V_5 = (KeyValuePair_2U5BU5D_t3571829976*)L_12;
		V_6 = (int32_t)0;
		goto IL_00aa;
	}

IL_0071:
	{
		KeyValuePair_2U5BU5D_t3571829976* L_13 = V_5;
		int32_t L_14 = V_6;
		NullCheck(L_13);
		V_4 = (KeyValuePair_2_t1211005109 )(*(KeyValuePair_2_t1211005109 *)((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_14))));
		ChildKey_t1197802383 * L_15 = KeyValuePair_2_get_Key_m182894088((KeyValuePair_2_t1211005109 *)(&V_4), /*hidden argument*/KeyValuePair_2_get_Key_m182894088_MethodInfo_var);
		Il2CppObject * L_16 = KeyValuePair_2_get_Value_m689905909((KeyValuePair_2_t1211005109 *)(&V_4), /*hidden argument*/KeyValuePair_2_get_Value_m689905909_MethodInfo_var);
		Tree_1_t3114744003 * L_17 = (Tree_1_t3114744003 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (Tree_1_t3114744003 *, ChildKey_t1197802383 *, Tree_1_t3114744003 *, TreeNode_1_t3829898795 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_17, (ChildKey_t1197802383 *)L_15, (Tree_1_t3114744003 *)__this, (TreeNode_1_t3829898795 *)((TreeNode_1_t3829898795 *)Castclass(L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_7 = (Tree_1_t3114744003 *)L_17;
		Il2CppObject* L_18 = ___visitor0;
		Tree_1_t3114744003 * L_19 = V_7;
		NullCheck((Il2CppObject*)L_18);
		InterfaceActionInvoker1< Tree_1_t3114744003 * >::Invoke(0 /* System.Void Firebase.Database.Internal.Core.Utilities.Tree`1/ITreeVisitor<System.Object>::VisitTree(Firebase.Database.Internal.Core.Utilities.Tree`1<T>) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (Il2CppObject*)L_18, (Tree_1_t3114744003 *)L_19);
		int32_t L_20 = V_6;
		V_6 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_00aa:
	{
		int32_t L_21 = V_6;
		KeyValuePair_2U5BU5D_t3571829976* L_22 = V_5;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))))))
		{
			goto IL_0071;
		}
	}
	{
		return;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::UpdateParents()
extern "C"  void Tree_1_UpdateParents_m1697369238_gshared (Tree_1_t3114744003 * __this, const MethodInfo* method)
{
	{
		Tree_1_t3114744003 * L_0 = (Tree_1_t3114744003 *)__this->get__parent_2();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Tree_1_t3114744003 * L_1 = (Tree_1_t3114744003 *)__this->get__parent_2();
		ChildKey_t1197802383 * L_2 = (ChildKey_t1197802383 *)__this->get__name_0();
		NullCheck((Tree_1_t3114744003 *)L_1);
		((  void (*) (Tree_1_t3114744003 *, ChildKey_t1197802383 *, Tree_1_t3114744003 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((Tree_1_t3114744003 *)L_1, (ChildKey_t1197802383 *)L_2, (Tree_1_t3114744003 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
	}

IL_001d:
	{
		return;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::UpdateChild(Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.Utilities.Tree`1<T>)
extern Il2CppClass* Extensions_t996338116_il2cpp_TypeInfo_var;
extern Il2CppClass* Collections_t4125780067_il2cpp_TypeInfo_var;
extern const MethodInfo* Extensions_Contains_TisChildKey_t1197802383_TisIl2CppObject_m1879317483_MethodInfo_var;
extern const MethodInfo* Collections_Remove_TisChildKey_t1197802383_TisIl2CppObject_m2048268851_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m3298269467_MethodInfo_var;
extern const uint32_t Tree_1_UpdateChild_m2370595022_MetadataUsageId;
extern "C"  void Tree_1_UpdateChild_m2370595022_gshared (Tree_1_t3114744003 * __this, ChildKey_t1197802383 * ___name0, Tree_1_t3114744003 * ___child1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tree_1_UpdateChild_m2370595022_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		Tree_1_t3114744003 * L_0 = ___child1;
		NullCheck((Tree_1_t3114744003 *)L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::IsEmpty() */, (Tree_1_t3114744003 *)L_0);
		V_0 = (bool)L_1;
		TreeNode_1_t3829898795 * L_2 = (TreeNode_1_t3829898795 *)__this->get__node_1();
		NullCheck(L_2);
		Dictionary_2_t3453659887 * L_3 = (Dictionary_2_t3453659887 *)L_2->get_Children_0();
		ChildKey_t1197802383 * L_4 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t996338116_il2cpp_TypeInfo_var);
		bool L_5 = Extensions_Contains_TisChildKey_t1197802383_TisIl2CppObject_m1879317483(NULL /*static, unused*/, (Il2CppObject*)L_3, (ChildKey_t1197802383 *)L_4, /*hidden argument*/Extensions_Contains_TisChildKey_t1197802383_TisIl2CppObject_m1879317483_MethodInfo_var);
		V_1 = (bool)L_5;
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		bool L_7 = V_1;
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		TreeNode_1_t3829898795 * L_8 = (TreeNode_1_t3829898795 *)__this->get__node_1();
		NullCheck(L_8);
		Dictionary_2_t3453659887 * L_9 = (Dictionary_2_t3453659887 *)L_8->get_Children_0();
		ChildKey_t1197802383 * L_10 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(Collections_t4125780067_il2cpp_TypeInfo_var);
		Collections_Remove_TisChildKey_t1197802383_TisIl2CppObject_m2048268851(NULL /*static, unused*/, (Il2CppObject*)L_9, (ChildKey_t1197802383 *)L_10, /*hidden argument*/Collections_Remove_TisChildKey_t1197802383_TisIl2CppObject_m2048268851_MethodInfo_var);
		NullCheck((Tree_1_t3114744003 *)__this);
		((  void (*) (Tree_1_t3114744003 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Tree_1_t3114744003 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		goto IL_006b;
	}

IL_0042:
	{
		bool L_11 = V_0;
		if (L_11)
		{
			goto IL_006b;
		}
	}
	{
		bool L_12 = V_1;
		if (L_12)
		{
			goto IL_006b;
		}
	}
	{
		TreeNode_1_t3829898795 * L_13 = (TreeNode_1_t3829898795 *)__this->get__node_1();
		NullCheck(L_13);
		Dictionary_2_t3453659887 * L_14 = (Dictionary_2_t3453659887 *)L_13->get_Children_0();
		ChildKey_t1197802383 * L_15 = ___name0;
		Tree_1_t3114744003 * L_16 = ___child1;
		NullCheck(L_16);
		TreeNode_1_t3829898795 * L_17 = (TreeNode_1_t3829898795 *)L_16->get__node_1();
		NullCheck((Dictionary_2_t3453659887 *)L_14);
		Dictionary_2_set_Item_m3298269467((Dictionary_2_t3453659887 *)L_14, (ChildKey_t1197802383 *)L_15, (Il2CppObject *)L_17, /*hidden argument*/Dictionary_2_set_Item_m3298269467_MethodInfo_var);
		NullCheck((Tree_1_t3114744003 *)__this);
		((  void (*) (Tree_1_t3114744003 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Tree_1_t3114744003 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_006b:
	{
		return;
	}
}
// System.String Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Tree_1_ToString_m2645147141_MetadataUsageId;
extern "C"  String_t* Tree_1_ToString_m2645147141_gshared (Tree_1_t3114744003 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tree_1_ToString_m2645147141_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck((Tree_1_t3114744003 *)__this);
		String_t* L_1 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(15 /* System.String Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::ToStringExpr(System.String) */, (Tree_1_t3114744003 *)__this, (String_t*)L_0);
		return L_1;
	}
}
// System.String Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>::ToStringExpr(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2284400962;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern Il2CppCodeGenString* _stringLiteral372029349;
extern const uint32_t Tree_1_ToStringExpr_m2908131828_MetadataUsageId;
extern "C"  String_t* Tree_1_ToStringExpr_m2908131828_gshared (Tree_1_t3114744003 * __this, String_t* ___prefix0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tree_1_ToStringExpr_m2908131828_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B3_0 = NULL;
	{
		ChildKey_t1197802383 * L_0 = (ChildKey_t1197802383 *)__this->get__name_0();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		G_B3_0 = _stringLiteral2284400962;
		goto IL_0020;
	}

IL_0015:
	{
		ChildKey_t1197802383 * L_1 = (ChildKey_t1197802383 *)__this->get__name_0();
		NullCheck((ChildKey_t1197802383 *)L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Firebase.Database.Internal.Snapshot.ChildKey::AsString() */, (ChildKey_t1197802383 *)L_1);
		G_B3_0 = L_2;
	}

IL_0020:
	{
		V_0 = (String_t*)G_B3_0;
		String_t* L_3 = ___prefix0;
		String_t* L_4 = V_0;
		TreeNode_1_t3829898795 * L_5 = (TreeNode_1_t3829898795 *)__this->get__node_1();
		String_t* L_6 = ___prefix0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)L_6, (String_t*)_stringLiteral372029349, /*hidden argument*/NULL);
		NullCheck((TreeNode_1_t3829898795 *)L_5);
		String_t* L_8 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(4 /* System.String Firebase.Database.Internal.Core.Utilities.TreeNode`1<System.Object>::ToStringExpr(System.String) */, (TreeNode_1_t3829898795 *)L_5, (String_t*)L_7);
		String_t* L_9 = String_Concat_m1561703559(NULL /*static, unused*/, (String_t*)L_3, (String_t*)L_4, (String_t*)_stringLiteral372029352, (String_t*)L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void Firebase.Database.Internal.Core.Utilities.TreeNode`1<System.Object>::.ctor()
extern Il2CppClass* Dictionary_2_t3453659887_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3440820291_MethodInfo_var;
extern const uint32_t TreeNode_1__ctor_m4209293980_MetadataUsageId;
extern "C"  void TreeNode_1__ctor_m4209293980_gshared (TreeNode_1_t3829898795 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TreeNode_1__ctor_m4209293980_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3453659887 * L_0 = (Dictionary_2_t3453659887 *)il2cpp_codegen_object_new(Dictionary_2_t3453659887_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3440820291(L_0, /*hidden argument*/Dictionary_2__ctor_m3440820291_MethodInfo_var);
		__this->set_Children_0(L_0);
		return;
	}
}
// System.String Firebase.Database.Internal.Core.Utilities.TreeNode`1<System.Object>::ToStringExpr(System.String)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Extensions_t996338116_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t478717293_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2981496232_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern const MethodInfo* Extensions_IsEmpty_TisKeyValuePair_2_t1211005109_m2908331326_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m129487310_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m182894088_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m689905909_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2186283539;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern Il2CppCodeGenString* _stringLiteral3140364395;
extern Il2CppCodeGenString* _stringLiteral2162321578;
extern Il2CppCodeGenString* _stringLiteral372029349;
extern const uint32_t TreeNode_1_ToStringExpr_m422485072_MetadataUsageId;
extern "C"  String_t* TreeNode_1_ToStringExpr_m422485072_gshared (TreeNode_1_t3829898795 * __this, String_t* ___prefix0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TreeNode_1_ToStringExpr_m422485072_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	KeyValuePair_2_t1211005109  V_2;
	memset(&V_2, 0, sizeof(V_2));
	String_t* V_3 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		String_t* L_1 = ___prefix0;
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)L_0;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, _stringLiteral2186283539);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral2186283539);
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)L_2;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_Value_1();
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = (ObjectU5BU5D_t3614634134*)L_3;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral372029352);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral372029352);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_5, /*hidden argument*/NULL);
		V_0 = (String_t*)L_6;
		Dictionary_2_t3453659887 * L_7 = (Dictionary_2_t3453659887 *)__this->get_Children_0();
		IL2CPP_RUNTIME_CLASS_INIT(Extensions_t996338116_il2cpp_TypeInfo_var);
		bool L_8 = Extensions_IsEmpty_TisKeyValuePair_2_t1211005109_m2908331326(NULL /*static, unused*/, (Il2CppObject*)L_7, /*hidden argument*/Extensions_IsEmpty_TisKeyValuePair_2_t1211005109_m2908331326_MethodInfo_var);
		if (!L_8)
		{
			goto IL_004b;
		}
	}
	{
		String_t* L_9 = V_0;
		String_t* L_10 = ___prefix0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m612901809(NULL /*static, unused*/, (String_t*)L_9, (String_t*)L_10, (String_t*)_stringLiteral3140364395, /*hidden argument*/NULL);
		return L_11;
	}

IL_004b:
	{
		Dictionary_2_t3453659887 * L_12 = (Dictionary_2_t3453659887 *)__this->get_Children_0();
		NullCheck((Dictionary_2_t3453659887 *)L_12);
		Enumerator_t478717293  L_13 = Dictionary_2_GetEnumerator_m129487310((Dictionary_2_t3453659887 *)L_12, /*hidden argument*/Dictionary_2_GetEnumerator_m129487310_MethodInfo_var);
		Enumerator_t478717293  L_14 = L_13;
		Il2CppObject * L_15 = Box(Enumerator_t478717293_il2cpp_TypeInfo_var, &L_14);
		V_1 = (Il2CppObject*)L_15;
		goto IL_00b7;
	}

IL_0061:
	{
		Il2CppObject* L_16 = V_1;
		NullCheck((Il2CppObject*)L_16);
		KeyValuePair_2_t1211005109  L_17 = InterfaceFuncInvoker0< KeyValuePair_2_t1211005109  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>>::get_Current() */, IEnumerator_1_t2981496232_il2cpp_TypeInfo_var, (Il2CppObject*)L_16);
		V_2 = (KeyValuePair_2_t1211005109 )L_17;
		String_t* L_18 = V_0;
		V_3 = (String_t*)L_18;
		ObjectU5BU5D_t3614634134* L_19 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)6));
		String_t* L_20 = V_3;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_20);
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)L_19;
		String_t* L_22 = ___prefix0;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)L_21;
		ChildKey_t1197802383 * L_24 = KeyValuePair_2_get_Key_m182894088((KeyValuePair_2_t1211005109 *)(&V_2), /*hidden argument*/KeyValuePair_2_get_Key_m182894088_MethodInfo_var);
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_24);
		ObjectU5BU5D_t3614634134* L_25 = (ObjectU5BU5D_t3614634134*)L_23;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral2162321578);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral2162321578);
		ObjectU5BU5D_t3614634134* L_26 = (ObjectU5BU5D_t3614634134*)L_25;
		Il2CppObject * L_27 = KeyValuePair_2_get_Value_m689905909((KeyValuePair_2_t1211005109 *)(&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m689905909_MethodInfo_var);
		String_t* L_28 = ___prefix0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)L_28, (String_t*)_stringLiteral372029349, /*hidden argument*/NULL);
		NullCheck((TreeNode_1_t3829898795 *)((TreeNode_1_t3829898795 *)Castclass(L_27, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		String_t* L_30 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(4 /* System.String Firebase.Database.Internal.Core.Utilities.TreeNode`1<System.Object>::ToStringExpr(System.String) */, (TreeNode_1_t3829898795 *)((TreeNode_1_t3829898795 *)Castclass(L_27, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (String_t*)L_29);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_30);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_30);
		ObjectU5BU5D_t3614634134* L_31 = (ObjectU5BU5D_t3614634134*)L_26;
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, _stringLiteral372029352);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral372029352);
		String_t* L_32 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_31, /*hidden argument*/NULL);
		V_0 = (String_t*)L_32;
	}

IL_00b7:
	{
		Il2CppObject* L_33 = V_1;
		NullCheck((Il2CppObject *)L_33);
		bool L_34 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_33);
		if (L_34)
		{
			goto IL_0061;
		}
	}
	{
		String_t* L_35 = V_0;
		return L_35;
	}
}
// System.Void Firebase.Database.Internal.Utilities.Pair`2<System.Object,System.Object>::.ctor(T,TU)
extern "C"  void Pair_2__ctor_m1137640203_gshared (Pair_2_t1259711689 * __this, Il2CppObject * ___first0, Il2CppObject * ___second1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___first0;
		__this->set__first_0(L_0);
		Il2CppObject * L_1 = ___second1;
		__this->set__second_1(L_1);
		return;
	}
}
// T Firebase.Database.Internal.Utilities.Pair`2<System.Object,System.Object>::GetFirst()
extern "C"  Il2CppObject * Pair_2_GetFirst_m3897815769_gshared (Pair_2_t1259711689 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__first_0();
		return L_0;
	}
}
// TU Firebase.Database.Internal.Utilities.Pair`2<System.Object,System.Object>::GetSecond()
extern "C"  Il2CppObject * Pair_2_GetSecond_m1675024216_gshared (Pair_2_t1259711689 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__second_1();
		return L_0;
	}
}
// System.Boolean Firebase.Database.Internal.Utilities.Pair`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool Pair_2_Equals_m45448119_gshared (Pair_2_t1259711689 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	Pair_2_t1259711689 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	int32_t G_B8_0 = 0;
	int32_t G_B13_0 = 0;
	{
		Il2CppObject * L_0 = ___o0;
		if ((!(((Il2CppObject*)(Pair_2_t1259711689 *)__this) == ((Il2CppObject*)(Il2CppObject *)L_0))))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)1;
	}

IL_0009:
	{
		Il2CppObject * L_1 = ___o0;
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		NullCheck((Il2CppObject *)__this);
		Type_t * L_2 = Object_GetType_m191970594((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_3 = ___o0;
		NullCheck((Il2CppObject *)L_3);
		Type_t * L_4 = Object_GetType_m191970594((Il2CppObject *)L_3, /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_2) == ((Il2CppObject*)(Type_t *)L_4)))
		{
			goto IL_0022;
		}
	}

IL_0020:
	{
		return (bool)0;
	}

IL_0022:
	{
		Il2CppObject * L_5 = ___o0;
		V_0 = (Pair_2_t1259711689 *)((Pair_2_t1259711689 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)));
		Il2CppObject * L_6 = (Il2CppObject *)__this->get__first_0();
		if (!L_6)
		{
			goto IL_0060;
		}
	}
	{
		Il2CppObject * L_7 = (Il2CppObject *)__this->get__first_0();
		V_1 = (Il2CppObject *)L_7;
		Pair_2_t1259711689 * L_8 = V_0;
		NullCheck(L_8);
		Il2CppObject * L_9 = (Il2CppObject *)L_8->get__first_0();
		NullCheck((Il2CppObject *)(*(&V_1)));
		bool L_10 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&V_1)), (Il2CppObject *)L_9);
		G_B8_0 = ((((int32_t)L_10) == ((int32_t)0))? 1 : 0);
		goto IL_0071;
	}

IL_0060:
	{
		Pair_2_t1259711689 * L_11 = V_0;
		NullCheck(L_11);
		Il2CppObject * L_12 = (Il2CppObject *)L_11->get__first_0();
		G_B8_0 = ((((int32_t)((((Il2CppObject*)(Il2CppObject *)L_12) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_0071:
	{
		if (!G_B8_0)
		{
			goto IL_0078;
		}
	}
	{
		return (bool)0;
	}

IL_0078:
	{
		Il2CppObject * L_13 = (Il2CppObject *)__this->get__second_1();
		if (!L_13)
		{
			goto IL_00af;
		}
	}
	{
		Il2CppObject * L_14 = (Il2CppObject *)__this->get__second_1();
		V_2 = (Il2CppObject *)L_14;
		Pair_2_t1259711689 * L_15 = V_0;
		NullCheck(L_15);
		Il2CppObject * L_16 = (Il2CppObject *)L_15->get__second_1();
		NullCheck((Il2CppObject *)(*(&V_2)));
		bool L_17 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&V_2)), (Il2CppObject *)L_16);
		G_B13_0 = ((((int32_t)L_17) == ((int32_t)0))? 1 : 0);
		goto IL_00c0;
	}

IL_00af:
	{
		Pair_2_t1259711689 * L_18 = V_0;
		NullCheck(L_18);
		Il2CppObject * L_19 = (Il2CppObject *)L_18->get__second_1();
		G_B13_0 = ((((int32_t)((((Il2CppObject*)(Il2CppObject *)L_19) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_00c0:
	{
		if (!G_B13_0)
		{
			goto IL_00c7;
		}
	}
	{
		return (bool)0;
	}

IL_00c7:
	{
		return (bool)1;
	}
}
// System.Int32 Firebase.Database.Internal.Utilities.Pair`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t Pair_2_GetHashCode_m2815204653_gshared (Pair_2_t1259711689 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get__first_0();
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject * L_1 = (Il2CppObject *)__this->get__first_0();
		V_1 = (Il2CppObject *)L_1;
		NullCheck((Il2CppObject *)(*(&V_1)));
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)(*(&V_1)));
		G_B3_0 = L_2;
		goto IL_002a;
	}

IL_0029:
	{
		G_B3_0 = 0;
	}

IL_002a:
	{
		V_0 = (int32_t)G_B3_0;
		int32_t L_3 = V_0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get__second_1();
		G_B4_0 = ((int32_t)((int32_t)((int32_t)31)*(int32_t)L_3));
		if (!L_4)
		{
			G_B5_0 = ((int32_t)((int32_t)((int32_t)31)*(int32_t)L_3));
			goto IL_0058;
		}
	}
	{
		Il2CppObject * L_5 = (Il2CppObject *)__this->get__second_1();
		V_2 = (Il2CppObject *)L_5;
		NullCheck((Il2CppObject *)(*(&V_2)));
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)(*(&V_2)));
		G_B6_0 = L_6;
		G_B6_1 = G_B4_0;
		goto IL_0059;
	}

IL_0058:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_0059:
	{
		V_0 = (int32_t)((int32_t)((int32_t)G_B6_1+(int32_t)G_B6_0));
		int32_t L_7 = V_0;
		return L_7;
	}
}
// System.String Firebase.Database.Internal.Utilities.Pair`2<System.Object,System.Object>::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1043849634;
extern Il2CppCodeGenString* _stringLiteral372029314;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t Pair_2_ToString_m2900895833_MetadataUsageId;
extern "C"  String_t* Pair_2_ToString_m2900895833_gshared (Pair_2_t1259711689 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pair_2_ToString_m2900895833_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral1043849634);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1043849634);
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)L_0;
		Il2CppObject * L_2 = (Il2CppObject *)__this->get__first_0();
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_2);
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)L_1;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral372029314);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029314);
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)L_3;
		Il2CppObject * L_5 = (Il2CppObject *)__this->get__second_1();
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_5);
		ObjectU5BU5D_t3614634134* L_6 = (ObjectU5BU5D_t3614634134*)L_4;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral372029317);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void Google.Sharpen.BlockingCollection`1<System.Object>::.ctor()
extern Il2CppClass* Semaphore_t159839144_il2cpp_TypeInfo_var;
extern const uint32_t BlockingCollection_1__ctor_m225173119_MetadataUsageId;
extern "C"  void BlockingCollection_1__ctor_m225173119_gshared (BlockingCollection_1_t3061287978 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlockingCollection_1__ctor_m225173119_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Semaphore_t159839144 * L_0 = (Semaphore_t159839144 *)il2cpp_codegen_object_new(Semaphore_t159839144_il2cpp_TypeInfo_var);
		Semaphore__ctor_m2906554191(L_0, (int32_t)0, (int32_t)((int32_t)2147483647LL), /*hidden argument*/NULL);
		__this->set__count_0(L_0);
		Queue_1_t2509106130 * L_1 = (Queue_1_t2509106130 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Queue_1_t2509106130 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set__underlyingQueue_1(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Google.Sharpen.BlockingCollection`1<System.Object>::get_Count()
extern "C"  int32_t BlockingCollection_1_get_Count_m472184031_gshared (BlockingCollection_1_t3061287978 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Queue_1_t2509106130 * L_0 = (Queue_1_t2509106130 *)__this->get__underlyingQueue_1();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Queue_1_t2509106130 * L_2 = (Queue_1_t2509106130 *)__this->get__underlyingQueue_1();
		NullCheck((Queue_1_t2509106130 *)L_2);
		int32_t L_3 = ((  int32_t (*) (Queue_1_t2509106130 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Queue_1_t2509106130 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_1 = (int32_t)L_3;
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0025:
	{
		int32_t L_5 = V_1;
		return L_5;
	}
}
// System.Void Google.Sharpen.BlockingCollection`1<System.Object>::Dispose()
extern "C"  void BlockingCollection_1_Dispose_m1813179332_gshared (BlockingCollection_1_t3061287978 * __this, const MethodInfo* method)
{
	{
		Semaphore_t159839144 * L_0 = (Semaphore_t159839144 *)__this->get__count_0();
		NullCheck((WaitHandle_t677569169 *)L_0);
		VirtActionInvoker0::Invoke(7 /* System.Void System.Threading.WaitHandle::Close() */, (WaitHandle_t677569169 *)L_0);
		return;
	}
}
// System.Void Google.Sharpen.BlockingCollection`1<System.Object>::Add(T)
extern "C"  void BlockingCollection_1_Add_m2390167760_gshared (BlockingCollection_1_t3061287978 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Queue_1_t2509106130 * L_0 = (Queue_1_t2509106130 *)__this->get__underlyingQueue_1();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Queue_1_t2509106130 * L_2 = (Queue_1_t2509106130 *)__this->get__underlyingQueue_1();
		Il2CppObject * L_3 = ___item0;
		NullCheck((Queue_1_t2509106130 *)L_2);
		((  void (*) (Queue_1_t2509106130 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Queue_1_t2509106130 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0025:
	{
		Semaphore_t159839144 * L_5 = (Semaphore_t159839144 *)__this->get__count_0();
		NullCheck((Semaphore_t159839144 *)L_5);
		Semaphore_Release_m1642555081((Semaphore_t159839144 *)L_5, (int32_t)1, /*hidden argument*/NULL);
		return;
	}
}
// T Google.Sharpen.BlockingCollection`1<System.Object>::Take()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t BlockingCollection_1_Take_m3350786155_MetadataUsageId;
extern "C"  Il2CppObject * BlockingCollection_1_Take_m3350786155_gshared (BlockingCollection_1_t3061287978 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlockingCollection_1_Take_m3350786155_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Semaphore_t159839144 * L_0 = (Semaphore_t159839144 *)__this->get__count_0();
			NullCheck((WaitHandle_t677569169 *)L_0);
			bool L_1 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean System.Threading.WaitHandle::WaitOne() */, (WaitHandle_t677569169 *)L_0);
			if (!L_1)
			{
				goto IL_0035;
			}
		}

IL_0010:
		{
			Queue_1_t2509106130 * L_2 = (Queue_1_t2509106130 *)__this->get__underlyingQueue_1();
			V_0 = (Il2CppObject *)L_2;
			Il2CppObject * L_3 = V_0;
			Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		}

IL_001d:
		try
		{ // begin try (depth: 2)
			Queue_1_t2509106130 * L_4 = (Queue_1_t2509106130 *)__this->get__underlyingQueue_1();
			NullCheck((Queue_1_t2509106130 *)L_4);
			Il2CppObject * L_5 = ((  Il2CppObject * (*) (Queue_1_t2509106130 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Queue_1_t2509106130 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
			V_1 = (Il2CppObject *)L_5;
			IL2CPP_LEAVE(0x4A, FINALLY_002e);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_002e;
		}

FINALLY_002e:
		{ // begin finally (depth: 2)
			Il2CppObject * L_6 = V_0;
			Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(46)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(46)
		{
			IL2CPP_JUMP_TBL(0x4A, IL_004a)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_0035:
		{
			goto IL_0040;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_003a;
		throw e;
	}

CATCH_003a:
	{ // begin catch(System.ObjectDisposedException)
		goto IL_0040;
	} // end catch (depth: 1)

IL_0040:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_7 = V_2;
		return L_7;
	}

IL_004a:
	{
		Il2CppObject * L_8 = V_1;
		return L_8;
	}
}
// System.Collections.Generic.IList`1<T> Google.Sharpen.Collections`1<System.Int64>::get_EMPTY_SET()
extern "C"  Il2CppObject* Collections_1_get_EMPTY_SET_m1324405184_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_0 = ((Collections_1_t957031954_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_empty_0();
		return L_0;
	}
}
// System.Void Google.Sharpen.Collections`1<System.Int64>::.cctor()
extern "C"  void Collections_1__cctor_m3923826052_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((Collections_1_t957031954_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_empty_0((Il2CppObject*)((Int64U5BU5D_t717125112*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IList`1<T> Google.Sharpen.Collections`1<System.Object>::get_EMPTY_SET()
extern "C"  Il2CppObject* Collections_1_get_EMPTY_SET_m4198404202_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject* L_0 = ((Collections_1_t2737403212_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_empty_0();
		return L_0;
	}
}
// System.Void Google.Sharpen.Collections`1<System.Object>::.cctor()
extern "C"  void Collections_1__cctor_m3636824690_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((Collections_1_t2737403212_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_empty_0((Il2CppObject*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (uint32_t)0)));
		return;
	}
}
// System.Void Google.Sharpen.MessageDigest`1<System.Object>::.ctor()
extern "C"  void MessageDigest_1__ctor_m1278820287_gshared (MessageDigest_1_t1512921346 * __this, const MethodInfo* method)
{
	{
		NullCheck((MessageDigest_t1820469897 *)__this);
		MessageDigest__ctor_m4036998384((MessageDigest_t1820469897 *)__this, /*hidden argument*/NULL);
		NullCheck((MessageDigest_1_t1512921346 *)__this);
		((  void (*) (MessageDigest_1_t1512921346 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((MessageDigest_1_t1512921346 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Byte[] Google.Sharpen.MessageDigest`1<System.Object>::Digest()
extern "C"  ByteU5BU5D_t3397334013* MessageDigest_1_Digest_m142710311_gshared (MessageDigest_1_t1512921346 * __this, const MethodInfo* method)
{
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		CryptoStream_t3531341937 * L_0 = (CryptoStream_t3531341937 *)__this->get__stream_1();
		NullCheck((CryptoStream_t3531341937 *)L_0);
		CryptoStream_FlushFinalBlock_m3450379637((CryptoStream_t3531341937 *)L_0, /*hidden argument*/NULL);
		Il2CppObject ** L_1 = (Il2CppObject **)__this->get_address_of__hash_0();
		NullCheck((HashAlgorithm_t2624936259 *)(*L_1));
		ByteU5BU5D_t3397334013* L_2 = VirtFuncInvoker0< ByteU5BU5D_t3397334013* >::Invoke(13 /* System.Byte[] System.Security.Cryptography.HashAlgorithm::get_Hash() */, (HashAlgorithm_t2624936259 *)(*L_1));
		V_0 = (ByteU5BU5D_t3397334013*)L_2;
		NullCheck((MessageDigest_t1820469897 *)__this);
		VirtActionInvoker0::Invoke(5 /* System.Void Google.Sharpen.MessageDigest::Reset() */, (MessageDigest_t1820469897 *)__this);
		ByteU5BU5D_t3397334013* L_3 = V_0;
		return L_3;
	}
}
// System.Void Google.Sharpen.MessageDigest`1<System.Object>::Dispose()
extern "C"  void MessageDigest_1_Dispose_m1405832584_gshared (MessageDigest_1_t1512921346 * __this, const MethodInfo* method)
{
	{
		CryptoStream_t3531341937 * L_0 = (CryptoStream_t3531341937 *)__this->get__stream_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CryptoStream_t3531341937 * L_1 = (CryptoStream_t3531341937 *)__this->get__stream_1();
		NullCheck((Stream_t3255436806 *)L_1);
		Stream_Dispose_m2417657914((Stream_t3255436806 *)L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		__this->set__stream_1((CryptoStream_t3531341937 *)NULL);
		return;
	}
}
// System.Void Google.Sharpen.MessageDigest`1<System.Object>::Init()
extern Il2CppClass* Stream_t3255436806_il2cpp_TypeInfo_var;
extern Il2CppClass* CryptoStream_t3531341937_il2cpp_TypeInfo_var;
extern const uint32_t MessageDigest_1_Init_m3249709105_MetadataUsageId;
extern "C"  void MessageDigest_1_Init_m3249709105_gshared (MessageDigest_1_t1512921346 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MessageDigest_1_Init_m3249709105_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		__this->set__hash_0(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Stream_t3255436806_il2cpp_TypeInfo_var);
		Stream_t3255436806 * L_1 = ((Stream_t3255436806_StaticFields*)Stream_t3255436806_il2cpp_TypeInfo_var->static_fields)->get_Null_1();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get__hash_0();
		CryptoStream_t3531341937 * L_3 = (CryptoStream_t3531341937 *)il2cpp_codegen_object_new(CryptoStream_t3531341937_il2cpp_TypeInfo_var);
		CryptoStream__ctor_m2578098817(L_3, (Stream_t3255436806 *)L_1, (Il2CppObject *)L_2, (int32_t)1, /*hidden argument*/NULL);
		__this->set__stream_1(L_3);
		return;
	}
}
// System.Void Google.Sharpen.MessageDigest`1<System.Object>::Reset()
extern "C"  void MessageDigest_1_Reset_m3892685886_gshared (MessageDigest_1_t1512921346 * __this, const MethodInfo* method)
{
	{
		NullCheck((MessageDigest_1_t1512921346 *)__this);
		((  void (*) (MessageDigest_1_t1512921346 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((MessageDigest_1_t1512921346 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		NullCheck((MessageDigest_1_t1512921346 *)__this);
		((  void (*) (MessageDigest_1_t1512921346 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((MessageDigest_1_t1512921346 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void Google.Sharpen.MessageDigest`1<System.Object>::Update(System.Byte[])
extern "C"  void MessageDigest_1_Update_m2121830137_gshared (MessageDigest_1_t1512921346 * __this, ByteU5BU5D_t3397334013* ___input0, const MethodInfo* method)
{
	{
		CryptoStream_t3531341937 * L_0 = (CryptoStream_t3531341937 *)__this->get__stream_1();
		ByteU5BU5D_t3397334013* L_1 = ___input0;
		ByteU5BU5D_t3397334013* L_2 = ___input0;
		NullCheck(L_2);
		NullCheck((Stream_t3255436806 *)L_0);
		VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(22 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, (Stream_t3255436806 *)L_0, (ByteU5BU5D_t3397334013*)L_1, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))));
		return;
	}
}
// System.Void Google.Sharpen.ScheduledThreadPoolExecutor/Task`1<System.Object>::.ctor()
extern Il2CppClass* ManualResetEvent_t926074657_il2cpp_TypeInfo_var;
extern const uint32_t Task_1__ctor_m1873130930_MetadataUsageId;
extern "C"  void Task_1__ctor_m1873130930_gshared (Task_1_t2765902707 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1__ctor_m1873130930_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ManualResetEvent_t926074657 * L_0 = (ManualResetEvent_t926074657 *)il2cpp_codegen_object_new(ManualResetEvent_t926074657_il2cpp_TypeInfo_var);
		ManualResetEvent__ctor_m3470249043(L_0, (bool)0, /*hidden argument*/NULL);
		__this->set_doneEvent_3(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.DateTime Google.Sharpen.ScheduledThreadPoolExecutor/Task`1<System.Object>::get_DueTime()
extern "C"  DateTime_t693205669  Task_1_get_DueTime_m817386727_gshared (Task_1_t2765902707 * __this, const MethodInfo* method)
{
	{
		DateTime_t693205669  L_0 = (DateTime_t693205669 )__this->get_U3CDueTimeU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void Google.Sharpen.ScheduledThreadPoolExecutor/Task`1<System.Object>::set_DueTime(System.DateTime)
extern "C"  void Task_1_set_DueTime_m745370016_gshared (Task_1_t2765902707 * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	{
		DateTime_t693205669  L_0 = ___value0;
		__this->set_U3CDueTimeU3Ek__BackingField_5(L_0);
		return;
	}
}
// Google.Sharpen.ScheduledThreadPoolExecutor Google.Sharpen.ScheduledThreadPoolExecutor/Task`1<System.Object>::get_Executor()
extern "C"  ScheduledThreadPoolExecutor_t2537379786 * Task_1_get_Executor_m231682525_gshared (Task_1_t2765902707 * __this, const MethodInfo* method)
{
	{
		ScheduledThreadPoolExecutor_t2537379786 * L_0 = (ScheduledThreadPoolExecutor_t2537379786 *)__this->get_U3CExecutorU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void Google.Sharpen.ScheduledThreadPoolExecutor/Task`1<System.Object>::set_Executor(Google.Sharpen.ScheduledThreadPoolExecutor)
extern "C"  void Task_1_set_Executor_m2168903056_gshared (Task_1_t2765902707 * __this, ScheduledThreadPoolExecutor_t2537379786 * ___value0, const MethodInfo* method)
{
	{
		ScheduledThreadPoolExecutor_t2537379786 * L_0 = ___value0;
		__this->set_U3CExecutorU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Object Google.Sharpen.ScheduledThreadPoolExecutor/Task`1<System.Object>::get_Owner()
extern "C"  Il2CppObject * Task_1_get_Owner_m4193204519_gshared (Task_1_t2765902707 * __this, const MethodInfo* method)
{
	{
		NullCheck((Task_1_t2765902707 *)__this);
		ScheduledThreadPoolExecutor_t2537379786 * L_0 = ((  ScheduledThreadPoolExecutor_t2537379786 * (*) (Task_1_t2765902707 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Task_1_t2765902707 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// System.Void Google.Sharpen.ScheduledThreadPoolExecutor/Task`1<System.Object>::Start()
extern "C"  void Task_1_Start_m3221464038_gshared (Task_1_t2765902707 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (Il2CppObject *)__this;
		Il2CppObject * L_0 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			bool L_1 = (bool)__this->get_canceled_1();
			if (!L_1)
			{
				goto IL_0018;
			}
		}

IL_0013:
		{
			IL2CPP_LEAVE(0x31, FINALLY_002a);
		}

IL_0018:
		{
			NullCheck((Task_1_t2765902707 *)__this);
			ScheduledThreadPoolExecutor_t2537379786 * L_2 = ((  ScheduledThreadPoolExecutor_t2537379786 * (*) (Task_1_t2765902707 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Task_1_t2765902707 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
			NullCheck((ThreadPoolExecutor_t376308723 *)L_2);
			ThreadPoolExecutor_InternalExecute_m1581097176((ThreadPoolExecutor_t376308723 *)L_2, (Il2CppObject *)__this, (bool)0, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x31, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(42)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x31, IL_0031)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0031:
	{
		return;
	}
}
// System.Void Google.Sharpen.ScheduledThreadPoolExecutor/Task`1<System.Object>::Run()
extern Il2CppClass* Thread_t1322377586_il2cpp_TypeInfo_var;
extern Il2CppClass* Runnable_t1446984663_il2cpp_TypeInfo_var;
extern const uint32_t Task_1_Run_m4247771159_MetadataUsageId;
extern "C"  void Task_1_Run_m4247771159_gshared (Task_1_t2765902707 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1_Run_m4247771159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (Il2CppObject *)__this;
		Il2CppObject * L_0 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t1322377586_il2cpp_TypeInfo_var);
		Thread_t1322377586 * L_1 = Thread_CurrentThread_m1539933479(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_thread_0(L_1);
		IL2CPP_LEAVE(0x1F, FINALLY_0018);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0018;
	}

FINALLY_0018:
	{ // begin finally (depth: 1)
		Il2CppObject * L_2 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(24)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(24)
	{
		IL2CPP_JUMP_TBL(0x1F, IL_001f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			bool L_3 = (bool)__this->get_canceled_1();
			if (L_3)
			{
				goto IL_0035;
			}
		}

IL_002a:
		{
			Il2CppObject * L_4 = (Il2CppObject *)__this->get_Action_4();
			NullCheck((Il2CppObject *)L_4);
			InterfaceActionInvoker0::Invoke(0 /* System.Void Google.Sharpen.Runnable::Run() */, Runnable_t1446984663_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		}

IL_0035:
		{
			IL2CPP_LEAVE(0x69, FINALLY_003a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		{
			V_1 = (Il2CppObject *)__this;
			Il2CppObject * L_5 = V_1;
			Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		}

IL_0042:
		try
		{ // begin try (depth: 2)
			__this->set_thread_0((Thread_t1322377586 *)NULL);
			ManualResetEvent_t926074657 * L_6 = (ManualResetEvent_t926074657 *)__this->get_doneEvent_3();
			NullCheck((EventWaitHandle_t2091316307 *)L_6);
			EventWaitHandle_Set_m2975776670((EventWaitHandle_t2091316307 *)L_6, /*hidden argument*/NULL);
			__this->set_completed_2((bool)1);
			IL2CPP_LEAVE(0x68, FINALLY_0061);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_0061;
		}

FINALLY_0061:
		{ // begin finally (depth: 2)
			Il2CppObject * L_7 = V_1;
			Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(97)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(97)
		{
			IL2CPP_JUMP_TBL(0x68, IL_0068)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_0068:
		{
			IL2CPP_END_FINALLY(58)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x69, IL_0069)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0069:
	{
		return;
	}
}
// System.Boolean Google.Sharpen.ScheduledThreadPoolExecutor/Task`1<System.Object>::Cancel(System.Boolean)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Task_1_Cancel_m2715282611_MetadataUsageId;
extern "C"  bool Task_1_Cancel_m2715282611_gshared (Task_1_t2765902707 * __this, bool ___mayInterruptIfRunning0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Task_1_Cancel_m2715282611_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (Il2CppObject *)__this;
		Il2CppObject * L_0 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			bool L_1 = (bool)__this->get_canceled_1();
			if (L_1)
			{
				goto IL_001e;
			}
		}

IL_0013:
		{
			bool L_2 = (bool)__this->get_completed_2();
			if (!L_2)
			{
				goto IL_0025;
			}
		}

IL_001e:
		{
			V_1 = (bool)0;
			IL2CPP_LEAVE(0x7D, FINALLY_0076);
		}

IL_0025:
		{
			int32_t L_3 = (int32_t)1;
			V_2 = (bool)L_3;
			__this->set_completed_2((bool)L_3);
			bool L_4 = V_2;
			__this->set_canceled_1(L_4);
			bool L_5 = ___mayInterruptIfRunning0;
			if (!L_5)
			{
				goto IL_0063;
			}
		}

IL_003b:
		{
			Thread_t1322377586 * L_6 = (Thread_t1322377586 *)__this->get_thread_0();
			if (!L_6)
			{
				goto IL_0063;
			}
		}

IL_0046:
		try
		{ // begin try (depth: 2)
			Thread_t1322377586 * L_7 = (Thread_t1322377586 *)__this->get_thread_0();
			NullCheck((Thread_t1322377586 *)L_7);
			Thread_Abort_m2478891017((Thread_t1322377586 *)L_7, /*hidden argument*/NULL);
			goto IL_005c;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1927440687 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0056;
			throw e;
		}

CATCH_0056:
		{ // begin catch(System.Object)
			goto IL_005c;
		} // end catch (depth: 2)

IL_005c:
		{
			__this->set_thread_0((Thread_t1322377586 *)NULL);
		}

IL_0063:
		{
			ManualResetEvent_t926074657 * L_8 = (ManualResetEvent_t926074657 *)__this->get_doneEvent_3();
			NullCheck((EventWaitHandle_t2091316307 *)L_8);
			EventWaitHandle_Set_m2975776670((EventWaitHandle_t2091316307 *)L_8, /*hidden argument*/NULL);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x7D, FINALLY_0076);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0076;
	}

FINALLY_0076:
	{ // begin finally (depth: 1)
		Il2CppObject * L_9 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(118)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(118)
	{
		IL2CPP_JUMP_TBL(0x7D, IL_007d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_007d:
	{
		bool L_10 = V_1;
		return L_10;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime>::.ctor(System.Object,System.IntPtr)
extern "C"  void WriteDelegate_1__ctor_m1631716844_gshared (WriteDelegate_1_t415313529 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime>::Invoke(Pathfinding.Serialization.JsonFx.JsonWriter,T)
extern "C"  void WriteDelegate_1_Invoke_m2787355611_gshared (WriteDelegate_1_t415313529 * __this, JsonWriter_t446744171 * ___writer0, DateTime_t693205669  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		WriteDelegate_1_Invoke_m2787355611((WriteDelegate_1_t415313529 *)__this->get_prev_9(),___writer0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, JsonWriter_t446744171 * ___writer0, DateTime_t693205669  ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___writer0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, JsonWriter_t446744171 * ___writer0, DateTime_t693205669  ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___writer0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, DateTime_t693205669  ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___writer0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime>::BeginInvoke(Pathfinding.Serialization.JsonFx.JsonWriter,T,System.AsyncCallback,System.Object)
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t WriteDelegate_1_BeginInvoke_m1434329302_MetadataUsageId;
extern "C"  Il2CppObject * WriteDelegate_1_BeginInvoke_m1434329302_gshared (WriteDelegate_1_t415313529 * __this, JsonWriter_t446744171 * ___writer0, DateTime_t693205669  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriteDelegate_1_BeginInvoke_m1434329302_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___writer0;
	__d_args[1] = Box(DateTime_t693205669_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime>::EndInvoke(System.IAsyncResult)
extern "C"  void WriteDelegate_1_EndInvoke_m3986240798_gshared (WriteDelegate_1_t415313529 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void WriteDelegate_1__ctor_m1719743498_gshared (WriteDelegate_1_t2411557155 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.Object>::Invoke(Pathfinding.Serialization.JsonFx.JsonWriter,T)
extern "C"  void WriteDelegate_1_Invoke_m593278659_gshared (WriteDelegate_1_t2411557155 * __this, JsonWriter_t446744171 * ___writer0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		WriteDelegate_1_Invoke_m593278659((WriteDelegate_1_t2411557155 *)__this->get_prev_9(),___writer0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, JsonWriter_t446744171 * ___writer0, Il2CppObject * ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___writer0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, JsonWriter_t446744171 * ___writer0, Il2CppObject * ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___writer0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___writer0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.Object>::BeginInvoke(Pathfinding.Serialization.JsonFx.JsonWriter,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * WriteDelegate_1_BeginInvoke_m1563488352_gshared (WriteDelegate_1_t2411557155 * __this, JsonWriter_t446744171 * ___writer0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___writer0;
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void WriteDelegate_1_EndInvoke_m780369088_gshared (WriteDelegate_1_t2411557155 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutHeartbeat>c__Iterator8`1<System.Object>::.ctor()
extern "C"  void U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1__ctor_m4000749742_gshared (U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t2997308713 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PubNubMessaging.Core.CoroutineClass/<CheckTimeoutHeartbeat>c__Iterator8`1<System.Object>::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_MoveNext_m3032790654_MetadataUsageId;
extern "C"  bool U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_MoveNext_m3032790654_gshared (U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t2997308713 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_MoveNext_m3032790654_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_004c;
		}
	}
	{
		goto IL_0064;
	}

IL_0021:
	{
		CoroutineParams_1_t3789821357 * L_2 = (CoroutineParams_1_t3789821357 *)__this->get_cp_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_timeout_1();
		WaitForSeconds_t3839502067 * L_4 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_4, (float)(((float)((float)L_3))), /*hidden argument*/NULL);
		__this->set_U24current_2(L_4);
		bool L_5 = (bool)__this->get_U24disposing_3();
		if (L_5)
		{
			goto IL_0047;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0047:
	{
		goto IL_0066;
	}

IL_004c:
	{
		CoroutineClass_t2476503358 * L_6 = (CoroutineClass_t2476503358 *)__this->get_U24this_1();
		CoroutineParams_1_t3789821357 * L_7 = (CoroutineParams_1_t3789821357 *)__this->get_cp_0();
		NullCheck((CoroutineClass_t2476503358 *)L_6);
		((  void (*) (CoroutineClass_t2476503358 *, CoroutineParams_1_t3789821357 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((CoroutineClass_t2476503358 *)L_6, (CoroutineParams_1_t3789821357 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_U24PC_4((-1));
	}

IL_0064:
	{
		return (bool)0;
	}

IL_0066:
	{
		return (bool)1;
	}
}
// System.Object PubNubMessaging.Core.CoroutineClass/<CheckTimeoutHeartbeat>c__Iterator8`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m13828898_gshared (U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t2997308713 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object PubNubMessaging.Core.CoroutineClass/<CheckTimeoutHeartbeat>c__Iterator8`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_System_Collections_IEnumerator_get_Current_m2831832106_gshared (U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t2997308713 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutHeartbeat>c__Iterator8`1<System.Object>::Dispose()
extern "C"  void U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_Dispose_m2970090467_gshared (U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t2997308713 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutHeartbeat>c__Iterator8`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_Reset_m2035169677_MetadataUsageId;
extern "C"  void U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_Reset_m2035169677_gshared (U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t2997308713 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_Reset_m2035169677_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutNonSub>c__Iterator6`1<System.Object>::.ctor()
extern "C"  void U3CCheckTimeoutNonSubU3Ec__Iterator6_1__ctor_m2525531305_gshared (U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PubNubMessaging.Core.CoroutineClass/<CheckTimeoutNonSub>c__Iterator6`1<System.Object>::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CCheckTimeoutNonSubU3Ec__Iterator6_1_MoveNext_m4271675359_MetadataUsageId;
extern "C"  bool U3CCheckTimeoutNonSubU3Ec__Iterator6_1_MoveNext_m4271675359_gshared (U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckTimeoutNonSubU3Ec__Iterator6_1_MoveNext_m4271675359_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_004c;
		}
	}
	{
		goto IL_0064;
	}

IL_0021:
	{
		CoroutineParams_1_t3789821357 * L_2 = (CoroutineParams_1_t3789821357 *)__this->get_cp_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_timeout_1();
		WaitForSeconds_t3839502067 * L_4 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_4, (float)(((float)((float)L_3))), /*hidden argument*/NULL);
		__this->set_U24current_2(L_4);
		bool L_5 = (bool)__this->get_U24disposing_3();
		if (L_5)
		{
			goto IL_0047;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0047:
	{
		goto IL_0066;
	}

IL_004c:
	{
		CoroutineClass_t2476503358 * L_6 = (CoroutineClass_t2476503358 *)__this->get_U24this_1();
		CoroutineParams_1_t3789821357 * L_7 = (CoroutineParams_1_t3789821357 *)__this->get_cp_0();
		NullCheck((CoroutineClass_t2476503358 *)L_6);
		((  void (*) (CoroutineClass_t2476503358 *, CoroutineParams_1_t3789821357 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((CoroutineClass_t2476503358 *)L_6, (CoroutineParams_1_t3789821357 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_U24PC_4((-1));
	}

IL_0064:
	{
		return (bool)0;
	}

IL_0066:
	{
		return (bool)1;
	}
}
// System.Object PubNubMessaging.Core.CoroutineClass/<CheckTimeoutNonSub>c__Iterator6`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckTimeoutNonSubU3Ec__Iterator6_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3929667235_gshared (U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object PubNubMessaging.Core.CoroutineClass/<CheckTimeoutNonSub>c__Iterator6`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckTimeoutNonSubU3Ec__Iterator6_1_System_Collections_IEnumerator_get_Current_m1300662283_gshared (U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutNonSub>c__Iterator6`1<System.Object>::Dispose()
extern "C"  void U3CCheckTimeoutNonSubU3Ec__Iterator6_1_Dispose_m2291771386_gshared (U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutNonSub>c__Iterator6`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCheckTimeoutNonSubU3Ec__Iterator6_1_Reset_m4282713708_MetadataUsageId;
extern "C"  void U3CCheckTimeoutNonSubU3Ec__Iterator6_1_Reset_m4282713708_gshared (U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t1941009296 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckTimeoutNonSubU3Ec__Iterator6_1_Reset_m4282713708_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutPresenceHeartbeat>c__Iterator7`1<System.Object>::.ctor()
extern "C"  void U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1__ctor_m1342820818_gshared (U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t163451287 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PubNubMessaging.Core.CoroutineClass/<CheckTimeoutPresenceHeartbeat>c__Iterator7`1<System.Object>::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_MoveNext_m90650718_MetadataUsageId;
extern "C"  bool U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_MoveNext_m90650718_gshared (U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t163451287 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_MoveNext_m90650718_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_004c;
		}
	}
	{
		goto IL_0064;
	}

IL_0021:
	{
		CoroutineParams_1_t3789821357 * L_2 = (CoroutineParams_1_t3789821357 *)__this->get_cp_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_timeout_1();
		WaitForSeconds_t3839502067 * L_4 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_4, (float)(((float)((float)L_3))), /*hidden argument*/NULL);
		__this->set_U24current_2(L_4);
		bool L_5 = (bool)__this->get_U24disposing_3();
		if (L_5)
		{
			goto IL_0047;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0047:
	{
		goto IL_0066;
	}

IL_004c:
	{
		CoroutineClass_t2476503358 * L_6 = (CoroutineClass_t2476503358 *)__this->get_U24this_1();
		CoroutineParams_1_t3789821357 * L_7 = (CoroutineParams_1_t3789821357 *)__this->get_cp_0();
		NullCheck((CoroutineClass_t2476503358 *)L_6);
		((  void (*) (CoroutineClass_t2476503358 *, CoroutineParams_1_t3789821357 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((CoroutineClass_t2476503358 *)L_6, (CoroutineParams_1_t3789821357 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_U24PC_4((-1));
	}

IL_0064:
	{
		return (bool)0;
	}

IL_0066:
	{
		return (bool)1;
	}
}
// System.Object PubNubMessaging.Core.CoroutineClass/<CheckTimeoutPresenceHeartbeat>c__Iterator7`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3648101918_gshared (U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t163451287 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object PubNubMessaging.Core.CoroutineClass/<CheckTimeoutPresenceHeartbeat>c__Iterator7`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_System_Collections_IEnumerator_get_Current_m1738581670_gshared (U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t163451287 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutPresenceHeartbeat>c__Iterator7`1<System.Object>::Dispose()
extern "C"  void U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_Dispose_m4258893909_gshared (U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t163451287 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutPresenceHeartbeat>c__Iterator7`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_Reset_m2783278763_MetadataUsageId;
extern "C"  void U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_Reset_m2783278763_gshared (U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t163451287 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_Reset_m2783278763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutSub>c__Iterator5`1<System.Object>::.ctor()
extern "C"  void U3CCheckTimeoutSubU3Ec__Iterator5_1__ctor_m1337258217_gshared (U3CCheckTimeoutSubU3Ec__Iterator5_1_t3644537212 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PubNubMessaging.Core.CoroutineClass/<CheckTimeoutSub>c__Iterator5`1<System.Object>::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CCheckTimeoutSubU3Ec__Iterator5_1_MoveNext_m761937791_MetadataUsageId;
extern "C"  bool U3CCheckTimeoutSubU3Ec__Iterator5_1_MoveNext_m761937791_gshared (U3CCheckTimeoutSubU3Ec__Iterator5_1_t3644537212 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckTimeoutSubU3Ec__Iterator5_1_MoveNext_m761937791_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_004c;
		}
	}
	{
		goto IL_0064;
	}

IL_0021:
	{
		CoroutineParams_1_t3789821357 * L_2 = (CoroutineParams_1_t3789821357 *)__this->get_cp_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_timeout_1();
		WaitForSeconds_t3839502067 * L_4 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_4, (float)(((float)((float)L_3))), /*hidden argument*/NULL);
		__this->set_U24current_2(L_4);
		bool L_5 = (bool)__this->get_U24disposing_3();
		if (L_5)
		{
			goto IL_0047;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0047:
	{
		goto IL_0066;
	}

IL_004c:
	{
		CoroutineClass_t2476503358 * L_6 = (CoroutineClass_t2476503358 *)__this->get_U24this_1();
		CoroutineParams_1_t3789821357 * L_7 = (CoroutineParams_1_t3789821357 *)__this->get_cp_0();
		NullCheck((CoroutineClass_t2476503358 *)L_6);
		((  void (*) (CoroutineClass_t2476503358 *, CoroutineParams_1_t3789821357 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((CoroutineClass_t2476503358 *)L_6, (CoroutineParams_1_t3789821357 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_U24PC_4((-1));
	}

IL_0064:
	{
		return (bool)0;
	}

IL_0066:
	{
		return (bool)1;
	}
}
// System.Object PubNubMessaging.Core.CoroutineClass/<CheckTimeoutSub>c__Iterator5`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCheckTimeoutSubU3Ec__Iterator5_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2123449087_gshared (U3CCheckTimeoutSubU3Ec__Iterator5_1_t3644537212 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object PubNubMessaging.Core.CoroutineClass/<CheckTimeoutSub>c__Iterator5`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCheckTimeoutSubU3Ec__Iterator5_1_System_Collections_IEnumerator_get_Current_m1780862263_gshared (U3CCheckTimeoutSubU3Ec__Iterator5_1_t3644537212 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutSub>c__Iterator5`1<System.Object>::Dispose()
extern "C"  void U3CCheckTimeoutSubU3Ec__Iterator5_1_Dispose_m2444302366_gshared (U3CCheckTimeoutSubU3Ec__Iterator5_1_t3644537212 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<CheckTimeoutSub>c__Iterator5`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCheckTimeoutSubU3Ec__Iterator5_1_Reset_m2412827112_MetadataUsageId;
extern "C"  void U3CCheckTimeoutSubU3Ec__Iterator5_1_Reset_m2412827112_gshared (U3CCheckTimeoutSubU3Ec__Iterator5_1_t3644537212 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCheckTimeoutSubU3Ec__Iterator5_1_Reset_m2412827112_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1<System.Object>::.ctor()
extern "C"  void U3CDelayRequestU3Ec__Iterator0_1__ctor_m3187285153_gshared (U3CDelayRequestU3Ec__Iterator0_1_t1605486090 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1<System.Object>::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CDelayRequestU3Ec__Iterator0_1_MoveNext_m1180190031_MetadataUsageId;
extern "C"  bool U3CDelayRequestU3Ec__Iterator0_1_MoveNext_m1180190031_gshared (U3CDelayRequestU3Ec__Iterator0_1_t1605486090 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDelayRequestU3Ec__Iterator0_1_MoveNext_m1180190031_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_8();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_8((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0047;
		}
	}
	{
		goto IL_0077;
	}

IL_0021:
	{
		int32_t L_2 = (int32_t)__this->get_pause_0();
		WaitForSeconds_t3839502067 * L_3 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_3, (float)(((float)((float)L_2))), /*hidden argument*/NULL);
		__this->set_U24current_6(L_3);
		bool L_4 = (bool)__this->get_U24disposing_7();
		if (L_4)
		{
			goto IL_0042;
		}
	}
	{
		__this->set_U24PC_8(1);
	}

IL_0042:
	{
		goto IL_0079;
	}

IL_0047:
	{
		CoroutineClass_t2476503358 * L_5 = (CoroutineClass_t2476503358 *)__this->get_U24this_5();
		String_t* L_6 = (String_t*)__this->get_url_1();
		RequestState_1_t8940997 * L_7 = (RequestState_1_t8940997 *)__this->get_pubnubRequestState_2();
		int32_t L_8 = (int32_t)__this->get_timeout_3();
		int32_t L_9 = (int32_t)__this->get_pause_0();
		int32_t L_10 = (int32_t)__this->get_crt_4();
		NullCheck((CoroutineClass_t2476503358 *)L_5);
		((  void (*) (CoroutineClass_t2476503358 *, String_t*, RequestState_1_t8940997 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((CoroutineClass_t2476503358 *)L_5, (String_t*)L_6, (RequestState_1_t8940997 *)L_7, (int32_t)L_8, (int32_t)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_U24PC_8((-1));
	}

IL_0077:
	{
		return (bool)0;
	}

IL_0079:
	{
		return (bool)1;
	}
}
// System.Object PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayRequestU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m442173987_gshared (U3CDelayRequestU3Ec__Iterator0_1_t1605486090 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_6();
		return L_0;
	}
}
// System.Object PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayRequestU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m3215447499_gshared (U3CDelayRequestU3Ec__Iterator0_1_t1605486090 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_6();
		return L_0;
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1<System.Object>::Dispose()
extern "C"  void U3CDelayRequestU3Ec__Iterator0_1_Dispose_m640606148_gshared (U3CDelayRequestU3Ec__Iterator0_1_t1605486090 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_7((bool)1);
		__this->set_U24PC_8((-1));
		return;
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<DelayRequest>c__Iterator0`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CDelayRequestU3Ec__Iterator0_1_Reset_m3747843318_MetadataUsageId;
extern "C"  void U3CDelayRequestU3Ec__Iterator0_1_Reset_m3747843318_gshared (U3CDelayRequestU3Ec__Iterator0_1_t1605486090 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDelayRequestU3Ec__Iterator0_1_Reset_m3747843318_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestHeartbeat>c__Iterator4`1<System.Object>::.ctor()
extern "C"  void U3CSendRequestHeartbeatU3Ec__Iterator4_1__ctor_m330375226_gshared (U3CSendRequestHeartbeatU3Ec__Iterator4_1_t1669783977 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PubNubMessaging.Core.CoroutineClass/<SendRequestHeartbeat>c__Iterator4`1<System.Object>::MoveNext()
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern const uint32_t U3CSendRequestHeartbeatU3Ec__Iterator4_1_MoveNext_m2485563690_MetadataUsageId;
extern "C"  bool U3CSendRequestHeartbeatU3Ec__Iterator4_1_MoveNext_m2485563690_gshared (U3CSendRequestHeartbeatU3Ec__Iterator4_1_t1669783977 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSendRequestHeartbeatU3Ec__Iterator4_1_MoveNext_m2485563690_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_006d;
		}
	}
	{
		goto IL_00cd;
	}

IL_0021:
	{
		CoroutineClass_t2476503358 * L_2 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		NullCheck(L_2);
		L_2->set_isHearbeatComplete_19((bool)0);
		CoroutineClass_t2476503358 * L_3 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		CoroutineParams_1_t3789821357 * L_4 = (CoroutineParams_1_t3789821357 *)__this->get_cp_0();
		NullCheck(L_4);
		String_t* L_5 = (String_t*)L_4->get_url_0();
		WWW_t2919945039 * L_6 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_6, (String_t*)L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_heartbeatWww_35(L_6);
		CoroutineClass_t2476503358 * L_7 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		NullCheck(L_7);
		WWW_t2919945039 * L_8 = (WWW_t2919945039 *)L_7->get_heartbeatWww_35();
		__this->set_U24current_3(L_8);
		bool L_9 = (bool)__this->get_U24disposing_4();
		if (L_9)
		{
			goto IL_0068;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_0068:
	{
		goto IL_00cf;
	}

IL_006d:
	{
		CoroutineClass_t2476503358 * L_10 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		NullCheck(L_10);
		WWW_t2919945039 * L_11 = (WWW_t2919945039 *)L_10->get_heartbeatWww_35();
		if (!L_11)
		{
			goto IL_00a8;
		}
	}
	{
		CoroutineClass_t2476503358 * L_12 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		NullCheck(L_12);
		WWW_t2919945039 * L_13 = (WWW_t2919945039 *)L_12->get_heartbeatWww_35();
		NullCheck((WWW_t2919945039 *)L_13);
		bool L_14 = WWW_get_isDone_m3240254121((WWW_t2919945039 *)L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00a8;
		}
	}
	{
		CoroutineClass_t2476503358 * L_15 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		NullCheck(L_15);
		WWW_t2919945039 * L_16 = (WWW_t2919945039 *)L_15->get_heartbeatWww_35();
		__this->set_U3CwwwU3E__0_1(L_16);
		goto IL_00af;
	}

IL_00a8:
	{
		__this->set_U3CwwwU3E__0_1((WWW_t2919945039 *)NULL);
	}

IL_00af:
	{
		CoroutineClass_t2476503358 * L_17 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		WWW_t2919945039 * L_18 = (WWW_t2919945039 *)__this->get_U3CwwwU3E__0_1();
		CoroutineParams_1_t3789821357 * L_19 = (CoroutineParams_1_t3789821357 *)__this->get_cp_0();
		NullCheck((CoroutineClass_t2476503358 *)L_17);
		((  void (*) (CoroutineClass_t2476503358 *, WWW_t2919945039 *, CoroutineParams_1_t3789821357 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((CoroutineClass_t2476503358 *)L_17, (WWW_t2919945039 *)L_18, (CoroutineParams_1_t3789821357 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_U24PC_5((-1));
	}

IL_00cd:
	{
		return (bool)0;
	}

IL_00cf:
	{
		return (bool)1;
	}
}
// System.Object PubNubMessaging.Core.CoroutineClass/<SendRequestHeartbeat>c__Iterator4`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendRequestHeartbeatU3Ec__Iterator4_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2749007252_gshared (U3CSendRequestHeartbeatU3Ec__Iterator4_1_t1669783977 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		return L_0;
	}
}
// System.Object PubNubMessaging.Core.CoroutineClass/<SendRequestHeartbeat>c__Iterator4`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendRequestHeartbeatU3Ec__Iterator4_1_System_Collections_IEnumerator_get_Current_m2404150444_gshared (U3CSendRequestHeartbeatU3Ec__Iterator4_1_t1669783977 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		return L_0;
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestHeartbeat>c__Iterator4`1<System.Object>::Dispose()
extern "C"  void U3CSendRequestHeartbeatU3Ec__Iterator4_1_Dispose_m3301539467_gshared (U3CSendRequestHeartbeatU3Ec__Iterator4_1_t1669783977 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestHeartbeat>c__Iterator4`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CSendRequestHeartbeatU3Ec__Iterator4_1_Reset_m1908191165_MetadataUsageId;
extern "C"  void U3CSendRequestHeartbeatU3Ec__Iterator4_1_Reset_m1908191165_gshared (U3CSendRequestHeartbeatU3Ec__Iterator4_1_t1669783977 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSendRequestHeartbeatU3Ec__Iterator4_1_Reset_m1908191165_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1<System.Object>::.ctor()
extern "C"  void U3CSendRequestNonSubU3Ec__Iterator2_1__ctor_m4150949169_gshared (U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1<System.Object>::MoveNext()
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern const uint32_t U3CSendRequestNonSubU3Ec__Iterator2_1_MoveNext_m4027539895_MetadataUsageId;
extern "C"  bool U3CSendRequestNonSubU3Ec__Iterator2_1_MoveNext_m4027539895_gshared (U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSendRequestNonSubU3Ec__Iterator2_1_MoveNext_m4027539895_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_006d;
		}
	}
	{
		goto IL_00cd;
	}

IL_0021:
	{
		CoroutineClass_t2476503358 * L_2 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		NullCheck(L_2);
		L_2->set_isNonSubscribeComplete_22((bool)0);
		CoroutineClass_t2476503358 * L_3 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		CoroutineParams_1_t3789821357 * L_4 = (CoroutineParams_1_t3789821357 *)__this->get_cp_0();
		NullCheck(L_4);
		String_t* L_5 = (String_t*)L_4->get_url_0();
		WWW_t2919945039 * L_6 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_6, (String_t*)L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_nonSubscribeWww_37(L_6);
		CoroutineClass_t2476503358 * L_7 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		NullCheck(L_7);
		WWW_t2919945039 * L_8 = (WWW_t2919945039 *)L_7->get_nonSubscribeWww_37();
		__this->set_U24current_3(L_8);
		bool L_9 = (bool)__this->get_U24disposing_4();
		if (L_9)
		{
			goto IL_0068;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_0068:
	{
		goto IL_00cf;
	}

IL_006d:
	{
		CoroutineClass_t2476503358 * L_10 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		NullCheck(L_10);
		WWW_t2919945039 * L_11 = (WWW_t2919945039 *)L_10->get_nonSubscribeWww_37();
		if (!L_11)
		{
			goto IL_00a8;
		}
	}
	{
		CoroutineClass_t2476503358 * L_12 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		NullCheck(L_12);
		WWW_t2919945039 * L_13 = (WWW_t2919945039 *)L_12->get_nonSubscribeWww_37();
		NullCheck((WWW_t2919945039 *)L_13);
		bool L_14 = WWW_get_isDone_m3240254121((WWW_t2919945039 *)L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00a8;
		}
	}
	{
		CoroutineClass_t2476503358 * L_15 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		NullCheck(L_15);
		WWW_t2919945039 * L_16 = (WWW_t2919945039 *)L_15->get_nonSubscribeWww_37();
		__this->set_U3CwwwU3E__0_1(L_16);
		goto IL_00af;
	}

IL_00a8:
	{
		__this->set_U3CwwwU3E__0_1((WWW_t2919945039 *)NULL);
	}

IL_00af:
	{
		CoroutineClass_t2476503358 * L_17 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		WWW_t2919945039 * L_18 = (WWW_t2919945039 *)__this->get_U3CwwwU3E__0_1();
		CoroutineParams_1_t3789821357 * L_19 = (CoroutineParams_1_t3789821357 *)__this->get_cp_0();
		NullCheck((CoroutineClass_t2476503358 *)L_17);
		((  void (*) (CoroutineClass_t2476503358 *, WWW_t2919945039 *, CoroutineParams_1_t3789821357 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((CoroutineClass_t2476503358 *)L_17, (WWW_t2919945039 *)L_18, (CoroutineParams_1_t3789821357 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_U24PC_5((-1));
	}

IL_00cd:
	{
		return (bool)0;
	}

IL_00cf:
	{
		return (bool)1;
	}
}
// System.Object PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendRequestNonSubU3Ec__Iterator2_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1805857471_gshared (U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		return L_0;
	}
}
// System.Object PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendRequestNonSubU3Ec__Iterator2_1_System_Collections_IEnumerator_get_Current_m3649035015_gshared (U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		return L_0;
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1<System.Object>::Dispose()
extern "C"  void U3CSendRequestNonSubU3Ec__Iterator2_1_Dispose_m3207820422_gshared (U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestNonSub>c__Iterator2`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CSendRequestNonSubU3Ec__Iterator2_1_Reset_m1576087936_MetadataUsageId;
extern "C"  void U3CSendRequestNonSubU3Ec__Iterator2_1_Reset_m1576087936_gshared (U3CSendRequestNonSubU3Ec__Iterator2_1_t4127451748 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSendRequestNonSubU3Ec__Iterator2_1_Reset_m1576087936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestPresenceHeartbeat>c__Iterator3`1<System.Object>::.ctor()
extern "C"  void U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1__ctor_m2415533718_gshared (U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1006883627 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PubNubMessaging.Core.CoroutineClass/<SendRequestPresenceHeartbeat>c__Iterator3`1<System.Object>::MoveNext()
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern const uint32_t U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_MoveNext_m3875715858_MetadataUsageId;
extern "C"  bool U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_MoveNext_m3875715858_gshared (U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1006883627 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_MoveNext_m3875715858_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_006d;
		}
	}
	{
		goto IL_00cd;
	}

IL_0021:
	{
		CoroutineClass_t2476503358 * L_2 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		NullCheck(L_2);
		L_2->set_isPresenceHeartbeatComplete_20((bool)0);
		CoroutineClass_t2476503358 * L_3 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		CoroutineParams_1_t3789821357 * L_4 = (CoroutineParams_1_t3789821357 *)__this->get_cp_0();
		NullCheck(L_4);
		String_t* L_5 = (String_t*)L_4->get_url_0();
		WWW_t2919945039 * L_6 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_6, (String_t*)L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_presenceHeartbeatWww_36(L_6);
		CoroutineClass_t2476503358 * L_7 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		NullCheck(L_7);
		WWW_t2919945039 * L_8 = (WWW_t2919945039 *)L_7->get_presenceHeartbeatWww_36();
		__this->set_U24current_3(L_8);
		bool L_9 = (bool)__this->get_U24disposing_4();
		if (L_9)
		{
			goto IL_0068;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_0068:
	{
		goto IL_00cf;
	}

IL_006d:
	{
		CoroutineClass_t2476503358 * L_10 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		NullCheck(L_10);
		WWW_t2919945039 * L_11 = (WWW_t2919945039 *)L_10->get_presenceHeartbeatWww_36();
		if (!L_11)
		{
			goto IL_00a8;
		}
	}
	{
		CoroutineClass_t2476503358 * L_12 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		NullCheck(L_12);
		WWW_t2919945039 * L_13 = (WWW_t2919945039 *)L_12->get_presenceHeartbeatWww_36();
		NullCheck((WWW_t2919945039 *)L_13);
		bool L_14 = WWW_get_isDone_m3240254121((WWW_t2919945039 *)L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00a8;
		}
	}
	{
		CoroutineClass_t2476503358 * L_15 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		NullCheck(L_15);
		WWW_t2919945039 * L_16 = (WWW_t2919945039 *)L_15->get_presenceHeartbeatWww_36();
		__this->set_U3CwwwU3E__0_1(L_16);
		goto IL_00af;
	}

IL_00a8:
	{
		__this->set_U3CwwwU3E__0_1((WWW_t2919945039 *)NULL);
	}

IL_00af:
	{
		CoroutineClass_t2476503358 * L_17 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		WWW_t2919945039 * L_18 = (WWW_t2919945039 *)__this->get_U3CwwwU3E__0_1();
		CoroutineParams_1_t3789821357 * L_19 = (CoroutineParams_1_t3789821357 *)__this->get_cp_0();
		NullCheck((CoroutineClass_t2476503358 *)L_17);
		((  void (*) (CoroutineClass_t2476503358 *, WWW_t2919945039 *, CoroutineParams_1_t3789821357 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((CoroutineClass_t2476503358 *)L_17, (WWW_t2919945039 *)L_18, (CoroutineParams_1_t3789821357 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_U24PC_5((-1));
	}

IL_00cd:
	{
		return (bool)0;
	}

IL_00cf:
	{
		return (bool)1;
	}
}
// System.Object PubNubMessaging.Core.CoroutineClass/<SendRequestPresenceHeartbeat>c__Iterator3`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2999441612_gshared (U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1006883627 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		return L_0;
	}
}
// System.Object PubNubMessaging.Core.CoroutineClass/<SendRequestPresenceHeartbeat>c__Iterator3`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_System_Collections_IEnumerator_get_Current_m2677775284_gshared (U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1006883627 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		return L_0;
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestPresenceHeartbeat>c__Iterator3`1<System.Object>::Dispose()
extern "C"  void U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_Dispose_m4134861085_gshared (U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1006883627 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestPresenceHeartbeat>c__Iterator3`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_Reset_m1260784827_MetadataUsageId;
extern "C"  void U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_Reset_m1260784827_gshared (U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1006883627 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_Reset_m1260784827_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestSub>c__Iterator1`1<System.Object>::.ctor()
extern "C"  void U3CSendRequestSubU3Ec__Iterator1_1__ctor_m3574381273_gshared (U3CSendRequestSubU3Ec__Iterator1_1_t4220044048 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PubNubMessaging.Core.CoroutineClass/<SendRequestSub>c__Iterator1`1<System.Object>::MoveNext()
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern const uint32_t U3CSendRequestSubU3Ec__Iterator1_1_MoveNext_m1609050287_MetadataUsageId;
extern "C"  bool U3CSendRequestSubU3Ec__Iterator1_1_MoveNext_m1609050287_gshared (U3CSendRequestSubU3Ec__Iterator1_1_t4220044048 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSendRequestSubU3Ec__Iterator1_1_MoveNext_m1609050287_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_006d;
		}
	}
	{
		goto IL_00cd;
	}

IL_0021:
	{
		CoroutineClass_t2476503358 * L_2 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		NullCheck(L_2);
		L_2->set_isSubscribeComplete_21((bool)0);
		CoroutineClass_t2476503358 * L_3 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		CoroutineParams_1_t3789821357 * L_4 = (CoroutineParams_1_t3789821357 *)__this->get_cp_0();
		NullCheck(L_4);
		String_t* L_5 = (String_t*)L_4->get_url_0();
		WWW_t2919945039 * L_6 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_6, (String_t*)L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_subscribeWww_34(L_6);
		CoroutineClass_t2476503358 * L_7 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		NullCheck(L_7);
		WWW_t2919945039 * L_8 = (WWW_t2919945039 *)L_7->get_subscribeWww_34();
		__this->set_U24current_3(L_8);
		bool L_9 = (bool)__this->get_U24disposing_4();
		if (L_9)
		{
			goto IL_0068;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_0068:
	{
		goto IL_00cf;
	}

IL_006d:
	{
		CoroutineClass_t2476503358 * L_10 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		NullCheck(L_10);
		WWW_t2919945039 * L_11 = (WWW_t2919945039 *)L_10->get_subscribeWww_34();
		if (!L_11)
		{
			goto IL_00a8;
		}
	}
	{
		CoroutineClass_t2476503358 * L_12 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		NullCheck(L_12);
		WWW_t2919945039 * L_13 = (WWW_t2919945039 *)L_12->get_subscribeWww_34();
		NullCheck((WWW_t2919945039 *)L_13);
		bool L_14 = WWW_get_isDone_m3240254121((WWW_t2919945039 *)L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00a8;
		}
	}
	{
		CoroutineClass_t2476503358 * L_15 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		NullCheck(L_15);
		WWW_t2919945039 * L_16 = (WWW_t2919945039 *)L_15->get_subscribeWww_34();
		__this->set_U3CwwwU3E__0_1(L_16);
		goto IL_00af;
	}

IL_00a8:
	{
		__this->set_U3CwwwU3E__0_1((WWW_t2919945039 *)NULL);
	}

IL_00af:
	{
		CoroutineClass_t2476503358 * L_17 = (CoroutineClass_t2476503358 *)__this->get_U24this_2();
		WWW_t2919945039 * L_18 = (WWW_t2919945039 *)__this->get_U3CwwwU3E__0_1();
		CoroutineParams_1_t3789821357 * L_19 = (CoroutineParams_1_t3789821357 *)__this->get_cp_0();
		NullCheck((CoroutineClass_t2476503358 *)L_17);
		((  void (*) (CoroutineClass_t2476503358 *, WWW_t2919945039 *, CoroutineParams_1_t3789821357 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((CoroutineClass_t2476503358 *)L_17, (WWW_t2919945039 *)L_18, (CoroutineParams_1_t3789821357 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_U24PC_5((-1));
	}

IL_00cd:
	{
		return (bool)0;
	}

IL_00cf:
	{
		return (bool)1;
	}
}
// System.Object PubNubMessaging.Core.CoroutineClass/<SendRequestSub>c__Iterator1`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendRequestSubU3Ec__Iterator1_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2997727339_gshared (U3CSendRequestSubU3Ec__Iterator1_1_t4220044048 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		return L_0;
	}
}
// System.Object PubNubMessaging.Core.CoroutineClass/<SendRequestSub>c__Iterator1`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendRequestSubU3Ec__Iterator1_1_System_Collections_IEnumerator_get_Current_m1369846019_gshared (U3CSendRequestSubU3Ec__Iterator1_1_t4220044048 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		return L_0;
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestSub>c__Iterator1`1<System.Object>::Dispose()
extern "C"  void U3CSendRequestSubU3Ec__Iterator1_1_Dispose_m1923455418_gshared (U3CSendRequestSubU3Ec__Iterator1_1_t4220044048 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void PubNubMessaging.Core.CoroutineClass/<SendRequestSub>c__Iterator1`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CSendRequestSubU3Ec__Iterator1_1_Reset_m2352479036_MetadataUsageId;
extern "C"  void U3CSendRequestSubU3Ec__Iterator1_1_Reset_m2352479036_gshared (U3CSendRequestSubU3Ec__Iterator1_1_t4220044048 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSendRequestSubU3Ec__Iterator1_1_Reset_m2352479036_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PubNubMessaging.Core.CoroutineParams`1<System.Object>::.ctor(System.String,System.Int32,System.Int32,PubNubMessaging.Core.CurrentRequestType,System.Type,PubNubMessaging.Core.RequestState`1<T>)
extern "C"  void CoroutineParams_1__ctor_m1002892099_gshared (CoroutineParams_1_t3789821357 * __this, String_t* ___url0, int32_t ___timeout1, int32_t ___pause2, int32_t ___crt3, Type_t * ___typeParameterType4, RequestState_1_t8940997 * ___requestState5, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		__this->set_url_0(L_0);
		int32_t L_1 = ___timeout1;
		__this->set_timeout_1(L_1);
		int32_t L_2 = ___pause2;
		__this->set_pause_2(L_2);
		int32_t L_3 = ___crt3;
		__this->set_crt_3(L_3);
		Type_t * L_4 = ___typeParameterType4;
		__this->set_typeParameterType_4(L_4);
		RequestState_1_t8940997 * L_5 = ___requestState5;
		__this->set_requestState_5(L_5);
		return;
	}
}
// System.Void PubNubMessaging.Core.CustomEventArgs`1<System.Object>::.ctor()
extern Il2CppClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern const uint32_t CustomEventArgs_1__ctor_m635681693_MetadataUsageId;
extern "C"  void CustomEventArgs_1__ctor_m635681693_gshared (CustomEventArgs_1_t1034156509 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CustomEventArgs_1__ctor_m635681693_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((EventArgs_t3289624707 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs__ctor_m3696060910((EventArgs_t3289624707 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PubNubMessaging.Core.Helpers/<FindChannelEntityAndCallback>c__AnonStorey1`1<System.Object>::.ctor()
extern "C"  void U3CFindChannelEntityAndCallbackU3Ec__AnonStorey1_1__ctor_m1813777470_gshared (U3CFindChannelEntityAndCallbackU3Ec__AnonStorey1_1_t242888489 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PubNubMessaging.Core.Helpers/<FindChannelEntityAndCallback>c__AnonStorey1`1<System.Object>::<>m__0(PubNubMessaging.Core.ChannelEntity)
extern Il2CppClass* ChannelIdentity_t1147162267_il2cpp_TypeInfo_var;
extern const uint32_t U3CFindChannelEntityAndCallbackU3Ec__AnonStorey1_1_U3CU3Em__0_m594612182_MetadataUsageId;
extern "C"  bool U3CFindChannelEntityAndCallbackU3Ec__AnonStorey1_1_U3CU3Em__0_m594612182_gshared (U3CFindChannelEntityAndCallbackU3Ec__AnonStorey1_1_t242888489 * __this, ChannelEntity_t3266154606 * ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFindChannelEntityAndCallbackU3Ec__AnonStorey1_1_U3CU3Em__0_m594612182_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ChannelEntity_t3266154606 * L_0 = ___x0;
		NullCheck(L_0);
		ChannelIdentity_t1147162267 * L_1 = (ChannelIdentity_t1147162267 *)L_0->get_address_of_ChannelID_0();
		ChannelIdentity_t1147162267  L_2 = (ChannelIdentity_t1147162267 )__this->get_ci_0();
		ChannelIdentity_t1147162267  L_3 = L_2;
		Il2CppObject * L_4 = Box(ChannelIdentity_t1147162267_il2cpp_TypeInfo_var, &L_3);
		Il2CppObject * L_5 = Box(ChannelIdentity_t1147162267_il2cpp_TypeInfo_var, L_1);
		NullCheck((Il2CppObject *)L_5);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_5, (Il2CppObject *)L_4);
		return L_6;
	}
}
// System.Void PubNubMessaging.Core.InternetState`1<System.Object>::.ctor()
extern "C"  void InternetState_1__ctor_m974510633_gshared (InternetState_1_t2062608855 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_Callback_0((Action_1_t3627374100 *)NULL);
		__this->set_ErrorCallback_1((Action_1_t4207084599 *)NULL);
		__this->set_Channels_2((StringU5BU5D_t1642385972*)NULL);
		return;
	}
}
// System.Void PubNubMessaging.Core.MultiplexExceptionEventArgs`1<System.Object>::.ctor()
extern Il2CppClass* EventArgs_t3289624707_il2cpp_TypeInfo_var;
extern const uint32_t MultiplexExceptionEventArgs_1__ctor_m506391253_MetadataUsageId;
extern "C"  void MultiplexExceptionEventArgs_1__ctor_m506391253_gshared (MultiplexExceptionEventArgs_1_t2662502103 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MultiplexExceptionEventArgs_1__ctor_m506391253_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((EventArgs_t3289624707 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t3289624707_il2cpp_TypeInfo_var);
		EventArgs__ctor_m3696060910((EventArgs_t3289624707 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PubNubMessaging.Core.PubnubChannelCallback`1<System.Object>::.ctor()
extern "C"  void PubnubChannelCallback_1__ctor_m3406287877_gshared (PubnubChannelCallback_1_t3499039769 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_SuccessCallback_0((Action_1_t2491248677 *)NULL);
		__this->set_ConnectCallback_3((Action_1_t2491248677 *)NULL);
		__this->set_DisconnectCallback_4((Action_1_t2491248677 *)NULL);
		__this->set_ErrorCallback_1((Action_1_t4207084599 *)NULL);
		__this->set_WildcardPresenceCallback_5((Action_1_t2491248677 *)NULL);
		__this->set_MessageCallback_2((Action_1_t557919624 *)NULL);
		return;
	}
}
// System.Void PubNubMessaging.Core.ReconnectState`1<System.Object>::.ctor()
extern "C"  void ReconnectState_1__ctor_m4216862355_gshared (ReconnectState_1_t3075722957 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_Channels_0((StringU5BU5D_t1642385972*)NULL);
		__this->set_Callback_2((Action_1_t2491248677 *)NULL);
		__this->set_ConnectCallback_4((Action_1_t2491248677 *)NULL);
		__this->set_Timetoken_5(NULL);
		__this->set_Reconnect_6((bool)0);
		return;
	}
}
// System.Void PubNubMessaging.Core.RequestState`1<System.Object>::.ctor()
extern "C"  void RequestState_1__ctor_m3717890875_gshared (RequestState_1_t8940997 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		__this->set_SuccessCallback_0((Action_1_t2491248677 *)NULL);
		__this->set_Request_2((PubnubWebRequest_t3863823607 *)NULL);
		__this->set_Response_3((PubnubWebResponse_t647984363 *)NULL);
		__this->set_ChannelEntities_5((List_1_t2635275738 *)NULL);
		__this->set_ID_10((((int64_t)((int64_t)0))));
		return;
	}
}
// System.Void PubNubMessaging.Core.RequestState`1<System.Object>::.ctor(PubNubMessaging.Core.RequestState`1<T>)
extern "C"  void RequestState_1__ctor_m4012044253_gshared (RequestState_1_t8940997 * __this, RequestState_1_t8940997 * ___requestState0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		RequestState_1_t8940997 * L_0 = ___requestState0;
		NullCheck(L_0);
		Action_1_t4207084599 * L_1 = (Action_1_t4207084599 *)L_0->get_ErrorCallback_1();
		__this->set_ErrorCallback_1(L_1);
		RequestState_1_t8940997 * L_2 = ___requestState0;
		NullCheck(L_2);
		List_1_t2635275738 * L_3 = (List_1_t2635275738 *)L_2->get_ChannelEntities_5();
		__this->set_ChannelEntities_5(L_3);
		RequestState_1_t8940997 * L_4 = ___requestState0;
		NullCheck(L_4);
		bool L_5 = (bool)L_4->get_Reconnect_7();
		__this->set_Reconnect_7(L_5);
		RequestState_1_t8940997 * L_6 = ___requestState0;
		NullCheck(L_6);
		PubnubWebRequest_t3863823607 * L_7 = (PubnubWebRequest_t3863823607 *)L_6->get_Request_2();
		__this->set_Request_2(L_7);
		RequestState_1_t8940997 * L_8 = ___requestState0;
		NullCheck(L_8);
		PubnubWebResponse_t647984363 * L_9 = (PubnubWebResponse_t647984363 *)L_8->get_Response_3();
		__this->set_Response_3(L_9);
		RequestState_1_t8940997 * L_10 = ___requestState0;
		NullCheck(L_10);
		bool L_11 = (bool)L_10->get_Timeout_6();
		__this->set_Timeout_6(L_11);
		RequestState_1_t8940997 * L_12 = ___requestState0;
		NullCheck(L_12);
		int64_t L_13 = (int64_t)L_12->get_Timetoken_8();
		__this->set_Timetoken_8(L_13);
		RequestState_1_t8940997 * L_14 = ___requestState0;
		NullCheck(L_14);
		Type_t * L_15 = (Type_t *)L_14->get_TypeParameterType_9();
		__this->set_TypeParameterType_9(L_15);
		RequestState_1_t8940997 * L_16 = ___requestState0;
		NullCheck(L_16);
		Action_1_t2491248677 * L_17 = (Action_1_t2491248677 *)L_16->get_SuccessCallback_0();
		__this->set_SuccessCallback_0(L_17);
		RequestState_1_t8940997 * L_18 = ___requestState0;
		NullCheck(L_18);
		int64_t L_19 = (int64_t)L_18->get_ID_10();
		__this->set_ID_10(L_19);
		RequestState_1_t8940997 * L_20 = ___requestState0;
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_RespType_4();
		__this->set_RespType_4(L_21);
		return;
	}
}
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t SafeDictionary_2__ctor_m477735165_MetadataUsageId;
extern "C"  void SafeDictionary_2__ctor_m477735165_gshared (SafeDictionary_2_t537630836 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SafeDictionary_2__ctor_m477735165_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		__this->set_syncRoot_0(L_0);
		Dictionary_2_t3624498739 * L_1 = (Dictionary_2_t3624498739 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Dictionary_2_t3624498739 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_d_1(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Add(TKey,TValue)
extern "C"  void SafeDictionary_2_Add_m2132422254_gshared (SafeDictionary_2_t537630836 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t3624498739 * L_2 = (Dictionary_2_t3624498739 *)__this->get_d_1();
		ChannelIdentity_t1147162267  L_3 = ___key0;
		Il2CppObject * L_4 = ___value1;
		NullCheck((Dictionary_2_t3624498739 *)L_2);
		((  void (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Dictionary_2_t3624498739 *)L_2, (ChannelIdentity_t1147162267 )L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		IL2CPP_LEAVE(0x26, FINALLY_001f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001f;
	}

FINALLY_001f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(31)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(31)
	{
		IL2CPP_JUMP_TBL(0x26, IL_0026)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0026:
	{
		return;
	}
}
// TValue PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::AddOrUpdate(TKey,TValue,System.Func`3<TKey,TValue,TValue>)
extern "C"  Il2CppObject * SafeDictionary_2_AddOrUpdate_m4069671251_gshared (SafeDictionary_2_t537630836 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, Func_3_t916471171 * ___f2, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t3624498739 * L_2 = (Dictionary_2_t3624498739 *)__this->get_d_1();
			ChannelIdentity_t1147162267  L_3 = ___key0;
			NullCheck((Dictionary_2_t3624498739 *)L_2);
			bool L_4 = ((  bool (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Dictionary_2_t3624498739 *)L_2, (ChannelIdentity_t1147162267 )L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
			if (!L_4)
			{
				goto IL_0030;
			}
		}

IL_001e:
		{
			Dictionary_2_t3624498739 * L_5 = (Dictionary_2_t3624498739 *)__this->get_d_1();
			ChannelIdentity_t1147162267  L_6 = ___key0;
			Il2CppObject * L_7 = ___value1;
			NullCheck((Dictionary_2_t3624498739 *)L_5);
			((  void (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Dictionary_2_t3624498739 *)L_5, (ChannelIdentity_t1147162267 )L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
			goto IL_003d;
		}

IL_0030:
		{
			Dictionary_2_t3624498739 * L_8 = (Dictionary_2_t3624498739 *)__this->get_d_1();
			ChannelIdentity_t1147162267  L_9 = ___key0;
			Il2CppObject * L_10 = ___value1;
			NullCheck((Dictionary_2_t3624498739 *)L_8);
			((  void (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Dictionary_2_t3624498739 *)L_8, (ChannelIdentity_t1147162267 )L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		}

IL_003d:
		{
			Dictionary_2_t3624498739 * L_11 = (Dictionary_2_t3624498739 *)__this->get_d_1();
			ChannelIdentity_t1147162267  L_12 = ___key0;
			NullCheck((Dictionary_2_t3624498739 *)L_11);
			Il2CppObject * L_13 = ((  Il2CppObject * (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3624498739 *)L_11, (ChannelIdentity_t1147162267 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			V_1 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x56, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_14 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(79)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x56, IL_0056)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0056:
	{
		Il2CppObject * L_15 = V_1;
		return L_15;
	}
}
// TValue PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::GetOrAdd(TKey,TValue)
extern "C"  Il2CppObject * SafeDictionary_2_GetOrAdd_m1330376947_gshared (SafeDictionary_2_t537630836 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t3624498739 * L_2 = (Dictionary_2_t3624498739 *)__this->get_d_1();
			ChannelIdentity_t1147162267  L_3 = ___key0;
			NullCheck((Dictionary_2_t3624498739 *)L_2);
			bool L_4 = ((  bool (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t3624498739 *)L_2, (ChannelIdentity_t1147162267 )L_3, (Il2CppObject **)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			if (!L_4)
			{
				goto IL_0027;
			}
		}

IL_0020:
		{
			Il2CppObject * L_5 = V_1;
			V_2 = (Il2CppObject *)L_5;
			IL2CPP_LEAVE(0x4D, FINALLY_0046);
		}

IL_0027:
		{
			Dictionary_2_t3624498739 * L_6 = (Dictionary_2_t3624498739 *)__this->get_d_1();
			ChannelIdentity_t1147162267  L_7 = ___key0;
			Il2CppObject * L_8 = ___value1;
			NullCheck((Dictionary_2_t3624498739 *)L_6);
			((  void (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Dictionary_2_t3624498739 *)L_6, (ChannelIdentity_t1147162267 )L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
			Dictionary_2_t3624498739 * L_9 = (Dictionary_2_t3624498739 *)__this->get_d_1();
			ChannelIdentity_t1147162267  L_10 = ___key0;
			NullCheck((Dictionary_2_t3624498739 *)L_9);
			Il2CppObject * L_11 = ((  Il2CppObject * (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3624498739 *)L_9, (ChannelIdentity_t1147162267 )L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			V_2 = (Il2CppObject *)L_11;
			IL2CPP_LEAVE(0x4D, FINALLY_0046);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0046;
	}

FINALLY_0046:
	{ // begin finally (depth: 1)
		Il2CppObject * L_12 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_12, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(70)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(70)
	{
		IL2CPP_JUMP_TBL(0x4D, IL_004d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_004d:
	{
		Il2CppObject * L_13 = V_2;
		return L_13;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::ContainsKey(TKey)
extern "C"  bool SafeDictionary_2_ContainsKey_m1555033472_gshared (SafeDictionary_2_t537630836 * __this, ChannelIdentity_t1147162267  ___key0, const MethodInfo* method)
{
	{
		Dictionary_2_t3624498739 * L_0 = (Dictionary_2_t3624498739 *)__this->get_d_1();
		ChannelIdentity_t1147162267  L_1 = ___key0;
		NullCheck((Dictionary_2_t3624498739 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Dictionary_2_t3624498739 *)L_0, (ChannelIdentity_t1147162267 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Collections.Generic.ICollection`1<TKey> PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Keys()
extern "C"  Il2CppObject* SafeDictionary_2_get_Keys_m3127251543_gshared (SafeDictionary_2_t537630836 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t3624498739 * L_2 = (Dictionary_2_t3624498739 *)__this->get_d_1();
		NullCheck((Dictionary_2_t3624498739 *)L_2);
		KeyCollection_t1813029214 * L_3 = ((  KeyCollection_t1813029214 * (*) (Dictionary_2_t3624498739 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t3624498739 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		V_1 = (Il2CppObject*)L_3;
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0025:
	{
		Il2CppObject* L_5 = V_1;
		return L_5;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Remove(TKey)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t SafeDictionary_2_Remove_m3302682888_MetadataUsageId;
extern "C"  bool SafeDictionary_2_Remove_m3302682888_gshared (SafeDictionary_2_t537630836 * __this, ChannelIdentity_t1147162267  ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SafeDictionary_2_Remove_m3302682888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Remove(TKey,TValue&)
extern "C"  bool SafeDictionary_2_Remove_m1992196365_gshared (SafeDictionary_2_t537630836 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t3624498739 * L_2 = (Dictionary_2_t3624498739 *)__this->get_d_1();
		ChannelIdentity_t1147162267  L_3 = ___key0;
		Il2CppObject ** L_4 = ___value1;
		NullCheck((Dictionary_2_t3624498739 *)L_2);
		((  bool (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t3624498739 *)L_2, (ChannelIdentity_t1147162267 )L_3, (Il2CppObject **)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Dictionary_2_t3624498739 * L_5 = (Dictionary_2_t3624498739 *)__this->get_d_1();
		ChannelIdentity_t1147162267  L_6 = ___key0;
		NullCheck((Dictionary_2_t3624498739 *)L_5);
		bool L_7 = ((  bool (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t3624498739 *)L_5, (ChannelIdentity_t1147162267 )L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_1 = (bool)L_7;
		IL2CPP_LEAVE(0x34, FINALLY_002d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002d;
	}

FINALLY_002d:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(45)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(45)
	{
		IL2CPP_JUMP_TBL(0x34, IL_0034)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0034:
	{
		bool L_9 = V_1;
		return L_9;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::TryRemove(TKey,TValue&)
extern "C"  bool SafeDictionary_2_TryRemove_m315854466_gshared (SafeDictionary_2_t537630836 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	{
		ChannelIdentity_t1147162267  L_0 = ___key0;
		Il2CppObject ** L_1 = ___value1;
		NullCheck((SafeDictionary_2_t537630836 *)__this);
		bool L_2 = ((  bool (*) (SafeDictionary_2_t537630836 *, ChannelIdentity_t1147162267 , Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((SafeDictionary_2_t537630836 *)__this, (ChannelIdentity_t1147162267 )L_0, (Il2CppObject **)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_2;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool SafeDictionary_2_TryGetValue_m187104957_gshared (SafeDictionary_2_t537630836 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t3624498739 * L_2 = (Dictionary_2_t3624498739 *)__this->get_d_1();
		ChannelIdentity_t1147162267  L_3 = ___key0;
		Il2CppObject ** L_4 = ___value1;
		NullCheck((Dictionary_2_t3624498739 *)L_2);
		bool L_5 = ((  bool (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t3624498739 *)L_2, (ChannelIdentity_t1147162267 )L_3, (Il2CppObject **)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_1 = (bool)L_5;
		IL2CPP_LEAVE(0x27, FINALLY_0020);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0020;
	}

FINALLY_0020:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(32)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(32)
	{
		IL2CPP_JUMP_TBL(0x27, IL_0027)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0027:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Collections.Generic.ICollection`1<TValue> PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Values()
extern "C"  Il2CppObject* SafeDictionary_2_get_Values_m3845365399_gshared (SafeDictionary_2_t537630836 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t3624498739 * L_2 = (Dictionary_2_t3624498739 *)__this->get_d_1();
		NullCheck((Dictionary_2_t3624498739 *)L_2);
		ValueCollection_t2327558582 * L_3 = ((  ValueCollection_t2327558582 * (*) (Dictionary_2_t3624498739 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t3624498739 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		V_1 = (Il2CppObject*)L_3;
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0025:
	{
		Il2CppObject* L_5 = V_1;
		return L_5;
	}
}
// TValue PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * SafeDictionary_2_get_Item_m1670049536_gshared (SafeDictionary_2_t537630836 * __this, ChannelIdentity_t1147162267  ___key0, const MethodInfo* method)
{
	{
		Dictionary_2_t3624498739 * L_0 = (Dictionary_2_t3624498739 *)__this->get_d_1();
		ChannelIdentity_t1147162267  L_1 = ___key0;
		NullCheck((Dictionary_2_t3624498739 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3624498739 *)L_0, (ChannelIdentity_t1147162267 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_2;
	}
}
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::set_Item(TKey,TValue)
extern "C"  void SafeDictionary_2_set_Item_m651937877_gshared (SafeDictionary_2_t537630836 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t3624498739 * L_2 = (Dictionary_2_t3624498739 *)__this->get_d_1();
		ChannelIdentity_t1147162267  L_3 = ___key0;
		Il2CppObject * L_4 = ___value1;
		NullCheck((Dictionary_2_t3624498739 *)L_2);
		((  void (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Dictionary_2_t3624498739 *)L_2, (ChannelIdentity_t1147162267 )L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		IL2CPP_LEAVE(0x26, FINALLY_001f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001f;
	}

FINALLY_001f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(31)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(31)
	{
		IL2CPP_JUMP_TBL(0x26, IL_0026)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0026:
	{
		return;
	}
}
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void SafeDictionary_2_Add_m2696016565_gshared (SafeDictionary_2_t537630836 * __this, KeyValuePair_2_t1381843961  ___item0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t3624498739 * L_2 = (Dictionary_2_t3624498739 *)__this->get_d_1();
		KeyValuePair_2_t1381843961  L_3 = ___item0;
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker1< KeyValuePair_2_t1381843961  >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::Add(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (Il2CppObject*)L_2, (KeyValuePair_2_t1381843961 )L_3);
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0025:
	{
		return;
	}
}
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Clear()
extern "C"  void SafeDictionary_2_Clear_m3344381162_gshared (SafeDictionary_2_t537630836 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t3624498739 * L_2 = (Dictionary_2_t3624498739 *)__this->get_d_1();
		NullCheck((Dictionary_2_t3624498739 *)L_2);
		((  void (*) (Dictionary_2_t3624498739 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3624498739 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		IL2CPP_LEAVE(0x24, FINALLY_001d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001d;
	}

FINALLY_001d:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(29)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(29)
	{
		IL2CPP_JUMP_TBL(0x24, IL_0024)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0024:
	{
		return;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SafeDictionary_2_Contains_m1452309491_gshared (SafeDictionary_2_t537630836 * __this, KeyValuePair_2_t1381843961  ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3624498739 * L_0 = (Dictionary_2_t3624498739 *)__this->get_d_1();
		KeyValuePair_2_t1381843961  L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, KeyValuePair_2_t1381843961  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::Contains(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (Il2CppObject*)L_0, (KeyValuePair_2_t1381843961 )L_1);
		return L_2;
	}
}
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void SafeDictionary_2_CopyTo_m3279940313_gshared (SafeDictionary_2_t537630836 * __this, KeyValuePair_2U5BU5D_t402044932* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t3624498739 * L_2 = (Dictionary_2_t3624498739 *)__this->get_d_1();
		KeyValuePair_2U5BU5D_t402044932* L_3 = ___array0;
		int32_t L_4 = ___arrayIndex1;
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker2< KeyValuePair_2U5BU5D_t402044932*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (Il2CppObject*)L_2, (KeyValuePair_2U5BU5D_t402044932*)L_3, (int32_t)L_4);
		IL2CPP_LEAVE(0x26, FINALLY_001f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001f;
	}

FINALLY_001f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(31)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(31)
	{
		IL2CPP_JUMP_TBL(0x26, IL_0026)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0026:
	{
		return;
	}
}
// System.Int32 PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Count()
extern "C"  int32_t SafeDictionary_2_get_Count_m2353695869_gshared (SafeDictionary_2_t537630836 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3624498739 * L_0 = (Dictionary_2_t3624498739 *)__this->get_d_1();
		NullCheck((Dictionary_2_t3624498739 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t3624498739 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((Dictionary_2_t3624498739 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_1;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_IsReadOnly()
extern "C"  bool SafeDictionary_2_get_IsReadOnly_m3203599390_gshared (SafeDictionary_2_t537630836 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SafeDictionary_2_Remove_m2963108272_gshared (SafeDictionary_2_t537630836 * __this, KeyValuePair_2_t1381843961  ___item0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t3624498739 * L_2 = (Dictionary_2_t3624498739 *)__this->get_d_1();
		KeyValuePair_2_t1381843961  L_3 = ___item0;
		NullCheck((Il2CppObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, KeyValuePair_2_t1381843961  >::Invoke(6 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::Remove(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (Il2CppObject*)L_2, (KeyValuePair_2_t1381843961 )L_3);
		V_1 = (bool)L_4;
		IL2CPP_LEAVE(0x26, FINALLY_001f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001f;
	}

FINALLY_001f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(31)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(31)
	{
		IL2CPP_JUMP_TBL(0x26, IL_0026)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0026:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* SafeDictionary_2_GetEnumerator_m4239269880_gshared (SafeDictionary_2_t537630836 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3624498739 * L_0 = (Dictionary_2_t3624498739 *)__this->get_d_1();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Collections.IEnumerator PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern const uint32_t SafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1565768858_MetadataUsageId;
extern "C"  Il2CppObject * SafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1565768858_gshared (SafeDictionary_2_t537630836 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1565768858_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3624498739 * L_0 = (Dictionary_2_t3624498739 *)__this->get_d_1();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t SafeDictionary_2__ctor_m3879644162_MetadataUsageId;
extern "C"  void SafeDictionary_2__ctor_m3879644162_gshared (SafeDictionary_2_t2260115521 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SafeDictionary_2__ctor_m3879644162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		__this->set_syncRoot_0(L_0);
		Dictionary_2_t1052016128 * L_1 = (Dictionary_2_t1052016128 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Dictionary_2_t1052016128 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_d_1(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Add(TKey,TValue)
extern "C"  void SafeDictionary_2_Add_m314587869_gshared (SafeDictionary_2_t2260115521 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t1052016128 * L_2 = (Dictionary_2_t1052016128 *)__this->get_d_1();
		int32_t L_3 = ___key0;
		Il2CppObject * L_4 = ___value1;
		NullCheck((Dictionary_2_t1052016128 *)L_2);
		((  void (*) (Dictionary_2_t1052016128 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Dictionary_2_t1052016128 *)L_2, (int32_t)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		IL2CPP_LEAVE(0x26, FINALLY_001f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001f;
	}

FINALLY_001f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(31)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(31)
	{
		IL2CPP_JUMP_TBL(0x26, IL_0026)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0026:
	{
		return;
	}
}
// TValue PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::AddOrUpdate(TKey,TValue,System.Func`3<TKey,TValue,TValue>)
extern "C"  Il2CppObject * SafeDictionary_2_AddOrUpdate_m451755460_gshared (SafeDictionary_2_t2260115521 * __this, int32_t ___key0, Il2CppObject * ___value1, Func_3_t1343333346 * ___f2, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t1052016128 * L_2 = (Dictionary_2_t1052016128 *)__this->get_d_1();
			int32_t L_3 = ___key0;
			NullCheck((Dictionary_2_t1052016128 *)L_2);
			bool L_4 = ((  bool (*) (Dictionary_2_t1052016128 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Dictionary_2_t1052016128 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
			if (!L_4)
			{
				goto IL_0030;
			}
		}

IL_001e:
		{
			Dictionary_2_t1052016128 * L_5 = (Dictionary_2_t1052016128 *)__this->get_d_1();
			int32_t L_6 = ___key0;
			Il2CppObject * L_7 = ___value1;
			NullCheck((Dictionary_2_t1052016128 *)L_5);
			((  void (*) (Dictionary_2_t1052016128 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Dictionary_2_t1052016128 *)L_5, (int32_t)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
			goto IL_003d;
		}

IL_0030:
		{
			Dictionary_2_t1052016128 * L_8 = (Dictionary_2_t1052016128 *)__this->get_d_1();
			int32_t L_9 = ___key0;
			Il2CppObject * L_10 = ___value1;
			NullCheck((Dictionary_2_t1052016128 *)L_8);
			((  void (*) (Dictionary_2_t1052016128 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Dictionary_2_t1052016128 *)L_8, (int32_t)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		}

IL_003d:
		{
			Dictionary_2_t1052016128 * L_11 = (Dictionary_2_t1052016128 *)__this->get_d_1();
			int32_t L_12 = ___key0;
			NullCheck((Dictionary_2_t1052016128 *)L_11);
			Il2CppObject * L_13 = ((  Il2CppObject * (*) (Dictionary_2_t1052016128 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1052016128 *)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			V_1 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x56, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_14 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(79)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x56, IL_0056)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0056:
	{
		Il2CppObject * L_15 = V_1;
		return L_15;
	}
}
// TValue PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::GetOrAdd(TKey,TValue)
extern "C"  Il2CppObject * SafeDictionary_2_GetOrAdd_m1063728122_gshared (SafeDictionary_2_t2260115521 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t1052016128 * L_2 = (Dictionary_2_t1052016128 *)__this->get_d_1();
			int32_t L_3 = ___key0;
			NullCheck((Dictionary_2_t1052016128 *)L_2);
			bool L_4 = ((  bool (*) (Dictionary_2_t1052016128 *, int32_t, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t1052016128 *)L_2, (int32_t)L_3, (Il2CppObject **)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			if (!L_4)
			{
				goto IL_0027;
			}
		}

IL_0020:
		{
			Il2CppObject * L_5 = V_1;
			V_2 = (Il2CppObject *)L_5;
			IL2CPP_LEAVE(0x4D, FINALLY_0046);
		}

IL_0027:
		{
			Dictionary_2_t1052016128 * L_6 = (Dictionary_2_t1052016128 *)__this->get_d_1();
			int32_t L_7 = ___key0;
			Il2CppObject * L_8 = ___value1;
			NullCheck((Dictionary_2_t1052016128 *)L_6);
			((  void (*) (Dictionary_2_t1052016128 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Dictionary_2_t1052016128 *)L_6, (int32_t)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
			Dictionary_2_t1052016128 * L_9 = (Dictionary_2_t1052016128 *)__this->get_d_1();
			int32_t L_10 = ___key0;
			NullCheck((Dictionary_2_t1052016128 *)L_9);
			Il2CppObject * L_11 = ((  Il2CppObject * (*) (Dictionary_2_t1052016128 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1052016128 *)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			V_2 = (Il2CppObject *)L_11;
			IL2CPP_LEAVE(0x4D, FINALLY_0046);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0046;
	}

FINALLY_0046:
	{ // begin finally (depth: 1)
		Il2CppObject * L_12 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_12, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(70)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(70)
	{
		IL2CPP_JUMP_TBL(0x4D, IL_004d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_004d:
	{
		Il2CppObject * L_13 = V_2;
		return L_13;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::ContainsKey(TKey)
extern "C"  bool SafeDictionary_2_ContainsKey_m98092443_gshared (SafeDictionary_2_t2260115521 * __this, int32_t ___key0, const MethodInfo* method)
{
	{
		Dictionary_2_t1052016128 * L_0 = (Dictionary_2_t1052016128 *)__this->get_d_1();
		int32_t L_1 = ___key0;
		NullCheck((Dictionary_2_t1052016128 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1052016128 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Dictionary_2_t1052016128 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Collections.Generic.ICollection`1<TKey> PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Keys()
extern "C"  Il2CppObject* SafeDictionary_2_get_Keys_m49952234_gshared (SafeDictionary_2_t2260115521 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t1052016128 * L_2 = (Dictionary_2_t1052016128 *)__this->get_d_1();
		NullCheck((Dictionary_2_t1052016128 *)L_2);
		KeyCollection_t3535513899 * L_3 = ((  KeyCollection_t3535513899 * (*) (Dictionary_2_t1052016128 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t1052016128 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		V_1 = (Il2CppObject*)L_3;
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0025:
	{
		Il2CppObject* L_5 = V_1;
		return L_5;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Remove(TKey)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t SafeDictionary_2_Remove_m1715787805_MetadataUsageId;
extern "C"  bool SafeDictionary_2_Remove_m1715787805_gshared (SafeDictionary_2_t2260115521 * __this, int32_t ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SafeDictionary_2_Remove_m1715787805_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Remove(TKey,TValue&)
extern "C"  bool SafeDictionary_2_Remove_m3758221292_gshared (SafeDictionary_2_t2260115521 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t1052016128 * L_2 = (Dictionary_2_t1052016128 *)__this->get_d_1();
		int32_t L_3 = ___key0;
		Il2CppObject ** L_4 = ___value1;
		NullCheck((Dictionary_2_t1052016128 *)L_2);
		((  bool (*) (Dictionary_2_t1052016128 *, int32_t, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t1052016128 *)L_2, (int32_t)L_3, (Il2CppObject **)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Dictionary_2_t1052016128 * L_5 = (Dictionary_2_t1052016128 *)__this->get_d_1();
		int32_t L_6 = ___key0;
		NullCheck((Dictionary_2_t1052016128 *)L_5);
		bool L_7 = ((  bool (*) (Dictionary_2_t1052016128 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t1052016128 *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_1 = (bool)L_7;
		IL2CPP_LEAVE(0x34, FINALLY_002d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002d;
	}

FINALLY_002d:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(45)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(45)
	{
		IL2CPP_JUMP_TBL(0x34, IL_0034)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0034:
	{
		bool L_9 = V_1;
		return L_9;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::TryRemove(TKey,TValue&)
extern "C"  bool SafeDictionary_2_TryRemove_m807166159_gshared (SafeDictionary_2_t2260115521 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		Il2CppObject ** L_1 = ___value1;
		NullCheck((SafeDictionary_2_t2260115521 *)__this);
		bool L_2 = ((  bool (*) (SafeDictionary_2_t2260115521 *, int32_t, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((SafeDictionary_2_t2260115521 *)__this, (int32_t)L_0, (Il2CppObject **)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_2;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool SafeDictionary_2_TryGetValue_m2503777870_gshared (SafeDictionary_2_t2260115521 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t1052016128 * L_2 = (Dictionary_2_t1052016128 *)__this->get_d_1();
		int32_t L_3 = ___key0;
		Il2CppObject ** L_4 = ___value1;
		NullCheck((Dictionary_2_t1052016128 *)L_2);
		bool L_5 = ((  bool (*) (Dictionary_2_t1052016128 *, int32_t, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t1052016128 *)L_2, (int32_t)L_3, (Il2CppObject **)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_1 = (bool)L_5;
		IL2CPP_LEAVE(0x27, FINALLY_0020);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0020;
	}

FINALLY_0020:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(32)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(32)
	{
		IL2CPP_JUMP_TBL(0x27, IL_0027)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0027:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Collections.Generic.ICollection`1<TValue> PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Values()
extern "C"  Il2CppObject* SafeDictionary_2_get_Values_m2932400594_gshared (SafeDictionary_2_t2260115521 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t1052016128 * L_2 = (Dictionary_2_t1052016128 *)__this->get_d_1();
		NullCheck((Dictionary_2_t1052016128 *)L_2);
		ValueCollection_t4050043267 * L_3 = ((  ValueCollection_t4050043267 * (*) (Dictionary_2_t1052016128 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t1052016128 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		V_1 = (Il2CppObject*)L_3;
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0025:
	{
		Il2CppObject* L_5 = V_1;
		return L_5;
	}
}
// TValue PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * SafeDictionary_2_get_Item_m3585663459_gshared (SafeDictionary_2_t2260115521 * __this, int32_t ___key0, const MethodInfo* method)
{
	{
		Dictionary_2_t1052016128 * L_0 = (Dictionary_2_t1052016128 *)__this->get_d_1();
		int32_t L_1 = ___key0;
		NullCheck((Dictionary_2_t1052016128 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Dictionary_2_t1052016128 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1052016128 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_2;
	}
}
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::set_Item(TKey,TValue)
extern "C"  void SafeDictionary_2_set_Item_m1676305360_gshared (SafeDictionary_2_t2260115521 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t1052016128 * L_2 = (Dictionary_2_t1052016128 *)__this->get_d_1();
		int32_t L_3 = ___key0;
		Il2CppObject * L_4 = ___value1;
		NullCheck((Dictionary_2_t1052016128 *)L_2);
		((  void (*) (Dictionary_2_t1052016128 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Dictionary_2_t1052016128 *)L_2, (int32_t)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		IL2CPP_LEAVE(0x26, FINALLY_001f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001f;
	}

FINALLY_001f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(31)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(31)
	{
		IL2CPP_JUMP_TBL(0x26, IL_0026)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0026:
	{
		return;
	}
}
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void SafeDictionary_2_Add_m4012251184_gshared (SafeDictionary_2_t2260115521 * __this, KeyValuePair_2_t3104328646  ___item0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t1052016128 * L_2 = (Dictionary_2_t1052016128 *)__this->get_d_1();
		KeyValuePair_2_t3104328646  L_3 = ___item0;
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker1< KeyValuePair_2_t3104328646  >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::Add(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (Il2CppObject*)L_2, (KeyValuePair_2_t3104328646 )L_3);
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0025:
	{
		return;
	}
}
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Clear()
extern "C"  void SafeDictionary_2_Clear_m2336160809_gshared (SafeDictionary_2_t2260115521 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t1052016128 * L_2 = (Dictionary_2_t1052016128 *)__this->get_d_1();
		NullCheck((Dictionary_2_t1052016128 *)L_2);
		((  void (*) (Dictionary_2_t1052016128 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1052016128 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		IL2CPP_LEAVE(0x24, FINALLY_001d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001d;
	}

FINALLY_001d:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(29)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(29)
	{
		IL2CPP_JUMP_TBL(0x24, IL_0024)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0024:
	{
		return;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SafeDictionary_2_Contains_m2506908634_gshared (SafeDictionary_2_t2260115521 * __this, KeyValuePair_2_t3104328646  ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t1052016128 * L_0 = (Dictionary_2_t1052016128 *)__this->get_d_1();
		KeyValuePair_2_t3104328646  L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, KeyValuePair_2_t3104328646  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::Contains(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (Il2CppObject*)L_0, (KeyValuePair_2_t3104328646 )L_1);
		return L_2;
	}
}
// System.Void PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void SafeDictionary_2_CopyTo_m804417436_gshared (SafeDictionary_2_t2260115521 * __this, KeyValuePair_2U5BU5D_t828907107* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t1052016128 * L_2 = (Dictionary_2_t1052016128 *)__this->get_d_1();
		KeyValuePair_2U5BU5D_t828907107* L_3 = ___array0;
		int32_t L_4 = ___arrayIndex1;
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker2< KeyValuePair_2U5BU5D_t828907107*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (Il2CppObject*)L_2, (KeyValuePair_2U5BU5D_t828907107*)L_3, (int32_t)L_4);
		IL2CPP_LEAVE(0x26, FINALLY_001f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001f;
	}

FINALLY_001f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(31)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(31)
	{
		IL2CPP_JUMP_TBL(0x26, IL_0026)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0026:
	{
		return;
	}
}
// System.Int32 PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Count()
extern "C"  int32_t SafeDictionary_2_get_Count_m877073808_gshared (SafeDictionary_2_t2260115521 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1052016128 * L_0 = (Dictionary_2_t1052016128 *)__this->get_d_1();
		NullCheck((Dictionary_2_t1052016128 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1052016128 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((Dictionary_2_t1052016128 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_1;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_IsReadOnly()
extern "C"  bool SafeDictionary_2_get_IsReadOnly_m3793001841_gshared (SafeDictionary_2_t2260115521 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SafeDictionary_2_Remove_m179165939_gshared (SafeDictionary_2_t2260115521 * __this, KeyValuePair_2_t3104328646  ___item0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t1052016128 * L_2 = (Dictionary_2_t1052016128 *)__this->get_d_1();
		KeyValuePair_2_t3104328646  L_3 = ___item0;
		NullCheck((Il2CppObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, KeyValuePair_2_t3104328646  >::Invoke(6 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::Remove(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (Il2CppObject*)L_2, (KeyValuePair_2_t3104328646 )L_3);
		V_1 = (bool)L_4;
		IL2CPP_LEAVE(0x26, FINALLY_001f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001f;
	}

FINALLY_001f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(31)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(31)
	{
		IL2CPP_JUMP_TBL(0x26, IL_0026)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0026:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* SafeDictionary_2_GetEnumerator_m1940543661_gshared (SafeDictionary_2_t2260115521 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1052016128 * L_0 = (Dictionary_2_t1052016128 *)__this->get_d_1();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Collections.IEnumerator PubNubMessaging.Core.SafeDictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern const uint32_t SafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m4022993505_MetadataUsageId;
extern "C"  Il2CppObject * SafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m4022993505_gshared (SafeDictionary_2_t2260115521 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m4022993505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1052016128 * L_0 = (Dictionary_2_t1052016128 *)__this->get_d_1();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t SafeDictionary_2__ctor_m185996061_MetadataUsageId;
extern "C"  void SafeDictionary_2__ctor_m185996061_gshared (SafeDictionary_2_t3489608816 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SafeDictionary_2__ctor_m185996061_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		__this->set_syncRoot_0(L_0);
		Dictionary_2_t2281509423 * L_1 = (Dictionary_2_t2281509423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_d_1(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C"  void SafeDictionary_2_Add_m3293730050_gshared (SafeDictionary_2_t3489608816 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t2281509423 * L_2 = (Dictionary_2_t2281509423 *)__this->get_d_1();
		Il2CppObject * L_3 = ___key0;
		Il2CppObject * L_4 = ___value1;
		NullCheck((Dictionary_2_t2281509423 *)L_2);
		((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Dictionary_2_t2281509423 *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		IL2CPP_LEAVE(0x26, FINALLY_001f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001f;
	}

FINALLY_001f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(31)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(31)
	{
		IL2CPP_JUMP_TBL(0x26, IL_0026)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0026:
	{
		return;
	}
}
// TValue PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::AddOrUpdate(TKey,TValue,System.Func`3<TKey,TValue,TValue>)
extern "C"  Il2CppObject * SafeDictionary_2_AddOrUpdate_m2465249327_gshared (SafeDictionary_2_t3489608816 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, Func_3_t3369346583 * ___f2, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t2281509423 * L_2 = (Dictionary_2_t2281509423 *)__this->get_d_1();
			Il2CppObject * L_3 = ___key0;
			NullCheck((Dictionary_2_t2281509423 *)L_2);
			bool L_4 = ((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Dictionary_2_t2281509423 *)L_2, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
			if (!L_4)
			{
				goto IL_0030;
			}
		}

IL_001e:
		{
			Dictionary_2_t2281509423 * L_5 = (Dictionary_2_t2281509423 *)__this->get_d_1();
			Il2CppObject * L_6 = ___key0;
			Il2CppObject * L_7 = ___value1;
			NullCheck((Dictionary_2_t2281509423 *)L_5);
			((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Dictionary_2_t2281509423 *)L_5, (Il2CppObject *)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
			goto IL_003d;
		}

IL_0030:
		{
			Dictionary_2_t2281509423 * L_8 = (Dictionary_2_t2281509423 *)__this->get_d_1();
			Il2CppObject * L_9 = ___key0;
			Il2CppObject * L_10 = ___value1;
			NullCheck((Dictionary_2_t2281509423 *)L_8);
			((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Dictionary_2_t2281509423 *)L_8, (Il2CppObject *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		}

IL_003d:
		{
			Dictionary_2_t2281509423 * L_11 = (Dictionary_2_t2281509423 *)__this->get_d_1();
			Il2CppObject * L_12 = ___key0;
			NullCheck((Dictionary_2_t2281509423 *)L_11);
			Il2CppObject * L_13 = ((  Il2CppObject * (*) (Dictionary_2_t2281509423 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2281509423 *)L_11, (Il2CppObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			V_1 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x56, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_14 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(79)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x56, IL_0056)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0056:
	{
		Il2CppObject * L_15 = V_1;
		return L_15;
	}
}
// TValue PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::GetOrAdd(TKey,TValue)
extern "C"  Il2CppObject * SafeDictionary_2_GetOrAdd_m4159390623_gshared (SafeDictionary_2_t3489608816 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t2281509423 * L_2 = (Dictionary_2_t2281509423 *)__this->get_d_1();
			Il2CppObject * L_3 = ___key0;
			NullCheck((Dictionary_2_t2281509423 *)L_2);
			bool L_4 = ((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t2281509423 *)L_2, (Il2CppObject *)L_3, (Il2CppObject **)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			if (!L_4)
			{
				goto IL_0027;
			}
		}

IL_0020:
		{
			Il2CppObject * L_5 = V_1;
			V_2 = (Il2CppObject *)L_5;
			IL2CPP_LEAVE(0x4D, FINALLY_0046);
		}

IL_0027:
		{
			Dictionary_2_t2281509423 * L_6 = (Dictionary_2_t2281509423 *)__this->get_d_1();
			Il2CppObject * L_7 = ___key0;
			Il2CppObject * L_8 = ___value1;
			NullCheck((Dictionary_2_t2281509423 *)L_6);
			((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((Dictionary_2_t2281509423 *)L_6, (Il2CppObject *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
			Dictionary_2_t2281509423 * L_9 = (Dictionary_2_t2281509423 *)__this->get_d_1();
			Il2CppObject * L_10 = ___key0;
			NullCheck((Dictionary_2_t2281509423 *)L_9);
			Il2CppObject * L_11 = ((  Il2CppObject * (*) (Dictionary_2_t2281509423 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2281509423 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
			V_2 = (Il2CppObject *)L_11;
			IL2CPP_LEAVE(0x4D, FINALLY_0046);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0046;
	}

FINALLY_0046:
	{ // begin finally (depth: 1)
		Il2CppObject * L_12 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_12, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(70)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(70)
	{
		IL2CPP_JUMP_TBL(0x4D, IL_004d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_004d:
	{
		Il2CppObject * L_13 = V_2;
		return L_13;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C"  bool SafeDictionary_2_ContainsKey_m2132572568_gshared (SafeDictionary_2_t3489608816 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_d_1();
		Il2CppObject * L_1 = ___key0;
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Dictionary_2_t2281509423 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Collections.Generic.ICollection`1<TKey> PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::get_Keys()
extern "C"  Il2CppObject* SafeDictionary_2_get_Keys_m1102826111_gshared (SafeDictionary_2_t3489608816 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t2281509423 * L_2 = (Dictionary_2_t2281509423 *)__this->get_d_1();
		NullCheck((Dictionary_2_t2281509423 *)L_2);
		KeyCollection_t470039898 * L_3 = ((  KeyCollection_t470039898 * (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t2281509423 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		V_1 = (Il2CppObject*)L_3;
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0025:
	{
		Il2CppObject* L_5 = V_1;
		return L_5;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::Remove(TKey)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t SafeDictionary_2_Remove_m595894796_MetadataUsageId;
extern "C"  bool SafeDictionary_2_Remove_m595894796_gshared (SafeDictionary_2_t3489608816 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SafeDictionary_2_Remove_m595894796_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::Remove(TKey,TValue&)
extern "C"  bool SafeDictionary_2_Remove_m428347805_gshared (SafeDictionary_2_t3489608816 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t2281509423 * L_2 = (Dictionary_2_t2281509423 *)__this->get_d_1();
		Il2CppObject * L_3 = ___key0;
		Il2CppObject ** L_4 = ___value1;
		NullCheck((Dictionary_2_t2281509423 *)L_2);
		((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t2281509423 *)L_2, (Il2CppObject *)L_3, (Il2CppObject **)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Dictionary_2_t2281509423 * L_5 = (Dictionary_2_t2281509423 *)__this->get_d_1();
		Il2CppObject * L_6 = ___key0;
		NullCheck((Dictionary_2_t2281509423 *)L_5);
		bool L_7 = ((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t2281509423 *)L_5, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_1 = (bool)L_7;
		IL2CPP_LEAVE(0x34, FINALLY_002d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002d;
	}

FINALLY_002d:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(45)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(45)
	{
		IL2CPP_JUMP_TBL(0x34, IL_0034)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0034:
	{
		bool L_9 = V_1;
		return L_9;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::TryRemove(TKey,TValue&)
extern "C"  bool SafeDictionary_2_TryRemove_m127498602_gshared (SafeDictionary_2_t3489608816 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		Il2CppObject ** L_1 = ___value1;
		NullCheck((SafeDictionary_2_t3489608816 *)__this);
		bool L_2 = ((  bool (*) (SafeDictionary_2_t3489608816 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((SafeDictionary_2_t3489608816 *)__this, (Il2CppObject *)L_0, (Il2CppObject **)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_2;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool SafeDictionary_2_TryGetValue_m2854498045_gshared (SafeDictionary_2_t3489608816 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t2281509423 * L_2 = (Dictionary_2_t2281509423 *)__this->get_d_1();
		Il2CppObject * L_3 = ___key0;
		Il2CppObject ** L_4 = ___value1;
		NullCheck((Dictionary_2_t2281509423 *)L_2);
		bool L_5 = ((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t2281509423 *)L_2, (Il2CppObject *)L_3, (Il2CppObject **)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_1 = (bool)L_5;
		IL2CPP_LEAVE(0x27, FINALLY_0020);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0020;
	}

FINALLY_0020:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(32)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(32)
	{
		IL2CPP_JUMP_TBL(0x27, IL_0027)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0027:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Collections.Generic.ICollection`1<TValue> PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::get_Values()
extern "C"  Il2CppObject* SafeDictionary_2_get_Values_m1477461839_gshared (SafeDictionary_2_t3489608816 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t2281509423 * L_2 = (Dictionary_2_t2281509423 *)__this->get_d_1();
		NullCheck((Dictionary_2_t2281509423 *)L_2);
		ValueCollection_t984569266 * L_3 = ((  ValueCollection_t984569266 * (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t2281509423 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		V_1 = (Il2CppObject*)L_3;
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0025:
	{
		Il2CppObject* L_5 = V_1;
		return L_5;
	}
}
// TValue PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * SafeDictionary_2_get_Item_m713319416_gshared (SafeDictionary_2_t3489608816 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_d_1();
		Il2CppObject * L_1 = ___key0;
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Dictionary_2_t2281509423 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2281509423 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return L_2;
	}
}
// System.Void PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C"  void SafeDictionary_2_set_Item_m1589065141_gshared (SafeDictionary_2_t3489608816 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t2281509423 * L_2 = (Dictionary_2_t2281509423 *)__this->get_d_1();
		Il2CppObject * L_3 = ___key0;
		Il2CppObject * L_4 = ___value1;
		NullCheck((Dictionary_2_t2281509423 *)L_2);
		((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((Dictionary_2_t2281509423 *)L_2, (Il2CppObject *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		IL2CPP_LEAVE(0x26, FINALLY_001f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001f;
	}

FINALLY_001f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(31)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(31)
	{
		IL2CPP_JUMP_TBL(0x26, IL_0026)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0026:
	{
		return;
	}
}
// System.Void PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void SafeDictionary_2_Add_m2131419317_gshared (SafeDictionary_2_t3489608816 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t2281509423 * L_2 = (Dictionary_2_t2281509423 *)__this->get_d_1();
		KeyValuePair_2_t38854645  L_3 = ___item0;
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker1< KeyValuePair_2_t38854645  >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (Il2CppObject*)L_2, (KeyValuePair_2_t38854645 )L_3);
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0025:
	{
		return;
	}
}
// System.Void PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::Clear()
extern "C"  void SafeDictionary_2_Clear_m2775642342_gshared (SafeDictionary_2_t3489608816 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t2281509423 * L_2 = (Dictionary_2_t2281509423 *)__this->get_d_1();
		NullCheck((Dictionary_2_t2281509423 *)L_2);
		((  void (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2281509423 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		IL2CPP_LEAVE(0x24, FINALLY_001d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001d;
	}

FINALLY_001d:
	{ // begin finally (depth: 1)
		Il2CppObject * L_3 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(29)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(29)
	{
		IL2CPP_JUMP_TBL(0x24, IL_0024)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0024:
	{
		return;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SafeDictionary_2_Contains_m3251567199_gshared (SafeDictionary_2_t3489608816 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_d_1();
		KeyValuePair_2_t38854645  L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, KeyValuePair_2_t38854645  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Contains(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (Il2CppObject*)L_0, (KeyValuePair_2_t38854645 )L_1);
		return L_2;
	}
}
// System.Void PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void SafeDictionary_2_CopyTo_m1628504833_gshared (SafeDictionary_2_t3489608816 * __this, KeyValuePair_2U5BU5D_t2854920344* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t2281509423 * L_2 = (Dictionary_2_t2281509423 *)__this->get_d_1();
		KeyValuePair_2U5BU5D_t2854920344* L_3 = ___array0;
		int32_t L_4 = ___arrayIndex1;
		NullCheck((Il2CppObject*)L_2);
		InterfaceActionInvoker2< KeyValuePair_2U5BU5D_t2854920344*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (Il2CppObject*)L_2, (KeyValuePair_2U5BU5D_t2854920344*)L_3, (int32_t)L_4);
		IL2CPP_LEAVE(0x26, FINALLY_001f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001f;
	}

FINALLY_001f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(31)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(31)
	{
		IL2CPP_JUMP_TBL(0x26, IL_0026)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0026:
	{
		return;
	}
}
// System.Int32 PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t SafeDictionary_2_get_Count_m2126862281_gshared (SafeDictionary_2_t3489608816 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_d_1();
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_1;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::get_IsReadOnly()
extern "C"  bool SafeDictionary_2_get_IsReadOnly_m2796149634_gshared (SafeDictionary_2_t3489608816 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool SafeDictionary_2_Remove_m503522568_gshared (SafeDictionary_2_t3489608816 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_0();
		V_0 = (Il2CppObject *)L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t2281509423 * L_2 = (Dictionary_2_t2281509423 *)__this->get_d_1();
		KeyValuePair_2_t38854645  L_3 = ___item0;
		NullCheck((Il2CppObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, KeyValuePair_2_t38854645  >::Invoke(6 /* System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Remove(!0) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 11), (Il2CppObject*)L_2, (KeyValuePair_2_t38854645 )L_3);
		V_1 = (bool)L_4;
		IL2CPP_LEAVE(0x26, FINALLY_001f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001f;
	}

FINALLY_001f:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(31)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(31)
	{
		IL2CPP_JUMP_TBL(0x26, IL_0026)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0026:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* SafeDictionary_2_GetEnumerator_m4207349278_gshared (SafeDictionary_2_t3489608816 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_d_1();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Collections.IEnumerator PubNubMessaging.Core.SafeDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern const uint32_t SafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m146141658_MetadataUsageId;
extern "C"  Il2CppObject * SafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m146141658_gshared (SafeDictionary_2_t3489608816 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m146141658_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_d_1();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3072925129_gshared (Action_1_t3627374100 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Boolean>::Invoke(T)
extern "C"  void Action_1_Invoke_m3662000152_gshared (Action_1_t3627374100 * __this, bool ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3662000152((Action_1_t3627374100 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m226849422_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m226849422_gshared (Action_1_t3627374100 * __this, bool ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m226849422_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2990292511_gshared (Action_1_t3627374100 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2939456407_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m101203496_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m101203496((Action_1_t2491248677 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1305519803_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2057605070_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Boolean,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m946854823_gshared (Action_2_t2525452034 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Boolean,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m3842146412_gshared (Action_2_t2525452034 * __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m3842146412((Action_2_t2525452034 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Boolean,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m3907381723_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m3907381723_gshared (Action_2_t2525452034 * __this, bool ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m3907381723_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Boolean,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m2798191693_gshared (Action_2_t2525452034 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m3362391082_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1501152969_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m1501152969((Action_2_t2572051853 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m1914861552_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m3956733788_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m1015489335_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1791706206_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2580780957_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m2489948797_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t2471096271 * L_2 = (ArrayReadOnlyList_1_t2471096271 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t2471096271 * L_9 = (ArrayReadOnlyList_1_t2471096271 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1859988746_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m1942816078_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeNamedArgument_t94157543  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m285299945_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = (CustomAttributeNamedArgument_t94157543 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m480171694_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = (CustomAttributeNamedArgument_t94157543 )__this->get_U24current_2();
		CustomAttributeNamedArgument_t94157543  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m949306872_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t4170771815 * L_2 = (ArrayReadOnlyList_1_t4170771815 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t94157543  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t4170771815 * L_9 = (ArrayReadOnlyList_1_t4170771815 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_10 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2403602883_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m409316647_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeTypedArgument_t1498197914  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m988222504_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = (CustomAttributeTypedArgument_t1498197914 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2332089385_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = (CustomAttributeTypedArgument_t1498197914 )__this->get_U24current_2();
		CustomAttributeTypedArgument_t1498197914  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m692741405_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t1279844890 * L_2 = (ArrayReadOnlyList_1_t1279844890 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_3 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t1498197914  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t1279844890 * L_9 = (ArrayReadOnlyList_1_t1279844890 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_10 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2201090542_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m2430810679_gshared (ArrayReadOnlyList_1_t2471096271 * __this, ObjectU5BU5D_t3614634134* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m2780765696_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t2471096271 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t2471096271 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t2471096271 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m176001975_MetadataUsageId;
extern "C"  Il2CppObject * ArrayReadOnlyList_1_get_Item_m176001975_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m176001975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m314687476_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m962317777_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m2717922212_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m3970067462_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m2539474626_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m1266627404_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m816115094_gshared (ArrayReadOnlyList_1_t2471096271 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m1078352793_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1537228832_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m1136669199_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m1875216835_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2701218731_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2772642243;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m2289309720_MetadataUsageId;
extern "C"  Exception_t1927440687 * ArrayReadOnlyList_1_ReadOnlyError_m2289309720_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m2289309720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral2772642243, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m691892240_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgumentU5BU5D_t3304067486* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m3039869667_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t4170771815 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t4170771815 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t4170771815 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m2694472846_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t94157543  ArrayReadOnlyList_1_get_Item_m2694472846_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m2694472846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_1 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t94157543  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m3536854615_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, CustomAttributeNamedArgument_t94157543  ___value1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2661355086_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m2189922207_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m961024239_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m1565299387_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m1269788217_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgument_t94157543  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_0, (CustomAttributeNamedArgument_t94157543 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m4003949395_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgumentU5BU5D_t3304067486* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m634288642_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1220844927_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgument_t94157543  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_0, (CustomAttributeNamedArgument_t94157543 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m2938723476_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, CustomAttributeNamedArgument_t94157543  ___item1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m2325516426_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m4104441984_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2772642243;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m2160816107_MetadataUsageId;
extern "C"  Exception_t1927440687 * ArrayReadOnlyList_1_ReadOnlyError_m2160816107_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m2160816107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral2772642243, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m3778554727_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgumentU5BU5D_t1075686591* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m3194679940_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t1279844890 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t1279844890 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t1279844890 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m2045253203_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t1498197914  ArrayReadOnlyList_1_get_Item_m2045253203_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m2045253203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_1 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_3 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t1498197914  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m1476592004_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, CustomAttributeTypedArgument_t1498197914  ___value1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2272682593_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m745254596_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m592463462_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m638842154_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m1984901664_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		CustomAttributeTypedArgument_t1498197914  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_0, (CustomAttributeTypedArgument_t1498197914 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m3708038182_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgumentU5BU5D_t1075686591* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m3821693737_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1809425308_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		CustomAttributeTypedArgument_t1498197914  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_0, (CustomAttributeTypedArgument_t1498197914 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m503707439_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, CustomAttributeTypedArgument_t1498197914  ___item1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m632503387_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2270349795_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2772642243;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m2158247090_MetadataUsageId;
extern "C"  Exception_t1927440687 * ArrayReadOnlyList_1_ReadOnlyError_m2158247090_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m2158247090_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral2772642243, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2265739932_gshared (InternalEnumerator_1_t2870158877 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2265739932_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	InternalEnumerator_1__ctor_m2265739932(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	{
		TableRange_t2011406615  L_0 = InternalEnumerator_1_get_Current_m2151132603((InternalEnumerator_1_t2870158877 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TableRange_t2011406615  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1050822571_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1050822571_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1050822571(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1979432532_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1979432532_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1979432532(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2151132603_MetadataUsageId;
extern "C"  TableRange_t2011406615  InternalEnumerator_1_get_Current_m2151132603_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2151132603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TableRange_t2011406615  L_8 = ((  TableRange_t2011406615  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TableRange_t2011406615  InternalEnumerator_1_get_Current_m2151132603_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2151132603(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2111763266_gshared (InternalEnumerator_1_t565169432 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2111763266_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	InternalEnumerator_1__ctor_m2111763266(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3847951219((InternalEnumerator_1_t565169432 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2038682075_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2038682075_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2038682075(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1182905290_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1182905290_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1182905290(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3847951219_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3847951219_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3847951219_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3847951219_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3847951219(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m815597740_gshared (InternalEnumerator_1_t1542250127 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m815597740_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1542250127 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1542250127 *>(__this + 1);
	InternalEnumerator_1__ctor_m815597740(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1254107312_gshared (InternalEnumerator_1_t1542250127 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1254107312_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1542250127 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1542250127 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1254107312(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2914362606_gshared (InternalEnumerator_1_t1542250127 * __this, const MethodInfo* method)
{
	{
		UriScheme_t683497865  L_0 = InternalEnumerator_1_get_Current_m3577122241((InternalEnumerator_1_t1542250127 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UriScheme_t683497865  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2914362606_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1542250127 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1542250127 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2914362606(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m766702873_gshared (InternalEnumerator_1_t1542250127 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m766702873_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1542250127 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1542250127 *>(__this + 1);
	InternalEnumerator_1_Dispose_m766702873(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2738761868_gshared (InternalEnumerator_1_t1542250127 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2738761868_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1542250127 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1542250127 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2738761868(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3577122241_MetadataUsageId;
extern "C"  UriScheme_t683497865  InternalEnumerator_1_get_Current_m3577122241_gshared (InternalEnumerator_1_t1542250127 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3577122241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UriScheme_t683497865  L_8 = ((  UriScheme_t683497865  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  UriScheme_t683497865  InternalEnumerator_1_get_Current_m3577122241_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1542250127 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1542250127 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3577122241(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1834162958_gshared (InternalEnumerator_1_t3199726719 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1834162958_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3199726719 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3199726719 *>(__this + 1);
	InternalEnumerator_1__ctor_m1834162958(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3824170762_gshared (InternalEnumerator_1_t3199726719 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3824170762_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3199726719 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3199726719 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3824170762(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2418052108_gshared (InternalEnumerator_1_t3199726719 * __this, const MethodInfo* method)
{
	{
		TagName_t2340974457  L_0 = InternalEnumerator_1_get_Current_m48002065((InternalEnumerator_1_t3199726719 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TagName_t2340974457  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2418052108_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3199726719 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3199726719 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2418052108(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1222535961_gshared (InternalEnumerator_1_t3199726719 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1222535961_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3199726719 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3199726719 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1222535961(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3920221998_gshared (InternalEnumerator_1_t3199726719 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3920221998_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3199726719 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3199726719 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3920221998(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m48002065_MetadataUsageId;
extern "C"  TagName_t2340974457  InternalEnumerator_1_get_Current_m48002065_gshared (InternalEnumerator_1_t3199726719 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m48002065_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TagName_t2340974457  L_8 = ((  TagName_t2340974457  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TagName_t2340974457  InternalEnumerator_1_get_Current_m48002065_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3199726719 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3199726719 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m48002065(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<PubNubMessaging.Core.ChannelIdentity>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2901382657_gshared (InternalEnumerator_1_t2005914529 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2901382657_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2005914529 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2005914529 *>(__this + 1);
	InternalEnumerator_1__ctor_m2901382657(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<PubNubMessaging.Core.ChannelIdentity>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m791631653_gshared (InternalEnumerator_1_t2005914529 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m791631653_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2005914529 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2005914529 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m791631653(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<PubNubMessaging.Core.ChannelIdentity>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1587478253_gshared (InternalEnumerator_1_t2005914529 * __this, const MethodInfo* method)
{
	{
		ChannelIdentity_t1147162267  L_0 = InternalEnumerator_1_get_Current_m2483800584((InternalEnumerator_1_t2005914529 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ChannelIdentity_t1147162267  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1587478253_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2005914529 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2005914529 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1587478253(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<PubNubMessaging.Core.ChannelIdentity>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1356736170_gshared (InternalEnumerator_1_t2005914529 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1356736170_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2005914529 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2005914529 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1356736170(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<PubNubMessaging.Core.ChannelIdentity>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4226233821_gshared (InternalEnumerator_1_t2005914529 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4226233821_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2005914529 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2005914529 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4226233821(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<PubNubMessaging.Core.ChannelIdentity>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2483800584_MetadataUsageId;
extern "C"  ChannelIdentity_t1147162267  InternalEnumerator_1_get_Current_m2483800584_gshared (InternalEnumerator_1_t2005914529 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2483800584_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ChannelIdentity_t1147162267  L_8 = ((  ChannelIdentity_t1147162267  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ChannelIdentity_t1147162267  InternalEnumerator_1_get_Current_m2483800584_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2005914529 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2005914529 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2483800584(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<PubNubMessaging.Core.CurrentRequestType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3953200560_gshared (InternalEnumerator_1_t4108980248 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3953200560_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4108980248 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4108980248 *>(__this + 1);
	InternalEnumerator_1__ctor_m3953200560(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<PubNubMessaging.Core.CurrentRequestType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m926743980_gshared (InternalEnumerator_1_t4108980248 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m926743980_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4108980248 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4108980248 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m926743980(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<PubNubMessaging.Core.CurrentRequestType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2975573170_gshared (InternalEnumerator_1_t4108980248 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3626705029((InternalEnumerator_1_t4108980248 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2975573170_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4108980248 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4108980248 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2975573170(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<PubNubMessaging.Core.CurrentRequestType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4005504469_gshared (InternalEnumerator_1_t4108980248 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4005504469_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4108980248 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4108980248 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4005504469(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<PubNubMessaging.Core.CurrentRequestType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1933234848_gshared (InternalEnumerator_1_t4108980248 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1933234848_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4108980248 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4108980248 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1933234848(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<PubNubMessaging.Core.CurrentRequestType>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3626705029_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3626705029_gshared (InternalEnumerator_1_t4108980248 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3626705029_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3626705029_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4108980248 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4108980248 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3626705029(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1866922360_gshared (InternalEnumerator_1_t3452969744 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1866922360_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	InternalEnumerator_1__ctor_m1866922360(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3840316164_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3840316164_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3840316164(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m945013892_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method)
{
	{
		ArraySegment_1_t2594217482  L_0 = InternalEnumerator_1_get_Current_m1894741129((InternalEnumerator_1_t3452969744 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ArraySegment_1_t2594217482  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m945013892_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m945013892(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m592267945_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m592267945_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	InternalEnumerator_1_Dispose_m592267945(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1460734872_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1460734872_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1460734872(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1894741129_MetadataUsageId;
extern "C"  ArraySegment_1_t2594217482  InternalEnumerator_1_get_Current_m1894741129_gshared (InternalEnumerator_1_t3452969744 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1894741129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ArraySegment_1_t2594217482  L_8 = ((  ArraySegment_1_t2594217482  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ArraySegment_1_t2594217482  InternalEnumerator_1_get_Current_m1894741129_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3452969744 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3452969744 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1894741129(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4119890600_gshared (InternalEnumerator_1_t389359684 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4119890600_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	InternalEnumerator_1__ctor_m4119890600(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	{
		bool L_0 = InternalEnumerator_1_get_Current_m1943362081((InternalEnumerator_1_t389359684 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1640363425_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1640363425_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1640363425(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1595676968_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1595676968_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1595676968(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1943362081_MetadataUsageId;
extern "C"  bool InternalEnumerator_1_get_Current_m1943362081_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1943362081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		bool L_8 = ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  bool InternalEnumerator_1_get_Current_m1943362081_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1943362081(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3043733612_gshared (InternalEnumerator_1_t246889402 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3043733612_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	InternalEnumerator_1__ctor_m3043733612(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m4154615771((InternalEnumerator_1_t246889402 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1148506519_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1148506519_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1148506519(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2651026500_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2651026500_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2651026500(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4154615771_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m4154615771_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4154615771_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m4154615771_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4154615771(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m960275522_gshared (InternalEnumerator_1_t18266304 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m960275522_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	InternalEnumerator_1__ctor_m960275522(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = InternalEnumerator_1_get_Current_m2960188445((InternalEnumerator_1_t18266304 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppChar L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m811081805_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m811081805_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	InternalEnumerator_1_Dispose_m811081805(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m412569442_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m412569442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m412569442(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2960188445_MetadataUsageId;
extern "C"  Il2CppChar InternalEnumerator_1_get_Current_m2960188445_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2960188445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Il2CppChar L_8 = ((  Il2CppChar (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Il2CppChar InternalEnumerator_1_get_Current_m2960188445_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2960188445(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m675130983_gshared (InternalEnumerator_1_t3907627660 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m675130983_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	InternalEnumerator_1__ctor_m675130983(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	{
		DictionaryEntry_t3048875398  L_0 = InternalEnumerator_1_get_Current_m2351441486((InternalEnumerator_1_t3907627660 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3597982928_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3597982928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3597982928(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1636015243_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1636015243_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1636015243(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2351441486_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  InternalEnumerator_1_get_Current_m2351441486_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2351441486_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DictionaryEntry_t3048875398  L_8 = ((  DictionaryEntry_t3048875398  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  DictionaryEntry_t3048875398  InternalEnumerator_1_get_Current_m2351441486_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2351441486(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int64>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1759383132_gshared (InternalEnumerator_1_t4238481571 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1759383132_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4238481571 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4238481571 *>(__this + 1);
	InternalEnumerator_1__ctor_m1759383132(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int64>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1446484396_gshared (InternalEnumerator_1_t4238481571 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1446484396_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4238481571 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4238481571 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1446484396(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int64>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1855157002_gshared (InternalEnumerator_1_t4238481571 * __this, const MethodInfo* method)
{
	{
		Link_t3379729309  L_0 = InternalEnumerator_1_get_Current_m1112720851((InternalEnumerator_1_t4238481571 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t3379729309  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1855157002_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4238481571 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4238481571 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1855157002(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int64>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3225871575_gshared (InternalEnumerator_1_t4238481571 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3225871575_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4238481571 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4238481571 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3225871575(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int64>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1730139156_gshared (InternalEnumerator_1_t4238481571 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1730139156_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4238481571 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4238481571 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1730139156(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int64>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1112720851_MetadataUsageId;
extern "C"  Link_t3379729309  InternalEnumerator_1_get_Current_m1112720851_gshared (InternalEnumerator_1_t4238481571 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1112720851_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t3379729309  L_8 = ((  Link_t3379729309  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Link_t3379729309  InternalEnumerator_1_get_Current_m1112720851_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4238481571 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4238481571 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1112720851(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2688327768_gshared (InternalEnumerator_1_t1723885533 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2688327768_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	InternalEnumerator_1__ctor_m2688327768(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4216238272_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4216238272_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4216238272(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3680087284_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	{
		Link_t865133271  L_0 = InternalEnumerator_1_get_Current_m1855333455((InternalEnumerator_1_t1723885533 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t865133271  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3680087284_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3680087284(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1064404287_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1064404287_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1064404287(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3585886944_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3585886944_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3585886944(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1855333455_MetadataUsageId;
extern "C"  Link_t865133271  InternalEnumerator_1_get_Current_m1855333455_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1855333455_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t865133271  L_8 = ((  Link_t865133271  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Link_t865133271  InternalEnumerator_1_get_Current_m1855333455_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1855333455(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1147704010_gshared (InternalEnumerator_1_t2240596223 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1147704010_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2240596223 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2240596223 *>(__this + 1);
	InternalEnumerator_1__ctor_m1147704010(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2081471538_gshared (InternalEnumerator_1_t2240596223 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2081471538_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2240596223 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2240596223 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2081471538(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1797999628_gshared (InternalEnumerator_1_t2240596223 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1381843961  L_0 = InternalEnumerator_1_get_Current_m3806426275((InternalEnumerator_1_t2240596223 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1381843961  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1797999628_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2240596223 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2240596223 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1797999628(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2517319879_gshared (InternalEnumerator_1_t2240596223 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2517319879_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2240596223 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2240596223 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2517319879(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m417041794_gshared (InternalEnumerator_1_t2240596223 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m417041794_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2240596223 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2240596223 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m417041794(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3806426275_MetadataUsageId;
extern "C"  KeyValuePair_2_t1381843961  InternalEnumerator_1_get_Current_m3806426275_gshared (InternalEnumerator_1_t2240596223 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3806426275_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1381843961  L_8 = ((  KeyValuePair_2_t1381843961  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t1381843961  InternalEnumerator_1_get_Current_m3806426275_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2240596223 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2240596223 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3806426275(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2687081715_gshared (InternalEnumerator_1_t3963080908 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2687081715_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3963080908 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3963080908 *>(__this + 1);
	InternalEnumerator_1__ctor_m2687081715(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2644263763_gshared (InternalEnumerator_1_t3963080908 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2644263763_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3963080908 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3963080908 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2644263763(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1416189151_gshared (InternalEnumerator_1_t3963080908 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3104328646  L_0 = InternalEnumerator_1_get_Current_m3054392282((InternalEnumerator_1_t3963080908 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3104328646  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1416189151_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3963080908 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3963080908 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1416189151(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1132968068_gshared (InternalEnumerator_1_t3963080908 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1132968068_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3963080908 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3963080908 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1132968068(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m411980503_gshared (InternalEnumerator_1_t3963080908 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m411980503_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3963080908 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3963080908 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m411980503(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3054392282_MetadataUsageId;
extern "C"  KeyValuePair_2_t3104328646  InternalEnumerator_1_get_Current_m3054392282_gshared (InternalEnumerator_1_t3963080908 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3054392282_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3104328646  L_8 = ((  KeyValuePair_2_t3104328646  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3104328646  InternalEnumerator_1_get_Current_m3054392282_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3963080908 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3963080908 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3054392282(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m971224554_gshared (InternalEnumerator_1_t3990767863 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m971224554_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	InternalEnumerator_1__ctor_m971224554(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2418489778_gshared (InternalEnumerator_1_t3990767863 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2418489778_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2418489778(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1125095150_gshared (InternalEnumerator_1_t3990767863 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3132015601  L_0 = InternalEnumerator_1_get_Current_m3903656979((InternalEnumerator_1_t3990767863 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3132015601  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1125095150_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1125095150(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2362056211_gshared (InternalEnumerator_1_t3990767863 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2362056211_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2362056211(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m68568274_gshared (InternalEnumerator_1_t3990767863 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m68568274_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m68568274(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3903656979_MetadataUsageId;
extern "C"  KeyValuePair_2_t3132015601  InternalEnumerator_1_get_Current_m3903656979_gshared (InternalEnumerator_1_t3990767863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3903656979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3132015601  L_8 = ((  KeyValuePair_2_t3132015601  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3132015601  InternalEnumerator_1_get_Current_m3903656979_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3903656979(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3441346029_gshared (InternalEnumerator_1_t313372414 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3441346029_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	InternalEnumerator_1__ctor_m3441346029(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715953809_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715953809_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715953809(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3584266157_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3749587448  L_0 = InternalEnumerator_1_get_Current_m3582710858((InternalEnumerator_1_t313372414 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3749587448  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3584266157_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3584266157(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m718416578_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m718416578_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	InternalEnumerator_1_Dispose_m718416578(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1791963761_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1791963761_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1791963761(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3582710858_MetadataUsageId;
extern "C"  KeyValuePair_2_t3749587448  InternalEnumerator_1_get_Current_m3582710858_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3582710858_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3749587448  L_8 = ((  KeyValuePair_2_t3749587448  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3749587448  InternalEnumerator_1_get_Current_m3582710858_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3582710858(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2937485256_gshared (InternalEnumerator_1_t1867125779 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2937485256_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1867125779 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1867125779 *>(__this + 1);
	InternalEnumerator_1__ctor_m2937485256(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2207498068_gshared (InternalEnumerator_1_t1867125779 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2207498068_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1867125779 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1867125779 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2207498068(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3676893172_gshared (InternalEnumerator_1_t1867125779 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1008373517  L_0 = InternalEnumerator_1_get_Current_m2831764057((InternalEnumerator_1_t1867125779 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1008373517  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3676893172_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1867125779 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1867125779 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3676893172(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m118068345_gshared (InternalEnumerator_1_t1867125779 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m118068345_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1867125779 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1867125779 *>(__this + 1);
	InternalEnumerator_1_Dispose_m118068345(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3352438664_gshared (InternalEnumerator_1_t1867125779 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3352438664_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1867125779 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1867125779 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3352438664(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2831764057_MetadataUsageId;
extern "C"  KeyValuePair_2_t1008373517  InternalEnumerator_1_get_Current_m2831764057_gshared (InternalEnumerator_1_t1867125779 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2831764057_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1008373517  L_8 = ((  KeyValuePair_2_t1008373517  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t1008373517  InternalEnumerator_1_get_Current_m2831764057_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1867125779 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1867125779 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2831764057(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
