﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Func`2<System.Object,System.Object>
struct Func_2_t2825504181;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Func`2<System.Object,System.Single>
struct Func_2_t2212564818;
// System.Func`2<UnityEngine.RuntimePlatform,System.Object>
struct Func_2_t845594077;
// System.Func`3<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Object>
struct Func_3_t916471171;
// System.Func`3<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Object>
struct Func_3_t1343333346;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t3369346583;
// System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>
struct U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>
struct U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265;
// System.Linq.Enumerable/<CreateGroupByIterator>c__Iterator5`2<System.Object,System.Object>
struct U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693;
// System.Linq.IGrouping`2<System.Object,System.Object>
struct IGrouping_2_t906937361;
// System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<System.Object,System.Object>>
struct IEnumerator_1_t2677428484;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.List`1<System.Object>>
struct Dictionary_2_t1650630555;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1902082073;
// System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>
struct U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408;
// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999;
// System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.RuntimePlatform,System.Object>
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t3981440191;
// System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator12`2<System.Object,System.Object>
struct U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136;
// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870;
// System.Linq.Grouping`2<System.Object,System.Object>
struct Grouping_2_t308796194;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_Core_System_Func_2_gen2825504181.h"
#include "System_Core_System_Func_2_gen2825504181MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "System_Core_System_Func_2_gen2212564818.h"
#include "System_Core_System_Func_2_gen2212564818MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "System_Core_System_Func_2_gen845594077.h"
#include "System_Core_System_Func_2_gen845594077MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "System_Core_System_Func_3_gen916471171.h"
#include "System_Core_System_Func_3_gen916471171MethodDeclarations.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelIden1147162267.h"
#include "System_Core_System_Func_3_gen1343333346.h"
#include "System_Core_System_Func_3_gen1343333346MethodDeclarations.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"
#include "System_Core_System_Func_3_gen3369346583.h"
#include "System_Core_System_Func_3_gen3369346583MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateCastIt1397100709.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateCastIt1397100709MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked1625106012MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateDistin3520812265.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateDistin3520812265MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g1022910149.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g1022910149MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateGroupB3689409693.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateGroupB3689409693MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1650630555.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1650630555MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable2148412300MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable2148412300.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2970655257.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2970655257MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23702943073.h"
#include "System_Core_System_Linq_Grouping_2_gen308796194.h"
#include "System_Core_System_Linq_Grouping_2_gen308796194MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23702943073MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateOfType4154598408.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateOfType4154598408MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect1666382999.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect1666382999MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect3981440191.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelect3981440191MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelectM288472136.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateSelectM288472136MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3117631226.h"
#include "System_Core_System_Func_2_gen3117631226MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI4121738870.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI4121738870MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3961629604.h"
#include "System_Core_System_Func_2_gen3961629604MethodDeclarations.h"

// System.Collections.Generic.List`1<!!1> System.Linq.Enumerable::ContainsGroup<System.Object,System.Object>(System.Collections.Generic.Dictionary`2<!!0,System.Collections.Generic.List`1<!!1>>,!!0,System.Collections.Generic.IEqualityComparer`1<!!0>)
extern "C"  List_1_t2058570427 * Enumerable_ContainsGroup_TisIl2CppObject_TisIl2CppObject_m610282755_gshared (Il2CppObject * __this /* static, unused */, Dictionary_2_t1650630555 * p0, Il2CppObject * p1, Il2CppObject* p2, const MethodInfo* method);
#define Enumerable_ContainsGroup_TisIl2CppObject_TisIl2CppObject_m610282755(__this /* static, unused */, p0, p1, p2, method) ((  List_1_t2058570427 * (*) (Il2CppObject * /* static, unused */, Dictionary_2_t1650630555 *, Il2CppObject *, Il2CppObject*, const MethodInfo*))Enumerable_ContainsGroup_TisIl2CppObject_TisIl2CppObject_m610282755_gshared)(__this /* static, unused */, p0, p1, p2, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m3134197304_gshared (Func_2_t2825504181 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Object,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Func_2_Invoke_m3288232740_gshared (Func_2_t2825504181 * __this, Il2CppObject * ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m3288232740((Func_2_t2825504181 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m4034295761_gshared (Func_2_t2825504181 * __this, Il2CppObject * ___arg10, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg10;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_2_EndInvoke_m1674435418_gshared (Func_2_t2825504181 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`2<System.Object,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1874497973_gshared (Func_2_t2212564818 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Object,System.Single>::Invoke(T)
extern "C"  float Func_2_Invoke_m1144286175_gshared (Func_2_t2212564818 * __this, Il2CppObject * ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m1144286175((Func_2_t2212564818 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef float (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Object,System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m669892004_gshared (Func_2_t2212564818 * __this, Il2CppObject * ___arg10, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg10;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Object,System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float Func_2_EndInvoke_m971580865_gshared (Func_2_t2212564818 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Func`2<UnityEngine.RuntimePlatform,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1146193114_gshared (Func_2_t845594077 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<UnityEngine.RuntimePlatform,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Func_2_Invoke_m2089686788_gshared (Func_2_t845594077 * __this, int32_t ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m2089686788((Func_2_t845594077 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<UnityEngine.RuntimePlatform,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* RuntimePlatform_t1869584967_il2cpp_TypeInfo_var;
extern const uint32_t Func_2_BeginInvoke_m4117396615_MetadataUsageId;
extern "C"  Il2CppObject * Func_2_BeginInvoke_m4117396615_gshared (Func_2_t845594077 * __this, int32_t ___arg10, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Func_2_BeginInvoke_m4117396615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RuntimePlatform_t1869584967_il2cpp_TypeInfo_var, &___arg10);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<UnityEngine.RuntimePlatform,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_2_EndInvoke_m1795508590_gshared (Func_2_t845594077 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`3<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m2548880321_gshared (Func_3_t916471171 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * Func_3_Invoke_m3051459976_gshared (Func_3_t916471171 * __this, ChannelIdentity_t1147162267  ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m3051459976((Func_3_t916471171 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, ChannelIdentity_t1147162267  ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, ChannelIdentity_t1147162267  ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* ChannelIdentity_t1147162267_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m380852609_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m380852609_gshared (Func_3_t916471171 * __this, ChannelIdentity_t1147162267  ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m380852609_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(ChannelIdentity_t1147162267_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_3_EndInvoke_m2054238817_gshared (Func_3_t916471171 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`3<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m690493764_gshared (Func_3_t1343333346 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * Func_3_Invoke_m3614701809_gshared (Func_3_t1343333346 * __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m3614701809((Func_3_t1343333346 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* CurrentRequestType_t3250227986_il2cpp_TypeInfo_var;
extern const uint32_t Func_3_BeginInvoke_m3916031316_MetadataUsageId;
extern "C"  Il2CppObject * Func_3_BeginInvoke_m3916031316_gshared (Func_3_t1343333346 * __this, int32_t ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Func_3_BeginInvoke_m3916031316_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(CurrentRequestType_t3250227986_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_3_EndInvoke_m2926965078_gshared (Func_3_t1343333346 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Func`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m3917225629_gshared (Func_3_t3369346583 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`3<System.Object,System.Object,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * Func_3_Invoke_m1588676556_gshared (Func_3_t3369346583 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_3_Invoke_m1588676556((Func_3_t3369346583 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`3<System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_3_BeginInvoke_m3007306193_gshared (Func_3_t3369346583 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TResult System.Func`3<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Func_3_EndInvoke_m917241105_gshared (Func_3_t3369346583 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::.ctor()
extern "C"  void U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m3321498162_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m2659351773_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m2940047778_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m3746809729_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m3785434980_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * __this, const MethodInfo* method)
{
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * L_2 = (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 *)L_2;
		U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * L_3 = V_0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CU24U3Esource_5();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m3444743414_MetadataUsageId;
extern "C"  bool U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m3444743414_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m3444743414_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	bool V_3 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00af;
	}

IL_0023:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_source_0();
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_U3CU24s_49U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0078;
			}
		}

IL_0043:
		{
			goto IL_0078;
		}

IL_0048:
		{
			Il2CppObject * L_5 = (Il2CppObject *)__this->get_U3CU24s_49U3E__0_1();
			NullCheck((Il2CppObject *)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			__this->set_U3CelementU3E__1_2(((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))));
			Il2CppObject * L_7 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			__this->set_U24current_4(L_7);
			__this->set_U24PC_3(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB1, FINALLY_008d);
		}

IL_0078:
		{
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CU24s_49U3E__0_1();
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_0048;
			}
		}

IL_0088:
		{
			IL2CPP_LEAVE(0xA8, FINALLY_008d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_008d;
	}

FINALLY_008d:
	{ // begin finally (depth: 1)
		{
			bool L_10 = V_1;
			if (!L_10)
			{
				goto IL_0091;
			}
		}

IL_0090:
		{
			IL2CPP_END_FINALLY(141)
		}

IL_0091:
		{
			Il2CppObject * L_11 = (Il2CppObject *)__this->get_U3CU24s_49U3E__0_1();
			V_2 = (Il2CppObject *)((Il2CppObject *)IsInst(L_11, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_12 = V_2;
			if (L_12)
			{
				goto IL_00a1;
			}
		}

IL_00a0:
		{
			IL2CPP_END_FINALLY(141)
		}

IL_00a1:
		{
			Il2CppObject * L_13 = V_2;
			NullCheck((Il2CppObject *)L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_13);
			IL2CPP_END_FINALLY(141)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(141)
	{
		IL2CPP_JUMP_TBL(0xB1, IL_00b1)
		IL2CPP_JUMP_TBL(0xA8, IL_00a8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00a8:
	{
		__this->set_U24PC_3((-1));
	}

IL_00af:
	{
		return (bool)0;
	}

IL_00b1:
	{
		return (bool)1;
	}
	// Dead block : IL_00b3: ldloc.3
}
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m3972648411_MetadataUsageId;
extern "C"  void U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m3972648411_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m3972648411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003d;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003d;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3D, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_2 = (Il2CppObject *)__this->get_U3CU24s_49U3E__0_1();
			V_1 = (Il2CppObject *)((Il2CppObject *)IsInst(L_2, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_3 = V_1;
			if (L_3)
			{
				goto IL_0036;
			}
		}

IL_0035:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_0036:
		{
			Il2CppObject * L_4 = V_1;
			NullCheck((Il2CppObject *)L_4);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003d:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateCastIteratorU3Ec__Iterator0_1_Reset_m2250535601_MetadataUsageId;
extern "C"  void U3CCreateCastIteratorU3Ec__Iterator0_1_Reset_m2250535601_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t1397100709 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateCastIteratorU3Ec__Iterator0_1_Reset_m2250535601_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::.ctor()
extern "C"  void U3CCreateDistinctIteratorU3Ec__Iterator3_1__ctor_m1619070986_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateDistinctIteratorU3Ec__Iterator3_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m4167922749_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_6();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateDistinctIteratorU3Ec__Iterator3_1_System_Collections_IEnumerator_get_Current_m2592365566_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_6();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateDistinctIteratorU3Ec__Iterator3_1_System_Collections_IEnumerable_GetEnumerator_m4009283937_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateDistinctIteratorU3Ec__Iterator3_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m193659212_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method)
{
	U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_5();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * L_2 = (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 *)L_2;
		U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Ecomparer_7();
		NullCheck(L_3);
		L_3->set_comparer_0(L_4);
		U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * L_5 = V_0;
		Il2CppObject* L_6 = (Il2CppObject*)__this->get_U3CU24U3Esource_8();
		NullCheck(L_5);
		L_5->set_source_2(L_6);
		U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateDistinctIteratorU3Ec__Iterator3_1_MoveNext_m270250114_MetadataUsageId;
extern "C"  bool U3CCreateDistinctIteratorU3Ec__Iterator3_1_MoveNext_m270250114_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateDistinctIteratorU3Ec__Iterator3_1_MoveNext_m270250114_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0048;
		}
	}
	{
		goto IL_00e1;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_comparer_0();
		HashSet_1_t1022910149 * L_3 = (HashSet_1_t1022910149 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (HashSet_1_t1022910149 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_3, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		__this->set_U3CitemsU3E__0_1(L_3);
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_source_2();
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject* L_5 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), (Il2CppObject*)L_4);
		__this->set_U3CU24s_55U3E__1_3(L_5);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0048:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_6 = V_0;
			if (((int32_t)((int32_t)L_6-(int32_t)1)) == 0)
			{
				goto IL_00ac;
			}
		}

IL_0054:
		{
			goto IL_00ac;
		}

IL_0059:
		{
			Il2CppObject* L_7 = (Il2CppObject*)__this->get_U3CU24s_55U3E__1_3();
			NullCheck((Il2CppObject*)L_7);
			Il2CppObject * L_8 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), (Il2CppObject*)L_7);
			__this->set_U3CelementU3E__2_4(L_8);
			HashSet_1_t1022910149 * L_9 = (HashSet_1_t1022910149 *)__this->get_U3CitemsU3E__0_1();
			Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CelementU3E__2_4();
			NullCheck((HashSet_1_t1022910149 *)L_9);
			bool L_11 = ((  bool (*) (HashSet_1_t1022910149 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((HashSet_1_t1022910149 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
			if (L_11)
			{
				goto IL_00ac;
			}
		}

IL_0080:
		{
			HashSet_1_t1022910149 * L_12 = (HashSet_1_t1022910149 *)__this->get_U3CitemsU3E__0_1();
			Il2CppObject * L_13 = (Il2CppObject *)__this->get_U3CelementU3E__2_4();
			NullCheck((HashSet_1_t1022910149 *)L_12);
			((  bool (*) (HashSet_1_t1022910149 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((HashSet_1_t1022910149 *)L_12, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			Il2CppObject * L_14 = (Il2CppObject *)__this->get_U3CelementU3E__2_4();
			__this->set_U24current_6(L_14);
			__this->set_U24PC_5(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xE3, FINALLY_00c1);
		}

IL_00ac:
		{
			Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_55U3E__1_3();
			NullCheck((Il2CppObject *)L_15);
			bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			if (L_16)
			{
				goto IL_0059;
			}
		}

IL_00bc:
		{
			IL2CPP_LEAVE(0xDA, FINALLY_00c1);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00c1;
	}

FINALLY_00c1:
	{ // begin finally (depth: 1)
		{
			bool L_17 = V_1;
			if (!L_17)
			{
				goto IL_00c5;
			}
		}

IL_00c4:
		{
			IL2CPP_END_FINALLY(193)
		}

IL_00c5:
		{
			Il2CppObject* L_18 = (Il2CppObject*)__this->get_U3CU24s_55U3E__1_3();
			if (L_18)
			{
				goto IL_00ce;
			}
		}

IL_00cd:
		{
			IL2CPP_END_FINALLY(193)
		}

IL_00ce:
		{
			Il2CppObject* L_19 = (Il2CppObject*)__this->get_U3CU24s_55U3E__1_3();
			NullCheck((Il2CppObject *)L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_19);
			IL2CPP_END_FINALLY(193)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(193)
	{
		IL2CPP_JUMP_TBL(0xE3, IL_00e3)
		IL2CPP_JUMP_TBL(0xDA, IL_00da)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00da:
	{
		__this->set_U24PC_5((-1));
	}

IL_00e1:
	{
		return (bool)0;
	}

IL_00e3:
	{
		return (bool)1;
	}
	// Dead block : IL_00e5: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateDistinctIteratorU3Ec__Iterator3_1_Dispose_m2370563095_MetadataUsageId;
extern "C"  void U3CCreateDistinctIteratorU3Ec__Iterator3_1_Dispose_m2370563095_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateDistinctIteratorU3Ec__Iterator3_1_Dispose_m2370563095_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_55U3E__1_3();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_55U3E__1_3();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateDistinctIterator>c__Iterator3`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateDistinctIteratorU3Ec__Iterator3_1_Reset_m3955730001_MetadataUsageId;
extern "C"  void U3CCreateDistinctIteratorU3Ec__Iterator3_1_Reset_m3955730001_gshared (U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3520812265 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateDistinctIteratorU3Ec__Iterator3_1_Reset_m3955730001_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateGroupByIterator>c__Iterator5`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CCreateGroupByIteratorU3Ec__Iterator5_2__ctor_m4025420589_gshared (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Linq.IGrouping`2<TKey,TSource> System.Linq.Enumerable/<CreateGroupByIterator>c__Iterator5`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<System.Linq.IGrouping<TKey,TSource>>.get_Current()
extern "C"  Il2CppObject* U3CCreateGroupByIteratorU3Ec__Iterator5_2_System_Collections_Generic_IEnumeratorU3CSystem_Linq_IGroupingU3CTKeyU2CTSourceU3EU3E_get_Current_m850969262_gshared (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_U24current_14();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateGroupByIterator>c__Iterator5`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateGroupByIteratorU3Ec__Iterator5_2_System_Collections_IEnumerator_get_Current_m747718943_gshared (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_U24current_14();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateGroupByIterator>c__Iterator5`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateGroupByIteratorU3Ec__Iterator5_2_System_Collections_IEnumerable_GetEnumerator_m1486229824_gshared (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TSource>> System.Linq.Enumerable/<CreateGroupByIterator>c__Iterator5`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<System.Linq.IGrouping<TKey,TSource>>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateGroupByIteratorU3Ec__Iterator5_2_System_Collections_Generic_IEnumerableU3CSystem_Linq_IGroupingU3CTKeyU2CTSourceU3EU3E_GetEnumerator_m750292983_gshared (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * __this, const MethodInfo* method)
{
	U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_13();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * L_2 = (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1));
		((  void (*) (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 *)L_2;
		U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_15();
		NullCheck(L_3);
		L_3->set_source_4(L_4);
		U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * L_5 = V_0;
		Func_2_t2825504181 * L_6 = (Func_2_t2825504181 *)__this->get_U3CU24U3EkeySelector_16();
		NullCheck(L_5);
		L_5->set_keySelector_7(L_6);
		U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * L_7 = V_0;
		Il2CppObject* L_8 = (Il2CppObject*)__this->get_U3CU24U3Ecomparer_17();
		NullCheck(L_7);
		L_7->set_comparer_9(L_8);
		U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * L_9 = V_0;
		return L_9;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateGroupByIterator>c__Iterator5`2<System.Object,System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateGroupByIteratorU3Ec__Iterator5_2_MoveNext_m2399691539_MetadataUsageId;
extern "C"  bool U3CCreateGroupByIteratorU3Ec__Iterator5_2_MoveNext_m2399691539_gshared (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateGroupByIteratorU3Ec__Iterator5_2_MoveNext_m2399691539_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	bool V_4 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_13();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_13((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002b;
		}
		if (L_1 == 1)
		{
			goto IL_0187;
		}
		if (L_1 == 2)
		{
			goto IL_0187;
		}
		if (L_1 == 3)
		{
			goto IL_0292;
		}
	}
	{
		goto IL_02a7;
	}

IL_002b:
	{
		Dictionary_2_t1650630555 * L_2 = (Dictionary_2_t1650630555 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (Dictionary_2_t1650630555 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		__this->set_U3CgroupsU3E__0_0(L_2);
		List_1_t2058570427 * L_3 = (List_1_t2058570427 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
		((  void (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		__this->set_U3CnullListU3E__1_1(L_3);
		__this->set_U3CcounterU3E__2_2(0);
		__this->set_U3CnullCounterU3E__3_3((-1));
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_source_4();
		NullCheck((Il2CppObject*)L_4);
		Il2CppObject* L_5 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), (Il2CppObject*)L_4);
		__this->set_U3CU24s_61U3E__4_5(L_5);
	}

IL_0060:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0142;
		}

IL_0065:
		{
			Il2CppObject* L_6 = (Il2CppObject*)__this->get_U3CU24s_61U3E__4_5();
			NullCheck((Il2CppObject*)L_6);
			Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (Il2CppObject*)L_6);
			__this->set_U3CelementU3E__5_6(L_7);
			Func_2_t2825504181 * L_8 = (Func_2_t2825504181 *)__this->get_keySelector_7();
			Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3CelementU3E__5_6();
			NullCheck((Func_2_t2825504181 *)L_8);
			Il2CppObject * L_10 = ((  Il2CppObject * (*) (Func_2_t2825504181 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Func_2_t2825504181 *)L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			__this->set_U3CkeyU3E__6_8(L_10);
			Il2CppObject * L_11 = (Il2CppObject *)__this->get_U3CkeyU3E__6_8();
			if (L_11)
			{
				goto IL_00d9;
			}
		}

IL_009d:
		{
			List_1_t2058570427 * L_12 = (List_1_t2058570427 *)__this->get_U3CnullListU3E__1_1();
			Il2CppObject * L_13 = (Il2CppObject *)__this->get_U3CelementU3E__5_6();
			NullCheck((List_1_t2058570427 *)L_12);
			((  void (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t2058570427 *)L_12, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
			int32_t L_14 = (int32_t)__this->get_U3CnullCounterU3E__3_3();
			if ((!(((uint32_t)L_14) == ((uint32_t)(-1)))))
			{
				goto IL_00d4;
			}
		}

IL_00ba:
		{
			int32_t L_15 = (int32_t)__this->get_U3CcounterU3E__2_2();
			__this->set_U3CnullCounterU3E__3_3(L_15);
			int32_t L_16 = (int32_t)__this->get_U3CcounterU3E__2_2();
			__this->set_U3CcounterU3E__2_2(((int32_t)((int32_t)L_16+(int32_t)1)));
		}

IL_00d4:
		{
			goto IL_0142;
		}

IL_00d9:
		{
			Dictionary_2_t1650630555 * L_17 = (Dictionary_2_t1650630555 *)__this->get_U3CgroupsU3E__0_0();
			Il2CppObject * L_18 = (Il2CppObject *)__this->get_U3CkeyU3E__6_8();
			Il2CppObject* L_19 = (Il2CppObject*)__this->get_comparer_9();
			List_1_t2058570427 * L_20 = ((  List_1_t2058570427 * (*) (Il2CppObject * /* static, unused */, Dictionary_2_t1650630555 *, Il2CppObject *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)(NULL /*static, unused*/, (Dictionary_2_t1650630555 *)L_17, (Il2CppObject *)L_18, (Il2CppObject*)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
			__this->set_U3CgroupU3E__7_10(L_20);
			List_1_t2058570427 * L_21 = (List_1_t2058570427 *)__this->get_U3CgroupU3E__7_10();
			if (L_21)
			{
				goto IL_0131;
			}
		}

IL_0101:
		{
			List_1_t2058570427 * L_22 = (List_1_t2058570427 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
			((  void (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			__this->set_U3CgroupU3E__7_10(L_22);
			Dictionary_2_t1650630555 * L_23 = (Dictionary_2_t1650630555 *)__this->get_U3CgroupsU3E__0_0();
			Il2CppObject * L_24 = (Il2CppObject *)__this->get_U3CkeyU3E__6_8();
			List_1_t2058570427 * L_25 = (List_1_t2058570427 *)__this->get_U3CgroupU3E__7_10();
			NullCheck((Dictionary_2_t1650630555 *)L_23);
			((  void (*) (Dictionary_2_t1650630555 *, Il2CppObject *, List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((Dictionary_2_t1650630555 *)L_23, (Il2CppObject *)L_24, (List_1_t2058570427 *)L_25, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
			int32_t L_26 = (int32_t)__this->get_U3CcounterU3E__2_2();
			__this->set_U3CcounterU3E__2_2(((int32_t)((int32_t)L_26+(int32_t)1)));
		}

IL_0131:
		{
			List_1_t2058570427 * L_27 = (List_1_t2058570427 *)__this->get_U3CgroupU3E__7_10();
			Il2CppObject * L_28 = (Il2CppObject *)__this->get_U3CelementU3E__5_6();
			NullCheck((List_1_t2058570427 *)L_27);
			((  void (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t2058570427 *)L_27, (Il2CppObject *)L_28, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		}

IL_0142:
		{
			Il2CppObject* L_29 = (Il2CppObject*)__this->get_U3CU24s_61U3E__4_5();
			NullCheck((Il2CppObject *)L_29);
			bool L_30 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_29);
			if (L_30)
			{
				goto IL_0065;
			}
		}

IL_0152:
		{
			IL2CPP_LEAVE(0x16C, FINALLY_0157);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0157;
	}

FINALLY_0157:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_31 = (Il2CppObject*)__this->get_U3CU24s_61U3E__4_5();
			if (L_31)
			{
				goto IL_0160;
			}
		}

IL_015f:
		{
			IL2CPP_END_FINALLY(343)
		}

IL_0160:
		{
			Il2CppObject* L_32 = (Il2CppObject*)__this->get_U3CU24s_61U3E__4_5();
			NullCheck((Il2CppObject *)L_32);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_32);
			IL2CPP_END_FINALLY(343)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(343)
	{
		IL2CPP_JUMP_TBL(0x16C, IL_016c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_016c:
	{
		__this->set_U3CcounterU3E__2_2(0);
		Dictionary_2_t1650630555 * L_33 = (Dictionary_2_t1650630555 *)__this->get_U3CgroupsU3E__0_0();
		NullCheck((Dictionary_2_t1650630555 *)L_33);
		Enumerator_t2970655257  L_34 = ((  Enumerator_t2970655257  (*) (Dictionary_2_t1650630555 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((Dictionary_2_t1650630555 *)L_33, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		__this->set_U3CU24s_62U3E__8_11(L_34);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0187:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_35 = V_0;
			if (((int32_t)((int32_t)L_35-(int32_t)1)) == 0)
			{
				goto IL_01e6;
			}
			if (((int32_t)((int32_t)L_35-(int32_t)1)) == 1)
			{
				goto IL_0223;
			}
		}

IL_0197:
		{
			goto IL_0231;
		}

IL_019c:
		{
			Enumerator_t2970655257 * L_36 = (Enumerator_t2970655257 *)__this->get_address_of_U3CU24s_62U3E__8_11();
			KeyValuePair_2_t3702943074  L_37 = Enumerator_get_Current_m1443704642((Enumerator_t2970655257 *)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
			__this->set_U3CgroupU3E__9_12(L_37);
			int32_t L_38 = (int32_t)__this->get_U3CcounterU3E__2_2();
			int32_t L_39 = (int32_t)__this->get_U3CnullCounterU3E__3_3();
			if ((!(((uint32_t)L_38) == ((uint32_t)L_39))))
			{
				goto IL_01f4;
			}
		}

IL_01be:
		{
			Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
			Il2CppObject * L_40 = V_2;
			List_1_t2058570427 * L_41 = (List_1_t2058570427 *)__this->get_U3CnullListU3E__1_1();
			Grouping_2_t308796194 * L_42 = (Grouping_2_t308796194 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16));
			((  void (*) (Grouping_2_t308796194 *, Il2CppObject *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)(L_42, (Il2CppObject *)L_40, (Il2CppObject*)L_41, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
			__this->set_U24current_14(L_42);
			__this->set_U24PC_13(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x2A9, FINALLY_0246);
		}

IL_01e6:
		{
			int32_t L_43 = (int32_t)__this->get_U3CcounterU3E__2_2();
			__this->set_U3CcounterU3E__2_2(((int32_t)((int32_t)L_43+(int32_t)1)));
		}

IL_01f4:
		{
			KeyValuePair_2_t3702943074 * L_44 = (KeyValuePair_2_t3702943074 *)__this->get_address_of_U3CgroupU3E__9_12();
			Il2CppObject * L_45 = KeyValuePair_2_get_Key_m185780481((KeyValuePair_2_t3702943074 *)L_44, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
			KeyValuePair_2_t3702943074 * L_46 = (KeyValuePair_2_t3702943074 *)__this->get_address_of_U3CgroupU3E__9_12();
			List_1_t2058570427 * L_47 = KeyValuePair_2_get_Value_m4024827393((KeyValuePair_2_t3702943074 *)L_46, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
			Grouping_2_t308796194 * L_48 = (Grouping_2_t308796194 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16));
			((  void (*) (Grouping_2_t308796194 *, Il2CppObject *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)(L_48, (Il2CppObject *)L_45, (Il2CppObject*)L_47, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
			__this->set_U24current_14(L_48);
			__this->set_U24PC_13(2);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x2A9, FINALLY_0246);
		}

IL_0223:
		{
			int32_t L_49 = (int32_t)__this->get_U3CcounterU3E__2_2();
			__this->set_U3CcounterU3E__2_2(((int32_t)((int32_t)L_49+(int32_t)1)));
		}

IL_0231:
		{
			Enumerator_t2970655257 * L_50 = (Enumerator_t2970655257 *)__this->get_address_of_U3CU24s_62U3E__8_11();
			bool L_51 = Enumerator_MoveNext_m1991620226((Enumerator_t2970655257 *)L_50, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
			if (L_51)
			{
				goto IL_019c;
			}
		}

IL_0241:
		{
			IL2CPP_LEAVE(0x25B, FINALLY_0246);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0246;
	}

FINALLY_0246:
	{ // begin finally (depth: 1)
		{
			bool L_52 = V_1;
			if (!L_52)
			{
				goto IL_024a;
			}
		}

IL_0249:
		{
			IL2CPP_END_FINALLY(582)
		}

IL_024a:
		{
			Enumerator_t2970655257  L_53 = (Enumerator_t2970655257 )__this->get_U3CU24s_62U3E__8_11();
			Enumerator_t2970655257  L_54 = L_53;
			Il2CppObject * L_55 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), &L_54);
			NullCheck((Il2CppObject *)L_55);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_55);
			IL2CPP_END_FINALLY(582)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(582)
	{
		IL2CPP_JUMP_TBL(0x2A9, IL_02a9)
		IL2CPP_JUMP_TBL(0x25B, IL_025b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_025b:
	{
		int32_t L_56 = (int32_t)__this->get_U3CcounterU3E__2_2();
		int32_t L_57 = (int32_t)__this->get_U3CnullCounterU3E__3_3();
		if ((!(((uint32_t)L_56) == ((uint32_t)L_57))))
		{
			goto IL_02a0;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_58 = V_3;
		List_1_t2058570427 * L_59 = (List_1_t2058570427 *)__this->get_U3CnullListU3E__1_1();
		Grouping_2_t308796194 * L_60 = (Grouping_2_t308796194 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 16));
		((  void (*) (Grouping_2_t308796194 *, Il2CppObject *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)(L_60, (Il2CppObject *)L_58, (Il2CppObject*)L_59, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		__this->set_U24current_14(L_60);
		__this->set_U24PC_13(3);
		goto IL_02a9;
	}

IL_0292:
	{
		int32_t L_61 = (int32_t)__this->get_U3CcounterU3E__2_2();
		__this->set_U3CcounterU3E__2_2(((int32_t)((int32_t)L_61+(int32_t)1)));
	}

IL_02a0:
	{
		__this->set_U24PC_13((-1));
	}

IL_02a7:
	{
		return (bool)0;
	}

IL_02a9:
	{
		return (bool)1;
	}
	// Dead block : IL_02ab: ldloc.s V_4
}
// System.Void System.Linq.Enumerable/<CreateGroupByIterator>c__Iterator5`2<System.Object,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateGroupByIteratorU3Ec__Iterator5_2_Dispose_m3365483304_MetadataUsageId;
extern "C"  void U3CCreateGroupByIteratorU3Ec__Iterator5_2_Dispose_m3365483304_gshared (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateGroupByIteratorU3Ec__Iterator5_2_Dispose_m3365483304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_13();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_13((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003f;
		}
		if (L_1 == 1)
		{
			goto IL_0029;
		}
		if (L_1 == 2)
		{
			goto IL_0029;
		}
		if (L_1 == 3)
		{
			goto IL_003f;
		}
	}
	{
		goto IL_003f;
	}

IL_0029:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3F, FINALLY_002e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002e;
	}

FINALLY_002e:
	{ // begin finally (depth: 1)
		Enumerator_t2970655257  L_2 = (Enumerator_t2970655257 )__this->get_U3CU24s_62U3E__8_11();
		Enumerator_t2970655257  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), &L_3);
		NullCheck((Il2CppObject *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
		IL2CPP_END_FINALLY(46)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(46)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003f:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateGroupByIterator>c__Iterator5`2<System.Object,System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateGroupByIteratorU3Ec__Iterator5_2_Reset_m318413546_MetadataUsageId;
extern "C"  void U3CCreateGroupByIteratorU3Ec__Iterator5_2_Reset_m318413546_gshared (U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3689409693 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateGroupByIteratorU3Ec__Iterator5_2_Reset_m318413546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::.ctor()
extern "C"  void U3CCreateOfTypeIteratorU3Ec__IteratorC_1__ctor_m1442118915_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateOfTypeIteratorU3Ec__IteratorC_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m2657146748_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateOfTypeIteratorU3Ec__IteratorC_1_System_Collections_IEnumerator_get_Current_m2253659481_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateOfTypeIteratorU3Ec__IteratorC_1_System_Collections_IEnumerable_GetEnumerator_m3988951508_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateOfTypeIteratorU3Ec__IteratorC_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m2665668891_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * L_2 = (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 *)L_2;
		U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * L_3 = V_0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CU24U3Esource_5();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateOfTypeIteratorU3Ec__IteratorC_1_MoveNext_m2883054397_MetadataUsageId;
extern "C"  bool U3CCreateOfTypeIteratorU3Ec__IteratorC_1_MoveNext_m2883054397_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_MoveNext_m2883054397_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	bool V_3 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00bf;
	}

IL_0023:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_source_0();
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_U3CU24s_80U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0088;
			}
		}

IL_0043:
		{
			goto IL_0088;
		}

IL_0048:
		{
			Il2CppObject * L_5 = (Il2CppObject *)__this->get_U3CU24s_80U3E__0_1();
			NullCheck((Il2CppObject *)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Il2CppObject * L_7 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			if (!((Il2CppObject *)IsInst(L_7, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))
			{
				goto IL_0088;
			}
		}

IL_0069:
		{
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			__this->set_U24current_4(((Il2CppObject *)Castclass(L_8, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))));
			__this->set_U24PC_3(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC1, FINALLY_009d);
		}

IL_0088:
		{
			Il2CppObject * L_9 = (Il2CppObject *)__this->get_U3CU24s_80U3E__0_1();
			NullCheck((Il2CppObject *)L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_9);
			if (L_10)
			{
				goto IL_0048;
			}
		}

IL_0098:
		{
			IL2CPP_LEAVE(0xB8, FINALLY_009d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_009d;
	}

FINALLY_009d:
	{ // begin finally (depth: 1)
		{
			bool L_11 = V_1;
			if (!L_11)
			{
				goto IL_00a1;
			}
		}

IL_00a0:
		{
			IL2CPP_END_FINALLY(157)
		}

IL_00a1:
		{
			Il2CppObject * L_12 = (Il2CppObject *)__this->get_U3CU24s_80U3E__0_1();
			V_2 = (Il2CppObject *)((Il2CppObject *)IsInst(L_12, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_13 = V_2;
			if (L_13)
			{
				goto IL_00b1;
			}
		}

IL_00b0:
		{
			IL2CPP_END_FINALLY(157)
		}

IL_00b1:
		{
			Il2CppObject * L_14 = V_2;
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(157)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(157)
	{
		IL2CPP_JUMP_TBL(0xC1, IL_00c1)
		IL2CPP_JUMP_TBL(0xB8, IL_00b8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b8:
	{
		__this->set_U24PC_3((-1));
	}

IL_00bf:
	{
		return (bool)0;
	}

IL_00c1:
	{
		return (bool)1;
	}
	// Dead block : IL_00c3: ldloc.3
}
// System.Void System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateOfTypeIteratorU3Ec__IteratorC_1_Dispose_m2751205856_MetadataUsageId;
extern "C"  void U3CCreateOfTypeIteratorU3Ec__IteratorC_1_Dispose_m2751205856_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_Dispose_m2751205856_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003d;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003d;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3D, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_2 = (Il2CppObject *)__this->get_U3CU24s_80U3E__0_1();
			V_1 = (Il2CppObject *)((Il2CppObject *)IsInst(L_2, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_3 = V_1;
			if (L_3)
			{
				goto IL_0036;
			}
		}

IL_0035:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_0036:
		{
			Il2CppObject * L_4 = V_1;
			NullCheck((Il2CppObject *)L_4);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003d:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateOfTypeIterator>c__IteratorC`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateOfTypeIteratorU3Ec__IteratorC_1_Reset_m2518766250_MetadataUsageId;
extern "C"  void U3CCreateOfTypeIteratorU3Ec__IteratorC_1_Reset_m2518766250_gshared (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t4154598408 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateOfTypeIteratorU3Ec__IteratorC_1_Reset_m2518766250_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m2480296441_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m1090887326_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m2800698283_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m291907278_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m3708752605_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * L_2 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 *)L_2;
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * L_5 = V_0;
		Func_2_t2825504181 * L_6 = (Func_2_t2825504181 *)__this->get_U3CU24U3Eselector_7();
		NullCheck(L_5);
		L_5->set_selector_3(L_6);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3623917119_MetadataUsageId;
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3623917119_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m3623917119_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00b3;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_81U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_007e;
			}
		}

IL_0043:
		{
			goto IL_007e;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t2825504181 * L_7 = (Func_2_t2825504181 *)__this->get_selector_3();
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t2825504181 *)L_7);
			Il2CppObject * L_9 = ((  Il2CppObject * (*) (Func_2_t2825504181 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t2825504181 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			__this->set_U24current_5(L_9);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB5, FINALLY_0093);
		}

IL_007e:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (L_11)
			{
				goto IL_0048;
			}
		}

IL_008e:
		{
			IL2CPP_LEAVE(0xAC, FINALLY_0093);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0093;
	}

FINALLY_0093:
	{ // begin finally (depth: 1)
		{
			bool L_12 = V_1;
			if (!L_12)
			{
				goto IL_0097;
			}
		}

IL_0096:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_0097:
		{
			Il2CppObject* L_13 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			if (L_13)
			{
				goto IL_00a0;
			}
		}

IL_009f:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_00a0:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(147)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(147)
	{
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_JUMP_TBL(0xAC, IL_00ac)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ac:
	{
		__this->set_U24PC_4((-1));
	}

IL_00b3:
	{
		return (bool)0;
	}

IL_00b5:
	{
		return (bool)1;
	}
	// Dead block : IL_00b7: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1234776362_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1234776362_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1234776362_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<System.Object,System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m4058120828_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m4058120828_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t1666382999 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m4058120828_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.RuntimePlatform,System.Object>::.ctor()
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2__ctor_m453822843_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3981440191 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.RuntimePlatform,System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m1799289190_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3981440191 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.RuntimePlatform,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerator_get_Current_m3428819033_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3981440191 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.RuntimePlatform,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_IEnumerable_GetEnumerator_m2578950478_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3981440191 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateSelectIteratorU3Ec__Iterator10_2_t3981440191 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3981440191 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateSelectIteratorU3Ec__Iterator10_2_t3981440191 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.RuntimePlatform,System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectIteratorU3Ec__Iterator10_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m2396326663_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3981440191 * __this, const MethodInfo* method)
{
	U3CCreateSelectIteratorU3Ec__Iterator10_2_t3981440191 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t3981440191 * L_2 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3981440191 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3981440191 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3981440191 *)L_2;
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t3981440191 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t3981440191 * L_5 = V_0;
		Func_2_t845594077 * L_6 = (Func_2_t845594077 *)__this->get_U3CU24U3Eselector_7();
		NullCheck(L_5);
		L_5->set_selector_3(L_6);
		U3CCreateSelectIteratorU3Ec__Iterator10_2_t3981440191 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.RuntimePlatform,System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m1120943561_MetadataUsageId;
extern "C"  bool U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m1120943561_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3981440191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_MoveNext_m1120943561_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00b3;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.RuntimePlatform>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_81U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_007e;
			}
		}

IL_0043:
		{
			goto IL_007e;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.RuntimePlatform>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t845594077 * L_7 = (Func_2_t845594077 *)__this->get_selector_3();
			int32_t L_8 = (int32_t)__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t845594077 *)L_7);
			Il2CppObject * L_9 = ((  Il2CppObject * (*) (Func_2_t845594077 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t845594077 *)L_7, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			__this->set_U24current_5(L_9);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB5, FINALLY_0093);
		}

IL_007e:
		{
			Il2CppObject* L_10 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (L_11)
			{
				goto IL_0048;
			}
		}

IL_008e:
		{
			IL2CPP_LEAVE(0xAC, FINALLY_0093);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0093;
	}

FINALLY_0093:
	{ // begin finally (depth: 1)
		{
			bool L_12 = V_1;
			if (!L_12)
			{
				goto IL_0097;
			}
		}

IL_0096:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_0097:
		{
			Il2CppObject* L_13 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			if (L_13)
			{
				goto IL_00a0;
			}
		}

IL_009f:
		{
			IL2CPP_END_FINALLY(147)
		}

IL_00a0:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
			IL2CPP_END_FINALLY(147)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(147)
	{
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_JUMP_TBL(0xAC, IL_00ac)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ac:
	{
		__this->set_U24PC_4((-1));
	}

IL_00b3:
	{
		return (bool)0;
	}

IL_00b5:
	{
		return (bool)1;
	}
	// Dead block : IL_00b7: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.RuntimePlatform,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1745298698_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1745298698_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3981440191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Dispose_m1745298698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_81U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<UnityEngine.RuntimePlatform,System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m3725242520_MetadataUsageId;
extern "C"  void U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m3725242520_gshared (U3CCreateSelectIteratorU3Ec__Iterator10_2_t3981440191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectIteratorU3Ec__Iterator10_2_Reset_m3725242520_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator12`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CCreateSelectManyIteratorU3Ec__Iterator12_2__ctor_m1032211654_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator12`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectManyIteratorU3Ec__Iterator12_2_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m2602589465_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_7();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator12`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateSelectManyIteratorU3Ec__Iterator12_2_System_Collections_IEnumerator_get_Current_m2008076796_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_7();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator12`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateSelectManyIteratorU3Ec__Iterator12_2_System_Collections_IEnumerable_GetEnumerator_m2451344661_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator12`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateSelectManyIteratorU3Ec__Iterator12_2_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m458591338_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * __this, const MethodInfo* method)
{
	U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_6();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * L_2 = (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 *)L_2;
		U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_8();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * L_5 = V_0;
		Func_2_t3117631226 * L_6 = (Func_2_t3117631226 *)__this->get_U3CU24U3Eselector_9();
		NullCheck(L_5);
		L_5->set_selector_3(L_6);
		U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator12`2<System.Object,System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectManyIteratorU3Ec__Iterator12_2_MoveNext_m2494996458_MetadataUsageId;
extern "C"  bool U3CCreateSelectManyIteratorU3Ec__Iterator12_2_MoveNext_m2494996458_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_MoveNext_m2494996458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_6();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_6((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_0117;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_83U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0078;
			}
		}

IL_0043:
		{
			goto IL_00e2;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_83U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t3117631226 * L_7 = (Func_2_t3117631226 *)__this->get_selector_3();
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t3117631226 *)L_7);
			Il2CppObject* L_9 = ((  Il2CppObject* (*) (Func_2_t3117631226 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3117631226 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			NullCheck((Il2CppObject*)L_9);
			Il2CppObject* L_10 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), (Il2CppObject*)L_9);
			__this->set_U3CU24s_84U3E__2_4(L_10);
			V_0 = (uint32_t)((int32_t)-3);
		}

IL_0078:
		try
		{ // begin try (depth: 2)
			{
				uint32_t L_11 = V_0;
				if (((int32_t)((int32_t)L_11-(int32_t)1)) == 0)
				{
					goto IL_00b4;
				}
			}

IL_0084:
			{
				goto IL_00b4;
			}

IL_0089:
			{
				Il2CppObject* L_12 = (Il2CppObject*)__this->get_U3CU24s_84U3E__2_4();
				NullCheck((Il2CppObject*)L_12);
				Il2CppObject * L_13 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), (Il2CppObject*)L_12);
				__this->set_U3CitemU3E__3_5(L_13);
				Il2CppObject * L_14 = (Il2CppObject *)__this->get_U3CitemU3E__3_5();
				__this->set_U24current_7(L_14);
				__this->set_U24PC_6(1);
				V_1 = (bool)1;
				IL2CPP_LEAVE(0x119, FINALLY_00c9);
			}

IL_00b4:
			{
				Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_84U3E__2_4();
				NullCheck((Il2CppObject *)L_15);
				bool L_16 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
				if (L_16)
				{
					goto IL_0089;
				}
			}

IL_00c4:
			{
				IL2CPP_LEAVE(0xE2, FINALLY_00c9);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_00c9;
		}

FINALLY_00c9:
		{ // begin finally (depth: 2)
			{
				bool L_17 = V_1;
				if (!L_17)
				{
					goto IL_00cd;
				}
			}

IL_00cc:
			{
				IL2CPP_END_FINALLY(201)
			}

IL_00cd:
			{
				Il2CppObject* L_18 = (Il2CppObject*)__this->get_U3CU24s_84U3E__2_4();
				if (L_18)
				{
					goto IL_00d6;
				}
			}

IL_00d5:
			{
				IL2CPP_END_FINALLY(201)
			}

IL_00d6:
			{
				Il2CppObject* L_19 = (Il2CppObject*)__this->get_U3CU24s_84U3E__2_4();
				NullCheck((Il2CppObject *)L_19);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_19);
				IL2CPP_END_FINALLY(201)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(201)
		{
			IL2CPP_END_CLEANUP(0x119, FINALLY_00f7);
			IL2CPP_JUMP_TBL(0xE2, IL_00e2)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_00e2:
		{
			Il2CppObject* L_20 = (Il2CppObject*)__this->get_U3CU24s_83U3E__0_1();
			NullCheck((Il2CppObject *)L_20);
			bool L_21 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_20);
			if (L_21)
			{
				goto IL_0048;
			}
		}

IL_00f2:
		{
			IL2CPP_LEAVE(0x110, FINALLY_00f7);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00f7;
	}

FINALLY_00f7:
	{ // begin finally (depth: 1)
		{
			bool L_22 = V_1;
			if (!L_22)
			{
				goto IL_00fb;
			}
		}

IL_00fa:
		{
			IL2CPP_END_FINALLY(247)
		}

IL_00fb:
		{
			Il2CppObject* L_23 = (Il2CppObject*)__this->get_U3CU24s_83U3E__0_1();
			if (L_23)
			{
				goto IL_0104;
			}
		}

IL_0103:
		{
			IL2CPP_END_FINALLY(247)
		}

IL_0104:
		{
			Il2CppObject* L_24 = (Il2CppObject*)__this->get_U3CU24s_83U3E__0_1();
			NullCheck((Il2CppObject *)L_24);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_24);
			IL2CPP_END_FINALLY(247)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(247)
	{
		IL2CPP_JUMP_TBL(0x119, IL_0119)
		IL2CPP_JUMP_TBL(0x110, IL_0110)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0110:
	{
		__this->set_U24PC_6((-1));
	}

IL_0117:
	{
		return (bool)0;
	}

IL_0119:
	{
		return (bool)1;
	}
	// Dead block : IL_011b: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator12`2<System.Object,System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectManyIteratorU3Ec__Iterator12_2_Dispose_m1260464891_MetadataUsageId;
extern "C"  void U3CCreateSelectManyIteratorU3Ec__Iterator12_2_Dispose_m1260464891_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_Dispose_m1260464891_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_6();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0055;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_0055;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			IL2CPP_LEAVE(0x3B, FINALLY_0026);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_0026;
		}

FINALLY_0026:
		{ // begin finally (depth: 2)
			{
				Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_84U3E__2_4();
				if (L_2)
				{
					goto IL_002f;
				}
			}

IL_002e:
			{
				IL2CPP_END_FINALLY(38)
			}

IL_002f:
			{
				Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_84U3E__2_4();
				NullCheck((Il2CppObject *)L_3);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
				IL2CPP_END_FINALLY(38)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(38)
		{
			IL2CPP_JUMP_TBL(0x3B, IL_003b)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_003b:
		{
			IL2CPP_LEAVE(0x55, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24s_83U3E__0_1();
			if (L_4)
			{
				goto IL_0049;
			}
		}

IL_0048:
		{
			IL2CPP_END_FINALLY(64)
		}

IL_0049:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_83U3E__0_1();
			NullCheck((Il2CppObject *)L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			IL2CPP_END_FINALLY(64)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0055:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator12`2<System.Object,System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateSelectManyIteratorU3Ec__Iterator12_2_Reset_m3641182809_MetadataUsageId;
extern "C"  void U3CCreateSelectManyIteratorU3Ec__Iterator12_2_Reset_m3641182809_gshared (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t288472136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateSelectManyIteratorU3Ec__Iterator12_2_Reset_m3641182809_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::.ctor()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m1958283157_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3602665650_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m269113779_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m3279674866_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m2682676065_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * L_2 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *)L_2;
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * L_5 = V_0;
		Func_2_t3961629604 * L_6 = (Func_2_t3961629604 *)__this->get_U3CU24U3Epredicate_7();
		NullCheck(L_5);
		L_5->set_predicate_3(L_6);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3533253043_MetadataUsageId;
extern "C"  bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3533253043_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3533253043_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00be;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_110U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0089;
			}
		}

IL_0043:
		{
			goto IL_0089;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t3961629604 * L_7 = (Func_2_t3961629604 *)__this->get_predicate_3();
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t3961629604 *)L_7);
			bool L_9 = ((  bool (*) (Func_2_t3961629604 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3961629604 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			if (!L_9)
			{
				goto IL_0089;
			}
		}

IL_006f:
		{
			Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			__this->set_U24current_5(L_10);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC0, FINALLY_009e);
		}

IL_0089:
		{
			Il2CppObject* L_11 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			NullCheck((Il2CppObject *)L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			if (L_12)
			{
				goto IL_0048;
			}
		}

IL_0099:
		{
			IL2CPP_LEAVE(0xB7, FINALLY_009e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_009e;
	}

FINALLY_009e:
	{ // begin finally (depth: 1)
		{
			bool L_13 = V_1;
			if (!L_13)
			{
				goto IL_00a2;
			}
		}

IL_00a1:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00a2:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			if (L_14)
			{
				goto IL_00ab;
			}
		}

IL_00aa:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00ab:
		{
			Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			IL2CPP_END_FINALLY(158)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(158)
	{
		IL2CPP_JUMP_TBL(0xC0, IL_00c0)
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b7:
	{
		__this->set_U24PC_4((-1));
	}

IL_00be:
	{
		return (bool)0;
	}

IL_00c0:
	{
		return (bool)1;
	}
	// Dead block : IL_00c2: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1879652802_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1879652802_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1879652802_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1773515612_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1773515612_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1773515612_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Linq.Grouping`2<System.Object,System.Object>::.ctor(K,System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void Grouping_2__ctor_m2008618495_gshared (Grouping_2_t308796194 * __this, Il2CppObject * ___key0, Il2CppObject* ___group1, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___group1;
		__this->set_group_1(L_0);
		Il2CppObject * L_1 = ___key0;
		__this->set_key_0(L_1);
		return;
	}
}
// System.Collections.IEnumerator System.Linq.Grouping`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Grouping_2_System_Collections_IEnumerable_GetEnumerator_m3099579774_gshared (Grouping_2_t308796194 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_group_1();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Linq.Grouping`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* Grouping_2_GetEnumerator_m4103693023_gshared (Grouping_2_t308796194 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_group_1();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), (Il2CppObject*)L_0);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
