﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityTest.ComparerBaseGeneric`1<UnityEngine.Vector4>
struct ComparerBaseGeneric_1_t3809740604;
// UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>
struct ComparerBaseGeneric_2_t3854387844;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>
struct ComparerBaseGeneric_2_t1563055900;
// UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>
struct ComparerBaseGeneric_2_t2764862240;
// UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>
struct ComparerBaseGeneric_2_t3409383436;
// UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>
struct ComparerBaseGeneric_2_t3470169984;
// UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>
struct ComparerBaseGeneric_2_t3685577712;
// UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>
struct ComparerBaseGeneric_2_t4171765452;
// UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>
struct ComparerBaseGeneric_2_t362985896;
// UnityTest.VectorComparerBase`1<System.Object>
struct VectorComparerBase_1_t1774955746;
// UnityTest.VectorComparerBase`1<UnityEngine.Vector2>
struct VectorComparerBase_1_t1329214030;
// UnityTest.VectorComparerBase`1<UnityEngine.Vector3>
struct VectorComparerBase_1_t1329214031;
// UnityTest.VectorComparerBase`1<UnityEngine.Vector4>
struct VectorComparerBase_1_t1329214032;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_1_3809740604.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_1_3809740604MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_2_g362985896MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_2_3854387844.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_2_3854387844MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBase429484414MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_ArrayTypes.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_2_1563055900.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_2_1563055900MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_2_2764862240.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_2_2764862240MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_2_3409383436.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_2_3409383436MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_2_3470169984.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_2_3470169984MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_2_3685577712.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_2_3685577712MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_2_4171765452.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_2_4171765452MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_2_g362985896.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "AssemblyU2DCSharp_UnityTest_VectorComparerBase_1_g1774955746.h"
#include "AssemblyU2DCSharp_UnityTest_VectorComparerBase_1_g1774955746MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_1_4255482318MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_Math2022911894MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_VectorComparerBase_1_g1329214030.h"
#include "AssemblyU2DCSharp_UnityTest_VectorComparerBase_1_g1329214030MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_1_3809740602MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_VectorComparerBase_1_g1329214031.h"
#include "AssemblyU2DCSharp_UnityTest_VectorComparerBase_1_g1329214031MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBaseGeneric_1_3809740603MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityTest_VectorComparerBase_1_g1329214032.h"
#include "AssemblyU2DCSharp_UnityTest_VectorComparerBase_1_g1329214032MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityTest.ComparerBaseGeneric`1<UnityEngine.Vector4>::.ctor()
extern "C"  void ComparerBaseGeneric_1__ctor_m3040067918_gshared (ComparerBaseGeneric_1_t3809740604 * __this, const MethodInfo* method)
{
	{
		NullCheck((ComparerBaseGeneric_2_t362985896 *)__this);
		((  void (*) (ComparerBaseGeneric_2_t362985896 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ComparerBaseGeneric_2_t362985896 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Void UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>::.ctor()
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2__ctor_m1420920721_MetadataUsageId;
extern "C"  void ComparerBaseGeneric_2__ctor_m1420920721_gshared (ComparerBaseGeneric_2_t3854387844 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2__ctor_m1420920721_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Initobj (Boolean_t3825574718_il2cpp_TypeInfo_var, (&V_0));
		bool L_0 = V_0;
		__this->set_constantValueGeneric_13(L_0);
		NullCheck((ComparerBase_t429484414 *)__this);
		ComparerBase__ctor_m731120606((ComparerBase_t429484414 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>::get_ConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_get_ConstValue_m2932615231_gshared (ComparerBaseGeneric_2_t3854387844 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_constantValueGeneric_13();
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Void UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>::set_ConstValue(System.Object)
extern "C"  void ComparerBaseGeneric_2_set_ConstValue_m2091596084_gshared (ComparerBaseGeneric_2_t3854387844 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_constantValueGeneric_13(((*(bool*)((bool*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))));
		return;
	}
}
// System.Object UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>::GetDefaultConstValue()
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetDefaultConstValue_m2891031835_MetadataUsageId;
extern "C"  Il2CppObject * ComparerBaseGeneric_2_GetDefaultConstValue_m2891031835_gshared (ComparerBaseGeneric_2_t3854387844 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetDefaultConstValue_m2891031835_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Initobj (Boolean_t3825574718_il2cpp_TypeInfo_var, (&V_0));
		bool L_0 = V_0;
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>::IsValueType(System.Type)
extern "C"  bool ComparerBaseGeneric_2_IsValueType_m2902129951_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		NullCheck((Type_t *)L_0);
		bool L_1 = Type_get_IsValueType_m1733572463((Type_t *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>::Compare(System.Object,System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral666768501;
extern const uint32_t ComparerBaseGeneric_2_Compare_m1255210266_MetadataUsageId;
extern "C"  bool ComparerBaseGeneric_2_Compare_m1255210266_gshared (ComparerBaseGeneric_2_t3854387844 * __this, Il2CppObject * ___a0, Il2CppObject * ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_Compare_m1255210266_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppObject * L_1 = ___b1;
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		Type_t * L_2 = V_0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Type_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentException_t3259014390 * L_4 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_4, (String_t*)_stringLiteral666768501, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0027:
	{
		Il2CppObject * L_5 = ___a0;
		Il2CppObject * L_6 = ___b1;
		NullCheck((ComparerBaseGeneric_2_t3854387844 *)__this);
		bool L_7 = VirtFuncInvoker2< bool, bool, bool >::Invoke(18 /* System.Boolean UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>::Compare(T1,T2) */, (ComparerBaseGeneric_2_t3854387844 *)__this, (bool)((*(bool*)((bool*)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), (bool)((*(bool*)((bool*)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))));
		return L_7;
	}
}
// System.Type[] UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>::GetAccepatbleTypesForA()
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetAccepatbleTypesForA_m2700426262_MetadataUsageId;
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForA_m2700426262_gshared (ComparerBaseGeneric_2_t3854387844 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetAccepatbleTypesForA_m2700426262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t1664964607* L_0 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 6)), /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		return L_0;
	}
}
// System.Type[] UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>::GetAccepatbleTypesForB()
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetAccepatbleTypesForB_m2559263761_MetadataUsageId;
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForB_m2559263761_gshared (ComparerBaseGeneric_2_t3854387844 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetAccepatbleTypesForB_m2559263761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t1664964607* L_0 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		return L_0;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Boolean,System.Boolean>::get_UseCache()
extern "C"  bool ComparerBaseGeneric_2_get_UseCache_m21161807_gshared (ComparerBaseGeneric_2_t3854387844 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>::.ctor()
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2__ctor_m554013261_MetadataUsageId;
extern "C"  void ComparerBaseGeneric_2__ctor_m554013261_gshared (ComparerBaseGeneric_2_t1563055900 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2__ctor_m554013261_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = V_0;
		__this->set_constantValueGeneric_13(L_0);
		NullCheck((ComparerBase_t429484414 *)__this);
		ComparerBase__ctor_m731120606((ComparerBase_t429484414 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>::get_ConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_get_ConstValue_m1615861079_gshared (ComparerBaseGeneric_2_t1563055900 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_constantValueGeneric_13();
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Void UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>::set_ConstValue(System.Object)
extern "C"  void ComparerBaseGeneric_2_set_ConstValue_m2429710928_gshared (ComparerBaseGeneric_2_t1563055900 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_constantValueGeneric_13(((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))));
		return;
	}
}
// System.Object UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>::GetDefaultConstValue()
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetDefaultConstValue_m2519031227_MetadataUsageId;
extern "C"  Il2CppObject * ComparerBaseGeneric_2_GetDefaultConstValue_m2519031227_gshared (ComparerBaseGeneric_2_t1563055900 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetDefaultConstValue_m2519031227_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = V_0;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>::IsValueType(System.Type)
extern "C"  bool ComparerBaseGeneric_2_IsValueType_m383119759_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		NullCheck((Type_t *)L_0);
		bool L_1 = Type_get_IsValueType_m1733572463((Type_t *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>::Compare(System.Object,System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral666768501;
extern const uint32_t ComparerBaseGeneric_2_Compare_m2198560970_MetadataUsageId;
extern "C"  bool ComparerBaseGeneric_2_Compare_m2198560970_gshared (ComparerBaseGeneric_2_t1563055900 * __this, Il2CppObject * ___a0, Il2CppObject * ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_Compare_m2198560970_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppObject * L_1 = ___b1;
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		Type_t * L_2 = V_0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Type_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentException_t3259014390 * L_4 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_4, (String_t*)_stringLiteral666768501, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0027:
	{
		Il2CppObject * L_5 = ___a0;
		Il2CppObject * L_6 = ___b1;
		NullCheck((ComparerBaseGeneric_2_t1563055900 *)__this);
		bool L_7 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(18 /* System.Boolean UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>::Compare(T1,T2) */, (ComparerBaseGeneric_2_t1563055900 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))));
		return L_7;
	}
}
// System.Type[] UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>::GetAccepatbleTypesForA()
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetAccepatbleTypesForA_m678438718_MetadataUsageId;
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForA_m678438718_gshared (ComparerBaseGeneric_2_t1563055900 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetAccepatbleTypesForA_m678438718_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t1664964607* L_0 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 6)), /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		return L_0;
	}
}
// System.Type[] UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>::GetAccepatbleTypesForB()
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetAccepatbleTypesForB_m1101926221_MetadataUsageId;
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForB_m1101926221_gshared (ComparerBaseGeneric_2_t1563055900 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetAccepatbleTypesForB_m1101926221_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t1664964607* L_0 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		return L_0;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Int32,System.Int32>::get_UseCache()
extern "C"  bool ComparerBaseGeneric_2_get_UseCache_m3664638855_gshared (ComparerBaseGeneric_2_t1563055900 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2__ctor_m2944922553_MetadataUsageId;
extern "C"  void ComparerBaseGeneric_2__ctor_m2944922553_gshared (ComparerBaseGeneric_2_t2764862240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2__ctor_m2944922553_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		__this->set_constantValueGeneric_13(L_0);
		NullCheck((ComparerBase_t429484414 *)__this);
		ComparerBase__ctor_m731120606((ComparerBase_t429484414 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>::get_ConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_get_ConstValue_m3589533995_gshared (ComparerBaseGeneric_2_t2764862240 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_constantValueGeneric_13();
		return L_0;
	}
}
// System.Void UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>::set_ConstValue(System.Object)
extern "C"  void ComparerBaseGeneric_2_set_ConstValue_m1223600742_gshared (ComparerBaseGeneric_2_t2764862240 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_constantValueGeneric_13(((Il2CppObject *)Castclass(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))));
		return;
	}
}
// System.Object UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>::GetDefaultConstValue()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetDefaultConstValue_m4190662983_MetadataUsageId;
extern "C"  Il2CppObject * ComparerBaseGeneric_2_GetDefaultConstValue_m4190662983_gshared (ComparerBaseGeneric_2_t2764862240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetDefaultConstValue_m4190662983_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>::IsValueType(System.Type)
extern "C"  bool ComparerBaseGeneric_2_IsValueType_m3892472595_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		NullCheck((Type_t *)L_0);
		bool L_1 = Type_get_IsValueType_m1733572463((Type_t *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>::Compare(System.Object,System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral666768501;
extern const uint32_t ComparerBaseGeneric_2_Compare_m3999256872_MetadataUsageId;
extern "C"  bool ComparerBaseGeneric_2_Compare_m3999256872_gshared (ComparerBaseGeneric_2_t2764862240 * __this, Il2CppObject * ___a0, Il2CppObject * ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_Compare_m3999256872_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppObject * L_1 = ___b1;
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		Type_t * L_2 = V_0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Type_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentException_t3259014390 * L_4 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_4, (String_t*)_stringLiteral666768501, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0027:
	{
		Il2CppObject * L_5 = ___a0;
		Il2CppObject * L_6 = ___b1;
		NullCheck((ComparerBaseGeneric_2_t2764862240 *)__this);
		bool L_7 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(18 /* System.Boolean UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>::Compare(T1,T2) */, (ComparerBaseGeneric_2_t2764862240 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))), (Il2CppObject *)((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))));
		return L_7;
	}
}
// System.Type[] UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>::GetAccepatbleTypesForA()
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetAccepatbleTypesForA_m3612969492_MetadataUsageId;
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForA_m3612969492_gshared (ComparerBaseGeneric_2_t2764862240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetAccepatbleTypesForA_m3612969492_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t1664964607* L_0 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 6)), /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		return L_0;
	}
}
// System.Type[] UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>::GetAccepatbleTypesForB()
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetAccepatbleTypesForB_m3754131993_MetadataUsageId;
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForB_m3754131993_gshared (ComparerBaseGeneric_2_t2764862240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetAccepatbleTypesForB_m3754131993_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t1664964607* L_0 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		return L_0;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Object,System.Object>::get_UseCache()
extern "C"  bool ComparerBaseGeneric_2_get_UseCache_m3855553179_gshared (ComparerBaseGeneric_2_t2764862240 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>::.ctor()
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2__ctor_m1173068249_MetadataUsageId;
extern "C"  void ComparerBaseGeneric_2__ctor_m1173068249_gshared (ComparerBaseGeneric_2_t3409383436 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2__ctor_m1173068249_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = V_0;
		__this->set_constantValueGeneric_13(L_0);
		NullCheck((ComparerBase_t429484414 *)__this);
		ComparerBase__ctor_m731120606((ComparerBase_t429484414 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>::get_ConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_get_ConstValue_m4270912331_gshared (ComparerBaseGeneric_2_t3409383436 * __this, const MethodInfo* method)
{
	{
		float L_0 = (float)__this->get_constantValueGeneric_13();
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Void UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>::set_ConstValue(System.Object)
extern "C"  void ComparerBaseGeneric_2_set_ConstValue_m2713844870_gshared (ComparerBaseGeneric_2_t3409383436 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_constantValueGeneric_13(((*(float*)((float*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))));
		return;
	}
}
// System.Object UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>::GetDefaultConstValue()
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetDefaultConstValue_m2772259111_MetadataUsageId;
extern "C"  Il2CppObject * ComparerBaseGeneric_2_GetDefaultConstValue_m2772259111_gshared (ComparerBaseGeneric_2_t3409383436 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetDefaultConstValue_m2772259111_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = V_0;
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>::IsValueType(System.Type)
extern "C"  bool ComparerBaseGeneric_2_IsValueType_m2742662963_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		NullCheck((Type_t *)L_0);
		bool L_1 = Type_get_IsValueType_m1733572463((Type_t *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>::Compare(System.Object,System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral666768501;
extern const uint32_t ComparerBaseGeneric_2_Compare_m560240328_MetadataUsageId;
extern "C"  bool ComparerBaseGeneric_2_Compare_m560240328_gshared (ComparerBaseGeneric_2_t3409383436 * __this, Il2CppObject * ___a0, Il2CppObject * ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_Compare_m560240328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppObject * L_1 = ___b1;
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		Type_t * L_2 = V_0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Type_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentException_t3259014390 * L_4 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_4, (String_t*)_stringLiteral666768501, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0027:
	{
		Il2CppObject * L_5 = ___a0;
		Il2CppObject * L_6 = ___b1;
		NullCheck((ComparerBaseGeneric_2_t3409383436 *)__this);
		bool L_7 = VirtFuncInvoker2< bool, float, float >::Invoke(18 /* System.Boolean UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>::Compare(T1,T2) */, (ComparerBaseGeneric_2_t3409383436 *)__this, (float)((*(float*)((float*)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), (float)((*(float*)((float*)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))));
		return L_7;
	}
}
// System.Type[] UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>::GetAccepatbleTypesForA()
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetAccepatbleTypesForA_m1665858868_MetadataUsageId;
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForA_m1665858868_gshared (ComparerBaseGeneric_2_t3409383436 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetAccepatbleTypesForA_m1665858868_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t1664964607* L_0 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 6)), /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		return L_0;
	}
}
// System.Type[] UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>::GetAccepatbleTypesForB()
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetAccepatbleTypesForB_m402302905_MetadataUsageId;
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForB_m402302905_gshared (ComparerBaseGeneric_2_t3409383436 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetAccepatbleTypesForB_m402302905_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t1664964607* L_0 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		return L_0;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<System.Single,System.Single>::get_UseCache()
extern "C"  bool ComparerBaseGeneric_2_get_UseCache_m504648699_gshared (ComparerBaseGeneric_2_t3409383436 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>::.ctor()
extern Il2CppClass* Bounds_t3033363703_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2__ctor_m536379533_MetadataUsageId;
extern "C"  void ComparerBaseGeneric_2__ctor_m536379533_gshared (ComparerBaseGeneric_2_t3470169984 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2__ctor_m536379533_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_t3033363703  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Bounds_t3033363703_il2cpp_TypeInfo_var, (&V_0));
		Bounds_t3033363703  L_0 = V_0;
		__this->set_constantValueGeneric_13(L_0);
		NullCheck((ComparerBase_t429484414 *)__this);
		ComparerBase__ctor_m731120606((ComparerBase_t429484414 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>::get_ConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_get_ConstValue_m789496931_gshared (ComparerBaseGeneric_2_t3470169984 * __this, const MethodInfo* method)
{
	{
		Bounds_t3033363703  L_0 = (Bounds_t3033363703 )__this->get_constantValueGeneric_13();
		Bounds_t3033363703  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Void UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>::set_ConstValue(System.Object)
extern "C"  void ComparerBaseGeneric_2_set_ConstValue_m1771588154_gshared (ComparerBaseGeneric_2_t3470169984 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_constantValueGeneric_13(((*(Bounds_t3033363703 *)((Bounds_t3033363703 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))));
		return;
	}
}
// System.Object UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>::GetDefaultConstValue()
extern Il2CppClass* Bounds_t3033363703_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetDefaultConstValue_m4272022687_MetadataUsageId;
extern "C"  Il2CppObject * ComparerBaseGeneric_2_GetDefaultConstValue_m4272022687_gshared (ComparerBaseGeneric_2_t3470169984 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetDefaultConstValue_m4272022687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_t3033363703  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Bounds_t3033363703_il2cpp_TypeInfo_var, (&V_0));
		Bounds_t3033363703  L_0 = V_0;
		Bounds_t3033363703  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>::IsValueType(System.Type)
extern "C"  bool ComparerBaseGeneric_2_IsValueType_m198709307_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		NullCheck((Type_t *)L_0);
		bool L_1 = Type_get_IsValueType_m1733572463((Type_t *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>::Compare(System.Object,System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral666768501;
extern const uint32_t ComparerBaseGeneric_2_Compare_m383510304_MetadataUsageId;
extern "C"  bool ComparerBaseGeneric_2_Compare_m383510304_gshared (ComparerBaseGeneric_2_t3470169984 * __this, Il2CppObject * ___a0, Il2CppObject * ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_Compare_m383510304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppObject * L_1 = ___b1;
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		Type_t * L_2 = V_0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Type_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentException_t3259014390 * L_4 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_4, (String_t*)_stringLiteral666768501, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0027:
	{
		Il2CppObject * L_5 = ___a0;
		Il2CppObject * L_6 = ___b1;
		NullCheck((ComparerBaseGeneric_2_t3470169984 *)__this);
		bool L_7 = VirtFuncInvoker2< bool, Bounds_t3033363703 , Bounds_t3033363703  >::Invoke(18 /* System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>::Compare(T1,T2) */, (ComparerBaseGeneric_2_t3470169984 *)__this, (Bounds_t3033363703 )((*(Bounds_t3033363703 *)((Bounds_t3033363703 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), (Bounds_t3033363703 )((*(Bounds_t3033363703 *)((Bounds_t3033363703 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))));
		return L_7;
	}
}
// System.Type[] UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>::GetAccepatbleTypesForA()
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetAccepatbleTypesForA_m3955461180_MetadataUsageId;
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForA_m3955461180_gshared (ComparerBaseGeneric_2_t3470169984 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetAccepatbleTypesForA_m3955461180_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t1664964607* L_0 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 6)), /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		return L_0;
	}
}
// System.Type[] UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>::GetAccepatbleTypesForB()
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetAccepatbleTypesForB_m3531973677_MetadataUsageId;
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForB_m3531973677_gshared (ComparerBaseGeneric_2_t3470169984 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetAccepatbleTypesForB_m3531973677_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t1664964607* L_0 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		return L_0;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Bounds,UnityEngine.Bounds>::get_UseCache()
extern "C"  bool ComparerBaseGeneric_2_get_UseCache_m3970344979_gshared (ComparerBaseGeneric_2_t3470169984 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>::.ctor()
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2__ctor_m3916373049_MetadataUsageId;
extern "C"  void ComparerBaseGeneric_2__ctor_m3916373049_gshared (ComparerBaseGeneric_2_t3685577712 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2__ctor_m3916373049_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector2_t2243707579_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t2243707579  L_0 = V_0;
		__this->set_constantValueGeneric_13(L_0);
		NullCheck((ComparerBase_t429484414 *)__this);
		ComparerBase__ctor_m731120606((ComparerBase_t429484414 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>::get_ConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_get_ConstValue_m2618265451_gshared (ComparerBaseGeneric_2_t3685577712 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = (Vector2_t2243707579 )__this->get_constantValueGeneric_13();
		Vector2_t2243707579  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Void UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>::set_ConstValue(System.Object)
extern "C"  void ComparerBaseGeneric_2_set_ConstValue_m2763752166_gshared (ComparerBaseGeneric_2_t3685577712 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_constantValueGeneric_13(((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))));
		return;
	}
}
// System.Object UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>::GetDefaultConstValue()
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetDefaultConstValue_m28733447_MetadataUsageId;
extern "C"  Il2CppObject * ComparerBaseGeneric_2_GetDefaultConstValue_m28733447_gshared (ComparerBaseGeneric_2_t3685577712 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetDefaultConstValue_m28733447_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector2_t2243707579_il2cpp_TypeInfo_var, (&V_0));
		Vector2_t2243707579  L_0 = V_0;
		Vector2_t2243707579  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>::IsValueType(System.Type)
extern "C"  bool ComparerBaseGeneric_2_IsValueType_m1921309075_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		NullCheck((Type_t *)L_0);
		bool L_1 = Type_get_IsValueType_m1733572463((Type_t *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>::Compare(System.Object,System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral666768501;
extern const uint32_t ComparerBaseGeneric_2_Compare_m1681931688_MetadataUsageId;
extern "C"  bool ComparerBaseGeneric_2_Compare_m1681931688_gshared (ComparerBaseGeneric_2_t3685577712 * __this, Il2CppObject * ___a0, Il2CppObject * ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_Compare_m1681931688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppObject * L_1 = ___b1;
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		Type_t * L_2 = V_0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Type_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentException_t3259014390 * L_4 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_4, (String_t*)_stringLiteral666768501, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0027:
	{
		Il2CppObject * L_5 = ___a0;
		Il2CppObject * L_6 = ___b1;
		NullCheck((ComparerBaseGeneric_2_t3685577712 *)__this);
		bool L_7 = VirtFuncInvoker2< bool, Vector2_t2243707579 , Vector2_t2243707579  >::Invoke(18 /* System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>::Compare(T1,T2) */, (ComparerBaseGeneric_2_t3685577712 *)__this, (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))));
		return L_7;
	}
}
// System.Type[] UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>::GetAccepatbleTypesForA()
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetAccepatbleTypesForA_m512542484_MetadataUsageId;
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForA_m512542484_gshared (ComparerBaseGeneric_2_t3685577712 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetAccepatbleTypesForA_m512542484_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t1664964607* L_0 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 6)), /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		return L_0;
	}
}
// System.Type[] UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>::GetAccepatbleTypesForB()
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetAccepatbleTypesForB_m653704985_MetadataUsageId;
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForB_m653704985_gshared (ComparerBaseGeneric_2_t3685577712 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetAccepatbleTypesForB_m653704985_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t1664964607* L_0 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		return L_0;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector2,UnityEngine.Vector2>::get_UseCache()
extern "C"  bool ComparerBaseGeneric_2_get_UseCache_m138643099_gshared (ComparerBaseGeneric_2_t3685577712 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>::.ctor()
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2__ctor_m2686062777_MetadataUsageId;
extern "C"  void ComparerBaseGeneric_2__ctor_m2686062777_gshared (ComparerBaseGeneric_2_t4171765452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2__ctor_m2686062777_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_0 = V_0;
		__this->set_constantValueGeneric_13(L_0);
		NullCheck((ComparerBase_t429484414 *)__this);
		ComparerBase__ctor_m731120606((ComparerBase_t429484414 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>::get_ConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_get_ConstValue_m759497451_gshared (ComparerBaseGeneric_2_t4171765452 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = (Vector3_t2243707580 )__this->get_constantValueGeneric_13();
		Vector3_t2243707580  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Void UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>::set_ConstValue(System.Object)
extern "C"  void ComparerBaseGeneric_2_set_ConstValue_m198485862_gshared (ComparerBaseGeneric_2_t4171765452 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_constantValueGeneric_13(((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))));
		return;
	}
}
// System.Object UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>::GetDefaultConstValue()
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetDefaultConstValue_m1109472135_MetadataUsageId;
extern "C"  Il2CppObject * ComparerBaseGeneric_2_GetDefaultConstValue_m1109472135_gshared (ComparerBaseGeneric_2_t4171765452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetDefaultConstValue_m1109472135_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_0 = V_0;
		Vector3_t2243707580  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>::IsValueType(System.Type)
extern "C"  bool ComparerBaseGeneric_2_IsValueType_m3061251603_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		NullCheck((Type_t *)L_0);
		bool L_1 = Type_get_IsValueType_m1733572463((Type_t *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>::Compare(System.Object,System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral666768501;
extern const uint32_t ComparerBaseGeneric_2_Compare_m56345128_MetadataUsageId;
extern "C"  bool ComparerBaseGeneric_2_Compare_m56345128_gshared (ComparerBaseGeneric_2_t4171765452 * __this, Il2CppObject * ___a0, Il2CppObject * ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_Compare_m56345128_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppObject * L_1 = ___b1;
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		Type_t * L_2 = V_0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Type_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentException_t3259014390 * L_4 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_4, (String_t*)_stringLiteral666768501, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0027:
	{
		Il2CppObject * L_5 = ___a0;
		Il2CppObject * L_6 = ___b1;
		NullCheck((ComparerBaseGeneric_2_t4171765452 *)__this);
		bool L_7 = VirtFuncInvoker2< bool, Vector3_t2243707580 , Vector3_t2243707580  >::Invoke(18 /* System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>::Compare(T1,T2) */, (ComparerBaseGeneric_2_t4171765452 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))));
		return L_7;
	}
}
// System.Type[] UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>::GetAccepatbleTypesForA()
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetAccepatbleTypesForA_m2871835540_MetadataUsageId;
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForA_m2871835540_gshared (ComparerBaseGeneric_2_t4171765452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetAccepatbleTypesForA_m2871835540_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t1664964607* L_0 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 6)), /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		return L_0;
	}
}
// System.Type[] UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>::GetAccepatbleTypesForB()
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetAccepatbleTypesForB_m3012998041_MetadataUsageId;
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForB_m3012998041_gshared (ComparerBaseGeneric_2_t4171765452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetAccepatbleTypesForB_m3012998041_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t1664964607* L_0 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		return L_0;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector3,UnityEngine.Vector3>::get_UseCache()
extern "C"  bool ComparerBaseGeneric_2_get_UseCache_m3333217051_gshared (ComparerBaseGeneric_2_t4171765452 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>::.ctor()
extern Il2CppClass* Vector4_t2243707581_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2__ctor_m509928505_MetadataUsageId;
extern "C"  void ComparerBaseGeneric_2__ctor_m509928505_gshared (ComparerBaseGeneric_2_t362985896 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2__ctor_m509928505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector4_t2243707581  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector4_t2243707581_il2cpp_TypeInfo_var, (&V_0));
		Vector4_t2243707581  L_0 = V_0;
		__this->set_constantValueGeneric_13(L_0);
		NullCheck((ComparerBase_t429484414 *)__this);
		ComparerBase__ctor_m731120606((ComparerBase_t429484414 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>::get_ConstValue()
extern "C"  Il2CppObject * ComparerBaseGeneric_2_get_ConstValue_m3792050027_gshared (ComparerBaseGeneric_2_t362985896 * __this, const MethodInfo* method)
{
	{
		Vector4_t2243707581  L_0 = (Vector4_t2243707581 )__this->get_constantValueGeneric_13();
		Vector4_t2243707581  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Void UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>::set_ConstValue(System.Object)
extern "C"  void ComparerBaseGeneric_2_set_ConstValue_m3615771366_gshared (ComparerBaseGeneric_2_t362985896 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_constantValueGeneric_13(((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))));
		return;
	}
}
// System.Object UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>::GetDefaultConstValue()
extern Il2CppClass* Vector4_t2243707581_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetDefaultConstValue_m1451227143_MetadataUsageId;
extern "C"  Il2CppObject * ComparerBaseGeneric_2_GetDefaultConstValue_m1451227143_gshared (ComparerBaseGeneric_2_t362985896 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetDefaultConstValue_m1451227143_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector4_t2243707581  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector4_t2243707581_il2cpp_TypeInfo_var, (&V_0));
		Vector4_t2243707581  L_0 = V_0;
		Vector4_t2243707581  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>::IsValueType(System.Type)
extern "C"  bool ComparerBaseGeneric_2_IsValueType_m3488305555_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		NullCheck((Type_t *)L_0);
		bool L_1 = Type_get_IsValueType_m1733572463((Type_t *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>::Compare(System.Object,System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral666768501;
extern const uint32_t ComparerBaseGeneric_2_Compare_m44086696_MetadataUsageId;
extern "C"  bool ComparerBaseGeneric_2_Compare_m44086696_gshared (ComparerBaseGeneric_2_t362985896 * __this, Il2CppObject * ___a0, Il2CppObject * ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_Compare_m44086696_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		V_0 = (Type_t *)L_0;
		Il2CppObject * L_1 = ___b1;
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		Type_t * L_2 = V_0;
		bool L_3 = ((  bool (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Type_t *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentException_t3259014390 * L_4 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_4, (String_t*)_stringLiteral666768501, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0027:
	{
		Il2CppObject * L_5 = ___a0;
		Il2CppObject * L_6 = ___b1;
		NullCheck((ComparerBaseGeneric_2_t362985896 *)__this);
		bool L_7 = VirtFuncInvoker2< bool, Vector4_t2243707581 , Vector4_t2243707581  >::Invoke(18 /* System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>::Compare(T1,T2) */, (ComparerBaseGeneric_2_t362985896 *)__this, (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))))), (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))));
		return L_7;
	}
}
// System.Type[] UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>::GetAccepatbleTypesForA()
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetAccepatbleTypesForA_m3986120724_MetadataUsageId;
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForA_m3986120724_gshared (ComparerBaseGeneric_2_t362985896 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetAccepatbleTypesForA_m3986120724_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t1664964607* L_0 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 6)), /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		return L_0;
	}
}
// System.Type[] UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>::GetAccepatbleTypesForB()
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ComparerBaseGeneric_2_GetAccepatbleTypesForB_m4127283225_MetadataUsageId;
extern "C"  TypeU5BU5D_t1664964607* ComparerBaseGeneric_2_GetAccepatbleTypesForB_m4127283225_gshared (ComparerBaseGeneric_2_t362985896 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ComparerBaseGeneric_2_GetAccepatbleTypesForB_m4127283225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t1664964607* L_0 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		return L_0;
	}
}
// System.Boolean UnityTest.ComparerBaseGeneric`2<UnityEngine.Vector4,UnityEngine.Vector4>::get_UseCache()
extern "C"  bool ComparerBaseGeneric_2_get_UseCache_m3586064027_gshared (ComparerBaseGeneric_2_t362985896 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void UnityTest.VectorComparerBase`1<System.Object>::.ctor()
extern "C"  void VectorComparerBase_1__ctor_m691832304_gshared (VectorComparerBase_1_t1774955746 * __this, const MethodInfo* method)
{
	{
		NullCheck((ComparerBaseGeneric_1_t4255482318 *)__this);
		((  void (*) (ComparerBaseGeneric_1_t4255482318 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ComparerBaseGeneric_1_t4255482318 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean UnityTest.VectorComparerBase`1<System.Object>::AreVectorMagnitudeEqual(System.Single,System.Single,System.Double)
extern "C"  bool VectorComparerBase_1_AreVectorMagnitudeEqual_m2812616117_gshared (VectorComparerBase_1_t1774955746 * __this, float ___a0, float ___b1, double ___floatingPointError2, const MethodInfo* method)
{
	{
		float L_0 = ___a0;
		float L_1 = fabsf((float)L_0);
		double L_2 = ___floatingPointError2;
		if ((!(((double)(((double)((double)L_1)))) < ((double)L_2))))
		{
			goto IL_001c;
		}
	}
	{
		float L_3 = ___b1;
		float L_4 = fabsf((float)L_3);
		double L_5 = ___floatingPointError2;
		if ((!(((double)(((double)((double)L_4)))) < ((double)L_5))))
		{
			goto IL_001c;
		}
	}
	{
		return (bool)1;
	}

IL_001c:
	{
		float L_6 = ___a0;
		float L_7 = ___b1;
		float L_8 = fabsf((float)((float)((float)L_6-(float)L_7)));
		double L_9 = ___floatingPointError2;
		if ((!(((double)(((double)((double)L_8)))) < ((double)L_9))))
		{
			goto IL_002d;
		}
	}
	{
		return (bool)1;
	}

IL_002d:
	{
		return (bool)0;
	}
}
// System.Void UnityTest.VectorComparerBase`1<UnityEngine.Vector2>::.ctor()
extern "C"  void VectorComparerBase_1__ctor_m1399026100_gshared (VectorComparerBase_1_t1329214030 * __this, const MethodInfo* method)
{
	{
		NullCheck((ComparerBaseGeneric_1_t3809740602 *)__this);
		((  void (*) (ComparerBaseGeneric_1_t3809740602 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ComparerBaseGeneric_1_t3809740602 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean UnityTest.VectorComparerBase`1<UnityEngine.Vector2>::AreVectorMagnitudeEqual(System.Single,System.Single,System.Double)
extern "C"  bool VectorComparerBase_1_AreVectorMagnitudeEqual_m1374791335_gshared (VectorComparerBase_1_t1329214030 * __this, float ___a0, float ___b1, double ___floatingPointError2, const MethodInfo* method)
{
	{
		float L_0 = ___a0;
		float L_1 = fabsf((float)L_0);
		double L_2 = ___floatingPointError2;
		if ((!(((double)(((double)((double)L_1)))) < ((double)L_2))))
		{
			goto IL_001c;
		}
	}
	{
		float L_3 = ___b1;
		float L_4 = fabsf((float)L_3);
		double L_5 = ___floatingPointError2;
		if ((!(((double)(((double)((double)L_4)))) < ((double)L_5))))
		{
			goto IL_001c;
		}
	}
	{
		return (bool)1;
	}

IL_001c:
	{
		float L_6 = ___a0;
		float L_7 = ___b1;
		float L_8 = fabsf((float)((float)((float)L_6-(float)L_7)));
		double L_9 = ___floatingPointError2;
		if ((!(((double)(((double)((double)L_8)))) < ((double)L_9))))
		{
			goto IL_002d;
		}
	}
	{
		return (bool)1;
	}

IL_002d:
	{
		return (bool)0;
	}
}
// System.Void UnityTest.VectorComparerBase`1<UnityEngine.Vector3>::.ctor()
extern "C"  void VectorComparerBase_1__ctor_m2196001199_gshared (VectorComparerBase_1_t1329214031 * __this, const MethodInfo* method)
{
	{
		NullCheck((ComparerBaseGeneric_1_t3809740603 *)__this);
		((  void (*) (ComparerBaseGeneric_1_t3809740603 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ComparerBaseGeneric_1_t3809740603 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean UnityTest.VectorComparerBase`1<UnityEngine.Vector3>::AreVectorMagnitudeEqual(System.Single,System.Single,System.Double)
extern "C"  bool VectorComparerBase_1_AreVectorMagnitudeEqual_m290049576_gshared (VectorComparerBase_1_t1329214031 * __this, float ___a0, float ___b1, double ___floatingPointError2, const MethodInfo* method)
{
	{
		float L_0 = ___a0;
		float L_1 = fabsf((float)L_0);
		double L_2 = ___floatingPointError2;
		if ((!(((double)(((double)((double)L_1)))) < ((double)L_2))))
		{
			goto IL_001c;
		}
	}
	{
		float L_3 = ___b1;
		float L_4 = fabsf((float)L_3);
		double L_5 = ___floatingPointError2;
		if ((!(((double)(((double)((double)L_4)))) < ((double)L_5))))
		{
			goto IL_001c;
		}
	}
	{
		return (bool)1;
	}

IL_001c:
	{
		float L_6 = ___a0;
		float L_7 = ___b1;
		float L_8 = fabsf((float)((float)((float)L_6-(float)L_7)));
		double L_9 = ___floatingPointError2;
		if ((!(((double)(((double)((double)L_8)))) < ((double)L_9))))
		{
			goto IL_002d;
		}
	}
	{
		return (bool)1;
	}

IL_002d:
	{
		return (bool)0;
	}
}
// System.Void UnityTest.VectorComparerBase`1<UnityEngine.Vector4>::.ctor()
extern "C"  void VectorComparerBase_1__ctor_m2827238570_gshared (VectorComparerBase_1_t1329214032 * __this, const MethodInfo* method)
{
	{
		NullCheck((ComparerBaseGeneric_1_t3809740604 *)__this);
		((  void (*) (ComparerBaseGeneric_1_t3809740604 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ComparerBaseGeneric_1_t3809740604 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return;
	}
}
// System.Boolean UnityTest.VectorComparerBase`1<UnityEngine.Vector4>::AreVectorMagnitudeEqual(System.Single,System.Single,System.Double)
extern "C"  bool VectorComparerBase_1_AreVectorMagnitudeEqual_m1121930541_gshared (VectorComparerBase_1_t1329214032 * __this, float ___a0, float ___b1, double ___floatingPointError2, const MethodInfo* method)
{
	{
		float L_0 = ___a0;
		float L_1 = fabsf((float)L_0);
		double L_2 = ___floatingPointError2;
		if ((!(((double)(((double)((double)L_1)))) < ((double)L_2))))
		{
			goto IL_001c;
		}
	}
	{
		float L_3 = ___b1;
		float L_4 = fabsf((float)L_3);
		double L_5 = ___floatingPointError2;
		if ((!(((double)(((double)((double)L_4)))) < ((double)L_5))))
		{
			goto IL_001c;
		}
	}
	{
		return (bool)1;
	}

IL_001c:
	{
		float L_6 = ___a0;
		float L_7 = ___b1;
		float L_8 = fabsf((float)((float)((float)L_6-(float)L_7)));
		double L_9 = ___floatingPointError2;
		if ((!(((double)(((double)((double)L_8)))) < ((double)L_9))))
		{
			goto IL_002d;
		}
	}
	{
		return (bool)1;
	}

IL_002d:
	{
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
