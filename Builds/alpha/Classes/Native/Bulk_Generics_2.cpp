﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>
struct Comparer_1_t2511286993;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>
struct Comparer_1_t94267937;
// System.Collections.Generic.Comparer`1<UnityEngine.Vector2>
struct Comparer_1_t1133716698;
// System.Collections.Generic.Comparer`1<UnityEngine.Vector3>
struct Comparer_1_t1133716699;
// System.Collections.Generic.Comparer`1<UnityEngine.Vector4>
struct Comparer_1_t1133716700;
// System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>
struct Dictionary_2_t3624498739;
// System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>
struct Dictionary_2_t1052016128;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1079703083;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1697274930;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Reflection.Emit.Label>
struct Dictionary_2_t3251028295;
// System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>
struct Dictionary_2_t3061395850;
// System.Collections.Generic.Dictionary`2<System.Int64,System.Object>
struct Dictionary_2_t3678967697;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>
struct Dictionary_2_t2513902766;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>
struct Dictionary_2_t3131474613;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t3417634846;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1663937576;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t501138165;
// System.Collections.Generic.Dictionary`2<System.Object,System.Nullable`1<System.Int32>>
struct Dictionary_2_t4221971187;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2281509423;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2730857826;
// System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>
struct KeyCollection_t1813029214;
// System.Collections.Generic.IEnumerator`1<PubNubMessaging.Core.ChannelIdentity>
struct IEnumerator_1_t2917653390;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,PubNubMessaging.Core.ChannelIdentity>
struct Transform_1_t2200833417;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// PubNubMessaging.Core.ChannelIdentity[]
struct ChannelIdentityU5BU5D_t662656154;
// System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>
struct KeyCollection_t3535513899;
// System.Collections.Generic.IEnumerator`1<PubNubMessaging.Core.CurrentRequestType>
struct IEnumerator_1_t725751813;
// System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.CurrentRequestType,System.Object,PubNubMessaging.Core.CurrentRequestType>
struct Transform_1_t435794015;
// PubNubMessaging.Core.CurrentRequestType[]
struct CurrentRequestTypeU5BU5D_t2385140839;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>
struct KeyCollection_t3563200854;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3842368571;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>
struct Transform_1_t2737799006;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>
struct KeyCollection_t4180772701;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>
struct Transform_1_t3322033499;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Reflection.Emit.Label>
struct KeyCollection_t1439558770;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Int32>
struct Transform_1_t4198889554;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>
struct KeyCollection_t1249926325;
// System.Collections.Generic.IEnumerator`1<System.Int64>
struct IEnumerator_1_t2679569160;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int64>
struct Transform_1_t3927277168;
// System.Int64[]
struct Int64U5BU5D_t717125112;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>
struct KeyCollection_t1867498172;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Int64>
struct Transform_1_t216544365;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>
struct KeyCollection_t702433241;
// System.Collections.Generic.IEnumerator`1<System.IntPtr>
struct IEnumerator_1_t4274551732;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.IntPtr>
struct Transform_1_t1584656104;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>
struct KeyCollection_t1320005088;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.IntPtr>
struct Transform_1_t2168890597;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>
struct KeyCollection_t1606165321;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Boolean,System.Object>
struct Transform_1_t1854428742;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>
struct KeyCollection_t4147435347;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int32,System.Object>
struct Transform_1_t1316794068;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int64>
struct KeyCollection_t2984635936;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Int64,System.Object>
struct Transform_1_t3298486835;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Nullable`1<System.Int32>>
struct KeyCollection_t2410501662;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Nullable`1<System.Int32>,System.Object>
struct Transform_1_t67869821;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
struct KeyCollection_t470039898;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>
struct Transform_1_t1901028561;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>
struct KeyCollection_t919388301;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.TextEditor/TextEditOp,System.Object>
struct Transform_1_t4054574962;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>
struct ShimEnumerator_t3729623560;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>
struct ShimEnumerator_t1157140949;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>
struct ShimEnumerator_t1184827904;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>
struct ShimEnumerator_t1802399751;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Reflection.Emit.Label>
struct ShimEnumerator_t3356153116;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Int32>
struct ShimEnumerator_t3166520671;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>
struct ShimEnumerator_t3784092518;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Int32>
struct ShimEnumerator_t2619027587;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>
struct ShimEnumerator_t3236599434;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>
struct ShimEnumerator_t3522759667;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>
struct ShimEnumerator_t1769062397;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>
struct ShimEnumerator_t606262986;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Nullable`1<System.Int32>>
struct ShimEnumerator_t32128712;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
struct ShimEnumerator_t2386634244;
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
struct ShimEnumerator_t2835982647;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t4102546548;
// System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>
struct Transform_1_t2435515111;
// System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Object>
struct Transform_1_t3743120445;
// System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t234441427;
// System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>
struct Transform_1_t289894675;
// System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Object>
struct Transform_1_t4169982620;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t3714796956;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct Transform_1_t3797937159;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t4064153;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct Transform_1_t704776203;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>
struct Transform_1_t3939605346;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.DictionaryEntry>
struct Transform_1_t880920208;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>
struct Transform_1_t3135385623;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Reflection.Emit.Label>
struct Transform_1_t2075247470;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t1772107233;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>>
struct Transform_1_t3836940203;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int32>
struct Transform_1_t795109283;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t2356341726;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>
struct Transform_1_t743779247;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Object>
struct Transform_1_t1996915623;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.DictionaryEntry>
struct Transform_1_t2129470893;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>>
struct Transform_1_t3646810779;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Int32>
struct Transform_1_t1152472943;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t2713705386;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>
struct Transform_1_t553649823;
// System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Object>
struct Transform_1_t2354279283;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2511286993.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2511286993MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Activator1850728717MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2110548107.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2110548107MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UILineInfo3621277874.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen94267937.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen94267937MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3988496347.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def3988496347MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716698.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716698MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977812.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977812MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716699.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716699MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977813.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977813MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716700.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1133716700MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977814.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa732977814MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En649556145.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En649556145MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3624498739.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21381843961.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21381843961MethodDeclarations.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelIden1147162267.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Link2723257478.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "mscorlib_System_ObjectDisposedException2695136451MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException2695136451.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2372040830.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2372040830MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1052016128.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23104328646.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23104328646MethodDeclarations.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2399727785.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2399727785MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1079703083.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1697274930.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En276085701.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En276085701MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3251028295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21008373517.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21008373517MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_Label4243202660.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enu86453256.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enu86453256MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3061395850.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_818741072.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_818741072MethodDeclarations.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En704025103.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En704025103MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3678967697.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21436312919.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21436312919MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3833927468.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3833927468MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2513902766.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_271247988.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_271247988MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En156532019.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En156532019MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3131474613.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_888819835.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_888819835MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En442692252.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En442692252MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3417634846.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2983962278.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2983962278MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1663937576.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1821162867.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1821162867MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge501138165.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22553450683.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22553450683MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1247028593.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1247028593MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4221971187.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21979316409.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21979316409MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4050882528.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4050882528MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2730857826.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_488203048.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_488203048MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp3138797698.h"
#include "UnityEngine_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2019034881.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2019034881MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3624498739MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3741519566.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3741519566MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1052016128MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3769206521.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3769206521MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1079703083MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key91811072.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key91811072MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1697274930MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1645564437.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1645564437MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3251028295MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1455931992.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1455931992MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3061395850MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2073503839.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2073503839MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3678967697MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke908438908.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke908438908MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2513902766MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1526010755.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1526010755MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3131474613MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1812170988.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1812170988MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3417634846MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key58473718.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Key58473718MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1663937576MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3190641603.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3190641603MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge501138165MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2616507329.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2616507329MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4221971187MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke676045565.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke676045565MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1125393968.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1125393968MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2730857826MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1813029214.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1813029214MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2200833417.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2200833417MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3535513899.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3535513899MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr435794015.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr435794015MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3563200854.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3563200854MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2737799006.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2737799006MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4180772701.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4180772701MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3322033499.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3322033499MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1439558770.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1439558770MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4198889554.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4198889554MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1249926325.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1249926325MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3927277168.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3927277168MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1867498172.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1867498172MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr216544365.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr216544365MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke702433241.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke702433241MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1584656104.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1584656104MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1320005088.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1320005088MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2168890597.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2168890597MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1606165321.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1606165321MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1854428742.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1854428742MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4147435347.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K4147435347MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1316794068.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1316794068MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2984635936.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2984635936MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3298486835.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3298486835MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2410501662.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2410501662MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tra67869821.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tra67869821MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke470039898.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke470039898MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1901028561.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1901028561MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke919388301.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke919388301MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4054574962.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4054574962MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3729623560.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3729623560MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1157140949.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1157140949MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1184827904.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1184827904MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1802399751.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1802399751MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3356153116.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3356153116MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3166520671.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3166520671MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3784092518.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3784092518MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2619027587.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2619027587MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3236599434.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3236599434MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3522759667.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S3522759667MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1769062397.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S1769062397MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Sh606262986.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Sh606262986MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Shi32128712.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Shi32128712MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2386634244.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2386634244MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2835982647.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_S2835982647MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4102546548.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4102546548MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2435515111.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2435515111MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3743120445.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3743120445MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr234441427.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr234441427MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr289894675.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr289894675MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4169982620.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4169982620MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3714796956.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3714796956MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3797937159.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3797937159MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tran4064153.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tran4064153MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr704776203.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr704776203MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3939605346.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3939605346MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr880920208.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr880920208MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3135385623.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3135385623MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2075247470.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2075247470MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1772107233.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1772107233MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3836940203.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3836940203MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr795109283.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr795109283MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2356341726.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2356341726MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr743779247.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr743779247MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1996915623.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1996915623MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2129470893.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2129470893MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3646810779.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3646810779MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1152472943.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1152472943MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2713705386.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2713705386MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr553649823.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr553649823MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2354279283.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2354279283MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Do_ICollectionCopyTo<PubNubMessaging.Core.ChannelIdentity>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisChannelIdentity_t1147162267_m791451731_gshared (Dictionary_2_t3624498739 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t2200833417 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisChannelIdentity_t1147162267_m791451731(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3624498739 *, Il2CppArray *, int32_t, Transform_1_t2200833417 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisChannelIdentity_t1147162267_m791451731_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Do_CopyTo<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelIdentity>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisChannelIdentity_t1147162267_TisChannelIdentity_t1147162267_m112022495_gshared (Dictionary_2_t3624498739 * __this, ChannelIdentityU5BU5D_t662656154* p0, int32_t p1, Transform_1_t2200833417 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisChannelIdentity_t1147162267_TisChannelIdentity_t1147162267_m112022495(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3624498739 *, ChannelIdentityU5BU5D_t662656154*, int32_t, Transform_1_t2200833417 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisChannelIdentity_t1147162267_TisChannelIdentity_t1147162267_m112022495_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Do_ICollectionCopyTo<PubNubMessaging.Core.CurrentRequestType>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisCurrentRequestType_t3250227986_m4004848675_gshared (Dictionary_2_t1052016128 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t435794015 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisCurrentRequestType_t3250227986_m4004848675(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1052016128 *, Il2CppArray *, int32_t, Transform_1_t435794015 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisCurrentRequestType_t3250227986_m4004848675_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Do_CopyTo<PubNubMessaging.Core.CurrentRequestType,PubNubMessaging.Core.CurrentRequestType>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisCurrentRequestType_t3250227986_TisCurrentRequestType_t3250227986_m2859524600_gshared (Dictionary_2_t1052016128 * __this, CurrentRequestTypeU5BU5D_t2385140839* p0, int32_t p1, Transform_1_t435794015 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisCurrentRequestType_t3250227986_TisCurrentRequestType_t3250227986_m2859524600(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1052016128 *, CurrentRequestTypeU5BU5D_t2385140839*, int32_t, Transform_1_t435794015 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisCurrentRequestType_t3250227986_TisCurrentRequestType_t3250227986_m2859524600_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Do_ICollectionCopyTo<System.Int32>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m4216657666_gshared (Dictionary_2_t1079703083 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t2737799006 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m4216657666(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1079703083 *, Il2CppArray *, int32_t, Transform_1_t2737799006 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m4216657666_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Do_CopyTo<System.Int32,System.Int32>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m2922514531_gshared (Dictionary_2_t1079703083 * __this, Int32U5BU5D_t3030399641* p0, int32_t p1, Transform_1_t2737799006 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m2922514531(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1079703083 *, Int32U5BU5D_t3030399641*, int32_t, Transform_1_t2737799006 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m2922514531_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Do_ICollectionCopyTo<System.Int32>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m1526703923_gshared (Dictionary_2_t1697274930 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t3322033499 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m1526703923(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1697274930 *, Il2CppArray *, int32_t, Transform_1_t3322033499 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m1526703923_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Do_CopyTo<System.Int32,System.Int32>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m838060550_gshared (Dictionary_2_t1697274930 * __this, Int32U5BU5D_t3030399641* p0, int32_t p1, Transform_1_t3322033499 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m838060550(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1697274930 *, Int32U5BU5D_t3030399641*, int32_t, Transform_1_t3322033499 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m838060550_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Reflection.Emit.Label>::Do_ICollectionCopyTo<System.Int32>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m3626415296_gshared (Dictionary_2_t3251028295 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t4198889554 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m3626415296(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3251028295 *, Il2CppArray *, int32_t, Transform_1_t4198889554 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisInt32_t2071877448_m3626415296_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Reflection.Emit.Label>::Do_CopyTo<System.Int32,System.Int32>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m2881859625_gshared (Dictionary_2_t3251028295 * __this, Int32U5BU5D_t3030399641* p0, int32_t p1, Transform_1_t4198889554 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m2881859625(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3251028295 *, Int32U5BU5D_t3030399641*, int32_t, Transform_1_t4198889554 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisInt32_t2071877448_TisInt32_t2071877448_m2881859625_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::Do_ICollectionCopyTo<System.Int64>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisInt64_t909078037_m1273217416_gshared (Dictionary_2_t3061395850 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t3927277168 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisInt64_t909078037_m1273217416(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3061395850 *, Il2CppArray *, int32_t, Transform_1_t3927277168 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisInt64_t909078037_m1273217416_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Int32>::Do_CopyTo<System.Int64,System.Int64>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisInt64_t909078037_TisInt64_t909078037_m550907210_gshared (Dictionary_2_t3061395850 * __this, Int64U5BU5D_t717125112* p0, int32_t p1, Transform_1_t3927277168 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisInt64_t909078037_TisInt64_t909078037_m550907210(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3061395850 *, Int64U5BU5D_t717125112*, int32_t, Transform_1_t3927277168 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisInt64_t909078037_TisInt64_t909078037_m550907210_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Object>::Do_ICollectionCopyTo<System.Int64>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisInt64_t909078037_m546108627_gshared (Dictionary_2_t3678967697 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t216544365 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisInt64_t909078037_m546108627(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3678967697 *, Il2CppArray *, int32_t, Transform_1_t216544365 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisInt64_t909078037_m546108627_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int64,System.Object>::Do_CopyTo<System.Int64,System.Int64>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisInt64_t909078037_TisInt64_t909078037_m2066450503_gshared (Dictionary_2_t3678967697 * __this, Int64U5BU5D_t717125112* p0, int32_t p1, Transform_1_t216544365 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisInt64_t909078037_TisInt64_t909078037_m2066450503(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3678967697 *, Int64U5BU5D_t717125112*, int32_t, Transform_1_t216544365 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisInt64_t909078037_TisInt64_t909078037_m2066450503_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::Do_ICollectionCopyTo<System.IntPtr>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIntPtr_t_m795684040_gshared (Dictionary_2_t2513902766 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t1584656104 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIntPtr_t_m795684040(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2513902766 *, Il2CppArray *, int32_t, Transform_1_t1584656104 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIntPtr_t_m795684040_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>::Do_CopyTo<System.IntPtr,System.IntPtr>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIntPtr_t_TisIntPtr_t_m2652456998_gshared (Dictionary_2_t2513902766 * __this, IntPtrU5BU5D_t169632028* p0, int32_t p1, Transform_1_t1584656104 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIntPtr_t_TisIntPtr_t_m2652456998(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2513902766 *, IntPtrU5BU5D_t169632028*, int32_t, Transform_1_t1584656104 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIntPtr_t_TisIntPtr_t_m2652456998_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::Do_ICollectionCopyTo<System.IntPtr>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIntPtr_t_m3403843327_gshared (Dictionary_2_t3131474613 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t2168890597 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIntPtr_t_m3403843327(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3131474613 *, Il2CppArray *, int32_t, Transform_1_t2168890597 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIntPtr_t_m3403843327_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::Do_CopyTo<System.IntPtr,System.IntPtr>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIntPtr_t_TisIntPtr_t_m3955064935_gshared (Dictionary_2_t3131474613 * __this, IntPtrU5BU5D_t169632028* p0, int32_t p1, Transform_1_t2168890597 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIntPtr_t_TisIntPtr_t_m3955064935(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3131474613 *, IntPtrU5BU5D_t169632028*, int32_t, Transform_1_t2168890597 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIntPtr_t_TisIntPtr_t_m3955064935_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1664293864_gshared (Dictionary_2_t3417634846 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t1854428742 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1664293864(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3417634846 *, Il2CppArray *, int32_t, Transform_1_t1854428742 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1664293864_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3704460300_gshared (Dictionary_2_t3417634846 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t1854428742 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3704460300(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t3417634846 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1854428742 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3704460300_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m63223048_gshared (Dictionary_2_t1663937576 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t1316794068 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m63223048(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1663937576 *, Il2CppArray *, int32_t, Transform_1_t1316794068 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m63223048_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3552744036_gshared (Dictionary_2_t1663937576 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t1316794068 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3552744036(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t1663937576 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1316794068 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m3552744036_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m2575996027_gshared (Dictionary_2_t501138165 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t3298486835 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m2575996027(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t501138165 *, Il2CppArray *, int32_t, Transform_1_t3298486835 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m2575996027_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m622838351_gshared (Dictionary_2_t501138165 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t3298486835 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m622838351(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t501138165 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t3298486835 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m622838351_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Nullable`1<System.Int32>>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1016488693_gshared (Dictionary_2_t4221971187 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t67869821 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1016488693(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t4221971187 *, Il2CppArray *, int32_t, Transform_1_t67869821 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m1016488693_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Nullable`1<System.Int32>>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m2749704489_gshared (Dictionary_2_t4221971187 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t67869821 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m2749704489(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t4221971187 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t67869821 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m2749704489_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m4092802079_gshared (Dictionary_2_t2281509423 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t1901028561 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m4092802079(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2281509423 *, Il2CppArray *, int32_t, Transform_1_t1901028561 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m4092802079_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m1804181923_gshared (Dictionary_2_t2281509423 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t1901028561 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m1804181923(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2281509423 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1901028561 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m1804181923_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Do_ICollectionCopyTo<System.Object>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m3006115099_gshared (Dictionary_2_t2730857826 * __this, Il2CppArray * p0, int32_t p1, Transform_1_t4054574962 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m3006115099(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2730857826 *, Il2CppArray *, int32_t, Transform_1_t4054574962 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisIl2CppObject_m3006115099_gshared)(__this, p0, p1, p2, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Do_CopyTo<System.Object,System.Object>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C"  void Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m2739099807_gshared (Dictionary_2_t2730857826 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, Transform_1_t4054574962 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m2739099807(__this, p0, p1, p2, method) ((  void (*) (Dictionary_2_t2730857826 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t4054574962 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisIl2CppObject_TisIl2CppObject_m2739099807_gshared)(__this, p0, p1, p2, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::.ctor()
extern "C"  void Comparer_1__ctor_m806168336_gshared (Comparer_1_t2511286993 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m3996541505_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m3996541505_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m3996541505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(91 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t2511286993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t2511286993 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2110548107 * L_8 = (DefaultComparer_t2110548107 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2110548107 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t2511286993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2964757477_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2964757477_gshared (Comparer_1_t2511286993 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2964757477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t2511286993 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, UILineInfo_t3621277874 , UILineInfo_t3621277874  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::Compare(T,T) */, (Comparer_1_t2511286993 *)__this, (UILineInfo_t3621277874 )((*(UILineInfo_t3621277874 *)((UILineInfo_t3621277874 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (UILineInfo_t3621277874 )((*(UILineInfo_t3621277874 *)((UILineInfo_t3621277874 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::get_Default()
extern "C"  Comparer_1_t2511286993 * Comparer_1_get_Default_m501796660_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t2511286993 * L_0 = ((Comparer_1_t2511286993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::.ctor()
extern "C"  void Comparer_1__ctor_m1157133632_gshared (Comparer_1_t94267937 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m4067993089_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m4067993089_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m4067993089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(91 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t94267937_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t94267937 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t3988496347 * L_8 = (DefaultComparer_t3988496347 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t3988496347 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t94267937_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2324509253_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2324509253_gshared (Comparer_1_t94267937 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2324509253_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t94267937 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, UIVertex_t1204258818 , UIVertex_t1204258818  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::Compare(T,T) */, (Comparer_1_t94267937 *)__this, (UIVertex_t1204258818 )((*(UIVertex_t1204258818 *)((UIVertex_t1204258818 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (UIVertex_t1204258818 )((*(UIVertex_t1204258818 *)((UIVertex_t1204258818 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::get_Default()
extern "C"  Comparer_1_t94267937 * Comparer_1_get_Default_m1960140044_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t94267937 * L_0 = ((Comparer_1_t94267937_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::.ctor()
extern "C"  void Comparer_1__ctor_m2941434245_gshared (Comparer_1_t1133716698 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m2253684996_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m2253684996_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m2253684996_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(91 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1133716698_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1133716698 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t732977812 * L_8 = (DefaultComparer_t732977812 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t732977812 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1133716698_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m637596782_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m637596782_gshared (Comparer_1_t1133716698 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m637596782_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1133716698 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Vector2_t2243707579 , Vector2_t2243707579  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::Compare(T,T) */, (Comparer_1_t1133716698 *)__this, (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Vector2>::get_Default()
extern "C"  Comparer_1_t1133716698 * Comparer_1_get_Default_m492688901_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1133716698 * L_0 = ((Comparer_1_t1133716698_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::.ctor()
extern "C"  void Comparer_1__ctor_m1169723274_gshared (Comparer_1_t1133716699 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m1573451391_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m1573451391_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m1573451391_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(91 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1133716699_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1133716699 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t732977813 * L_8 = (DefaultComparer_t732977813 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t732977813 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1133716699_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2615431023_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2615431023_gshared (Comparer_1_t1133716699 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2615431023_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1133716699 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Vector3_t2243707580 , Vector3_t2243707580  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::Compare(T,T) */, (Comparer_1_t1133716699 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::get_Default()
extern "C"  Comparer_1_t1133716699 * Comparer_1_get_Default_m3185432070_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1133716699 * L_0 = ((Comparer_1_t1133716699_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::.ctor()
extern "C"  void Comparer_1__ctor_m4052560291_gshared (Comparer_1_t1133716700 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::.cctor()
extern const Il2CppType* GenericComparer_1_t1787398723_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m1911230094_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m1911230094_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m1911230094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(GenericComparer_1_t1787398723_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(91 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t1664964607*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1133716700_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1133716700 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t732977814 * L_8 = (DefaultComparer_t732977814 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t732977814 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1133716700_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m577428976_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m577428976_gshared (Comparer_1_t1133716700 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m577428976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1133716700 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Vector4_t2243707581 , Vector4_t2243707581  >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::Compare(T,T) */, (Comparer_1_t1133716700 *)__this, (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox (L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))), (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox (L_6, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Vector4>::get_Default()
extern "C"  Comparer_1_t1133716700 * Comparer_1_get_Default_m48739979_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1133716700 * L_0 = ((Comparer_1_t1133716700_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3217945947_gshared (Enumerator_t649556145 * __this, Dictionary_2_t3624498739 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t3624498739 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t3624498739 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3217945947_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3624498739 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t649556145 * _thisAdjusted = reinterpret_cast<Enumerator_t649556145 *>(__this + 1);
	Enumerator__ctor_m3217945947(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1131689836_gshared (Enumerator_t649556145 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1085273890((Enumerator_t649556145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1381843961  L_0 = (KeyValuePair_2_t1381843961 )__this->get_current_3();
		KeyValuePair_2_t1381843961  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1131689836_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t649556145 * _thisAdjusted = reinterpret_cast<Enumerator_t649556145 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1131689836(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1672675594_gshared (Enumerator_t649556145 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m87609553((Enumerator_t649556145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1672675594_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t649556145 * _thisAdjusted = reinterpret_cast<Enumerator_t649556145 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1672675594(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m636749937_gshared (Enumerator_t649556145 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1085273890((Enumerator_t649556145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1381843961 * L_0 = (KeyValuePair_2_t1381843961 *)__this->get_address_of_current_3();
		ChannelIdentity_t1147162267  L_1 = KeyValuePair_2_get_Key_m359652951((KeyValuePair_2_t1381843961 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		ChannelIdentity_t1147162267  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t1381843961 * L_4 = (KeyValuePair_2_t1381843961 *)__this->get_address_of_current_3();
		Il2CppObject * L_5 = KeyValuePair_2_get_Value_m2841136671((KeyValuePair_2_t1381843961 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m636749937_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t649556145 * _thisAdjusted = reinterpret_cast<Enumerator_t649556145 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m636749937(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m349546614_gshared (Enumerator_t649556145 * __this, const MethodInfo* method)
{
	{
		ChannelIdentity_t1147162267  L_0 = Enumerator_get_CurrentKey_m1881066775((Enumerator_t649556145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		ChannelIdentity_t1147162267  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m349546614_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t649556145 * _thisAdjusted = reinterpret_cast<Enumerator_t649556145 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m349546614(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3566584116_gshared (Enumerator_t649556145 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentValue_m3451592935((Enumerator_t649556145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3566584116_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t649556145 * _thisAdjusted = reinterpret_cast<Enumerator_t649556145 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3566584116(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2703835770_gshared (Enumerator_t649556145 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m1450064902((Enumerator_t649556145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t3624498739 * L_4 = (Dictionary_2_t3624498739 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t3624498739 * L_8 = (Dictionary_2_t3624498739 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ChannelIdentityU5BU5D_t662656154* L_9 = (ChannelIdentityU5BU5D_t662656154*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		ChannelIdentity_t1147162267  L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t3624498739 * L_13 = (Dictionary_2_t3624498739 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t1381843961  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m623995145(&L_18, (ChannelIdentity_t1147162267 )L_12, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t3624498739 * L_20 = (Dictionary_2_t3624498739 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2703835770_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t649556145 * _thisAdjusted = reinterpret_cast<Enumerator_t649556145 *>(__this + 1);
	return Enumerator_MoveNext_m2703835770(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t1381843961  Enumerator_get_Current_m1373918882_gshared (Enumerator_t649556145 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1381843961  L_0 = (KeyValuePair_2_t1381843961 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t1381843961  Enumerator_get_Current_m1373918882_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t649556145 * _thisAdjusted = reinterpret_cast<Enumerator_t649556145 *>(__this + 1);
	return Enumerator_get_Current_m1373918882(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_CurrentKey()
extern "C"  ChannelIdentity_t1147162267  Enumerator_get_CurrentKey_m1881066775_gshared (Enumerator_t649556145 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1085273890((Enumerator_t649556145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1381843961 * L_0 = (KeyValuePair_2_t1381843961 *)__this->get_address_of_current_3();
		ChannelIdentity_t1147162267  L_1 = KeyValuePair_2_get_Key_m359652951((KeyValuePair_2_t1381843961 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  ChannelIdentity_t1147162267  Enumerator_get_CurrentKey_m1881066775_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t649556145 * _thisAdjusted = reinterpret_cast<Enumerator_t649556145 *>(__this + 1);
	return Enumerator_get_CurrentKey_m1881066775(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3451592935_gshared (Enumerator_t649556145 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1085273890((Enumerator_t649556145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1381843961 * L_0 = (KeyValuePair_2_t1381843961 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m2841136671((KeyValuePair_2_t1381843961 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3451592935_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t649556145 * _thisAdjusted = reinterpret_cast<Enumerator_t649556145 *>(__this + 1);
	return Enumerator_get_CurrentValue_m3451592935(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m87609553_gshared (Enumerator_t649556145 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1450064902((Enumerator_t649556145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m87609553_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t649556145 * _thisAdjusted = reinterpret_cast<Enumerator_t649556145 *>(__this + 1);
	Enumerator_Reset_m87609553(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m1450064902_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1450064902_gshared (Enumerator_t649556145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1450064902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3624498739 * L_0 = (Dictionary_2_t3624498739 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t3624498739 * L_2 = (Dictionary_2_t3624498739 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1450064902_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t649556145 * _thisAdjusted = reinterpret_cast<Enumerator_t649556145 *>(__this + 1);
	Enumerator_VerifyState_m1450064902(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m1085273890_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m1085273890_gshared (Enumerator_t649556145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m1085273890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1450064902((Enumerator_t649556145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m1085273890_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t649556145 * _thisAdjusted = reinterpret_cast<Enumerator_t649556145 *>(__this + 1);
	Enumerator_VerifyCurrent_m1085273890(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m4288624127_gshared (Enumerator_t649556145 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t3624498739 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m4288624127_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t649556145 * _thisAdjusted = reinterpret_cast<Enumerator_t649556145 *>(__this + 1);
	Enumerator_Dispose_m4288624127(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m276105898_gshared (Enumerator_t2372040830 * __this, Dictionary_2_t1052016128 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t1052016128 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t1052016128 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m276105898_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1052016128 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t2372040830 * _thisAdjusted = reinterpret_cast<Enumerator_t2372040830 *>(__this + 1);
	Enumerator__ctor_m276105898(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m517611691_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3176930503((Enumerator_t2372040830 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3104328646  L_0 = (KeyValuePair_2_t3104328646 )__this->get_current_3();
		KeyValuePair_2_t3104328646  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m517611691_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2372040830 * _thisAdjusted = reinterpret_cast<Enumerator_t2372040830 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m517611691(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2745913687_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m1143911664((Enumerator_t2372040830 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2745913687_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2372040830 * _thisAdjusted = reinterpret_cast<Enumerator_t2372040830 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2745913687(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m388892492_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3176930503((Enumerator_t2372040830 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3104328646 * L_0 = (KeyValuePair_2_t3104328646 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m266456220((KeyValuePair_2_t3104328646 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t3104328646 * L_4 = (KeyValuePair_2_t3104328646 *)__this->get_address_of_current_3();
		Il2CppObject * L_5 = KeyValuePair_2_get_Value_m1416610908((KeyValuePair_2_t3104328646 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m388892492_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2372040830 * _thisAdjusted = reinterpret_cast<Enumerator_t2372040830 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m388892492(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3374398445_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentKey_m103879330((Enumerator_t2372040830 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3374398445_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2372040830 * _thisAdjusted = reinterpret_cast<Enumerator_t2372040830 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3374398445(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2248508013_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentValue_m1777489826((Enumerator_t2372040830 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2248508013_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2372040830 * _thisAdjusted = reinterpret_cast<Enumerator_t2372040830 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2248508013(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3717365715_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m1995051793((Enumerator_t2372040830 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1052016128 * L_4 = (Dictionary_2_t1052016128 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1052016128 * L_8 = (Dictionary_2_t1052016128 *)__this->get_dictionary_0();
		NullCheck(L_8);
		CurrentRequestTypeU5BU5D_t2385140839* L_9 = (CurrentRequestTypeU5BU5D_t2385140839*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t1052016128 * L_13 = (Dictionary_2_t1052016128 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t3104328646  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m3851400962(&L_18, (int32_t)L_12, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t1052016128 * L_20 = (Dictionary_2_t1052016128 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3717365715_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2372040830 * _thisAdjusted = reinterpret_cast<Enumerator_t2372040830 *>(__this + 1);
	return Enumerator_MoveNext_m3717365715(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t3104328646  Enumerator_get_Current_m2135551627_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3104328646  L_0 = (KeyValuePair_2_t3104328646 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t3104328646  Enumerator_get_Current_m2135551627_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2372040830 * _thisAdjusted = reinterpret_cast<Enumerator_t2372040830 *>(__this + 1);
	return Enumerator_get_Current_m2135551627(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m103879330_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3176930503((Enumerator_t2372040830 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3104328646 * L_0 = (KeyValuePair_2_t3104328646 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m266456220((KeyValuePair_2_t3104328646 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentKey_m103879330_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2372040830 * _thisAdjusted = reinterpret_cast<Enumerator_t2372040830 *>(__this + 1);
	return Enumerator_get_CurrentKey_m103879330(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1777489826_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3176930503((Enumerator_t2372040830 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3104328646 * L_0 = (KeyValuePair_2_t3104328646 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m1416610908((KeyValuePair_2_t3104328646 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1777489826_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2372040830 * _thisAdjusted = reinterpret_cast<Enumerator_t2372040830 *>(__this + 1);
	return Enumerator_get_CurrentValue_m1777489826(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m1143911664_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1995051793((Enumerator_t2372040830 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m1143911664_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2372040830 * _thisAdjusted = reinterpret_cast<Enumerator_t2372040830 *>(__this + 1);
	Enumerator_Reset_m1143911664(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m1995051793_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1995051793_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1995051793_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1052016128 * L_0 = (Dictionary_2_t1052016128 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t1052016128 * L_2 = (Dictionary_2_t1052016128 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1995051793_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2372040830 * _thisAdjusted = reinterpret_cast<Enumerator_t2372040830 *>(__this + 1);
	Enumerator_VerifyState_m1995051793(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m3176930503_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3176930503_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3176930503_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1995051793((Enumerator_t2372040830 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m3176930503_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2372040830 * _thisAdjusted = reinterpret_cast<Enumerator_t2372040830 *>(__this + 1);
	Enumerator_VerifyCurrent_m3176930503(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3484064310_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t1052016128 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3484064310_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2372040830 * _thisAdjusted = reinterpret_cast<Enumerator_t2372040830 *>(__this + 1);
	Enumerator_Dispose_m3484064310(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m648457671_gshared (Enumerator_t2399727785 * __this, Dictionary_2_t1079703083 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t1079703083 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m648457671_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1079703083 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	Enumerator__ctor_m648457671(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2741808790_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3667735842((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3132015601  L_0 = (KeyValuePair_2_t3132015601 )__this->get_current_3();
		KeyValuePair_2_t3132015601  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2741808790_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2741808790(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2861488778_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m2787013825((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2861488778_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2861488778(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1723581873_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3667735842((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3132015601 * L_0 = (KeyValuePair_2_t3132015601 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m1683812983((KeyValuePair_2_t3132015601 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t3132015601 * L_4 = (KeyValuePair_2_t3132015601 *)__this->get_address_of_current_3();
		int32_t L_5 = KeyValuePair_2_get_Value_m1136571287((KeyValuePair_2_t3132015601 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_6);
		DictionaryEntry_t3048875398  L_8;
		memset(&L_8, 0, sizeof(L_8));
		DictionaryEntry__ctor_m2901884110(&L_8, (Il2CppObject *)L_3, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1723581873_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1723581873(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4169232220_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentKey_m3410494779((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4169232220_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4169232220(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3772256406_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentValue_m2708916123((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3772256406_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3772256406(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3809319098_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m4145755786((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1079703083 * L_4 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1079703083 * L_8 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck(L_8);
		Int32U5BU5D_t3030399641* L_9 = (Int32U5BU5D_t3030399641*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t1079703083 * L_13 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck(L_13);
		Int32U5BU5D_t3030399641* L_14 = (Int32U5BU5D_t3030399641*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t3132015601  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m2177183229(&L_18, (int32_t)L_12, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t1079703083 * L_20 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3809319098_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_MoveNext_m3809319098(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t3132015601  Enumerator_get_Current_m3655806242_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3132015601  L_0 = (KeyValuePair_2_t3132015601 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t3132015601  Enumerator_get_Current_m3655806242_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_get_Current_m3655806242(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m3410494779_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3667735842((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3132015601 * L_0 = (KeyValuePair_2_t3132015601 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m1683812983((KeyValuePair_2_t3132015601 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentKey_m3410494779_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_get_CurrentKey_m3410494779(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m2708916123_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3667735842((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3132015601 * L_0 = (KeyValuePair_2_t3132015601 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Value_m1136571287((KeyValuePair_2_t3132015601 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentValue_m2708916123_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	return Enumerator_get_CurrentValue_m2708916123(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::Reset()
extern "C"  void Enumerator_Reset_m2787013825_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m4145755786((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m2787013825_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	Enumerator_Reset_m2787013825(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m4145755786_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m4145755786_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m4145755786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t1079703083 * L_2 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m4145755786_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	Enumerator_VerifyState_m4145755786(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m3667735842_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3667735842_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3667735842_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m4145755786((Enumerator_t2399727785 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m3667735842_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	Enumerator_VerifyCurrent_m3667735842(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m1631869499_gshared (Enumerator_t2399727785 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t1079703083 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1631869499_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2399727785 * _thisAdjusted = reinterpret_cast<Enumerator_t2399727785 *>(__this + 1);
	Enumerator_Dispose_m1631869499(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1702560852_gshared (Enumerator_t3017299632 * __this, Dictionary_2_t1697274930 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t1697274930 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1702560852_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1697274930 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator__ctor_m1702560852(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4246196125((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3749587448  L_0 = (KeyValuePair_2_t3749587448 )__this->get_current_3();
		KeyValuePair_2_t3749587448  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1631145297(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m761796566((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2828524109_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2828524109(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4246196125((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3749587448 * L_0 = (KeyValuePair_2_t3749587448 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t3749587448 * L_4 = (KeyValuePair_2_t3749587448 *)__this->get_address_of_current_3();
		Il2CppObject * L_5 = KeyValuePair_2_get_Value_m3690000728((KeyValuePair_2_t3749587448 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentKey_m447338908((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentValue_m3562053380((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2770956757_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m2118679243((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1697274930 * L_4 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1697274930 * L_8 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck(L_8);
		Int32U5BU5D_t3030399641* L_9 = (Int32U5BU5D_t3030399641*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t1697274930 * L_13 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t3749587448  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m3201181706(&L_18, (int32_t)L_12, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t1697274930 * L_20 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2770956757_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_MoveNext_m2770956757(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t3749587448  Enumerator_get_Current_m2230224741_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3749587448  L_0 = (KeyValuePair_2_t3749587448 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t3749587448  Enumerator_get_Current_m2230224741_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_get_Current_m2230224741(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m447338908_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4246196125((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3749587448 * L_0 = (KeyValuePair_2_t3749587448 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentKey_m447338908_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_get_CurrentKey_m447338908(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3562053380_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m4246196125((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3749587448 * L_0 = (KeyValuePair_2_t3749587448 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m3690000728((KeyValuePair_2_t3749587448 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3562053380_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	return Enumerator_get_CurrentValue_m3562053380(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m761796566_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2118679243((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m761796566_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator_Reset_m761796566(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m2118679243_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2118679243_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2118679243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t1697274930 * L_2 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2118679243_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator_VerifyState_m2118679243(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m4246196125_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m4246196125_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m4246196125_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2118679243((Enumerator_t3017299632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m4246196125_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator_VerifyCurrent_m4246196125(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2243145188_gshared (Enumerator_t3017299632 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t1697274930 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2243145188_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3017299632 * _thisAdjusted = reinterpret_cast<Enumerator_t3017299632 *>(__this + 1);
	Enumerator_Dispose_m2243145188(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m496941373_gshared (Enumerator_t276085701 * __this, Dictionary_2_t3251028295 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t3251028295 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t3251028295 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m496941373_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3251028295 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t276085701 * _thisAdjusted = reinterpret_cast<Enumerator_t276085701 *>(__this + 1);
	Enumerator__ctor_m496941373(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m984328696_gshared (Enumerator_t276085701 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1931981752((Enumerator_t276085701 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1008373517  L_0 = (KeyValuePair_2_t1008373517 )__this->get_current_3();
		KeyValuePair_2_t1008373517  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m984328696_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t276085701 * _thisAdjusted = reinterpret_cast<Enumerator_t276085701 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m984328696(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m222673160_gshared (Enumerator_t276085701 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m2015547391((Enumerator_t276085701 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m222673160_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t276085701 * _thisAdjusted = reinterpret_cast<Enumerator_t276085701 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m222673160(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1762091339_gshared (Enumerator_t276085701 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1931981752((Enumerator_t276085701 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1008373517 * L_0 = (KeyValuePair_2_t1008373517 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m2216667373((KeyValuePair_2_t1008373517 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t1008373517 * L_4 = (KeyValuePair_2_t1008373517 *)__this->get_address_of_current_3();
		Label_t4243202660  L_5 = KeyValuePair_2_get_Value_m590593229((KeyValuePair_2_t1008373517 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Label_t4243202660  L_6 = L_5;
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_6);
		DictionaryEntry_t3048875398  L_8;
		memset(&L_8, 0, sizeof(L_8));
		DictionaryEntry__ctor_m2901884110(&L_8, (Il2CppObject *)L_3, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1762091339_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t276085701 * _thisAdjusted = reinterpret_cast<Enumerator_t276085701 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1762091339(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1625582782_gshared (Enumerator_t276085701 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentKey_m2503592769((Enumerator_t276085701 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1625582782_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t276085701 * _thisAdjusted = reinterpret_cast<Enumerator_t276085701 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1625582782(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1261096000_gshared (Enumerator_t276085701 * __this, const MethodInfo* method)
{
	{
		Label_t4243202660  L_0 = Enumerator_get_CurrentValue_m3952904993((Enumerator_t276085701 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Label_t4243202660  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1261096000_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t276085701 * _thisAdjusted = reinterpret_cast<Enumerator_t276085701 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1261096000(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3018149748_gshared (Enumerator_t276085701 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m2737116096((Enumerator_t276085701 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t3251028295 * L_4 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t3251028295 * L_8 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		NullCheck(L_8);
		Int32U5BU5D_t3030399641* L_9 = (Int32U5BU5D_t3030399641*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t3251028295 * L_13 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		NullCheck(L_13);
		LabelU5BU5D_t196522893* L_14 = (LabelU5BU5D_t196522893*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Label_t4243202660  L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t1008373517  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m1870434531(&L_18, (int32_t)L_12, (Label_t4243202660 )L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t3251028295 * L_20 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3018149748_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t276085701 * _thisAdjusted = reinterpret_cast<Enumerator_t276085701 *>(__this + 1);
	return Enumerator_MoveNext_m3018149748(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::get_Current()
extern "C"  KeyValuePair_2_t1008373517  Enumerator_get_Current_m3130898880_gshared (Enumerator_t276085701 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1008373517  L_0 = (KeyValuePair_2_t1008373517 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t1008373517  Enumerator_get_Current_m3130898880_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t276085701 * _thisAdjusted = reinterpret_cast<Enumerator_t276085701 *>(__this + 1);
	return Enumerator_get_Current_m3130898880(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m2503592769_gshared (Enumerator_t276085701 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1931981752((Enumerator_t276085701 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1008373517 * L_0 = (KeyValuePair_2_t1008373517 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Key_m2216667373((KeyValuePair_2_t1008373517 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentKey_m2503592769_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t276085701 * _thisAdjusted = reinterpret_cast<Enumerator_t276085701 *>(__this + 1);
	return Enumerator_get_CurrentKey_m2503592769(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::get_CurrentValue()
extern "C"  Label_t4243202660  Enumerator_get_CurrentValue_m3952904993_gshared (Enumerator_t276085701 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1931981752((Enumerator_t276085701 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1008373517 * L_0 = (KeyValuePair_2_t1008373517 *)__this->get_address_of_current_3();
		Label_t4243202660  L_1 = KeyValuePair_2_get_Value_m590593229((KeyValuePair_2_t1008373517 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Label_t4243202660  Enumerator_get_CurrentValue_m3952904993_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t276085701 * _thisAdjusted = reinterpret_cast<Enumerator_t276085701 *>(__this + 1);
	return Enumerator_get_CurrentValue_m3952904993(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::Reset()
extern "C"  void Enumerator_Reset_m2015547391_gshared (Enumerator_t276085701 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2737116096((Enumerator_t276085701 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m2015547391_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t276085701 * _thisAdjusted = reinterpret_cast<Enumerator_t276085701 *>(__this + 1);
	Enumerator_Reset_m2015547391(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m2737116096_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2737116096_gshared (Enumerator_t276085701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2737116096_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3251028295 * L_0 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t3251028295 * L_2 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2737116096_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t276085701 * _thisAdjusted = reinterpret_cast<Enumerator_t276085701 *>(__this + 1);
	Enumerator_VerifyState_m2737116096(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m1931981752_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m1931981752_gshared (Enumerator_t276085701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m1931981752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2737116096((Enumerator_t276085701 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m1931981752_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t276085701 * _thisAdjusted = reinterpret_cast<Enumerator_t276085701 *>(__this + 1);
	Enumerator_VerifyCurrent_m1931981752(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::Dispose()
extern "C"  void Enumerator_Dispose_m2214493861_gshared (Enumerator_t276085701 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t3251028295 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2214493861_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t276085701 * _thisAdjusted = reinterpret_cast<Enumerator_t276085701 *>(__this + 1);
	Enumerator_Dispose_m2214493861(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3025226368_gshared (Enumerator_t86453256 * __this, Dictionary_2_t3061395850 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t3061395850 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t3061395850 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3025226368_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3061395850 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t86453256 * _thisAdjusted = reinterpret_cast<Enumerator_t86453256 *>(__this + 1);
	Enumerator__ctor_m3025226368(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2591061757_gshared (Enumerator_t86453256 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2643482197((Enumerator_t86453256 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t818741072  L_0 = (KeyValuePair_2_t818741072 )__this->get_current_3();
		KeyValuePair_2_t818741072  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2591061757_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t86453256 * _thisAdjusted = reinterpret_cast<Enumerator_t86453256 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2591061757(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2645290253_gshared (Enumerator_t86453256 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m199703458((Enumerator_t86453256 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2645290253_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t86453256 * _thisAdjusted = reinterpret_cast<Enumerator_t86453256 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2645290253(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2076077042_gshared (Enumerator_t86453256 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2643482197((Enumerator_t86453256 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t818741072 * L_0 = (KeyValuePair_2_t818741072 *)__this->get_address_of_current_3();
		int64_t L_1 = KeyValuePair_2_get_Key_m4268935818((KeyValuePair_2_t818741072 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int64_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t818741072 * L_4 = (KeyValuePair_2_t818741072 *)__this->get_address_of_current_3();
		int32_t L_5 = KeyValuePair_2_get_Value_m3901703242((KeyValuePair_2_t818741072 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_6);
		DictionaryEntry_t3048875398  L_8;
		memset(&L_8, 0, sizeof(L_8));
		DictionaryEntry__ctor_m2901884110(&L_8, (Il2CppObject *)L_3, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2076077042_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t86453256 * _thisAdjusted = reinterpret_cast<Enumerator_t86453256 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2076077042(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2928849115_gshared (Enumerator_t86453256 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = Enumerator_get_CurrentKey_m3844258184((Enumerator_t86453256 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2928849115_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t86453256 * _thisAdjusted = reinterpret_cast<Enumerator_t86453256 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2928849115(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2052459099_gshared (Enumerator_t86453256 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentValue_m1182545736((Enumerator_t86453256 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2052459099_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t86453256 * _thisAdjusted = reinterpret_cast<Enumerator_t86453256 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2052459099(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3282539621_gshared (Enumerator_t86453256 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m1919460771((Enumerator_t86453256 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t3061395850 * L_4 = (Dictionary_2_t3061395850 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t3061395850 * L_8 = (Dictionary_2_t3061395850 *)__this->get_dictionary_0();
		NullCheck(L_8);
		Int64U5BU5D_t717125112* L_9 = (Int64U5BU5D_t717125112*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int64_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t3061395850 * L_13 = (Dictionary_2_t3061395850 *)__this->get_dictionary_0();
		NullCheck(L_13);
		Int32U5BU5D_t3030399641* L_14 = (Int32U5BU5D_t3030399641*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t818741072  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m1716701620(&L_18, (int64_t)L_12, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t3061395850 * L_20 = (Dictionary_2_t3061395850 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3282539621_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t86453256 * _thisAdjusted = reinterpret_cast<Enumerator_t86453256 *>(__this + 1);
	return Enumerator_MoveNext_m3282539621(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t818741072  Enumerator_get_Current_m2886623121_gshared (Enumerator_t86453256 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t818741072  L_0 = (KeyValuePair_2_t818741072 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t818741072  Enumerator_get_Current_m2886623121_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t86453256 * _thisAdjusted = reinterpret_cast<Enumerator_t86453256 *>(__this + 1);
	return Enumerator_get_Current_m2886623121(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::get_CurrentKey()
extern "C"  int64_t Enumerator_get_CurrentKey_m3844258184_gshared (Enumerator_t86453256 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2643482197((Enumerator_t86453256 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t818741072 * L_0 = (KeyValuePair_2_t818741072 *)__this->get_address_of_current_3();
		int64_t L_1 = KeyValuePair_2_get_Key_m4268935818((KeyValuePair_2_t818741072 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  int64_t Enumerator_get_CurrentKey_m3844258184_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t86453256 * _thisAdjusted = reinterpret_cast<Enumerator_t86453256 *>(__this + 1);
	return Enumerator_get_CurrentKey_m3844258184(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m1182545736_gshared (Enumerator_t86453256 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2643482197((Enumerator_t86453256 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t818741072 * L_0 = (KeyValuePair_2_t818741072 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Value_m3901703242((KeyValuePair_2_t818741072 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentValue_m1182545736_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t86453256 * _thisAdjusted = reinterpret_cast<Enumerator_t86453256 *>(__this + 1);
	return Enumerator_get_CurrentValue_m1182545736(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::Reset()
extern "C"  void Enumerator_Reset_m199703458_gshared (Enumerator_t86453256 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1919460771((Enumerator_t86453256 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m199703458_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t86453256 * _thisAdjusted = reinterpret_cast<Enumerator_t86453256 *>(__this + 1);
	Enumerator_Reset_m199703458(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m1919460771_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1919460771_gshared (Enumerator_t86453256 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1919460771_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3061395850 * L_0 = (Dictionary_2_t3061395850 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t3061395850 * L_2 = (Dictionary_2_t3061395850 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1919460771_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t86453256 * _thisAdjusted = reinterpret_cast<Enumerator_t86453256 *>(__this + 1);
	Enumerator_VerifyState_m1919460771(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m2643482197_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m2643482197_gshared (Enumerator_t86453256 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m2643482197_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1919460771((Enumerator_t86453256 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m2643482197_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t86453256 * _thisAdjusted = reinterpret_cast<Enumerator_t86453256 *>(__this + 1);
	Enumerator_VerifyCurrent_m2643482197(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m3531528168_gshared (Enumerator_t86453256 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t3061395850 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3531528168_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t86453256 * _thisAdjusted = reinterpret_cast<Enumerator_t86453256 *>(__this + 1);
	Enumerator_Dispose_m3531528168(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3045959731_gshared (Enumerator_t704025103 * __this, Dictionary_2_t3678967697 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t3678967697 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t3678967697 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3045959731_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3678967697 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	Enumerator__ctor_m3045959731(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m524777314_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1011807396((Enumerator_t704025103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1436312919  L_0 = (KeyValuePair_2_t1436312919 )__this->get_current_3();
		KeyValuePair_2_t1436312919  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m524777314_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m524777314(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3684036852_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m598725905((Enumerator_t704025103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3684036852_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3684036852(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1310647545_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1011807396((Enumerator_t704025103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1436312919 * L_0 = (KeyValuePair_2_t1436312919 *)__this->get_address_of_current_3();
		int64_t L_1 = KeyValuePair_2_get_Key_m3157806383((KeyValuePair_2_t1436312919 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int64_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t1436312919 * L_4 = (KeyValuePair_2_t1436312919 *)__this->get_address_of_current_3();
		Il2CppObject * L_5 = KeyValuePair_2_get_Value_m1978486399((KeyValuePair_2_t1436312919 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1310647545_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1310647545(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m529281260_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = Enumerator_get_CurrentKey_m4167196999((Enumerator_t704025103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m529281260_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m529281260(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2302706078_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentValue_m659195263((Enumerator_t704025103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2302706078_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2302706078(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3139459564_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m3803534940((Enumerator_t704025103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t3678967697 * L_4 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t3678967697 * L_8 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		NullCheck(L_8);
		Int64U5BU5D_t717125112* L_9 = (Int64U5BU5D_t717125112*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int64_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t3678967697 * L_13 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t1436312919  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m1940188057(&L_18, (int64_t)L_12, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t3678967697 * L_20 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3139459564_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	return Enumerator_MoveNext_m3139459564(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t1436312919  Enumerator_get_Current_m3988231868_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1436312919  L_0 = (KeyValuePair_2_t1436312919 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t1436312919  Enumerator_get_Current_m3988231868_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	return Enumerator_get_Current_m3988231868(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::get_CurrentKey()
extern "C"  int64_t Enumerator_get_CurrentKey_m4167196999_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1011807396((Enumerator_t704025103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1436312919 * L_0 = (KeyValuePair_2_t1436312919 *)__this->get_address_of_current_3();
		int64_t L_1 = KeyValuePair_2_get_Key_m3157806383((KeyValuePair_2_t1436312919 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  int64_t Enumerator_get_CurrentKey_m4167196999_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	return Enumerator_get_CurrentKey_m4167196999(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m659195263_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1011807396((Enumerator_t704025103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1436312919 * L_0 = (KeyValuePair_2_t1436312919 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m1978486399((KeyValuePair_2_t1436312919 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m659195263_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	return Enumerator_get_CurrentValue_m659195263(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m598725905_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3803534940((Enumerator_t704025103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m598725905_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	Enumerator_Reset_m598725905(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m3803534940_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3803534940_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3803534940_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3678967697 * L_0 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t3678967697 * L_2 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3803534940_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	Enumerator_VerifyState_m3803534940(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m1011807396_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m1011807396_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m1011807396_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m3803534940((Enumerator_t704025103 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m1011807396_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	Enumerator_VerifyCurrent_m1011807396(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int64,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3446908287_gshared (Enumerator_t704025103 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t3678967697 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3446908287_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t704025103 * _thisAdjusted = reinterpret_cast<Enumerator_t704025103 *>(__this + 1);
	Enumerator_Dispose_m3446908287(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1446562946_gshared (Enumerator_t3833927468 * __this, Dictionary_2_t2513902766 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t2513902766 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t2513902766 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1446562946_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2513902766 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t3833927468 * _thisAdjusted = reinterpret_cast<Enumerator_t3833927468 *>(__this + 1);
	Enumerator__ctor_m1446562946(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2199743181_gshared (Enumerator_t3833927468 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3286417977((Enumerator_t3833927468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t271247988  L_0 = (KeyValuePair_2_t271247988 )__this->get_current_3();
		KeyValuePair_2_t271247988  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2199743181_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3833927468 * _thisAdjusted = reinterpret_cast<Enumerator_t3833927468 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2199743181(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1741774345_gshared (Enumerator_t3833927468 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m1791350284((Enumerator_t3833927468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1741774345_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3833927468 * _thisAdjusted = reinterpret_cast<Enumerator_t3833927468 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1741774345(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1601134310_gshared (Enumerator_t3833927468 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3286417977((Enumerator_t3833927468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t271247988 * L_0 = (KeyValuePair_2_t271247988 *)__this->get_address_of_current_3();
		IntPtr_t L_1 = KeyValuePair_2_get_Key_m3024592206((KeyValuePair_2_t271247988 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		IntPtr_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t271247988 * L_4 = (KeyValuePair_2_t271247988 *)__this->get_address_of_current_3();
		int32_t L_5 = KeyValuePair_2_get_Value_m2072891806((KeyValuePair_2_t271247988 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_6);
		DictionaryEntry_t3048875398  L_8;
		memset(&L_8, 0, sizeof(L_8));
		DictionaryEntry__ctor_m2901884110(&L_8, (Il2CppObject *)L_3, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1601134310_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3833927468 * _thisAdjusted = reinterpret_cast<Enumerator_t3833927468 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1601134310(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1520541099_gshared (Enumerator_t3833927468 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = Enumerator_get_CurrentKey_m1901550810((Enumerator_t3833927468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		IntPtr_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1520541099_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3833927468 * _thisAdjusted = reinterpret_cast<Enumerator_t3833927468 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1520541099(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m29311651_gshared (Enumerator_t3833927468 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentValue_m2248732114((Enumerator_t3833927468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m29311651_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3833927468 * _thisAdjusted = reinterpret_cast<Enumerator_t3833927468 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m29311651(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3274679113_gshared (Enumerator_t3833927468 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m1601720759((Enumerator_t3833927468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t2513902766 * L_4 = (Dictionary_2_t2513902766 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t2513902766 * L_8 = (Dictionary_2_t2513902766 *)__this->get_dictionary_0();
		NullCheck(L_8);
		IntPtrU5BU5D_t169632028* L_9 = (IntPtrU5BU5D_t169632028*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		IntPtr_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t2513902766 * L_13 = (Dictionary_2_t2513902766 *)__this->get_dictionary_0();
		NullCheck(L_13);
		Int32U5BU5D_t3030399641* L_14 = (Int32U5BU5D_t3030399641*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t271247988  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m649163880(&L_18, (IntPtr_t)L_12, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t2513902766 * L_20 = (Dictionary_2_t2513902766 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3274679113_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3833927468 * _thisAdjusted = reinterpret_cast<Enumerator_t3833927468 *>(__this + 1);
	return Enumerator_MoveNext_m3274679113(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t271247988  Enumerator_get_Current_m1909749433_gshared (Enumerator_t3833927468 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t271247988  L_0 = (KeyValuePair_2_t271247988 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t271247988  Enumerator_get_Current_m1909749433_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3833927468 * _thisAdjusted = reinterpret_cast<Enumerator_t3833927468 *>(__this + 1);
	return Enumerator_get_Current_m1909749433(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::get_CurrentKey()
extern "C"  IntPtr_t Enumerator_get_CurrentKey_m1901550810_gshared (Enumerator_t3833927468 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3286417977((Enumerator_t3833927468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t271247988 * L_0 = (KeyValuePair_2_t271247988 *)__this->get_address_of_current_3();
		IntPtr_t L_1 = KeyValuePair_2_get_Key_m3024592206((KeyValuePair_2_t271247988 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  IntPtr_t Enumerator_get_CurrentKey_m1901550810_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3833927468 * _thisAdjusted = reinterpret_cast<Enumerator_t3833927468 *>(__this + 1);
	return Enumerator_get_CurrentKey_m1901550810(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m2248732114_gshared (Enumerator_t3833927468 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3286417977((Enumerator_t3833927468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t271247988 * L_0 = (KeyValuePair_2_t271247988 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Value_m2072891806((KeyValuePair_2_t271247988 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentValue_m2248732114_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3833927468 * _thisAdjusted = reinterpret_cast<Enumerator_t3833927468 *>(__this + 1);
	return Enumerator_get_CurrentValue_m2248732114(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::Reset()
extern "C"  void Enumerator_Reset_m1791350284_gshared (Enumerator_t3833927468 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1601720759((Enumerator_t3833927468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m1791350284_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3833927468 * _thisAdjusted = reinterpret_cast<Enumerator_t3833927468 *>(__this + 1);
	Enumerator_Reset_m1791350284(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m1601720759_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1601720759_gshared (Enumerator_t3833927468 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1601720759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2513902766 * L_0 = (Dictionary_2_t2513902766 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t2513902766 * L_2 = (Dictionary_2_t2513902766 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1601720759_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3833927468 * _thisAdjusted = reinterpret_cast<Enumerator_t3833927468 *>(__this + 1);
	Enumerator_VerifyState_m1601720759(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m3286417977_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3286417977_gshared (Enumerator_t3833927468 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3286417977_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1601720759((Enumerator_t3833927468 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m3286417977_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3833927468 * _thisAdjusted = reinterpret_cast<Enumerator_t3833927468 *>(__this + 1);
	Enumerator_VerifyCurrent_m3286417977(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m3738173754_gshared (Enumerator_t3833927468 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t2513902766 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3738173754_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3833927468 * _thisAdjusted = reinterpret_cast<Enumerator_t3833927468 *>(__this + 1);
	Enumerator_Dispose_m3738173754(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1897170543_gshared (Enumerator_t156532019 * __this, Dictionary_2_t3131474613 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t3131474613 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t3131474613 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1897170543_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3131474613 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	Enumerator__ctor_m1897170543(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2389891262_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m213906106((Enumerator_t156532019 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t888819835  L_0 = (KeyValuePair_2_t888819835 )__this->get_current_3();
		KeyValuePair_2_t888819835  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2389891262_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2389891262(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3302454658_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m1147555085((Enumerator_t156532019 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3302454658_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3302454658(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4077548725_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m213906106((Enumerator_t156532019 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t888819835 * L_0 = (KeyValuePair_2_t888819835 *)__this->get_address_of_current_3();
		IntPtr_t L_1 = KeyValuePair_2_get_Key_m1574332879((KeyValuePair_2_t888819835 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		IntPtr_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t888819835 * L_4 = (KeyValuePair_2_t888819835 *)__this->get_address_of_current_3();
		Il2CppObject * L_5 = KeyValuePair_2_get_Value_m544293807((KeyValuePair_2_t888819835 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4077548725_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4077548725(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2319419488_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = Enumerator_get_CurrentKey_m2387752419((Enumerator_t156532019 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		IntPtr_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2319419488_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2319419488(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m120250090_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentValue_m3147984131((Enumerator_t156532019 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m120250090_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m120250090(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m266619810_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m1547102902((Enumerator_t156532019 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t3131474613 * L_4 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t3131474613 * L_8 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		NullCheck(L_8);
		IntPtrU5BU5D_t169632028* L_9 = (IntPtrU5BU5D_t169632028*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		IntPtr_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t3131474613 * L_13 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t888819835  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m477426041(&L_18, (IntPtr_t)L_12, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t3131474613 * L_20 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m266619810_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	return Enumerator_MoveNext_m266619810(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t888819835  Enumerator_get_Current_m3216113338_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t888819835  L_0 = (KeyValuePair_2_t888819835 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t888819835  Enumerator_get_Current_m3216113338_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	return Enumerator_get_Current_m3216113338(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::get_CurrentKey()
extern "C"  IntPtr_t Enumerator_get_CurrentKey_m2387752419_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m213906106((Enumerator_t156532019 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t888819835 * L_0 = (KeyValuePair_2_t888819835 *)__this->get_address_of_current_3();
		IntPtr_t L_1 = KeyValuePair_2_get_Key_m1574332879((KeyValuePair_2_t888819835 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  IntPtr_t Enumerator_get_CurrentKey_m2387752419_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	return Enumerator_get_CurrentKey_m2387752419(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3147984131_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m213906106((Enumerator_t156532019 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t888819835 * L_0 = (KeyValuePair_2_t888819835 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m544293807((KeyValuePair_2_t888819835 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3147984131_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	return Enumerator_get_CurrentValue_m3147984131(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m1147555085_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1547102902((Enumerator_t156532019 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m1147555085_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	Enumerator_Reset_m1147555085(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m1547102902_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1547102902_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1547102902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3131474613 * L_0 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t3131474613 * L_2 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1547102902_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	Enumerator_VerifyState_m1547102902(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m213906106_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m213906106_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m213906106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1547102902((Enumerator_t156532019 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m213906106_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	Enumerator_VerifyCurrent_m213906106(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2117437651_gshared (Enumerator_t156532019 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t3131474613 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2117437651_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t156532019 * _thisAdjusted = reinterpret_cast<Enumerator_t156532019 *>(__this + 1);
	Enumerator_Dispose_m2117437651(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m661036428_gshared (Enumerator_t442692252 * __this, Dictionary_2_t3417634846 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t3417634846 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m661036428_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3417634846 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator__ctor_m661036428(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1692692619_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3330382363((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1174980068  L_0 = (KeyValuePair_2_t1174980068 )__this->get_current_3();
		KeyValuePair_2_t1174980068  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1692692619_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1692692619(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m70453843_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m3115320746((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m70453843_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m70453843(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3667889028_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3330382363((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1174980068 * L_0 = (KeyValuePair_2_t1174980068 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t1174980068 * L_2 = (KeyValuePair_2_t1174980068 *)__this->get_address_of_current_3();
		bool L_3 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3667889028_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3667889028(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1214978221_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m565000604((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1214978221_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1214978221(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m313528997_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Enumerator_get_CurrentValue_m4143929484((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m313528997_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m313528997(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1856697671_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m1165543189((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t3417634846 * L_4 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t3417634846 * L_8 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t3417634846 * L_13 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck(L_13);
		BooleanU5BU5D_t3568034315* L_14 = (BooleanU5BU5D_t3568034315*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		bool L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t1174980068  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m4040336782(&L_18, (Il2CppObject *)L_12, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t3417634846 * L_20 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1856697671_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_MoveNext_m1856697671(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C"  KeyValuePair_2_t1174980068  Enumerator_get_Current_m1020413567_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1174980068  L_0 = (KeyValuePair_2_t1174980068 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t1174980068  Enumerator_get_Current_m1020413567_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_get_Current_m1020413567(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m565000604_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3330382363((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1174980068 * L_0 = (KeyValuePair_2_t1174980068 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m565000604_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_get_CurrentKey_m565000604(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentValue()
extern "C"  bool Enumerator_get_CurrentValue_m4143929484_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3330382363((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1174980068 * L_0 = (KeyValuePair_2_t1174980068 *)__this->get_address_of_current_3();
		bool L_1 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_get_CurrentValue_m4143929484_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	return Enumerator_get_CurrentValue_m4143929484(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Reset()
extern "C"  void Enumerator_Reset_m3115320746_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1165543189((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m3115320746_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator_Reset_m3115320746(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m1165543189_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1165543189_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1165543189_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3417634846 * L_0 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t3417634846 * L_2 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1165543189_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator_VerifyState_m1165543189(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m3330382363_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3330382363_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3330382363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1165543189((Enumerator_t442692252 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m3330382363_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator_VerifyCurrent_m3330382363(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C"  void Enumerator_Dispose_m2711120408_gshared (Enumerator_t442692252 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t3417634846 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2711120408_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t442692252 * _thisAdjusted = reinterpret_cast<Enumerator_t442692252 *>(__this + 1);
	Enumerator_Dispose_m2711120408(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3597047336_gshared (Enumerator_t2983962278 * __this, Dictionary_2_t1663937576 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t1663937576 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3597047336_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1663937576 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator__ctor_m3597047336(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2010873149_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3278789713((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3716250094  L_0 = (KeyValuePair_2_t3716250094 )__this->get_current_3();
		KeyValuePair_2_t3716250094  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2010873149_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2010873149(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3085583937_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m1132695838((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3085583937_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3085583937(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m487599172_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3278789713((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3716250094 * L_0 = (KeyValuePair_2_t3716250094 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t3716250094 * L_2 = (KeyValuePair_2_t3716250094 *)__this->get_address_of_current_3();
		int32_t L_3 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m487599172_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m487599172(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m677423231_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m1408186928((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m677423231_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m677423231(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3005608231_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentValue_m2645962456((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3005608231_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3005608231(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m435964161_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m3173176371((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1663937576 * L_4 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1663937576 * L_8 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t1663937576 * L_13 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck(L_13);
		Int32U5BU5D_t3030399641* L_14 = (Int32U5BU5D_t3030399641*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t3716250094  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m1877755778(&L_18, (Il2CppObject *)L_12, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t1663937576 * L_20 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m435964161_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_MoveNext_m435964161(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t3716250094  Enumerator_get_Current_m1932198897_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3716250094  L_0 = (KeyValuePair_2_t3716250094 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t3716250094  Enumerator_get_Current_m1932198897_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_get_Current_m1932198897(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1408186928_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3278789713((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3716250094 * L_0 = (KeyValuePair_2_t3716250094 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1408186928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_get_CurrentKey_m1408186928(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m2645962456_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3278789713((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3716250094 * L_0 = (KeyValuePair_2_t3716250094 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentValue_m2645962456_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	return Enumerator_get_CurrentValue_m2645962456(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Reset()
extern "C"  void Enumerator_Reset_m1132695838_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3173176371((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m1132695838_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator_Reset_m1132695838(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m3173176371_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3173176371_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3173176371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t1663937576 * L_2 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3173176371_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator_VerifyState_m3173176371(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m3278789713_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3278789713_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3278789713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m3173176371((Enumerator_t2983962278 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m3278789713_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator_VerifyCurrent_m3278789713(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m401572848_gshared (Enumerator_t2983962278 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t1663937576 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m401572848_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2983962278 * _thisAdjusted = reinterpret_cast<Enumerator_t2983962278 *>(__this + 1);
	Enumerator_Dispose_m401572848(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1794172859_gshared (Enumerator_t1821162867 * __this, Dictionary_2_t501138165 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t501138165 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t501138165 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1794172859_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t501138165 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t1821162867 * _thisAdjusted = reinterpret_cast<Enumerator_t1821162867 *>(__this + 1);
	Enumerator__ctor_m1794172859(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2086793562_gshared (Enumerator_t1821162867 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1399210572((Enumerator_t1821162867 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2553450683  L_0 = (KeyValuePair_2_t2553450683 )__this->get_current_3();
		KeyValuePair_2_t2553450683  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2086793562_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1821162867 * _thisAdjusted = reinterpret_cast<Enumerator_t1821162867 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2086793562(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3916618268_gshared (Enumerator_t1821162867 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m489023909((Enumerator_t1821162867 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3916618268_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1821162867 * _thisAdjusted = reinterpret_cast<Enumerator_t1821162867 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3916618268(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4024664141_gshared (Enumerator_t1821162867 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1399210572((Enumerator_t1821162867 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2553450683 * L_0 = (KeyValuePair_2_t2553450683 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m289678647((KeyValuePair_2_t2553450683 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t2553450683 * L_2 = (KeyValuePair_2_t2553450683 *)__this->get_address_of_current_3();
		int64_t L_3 = KeyValuePair_2_get_Value_m2459084999((KeyValuePair_2_t2553450683 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int64_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4024664141_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1821162867 * _thisAdjusted = reinterpret_cast<Enumerator_t1821162867 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4024664141(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4251423544_gshared (Enumerator_t1821162867 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m1241282303((Enumerator_t1821162867 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4251423544_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1821162867 * _thisAdjusted = reinterpret_cast<Enumerator_t1821162867 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4251423544(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3205853258_gshared (Enumerator_t1821162867 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = Enumerator_get_CurrentValue_m2983776503((Enumerator_t1821162867 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3205853258_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1821162867 * _thisAdjusted = reinterpret_cast<Enumerator_t1821162867 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3205853258(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3870539252_gshared (Enumerator_t1821162867 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m152500944((Enumerator_t1821162867 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t501138165 * L_4 = (Dictionary_2_t501138165 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t501138165 * L_8 = (Dictionary_2_t501138165 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t501138165 * L_13 = (Dictionary_2_t501138165 *)__this->get_dictionary_0();
		NullCheck(L_13);
		Int64U5BU5D_t717125112* L_14 = (Int64U5BU5D_t717125112*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		int64_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t2553450683  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m2353880237(&L_18, (Il2CppObject *)L_12, (int64_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t501138165 * L_20 = (Dictionary_2_t501138165 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3870539252_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1821162867 * _thisAdjusted = reinterpret_cast<Enumerator_t1821162867 *>(__this + 1);
	return Enumerator_MoveNext_m3870539252(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_Current()
extern "C"  KeyValuePair_2_t2553450683  Enumerator_get_Current_m4123044964_gshared (Enumerator_t1821162867 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2553450683  L_0 = (KeyValuePair_2_t2553450683 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t2553450683  Enumerator_get_Current_m4123044964_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1821162867 * _thisAdjusted = reinterpret_cast<Enumerator_t1821162867 *>(__this + 1);
	return Enumerator_get_Current_m4123044964(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1241282303_gshared (Enumerator_t1821162867 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1399210572((Enumerator_t1821162867 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2553450683 * L_0 = (KeyValuePair_2_t2553450683 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m289678647((KeyValuePair_2_t2553450683 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1241282303_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1821162867 * _thisAdjusted = reinterpret_cast<Enumerator_t1821162867 *>(__this + 1);
	return Enumerator_get_CurrentKey_m1241282303(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_CurrentValue()
extern "C"  int64_t Enumerator_get_CurrentValue_m2983776503_gshared (Enumerator_t1821162867 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1399210572((Enumerator_t1821162867 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2553450683 * L_0 = (KeyValuePair_2_t2553450683 *)__this->get_address_of_current_3();
		int64_t L_1 = KeyValuePair_2_get_Value_m2459084999((KeyValuePair_2_t2553450683 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  int64_t Enumerator_get_CurrentValue_m2983776503_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1821162867 * _thisAdjusted = reinterpret_cast<Enumerator_t1821162867 *>(__this + 1);
	return Enumerator_get_CurrentValue_m2983776503(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::Reset()
extern "C"  void Enumerator_Reset_m489023909_gshared (Enumerator_t1821162867 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m152500944((Enumerator_t1821162867 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m489023909_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1821162867 * _thisAdjusted = reinterpret_cast<Enumerator_t1821162867 *>(__this + 1);
	Enumerator_Reset_m489023909(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m152500944_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m152500944_gshared (Enumerator_t1821162867 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m152500944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t501138165 * L_0 = (Dictionary_2_t501138165 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t501138165 * L_2 = (Dictionary_2_t501138165 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m152500944_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1821162867 * _thisAdjusted = reinterpret_cast<Enumerator_t1821162867 *>(__this + 1);
	Enumerator_VerifyState_m152500944(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m1399210572_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m1399210572_gshared (Enumerator_t1821162867 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m1399210572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m152500944((Enumerator_t1821162867 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m1399210572_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1821162867 * _thisAdjusted = reinterpret_cast<Enumerator_t1821162867 *>(__this + 1);
	Enumerator_VerifyCurrent_m1399210572(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::Dispose()
extern "C"  void Enumerator_Dispose_m2354198679_gshared (Enumerator_t1821162867 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t501138165 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2354198679_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1821162867 * _thisAdjusted = reinterpret_cast<Enumerator_t1821162867 *>(__this + 1);
	Enumerator_Dispose_m2354198679(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4026215401_gshared (Enumerator_t1247028593 * __this, Dictionary_2_t4221971187 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t4221971187 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t4221971187 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m4026215401_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t4221971187 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t1247028593 * _thisAdjusted = reinterpret_cast<Enumerator_t1247028593 *>(__this + 1);
	Enumerator__ctor_m4026215401(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2974563846_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3518573238((Enumerator_t1247028593 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1979316409  L_0 = (KeyValuePair_2_t1979316409 )__this->get_current_3();
		KeyValuePair_2_t1979316409  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2974563846_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1247028593 * _thisAdjusted = reinterpret_cast<Enumerator_t1247028593 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2974563846(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1209144614_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m2206907019((Enumerator_t1247028593 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1209144614_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1247028593 * _thisAdjusted = reinterpret_cast<Enumerator_t1247028593 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1209144614(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3651401679_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3518573238((Enumerator_t1247028593 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1979316409 * L_0 = (KeyValuePair_2_t1979316409 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2225619841((KeyValuePair_2_t1979316409 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t1979316409 * L_2 = (KeyValuePair_2_t1979316409 *)__this->get_address_of_current_3();
		Nullable_1_t334943763  L_3 = KeyValuePair_2_get_Value_m2361283873((KeyValuePair_2_t1979316409 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Nullable_1_t334943763  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3651401679_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1247028593 * _thisAdjusted = reinterpret_cast<Enumerator_t1247028593 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3651401679(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3577756924_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m672484517((Enumerator_t1247028593 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3577756924_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1247028593 * _thisAdjusted = reinterpret_cast<Enumerator_t1247028593 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3577756924(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2614586210_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method)
{
	{
		Nullable_1_t334943763  L_0 = Enumerator_get_CurrentValue_m1012672069((Enumerator_t1247028593 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Nullable_1_t334943763  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2614586210_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1247028593 * _thisAdjusted = reinterpret_cast<Enumerator_t1247028593 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2614586210(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m688389906_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m4272604834((Enumerator_t1247028593 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t4221971187 * L_4 = (Dictionary_2_t4221971187 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t4221971187 * L_8 = (Dictionary_2_t4221971187 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t4221971187 * L_13 = (Dictionary_2_t4221971187 *)__this->get_dictionary_0();
		NullCheck(L_13);
		Nullable_1U5BU5D_t1781475394* L_14 = (Nullable_1U5BU5D_t1781475394*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Nullable_1_t334943763  L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t1979316409  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m848228575(&L_18, (Il2CppObject *)L_12, (Nullable_1_t334943763 )L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t4221971187 * L_20 = (Dictionary_2_t4221971187 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m688389906_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1247028593 * _thisAdjusted = reinterpret_cast<Enumerator_t1247028593 *>(__this + 1);
	return Enumerator_MoveNext_m688389906(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::get_Current()
extern "C"  KeyValuePair_2_t1979316409  Enumerator_get_Current_m4069600302_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1979316409  L_0 = (KeyValuePair_2_t1979316409 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t1979316409  Enumerator_get_Current_m4069600302_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1247028593 * _thisAdjusted = reinterpret_cast<Enumerator_t1247028593 *>(__this + 1);
	return Enumerator_get_Current_m4069600302(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m672484517_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3518573238((Enumerator_t1247028593 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1979316409 * L_0 = (KeyValuePair_2_t1979316409 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2225619841((KeyValuePair_2_t1979316409 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m672484517_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1247028593 * _thisAdjusted = reinterpret_cast<Enumerator_t1247028593 *>(__this + 1);
	return Enumerator_get_CurrentKey_m672484517(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::get_CurrentValue()
extern "C"  Nullable_1_t334943763  Enumerator_get_CurrentValue_m1012672069_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m3518573238((Enumerator_t1247028593 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1979316409 * L_0 = (KeyValuePair_2_t1979316409 *)__this->get_address_of_current_3();
		Nullable_1_t334943763  L_1 = KeyValuePair_2_get_Value_m2361283873((KeyValuePair_2_t1979316409 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Nullable_1_t334943763  Enumerator_get_CurrentValue_m1012672069_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1247028593 * _thisAdjusted = reinterpret_cast<Enumerator_t1247028593 *>(__this + 1);
	return Enumerator_get_CurrentValue_m1012672069(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::Reset()
extern "C"  void Enumerator_Reset_m2206907019_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m4272604834((Enumerator_t1247028593 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m2206907019_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1247028593 * _thisAdjusted = reinterpret_cast<Enumerator_t1247028593 *>(__this + 1);
	Enumerator_Reset_m2206907019(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m4272604834_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m4272604834_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m4272604834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t4221971187 * L_0 = (Dictionary_2_t4221971187 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t4221971187 * L_2 = (Dictionary_2_t4221971187 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m4272604834_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1247028593 * _thisAdjusted = reinterpret_cast<Enumerator_t1247028593 *>(__this + 1);
	Enumerator_VerifyState_m4272604834(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m3518573238_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3518573238_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3518573238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m4272604834((Enumerator_t1247028593 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m3518573238_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1247028593 * _thisAdjusted = reinterpret_cast<Enumerator_t1247028593 *>(__this + 1);
	Enumerator_VerifyCurrent_m3518573238(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::Dispose()
extern "C"  void Enumerator_Dispose_m2832749025_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t4221971187 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2832749025_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1247028593 * _thisAdjusted = reinterpret_cast<Enumerator_t1247028593 *>(__this + 1);
	Enumerator_Dispose_m2832749025(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3742107451_gshared (Enumerator_t3601534125 * __this, Dictionary_2_t2281509423 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t2281509423 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3742107451_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2281509423 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator__ctor_m3742107451(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1702320752((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645  L_0 = (KeyValuePair_2_t38854645 )__this->get_current_3();
		KeyValuePair_2_t38854645  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m229223308_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m229223308(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m3129803197((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3225937576_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3225937576(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1702320752((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645 * L_0 = (KeyValuePair_2_t38854645 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t38854645 * L_2 = (KeyValuePair_2_t38854645 *)__this->get_address_of_current_3();
		Il2CppObject * L_3 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t3048875398  L_4;
		memset(&L_4, 0, sizeof(L_4));
		DictionaryEntry__ctor_m2901884110(&L_4, (Il2CppObject *)L_1, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m3839846791((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentValue_m402763047((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3349738440_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m262343092((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t2281509423 * L_4 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t2281509423 * L_8 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t2281509423 * L_13 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck(L_13);
		ObjectU5BU5D_t3614634134* L_14 = (ObjectU5BU5D_t3614634134*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t38854645  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m1640124561(&L_18, (Il2CppObject *)L_12, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t2281509423 * L_20 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3349738440_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_MoveNext_m3349738440(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_Current_m25299632_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = (KeyValuePair_2_t38854645 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_Current_m25299632_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_get_Current_m25299632(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m3839846791_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1702320752((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645 * L_0 = (KeyValuePair_2_t38854645 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m3839846791_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_get_CurrentKey_m3839846791(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m402763047_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m1702320752((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645 * L_0 = (KeyValuePair_2_t38854645 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m402763047_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	return Enumerator_get_CurrentValue_m402763047(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m3129803197_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m262343092((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m3129803197_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator_Reset_m3129803197(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m262343092_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m262343092_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m262343092_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t2281509423 * L_2 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m262343092_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator_VerifyState_m262343092(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m1702320752_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m1702320752_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m1702320752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m262343092((Enumerator_t3601534125 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m1702320752_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator_VerifyCurrent_m1702320752(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1905011127_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t2281509423 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1905011127_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3601534125 * _thisAdjusted = reinterpret_cast<Enumerator_t3601534125 *>(__this + 1);
	Enumerator_Dispose_m1905011127(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1874499463_gshared (Enumerator_t4050882528 * __this, Dictionary_2_t2730857826 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t2730857826 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t2730857826 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1874499463_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2730857826 * ___dictionary0, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	Enumerator__ctor_m1874499463(_thisAdjusted, ___dictionary0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2507884600_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2900414404((Enumerator_t4050882528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t488203048  L_0 = (KeyValuePair_2_t488203048 )__this->get_current_3();
		KeyValuePair_2_t488203048  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2507884600_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2507884600(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4018492724_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	{
		Enumerator_Reset_m2489846157((Enumerator_t4050882528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4018492724_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4018492724(_thisAdjusted, method);
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m744625813_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2900414404((Enumerator_t4050882528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t488203048 * L_0 = (KeyValuePair_2_t488203048 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m573362703((KeyValuePair_2_t488203048 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		KeyValuePair_2_t488203048 * L_2 = (KeyValuePair_2_t488203048 *)__this->get_address_of_current_3();
		int32_t L_3 = KeyValuePair_2_get_Value_m1644876463((KeyValuePair_2_t488203048 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_4);
		DictionaryEntry_t3048875398  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2901884110(&L_6, (Il2CppObject *)L_1, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m744625813_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m744625813(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2958681418_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_CurrentKey_m2295079915((Enumerator_t4050882528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2958681418_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2958681418(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2914875968_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Enumerator_get_CurrentValue_m3498625419((Enumerator_t4050882528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2914875968_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	return Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2914875968(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2619008060_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Enumerator_VerifyState_m1671341316((Enumerator_t4050882528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t2730857826 * L_4 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t62501539* L_5 = (LinkU5BU5D_t62501539*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t2730857826 * L_8 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		NullCheck(L_8);
		ObjectU5BU5D_t3614634134* L_9 = (ObjectU5BU5D_t3614634134*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Dictionary_2_t2730857826 * L_13 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		NullCheck(L_13);
		TextEditOpU5BU5D_t1473213239* L_14 = (TextEditOpU5BU5D_t1473213239*)L_13->get_valueSlots_7();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		KeyValuePair_2_t488203048  L_18;
		memset(&L_18, 0, sizeof(L_18));
		KeyValuePair_2__ctor_m3870834457(&L_18, (Il2CppObject *)L_12, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_18);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_19 = (int32_t)__this->get_next_1();
		Dictionary_2_t2730857826 * L_20 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		NullCheck(L_20);
		int32_t L_21 = (int32_t)L_20->get_touchedSlots_8();
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2619008060_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	return Enumerator_MoveNext_m2619008060(_thisAdjusted, method);
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C"  KeyValuePair_2_t488203048  Enumerator_get_Current_m492584036_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t488203048  L_0 = (KeyValuePair_2_t488203048 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t488203048  Enumerator_get_Current_m492584036_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	return Enumerator_get_Current_m492584036(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m2295079915_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2900414404((Enumerator_t4050882528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t488203048 * L_0 = (KeyValuePair_2_t488203048 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = KeyValuePair_2_get_Key_m573362703((KeyValuePair_2_t488203048 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m2295079915_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	return Enumerator_get_CurrentKey_m2295079915(_thisAdjusted, method);
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m3498625419_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyCurrent_m2900414404((Enumerator_t4050882528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t488203048 * L_0 = (KeyValuePair_2_t488203048 *)__this->get_address_of_current_3();
		int32_t L_1 = KeyValuePair_2_get_Value_m1644876463((KeyValuePair_2_t488203048 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  int32_t Enumerator_get_CurrentValue_m3498625419_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	return Enumerator_get_CurrentValue_m3498625419(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Reset()
extern "C"  void Enumerator_Reset_m2489846157_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1671341316((Enumerator_t4050882528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_Reset_m2489846157_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	Enumerator_Reset_m2489846157(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral671556834;
extern const uint32_t Enumerator_VerifyState_m1671341316_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1671341316_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1671341316_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2730857826 * L_0 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t2730857826 * L_2 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral671556834, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1671341316_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	Enumerator_VerifyState_m1671341316(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2382073330;
extern const uint32_t Enumerator_VerifyCurrent_m2900414404_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m2900414404_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m2900414404_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1671341316((Enumerator_t4050882528 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2382073330, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyCurrent_m2900414404_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	Enumerator_VerifyCurrent_m2900414404(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C"  void Enumerator_Dispose_m793495907_gshared (Enumerator_t4050882528 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t2730857826 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m793495907_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4050882528 * _thisAdjusted = reinterpret_cast<Enumerator_t4050882528 *>(__this + 1);
	Enumerator_Dispose_m793495907(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1552363313_gshared (Enumerator_t2019034881 * __this, Dictionary_2_t3624498739 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3624498739 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3624498739 *)L_0);
		Enumerator_t649556145  L_1 = ((  Enumerator_t649556145  (*) (Dictionary_2_t3624498739 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3624498739 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1552363313_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3624498739 * ___host0, const MethodInfo* method)
{
	Enumerator_t2019034881 * _thisAdjusted = reinterpret_cast<Enumerator_t2019034881 *>(__this + 1);
	Enumerator__ctor_m1552363313(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2221565188_gshared (Enumerator_t2019034881 * __this, const MethodInfo* method)
{
	{
		Enumerator_t649556145 * L_0 = (Enumerator_t649556145 *)__this->get_address_of_host_enumerator_0();
		ChannelIdentity_t1147162267  L_1 = Enumerator_get_CurrentKey_m1881066775((Enumerator_t649556145 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		ChannelIdentity_t1147162267  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2221565188_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2019034881 * _thisAdjusted = reinterpret_cast<Enumerator_t2019034881 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2221565188(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3085171034_gshared (Enumerator_t2019034881 * __this, const MethodInfo* method)
{
	{
		Enumerator_t649556145 * L_0 = (Enumerator_t649556145 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m87609553((Enumerator_t649556145 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3085171034_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2019034881 * _thisAdjusted = reinterpret_cast<Enumerator_t2019034881 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3085171034(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m223457449_gshared (Enumerator_t2019034881 * __this, const MethodInfo* method)
{
	{
		Enumerator_t649556145 * L_0 = (Enumerator_t649556145 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m4288624127((Enumerator_t649556145 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m223457449_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2019034881 * _thisAdjusted = reinterpret_cast<Enumerator_t2019034881 *>(__this + 1);
	Enumerator_Dispose_m223457449(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m365687422_gshared (Enumerator_t2019034881 * __this, const MethodInfo* method)
{
	{
		Enumerator_t649556145 * L_0 = (Enumerator_t649556145 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2703835770((Enumerator_t649556145 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m365687422_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2019034881 * _thisAdjusted = reinterpret_cast<Enumerator_t2019034881 *>(__this + 1);
	return Enumerator_MoveNext_m365687422(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Current()
extern "C"  ChannelIdentity_t1147162267  Enumerator_get_Current_m1630057566_gshared (Enumerator_t2019034881 * __this, const MethodInfo* method)
{
	{
		Enumerator_t649556145 * L_0 = (Enumerator_t649556145 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1381843961 * L_1 = (KeyValuePair_2_t1381843961 *)L_0->get_address_of_current_3();
		ChannelIdentity_t1147162267  L_2 = KeyValuePair_2_get_Key_m359652951((KeyValuePair_2_t1381843961 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  ChannelIdentity_t1147162267  Enumerator_get_Current_m1630057566_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2019034881 * _thisAdjusted = reinterpret_cast<Enumerator_t2019034881 *>(__this + 1);
	return Enumerator_get_Current_m1630057566(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1498955086_gshared (Enumerator_t3741519566 * __this, Dictionary_2_t1052016128 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1052016128 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1052016128 *)L_0);
		Enumerator_t2372040830  L_1 = ((  Enumerator_t2372040830  (*) (Dictionary_2_t1052016128 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1052016128 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1498955086_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1052016128 * ___host0, const MethodInfo* method)
{
	Enumerator_t3741519566 * _thisAdjusted = reinterpret_cast<Enumerator_t3741519566 *>(__this + 1);
	Enumerator__ctor_m1498955086(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2505623353_gshared (Enumerator_t3741519566 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2372040830 * L_0 = (Enumerator_t2372040830 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentKey_m103879330((Enumerator_t2372040830 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2505623353_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3741519566 * _thisAdjusted = reinterpret_cast<Enumerator_t3741519566 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2505623353(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4244405393_gshared (Enumerator_t3741519566 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2372040830 * L_0 = (Enumerator_t2372040830 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1143911664((Enumerator_t2372040830 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4244405393_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3741519566 * _thisAdjusted = reinterpret_cast<Enumerator_t3741519566 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4244405393(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m535518462_gshared (Enumerator_t3741519566 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2372040830 * L_0 = (Enumerator_t2372040830 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m3484064310((Enumerator_t2372040830 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m535518462_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3741519566 * _thisAdjusted = reinterpret_cast<Enumerator_t3741519566 *>(__this + 1);
	Enumerator_Dispose_m535518462(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2361184961_gshared (Enumerator_t3741519566 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2372040830 * L_0 = (Enumerator_t2372040830 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3717365715((Enumerator_t2372040830 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m2361184961_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3741519566 * _thisAdjusted = reinterpret_cast<Enumerator_t3741519566 *>(__this + 1);
	return Enumerator_MoveNext_m2361184961(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3083734335_gshared (Enumerator_t3741519566 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2372040830 * L_0 = (Enumerator_t2372040830 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3104328646 * L_1 = (KeyValuePair_2_t3104328646 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Key_m266456220((KeyValuePair_2_t3104328646 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m3083734335_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3741519566 * _thisAdjusted = reinterpret_cast<Enumerator_t3741519566 *>(__this + 1);
	return Enumerator_get_Current_m3083734335(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m356032285_gshared (Enumerator_t3769206521 * __this, Dictionary_2_t1079703083 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		Enumerator_t2399727785  L_1 = ((  Enumerator_t2399727785  (*) (Dictionary_2_t1079703083 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1079703083 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m356032285_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1079703083 * ___host0, const MethodInfo* method)
{
	Enumerator_t3769206521 * _thisAdjusted = reinterpret_cast<Enumerator_t3769206521 *>(__this + 1);
	Enumerator__ctor_m356032285(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1856980258_gshared (Enumerator_t3769206521 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentKey_m3410494779((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1856980258_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3769206521 * _thisAdjusted = reinterpret_cast<Enumerator_t3769206521 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1856980258(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m84942922_gshared (Enumerator_t3769206521 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2787013825((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m84942922_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3769206521 * _thisAdjusted = reinterpret_cast<Enumerator_t3769206521 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m84942922(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m2091286541_gshared (Enumerator_t3769206521 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m1631869499((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2091286541_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3769206521 * _thisAdjusted = reinterpret_cast<Enumerator_t3769206521 *>(__this + 1);
	Enumerator_Dispose_m2091286541(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3192922974_gshared (Enumerator_t3769206521 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3809319098((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m3192922974_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3769206521 * _thisAdjusted = reinterpret_cast<Enumerator_t3769206521 *>(__this + 1);
	return Enumerator_MoveNext_m3192922974(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m4198336486_gshared (Enumerator_t3769206521 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3132015601 * L_1 = (KeyValuePair_2_t3132015601 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Key_m1683812983((KeyValuePair_2_t3132015601 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m4198336486_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3769206521 * _thisAdjusted = reinterpret_cast<Enumerator_t3769206521 *>(__this + 1);
	return Enumerator_get_Current_m4198336486(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2975000652_gshared (Enumerator_t91811072 * __this, Dictionary_2_t1697274930 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		Enumerator_t3017299632  L_1 = ((  Enumerator_t3017299632  (*) (Dictionary_2_t1697274930 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1697274930 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2975000652_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1697274930 * ___host0, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	Enumerator__ctor_m2975000652(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1197115851_gshared (Enumerator_t91811072 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentKey_m447338908((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1197115851_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1197115851(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1291229611_gshared (Enumerator_t91811072 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m761796566((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1291229611_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1291229611(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3298895664_gshared (Enumerator_t91811072 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2243145188((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3298895664_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	Enumerator_Dispose_m3298895664(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1388125215_gshared (Enumerator_t91811072 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2770956757((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1388125215_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	return Enumerator_MoveNext_m1388125215(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m862874469_gshared (Enumerator_t91811072 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3749587448 * L_1 = (KeyValuePair_2_t3749587448 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m862874469_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t91811072 * _thisAdjusted = reinterpret_cast<Enumerator_t91811072 *>(__this + 1);
	return Enumerator_get_Current_m862874469(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m393153339_gshared (Enumerator_t1645564437 * __this, Dictionary_2_t3251028295 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3251028295 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3251028295 *)L_0);
		Enumerator_t276085701  L_1 = ((  Enumerator_t276085701  (*) (Dictionary_2_t3251028295 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3251028295 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m393153339_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3251028295 * ___host0, const MethodInfo* method)
{
	Enumerator_t1645564437 * _thisAdjusted = reinterpret_cast<Enumerator_t1645564437 *>(__this + 1);
	Enumerator__ctor_m393153339(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2050778348_gshared (Enumerator_t1645564437 * __this, const MethodInfo* method)
{
	{
		Enumerator_t276085701 * L_0 = (Enumerator_t276085701 *)__this->get_address_of_host_enumerator_0();
		int32_t L_1 = Enumerator_get_CurrentKey_m2503592769((Enumerator_t276085701 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2050778348_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1645564437 * _thisAdjusted = reinterpret_cast<Enumerator_t1645564437 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2050778348(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2480897632_gshared (Enumerator_t1645564437 * __this, const MethodInfo* method)
{
	{
		Enumerator_t276085701 * L_0 = (Enumerator_t276085701 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2015547391((Enumerator_t276085701 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2480897632_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1645564437 * _thisAdjusted = reinterpret_cast<Enumerator_t1645564437 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2480897632(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::Dispose()
extern "C"  void Enumerator_Dispose_m547675919_gshared (Enumerator_t1645564437 * __this, const MethodInfo* method)
{
	{
		Enumerator_t276085701 * L_0 = (Enumerator_t276085701 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2214493861((Enumerator_t276085701 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m547675919_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1645564437 * _thisAdjusted = reinterpret_cast<Enumerator_t1645564437 *>(__this + 1);
	Enumerator_Dispose_m547675919(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1035269024_gshared (Enumerator_t1645564437 * __this, const MethodInfo* method)
{
	{
		Enumerator_t276085701 * L_0 = (Enumerator_t276085701 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3018149748((Enumerator_t276085701 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1035269024_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1645564437 * _thisAdjusted = reinterpret_cast<Enumerator_t1645564437 *>(__this + 1);
	return Enumerator_MoveNext_m1035269024(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Reflection.Emit.Label>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2469047616_gshared (Enumerator_t1645564437 * __this, const MethodInfo* method)
{
	{
		Enumerator_t276085701 * L_0 = (Enumerator_t276085701 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1008373517 * L_1 = (KeyValuePair_2_t1008373517 *)L_0->get_address_of_current_3();
		int32_t L_2 = KeyValuePair_2_get_Key_m2216667373((KeyValuePair_2_t1008373517 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int32_t Enumerator_get_Current_m2469047616_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1645564437 * _thisAdjusted = reinterpret_cast<Enumerator_t1645564437 *>(__this + 1);
	return Enumerator_get_Current_m2469047616(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3427160064_gshared (Enumerator_t1455931992 * __this, Dictionary_2_t3061395850 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3061395850 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3061395850 *)L_0);
		Enumerator_t86453256  L_1 = ((  Enumerator_t86453256  (*) (Dictionary_2_t3061395850 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3061395850 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3427160064_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3061395850 * ___host0, const MethodInfo* method)
{
	Enumerator_t1455931992 * _thisAdjusted = reinterpret_cast<Enumerator_t1455931992 *>(__this + 1);
	Enumerator__ctor_m3427160064(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2233211951_gshared (Enumerator_t1455931992 * __this, const MethodInfo* method)
{
	{
		Enumerator_t86453256 * L_0 = (Enumerator_t86453256 *)__this->get_address_of_host_enumerator_0();
		int64_t L_1 = Enumerator_get_CurrentKey_m3844258184((Enumerator_t86453256 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int64_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2233211951_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1455931992 * _thisAdjusted = reinterpret_cast<Enumerator_t1455931992 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2233211951(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3955104227_gshared (Enumerator_t1455931992 * __this, const MethodInfo* method)
{
	{
		Enumerator_t86453256 * L_0 = (Enumerator_t86453256 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m199703458((Enumerator_t86453256 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3955104227_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1455931992 * _thisAdjusted = reinterpret_cast<Enumerator_t1455931992 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3955104227(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m3674293844_gshared (Enumerator_t1455931992 * __this, const MethodInfo* method)
{
	{
		Enumerator_t86453256 * L_0 = (Enumerator_t86453256 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m3531528168((Enumerator_t86453256 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3674293844_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1455931992 * _thisAdjusted = reinterpret_cast<Enumerator_t1455931992 *>(__this + 1);
	Enumerator_Dispose_m3674293844(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4041166087_gshared (Enumerator_t1455931992 * __this, const MethodInfo* method)
{
	{
		Enumerator_t86453256 * L_0 = (Enumerator_t86453256 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3282539621((Enumerator_t86453256 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m4041166087_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1455931992 * _thisAdjusted = reinterpret_cast<Enumerator_t1455931992 *>(__this + 1);
	return Enumerator_MoveNext_m4041166087(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Int32>::get_Current()
extern "C"  int64_t Enumerator_get_Current_m2733093241_gshared (Enumerator_t1455931992 * __this, const MethodInfo* method)
{
	{
		Enumerator_t86453256 * L_0 = (Enumerator_t86453256 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t818741072 * L_1 = (KeyValuePair_2_t818741072 *)L_0->get_address_of_current_3();
		int64_t L_2 = KeyValuePair_2_get_Key_m4268935818((KeyValuePair_2_t818741072 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int64_t Enumerator_get_Current_m2733093241_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1455931992 * _thisAdjusted = reinterpret_cast<Enumerator_t1455931992 *>(__this + 1);
	return Enumerator_get_Current_m2733093241(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3400877137_gshared (Enumerator_t2073503839 * __this, Dictionary_2_t3678967697 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3678967697 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3678967697 *)L_0);
		Enumerator_t704025103  L_1 = ((  Enumerator_t704025103  (*) (Dictionary_2_t3678967697 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3678967697 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3400877137_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3678967697 * ___host0, const MethodInfo* method)
{
	Enumerator_t2073503839 * _thisAdjusted = reinterpret_cast<Enumerator_t2073503839 *>(__this + 1);
	Enumerator__ctor_m3400877137(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m247337542_gshared (Enumerator_t2073503839 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		int64_t L_1 = Enumerator_get_CurrentKey_m4167196999((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int64_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m247337542_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2073503839 * _thisAdjusted = reinterpret_cast<Enumerator_t2073503839 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m247337542(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4166432760_gshared (Enumerator_t2073503839 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m598725905((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4166432760_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2073503839 * _thisAdjusted = reinterpret_cast<Enumerator_t2073503839 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4166432760(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3239242417_gshared (Enumerator_t2073503839 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m3446908287((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3239242417_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2073503839 * _thisAdjusted = reinterpret_cast<Enumerator_t2073503839 *>(__this + 1);
	Enumerator_Dispose_m3239242417(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1062748132_gshared (Enumerator_t2073503839 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3139459564((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1062748132_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2073503839 * _thisAdjusted = reinterpret_cast<Enumerator_t2073503839 *>(__this + 1);
	return Enumerator_MoveNext_m1062748132(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int64,System.Object>::get_Current()
extern "C"  int64_t Enumerator_get_Current_m3153023504_gshared (Enumerator_t2073503839 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1436312919 * L_1 = (KeyValuePair_2_t1436312919 *)L_0->get_address_of_current_3();
		int64_t L_2 = KeyValuePair_2_get_Key_m3157806383((KeyValuePair_2_t1436312919 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  int64_t Enumerator_get_Current_m3153023504_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2073503839 * _thisAdjusted = reinterpret_cast<Enumerator_t2073503839 *>(__this + 1);
	return Enumerator_get_Current_m3153023504(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1331665702_gshared (Enumerator_t908438908 * __this, Dictionary_2_t2513902766 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t2513902766 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2513902766 *)L_0);
		Enumerator_t3833927468  L_1 = ((  Enumerator_t3833927468  (*) (Dictionary_2_t2513902766 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t2513902766 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1331665702_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2513902766 * ___host0, const MethodInfo* method)
{
	Enumerator_t908438908 * _thisAdjusted = reinterpret_cast<Enumerator_t908438908 *>(__this + 1);
	Enumerator__ctor_m1331665702(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m565527927_gshared (Enumerator_t908438908 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3833927468 * L_0 = (Enumerator_t3833927468 *)__this->get_address_of_host_enumerator_0();
		IntPtr_t L_1 = Enumerator_get_CurrentKey_m1901550810((Enumerator_t3833927468 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IntPtr_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m565527927_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t908438908 * _thisAdjusted = reinterpret_cast<Enumerator_t908438908 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m565527927(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m955232903_gshared (Enumerator_t908438908 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3833927468 * L_0 = (Enumerator_t3833927468 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1791350284((Enumerator_t3833927468 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m955232903_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t908438908 * _thisAdjusted = reinterpret_cast<Enumerator_t908438908 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m955232903(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m464596946_gshared (Enumerator_t908438908 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3833927468 * L_0 = (Enumerator_t3833927468 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m3738173754((Enumerator_t3833927468 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m464596946_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t908438908 * _thisAdjusted = reinterpret_cast<Enumerator_t908438908 *>(__this + 1);
	Enumerator_Dispose_m464596946(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4172265267_gshared (Enumerator_t908438908 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3833927468 * L_0 = (Enumerator_t3833927468 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3274679113((Enumerator_t3833927468 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m4172265267_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t908438908 * _thisAdjusted = reinterpret_cast<Enumerator_t908438908 *>(__this + 1);
	return Enumerator_MoveNext_m4172265267(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Int32>::get_Current()
extern "C"  IntPtr_t Enumerator_get_Current_m2541778865_gshared (Enumerator_t908438908 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3833927468 * L_0 = (Enumerator_t3833927468 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t271247988 * L_1 = (KeyValuePair_2_t271247988 *)L_0->get_address_of_current_3();
		IntPtr_t L_2 = KeyValuePair_2_get_Key_m3024592206((KeyValuePair_2_t271247988 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  IntPtr_t Enumerator_get_Current_m2541778865_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t908438908 * _thisAdjusted = reinterpret_cast<Enumerator_t908438908 *>(__this + 1);
	return Enumerator_get_Current_m2541778865(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3235517721_gshared (Enumerator_t1526010755 * __this, Dictionary_2_t3131474613 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3131474613 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3131474613 *)L_0);
		Enumerator_t156532019  L_1 = ((  Enumerator_t156532019  (*) (Dictionary_2_t3131474613 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3131474613 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3235517721_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3131474613 * ___host0, const MethodInfo* method)
{
	Enumerator_t1526010755 * _thisAdjusted = reinterpret_cast<Enumerator_t1526010755 *>(__this + 1);
	Enumerator__ctor_m3235517721(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3071477686_gshared (Enumerator_t1526010755 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		IntPtr_t L_1 = Enumerator_get_CurrentKey_m2387752419((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IntPtr_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3071477686_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1526010755 * _thisAdjusted = reinterpret_cast<Enumerator_t1526010755 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3071477686(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1082414166_gshared (Enumerator_t1526010755 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1147555085((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1082414166_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1526010755 * _thisAdjusted = reinterpret_cast<Enumerator_t1526010755 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1082414166(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m168635025_gshared (Enumerator_t1526010755 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2117437651((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m168635025_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1526010755 * _thisAdjusted = reinterpret_cast<Enumerator_t1526010755 *>(__this + 1);
	Enumerator_Dispose_m168635025(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3535822434_gshared (Enumerator_t1526010755 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m266619810((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m3535822434_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1526010755 * _thisAdjusted = reinterpret_cast<Enumerator_t1526010755 *>(__this + 1);
	return Enumerator_MoveNext_m3535822434(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Object>::get_Current()
extern "C"  IntPtr_t Enumerator_get_Current_m286596974_gshared (Enumerator_t1526010755 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t888819835 * L_1 = (KeyValuePair_2_t888819835 *)L_0->get_address_of_current_3();
		IntPtr_t L_2 = KeyValuePair_2_get_Key_m1574332879((KeyValuePair_2_t888819835 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  IntPtr_t Enumerator_get_Current_m286596974_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1526010755 * _thisAdjusted = reinterpret_cast<Enumerator_t1526010755 *>(__this + 1);
	return Enumerator_get_Current_m286596974(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4035163080_gshared (Enumerator_t1812170988 * __this, Dictionary_2_t3417634846 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3417634846 *)L_0);
		Enumerator_t442692252  L_1 = ((  Enumerator_t442692252  (*) (Dictionary_2_t3417634846 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t3417634846 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m4035163080_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t3417634846 * ___host0, const MethodInfo* method)
{
	Enumerator_t1812170988 * _thisAdjusted = reinterpret_cast<Enumerator_t1812170988 *>(__this + 1);
	Enumerator__ctor_m4035163080(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3424705613_gshared (Enumerator_t1812170988 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentKey_m565000604((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3424705613_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1812170988 * _thisAdjusted = reinterpret_cast<Enumerator_t1812170988 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3424705613(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2102606953_gshared (Enumerator_t1812170988 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3115320746((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2102606953_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1812170988 * _thisAdjusted = reinterpret_cast<Enumerator_t1812170988 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2102606953(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C"  void Enumerator_Dispose_m4156741384_gshared (Enumerator_t1812170988 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2711120408((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m4156741384_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1812170988 * _thisAdjusted = reinterpret_cast<Enumerator_t1812170988 *>(__this + 1);
	Enumerator_Dispose_m4156741384(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3293466169_gshared (Enumerator_t1812170988 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1856697671((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m3293466169_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1812170988 * _thisAdjusted = reinterpret_cast<Enumerator_t1812170988 *>(__this + 1);
	return Enumerator_MoveNext_m3293466169(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3073091827_gshared (Enumerator_t1812170988 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1174980068 * L_1 = (KeyValuePair_2_t1174980068 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m3073091827_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1812170988 * _thisAdjusted = reinterpret_cast<Enumerator_t1812170988 *>(__this + 1);
	return Enumerator_get_Current_m3073091827(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1642475044_gshared (Enumerator_t58473718 * __this, Dictionary_2_t1663937576 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		Enumerator_t2983962278  L_1 = ((  Enumerator_t2983962278  (*) (Dictionary_2_t1663937576 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t1663937576 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1642475044_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t1663937576 * ___host0, const MethodInfo* method)
{
	Enumerator_t58473718 * _thisAdjusted = reinterpret_cast<Enumerator_t58473718 *>(__this + 1);
	Enumerator__ctor_m1642475044(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m610493107_gshared (Enumerator_t58473718 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentKey_m1408186928((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m610493107_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t58473718 * _thisAdjusted = reinterpret_cast<Enumerator_t58473718 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m610493107(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1684647011_gshared (Enumerator_t58473718 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1132695838((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1684647011_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t58473718 * _thisAdjusted = reinterpret_cast<Enumerator_t58473718 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1684647011(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m688620792_gshared (Enumerator_t58473718 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m401572848((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m688620792_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t58473718 * _thisAdjusted = reinterpret_cast<Enumerator_t58473718 *>(__this + 1);
	Enumerator_Dispose_m688620792(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m639383991_gshared (Enumerator_t58473718 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m435964161((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m639383991_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t58473718 * _thisAdjusted = reinterpret_cast<Enumerator_t58473718 *>(__this + 1);
	return Enumerator_MoveNext_m639383991(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1529104153_gshared (Enumerator_t58473718 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3716250094 * L_1 = (KeyValuePair_2_t3716250094 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m1529104153_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t58473718 * _thisAdjusted = reinterpret_cast<Enumerator_t58473718 *>(__this + 1);
	return Enumerator_get_Current_m1529104153(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3766187165_gshared (Enumerator_t3190641603 * __this, Dictionary_2_t501138165 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t501138165 * L_0 = ___host0;
		NullCheck((Dictionary_2_t501138165 *)L_0);
		Enumerator_t1821162867  L_1 = ((  Enumerator_t1821162867  (*) (Dictionary_2_t501138165 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t501138165 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3766187165_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t501138165 * ___host0, const MethodInfo* method)
{
	Enumerator_t3190641603 * _thisAdjusted = reinterpret_cast<Enumerator_t3190641603 *>(__this + 1);
	Enumerator__ctor_m3766187165(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m254358010_gshared (Enumerator_t3190641603 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1821162867 * L_0 = (Enumerator_t1821162867 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentKey_m1241282303((Enumerator_t1821162867 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m254358010_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3190641603 * _thisAdjusted = reinterpret_cast<Enumerator_t3190641603 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m254358010(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3092560164_gshared (Enumerator_t3190641603 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1821162867 * L_0 = (Enumerator_t1821162867 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m489023909((Enumerator_t1821162867 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3092560164_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3190641603 * _thisAdjusted = reinterpret_cast<Enumerator_t3190641603 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3092560164(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::Dispose()
extern "C"  void Enumerator_Dispose_m2438322117_gshared (Enumerator_t3190641603 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1821162867 * L_0 = (Enumerator_t1821162867 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2354198679((Enumerator_t1821162867 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2438322117_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3190641603 * _thisAdjusted = reinterpret_cast<Enumerator_t3190641603 *>(__this + 1);
	Enumerator_Dispose_m2438322117(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3431020440_gshared (Enumerator_t3190641603 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1821162867 * L_0 = (Enumerator_t1821162867 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3870539252((Enumerator_t1821162867 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m3431020440_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3190641603 * _thisAdjusted = reinterpret_cast<Enumerator_t3190641603 *>(__this + 1);
	return Enumerator_MoveNext_m3431020440(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2489864552_gshared (Enumerator_t3190641603 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1821162867 * L_0 = (Enumerator_t1821162867 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2553450683 * L_1 = (KeyValuePair_2_t2553450683 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m289678647((KeyValuePair_2_t2553450683 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m2489864552_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3190641603 * _thisAdjusted = reinterpret_cast<Enumerator_t3190641603 *>(__this + 1);
	return Enumerator_get_Current_m2489864552(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m556353567_gshared (Enumerator_t2616507329 * __this, Dictionary_2_t4221971187 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t4221971187 * L_0 = ___host0;
		NullCheck((Dictionary_2_t4221971187 *)L_0);
		Enumerator_t1247028593  L_1 = ((  Enumerator_t1247028593  (*) (Dictionary_2_t4221971187 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t4221971187 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m556353567_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t4221971187 * ___host0, const MethodInfo* method)
{
	Enumerator_t2616507329 * _thisAdjusted = reinterpret_cast<Enumerator_t2616507329 *>(__this + 1);
	Enumerator__ctor_m556353567(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3623312238_gshared (Enumerator_t2616507329 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1247028593 * L_0 = (Enumerator_t1247028593 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentKey_m672484517((Enumerator_t1247028593 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3623312238_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2616507329 * _thisAdjusted = reinterpret_cast<Enumerator_t2616507329 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3623312238(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3043893426_gshared (Enumerator_t2616507329 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1247028593 * L_0 = (Enumerator_t1247028593 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2206907019((Enumerator_t1247028593 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3043893426_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2616507329 * _thisAdjusted = reinterpret_cast<Enumerator_t2616507329 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3043893426(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::Dispose()
extern "C"  void Enumerator_Dispose_m495764931_gshared (Enumerator_t2616507329 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1247028593 * L_0 = (Enumerator_t1247028593 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m2832749025((Enumerator_t1247028593 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m495764931_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2616507329 * _thisAdjusted = reinterpret_cast<Enumerator_t2616507329 *>(__this + 1);
	Enumerator_Dispose_m495764931(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m623691698_gshared (Enumerator_t2616507329 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1247028593 * L_0 = (Enumerator_t1247028593 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m688389906((Enumerator_t1247028593 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m623691698_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2616507329 * _thisAdjusted = reinterpret_cast<Enumerator_t2616507329 *>(__this + 1);
	return Enumerator_MoveNext_m623691698(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1426407534_gshared (Enumerator_t2616507329 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1247028593 * L_0 = (Enumerator_t1247028593 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1979316409 * L_1 = (KeyValuePair_2_t1979316409 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2225619841((KeyValuePair_2_t1979316409 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m1426407534_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2616507329 * _thisAdjusted = reinterpret_cast<Enumerator_t2616507329 *>(__this + 1);
	return Enumerator_get_Current_m1426407534(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2650732273_gshared (Enumerator_t676045565 * __this, Dictionary_2_t2281509423 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		Enumerator_t3601534125  L_1 = ((  Enumerator_t3601534125  (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2650732273_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2281509423 * ___host0, const MethodInfo* method)
{
	Enumerator_t676045565 * _thisAdjusted = reinterpret_cast<Enumerator_t676045565 *>(__this + 1);
	Enumerator__ctor_m2650732273(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1623176564_gshared (Enumerator_t676045565 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentKey_m3839846791((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1623176564_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t676045565 * _thisAdjusted = reinterpret_cast<Enumerator_t676045565 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1623176564(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3446745716_gshared (Enumerator_t676045565 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3129803197((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3446745716_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t676045565 * _thisAdjusted = reinterpret_cast<Enumerator_t676045565 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3446745716(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1587212441_gshared (Enumerator_t676045565 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m1905011127((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1587212441_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t676045565 * _thisAdjusted = reinterpret_cast<Enumerator_t676045565 *>(__this + 1);
	Enumerator_Dispose_m1587212441(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m235441832_gshared (Enumerator_t676045565 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3349738440((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m235441832_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t676045565 * _thisAdjusted = reinterpret_cast<Enumerator_t676045565 *>(__this + 1);
	return Enumerator_MoveNext_m235441832(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m937156828_gshared (Enumerator_t676045565 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t38854645 * L_1 = (KeyValuePair_2_t38854645 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m937156828_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t676045565 * _thisAdjusted = reinterpret_cast<Enumerator_t676045565 *>(__this + 1);
	return Enumerator_get_Current_m937156828(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1836822257_gshared (Enumerator_t1125393968 * __this, Dictionary_2_t2730857826 * ___host0, const MethodInfo* method)
{
	{
		Dictionary_2_t2730857826 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2730857826 *)L_0);
		Enumerator_t4050882528  L_1 = ((  Enumerator_t4050882528  (*) (Dictionary_2_t2730857826 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)((Dictionary_2_t2730857826 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1836822257_AdjustorThunk (Il2CppObject * __this, Dictionary_2_t2730857826 * ___host0, const MethodInfo* method)
{
	Enumerator_t1125393968 * _thisAdjusted = reinterpret_cast<Enumerator_t1125393968 *>(__this + 1);
	Enumerator__ctor_m1836822257(_thisAdjusted, ___host0, method);
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2956120428_gshared (Enumerator_t1125393968 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		Il2CppObject * L_1 = Enumerator_get_CurrentKey_m2295079915((Enumerator_t4050882528 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2956120428_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1125393968 * _thisAdjusted = reinterpret_cast<Enumerator_t1125393968 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2956120428(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2980420916_gshared (Enumerator_t1125393968 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2489846157((Enumerator_t4050882528 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2980420916_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1125393968 * _thisAdjusted = reinterpret_cast<Enumerator_t1125393968 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2980420916(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C"  void Enumerator_Dispose_m2948365329_gshared (Enumerator_t1125393968 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Dispose_m793495907((Enumerator_t4050882528 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2948365329_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1125393968 * _thisAdjusted = reinterpret_cast<Enumerator_t1125393968 *>(__this + 1);
	Enumerator_Dispose_m2948365329(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1064937496_gshared (Enumerator_t1125393968 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2619008060((Enumerator_t4050882528 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1064937496_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1125393968 * _thisAdjusted = reinterpret_cast<Enumerator_t1125393968 *>(__this + 1);
	return Enumerator_MoveNext_m1064937496(_thisAdjusted, method);
}
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m892120696_gshared (Enumerator_t1125393968 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t488203048 * L_1 = (KeyValuePair_2_t488203048 *)L_0->get_address_of_current_3();
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m573362703((KeyValuePair_2_t488203048 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m892120696_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1125393968 * _thisAdjusted = reinterpret_cast<Enumerator_t1125393968 *>(__this + 1);
	return Enumerator_get_Current_m892120696(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m3844954056_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m3844954056_gshared (KeyCollection_t1813029214 * __this, Dictionary_2_t3624498739 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m3844954056_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3624498739 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t3624498739 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1586359978_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1586359978_gshared (KeyCollection_t1813029214 * __this, ChannelIdentity_t1147162267  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1586359978_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m141495059_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m141495059_gshared (KeyCollection_t1813029214 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m141495059_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m618768654_gshared (KeyCollection_t1813029214 * __this, ChannelIdentity_t1147162267  ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3624498739 * L_0 = (Dictionary_2_t3624498739 *)__this->get_dictionary_0();
		ChannelIdentity_t1147162267  L_1 = ___item0;
		NullCheck((Dictionary_2_t3624498739 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3624498739 *)L_0, (ChannelIdentity_t1147162267 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m381423875_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m381423875_gshared (KeyCollection_t1813029214 * __this, ChannelIdentity_t1147162267  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m381423875_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1377390213_gshared (KeyCollection_t1813029214 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1813029214 *)__this);
		Enumerator_t2019034881  L_0 = ((  Enumerator_t2019034881  (*) (KeyCollection_t1813029214 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1813029214 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2019034881  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3390687461_gshared (KeyCollection_t1813029214 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ChannelIdentityU5BU5D_t662656154* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ChannelIdentityU5BU5D_t662656154*)((ChannelIdentityU5BU5D_t662656154*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ChannelIdentityU5BU5D_t662656154* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ChannelIdentityU5BU5D_t662656154* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t1813029214 *)__this);
		((  void (*) (KeyCollection_t1813029214 *, ChannelIdentityU5BU5D_t662656154*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t1813029214 *)__this, (ChannelIdentityU5BU5D_t662656154*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t3624498739 * L_4 = (Dictionary_2_t3624498739 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t3624498739 *)L_4);
		((  void (*) (Dictionary_2_t3624498739 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3624498739 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3624498739 * L_7 = (Dictionary_2_t3624498739 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2200833417 * L_11 = (Transform_1_t2200833417 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2200833417 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3624498739 *)L_7);
		((  void (*) (Dictionary_2_t3624498739 *, Il2CppArray *, int32_t, Transform_1_t2200833417 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3624498739 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t2200833417 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m752030678_gshared (KeyCollection_t1813029214 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1813029214 *)__this);
		Enumerator_t2019034881  L_0 = ((  Enumerator_t2019034881  (*) (KeyCollection_t1813029214 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1813029214 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2019034881  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m49129367_gshared (KeyCollection_t1813029214 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3616415873_gshared (KeyCollection_t1813029214 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m3690809537_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3690809537_gshared (KeyCollection_t1813029214 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m3690809537_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3624498739 * L_0 = (Dictionary_2_t3624498739 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2763756323_gshared (KeyCollection_t1813029214 * __this, ChannelIdentityU5BU5D_t662656154* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t3624498739 * L_0 = (Dictionary_2_t3624498739 *)__this->get_dictionary_0();
		ChannelIdentityU5BU5D_t662656154* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t3624498739 *)L_0);
		((  void (*) (Dictionary_2_t3624498739 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3624498739 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3624498739 * L_3 = (Dictionary_2_t3624498739 *)__this->get_dictionary_0();
		ChannelIdentityU5BU5D_t662656154* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2200833417 * L_7 = (Transform_1_t2200833417 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2200833417 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3624498739 *)L_3);
		((  void (*) (Dictionary_2_t3624498739 *, ChannelIdentityU5BU5D_t662656154*, int32_t, Transform_1_t2200833417 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t3624498739 *)L_3, (ChannelIdentityU5BU5D_t662656154*)L_4, (int32_t)L_5, (Transform_1_t2200833417 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2019034881  KeyCollection_GetEnumerator_m2813588942_gshared (KeyCollection_t1813029214 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3624498739 * L_0 = (Dictionary_2_t3624498739 *)__this->get_dictionary_0();
		Enumerator_t2019034881  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m1552363313(&L_1, (Dictionary_2_t3624498739 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m136356229_gshared (KeyCollection_t1813029214 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3624498739 * L_0 = (Dictionary_2_t3624498739 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t3624498739 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t3624498739 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3624498739 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m3637797411_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m3637797411_gshared (KeyCollection_t3535513899 * __this, Dictionary_2_t1052016128 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m3637797411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1052016128 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t1052016128 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3195744869_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3195744869_gshared (KeyCollection_t3535513899 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3195744869_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m487537122_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m487537122_gshared (KeyCollection_t3535513899 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m487537122_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1128639255_gshared (KeyCollection_t3535513899 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t1052016128 * L_0 = (Dictionary_2_t1052016128 *)__this->get_dictionary_0();
		int32_t L_1 = ___item0;
		NullCheck((Dictionary_2_t1052016128 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1052016128 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1052016128 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2379875470_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2379875470_gshared (KeyCollection_t3535513899 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2379875470_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3003424020_gshared (KeyCollection_t3535513899 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t3535513899 *)__this);
		Enumerator_t3741519566  L_0 = ((  Enumerator_t3741519566  (*) (KeyCollection_t3535513899 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t3535513899 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3741519566  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m456001620_gshared (KeyCollection_t3535513899 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	CurrentRequestTypeU5BU5D_t2385140839* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (CurrentRequestTypeU5BU5D_t2385140839*)((CurrentRequestTypeU5BU5D_t2385140839*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		CurrentRequestTypeU5BU5D_t2385140839* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		CurrentRequestTypeU5BU5D_t2385140839* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t3535513899 *)__this);
		((  void (*) (KeyCollection_t3535513899 *, CurrentRequestTypeU5BU5D_t2385140839*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t3535513899 *)__this, (CurrentRequestTypeU5BU5D_t2385140839*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1052016128 * L_4 = (Dictionary_2_t1052016128 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t1052016128 *)L_4);
		((  void (*) (Dictionary_2_t1052016128 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1052016128 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1052016128 * L_7 = (Dictionary_2_t1052016128 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t435794015 * L_11 = (Transform_1_t435794015 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t435794015 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1052016128 *)L_7);
		((  void (*) (Dictionary_2_t1052016128 *, Il2CppArray *, int32_t, Transform_1_t435794015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1052016128 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t435794015 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4123609649_gshared (KeyCollection_t3535513899 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t3535513899 *)__this);
		Enumerator_t3741519566  L_0 = ((  Enumerator_t3741519566  (*) (KeyCollection_t3535513899 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t3535513899 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3741519566  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4118201988_gshared (KeyCollection_t3535513899 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4232312780_gshared (KeyCollection_t3535513899 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m4219179668_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m4219179668_gshared (KeyCollection_t3535513899 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m4219179668_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1052016128 * L_0 = (Dictionary_2_t1052016128 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3909347418_gshared (KeyCollection_t3535513899 * __this, CurrentRequestTypeU5BU5D_t2385140839* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t1052016128 * L_0 = (Dictionary_2_t1052016128 *)__this->get_dictionary_0();
		CurrentRequestTypeU5BU5D_t2385140839* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t1052016128 *)L_0);
		((  void (*) (Dictionary_2_t1052016128 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1052016128 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1052016128 * L_3 = (Dictionary_2_t1052016128 *)__this->get_dictionary_0();
		CurrentRequestTypeU5BU5D_t2385140839* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t435794015 * L_7 = (Transform_1_t435794015 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t435794015 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1052016128 *)L_3);
		((  void (*) (Dictionary_2_t1052016128 *, CurrentRequestTypeU5BU5D_t2385140839*, int32_t, Transform_1_t435794015 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t1052016128 *)L_3, (CurrentRequestTypeU5BU5D_t2385140839*)L_4, (int32_t)L_5, (Transform_1_t435794015 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3741519566  KeyCollection_GetEnumerator_m1163147327_gshared (KeyCollection_t3535513899 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1052016128 * L_0 = (Dictionary_2_t1052016128 *)__this->get_dictionary_0();
		Enumerator_t3741519566  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m1498955086(&L_1, (Dictionary_2_t1052016128 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3302451028_gshared (KeyCollection_t3535513899 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1052016128 * L_0 = (Dictionary_2_t1052016128 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t1052016128 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1052016128 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1052016128 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m3425792570_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m3425792570_gshared (KeyCollection_t3563200854 * __this, Dictionary_2_t1079703083 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m3425792570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1079703083 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t1079703083 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3194320320_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3194320320_gshared (KeyCollection_t3563200854 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3194320320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1966210003_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1966210003_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1966210003_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1541777540_gshared (KeyCollection_t3563200854 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		int32_t L_1 = ___item0;
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1079703083 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1079703083 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4021128907_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4021128907_gshared (KeyCollection_t3563200854 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4021128907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2865503421_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t3563200854 *)__this);
		Enumerator_t3769206521  L_0 = ((  Enumerator_t3769206521  (*) (KeyCollection_t3563200854 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t3563200854 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3769206521  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m4204328697_gshared (KeyCollection_t3563200854 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	Int32U5BU5D_t3030399641* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (Int32U5BU5D_t3030399641*)((Int32U5BU5D_t3030399641*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		Int32U5BU5D_t3030399641* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t3563200854 *)__this);
		((  void (*) (KeyCollection_t3563200854 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t3563200854 *)__this, (Int32U5BU5D_t3030399641*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1079703083 * L_4 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t1079703083 *)L_4);
		((  void (*) (Dictionary_2_t1079703083 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1079703083 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1079703083 * L_7 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2737799006 * L_11 = (Transform_1_t2737799006 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2737799006 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1079703083 *)L_7);
		((  void (*) (Dictionary_2_t1079703083 *, Il2CppArray *, int32_t, Transform_1_t2737799006 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1079703083 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t2737799006 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2660051048_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t3563200854 *)__this);
		Enumerator_t3769206521  L_0 = ((  Enumerator_t3769206521  (*) (KeyCollection_t3563200854 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t3563200854 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3769206521  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1819096223_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1330657765_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m1792869049_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1792869049_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m1792869049_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1956025779_gshared (KeyCollection_t3563200854 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Int32U5BU5D_t3030399641* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		((  void (*) (Dictionary_2_t1079703083 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1079703083 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1079703083 * L_3 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Int32U5BU5D_t3030399641* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2737799006 * L_7 = (Transform_1_t2737799006 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2737799006 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1079703083 *)L_3);
		((  void (*) (Dictionary_2_t1079703083 *, Int32U5BU5D_t3030399641*, int32_t, Transform_1_t2737799006 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t1079703083 *)L_3, (Int32U5BU5D_t3030399641*)L_4, (int32_t)L_5, (Transform_1_t2737799006 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t3769206521  KeyCollection_GetEnumerator_m2531464174_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		Enumerator_t3769206521  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m356032285(&L_1, (Dictionary_2_t1079703083 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1839036997_gshared (KeyCollection_t3563200854 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1079703083 * L_0 = (Dictionary_2_t1079703083 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1079703083 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1079703083 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m3555561037_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m3555561037_gshared (KeyCollection_t4180772701 * __this, Dictionary_2_t1697274930 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m3555561037_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1697274930 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t1697274930 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3743129415_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3743129415_gshared (KeyCollection_t4180772701 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3743129415_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m522538414_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m522538414_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m522538414_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3840423669_gshared (KeyCollection_t4180772701 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		int32_t L_1 = ___item0;
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1697274930 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1697274930 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2545700230_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2545700230_gshared (KeyCollection_t4180772701 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2545700230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3763330846_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t4180772701 *)__this);
		Enumerator_t91811072  L_0 = ((  Enumerator_t91811072  (*) (KeyCollection_t4180772701 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t4180772701 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t91811072  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m4142454252_gshared (KeyCollection_t4180772701 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	Int32U5BU5D_t3030399641* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (Int32U5BU5D_t3030399641*)((Int32U5BU5D_t3030399641*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		Int32U5BU5D_t3030399641* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t4180772701 *)__this);
		((  void (*) (KeyCollection_t4180772701 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t4180772701 *)__this, (Int32U5BU5D_t3030399641*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1697274930 * L_4 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t1697274930 *)L_4);
		((  void (*) (Dictionary_2_t1697274930 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1697274930 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1697274930 * L_7 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t3322033499 * L_11 = (Transform_1_t3322033499 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t3322033499 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1697274930 *)L_7);
		((  void (*) (Dictionary_2_t1697274930 *, Il2CppArray *, int32_t, Transform_1_t3322033499 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1697274930 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t3322033499 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1589406383_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t4180772701 *)__this);
		Enumerator_t91811072  L_0 = ((  Enumerator_t91811072  (*) (KeyCollection_t4180772701 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t4180772701 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t91811072  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1119857328_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1423289640_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m3802319686_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3802319686_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m3802319686_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m4056130090_gshared (KeyCollection_t4180772701 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Int32U5BU5D_t3030399641* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		((  void (*) (Dictionary_2_t1697274930 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1697274930 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1697274930 * L_3 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Int32U5BU5D_t3030399641* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t3322033499 * L_7 = (Transform_1_t3322033499 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t3322033499 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1697274930 *)L_3);
		((  void (*) (Dictionary_2_t1697274930 *, Int32U5BU5D_t3030399641*, int32_t, Transform_1_t3322033499 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t1697274930 *)L_3, (Int32U5BU5D_t3030399641*)L_4, (int32_t)L_5, (Transform_1_t3322033499 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::GetEnumerator()
extern "C"  Enumerator_t91811072  KeyCollection_GetEnumerator_m627495629_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		Enumerator_t91811072  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m2975000652(&L_1, (Dictionary_2_t1697274930 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2942903136_gshared (KeyCollection_t4180772701 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1697274930 * L_0 = (Dictionary_2_t1697274930 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1697274930 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1697274930 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Reflection.Emit.Label>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m1960943224_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m1960943224_gshared (KeyCollection_t1439558770 * __this, Dictionary_2_t3251028295 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m1960943224_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3251028295 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t3251028295 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Reflection.Emit.Label>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3765215162_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3765215162_gshared (KeyCollection_t1439558770 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3765215162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Reflection.Emit.Label>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4172046637_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4172046637_gshared (KeyCollection_t1439558770 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4172046637_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Reflection.Emit.Label>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3591138502_gshared (KeyCollection_t1439558770 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3251028295 * L_0 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		int32_t L_1 = ___item0;
		NullCheck((Dictionary_2_t3251028295 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3251028295 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3251028295 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Reflection.Emit.Label>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1687853025_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1687853025_gshared (KeyCollection_t1439558770 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1687853025_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Reflection.Emit.Label>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3833884007_gshared (KeyCollection_t1439558770 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1439558770 *)__this);
		Enumerator_t1645564437  L_0 = ((  Enumerator_t1645564437  (*) (KeyCollection_t1439558770 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1439558770 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1645564437  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Reflection.Emit.Label>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m735753907_gshared (KeyCollection_t1439558770 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	Int32U5BU5D_t3030399641* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (Int32U5BU5D_t3030399641*)((Int32U5BU5D_t3030399641*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		Int32U5BU5D_t3030399641* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t1439558770 *)__this);
		((  void (*) (KeyCollection_t1439558770 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t1439558770 *)__this, (Int32U5BU5D_t3030399641*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t3251028295 * L_4 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t3251028295 *)L_4);
		((  void (*) (Dictionary_2_t3251028295 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3251028295 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3251028295 * L_7 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t4198889554 * L_11 = (Transform_1_t4198889554 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t4198889554 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3251028295 *)L_7);
		((  void (*) (Dictionary_2_t3251028295 *, Il2CppArray *, int32_t, Transform_1_t4198889554 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3251028295 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t4198889554 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Reflection.Emit.Label>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4274130206_gshared (KeyCollection_t1439558770 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1439558770 *)__this);
		Enumerator_t1645564437  L_0 = ((  Enumerator_t1645564437  (*) (KeyCollection_t1439558770 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1439558770 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1645564437  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Reflection.Emit.Label>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1745579221_gshared (KeyCollection_t1439558770 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Reflection.Emit.Label>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2961185403_gshared (KeyCollection_t1439558770 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Reflection.Emit.Label>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m1520541523_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1520541523_gshared (KeyCollection_t1439558770 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m1520541523_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3251028295 * L_0 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Reflection.Emit.Label>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3712619129_gshared (KeyCollection_t1439558770 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t3251028295 * L_0 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		Int32U5BU5D_t3030399641* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t3251028295 *)L_0);
		((  void (*) (Dictionary_2_t3251028295 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3251028295 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3251028295 * L_3 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		Int32U5BU5D_t3030399641* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t4198889554 * L_7 = (Transform_1_t4198889554 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t4198889554 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3251028295 *)L_3);
		((  void (*) (Dictionary_2_t3251028295 *, Int32U5BU5D_t3030399641*, int32_t, Transform_1_t4198889554 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t3251028295 *)L_3, (Int32U5BU5D_t3030399641*)L_4, (int32_t)L_5, (Transform_1_t4198889554 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Reflection.Emit.Label>::GetEnumerator()
extern "C"  Enumerator_t1645564437  KeyCollection_GetEnumerator_m190454004_gshared (KeyCollection_t1439558770 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3251028295 * L_0 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		Enumerator_t1645564437  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m393153339(&L_1, (Dictionary_2_t3251028295 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Reflection.Emit.Label>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3461380643_gshared (KeyCollection_t1439558770 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3251028295 * L_0 = (Dictionary_2_t3251028295 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t3251028295 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t3251028295 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3251028295 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m2090490505_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m2090490505_gshared (KeyCollection_t1249926325 * __this, Dictionary_2_t3061395850 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m2090490505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3061395850 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t3061395850 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4146125139_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4146125139_gshared (KeyCollection_t1249926325 * __this, int64_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4146125139_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3389197820_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3389197820_gshared (KeyCollection_t1249926325 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3389197820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1682535721_gshared (KeyCollection_t1249926325 * __this, int64_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3061395850 * L_0 = (Dictionary_2_t3061395850 *)__this->get_dictionary_0();
		int64_t L_1 = ___item0;
		NullCheck((Dictionary_2_t3061395850 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3061395850 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3061395850 *)L_0, (int64_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3267765020_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3267765020_gshared (KeyCollection_t1249926325 * __this, int64_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3267765020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2903263754_gshared (KeyCollection_t1249926325 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1249926325 *)__this);
		Enumerator_t1455931992  L_0 = ((  Enumerator_t1455931992  (*) (KeyCollection_t1249926325 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1249926325 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1455931992  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3315901882_gshared (KeyCollection_t1249926325 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	Int64U5BU5D_t717125112* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (Int64U5BU5D_t717125112*)((Int64U5BU5D_t717125112*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		Int64U5BU5D_t717125112* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		Int64U5BU5D_t717125112* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t1249926325 *)__this);
		((  void (*) (KeyCollection_t1249926325 *, Int64U5BU5D_t717125112*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t1249926325 *)__this, (Int64U5BU5D_t717125112*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t3061395850 * L_4 = (Dictionary_2_t3061395850 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t3061395850 *)L_4);
		((  void (*) (Dictionary_2_t3061395850 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3061395850 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3061395850 * L_7 = (Dictionary_2_t3061395850 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t3927277168 * L_11 = (Transform_1_t3927277168 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t3927277168 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3061395850 *)L_7);
		((  void (*) (Dictionary_2_t3061395850 *, Il2CppArray *, int32_t, Transform_1_t3927277168 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3061395850 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t3927277168 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m112939771_gshared (KeyCollection_t1249926325 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1249926325 *)__this);
		Enumerator_t1455931992  L_0 = ((  Enumerator_t1455931992  (*) (KeyCollection_t1249926325 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1249926325 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1455931992  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m343543538_gshared (KeyCollection_t1249926325 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2813120990_gshared (KeyCollection_t1249926325 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m290654106_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m290654106_gshared (KeyCollection_t1249926325 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m290654106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3061395850 * L_0 = (Dictionary_2_t3061395850 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2833774912_gshared (KeyCollection_t1249926325 * __this, Int64U5BU5D_t717125112* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t3061395850 * L_0 = (Dictionary_2_t3061395850 *)__this->get_dictionary_0();
		Int64U5BU5D_t717125112* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t3061395850 *)L_0);
		((  void (*) (Dictionary_2_t3061395850 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3061395850 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3061395850 * L_3 = (Dictionary_2_t3061395850 *)__this->get_dictionary_0();
		Int64U5BU5D_t717125112* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t3927277168 * L_7 = (Transform_1_t3927277168 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t3927277168 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3061395850 *)L_3);
		((  void (*) (Dictionary_2_t3061395850 *, Int64U5BU5D_t717125112*, int32_t, Transform_1_t3927277168 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t3061395850 *)L_3, (Int64U5BU5D_t717125112*)L_4, (int32_t)L_5, (Transform_1_t3927277168 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t1455931992  KeyCollection_GetEnumerator_m547080581_gshared (KeyCollection_t1249926325 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3061395850 * L_0 = (Dictionary_2_t3061395850 *)__this->get_dictionary_0();
		Enumerator_t1455931992  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m3427160064(&L_1, (Dictionary_2_t3061395850 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Int32>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1938989726_gshared (KeyCollection_t1249926325 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3061395850 * L_0 = (Dictionary_2_t3061395850 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t3061395850 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t3061395850 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3061395850 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m3885543862_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m3885543862_gshared (KeyCollection_t1867498172 * __this, Dictionary_2_t3678967697 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m3885543862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3678967697 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t3678967697 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3319720888_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3319720888_gshared (KeyCollection_t1867498172 * __this, int64_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3319720888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3601347139_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3601347139_gshared (KeyCollection_t1867498172 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3601347139_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2597166232_gshared (KeyCollection_t1867498172 * __this, int64_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3678967697 * L_0 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		int64_t L_1 = ___item0;
		NullCheck((Dictionary_2_t3678967697 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3678967697 *, int64_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3678967697 *)L_0, (int64_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1060868683_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1060868683_gshared (KeyCollection_t1867498172 * __this, int64_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1060868683_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m152152421_gshared (KeyCollection_t1867498172 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1867498172 *)__this);
		Enumerator_t2073503839  L_0 = ((  Enumerator_t2073503839  (*) (KeyCollection_t1867498172 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1867498172 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2073503839  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1539094133_gshared (KeyCollection_t1867498172 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	Int64U5BU5D_t717125112* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (Int64U5BU5D_t717125112*)((Int64U5BU5D_t717125112*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		Int64U5BU5D_t717125112* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		Int64U5BU5D_t717125112* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t1867498172 *)__this);
		((  void (*) (KeyCollection_t1867498172 *, Int64U5BU5D_t717125112*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t1867498172 *)__this, (Int64U5BU5D_t717125112*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t3678967697 * L_4 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t3678967697 *)L_4);
		((  void (*) (Dictionary_2_t3678967697 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3678967697 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3678967697 * L_7 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t216544365 * L_11 = (Transform_1_t216544365 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t216544365 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3678967697 *)L_7);
		((  void (*) (Dictionary_2_t3678967697 *, Il2CppArray *, int32_t, Transform_1_t216544365 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3678967697 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t216544365 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1351799188_gshared (KeyCollection_t1867498172 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1867498172 *)__this);
		Enumerator_t2073503839  L_0 = ((  Enumerator_t2073503839  (*) (KeyCollection_t1867498172 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1867498172 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2073503839  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3534394359_gshared (KeyCollection_t1867498172 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3200643657_gshared (KeyCollection_t1867498172 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m1440087769_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1440087769_gshared (KeyCollection_t1867498172 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m1440087769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3678967697 * L_0 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1238003947_gshared (KeyCollection_t1867498172 * __this, Int64U5BU5D_t717125112* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t3678967697 * L_0 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		Int64U5BU5D_t717125112* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t3678967697 *)L_0);
		((  void (*) (Dictionary_2_t3678967697 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3678967697 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3678967697 * L_3 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		Int64U5BU5D_t717125112* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t216544365 * L_7 = (Transform_1_t216544365 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t216544365 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3678967697 *)L_3);
		((  void (*) (Dictionary_2_t3678967697 *, Int64U5BU5D_t717125112*, int32_t, Transform_1_t216544365 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t3678967697 *)L_3, (Int64U5BU5D_t717125112*)L_4, (int32_t)L_5, (Transform_1_t216544365 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2073503839  KeyCollection_GetEnumerator_m3687526536_gshared (KeyCollection_t1867498172 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3678967697 * L_0 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		Enumerator_t2073503839  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m3400877137(&L_1, (Dictionary_2_t3678967697 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m972382317_gshared (KeyCollection_t1867498172 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3678967697 * L_0 = (Dictionary_2_t3678967697 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t3678967697 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t3678967697 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3678967697 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m323559081_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m323559081_gshared (KeyCollection_t702433241 * __this, Dictionary_2_t2513902766 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m323559081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2513902766 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t2513902766 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2151978331_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2151978331_gshared (KeyCollection_t702433241 * __this, IntPtr_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2151978331_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2029052144_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2029052144_gshared (KeyCollection_t702433241 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2029052144_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m164383913_gshared (KeyCollection_t702433241 * __this, IntPtr_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t2513902766 * L_0 = (Dictionary_2_t2513902766 *)__this->get_dictionary_0();
		IntPtr_t L_1 = ___item0;
		NullCheck((Dictionary_2_t2513902766 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t2513902766 *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2513902766 *)L_0, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m305519472_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m305519472_gshared (KeyCollection_t702433241 * __this, IntPtr_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m305519472_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m228846932_gshared (KeyCollection_t702433241 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t702433241 *)__this);
		Enumerator_t908438908  L_0 = ((  Enumerator_t908438908  (*) (KeyCollection_t702433241 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t702433241 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t908438908  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m748083542_gshared (KeyCollection_t702433241 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	IntPtrU5BU5D_t169632028* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (IntPtrU5BU5D_t169632028*)((IntPtrU5BU5D_t169632028*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		IntPtrU5BU5D_t169632028* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		IntPtrU5BU5D_t169632028* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t702433241 *)__this);
		((  void (*) (KeyCollection_t702433241 *, IntPtrU5BU5D_t169632028*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t702433241 *)__this, (IntPtrU5BU5D_t169632028*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t2513902766 * L_4 = (Dictionary_2_t2513902766 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t2513902766 *)L_4);
		((  void (*) (Dictionary_2_t2513902766 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2513902766 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2513902766 * L_7 = (Dictionary_2_t2513902766 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1584656104 * L_11 = (Transform_1_t1584656104 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1584656104 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2513902766 *)L_7);
		((  void (*) (Dictionary_2_t2513902766 *, Il2CppArray *, int32_t, Transform_1_t1584656104 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2513902766 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t1584656104 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1236899363_gshared (KeyCollection_t702433241 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t702433241 *)__this);
		Enumerator_t908438908  L_0 = ((  Enumerator_t908438908  (*) (KeyCollection_t702433241 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t702433241 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t908438908  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m547843110_gshared (KeyCollection_t702433241 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4009966266_gshared (KeyCollection_t702433241 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m3245291068_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3245291068_gshared (KeyCollection_t702433241 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m3245291068_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2513902766 * L_0 = (Dictionary_2_t2513902766 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m4123737228_gshared (KeyCollection_t702433241 * __this, IntPtrU5BU5D_t169632028* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t2513902766 * L_0 = (Dictionary_2_t2513902766 *)__this->get_dictionary_0();
		IntPtrU5BU5D_t169632028* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t2513902766 *)L_0);
		((  void (*) (Dictionary_2_t2513902766 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2513902766 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2513902766 * L_3 = (Dictionary_2_t2513902766 *)__this->get_dictionary_0();
		IntPtrU5BU5D_t169632028* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1584656104 * L_7 = (Transform_1_t1584656104 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1584656104 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2513902766 *)L_3);
		((  void (*) (Dictionary_2_t2513902766 *, IntPtrU5BU5D_t169632028*, int32_t, Transform_1_t1584656104 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t2513902766 *)L_3, (IntPtrU5BU5D_t169632028*)L_4, (int32_t)L_5, (Transform_1_t1584656104 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t908438908  KeyCollection_GetEnumerator_m2489792873_gshared (KeyCollection_t702433241 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2513902766 * L_0 = (Dictionary_2_t2513902766 *)__this->get_dictionary_0();
		Enumerator_t908438908  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m1331665702(&L_1, (Dictionary_2_t2513902766 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Int32>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2256401266_gshared (KeyCollection_t702433241 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2513902766 * L_0 = (Dictionary_2_t2513902766 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t2513902766 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t2513902766 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2513902766 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m40025794_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m40025794_gshared (KeyCollection_t1320005088 * __this, Dictionary_2_t3131474613 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m40025794_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3131474613 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t3131474613 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m295062388_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m295062388_gshared (KeyCollection_t1320005088 * __this, IntPtr_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m295062388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1170030795_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1170030795_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1170030795_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m671614396_gshared (KeyCollection_t1320005088 * __this, IntPtr_t ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3131474613 * L_0 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		IntPtr_t L_1 = ___item0;
		NullCheck((Dictionary_2_t3131474613 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3131474613 *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3131474613 *)L_0, (IntPtr_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3118644019_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3118644019_gshared (KeyCollection_t1320005088 * __this, IntPtr_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3118644019_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2104224529_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1320005088 *)__this);
		Enumerator_t1526010755  L_0 = ((  Enumerator_t1526010755  (*) (KeyCollection_t1320005088 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1320005088 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1526010755  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m236144141_gshared (KeyCollection_t1320005088 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	IntPtrU5BU5D_t169632028* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (IntPtrU5BU5D_t169632028*)((IntPtrU5BU5D_t169632028*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		IntPtrU5BU5D_t169632028* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		IntPtrU5BU5D_t169632028* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t1320005088 *)__this);
		((  void (*) (KeyCollection_t1320005088 *, IntPtrU5BU5D_t169632028*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t1320005088 *)__this, (IntPtrU5BU5D_t169632028*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t3131474613 * L_4 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t3131474613 *)L_4);
		((  void (*) (Dictionary_2_t3131474613 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3131474613 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3131474613 * L_7 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2168890597 * L_11 = (Transform_1_t2168890597 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2168890597 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3131474613 *)L_7);
		((  void (*) (Dictionary_2_t3131474613 *, Il2CppArray *, int32_t, Transform_1_t2168890597 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3131474613 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t2168890597 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2864841844_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1320005088 *)__this);
		Enumerator_t1526010755  L_0 = ((  Enumerator_t1526010755  (*) (KeyCollection_t1320005088 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1320005088 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1526010755  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2924879015_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1336146065_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m1967424173_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1967424173_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m1967424173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3131474613 * L_0 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3820790603_gshared (KeyCollection_t1320005088 * __this, IntPtrU5BU5D_t169632028* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t3131474613 * L_0 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		IntPtrU5BU5D_t169632028* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t3131474613 *)L_0);
		((  void (*) (Dictionary_2_t3131474613 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3131474613 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3131474613 * L_3 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		IntPtrU5BU5D_t169632028* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t2168890597 * L_7 = (Transform_1_t2168890597 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t2168890597 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3131474613 *)L_3);
		((  void (*) (Dictionary_2_t3131474613 *, IntPtrU5BU5D_t169632028*, int32_t, Transform_1_t2168890597 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t3131474613 *)L_3, (IntPtrU5BU5D_t169632028*)L_4, (int32_t)L_5, (Transform_1_t2168890597 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1526010755  KeyCollection_GetEnumerator_m349130886_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3131474613 * L_0 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		Enumerator_t1526010755  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m3235517721(&L_1, (Dictionary_2_t3131474613 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1219588529_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3131474613 * L_0 = (Dictionary_2_t3131474613 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t3131474613 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t3131474613 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3131474613 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m1150705803_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m1150705803_gshared (KeyCollection_t1606165321 * __this, Dictionary_2_t3417634846 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m1150705803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3417634846 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t3417634846 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3083232821_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3083232821_gshared (KeyCollection_t1606165321 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3083232821_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1899586830_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1899586830_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1899586830_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m4184116031_gshared (KeyCollection_t1606165321 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t3417634846 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t3417634846 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3417634846 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2523591834_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2523591834_gshared (KeyCollection_t1606165321 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2523591834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2668668382_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1606165321 *)__this);
		Enumerator_t1812170988  L_0 = ((  Enumerator_t1812170988  (*) (KeyCollection_t1606165321 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1606165321 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1812170988  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2400289464_gshared (KeyCollection_t1606165321 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t1606165321 *)__this);
		((  void (*) (KeyCollection_t1606165321 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t1606165321 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t3417634846 * L_4 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t3417634846 *)L_4);
		((  void (*) (Dictionary_2_t3417634846 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3417634846 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3417634846 * L_7 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1854428742 * L_11 = (Transform_1_t1854428742 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1854428742 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3417634846 *)L_7);
		((  void (*) (Dictionary_2_t3417634846 *, Il2CppArray *, int32_t, Transform_1_t1854428742 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t3417634846 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t1854428742 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3918984589_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t1606165321 *)__this);
		Enumerator_t1812170988  L_0 = ((  Enumerator_t1812170988  (*) (KeyCollection_t1606165321 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t1606165321 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1812170988  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2773140624_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1443910992_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m4267671038_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m4267671038_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m4267671038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t3417634846 * L_0 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m668976366_gshared (KeyCollection_t1606165321 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t3417634846 *)L_0);
		((  void (*) (Dictionary_2_t3417634846 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t3417634846 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t3417634846 * L_3 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1854428742 * L_7 = (Transform_1_t1854428742 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1854428742 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t3417634846 *)L_3);
		((  void (*) (Dictionary_2_t3417634846 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1854428742 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t3417634846 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t1854428742 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::GetEnumerator()
extern "C"  Enumerator_t1812170988  KeyCollection_GetEnumerator_m136587363_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		Enumerator_t1812170988  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m4035163080(&L_1, (Dictionary_2_t3417634846 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m515229044_gshared (KeyCollection_t1606165321 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3417634846 * L_0 = (Dictionary_2_t3417634846 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t3417634846 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t3417634846 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t3417634846 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m1627846113_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m1627846113_gshared (KeyCollection_t4147435347 * __this, Dictionary_2_t1663937576 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m1627846113_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1663937576 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t1663937576 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3888838575_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3888838575_gshared (KeyCollection_t4147435347 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3888838575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m237922362_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m237922362_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m237922362_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1114577217_gshared (KeyCollection_t4147435347 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1663937576 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1663937576 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2082552954_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2082552954_gshared (KeyCollection_t4147435347 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2082552954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4237862166_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t4147435347 *)__this);
		Enumerator_t58473718  L_0 = ((  Enumerator_t58473718  (*) (KeyCollection_t4147435347 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t4147435347 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t58473718  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3219210900_gshared (KeyCollection_t4147435347 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t4147435347 *)__this);
		((  void (*) (KeyCollection_t4147435347 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t4147435347 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t1663937576 * L_4 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t1663937576 *)L_4);
		((  void (*) (Dictionary_2_t1663937576 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1663937576 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1663937576 * L_7 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1316794068 * L_11 = (Transform_1_t1316794068 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1316794068 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1663937576 *)L_7);
		((  void (*) (Dictionary_2_t1663937576 *, Il2CppArray *, int32_t, Transform_1_t1316794068 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t1663937576 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t1316794068 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3069883463_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t4147435347 *)__this);
		Enumerator_t58473718  L_0 = ((  Enumerator_t58473718  (*) (KeyCollection_t4147435347 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t4147435347 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t58473718  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2823494596_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1497186736_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m3825728526_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3825728526_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m3825728526_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1942070334_gshared (KeyCollection_t4147435347 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		((  void (*) (Dictionary_2_t1663937576 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t1663937576 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t1663937576 * L_3 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1316794068 * L_7 = (Transform_1_t1316794068 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1316794068 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t1663937576 *)L_3);
		((  void (*) (Dictionary_2_t1663937576 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1316794068 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t1663937576 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t1316794068 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t58473718  KeyCollection_GetEnumerator_m3925297057_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		Enumerator_t58473718  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m1642475044(&L_1, (Dictionary_2_t1663937576 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3930094200_gshared (KeyCollection_t4147435347 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t1663937576 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1663937576 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m2305984430_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m2305984430_gshared (KeyCollection_t2984635936 * __this, Dictionary_2_t501138165 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m2305984430_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t501138165 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t501138165 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int64>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1499493700_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1499493700_gshared (KeyCollection_t2984635936 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1499493700_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int64>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2719844203_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2719844203_gshared (KeyCollection_t2984635936 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2719844203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int64>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1004967664_gshared (KeyCollection_t2984635936 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t501138165 * L_0 = (Dictionary_2_t501138165 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t501138165 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t501138165 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t501138165 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int64>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3322301491_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3322301491_gshared (KeyCollection_t2984635936 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3322301491_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int64>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m373755025_gshared (KeyCollection_t2984635936 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t2984635936 *)__this);
		Enumerator_t3190641603  L_0 = ((  Enumerator_t3190641603  (*) (KeyCollection_t2984635936 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t2984635936 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3190641603  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int64>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3115402881_gshared (KeyCollection_t2984635936 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t2984635936 *)__this);
		((  void (*) (KeyCollection_t2984635936 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t2984635936 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t501138165 * L_4 = (Dictionary_2_t501138165 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t501138165 *)L_4);
		((  void (*) (Dictionary_2_t501138165 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t501138165 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t501138165 * L_7 = (Dictionary_2_t501138165 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t3298486835 * L_11 = (Transform_1_t3298486835 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t3298486835 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t501138165 *)L_7);
		((  void (*) (Dictionary_2_t501138165 *, Il2CppArray *, int32_t, Transform_1_t3298486835 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t501138165 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t3298486835 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int64>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2515400352_gshared (KeyCollection_t2984635936 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t2984635936 *)__this);
		Enumerator_t3190641603  L_0 = ((  Enumerator_t3190641603  (*) (KeyCollection_t2984635936 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t2984635936 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t3190641603  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int64>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4102039615_gshared (KeyCollection_t2984635936 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int64>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3993935453_gshared (KeyCollection_t2984635936 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int64>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m3854979461_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3854979461_gshared (KeyCollection_t2984635936 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m3854979461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t501138165 * L_0 = (Dictionary_2_t501138165 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int64>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m520703075_gshared (KeyCollection_t2984635936 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t501138165 * L_0 = (Dictionary_2_t501138165 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t501138165 *)L_0);
		((  void (*) (Dictionary_2_t501138165 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t501138165 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t501138165 * L_3 = (Dictionary_2_t501138165 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t3298486835 * L_7 = (Transform_1_t3298486835 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t3298486835 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t501138165 *)L_3);
		((  void (*) (Dictionary_2_t501138165 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t3298486835 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t501138165 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t3298486835 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int64>::GetEnumerator()
extern "C"  Enumerator_t3190641603  KeyCollection_GetEnumerator_m33788928_gshared (KeyCollection_t2984635936 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t501138165 * L_0 = (Dictionary_2_t501138165 *)__this->get_dictionary_0();
		Enumerator_t3190641603  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m3766187165(&L_1, (Dictionary_2_t501138165 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int64>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m950030649_gshared (KeyCollection_t2984635936 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t501138165 * L_0 = (Dictionary_2_t501138165 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t501138165 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t501138165 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t501138165 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Nullable`1<System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m2043260022_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m2043260022_gshared (KeyCollection_t2410501662 * __this, Dictionary_2_t4221971187 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m2043260022_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t4221971187 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t4221971187 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Nullable`1<System.Int32>>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3428827944_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3428827944_gshared (KeyCollection_t2410501662 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3428827944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Nullable`1<System.Int32>>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3627762673_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3627762673_gshared (KeyCollection_t2410501662 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3627762673_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Nullable`1<System.Int32>>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m679768904_gshared (KeyCollection_t2410501662 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t4221971187 * L_0 = (Dictionary_2_t4221971187 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t4221971187 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t4221971187 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t4221971187 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Nullable`1<System.Int32>>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4140358365_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4140358365_gshared (KeyCollection_t2410501662 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4140358365_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Nullable`1<System.Int32>>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4005946819_gshared (KeyCollection_t2410501662 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t2410501662 *)__this);
		Enumerator_t2616507329  L_0 = ((  Enumerator_t2616507329  (*) (KeyCollection_t2410501662 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t2410501662 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2616507329  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Nullable`1<System.Int32>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3422803479_gshared (KeyCollection_t2410501662 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t2410501662 *)__this);
		((  void (*) (KeyCollection_t2410501662 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t2410501662 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t4221971187 * L_4 = (Dictionary_2_t4221971187 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t4221971187 *)L_4);
		((  void (*) (Dictionary_2_t4221971187 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t4221971187 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t4221971187 * L_7 = (Dictionary_2_t4221971187 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t67869821 * L_11 = (Transform_1_t67869821 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t67869821 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t4221971187 *)L_7);
		((  void (*) (Dictionary_2_t4221971187 *, Il2CppArray *, int32_t, Transform_1_t67869821 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t4221971187 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t67869821 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1277044908_gshared (KeyCollection_t2410501662 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t2410501662 *)__this);
		Enumerator_t2616507329  L_0 = ((  Enumerator_t2616507329  (*) (KeyCollection_t2410501662 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t2410501662 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t2616507329  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Nullable`1<System.Int32>>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2857207849_gshared (KeyCollection_t2410501662 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Nullable`1<System.Int32>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m492541175_gshared (KeyCollection_t2410501662 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Nullable`1<System.Int32>>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m330716391_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m330716391_gshared (KeyCollection_t2410501662 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m330716391_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t4221971187 * L_0 = (Dictionary_2_t4221971187 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Nullable`1<System.Int32>>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2605093053_gshared (KeyCollection_t2410501662 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t4221971187 * L_0 = (Dictionary_2_t4221971187 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t4221971187 *)L_0);
		((  void (*) (Dictionary_2_t4221971187 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t4221971187 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t4221971187 * L_3 = (Dictionary_2_t4221971187 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t67869821 * L_7 = (Transform_1_t67869821 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t67869821 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t4221971187 *)L_3);
		((  void (*) (Dictionary_2_t4221971187 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t67869821 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t4221971187 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t67869821 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Nullable`1<System.Int32>>::GetEnumerator()
extern "C"  Enumerator_t2616507329  KeyCollection_GetEnumerator_m1530211906_gshared (KeyCollection_t2410501662 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t4221971187 * L_0 = (Dictionary_2_t4221971187 *)__this->get_dictionary_0();
		Enumerator_t2616507329  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m556353567(&L_1, (Dictionary_2_t4221971187 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Nullable`1<System.Int32>>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3837465343_gshared (KeyCollection_t2410501662 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t4221971187 * L_0 = (Dictionary_2_t4221971187 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t4221971187 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t4221971187 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t4221971187 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m4000691336_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m4000691336_gshared (KeyCollection_t470039898 * __this, Dictionary_2_t2281509423 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m4000691336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2281509423 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t2281509423 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m726860246_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m726860246_gshared (KeyCollection_t470039898 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m726860246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3185000447_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3185000447_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3185000447_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m580889838_gshared (KeyCollection_t470039898 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2281509423 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1818919095_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1818919095_gshared (KeyCollection_t470039898 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1818919095_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m701895513_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t470039898 *)__this);
		Enumerator_t676045565  L_0 = ((  Enumerator_t676045565  (*) (KeyCollection_t470039898 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t470039898 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t676045565  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m201091229_gshared (KeyCollection_t470039898 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t470039898 *)__this);
		((  void (*) (KeyCollection_t470039898 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t470039898 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t2281509423 * L_4 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t2281509423 *)L_4);
		((  void (*) (Dictionary_2_t2281509423 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2281509423 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2281509423 * L_7 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1901028561 * L_11 = (Transform_1_t1901028561 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1901028561 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2281509423 *)L_7);
		((  void (*) (Dictionary_2_t2281509423 *, Il2CppArray *, int32_t, Transform_1_t1901028561 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2281509423 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t1901028561 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1743416022_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t470039898 *)__this);
		Enumerator_t676045565  L_0 = ((  Enumerator_t676045565  (*) (KeyCollection_t470039898 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t470039898 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t676045565  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m701366755_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4278618649_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m3348206461_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3348206461_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m3348206461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1469814847_gshared (KeyCollection_t470039898 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		((  void (*) (Dictionary_2_t2281509423 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2281509423 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2281509423 * L_3 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t1901028561 * L_7 = (Transform_1_t1901028561 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t1901028561 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2281509423 *)L_3);
		((  void (*) (Dictionary_2_t2281509423 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t1901028561 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t2281509423 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t1901028561 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t676045565  KeyCollection_GetEnumerator_m3123493604_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		Enumerator_t676045565  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m2650732273(&L_1, (Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2913499705_gshared (KeyCollection_t470039898 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3629755714;
extern const uint32_t KeyCollection__ctor_m2534614652_MetadataUsageId;
extern "C"  void KeyCollection__ctor_m2534614652_gshared (KeyCollection_t919388301 * __this, Dictionary_2_t2730857826 * ___dictionary0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection__ctor_m2534614652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2730857826 * L_0 = ___dictionary0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3629755714, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Dictionary_2_t2730857826 * L_2 = ___dictionary0;
		__this->set_dictionary_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m93312222_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m93312222_gshared (KeyCollection_t919388301 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m93312222_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<TKey>.Clear()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1060683979_MetadataUsageId;
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1060683979_gshared (KeyCollection_t919388301 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1060683979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m167656554_gshared (KeyCollection_t919388301 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Dictionary_2_t2730857826 * L_0 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t2730857826 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t2730857826 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2730857826 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4128881964;
extern const uint32_t KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m936199739_MetadataUsageId;
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m936199739_gshared (KeyCollection_t919388301 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m936199739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral4128881964, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4209558273_gshared (KeyCollection_t919388301 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t919388301 *)__this);
		Enumerator_t1125393968  L_0 = ((  Enumerator_t1125393968  (*) (KeyCollection_t919388301 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t919388301 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1125393968  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m196917069_gshared (KeyCollection_t919388301 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = ___index1;
		NullCheck((KeyCollection_t919388301 *)__this);
		((  void (*) (KeyCollection_t919388301 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((KeyCollection_t919388301 *)__this, (ObjectU5BU5D_t3614634134*)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return;
	}

IL_0016:
	{
		Dictionary_2_t2730857826 * L_4 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		Il2CppArray * L_5 = ___array0;
		int32_t L_6 = ___index1;
		NullCheck((Dictionary_2_t2730857826 *)L_4);
		((  void (*) (Dictionary_2_t2730857826 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2730857826 *)L_4, (Il2CppArray *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2730857826 * L_7 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		Il2CppArray * L_8 = ___array0;
		int32_t L_9 = ___index1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t4054574962 * L_11 = (Transform_1_t4054574962 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t4054574962 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_11, (Il2CppObject *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2730857826 *)L_7);
		((  void (*) (Dictionary_2_t2730857826 *, Il2CppArray *, int32_t, Transform_1_t4054574962 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2730857826 *)L_7, (Il2CppArray *)L_8, (int32_t)L_9, (Transform_1_t4054574962 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1631701166_gshared (KeyCollection_t919388301 * __this, const MethodInfo* method)
{
	{
		NullCheck((KeyCollection_t919388301 *)__this);
		Enumerator_t1125393968  L_0 = ((  Enumerator_t1125393968  (*) (KeyCollection_t919388301 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((KeyCollection_t919388301 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Enumerator_t1125393968  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1142379495_gshared (KeyCollection_t919388301 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m778075033_gshared (KeyCollection_t919388301 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t91669223_il2cpp_TypeInfo_var;
extern const uint32_t KeyCollection_System_Collections_ICollection_get_SyncRoot_m2096719453_MetadataUsageId;
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2096719453_gshared (KeyCollection_t919388301 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyCollection_System_Collections_ICollection_get_SyncRoot_m2096719453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2730857826 * L_0 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t91669223_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1945683251_gshared (KeyCollection_t919388301 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Dictionary_2_t2730857826 * L_0 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Dictionary_2_t2730857826 *)L_0);
		((  void (*) (Dictionary_2_t2730857826 *, Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((Dictionary_2_t2730857826 *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		Dictionary_2_t2730857826 * L_3 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		ObjectU5BU5D_t3614634134* L_4 = ___array0;
		int32_t L_5 = ___index1;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Transform_1_t4054574962 * L_7 = (Transform_1_t4054574962 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (Transform_1_t4054574962 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_7, (Il2CppObject *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((Dictionary_2_t2730857826 *)L_3);
		((  void (*) (Dictionary_2_t2730857826 *, ObjectU5BU5D_t3614634134*, int32_t, Transform_1_t4054574962 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t2730857826 *)L_3, (ObjectU5BU5D_t3614634134*)L_4, (int32_t)L_5, (Transform_1_t4054574962 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::GetEnumerator()
extern "C"  Enumerator_t1125393968  KeyCollection_GetEnumerator_m2119247512_gshared (KeyCollection_t919388301 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2730857826 * L_0 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		Enumerator_t1125393968  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Enumerator__ctor_m1836822257(&L_1, (Dictionary_2_t2730857826 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1745786689_gshared (KeyCollection_t919388301 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2730857826 * L_0 = (Dictionary_2_t2730857826 *)__this->get_dictionary_0();
		NullCheck((Dictionary_2_t2730857826 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t2730857826 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2730857826 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1914092602_gshared (ShimEnumerator_t3729623560 * __this, Dictionary_2_t3624498739 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3624498739 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3624498739 *)L_0);
		Enumerator_t649556145  L_1 = ((  Enumerator_t649556145  (*) (Dictionary_2_t3624498739 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3624498739 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2091134001_gshared (ShimEnumerator_t3729623560 * __this, const MethodInfo* method)
{
	{
		Enumerator_t649556145 * L_0 = (Enumerator_t649556145 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2703835770((Enumerator_t649556145 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m555340501_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m555340501_gshared (ShimEnumerator_t3729623560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m555340501_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t649556145  L_0 = (Enumerator_t649556145 )__this->get_host_enumerator_0();
		Enumerator_t649556145  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m4155475114_gshared (ShimEnumerator_t3729623560 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1381843961  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t649556145 * L_0 = (Enumerator_t649556145 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1381843961  L_1 = Enumerator_get_Current_m1373918882((Enumerator_t649556145 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1381843961 )L_1;
		ChannelIdentity_t1147162267  L_2 = KeyValuePair_2_get_Key_m359652951((KeyValuePair_2_t1381843961 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ChannelIdentity_t1147162267  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1980378472_gshared (ShimEnumerator_t3729623560 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1381843961  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t649556145 * L_0 = (Enumerator_t649556145 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1381843961  L_1 = Enumerator_get_Current_m1373918882((Enumerator_t649556145 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1381843961 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m2841136671((KeyValuePair_2_t1381843961 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2412490000_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2412490000_gshared (ShimEnumerator_t3729623560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2412490000_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t3729623560 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t3729623560 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t3729623560 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m3465475748_gshared (ShimEnumerator_t3729623560 * __this, const MethodInfo* method)
{
	{
		Enumerator_t649556145 * L_0 = (Enumerator_t649556145 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m87609553((Enumerator_t649556145 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m883854621_gshared (ShimEnumerator_t1157140949 * __this, Dictionary_2_t1052016128 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1052016128 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1052016128 *)L_0);
		Enumerator_t2372040830  L_1 = ((  Enumerator_t2372040830  (*) (Dictionary_2_t1052016128 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1052016128 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2801184868_gshared (ShimEnumerator_t1157140949 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2372040830 * L_0 = (Enumerator_t2372040830 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3717365715((Enumerator_t2372040830 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m307455920_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m307455920_gshared (ShimEnumerator_t1157140949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m307455920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t2372040830  L_0 = (Enumerator_t2372040830 )__this->get_host_enumerator_0();
		Enumerator_t2372040830  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1543596449_gshared (ShimEnumerator_t1157140949 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3104328646  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2372040830 * L_0 = (Enumerator_t2372040830 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3104328646  L_1 = Enumerator_get_Current_m2135551627((Enumerator_t2372040830 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3104328646 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m266456220((KeyValuePair_2_t3104328646 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3123648713_gshared (ShimEnumerator_t1157140949 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3104328646  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2372040830 * L_0 = (Enumerator_t2372040830 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3104328646  L_1 = Enumerator_get_Current_m2135551627((Enumerator_t2372040830 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3104328646 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m1416610908((KeyValuePair_2_t3104328646 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m3118454683_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3118454683_gshared (ShimEnumerator_t1157140949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m3118454683_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1157140949 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1157140949 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1157140949 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m4237967875_gshared (ShimEnumerator_t1157140949 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2372040830 * L_0 = (Enumerator_t2372040830 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1143911664((Enumerator_t2372040830 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m4003681516_gshared (ShimEnumerator_t1184827904 * __this, Dictionary_2_t1079703083 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1079703083 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1079703083 *)L_0);
		Enumerator_t2399727785  L_1 = ((  Enumerator_t2399727785  (*) (Dictionary_2_t1079703083 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1079703083 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2566611377_gshared (ShimEnumerator_t1184827904 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3809319098((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m1209161525_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m1209161525_gshared (ShimEnumerator_t1184827904 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m1209161525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t2399727785  L_0 = (Enumerator_t2399727785 )__this->get_host_enumerator_0();
		Enumerator_t2399727785  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3497731930_gshared (ShimEnumerator_t1184827904 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3132015601  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3132015601  L_1 = Enumerator_get_Current_m3655806242((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3132015601 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m1683812983((KeyValuePair_2_t3132015601 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m184655936_gshared (ShimEnumerator_t1184827904 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3132015601  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3132015601  L_1 = Enumerator_get_Current_m3655806242((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3132015601 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Value_m1136571287((KeyValuePair_2_t3132015601 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m972214768_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m972214768_gshared (ShimEnumerator_t1184827904 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m972214768_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1184827904 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1184827904 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1184827904 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::Reset()
extern "C"  void ShimEnumerator_Reset_m2258067398_gshared (ShimEnumerator_t1184827904 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2399727785 * L_0 = (Enumerator_t2399727785 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2787013825((Enumerator_t2399727785 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3996137855_gshared (ShimEnumerator_t1802399751 * __this, Dictionary_2_t1697274930 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1697274930 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1697274930 *)L_0);
		Enumerator_t3017299632  L_1 = ((  Enumerator_t3017299632  (*) (Dictionary_2_t1697274930 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1697274930 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3313047792_gshared (ShimEnumerator_t1802399751 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2770956757((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2387156530_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2387156530_gshared (ShimEnumerator_t1802399751 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2387156530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t3017299632  L_0 = (Enumerator_t3017299632 )__this->get_host_enumerator_0();
		Enumerator_t3017299632  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2823867931_gshared (ShimEnumerator_t1802399751 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3749587448  L_1 = Enumerator_get_Current_m2230224741((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3749587448 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3551354763_gshared (ShimEnumerator_t1802399751 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3749587448  L_1 = Enumerator_get_Current_m2230224741((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3749587448 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m3690000728((KeyValuePair_2_t3749587448 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m1093801549_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1093801549_gshared (ShimEnumerator_t1802399751 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m1093801549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1802399751 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1802399751 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1802399751 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m98005789_gshared (ShimEnumerator_t1802399751 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3017299632 * L_0 = (Enumerator_t3017299632 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m761796566((Enumerator_t3017299632 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Reflection.Emit.Label>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m4135696174_gshared (ShimEnumerator_t3356153116 * __this, Dictionary_2_t3251028295 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3251028295 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3251028295 *)L_0);
		Enumerator_t276085701  L_1 = ((  Enumerator_t276085701  (*) (Dictionary_2_t3251028295 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3251028295 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Reflection.Emit.Label>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1464021775_gshared (ShimEnumerator_t3356153116 * __this, const MethodInfo* method)
{
	{
		Enumerator_t276085701 * L_0 = (Enumerator_t276085701 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3018149748((Enumerator_t276085701 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Reflection.Emit.Label>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m996208463_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m996208463_gshared (ShimEnumerator_t3356153116 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m996208463_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t276085701  L_0 = (Enumerator_t276085701 )__this->get_host_enumerator_0();
		Enumerator_t276085701  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Reflection.Emit.Label>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m4149202192_gshared (ShimEnumerator_t3356153116 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1008373517  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t276085701 * L_0 = (Enumerator_t276085701 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1008373517  L_1 = Enumerator_get_Current_m3130898880((Enumerator_t276085701 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1008373517 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Key_m2216667373((KeyValuePair_2_t1008373517 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Reflection.Emit.Label>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m4097177750_gshared (ShimEnumerator_t3356153116 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1008373517  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t276085701 * L_0 = (Enumerator_t276085701 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1008373517  L_1 = Enumerator_get_Current_m3130898880((Enumerator_t276085701 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1008373517 )L_1;
		Label_t4243202660  L_2 = KeyValuePair_2_get_Value_m590593229((KeyValuePair_2_t1008373517 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Label_t4243202660  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Reflection.Emit.Label>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m30963950_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m30963950_gshared (ShimEnumerator_t3356153116 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m30963950_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t3356153116 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t3356153116 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t3356153116 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Reflection.Emit.Label>::Reset()
extern "C"  void ShimEnumerator_Reset_m4018990736_gshared (ShimEnumerator_t3356153116 * __this, const MethodInfo* method)
{
	{
		Enumerator_t276085701 * L_0 = (Enumerator_t276085701 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2015547391((Enumerator_t276085701 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m112420235_gshared (ShimEnumerator_t3166520671 * __this, Dictionary_2_t3061395850 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3061395850 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3061395850 *)L_0);
		Enumerator_t86453256  L_1 = ((  Enumerator_t86453256  (*) (Dictionary_2_t3061395850 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3061395850 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Int32>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2324958462_gshared (ShimEnumerator_t3166520671 * __this, const MethodInfo* method)
{
	{
		Enumerator_t86453256 * L_0 = (Enumerator_t86453256 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3282539621((Enumerator_t86453256 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Int32>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m3218951198_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m3218951198_gshared (ShimEnumerator_t3166520671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m3218951198_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t86453256  L_0 = (Enumerator_t86453256 )__this->get_host_enumerator_0();
		Enumerator_t86453256  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Int32>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2284555955_gshared (ShimEnumerator_t3166520671 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t818741072  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t86453256 * L_0 = (Enumerator_t86453256 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t818741072  L_1 = Enumerator_get_Current_m2886623121((Enumerator_t86453256 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t818741072 )L_1;
		int64_t L_2 = KeyValuePair_2_get_Key_m4268935818((KeyValuePair_2_t818741072 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int64_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Int32>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m582342131_gshared (ShimEnumerator_t3166520671 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t818741072  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t86453256 * L_0 = (Enumerator_t86453256 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t818741072  L_1 = Enumerator_get_Current_m2886623121((Enumerator_t86453256 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t818741072 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Value_m3901703242((KeyValuePair_2_t818741072 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Int32>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2024727857_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2024727857_gshared (ShimEnumerator_t3166520671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2024727857_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t3166520671 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t3166520671 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t3166520671 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Int32>::Reset()
extern "C"  void ShimEnumerator_Reset_m1765049101_gshared (ShimEnumerator_t3166520671 * __this, const MethodInfo* method)
{
	{
		Enumerator_t86453256 * L_0 = (Enumerator_t86453256 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m199703458((Enumerator_t86453256 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1960799224_gshared (ShimEnumerator_t3784092518 * __this, Dictionary_2_t3678967697 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3678967697 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3678967697 *)L_0);
		Enumerator_t704025103  L_1 = ((  Enumerator_t704025103  (*) (Dictionary_2_t3678967697 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3678967697 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3904393313_gshared (ShimEnumerator_t3784092518 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3139459564((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m1063618285_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m1063618285_gshared (ShimEnumerator_t3784092518 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m1063618285_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t704025103  L_0 = (Enumerator_t704025103 )__this->get_host_enumerator_0();
		Enumerator_t704025103  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m827112712_gshared (ShimEnumerator_t3784092518 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1436312919  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1436312919  L_1 = Enumerator_get_Current_m3988231868((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1436312919 )L_1;
		int64_t L_2 = KeyValuePair_2_get_Key_m3157806383((KeyValuePair_2_t1436312919 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		int64_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1524802482_gshared (ShimEnumerator_t3784092518 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1436312919  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1436312919  L_1 = Enumerator_get_Current_m3988231868((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1436312919 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m1978486399((KeyValuePair_2_t1436312919 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2391401798_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2391401798_gshared (ShimEnumerator_t3784092518 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2391401798_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t3784092518 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t3784092518 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t3784092518 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int64,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m2823211066_gshared (ShimEnumerator_t3784092518 * __this, const MethodInfo* method)
{
	{
		Enumerator_t704025103 * L_0 = (Enumerator_t704025103 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m598725905((Enumerator_t704025103 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3590362371_gshared (ShimEnumerator_t2619027587 * __this, Dictionary_2_t2513902766 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2513902766 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2513902766 *)L_0);
		Enumerator_t3833927468  L_1 = ((  Enumerator_t3833927468  (*) (Dictionary_2_t2513902766 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2513902766 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Int32>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1814671474_gshared (ShimEnumerator_t2619027587 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3833927468 * L_0 = (Enumerator_t3833927468 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3274679113((Enumerator_t3833927468 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Int32>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2470154576_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2470154576_gshared (ShimEnumerator_t2619027587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2470154576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t3833927468  L_0 = (Enumerator_t3833927468 )__this->get_host_enumerator_0();
		Enumerator_t3833927468  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Int32>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3155610519_gshared (ShimEnumerator_t2619027587 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t271247988  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3833927468 * L_0 = (Enumerator_t3833927468 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t271247988  L_1 = Enumerator_get_Current_m1909749433((Enumerator_t3833927468 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t271247988 )L_1;
		IntPtr_t L_2 = KeyValuePair_2_get_Key_m3024592206((KeyValuePair_2_t271247988 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		IntPtr_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Int32>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m497904711_gshared (ShimEnumerator_t2619027587 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t271247988  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3833927468 * L_0 = (Enumerator_t3833927468 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t271247988  L_1 = Enumerator_get_Current_m1909749433((Enumerator_t3833927468 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t271247988 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Value_m2072891806((KeyValuePair_2_t271247988 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Int32>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m3862045777_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3862045777_gshared (ShimEnumerator_t2619027587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m3862045777_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t2619027587 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t2619027587 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t2619027587 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Int32>::Reset()
extern "C"  void ShimEnumerator_Reset_m246181529_gshared (ShimEnumerator_t2619027587 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3833927468 * L_0 = (Enumerator_t3833927468 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1791350284((Enumerator_t3833927468 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1293350960_gshared (ShimEnumerator_t3236599434 * __this, Dictionary_2_t3131474613 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3131474613 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3131474613 *)L_0);
		Enumerator_t156532019  L_1 = ((  Enumerator_t156532019  (*) (Dictionary_2_t3131474613 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3131474613 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3308190317_gshared (ShimEnumerator_t3236599434 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m266619810((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m3789806921_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m3789806921_gshared (ShimEnumerator_t3236599434 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m3789806921_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t156532019  L_0 = (Enumerator_t156532019 )__this->get_host_enumerator_0();
		Enumerator_t156532019  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1171725158_gshared (ShimEnumerator_t3236599434 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t888819835  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t888819835  L_1 = Enumerator_get_Current_m3216113338((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t888819835 )L_1;
		IntPtr_t L_2 = KeyValuePair_2_get_Key_m1574332879((KeyValuePair_2_t888819835 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		IntPtr_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m635365372_gshared (ShimEnumerator_t3236599434 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t888819835  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t888819835  L_1 = Enumerator_get_Current_m3216113338((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t888819835 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m544293807((KeyValuePair_2_t888819835 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m3610131784_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3610131784_gshared (ShimEnumerator_t3236599434 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m3610131784_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t3236599434 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t3236599434 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t3236599434 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m2611430510_gshared (ShimEnumerator_t3236599434 * __this, const MethodInfo* method)
{
	{
		Enumerator_t156532019 * L_0 = (Enumerator_t156532019 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1147555085((Enumerator_t156532019 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2428699265_gshared (ShimEnumerator_t3522759667 * __this, Dictionary_2_t3417634846 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3417634846 * L_0 = ___host0;
		NullCheck((Dictionary_2_t3417634846 *)L_0);
		Enumerator_t442692252  L_1 = ((  Enumerator_t442692252  (*) (Dictionary_2_t3417634846 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t3417634846 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2943029388_gshared (ShimEnumerator_t3522759667 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m1856697671((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2332479818_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2332479818_gshared (ShimEnumerator_t3522759667 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2332479818_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t442692252  L_0 = (Enumerator_t442692252 )__this->get_host_enumerator_0();
		Enumerator_t442692252  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m616785465_gshared (ShimEnumerator_t3522759667 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1174980068  L_1 = Enumerator_get_Current_m1020413567((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1174980068 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1396288849_gshared (ShimEnumerator_t3522759667 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1174980068  L_1 = Enumerator_get_Current_m1020413567((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1174980068 )L_1;
		bool L_2 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		bool L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2516732679_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2516732679_gshared (ShimEnumerator_t3522759667 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2516732679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t3522759667 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t3522759667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t3522759667 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::Reset()
extern "C"  void ShimEnumerator_Reset_m2247049027_gshared (ShimEnumerator_t3522759667 * __this, const MethodInfo* method)
{
	{
		Enumerator_t442692252 * L_0 = (Enumerator_t442692252 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3115320746((Enumerator_t442692252 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1807768263_gshared (ShimEnumerator_t1769062397 * __this, Dictionary_2_t1663937576 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t1663937576 * L_0 = ___host0;
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		Enumerator_t2983962278  L_1 = ((  Enumerator_t2983962278  (*) (Dictionary_2_t1663937576 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t1663937576 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2728191736_gshared (ShimEnumerator_t1769062397 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m435964161((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2171963450_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2171963450_gshared (ShimEnumerator_t1769062397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2171963450_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t2983962278  L_0 = (Enumerator_t2983962278 )__this->get_host_enumerator_0();
		Enumerator_t2983962278  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m4014537779_gshared (ShimEnumerator_t1769062397 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3716250094  L_1 = Enumerator_get_Current_m1932198897((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3716250094 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1198202883_gshared (ShimEnumerator_t1769062397 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t3716250094  L_1 = Enumerator_get_Current_m1932198897((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3716250094 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m696250329_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m696250329_gshared (ShimEnumerator_t1769062397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m696250329_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t1769062397 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1769062397 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t1769062397 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::Reset()
extern "C"  void ShimEnumerator_Reset_m208070833_gshared (ShimEnumerator_t1769062397 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2983962278 * L_0 = (Enumerator_t2983962278 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m1132695838((Enumerator_t2983962278 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3311321100_gshared (ShimEnumerator_t606262986 * __this, Dictionary_2_t501138165 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t501138165 * L_0 = ___host0;
		NullCheck((Dictionary_2_t501138165 *)L_0);
		Enumerator_t1821162867  L_1 = ((  Enumerator_t1821162867  (*) (Dictionary_2_t501138165 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t501138165 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m917329165_gshared (ShimEnumerator_t606262986 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1821162867 * L_0 = (Enumerator_t1821162867 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3870539252((Enumerator_t1821162867 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m995060505_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m995060505_gshared (ShimEnumerator_t606262986 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m995060505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t1821162867  L_0 = (Enumerator_t1821162867 )__this->get_host_enumerator_0();
		Enumerator_t1821162867  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3837048596_gshared (ShimEnumerator_t606262986 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2553450683  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t1821162867 * L_0 = (Enumerator_t1821162867 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2553450683  L_1 = Enumerator_get_Current_m4123044964((Enumerator_t1821162867 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t2553450683 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m289678647((KeyValuePair_2_t2553450683 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2801686110_gshared (ShimEnumerator_t606262986 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2553450683  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t1821162867 * L_0 = (Enumerator_t1821162867 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t2553450683  L_1 = Enumerator_get_Current_m4123044964((Enumerator_t1821162867 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t2553450683 )L_1;
		int64_t L_2 = KeyValuePair_2_get_Value_m2459084999((KeyValuePair_2_t2553450683 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int64_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m3366557086_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3366557086_gshared (ShimEnumerator_t606262986 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m3366557086_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t606262986 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t606262986 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t606262986 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::Reset()
extern "C"  void ShimEnumerator_Reset_m3945520898_gshared (ShimEnumerator_t606262986 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1821162867 * L_0 = (Enumerator_t1821162867 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m489023909((Enumerator_t1821162867 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Nullable`1<System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m904238124_gshared (ShimEnumerator_t32128712 * __this, Dictionary_2_t4221971187 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t4221971187 * L_0 = ___host0;
		NullCheck((Dictionary_2_t4221971187 *)L_0);
		Enumerator_t1247028593  L_1 = ((  Enumerator_t1247028593  (*) (Dictionary_2_t4221971187 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t4221971187 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Nullable`1<System.Int32>>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1815043987_gshared (ShimEnumerator_t32128712 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1247028593 * L_0 = (Enumerator_t1247028593 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m688389906((Enumerator_t1247028593 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Nullable`1<System.Int32>>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m1435317939_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m1435317939_gshared (ShimEnumerator_t32128712 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m1435317939_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t1247028593  L_0 = (Enumerator_t1247028593 )__this->get_host_enumerator_0();
		Enumerator_t1247028593  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Nullable`1<System.Int32>>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1526017378_gshared (ShimEnumerator_t32128712 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1979316409  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t1247028593 * L_0 = (Enumerator_t1247028593 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1979316409  L_1 = Enumerator_get_Current_m4069600302((Enumerator_t1247028593 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1979316409 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2225619841((KeyValuePair_2_t1979316409 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Nullable`1<System.Int32>>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1724190548_gshared (ShimEnumerator_t32128712 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1979316409  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t1247028593 * L_0 = (Enumerator_t1247028593 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t1979316409  L_1 = Enumerator_get_Current_m4069600302((Enumerator_t1247028593 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t1979316409 )L_1;
		Nullable_1_t334943763  L_2 = KeyValuePair_2_get_Value_m2361283873((KeyValuePair_2_t1979316409 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Nullable_1_t334943763  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Nullable`1<System.Int32>>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m3897818928_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3897818928_gshared (ShimEnumerator_t32128712 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m3897818928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t32128712 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t32128712 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t32128712 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Nullable`1<System.Int32>>::Reset()
extern "C"  void ShimEnumerator_Reset_m3158839854_gshared (ShimEnumerator_t32128712 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1247028593 * L_0 = (Enumerator_t1247028593 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2206907019((Enumerator_t1247028593 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m119758426_gshared (ShimEnumerator_t2386634244 * __this, Dictionary_2_t2281509423 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2281509423 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		Enumerator_t3601534125  L_1 = ((  Enumerator_t3601534125  (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2013866013_gshared (ShimEnumerator_t2386634244 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m3349738440((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m4233876641_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m4233876641_gshared (ShimEnumerator_t2386634244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m4233876641_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t3601534125  L_0 = (Enumerator_t3601534125 )__this->get_host_enumerator_0();
		Enumerator_t3601534125  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3962796804_gshared (ShimEnumerator_t2386634244 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t38854645  L_1 = Enumerator_get_Current_m25299632((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t38854645 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2522747790_gshared (ShimEnumerator_t2386634244 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t38854645  L_1 = Enumerator_get_Current_m25299632((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t38854645 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m2121723938_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2121723938_gshared (ShimEnumerator_t2386634244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m2121723938_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t2386634244 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t2386634244 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t2386634244 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m1100368508_gshared (ShimEnumerator_t2386634244 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3601534125 * L_0 = (Enumerator_t3601534125 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m3129803197((Enumerator_t3601534125 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m16282010_gshared (ShimEnumerator_t2835982647 * __this, Dictionary_2_t2730857826 * ___host0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Dictionary_2_t2730857826 * L_0 = ___host0;
		NullCheck((Dictionary_2_t2730857826 *)L_0);
		Enumerator_t4050882528  L_1 = ((  Enumerator_t4050882528  (*) (Dictionary_2_t2730857826 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2730857826 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_host_enumerator_0(L_1);
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1608656109_gshared (ShimEnumerator_t2835982647 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		bool L_1 = Enumerator_MoveNext_m2619008060((Enumerator_t4050882528 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Entry()
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Entry_m2332854825_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2332854825_gshared (ShimEnumerator_t2835982647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Entry_m2332854825_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_t4050882528  L_0 = (Enumerator_t4050882528 )__this->get_host_enumerator_0();
		Enumerator_t4050882528  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		DictionaryEntry_t3048875398  L_3 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m513105924_gshared (ShimEnumerator_t2835982647 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t488203048  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t488203048  L_1 = Enumerator_get_Current_m492584036((Enumerator_t4050882528 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t488203048 )L_1;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m573362703((KeyValuePair_2_t488203048 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3758954534_gshared (ShimEnumerator_t2835982647 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t488203048  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		KeyValuePair_2_t488203048  L_1 = Enumerator_get_Current_m492584036((Enumerator_t4050882528 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t488203048 )L_1;
		int32_t L_2 = KeyValuePair_2_get_Value_m1644876463((KeyValuePair_2_t488203048 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern Il2CppClass* DictionaryEntry_t3048875398_il2cpp_TypeInfo_var;
extern const uint32_t ShimEnumerator_get_Current_m3364144670_MetadataUsageId;
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3364144670_gshared (ShimEnumerator_t2835982647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShimEnumerator_get_Current_m3364144670_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((ShimEnumerator_t2835982647 *)__this);
		DictionaryEntry_t3048875398  L_0 = ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t2835982647 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((ShimEnumerator_t2835982647 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t3048875398_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Reset()
extern "C"  void ShimEnumerator_Reset_m1247878208_gshared (ShimEnumerator_t2835982647 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4050882528 * L_0 = (Enumerator_t4050882528 *)__this->get_address_of_host_enumerator_0();
		Enumerator_Reset_m2489846157((Enumerator_t4050882528 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,PubNubMessaging.Core.ChannelIdentity>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m901512951_gshared (Transform_1_t2200833417 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,PubNubMessaging.Core.ChannelIdentity>::Invoke(TKey,TValue)
extern "C"  ChannelIdentity_t1147162267  Transform_1_Invoke_m3488829539_gshared (Transform_1_t2200833417 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3488829539((Transform_1_t2200833417 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef ChannelIdentity_t1147162267  (*FunctionPointerType) (Il2CppObject *, void* __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef ChannelIdentity_t1147162267  (*FunctionPointerType) (void* __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,PubNubMessaging.Core.ChannelIdentity>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ChannelIdentity_t1147162267_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1049470850_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1049470850_gshared (Transform_1_t2200833417 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1049470850_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(ChannelIdentity_t1147162267_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,PubNubMessaging.Core.ChannelIdentity>::EndInvoke(System.IAsyncResult)
extern "C"  ChannelIdentity_t1147162267  Transform_1_EndInvoke_m1424772001_gshared (Transform_1_t2200833417 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(ChannelIdentity_t1147162267 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m115431721_gshared (Transform_1_t4102546548 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m443213181_gshared (Transform_1_t4102546548 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m443213181((Transform_1_t4102546548 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ChannelIdentity_t1147162267_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m370790320_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m370790320_gshared (Transform_1_t4102546548 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m370790320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(ChannelIdentity_t1147162267_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m2686178043_gshared (Transform_1_t4102546548 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1579721524_gshared (Transform_1_t2435515111 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1381843961  Transform_1_Invoke_m1998663892_gshared (Transform_1_t2435515111 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1998663892((Transform_1_t2435515111 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1381843961  (*FunctionPointerType) (Il2CppObject *, void* __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1381843961  (*FunctionPointerType) (void* __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ChannelIdentity_t1147162267_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m121187211_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m121187211_gshared (Transform_1_t2435515111 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m121187211_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(ChannelIdentity_t1147162267_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1381843961  Transform_1_EndInvoke_m667109630_gshared (Transform_1_t2435515111 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1381843961 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2653825051_gshared (Transform_1_t3743120445 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m641085887_gshared (Transform_1_t3743120445 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m641085887((Transform_1_t3743120445 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* ChannelIdentity_t1147162267_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1802904842_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1802904842_gshared (Transform_1_t3743120445 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1802904842_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(ChannelIdentity_t1147162267_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1346730025_gshared (Transform_1_t3743120445 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.CurrentRequestType,System.Object,PubNubMessaging.Core.CurrentRequestType>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1473731351_gshared (Transform_1_t435794015 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.CurrentRequestType,System.Object,PubNubMessaging.Core.CurrentRequestType>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m2021695811_gshared (Transform_1_t435794015 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2021695811((Transform_1_t435794015 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.CurrentRequestType,System.Object,PubNubMessaging.Core.CurrentRequestType>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* CurrentRequestType_t3250227986_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2576688802_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2576688802_gshared (Transform_1_t435794015 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2576688802_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(CurrentRequestType_t3250227986_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.CurrentRequestType,System.Object,PubNubMessaging.Core.CurrentRequestType>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m3649814337_gshared (Transform_1_t435794015 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m710910354_gshared (Transform_1_t234441427 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2041979434_gshared (Transform_1_t234441427 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2041979434((Transform_1_t234441427 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* CurrentRequestType_t3250227986_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2850360155_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2850360155_gshared (Transform_1_t234441427 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2850360155_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(CurrentRequestType_t3250227986_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m1178563560_gshared (Transform_1_t234441427 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3131616566_gshared (Transform_1_t289894675 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3104328646  Transform_1_Invoke_m762512310_gshared (Transform_1_t289894675 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m762512310((Transform_1_t289894675 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3104328646  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3104328646  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* CurrentRequestType_t3250227986_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1772032095_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1772032095_gshared (Transform_1_t289894675 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1772032095_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(CurrentRequestType_t3250227986_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3104328646  Transform_1_EndInvoke_m698497932_gshared (Transform_1_t289894675 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3104328646 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1231103122_gshared (Transform_1_t4169982620 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m1531815762_gshared (Transform_1_t4169982620 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1531815762((Transform_1_t4169982620 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* CurrentRequestType_t3250227986_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m734126353_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m734126353_gshared (Transform_1_t4169982620 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m734126353_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(CurrentRequestType_t3250227986_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.CurrentRequestType,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m3267342460_gshared (Transform_1_t4169982620 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1301543437_gshared (Transform_1_t3714796956 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m3240675425_gshared (Transform_1_t3714796956 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3240675425((Transform_1_t3714796956 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2628363682_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2628363682_gshared (Transform_1_t3714796956 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2628363682_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m1726077867_gshared (Transform_1_t3714796956 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3002075318_gshared (Transform_1_t3797937159 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3132015601  Transform_1_Invoke_m388048438_gshared (Transform_1_t3797937159 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m388048438((Transform_1_t3797937159 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3132015601  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3132015601  (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3071137759_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3071137759_gshared (Transform_1_t3797937159 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3071137759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3132015601  Transform_1_EndInvoke_m3831512460_gshared (Transform_1_t3797937159 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3132015601 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3512061196_gshared (Transform_1_t2737799006 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m488493188_gshared (Transform_1_t2737799006 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m488493188((Transform_1_t2737799006 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m929665079_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m929665079_gshared (Transform_1_t2737799006 * __this, int32_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m929665079_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m805300450_gshared (Transform_1_t2737799006 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2152205186_gshared (Transform_1_t4064153 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m4020530914_gshared (Transform_1_t4064153 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m4020530914((Transform_1_t4064153 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2179239469_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2179239469_gshared (Transform_1_t4064153 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2179239469_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m620026520_gshared (Transform_1_t4064153 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m713310742_gshared (Transform_1_t704776203 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3749587448  Transform_1_Invoke_m1436021910_gshared (Transform_1_t704776203 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1436021910((Transform_1_t704776203 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3749587448  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t3749587448  (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1786442111_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1786442111_gshared (Transform_1_t704776203 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1786442111_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3749587448  Transform_1_EndInvoke_m590952364_gshared (Transform_1_t704776203 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t3749587448 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3844763875_gshared (Transform_1_t3322033499 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m3763570295_gshared (Transform_1_t3322033499 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3763570295((Transform_1_t3322033499 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m802712344_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m802712344_gshared (Transform_1_t3322033499 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m802712344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m336301945_gshared (Transform_1_t3322033499 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2914458810_gshared (Transform_1_t3939605346 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m2347662626_gshared (Transform_1_t3939605346 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2347662626((Transform_1_t3939605346 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1919808363_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1919808363_gshared (Transform_1_t3939605346 * __this, int32_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1919808363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m1010744720_gshared (Transform_1_t3939605346 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4097589907_gshared (Transform_1_t880920208 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2130809719_gshared (Transform_1_t880920208 * __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2130809719((Transform_1_t880920208 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Label_t4243202660_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1074189372_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1074189372_gshared (Transform_1_t880920208 * __this, int32_t ___key0, Label_t4243202660  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1074189372_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Label_t4243202660_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m3923273717_gshared (Transform_1_t880920208 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m219296054_gshared (Transform_1_t3135385623 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1008373517  Transform_1_Invoke_m387254326_gshared (Transform_1_t3135385623 * __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m387254326((Transform_1_t3135385623 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1008373517  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1008373517  (*FunctionPointerType) (void* __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Label_t4243202660_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1679317215_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1679317215_gshared (Transform_1_t3135385623 * __this, int32_t ___key0, Label_t4243202660  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1679317215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Label_t4243202660_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1008373517  Transform_1_EndInvoke_m3592138636_gshared (Transform_1_t3135385623 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1008373517 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4251647142_gshared (Transform_1_t4198889554 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m1740004046_gshared (Transform_1_t4198889554 * __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1740004046((Transform_1_t4198889554 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Label_t4243202660_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3403453437_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3403453437_gshared (Transform_1_t4198889554 * __this, int32_t ___key0, Label_t4243202660  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3403453437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Label_t4243202660_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m1497660408_gshared (Transform_1_t4198889554 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Reflection.Emit.Label>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1165979048_gshared (Transform_1_t2075247470 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Reflection.Emit.Label>::Invoke(TKey,TValue)
extern "C"  Label_t4243202660  Transform_1_Invoke_m2635029648_gshared (Transform_1_t2075247470 * __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2635029648((Transform_1_t2075247470 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Label_t4243202660  (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Label_t4243202660  (*FunctionPointerType) (void* __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Reflection.Emit.Label>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Label_t4243202660_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2566047391_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2566047391_gshared (Transform_1_t2075247470 * __this, int32_t ___key0, Label_t4243202660  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2566047391_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Label_t4243202660_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,System.Reflection.Emit.Label,System.Reflection.Emit.Label>::EndInvoke(System.IAsyncResult)
extern "C"  Label_t4243202660  Transform_1_EndInvoke_m2836568466_gshared (Transform_1_t2075247470 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Label_t4243202660 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3506255812_gshared (Transform_1_t1772107233 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m1135920892_gshared (Transform_1_t1772107233 * __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1135920892((Transform_1_t1772107233 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m652403149_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m652403149_gshared (Transform_1_t1772107233 * __this, int64_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m652403149_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m2279994258_gshared (Transform_1_t1772107233 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m59718966_gshared (Transform_1_t3836940203 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t818741072  Transform_1_Invoke_m2933042102_gshared (Transform_1_t3836940203 * __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2933042102((Transform_1_t3836940203 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t818741072  (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t818741072  (*FunctionPointerType) (void* __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1482372703_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1482372703_gshared (Transform_1_t3836940203 * __this, int64_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1482372703_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t818741072  Transform_1_EndInvoke_m2262549260_gshared (Transform_1_t3836940203 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t818741072 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3207773655_gshared (Transform_1_t795109283 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m1013242963_gshared (Transform_1_t795109283 * __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1013242963((Transform_1_t795109283 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1955297614_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1955297614_gshared (Transform_1_t795109283 * __this, int64_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1955297614_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m1055649173_gshared (Transform_1_t795109283 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m271886956_gshared (Transform_1_t3927277168 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int64>::Invoke(TKey,TValue)
extern "C"  int64_t Transform_1_Invoke_m3525936996_gshared (Transform_1_t3927277168 * __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3525936996((Transform_1_t3927277168 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int64_t (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int64_t (*FunctionPointerType) (void* __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int64>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1101679575_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1101679575_gshared (Transform_1_t3927277168 * __this, int64_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1101679575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Int32,System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  int64_t Transform_1_EndInvoke_m3184135298_gshared (Transform_1_t3927277168 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3373533409_gshared (Transform_1_t2356341726 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m1299483797_gshared (Transform_1_t2356341726 * __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1299483797((Transform_1_t2356341726 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3448776694_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3448776694_gshared (Transform_1_t2356341726 * __this, int64_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3448776694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m3006940059_gshared (Transform_1_t2356341726 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m232902212_gshared (Transform_1_t743779247 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t1436312919  Transform_1_Invoke_m1183831012_gshared (Transform_1_t743779247 * __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1183831012((Transform_1_t743779247 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t1436312919  (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t1436312919  (*FunctionPointerType) (void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m655666259_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m655666259_gshared (Transform_1_t743779247 * __this, int64_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m655666259_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t1436312919  Transform_1_EndInvoke_m229614702_gshared (Transform_1_t743779247 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t1436312919 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3927316327_gshared (Transform_1_t216544365 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Int64>::Invoke(TKey,TValue)
extern "C"  int64_t Transform_1_Invoke_m3042893651_gshared (Transform_1_t216544365 * __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3042893651((Transform_1_t216544365 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int64_t (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int64_t (*FunctionPointerType) (void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Int64>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3566819466_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3566819466_gshared (Transform_1_t216544365 * __this, int64_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3566819466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  int64_t Transform_1_EndInvoke_m2544704049_gshared (Transform_1_t216544365 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4269627275_gshared (Transform_1_t1996915623 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m333092703_gshared (Transform_1_t1996915623 * __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m333092703((Transform_1_t1996915623 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m4016145208_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4016145208_gshared (Transform_1_t1996915623 * __this, int64_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m4016145208_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int64,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m3793097937_gshared (Transform_1_t1996915623 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2937663280_gshared (Transform_1_t2129470893 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m810218400_gshared (Transform_1_t2129470893 * __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m810218400((Transform_1_t2129470893 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1858167433_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1858167433_gshared (Transform_1_t2129470893 * __this, IntPtr_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1858167433_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m407226534_gshared (Transform_1_t2129470893 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3843771734_gshared (Transform_1_t3646810779 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t271247988  Transform_1_Invoke_m3442926198_gshared (Transform_1_t3646810779 * __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m3442926198((Transform_1_t3646810779 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t271247988  (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t271247988  (*FunctionPointerType) (void* __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1958180191_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1958180191_gshared (Transform_1_t3646810779 * __this, IntPtr_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1958180191_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t271247988  Transform_1_EndInvoke_m551042604_gshared (Transform_1_t3646810779 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t271247988 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1035489375_gshared (Transform_1_t1152472943 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Int32>::Invoke(TKey,TValue)
extern "C"  int32_t Transform_1_Invoke_m1529026043_gshared (Transform_1_t1152472943 * __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1529026043((Transform_1_t1152472943 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2708840102_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2708840102_gshared (Transform_1_t1152472943 * __this, IntPtr_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2708840102_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Transform_1_EndInvoke_m2656215677_gshared (Transform_1_t1152472943 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.IntPtr>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m963744174_gshared (Transform_1_t1584656104 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.IntPtr>::Invoke(TKey,TValue)
extern "C"  IntPtr_t Transform_1_Invoke_m2250487894_gshared (Transform_1_t1584656104 * __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2250487894((Transform_1_t1584656104 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef IntPtr_t (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef IntPtr_t (*FunctionPointerType) (void* __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.IntPtr>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m1506524387_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1506524387_gshared (Transform_1_t1584656104 * __this, IntPtr_t ___key0, int32_t ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m1506524387_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Int32,System.IntPtr>::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t Transform_1_EndInvoke_m3407152920_gshared (Transform_1_t1584656104 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(IntPtr_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m1237139657_gshared (Transform_1_t2713705386 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Transform_1_Invoke_m2675095773_gshared (Transform_1_t2713705386 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m2675095773((Transform_1_t2713705386 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef DictionaryEntry_t3048875398  (*FunctionPointerType) (void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m4270324442_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m4270324442_gshared (Transform_1_t2713705386 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m4270324442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C"  DictionaryEntry_t3048875398  Transform_1_EndInvoke_m4121732147_gshared (Transform_1_t2713705386 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(DictionaryEntry_t3048875398 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m4075225366_gshared (Transform_1_t553649823 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t888819835  Transform_1_Invoke_m1023901590_gshared (Transform_1_t553649823 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1023901590((Transform_1_t553649823 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t888819835  (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef KeyValuePair_2_t888819835  (*FunctionPointerType) (void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m345445247_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m345445247_gshared (Transform_1_t553649823 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m345445247_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t888819835  Transform_1_EndInvoke_m3664875948_gshared (Transform_1_t553649823 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(KeyValuePair_2_t888819835 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.IntPtr>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2284483831_gshared (Transform_1_t2168890597 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.IntPtr>::Invoke(TKey,TValue)
extern "C"  IntPtr_t Transform_1_Invoke_m524403747_gshared (Transform_1_t2168890597 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m524403747((Transform_1_t2168890597 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef IntPtr_t (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef IntPtr_t (*FunctionPointerType) (void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.IntPtr>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m2914101698_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2914101698_gshared (Transform_1_t2168890597 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m2914101698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.IntPtr>::EndInvoke(System.IAsyncResult)
extern "C"  IntPtr_t Transform_1_EndInvoke_m1944338913_gshared (Transform_1_t2168890597 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(IntPtr_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m3731381235_gshared (Transform_1_t2354279283 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m1111426439_gshared (Transform_1_t2354279283 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Transform_1_Invoke_m1111426439((Transform_1_t2354279283 *)__this->get_prev_9(),___key0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, IntPtr_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___key0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Transform_1_BeginInvoke_m3901963208_MetadataUsageId;
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m3901963208_gshared (Transform_1_t2354279283 * __this, IntPtr_t ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_1_BeginInvoke_m3901963208_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___key0);
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.IntPtr,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m275018665_gshared (Transform_1_t2354279283 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
