﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Pathfinding.Serialization.JsonFx.JsonConverter
struct JsonConverter_t4092422604;
// Pathfinding.Serialization.JsonFx.JsonWriter
struct JsonWriter_t446744171;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// Pathfinding.Serialization.JsonFx.JsonReader
struct JsonReader_t2021823321;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// Pathfinding.Serialization.JsonFx.JsonDeserializationException
struct JsonDeserializationException_t1886537714;
// System.String
struct String_t;
// Pathfinding.Serialization.JsonFx.JsonNameAttribute
struct JsonNameAttribute_t2109979655;
// Pathfinding.Serialization.JsonFx.JsonReaderSettings
struct JsonReaderSettings_t1410336530;
// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>
struct Dictionary_2_t1662909226;
// System.Collections.IEnumerable
struct IEnumerable_t2911409499;
// Pathfinding.Serialization.JsonFx.JsonSerializationException
struct JsonSerializationException_t3292412897;
// Pathfinding.Serialization.JsonFx.JsonSpecifiedPropertyAttribute
struct JsonSpecifiedPropertyAttribute_t4080780537;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// Pathfinding.Serialization.JsonFx.JsonTypeCoercionException
struct JsonTypeCoercionException_t1571553219;
// System.Exception
struct Exception_t1927440687;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// Pathfinding.Serialization.JsonFx.JsonWriterSettings
struct JsonWriterSettings_t2950311970;
// System.Enum
struct Enum_t2459695545;
// System.Uri
struct Uri_t19570940;
// System.Version
struct Version_t1755874712;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Enum[]
struct EnumU5BU5D_t975156036;
// Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime>
struct WriteDelegate_1_t415313529;
// Pathfinding.Serialization.JsonFx.TypeCoercionUtility
struct TypeCoercionUtility_t3996825458;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>>
struct Dictionary_2_t3600267123;
// System.Array
struct Il2CppArray;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "JsonFx_Json_U3CModuleU3E3783534214.h"
#include "JsonFx_Json_U3CModuleU3E3783534214MethodDeclarations.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonC4092422604.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonC4092422604MethodDeclarations.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonWr446744171.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Void1841601450.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonWr446744171MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonR2021823321.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonD1886537714.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonD1886537714MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Int322071877448.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonS3292412897MethodDeclarations.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonI1226473646.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonI1226473646MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Enum2459695545MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfo255040150.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonMe797088438.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonMe797088438MethodDeclarations.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonN2109979655.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonN2109979655MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Attribute542643598MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "mscorlib_System_Attribute542643598.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonO2568113370.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonO2568113370MethodDeclarations.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonR2021823321MethodDeclarations.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonR1410336530MethodDeclarations.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonR1410336530.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_gen1044894425MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "System_System_Collections_Generic_Stack_1_gen1044894425.h"
#include "mscorlib_System_Console2311202731MethodDeclarations.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonT2143717411.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonT1571553219.h"
#include "mscorlib_System_Double4078015681.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_TypeC3996825458MethodDeclarations.h"
#include "System_System_Diagnostics_Debug2273457373MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1662909226.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_TypeC3996825458.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Collections_ArrayList4252133567MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "mscorlib_System_Text_StringBuilder1221177846MethodDeclarations.h"
#include "mscorlib_System_Globalization_NumberFormatInfo104580544MethodDeclarations.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Char3454481338MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_Globalization_NumberFormatInfo104580544.h"
#include "mscorlib_System_Globalization_NumberStyles3408984435.h"
#include "mscorlib_System_Decimal724701077MethodDeclarations.h"
#include "mscorlib_System_Double4078015681MethodDeclarations.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_StringComparer1574862926MethodDeclarations.h"
#include "mscorlib_System_StringComparer1574862926.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3461543736MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3461543736.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonS3292412897.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonS4080780537.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonS4080780537MethodDeclarations.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonT2143717411MethodDeclarations.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonT1571553219MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonW2950311970MethodDeclarations.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonW2950311970.h"
#include "mscorlib_System_ArgumentNullException628810857MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524MethodDeclarations.h"
#include "mscorlib_System_IO_StringWriter4139609088MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524.h"
#include "mscorlib_System_IO_StringWriter4139609088.h"
#include "mscorlib_System_IO_TextWriter4027217640.h"
#include "mscorlib_System_IO_TextWriter4027217640MethodDeclarations.h"
#include "mscorlib_System_TypeCode2536926201.h"
#include "mscorlib_System_Enum2459695545.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Int164041245914.h"
#include "mscorlib_System_SByte454417549.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_UInt16986882611.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_UInt642909196914.h"
#include "mscorlib_System_Guid2533601593.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Version1755874712.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_WriteD415313529MethodDeclarations.h"
#include "mscorlib_System_DateTimeKind2186819611.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_WriteD415313529.h"
#include "mscorlib_System_DateTime693205669MethodDeclarations.h"
#include "mscorlib_System_Guid2533601593MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260MethodDeclarations.h"
#include "mscorlib_System_Byte3683104436MethodDeclarations.h"
#include "mscorlib_System_SByte454417549MethodDeclarations.h"
#include "mscorlib_System_Int164041245914MethodDeclarations.h"
#include "mscorlib_System_UInt16986882611MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021MethodDeclarations.h"
#include "mscorlib_System_Int64909078037MethodDeclarations.h"
#include "mscorlib_System_UInt642909196914MethodDeclarations.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "mscorlib_System_TimeSpan3430258949MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1663937576MethodDeclarations.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1663937576.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398MethodDeclarations.h"
#include "mscorlib_System_Reflection_AssemblyName894705941MethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfo255040150MethodDeclarations.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065.h"
#include "mscorlib_System_Reflection_Assembly4268412390.h"
#include "mscorlib_System_Reflection_Assembly4268412390MethodDeclarations.h"
#include "mscorlib_System_Reflection_AssemblyName894705941.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065MethodDeclarations.h"
#include "mscorlib_System_Reflection_ParameterInfo2249040075.h"
#include "System_System_ComponentModel_DefaultValueAttribute1302720498.h"
#include "System_System_ComponentModel_DefaultValueAttribute1302720498MethodDeclarations.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1828816677MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1828816677.h"
#include "mscorlib_System_Environment3662374671MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3600267123.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3600267123MethodDeclarations.h"
#include "mscorlib_System_Reflection_ConstructorInfo2851816542MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687MethodDeclarations.h"
#include "mscorlib_System_Reflection_ConstructorInfo2851816542.h"
#include "mscorlib_System_Reflection_TargetInvocationExcepti4098620458.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1662909226MethodDeclarations.h"
#include "mscorlib_System_Globalization_DateTimeFormatInfo2187473504MethodDeclarations.h"
#include "System_System_Uri19570940MethodDeclarations.h"
#include "mscorlib_System_Version1755874712MethodDeclarations.h"
#include "System_System_ComponentModel_TypeDescriptor3595688691MethodDeclarations.h"
#include "System_System_ComponentModel_TypeConverter745995970MethodDeclarations.h"
#include "System_System_ComponentModel_TypeConverter745995970.h"
#include "mscorlib_System_Globalization_DateTimeFormatInfo2187473504.h"
#include "mscorlib_System_Globalization_DateTimeStyles370343085.h"
#include "System_System_UriKind1128731744.h"
#include "mscorlib_System_Reflection_MethodBase904190842MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "mscorlib_System_Reflection_MethodBase904190842.h"
#include "mscorlib_System_Reflection_ParameterInfo2249040075MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Pathfinding.Serialization.JsonFx.JsonConverter::Write(Pathfinding.Serialization.JsonFx.JsonWriter,System.Type,System.Object)
extern "C"  void JsonConverter_Write_m2427477930 (JsonConverter_t4092422604 * __this, JsonWriter_t446744171 * ___writer0, Type_t * ___type1, Il2CppObject * ___value2, const MethodInfo* method)
{
	Dictionary_2_t309261261 * V_0 = NULL;
	{
		Type_t * L_0 = ___type1;
		Il2CppObject * L_1 = ___value2;
		Dictionary_2_t309261261 * L_2 = VirtFuncInvoker2< Dictionary_2_t309261261 *, Type_t *, Il2CppObject * >::Invoke(5 /* System.Collections.Generic.Dictionary`2<System.String,System.Object> Pathfinding.Serialization.JsonFx.JsonConverter::WriteJson(System.Type,System.Object) */, __this, L_0, L_1);
		V_0 = L_2;
		JsonWriter_t446744171 * L_3 = ___writer0;
		Dictionary_2_t309261261 * L_4 = V_0;
		NullCheck(L_3);
		JsonWriter_Write_m834573621(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.JsonConverter::Read(Pathfinding.Serialization.JsonFx.JsonReader,System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  Il2CppObject * JsonConverter_Read_m1009477653 (JsonConverter_t4092422604 * __this, JsonReader_t2021823321 * ___reader0, Type_t * ___type1, Dictionary_2_t309261261 * ___value2, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Type_t * L_0 = ___type1;
		Dictionary_2_t309261261 * L_1 = ___value2;
		Il2CppObject * L_2 = VirtFuncInvoker2< Il2CppObject *, Type_t *, Dictionary_2_t309261261 * >::Invoke(6 /* System.Object Pathfinding.Serialization.JsonFx.JsonConverter::ReadJson(System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Object>) */, __this, L_0, L_1);
		V_0 = L_2;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonDeserializationException::.ctor(System.String,System.Int32)
extern "C"  void JsonDeserializationException__ctor_m897186328 (JsonDeserializationException_t1886537714 * __this, String_t* ___message0, int32_t ___index1, const MethodInfo* method)
{
	{
		__this->set_index_12((-1));
		String_t* L_0 = ___message0;
		JsonSerializationException__ctor_m2868596966(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___index1;
		__this->set_index_12(L_1);
		return;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonIgnoreAttribute::IsJsonIgnore(System.Object)
extern const Il2CppType* JsonIgnoreAttribute_t1226473646_0_0_0_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern Il2CppClass* ICustomAttributeProvider_t502202687_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t JsonIgnoreAttribute_IsJsonIgnore_m3752554121_MetadataUsageId;
extern "C"  bool JsonIgnoreAttribute_IsJsonIgnore_m3752554121 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonIgnoreAttribute_IsJsonIgnore_m3752554121_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	{
		Il2CppObject * L_0 = ___value0;
		V_2 = (bool)((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_1 = V_2;
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		V_3 = (bool)0;
		goto IL_0063;
	}

IL_000e:
	{
		Il2CppObject * L_2 = ___value0;
		NullCheck(L_2);
		Type_t * L_3 = Object_GetType_m191970594(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = (Il2CppObject *)NULL;
		Type_t * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = Type_get_IsEnum_m313908919(L_4, /*hidden argument*/NULL);
		V_4 = L_5;
		bool L_6 = V_4;
		if (!L_6)
		{
			goto IL_0035;
		}
	}
	{
		Type_t * L_7 = V_0;
		Type_t * L_8 = V_0;
		Il2CppObject * L_9 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		String_t* L_10 = Enum_GetName_m1226611481(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		FieldInfo_t * L_11 = Type_GetField_m3036413258(L_7, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		goto IL_003e;
	}

IL_0035:
	{
		Il2CppObject * L_12 = ___value0;
		V_1 = ((Il2CppObject *)IsInst(L_12, ICustomAttributeProvider_t502202687_il2cpp_TypeInfo_var));
	}

IL_003e:
	{
		Il2CppObject * L_13 = V_1;
		V_5 = (bool)((((Il2CppObject*)(Il2CppObject *)L_13) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_14 = V_5;
		if (!L_14)
		{
			goto IL_004f;
		}
	}
	{
		ArgumentException_t3259014390 * L_15 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_15, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_004f:
	{
		Il2CppObject * L_16 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JsonIgnoreAttribute_t1226473646_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_16);
		bool L_18 = InterfaceFuncInvoker2< bool, Type_t *, bool >::Invoke(1 /* System.Boolean System.Reflection.ICustomAttributeProvider::IsDefined(System.Type,System.Boolean) */, ICustomAttributeProvider_t502202687_il2cpp_TypeInfo_var, L_16, L_17, (bool)1);
		V_3 = L_18;
		goto IL_0063;
	}

IL_0063:
	{
		bool L_19 = V_3;
		return L_19;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonIgnoreAttribute::IsXmlIgnore(System.Object)
extern "C"  bool JsonIgnoreAttribute_IsXmlIgnore_m1553163884 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	Type_t * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	{
		Il2CppObject * L_0 = ___value0;
		V_1 = (bool)((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_1 = V_1;
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_0019;
	}

IL_000e:
	{
		Il2CppObject * L_2 = ___value0;
		NullCheck(L_2);
		Type_t * L_3 = Object_GetType_m191970594(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_2 = (bool)0;
		goto IL_0019;
	}

IL_0019:
	{
		bool L_4 = V_2;
		return L_4;
	}
}
// System.String Pathfinding.Serialization.JsonFx.JsonNameAttribute::get_Name()
extern "C"  String_t* JsonNameAttribute_get_Name_m2966657277 (JsonNameAttribute_t2109979655 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_jsonName_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String Pathfinding.Serialization.JsonFx.JsonNameAttribute::GetJsonName(System.Object)
extern const Il2CppType* JsonNameAttribute_t2109979655_0_0_0_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MemberInfo_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonNameAttribute_t2109979655_il2cpp_TypeInfo_var;
extern const uint32_t JsonNameAttribute_GetJsonName_m3020263406_MetadataUsageId;
extern "C"  String_t* JsonNameAttribute_GetJsonName_m3020263406 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonNameAttribute_GetJsonName_m3020263406_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	MemberInfo_t * V_1 = NULL;
	JsonNameAttribute_t2109979655 * V_2 = NULL;
	bool V_3 = false;
	String_t* V_4 = NULL;
	bool V_5 = false;
	String_t* V_6 = NULL;
	bool V_7 = false;
	bool V_8 = false;
	bool V_9 = false;
	{
		Il2CppObject * L_0 = ___value0;
		V_3 = (bool)((((Il2CppObject*)(Il2CppObject *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_1 = V_3;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		V_4 = (String_t*)NULL;
		goto IL_00a9;
	}

IL_0012:
	{
		Il2CppObject * L_2 = ___value0;
		NullCheck(L_2);
		Type_t * L_3 = Object_GetType_m191970594(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = (MemberInfo_t *)NULL;
		Type_t * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = Type_get_IsEnum_m313908919(L_4, /*hidden argument*/NULL);
		V_5 = L_5;
		bool L_6 = V_5;
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		Type_t * L_7 = V_0;
		Il2CppObject * L_8 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		String_t* L_9 = Enum_GetName_m1226611481(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_6 = L_9;
		String_t* L_10 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_7 = L_11;
		bool L_12 = V_7;
		if (!L_12)
		{
			goto IL_0044;
		}
	}
	{
		V_4 = (String_t*)NULL;
		goto IL_00a9;
	}

IL_0044:
	{
		Type_t * L_13 = V_0;
		String_t* L_14 = V_6;
		NullCheck(L_13);
		FieldInfo_t * L_15 = Type_GetField_m3036413258(L_13, L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		goto IL_0059;
	}

IL_0050:
	{
		Il2CppObject * L_16 = ___value0;
		V_1 = ((MemberInfo_t *)IsInstClass(L_16, MemberInfo_t_il2cpp_TypeInfo_var));
	}

IL_0059:
	{
		MemberInfo_t * L_17 = V_1;
		V_8 = (bool)((((Il2CppObject*)(MemberInfo_t *)L_17) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_18 = V_8;
		if (!L_18)
		{
			goto IL_006a;
		}
	}
	{
		ArgumentException_t3259014390 * L_19 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2105824819(L_19, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_006a:
	{
		MemberInfo_t * L_20 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JsonNameAttribute_t2109979655_0_0_0_var), /*hidden argument*/NULL);
		bool L_22 = Attribute_IsDefined_m1350918959(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		V_9 = (bool)((((int32_t)L_22) == ((int32_t)0))? 1 : 0);
		bool L_23 = V_9;
		if (!L_23)
		{
			goto IL_0089;
		}
	}
	{
		V_4 = (String_t*)NULL;
		goto IL_00a9;
	}

IL_0089:
	{
		MemberInfo_t * L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_25 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JsonNameAttribute_t2109979655_0_0_0_var), /*hidden argument*/NULL);
		Attribute_t542643598 * L_26 = Attribute_GetCustomAttribute_m2157205805(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		V_2 = ((JsonNameAttribute_t2109979655 *)CastclassClass(L_26, JsonNameAttribute_t2109979655_il2cpp_TypeInfo_var));
		JsonNameAttribute_t2109979655 * L_27 = V_2;
		NullCheck(L_27);
		String_t* L_28 = JsonNameAttribute_get_Name_m2966657277(L_27, /*hidden argument*/NULL);
		V_4 = L_28;
		goto IL_00a9;
	}

IL_00a9:
	{
		String_t* L_29 = V_4;
		return L_29;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonReader::.ctor(System.String)
extern Il2CppClass* JsonReaderSettings_t1410336530_il2cpp_TypeInfo_var;
extern const uint32_t JsonReader__ctor_m1860805928_MetadataUsageId;
extern "C"  void JsonReader__ctor_m1860805928 (JsonReader_t2021823321 * __this, String_t* ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader__ctor_m1860805928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___input0;
		JsonReaderSettings_t1410336530 * L_1 = (JsonReaderSettings_t1410336530 *)il2cpp_codegen_object_new(JsonReaderSettings_t1410336530_il2cpp_TypeInfo_var);
		JsonReaderSettings__ctor_m3778525097(L_1, /*hidden argument*/NULL);
		JsonReader__ctor_m1313609786(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonReader::.ctor(System.String,Pathfinding.Serialization.JsonFx.JsonReaderSettings)
extern Il2CppClass* JsonReaderSettings_t1410336530_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2058570427_il2cpp_TypeInfo_var;
extern Il2CppClass* Stack_1_t1044894425_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4043684879_MethodInfo_var;
extern const MethodInfo* Stack_1__ctor_m1534803684_MethodInfo_var;
extern const uint32_t JsonReader__ctor_m1313609786_MetadataUsageId;
extern "C"  void JsonReader__ctor_m1313609786 (JsonReader_t2021823321 * __this, String_t* ___input0, JsonReaderSettings_t1410336530 * ___settings1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader__ctor_m1313609786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		JsonReaderSettings_t1410336530 * L_0 = (JsonReaderSettings_t1410336530 *)il2cpp_codegen_object_new(JsonReaderSettings_t1410336530_il2cpp_TypeInfo_var);
		JsonReaderSettings__ctor_m3778525097(L_0, /*hidden argument*/NULL);
		__this->set_Settings_0(L_0);
		__this->set_Source_1((String_t*)NULL);
		__this->set_SourceLength_2(0);
		List_1_t2058570427 * L_1 = (List_1_t2058570427 *)il2cpp_codegen_object_new(List_1_t2058570427_il2cpp_TypeInfo_var);
		List_1__ctor_m4043684879(L_1, /*hidden argument*/List_1__ctor_m4043684879_MethodInfo_var);
		__this->set_previouslyDeserialized_4(L_1);
		Stack_1_t1044894425 * L_2 = (Stack_1_t1044894425 *)il2cpp_codegen_object_new(Stack_1_t1044894425_il2cpp_TypeInfo_var);
		Stack_1__ctor_m1534803684(L_2, /*hidden argument*/Stack_1__ctor_m1534803684_MethodInfo_var);
		__this->set_jsArrays_5(L_2);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		JsonReaderSettings_t1410336530 * L_3 = ___settings1;
		__this->set_Settings_0(L_3);
		String_t* L_4 = ___input0;
		__this->set_Source_1(L_4);
		String_t* L_5 = __this->get_Source_1();
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m1606060069(L_5, /*hidden argument*/NULL);
		__this->set_SourceLength_2(L_6);
		return;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.JsonReader::Deserialize(System.Int32,System.Type)
extern "C"  Il2CppObject * JsonReader_Deserialize_m1628683004 (JsonReader_t2021823321 * __this, int32_t ___start0, Type_t * ___type1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		int32_t L_0 = ___start0;
		__this->set_index_3(L_0);
		Type_t * L_1 = ___type1;
		Il2CppObject * L_2 = JsonReader_Read_m1526966819(__this, L_1, (bool)0, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.JsonReader::Read(System.Type,System.Boolean)
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern const Il2CppType* Dictionary_2_t309261261_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonTypeCoercionException_t1571553219_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Console_t2311202731_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3814293515;
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern Il2CppCodeGenString* _stringLiteral696030553;
extern Il2CppCodeGenString* _stringLiteral487594890;
extern Il2CppCodeGenString* _stringLiteral55341327;
extern Il2CppCodeGenString* _stringLiteral3366826370;
extern const uint32_t JsonReader_Read_m1526966819_MetadataUsageId;
extern "C"  Il2CppObject * JsonReader_Read_m1526966819 (JsonReader_t2021823321 * __this, Type_t * ___expectedType0, bool ___typeIsHint1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_Read_m1526966819_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	JsonConverter_t4092422604 * V_3 = NULL;
	bool V_4 = false;
	Il2CppObject * V_5 = NULL;
	Dictionary_2_t309261261 * V_6 = NULL;
	Il2CppObject * V_7 = NULL;
	bool V_8 = false;
	Il2CppObject * V_9 = NULL;
	JsonTypeCoercionException_t1571553219 * V_10 = NULL;
	int32_t V_11 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B5_0 = 0;
	JsonReader_t2021823321 * G_B18_0 = NULL;
	JsonReader_t2021823321 * G_B17_0 = NULL;
	Type_t * G_B19_0 = NULL;
	JsonReader_t2021823321 * G_B19_1 = NULL;
	JsonReader_t2021823321 * G_B22_0 = NULL;
	JsonReader_t2021823321 * G_B21_0 = NULL;
	Type_t * G_B23_0 = NULL;
	JsonReader_t2021823321 * G_B23_1 = NULL;
	JsonReader_t2021823321 * G_B26_0 = NULL;
	JsonReader_t2021823321 * G_B25_0 = NULL;
	Type_t * G_B27_0 = NULL;
	JsonReader_t2021823321 * G_B27_1 = NULL;
	JsonReader_t2021823321 * G_B30_0 = NULL;
	JsonReader_t2021823321 * G_B29_0 = NULL;
	Type_t * G_B31_0 = NULL;
	JsonReader_t2021823321 * G_B31_1 = NULL;
	{
		Type_t * L_0 = ___expectedType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		V_1 = (bool)((((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		___expectedType0 = (Type_t *)NULL;
	}

IL_0017:
	{
		int32_t L_3 = JsonReader_Tokenize_m3135568107(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		Type_t * L_4 = ___expectedType0;
		if (!L_4)
		{
			goto IL_002c;
		}
	}
	{
		Type_t * L_5 = ___expectedType0;
		NullCheck(L_5);
		bool L_6 = Type_get_IsPrimitive_m1522841565(L_5, /*hidden argument*/NULL);
		G_B5_0 = ((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		goto IL_002d;
	}

IL_002c:
	{
		G_B5_0 = 0;
	}

IL_002d:
	{
		V_2 = (bool)G_B5_0;
		bool L_7 = V_2;
		if (!L_7)
		{
			goto IL_00b0;
		}
	}
	{
		JsonReaderSettings_t1410336530 * L_8 = __this->get_Settings_0();
		Type_t * L_9 = ___expectedType0;
		NullCheck(L_8);
		JsonConverter_t4092422604 * L_10 = VirtFuncInvoker1< JsonConverter_t4092422604 *, Type_t * >::Invoke(4 /* Pathfinding.Serialization.JsonFx.JsonConverter Pathfinding.Serialization.JsonFx.JsonReaderSettings::GetConverter(System.Type) */, L_8, L_9);
		V_3 = L_10;
		JsonConverter_t4092422604 * L_11 = V_3;
		V_4 = (bool)((!(((Il2CppObject*)(JsonConverter_t4092422604 *)L_11) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_12 = V_4;
		if (!L_12)
		{
			goto IL_00af;
		}
	}
	{
	}

IL_004a:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_13 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Dictionary_2_t309261261_0_0_0_var), /*hidden argument*/NULL);
			Il2CppObject * L_14 = JsonReader_Read_m1526966819(__this, L_13, (bool)0, /*hidden argument*/NULL);
			V_5 = L_14;
			Il2CppObject * L_15 = V_5;
			V_6 = ((Dictionary_2_t309261261 *)IsInstClass(L_15, Dictionary_2_t309261261_il2cpp_TypeInfo_var));
			Dictionary_2_t309261261 * L_16 = V_6;
			V_8 = (bool)((((Il2CppObject*)(Dictionary_2_t309261261 *)L_16) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
			bool L_17 = V_8;
			if (!L_17)
			{
				goto IL_007a;
			}
		}

IL_0072:
		{
			V_9 = NULL;
			goto IL_0252;
		}

IL_007a:
		{
			JsonConverter_t4092422604 * L_18 = V_3;
			Type_t * L_19 = ___expectedType0;
			Dictionary_2_t309261261 * L_20 = V_6;
			NullCheck(L_18);
			Il2CppObject * L_21 = JsonConverter_Read_m1009477653(L_18, __this, L_19, L_20, /*hidden argument*/NULL);
			V_7 = L_21;
			Il2CppObject * L_22 = V_7;
			V_9 = L_22;
			goto IL_0252;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (JsonTypeCoercionException_t1571553219_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_008f;
		throw e;
	}

CATCH_008f:
	{ // begin catch(Pathfinding.Serialization.JsonFx.JsonTypeCoercionException)
		V_10 = ((JsonTypeCoercionException_t1571553219 *)__exception_local);
		JsonTypeCoercionException_t1571553219 * L_23 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral3814293515, L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t2311202731_il2cpp_TypeInfo_var);
		Console_WriteLine_m3271989373(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		goto IL_00a7;
	} // end catch (depth: 1)

IL_00a7:
	{
		V_9 = NULL;
		goto IL_0252;
	}

IL_00af:
	{
	}

IL_00b0:
	{
		int32_t L_25 = V_0;
		V_11 = L_25;
		int32_t L_26 = V_11;
		if (L_26 == 0)
		{
			goto IL_024c;
		}
		if (L_26 == 1)
		{
			goto IL_022f;
		}
		if (L_26 == 2)
		{
			goto IL_0191;
		}
		if (L_26 == 3)
		{
			goto IL_0147;
		}
		if (L_26 == 4)
		{
			goto IL_016c;
		}
		if (L_26 == 5)
		{
			goto IL_01b1;
		}
		if (L_26 == 6)
		{
			goto IL_01db;
		}
		if (L_26 == 7)
		{
			goto IL_0205;
		}
		if (L_26 == 8)
		{
			goto IL_0132;
		}
		if (L_26 == 9)
		{
			goto IL_011d;
		}
		if (L_26 == 10)
		{
			goto IL_0108;
		}
		if (L_26 == 11)
		{
			goto IL_024c;
		}
		if (L_26 == 12)
		{
			goto IL_00f3;
		}
	}
	{
		goto IL_024c;
	}

IL_00f3:
	{
		bool L_27 = ___typeIsHint1;
		G_B17_0 = __this;
		if (L_27)
		{
			G_B18_0 = __this;
			goto IL_00fb;
		}
	}
	{
		Type_t * L_28 = ___expectedType0;
		G_B19_0 = L_28;
		G_B19_1 = G_B17_0;
		goto IL_00fc;
	}

IL_00fb:
	{
		G_B19_0 = ((Type_t *)(NULL));
		G_B19_1 = G_B18_0;
	}

IL_00fc:
	{
		NullCheck(G_B19_1);
		Il2CppObject * L_29 = JsonReader_ReadObject_m1070998081(G_B19_1, G_B19_0, /*hidden argument*/NULL);
		V_9 = L_29;
		goto IL_0252;
	}

IL_0108:
	{
		bool L_30 = ___typeIsHint1;
		G_B21_0 = __this;
		if (L_30)
		{
			G_B22_0 = __this;
			goto IL_0110;
		}
	}
	{
		Type_t * L_31 = ___expectedType0;
		G_B23_0 = L_31;
		G_B23_1 = G_B21_0;
		goto IL_0111;
	}

IL_0110:
	{
		G_B23_0 = ((Type_t *)(NULL));
		G_B23_1 = G_B22_0;
	}

IL_0111:
	{
		NullCheck(G_B23_1);
		Il2CppObject * L_32 = JsonReader_ReadArray_m2359650156(G_B23_1, G_B23_0, /*hidden argument*/NULL);
		V_9 = L_32;
		goto IL_0252;
	}

IL_011d:
	{
		bool L_33 = ___typeIsHint1;
		G_B25_0 = __this;
		if (L_33)
		{
			G_B26_0 = __this;
			goto IL_0125;
		}
	}
	{
		Type_t * L_34 = ___expectedType0;
		G_B27_0 = L_34;
		G_B27_1 = G_B25_0;
		goto IL_0126;
	}

IL_0125:
	{
		G_B27_0 = ((Type_t *)(NULL));
		G_B27_1 = G_B26_0;
	}

IL_0126:
	{
		NullCheck(G_B27_1);
		Il2CppObject * L_35 = JsonReader_ReadString_m2145598429(G_B27_1, G_B27_0, /*hidden argument*/NULL);
		V_9 = L_35;
		goto IL_0252;
	}

IL_0132:
	{
		bool L_36 = ___typeIsHint1;
		G_B29_0 = __this;
		if (L_36)
		{
			G_B30_0 = __this;
			goto IL_013a;
		}
	}
	{
		Type_t * L_37 = ___expectedType0;
		G_B31_0 = L_37;
		G_B31_1 = G_B29_0;
		goto IL_013b;
	}

IL_013a:
	{
		G_B31_0 = ((Type_t *)(NULL));
		G_B31_1 = G_B30_0;
	}

IL_013b:
	{
		NullCheck(G_B31_1);
		Il2CppObject * L_38 = JsonReader_ReadNumber_m218548847(G_B31_1, G_B31_0, /*hidden argument*/NULL);
		V_9 = L_38;
		goto IL_0252;
	}

IL_0147:
	{
		int32_t L_39 = __this->get_index_3();
		NullCheck(_stringLiteral2609877245);
		int32_t L_40 = String_get_Length_m1606060069(_stringLiteral2609877245, /*hidden argument*/NULL);
		__this->set_index_3(((int32_t)((int32_t)L_39+(int32_t)L_40)));
		bool L_41 = ((bool)0);
		Il2CppObject * L_42 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_41);
		V_9 = L_42;
		goto IL_0252;
	}

IL_016c:
	{
		int32_t L_43 = __this->get_index_3();
		NullCheck(_stringLiteral3323263070);
		int32_t L_44 = String_get_Length_m1606060069(_stringLiteral3323263070, /*hidden argument*/NULL);
		__this->set_index_3(((int32_t)((int32_t)L_43+(int32_t)L_44)));
		bool L_45 = ((bool)1);
		Il2CppObject * L_46 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_45);
		V_9 = L_46;
		goto IL_0252;
	}

IL_0191:
	{
		int32_t L_47 = __this->get_index_3();
		NullCheck(_stringLiteral1743624307);
		int32_t L_48 = String_get_Length_m1606060069(_stringLiteral1743624307, /*hidden argument*/NULL);
		__this->set_index_3(((int32_t)((int32_t)L_47+(int32_t)L_48)));
		V_9 = NULL;
		goto IL_0252;
	}

IL_01b1:
	{
		int32_t L_49 = __this->get_index_3();
		NullCheck(_stringLiteral696030553);
		int32_t L_50 = String_get_Length_m1606060069(_stringLiteral696030553, /*hidden argument*/NULL);
		__this->set_index_3(((int32_t)((int32_t)L_49+(int32_t)L_50)));
		double L_51 = (std::numeric_limits<double>::quiet_NaN());
		Il2CppObject * L_52 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_51);
		V_9 = L_52;
		goto IL_0252;
	}

IL_01db:
	{
		int32_t L_53 = __this->get_index_3();
		NullCheck(_stringLiteral487594890);
		int32_t L_54 = String_get_Length_m1606060069(_stringLiteral487594890, /*hidden argument*/NULL);
		__this->set_index_3(((int32_t)((int32_t)L_53+(int32_t)L_54)));
		double L_55 = (std::numeric_limits<double>::infinity());
		Il2CppObject * L_56 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_55);
		V_9 = L_56;
		goto IL_0252;
	}

IL_0205:
	{
		int32_t L_57 = __this->get_index_3();
		NullCheck(_stringLiteral55341327);
		int32_t L_58 = String_get_Length_m1606060069(_stringLiteral55341327, /*hidden argument*/NULL);
		__this->set_index_3(((int32_t)((int32_t)L_57+(int32_t)L_58)));
		double L_59 = (-std::numeric_limits<double>::infinity());
		Il2CppObject * L_60 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_59);
		V_9 = L_60;
		goto IL_0252;
	}

IL_022f:
	{
		int32_t L_61 = __this->get_index_3();
		NullCheck(_stringLiteral3366826370);
		int32_t L_62 = String_get_Length_m1606060069(_stringLiteral3366826370, /*hidden argument*/NULL);
		__this->set_index_3(((int32_t)((int32_t)L_61+(int32_t)L_62)));
		V_9 = NULL;
		goto IL_0252;
	}

IL_024c:
	{
		V_9 = NULL;
		goto IL_0252;
	}

IL_0252:
	{
		Il2CppObject * L_63 = V_9;
		return L_63;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.JsonReader::ReadObject(System.Type)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m1065152471_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1330780531_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2199215334_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m2873640625_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2197690559;
extern Il2CppCodeGenString* _stringLiteral1479106273;
extern const uint32_t JsonReader_ReadObject_m1070998081_MetadataUsageId;
extern "C"  Il2CppObject * JsonReader_ReadObject_m1070998081 (JsonReader_t2021823321 * __this, Type_t * ___objectType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_ReadObject_m1070998081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Dictionary_2_t1662909226 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	bool V_4 = false;
	bool V_5 = false;
	bool V_6 = false;
	Il2CppObject * V_7 = NULL;
	{
		V_0 = (Type_t *)NULL;
		V_1 = (Dictionary_2_t1662909226 *)NULL;
		Type_t * L_0 = ___objectType0;
		V_4 = (bool)((!(((Il2CppObject*)(Type_t *)L_0) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_1 = V_4;
		if (!L_1)
		{
			goto IL_0069;
		}
	}
	{
		JsonReaderSettings_t1410336530 * L_2 = __this->get_Settings_0();
		NullCheck(L_2);
		TypeCoercionUtility_t3996825458 * L_3 = L_2->get_Coercion_0();
		Type_t * L_4 = ___objectType0;
		NullCheck(L_3);
		Il2CppObject * L_5 = TypeCoercionUtility_InstantiateObject_m123346059(L_3, L_4, (&V_1), /*hidden argument*/NULL);
		V_2 = L_5;
		Il2CppObject * L_6 = V_2;
		List_1_t2058570427 * L_7 = __this->get_previouslyDeserialized_4();
		NullCheck(L_7);
		int32_t L_8 = List_1_get_Count_m1065152471(L_7, /*hidden argument*/List_1_get_Count_m1065152471_MethodInfo_var);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral2197690559, L_6, L_10, /*hidden argument*/NULL);
		Debug_WriteLine_m544460363(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		List_1_t2058570427 * L_12 = __this->get_previouslyDeserialized_4();
		Il2CppObject * L_13 = V_2;
		NullCheck(L_12);
		List_1_Add_m1330780531(L_12, L_13, /*hidden argument*/List_1_Add_m1330780531_MethodInfo_var);
		Dictionary_2_t1662909226 * L_14 = V_1;
		V_5 = (bool)((((Il2CppObject*)(Dictionary_2_t1662909226 *)L_14) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_15 = V_5;
		if (!L_15)
		{
			goto IL_0066;
		}
	}
	{
		Type_t * L_16 = ___objectType0;
		Type_t * L_17 = JsonReader_GetGenericDictionaryType_m42840060(__this, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
	}

IL_0066:
	{
		goto IL_0071;
	}

IL_0069:
	{
		Dictionary_2_t309261261 * L_18 = (Dictionary_2_t309261261 *)il2cpp_codegen_object_new(Dictionary_2_t309261261_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2199215334(L_18, /*hidden argument*/Dictionary_2__ctor_m2199215334_MethodInfo_var);
		V_2 = L_18;
	}

IL_0071:
	{
		Il2CppObject * L_19 = V_2;
		V_3 = L_19;
		Type_t * L_20 = ___objectType0;
		Dictionary_2_t1662909226 * L_21 = V_1;
		Type_t * L_22 = V_0;
		JsonReader_PopulateObject_m152476262(__this, (&V_2), L_20, L_21, L_22, /*hidden argument*/NULL);
		Il2CppObject * L_23 = V_3;
		Il2CppObject * L_24 = V_2;
		V_6 = (bool)((((int32_t)((((Il2CppObject*)(Il2CppObject *)L_23) == ((Il2CppObject*)(Il2CppObject *)L_24))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_25 = V_6;
		if (!L_25)
		{
			goto IL_00c8;
		}
	}
	{
		Il2CppObject * L_26 = V_2;
		List_1_t2058570427 * L_27 = __this->get_previouslyDeserialized_4();
		NullCheck(L_27);
		int32_t L_28 = List_1_get_Count_m1065152471(L_27, /*hidden argument*/List_1_get_Count_m1065152471_MethodInfo_var);
		int32_t L_29 = L_28;
		Il2CppObject * L_30 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_29);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral1479106273, L_26, L_30, /*hidden argument*/NULL);
		Debug_WriteLine_m544460363(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		List_1_t2058570427 * L_32 = __this->get_previouslyDeserialized_4();
		List_1_t2058570427 * L_33 = __this->get_previouslyDeserialized_4();
		NullCheck(L_33);
		int32_t L_34 = List_1_get_Count_m1065152471(L_33, /*hidden argument*/List_1_get_Count_m1065152471_MethodInfo_var);
		NullCheck(L_32);
		List_1_RemoveAt_m2873640625(L_32, ((int32_t)((int32_t)L_34-(int32_t)1)), /*hidden argument*/List_1_RemoveAt_m2873640625_MethodInfo_var);
	}

IL_00c8:
	{
		Il2CppObject * L_35 = V_2;
		V_7 = L_35;
		goto IL_00cd;
	}

IL_00cd:
	{
		Il2CppObject * L_36 = V_7;
		return L_36;
	}
}
// System.Type Pathfinding.Serialization.JsonFx.JsonReader::GetGenericDictionaryType(System.Type)
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1708270712;
extern Il2CppCodeGenString* _stringLiteral656848023;
extern const uint32_t JsonReader_GetGenericDictionaryType_m42840060_MetadataUsageId;
extern "C"  Type_t * JsonReader_GetGenericDictionaryType_m42840060 (JsonReader_t2021823321 * __this, Type_t * ___objectType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_GetGenericDictionaryType_m42840060_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	bool V_1 = false;
	TypeU5BU5D_t1664964607* V_2 = NULL;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	Type_t * V_6 = NULL;
	{
		Type_t * L_0 = ___objectType0;
		NullCheck(L_0);
		Type_t * L_1 = Type_GetInterface_m1193646790(L_0, _stringLiteral1708270712, /*hidden argument*/NULL);
		V_0 = L_1;
		Type_t * L_2 = V_0;
		V_1 = (bool)((!(((Il2CppObject*)(Type_t *)L_2) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_007a;
		}
	}
	{
		Type_t * L_4 = V_0;
		NullCheck(L_4);
		TypeU5BU5D_t1664964607* L_5 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type[] System.Type::GetGenericArguments() */, L_4);
		V_2 = L_5;
		TypeU5BU5D_t1664964607* L_6 = V_2;
		NullCheck(L_6);
		V_3 = (bool)((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) == ((int32_t)2))? 1 : 0);
		bool L_7 = V_3;
		if (!L_7)
		{
			goto IL_0079;
		}
	}
	{
		TypeU5BU5D_t1664964607* L_8 = V_2;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Type_t * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		V_4 = (bool)((((int32_t)((((Il2CppObject*)(Type_t *)L_10) == ((Il2CppObject*)(Type_t *)L_11))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_12 = V_4;
		if (!L_12)
		{
			goto IL_0058;
		}
	}
	{
		Type_t * L_13 = ___objectType0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral656848023, L_13, /*hidden argument*/NULL);
		int32_t L_15 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_16 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_16, L_14, L_15, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_0058:
	{
		TypeU5BU5D_t1664964607* L_17 = V_2;
		NullCheck(L_17);
		int32_t L_18 = 1;
		Type_t * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		V_5 = (bool)((((int32_t)((((Il2CppObject*)(Type_t *)L_19) == ((Il2CppObject*)(Type_t *)L_20))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_21 = V_5;
		if (!L_21)
		{
			goto IL_0078;
		}
	}
	{
		TypeU5BU5D_t1664964607* L_22 = V_2;
		NullCheck(L_22);
		int32_t L_23 = 1;
		Type_t * L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		V_6 = L_24;
		goto IL_007f;
	}

IL_0078:
	{
	}

IL_0079:
	{
	}

IL_007a:
	{
		V_6 = (Type_t *)NULL;
		goto IL_007f;
	}

IL_007f:
	{
		Type_t * L_25 = V_6;
		return L_25;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonReader::PopulateObject(System.Object&,System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>,System.Type)
extern const Il2CppType* Int32_t2071877448_0_0_0_var;
extern Il2CppClass* JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m2432474082_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3378956889;
extern Il2CppCodeGenString* _stringLiteral1708270712;
extern Il2CppCodeGenString* _stringLiteral613757631;
extern Il2CppCodeGenString* _stringLiteral3839397925;
extern Il2CppCodeGenString* _stringLiteral2136900813;
extern Il2CppCodeGenString* _stringLiteral884116380;
extern Il2CppCodeGenString* _stringLiteral1757179989;
extern const uint32_t JsonReader_PopulateObject_m152476262_MetadataUsageId;
extern "C"  void JsonReader_PopulateObject_m152476262 (JsonReader_t2021823321 * __this, Il2CppObject ** ___result0, Type_t * ___objectType1, Dictionary_2_t1662909226 * ___memberMap2, Type_t * ___genericDictionaryType3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_PopulateObject_m152476262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	bool V_3 = false;
	Type_t * V_4 = NULL;
	MemberInfo_t * V_5 = NULL;
	String_t* V_6 = NULL;
	Il2CppObject * V_7 = NULL;
	bool V_8 = false;
	bool V_9 = false;
	bool V_10 = false;
	bool V_11 = false;
	bool V_12 = false;
	bool V_13 = false;
	bool V_14 = false;
	int32_t V_15 = 0;
	bool V_16 = false;
	bool V_17 = false;
	bool V_18 = false;
	bool V_19 = false;
	int32_t G_B5_0 = 0;
	int32_t G_B14_0 = 0;
	String_t* G_B19_0 = NULL;
	int32_t G_B22_0 = 0;
	int32_t G_B32_0 = 0;
	int32_t G_B38_0 = 0;
	{
		String_t* L_0 = __this->get_Source_1();
		int32_t L_1 = __this->get_index_3();
		NullCheck(L_0);
		Il2CppChar L_2 = String_get_Chars_m4230566705(L_0, L_1, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)((((int32_t)L_2) == ((int32_t)((int32_t)123)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_3 = V_2;
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		int32_t L_4 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_5 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_5, _stringLiteral3378956889, L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002f:
	{
		Il2CppObject ** L_6 = ___result0;
		V_0 = ((Il2CppObject *)IsInst((*((Il2CppObject **)L_6)), IDictionary_t596158605_il2cpp_TypeInfo_var));
		Il2CppObject * L_7 = V_0;
		if (L_7)
		{
			goto IL_004a;
		}
	}
	{
		Type_t * L_8 = ___objectType1;
		NullCheck(L_8);
		Type_t * L_9 = Type_GetInterface_m1193646790(L_8, _stringLiteral1708270712, /*hidden argument*/NULL);
		G_B5_0 = ((!(((Il2CppObject*)(Type_t *)L_9) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		goto IL_004b;
	}

IL_004a:
	{
		G_B5_0 = 0;
	}

IL_004b:
	{
		V_3 = (bool)G_B5_0;
		bool L_10 = V_3;
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		Type_t * L_11 = ___objectType1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral613757631, L_11, /*hidden argument*/NULL);
		int32_t L_13 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_14 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_14, L_12, L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_0067:
	{
		int32_t L_15 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_15+(int32_t)1)));
		int32_t L_16 = __this->get_index_3();
		int32_t L_17 = __this->get_SourceLength_2();
		V_8 = (bool)((((int32_t)((((int32_t)L_16) < ((int32_t)L_17))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_18 = V_8;
		if (!L_18)
		{
			goto IL_009f;
		}
	}
	{
		int32_t L_19 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_20 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_20, _stringLiteral3839397925, L_19, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20);
	}

IL_009f:
	{
		JsonReaderSettings_t1410336530 * L_21 = __this->get_Settings_0();
		NullCheck(L_21);
		bool L_22 = JsonReaderSettings_get_AllowUnquotedObjectKeys_m1632960917(L_21, /*hidden argument*/NULL);
		int32_t L_23 = JsonReader_Tokenize_m3812013816(__this, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		int32_t L_24 = V_1;
		V_9 = (bool)((((int32_t)L_24) == ((int32_t)((int32_t)13)))? 1 : 0);
		bool L_25 = V_9;
		if (!L_25)
		{
			goto IL_00c2;
		}
	}
	{
		goto IL_0277;
	}

IL_00c2:
	{
		int32_t L_26 = V_1;
		if ((((int32_t)L_26) == ((int32_t)((int32_t)9))))
		{
			goto IL_00d1;
		}
	}
	{
		int32_t L_27 = V_1;
		G_B14_0 = ((((int32_t)((((int32_t)L_27) == ((int32_t)((int32_t)16)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_00d2;
	}

IL_00d1:
	{
		G_B14_0 = 0;
	}

IL_00d2:
	{
		V_10 = (bool)G_B14_0;
		bool L_28 = V_10;
		if (!L_28)
		{
			goto IL_00ea;
		}
	}
	{
		int32_t L_29 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_30 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_30, _stringLiteral2136900813, L_29, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_30);
	}

IL_00ea:
	{
		int32_t L_31 = V_1;
		if ((((int32_t)L_31) == ((int32_t)((int32_t)9))))
		{
			goto IL_00f7;
		}
	}
	{
		String_t* L_32 = JsonReader_ReadUnquotedKey_m2689293933(__this, /*hidden argument*/NULL);
		G_B19_0 = L_32;
		goto IL_0103;
	}

IL_00f7:
	{
		Il2CppObject * L_33 = JsonReader_ReadString_m2145598429(__this, (Type_t *)NULL, /*hidden argument*/NULL);
		G_B19_0 = ((String_t*)CastclassSealed(L_33, String_t_il2cpp_TypeInfo_var));
	}

IL_0103:
	{
		V_6 = G_B19_0;
		Type_t * L_34 = ___genericDictionaryType3;
		if (L_34)
		{
			goto IL_010f;
		}
	}
	{
		Dictionary_2_t1662909226 * L_35 = ___memberMap2;
		G_B22_0 = ((!(((Il2CppObject*)(Dictionary_2_t1662909226 *)L_35) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		goto IL_0110;
	}

IL_010f:
	{
		G_B22_0 = 0;
	}

IL_0110:
	{
		V_11 = (bool)G_B22_0;
		bool L_36 = V_11;
		if (!L_36)
		{
			goto IL_0126;
		}
	}
	{
		Dictionary_2_t1662909226 * L_37 = ___memberMap2;
		String_t* L_38 = V_6;
		Type_t * L_39 = TypeCoercionUtility_GetMemberInfo_m658257860(NULL /*static, unused*/, L_37, L_38, (&V_5), /*hidden argument*/NULL);
		V_4 = L_39;
		goto IL_012f;
	}

IL_0126:
	{
		Type_t * L_40 = ___genericDictionaryType3;
		V_4 = L_40;
		V_5 = (MemberInfo_t *)NULL;
	}

IL_012f:
	{
		int32_t L_41 = JsonReader_Tokenize_m3135568107(__this, /*hidden argument*/NULL);
		V_1 = L_41;
		int32_t L_42 = V_1;
		V_12 = (bool)((((int32_t)((((int32_t)L_42) == ((int32_t)((int32_t)14)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_43 = V_12;
		if (!L_43)
		{
			goto IL_0156;
		}
	}
	{
		int32_t L_44 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_45 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_45, _stringLiteral884116380, L_44, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_45);
	}

IL_0156:
	{
		int32_t L_46 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_46+(int32_t)1)));
		int32_t L_47 = __this->get_index_3();
		int32_t L_48 = __this->get_SourceLength_2();
		V_13 = (bool)((((int32_t)((((int32_t)L_47) < ((int32_t)L_48))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_49 = V_13;
		if (!L_49)
		{
			goto IL_018d;
		}
	}
	{
		int32_t L_50 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_51 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_51, _stringLiteral3839397925, L_50, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_51);
	}

IL_018d:
	{
		JsonReaderSettings_t1410336530 * L_52 = __this->get_Settings_0();
		NullCheck(L_52);
		bool L_53 = JsonReaderSettings_get_HandleCyclicReferences_m3051340765(L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_01a8;
		}
	}
	{
		String_t* L_54 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_55 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_54, _stringLiteral1757179989, /*hidden argument*/NULL);
		G_B32_0 = ((int32_t)(L_55));
		goto IL_01a9;
	}

IL_01a8:
	{
		G_B32_0 = 0;
	}

IL_01a9:
	{
		V_14 = (bool)G_B32_0;
		bool L_56 = V_14;
		if (!L_56)
		{
			goto IL_01e3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_57 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Int32_t2071877448_0_0_0_var), /*hidden argument*/NULL);
		Il2CppObject * L_58 = JsonReader_Read_m1526966819(__this, L_57, (bool)0, /*hidden argument*/NULL);
		V_15 = ((*(int32_t*)((int32_t*)UnBox (L_58, Int32_t2071877448_il2cpp_TypeInfo_var))));
		Il2CppObject ** L_59 = ___result0;
		List_1_t2058570427 * L_60 = __this->get_previouslyDeserialized_4();
		int32_t L_61 = V_15;
		NullCheck(L_60);
		Il2CppObject * L_62 = List_1_get_Item_m2432474082(L_60, L_61, /*hidden argument*/List_1_get_Item_m2432474082_MethodInfo_var);
		*((Il2CppObject **)(L_59)) = (Il2CppObject *)L_62;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_59), (Il2CppObject *)L_62);
		int32_t L_63 = JsonReader_Tokenize_m3135568107(__this, /*hidden argument*/NULL);
		V_1 = L_63;
		goto IL_0269;
	}

IL_01e3:
	{
		Type_t * L_64 = V_4;
		Il2CppObject * L_65 = JsonReader_Read_m1526966819(__this, L_64, (bool)0, /*hidden argument*/NULL);
		V_7 = L_65;
		Il2CppObject * L_66 = V_0;
		V_16 = (bool)((!(((Il2CppObject*)(Il2CppObject *)L_66) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_67 = V_16;
		if (!L_67)
		{
			goto IL_0246;
		}
	}
	{
		Type_t * L_68 = ___objectType1;
		if (L_68)
		{
			goto IL_020d;
		}
	}
	{
		JsonReaderSettings_t1410336530 * L_69 = __this->get_Settings_0();
		String_t* L_70 = V_6;
		NullCheck(L_69);
		bool L_71 = JsonReaderSettings_IsTypeHintName_m3613446497(L_69, L_70, /*hidden argument*/NULL);
		G_B38_0 = ((int32_t)(L_71));
		goto IL_020e;
	}

IL_020d:
	{
		G_B38_0 = 0;
	}

IL_020e:
	{
		V_17 = (bool)G_B38_0;
		bool L_72 = V_17;
		if (!L_72)
		{
			goto IL_0236;
		}
	}
	{
		Il2CppObject ** L_73 = ___result0;
		JsonReaderSettings_t1410336530 * L_74 = __this->get_Settings_0();
		NullCheck(L_74);
		TypeCoercionUtility_t3996825458 * L_75 = L_74->get_Coercion_0();
		Il2CppObject * L_76 = V_0;
		Il2CppObject * L_77 = V_7;
		NullCheck(L_75);
		Il2CppObject * L_78 = TypeCoercionUtility_ProcessTypeHint_m3882009641(L_75, L_76, ((String_t*)IsInstSealed(L_77, String_t_il2cpp_TypeInfo_var)), (&___objectType1), (&___memberMap2), /*hidden argument*/NULL);
		*((Il2CppObject **)(L_73)) = (Il2CppObject *)L_78;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_73), (Il2CppObject *)L_78);
		goto IL_0243;
	}

IL_0236:
	{
		Il2CppObject * L_79 = V_0;
		String_t* L_80 = V_6;
		Il2CppObject * L_81 = V_7;
		NullCheck(L_79);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(1 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_79, L_80, L_81);
	}

IL_0243:
	{
		goto IL_0261;
	}

IL_0246:
	{
		JsonReaderSettings_t1410336530 * L_82 = __this->get_Settings_0();
		NullCheck(L_82);
		TypeCoercionUtility_t3996825458 * L_83 = L_82->get_Coercion_0();
		Il2CppObject ** L_84 = ___result0;
		Type_t * L_85 = V_4;
		MemberInfo_t * L_86 = V_5;
		Il2CppObject * L_87 = V_7;
		NullCheck(L_83);
		TypeCoercionUtility_SetMemberValue_m1207607791(L_83, (*((Il2CppObject **)L_84)), L_85, L_86, L_87, /*hidden argument*/NULL);
	}

IL_0261:
	{
		int32_t L_88 = JsonReader_Tokenize_m3135568107(__this, /*hidden argument*/NULL);
		V_1 = L_88;
	}

IL_0269:
	{
		int32_t L_89 = V_1;
		V_18 = (bool)((((int32_t)L_89) == ((int32_t)((int32_t)15)))? 1 : 0);
		bool L_90 = V_18;
		if (L_90)
		{
			goto IL_0067;
		}
	}

IL_0277:
	{
		int32_t L_91 = V_1;
		V_19 = (bool)((((int32_t)((((int32_t)L_91) == ((int32_t)((int32_t)13)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_92 = V_19;
		if (!L_92)
		{
			goto IL_0297;
		}
	}
	{
		int32_t L_93 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_94 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_94, _stringLiteral3839397925, L_93, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_94);
	}

IL_0297:
	{
		int32_t L_95 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_95+(int32_t)1)));
		return;
	}
}
// System.Collections.IEnumerable Pathfinding.Serialization.JsonFx.JsonReader::ReadArray(System.Type)
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern Il2CppClass* JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1_get_Count_m1394871142_MethodInfo_var;
extern const MethodInfo* Stack_1_Pop_m3495564675_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m3588050777_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3645816657;
extern Il2CppCodeGenString* _stringLiteral87298469;
extern const uint32_t JsonReader_ReadArray_m2359650156_MetadataUsageId;
extern "C"  Il2CppObject * JsonReader_ReadArray_m2359650156 (JsonReader_t2021823321 * __this, Type_t * ___arrayType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_ReadArray_m2359650156_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	Type_t * V_2 = NULL;
	ArrayList_t4252133567 * V_3 = NULL;
	int32_t V_4 = 0;
	bool V_5 = false;
	bool V_6 = false;
	bool V_7 = false;
	bool V_8 = false;
	TypeU5BU5D_t1664964607* V_9 = NULL;
	bool V_10 = false;
	Il2CppObject * V_11 = NULL;
	bool V_12 = false;
	bool V_13 = false;
	bool V_14 = false;
	bool V_15 = false;
	bool V_16 = false;
	bool V_17 = false;
	bool V_18 = false;
	bool V_19 = false;
	bool V_20 = false;
	bool V_21 = false;
	Il2CppObject * V_22 = NULL;
	ArrayList_t4252133567 * G_B13_0 = NULL;
	int32_t G_B22_0 = 0;
	int32_t G_B28_0 = 0;
	int32_t G_B41_0 = 0;
	{
		String_t* L_0 = __this->get_Source_1();
		int32_t L_1 = __this->get_index_3();
		NullCheck(L_0);
		Il2CppChar L_2 = String_get_Chars_m4230566705(L_0, L_1, /*hidden argument*/NULL);
		V_5 = (bool)((((int32_t)((((int32_t)L_2) == ((int32_t)((int32_t)91)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_3 = V_5;
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		int32_t L_4 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_5 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_5, _stringLiteral3645816657, L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0031:
	{
		Type_t * L_6 = ___arrayType0;
		V_0 = (bool)((!(((Il2CppObject*)(Type_t *)L_6) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_7 = V_0;
		V_1 = (bool)((((int32_t)L_7) == ((int32_t)0))? 1 : 0);
		V_2 = (Type_t *)NULL;
		bool L_8 = V_0;
		V_6 = L_8;
		bool L_9 = V_6;
		if (!L_9)
		{
			goto IL_0087;
		}
	}
	{
		Type_t * L_10 = ___arrayType0;
		NullCheck(L_10);
		bool L_11 = Type_get_HasElementType_m3319917896(L_10, /*hidden argument*/NULL);
		V_7 = L_11;
		bool L_12 = V_7;
		if (!L_12)
		{
			goto IL_005c;
		}
	}
	{
		Type_t * L_13 = ___arrayType0;
		NullCheck(L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(46 /* System.Type System.Type::GetElementType() */, L_13);
		V_2 = L_14;
		goto IL_0086;
	}

IL_005c:
	{
		Type_t * L_15 = ___arrayType0;
		NullCheck(L_15);
		bool L_16 = VirtFuncInvoker0< bool >::Invoke(90 /* System.Boolean System.Type::get_IsGenericType() */, L_15);
		V_8 = L_16;
		bool L_17 = V_8;
		if (!L_17)
		{
			goto IL_0086;
		}
	}
	{
		Type_t * L_18 = ___arrayType0;
		NullCheck(L_18);
		TypeU5BU5D_t1664964607* L_19 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type[] System.Type::GetGenericArguments() */, L_18);
		V_9 = L_19;
		TypeU5BU5D_t1664964607* L_20 = V_9;
		NullCheck(L_20);
		V_10 = (bool)((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length))))) == ((int32_t)1))? 1 : 0);
		bool L_21 = V_10;
		if (!L_21)
		{
			goto IL_0085;
		}
	}
	{
		TypeU5BU5D_t1664964607* L_22 = V_9;
		NullCheck(L_22);
		int32_t L_23 = 0;
		Type_t * L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		V_2 = L_24;
	}

IL_0085:
	{
	}

IL_0086:
	{
	}

IL_0087:
	{
		Stack_1_t1044894425 * L_25 = __this->get_jsArrays_5();
		NullCheck(L_25);
		int32_t L_26 = Stack_1_get_Count_m1394871142(L_25, /*hidden argument*/Stack_1_get_Count_m1394871142_MethodInfo_var);
		if ((((int32_t)L_26) > ((int32_t)0)))
		{
			goto IL_009c;
		}
	}
	{
		ArrayList_t4252133567 * L_27 = (ArrayList_t4252133567 *)il2cpp_codegen_object_new(ArrayList_t4252133567_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4012174379(L_27, /*hidden argument*/NULL);
		G_B13_0 = L_27;
		goto IL_00a7;
	}

IL_009c:
	{
		Stack_1_t1044894425 * L_28 = __this->get_jsArrays_5();
		NullCheck(L_28);
		ArrayList_t4252133567 * L_29 = Stack_1_Pop_m3495564675(L_28, /*hidden argument*/Stack_1_Pop_m3495564675_MethodInfo_var);
		G_B13_0 = L_29;
	}

IL_00a7:
	{
		V_3 = G_B13_0;
		ArrayList_t4252133567 * L_30 = V_3;
		NullCheck(L_30);
		VirtActionInvoker0::Invoke(31 /* System.Void System.Collections.ArrayList::Clear() */, L_30);
	}

IL_00af:
	{
		int32_t L_31 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_31+(int32_t)1)));
		int32_t L_32 = __this->get_index_3();
		int32_t L_33 = __this->get_SourceLength_2();
		V_12 = (bool)((((int32_t)((((int32_t)L_32) < ((int32_t)L_33))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_34 = V_12;
		if (!L_34)
		{
			goto IL_00e7;
		}
	}
	{
		int32_t L_35 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_36 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_36, _stringLiteral87298469, L_35, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_36);
	}

IL_00e7:
	{
		int32_t L_37 = JsonReader_Tokenize_m3135568107(__this, /*hidden argument*/NULL);
		V_4 = L_37;
		int32_t L_38 = V_4;
		V_13 = (bool)((((int32_t)L_38) == ((int32_t)((int32_t)11)))? 1 : 0);
		bool L_39 = V_13;
		if (!L_39)
		{
			goto IL_0101;
		}
	}
	{
		goto IL_01ae;
	}

IL_0101:
	{
		Type_t * L_40 = V_2;
		bool L_41 = V_1;
		Il2CppObject * L_42 = JsonReader_Read_m1526966819(__this, L_40, L_41, /*hidden argument*/NULL);
		V_11 = L_42;
		ArrayList_t4252133567 * L_43 = V_3;
		Il2CppObject * L_44 = V_11;
		NullCheck(L_43);
		VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_43, L_44);
		Il2CppObject * L_45 = V_11;
		V_14 = (bool)((((Il2CppObject*)(Il2CppObject *)L_45) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_46 = V_14;
		if (!L_46)
		{
			goto IL_013b;
		}
	}
	{
		Type_t * L_47 = V_2;
		if (!L_47)
		{
			goto IL_012b;
		}
	}
	{
		Type_t * L_48 = V_2;
		NullCheck(L_48);
		bool L_49 = Type_get_IsValueType_m1733572463(L_48, /*hidden argument*/NULL);
		G_B22_0 = ((int32_t)(L_49));
		goto IL_012c;
	}

IL_012b:
	{
		G_B22_0 = 0;
	}

IL_012c:
	{
		V_15 = (bool)G_B22_0;
		bool L_50 = V_15;
		if (!L_50)
		{
			goto IL_0136;
		}
	}
	{
		V_2 = (Type_t *)NULL;
	}

IL_0136:
	{
		V_0 = (bool)1;
		goto IL_0196;
	}

IL_013b:
	{
		Type_t * L_51 = V_2;
		if (!L_51)
		{
			goto IL_0150;
		}
	}
	{
		Type_t * L_52 = V_2;
		Il2CppObject * L_53 = V_11;
		NullCheck(L_53);
		Type_t * L_54 = Object_GetType_m191970594(L_53, /*hidden argument*/NULL);
		NullCheck(L_52);
		bool L_55 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_52, L_54);
		G_B28_0 = ((((int32_t)L_55) == ((int32_t)0))? 1 : 0);
		goto IL_0151;
	}

IL_0150:
	{
		G_B28_0 = 0;
	}

IL_0151:
	{
		V_16 = (bool)G_B28_0;
		bool L_56 = V_16;
		if (!L_56)
		{
			goto IL_0180;
		}
	}
	{
		Il2CppObject * L_57 = V_11;
		NullCheck(L_57);
		Type_t * L_58 = Object_GetType_m191970594(L_57, /*hidden argument*/NULL);
		Type_t * L_59 = V_2;
		NullCheck(L_58);
		bool L_60 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_58, L_59);
		V_17 = L_60;
		bool L_61 = V_17;
		if (!L_61)
		{
			goto IL_0177;
		}
	}
	{
		Il2CppObject * L_62 = V_11;
		NullCheck(L_62);
		Type_t * L_63 = Object_GetType_m191970594(L_62, /*hidden argument*/NULL);
		V_2 = L_63;
		goto IL_017d;
	}

IL_0177:
	{
		V_2 = (Type_t *)NULL;
		V_0 = (bool)1;
	}

IL_017d:
	{
		goto IL_0196;
	}

IL_0180:
	{
		bool L_64 = V_0;
		V_18 = (bool)((((int32_t)L_64) == ((int32_t)0))? 1 : 0);
		bool L_65 = V_18;
		if (!L_65)
		{
			goto IL_0196;
		}
	}
	{
		Il2CppObject * L_66 = V_11;
		NullCheck(L_66);
		Type_t * L_67 = Object_GetType_m191970594(L_66, /*hidden argument*/NULL);
		V_2 = L_67;
		V_0 = (bool)1;
	}

IL_0196:
	{
		int32_t L_68 = JsonReader_Tokenize_m3135568107(__this, /*hidden argument*/NULL);
		V_4 = L_68;
		int32_t L_69 = V_4;
		V_19 = (bool)((((int32_t)L_69) == ((int32_t)((int32_t)15)))? 1 : 0);
		bool L_70 = V_19;
		if (L_70)
		{
			goto IL_00af;
		}
	}

IL_01ae:
	{
		int32_t L_71 = V_4;
		V_20 = (bool)((((int32_t)((((int32_t)L_71) == ((int32_t)((int32_t)11)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_72 = V_20;
		if (!L_72)
		{
			goto IL_01cf;
		}
	}
	{
		int32_t L_73 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_74 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_74, _stringLiteral87298469, L_73, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_74);
	}

IL_01cf:
	{
		int32_t L_75 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_75+(int32_t)1)));
		Stack_1_t1044894425 * L_76 = __this->get_jsArrays_5();
		ArrayList_t4252133567 * L_77 = V_3;
		NullCheck(L_76);
		Stack_1_Push_m3588050777(L_76, L_77, /*hidden argument*/Stack_1_Push_m3588050777_MethodInfo_var);
		Type_t * L_78 = V_2;
		if (!L_78)
		{
			goto IL_01ff;
		}
	}
	{
		Type_t * L_79 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_80 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		G_B41_0 = ((((int32_t)((((Il2CppObject*)(Type_t *)L_79) == ((Il2CppObject*)(Type_t *)L_80))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0200;
	}

IL_01ff:
	{
		G_B41_0 = 0;
	}

IL_0200:
	{
		V_21 = (bool)G_B41_0;
		bool L_81 = V_21;
		if (!L_81)
		{
			goto IL_0212;
		}
	}
	{
		ArrayList_t4252133567 * L_82 = V_3;
		Type_t * L_83 = V_2;
		NullCheck(L_82);
		Il2CppArray * L_84 = VirtFuncInvoker1< Il2CppArray *, Type_t * >::Invoke(48 /* System.Array System.Collections.ArrayList::ToArray(System.Type) */, L_82, L_83);
		V_22 = L_84;
		goto IL_021c;
	}

IL_0212:
	{
		ArrayList_t4252133567 * L_85 = V_3;
		NullCheck(L_85);
		ObjectU5BU5D_t3614634134* L_86 = VirtFuncInvoker0< ObjectU5BU5D_t3614634134* >::Invoke(47 /* System.Object[] System.Collections.ArrayList::ToArray() */, L_85);
		V_22 = (Il2CppObject *)L_86;
		goto IL_021c;
	}

IL_021c:
	{
		Il2CppObject * L_87 = V_22;
		return L_87;
	}
}
// System.String Pathfinding.Serialization.JsonFx.JsonReader::ReadUnquotedKey()
extern "C"  String_t* JsonReader_ReadUnquotedKey_m2689293933 (JsonReader_t2021823321 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	String_t* V_2 = NULL;
	{
		int32_t L_0 = __this->get_index_3();
		V_0 = L_0;
	}

IL_0008:
	{
		int32_t L_1 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_1+(int32_t)1)));
		int32_t L_2 = JsonReader_Tokenize_m3812013816(__this, (bool)1, /*hidden argument*/NULL);
		V_1 = (bool)((((int32_t)L_2) == ((int32_t)((int32_t)16)))? 1 : 0);
		bool L_3 = V_1;
		if (L_3)
		{
			goto IL_0008;
		}
	}
	{
		String_t* L_4 = __this->get_Source_1();
		int32_t L_5 = V_0;
		int32_t L_6 = __this->get_index_3();
		int32_t L_7 = V_0;
		NullCheck(L_4);
		String_t* L_8 = String_Substring_m12482732(L_4, L_5, ((int32_t)((int32_t)L_6-(int32_t)L_7)), /*hidden argument*/NULL);
		V_2 = L_8;
		goto IL_003e;
	}

IL_003e:
	{
		String_t* L_9 = V_2;
		return L_9;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.JsonReader::ReadString(System.Type)
extern const Il2CppType* String_t_0_0_0_var;
extern Il2CppClass* JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* NumberFormatInfo_t104580544_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4158660541;
extern Il2CppCodeGenString* _stringLiteral1317996913;
extern const uint32_t JsonReader_ReadString_m2145598429_MetadataUsageId;
extern "C"  Il2CppObject * JsonReader_ReadString_m2145598429 (JsonReader_t2021823321 * __this, Type_t * ___expectedType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_ReadString_m2145598429_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	int32_t V_1 = 0;
	StringBuilder_t1221177846 * V_2 = NULL;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	bool V_6 = false;
	Il2CppChar V_7 = 0x0;
	int32_t V_8 = 0;
	bool V_9 = false;
	bool V_10 = false;
	bool V_11 = false;
	bool V_12 = false;
	bool V_13 = false;
	Il2CppObject * V_14 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B31_0 = 0;
	int32_t G_B47_0 = 0;
	{
		String_t* L_0 = __this->get_Source_1();
		int32_t L_1 = __this->get_index_3();
		NullCheck(L_0);
		Il2CppChar L_2 = String_get_Chars_m4230566705(L_0, L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)((int32_t)34))))
		{
			goto IL_0030;
		}
	}
	{
		String_t* L_3 = __this->get_Source_1();
		int32_t L_4 = __this->get_index_3();
		NullCheck(L_3);
		Il2CppChar L_5 = String_get_Chars_m4230566705(L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)((((int32_t)L_5) == ((int32_t)((int32_t)39)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0031;
	}

IL_0030:
	{
		G_B3_0 = 0;
	}

IL_0031:
	{
		V_3 = (bool)G_B3_0;
		bool L_6 = V_3;
		if (!L_6)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_7 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_8 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_8, _stringLiteral4158660541, L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0047:
	{
		String_t* L_9 = __this->get_Source_1();
		int32_t L_10 = __this->get_index_3();
		NullCheck(L_9);
		Il2CppChar L_11 = String_get_Chars_m4230566705(L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		int32_t L_12 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = __this->get_index_3();
		int32_t L_14 = __this->get_SourceLength_2();
		V_4 = (bool)((((int32_t)((((int32_t)L_13) < ((int32_t)L_14))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_15 = V_4;
		if (!L_15)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_16 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_17 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_17, _stringLiteral1317996913, L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_0090:
	{
		int32_t L_18 = __this->get_index_3();
		V_1 = L_18;
		StringBuilder_t1221177846 * L_19 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_19, /*hidden argument*/NULL);
		V_2 = L_19;
		goto IL_02cb;
	}

IL_00a2:
	{
		String_t* L_20 = __this->get_Source_1();
		int32_t L_21 = __this->get_index_3();
		NullCheck(L_20);
		Il2CppChar L_22 = String_get_Chars_m4230566705(L_20, L_21, /*hidden argument*/NULL);
		V_5 = (bool)((((int32_t)L_22) == ((int32_t)((int32_t)92)))? 1 : 0);
		bool L_23 = V_5;
		if (!L_23)
		{
			goto IL_0291;
		}
	}
	{
		StringBuilder_t1221177846 * L_24 = V_2;
		String_t* L_25 = __this->get_Source_1();
		int32_t L_26 = V_1;
		int32_t L_27 = __this->get_index_3();
		int32_t L_28 = V_1;
		NullCheck(L_24);
		StringBuilder_Append_m1462406979(L_24, L_25, L_26, ((int32_t)((int32_t)L_27-(int32_t)L_28)), /*hidden argument*/NULL);
		int32_t L_29 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_29+(int32_t)1)));
		int32_t L_30 = __this->get_index_3();
		int32_t L_31 = __this->get_SourceLength_2();
		V_6 = (bool)((((int32_t)((((int32_t)L_30) < ((int32_t)L_31))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_32 = V_6;
		if (!L_32)
		{
			goto IL_010f;
		}
	}
	{
		int32_t L_33 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_34 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_34, _stringLiteral1317996913, L_33, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_34);
	}

IL_010f:
	{
		String_t* L_35 = __this->get_Source_1();
		int32_t L_36 = __this->get_index_3();
		NullCheck(L_35);
		Il2CppChar L_37 = String_get_Chars_m4230566705(L_35, L_36, /*hidden argument*/NULL);
		V_7 = L_37;
		Il2CppChar L_38 = V_7;
		if ((!(((uint32_t)L_38) <= ((uint32_t)((int32_t)98)))))
		{
			goto IL_013b;
		}
	}
	{
		Il2CppChar L_39 = V_7;
		if ((((int32_t)L_39) == ((int32_t)((int32_t)48))))
		{
			goto IL_016a;
		}
	}
	{
		goto IL_0130;
	}

IL_0130:
	{
		Il2CppChar L_40 = V_7;
		if ((((int32_t)L_40) == ((int32_t)((int32_t)98))))
		{
			goto IL_0170;
		}
	}
	{
		goto IL_0235;
	}

IL_013b:
	{
		Il2CppChar L_41 = V_7;
		if ((((int32_t)L_41) == ((int32_t)((int32_t)102))))
		{
			goto IL_017e;
		}
	}
	{
		goto IL_0143;
	}

IL_0143:
	{
		Il2CppChar L_42 = V_7;
		if ((((int32_t)L_42) == ((int32_t)((int32_t)110))))
		{
			goto IL_018d;
		}
	}
	{
		goto IL_014b;
	}

IL_014b:
	{
		Il2CppChar L_43 = V_7;
		if (((int32_t)((int32_t)L_43-(int32_t)((int32_t)114))) == 0)
		{
			goto IL_019c;
		}
		if (((int32_t)((int32_t)L_43-(int32_t)((int32_t)114))) == 1)
		{
			goto IL_0235;
		}
		if (((int32_t)((int32_t)L_43-(int32_t)((int32_t)114))) == 2)
		{
			goto IL_01ab;
		}
		if (((int32_t)((int32_t)L_43-(int32_t)((int32_t)114))) == 3)
		{
			goto IL_01ba;
		}
	}
	{
		goto IL_0235;
	}

IL_016a:
	{
		goto IL_0250;
	}

IL_0170:
	{
		StringBuilder_t1221177846 * L_44 = V_2;
		NullCheck(L_44);
		StringBuilder_Append_m3618697540(L_44, 8, /*hidden argument*/NULL);
		goto IL_0250;
	}

IL_017e:
	{
		StringBuilder_t1221177846 * L_45 = V_2;
		NullCheck(L_45);
		StringBuilder_Append_m3618697540(L_45, ((int32_t)12), /*hidden argument*/NULL);
		goto IL_0250;
	}

IL_018d:
	{
		StringBuilder_t1221177846 * L_46 = V_2;
		NullCheck(L_46);
		StringBuilder_Append_m3618697540(L_46, ((int32_t)10), /*hidden argument*/NULL);
		goto IL_0250;
	}

IL_019c:
	{
		StringBuilder_t1221177846 * L_47 = V_2;
		NullCheck(L_47);
		StringBuilder_Append_m3618697540(L_47, ((int32_t)13), /*hidden argument*/NULL);
		goto IL_0250;
	}

IL_01ab:
	{
		StringBuilder_t1221177846 * L_48 = V_2;
		NullCheck(L_48);
		StringBuilder_Append_m3618697540(L_48, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_0250;
	}

IL_01ba:
	{
		int32_t L_49 = __this->get_index_3();
		int32_t L_50 = __this->get_SourceLength_2();
		if ((((int32_t)((int32_t)((int32_t)L_49+(int32_t)4))) >= ((int32_t)L_50)))
		{
			goto IL_01f2;
		}
	}
	{
		String_t* L_51 = __this->get_Source_1();
		int32_t L_52 = __this->get_index_3();
		NullCheck(L_51);
		String_t* L_53 = String_Substring_m12482732(L_51, ((int32_t)((int32_t)L_52+(int32_t)1)), 4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t104580544_il2cpp_TypeInfo_var);
		NumberFormatInfo_t104580544 * L_54 = NumberFormatInfo_get_InvariantInfo_m2658215204(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_55 = Int32_TryParse_m2941352213(NULL /*static, unused*/, L_53, ((int32_t)512), L_54, (&V_8), /*hidden argument*/NULL);
		G_B31_0 = ((int32_t)(L_55));
		goto IL_01f3;
	}

IL_01f2:
	{
		G_B31_0 = 0;
	}

IL_01f3:
	{
		V_9 = (bool)G_B31_0;
		bool L_56 = V_9;
		if (!L_56)
		{
			goto IL_0219;
		}
	}
	{
		StringBuilder_t1221177846 * L_57 = V_2;
		int32_t L_58 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		String_t* L_59 = Char_ConvertFromUtf32_m4053081558(NULL /*static, unused*/, L_58, /*hidden argument*/NULL);
		NullCheck(L_57);
		StringBuilder_Append_m3636508479(L_57, L_59, /*hidden argument*/NULL);
		int32_t L_60 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_60+(int32_t)4)));
		goto IL_0233;
	}

IL_0219:
	{
		StringBuilder_t1221177846 * L_61 = V_2;
		String_t* L_62 = __this->get_Source_1();
		int32_t L_63 = __this->get_index_3();
		NullCheck(L_62);
		Il2CppChar L_64 = String_get_Chars_m4230566705(L_62, L_63, /*hidden argument*/NULL);
		NullCheck(L_61);
		StringBuilder_Append_m3618697540(L_61, L_64, /*hidden argument*/NULL);
	}

IL_0233:
	{
		goto IL_0250;
	}

IL_0235:
	{
		StringBuilder_t1221177846 * L_65 = V_2;
		String_t* L_66 = __this->get_Source_1();
		int32_t L_67 = __this->get_index_3();
		NullCheck(L_66);
		Il2CppChar L_68 = String_get_Chars_m4230566705(L_66, L_67, /*hidden argument*/NULL);
		NullCheck(L_65);
		StringBuilder_Append_m3618697540(L_65, L_68, /*hidden argument*/NULL);
		goto IL_0250;
	}

IL_0250:
	{
		int32_t L_69 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_69+(int32_t)1)));
		int32_t L_70 = __this->get_index_3();
		int32_t L_71 = __this->get_SourceLength_2();
		V_10 = (bool)((((int32_t)((((int32_t)L_70) < ((int32_t)L_71))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_72 = V_10;
		if (!L_72)
		{
			goto IL_0287;
		}
	}
	{
		int32_t L_73 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_74 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_74, _stringLiteral1317996913, L_73, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_74);
	}

IL_0287:
	{
		int32_t L_75 = __this->get_index_3();
		V_1 = L_75;
		goto IL_02ca;
	}

IL_0291:
	{
		int32_t L_76 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_76+(int32_t)1)));
		int32_t L_77 = __this->get_index_3();
		int32_t L_78 = __this->get_SourceLength_2();
		V_11 = (bool)((((int32_t)((((int32_t)L_77) < ((int32_t)L_78))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_79 = V_11;
		if (!L_79)
		{
			goto IL_02c9;
		}
	}
	{
		int32_t L_80 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_81 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_81, _stringLiteral1317996913, L_80, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_81);
	}

IL_02c9:
	{
	}

IL_02ca:
	{
	}

IL_02cb:
	{
		String_t* L_82 = __this->get_Source_1();
		int32_t L_83 = __this->get_index_3();
		NullCheck(L_82);
		Il2CppChar L_84 = String_get_Chars_m4230566705(L_82, L_83, /*hidden argument*/NULL);
		Il2CppChar L_85 = V_0;
		V_12 = (bool)((((int32_t)((((int32_t)L_84) == ((int32_t)L_85))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_86 = V_12;
		if (L_86)
		{
			goto IL_00a2;
		}
	}
	{
		StringBuilder_t1221177846 * L_87 = V_2;
		String_t* L_88 = __this->get_Source_1();
		int32_t L_89 = V_1;
		int32_t L_90 = __this->get_index_3();
		int32_t L_91 = V_1;
		NullCheck(L_87);
		StringBuilder_Append_m1462406979(L_87, L_88, L_89, ((int32_t)((int32_t)L_90-(int32_t)L_91)), /*hidden argument*/NULL);
		int32_t L_92 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_92+(int32_t)1)));
		Type_t * L_93 = ___expectedType0;
		if (!L_93)
		{
			goto IL_0324;
		}
	}
	{
		Type_t * L_94 = ___expectedType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_95 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		G_B47_0 = ((((int32_t)((((Il2CppObject*)(Type_t *)L_94) == ((Il2CppObject*)(Type_t *)L_95))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0325;
	}

IL_0324:
	{
		G_B47_0 = 0;
	}

IL_0325:
	{
		V_13 = (bool)G_B47_0;
		bool L_96 = V_13;
		if (!L_96)
		{
			goto IL_0347;
		}
	}
	{
		JsonReaderSettings_t1410336530 * L_97 = __this->get_Settings_0();
		NullCheck(L_97);
		TypeCoercionUtility_t3996825458 * L_98 = L_97->get_Coercion_0();
		Type_t * L_99 = ___expectedType0;
		StringBuilder_t1221177846 * L_100 = V_2;
		NullCheck(L_100);
		String_t* L_101 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_100);
		NullCheck(L_98);
		Il2CppObject * L_102 = TypeCoercionUtility_CoerceType_m3744809450(L_98, L_99, L_101, /*hidden argument*/NULL);
		V_14 = L_102;
		goto IL_0351;
	}

IL_0347:
	{
		StringBuilder_t1221177846 * L_103 = V_2;
		NullCheck(L_103);
		String_t* L_104 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_103);
		V_14 = L_104;
		goto IL_0351;
	}

IL_0351:
	{
		Il2CppObject * L_105 = V_14;
		return L_105;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.JsonReader::ReadNumber(System.Type)
extern const Il2CppType* Decimal_t724701077_0_0_0_var;
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var;
extern Il2CppClass* NumberFormatInfo_t104580544_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3425045059;
extern const uint32_t JsonReader_ReadNumber_m218548847_MetadataUsageId;
extern "C"  Il2CppObject * JsonReader_ReadNumber_m218548847 (JsonReader_t2021823321 * __this, Type_t * ___expectedType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_ReadNumber_m218548847_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	String_t* V_5 = NULL;
	bool V_6 = false;
	bool V_7 = false;
	bool V_8 = false;
	bool V_9 = false;
	bool V_10 = false;
	bool V_11 = false;
	bool V_12 = false;
	int32_t V_13 = 0;
	bool V_14 = false;
	bool V_15 = false;
	bool V_16 = false;
	bool V_17 = false;
	bool V_18 = false;
	bool V_19 = false;
	Decimal_t724701077  V_20;
	memset(&V_20, 0, sizeof(V_20));
	bool V_21 = false;
	Il2CppObject * V_22 = NULL;
	bool V_23 = false;
	bool V_24 = false;
	double V_25 = 0.0;
	bool V_26 = false;
	bool V_27 = false;
	int32_t G_B4_0 = 0;
	int32_t G_B12_0 = 0;
	int32_t G_B16_0 = 0;
	int32_t G_B20_0 = 0;
	int32_t G_B27_0 = 0;
	int32_t G_B31_0 = 0;
	int32_t G_B30_0 = 0;
	int32_t G_B32_0 = 0;
	int32_t G_B32_1 = 0;
	int32_t G_B36_0 = 0;
	int32_t G_B38_0 = 0;
	int32_t G_B44_0 = 0;
	int32_t G_B48_0 = 0;
	int32_t G_B59_0 = 0;
	int32_t G_B65_0 = 0;
	int32_t G_B71_0 = 0;
	int32_t G_B76_0 = 0;
	{
		V_0 = (bool)0;
		V_1 = (bool)0;
		int32_t L_0 = __this->get_index_3();
		V_2 = L_0;
		V_3 = 0;
		V_4 = 0;
		String_t* L_1 = __this->get_Source_1();
		int32_t L_2 = __this->get_index_3();
		NullCheck(L_1);
		Il2CppChar L_3 = String_get_Chars_m4230566705(L_1, L_2, /*hidden argument*/NULL);
		V_6 = (bool)((((int32_t)L_3) == ((int32_t)((int32_t)45)))? 1 : 0);
		bool L_4 = V_6;
		if (!L_4)
		{
			goto IL_007d;
		}
	}
	{
		int32_t L_5 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = __this->get_index_3();
		int32_t L_7 = __this->get_SourceLength_2();
		if ((((int32_t)L_6) >= ((int32_t)L_7)))
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_8 = __this->get_Source_1();
		int32_t L_9 = __this->get_index_3();
		NullCheck(L_8);
		Il2CppChar L_10 = String_get_Chars_m4230566705(L_8, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		bool L_11 = Char_IsDigit_m751559221(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)L_11) == ((int32_t)0))? 1 : 0);
		goto IL_0065;
	}

IL_0064:
	{
		G_B4_0 = 1;
	}

IL_0065:
	{
		V_7 = (bool)G_B4_0;
		bool L_12 = V_7;
		if (!L_12)
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_13 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_14 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_14, _stringLiteral3425045059, L_13, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_007c:
	{
	}

IL_007d:
	{
		goto IL_008f;
	}

IL_007f:
	{
		int32_t L_15 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_15+(int32_t)1)));
	}

IL_008f:
	{
		int32_t L_16 = __this->get_index_3();
		int32_t L_17 = __this->get_SourceLength_2();
		if ((((int32_t)L_16) >= ((int32_t)L_17)))
		{
			goto IL_00b5;
		}
	}
	{
		String_t* L_18 = __this->get_Source_1();
		int32_t L_19 = __this->get_index_3();
		NullCheck(L_18);
		Il2CppChar L_20 = String_get_Chars_m4230566705(L_18, L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		bool L_21 = Char_IsDigit_m751559221(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		G_B12_0 = ((int32_t)(L_21));
		goto IL_00b6;
	}

IL_00b5:
	{
		G_B12_0 = 0;
	}

IL_00b6:
	{
		V_8 = (bool)G_B12_0;
		bool L_22 = V_8;
		if (L_22)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_23 = __this->get_index_3();
		int32_t L_24 = __this->get_SourceLength_2();
		if ((((int32_t)L_23) >= ((int32_t)L_24)))
		{
			goto IL_00e1;
		}
	}
	{
		String_t* L_25 = __this->get_Source_1();
		int32_t L_26 = __this->get_index_3();
		NullCheck(L_25);
		Il2CppChar L_27 = String_get_Chars_m4230566705(L_25, L_26, /*hidden argument*/NULL);
		G_B16_0 = ((((int32_t)L_27) == ((int32_t)((int32_t)46)))? 1 : 0);
		goto IL_00e2;
	}

IL_00e1:
	{
		G_B16_0 = 0;
	}

IL_00e2:
	{
		V_9 = (bool)G_B16_0;
		bool L_28 = V_9;
		if (!L_28)
		{
			goto IL_017e;
		}
	}
	{
		V_0 = (bool)1;
		int32_t L_29 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_29+(int32_t)1)));
		int32_t L_30 = __this->get_index_3();
		int32_t L_31 = __this->get_SourceLength_2();
		if ((((int32_t)L_30) >= ((int32_t)L_31)))
		{
			goto IL_0125;
		}
	}
	{
		String_t* L_32 = __this->get_Source_1();
		int32_t L_33 = __this->get_index_3();
		NullCheck(L_32);
		Il2CppChar L_34 = String_get_Chars_m4230566705(L_32, L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		bool L_35 = Char_IsDigit_m751559221(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		G_B20_0 = ((((int32_t)L_35) == ((int32_t)0))? 1 : 0);
		goto IL_0126;
	}

IL_0125:
	{
		G_B20_0 = 1;
	}

IL_0126:
	{
		V_10 = (bool)G_B20_0;
		bool L_36 = V_10;
		if (!L_36)
		{
			goto IL_013e;
		}
	}
	{
		int32_t L_37 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_38 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_38, _stringLiteral3425045059, L_37, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_38);
	}

IL_013e:
	{
		goto IL_0150;
	}

IL_0140:
	{
		int32_t L_39 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_39+(int32_t)1)));
	}

IL_0150:
	{
		int32_t L_40 = __this->get_index_3();
		int32_t L_41 = __this->get_SourceLength_2();
		if ((((int32_t)L_40) >= ((int32_t)L_41)))
		{
			goto IL_0176;
		}
	}
	{
		String_t* L_42 = __this->get_Source_1();
		int32_t L_43 = __this->get_index_3();
		NullCheck(L_42);
		Il2CppChar L_44 = String_get_Chars_m4230566705(L_42, L_43, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		bool L_45 = Char_IsDigit_m751559221(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		G_B27_0 = ((int32_t)(L_45));
		goto IL_0177;
	}

IL_0176:
	{
		G_B27_0 = 0;
	}

IL_0177:
	{
		V_11 = (bool)G_B27_0;
		bool L_46 = V_11;
		if (L_46)
		{
			goto IL_0140;
		}
	}
	{
	}

IL_017e:
	{
		int32_t L_47 = __this->get_index_3();
		int32_t L_48 = V_2;
		bool L_49 = V_0;
		G_B30_0 = ((int32_t)((int32_t)L_47-(int32_t)L_48));
		if (L_49)
		{
			G_B31_0 = ((int32_t)((int32_t)L_47-(int32_t)L_48));
			goto IL_018c;
		}
	}
	{
		G_B32_0 = 0;
		G_B32_1 = G_B30_0;
		goto IL_018d;
	}

IL_018c:
	{
		G_B32_0 = 1;
		G_B32_1 = G_B31_0;
	}

IL_018d:
	{
		V_3 = ((int32_t)((int32_t)G_B32_1-(int32_t)G_B32_0));
		int32_t L_50 = __this->get_index_3();
		int32_t L_51 = __this->get_SourceLength_2();
		if ((((int32_t)L_50) >= ((int32_t)L_51)))
		{
			goto IL_01cc;
		}
	}
	{
		String_t* L_52 = __this->get_Source_1();
		int32_t L_53 = __this->get_index_3();
		NullCheck(L_52);
		Il2CppChar L_54 = String_get_Chars_m4230566705(L_52, L_53, /*hidden argument*/NULL);
		if ((((int32_t)L_54) == ((int32_t)((int32_t)101))))
		{
			goto IL_01c9;
		}
	}
	{
		String_t* L_55 = __this->get_Source_1();
		int32_t L_56 = __this->get_index_3();
		NullCheck(L_55);
		Il2CppChar L_57 = String_get_Chars_m4230566705(L_55, L_56, /*hidden argument*/NULL);
		G_B36_0 = ((((int32_t)L_57) == ((int32_t)((int32_t)69)))? 1 : 0);
		goto IL_01ca;
	}

IL_01c9:
	{
		G_B36_0 = 1;
	}

IL_01ca:
	{
		G_B38_0 = G_B36_0;
		goto IL_01cd;
	}

IL_01cc:
	{
		G_B38_0 = 0;
	}

IL_01cd:
	{
		V_12 = (bool)G_B38_0;
		bool L_58 = V_12;
		if (!L_58)
		{
			goto IL_0336;
		}
	}
	{
		V_1 = (bool)1;
		int32_t L_59 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_59+(int32_t)1)));
		int32_t L_60 = __this->get_index_3();
		int32_t L_61 = __this->get_SourceLength_2();
		V_14 = (bool)((((int32_t)((((int32_t)L_60) < ((int32_t)L_61))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_62 = V_14;
		if (!L_62)
		{
			goto IL_0210;
		}
	}
	{
		int32_t L_63 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_64 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_64, _stringLiteral3425045059, L_63, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_64);
	}

IL_0210:
	{
		int32_t L_65 = __this->get_index_3();
		V_13 = L_65;
		String_t* L_66 = __this->get_Source_1();
		int32_t L_67 = __this->get_index_3();
		NullCheck(L_66);
		Il2CppChar L_68 = String_get_Chars_m4230566705(L_66, L_67, /*hidden argument*/NULL);
		if ((((int32_t)L_68) == ((int32_t)((int32_t)45))))
		{
			goto IL_0244;
		}
	}
	{
		String_t* L_69 = __this->get_Source_1();
		int32_t L_70 = __this->get_index_3();
		NullCheck(L_69);
		Il2CppChar L_71 = String_get_Chars_m4230566705(L_69, L_70, /*hidden argument*/NULL);
		G_B44_0 = ((((int32_t)L_71) == ((int32_t)((int32_t)43)))? 1 : 0);
		goto IL_0245;
	}

IL_0244:
	{
		G_B44_0 = 1;
	}

IL_0245:
	{
		V_15 = (bool)G_B44_0;
		bool L_72 = V_15;
		if (!L_72)
		{
			goto IL_029f;
		}
	}
	{
		int32_t L_73 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_73+(int32_t)1)));
		int32_t L_74 = __this->get_index_3();
		int32_t L_75 = __this->get_SourceLength_2();
		if ((((int32_t)L_74) >= ((int32_t)L_75)))
		{
			goto IL_0283;
		}
	}
	{
		String_t* L_76 = __this->get_Source_1();
		int32_t L_77 = __this->get_index_3();
		NullCheck(L_76);
		Il2CppChar L_78 = String_get_Chars_m4230566705(L_76, L_77, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		bool L_79 = Char_IsDigit_m751559221(NULL /*static, unused*/, L_78, /*hidden argument*/NULL);
		G_B48_0 = ((((int32_t)L_79) == ((int32_t)0))? 1 : 0);
		goto IL_0284;
	}

IL_0283:
	{
		G_B48_0 = 1;
	}

IL_0284:
	{
		V_16 = (bool)G_B48_0;
		bool L_80 = V_16;
		if (!L_80)
		{
			goto IL_029c;
		}
	}
	{
		int32_t L_81 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_82 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_82, _stringLiteral3425045059, L_81, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_82);
	}

IL_029c:
	{
		goto IL_02d2;
	}

IL_029f:
	{
		String_t* L_83 = __this->get_Source_1();
		int32_t L_84 = __this->get_index_3();
		NullCheck(L_83);
		Il2CppChar L_85 = String_get_Chars_m4230566705(L_83, L_84, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		bool L_86 = Char_IsDigit_m751559221(NULL /*static, unused*/, L_85, /*hidden argument*/NULL);
		V_17 = (bool)((((int32_t)L_86) == ((int32_t)0))? 1 : 0);
		bool L_87 = V_17;
		if (!L_87)
		{
			goto IL_02d1;
		}
	}
	{
		int32_t L_88 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_89 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_89, _stringLiteral3425045059, L_88, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_89);
	}

IL_02d1:
	{
	}

IL_02d2:
	{
		goto IL_02e4;
	}

IL_02d4:
	{
		int32_t L_90 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_90+(int32_t)1)));
	}

IL_02e4:
	{
		int32_t L_91 = __this->get_index_3();
		int32_t L_92 = __this->get_SourceLength_2();
		if ((((int32_t)L_91) >= ((int32_t)L_92)))
		{
			goto IL_030a;
		}
	}
	{
		String_t* L_93 = __this->get_Source_1();
		int32_t L_94 = __this->get_index_3();
		NullCheck(L_93);
		Il2CppChar L_95 = String_get_Chars_m4230566705(L_93, L_94, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		bool L_96 = Char_IsDigit_m751559221(NULL /*static, unused*/, L_95, /*hidden argument*/NULL);
		G_B59_0 = ((int32_t)(L_96));
		goto IL_030b;
	}

IL_030a:
	{
		G_B59_0 = 0;
	}

IL_030b:
	{
		V_18 = (bool)G_B59_0;
		bool L_97 = V_18;
		if (L_97)
		{
			goto IL_02d4;
		}
	}
	{
		String_t* L_98 = __this->get_Source_1();
		int32_t L_99 = V_13;
		int32_t L_100 = __this->get_index_3();
		int32_t L_101 = V_13;
		NullCheck(L_98);
		String_t* L_102 = String_Substring_m12482732(L_98, L_99, ((int32_t)((int32_t)L_100-(int32_t)L_101)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t104580544_il2cpp_TypeInfo_var);
		NumberFormatInfo_t104580544 * L_103 = NumberFormatInfo_get_InvariantInfo_m2658215204(NULL /*static, unused*/, /*hidden argument*/NULL);
		Int32_TryParse_m2941352213(NULL /*static, unused*/, L_102, 7, L_103, (&V_4), /*hidden argument*/NULL);
	}

IL_0336:
	{
		String_t* L_104 = __this->get_Source_1();
		int32_t L_105 = V_2;
		int32_t L_106 = __this->get_index_3();
		int32_t L_107 = V_2;
		NullCheck(L_104);
		String_t* L_108 = String_Substring_m12482732(L_104, L_105, ((int32_t)((int32_t)L_106-(int32_t)L_107)), /*hidden argument*/NULL);
		V_5 = L_108;
		bool L_109 = V_0;
		if (L_109)
		{
			goto IL_0359;
		}
	}
	{
		bool L_110 = V_1;
		if (L_110)
		{
			goto IL_0359;
		}
	}
	{
		int32_t L_111 = V_3;
		G_B65_0 = ((((int32_t)L_111) < ((int32_t)((int32_t)19)))? 1 : 0);
		goto IL_035a;
	}

IL_0359:
	{
		G_B65_0 = 0;
	}

IL_035a:
	{
		V_19 = (bool)G_B65_0;
		bool L_112 = V_19;
		if (!L_112)
		{
			goto IL_042f;
		}
	}
	{
		String_t* L_113 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t104580544_il2cpp_TypeInfo_var);
		NumberFormatInfo_t104580544 * L_114 = NumberFormatInfo_get_InvariantInfo_m2658215204(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t724701077_il2cpp_TypeInfo_var);
		Decimal_t724701077  L_115 = Decimal_Parse_m2923785927(NULL /*static, unused*/, L_113, 7, L_114, /*hidden argument*/NULL);
		V_20 = L_115;
		Type_t * L_116 = ___expectedType0;
		V_21 = (bool)((!(((Il2CppObject*)(Type_t *)L_116) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_117 = V_21;
		if (!L_117)
		{
			goto IL_039d;
		}
	}
	{
		JsonReaderSettings_t1410336530 * L_118 = __this->get_Settings_0();
		NullCheck(L_118);
		TypeCoercionUtility_t3996825458 * L_119 = L_118->get_Coercion_0();
		Type_t * L_120 = ___expectedType0;
		Decimal_t724701077  L_121 = V_20;
		Decimal_t724701077  L_122 = L_121;
		Il2CppObject * L_123 = Box(Decimal_t724701077_il2cpp_TypeInfo_var, &L_122);
		NullCheck(L_119);
		Il2CppObject * L_124 = TypeCoercionUtility_CoerceType_m3744809450(L_119, L_120, L_123, /*hidden argument*/NULL);
		V_22 = L_124;
		goto IL_04a3;
	}

IL_039d:
	{
		Decimal_t724701077  L_125 = V_20;
		Decimal_t724701077  L_126;
		memset(&L_126, 0, sizeof(L_126));
		Decimal__ctor_m1010012873(&L_126, ((int32_t)-2147483648LL), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t724701077_il2cpp_TypeInfo_var);
		bool L_127 = Decimal_op_GreaterThanOrEqual_m3020777826(NULL /*static, unused*/, L_125, L_126, /*hidden argument*/NULL);
		if (!L_127)
		{
			goto IL_03c3;
		}
	}
	{
		Decimal_t724701077  L_128 = V_20;
		Decimal_t724701077  L_129;
		memset(&L_129, 0, sizeof(L_129));
		Decimal__ctor_m1010012873(&L_129, ((int32_t)2147483647LL), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t724701077_il2cpp_TypeInfo_var);
		bool L_130 = Decimal_op_LessThanOrEqual_m2510179999(NULL /*static, unused*/, L_128, L_129, /*hidden argument*/NULL);
		G_B71_0 = ((int32_t)(L_130));
		goto IL_03c4;
	}

IL_03c3:
	{
		G_B71_0 = 0;
	}

IL_03c4:
	{
		V_23 = (bool)G_B71_0;
		bool L_131 = V_23;
		if (!L_131)
		{
			goto IL_03de;
		}
	}
	{
		Decimal_t724701077  L_132 = V_20;
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t724701077_il2cpp_TypeInfo_var);
		int32_t L_133 = Decimal_op_Explicit_m2494894232(NULL /*static, unused*/, L_132, /*hidden argument*/NULL);
		int32_t L_134 = L_133;
		Il2CppObject * L_135 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_134);
		V_22 = L_135;
		goto IL_04a3;
	}

IL_03de:
	{
		Decimal_t724701077  L_136 = V_20;
		Decimal_t724701077  L_137;
		memset(&L_137, 0, sizeof(L_137));
		Decimal__ctor_m1991243832(&L_137, ((int64_t)std::numeric_limits<int64_t>::min()), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t724701077_il2cpp_TypeInfo_var);
		bool L_138 = Decimal_op_GreaterThanOrEqual_m3020777826(NULL /*static, unused*/, L_136, L_137, /*hidden argument*/NULL);
		if (!L_138)
		{
			goto IL_040c;
		}
	}
	{
		Decimal_t724701077  L_139 = V_20;
		Decimal_t724701077  L_140;
		memset(&L_140, 0, sizeof(L_140));
		Decimal__ctor_m1991243832(&L_140, ((int64_t)std::numeric_limits<int64_t>::max()), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t724701077_il2cpp_TypeInfo_var);
		bool L_141 = Decimal_op_LessThanOrEqual_m2510179999(NULL /*static, unused*/, L_139, L_140, /*hidden argument*/NULL);
		G_B76_0 = ((int32_t)(L_141));
		goto IL_040d;
	}

IL_040c:
	{
		G_B76_0 = 0;
	}

IL_040d:
	{
		V_24 = (bool)G_B76_0;
		bool L_142 = V_24;
		if (!L_142)
		{
			goto IL_0424;
		}
	}
	{
		Decimal_t724701077  L_143 = V_20;
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t724701077_il2cpp_TypeInfo_var);
		int64_t L_144 = Decimal_op_Explicit_m682912811(NULL /*static, unused*/, L_143, /*hidden argument*/NULL);
		int64_t L_145 = L_144;
		Il2CppObject * L_146 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_145);
		V_22 = L_146;
		goto IL_04a3;
	}

IL_0424:
	{
		Decimal_t724701077  L_147 = V_20;
		Decimal_t724701077  L_148 = L_147;
		Il2CppObject * L_149 = Box(Decimal_t724701077_il2cpp_TypeInfo_var, &L_148);
		V_22 = L_149;
		goto IL_04a3;
	}

IL_042f:
	{
		Type_t * L_150 = ___expectedType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_151 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Decimal_t724701077_0_0_0_var), /*hidden argument*/NULL);
		V_26 = (bool)((((Il2CppObject*)(Type_t *)L_150) == ((Il2CppObject*)(Type_t *)L_151))? 1 : 0);
		bool L_152 = V_26;
		if (!L_152)
		{
			goto IL_045e;
		}
	}
	{
		String_t* L_153 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t104580544_il2cpp_TypeInfo_var);
		NumberFormatInfo_t104580544 * L_154 = NumberFormatInfo_get_InvariantInfo_m2658215204(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t724701077_il2cpp_TypeInfo_var);
		Decimal_t724701077  L_155 = Decimal_Parse_m2923785927(NULL /*static, unused*/, L_153, ((int32_t)167), L_154, /*hidden argument*/NULL);
		Decimal_t724701077  L_156 = L_155;
		Il2CppObject * L_157 = Box(Decimal_t724701077_il2cpp_TypeInfo_var, &L_156);
		V_22 = L_157;
		goto IL_04a3;
	}

IL_045e:
	{
		String_t* L_158 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t104580544_il2cpp_TypeInfo_var);
		NumberFormatInfo_t104580544 * L_159 = NumberFormatInfo_get_InvariantInfo_m2658215204(NULL /*static, unused*/, /*hidden argument*/NULL);
		double L_160 = Double_Parse_m2344589511(NULL /*static, unused*/, L_158, ((int32_t)167), L_159, /*hidden argument*/NULL);
		V_25 = L_160;
		Type_t * L_161 = ___expectedType0;
		V_27 = (bool)((!(((Il2CppObject*)(Type_t *)L_161) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_162 = V_27;
		if (!L_162)
		{
			goto IL_0498;
		}
	}
	{
		JsonReaderSettings_t1410336530 * L_163 = __this->get_Settings_0();
		NullCheck(L_163);
		TypeCoercionUtility_t3996825458 * L_164 = L_163->get_Coercion_0();
		Type_t * L_165 = ___expectedType0;
		double L_166 = V_25;
		double L_167 = L_166;
		Il2CppObject * L_168 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_167);
		NullCheck(L_164);
		Il2CppObject * L_169 = TypeCoercionUtility_CoerceType_m3744809450(L_164, L_165, L_168, /*hidden argument*/NULL);
		V_22 = L_169;
		goto IL_04a3;
	}

IL_0498:
	{
		double L_170 = V_25;
		double L_171 = L_170;
		Il2CppObject * L_172 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_171);
		V_22 = L_172;
		goto IL_04a3;
	}

IL_04a3:
	{
		Il2CppObject * L_173 = V_22;
		return L_173;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.JsonReader::Deserialize(System.String,System.Int32,System.Type)
extern Il2CppClass* JsonReader_t2021823321_il2cpp_TypeInfo_var;
extern const uint32_t JsonReader_Deserialize_m2812844944_MetadataUsageId;
extern "C"  Il2CppObject * JsonReader_Deserialize_m2812844944 (Il2CppObject * __this /* static, unused */, String_t* ___value0, int32_t ___start1, Type_t * ___type2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_Deserialize_m2812844944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		String_t* L_0 = ___value0;
		JsonReader_t2021823321 * L_1 = (JsonReader_t2021823321 *)il2cpp_codegen_object_new(JsonReader_t2021823321_il2cpp_TypeInfo_var);
		JsonReader__ctor_m1860805928(L_1, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___start1;
		Type_t * L_3 = ___type2;
		NullCheck(L_1);
		Il2CppObject * L_4 = JsonReader_Deserialize_m1628683004(L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0011;
	}

IL_0011:
	{
		Il2CppObject * L_5 = V_0;
		return L_5;
	}
}
// Pathfinding.Serialization.JsonFx.JsonToken Pathfinding.Serialization.JsonFx.JsonReader::Tokenize()
extern "C"  int32_t JsonReader_Tokenize_m3135568107 (JsonReader_t2021823321 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = JsonReader_Tokenize_m3812013816(__this, (bool)0, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000b;
	}

IL_000b:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// Pathfinding.Serialization.JsonFx.JsonToken Pathfinding.Serialization.JsonFx.JsonReader::Tokenize(System.Boolean)
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3587242997;
extern Il2CppCodeGenString* _stringLiteral1137912791;
extern Il2CppCodeGenString* _stringLiteral51790588;
extern Il2CppCodeGenString* _stringLiteral2454835114;
extern Il2CppCodeGenString* _stringLiteral51790593;
extern Il2CppCodeGenString* _stringLiteral2162321587;
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern Il2CppCodeGenString* _stringLiteral696030553;
extern Il2CppCodeGenString* _stringLiteral487594890;
extern Il2CppCodeGenString* _stringLiteral55341327;
extern Il2CppCodeGenString* _stringLiteral3366826370;
extern const uint32_t JsonReader_Tokenize_m3812013816_MetadataUsageId;
extern "C"  int32_t JsonReader_Tokenize_m3812013816 (JsonReader_t2021823321 * __this, bool ___allowUnquotedString0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_Tokenize_m3812013816_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	bool V_6 = false;
	bool V_7 = false;
	bool V_8 = false;
	bool V_9 = false;
	int32_t V_10 = 0;
	bool V_11 = false;
	bool V_12 = false;
	bool V_13 = false;
	bool V_14 = false;
	bool V_15 = false;
	bool V_16 = false;
	bool V_17 = false;
	bool V_18 = false;
	bool V_19 = false;
	bool V_20 = false;
	Il2CppChar V_21 = 0x0;
	bool V_22 = false;
	bool V_23 = false;
	bool V_24 = false;
	bool V_25 = false;
	bool V_26 = false;
	bool V_27 = false;
	bool V_28 = false;
	bool V_29 = false;
	bool V_30 = false;
	int32_t G_B24_0 = 0;
	int32_t G_B76_0 = 0;
	int32_t G_B78_0 = 0;
	{
		int32_t L_0 = __this->get_index_3();
		int32_t L_1 = __this->get_SourceLength_2();
		V_0 = (bool)((((int32_t)((((int32_t)L_0) < ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		V_1 = 0;
		goto IL_04ed;
	}

IL_001e:
	{
		goto IL_004d;
	}

IL_0020:
	{
		int32_t L_3 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		int32_t L_4 = __this->get_index_3();
		int32_t L_5 = __this->get_SourceLength_2();
		V_2 = (bool)((((int32_t)((((int32_t)L_4) < ((int32_t)L_5))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_6 = V_2;
		if (!L_6)
		{
			goto IL_004c;
		}
	}
	{
		V_1 = 0;
		goto IL_04ed;
	}

IL_004c:
	{
	}

IL_004d:
	{
		String_t* L_7 = __this->get_Source_1();
		int32_t L_8 = __this->get_index_3();
		NullCheck(L_7);
		Il2CppChar L_9 = String_get_Chars_m4230566705(L_7, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		bool L_10 = Char_IsWhiteSpace_m1507160293(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		bool L_11 = V_3;
		if (L_11)
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_12 = __this->get_Source_1();
		int32_t L_13 = __this->get_index_3();
		NullCheck(L_12);
		Il2CppChar L_14 = String_get_Chars_m4230566705(L_12, L_13, /*hidden argument*/NULL);
		NullCheck(_stringLiteral3587242997);
		Il2CppChar L_15 = String_get_Chars_m4230566705(_stringLiteral3587242997, 0, /*hidden argument*/NULL);
		V_4 = (bool)((((int32_t)L_14) == ((int32_t)L_15))? 1 : 0);
		bool L_16 = V_4;
		if (!L_16)
		{
			goto IL_02d5;
		}
	}
	{
		int32_t L_17 = __this->get_index_3();
		int32_t L_18 = __this->get_SourceLength_2();
		V_6 = (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_17+(int32_t)1))) < ((int32_t)L_18))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_19 = V_6;
		if (!L_19)
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_20 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_21 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_21, _stringLiteral1137912791, L_20, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_00ba:
	{
		int32_t L_22 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_22+(int32_t)1)));
		V_5 = (bool)0;
		String_t* L_23 = __this->get_Source_1();
		int32_t L_24 = __this->get_index_3();
		NullCheck(L_23);
		Il2CppChar L_25 = String_get_Chars_m4230566705(L_23, L_24, /*hidden argument*/NULL);
		NullCheck(_stringLiteral3587242997);
		Il2CppChar L_26 = String_get_Chars_m4230566705(_stringLiteral3587242997, 1, /*hidden argument*/NULL);
		V_7 = (bool)((((int32_t)L_25) == ((int32_t)L_26))? 1 : 0);
		bool L_27 = V_7;
		if (!L_27)
		{
			goto IL_00f6;
		}
	}
	{
		V_5 = (bool)1;
		goto IL_012f;
	}

IL_00f6:
	{
		String_t* L_28 = __this->get_Source_1();
		int32_t L_29 = __this->get_index_3();
		NullCheck(L_28);
		Il2CppChar L_30 = String_get_Chars_m4230566705(L_28, L_29, /*hidden argument*/NULL);
		NullCheck(_stringLiteral51790588);
		Il2CppChar L_31 = String_get_Chars_m4230566705(_stringLiteral51790588, 1, /*hidden argument*/NULL);
		V_8 = (bool)((((int32_t)((((int32_t)L_30) == ((int32_t)L_31))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_32 = V_8;
		if (!L_32)
		{
			goto IL_012f;
		}
	}
	{
		int32_t L_33 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_34 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_34, _stringLiteral1137912791, L_33, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_34);
	}

IL_012f:
	{
		int32_t L_35 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_35+(int32_t)1)));
		bool L_36 = V_5;
		V_9 = L_36;
		bool L_37 = V_9;
		if (!L_37)
		{
			goto IL_0230;
		}
	}
	{
		int32_t L_38 = __this->get_index_3();
		V_10 = ((int32_t)((int32_t)L_38-(int32_t)2));
		int32_t L_39 = __this->get_index_3();
		int32_t L_40 = __this->get_SourceLength_2();
		V_11 = (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_39+(int32_t)1))) < ((int32_t)L_40))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_41 = V_11;
		if (!L_41)
		{
			goto IL_017a;
		}
	}
	{
		int32_t L_42 = V_10;
		JsonDeserializationException_t1886537714 * L_43 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_43, _stringLiteral2454835114, L_42, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_43);
	}

IL_017a:
	{
		goto IL_01b3;
	}

IL_017c:
	{
		int32_t L_44 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_44+(int32_t)1)));
		int32_t L_45 = __this->get_index_3();
		int32_t L_46 = __this->get_SourceLength_2();
		V_12 = (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_45+(int32_t)1))) < ((int32_t)L_46))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_47 = V_12;
		if (!L_47)
		{
			goto IL_01b2;
		}
	}
	{
		int32_t L_48 = V_10;
		JsonDeserializationException_t1886537714 * L_49 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_49, _stringLiteral2454835114, L_48, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_49);
	}

IL_01b2:
	{
	}

IL_01b3:
	{
		String_t* L_50 = __this->get_Source_1();
		int32_t L_51 = __this->get_index_3();
		NullCheck(L_50);
		Il2CppChar L_52 = String_get_Chars_m4230566705(L_50, L_51, /*hidden argument*/NULL);
		NullCheck(_stringLiteral51790593);
		Il2CppChar L_53 = String_get_Chars_m4230566705(_stringLiteral51790593, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_52) == ((uint32_t)L_53))))
		{
			goto IL_01f6;
		}
	}
	{
		String_t* L_54 = __this->get_Source_1();
		int32_t L_55 = __this->get_index_3();
		NullCheck(L_54);
		Il2CppChar L_56 = String_get_Chars_m4230566705(L_54, ((int32_t)((int32_t)L_55+(int32_t)1)), /*hidden argument*/NULL);
		NullCheck(_stringLiteral51790593);
		Il2CppChar L_57 = String_get_Chars_m4230566705(_stringLiteral51790593, 1, /*hidden argument*/NULL);
		G_B24_0 = ((((int32_t)((((int32_t)L_56) == ((int32_t)L_57))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_01f7;
	}

IL_01f6:
	{
		G_B24_0 = 1;
	}

IL_01f7:
	{
		V_13 = (bool)G_B24_0;
		bool L_58 = V_13;
		if (L_58)
		{
			goto IL_017c;
		}
	}
	{
		int32_t L_59 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_59+(int32_t)2)));
		int32_t L_60 = __this->get_index_3();
		int32_t L_61 = __this->get_SourceLength_2();
		V_14 = (bool)((((int32_t)((((int32_t)L_60) < ((int32_t)L_61))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_62 = V_14;
		if (!L_62)
		{
			goto IL_022d;
		}
	}
	{
		V_1 = 0;
		goto IL_04ed;
	}

IL_022d:
	{
		goto IL_0287;
	}

IL_0230:
	{
		goto IL_0262;
	}

IL_0233:
	{
		int32_t L_63 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_63+(int32_t)1)));
		int32_t L_64 = __this->get_index_3();
		int32_t L_65 = __this->get_SourceLength_2();
		V_15 = (bool)((((int32_t)((((int32_t)L_64) < ((int32_t)L_65))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_66 = V_15;
		if (!L_66)
		{
			goto IL_0261;
		}
	}
	{
		V_1 = 0;
		goto IL_04ed;
	}

IL_0261:
	{
	}

IL_0262:
	{
		String_t* L_67 = __this->get_Source_1();
		int32_t L_68 = __this->get_index_3();
		NullCheck(L_67);
		Il2CppChar L_69 = String_get_Chars_m4230566705(L_67, L_68, /*hidden argument*/NULL);
		NullCheck(_stringLiteral2162321587);
		int32_t L_70 = String_IndexOf_m2358239236(_stringLiteral2162321587, L_69, /*hidden argument*/NULL);
		V_16 = (bool)((((int32_t)L_70) < ((int32_t)0))? 1 : 0);
		bool L_71 = V_16;
		if (L_71)
		{
			goto IL_0233;
		}
	}
	{
	}

IL_0287:
	{
		goto IL_02b8;
	}

IL_0289:
	{
		int32_t L_72 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_72+(int32_t)1)));
		int32_t L_73 = __this->get_index_3();
		int32_t L_74 = __this->get_SourceLength_2();
		V_17 = (bool)((((int32_t)((((int32_t)L_73) < ((int32_t)L_74))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_75 = V_17;
		if (!L_75)
		{
			goto IL_02b7;
		}
	}
	{
		V_1 = 0;
		goto IL_04ed;
	}

IL_02b7:
	{
	}

IL_02b8:
	{
		String_t* L_76 = __this->get_Source_1();
		int32_t L_77 = __this->get_index_3();
		NullCheck(L_76);
		Il2CppChar L_78 = String_get_Chars_m4230566705(L_76, L_77, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		bool L_79 = Char_IsWhiteSpace_m1507160293(NULL /*static, unused*/, L_78, /*hidden argument*/NULL);
		V_18 = L_79;
		bool L_80 = V_18;
		if (L_80)
		{
			goto IL_0289;
		}
	}
	{
	}

IL_02d5:
	{
		String_t* L_81 = __this->get_Source_1();
		int32_t L_82 = __this->get_index_3();
		NullCheck(L_81);
		Il2CppChar L_83 = String_get_Chars_m4230566705(L_81, L_82, /*hidden argument*/NULL);
		V_19 = (bool)((((int32_t)L_83) == ((int32_t)((int32_t)43)))? 1 : 0);
		bool L_84 = V_19;
		if (!L_84)
		{
			goto IL_031f;
		}
	}
	{
		int32_t L_85 = __this->get_index_3();
		__this->set_index_3(((int32_t)((int32_t)L_85+(int32_t)1)));
		int32_t L_86 = __this->get_index_3();
		int32_t L_87 = __this->get_SourceLength_2();
		V_20 = (bool)((((int32_t)((((int32_t)L_86) < ((int32_t)L_87))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_88 = V_20;
		if (!L_88)
		{
			goto IL_031e;
		}
	}
	{
		V_1 = 0;
		goto IL_04ed;
	}

IL_031e:
	{
	}

IL_031f:
	{
		String_t* L_89 = __this->get_Source_1();
		int32_t L_90 = __this->get_index_3();
		NullCheck(L_89);
		Il2CppChar L_91 = String_get_Chars_m4230566705(L_89, L_90, /*hidden argument*/NULL);
		V_21 = L_91;
		Il2CppChar L_92 = V_21;
		if ((!(((uint32_t)L_92) <= ((uint32_t)((int32_t)58)))))
		{
			goto IL_035e;
		}
	}
	{
		Il2CppChar L_93 = V_21;
		if ((!(((uint32_t)L_93) <= ((uint32_t)((int32_t)39)))))
		{
			goto IL_034e;
		}
	}
	{
		Il2CppChar L_94 = V_21;
		if ((((int32_t)L_94) == ((int32_t)((int32_t)34))))
		{
			goto IL_03a8;
		}
	}
	{
		goto IL_0346;
	}

IL_0346:
	{
		Il2CppChar L_95 = V_21;
		if ((((int32_t)L_95) == ((int32_t)((int32_t)39))))
		{
			goto IL_03a8;
		}
	}
	{
		goto IL_03c3;
	}

IL_034e:
	{
		Il2CppChar L_96 = V_21;
		if ((((int32_t)L_96) == ((int32_t)((int32_t)44))))
		{
			goto IL_03b1;
		}
	}
	{
		goto IL_0356;
	}

IL_0356:
	{
		Il2CppChar L_97 = V_21;
		if ((((int32_t)L_97) == ((int32_t)((int32_t)58))))
		{
			goto IL_03ba;
		}
	}
	{
		goto IL_03c3;
	}

IL_035e:
	{
		Il2CppChar L_98 = V_21;
		if ((!(((uint32_t)L_98) <= ((uint32_t)((int32_t)93)))))
		{
			goto IL_0374;
		}
	}
	{
		Il2CppChar L_99 = V_21;
		if ((((int32_t)L_99) == ((int32_t)((int32_t)91))))
		{
			goto IL_0384;
		}
	}
	{
		goto IL_036c;
	}

IL_036c:
	{
		Il2CppChar L_100 = V_21;
		if ((((int32_t)L_100) == ((int32_t)((int32_t)93))))
		{
			goto IL_038d;
		}
	}
	{
		goto IL_03c3;
	}

IL_0374:
	{
		Il2CppChar L_101 = V_21;
		if ((((int32_t)L_101) == ((int32_t)((int32_t)123))))
		{
			goto IL_0396;
		}
	}
	{
		goto IL_037c;
	}

IL_037c:
	{
		Il2CppChar L_102 = V_21;
		if ((((int32_t)L_102) == ((int32_t)((int32_t)125))))
		{
			goto IL_039f;
		}
	}
	{
		goto IL_03c3;
	}

IL_0384:
	{
		V_1 = ((int32_t)10);
		goto IL_04ed;
	}

IL_038d:
	{
		V_1 = ((int32_t)11);
		goto IL_04ed;
	}

IL_0396:
	{
		V_1 = ((int32_t)12);
		goto IL_04ed;
	}

IL_039f:
	{
		V_1 = ((int32_t)13);
		goto IL_04ed;
	}

IL_03a8:
	{
		V_1 = ((int32_t)9);
		goto IL_04ed;
	}

IL_03b1:
	{
		V_1 = ((int32_t)15);
		goto IL_04ed;
	}

IL_03ba:
	{
		V_1 = ((int32_t)14);
		goto IL_04ed;
	}

IL_03c3:
	{
		goto IL_03c6;
	}

IL_03c6:
	{
		String_t* L_103 = __this->get_Source_1();
		int32_t L_104 = __this->get_index_3();
		NullCheck(L_103);
		Il2CppChar L_105 = String_get_Chars_m4230566705(L_103, L_104, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		bool L_106 = Char_IsDigit_m751559221(NULL /*static, unused*/, L_105, /*hidden argument*/NULL);
		if (L_106)
		{
			goto IL_0420;
		}
	}
	{
		String_t* L_107 = __this->get_Source_1();
		int32_t L_108 = __this->get_index_3();
		NullCheck(L_107);
		Il2CppChar L_109 = String_get_Chars_m4230566705(L_107, L_108, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_109) == ((uint32_t)((int32_t)45)))))
		{
			goto IL_041d;
		}
	}
	{
		int32_t L_110 = __this->get_index_3();
		int32_t L_111 = __this->get_SourceLength_2();
		if ((((int32_t)((int32_t)((int32_t)L_110+(int32_t)1))) >= ((int32_t)L_111)))
		{
			goto IL_041d;
		}
	}
	{
		String_t* L_112 = __this->get_Source_1();
		int32_t L_113 = __this->get_index_3();
		NullCheck(L_112);
		Il2CppChar L_114 = String_get_Chars_m4230566705(L_112, ((int32_t)((int32_t)L_113+(int32_t)1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		bool L_115 = Char_IsDigit_m751559221(NULL /*static, unused*/, L_114, /*hidden argument*/NULL);
		G_B76_0 = ((int32_t)(L_115));
		goto IL_041e;
	}

IL_041d:
	{
		G_B76_0 = 0;
	}

IL_041e:
	{
		G_B78_0 = G_B76_0;
		goto IL_0421;
	}

IL_0420:
	{
		G_B78_0 = 1;
	}

IL_0421:
	{
		V_22 = (bool)G_B78_0;
		bool L_116 = V_22;
		if (!L_116)
		{
			goto IL_042f;
		}
	}
	{
		V_1 = 8;
		goto IL_04ed;
	}

IL_042f:
	{
		bool L_117 = JsonReader_MatchLiteral_m4215823740(__this, _stringLiteral2609877245, /*hidden argument*/NULL);
		V_23 = L_117;
		bool L_118 = V_23;
		if (!L_118)
		{
			goto IL_0448;
		}
	}
	{
		V_1 = 3;
		goto IL_04ed;
	}

IL_0448:
	{
		bool L_119 = JsonReader_MatchLiteral_m4215823740(__this, _stringLiteral3323263070, /*hidden argument*/NULL);
		V_24 = L_119;
		bool L_120 = V_24;
		if (!L_120)
		{
			goto IL_0461;
		}
	}
	{
		V_1 = 4;
		goto IL_04ed;
	}

IL_0461:
	{
		bool L_121 = JsonReader_MatchLiteral_m4215823740(__this, _stringLiteral1743624307, /*hidden argument*/NULL);
		V_25 = L_121;
		bool L_122 = V_25;
		if (!L_122)
		{
			goto IL_0477;
		}
	}
	{
		V_1 = 2;
		goto IL_04ed;
	}

IL_0477:
	{
		bool L_123 = JsonReader_MatchLiteral_m4215823740(__this, _stringLiteral696030553, /*hidden argument*/NULL);
		V_26 = L_123;
		bool L_124 = V_26;
		if (!L_124)
		{
			goto IL_048d;
		}
	}
	{
		V_1 = 5;
		goto IL_04ed;
	}

IL_048d:
	{
		bool L_125 = JsonReader_MatchLiteral_m4215823740(__this, _stringLiteral487594890, /*hidden argument*/NULL);
		V_27 = L_125;
		bool L_126 = V_27;
		if (!L_126)
		{
			goto IL_04a3;
		}
	}
	{
		V_1 = 6;
		goto IL_04ed;
	}

IL_04a3:
	{
		bool L_127 = JsonReader_MatchLiteral_m4215823740(__this, _stringLiteral55341327, /*hidden argument*/NULL);
		V_28 = L_127;
		bool L_128 = V_28;
		if (!L_128)
		{
			goto IL_04b9;
		}
	}
	{
		V_1 = 7;
		goto IL_04ed;
	}

IL_04b9:
	{
		bool L_129 = JsonReader_MatchLiteral_m4215823740(__this, _stringLiteral3366826370, /*hidden argument*/NULL);
		V_29 = L_129;
		bool L_130 = V_29;
		if (!L_130)
		{
			goto IL_04cf;
		}
	}
	{
		V_1 = 1;
		goto IL_04ed;
	}

IL_04cf:
	{
		bool L_131 = ___allowUnquotedString0;
		V_30 = L_131;
		bool L_132 = V_30;
		if (!L_132)
		{
			goto IL_04dc;
		}
	}
	{
		V_1 = ((int32_t)16);
		goto IL_04ed;
	}

IL_04dc:
	{
		int32_t L_133 = __this->get_index_3();
		JsonDeserializationException_t1886537714 * L_134 = (JsonDeserializationException_t1886537714 *)il2cpp_codegen_object_new(JsonDeserializationException_t1886537714_il2cpp_TypeInfo_var);
		JsonDeserializationException__ctor_m897186328(L_134, _stringLiteral1137912791, L_133, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_134);
	}

IL_04ed:
	{
		int32_t L_135 = V_1;
		return L_135;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonReader::MatchLiteral(System.String)
extern "C"  bool JsonReader_MatchLiteral_m4215823740 (JsonReader_t2021823321 * __this, String_t* ___literal0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	int32_t G_B7_0 = 0;
	{
		String_t* L_0 = ___literal0;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1606060069(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = 0;
		int32_t L_2 = __this->get_index_3();
		V_2 = L_2;
		goto IL_003f;
	}

IL_0013:
	{
		String_t* L_3 = ___literal0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		Il2CppChar L_5 = String_get_Chars_m4230566705(L_3, L_4, /*hidden argument*/NULL);
		String_t* L_6 = __this->get_Source_1();
		int32_t L_7 = V_2;
		NullCheck(L_6);
		Il2CppChar L_8 = String_get_Chars_m4230566705(L_6, L_7, /*hidden argument*/NULL);
		V_3 = (bool)((((int32_t)((((int32_t)L_5) == ((int32_t)L_8))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_9 = V_3;
		if (!L_9)
		{
			goto IL_0036;
		}
	}
	{
		V_4 = (bool)0;
		goto IL_005a;
	}

IL_0036:
	{
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
		int32_t L_11 = V_2;
		V_2 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) >= ((int32_t)L_13)))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_14 = V_2;
		int32_t L_15 = __this->get_SourceLength_2();
		G_B7_0 = ((((int32_t)L_14) < ((int32_t)L_15))? 1 : 0);
		goto IL_004f;
	}

IL_004e:
	{
		G_B7_0 = 0;
	}

IL_004f:
	{
		V_5 = (bool)G_B7_0;
		bool L_16 = V_5;
		if (L_16)
		{
			goto IL_0013;
		}
	}
	{
		V_4 = (bool)1;
		goto IL_005a;
	}

IL_005a:
	{
		bool L_17 = V_4;
		return L_17;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonReaderSettings::get_HandleCyclicReferences()
extern "C"  bool JsonReaderSettings_get_HandleCyclicReferences_m3051340765 (JsonReaderSettings_t1410336530 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CHandleCyclicReferencesU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonReaderSettings::get_AllowUnquotedObjectKeys()
extern "C"  bool JsonReaderSettings_get_AllowUnquotedObjectKeys_m1632960917 (JsonReaderSettings_t1410336530 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_allowUnquotedObjectKeys_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonReaderSettings::IsTypeHintName(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringComparer_t1574862926_il2cpp_TypeInfo_var;
extern const uint32_t JsonReaderSettings_IsTypeHintName_m3613446497_MetadataUsageId;
extern "C"  bool JsonReaderSettings_IsTypeHintName_m3613446497 (JsonReaderSettings_t1410336530 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReaderSettings_IsTypeHintName_m3613446497_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0029;
		}
	}
	{
		String_t* L_2 = __this->get_typeHintName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t1574862926_il2cpp_TypeInfo_var);
		StringComparer_t1574862926 * L_4 = StringComparer_get_Ordinal_m3140767557(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = __this->get_typeHintName_2();
		String_t* L_6 = ___name0;
		NullCheck(L_4);
		bool L_7 = VirtFuncInvoker2< bool, String_t*, String_t* >::Invoke(11 /* System.Boolean System.StringComparer::Equals(System.String,System.String) */, L_4, L_5, L_6);
		G_B4_0 = ((int32_t)(L_7));
		goto IL_002a;
	}

IL_0029:
	{
		G_B4_0 = 0;
	}

IL_002a:
	{
		V_0 = (bool)G_B4_0;
		goto IL_002d;
	}

IL_002d:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
// Pathfinding.Serialization.JsonFx.JsonConverter Pathfinding.Serialization.JsonFx.JsonReaderSettings::GetConverter(System.Type)
extern const MethodInfo* List_1_get_Item_m4187249556_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3499622935_MethodInfo_var;
extern const uint32_t JsonReaderSettings_GetConverter_m1578103157_MetadataUsageId;
extern "C"  JsonConverter_t4092422604 * JsonReaderSettings_GetConverter_m1578103157 (JsonReaderSettings_t1410336530 * __this, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReaderSettings_GetConverter_m1578103157_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	JsonConverter_t4092422604 * V_2 = NULL;
	bool V_3 = false;
	{
		V_0 = 0;
		goto IL_002e;
	}

IL_0005:
	{
		List_1_t3461543736 * L_0 = __this->get_converters_4();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		JsonConverter_t4092422604 * L_2 = List_1_get_Item_m4187249556(L_0, L_1, /*hidden argument*/List_1_get_Item_m4187249556_MethodInfo_var);
		Type_t * L_3 = ___type0;
		NullCheck(L_2);
		bool L_4 = VirtFuncInvoker1< bool, Type_t * >::Invoke(4 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonConverter::CanConvert(System.Type) */, L_2, L_3);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_002a;
		}
	}
	{
		List_1_t3461543736 * L_6 = __this->get_converters_4();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JsonConverter_t4092422604 * L_8 = List_1_get_Item_m4187249556(L_6, L_7, /*hidden argument*/List_1_get_Item_m4187249556_MethodInfo_var);
		V_2 = L_8;
		goto IL_0044;
	}

IL_002a:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_0;
		List_1_t3461543736 * L_11 = __this->get_converters_4();
		NullCheck(L_11);
		int32_t L_12 = List_1_get_Count_m3499622935(L_11, /*hidden argument*/List_1_get_Count_m3499622935_MethodInfo_var);
		V_3 = (bool)((((int32_t)L_10) < ((int32_t)L_12))? 1 : 0);
		bool L_13 = V_3;
		if (L_13)
		{
			goto IL_0005;
		}
	}
	{
		V_2 = (JsonConverter_t4092422604 *)NULL;
		goto IL_0044;
	}

IL_0044:
	{
		JsonConverter_t4092422604 * L_14 = V_2;
		return L_14;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonReaderSettings::.ctor()
extern Il2CppClass* TypeCoercionUtility_t3996825458_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3461543736_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4179343751_MethodInfo_var;
extern const uint32_t JsonReaderSettings__ctor_m3778525097_MetadataUsageId;
extern "C"  void JsonReaderSettings__ctor_m3778525097 (JsonReaderSettings_t1410336530 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonReaderSettings__ctor_m3778525097_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeCoercionUtility_t3996825458 * L_0 = (TypeCoercionUtility_t3996825458 *)il2cpp_codegen_object_new(TypeCoercionUtility_t3996825458_il2cpp_TypeInfo_var);
		TypeCoercionUtility__ctor_m3459939157(L_0, /*hidden argument*/NULL);
		__this->set_Coercion_0(L_0);
		__this->set_allowUnquotedObjectKeys_1((bool)0);
		List_1_t3461543736 * L_1 = (List_1_t3461543736 *)il2cpp_codegen_object_new(List_1_t3461543736_il2cpp_TypeInfo_var);
		List_1__ctor_m4179343751(L_1, /*hidden argument*/List_1__ctor_m4179343751_MethodInfo_var);
		__this->set_converters_4(L_1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonSerializationException::.ctor(System.String)
extern "C"  void JsonSerializationException__ctor_m2868596966 (JsonSerializationException_t3292412897 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		InvalidOperationException__ctor_m2801133788(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String Pathfinding.Serialization.JsonFx.JsonSpecifiedPropertyAttribute::get_SpecifiedProperty()
extern "C"  String_t* JsonSpecifiedPropertyAttribute_get_SpecifiedProperty_m999280689 (JsonSpecifiedPropertyAttribute_t4080780537 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_specifiedProperty_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String Pathfinding.Serialization.JsonFx.JsonSpecifiedPropertyAttribute::GetJsonSpecifiedProperty(System.Reflection.MemberInfo)
extern const Il2CppType* JsonSpecifiedPropertyAttribute_t4080780537_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonSpecifiedPropertyAttribute_t4080780537_il2cpp_TypeInfo_var;
extern const uint32_t JsonSpecifiedPropertyAttribute_GetJsonSpecifiedProperty_m386584320_MetadataUsageId;
extern "C"  String_t* JsonSpecifiedPropertyAttribute_GetJsonSpecifiedProperty_m386584320 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___memberInfo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonSpecifiedPropertyAttribute_GetJsonSpecifiedProperty_m386584320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	JsonSpecifiedPropertyAttribute_t4080780537 * V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	int32_t G_B3_0 = 0;
	{
		MemberInfo_t * L_0 = ___memberInfo0;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		MemberInfo_t * L_1 = ___memberInfo0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JsonSpecifiedPropertyAttribute_t4080780537_0_0_0_var), /*hidden argument*/NULL);
		bool L_3 = Attribute_IsDefined_m1350918959(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 1;
	}

IL_001a:
	{
		V_1 = (bool)G_B3_0;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0023;
		}
	}
	{
		V_2 = (String_t*)NULL;
		goto IL_0042;
	}

IL_0023:
	{
		MemberInfo_t * L_5 = ___memberInfo0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JsonSpecifiedPropertyAttribute_t4080780537_0_0_0_var), /*hidden argument*/NULL);
		Attribute_t542643598 * L_7 = Attribute_GetCustomAttribute_m2157205805(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_0 = ((JsonSpecifiedPropertyAttribute_t4080780537 *)CastclassClass(L_7, JsonSpecifiedPropertyAttribute_t4080780537_il2cpp_TypeInfo_var));
		JsonSpecifiedPropertyAttribute_t4080780537 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = JsonSpecifiedPropertyAttribute_get_SpecifiedProperty_m999280689(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		goto IL_0042;
	}

IL_0042:
	{
		String_t* L_10 = V_2;
		return L_10;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonTypeCoercionException::.ctor(System.String)
extern "C"  void JsonTypeCoercionException__ctor_m695869114 (JsonTypeCoercionException_t1571553219 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		ArgumentException__ctor_m3739475201(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonTypeCoercionException::.ctor(System.String,System.Exception)
extern "C"  void JsonTypeCoercionException__ctor_m1497116290 (JsonTypeCoercionException_t1571553219 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		ArgumentException__ctor_m3553968249(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::.ctor(System.Text.StringBuilder)
extern Il2CppClass* JsonWriterSettings_t2950311970_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter__ctor_m2497554504_MetadataUsageId;
extern "C"  void JsonWriter__ctor_m2497554504 (JsonWriter_t446744171 * __this, StringBuilder_t1221177846 * ___output0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter__ctor_m2497554504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringBuilder_t1221177846 * L_0 = ___output0;
		JsonWriterSettings_t2950311970 * L_1 = (JsonWriterSettings_t2950311970 *)il2cpp_codegen_object_new(JsonWriterSettings_t2950311970_il2cpp_TypeInfo_var);
		JsonWriterSettings__ctor_m1617359135(L_1, /*hidden argument*/NULL);
		JsonWriter__ctor_m4251693306(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::.ctor(System.Text.StringBuilder,Pathfinding.Serialization.JsonFx.JsonWriterSettings)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppClass* StringWriter_t4139609088_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1879432687;
extern Il2CppCodeGenString* _stringLiteral680599737;
extern const uint32_t JsonWriter__ctor_m4251693306_MetadataUsageId;
extern "C"  void JsonWriter__ctor_m4251693306 (JsonWriter_t446744171 * __this, StringBuilder_t1221177846 * ___output0, JsonWriterSettings_t2950311970 * ___settings1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter__ctor_m4251693306_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_0 = ___output0;
		V_0 = (bool)((((Il2CppObject*)(StringBuilder_t1221177846 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		ArgumentNullException_t628810857 * L_2 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_2, _stringLiteral1879432687, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001c:
	{
		JsonWriterSettings_t2950311970 * L_3 = ___settings1;
		V_1 = (bool)((((Il2CppObject*)(JsonWriterSettings_t2950311970 *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0030;
		}
	}
	{
		ArgumentNullException_t628810857 * L_5 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_5, _stringLiteral680599737, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0030:
	{
		StringBuilder_t1221177846 * L_6 = ___output0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_7 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		StringWriter_t4139609088 * L_8 = (StringWriter_t4139609088 *)il2cpp_codegen_object_new(StringWriter_t4139609088_il2cpp_TypeInfo_var);
		StringWriter__ctor_m2262370389(L_8, L_6, L_7, /*hidden argument*/NULL);
		__this->set_Writer_0(L_8);
		JsonWriterSettings_t2950311970 * L_9 = ___settings1;
		__this->set_settings_1(L_9);
		TextWriter_t4027217640 * L_10 = __this->get_Writer_0();
		JsonWriterSettings_t2950311970 * L_11 = __this->get_settings_1();
		NullCheck(L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_NewLine() */, L_11);
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void System.IO.TextWriter::set_NewLine(System.String) */, L_10, L_12);
		return;
	}
}
// Pathfinding.Serialization.JsonFx.JsonWriterSettings Pathfinding.Serialization.JsonFx.JsonWriter::get_Settings()
extern "C"  JsonWriterSettings_t2950311970 * JsonWriter_get_Settings_m3858231505 (JsonWriter_t446744171 * __this, const MethodInfo* method)
{
	JsonWriterSettings_t2950311970 * V_0 = NULL;
	{
		JsonWriterSettings_t2950311970 * L_0 = __this->get_settings_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		JsonWriterSettings_t2950311970 * L_1 = V_0;
		return L_1;
	}
}
// System.String Pathfinding.Serialization.JsonFx.JsonWriter::Serialize(System.Object)
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonWriter_t446744171_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter_Serialize_m1912680015_MetadataUsageId;
extern "C"  String_t* JsonWriter_Serialize_m1912680015 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Serialize_m1912680015_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	JsonWriter_t446744171 * V_1 = NULL;
	String_t* V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringBuilder_t1221177846 * L_1 = V_0;
		JsonWriter_t446744171 * L_2 = (JsonWriter_t446744171 *)il2cpp_codegen_object_new(JsonWriter_t446744171_il2cpp_TypeInfo_var);
		JsonWriter__ctor_m2497554504(L_2, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		JsonWriter_t446744171 * L_3 = V_1;
		Il2CppObject * L_4 = ___value0;
		NullCheck(L_3);
		JsonWriter_Write_m834573621(L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x25, FINALLY_001a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_001a;
	}

FINALLY_001a:
	{ // begin finally (depth: 1)
		{
			JsonWriter_t446744171 * L_5 = V_1;
			if (!L_5)
			{
				goto IL_0024;
			}
		}

IL_001d:
		{
			JsonWriter_t446744171 * L_6 = V_1;
			NullCheck(L_6);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_6);
		}

IL_0024:
		{
			IL2CPP_END_FINALLY(26)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(26)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0025:
	{
		StringBuilder_t1221177846 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_7);
		V_2 = L_8;
		goto IL_002e;
	}

IL_002e:
	{
		String_t* L_9 = V_2;
		return L_9;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Object)
extern "C"  void JsonWriter_Write_m834573621 (JsonWriter_t446744171 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		VirtActionInvoker2< Il2CppObject *, bool >::Invoke(5 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Object,System.Boolean) */, __this, L_0, (bool)0);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Object,System.Boolean)
extern Il2CppClass* IJsonSerializable_t2396137892_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonSerializationException_t3292412897_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16_t4041245914_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* SByte_t454417549_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt16_t986882611_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* Guid_t2533601593_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeSpan_t3430258949_il2cpp_TypeInfo_var;
extern Il2CppClass* Version_t1755874712_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern Il2CppCodeGenString* _stringLiteral504030304;
extern Il2CppCodeGenString* _stringLiteral1708270712;
extern const uint32_t JsonWriter_Write_m1483103282_MetadataUsageId;
extern "C"  void JsonWriter_Write_m1483103282 (JsonWriter_t446744171 * __this, Il2CppObject * ___value0, bool ___isProperty1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m1483103282_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	JsonConverter_t4092422604 * V_1 = NULL;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	bool V_6 = false;
	bool V_7 = false;
	bool V_8 = false;
	bool V_9 = false;
	int32_t V_10 = 0;
	bool V_11 = false;
	bool V_12 = false;
	bool V_13 = false;
	bool V_14 = false;
	bool V_15 = false;
	bool V_16 = false;
	bool V_17 = false;
	bool V_18 = false;
	bool V_19 = false;
	bool V_20 = false;
	bool V_21 = false;
	bool V_22 = false;
	bool V_23 = false;
	bool V_24 = false;
	bool V_25 = false;
	bool V_26 = false;
	bool V_27 = false;
	bool V_28 = false;
	bool V_29 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B3_0 = 0;
	{
		bool L_0 = ___isProperty1;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		JsonWriterSettings_t2950311970 * L_1 = __this->get_settings_1();
		NullCheck(L_1);
		bool L_2 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_PrettyPrint() */, L_1);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 0;
	}

IL_0012:
	{
		V_2 = (bool)G_B3_0;
		bool L_3 = V_2;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		TextWriter_t4027217640 * L_4 = __this->get_Writer_0();
		NullCheck(L_4);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_4, ((int32_t)32));
	}

IL_0026:
	{
		Il2CppObject * L_5 = ___value0;
		V_3 = (bool)((((Il2CppObject*)(Il2CppObject *)L_5) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_6 = V_3;
		if (!L_6)
		{
			goto IL_0045;
		}
	}
	{
		TextWriter_t4027217640 * L_7 = __this->get_Writer_0();
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_7, _stringLiteral1743624307);
		goto IL_05a6;
	}

IL_0045:
	{
		Il2CppObject * L_8 = ___value0;
		V_4 = (bool)((!(((Il2CppObject*)(Il2CppObject *)((Il2CppObject *)IsInst(L_8, IJsonSerializable_t2396137892_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_9 = V_4;
		if (!L_9)
		{
			goto IL_00e0;
		}
	}
	{
	}

IL_0058:
	try
	{ // begin try (depth: 1)
		{
			bool L_10 = ___isProperty1;
			V_5 = L_10;
			bool L_11 = V_5;
			if (!L_11)
			{
				goto IL_00b1;
			}
		}

IL_0060:
		{
			int32_t L_12 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_12+(int32_t)1)));
			int32_t L_13 = __this->get_depth_2();
			JsonWriterSettings_t2950311970 * L_14 = __this->get_settings_1();
			NullCheck(L_14);
			int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_14);
			V_6 = (bool)((((int32_t)L_13) > ((int32_t)L_15))? 1 : 0);
			bool L_16 = V_6;
			if (!L_16)
			{
				goto IL_00a9;
			}
		}

IL_0088:
		{
			JsonWriterSettings_t2950311970 * L_17 = __this->get_settings_1();
			NullCheck(L_17);
			int32_t L_18 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_17);
			int32_t L_19 = L_18;
			Il2CppObject * L_20 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_19);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_21 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral504030304, L_20, /*hidden argument*/NULL);
			JsonSerializationException_t3292412897 * L_22 = (JsonSerializationException_t3292412897 *)il2cpp_codegen_object_new(JsonSerializationException_t3292412897_il2cpp_TypeInfo_var);
			JsonSerializationException__ctor_m2868596966(L_22, L_21, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
		}

IL_00a9:
		{
			VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
		}

IL_00b1:
		{
			Il2CppObject * L_23 = ___value0;
			NullCheck(((Il2CppObject *)Castclass(L_23, IJsonSerializable_t2396137892_il2cpp_TypeInfo_var)));
			InterfaceActionInvoker1< JsonWriter_t446744171 * >::Invoke(0 /* System.Void Pathfinding.Serialization.JsonFx.IJsonSerializable::WriteJson(Pathfinding.Serialization.JsonFx.JsonWriter) */, IJsonSerializable_t2396137892_il2cpp_TypeInfo_var, ((Il2CppObject *)Castclass(L_23, IJsonSerializable_t2396137892_il2cpp_TypeInfo_var)), __this);
			IL2CPP_LEAVE(0xDB, FINALLY_00c1);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00c1;
	}

FINALLY_00c1:
	{ // begin finally (depth: 1)
		{
			bool L_24 = ___isProperty1;
			V_7 = L_24;
			bool L_25 = V_7;
			if (!L_25)
			{
				goto IL_00d9;
			}
		}

IL_00c9:
		{
			int32_t L_26 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_26-(int32_t)1)));
		}

IL_00d9:
		{
			IL2CPP_END_FINALLY(193)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(193)
	{
		IL2CPP_JUMP_TBL(0xDB, IL_00db)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00db:
	{
		goto IL_05a6;
	}

IL_00e0:
	{
		Il2CppObject * L_27 = ___value0;
		V_8 = (bool)((!(((Il2CppObject*)(Enum_t2459695545 *)((Enum_t2459695545 *)IsInstClass(L_27, Enum_t2459695545_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_28 = V_8;
		if (!L_28)
		{
			goto IL_0102;
		}
	}
	{
		Il2CppObject * L_29 = ___value0;
		VirtActionInvoker1< Enum_t2459695545 * >::Invoke(8 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Enum) */, __this, ((Enum_t2459695545 *)CastclassClass(L_29, Enum_t2459695545_il2cpp_TypeInfo_var)));
		goto IL_05a6;
	}

IL_0102:
	{
		Il2CppObject * L_30 = ___value0;
		NullCheck(L_30);
		Type_t * L_31 = Object_GetType_m191970594(L_30, /*hidden argument*/NULL);
		V_0 = L_31;
		JsonWriterSettings_t2950311970 * L_32 = JsonWriter_get_Settings_m3858231505(__this, /*hidden argument*/NULL);
		Type_t * L_33 = V_0;
		NullCheck(L_32);
		JsonConverter_t4092422604 * L_34 = VirtFuncInvoker1< JsonConverter_t4092422604 *, Type_t * >::Invoke(12 /* Pathfinding.Serialization.JsonFx.JsonConverter Pathfinding.Serialization.JsonFx.JsonWriterSettings::GetConverter(System.Type) */, L_32, L_33);
		V_1 = L_34;
		JsonConverter_t4092422604 * L_35 = V_1;
		V_9 = (bool)((!(((Il2CppObject*)(JsonConverter_t4092422604 *)L_35) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_36 = V_9;
		if (!L_36)
		{
			goto IL_0130;
		}
	}
	{
		JsonConverter_t4092422604 * L_37 = V_1;
		Type_t * L_38 = V_0;
		Il2CppObject * L_39 = ___value0;
		NullCheck(L_37);
		JsonConverter_Write_m2427477930(L_37, __this, L_38, L_39, /*hidden argument*/NULL);
		goto IL_05a6;
	}

IL_0130:
	{
		Type_t * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		int32_t L_41 = Type_GetTypeCode_m1044483454(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		V_10 = L_41;
		int32_t L_42 = V_10;
		if (L_42 == 0)
		{
			goto IL_01dc;
		}
		if (L_42 == 1)
		{
			goto IL_02c4;
		}
		if (L_42 == 2)
		{
			goto IL_01dc;
		}
		if (L_42 == 3)
		{
			goto IL_0190;
		}
		if (L_42 == 4)
		{
			goto IL_01b6;
		}
		if (L_42 == 5)
		{
			goto IL_0252;
		}
		if (L_42 == 6)
		{
			goto IL_01a3;
		}
		if (L_42 == 7)
		{
			goto IL_0219;
		}
		if (L_42 == 8)
		{
			goto IL_028b;
		}
		if (L_42 == 9)
		{
			goto IL_022c;
		}
		if (L_42 == 10)
		{
			goto IL_029e;
		}
		if (L_42 == 11)
		{
			goto IL_023f;
		}
		if (L_42 == 12)
		{
			goto IL_02b1;
		}
		if (L_42 == 13)
		{
			goto IL_0265;
		}
		if (L_42 == 14)
		{
			goto IL_0206;
		}
		if (L_42 == 15)
		{
			goto IL_01f3;
		}
		if (L_42 == 16)
		{
			goto IL_01c9;
		}
		if (L_42 == 17)
		{
			goto IL_02c4;
		}
		if (L_42 == 18)
		{
			goto IL_0278;
		}
	}
	{
		goto IL_02c4;
	}

IL_0190:
	{
		Il2CppObject * L_43 = ___value0;
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Boolean) */, __this, ((*(bool*)((bool*)UnBox (L_43, Boolean_t3825574718_il2cpp_TypeInfo_var)))));
		goto IL_05a6;
	}

IL_01a3:
	{
		Il2CppObject * L_44 = ___value0;
		VirtActionInvoker1< uint8_t >::Invoke(11 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Byte) */, __this, ((*(uint8_t*)((uint8_t*)UnBox (L_44, Byte_t3683104436_il2cpp_TypeInfo_var)))));
		goto IL_05a6;
	}

IL_01b6:
	{
		Il2CppObject * L_45 = ___value0;
		VirtActionInvoker1< Il2CppChar >::Invoke(22 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Char) */, __this, ((*(Il2CppChar*)((Il2CppChar*)UnBox (L_45, Char_t3454481338_il2cpp_TypeInfo_var)))));
		goto IL_05a6;
	}

IL_01c9:
	{
		Il2CppObject * L_46 = ___value0;
		VirtActionInvoker1< DateTime_t693205669  >::Invoke(6 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.DateTime) */, __this, ((*(DateTime_t693205669 *)((DateTime_t693205669 *)UnBox (L_46, DateTime_t693205669_il2cpp_TypeInfo_var)))));
		goto IL_05a6;
	}

IL_01dc:
	{
		TextWriter_t4027217640 * L_47 = __this->get_Writer_0();
		NullCheck(L_47);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_47, _stringLiteral1743624307);
		goto IL_05a6;
	}

IL_01f3:
	{
		Il2CppObject * L_48 = ___value0;
		VirtActionInvoker1< Decimal_t724701077  >::Invoke(21 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Decimal) */, __this, ((*(Decimal_t724701077 *)((Decimal_t724701077 *)UnBox (L_48, Decimal_t724701077_il2cpp_TypeInfo_var)))));
		goto IL_05a6;
	}

IL_0206:
	{
		Il2CppObject * L_49 = ___value0;
		VirtActionInvoker1< double >::Invoke(20 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Double) */, __this, ((*(double*)((double*)UnBox (L_49, Double_t4078015681_il2cpp_TypeInfo_var)))));
		goto IL_05a6;
	}

IL_0219:
	{
		Il2CppObject * L_50 = ___value0;
		VirtActionInvoker1< int16_t >::Invoke(13 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Int16) */, __this, ((*(int16_t*)((int16_t*)UnBox (L_50, Int16_t4041245914_il2cpp_TypeInfo_var)))));
		goto IL_05a6;
	}

IL_022c:
	{
		Il2CppObject * L_51 = ___value0;
		VirtActionInvoker1< int32_t >::Invoke(15 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Int32) */, __this, ((*(int32_t*)((int32_t*)UnBox (L_51, Int32_t2071877448_il2cpp_TypeInfo_var)))));
		goto IL_05a6;
	}

IL_023f:
	{
		Il2CppObject * L_52 = ___value0;
		VirtActionInvoker1< int64_t >::Invoke(17 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Int64) */, __this, ((*(int64_t*)((int64_t*)UnBox (L_52, Int64_t909078037_il2cpp_TypeInfo_var)))));
		goto IL_05a6;
	}

IL_0252:
	{
		Il2CppObject * L_53 = ___value0;
		VirtActionInvoker1< int8_t >::Invoke(12 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.SByte) */, __this, ((*(int8_t*)((int8_t*)UnBox (L_53, SByte_t454417549_il2cpp_TypeInfo_var)))));
		goto IL_05a6;
	}

IL_0265:
	{
		Il2CppObject * L_54 = ___value0;
		VirtActionInvoker1< float >::Invoke(19 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Single) */, __this, ((*(float*)((float*)UnBox (L_54, Single_t2076509932_il2cpp_TypeInfo_var)))));
		goto IL_05a6;
	}

IL_0278:
	{
		Il2CppObject * L_55 = ___value0;
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, ((String_t*)CastclassSealed(L_55, String_t_il2cpp_TypeInfo_var)));
		goto IL_05a6;
	}

IL_028b:
	{
		Il2CppObject * L_56 = ___value0;
		VirtActionInvoker1< uint16_t >::Invoke(14 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.UInt16) */, __this, ((*(uint16_t*)((uint16_t*)UnBox (L_56, UInt16_t986882611_il2cpp_TypeInfo_var)))));
		goto IL_05a6;
	}

IL_029e:
	{
		Il2CppObject * L_57 = ___value0;
		VirtActionInvoker1< uint32_t >::Invoke(16 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.UInt32) */, __this, ((*(uint32_t*)((uint32_t*)UnBox (L_57, UInt32_t2149682021_il2cpp_TypeInfo_var)))));
		goto IL_05a6;
	}

IL_02b1:
	{
		Il2CppObject * L_58 = ___value0;
		VirtActionInvoker1< uint64_t >::Invoke(18 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.UInt64) */, __this, ((*(uint64_t*)((uint64_t*)UnBox (L_58, UInt64_t2909196914_il2cpp_TypeInfo_var)))));
		goto IL_05a6;
	}

IL_02c4:
	{
		goto IL_02c7;
	}

IL_02c7:
	{
		Il2CppObject * L_59 = ___value0;
		V_11 = (bool)((!(((Il2CppObject*)(Il2CppObject *)((Il2CppObject *)IsInstSealed(L_59, Guid_t2533601593_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_60 = V_11;
		if (!L_60)
		{
			goto IL_02e9;
		}
	}
	{
		Il2CppObject * L_61 = ___value0;
		VirtActionInvoker1< Guid_t2533601593  >::Invoke(7 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Guid) */, __this, ((*(Guid_t2533601593 *)((Guid_t2533601593 *)UnBox (L_61, Guid_t2533601593_il2cpp_TypeInfo_var)))));
		goto IL_05a6;
	}

IL_02e9:
	{
		Il2CppObject * L_62 = ___value0;
		V_12 = (bool)((!(((Il2CppObject*)(Uri_t19570940 *)((Uri_t19570940 *)IsInstClass(L_62, Uri_t19570940_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_63 = V_12;
		if (!L_63)
		{
			goto IL_030b;
		}
	}
	{
		Il2CppObject * L_64 = ___value0;
		VirtActionInvoker1< Uri_t19570940 * >::Invoke(24 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Uri) */, __this, ((Uri_t19570940 *)CastclassClass(L_64, Uri_t19570940_il2cpp_TypeInfo_var)));
		goto IL_05a6;
	}

IL_030b:
	{
		Il2CppObject * L_65 = ___value0;
		V_13 = (bool)((!(((Il2CppObject*)(Il2CppObject *)((Il2CppObject *)IsInstSealed(L_65, TimeSpan_t3430258949_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_66 = V_13;
		if (!L_66)
		{
			goto IL_032d;
		}
	}
	{
		Il2CppObject * L_67 = ___value0;
		VirtActionInvoker1< TimeSpan_t3430258949  >::Invoke(23 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.TimeSpan) */, __this, ((*(TimeSpan_t3430258949 *)((TimeSpan_t3430258949 *)UnBox (L_67, TimeSpan_t3430258949_il2cpp_TypeInfo_var)))));
		goto IL_05a6;
	}

IL_032d:
	{
		Il2CppObject * L_68 = ___value0;
		V_14 = (bool)((!(((Il2CppObject*)(Version_t1755874712 *)((Version_t1755874712 *)IsInstSealed(L_68, Version_t1755874712_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_69 = V_14;
		if (!L_69)
		{
			goto IL_034f;
		}
	}
	{
		Il2CppObject * L_70 = ___value0;
		VirtActionInvoker1< Version_t1755874712 * >::Invoke(25 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Version) */, __this, ((Version_t1755874712 *)CastclassSealed(L_70, Version_t1755874712_il2cpp_TypeInfo_var)));
		goto IL_05a6;
	}

IL_034f:
	{
		Il2CppObject * L_71 = ___value0;
		V_15 = (bool)((!(((Il2CppObject*)(Il2CppObject *)((Il2CppObject *)IsInst(L_71, IDictionary_t596158605_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_72 = V_15;
		if (!L_72)
		{
			goto IL_03ea;
		}
	}
	{
	}

IL_0362:
	try
	{ // begin try (depth: 1)
		{
			bool L_73 = ___isProperty1;
			V_16 = L_73;
			bool L_74 = V_16;
			if (!L_74)
			{
				goto IL_03bb;
			}
		}

IL_036a:
		{
			int32_t L_75 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_75+(int32_t)1)));
			int32_t L_76 = __this->get_depth_2();
			JsonWriterSettings_t2950311970 * L_77 = __this->get_settings_1();
			NullCheck(L_77);
			int32_t L_78 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_77);
			V_17 = (bool)((((int32_t)L_76) > ((int32_t)L_78))? 1 : 0);
			bool L_79 = V_17;
			if (!L_79)
			{
				goto IL_03b3;
			}
		}

IL_0392:
		{
			JsonWriterSettings_t2950311970 * L_80 = __this->get_settings_1();
			NullCheck(L_80);
			int32_t L_81 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_80);
			int32_t L_82 = L_81;
			Il2CppObject * L_83 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_82);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_84 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral504030304, L_83, /*hidden argument*/NULL);
			JsonSerializationException_t3292412897 * L_85 = (JsonSerializationException_t3292412897 *)il2cpp_codegen_object_new(JsonSerializationException_t3292412897_il2cpp_TypeInfo_var);
			JsonSerializationException__ctor_m2868596966(L_85, L_84, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_85);
		}

IL_03b3:
		{
			VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
		}

IL_03bb:
		{
			Il2CppObject * L_86 = ___value0;
			VirtActionInvoker1< Il2CppObject * >::Invoke(28 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObject(System.Collections.IDictionary) */, __this, ((Il2CppObject *)Castclass(L_86, IDictionary_t596158605_il2cpp_TypeInfo_var)));
			IL2CPP_LEAVE(0x3E5, FINALLY_03cb);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_03cb;
	}

FINALLY_03cb:
	{ // begin finally (depth: 1)
		{
			bool L_87 = ___isProperty1;
			V_18 = L_87;
			bool L_88 = V_18;
			if (!L_88)
			{
				goto IL_03e3;
			}
		}

IL_03d3:
		{
			int32_t L_89 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_89-(int32_t)1)));
		}

IL_03e3:
		{
			IL2CPP_END_FINALLY(971)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(971)
	{
		IL2CPP_JUMP_TBL(0x3E5, IL_03e5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_03e5:
	{
		goto IL_05a6;
	}

IL_03ea:
	{
		Type_t * L_90 = V_0;
		NullCheck(L_90);
		Type_t * L_91 = VirtFuncInvoker2< Type_t *, String_t*, bool >::Invoke(41 /* System.Type System.Type::GetInterface(System.String,System.Boolean) */, L_90, _stringLiteral1708270712, (bool)0);
		V_19 = (bool)((!(((Il2CppObject*)(Type_t *)L_91) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_92 = V_19;
		if (!L_92)
		{
			goto IL_048b;
		}
	}
	{
	}

IL_0403:
	try
	{ // begin try (depth: 1)
		{
			bool L_93 = ___isProperty1;
			V_20 = L_93;
			bool L_94 = V_20;
			if (!L_94)
			{
				goto IL_045c;
			}
		}

IL_040b:
		{
			int32_t L_95 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_95+(int32_t)1)));
			int32_t L_96 = __this->get_depth_2();
			JsonWriterSettings_t2950311970 * L_97 = __this->get_settings_1();
			NullCheck(L_97);
			int32_t L_98 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_97);
			V_21 = (bool)((((int32_t)L_96) > ((int32_t)L_98))? 1 : 0);
			bool L_99 = V_21;
			if (!L_99)
			{
				goto IL_0454;
			}
		}

IL_0433:
		{
			JsonWriterSettings_t2950311970 * L_100 = __this->get_settings_1();
			NullCheck(L_100);
			int32_t L_101 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_100);
			int32_t L_102 = L_101;
			Il2CppObject * L_103 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_102);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_104 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral504030304, L_103, /*hidden argument*/NULL);
			JsonSerializationException_t3292412897 * L_105 = (JsonSerializationException_t3292412897 *)il2cpp_codegen_object_new(JsonSerializationException_t3292412897_il2cpp_TypeInfo_var);
			JsonSerializationException__ctor_m2868596966(L_105, L_104, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_105);
		}

IL_0454:
		{
			VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
		}

IL_045c:
		{
			Il2CppObject * L_106 = ___value0;
			VirtActionInvoker1< Il2CppObject * >::Invoke(29 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteDictionary(System.Collections.IEnumerable) */, __this, ((Il2CppObject *)Castclass(L_106, IEnumerable_t2911409499_il2cpp_TypeInfo_var)));
			IL2CPP_LEAVE(0x486, FINALLY_046c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_046c;
	}

FINALLY_046c:
	{ // begin finally (depth: 1)
		{
			bool L_107 = ___isProperty1;
			V_22 = L_107;
			bool L_108 = V_22;
			if (!L_108)
			{
				goto IL_0484;
			}
		}

IL_0474:
		{
			int32_t L_109 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_109-(int32_t)1)));
		}

IL_0484:
		{
			IL2CPP_END_FINALLY(1132)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1132)
	{
		IL2CPP_JUMP_TBL(0x486, IL_0486)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0486:
	{
		goto IL_05a6;
	}

IL_048b:
	{
		Il2CppObject * L_110 = ___value0;
		V_23 = (bool)((!(((Il2CppObject*)(Il2CppObject *)((Il2CppObject *)IsInst(L_110, IEnumerable_t2911409499_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_111 = V_23;
		if (!L_111)
		{
			goto IL_0526;
		}
	}
	{
	}

IL_049e:
	try
	{ // begin try (depth: 1)
		{
			bool L_112 = ___isProperty1;
			V_24 = L_112;
			bool L_113 = V_24;
			if (!L_113)
			{
				goto IL_04f7;
			}
		}

IL_04a6:
		{
			int32_t L_114 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_114+(int32_t)1)));
			int32_t L_115 = __this->get_depth_2();
			JsonWriterSettings_t2950311970 * L_116 = __this->get_settings_1();
			NullCheck(L_116);
			int32_t L_117 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_116);
			V_25 = (bool)((((int32_t)L_115) > ((int32_t)L_117))? 1 : 0);
			bool L_118 = V_25;
			if (!L_118)
			{
				goto IL_04ef;
			}
		}

IL_04ce:
		{
			JsonWriterSettings_t2950311970 * L_119 = __this->get_settings_1();
			NullCheck(L_119);
			int32_t L_120 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_119);
			int32_t L_121 = L_120;
			Il2CppObject * L_122 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_121);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_123 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral504030304, L_122, /*hidden argument*/NULL);
			JsonSerializationException_t3292412897 * L_124 = (JsonSerializationException_t3292412897 *)il2cpp_codegen_object_new(JsonSerializationException_t3292412897_il2cpp_TypeInfo_var);
			JsonSerializationException__ctor_m2868596966(L_124, L_123, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_124);
		}

IL_04ef:
		{
			VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
		}

IL_04f7:
		{
			Il2CppObject * L_125 = ___value0;
			VirtActionInvoker1< Il2CppObject * >::Invoke(26 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteArray(System.Collections.IEnumerable) */, __this, ((Il2CppObject *)Castclass(L_125, IEnumerable_t2911409499_il2cpp_TypeInfo_var)));
			IL2CPP_LEAVE(0x521, FINALLY_0507);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0507;
	}

FINALLY_0507:
	{ // begin finally (depth: 1)
		{
			bool L_126 = ___isProperty1;
			V_26 = L_126;
			bool L_127 = V_26;
			if (!L_127)
			{
				goto IL_051f;
			}
		}

IL_050f:
		{
			int32_t L_128 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_128-(int32_t)1)));
		}

IL_051f:
		{
			IL2CPP_END_FINALLY(1287)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1287)
	{
		IL2CPP_JUMP_TBL(0x521, IL_0521)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0521:
	{
		goto IL_05a6;
	}

IL_0526:
	{
	}

IL_0527:
	try
	{ // begin try (depth: 1)
		{
			bool L_129 = ___isProperty1;
			V_27 = L_129;
			bool L_130 = V_27;
			if (!L_130)
			{
				goto IL_0580;
			}
		}

IL_052f:
		{
			int32_t L_131 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_131+(int32_t)1)));
			int32_t L_132 = __this->get_depth_2();
			JsonWriterSettings_t2950311970 * L_133 = __this->get_settings_1();
			NullCheck(L_133);
			int32_t L_134 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_133);
			V_28 = (bool)((((int32_t)L_132) > ((int32_t)L_134))? 1 : 0);
			bool L_135 = V_28;
			if (!L_135)
			{
				goto IL_0578;
			}
		}

IL_0557:
		{
			JsonWriterSettings_t2950311970 * L_136 = __this->get_settings_1();
			NullCheck(L_136);
			int32_t L_137 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_136);
			int32_t L_138 = L_137;
			Il2CppObject * L_139 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_138);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_140 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral504030304, L_139, /*hidden argument*/NULL);
			JsonSerializationException_t3292412897 * L_141 = (JsonSerializationException_t3292412897 *)il2cpp_codegen_object_new(JsonSerializationException_t3292412897_il2cpp_TypeInfo_var);
			JsonSerializationException__ctor_m2868596966(L_141, L_140, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_141);
		}

IL_0578:
		{
			VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
		}

IL_0580:
		{
			Il2CppObject * L_142 = ___value0;
			Type_t * L_143 = V_0;
			VirtActionInvoker2< Il2CppObject *, Type_t * >::Invoke(32 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObject(System.Object,System.Type) */, __this, L_142, L_143);
			IL2CPP_LEAVE(0x5A6, FINALLY_058c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_058c;
	}

FINALLY_058c:
	{ // begin finally (depth: 1)
		{
			bool L_144 = ___isProperty1;
			V_29 = L_144;
			bool L_145 = V_29;
			if (!L_145)
			{
				goto IL_05a4;
			}
		}

IL_0594:
		{
			int32_t L_146 = __this->get_depth_2();
			__this->set_depth_2(((int32_t)((int32_t)L_146-(int32_t)1)));
		}

IL_05a4:
		{
			IL2CPP_END_FINALLY(1420)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1420)
	{
		IL2CPP_JUMP_TBL(0x5A6, IL_05a6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_05a6:
	{
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.DateTime)
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* WriteDelegate_1_Invoke_m2787355611_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4206842789;
extern Il2CppCodeGenString* _stringLiteral8113143;
extern const uint32_t JsonWriter_Write_m2451896681_MetadataUsageId;
extern "C"  void JsonWriter_Write_m2451896681 (JsonWriter_t446744171 * __this, DateTime_t693205669  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m2451896681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		JsonWriterSettings_t2950311970 * L_0 = __this->get_settings_1();
		NullCheck(L_0);
		WriteDelegate_1_t415313529 * L_1 = VirtFuncInvoker0< WriteDelegate_1_t415313529 * >::Invoke(10 /* Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime> Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DateTimeSerializer() */, L_0);
		V_0 = (bool)((!(((Il2CppObject*)(WriteDelegate_1_t415313529 *)L_1) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		JsonWriterSettings_t2950311970 * L_3 = __this->get_settings_1();
		NullCheck(L_3);
		WriteDelegate_1_t415313529 * L_4 = VirtFuncInvoker0< WriteDelegate_1_t415313529 * >::Invoke(10 /* Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime> Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DateTimeSerializer() */, L_3);
		DateTime_t693205669  L_5 = ___value0;
		NullCheck(L_4);
		WriteDelegate_1_Invoke_m2787355611(L_4, __this, L_5, /*hidden argument*/WriteDelegate_1_Invoke_m2787355611_MethodInfo_var);
		goto IL_007d;
	}

IL_0029:
	{
		int32_t L_6 = DateTime_get_Kind_m1331920314((&___value0), /*hidden argument*/NULL);
		V_1 = L_6;
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_0049;
		}
	}
	{
		goto IL_0037;
	}

IL_0037:
	{
		int32_t L_8 = V_1;
		if ((((int32_t)L_8) == ((int32_t)2)))
		{
			goto IL_003d;
		}
	}
	{
		goto IL_0063;
	}

IL_003d:
	{
		DateTime_t693205669  L_9 = DateTime_ToUniversalTime_m1815024752((&___value0), /*hidden argument*/NULL);
		___value0 = L_9;
		goto IL_0049;
	}

IL_0049:
	{
		DateTime_t693205669  L_10 = ___value0;
		DateTime_t693205669  L_11 = L_10;
		Il2CppObject * L_12 = Box(DateTime_t693205669_il2cpp_TypeInfo_var, &L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral4206842789, L_12, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_13);
		goto IL_007d;
	}

IL_0063:
	{
		DateTime_t693205669  L_14 = ___value0;
		DateTime_t693205669  L_15 = L_14;
		Il2CppObject * L_16 = Box(DateTime_t693205669_il2cpp_TypeInfo_var, &L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral8113143, L_16, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_17);
		goto IL_007d;
	}

IL_007d:
	{
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Guid)
extern Il2CppCodeGenString* _stringLiteral372029402;
extern const uint32_t JsonWriter_Write_m377216803_MetadataUsageId;
extern "C"  void JsonWriter_Write_m377216803 (JsonWriter_t446744171 * __this, Guid_t2533601593  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m377216803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = Guid_ToString_m51661589((&___value0), _stringLiteral372029402, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_0);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Enum)
extern const Il2CppType* FlagsAttribute_t859561169_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029372;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern const uint32_t JsonWriter_Write_m4262245103_MetadataUsageId;
extern "C"  void JsonWriter_Write_m4262245103 (JsonWriter_t446744171 * __this, Enum_t2459695545 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m4262245103_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Type_t * V_1 = NULL;
	bool V_2 = false;
	EnumU5BU5D_t975156036* V_3 = NULL;
	StringU5BU5D_t1642385972* V_4 = NULL;
	int32_t V_5 = 0;
	bool V_6 = false;
	bool V_7 = false;
	bool V_8 = false;
	int32_t G_B3_0 = 0;
	{
		V_0 = (String_t*)NULL;
		Enum_t2459695545 * L_0 = ___value0;
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m191970594(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		Type_t * L_2 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(FlagsAttribute_t859561169_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_4 = VirtFuncInvoker2< bool, Type_t *, bool >::Invoke(11 /* System.Boolean System.Reflection.MemberInfo::IsDefined(System.Type,System.Boolean) */, L_2, L_3, (bool)1);
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		Type_t * L_5 = V_1;
		Enum_t2459695545 * L_6 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		bool L_7 = Enum_IsDefined_m92789062(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_7) == ((int32_t)0))? 1 : 0);
		goto IL_002a;
	}

IL_0029:
	{
		G_B3_0 = 0;
	}

IL_002a:
	{
		V_2 = (bool)G_B3_0;
		bool L_8 = V_2;
		if (!L_8)
		{
			goto IL_009e;
		}
	}
	{
		Type_t * L_9 = V_1;
		Enum_t2459695545 * L_10 = ___value0;
		EnumU5BU5D_t975156036* L_11 = JsonWriter_GetFlagList_m2306565768(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		EnumU5BU5D_t975156036* L_12 = V_3;
		NullCheck(L_12);
		V_4 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length))))));
		V_5 = 0;
		goto IL_0081;
	}

IL_0046:
	{
		StringU5BU5D_t1642385972* L_13 = V_4;
		int32_t L_14 = V_5;
		EnumU5BU5D_t975156036* L_15 = V_3;
		int32_t L_16 = V_5;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		Enum_t2459695545 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		String_t* L_19 = JsonNameAttribute_GetJsonName_m3020263406(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_19);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (String_t*)L_19);
		StringU5BU5D_t1642385972* L_20 = V_4;
		int32_t L_21 = V_5;
		NullCheck(L_20);
		int32_t L_22 = L_21;
		String_t* L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_24 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		V_6 = L_24;
		bool L_25 = V_6;
		if (!L_25)
		{
			goto IL_007a;
		}
	}
	{
		StringU5BU5D_t1642385972* L_26 = V_4;
		int32_t L_27 = V_5;
		EnumU5BU5D_t975156036* L_28 = V_3;
		int32_t L_29 = V_5;
		NullCheck(L_28);
		int32_t L_30 = L_29;
		Enum_t2459695545 * L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck(L_31);
		String_t* L_32 = Enum_ToString_m947578005(L_31, _stringLiteral372029372, /*hidden argument*/NULL);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_32);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(L_27), (String_t*)L_32);
	}

IL_007a:
	{
		int32_t L_33 = V_5;
		V_5 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_0081:
	{
		int32_t L_34 = V_5;
		EnumU5BU5D_t975156036* L_35 = V_3;
		NullCheck(L_35);
		V_7 = (bool)((((int32_t)L_34) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_35)->max_length))))))? 1 : 0);
		bool L_36 = V_7;
		if (L_36)
		{
			goto IL_0046;
		}
	}
	{
		StringU5BU5D_t1642385972* L_37 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_38 = String_Join_m1966872927(NULL /*static, unused*/, _stringLiteral811305474, L_37, /*hidden argument*/NULL);
		V_0 = L_38;
		goto IL_00c1;
	}

IL_009e:
	{
		Enum_t2459695545 * L_39 = ___value0;
		String_t* L_40 = JsonNameAttribute_GetJsonName_m3020263406(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		V_0 = L_40;
		String_t* L_41 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_42 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		V_8 = L_42;
		bool L_43 = V_8;
		if (!L_43)
		{
			goto IL_00c0;
		}
	}
	{
		Enum_t2459695545 * L_44 = ___value0;
		NullCheck(L_44);
		String_t* L_45 = Enum_ToString_m947578005(L_44, _stringLiteral372029372, /*hidden argument*/NULL);
		V_0 = L_45;
	}

IL_00c0:
	{
	}

IL_00c1:
	{
		String_t* L_46 = V_0;
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_46);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String)
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern Il2CppCodeGenString* _stringLiteral1093630588;
extern Il2CppCodeGenString* _stringLiteral3419229416;
extern Il2CppCodeGenString* _stringLiteral3062999056;
extern Il2CppCodeGenString* _stringLiteral381169868;
extern Il2CppCodeGenString* _stringLiteral3869568110;
extern Il2CppCodeGenString* _stringLiteral2303484169;
extern Il2CppCodeGenString* _stringLiteral2424443698;
extern const uint32_t JsonWriter_Write_m1445053257_MetadataUsageId;
extern "C"  void JsonWriter_Write_m1445053257 (JsonWriter_t446744171 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m1445053257_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	Il2CppChar V_4 = 0x0;
	bool V_5 = false;
	bool V_6 = false;
	Il2CppChar V_7 = 0x0;
	int32_t V_8 = 0;
	bool V_9 = false;
	bool V_10 = false;
	int32_t G_B9_0 = 0;
	{
		String_t* L_0 = ___value0;
		V_2 = (bool)((((Il2CppObject*)(String_t*)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_1 = V_2;
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		TextWriter_t4027217640 * L_2 = __this->get_Writer_0();
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_2, _stringLiteral1743624307);
		goto IL_01d3;
	}

IL_0020:
	{
		V_0 = 0;
		String_t* L_3 = ___value0;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m1606060069(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		TextWriter_t4027217640 * L_5 = __this->get_Writer_0();
		NullCheck(L_5);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_5, ((int32_t)34));
		int32_t L_6 = V_0;
		V_3 = L_6;
		goto IL_0196;
	}

IL_003e:
	{
		String_t* L_7 = ___value0;
		int32_t L_8 = V_3;
		NullCheck(L_7);
		Il2CppChar L_9 = String_get_Chars_m4230566705(L_7, L_8, /*hidden argument*/NULL);
		V_4 = L_9;
		Il2CppChar L_10 = V_4;
		if ((((int32_t)L_10) <= ((int32_t)((int32_t)31))))
		{
			goto IL_0068;
		}
	}
	{
		Il2CppChar L_11 = V_4;
		if ((((int32_t)L_11) >= ((int32_t)((int32_t)127))))
		{
			goto IL_0068;
		}
	}
	{
		Il2CppChar L_12 = V_4;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)60))))
		{
			goto IL_0068;
		}
	}
	{
		Il2CppChar L_13 = V_4;
		if ((((int32_t)L_13) == ((int32_t)((int32_t)34))))
		{
			goto IL_0068;
		}
	}
	{
		Il2CppChar L_14 = V_4;
		G_B9_0 = ((((int32_t)L_14) == ((int32_t)((int32_t)92)))? 1 : 0);
		goto IL_0069;
	}

IL_0068:
	{
		G_B9_0 = 1;
	}

IL_0069:
	{
		V_5 = (bool)G_B9_0;
		bool L_15 = V_5;
		if (!L_15)
		{
			goto IL_0191;
		}
	}
	{
		int32_t L_16 = V_3;
		int32_t L_17 = V_0;
		V_6 = (bool)((((int32_t)L_16) > ((int32_t)L_17))? 1 : 0);
		bool L_18 = V_6;
		if (!L_18)
		{
			goto IL_0095;
		}
	}
	{
		TextWriter_t4027217640 * L_19 = __this->get_Writer_0();
		String_t* L_20 = ___value0;
		int32_t L_21 = V_0;
		int32_t L_22 = V_3;
		int32_t L_23 = V_0;
		NullCheck(L_20);
		String_t* L_24 = String_Substring_m12482732(L_20, L_21, ((int32_t)((int32_t)L_22-(int32_t)L_23)), /*hidden argument*/NULL);
		NullCheck(L_19);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_19, L_24);
	}

IL_0095:
	{
		int32_t L_25 = V_3;
		V_0 = ((int32_t)((int32_t)L_25+(int32_t)1));
		Il2CppChar L_26 = V_4;
		V_7 = L_26;
		Il2CppChar L_27 = V_7;
		if (((int32_t)((int32_t)L_27-(int32_t)8)) == 0)
		{
			goto IL_00f5;
		}
		if (((int32_t)((int32_t)L_27-(int32_t)8)) == 1)
		{
			goto IL_0148;
		}
		if (((int32_t)((int32_t)L_27-(int32_t)8)) == 2)
		{
			goto IL_0120;
		}
		if (((int32_t)((int32_t)L_27-(int32_t)8)) == 3)
		{
			goto IL_015c;
		}
		if (((int32_t)((int32_t)L_27-(int32_t)8)) == 4)
		{
			goto IL_010c;
		}
		if (((int32_t)((int32_t)L_27-(int32_t)8)) == 5)
		{
			goto IL_0134;
		}
	}
	{
		goto IL_00c0;
	}

IL_00c0:
	{
		Il2CppChar L_28 = V_7;
		if ((((int32_t)L_28) == ((int32_t)((int32_t)34))))
		{
			goto IL_00d3;
		}
	}
	{
		goto IL_00c8;
	}

IL_00c8:
	{
		Il2CppChar L_29 = V_7;
		if ((((int32_t)L_29) == ((int32_t)((int32_t)92))))
		{
			goto IL_00d3;
		}
	}
	{
		goto IL_015c;
	}

IL_00d3:
	{
		TextWriter_t4027217640 * L_30 = __this->get_Writer_0();
		NullCheck(L_30);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_30, ((int32_t)92));
		TextWriter_t4027217640 * L_31 = __this->get_Writer_0();
		Il2CppChar L_32 = V_4;
		NullCheck(L_31);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_31, L_32);
		goto IL_0192;
	}

IL_00f5:
	{
		TextWriter_t4027217640 * L_33 = __this->get_Writer_0();
		NullCheck(L_33);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_33, _stringLiteral1093630588);
		goto IL_0192;
	}

IL_010c:
	{
		TextWriter_t4027217640 * L_34 = __this->get_Writer_0();
		NullCheck(L_34);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_34, _stringLiteral3419229416);
		goto IL_0192;
	}

IL_0120:
	{
		TextWriter_t4027217640 * L_35 = __this->get_Writer_0();
		NullCheck(L_35);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_35, _stringLiteral3062999056);
		goto IL_0192;
	}

IL_0134:
	{
		TextWriter_t4027217640 * L_36 = __this->get_Writer_0();
		NullCheck(L_36);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_36, _stringLiteral381169868);
		goto IL_0192;
	}

IL_0148:
	{
		TextWriter_t4027217640 * L_37 = __this->get_Writer_0();
		NullCheck(L_37);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_37, _stringLiteral3869568110);
		goto IL_0192;
	}

IL_015c:
	{
		TextWriter_t4027217640 * L_38 = __this->get_Writer_0();
		NullCheck(L_38);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_38, _stringLiteral2303484169);
		TextWriter_t4027217640 * L_39 = __this->get_Writer_0();
		String_t* L_40 = ___value0;
		int32_t L_41 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		int32_t L_42 = Char_ConvertToUtf32_m2805463614(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/NULL);
		V_8 = L_42;
		String_t* L_43 = Int32_ToString_m1064459878((&V_8), _stringLiteral2424443698, /*hidden argument*/NULL);
		NullCheck(L_39);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_39, L_43);
		goto IL_0192;
	}

IL_0191:
	{
	}

IL_0192:
	{
		int32_t L_44 = V_3;
		V_3 = ((int32_t)((int32_t)L_44+(int32_t)1));
	}

IL_0196:
	{
		int32_t L_45 = V_3;
		int32_t L_46 = V_1;
		V_9 = (bool)((((int32_t)L_45) < ((int32_t)L_46))? 1 : 0);
		bool L_47 = V_9;
		if (L_47)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_48 = V_1;
		int32_t L_49 = V_0;
		V_10 = (bool)((((int32_t)L_48) > ((int32_t)L_49))? 1 : 0);
		bool L_50 = V_10;
		if (!L_50)
		{
			goto IL_01c5;
		}
	}
	{
		TextWriter_t4027217640 * L_51 = __this->get_Writer_0();
		String_t* L_52 = ___value0;
		int32_t L_53 = V_0;
		int32_t L_54 = V_1;
		int32_t L_55 = V_0;
		NullCheck(L_52);
		String_t* L_56 = String_Substring_m12482732(L_52, L_53, ((int32_t)((int32_t)L_54-(int32_t)L_55)), /*hidden argument*/NULL);
		NullCheck(L_51);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_51, L_56);
	}

IL_01c5:
	{
		TextWriter_t4027217640 * L_57 = __this->get_Writer_0();
		NullCheck(L_57);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_57, ((int32_t)34));
	}

IL_01d3:
	{
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Boolean)
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern const uint32_t JsonWriter_Write_m1363201044_MetadataUsageId;
extern "C"  void JsonWriter_Write_m1363201044 (JsonWriter_t446744171 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m1363201044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TextWriter_t4027217640 * G_B2_0 = NULL;
	TextWriter_t4027217640 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	TextWriter_t4027217640 * G_B3_1 = NULL;
	{
		TextWriter_t4027217640 * L_0 = __this->get_Writer_0();
		bool L_1 = ___value0;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0011;
		}
	}
	{
		G_B3_0 = _stringLiteral2609877245;
		G_B3_1 = G_B1_0;
		goto IL_0016;
	}

IL_0011:
	{
		G_B3_0 = _stringLiteral3323263070;
		G_B3_1 = G_B2_0;
	}

IL_0016:
	{
		NullCheck(G_B3_1);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, G_B3_1, G_B3_0);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Byte)
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029371;
extern const uint32_t JsonWriter_Write_m1734869828_MetadataUsageId;
extern "C"  void JsonWriter_Write_m1734869828 (JsonWriter_t446744171 * __this, uint8_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m1734869828_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextWriter_t4027217640 * L_0 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_1 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = Byte_ToString_m3956930818((&___value0), _stringLiteral372029371, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_0, L_2);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.SByte)
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029371;
extern const uint32_t JsonWriter_Write_m3502806265_MetadataUsageId;
extern "C"  void JsonWriter_Write_m3502806265 (JsonWriter_t446744171 * __this, int8_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m3502806265_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextWriter_t4027217640 * L_0 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_1 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = SByte_ToString_m353340795((&___value0), _stringLiteral372029371, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_0, L_2);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Int16)
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029371;
extern const uint32_t JsonWriter_Write_m2352567266_MetadataUsageId;
extern "C"  void JsonWriter_Write_m2352567266 (JsonWriter_t446744171 * __this, int16_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m2352567266_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextWriter_t4027217640 * L_0 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_1 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = Int16_ToString_m4099416068((&___value0), _stringLiteral372029371, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_0, L_2);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.UInt16)
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029371;
extern const uint32_t JsonWriter_Write_m1799894225_MetadataUsageId;
extern "C"  void JsonWriter_Write_m1799894225 (JsonWriter_t446744171 * __this, uint16_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m1799894225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextWriter_t4027217640 * L_0 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_1 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = UInt16_ToString_m2458826323((&___value0), _stringLiteral372029371, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_0, L_2);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Int32)
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029371;
extern const uint32_t JsonWriter_Write_m2070242268_MetadataUsageId;
extern "C"  void JsonWriter_Write_m2070242268 (JsonWriter_t446744171 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m2070242268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextWriter_t4027217640 * L_0 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_1 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = Int32_ToString_m1128850770((&___value0), _stringLiteral372029371, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_0, L_2);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.UInt32)
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029371;
extern const uint32_t JsonWriter_Write_m4125492987_MetadataUsageId;
extern "C"  void JsonWriter_Write_m4125492987 (JsonWriter_t446744171 * __this, uint32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m4125492987_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		uint32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t724701077_il2cpp_TypeInfo_var);
		Decimal_t724701077  L_1 = Decimal_op_Implicit_m4246329390(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		bool L_2 = VirtFuncInvoker1< bool, Decimal_t724701077  >::Invoke(36 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriter::InvalidIeee754(System.Decimal) */, __this, L_1);
		V_0 = L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_4 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = UInt32_ToString_m85246525((&___value0), _stringLiteral372029371, L_4, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_5);
		goto IL_0049;
	}

IL_002c:
	{
		TextWriter_t4027217640 * L_6 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_7 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = UInt32_ToString_m85246525((&___value0), _stringLiteral372029371, L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_6, L_8);
	}

IL_0049:
	{
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Int64)
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029371;
extern const uint32_t JsonWriter_Write_m2493729765_MetadataUsageId;
extern "C"  void JsonWriter_Write_m2493729765 (JsonWriter_t446744171 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m2493729765_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		int64_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t724701077_il2cpp_TypeInfo_var);
		Decimal_t724701077  L_1 = Decimal_op_Implicit_m2149927308(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		bool L_2 = VirtFuncInvoker1< bool, Decimal_t724701077  >::Invoke(36 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriter::InvalidIeee754(System.Decimal) */, __this, L_1);
		V_0 = L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_4 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = Int64_ToString_m1544341651((&___value0), _stringLiteral372029371, L_4, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_5);
		goto IL_0049;
	}

IL_002c:
	{
		TextWriter_t4027217640 * L_6 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_7 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = Int64_ToString_m1544341651((&___value0), _stringLiteral372029371, L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_6, L_8);
	}

IL_0049:
	{
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.UInt64)
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029371;
extern const uint32_t JsonWriter_Write_m2962693800_MetadataUsageId;
extern "C"  void JsonWriter_Write_m2962693800 (JsonWriter_t446744171 * __this, uint64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m2962693800_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		uint64_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t724701077_il2cpp_TypeInfo_var);
		Decimal_t724701077  L_1 = Decimal_op_Implicit_m2135798419(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		bool L_2 = VirtFuncInvoker1< bool, Decimal_t724701077  >::Invoke(36 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriter::InvalidIeee754(System.Decimal) */, __this, L_1);
		V_0 = L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_4 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = UInt64_ToString_m308141014((&___value0), _stringLiteral372029371, L_4, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_5);
		goto IL_0049;
	}

IL_002c:
	{
		TextWriter_t4027217640 * L_6 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_7 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = UInt64_ToString_m308141014((&___value0), _stringLiteral372029371, L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_6, L_8);
	}

IL_0049:
	{
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Single)
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern Il2CppCodeGenString* _stringLiteral372029392;
extern const uint32_t JsonWriter_Write_m751860230_MetadataUsageId;
extern "C"  void JsonWriter_Write_m751860230 (JsonWriter_t446744171 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m751860230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		float L_0 = ___value0;
		bool L_1 = Single_IsNaN_m2349591895(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		float L_2 = ___value0;
		bool L_3 = Single_IsInfinity_m3331110346(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 1;
	}

IL_0012:
	{
		V_0 = (bool)G_B3_0;
		bool L_4 = V_0;
		if (!L_4)
		{
			goto IL_002b;
		}
	}
	{
		TextWriter_t4027217640 * L_5 = __this->get_Writer_0();
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_5, _stringLiteral1743624307);
		goto IL_004a;
	}

IL_002b:
	{
		TextWriter_t4027217640 * L_6 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_7 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = Single_ToString_m3667608664((&___value0), _stringLiteral372029392, L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_6, L_8);
	}

IL_004a:
	{
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Double)
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern Il2CppCodeGenString* _stringLiteral372029392;
extern const uint32_t JsonWriter_Write_m3571657501_MetadataUsageId;
extern "C"  void JsonWriter_Write_m3571657501 (JsonWriter_t446744171 * __this, double ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m3571657501_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		double L_0 = ___value0;
		bool L_1 = Double_IsNaN_m2289494211(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		double L_2 = ___value0;
		bool L_3 = Double_IsInfinity_m1190290474(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 1;
	}

IL_0012:
	{
		V_0 = (bool)G_B3_0;
		bool L_4 = V_0;
		if (!L_4)
		{
			goto IL_002b;
		}
	}
	{
		TextWriter_t4027217640 * L_5 = __this->get_Writer_0();
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_5, _stringLiteral1743624307);
		goto IL_004a;
	}

IL_002b:
	{
		TextWriter_t4027217640 * L_6 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_7 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = Double_ToString_m1474956491((&___value0), _stringLiteral372029392, L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_6, L_8);
	}

IL_004a:
	{
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Decimal)
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029371;
extern const uint32_t JsonWriter_Write_m432403331_MetadataUsageId;
extern "C"  void JsonWriter_Write_m432403331 (JsonWriter_t446744171 * __this, Decimal_t724701077  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m432403331_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Decimal_t724701077  L_0 = ___value0;
		bool L_1 = VirtFuncInvoker1< bool, Decimal_t724701077  >::Invoke(36 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriter::InvalidIeee754(System.Decimal) */, __this, L_0);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_3 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_4 = Decimal_ToString_m541380585((&___value0), _stringLiteral372029371, L_3, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_4);
		goto IL_0044;
	}

IL_0027:
	{
		TextWriter_t4027217640 * L_5 = __this->get_Writer_0();
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_6 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_7 = Decimal_ToString_m541380585((&___value0), _stringLiteral372029371, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_5, L_7);
	}

IL_0044:
	{
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Char)
extern "C"  void JsonWriter_Write_m2642460430 (JsonWriter_t446744171 * __this, Il2CppChar ___value0, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = ___value0;
		String_t* L_1 = String_CreateString_m2556700934(NULL, L_0, 1, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_1);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.TimeSpan)
extern "C"  void JsonWriter_Write_m1285670029 (JsonWriter_t446744171 * __this, TimeSpan_t3430258949  ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = TimeSpan_get_Ticks_m2285358246((&___value0), /*hidden argument*/NULL);
		VirtActionInvoker1< int64_t >::Invoke(17 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Int64) */, __this, L_0);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Uri)
extern "C"  void JsonWriter_Write_m1759937626 (JsonWriter_t446744171 * __this, Uri_t19570940 * ___value0, const MethodInfo* method)
{
	{
		Uri_t19570940 * L_0 = ___value0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_1);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Version)
extern "C"  void JsonWriter_Write_m811580164 (JsonWriter_t446744171 * __this, Version_t1755874712 * ___value0, const MethodInfo* method)
{
	{
		Version_t1755874712 * L_0 = ___value0;
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_1);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteArray(System.Collections.IEnumerable)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonSerializationException_t3292412897_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral504030304;
extern const uint32_t JsonWriter_WriteArray_m4192356609_MetadataUsageId;
extern "C"  void JsonWriter_WriteArray_m4192356609 (JsonWriter_t446744171 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_WriteArray_m4192356609_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	bool V_4 = false;
	Il2CppObject * V_5 = NULL;
	bool V_6 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)0;
		TextWriter_t4027217640 * L_0 = __this->get_Writer_0();
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_0, ((int32_t)91));
		int32_t L_1 = __this->get_depth_2();
		__this->set_depth_2(((int32_t)((int32_t)L_1+(int32_t)1)));
		int32_t L_2 = __this->get_depth_2();
		JsonWriterSettings_t2950311970 * L_3 = __this->get_settings_1();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_3);
		V_1 = (bool)((((int32_t)L_2) > ((int32_t)L_4))? 1 : 0);
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_0057;
		}
	}
	{
		JsonWriterSettings_t2950311970 * L_6 = __this->get_settings_1();
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_6);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral504030304, L_9, /*hidden argument*/NULL);
		JsonSerializationException_t3292412897 * L_11 = (JsonSerializationException_t3292412897 *)il2cpp_codegen_object_new(JsonSerializationException_t3292412897_il2cpp_TypeInfo_var);
		JsonSerializationException__ctor_m2868596966(L_11, L_10, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_0057:
	{
	}

IL_0058:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_12 = ___value0;
			NullCheck(L_12);
			Il2CppObject * L_13 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_12);
			V_2 = L_13;
		}

IL_0061:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0091;
			}

IL_0063:
			{
				Il2CppObject * L_14 = V_2;
				NullCheck(L_14);
				Il2CppObject * L_15 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_14);
				V_3 = L_15;
				bool L_16 = V_0;
				V_4 = L_16;
				bool L_17 = V_4;
				if (!L_17)
				{
					goto IL_007d;
				}
			}

IL_0072:
			{
				VirtActionInvoker0::Invoke(33 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteArrayItemDelim() */, __this);
				goto IL_0081;
			}

IL_007d:
			{
				V_0 = (bool)1;
			}

IL_0081:
			{
				VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
				Il2CppObject * L_18 = V_3;
				VirtActionInvoker1< Il2CppObject * >::Invoke(27 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteArrayItem(System.Object) */, __this, L_18);
			}

IL_0091:
			{
				Il2CppObject * L_19 = V_2;
				NullCheck(L_19);
				bool L_20 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_19);
				if (L_20)
				{
					goto IL_0063;
				}
			}

IL_0099:
			{
				IL2CPP_LEAVE(0xB0, FINALLY_009b);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_009b;
		}

FINALLY_009b:
		{ // begin finally (depth: 2)
			{
				Il2CppObject * L_21 = V_2;
				V_5 = ((Il2CppObject *)IsInst(L_21, IDisposable_t2427283555_il2cpp_TypeInfo_var));
				Il2CppObject * L_22 = V_5;
				if (!L_22)
				{
					goto IL_00af;
				}
			}

IL_00a7:
			{
				Il2CppObject * L_23 = V_5;
				NullCheck(L_23);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_23);
			}

IL_00af:
			{
				IL2CPP_END_FINALLY(155)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(155)
		{
			IL2CPP_JUMP_TBL(0xB0, IL_00b0)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_00b0:
		{
			IL2CPP_LEAVE(0xC4, FINALLY_00b3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00b3;
	}

FINALLY_00b3:
	{ // begin finally (depth: 1)
		int32_t L_24 = __this->get_depth_2();
		__this->set_depth_2(((int32_t)((int32_t)L_24-(int32_t)1)));
		IL2CPP_END_FINALLY(179)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(179)
	{
		IL2CPP_JUMP_TBL(0xC4, IL_00c4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00c4:
	{
		bool L_25 = V_0;
		V_6 = L_25;
		bool L_26 = V_6;
		if (!L_26)
		{
			goto IL_00d4;
		}
	}
	{
		VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
	}

IL_00d4:
	{
		TextWriter_t4027217640 * L_27 = __this->get_Writer_0();
		NullCheck(L_27);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_27, ((int32_t)93));
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteArrayItem(System.Object)
extern "C"  void JsonWriter_WriteArrayItem_m3308581641 (JsonWriter_t446744171 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___item0;
		VirtActionInvoker2< Il2CppObject *, bool >::Invoke(5 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Object,System.Boolean) */, __this, L_0, (bool)0);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObject(System.Collections.IDictionary)
extern "C"  void JsonWriter_WriteObject_m4159325587 (JsonWriter_t446744171 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		VirtActionInvoker1< Il2CppObject * >::Invoke(29 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteDictionary(System.Collections.IEnumerable) */, __this, L_0);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteDictionary(System.Collections.IEnumerable)
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonSerializationException_t3292412897_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m1752161452_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Count_m1702369271_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m3478767943_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral654820977;
extern Il2CppCodeGenString* _stringLiteral1757179989;
extern Il2CppCodeGenString* _stringLiteral504030304;
extern const uint32_t JsonWriter_WriteDictionary_m2019323386_MetadataUsageId;
extern "C"  void JsonWriter_WriteDictionary_m2019323386 (JsonWriter_t446744171 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_WriteDictionary_m2019323386_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	int32_t V_4 = 0;
	bool V_5 = false;
	bool V_6 = false;
	bool V_7 = false;
	DictionaryEntry_t3048875398  V_8;
	memset(&V_8, 0, sizeof(V_8));
	bool V_9 = false;
	bool V_10 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_0);
		V_0 = ((Il2CppObject *)IsInst(L_1, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var));
		Il2CppObject * L_2 = V_0;
		V_2 = (bool)((((Il2CppObject*)(Il2CppObject *)L_2) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_3 = V_2;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_4 = ___value0;
		NullCheck(L_4);
		Type_t * L_5 = Object_GetType_m191970594(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral654820977, L_5, /*hidden argument*/NULL);
		JsonSerializationException_t3292412897 * L_7 = (JsonSerializationException_t3292412897 *)il2cpp_codegen_object_new(JsonSerializationException_t3292412897_il2cpp_TypeInfo_var);
		JsonSerializationException__ctor_m2868596966(L_7, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_002c:
	{
		V_1 = (bool)0;
		JsonWriterSettings_t2950311970 * L_8 = __this->get_settings_1();
		NullCheck(L_8);
		bool L_9 = JsonWriterSettings_get_HandleCyclicReferences_m3720345431(L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		bool L_10 = V_3;
		if (!L_10)
		{
			goto IL_00ac;
		}
	}
	{
		V_4 = 0;
		Dictionary_2_t1663937576 * L_11 = __this->get_previouslySerializedObjects_3();
		Il2CppObject * L_12 = ___value0;
		NullCheck(L_11);
		bool L_13 = Dictionary_2_TryGetValue_m1752161452(L_11, L_12, (&V_4), /*hidden argument*/Dictionary_2_TryGetValue_m1752161452_MethodInfo_var);
		V_5 = L_13;
		bool L_14 = V_5;
		if (!L_14)
		{
			goto IL_0091;
		}
	}
	{
		TextWriter_t4027217640 * L_15 = __this->get_Writer_0();
		NullCheck(L_15);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_15, ((int32_t)123));
		int32_t L_16 = V_4;
		int32_t L_17 = L_16;
		Il2CppObject * L_18 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_17);
		JsonWriter_WriteObjectProperty_m1477974781(__this, _stringLiteral1757179989, L_18, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
		TextWriter_t4027217640 * L_19 = __this->get_Writer_0();
		NullCheck(L_19);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_19, ((int32_t)125));
		goto IL_0186;
	}

IL_0091:
	{
		Dictionary_2_t1663937576 * L_20 = __this->get_previouslySerializedObjects_3();
		Il2CppObject * L_21 = ___value0;
		Dictionary_2_t1663937576 * L_22 = __this->get_previouslySerializedObjects_3();
		NullCheck(L_22);
		int32_t L_23 = Dictionary_2_get_Count_m1702369271(L_22, /*hidden argument*/Dictionary_2_get_Count_m1702369271_MethodInfo_var);
		NullCheck(L_20);
		Dictionary_2_Add_m3478767943(L_20, L_21, L_23, /*hidden argument*/Dictionary_2_Add_m3478767943_MethodInfo_var);
	}

IL_00ac:
	{
		TextWriter_t4027217640 * L_24 = __this->get_Writer_0();
		NullCheck(L_24);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_24, ((int32_t)123));
		int32_t L_25 = __this->get_depth_2();
		__this->set_depth_2(((int32_t)((int32_t)L_25+(int32_t)1)));
		int32_t L_26 = __this->get_depth_2();
		JsonWriterSettings_t2950311970 * L_27 = __this->get_settings_1();
		NullCheck(L_27);
		int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_27);
		V_6 = (bool)((((int32_t)L_26) > ((int32_t)L_28))? 1 : 0);
		bool L_29 = V_6;
		if (!L_29)
		{
			goto IL_0102;
		}
	}
	{
		JsonWriterSettings_t2950311970 * L_30 = __this->get_settings_1();
		NullCheck(L_30);
		int32_t L_31 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_30);
		int32_t L_32 = L_31;
		Il2CppObject * L_33 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_32);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral504030304, L_33, /*hidden argument*/NULL);
		JsonSerializationException_t3292412897 * L_35 = (JsonSerializationException_t3292412897 *)il2cpp_codegen_object_new(JsonSerializationException_t3292412897_il2cpp_TypeInfo_var);
		JsonSerializationException__ctor_m2868596966(L_35, L_34, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_35);
	}

IL_0102:
	{
	}

IL_0103:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0148;
		}

IL_0106:
		{
			bool L_36 = V_1;
			V_7 = L_36;
			bool L_37 = V_7;
			if (!L_37)
			{
				goto IL_0119;
			}
		}

IL_010e:
		{
			VirtActionInvoker0::Invoke(34 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyDelim() */, __this);
			goto IL_011d;
		}

IL_0119:
		{
			V_1 = (bool)1;
		}

IL_011d:
		{
			Il2CppObject * L_38 = V_0;
			NullCheck(L_38);
			DictionaryEntry_t3048875398  L_39 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, L_38);
			V_8 = L_39;
			Il2CppObject * L_40 = DictionaryEntry_get_Key_m3623293571((&V_8), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
			String_t* L_41 = Convert_ToString_m2915777777(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
			Il2CppObject * L_42 = V_0;
			NullCheck(L_42);
			DictionaryEntry_t3048875398  L_43 = InterfaceFuncInvoker0< DictionaryEntry_t3048875398  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t259680273_il2cpp_TypeInfo_var, L_42);
			V_8 = L_43;
			Il2CppObject * L_44 = DictionaryEntry_get_Value_m2812883243((&V_8), /*hidden argument*/NULL);
			JsonWriter_WriteObjectProperty_m1477974781(__this, L_41, L_44, /*hidden argument*/NULL);
		}

IL_0148:
		{
			Il2CppObject * L_45 = V_0;
			NullCheck(L_45);
			bool L_46 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_45);
			V_9 = L_46;
			bool L_47 = V_9;
			if (L_47)
			{
				goto IL_0106;
			}
		}

IL_0154:
		{
			IL2CPP_LEAVE(0x168, FINALLY_0157);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0157;
	}

FINALLY_0157:
	{ // begin finally (depth: 1)
		int32_t L_48 = __this->get_depth_2();
		__this->set_depth_2(((int32_t)((int32_t)L_48-(int32_t)1)));
		IL2CPP_END_FINALLY(343)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(343)
	{
		IL2CPP_JUMP_TBL(0x168, IL_0168)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0168:
	{
		bool L_49 = V_1;
		V_10 = L_49;
		bool L_50 = V_10;
		if (!L_50)
		{
			goto IL_0178;
		}
	}
	{
		VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
	}

IL_0178:
	{
		TextWriter_t4027217640 * L_51 = __this->get_Writer_0();
		NullCheck(L_51);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_51, ((int32_t)125));
	}

IL_0186:
	{
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectProperty(System.String,System.Object)
extern "C"  void JsonWriter_WriteObjectProperty_m1477974781 (JsonWriter_t446744171 * __this, String_t* ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
		String_t* L_0 = ___key0;
		VirtActionInvoker1< String_t* >::Invoke(30 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyName(System.String) */, __this, L_0);
		TextWriter_t4027217640 * L_1 = __this->get_Writer_0();
		NullCheck(L_1);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_1, ((int32_t)58));
		Il2CppObject * L_2 = ___value1;
		VirtActionInvoker1< Il2CppObject * >::Invoke(31 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyValue(System.Object) */, __this, L_2);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyName(System.String)
extern "C"  void JsonWriter_WriteObjectPropertyName_m4107500458 (JsonWriter_t446744171 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String) */, __this, L_0);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyValue(System.Object)
extern "C"  void JsonWriter_WriteObjectPropertyValue_m1955779618 (JsonWriter_t446744171 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		VirtActionInvoker2< Il2CppObject *, bool >::Invoke(5 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Object,System.Boolean) */, __this, L_0, (bool)1);
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObject(System.Object,System.Type)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonSerializationException_t3292412897_il2cpp_TypeInfo_var;
extern Il2CppClass* Console_t2311202731_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m1752161452_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Count_m1702369271_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m3478767943_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1757179989;
extern Il2CppCodeGenString* _stringLiteral504030304;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral232057467;
extern Il2CppCodeGenString* _stringLiteral2805268919;
extern Il2CppCodeGenString* _stringLiteral3426503481;
extern Il2CppCodeGenString* _stringLiteral3882481132;
extern Il2CppCodeGenString* _stringLiteral2488150080;
extern Il2CppCodeGenString* _stringLiteral25793727;
extern Il2CppCodeGenString* _stringLiteral157369980;
extern Il2CppCodeGenString* _stringLiteral1888200053;
extern Il2CppCodeGenString* _stringLiteral3530843512;
extern const uint32_t JsonWriter_WriteObject_m695765779_MetadataUsageId;
extern "C"  void JsonWriter_WriteObject_m695765779 (JsonWriter_t446744171 * __this, Il2CppObject * ___value0, Type_t * ___type1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_WriteObject_m695765779_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	int32_t V_2 = 0;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	PropertyInfoU5BU5D_t1736152084* V_6 = NULL;
	FieldInfoU5BU5D_t125053523* V_7 = NULL;
	bool V_8 = false;
	bool V_9 = false;
	PropertyInfoU5BU5D_t1736152084* V_10 = NULL;
	int32_t V_11 = 0;
	PropertyInfo_t * V_12 = NULL;
	Il2CppObject * V_13 = NULL;
	String_t* V_14 = NULL;
	bool V_15 = false;
	bool V_16 = false;
	bool V_17 = false;
	bool V_18 = false;
	bool V_19 = false;
	bool V_20 = false;
	bool V_21 = false;
	bool V_22 = false;
	bool V_23 = false;
	bool V_24 = false;
	bool V_25 = false;
	bool V_26 = false;
	FieldInfoU5BU5D_t125053523* V_27 = NULL;
	int32_t V_28 = 0;
	FieldInfo_t * V_29 = NULL;
	Il2CppObject * V_30 = NULL;
	String_t* V_31 = NULL;
	bool V_32 = false;
	bool V_33 = false;
	bool V_34 = false;
	bool V_35 = false;
	bool V_36 = false;
	bool V_37 = false;
	bool V_38 = false;
	bool V_39 = false;
	bool V_40 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B3_0 = 0;
	int32_t G_B18_0 = 0;
	int32_t G_B26_0 = 0;
	int32_t G_B54_0 = 0;
	{
		V_0 = (bool)0;
		JsonWriterSettings_t2950311970 * L_0 = __this->get_settings_1();
		NullCheck(L_0);
		bool L_1 = JsonWriterSettings_get_HandleCyclicReferences_m3720345431(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Type_t * L_2 = ___type1;
		NullCheck(L_2);
		bool L_3 = Type_get_IsValueType_m1733572463(L_2, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 0;
	}

IL_001c:
	{
		V_1 = (bool)G_B3_0;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_008b;
		}
	}
	{
		V_2 = 0;
		Dictionary_2_t1663937576 * L_5 = __this->get_previouslySerializedObjects_3();
		Il2CppObject * L_6 = ___value0;
		NullCheck(L_5);
		bool L_7 = Dictionary_2_TryGetValue_m1752161452(L_5, L_6, (&V_2), /*hidden argument*/Dictionary_2_TryGetValue_m1752161452_MethodInfo_var);
		V_3 = L_7;
		bool L_8 = V_3;
		if (!L_8)
		{
			goto IL_0070;
		}
	}
	{
		TextWriter_t4027217640 * L_9 = __this->get_Writer_0();
		NullCheck(L_9);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_9, ((int32_t)123));
		int32_t L_10 = V_2;
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_11);
		JsonWriter_WriteObjectProperty_m1477974781(__this, _stringLiteral1757179989, L_12, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
		TextWriter_t4027217640 * L_13 = __this->get_Writer_0();
		NullCheck(L_13);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_13, ((int32_t)125));
		goto IL_04ac;
	}

IL_0070:
	{
		Dictionary_2_t1663937576 * L_14 = __this->get_previouslySerializedObjects_3();
		Il2CppObject * L_15 = ___value0;
		Dictionary_2_t1663937576 * L_16 = __this->get_previouslySerializedObjects_3();
		NullCheck(L_16);
		int32_t L_17 = Dictionary_2_get_Count_m1702369271(L_16, /*hidden argument*/Dictionary_2_get_Count_m1702369271_MethodInfo_var);
		NullCheck(L_14);
		Dictionary_2_Add_m3478767943(L_14, L_15, L_17, /*hidden argument*/Dictionary_2_Add_m3478767943_MethodInfo_var);
	}

IL_008b:
	{
		TextWriter_t4027217640 * L_18 = __this->get_Writer_0();
		NullCheck(L_18);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_18, ((int32_t)123));
		int32_t L_19 = __this->get_depth_2();
		__this->set_depth_2(((int32_t)((int32_t)L_19+(int32_t)1)));
		int32_t L_20 = __this->get_depth_2();
		JsonWriterSettings_t2950311970 * L_21 = __this->get_settings_1();
		NullCheck(L_21);
		int32_t L_22 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_21);
		V_4 = (bool)((((int32_t)L_20) > ((int32_t)L_22))? 1 : 0);
		bool L_23 = V_4;
		if (!L_23)
		{
			goto IL_00e1;
		}
	}
	{
		JsonWriterSettings_t2950311970 * L_24 = __this->get_settings_1();
		NullCheck(L_24);
		int32_t L_25 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth() */, L_24);
		int32_t L_26 = L_25;
		Il2CppObject * L_27 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_26);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral504030304, L_27, /*hidden argument*/NULL);
		JsonSerializationException_t3292412897 * L_29 = (JsonSerializationException_t3292412897 *)il2cpp_codegen_object_new(JsonSerializationException_t3292412897_il2cpp_TypeInfo_var);
		JsonSerializationException__ctor_m2868596966(L_29, L_28, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_29);
	}

IL_00e1:
	{
	}

IL_00e2:
	try
	{ // begin try (depth: 1)
		{
			JsonWriterSettings_t2950311970 * L_30 = __this->get_settings_1();
			NullCheck(L_30);
			String_t* L_31 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_TypeHintName() */, L_30);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_32 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
			V_8 = (bool)((((int32_t)L_32) == ((int32_t)0))? 1 : 0);
			bool L_33 = V_8;
			if (!L_33)
			{
				goto IL_0146;
			}
		}

IL_00fc:
		{
			bool L_34 = V_0;
			V_9 = L_34;
			bool L_35 = V_9;
			if (!L_35)
			{
				goto IL_010f;
			}
		}

IL_0104:
		{
			VirtActionInvoker0::Invoke(34 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyDelim() */, __this);
			goto IL_0113;
		}

IL_010f:
		{
			V_0 = (bool)1;
		}

IL_0113:
		{
			JsonWriterSettings_t2950311970 * L_36 = __this->get_settings_1();
			NullCheck(L_36);
			String_t* L_37 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_TypeHintName() */, L_36);
			Type_t * L_38 = ___type1;
			NullCheck(L_38);
			String_t* L_39 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_38);
			Type_t * L_40 = ___type1;
			NullCheck(L_40);
			Assembly_t4268412390 * L_41 = VirtFuncInvoker0< Assembly_t4268412390 * >::Invoke(14 /* System.Reflection.Assembly System.Type::get_Assembly() */, L_40);
			NullCheck(L_41);
			AssemblyName_t894705941 * L_42 = VirtFuncInvoker0< AssemblyName_t894705941 * >::Invoke(21 /* System.Reflection.AssemblyName System.Reflection.Assembly::GetName() */, L_41);
			NullCheck(L_42);
			String_t* L_43 = AssemblyName_get_Name_m1815759940(L_42, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_44 = String_Concat_m612901809(NULL /*static, unused*/, L_39, _stringLiteral811305474, L_43, /*hidden argument*/NULL);
			JsonWriter_WriteObjectProperty_m1477974781(__this, L_37, L_44, /*hidden argument*/NULL);
		}

IL_0146:
		{
			Type_t * L_45 = ___type1;
			NullCheck(L_45);
			bool L_46 = VirtFuncInvoker0< bool >::Invoke(90 /* System.Boolean System.Type::get_IsGenericType() */, L_45);
			if (!L_46)
			{
				goto IL_0160;
			}
		}

IL_014e:
		{
			Type_t * L_47 = ___type1;
			NullCheck(L_47);
			String_t* L_48 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_47);
			NullCheck(L_48);
			bool L_49 = String_StartsWith_m1841920685(L_48, _stringLiteral232057467, /*hidden argument*/NULL);
			G_B18_0 = ((int32_t)(L_49));
			goto IL_0161;
		}

IL_0160:
		{
			G_B18_0 = 0;
		}

IL_0161:
		{
			V_5 = (bool)G_B18_0;
			Type_t * L_50 = ___type1;
			NullCheck(L_50);
			PropertyInfoU5BU5D_t1736152084* L_51 = Type_GetProperties_m2803026104(L_50, /*hidden argument*/NULL);
			V_6 = L_51;
			PropertyInfoU5BU5D_t1736152084* L_52 = V_6;
			V_10 = L_52;
			V_11 = 0;
			goto IL_0322;
		}

IL_0178:
		{
			PropertyInfoU5BU5D_t1736152084* L_53 = V_10;
			int32_t L_54 = V_11;
			NullCheck(L_53);
			int32_t L_55 = L_54;
			PropertyInfo_t * L_56 = (L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
			V_12 = L_56;
			PropertyInfo_t * L_57 = V_12;
			NullCheck(L_57);
			bool L_58 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Reflection.PropertyInfo::get_CanRead() */, L_57);
			V_15 = (bool)((((int32_t)L_58) == ((int32_t)0))? 1 : 0);
			bool L_59 = V_15;
			if (!L_59)
			{
				goto IL_01c3;
			}
		}

IL_0190:
		{
			JsonWriterSettings_t2950311970 * L_60 = JsonWriter_get_Settings_m3858231505(__this, /*hidden argument*/NULL);
			NullCheck(L_60);
			bool L_61 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DebugMode() */, L_60);
			V_16 = L_61;
			bool L_62 = V_16;
			if (!L_62)
			{
				goto IL_01be;
			}
		}

IL_01a2:
		{
			PropertyInfo_t * L_63 = V_12;
			NullCheck(L_63);
			String_t* L_64 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_63);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_65 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2805268919, L_64, _stringLiteral3426503481, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Console_t2311202731_il2cpp_TypeInfo_var);
			Console_WriteLine_m3271989373(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
		}

IL_01be:
		{
			goto IL_031c;
		}

IL_01c3:
		{
			PropertyInfo_t * L_66 = V_12;
			NullCheck(L_66);
			bool L_67 = VirtFuncInvoker0< bool >::Invoke(16 /* System.Boolean System.Reflection.PropertyInfo::get_CanWrite() */, L_66);
			if (L_67)
			{
				goto IL_01d3;
			}
		}

IL_01cc:
		{
			bool L_68 = V_5;
			G_B26_0 = ((((int32_t)L_68) == ((int32_t)0))? 1 : 0);
			goto IL_01d4;
		}

IL_01d3:
		{
			G_B26_0 = 0;
		}

IL_01d4:
		{
			V_17 = (bool)G_B26_0;
			bool L_69 = V_17;
			if (!L_69)
			{
				goto IL_020d;
			}
		}

IL_01da:
		{
			JsonWriterSettings_t2950311970 * L_70 = JsonWriter_get_Settings_m3858231505(__this, /*hidden argument*/NULL);
			NullCheck(L_70);
			bool L_71 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DebugMode() */, L_70);
			V_18 = L_71;
			bool L_72 = V_18;
			if (!L_72)
			{
				goto IL_0208;
			}
		}

IL_01ec:
		{
			PropertyInfo_t * L_73 = V_12;
			NullCheck(L_73);
			String_t* L_74 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_73);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_75 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2805268919, L_74, _stringLiteral3882481132, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Console_t2311202731_il2cpp_TypeInfo_var);
			Console_WriteLine_m3271989373(NULL /*static, unused*/, L_75, /*hidden argument*/NULL);
		}

IL_0208:
		{
			goto IL_031c;
		}

IL_020d:
		{
			Type_t * L_76 = ___type1;
			PropertyInfo_t * L_77 = V_12;
			Il2CppObject * L_78 = ___value0;
			bool L_79 = JsonWriter_IsIgnored_m1951341061(__this, L_76, L_77, L_78, /*hidden argument*/NULL);
			V_19 = L_79;
			bool L_80 = V_19;
			if (!L_80)
			{
				goto IL_0250;
			}
		}

IL_021d:
		{
			JsonWriterSettings_t2950311970 * L_81 = JsonWriter_get_Settings_m3858231505(__this, /*hidden argument*/NULL);
			NullCheck(L_81);
			bool L_82 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DebugMode() */, L_81);
			V_20 = L_82;
			bool L_83 = V_20;
			if (!L_83)
			{
				goto IL_024b;
			}
		}

IL_022f:
		{
			PropertyInfo_t * L_84 = V_12;
			NullCheck(L_84);
			String_t* L_85 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_84);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_86 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2805268919, L_85, _stringLiteral2488150080, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Console_t2311202731_il2cpp_TypeInfo_var);
			Console_WriteLine_m3271989373(NULL /*static, unused*/, L_86, /*hidden argument*/NULL);
		}

IL_024b:
		{
			goto IL_031c;
		}

IL_0250:
		{
			PropertyInfo_t * L_87 = V_12;
			NullCheck(L_87);
			ParameterInfoU5BU5D_t2275869610* L_88 = VirtFuncInvoker0< ParameterInfoU5BU5D_t2275869610* >::Invoke(21 /* System.Reflection.ParameterInfo[] System.Reflection.PropertyInfo::GetIndexParameters() */, L_87);
			NullCheck(L_88);
			V_21 = (bool)((!(((uint32_t)(((Il2CppArray *)L_88)->max_length)) <= ((uint32_t)0)))? 1 : 0);
			bool L_89 = V_21;
			if (!L_89)
			{
				goto IL_0294;
			}
		}

IL_0261:
		{
			JsonWriterSettings_t2950311970 * L_90 = JsonWriter_get_Settings_m3858231505(__this, /*hidden argument*/NULL);
			NullCheck(L_90);
			bool L_91 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DebugMode() */, L_90);
			V_22 = L_91;
			bool L_92 = V_22;
			if (!L_92)
			{
				goto IL_028f;
			}
		}

IL_0273:
		{
			PropertyInfo_t * L_93 = V_12;
			NullCheck(L_93);
			String_t* L_94 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_93);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_95 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2805268919, L_94, _stringLiteral25793727, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Console_t2311202731_il2cpp_TypeInfo_var);
			Console_WriteLine_m3271989373(NULL /*static, unused*/, L_95, /*hidden argument*/NULL);
		}

IL_028f:
		{
			goto IL_031c;
		}

IL_0294:
		{
			PropertyInfo_t * L_96 = V_12;
			Il2CppObject * L_97 = ___value0;
			NullCheck(L_96);
			Il2CppObject * L_98 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t3614634134* >::Invoke(23 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_96, L_97, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL);
			V_13 = L_98;
			PropertyInfo_t * L_99 = V_12;
			Il2CppObject * L_100 = V_13;
			bool L_101 = JsonWriter_IsDefaultValue_m2413175006(__this, L_99, L_100, /*hidden argument*/NULL);
			V_23 = L_101;
			bool L_102 = V_23;
			if (!L_102)
			{
				goto IL_02df;
			}
		}

IL_02af:
		{
			JsonWriterSettings_t2950311970 * L_103 = JsonWriter_get_Settings_m3858231505(__this, /*hidden argument*/NULL);
			NullCheck(L_103);
			bool L_104 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DebugMode() */, L_103);
			V_24 = L_104;
			bool L_105 = V_24;
			if (!L_105)
			{
				goto IL_02dd;
			}
		}

IL_02c1:
		{
			PropertyInfo_t * L_106 = V_12;
			NullCheck(L_106);
			String_t* L_107 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_106);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_108 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2805268919, L_107, _stringLiteral157369980, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Console_t2311202731_il2cpp_TypeInfo_var);
			Console_WriteLine_m3271989373(NULL /*static, unused*/, L_108, /*hidden argument*/NULL);
		}

IL_02dd:
		{
			goto IL_031c;
		}

IL_02df:
		{
			bool L_109 = V_0;
			V_25 = L_109;
			bool L_110 = V_25;
			if (!L_110)
			{
				goto IL_02ef;
			}
		}

IL_02e6:
		{
			VirtActionInvoker0::Invoke(34 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyDelim() */, __this);
			goto IL_02f1;
		}

IL_02ef:
		{
			V_0 = (bool)1;
		}

IL_02f1:
		{
			PropertyInfo_t * L_111 = V_12;
			String_t* L_112 = JsonNameAttribute_GetJsonName_m3020263406(NULL /*static, unused*/, L_111, /*hidden argument*/NULL);
			V_14 = L_112;
			String_t* L_113 = V_14;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_114 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_113, /*hidden argument*/NULL);
			V_26 = L_114;
			bool L_115 = V_26;
			if (!L_115)
			{
				goto IL_0310;
			}
		}

IL_0307:
		{
			PropertyInfo_t * L_116 = V_12;
			NullCheck(L_116);
			String_t* L_117 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_116);
			V_14 = L_117;
		}

IL_0310:
		{
			String_t* L_118 = V_14;
			Il2CppObject * L_119 = V_13;
			JsonWriter_WriteObjectProperty_m1477974781(__this, L_118, L_119, /*hidden argument*/NULL);
		}

IL_031c:
		{
			int32_t L_120 = V_11;
			V_11 = ((int32_t)((int32_t)L_120+(int32_t)1));
		}

IL_0322:
		{
			int32_t L_121 = V_11;
			PropertyInfoU5BU5D_t1736152084* L_122 = V_10;
			NullCheck(L_122);
			if ((((int32_t)L_121) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_122)->max_length)))))))
			{
				goto IL_0178;
			}
		}

IL_032d:
		{
			Type_t * L_123 = ___type1;
			NullCheck(L_123);
			FieldInfoU5BU5D_t125053523* L_124 = Type_GetFields_m445058499(L_123, /*hidden argument*/NULL);
			V_7 = L_124;
			FieldInfoU5BU5D_t125053523* L_125 = V_7;
			V_27 = L_125;
			V_28 = 0;
			goto IL_0471;
		}

IL_0342:
		{
			FieldInfoU5BU5D_t125053523* L_126 = V_27;
			int32_t L_127 = V_28;
			NullCheck(L_126);
			int32_t L_128 = L_127;
			FieldInfo_t * L_129 = (L_126)->GetAt(static_cast<il2cpp_array_size_t>(L_128));
			V_29 = L_129;
			FieldInfo_t * L_130 = V_29;
			NullCheck(L_130);
			bool L_131 = FieldInfo_get_IsPublic_m618896610(L_130, /*hidden argument*/NULL);
			if (!L_131)
			{
				goto IL_035c;
			}
		}

IL_0353:
		{
			FieldInfo_t * L_132 = V_29;
			NullCheck(L_132);
			bool L_133 = FieldInfo_get_IsStatic_m1411173225(L_132, /*hidden argument*/NULL);
			G_B54_0 = ((int32_t)(L_133));
			goto IL_035d;
		}

IL_035c:
		{
			G_B54_0 = 1;
		}

IL_035d:
		{
			V_32 = (bool)G_B54_0;
			bool L_134 = V_32;
			if (!L_134)
			{
				goto IL_0396;
			}
		}

IL_0363:
		{
			JsonWriterSettings_t2950311970 * L_135 = JsonWriter_get_Settings_m3858231505(__this, /*hidden argument*/NULL);
			NullCheck(L_135);
			bool L_136 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DebugMode() */, L_135);
			V_33 = L_136;
			bool L_137 = V_33;
			if (!L_137)
			{
				goto IL_0391;
			}
		}

IL_0375:
		{
			FieldInfo_t * L_138 = V_29;
			NullCheck(L_138);
			String_t* L_139 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_138);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_140 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2805268919, L_139, _stringLiteral1888200053, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Console_t2311202731_il2cpp_TypeInfo_var);
			Console_WriteLine_m3271989373(NULL /*static, unused*/, L_140, /*hidden argument*/NULL);
		}

IL_0391:
		{
			goto IL_046b;
		}

IL_0396:
		{
			Type_t * L_141 = ___type1;
			FieldInfo_t * L_142 = V_29;
			Il2CppObject * L_143 = ___value0;
			bool L_144 = JsonWriter_IsIgnored_m1951341061(__this, L_141, L_142, L_143, /*hidden argument*/NULL);
			V_34 = L_144;
			bool L_145 = V_34;
			if (!L_145)
			{
				goto IL_03d9;
			}
		}

IL_03a6:
		{
			JsonWriterSettings_t2950311970 * L_146 = JsonWriter_get_Settings_m3858231505(__this, /*hidden argument*/NULL);
			NullCheck(L_146);
			bool L_147 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DebugMode() */, L_146);
			V_35 = L_147;
			bool L_148 = V_35;
			if (!L_148)
			{
				goto IL_03d4;
			}
		}

IL_03b8:
		{
			FieldInfo_t * L_149 = V_29;
			NullCheck(L_149);
			String_t* L_150 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_149);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_151 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2805268919, L_150, _stringLiteral3530843512, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Console_t2311202731_il2cpp_TypeInfo_var);
			Console_WriteLine_m3271989373(NULL /*static, unused*/, L_151, /*hidden argument*/NULL);
		}

IL_03d4:
		{
			goto IL_046b;
		}

IL_03d9:
		{
			FieldInfo_t * L_152 = V_29;
			Il2CppObject * L_153 = ___value0;
			NullCheck(L_152);
			Il2CppObject * L_154 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(17 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_152, L_153);
			V_30 = L_154;
			FieldInfo_t * L_155 = V_29;
			Il2CppObject * L_156 = V_30;
			bool L_157 = JsonWriter_IsDefaultValue_m2413175006(__this, L_155, L_156, /*hidden argument*/NULL);
			V_36 = L_157;
			bool L_158 = V_36;
			if (!L_158)
			{
				goto IL_0423;
			}
		}

IL_03f3:
		{
			JsonWriterSettings_t2950311970 * L_159 = JsonWriter_get_Settings_m3858231505(__this, /*hidden argument*/NULL);
			NullCheck(L_159);
			bool L_160 = VirtFuncInvoker0< bool >::Invoke(11 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DebugMode() */, L_159);
			V_37 = L_160;
			bool L_161 = V_37;
			if (!L_161)
			{
				goto IL_0421;
			}
		}

IL_0405:
		{
			FieldInfo_t * L_162 = V_29;
			NullCheck(L_162);
			String_t* L_163 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_162);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_164 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2805268919, L_163, _stringLiteral157369980, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Console_t2311202731_il2cpp_TypeInfo_var);
			Console_WriteLine_m3271989373(NULL /*static, unused*/, L_164, /*hidden argument*/NULL);
		}

IL_0421:
		{
			goto IL_046b;
		}

IL_0423:
		{
			bool L_165 = V_0;
			V_38 = L_165;
			bool L_166 = V_38;
			if (!L_166)
			{
				goto IL_043c;
			}
		}

IL_042a:
		{
			VirtActionInvoker0::Invoke(34 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyDelim() */, __this);
			VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
			goto IL_0440;
		}

IL_043c:
		{
			V_0 = (bool)1;
		}

IL_0440:
		{
			FieldInfo_t * L_167 = V_29;
			String_t* L_168 = JsonNameAttribute_GetJsonName_m3020263406(NULL /*static, unused*/, L_167, /*hidden argument*/NULL);
			V_31 = L_168;
			String_t* L_169 = V_31;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_170 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_169, /*hidden argument*/NULL);
			V_39 = L_170;
			bool L_171 = V_39;
			if (!L_171)
			{
				goto IL_045f;
			}
		}

IL_0456:
		{
			FieldInfo_t * L_172 = V_29;
			NullCheck(L_172);
			String_t* L_173 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_172);
			V_31 = L_173;
		}

IL_045f:
		{
			String_t* L_174 = V_31;
			Il2CppObject * L_175 = V_30;
			JsonWriter_WriteObjectProperty_m1477974781(__this, L_174, L_175, /*hidden argument*/NULL);
		}

IL_046b:
		{
			int32_t L_176 = V_28;
			V_28 = ((int32_t)((int32_t)L_176+(int32_t)1));
		}

IL_0471:
		{
			int32_t L_177 = V_28;
			FieldInfoU5BU5D_t125053523* L_178 = V_27;
			NullCheck(L_178);
			if ((((int32_t)L_177) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_178)->max_length)))))))
			{
				goto IL_0342;
			}
		}

IL_047c:
		{
			IL2CPP_LEAVE(0x490, FINALLY_047f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_047f;
	}

FINALLY_047f:
	{ // begin finally (depth: 1)
		int32_t L_179 = __this->get_depth_2();
		__this->set_depth_2(((int32_t)((int32_t)L_179-(int32_t)1)));
		IL2CPP_END_FINALLY(1151)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1151)
	{
		IL2CPP_JUMP_TBL(0x490, IL_0490)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0490:
	{
		bool L_180 = V_0;
		V_40 = L_180;
		bool L_181 = V_40;
		if (!L_181)
		{
			goto IL_049e;
		}
	}
	{
		VirtActionInvoker0::Invoke(35 /* System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine() */, __this);
	}

IL_049e:
	{
		TextWriter_t4027217640 * L_182 = __this->get_Writer_0();
		NullCheck(L_182);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_182, ((int32_t)125));
	}

IL_04ac:
	{
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteArrayItemDelim()
extern "C"  void JsonWriter_WriteArrayItemDelim_m925569848 (JsonWriter_t446744171 * __this, const MethodInfo* method)
{
	{
		TextWriter_t4027217640 * L_0 = __this->get_Writer_0();
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_0, ((int32_t)44));
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyDelim()
extern "C"  void JsonWriter_WriteObjectPropertyDelim_m2333488564 (JsonWriter_t446744171 * __this, const MethodInfo* method)
{
	{
		TextWriter_t4027217640 * L_0 = __this->get_Writer_0();
		NullCheck(L_0);
		VirtActionInvoker1< Il2CppChar >::Invoke(13 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_0, ((int32_t)44));
		return;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine()
extern "C"  void JsonWriter_WriteLine_m1547489601 (JsonWriter_t446744171 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		JsonWriterSettings_t2950311970 * L_0 = __this->get_settings_1();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_PrettyPrint() */, L_0);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		goto IL_0050;
	}

IL_0016:
	{
		TextWriter_t4027217640 * L_3 = __this->get_Writer_0();
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(22 /* System.Void System.IO.TextWriter::WriteLine() */, L_3);
		V_1 = 0;
		goto IL_0043;
	}

IL_0026:
	{
		TextWriter_t4027217640 * L_4 = __this->get_Writer_0();
		JsonWriterSettings_t2950311970 * L_5 = __this->get_settings_1();
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_Tab() */, L_5);
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(16 /* System.Void System.IO.TextWriter::Write(System.String) */, L_4, L_6);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0043:
	{
		int32_t L_8 = V_1;
		int32_t L_9 = __this->get_depth_2();
		V_2 = (bool)((((int32_t)L_8) < ((int32_t)L_9))? 1 : 0);
		bool L_10 = V_2;
		if (L_10)
		{
			goto IL_0026;
		}
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonWriter::IsIgnored(System.Type,System.Reflection.MemberInfo,System.Object)
extern const Il2CppType* JsonOptInAttribute_t2568113370_0_0_0_var;
extern const Il2CppType* JsonMemberAttribute_t797088438_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2256878656;
extern const uint32_t JsonWriter_IsIgnored_m1951341061_MetadataUsageId;
extern "C"  bool JsonWriter_IsIgnored_m1951341061 (JsonWriter_t446744171 * __this, Type_t * ___objType0, MemberInfo_t * ___member1, Il2CppObject * ___obj2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_IsIgnored_m1951341061_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	PropertyInfo_t * V_4 = NULL;
	bool V_5 = false;
	Il2CppObject * V_6 = NULL;
	bool V_7 = false;
	bool V_8 = false;
	bool V_9 = false;
	bool V_10 = false;
	PropertyInfo_t * V_11 = NULL;
	bool V_12 = false;
	bool V_13 = false;
	Il2CppObject * V_14 = NULL;
	bool V_15 = false;
	int32_t G_B7_0 = 0;
	int32_t G_B22_0 = 0;
	{
		MemberInfo_t * L_0 = ___member1;
		bool L_1 = JsonIgnoreAttribute_IsJsonIgnore_m3752554121(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0124;
	}

IL_0013:
	{
		MemberInfo_t * L_3 = ___member1;
		String_t* L_4 = JsonSpecifiedPropertyAttribute_GetJsonSpecifiedProperty_m386584320(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_3 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		bool L_7 = V_3;
		if (!L_7)
		{
			goto IL_006e;
		}
	}
	{
		Type_t * L_8 = ___objType0;
		String_t* L_9 = V_0;
		NullCheck(L_8);
		PropertyInfo_t * L_10 = Type_GetProperty_m808359402(L_8, L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		PropertyInfo_t * L_11 = V_4;
		V_5 = (bool)((!(((Il2CppObject*)(PropertyInfo_t *)L_11) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_12 = V_5;
		if (!L_12)
		{
			goto IL_006d;
		}
	}
	{
		PropertyInfo_t * L_13 = V_4;
		Il2CppObject * L_14 = ___obj2;
		NullCheck(L_13);
		Il2CppObject * L_15 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t3614634134* >::Invoke(23 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_13, L_14, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL);
		V_6 = L_15;
		Il2CppObject * L_16 = V_6;
		if (!((Il2CppObject *)IsInstSealed(L_16, Boolean_t3825574718_il2cpp_TypeInfo_var)))
		{
			goto IL_005d;
		}
	}
	{
		Il2CppObject * L_17 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		bool L_18 = Convert_ToBoolean_m3380084829(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		G_B7_0 = ((((int32_t)L_18) == ((int32_t)0))? 1 : 0);
		goto IL_005e;
	}

IL_005d:
	{
		G_B7_0 = 0;
	}

IL_005e:
	{
		V_7 = (bool)G_B7_0;
		bool L_19 = V_7;
		if (!L_19)
		{
			goto IL_006c;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0124;
	}

IL_006c:
	{
	}

IL_006d:
	{
	}

IL_006e:
	{
		Type_t * L_20 = ___objType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JsonOptInAttribute_t2568113370_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_20);
		ObjectU5BU5D_t3614634134* L_22 = VirtFuncInvoker2< ObjectU5BU5D_t3614634134*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_20, L_21, (bool)1);
		NullCheck(L_22);
		V_8 = (bool)((!(((uint32_t)(((Il2CppArray *)L_22)->max_length)) <= ((uint32_t)0)))? 1 : 0);
		bool L_23 = V_8;
		if (!L_23)
		{
			goto IL_00ab;
		}
	}
	{
		MemberInfo_t * L_24 = ___member1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_25 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(JsonMemberAttribute_t797088438_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_24);
		ObjectU5BU5D_t3614634134* L_26 = VirtFuncInvoker2< ObjectU5BU5D_t3614634134*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_24, L_25, (bool)1);
		NullCheck(L_26);
		V_9 = (bool)((((int32_t)(((Il2CppArray *)L_26)->max_length)) == ((int32_t)0))? 1 : 0);
		bool L_27 = V_9;
		if (!L_27)
		{
			goto IL_00aa;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0124;
	}

IL_00aa:
	{
	}

IL_00ab:
	{
		JsonWriterSettings_t2950311970 * L_28 = __this->get_settings_1();
		NullCheck(L_28);
		bool L_29 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_UseXmlSerializationAttributes() */, L_28);
		V_10 = L_29;
		bool L_30 = V_10;
		if (!L_30)
		{
			goto IL_0120;
		}
	}
	{
		MemberInfo_t * L_31 = ___member1;
		bool L_32 = JsonIgnoreAttribute_IsXmlIgnore_m1553163884(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		V_12 = L_32;
		bool L_33 = V_12;
		if (!L_33)
		{
			goto IL_00ce;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0124;
	}

IL_00ce:
	{
		Type_t * L_34 = ___objType0;
		MemberInfo_t * L_35 = ___member1;
		NullCheck(L_35);
		String_t* L_36 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_35);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = String_Concat_m2596409543(NULL /*static, unused*/, L_36, _stringLiteral2256878656, /*hidden argument*/NULL);
		NullCheck(L_34);
		PropertyInfo_t * L_38 = Type_GetProperty_m808359402(L_34, L_37, /*hidden argument*/NULL);
		V_11 = L_38;
		PropertyInfo_t * L_39 = V_11;
		V_13 = (bool)((!(((Il2CppObject*)(PropertyInfo_t *)L_39) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_40 = V_13;
		if (!L_40)
		{
			goto IL_011f;
		}
	}
	{
		PropertyInfo_t * L_41 = V_11;
		Il2CppObject * L_42 = ___obj2;
		NullCheck(L_41);
		Il2CppObject * L_43 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t3614634134* >::Invoke(23 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_41, L_42, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL);
		V_14 = L_43;
		Il2CppObject * L_44 = V_14;
		if (!((Il2CppObject *)IsInstSealed(L_44, Boolean_t3825574718_il2cpp_TypeInfo_var)))
		{
			goto IL_0112;
		}
	}
	{
		Il2CppObject * L_45 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		bool L_46 = Convert_ToBoolean_m3380084829(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		G_B22_0 = ((((int32_t)L_46) == ((int32_t)0))? 1 : 0);
		goto IL_0113;
	}

IL_0112:
	{
		G_B22_0 = 0;
	}

IL_0113:
	{
		V_15 = (bool)G_B22_0;
		bool L_47 = V_15;
		if (!L_47)
		{
			goto IL_011e;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0124;
	}

IL_011e:
	{
	}

IL_011f:
	{
	}

IL_0120:
	{
		V_2 = (bool)0;
		goto IL_0124;
	}

IL_0124:
	{
		bool L_48 = V_2;
		return L_48;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonWriter::IsDefaultValue(System.Reflection.MemberInfo,System.Object)
extern const Il2CppType* DefaultValueAttribute_t1302720498_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DefaultValueAttribute_t1302720498_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter_IsDefaultValue_m2413175006_MetadataUsageId;
extern "C"  bool JsonWriter_IsDefaultValue_m2413175006 (JsonWriter_t446744171 * __this, MemberInfo_t * ___member0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_IsDefaultValue_m2413175006_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DefaultValueAttribute_t1302720498 * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	{
		MemberInfo_t * L_0 = ___member0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(DefaultValueAttribute_t1302720498_0_0_0_var), /*hidden argument*/NULL);
		Attribute_t542643598 * L_2 = Attribute_GetCustomAttribute_m2157205805(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = ((DefaultValueAttribute_t1302720498 *)IsInstClass(L_2, DefaultValueAttribute_t1302720498_il2cpp_TypeInfo_var));
		DefaultValueAttribute_t1302720498 * L_3 = V_0;
		V_1 = (bool)((((Il2CppObject*)(DefaultValueAttribute_t1302720498 *)L_3) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0024;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_0048;
	}

IL_0024:
	{
		DefaultValueAttribute_t1302720498 * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = VirtFuncInvoker0< Il2CppObject * >::Invoke(6 /* System.Object System.ComponentModel.DefaultValueAttribute::get_Value() */, L_5);
		V_3 = (bool)((((Il2CppObject*)(Il2CppObject *)L_6) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_7 = V_3;
		if (!L_7)
		{
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_8 = ___value1;
		V_2 = (bool)((((Il2CppObject*)(Il2CppObject *)L_8) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		goto IL_0048;
	}

IL_0039:
	{
		DefaultValueAttribute_t1302720498 * L_9 = V_0;
		NullCheck(L_9);
		Il2CppObject * L_10 = VirtFuncInvoker0< Il2CppObject * >::Invoke(6 /* System.Object System.ComponentModel.DefaultValueAttribute::get_Value() */, L_9);
		Il2CppObject * L_11 = ___value1;
		NullCheck(L_10);
		bool L_12 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_10, L_11);
		V_2 = L_12;
		goto IL_0048;
	}

IL_0048:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Enum[] Pathfinding.Serialization.JsonFx.JsonWriter::GetFlagList(System.Type,System.Object)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1828816677_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1367259542_MethodInfo_var;
extern const MethodInfo* List_1_Add_m148518617_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m407849691_MethodInfo_var;
extern const uint32_t JsonWriter_GetFlagList_m2306565768_MetadataUsageId;
extern "C"  EnumU5BU5D_t975156036* JsonWriter_GetFlagList_m2306565768 (Il2CppObject * __this /* static, unused */, Type_t * ___enumType0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_GetFlagList_m2306565768_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint64_t V_0 = 0;
	Il2CppArray * V_1 = NULL;
	List_1_t1828816677 * V_2 = NULL;
	bool V_3 = false;
	EnumU5BU5D_t975156036* V_4 = NULL;
	int32_t V_5 = 0;
	uint64_t V_6 = 0;
	bool V_7 = false;
	bool V_8 = false;
	bool V_9 = false;
	bool V_10 = false;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		uint64_t L_1 = Convert_ToUInt64_m1896191125(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Type_t * L_2 = ___enumType0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Il2CppArray * L_3 = Enum_GetValues_m2107059536(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Il2CppArray * L_4 = V_1;
		NullCheck(L_4);
		int32_t L_5 = Array_get_Length_m1498215565(L_4, /*hidden argument*/NULL);
		List_1_t1828816677 * L_6 = (List_1_t1828816677 *)il2cpp_codegen_object_new(List_1_t1828816677_il2cpp_TypeInfo_var);
		List_1__ctor_m1367259542(L_6, L_5, /*hidden argument*/List_1__ctor_m1367259542_MethodInfo_var);
		V_2 = L_6;
		uint64_t L_7 = V_0;
		V_3 = (bool)((((int64_t)L_7) == ((int64_t)(((int64_t)((int64_t)0)))))? 1 : 0);
		bool L_8 = V_3;
		if (!L_8)
		{
			goto IL_0045;
		}
	}
	{
		List_1_t1828816677 * L_9 = V_2;
		Il2CppObject * L_10 = ___value1;
		Type_t * L_11 = ___enumType0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_12 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		List_1_Add_m148518617(L_9, ((Enum_t2459695545 *)CastclassClass(L_12, Enum_t2459695545_il2cpp_TypeInfo_var)), /*hidden argument*/List_1_Add_m148518617_MethodInfo_var);
		List_1_t1828816677 * L_13 = V_2;
		NullCheck(L_13);
		EnumU5BU5D_t975156036* L_14 = List_1_ToArray_m407849691(L_13, /*hidden argument*/List_1_ToArray_m407849691_MethodInfo_var);
		V_4 = L_14;
		goto IL_00df;
	}

IL_0045:
	{
		Il2CppArray * L_15 = V_1;
		NullCheck(L_15);
		int32_t L_16 = Array_get_Length_m1498215565(L_15, /*hidden argument*/NULL);
		V_5 = ((int32_t)((int32_t)L_16-(int32_t)1));
		goto IL_00a7;
	}

IL_0051:
	{
		Il2CppArray * L_17 = V_1;
		int32_t L_18 = V_5;
		NullCheck(L_17);
		Il2CppObject * L_19 = Array_GetValue_m3284370071(L_17, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		uint64_t L_20 = Convert_ToUInt64_m1896191125(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		V_6 = L_20;
		int32_t L_21 = V_5;
		if (L_21)
		{
			goto IL_006d;
		}
	}
	{
		uint64_t L_22 = V_6;
		G_B6_0 = ((((int64_t)L_22) == ((int64_t)(((int64_t)((int64_t)0)))))? 1 : 0);
		goto IL_006e;
	}

IL_006d:
	{
		G_B6_0 = 0;
	}

IL_006e:
	{
		V_7 = (bool)G_B6_0;
		bool L_23 = V_7;
		if (!L_23)
		{
			goto IL_0077;
		}
	}
	{
		goto IL_00a1;
	}

IL_0077:
	{
		uint64_t L_24 = V_0;
		uint64_t L_25 = V_6;
		uint64_t L_26 = V_6;
		V_8 = (bool)((((int64_t)((int64_t)((int64_t)L_24&(int64_t)L_25))) == ((int64_t)L_26))? 1 : 0);
		bool L_27 = V_8;
		if (!L_27)
		{
			goto IL_00a0;
		}
	}
	{
		uint64_t L_28 = V_0;
		uint64_t L_29 = V_6;
		V_0 = ((int64_t)((int64_t)L_28-(int64_t)L_29));
		List_1_t1828816677 * L_30 = V_2;
		Il2CppArray * L_31 = V_1;
		int32_t L_32 = V_5;
		NullCheck(L_31);
		Il2CppObject * L_33 = Array_GetValue_m3284370071(L_31, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		List_1_Add_m148518617(L_30, ((Enum_t2459695545 *)IsInstClass(L_33, Enum_t2459695545_il2cpp_TypeInfo_var)), /*hidden argument*/List_1_Add_m148518617_MethodInfo_var);
	}

IL_00a0:
	{
	}

IL_00a1:
	{
		int32_t L_34 = V_5;
		V_5 = ((int32_t)((int32_t)L_34-(int32_t)1));
	}

IL_00a7:
	{
		int32_t L_35 = V_5;
		V_9 = (bool)((((int32_t)((((int32_t)L_35) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_36 = V_9;
		if (L_36)
		{
			goto IL_0051;
		}
	}
	{
		uint64_t L_37 = V_0;
		V_10 = (bool)((!(((uint64_t)L_37) <= ((uint64_t)(((int64_t)((int64_t)0))))))? 1 : 0);
		bool L_38 = V_10;
		if (!L_38)
		{
			goto IL_00d5;
		}
	}
	{
		List_1_t1828816677 * L_39 = V_2;
		Type_t * L_40 = ___enumType0;
		uint64_t L_41 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Il2CppObject * L_42 = Enum_ToObject_m1438724003(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/NULL);
		NullCheck(L_39);
		List_1_Add_m148518617(L_39, ((Enum_t2459695545 *)IsInstClass(L_42, Enum_t2459695545_il2cpp_TypeInfo_var)), /*hidden argument*/List_1_Add_m148518617_MethodInfo_var);
	}

IL_00d5:
	{
		List_1_t1828816677 * L_43 = V_2;
		NullCheck(L_43);
		EnumU5BU5D_t975156036* L_44 = List_1_ToArray_m407849691(L_43, /*hidden argument*/List_1_ToArray_m407849691_MethodInfo_var);
		V_4 = L_44;
		goto IL_00df;
	}

IL_00df:
	{
		EnumU5BU5D_t975156036* L_45 = V_4;
		return L_45;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonWriter::InvalidIeee754(System.Decimal)
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter_InvalidIeee754_m2621332041_MetadataUsageId;
extern "C"  bool JsonWriter_InvalidIeee754_m2621332041 (JsonWriter_t446744171 * __this, Decimal_t724701077  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_InvalidIeee754_m2621332041_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		Decimal_t724701077  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t724701077_il2cpp_TypeInfo_var);
		double L_1 = Decimal_op_Explicit_m16014099(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Decimal_t724701077  L_2 = Decimal_op_Explicit_m1194935507(NULL /*static, unused*/, (((double)((double)L_1))), /*hidden argument*/NULL);
		Decimal_t724701077  L_3 = ___value0;
		bool L_4 = Decimal_op_Inequality_m519758535(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001d;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.Object)
		V_0 = (bool)1;
		goto IL_001d;
	} // end catch (depth: 1)

IL_001d:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::System.IDisposable.Dispose()
extern "C"  void JsonWriter_System_IDisposable_Dispose_m341063705 (JsonWriter_t446744171 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		TextWriter_t4027217640 * L_0 = __this->get_Writer_0();
		V_0 = (bool)((!(((Il2CppObject*)(TextWriter_t4027217640 *)L_0) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		TextWriter_t4027217640 * L_2 = __this->get_Writer_0();
		NullCheck(L_2);
		TextWriter_Dispose_m700634098(L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_HandleCyclicReferences()
extern "C"  bool JsonWriterSettings_get_HandleCyclicReferences_m3720345431 (JsonWriterSettings_t2950311970 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CHandleCyclicReferencesU3Ek__BackingField_7();
		return L_0;
	}
}
// System.String Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_TypeHintName()
extern "C"  String_t* JsonWriterSettings_get_TypeHintName_m1998258859 (JsonWriterSettings_t2950311970 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_typeHintName_5();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_PrettyPrint()
extern "C"  bool JsonWriterSettings_get_PrettyPrint_m2062589073 (JsonWriterSettings_t2950311970 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_prettyPrint_3();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.String Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_Tab()
extern "C"  String_t* JsonWriterSettings_get_Tab_m526246488 (JsonWriterSettings_t2950311970 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_tab_4();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_NewLine()
extern "C"  String_t* JsonWriterSettings_get_NewLine_m1206713581 (JsonWriterSettings_t2950311970 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_newLine_2();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Int32 Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_MaxDepth()
extern "C"  int32_t JsonWriterSettings_get_MaxDepth_m2067774485 (JsonWriterSettings_t2950311970 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_maxDepth_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_UseXmlSerializationAttributes()
extern "C"  bool JsonWriterSettings_get_UseXmlSerializationAttributes_m1706967417 (JsonWriterSettings_t2950311970 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_useXmlSerializationAttributes_6();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime> Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DateTimeSerializer()
extern "C"  WriteDelegate_1_t415313529 * JsonWriterSettings_get_DateTimeSerializer_m2992878757 (JsonWriterSettings_t2950311970 * __this, const MethodInfo* method)
{
	WriteDelegate_1_t415313529 * V_0 = NULL;
	{
		WriteDelegate_1_t415313529 * L_0 = __this->get_dateTimeSerializer_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		WriteDelegate_1_t415313529 * L_1 = V_0;
		return L_1;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.JsonWriterSettings::get_DebugMode()
extern "C"  bool JsonWriterSettings_get_DebugMode_m1343959352 (JsonWriterSettings_t2950311970 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CDebugModeU3Ek__BackingField_8();
		return L_0;
	}
}
// Pathfinding.Serialization.JsonFx.JsonConverter Pathfinding.Serialization.JsonFx.JsonWriterSettings::GetConverter(System.Type)
extern const MethodInfo* List_1_get_Item_m4187249556_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3499622935_MethodInfo_var;
extern const uint32_t JsonWriterSettings_GetConverter_m956843915_MetadataUsageId;
extern "C"  JsonConverter_t4092422604 * JsonWriterSettings_GetConverter_m956843915 (JsonWriterSettings_t2950311970 * __this, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriterSettings_GetConverter_m956843915_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	JsonConverter_t4092422604 * V_2 = NULL;
	bool V_3 = false;
	{
		V_0 = 0;
		goto IL_002e;
	}

IL_0005:
	{
		List_1_t3461543736 * L_0 = __this->get_converters_9();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		JsonConverter_t4092422604 * L_2 = List_1_get_Item_m4187249556(L_0, L_1, /*hidden argument*/List_1_get_Item_m4187249556_MethodInfo_var);
		Type_t * L_3 = ___type0;
		NullCheck(L_2);
		bool L_4 = VirtFuncInvoker1< bool, Type_t * >::Invoke(4 /* System.Boolean Pathfinding.Serialization.JsonFx.JsonConverter::CanConvert(System.Type) */, L_2, L_3);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_002a;
		}
	}
	{
		List_1_t3461543736 * L_6 = __this->get_converters_9();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		JsonConverter_t4092422604 * L_8 = List_1_get_Item_m4187249556(L_6, L_7, /*hidden argument*/List_1_get_Item_m4187249556_MethodInfo_var);
		V_2 = L_8;
		goto IL_0044;
	}

IL_002a:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_10 = V_0;
		List_1_t3461543736 * L_11 = __this->get_converters_9();
		NullCheck(L_11);
		int32_t L_12 = List_1_get_Count_m3499622935(L_11, /*hidden argument*/List_1_get_Count_m3499622935_MethodInfo_var);
		V_3 = (bool)((((int32_t)L_10) < ((int32_t)L_12))? 1 : 0);
		bool L_13 = V_3;
		if (L_13)
		{
			goto IL_0005;
		}
	}
	{
		V_2 = (JsonConverter_t4092422604 *)NULL;
		goto IL_0044;
	}

IL_0044:
	{
		JsonConverter_t4092422604 * L_14 = V_2;
		return L_14;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.JsonWriterSettings::.ctor()
extern Il2CppClass* List_1_t3461543736_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4179343751_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029349;
extern const uint32_t JsonWriterSettings__ctor_m1617359135_MetadataUsageId;
extern "C"  void JsonWriterSettings__ctor_m1617359135 (JsonWriterSettings_t2950311970 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JsonWriterSettings__ctor_m1617359135_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_maxDepth_1(((int32_t)25));
		String_t* L_0 = Environment_get_NewLine_m266316410(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_newLine_2(L_0);
		__this->set_tab_4(_stringLiteral372029349);
		List_1_t3461543736 * L_1 = (List_1_t3461543736 *)il2cpp_codegen_object_new(List_1_t3461543736_il2cpp_TypeInfo_var);
		List_1__ctor_m4179343751(L_1, /*hidden argument*/List_1__ctor_m4179343751_MethodInfo_var);
		__this->set_converters_9(L_1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>> Pathfinding.Serialization.JsonFx.TypeCoercionUtility::get_MemberMapCache()
extern Il2CppClass* Dictionary_2_t3600267123_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2457656734_MethodInfo_var;
extern const uint32_t TypeCoercionUtility_get_MemberMapCache_m1544955808_MetadataUsageId;
extern "C"  Dictionary_2_t3600267123 * TypeCoercionUtility_get_MemberMapCache_m1544955808 (TypeCoercionUtility_t3996825458 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_get_MemberMapCache_m1544955808_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Dictionary_2_t3600267123 * V_1 = NULL;
	{
		Dictionary_2_t3600267123 * L_0 = __this->get_memberMapCache_0();
		V_0 = (bool)((((Il2CppObject*)(Dictionary_2_t3600267123 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Dictionary_2_t3600267123 * L_2 = (Dictionary_2_t3600267123 *)il2cpp_codegen_object_new(Dictionary_2_t3600267123_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2457656734(L_2, /*hidden argument*/Dictionary_2__ctor_m2457656734_MethodInfo_var);
		__this->set_memberMapCache_0(L_2);
	}

IL_001b:
	{
		Dictionary_2_t3600267123 * L_3 = __this->get_memberMapCache_0();
		V_1 = L_3;
		goto IL_0024;
	}

IL_0024:
	{
		Dictionary_2_t3600267123 * L_4 = V_1;
		return L_4;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.TypeCoercionUtility::ProcessTypeHint(System.Collections.IDictionary,System.String,System.Type&,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>&)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeCoercionUtility_ProcessTypeHint_m3882009641_MetadataUsageId;
extern "C"  Il2CppObject * TypeCoercionUtility_ProcessTypeHint_m3882009641 (TypeCoercionUtility_t3996825458 * __this, Il2CppObject * ___result0, String_t* ___typeInfo1, Type_t ** ___objectType2, Dictionary_2_t1662909226 ** ___memberMap3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_ProcessTypeHint_m3882009641_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	bool V_3 = false;
	{
		String_t* L_0 = ___typeInfo1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		Type_t ** L_3 = ___objectType2;
		*((Il2CppObject **)(L_3)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_3), (Il2CppObject *)NULL);
		Dictionary_2_t1662909226 ** L_4 = ___memberMap3;
		*((Il2CppObject **)(L_4)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_4), (Il2CppObject *)NULL);
		Il2CppObject * L_5 = ___result0;
		V_2 = L_5;
		goto IL_0043;
	}

IL_0017:
	{
		String_t* L_6 = ___typeInfo1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m402049910, L_6, (bool)0, "JsonFx.Json, Version=1.4.1003.3008, Culture=neutral, PublicKeyToken=null");
		V_0 = L_7;
		Type_t * L_8 = V_0;
		V_3 = (bool)((((Il2CppObject*)(Type_t *)L_8) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_9 = V_3;
		if (!L_9)
		{
			goto IL_0033;
		}
	}
	{
		Type_t ** L_10 = ___objectType2;
		*((Il2CppObject **)(L_10)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_10), (Il2CppObject *)NULL);
		Dictionary_2_t1662909226 ** L_11 = ___memberMap3;
		*((Il2CppObject **)(L_11)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_11), (Il2CppObject *)NULL);
		Il2CppObject * L_12 = ___result0;
		V_2 = L_12;
		goto IL_0043;
	}

IL_0033:
	{
		Type_t ** L_13 = ___objectType2;
		Type_t * L_14 = V_0;
		*((Il2CppObject **)(L_13)) = (Il2CppObject *)L_14;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_13), (Il2CppObject *)L_14);
		Type_t * L_15 = V_0;
		Il2CppObject * L_16 = ___result0;
		Dictionary_2_t1662909226 ** L_17 = ___memberMap3;
		Il2CppObject * L_18 = TypeCoercionUtility_CoerceType_m3531708896(__this, L_15, L_16, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		goto IL_0043;
	}

IL_0043:
	{
		Il2CppObject * L_19 = V_2;
		return L_19;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.TypeCoercionUtility::InstantiateObject(System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>&)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonTypeCoercionException_t1571553219_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TargetInvocationException_t4098620458_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3785196404;
extern Il2CppCodeGenString* _stringLiteral625868345;
extern Il2CppCodeGenString* _stringLiteral2675436571;
extern const uint32_t TypeCoercionUtility_InstantiateObject_m123346059_MetadataUsageId;
extern "C"  Il2CppObject * TypeCoercionUtility_InstantiateObject_m123346059 (TypeCoercionUtility_t3996825458 * __this, Type_t * ___objectType0, Dictionary_2_t1662909226 ** ___memberMap1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_InstantiateObject_m123346059_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ConstructorInfo_t2851816542 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	bool V_2 = false;
	bool V_3 = false;
	TargetInvocationException_t4098620458 * V_4 = NULL;
	bool V_5 = false;
	Il2CppObject * V_6 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B4_0 = 0;
	{
		Type_t * L_0 = ___objectType0;
		NullCheck(L_0);
		bool L_1 = Type_get_IsInterface_m3583817465(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0019;
		}
	}
	{
		Type_t * L_2 = ___objectType0;
		NullCheck(L_2);
		bool L_3 = Type_get_IsAbstract_m2532060002(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0019;
		}
	}
	{
		Type_t * L_4 = ___objectType0;
		NullCheck(L_4);
		bool L_5 = Type_get_IsValueType_m1733572463(L_4, /*hidden argument*/NULL);
		G_B4_0 = ((int32_t)(L_5));
		goto IL_001a;
	}

IL_0019:
	{
		G_B4_0 = 1;
	}

IL_001a:
	{
		V_2 = (bool)G_B4_0;
		bool L_6 = V_2;
		if (!L_6)
		{
			goto IL_0035;
		}
	}
	{
		Type_t * L_7 = ___objectType0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral3785196404, L_8, /*hidden argument*/NULL);
		JsonTypeCoercionException_t1571553219 * L_10 = (JsonTypeCoercionException_t1571553219 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t1571553219_il2cpp_TypeInfo_var);
		JsonTypeCoercionException__ctor_m695869114(L_10, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_0035:
	{
		Type_t * L_11 = ___objectType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		TypeU5BU5D_t1664964607* L_12 = ((Type_t_StaticFields*)Type_t_il2cpp_TypeInfo_var->static_fields)->get_EmptyTypes_3();
		NullCheck(L_11);
		ConstructorInfo_t2851816542 * L_13 = Type_GetConstructor_m132234455(L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		ConstructorInfo_t2851816542 * L_14 = V_0;
		V_3 = (bool)((((Il2CppObject*)(ConstructorInfo_t2851816542 *)L_14) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_15 = V_3;
		if (!L_15)
		{
			goto IL_0060;
		}
	}
	{
		Type_t * L_16 = ___objectType0;
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral625868345, L_17, /*hidden argument*/NULL);
		JsonTypeCoercionException_t1571553219 * L_19 = (JsonTypeCoercionException_t1571553219 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t1571553219_il2cpp_TypeInfo_var);
		JsonTypeCoercionException__ctor_m695869114(L_19, L_18, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_0060:
	{
	}

IL_0061:
	try
	{ // begin try (depth: 1)
		ConstructorInfo_t2851816542 * L_20 = V_0;
		NullCheck(L_20);
		Il2CppObject * L_21 = ConstructorInfo_Invoke_m2144827141(L_20, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL, /*hidden argument*/NULL);
		V_1 = L_21;
		goto IL_00b2;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (TargetInvocationException_t4098620458_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_006d;
		throw e;
	}

CATCH_006d:
	{ // begin catch(System.Reflection.TargetInvocationException)
		{
			V_4 = ((TargetInvocationException_t4098620458 *)__exception_local);
			TargetInvocationException_t4098620458 * L_22 = V_4;
			NullCheck(L_22);
			Exception_t1927440687 * L_23 = Exception_get_InnerException_m3722561235(L_22, /*hidden argument*/NULL);
			V_5 = (bool)((!(((Il2CppObject*)(Exception_t1927440687 *)L_23) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
			bool L_24 = V_5;
			if (!L_24)
			{
				goto IL_009a;
			}
		}

IL_0080:
		{
			TargetInvocationException_t4098620458 * L_25 = V_4;
			NullCheck(L_25);
			Exception_t1927440687 * L_26 = Exception_get_InnerException_m3722561235(L_25, /*hidden argument*/NULL);
			NullCheck(L_26);
			String_t* L_27 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_26);
			TargetInvocationException_t4098620458 * L_28 = V_4;
			NullCheck(L_28);
			Exception_t1927440687 * L_29 = Exception_get_InnerException_m3722561235(L_28, /*hidden argument*/NULL);
			JsonTypeCoercionException_t1571553219 * L_30 = (JsonTypeCoercionException_t1571553219 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t1571553219_il2cpp_TypeInfo_var);
			JsonTypeCoercionException__ctor_m1497116290(L_30, L_27, L_29, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_30);
		}

IL_009a:
		{
			Type_t * L_31 = ___objectType0;
			NullCheck(L_31);
			String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_31);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_33 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2675436571, L_32, /*hidden argument*/NULL);
			TargetInvocationException_t4098620458 * L_34 = V_4;
			JsonTypeCoercionException_t1571553219 * L_35 = (JsonTypeCoercionException_t1571553219 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t1571553219_il2cpp_TypeInfo_var);
			JsonTypeCoercionException__ctor_m1497116290(L_35, L_33, L_34, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_35);
		}
	} // end catch (depth: 1)

IL_00b2:
	{
		Dictionary_2_t1662909226 ** L_36 = ___memberMap1;
		Type_t * L_37 = ___objectType0;
		Dictionary_2_t1662909226 * L_38 = TypeCoercionUtility_GetMemberMap_m2996775296(__this, L_37, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_36)) = (Il2CppObject *)L_38;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_36), (Il2CppObject *)L_38);
		Il2CppObject * L_39 = V_1;
		V_6 = L_39;
		goto IL_00c0;
	}

IL_00c0:
	{
		Il2CppObject * L_40 = V_6;
		return L_40;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo> Pathfinding.Serialization.JsonFx.TypeCoercionUtility::GetMemberMap(System.Type)
extern const Il2CppType* IDictionary_t596158605_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeCoercionUtility_GetMemberMap_m2996775296_MetadataUsageId;
extern "C"  Dictionary_2_t1662909226 * TypeCoercionUtility_GetMemberMap_m2996775296 (TypeCoercionUtility_t3996825458 * __this, Type_t * ___objectType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_GetMemberMap_m2996775296_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Dictionary_2_t1662909226 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(IDictionary_t596158605_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_1 = ___objectType0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_0, L_1);
		V_0 = L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		V_1 = (Dictionary_2_t1662909226 *)NULL;
		goto IL_0025;
	}

IL_001a:
	{
		Type_t * L_4 = ___objectType0;
		Dictionary_2_t1662909226 * L_5 = TypeCoercionUtility_CreateMemberMap_m3120295732(__this, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0025;
	}

IL_0025:
	{
		Dictionary_2_t1662909226 * L_6 = V_1;
		return L_6;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo> Pathfinding.Serialization.JsonFx.TypeCoercionUtility::CreateMemberMap(System.Type)
extern Il2CppClass* Dictionary_2_t1662909226_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m1974758459_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m280163940_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m1350612299_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m167158859_MethodInfo_var;
extern const uint32_t TypeCoercionUtility_CreateMemberMap_m3120295732_MetadataUsageId;
extern "C"  Dictionary_2_t1662909226 * TypeCoercionUtility_CreateMemberMap_m3120295732 (TypeCoercionUtility_t3996825458 * __this, Type_t * ___objectType0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_CreateMemberMap_m3120295732_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t1662909226 * V_0 = NULL;
	PropertyInfoU5BU5D_t1736152084* V_1 = NULL;
	FieldInfoU5BU5D_t125053523* V_2 = NULL;
	bool V_3 = false;
	Dictionary_2_t1662909226 * V_4 = NULL;
	PropertyInfoU5BU5D_t1736152084* V_5 = NULL;
	int32_t V_6 = 0;
	PropertyInfo_t * V_7 = NULL;
	String_t* V_8 = NULL;
	bool V_9 = false;
	bool V_10 = false;
	bool V_11 = false;
	FieldInfoU5BU5D_t125053523* V_12 = NULL;
	int32_t V_13 = 0;
	FieldInfo_t * V_14 = NULL;
	String_t* V_15 = NULL;
	bool V_16 = false;
	bool V_17 = false;
	bool V_18 = false;
	int32_t G_B6_0 = 0;
	{
		Dictionary_2_t3600267123 * L_0 = TypeCoercionUtility_get_MemberMapCache_m1544955808(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___objectType0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m1974758459(L_0, L_1, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m1974758459_MethodInfo_var);
		V_3 = L_2;
		bool L_3 = V_3;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		Dictionary_2_t1662909226 * L_4 = V_0;
		V_4 = L_4;
		goto IL_0143;
	}

IL_001c:
	{
		Dictionary_2_t1662909226 * L_5 = (Dictionary_2_t1662909226 *)il2cpp_codegen_object_new(Dictionary_2_t1662909226_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m280163940(L_5, /*hidden argument*/Dictionary_2__ctor_m280163940_MethodInfo_var);
		V_0 = L_5;
		Type_t * L_6 = ___objectType0;
		NullCheck(L_6);
		PropertyInfoU5BU5D_t1736152084* L_7 = Type_GetProperties_m2803026104(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		PropertyInfoU5BU5D_t1736152084* L_8 = V_1;
		V_5 = L_8;
		V_6 = 0;
		goto IL_00a7;
	}

IL_0032:
	{
		PropertyInfoU5BU5D_t1736152084* L_9 = V_5;
		int32_t L_10 = V_6;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		PropertyInfo_t * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		V_7 = L_12;
		PropertyInfo_t * L_13 = V_7;
		NullCheck(L_13);
		bool L_14 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Reflection.PropertyInfo::get_CanRead() */, L_13);
		if (!L_14)
		{
			goto IL_004f;
		}
	}
	{
		PropertyInfo_t * L_15 = V_7;
		NullCheck(L_15);
		bool L_16 = VirtFuncInvoker0< bool >::Invoke(16 /* System.Boolean System.Reflection.PropertyInfo::get_CanWrite() */, L_15);
		G_B6_0 = ((((int32_t)L_16) == ((int32_t)0))? 1 : 0);
		goto IL_0050;
	}

IL_004f:
	{
		G_B6_0 = 1;
	}

IL_0050:
	{
		V_9 = (bool)G_B6_0;
		bool L_17 = V_9;
		if (!L_17)
		{
			goto IL_0059;
		}
	}
	{
		goto IL_00a1;
	}

IL_0059:
	{
		PropertyInfo_t * L_18 = V_7;
		bool L_19 = JsonIgnoreAttribute_IsJsonIgnore_m3752554121(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		V_10 = L_19;
		bool L_20 = V_10;
		if (!L_20)
		{
			goto IL_0069;
		}
	}
	{
		goto IL_00a1;
	}

IL_0069:
	{
		PropertyInfo_t * L_21 = V_7;
		String_t* L_22 = JsonNameAttribute_GetJsonName_m3020263406(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		V_8 = L_22;
		String_t* L_23 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_24 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		V_11 = L_24;
		bool L_25 = V_11;
		if (!L_25)
		{
			goto IL_0093;
		}
	}
	{
		Dictionary_2_t1662909226 * L_26 = V_0;
		PropertyInfo_t * L_27 = V_7;
		NullCheck(L_27);
		String_t* L_28 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_27);
		PropertyInfo_t * L_29 = V_7;
		NullCheck(L_26);
		Dictionary_2_set_Item_m1350612299(L_26, L_28, L_29, /*hidden argument*/Dictionary_2_set_Item_m1350612299_MethodInfo_var);
		goto IL_00a0;
	}

IL_0093:
	{
		Dictionary_2_t1662909226 * L_30 = V_0;
		String_t* L_31 = V_8;
		PropertyInfo_t * L_32 = V_7;
		NullCheck(L_30);
		Dictionary_2_set_Item_m1350612299(L_30, L_31, L_32, /*hidden argument*/Dictionary_2_set_Item_m1350612299_MethodInfo_var);
	}

IL_00a0:
	{
	}

IL_00a1:
	{
		int32_t L_33 = V_6;
		V_6 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00a7:
	{
		int32_t L_34 = V_6;
		PropertyInfoU5BU5D_t1736152084* L_35 = V_5;
		NullCheck(L_35);
		if ((((int32_t)L_34) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_35)->max_length)))))))
		{
			goto IL_0032;
		}
	}
	{
		Type_t * L_36 = ___objectType0;
		NullCheck(L_36);
		FieldInfoU5BU5D_t125053523* L_37 = Type_GetFields_m445058499(L_36, /*hidden argument*/NULL);
		V_2 = L_37;
		FieldInfoU5BU5D_t125053523* L_38 = V_2;
		V_12 = L_38;
		V_13 = 0;
		goto IL_0128;
	}

IL_00bf:
	{
		FieldInfoU5BU5D_t125053523* L_39 = V_12;
		int32_t L_40 = V_13;
		NullCheck(L_39);
		int32_t L_41 = L_40;
		FieldInfo_t * L_42 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		V_14 = L_42;
		FieldInfo_t * L_43 = V_14;
		NullCheck(L_43);
		bool L_44 = FieldInfo_get_IsPublic_m618896610(L_43, /*hidden argument*/NULL);
		V_16 = (bool)((((int32_t)L_44) == ((int32_t)0))? 1 : 0);
		bool L_45 = V_16;
		if (!L_45)
		{
			goto IL_00da;
		}
	}
	{
		goto IL_0122;
	}

IL_00da:
	{
		FieldInfo_t * L_46 = V_14;
		bool L_47 = JsonIgnoreAttribute_IsJsonIgnore_m3752554121(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		V_17 = L_47;
		bool L_48 = V_17;
		if (!L_48)
		{
			goto IL_00ea;
		}
	}
	{
		goto IL_0122;
	}

IL_00ea:
	{
		FieldInfo_t * L_49 = V_14;
		String_t* L_50 = JsonNameAttribute_GetJsonName_m3020263406(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		V_15 = L_50;
		String_t* L_51 = V_15;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_52 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		V_18 = L_52;
		bool L_53 = V_18;
		if (!L_53)
		{
			goto IL_0114;
		}
	}
	{
		Dictionary_2_t1662909226 * L_54 = V_0;
		FieldInfo_t * L_55 = V_14;
		NullCheck(L_55);
		String_t* L_56 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_55);
		FieldInfo_t * L_57 = V_14;
		NullCheck(L_54);
		Dictionary_2_set_Item_m1350612299(L_54, L_56, L_57, /*hidden argument*/Dictionary_2_set_Item_m1350612299_MethodInfo_var);
		goto IL_0121;
	}

IL_0114:
	{
		Dictionary_2_t1662909226 * L_58 = V_0;
		String_t* L_59 = V_15;
		FieldInfo_t * L_60 = V_14;
		NullCheck(L_58);
		Dictionary_2_set_Item_m1350612299(L_58, L_59, L_60, /*hidden argument*/Dictionary_2_set_Item_m1350612299_MethodInfo_var);
	}

IL_0121:
	{
	}

IL_0122:
	{
		int32_t L_61 = V_13;
		V_13 = ((int32_t)((int32_t)L_61+(int32_t)1));
	}

IL_0128:
	{
		int32_t L_62 = V_13;
		FieldInfoU5BU5D_t125053523* L_63 = V_12;
		NullCheck(L_63);
		if ((((int32_t)L_62) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_63)->max_length)))))))
		{
			goto IL_00bf;
		}
	}
	{
		Dictionary_2_t3600267123 * L_64 = TypeCoercionUtility_get_MemberMapCache_m1544955808(__this, /*hidden argument*/NULL);
		Type_t * L_65 = ___objectType0;
		Dictionary_2_t1662909226 * L_66 = V_0;
		NullCheck(L_64);
		Dictionary_2_set_Item_m167158859(L_64, L_65, L_66, /*hidden argument*/Dictionary_2_set_Item_m167158859_MethodInfo_var);
		Dictionary_2_t1662909226 * L_67 = V_0;
		V_4 = L_67;
		goto IL_0143;
	}

IL_0143:
	{
		Dictionary_2_t1662909226 * L_68 = V_4;
		return L_68;
	}
}
// System.Type Pathfinding.Serialization.JsonFx.TypeCoercionUtility::GetMemberInfo(System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>,System.String,System.Reflection.MemberInfo&)
extern Il2CppClass* PropertyInfo_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FieldInfo_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m1048848123_MethodInfo_var;
extern const uint32_t TypeCoercionUtility_GetMemberInfo_m658257860_MetadataUsageId;
extern "C"  Type_t * TypeCoercionUtility_GetMemberInfo_m658257860 (Il2CppObject * __this /* static, unused */, Dictionary_2_t1662909226 * ___memberMap0, String_t* ___memberName1, MemberInfo_t ** ___memberInfo2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_GetMemberInfo_m658257860_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	Type_t * V_2 = NULL;
	bool V_3 = false;
	int32_t G_B3_0 = 0;
	{
		Dictionary_2_t1662909226 * L_0 = ___memberMap0;
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		Dictionary_2_t1662909226 * L_1 = ___memberMap0;
		String_t* L_2 = ___memberName1;
		MemberInfo_t ** L_3 = ___memberInfo2;
		NullCheck(L_1);
		bool L_4 = Dictionary_2_TryGetValue_m1048848123(L_1, L_2, L_3, /*hidden argument*/Dictionary_2_TryGetValue_m1048848123_MethodInfo_var);
		G_B3_0 = ((int32_t)(L_4));
		goto IL_000f;
	}

IL_000e:
	{
		G_B3_0 = 0;
	}

IL_000f:
	{
		V_0 = (bool)G_B3_0;
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_0051;
		}
	}
	{
		MemberInfo_t ** L_6 = ___memberInfo2;
		V_1 = (bool)((!(((Il2CppObject*)(PropertyInfo_t *)((PropertyInfo_t *)IsInstClass((*((MemberInfo_t **)L_6)), PropertyInfo_t_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_7 = V_1;
		if (!L_7)
		{
			goto IL_0032;
		}
	}
	{
		MemberInfo_t ** L_8 = ___memberInfo2;
		NullCheck(((PropertyInfo_t *)CastclassClass((*((MemberInfo_t **)L_8)), PropertyInfo_t_il2cpp_TypeInfo_var)));
		Type_t * L_9 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, ((PropertyInfo_t *)CastclassClass((*((MemberInfo_t **)L_8)), PropertyInfo_t_il2cpp_TypeInfo_var)));
		V_2 = L_9;
		goto IL_0058;
	}

IL_0032:
	{
		MemberInfo_t ** L_10 = ___memberInfo2;
		V_3 = (bool)((!(((Il2CppObject*)(FieldInfo_t *)((FieldInfo_t *)IsInstClass((*((MemberInfo_t **)L_10)), FieldInfo_t_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_11 = V_3;
		if (!L_11)
		{
			goto IL_0050;
		}
	}
	{
		MemberInfo_t ** L_12 = ___memberInfo2;
		NullCheck(((FieldInfo_t *)CastclassClass((*((MemberInfo_t **)L_12)), FieldInfo_t_il2cpp_TypeInfo_var)));
		Type_t * L_13 = VirtFuncInvoker0< Type_t * >::Invoke(16 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, ((FieldInfo_t *)CastclassClass((*((MemberInfo_t **)L_12)), FieldInfo_t_il2cpp_TypeInfo_var)));
		V_2 = L_13;
		goto IL_0058;
	}

IL_0050:
	{
	}

IL_0051:
	{
		MemberInfo_t ** L_14 = ___memberInfo2;
		*((Il2CppObject **)(L_14)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_14), (Il2CppObject *)NULL);
		V_2 = (Type_t *)NULL;
		goto IL_0058;
	}

IL_0058:
	{
		Type_t * L_15 = V_2;
		return L_15;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.TypeCoercionUtility::SetMemberValue(System.Object,System.Type,System.Reflection.MemberInfo,System.Object)
extern Il2CppClass* PropertyInfo_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FieldInfo_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeCoercionUtility_SetMemberValue_m1207607791_MetadataUsageId;
extern "C"  void TypeCoercionUtility_SetMemberValue_m1207607791 (TypeCoercionUtility_t3996825458 * __this, Il2CppObject * ___result0, Type_t * ___memberType1, MemberInfo_t * ___memberInfo2, Il2CppObject * ___value3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_SetMemberValue_m1207607791_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		MemberInfo_t * L_0 = ___memberInfo2;
		V_0 = (bool)((!(((Il2CppObject*)(PropertyInfo_t *)((PropertyInfo_t *)IsInstClass(L_0, PropertyInfo_t_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		MemberInfo_t * L_2 = ___memberInfo2;
		Il2CppObject * L_3 = ___result0;
		Type_t * L_4 = ___memberType1;
		Il2CppObject * L_5 = ___value3;
		Il2CppObject * L_6 = TypeCoercionUtility_CoerceType_m3744809450(__this, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(((PropertyInfo_t *)CastclassClass(L_2, PropertyInfo_t_il2cpp_TypeInfo_var)));
		VirtActionInvoker3< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t3614634134* >::Invoke(25 /* System.Void System.Reflection.PropertyInfo::SetValue(System.Object,System.Object,System.Object[]) */, ((PropertyInfo_t *)CastclassClass(L_2, PropertyInfo_t_il2cpp_TypeInfo_var)), L_3, L_6, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL);
		goto IL_004e;
	}

IL_0029:
	{
		MemberInfo_t * L_7 = ___memberInfo2;
		V_1 = (bool)((!(((Il2CppObject*)(FieldInfo_t *)((FieldInfo_t *)IsInstClass(L_7, FieldInfo_t_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_8 = V_1;
		if (!L_8)
		{
			goto IL_004e;
		}
	}
	{
		MemberInfo_t * L_9 = ___memberInfo2;
		Il2CppObject * L_10 = ___result0;
		Type_t * L_11 = ___memberType1;
		Il2CppObject * L_12 = ___value3;
		Il2CppObject * L_13 = TypeCoercionUtility_CoerceType_m3744809450(__this, L_11, L_12, /*hidden argument*/NULL);
		NullCheck(((FieldInfo_t *)CastclassClass(L_9, FieldInfo_t_il2cpp_TypeInfo_var)));
		FieldInfo_SetValue_m2504255891(((FieldInfo_t *)CastclassClass(L_9, FieldInfo_t_il2cpp_TypeInfo_var)), L_10, L_13, /*hidden argument*/NULL);
	}

IL_004e:
	{
		return;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.TypeCoercionUtility::CoerceType(System.Type,System.Object)
extern const Il2CppType* IEnumerable_t2911409499_0_0_0_var;
extern const Il2CppType* DateTime_t693205669_0_0_0_var;
extern const Il2CppType* Guid_t2533601593_0_0_0_var;
extern const Il2CppType* Char_t3454481338_0_0_0_var;
extern const Il2CppType* Uri_t19570940_0_0_0_var;
extern const Il2CppType* Version_t1755874712_0_0_0_var;
extern const Il2CppType* TimeSpan_t3430258949_0_0_0_var;
extern const Il2CppType* Int64_t909078037_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonTypeCoercionException_t1571553219_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTimeFormatInfo_t2187473504_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* Guid_t2533601593_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* Version_t1755874712_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeSpan_t3430258949_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeDescriptor_t3595688691_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral961691697;
extern Il2CppCodeGenString* _stringLiteral1098351937;
extern const uint32_t TypeCoercionUtility_CoerceType_m3744809450_MetadataUsageId;
extern "C"  Il2CppObject * TypeCoercionUtility_CoerceType_m3744809450 (TypeCoercionUtility_t3996825458 * __this, Type_t * ___targetType0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_CoerceType_m3744809450_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Type_t * V_1 = NULL;
	TypeConverter_t745995970 * V_2 = NULL;
	bool V_3 = false;
	bool V_4 = false;
	Il2CppObject * V_5 = NULL;
	bool V_6 = false;
	TypeU5BU5D_t1664964607* V_7 = NULL;
	bool V_8 = false;
	bool V_9 = false;
	bool V_10 = false;
	bool V_11 = false;
	bool V_12 = false;
	FieldInfoU5BU5D_t125053523* V_13 = NULL;
	int32_t V_14 = 0;
	FieldInfo_t * V_15 = NULL;
	String_t* V_16 = NULL;
	bool V_17 = false;
	bool V_18 = false;
	Dictionary_2_t1662909226 * V_19 = NULL;
	bool V_20 = false;
	bool V_21 = false;
	bool V_22 = false;
	DateTime_t693205669  V_23;
	memset(&V_23, 0, sizeof(V_23));
	bool V_24 = false;
	bool V_25 = false;
	bool V_26 = false;
	bool V_27 = false;
	bool V_28 = false;
	Uri_t19570940 * V_29 = NULL;
	bool V_30 = false;
	bool V_31 = false;
	bool V_32 = false;
	bool V_33 = false;
	bool V_34 = false;
	Exception_t1927440687 * V_35 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B5_0 = 0;
	int32_t G_B30_0 = 0;
	{
		Type_t * L_0 = ___targetType0;
		bool L_1 = TypeCoercionUtility_IsNullable_m1309334937(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppObject * L_2 = ___value1;
		V_3 = (bool)((((Il2CppObject*)(Il2CppObject *)L_2) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_3 = V_3;
		if (!L_3)
		{
			goto IL_004d;
		}
	}
	{
		bool L_4 = __this->get_allowNullValueTypes_1();
		if (L_4)
		{
			goto IL_0027;
		}
	}
	{
		Type_t * L_5 = ___targetType0;
		NullCheck(L_5);
		bool L_6 = Type_get_IsValueType_m1733572463(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0027;
		}
	}
	{
		bool L_7 = V_0;
		G_B5_0 = ((((int32_t)L_7) == ((int32_t)0))? 1 : 0);
		goto IL_0028;
	}

IL_0027:
	{
		G_B5_0 = 0;
	}

IL_0028:
	{
		V_4 = (bool)G_B5_0;
		bool L_8 = V_4;
		if (!L_8)
		{
			goto IL_0045;
		}
	}
	{
		Type_t * L_9 = ___targetType0;
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral961691697, L_10, /*hidden argument*/NULL);
		JsonTypeCoercionException_t1571553219 * L_12 = (JsonTypeCoercionException_t1571553219 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t1571553219_il2cpp_TypeInfo_var);
		JsonTypeCoercionException__ctor_m695869114(L_12, L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0045:
	{
		Il2CppObject * L_13 = ___value1;
		V_5 = L_13;
		goto IL_0379;
	}

IL_004d:
	{
		bool L_14 = V_0;
		V_6 = L_14;
		bool L_15 = V_6;
		if (!L_15)
		{
			goto IL_0073;
		}
	}
	{
		Type_t * L_16 = ___targetType0;
		NullCheck(L_16);
		TypeU5BU5D_t1664964607* L_17 = VirtFuncInvoker0< TypeU5BU5D_t1664964607* >::Invoke(86 /* System.Type[] System.Type::GetGenericArguments() */, L_16);
		V_7 = L_17;
		TypeU5BU5D_t1664964607* L_18 = V_7;
		NullCheck(L_18);
		V_8 = (bool)((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length))))) == ((int32_t)1))? 1 : 0);
		bool L_19 = V_8;
		if (!L_19)
		{
			goto IL_0072;
		}
	}
	{
		TypeU5BU5D_t1664964607* L_20 = V_7;
		NullCheck(L_20);
		int32_t L_21 = 0;
		Type_t * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		___targetType0 = L_22;
	}

IL_0072:
	{
	}

IL_0073:
	{
		Il2CppObject * L_23 = ___value1;
		NullCheck(L_23);
		Type_t * L_24 = Object_GetType_m191970594(L_23, /*hidden argument*/NULL);
		V_1 = L_24;
		Type_t * L_25 = ___targetType0;
		Type_t * L_26 = V_1;
		NullCheck(L_25);
		bool L_27 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_25, L_26);
		V_9 = L_27;
		bool L_28 = V_9;
		if (!L_28)
		{
			goto IL_0090;
		}
	}
	{
		Il2CppObject * L_29 = ___value1;
		V_5 = L_29;
		goto IL_0379;
	}

IL_0090:
	{
		Type_t * L_30 = ___targetType0;
		NullCheck(L_30);
		bool L_31 = Type_get_IsEnum_m313908919(L_30, /*hidden argument*/NULL);
		V_10 = L_31;
		bool L_32 = V_10;
		if (!L_32)
		{
			goto IL_0140;
		}
	}
	{
		Il2CppObject * L_33 = ___value1;
		V_11 = (bool)((!(((Il2CppObject*)(String_t*)((String_t*)IsInstSealed(L_33, String_t_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_34 = V_11;
		if (!L_34)
		{
			goto IL_0122;
		}
	}
	{
		Type_t * L_35 = ___targetType0;
		Il2CppObject * L_36 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		bool L_37 = Enum_IsDefined_m92789062(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		V_12 = (bool)((((int32_t)L_37) == ((int32_t)0))? 1 : 0);
		bool L_38 = V_12;
		if (!L_38)
		{
			goto IL_010f;
		}
	}
	{
		Type_t * L_39 = ___targetType0;
		NullCheck(L_39);
		FieldInfoU5BU5D_t125053523* L_40 = Type_GetFields_m445058499(L_39, /*hidden argument*/NULL);
		V_13 = L_40;
		V_14 = 0;
		goto IL_0106;
	}

IL_00cf:
	{
		FieldInfoU5BU5D_t125053523* L_41 = V_13;
		int32_t L_42 = V_14;
		NullCheck(L_41);
		int32_t L_43 = L_42;
		FieldInfo_t * L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		V_15 = L_44;
		FieldInfo_t * L_45 = V_15;
		String_t* L_46 = JsonNameAttribute_GetJsonName_m3020263406(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		V_16 = L_46;
		Il2CppObject * L_47 = ___value1;
		String_t* L_48 = V_16;
		NullCheck(((String_t*)CastclassSealed(L_47, String_t_il2cpp_TypeInfo_var)));
		bool L_49 = String_Equals_m2633592423(((String_t*)CastclassSealed(L_47, String_t_il2cpp_TypeInfo_var)), L_48, /*hidden argument*/NULL);
		V_17 = L_49;
		bool L_50 = V_17;
		if (!L_50)
		{
			goto IL_00ff;
		}
	}
	{
		FieldInfo_t * L_51 = V_15;
		NullCheck(L_51);
		String_t* L_52 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_51);
		___value1 = L_52;
		goto IL_010e;
	}

IL_00ff:
	{
		int32_t L_53 = V_14;
		V_14 = ((int32_t)((int32_t)L_53+(int32_t)1));
	}

IL_0106:
	{
		int32_t L_54 = V_14;
		FieldInfoU5BU5D_t125053523* L_55 = V_13;
		NullCheck(L_55);
		if ((((int32_t)L_54) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_55)->max_length)))))))
		{
			goto IL_00cf;
		}
	}

IL_010e:
	{
	}

IL_010f:
	{
		Type_t * L_56 = ___targetType0;
		Il2CppObject * L_57 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Il2CppObject * L_58 = Enum_Parse_m2561000069(NULL /*static, unused*/, L_56, ((String_t*)CastclassSealed(L_57, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_5 = L_58;
		goto IL_0379;
	}

IL_0122:
	{
		Type_t * L_59 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Type_t * L_60 = Enum_GetUnderlyingType_m3513899012(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		Il2CppObject * L_61 = ___value1;
		Il2CppObject * L_62 = TypeCoercionUtility_CoerceType_m3744809450(__this, L_60, L_61, /*hidden argument*/NULL);
		___value1 = L_62;
		Type_t * L_63 = ___targetType0;
		Il2CppObject * L_64 = ___value1;
		Il2CppObject * L_65 = Enum_ToObject_m2460371738(NULL /*static, unused*/, L_63, L_64, /*hidden argument*/NULL);
		V_5 = L_65;
		goto IL_0379;
	}

IL_0140:
	{
		Il2CppObject * L_66 = ___value1;
		V_18 = (bool)((!(((Il2CppObject*)(Il2CppObject *)((Il2CppObject *)IsInst(L_66, IDictionary_t596158605_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_67 = V_18;
		if (!L_67)
		{
			goto IL_0166;
		}
	}
	{
		Type_t * L_68 = ___targetType0;
		Il2CppObject * L_69 = ___value1;
		Il2CppObject * L_70 = TypeCoercionUtility_CoerceType_m3531708896(__this, L_68, ((Il2CppObject *)Castclass(L_69, IDictionary_t596158605_il2cpp_TypeInfo_var)), (&V_19), /*hidden argument*/NULL);
		V_5 = L_70;
		goto IL_0379;
	}

IL_0166:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_71 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(IEnumerable_t2911409499_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_72 = ___targetType0;
		NullCheck(L_71);
		bool L_73 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_71, L_72);
		if (!L_73)
		{
			goto IL_018a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_74 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(IEnumerable_t2911409499_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_75 = V_1;
		NullCheck(L_74);
		bool L_76 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_74, L_75);
		G_B30_0 = ((int32_t)(L_76));
		goto IL_018b;
	}

IL_018a:
	{
		G_B30_0 = 0;
	}

IL_018b:
	{
		V_20 = (bool)G_B30_0;
		bool L_77 = V_20;
		if (!L_77)
		{
			goto IL_01a7;
		}
	}
	{
		Type_t * L_78 = ___targetType0;
		Type_t * L_79 = V_1;
		Il2CppObject * L_80 = ___value1;
		Il2CppObject * L_81 = TypeCoercionUtility_CoerceList_m2435065360(__this, L_78, L_79, ((Il2CppObject *)Castclass(L_80, IEnumerable_t2911409499_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_5 = L_81;
		goto IL_0379;
	}

IL_01a7:
	{
		Il2CppObject * L_82 = ___value1;
		V_21 = (bool)((!(((Il2CppObject*)(String_t*)((String_t*)IsInstSealed(L_82, String_t_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_83 = V_21;
		if (!L_83)
		{
			goto IL_02cd;
		}
	}
	{
		Type_t * L_84 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_85 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(DateTime_t693205669_0_0_0_var), /*hidden argument*/NULL);
		V_22 = (bool)((((Il2CppObject*)(Type_t *)L_84) == ((Il2CppObject*)(Type_t *)L_85))? 1 : 0);
		bool L_86 = V_22;
		if (!L_86)
		{
			goto IL_0200;
		}
	}
	{
		Il2CppObject * L_87 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(DateTimeFormatInfo_t2187473504_il2cpp_TypeInfo_var);
		DateTimeFormatInfo_t2187473504 * L_88 = DateTimeFormatInfo_get_InvariantInfo_m1865598692(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		bool L_89 = DateTime_TryParse_m2173097299(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_87, String_t_il2cpp_TypeInfo_var)), L_88, ((int32_t)143), (&V_23), /*hidden argument*/NULL);
		V_24 = L_89;
		bool L_90 = V_24;
		if (!L_90)
		{
			goto IL_01fa;
		}
	}
	{
		DateTime_t693205669  L_91 = V_23;
		DateTime_t693205669  L_92 = L_91;
		Il2CppObject * L_93 = Box(DateTime_t693205669_il2cpp_TypeInfo_var, &L_92);
		V_5 = L_93;
		goto IL_0379;
	}

IL_01fa:
	{
		goto IL_02ca;
	}

IL_0200:
	{
		Type_t * L_94 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_95 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Guid_t2533601593_0_0_0_var), /*hidden argument*/NULL);
		V_25 = (bool)((((Il2CppObject*)(Type_t *)L_94) == ((Il2CppObject*)(Type_t *)L_95))? 1 : 0);
		bool L_96 = V_25;
		if (!L_96)
		{
			goto IL_022b;
		}
	}
	{
		Il2CppObject * L_97 = ___value1;
		Guid_t2533601593  L_98;
		memset(&L_98, 0, sizeof(L_98));
		Guid__ctor_m2599802704(&L_98, ((String_t*)CastclassSealed(L_97, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Guid_t2533601593  L_99 = L_98;
		Il2CppObject * L_100 = Box(Guid_t2533601593_il2cpp_TypeInfo_var, &L_99);
		V_5 = L_100;
		goto IL_0379;
	}

IL_022b:
	{
		Type_t * L_101 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_102 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Char_t3454481338_0_0_0_var), /*hidden argument*/NULL);
		V_26 = (bool)((((Il2CppObject*)(Type_t *)L_101) == ((Il2CppObject*)(Type_t *)L_102))? 1 : 0);
		bool L_103 = V_26;
		if (!L_103)
		{
			goto IL_026f;
		}
	}
	{
		Il2CppObject * L_104 = ___value1;
		NullCheck(((String_t*)CastclassSealed(L_104, String_t_il2cpp_TypeInfo_var)));
		int32_t L_105 = String_get_Length_m1606060069(((String_t*)CastclassSealed(L_104, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_27 = (bool)((((int32_t)L_105) == ((int32_t)1))? 1 : 0);
		bool L_106 = V_27;
		if (!L_106)
		{
			goto IL_026c;
		}
	}
	{
		Il2CppObject * L_107 = ___value1;
		NullCheck(((String_t*)CastclassSealed(L_107, String_t_il2cpp_TypeInfo_var)));
		Il2CppChar L_108 = String_get_Chars_m4230566705(((String_t*)CastclassSealed(L_107, String_t_il2cpp_TypeInfo_var)), 0, /*hidden argument*/NULL);
		Il2CppChar L_109 = L_108;
		Il2CppObject * L_110 = Box(Char_t3454481338_il2cpp_TypeInfo_var, &L_109);
		V_5 = L_110;
		goto IL_0379;
	}

IL_026c:
	{
		goto IL_02ca;
	}

IL_026f:
	{
		Type_t * L_111 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_112 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Uri_t19570940_0_0_0_var), /*hidden argument*/NULL);
		V_28 = (bool)((((Il2CppObject*)(Type_t *)L_111) == ((Il2CppObject*)(Type_t *)L_112))? 1 : 0);
		bool L_113 = V_28;
		if (!L_113)
		{
			goto IL_02a4;
		}
	}
	{
		Il2CppObject * L_114 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t19570940_il2cpp_TypeInfo_var);
		bool L_115 = Uri_TryCreate_m16561530(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_114, String_t_il2cpp_TypeInfo_var)), 0, (&V_29), /*hidden argument*/NULL);
		V_30 = L_115;
		bool L_116 = V_30;
		if (!L_116)
		{
			goto IL_02a1;
		}
	}
	{
		Uri_t19570940 * L_117 = V_29;
		V_5 = L_117;
		goto IL_0379;
	}

IL_02a1:
	{
		goto IL_02ca;
	}

IL_02a4:
	{
		Type_t * L_118 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_119 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Version_t1755874712_0_0_0_var), /*hidden argument*/NULL);
		V_31 = (bool)((((Il2CppObject*)(Type_t *)L_118) == ((Il2CppObject*)(Type_t *)L_119))? 1 : 0);
		bool L_120 = V_31;
		if (!L_120)
		{
			goto IL_02ca;
		}
	}
	{
		Il2CppObject * L_121 = ___value1;
		Version_t1755874712 * L_122 = (Version_t1755874712 *)il2cpp_codegen_object_new(Version_t1755874712_il2cpp_TypeInfo_var);
		Version__ctor_m2972638031(L_122, ((String_t*)CastclassSealed(L_121, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_5 = L_122;
		goto IL_0379;
	}

IL_02ca:
	{
		goto IL_0305;
	}

IL_02cd:
	{
		Type_t * L_123 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_124 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(TimeSpan_t3430258949_0_0_0_var), /*hidden argument*/NULL);
		V_32 = (bool)((((Il2CppObject*)(Type_t *)L_123) == ((Il2CppObject*)(Type_t *)L_124))? 1 : 0);
		bool L_125 = V_32;
		if (!L_125)
		{
			goto IL_0305;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_126 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Int64_t909078037_0_0_0_var), /*hidden argument*/NULL);
		Il2CppObject * L_127 = ___value1;
		Il2CppObject * L_128 = TypeCoercionUtility_CoerceType_m3744809450(__this, L_126, L_127, /*hidden argument*/NULL);
		TimeSpan_t3430258949  L_129;
		memset(&L_129, 0, sizeof(L_129));
		TimeSpan__ctor_m96381766(&L_129, ((*(int64_t*)((int64_t*)UnBox (L_128, Int64_t909078037_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		TimeSpan_t3430258949  L_130 = L_129;
		Il2CppObject * L_131 = Box(TimeSpan_t3430258949_il2cpp_TypeInfo_var, &L_130);
		V_5 = L_131;
		goto IL_0379;
	}

IL_0305:
	{
		Type_t * L_132 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(TypeDescriptor_t3595688691_il2cpp_TypeInfo_var);
		TypeConverter_t745995970 * L_133 = TypeDescriptor_GetConverter_m1788154595(NULL /*static, unused*/, L_132, /*hidden argument*/NULL);
		V_2 = L_133;
		TypeConverter_t745995970 * L_134 = V_2;
		Type_t * L_135 = V_1;
		NullCheck(L_134);
		bool L_136 = TypeConverter_CanConvertFrom_m1824018217(L_134, L_135, /*hidden argument*/NULL);
		V_33 = L_136;
		bool L_137 = V_33;
		if (!L_137)
		{
			goto IL_0325;
		}
	}
	{
		TypeConverter_t745995970 * L_138 = V_2;
		Il2CppObject * L_139 = ___value1;
		NullCheck(L_138);
		Il2CppObject * L_140 = TypeConverter_ConvertFrom_m757293553(L_138, L_139, /*hidden argument*/NULL);
		V_5 = L_140;
		goto IL_0379;
	}

IL_0325:
	{
		Type_t * L_141 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(TypeDescriptor_t3595688691_il2cpp_TypeInfo_var);
		TypeConverter_t745995970 * L_142 = TypeDescriptor_GetConverter_m1788154595(NULL /*static, unused*/, L_141, /*hidden argument*/NULL);
		V_2 = L_142;
		TypeConverter_t745995970 * L_143 = V_2;
		Type_t * L_144 = ___targetType0;
		NullCheck(L_143);
		bool L_145 = TypeConverter_CanConvertTo_m3984515018(L_143, L_144, /*hidden argument*/NULL);
		V_34 = L_145;
		bool L_146 = V_34;
		if (!L_146)
		{
			goto IL_0346;
		}
	}
	{
		TypeConverter_t745995970 * L_147 = V_2;
		Il2CppObject * L_148 = ___value1;
		Type_t * L_149 = ___targetType0;
		NullCheck(L_147);
		Il2CppObject * L_150 = TypeConverter_ConvertTo_m2094680623(L_147, L_148, L_149, /*hidden argument*/NULL);
		V_5 = L_150;
		goto IL_0379;
	}

IL_0346:
	{
	}

IL_0347:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_151 = ___value1;
		Type_t * L_152 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_153 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, L_151, L_152, /*hidden argument*/NULL);
		V_5 = L_153;
		goto IL_0379;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0353;
		throw e;
	}

CATCH_0353:
	{ // begin catch(System.Exception)
		V_35 = ((Exception_t1927440687 *)__exception_local);
		Il2CppObject * L_154 = ___value1;
		NullCheck(L_154);
		Type_t * L_155 = Object_GetType_m191970594(L_154, /*hidden argument*/NULL);
		NullCheck(L_155);
		String_t* L_156 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_155);
		Type_t * L_157 = ___targetType0;
		NullCheck(L_157);
		String_t* L_158 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_157);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_159 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral1098351937, L_156, L_158, /*hidden argument*/NULL);
		Exception_t1927440687 * L_160 = V_35;
		JsonTypeCoercionException_t1571553219 * L_161 = (JsonTypeCoercionException_t1571553219 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t1571553219_il2cpp_TypeInfo_var);
		JsonTypeCoercionException__ctor_m1497116290(L_161, L_159, L_160, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_161);
	} // end catch (depth: 1)

IL_0379:
	{
		Il2CppObject * L_162 = V_5;
		return L_162;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.TypeCoercionUtility::CoerceType(System.Type,System.Collections.IDictionary,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>&)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t TypeCoercionUtility_CoerceType_m3531708896_MetadataUsageId;
extern "C"  Il2CppObject * TypeCoercionUtility_CoerceType_m3531708896 (TypeCoercionUtility_t3996825458 * __this, Type_t * ___targetType0, Il2CppObject * ___value1, Dictionary_2_t1662909226 ** ___memberMap2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_CoerceType_m3531708896_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	MemberInfo_t * V_4 = NULL;
	Type_t * V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	Il2CppObject * V_7 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Type_t * L_0 = ___targetType0;
		Dictionary_2_t1662909226 ** L_1 = ___memberMap2;
		Il2CppObject * L_2 = TypeCoercionUtility_InstantiateObject_m123346059(__this, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Dictionary_2_t1662909226 ** L_3 = ___memberMap2;
		V_1 = (bool)((!(((Il2CppObject*)(Dictionary_2_t1662909226 *)(*((Dictionary_2_t1662909226 **)L_3))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0070;
		}
	}
	{
		Il2CppObject * L_5 = ___value1;
		NullCheck(L_5);
		Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_5);
		NullCheck(L_6);
		Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_6);
		V_2 = L_7;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0050;
		}

IL_0023:
		{
			Il2CppObject * L_8 = V_2;
			NullCheck(L_8);
			Il2CppObject * L_9 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_8);
			V_3 = L_9;
			Dictionary_2_t1662909226 ** L_10 = ___memberMap2;
			Il2CppObject * L_11 = V_3;
			Type_t * L_12 = TypeCoercionUtility_GetMemberInfo_m658257860(NULL /*static, unused*/, (*((Dictionary_2_t1662909226 **)L_10)), ((String_t*)IsInstSealed(L_11, String_t_il2cpp_TypeInfo_var)), (&V_4), /*hidden argument*/NULL);
			V_5 = L_12;
			Il2CppObject * L_13 = V_0;
			Type_t * L_14 = V_5;
			MemberInfo_t * L_15 = V_4;
			Il2CppObject * L_16 = ___value1;
			Il2CppObject * L_17 = V_3;
			NullCheck(L_16);
			Il2CppObject * L_18 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_16, L_17);
			TypeCoercionUtility_SetMemberValue_m1207607791(__this, L_13, L_14, L_15, L_18, /*hidden argument*/NULL);
		}

IL_0050:
		{
			Il2CppObject * L_19 = V_2;
			NullCheck(L_19);
			bool L_20 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_19);
			if (L_20)
			{
				goto IL_0023;
			}
		}

IL_0058:
		{
			IL2CPP_LEAVE(0x6F, FINALLY_005a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_005a;
	}

FINALLY_005a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_21 = V_2;
			V_6 = ((Il2CppObject *)IsInst(L_21, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_22 = V_6;
			if (!L_22)
			{
				goto IL_006e;
			}
		}

IL_0066:
		{
			Il2CppObject * L_23 = V_6;
			NullCheck(L_23);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_23);
		}

IL_006e:
		{
			IL2CPP_END_FINALLY(90)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(90)
	{
		IL2CPP_JUMP_TBL(0x6F, IL_006f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_006f:
	{
	}

IL_0070:
	{
		Il2CppObject * L_24 = V_0;
		V_7 = L_24;
		goto IL_0075;
	}

IL_0075:
	{
		Il2CppObject * L_25 = V_7;
		return L_25;
	}
}
// System.Object Pathfinding.Serialization.JsonFx.TypeCoercionUtility::CoerceList(System.Type,System.Type,System.Collections.IEnumerable)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonTypeCoercionException_t1571553219_il2cpp_TypeInfo_var;
extern Il2CppClass* TargetInvocationException_t4098620458_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral625868345;
extern Il2CppCodeGenString* _stringLiteral2675436571;
extern Il2CppCodeGenString* _stringLiteral2939771454;
extern Il2CppCodeGenString* _stringLiteral762201177;
extern Il2CppCodeGenString* _stringLiteral292745765;
extern Il2CppCodeGenString* _stringLiteral3405993280;
extern Il2CppCodeGenString* _stringLiteral1098351937;
extern const uint32_t TypeCoercionUtility_CoerceList_m2435065360_MetadataUsageId;
extern "C"  Il2CppObject * TypeCoercionUtility_CoerceList_m2435065360 (TypeCoercionUtility_t3996825458 * __this, Type_t * ___targetType0, Type_t * ___arrayType1, Il2CppObject * ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_CoerceList_m2435065360_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ConstructorInfoU5BU5D_t1996683371* V_0 = NULL;
	ConstructorInfo_t2851816542 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	MethodInfo_t * V_3 = NULL;
	ParameterInfoU5BU5D_t2275869610* V_4 = NULL;
	Type_t * V_5 = NULL;
	bool V_6 = false;
	Il2CppObject * V_7 = NULL;
	ConstructorInfoU5BU5D_t1996683371* V_8 = NULL;
	int32_t V_9 = 0;
	ConstructorInfo_t2851816542 * V_10 = NULL;
	ParameterInfoU5BU5D_t2275869610* V_11 = NULL;
	bool V_12 = false;
	bool V_13 = false;
	bool V_14 = false;
	TargetInvocationException_t4098620458 * V_15 = NULL;
	bool V_16 = false;
	bool V_17 = false;
	TargetInvocationException_t4098620458 * V_18 = NULL;
	bool V_19 = false;
	bool V_20 = false;
	Il2CppObject * V_21 = NULL;
	Il2CppObject * V_22 = NULL;
	TargetInvocationException_t4098620458 * V_23 = NULL;
	bool V_24 = false;
	Il2CppObject * V_25 = NULL;
	Exception_t1927440687 * V_26 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B8_0 = 0;
	ParameterInfoU5BU5D_t2275869610* G_B25_0 = NULL;
	Type_t * G_B29_0 = NULL;
	int32_t G_B32_0 = 0;
	ParameterInfoU5BU5D_t2275869610* G_B42_0 = NULL;
	Type_t * G_B46_0 = NULL;
	{
		Type_t * L_0 = ___targetType0;
		NullCheck(L_0);
		bool L_1 = Type_get_IsArray_m811277129(L_0, /*hidden argument*/NULL);
		V_6 = L_1;
		bool L_2 = V_6;
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		Type_t * L_3 = ___targetType0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(46 /* System.Type System.Type::GetElementType() */, L_3);
		Il2CppObject * L_5 = ___value2;
		Il2CppArray * L_6 = TypeCoercionUtility_CoerceArray_m3112982546(__this, L_4, L_5, /*hidden argument*/NULL);
		V_7 = L_6;
		goto IL_02d9;
	}

IL_0022:
	{
		Type_t * L_7 = ___targetType0;
		NullCheck(L_7);
		ConstructorInfoU5BU5D_t1996683371* L_8 = Type_GetConstructors_m52202211(L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		V_1 = (ConstructorInfo_t2851816542 *)NULL;
		ConstructorInfoU5BU5D_t1996683371* L_9 = V_0;
		V_8 = L_9;
		V_9 = 0;
		goto IL_009b;
	}

IL_0034:
	{
		ConstructorInfoU5BU5D_t1996683371* L_10 = V_8;
		int32_t L_11 = V_9;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		ConstructorInfo_t2851816542 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_10 = L_13;
		ConstructorInfo_t2851816542 * L_14 = V_10;
		NullCheck(L_14);
		ParameterInfoU5BU5D_t2275869610* L_15 = VirtFuncInvoker0< ParameterInfoU5BU5D_t2275869610* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_14);
		V_11 = L_15;
		ParameterInfoU5BU5D_t2275869610* L_16 = V_11;
		NullCheck(L_16);
		V_12 = (bool)((((int32_t)(((Il2CppArray *)L_16)->max_length)) == ((int32_t)0))? 1 : 0);
		bool L_17 = V_12;
		if (!L_17)
		{
			goto IL_0057;
		}
	}
	{
		ConstructorInfo_t2851816542 * L_18 = V_10;
		V_1 = L_18;
		goto IL_0095;
	}

IL_0057:
	{
		ParameterInfoU5BU5D_t2275869610* L_19 = V_11;
		NullCheck(L_19);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_006f;
		}
	}
	{
		ParameterInfoU5BU5D_t2275869610* L_20 = V_11;
		NullCheck(L_20);
		int32_t L_21 = 0;
		ParameterInfo_t2249040075 * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		Type_t * L_23 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_22);
		Type_t * L_24 = ___arrayType1;
		NullCheck(L_23);
		bool L_25 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_23, L_24);
		G_B8_0 = ((int32_t)(L_25));
		goto IL_0070;
	}

IL_006f:
	{
		G_B8_0 = 0;
	}

IL_0070:
	{
		V_13 = (bool)G_B8_0;
		bool L_26 = V_13;
		if (!L_26)
		{
			goto IL_0094;
		}
	}
	{
	}

IL_0077:
	try
	{ // begin try (depth: 1)
		ConstructorInfo_t2851816542 * L_27 = V_10;
		ObjectU5BU5D_t3614634134* L_28 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_29 = ___value2;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, L_29);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_29);
		NullCheck(L_27);
		Il2CppObject * L_30 = ConstructorInfo_Invoke_m2144827141(L_27, L_28, /*hidden argument*/NULL);
		V_7 = L_30;
		goto IL_02d9;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0090;
		throw e;
	}

CATCH_0090:
	{ // begin catch(System.Object)
		goto IL_0095;
	} // end catch (depth: 1)

IL_0094:
	{
	}

IL_0095:
	{
		int32_t L_31 = V_9;
		V_9 = ((int32_t)((int32_t)L_31+(int32_t)1));
	}

IL_009b:
	{
		int32_t L_32 = V_9;
		ConstructorInfoU5BU5D_t1996683371* L_33 = V_8;
		NullCheck(L_33);
		if ((((int32_t)L_32) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_33)->max_length)))))))
		{
			goto IL_0034;
		}
	}
	{
		ConstructorInfo_t2851816542 * L_34 = V_1;
		V_14 = (bool)((((Il2CppObject*)(ConstructorInfo_t2851816542 *)L_34) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_35 = V_14;
		if (!L_35)
		{
			goto IL_00c4;
		}
	}
	{
		Type_t * L_36 = ___targetType0;
		NullCheck(L_36);
		String_t* L_37 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_36);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_38 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral625868345, L_37, /*hidden argument*/NULL);
		JsonTypeCoercionException_t1571553219 * L_39 = (JsonTypeCoercionException_t1571553219 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t1571553219_il2cpp_TypeInfo_var);
		JsonTypeCoercionException__ctor_m695869114(L_39, L_38, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_39);
	}

IL_00c4:
	{
	}

IL_00c5:
	try
	{ // begin try (depth: 1)
		ConstructorInfo_t2851816542 * L_40 = V_1;
		NullCheck(L_40);
		Il2CppObject * L_41 = ConstructorInfo_Invoke_m2144827141(L_40, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL, /*hidden argument*/NULL);
		V_2 = L_41;
		goto IL_0116;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (TargetInvocationException_t4098620458_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00d1;
		throw e;
	}

CATCH_00d1:
	{ // begin catch(System.Reflection.TargetInvocationException)
		{
			V_15 = ((TargetInvocationException_t4098620458 *)__exception_local);
			TargetInvocationException_t4098620458 * L_42 = V_15;
			NullCheck(L_42);
			Exception_t1927440687 * L_43 = Exception_get_InnerException_m3722561235(L_42, /*hidden argument*/NULL);
			V_16 = (bool)((!(((Il2CppObject*)(Exception_t1927440687 *)L_43) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
			bool L_44 = V_16;
			if (!L_44)
			{
				goto IL_00fe;
			}
		}

IL_00e4:
		{
			TargetInvocationException_t4098620458 * L_45 = V_15;
			NullCheck(L_45);
			Exception_t1927440687 * L_46 = Exception_get_InnerException_m3722561235(L_45, /*hidden argument*/NULL);
			NullCheck(L_46);
			String_t* L_47 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_46);
			TargetInvocationException_t4098620458 * L_48 = V_15;
			NullCheck(L_48);
			Exception_t1927440687 * L_49 = Exception_get_InnerException_m3722561235(L_48, /*hidden argument*/NULL);
			JsonTypeCoercionException_t1571553219 * L_50 = (JsonTypeCoercionException_t1571553219 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t1571553219_il2cpp_TypeInfo_var);
			JsonTypeCoercionException__ctor_m1497116290(L_50, L_47, L_49, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_50);
		}

IL_00fe:
		{
			Type_t * L_51 = ___targetType0;
			NullCheck(L_51);
			String_t* L_52 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_51);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_53 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2675436571, L_52, /*hidden argument*/NULL);
			TargetInvocationException_t4098620458 * L_54 = V_15;
			JsonTypeCoercionException_t1571553219 * L_55 = (JsonTypeCoercionException_t1571553219 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t1571553219_il2cpp_TypeInfo_var);
			JsonTypeCoercionException__ctor_m1497116290(L_55, L_53, L_54, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_55);
		}
	} // end catch (depth: 1)

IL_0116:
	{
		Type_t * L_56 = ___targetType0;
		NullCheck(L_56);
		MethodInfo_t * L_57 = Type_GetMethod_m1197504218(L_56, _stringLiteral2939771454, /*hidden argument*/NULL);
		V_3 = L_57;
		MethodInfo_t * L_58 = V_3;
		if (!L_58)
		{
			goto IL_012d;
		}
	}
	{
		MethodInfo_t * L_59 = V_3;
		NullCheck(L_59);
		ParameterInfoU5BU5D_t2275869610* L_60 = VirtFuncInvoker0< ParameterInfoU5BU5D_t2275869610* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_59);
		G_B25_0 = L_60;
		goto IL_012e;
	}

IL_012d:
	{
		G_B25_0 = ((ParameterInfoU5BU5D_t2275869610*)(NULL));
	}

IL_012e:
	{
		V_4 = G_B25_0;
		ParameterInfoU5BU5D_t2275869610* L_61 = V_4;
		if (!L_61)
		{
			goto IL_0146;
		}
	}
	{
		ParameterInfoU5BU5D_t2275869610* L_62 = V_4;
		NullCheck(L_62);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_62)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_0146;
		}
	}
	{
		ParameterInfoU5BU5D_t2275869610* L_63 = V_4;
		NullCheck(L_63);
		int32_t L_64 = 0;
		ParameterInfo_t2249040075 * L_65 = (L_63)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		NullCheck(L_65);
		Type_t * L_66 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_65);
		G_B29_0 = L_66;
		goto IL_0147;
	}

IL_0146:
	{
		G_B29_0 = ((Type_t *)(NULL));
	}

IL_0147:
	{
		V_5 = G_B29_0;
		Type_t * L_67 = V_5;
		if (!L_67)
		{
			goto IL_0157;
		}
	}
	{
		Type_t * L_68 = V_5;
		Type_t * L_69 = ___arrayType1;
		NullCheck(L_68);
		bool L_70 = VirtFuncInvoker1< bool, Type_t * >::Invoke(43 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_68, L_69);
		G_B32_0 = ((int32_t)(L_70));
		goto IL_0158;
	}

IL_0157:
	{
		G_B32_0 = 0;
	}

IL_0158:
	{
		V_17 = (bool)G_B32_0;
		bool L_71 = V_17;
		if (!L_71)
		{
			goto IL_01c2;
		}
	}
	{
	}

IL_015f:
	try
	{ // begin try (depth: 1)
		MethodInfo_t * L_72 = V_3;
		Il2CppObject * L_73 = V_2;
		ObjectU5BU5D_t3614634134* L_74 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_75 = ___value2;
		NullCheck(L_74);
		ArrayElementTypeCheck (L_74, L_75);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_75);
		NullCheck(L_72);
		MethodBase_Invoke_m1075809207(L_72, L_73, L_74, /*hidden argument*/NULL);
		goto IL_01ba;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (TargetInvocationException_t4098620458_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0175;
		throw e;
	}

CATCH_0175:
	{ // begin catch(System.Reflection.TargetInvocationException)
		{
			V_18 = ((TargetInvocationException_t4098620458 *)__exception_local);
			TargetInvocationException_t4098620458 * L_76 = V_18;
			NullCheck(L_76);
			Exception_t1927440687 * L_77 = Exception_get_InnerException_m3722561235(L_76, /*hidden argument*/NULL);
			V_19 = (bool)((!(((Il2CppObject*)(Exception_t1927440687 *)L_77) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
			bool L_78 = V_19;
			if (!L_78)
			{
				goto IL_01a2;
			}
		}

IL_0188:
		{
			TargetInvocationException_t4098620458 * L_79 = V_18;
			NullCheck(L_79);
			Exception_t1927440687 * L_80 = Exception_get_InnerException_m3722561235(L_79, /*hidden argument*/NULL);
			NullCheck(L_80);
			String_t* L_81 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_80);
			TargetInvocationException_t4098620458 * L_82 = V_18;
			NullCheck(L_82);
			Exception_t1927440687 * L_83 = Exception_get_InnerException_m3722561235(L_82, /*hidden argument*/NULL);
			JsonTypeCoercionException_t1571553219 * L_84 = (JsonTypeCoercionException_t1571553219 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t1571553219_il2cpp_TypeInfo_var);
			JsonTypeCoercionException__ctor_m1497116290(L_84, L_81, L_83, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_84);
		}

IL_01a2:
		{
			Type_t * L_85 = ___targetType0;
			NullCheck(L_85);
			String_t* L_86 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_85);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_87 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral762201177, L_86, /*hidden argument*/NULL);
			TargetInvocationException_t4098620458 * L_88 = V_18;
			JsonTypeCoercionException_t1571553219 * L_89 = (JsonTypeCoercionException_t1571553219 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t1571553219_il2cpp_TypeInfo_var);
			JsonTypeCoercionException__ctor_m1497116290(L_89, L_87, L_88, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_89);
		}
	} // end catch (depth: 1)

IL_01ba:
	{
		Il2CppObject * L_90 = V_2;
		V_7 = L_90;
		goto IL_02d9;
	}

IL_01c2:
	{
		Type_t * L_91 = ___targetType0;
		NullCheck(L_91);
		MethodInfo_t * L_92 = Type_GetMethod_m1197504218(L_91, _stringLiteral292745765, /*hidden argument*/NULL);
		V_3 = L_92;
		MethodInfo_t * L_93 = V_3;
		if (!L_93)
		{
			goto IL_01da;
		}
	}
	{
		MethodInfo_t * L_94 = V_3;
		NullCheck(L_94);
		ParameterInfoU5BU5D_t2275869610* L_95 = VirtFuncInvoker0< ParameterInfoU5BU5D_t2275869610* >::Invoke(15 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_94);
		G_B42_0 = L_95;
		goto IL_01db;
	}

IL_01da:
	{
		G_B42_0 = ((ParameterInfoU5BU5D_t2275869610*)(NULL));
	}

IL_01db:
	{
		V_4 = G_B42_0;
		ParameterInfoU5BU5D_t2275869610* L_96 = V_4;
		if (!L_96)
		{
			goto IL_01f3;
		}
	}
	{
		ParameterInfoU5BU5D_t2275869610* L_97 = V_4;
		NullCheck(L_97);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_97)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_01f3;
		}
	}
	{
		ParameterInfoU5BU5D_t2275869610* L_98 = V_4;
		NullCheck(L_98);
		int32_t L_99 = 0;
		ParameterInfo_t2249040075 * L_100 = (L_98)->GetAt(static_cast<il2cpp_array_size_t>(L_99));
		NullCheck(L_100);
		Type_t * L_101 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_100);
		G_B46_0 = L_101;
		goto IL_01f4;
	}

IL_01f3:
	{
		G_B46_0 = ((Type_t *)(NULL));
	}

IL_01f4:
	{
		V_5 = G_B46_0;
		Type_t * L_102 = V_5;
		V_20 = (bool)((!(((Il2CppObject*)(Type_t *)L_102) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		bool L_103 = V_20;
		if (!L_103)
		{
			goto IL_02a5;
		}
	}
	{
		Il2CppObject * L_104 = ___value2;
		NullCheck(L_104);
		Il2CppObject * L_105 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_104);
		V_21 = L_105;
	}

IL_020e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_027f;
		}

IL_0210:
		{
			Il2CppObject * L_106 = V_21;
			NullCheck(L_106);
			Il2CppObject * L_107 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_106);
			V_22 = L_107;
		}

IL_021a:
		try
		{ // begin try (depth: 2)
			MethodInfo_t * L_108 = V_3;
			Il2CppObject * L_109 = V_2;
			ObjectU5BU5D_t3614634134* L_110 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
			Type_t * L_111 = V_5;
			Il2CppObject * L_112 = V_22;
			Il2CppObject * L_113 = TypeCoercionUtility_CoerceType_m3744809450(__this, L_111, L_112, /*hidden argument*/NULL);
			NullCheck(L_110);
			ArrayElementTypeCheck (L_110, L_113);
			(L_110)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_113);
			NullCheck(L_108);
			MethodBase_Invoke_m1075809207(L_108, L_109, L_110, /*hidden argument*/NULL);
			goto IL_027e;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1927440687 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (TargetInvocationException_t4098620458_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0239;
			throw e;
		}

CATCH_0239:
		{ // begin catch(System.Reflection.TargetInvocationException)
			{
				V_23 = ((TargetInvocationException_t4098620458 *)__exception_local);
				TargetInvocationException_t4098620458 * L_114 = V_23;
				NullCheck(L_114);
				Exception_t1927440687 * L_115 = Exception_get_InnerException_m3722561235(L_114, /*hidden argument*/NULL);
				V_24 = (bool)((!(((Il2CppObject*)(Exception_t1927440687 *)L_115) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
				bool L_116 = V_24;
				if (!L_116)
				{
					goto IL_0266;
				}
			}

IL_024c:
			{
				TargetInvocationException_t4098620458 * L_117 = V_23;
				NullCheck(L_117);
				Exception_t1927440687 * L_118 = Exception_get_InnerException_m3722561235(L_117, /*hidden argument*/NULL);
				NullCheck(L_118);
				String_t* L_119 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_118);
				TargetInvocationException_t4098620458 * L_120 = V_23;
				NullCheck(L_120);
				Exception_t1927440687 * L_121 = Exception_get_InnerException_m3722561235(L_120, /*hidden argument*/NULL);
				JsonTypeCoercionException_t1571553219 * L_122 = (JsonTypeCoercionException_t1571553219 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t1571553219_il2cpp_TypeInfo_var);
				JsonTypeCoercionException__ctor_m1497116290(L_122, L_119, L_121, /*hidden argument*/NULL);
				IL2CPP_RAISE_MANAGED_EXCEPTION(L_122);
			}

IL_0266:
			{
				Type_t * L_123 = ___targetType0;
				NullCheck(L_123);
				String_t* L_124 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_123);
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_125 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3405993280, L_124, /*hidden argument*/NULL);
				TargetInvocationException_t4098620458 * L_126 = V_23;
				JsonTypeCoercionException_t1571553219 * L_127 = (JsonTypeCoercionException_t1571553219 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t1571553219_il2cpp_TypeInfo_var);
				JsonTypeCoercionException__ctor_m1497116290(L_127, L_125, L_126, /*hidden argument*/NULL);
				IL2CPP_RAISE_MANAGED_EXCEPTION(L_127);
			}
		} // end catch (depth: 2)

IL_027e:
		{
		}

IL_027f:
		{
			Il2CppObject * L_128 = V_21;
			NullCheck(L_128);
			bool L_129 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_128);
			if (L_129)
			{
				goto IL_0210;
			}
		}

IL_0288:
		{
			IL2CPP_LEAVE(0x2A0, FINALLY_028a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_028a;
	}

FINALLY_028a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_130 = V_21;
			V_25 = ((Il2CppObject *)IsInst(L_130, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_131 = V_25;
			if (!L_131)
			{
				goto IL_029f;
			}
		}

IL_0297:
		{
			Il2CppObject * L_132 = V_25;
			NullCheck(L_132);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_132);
		}

IL_029f:
		{
			IL2CPP_END_FINALLY(650)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(650)
	{
		IL2CPP_JUMP_TBL(0x2A0, IL_02a0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_02a0:
	{
		Il2CppObject * L_133 = V_2;
		V_7 = L_133;
		goto IL_02d9;
	}

IL_02a5:
	{
	}

IL_02a7:
	try
	{ // begin try (depth: 1)
		Il2CppObject * L_134 = ___value2;
		Type_t * L_135 = ___targetType0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_136 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, L_134, L_135, /*hidden argument*/NULL);
		V_7 = L_136;
		goto IL_02d9;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1927440687_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_02b3;
		throw e;
	}

CATCH_02b3:
	{ // begin catch(System.Exception)
		V_26 = ((Exception_t1927440687 *)__exception_local);
		Il2CppObject * L_137 = ___value2;
		NullCheck(L_137);
		Type_t * L_138 = Object_GetType_m191970594(L_137, /*hidden argument*/NULL);
		NullCheck(L_138);
		String_t* L_139 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_138);
		Type_t * L_140 = ___targetType0;
		NullCheck(L_140);
		String_t* L_141 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_140);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_142 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral1098351937, L_139, L_141, /*hidden argument*/NULL);
		Exception_t1927440687 * L_143 = V_26;
		JsonTypeCoercionException_t1571553219 * L_144 = (JsonTypeCoercionException_t1571553219 *)il2cpp_codegen_object_new(JsonTypeCoercionException_t1571553219_il2cpp_TypeInfo_var);
		JsonTypeCoercionException__ctor_m1497116290(L_144, L_142, L_143, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_144);
	} // end catch (depth: 1)

IL_02d9:
	{
		Il2CppObject * L_145 = V_7;
		return L_145;
	}
}
// System.Array Pathfinding.Serialization.JsonFx.TypeCoercionUtility::CoerceArray(System.Type,System.Collections.IEnumerable)
extern Il2CppClass* ArrayList_t4252133567_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t TypeCoercionUtility_CoerceArray_m3112982546_MetadataUsageId;
extern "C"  Il2CppArray * TypeCoercionUtility_CoerceArray_m3112982546 (TypeCoercionUtility_t3996825458 * __this, Type_t * ___elementType0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_CoerceArray_m3112982546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ArrayList_t4252133567 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Il2CppArray * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ArrayList_t4252133567 * L_0 = (ArrayList_t4252133567 *)il2cpp_codegen_object_new(ArrayList_t4252133567_il2cpp_TypeInfo_var);
		ArrayList__ctor_m4012174379(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Il2CppObject * L_1 = ___value1;
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_1);
		V_1 = L_2;
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0029;
		}

IL_0011:
		{
			Il2CppObject * L_3 = V_1;
			NullCheck(L_3);
			Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_3);
			V_2 = L_4;
			ArrayList_t4252133567 * L_5 = V_0;
			Type_t * L_6 = ___elementType0;
			Il2CppObject * L_7 = V_2;
			Il2CppObject * L_8 = TypeCoercionUtility_CoerceType_m3744809450(__this, L_6, L_7, /*hidden argument*/NULL);
			NullCheck(L_5);
			VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(30 /* System.Int32 System.Collections.ArrayList::Add(System.Object) */, L_5, L_8);
		}

IL_0029:
		{
			Il2CppObject * L_9 = V_1;
			NullCheck(L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_9);
			if (L_10)
			{
				goto IL_0011;
			}
		}

IL_0031:
		{
			IL2CPP_LEAVE(0x45, FINALLY_0033);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0033;
	}

FINALLY_0033:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_11 = V_1;
			V_3 = ((Il2CppObject *)IsInst(L_11, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_12 = V_3;
			if (!L_12)
			{
				goto IL_0044;
			}
		}

IL_003d:
		{
			Il2CppObject * L_13 = V_3;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_13);
		}

IL_0044:
		{
			IL2CPP_END_FINALLY(51)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(51)
	{
		IL2CPP_JUMP_TBL(0x45, IL_0045)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0045:
	{
		ArrayList_t4252133567 * L_14 = V_0;
		Type_t * L_15 = ___elementType0;
		NullCheck(L_14);
		Il2CppArray * L_16 = VirtFuncInvoker1< Il2CppArray *, Type_t * >::Invoke(48 /* System.Array System.Collections.ArrayList::ToArray(System.Type) */, L_14, L_15);
		V_4 = L_16;
		goto IL_0050;
	}

IL_0050:
	{
		Il2CppArray * L_17 = V_4;
		return L_17;
	}
}
// System.Boolean Pathfinding.Serialization.JsonFx.TypeCoercionUtility::IsNullable(System.Type)
extern const Il2CppType* Nullable_1_t1398937014_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TypeCoercionUtility_IsNullable_m1309334937_MetadataUsageId;
extern "C"  bool TypeCoercionUtility_IsNullable_m1309334937 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeCoercionUtility_IsNullable_m1309334937_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(90 /* System.Boolean System.Type::get_IsGenericType() */, L_0);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Nullable_1_t1398937014_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_3 = ___type0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(89 /* System.Type System.Type::GetGenericTypeDefinition() */, L_3);
		G_B3_0 = ((((Il2CppObject*)(Type_t *)L_2) == ((Il2CppObject*)(Type_t *)L_4))? 1 : 0);
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0021;
	}

IL_0021:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Void Pathfinding.Serialization.JsonFx.TypeCoercionUtility::.ctor()
extern "C"  void TypeCoercionUtility__ctor_m3459939157 (TypeCoercionUtility_t3996825458 * __this, const MethodInfo* method)
{
	{
		__this->set_allowNullValueTypes_1((bool)1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
