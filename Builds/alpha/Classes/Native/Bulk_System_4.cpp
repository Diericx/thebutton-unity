﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.UriParser
struct UriParser_t1012511323;
// System.Uri
struct Uri_t19570940;
// System.UriFormatException
struct UriFormatException_t3682083048;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.UriTypeConverter
struct UriTypeConverter_t3912970448;
// System.Type
struct Type_t;
// System.ComponentModel.ITypeDescriptorContext
struct ITypeDescriptorContext_t3633625151;
// System.Object
struct Il2CppObject;
// System.Globalization.CultureInfo
struct CultureInfo_t3500843524;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_System_UriKind1128731744.h"
#include "System_System_UriKind1128731744MethodDeclarations.h"
#include "System_System_UriParser1012511323.h"
#include "System_System_UriParser1012511323MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Regex1803876613MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Text_RegularExpressions_Regex1803876613.h"
#include "System_System_Text_RegularExpressions_RegexOptions2418259727.h"
#include "System_System_Uri19570940.h"
#include "System_System_UriFormatException3682083048.h"
#include "System_System_Uri19570940MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "System_System_UriFormatException3682083048MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Collections_Hashtable909839986MethodDeclarations.h"
#include "System_System_DefaultUriParser1591960796MethodDeclarations.h"
#include "mscorlib_System_Threading_Monitor3228523394MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"
#include "System_System_DefaultUriParser1591960796.h"
#include "System_System_GenericUriParser2599285286.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524.h"
#include "System_System_UriPartial112107391.h"
#include "System_System_UriPartial112107391MethodDeclarations.h"
#include "System_System_UriTypeConverter3912970448.h"
#include "System_System_UriTypeConverter3912970448MethodDeclarations.h"
#include "System_System_ComponentModel_TypeConverter745995970MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_ArgumentNullException628810857MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "System_Locale4255929014MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "System_System_ComponentModel_Design_Serialization_1404033120MethodDeclarations.h"
#include "System_System_ComponentModel_Design_Serialization_1404033120.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "System_System_ComponentModel_TypeConverter745995970.h"
#include "mscorlib_System_Reflection_ConstructorInfo2851816542.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.UriParser::.ctor()
extern "C"  void UriParser__ctor_m1282308392 (UriParser_t1012511323 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.UriParser::.cctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* UriParser_t1012511323_il2cpp_TypeInfo_var;
extern Il2CppClass* Regex_t1803876613_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral704347881;
extern Il2CppCodeGenString* _stringLiteral1509538344;
extern const uint32_t UriParser__cctor_m1839415991_MetadataUsageId;
extern "C"  void UriParser__cctor_m1839415991 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UriParser__cctor_m1839415991_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		((UriParser_t1012511323_StaticFields*)UriParser_t1012511323_il2cpp_TypeInfo_var->static_fields)->set_lock_object_0(L_0);
		Regex_t1803876613 * L_1 = (Regex_t1803876613 *)il2cpp_codegen_object_new(Regex_t1803876613_il2cpp_TypeInfo_var);
		Regex__ctor_m2521903438(L_1, _stringLiteral704347881, 8, /*hidden argument*/NULL);
		((UriParser_t1012511323_StaticFields*)UriParser_t1012511323_il2cpp_TypeInfo_var->static_fields)->set_uri_regex_4(L_1);
		Regex_t1803876613 * L_2 = (Regex_t1803876613 *)il2cpp_codegen_object_new(Regex_t1803876613_il2cpp_TypeInfo_var);
		Regex__ctor_m1229307206(L_2, _stringLiteral1509538344, /*hidden argument*/NULL);
		((UriParser_t1012511323_StaticFields*)UriParser_t1012511323_il2cpp_TypeInfo_var->static_fields)->set_auth_regex_5(L_2);
		return;
	}
}
// System.Void System.UriParser::InitializeAndValidate(System.Uri,System.UriFormatException&)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* UriFormatException_t3682083048_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029320;
extern Il2CppCodeGenString* _stringLiteral1096536852;
extern const uint32_t UriParser_InitializeAndValidate_m251943319_MetadataUsageId;
extern "C"  void UriParser_InitializeAndValidate_m251943319 (UriParser_t1012511323 * __this, Uri_t19570940 * ___uri0, UriFormatException_t3682083048 ** ___parsingError1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UriParser_InitializeAndValidate_m251943319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Uri_t19570940 * L_0 = ___uri0;
		NullCheck(L_0);
		String_t* L_1 = Uri_get_Scheme_m55908894(L_0, /*hidden argument*/NULL);
		String_t* L_2 = __this->get_scheme_name_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003c;
		}
	}
	{
		String_t* L_4 = __this->get_scheme_name_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_4, _stringLiteral372029320, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		UriFormatException_t3682083048 ** L_6 = ___parsingError1;
		UriFormatException_t3682083048 * L_7 = (UriFormatException_t3682083048 *)il2cpp_codegen_object_new(UriFormatException_t3682083048_il2cpp_TypeInfo_var);
		UriFormatException__ctor_m3352702581(L_7, _stringLiteral1096536852, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_6)) = (Il2CppObject *)L_7;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_6), (Il2CppObject *)L_7);
		goto IL_003f;
	}

IL_003c:
	{
		UriFormatException_t3682083048 ** L_8 = ___parsingError1;
		*((Il2CppObject **)(L_8)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_8), (Il2CppObject *)NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void System.UriParser::OnRegister(System.String,System.Int32)
extern "C"  void UriParser_OnRegister_m4010407891 (UriParser_t1012511323 * __this, String_t* ___schemeName0, int32_t ___defaultPort1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void System.UriParser::set_SchemeName(System.String)
extern "C"  void UriParser_set_SchemeName_m3624672465 (UriParser_t1012511323 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_scheme_name_2(L_0);
		return;
	}
}
// System.Int32 System.UriParser::get_DefaultPort()
extern "C"  int32_t UriParser_get_DefaultPort_m1377931533 (UriParser_t1012511323 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_default_port_3();
		return L_0;
	}
}
// System.Void System.UriParser::set_DefaultPort(System.Int32)
extern "C"  void UriParser_set_DefaultPort_m1159216960 (UriParser_t1012511323 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_default_port_3(L_0);
		return;
	}
}
// System.Void System.UriParser::CreateDefaults()
extern Il2CppClass* UriParser_t1012511323_il2cpp_TypeInfo_var;
extern Il2CppClass* Hashtable_t909839986_il2cpp_TypeInfo_var;
extern Il2CppClass* DefaultUriParser_t1591960796_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral150430653;
extern const uint32_t UriParser_CreateDefaults_m295980432_MetadataUsageId;
extern "C"  void UriParser_CreateDefaults_m295980432 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UriParser_CreateDefaults_m295980432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Hashtable_t909839986 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(UriParser_t1012511323_il2cpp_TypeInfo_var);
		Hashtable_t909839986 * L_0 = ((UriParser_t1012511323_StaticFields*)UriParser_t1012511323_il2cpp_TypeInfo_var->static_fields)->get_table_1();
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		Hashtable_t909839986 * L_1 = (Hashtable_t909839986 *)il2cpp_codegen_object_new(Hashtable_t909839986_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1884964176(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		Hashtable_t909839986 * L_2 = V_0;
		DefaultUriParser_t1591960796 * L_3 = (DefaultUriParser_t1591960796 *)il2cpp_codegen_object_new(DefaultUriParser_t1591960796_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m4218024811(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t19570940_il2cpp_TypeInfo_var);
		String_t* L_4 = ((Uri_t19570940_StaticFields*)Uri_t19570940_il2cpp_TypeInfo_var->static_fields)->get_UriSchemeFile_19();
		IL2CPP_RUNTIME_CLASS_INIT(UriParser_t1012511323_il2cpp_TypeInfo_var);
		UriParser_InternalRegister_m416643159(NULL /*static, unused*/, L_2, L_3, L_4, (-1), /*hidden argument*/NULL);
		Hashtable_t909839986 * L_5 = V_0;
		DefaultUriParser_t1591960796 * L_6 = (DefaultUriParser_t1591960796 *)il2cpp_codegen_object_new(DefaultUriParser_t1591960796_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m4218024811(L_6, /*hidden argument*/NULL);
		String_t* L_7 = ((Uri_t19570940_StaticFields*)Uri_t19570940_il2cpp_TypeInfo_var->static_fields)->get_UriSchemeFtp_20();
		UriParser_InternalRegister_m416643159(NULL /*static, unused*/, L_5, L_6, L_7, ((int32_t)21), /*hidden argument*/NULL);
		Hashtable_t909839986 * L_8 = V_0;
		DefaultUriParser_t1591960796 * L_9 = (DefaultUriParser_t1591960796 *)il2cpp_codegen_object_new(DefaultUriParser_t1591960796_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m4218024811(L_9, /*hidden argument*/NULL);
		String_t* L_10 = ((Uri_t19570940_StaticFields*)Uri_t19570940_il2cpp_TypeInfo_var->static_fields)->get_UriSchemeGopher_21();
		UriParser_InternalRegister_m416643159(NULL /*static, unused*/, L_8, L_9, L_10, ((int32_t)70), /*hidden argument*/NULL);
		Hashtable_t909839986 * L_11 = V_0;
		DefaultUriParser_t1591960796 * L_12 = (DefaultUriParser_t1591960796 *)il2cpp_codegen_object_new(DefaultUriParser_t1591960796_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m4218024811(L_12, /*hidden argument*/NULL);
		String_t* L_13 = ((Uri_t19570940_StaticFields*)Uri_t19570940_il2cpp_TypeInfo_var->static_fields)->get_UriSchemeHttp_22();
		UriParser_InternalRegister_m416643159(NULL /*static, unused*/, L_11, L_12, L_13, ((int32_t)80), /*hidden argument*/NULL);
		Hashtable_t909839986 * L_14 = V_0;
		DefaultUriParser_t1591960796 * L_15 = (DefaultUriParser_t1591960796 *)il2cpp_codegen_object_new(DefaultUriParser_t1591960796_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m4218024811(L_15, /*hidden argument*/NULL);
		String_t* L_16 = ((Uri_t19570940_StaticFields*)Uri_t19570940_il2cpp_TypeInfo_var->static_fields)->get_UriSchemeHttps_23();
		UriParser_InternalRegister_m416643159(NULL /*static, unused*/, L_14, L_15, L_16, ((int32_t)443), /*hidden argument*/NULL);
		Hashtable_t909839986 * L_17 = V_0;
		DefaultUriParser_t1591960796 * L_18 = (DefaultUriParser_t1591960796 *)il2cpp_codegen_object_new(DefaultUriParser_t1591960796_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m4218024811(L_18, /*hidden argument*/NULL);
		String_t* L_19 = ((Uri_t19570940_StaticFields*)Uri_t19570940_il2cpp_TypeInfo_var->static_fields)->get_UriSchemeMailto_24();
		UriParser_InternalRegister_m416643159(NULL /*static, unused*/, L_17, L_18, L_19, ((int32_t)25), /*hidden argument*/NULL);
		Hashtable_t909839986 * L_20 = V_0;
		DefaultUriParser_t1591960796 * L_21 = (DefaultUriParser_t1591960796 *)il2cpp_codegen_object_new(DefaultUriParser_t1591960796_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m4218024811(L_21, /*hidden argument*/NULL);
		String_t* L_22 = ((Uri_t19570940_StaticFields*)Uri_t19570940_il2cpp_TypeInfo_var->static_fields)->get_UriSchemeNetPipe_27();
		UriParser_InternalRegister_m416643159(NULL /*static, unused*/, L_20, L_21, L_22, (-1), /*hidden argument*/NULL);
		Hashtable_t909839986 * L_23 = V_0;
		DefaultUriParser_t1591960796 * L_24 = (DefaultUriParser_t1591960796 *)il2cpp_codegen_object_new(DefaultUriParser_t1591960796_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m4218024811(L_24, /*hidden argument*/NULL);
		String_t* L_25 = ((Uri_t19570940_StaticFields*)Uri_t19570940_il2cpp_TypeInfo_var->static_fields)->get_UriSchemeNetTcp_28();
		UriParser_InternalRegister_m416643159(NULL /*static, unused*/, L_23, L_24, L_25, (-1), /*hidden argument*/NULL);
		Hashtable_t909839986 * L_26 = V_0;
		DefaultUriParser_t1591960796 * L_27 = (DefaultUriParser_t1591960796 *)il2cpp_codegen_object_new(DefaultUriParser_t1591960796_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m4218024811(L_27, /*hidden argument*/NULL);
		String_t* L_28 = ((Uri_t19570940_StaticFields*)Uri_t19570940_il2cpp_TypeInfo_var->static_fields)->get_UriSchemeNews_25();
		UriParser_InternalRegister_m416643159(NULL /*static, unused*/, L_26, L_27, L_28, ((int32_t)119), /*hidden argument*/NULL);
		Hashtable_t909839986 * L_29 = V_0;
		DefaultUriParser_t1591960796 * L_30 = (DefaultUriParser_t1591960796 *)il2cpp_codegen_object_new(DefaultUriParser_t1591960796_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m4218024811(L_30, /*hidden argument*/NULL);
		String_t* L_31 = ((Uri_t19570940_StaticFields*)Uri_t19570940_il2cpp_TypeInfo_var->static_fields)->get_UriSchemeNntp_26();
		UriParser_InternalRegister_m416643159(NULL /*static, unused*/, L_29, L_30, L_31, ((int32_t)119), /*hidden argument*/NULL);
		Hashtable_t909839986 * L_32 = V_0;
		DefaultUriParser_t1591960796 * L_33 = (DefaultUriParser_t1591960796 *)il2cpp_codegen_object_new(DefaultUriParser_t1591960796_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m4218024811(L_33, /*hidden argument*/NULL);
		UriParser_InternalRegister_m416643159(NULL /*static, unused*/, L_32, L_33, _stringLiteral150430653, ((int32_t)389), /*hidden argument*/NULL);
		Il2CppObject * L_34 = ((UriParser_t1012511323_StaticFields*)UriParser_t1012511323_il2cpp_TypeInfo_var->static_fields)->get_lock_object_0();
		V_1 = L_34;
		Il2CppObject * L_35 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
	}

IL_00e6:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(UriParser_t1012511323_il2cpp_TypeInfo_var);
			Hashtable_t909839986 * L_36 = ((UriParser_t1012511323_StaticFields*)UriParser_t1012511323_il2cpp_TypeInfo_var->static_fields)->get_table_1();
			if (L_36)
			{
				goto IL_00fb;
			}
		}

IL_00f0:
		{
			Hashtable_t909839986 * L_37 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(UriParser_t1012511323_il2cpp_TypeInfo_var);
			((UriParser_t1012511323_StaticFields*)UriParser_t1012511323_il2cpp_TypeInfo_var->static_fields)->set_table_1(L_37);
			goto IL_00fd;
		}

IL_00fb:
		{
			V_0 = (Hashtable_t909839986 *)NULL;
		}

IL_00fd:
		{
			IL2CPP_LEAVE(0x109, FINALLY_0102);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0102;
	}

FINALLY_0102:
	{ // begin finally (depth: 1)
		Il2CppObject * L_38 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(258)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(258)
	{
		IL2CPP_JUMP_TBL(0x109, IL_0109)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0109:
	{
		return;
	}
}
// System.Void System.UriParser::InternalRegister(System.Collections.Hashtable,System.UriParser,System.String,System.Int32)
extern Il2CppClass* GenericUriParser_t2599285286_il2cpp_TypeInfo_var;
extern Il2CppClass* DefaultUriParser_t1591960796_il2cpp_TypeInfo_var;
extern const uint32_t UriParser_InternalRegister_m416643159_MetadataUsageId;
extern "C"  void UriParser_InternalRegister_m416643159 (Il2CppObject * __this /* static, unused */, Hashtable_t909839986 * ___table0, UriParser_t1012511323 * ___uriParser1, String_t* ___schemeName2, int32_t ___defaultPort3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UriParser_InternalRegister_m416643159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DefaultUriParser_t1591960796 * V_0 = NULL;
	{
		UriParser_t1012511323 * L_0 = ___uriParser1;
		String_t* L_1 = ___schemeName2;
		NullCheck(L_0);
		UriParser_set_SchemeName_m3624672465(L_0, L_1, /*hidden argument*/NULL);
		UriParser_t1012511323 * L_2 = ___uriParser1;
		int32_t L_3 = ___defaultPort3;
		NullCheck(L_2);
		UriParser_set_DefaultPort_m1159216960(L_2, L_3, /*hidden argument*/NULL);
		UriParser_t1012511323 * L_4 = ___uriParser1;
		if (!((GenericUriParser_t2599285286 *)IsInstClass(L_4, GenericUriParser_t2599285286_il2cpp_TypeInfo_var)))
		{
			goto IL_0026;
		}
	}
	{
		Hashtable_t909839986 * L_5 = ___table0;
		String_t* L_6 = ___schemeName2;
		UriParser_t1012511323 * L_7 = ___uriParser1;
		NullCheck(L_5);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(29 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_5, L_6, L_7);
		goto IL_0042;
	}

IL_0026:
	{
		DefaultUriParser_t1591960796 * L_8 = (DefaultUriParser_t1591960796 *)il2cpp_codegen_object_new(DefaultUriParser_t1591960796_il2cpp_TypeInfo_var);
		DefaultUriParser__ctor_m4218024811(L_8, /*hidden argument*/NULL);
		V_0 = L_8;
		DefaultUriParser_t1591960796 * L_9 = V_0;
		String_t* L_10 = ___schemeName2;
		NullCheck(L_9);
		UriParser_set_SchemeName_m3624672465(L_9, L_10, /*hidden argument*/NULL);
		DefaultUriParser_t1591960796 * L_11 = V_0;
		int32_t L_12 = ___defaultPort3;
		NullCheck(L_11);
		UriParser_set_DefaultPort_m1159216960(L_11, L_12, /*hidden argument*/NULL);
		Hashtable_t909839986 * L_13 = ___table0;
		String_t* L_14 = ___schemeName2;
		DefaultUriParser_t1591960796 * L_15 = V_0;
		NullCheck(L_13);
		VirtActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(29 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, L_13, L_14, L_15);
	}

IL_0042:
	{
		UriParser_t1012511323 * L_16 = ___uriParser1;
		String_t* L_17 = ___schemeName2;
		int32_t L_18 = ___defaultPort3;
		NullCheck(L_16);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(5 /* System.Void System.UriParser::OnRegister(System.String,System.Int32) */, L_16, L_17, L_18);
		return;
	}
}
// System.UriParser System.UriParser::GetParser(System.String)
extern Il2CppClass* UriParser_t1012511323_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern const uint32_t UriParser_GetParser_m1453767844_MetadataUsageId;
extern "C"  UriParser_t1012511323 * UriParser_GetParser_m1453767844 (Il2CppObject * __this /* static, unused */, String_t* ___schemeName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UriParser_GetParser_m1453767844_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___schemeName0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (UriParser_t1012511323 *)NULL;
	}

IL_0008:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UriParser_t1012511323_il2cpp_TypeInfo_var);
		UriParser_CreateDefaults_m295980432(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___schemeName0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_2 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_3 = String_ToLower_m743194025(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Hashtable_t909839986 * L_4 = ((UriParser_t1012511323_StaticFields*)UriParser_t1012511323_il2cpp_TypeInfo_var->static_fields)->get_table_1();
		String_t* L_5 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_6 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(26 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_4, L_5);
		return ((UriParser_t1012511323 *)CastclassClass(L_6, UriParser_t1012511323_il2cpp_TypeInfo_var));
	}
}
// System.Void System.UriTypeConverter::.ctor()
extern "C"  void UriTypeConverter__ctor_m3743725527 (UriTypeConverter_t3912970448 * __this, const MethodInfo* method)
{
	{
		TypeConverter__ctor_m3380301159(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.UriTypeConverter::CanConvert(System.Type)
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Uri_t19570940_0_0_0_var;
extern const Il2CppType* InstanceDescriptor_t1404033120_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UriTypeConverter_CanConvert_m1602775223_MetadataUsageId;
extern "C"  bool UriTypeConverter_CanConvert_m1602775223 (UriTypeConverter_t3912970448 * __this, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UriTypeConverter_CanConvert_m1602775223_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		return (bool)1;
	}

IL_0012:
	{
		Type_t * L_2 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Uri_t19570940_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_2) == ((Il2CppObject*)(Type_t *)L_3))))
		{
			goto IL_0024;
		}
	}
	{
		return (bool)1;
	}

IL_0024:
	{
		Type_t * L_4 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(InstanceDescriptor_t1404033120_0_0_0_var), /*hidden argument*/NULL);
		return (bool)((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5))? 1 : 0);
	}
}
// System.Boolean System.UriTypeConverter::CanConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral954063241;
extern const uint32_t UriTypeConverter_CanConvertFrom_m3617467917_MetadataUsageId;
extern "C"  bool UriTypeConverter_CanConvertFrom_m3617467917 (UriTypeConverter_t3912970448 * __this, Il2CppObject * ___context0, Type_t * ___sourceType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UriTypeConverter_CanConvertFrom_m3617467917_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Type_t * L_0 = ___sourceType1;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral954063241, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Type_t * L_2 = ___sourceType1;
		bool L_3 = UriTypeConverter_CanConvert_m1602775223(__this, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean System.UriTypeConverter::CanConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Type)
extern "C"  bool UriTypeConverter_CanConvertTo_m1561226854 (UriTypeConverter_t3912970448 * __this, Il2CppObject * ___context0, Type_t * ___destinationType1, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___destinationType1;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		Type_t * L_1 = ___destinationType1;
		bool L_2 = UriTypeConverter_CanConvert_m1602775223(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Object System.UriTypeConverter::ConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* InstanceDescriptor_t1404033120_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern Il2CppCodeGenString* _stringLiteral2012645217;
extern const uint32_t UriTypeConverter_ConvertFrom_m2743854187_MetadataUsageId;
extern "C"  Il2CppObject * UriTypeConverter_ConvertFrom_m2743854187 (UriTypeConverter_t3912970448 * __this, Il2CppObject * ___context0, CultureInfo_t3500843524 * ___culture1, Il2CppObject * ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UriTypeConverter_ConvertFrom_m2743854187_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	InstanceDescriptor_t1404033120 * V_1 = NULL;
	{
		Il2CppObject * L_0 = ___value2;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1803325615, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___context0;
		Il2CppObject * L_3 = ___value2;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m191970594(L_3, /*hidden argument*/NULL);
		bool L_5 = VirtFuncInvoker2< bool, Il2CppObject *, Type_t * >::Invoke(4 /* System.Boolean System.UriTypeConverter::CanConvertFrom(System.ComponentModel.ITypeDescriptorContext,System.Type) */, __this, L_2, L_4);
		if (L_5)
		{
			goto IL_0033;
		}
	}
	{
		String_t* L_6 = Locale_GetText_m4034107474(NULL /*static, unused*/, _stringLiteral2012645217, /*hidden argument*/NULL);
		NotSupportedException_t1793819818 * L_7 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_7, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0033:
	{
		Il2CppObject * L_8 = ___value2;
		if (!((Uri_t19570940 *)IsInstClass(L_8, Uri_t19570940_il2cpp_TypeInfo_var)))
		{
			goto IL_0040;
		}
	}
	{
		Il2CppObject * L_9 = ___value2;
		return L_9;
	}

IL_0040:
	{
		Il2CppObject * L_10 = ___value2;
		V_0 = ((String_t*)IsInstSealed(L_10, String_t_il2cpp_TypeInfo_var));
		String_t* L_11 = V_0;
		if (!L_11)
		{
			goto IL_0055;
		}
	}
	{
		String_t* L_12 = V_0;
		Uri_t19570940 * L_13 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m1027317340(L_13, L_12, 0, /*hidden argument*/NULL);
		return L_13;
	}

IL_0055:
	{
		Il2CppObject * L_14 = ___value2;
		V_1 = ((InstanceDescriptor_t1404033120 *)IsInstSealed(L_14, InstanceDescriptor_t1404033120_il2cpp_TypeInfo_var));
		InstanceDescriptor_t1404033120 * L_15 = V_1;
		if (!L_15)
		{
			goto IL_0069;
		}
	}
	{
		InstanceDescriptor_t1404033120 * L_16 = V_1;
		NullCheck(L_16);
		Il2CppObject * L_17 = InstanceDescriptor_Invoke_m3441712808(L_16, /*hidden argument*/NULL);
		return L_17;
	}

IL_0069:
	{
		Il2CppObject * L_18 = ___context0;
		CultureInfo_t3500843524 * L_19 = ___culture1;
		Il2CppObject * L_20 = ___value2;
		Il2CppObject * L_21 = TypeConverter_ConvertFrom_m3174062747(__this, L_18, L_19, L_20, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.Object System.UriTypeConverter::ConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Globalization.CultureInfo,System.Object,System.Type)
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Uri_t19570940_0_0_0_var;
extern const Il2CppType* InstanceDescriptor_t1404033120_0_0_0_var;
extern const Il2CppType* UriKind_t1128731744_0_0_0_var;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* UriKind_t1128731744_il2cpp_TypeInfo_var;
extern Il2CppClass* InstanceDescriptor_t1404033120_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3186378199;
extern const uint32_t UriTypeConverter_ConvertTo_m507771013_MetadataUsageId;
extern "C"  Il2CppObject * UriTypeConverter_ConvertTo_m507771013 (UriTypeConverter_t3912970448 * __this, Il2CppObject * ___context0, CultureInfo_t3500843524 * ___culture1, Il2CppObject * ___value2, Type_t * ___destinationType3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UriTypeConverter_ConvertTo_m507771013_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Uri_t19570940 * V_0 = NULL;
	ConstructorInfo_t2851816542 * V_1 = NULL;
	int32_t G_B10_0 = 0;
	ObjectU5BU5D_t3614634134* G_B10_1 = NULL;
	ObjectU5BU5D_t3614634134* G_B10_2 = NULL;
	ConstructorInfo_t2851816542 * G_B10_3 = NULL;
	int32_t G_B9_0 = 0;
	ObjectU5BU5D_t3614634134* G_B9_1 = NULL;
	ObjectU5BU5D_t3614634134* G_B9_2 = NULL;
	ConstructorInfo_t2851816542 * G_B9_3 = NULL;
	int32_t G_B11_0 = 0;
	int32_t G_B11_1 = 0;
	ObjectU5BU5D_t3614634134* G_B11_2 = NULL;
	ObjectU5BU5D_t3614634134* G_B11_3 = NULL;
	ConstructorInfo_t2851816542 * G_B11_4 = NULL;
	{
		Il2CppObject * L_0 = ___context0;
		Type_t * L_1 = ___destinationType3;
		bool L_2 = VirtFuncInvoker2< bool, Il2CppObject *, Type_t * >::Invoke(5 /* System.Boolean System.UriTypeConverter::CanConvertTo(System.ComponentModel.ITypeDescriptorContext,System.Type) */, __this, L_0, L_1);
		if (L_2)
		{
			goto IL_001e;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m4034107474(NULL /*static, unused*/, _stringLiteral3186378199, /*hidden argument*/NULL);
		NotSupportedException_t1793819818 * L_4 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_4, L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_001e:
	{
		Il2CppObject * L_5 = ___value2;
		V_0 = ((Uri_t19570940 *)IsInstClass(L_5, Uri_t19570940_il2cpp_TypeInfo_var));
		Uri_t19570940 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t19570940_il2cpp_TypeInfo_var);
		bool L_7 = Uri_op_Inequality_m853767938(NULL /*static, unused*/, L_6, (Uri_t19570940 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00cd;
		}
	}
	{
		Type_t * L_8 = ___destinationType3;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_8) == ((Il2CppObject*)(Type_t *)L_9))))
		{
			goto IL_0049;
		}
	}
	{
		Uri_t19570940 * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Uri::ToString() */, L_10);
		return L_11;
	}

IL_0049:
	{
		Type_t * L_12 = ___destinationType3;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Uri_t19570940_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_12) == ((Il2CppObject*)(Type_t *)L_13))))
		{
			goto IL_005c;
		}
	}
	{
		Uri_t19570940 * L_14 = V_0;
		return L_14;
	}

IL_005c:
	{
		Type_t * L_15 = ___destinationType3;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(InstanceDescriptor_t1404033120_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_15) == ((Il2CppObject*)(Type_t *)L_16))))
		{
			goto IL_00cd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Uri_t19570940_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t1664964607* L_18 = ((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)2));
		Type_t * L_19 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_19);
		TypeU5BU5D_t1664964607* L_20 = L_18;
		Type_t * L_21 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(UriKind_t1128731744_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_21);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_21);
		NullCheck(L_17);
		ConstructorInfo_t2851816542 * L_22 = Type_GetConstructor_m132234455(L_17, L_20, /*hidden argument*/NULL);
		V_1 = L_22;
		ConstructorInfo_t2851816542 * L_23 = V_1;
		ObjectU5BU5D_t3614634134* L_24 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)2));
		Uri_t19570940 * L_25 = V_0;
		NullCheck(L_25);
		String_t* L_26 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Uri::ToString() */, L_25);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_26);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_26);
		ObjectU5BU5D_t3614634134* L_27 = L_24;
		Uri_t19570940 * L_28 = V_0;
		NullCheck(L_28);
		bool L_29 = Uri_get_IsAbsoluteUri_m4123650233(L_28, /*hidden argument*/NULL);
		G_B9_0 = 1;
		G_B9_1 = L_27;
		G_B9_2 = L_27;
		G_B9_3 = L_23;
		if (!L_29)
		{
			G_B10_0 = 1;
			G_B10_1 = L_27;
			G_B10_2 = L_27;
			G_B10_3 = L_23;
			goto IL_00c0;
		}
	}
	{
		G_B11_0 = 1;
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		G_B11_4 = G_B9_3;
		goto IL_00c1;
	}

IL_00c0:
	{
		G_B11_0 = 2;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
		G_B11_4 = G_B10_3;
	}

IL_00c1:
	{
		int32_t L_30 = ((int32_t)G_B11_0);
		Il2CppObject * L_31 = Box(UriKind_t1128731744_il2cpp_TypeInfo_var, &L_30);
		NullCheck(G_B11_2);
		ArrayElementTypeCheck (G_B11_2, L_31);
		(G_B11_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B11_1), (Il2CppObject *)L_31);
		InstanceDescriptor_t1404033120 * L_32 = (InstanceDescriptor_t1404033120 *)il2cpp_codegen_object_new(InstanceDescriptor_t1404033120_il2cpp_TypeInfo_var);
		InstanceDescriptor__ctor_m1126602508(L_32, G_B11_4, (Il2CppObject *)(Il2CppObject *)G_B11_3, /*hidden argument*/NULL);
		return L_32;
	}

IL_00cd:
	{
		Il2CppObject * L_33 = ___context0;
		CultureInfo_t3500843524 * L_34 = ___culture1;
		Il2CppObject * L_35 = ___value2;
		Type_t * L_36 = ___destinationType3;
		Il2CppObject * L_37 = TypeConverter_ConvertTo_m528793397(__this, L_33, L_34, L_35, L_36, /*hidden argument*/NULL);
		return L_37;
	}
}
// System.Boolean System.UriTypeConverter::IsValid(System.ComponentModel.ITypeDescriptorContext,System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern const uint32_t UriTypeConverter_IsValid_m2806107649_MetadataUsageId;
extern "C"  bool UriTypeConverter_IsValid_m2806107649 (UriTypeConverter_t3912970448 * __this, Il2CppObject * ___context0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UriTypeConverter_IsValid_m2806107649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B5_0 = 0;
	{
		Il2CppObject * L_0 = ___value1;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___value1;
		if (((String_t*)IsInstSealed(L_1, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppObject * L_2 = ___value1;
		G_B5_0 = ((!(((Il2CppObject*)(Uri_t19570940 *)((Uri_t19570940 *)IsInstClass(L_2, Uri_t19570940_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0);
		goto IL_001f;
	}

IL_001e:
	{
		G_B5_0 = 1;
	}

IL_001f:
	{
		return (bool)G_B5_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
