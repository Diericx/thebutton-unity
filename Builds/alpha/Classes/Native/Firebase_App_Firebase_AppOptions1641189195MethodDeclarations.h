﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.AppOptions
struct AppOptions_t1641189195;
// System.Uri
struct Uri_t19570940;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void Firebase.AppOptions::.ctor(System.IntPtr,System.Boolean)
extern "C"  void AppOptions__ctor_m1733148272 (AppOptions_t1641189195 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppOptions::Finalize()
extern "C"  void AppOptions_Finalize_m285105145 (AppOptions_t1641189195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppOptions::Dispose()
extern "C"  void AppOptions_Dispose_m287632940 (AppOptions_t1641189195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri Firebase.AppOptions::get_DatabaseUrl()
extern "C"  Uri_t19570940 * AppOptions_get_DatabaseUrl_m241783396 (AppOptions_t1641189195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppOptions::GetDatabaseUrlInternal()
extern "C"  String_t* AppOptions_GetDatabaseUrlInternal_m3701450289 (AppOptions_t1641189195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
