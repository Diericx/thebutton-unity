﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// Firebase.FutureString/SWIG_CompletionDelegate
struct SWIG_CompletionDelegate_t1819656562;
// Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate
struct LogMessageDelegate_t1988210674;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_HandleRef2419939847.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "Firebase_App_Firebase_FutureString_SWIG_Completion1819656562.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler_1988210674.h"

// System.Void Firebase.AppUtilPINVOKE::.cctor()
extern "C"  void AppUtilPINVOKE__cctor_m1477430606 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_FutureBase(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_FutureBase_m3328440028 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.AppUtilPINVOKE::Firebase_App_FutureBase_Status(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t AppUtilPINVOKE_Firebase_App_FutureBase_Status_m3094756663 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.AppUtilPINVOKE::Firebase_App_FutureBase_Error(System.Runtime.InteropServices.HandleRef)
extern "C"  int32_t AppUtilPINVOKE_Firebase_App_FutureBase_Error_m1475679819 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE::Firebase_App_FutureBase_ErrorMessage(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_FutureBase_ErrorMessage_m1466285231 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FutureString_SWIG_OnCompletion(System.Runtime.InteropServices.HandleRef,Firebase.FutureString/SWIG_CompletionDelegate,System.Int32)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_FutureString_SWIG_OnCompletion_m2011264479 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, SWIG_CompletionDelegate_t1819656562 * ___jarg21, int32_t ___jarg32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_FutureString_SWIG_FreeCompletionData(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern "C"  void AppUtilPINVOKE_Firebase_App_FutureString_SWIG_FreeCompletionData_m628326608 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, IntPtr_t ___jarg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE::Firebase_App_FutureString_Result(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_FutureString_Result_m1458338797 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_FutureString(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_FutureString_m298034936 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE::Firebase_App_AppOptions_GetDatabaseUrlInternal(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_AppOptions_GetDatabaseUrlInternal_m2442888338 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_AppOptions(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_AppOptions_m4039302585 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_FirebaseApp(System.Runtime.InteropServices.HandleRef)
extern "C"  void AppUtilPINVOKE_Firebase_App_delete_FirebaseApp_m1137114670 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_Name_get(System.Runtime.InteropServices.HandleRef)
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_FirebaseApp_Name_get_m2545680430 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_Options_get(System.Runtime.InteropServices.HandleRef)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_FirebaseApp_Options_get_m2245867877 (Il2CppObject * __this /* static, unused */, HandleRef_t2419939847  ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_CreateInternal__SWIG_0()
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_FirebaseApp_CreateInternal__SWIG_0_m2787922779 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_DefaultName_get()
extern "C"  String_t* AppUtilPINVOKE_Firebase_App_FirebaseApp_DefaultName_get_m4097276820 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_PollCallbacks()
extern "C"  void AppUtilPINVOKE_Firebase_App_PollCallbacks_m784944262 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_AppEnableLogCallback(System.Boolean)
extern "C"  void AppUtilPINVOKE_Firebase_App_AppEnableLogCallback_m3057939313 (Il2CppObject * __this /* static, unused */, bool ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.AppUtilPINVOKE::Firebase_App_AppGetLogLevel()
extern "C"  int32_t AppUtilPINVOKE_Firebase_App_AppGetLogLevel_m1131543554 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_SetEnabledAllAppCallbacks(System.Boolean)
extern "C"  void AppUtilPINVOKE_Firebase_App_SetEnabledAllAppCallbacks_m3339389901 (Il2CppObject * __this /* static, unused */, bool ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.AppUtilPINVOKE::Firebase_App_SetLogFunction(Firebase.FirebaseApp/FirebaseHandler/LogMessageDelegate)
extern "C"  void AppUtilPINVOKE_Firebase_App_SetLogFunction_m3733654825 (Il2CppObject * __this /* static, unused */, LogMessageDelegate_t1988210674 * ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FutureString_SWIGUpcast(System.IntPtr)
extern "C"  IntPtr_t AppUtilPINVOKE_Firebase_App_FutureString_SWIGUpcast_m1098833757 (Il2CppObject * __this /* static, unused */, IntPtr_t ___jarg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
