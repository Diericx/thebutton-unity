﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FirebaseApp/FirebaseHandler
struct FirebaseHandler_t2907300047;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_LogLevel543421840.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.FirebaseApp/FirebaseHandler::.ctor()
extern "C"  void FirebaseHandler__ctor_m1949563562 (FirebaseHandler_t2907300047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/FirebaseHandler::Create()
extern "C"  void FirebaseHandler_Create_m3248912600 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/FirebaseHandler::LogMessage(Firebase.LogLevel,System.String)
extern "C"  void FirebaseHandler_LogMessage_m1734897910 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_FirebaseHandler_LogMessage_m1734897910(int32_t ___logLevel0, char* ___message1);
// System.Void Firebase.FirebaseApp/FirebaseHandler::Update()
extern "C"  void FirebaseHandler_Update_m164025069 (FirebaseHandler_t2907300047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/FirebaseHandler::OnApplicationQuit()
extern "C"  void FirebaseHandler_OnApplicationQuit_m2676139308 (FirebaseHandler_t2907300047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseApp/FirebaseHandler::.cctor()
extern "C"  void FirebaseHandler__cctor_m901621883 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
