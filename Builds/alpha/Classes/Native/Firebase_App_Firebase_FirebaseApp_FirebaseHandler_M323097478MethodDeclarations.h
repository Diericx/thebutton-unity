﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FirebaseApp/FirebaseHandler/MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t323097478;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void Firebase.FirebaseApp/FirebaseHandler/MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern "C"  void MonoPInvokeCallbackAttribute__ctor_m287189802 (MonoPInvokeCallbackAttribute_t323097478 * __this, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
