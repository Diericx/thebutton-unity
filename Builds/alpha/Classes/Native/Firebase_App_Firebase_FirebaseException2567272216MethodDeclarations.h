﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FirebaseException
struct FirebaseException_t2567272216;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.FirebaseException::.ctor(System.Int32,System.String)
extern "C"  void FirebaseException__ctor_m3734642555 (FirebaseException_t2567272216 * __this, int32_t ___errorCode0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.FirebaseException::set_ErrorCode(System.Int32)
extern "C"  void FirebaseException_set_ErrorCode_m1261295849 (FirebaseException_t2567272216 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
