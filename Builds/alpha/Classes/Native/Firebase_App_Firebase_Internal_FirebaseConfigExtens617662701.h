﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct Dictionary_2_t2951825130;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Internal.FirebaseConfigExtensions
struct  FirebaseConfigExtensions_t617662701  : public Il2CppObject
{
public:

public:
};

struct FirebaseConfigExtensions_t617662701_StaticFields
{
public:
	// System.String Firebase.Internal.FirebaseConfigExtensions::Default
	String_t* ___Default_0;
	// System.Object Firebase.Internal.FirebaseConfigExtensions::Sync
	Il2CppObject * ___Sync_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.String>> Firebase.Internal.FirebaseConfigExtensions::SStringState
	Dictionary_2_t2951825130 * ___SStringState_2;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(FirebaseConfigExtensions_t617662701_StaticFields, ___Default_0)); }
	inline String_t* get_Default_0() const { return ___Default_0; }
	inline String_t** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(String_t* value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier(&___Default_0, value);
	}

	inline static int32_t get_offset_of_Sync_1() { return static_cast<int32_t>(offsetof(FirebaseConfigExtensions_t617662701_StaticFields, ___Sync_1)); }
	inline Il2CppObject * get_Sync_1() const { return ___Sync_1; }
	inline Il2CppObject ** get_address_of_Sync_1() { return &___Sync_1; }
	inline void set_Sync_1(Il2CppObject * value)
	{
		___Sync_1 = value;
		Il2CppCodeGenWriteBarrier(&___Sync_1, value);
	}

	inline static int32_t get_offset_of_SStringState_2() { return static_cast<int32_t>(offsetof(FirebaseConfigExtensions_t617662701_StaticFields, ___SStringState_2)); }
	inline Dictionary_2_t2951825130 * get_SStringState_2() const { return ___SStringState_2; }
	inline Dictionary_2_t2951825130 ** get_address_of_SStringState_2() { return &___SStringState_2; }
	inline void set_SStringState_2(Dictionary_2_t2951825130 * value)
	{
		___SStringState_2 = value;
		Il2CppCodeGenWriteBarrier(&___SStringState_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
