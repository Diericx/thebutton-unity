﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct Dictionary_2_t2951825130;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Internal.FirebaseEditorExtensions
struct  FirebaseEditorExtensions_t1010167188  : public Il2CppObject
{
public:

public:
};

struct FirebaseEditorExtensions_t1010167188_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.String>> Firebase.Internal.FirebaseEditorExtensions::SStringState
	Dictionary_2_t2951825130 * ___SStringState_0;

public:
	inline static int32_t get_offset_of_SStringState_0() { return static_cast<int32_t>(offsetof(FirebaseEditorExtensions_t1010167188_StaticFields, ___SStringState_0)); }
	inline Dictionary_2_t2951825130 * get_SStringState_0() const { return ___SStringState_0; }
	inline Dictionary_2_t2951825130 ** get_address_of_SStringState_0() { return &___SStringState_0; }
	inline void set_SStringState_0(Dictionary_2_t2951825130 * value)
	{
		___SStringState_0 = value;
		Il2CppCodeGenWriteBarrier(&___SStringState_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
