﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// Firebase.FirebaseApp
struct FirebaseApp_t210707726;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_FirebaseApp210707726.h"

// System.String Firebase.Internal.FirebaseEditorExtensions::GetEditorP12Password(Firebase.FirebaseApp)
extern "C"  String_t* FirebaseEditorExtensions_GetEditorP12Password_m873176310 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Internal.FirebaseEditorExtensions::GetEditorP12FileName(Firebase.FirebaseApp)
extern "C"  String_t* FirebaseEditorExtensions_GetEditorP12FileName_m620685606 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Internal.FirebaseEditorExtensions::GetEditorServiceAccountEmail(Firebase.FirebaseApp)
extern "C"  String_t* FirebaseEditorExtensions_GetEditorServiceAccountEmail_m460851250 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Internal.FirebaseEditorExtensions::GetEditorAuthUserId(Firebase.FirebaseApp)
extern "C"  String_t* FirebaseEditorExtensions_GetEditorAuthUserId_m2961892456 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Internal.FirebaseEditorExtensions::GetCertPemFile(Firebase.FirebaseApp)
extern "C"  String_t* FirebaseEditorExtensions_GetCertPemFile_m1950008923 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Internal.FirebaseEditorExtensions::.cctor()
extern "C"  void FirebaseEditorExtensions__cctor_m722228402 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
