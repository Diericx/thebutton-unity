﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>
struct Dictionary_2_t1664293826;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Internal.InstallRootCerts
struct  InstallRootCerts_t1106242208  : public Il2CppObject
{
public:

public:
};

struct InstallRootCerts_t1106242208_StaticFields
{
public:
	// System.Object Firebase.Internal.InstallRootCerts::Sync
	Il2CppObject * ___Sync_0;
	// System.Collections.Generic.Dictionary`2<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection> Firebase.Internal.InstallRootCerts::_installedRoots
	Dictionary_2_t1664293826 * ____installedRoots_1;
	// System.String Firebase.Internal.InstallRootCerts::TrustedRoot
	String_t* ___TrustedRoot_2;
	// System.String Firebase.Internal.InstallRootCerts::IntermediateCA
	String_t* ___IntermediateCA_3;

public:
	inline static int32_t get_offset_of_Sync_0() { return static_cast<int32_t>(offsetof(InstallRootCerts_t1106242208_StaticFields, ___Sync_0)); }
	inline Il2CppObject * get_Sync_0() const { return ___Sync_0; }
	inline Il2CppObject ** get_address_of_Sync_0() { return &___Sync_0; }
	inline void set_Sync_0(Il2CppObject * value)
	{
		___Sync_0 = value;
		Il2CppCodeGenWriteBarrier(&___Sync_0, value);
	}

	inline static int32_t get_offset_of__installedRoots_1() { return static_cast<int32_t>(offsetof(InstallRootCerts_t1106242208_StaticFields, ____installedRoots_1)); }
	inline Dictionary_2_t1664293826 * get__installedRoots_1() const { return ____installedRoots_1; }
	inline Dictionary_2_t1664293826 ** get_address_of__installedRoots_1() { return &____installedRoots_1; }
	inline void set__installedRoots_1(Dictionary_2_t1664293826 * value)
	{
		____installedRoots_1 = value;
		Il2CppCodeGenWriteBarrier(&____installedRoots_1, value);
	}

	inline static int32_t get_offset_of_TrustedRoot_2() { return static_cast<int32_t>(offsetof(InstallRootCerts_t1106242208_StaticFields, ___TrustedRoot_2)); }
	inline String_t* get_TrustedRoot_2() const { return ___TrustedRoot_2; }
	inline String_t** get_address_of_TrustedRoot_2() { return &___TrustedRoot_2; }
	inline void set_TrustedRoot_2(String_t* value)
	{
		___TrustedRoot_2 = value;
		Il2CppCodeGenWriteBarrier(&___TrustedRoot_2, value);
	}

	inline static int32_t get_offset_of_IntermediateCA_3() { return static_cast<int32_t>(offsetof(InstallRootCerts_t1106242208_StaticFields, ___IntermediateCA_3)); }
	inline String_t* get_IntermediateCA_3() const { return ___IntermediateCA_3; }
	inline String_t** get_address_of_IntermediateCA_3() { return &___IntermediateCA_3; }
	inline void set_IntermediateCA_3(String_t* value)
	{
		___IntermediateCA_3 = value;
		Il2CppCodeGenWriteBarrier(&___IntermediateCA_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
