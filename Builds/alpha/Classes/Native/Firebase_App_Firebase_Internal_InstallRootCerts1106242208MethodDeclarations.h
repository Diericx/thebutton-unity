﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t2766455145;
// System.String
struct String_t;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t1197680765;
// Firebase.FirebaseApp
struct FirebaseApp_t210707726;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t4056456767;
// System.Security.Cryptography.X509Certificates.X509ChainStatus[]
struct X509ChainStatusU5BU5D_t830390908;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "Firebase_App_Firebase_FirebaseApp210707726.h"
#include "Firebase_App_Firebase_LogLevel543421840.h"
#include "System_System_Security_Cryptography_X509Certificat4056456767.h"

// System.Collections.Generic.List`1<System.Byte[]> Firebase.Internal.InstallRootCerts::DecodeBase64Blobs(System.String,System.String,System.String)
extern "C"  List_1_t2766455145 * InstallRootCerts_DecodeBase64Blobs_m4010299014 (Il2CppObject * __this /* static, unused */, String_t* ___base64BlobList0, String_t* ___startLine1, String_t* ___endLine2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection Firebase.Internal.InstallRootCerts::DecodeCertificateCollectionFromString(System.String)
extern "C"  X509CertificateCollection_t1197680765 * InstallRootCerts_DecodeCertificateCollectionFromString_m2530752882 (Il2CppObject * __this /* static, unused */, String_t* ___certString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection Firebase.Internal.InstallRootCerts::DecodeDefaultCollection()
extern "C"  X509CertificateCollection_t1197680765 * InstallRootCerts_DecodeDefaultCollection_m56704705 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection Firebase.Internal.InstallRootCerts::DecodeCollection(Firebase.FirebaseApp)
extern "C"  X509CertificateCollection_t1197680765 * InstallRootCerts_DecodeCollection_m3158849309 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Internal.InstallRootCerts::InstallDefaultCRLs(System.String,System.String)
extern "C"  void InstallRootCerts_InstallDefaultCRLs_m2824657295 (Il2CppObject * __this /* static, unused */, String_t* ___resource_name0, String_t* ___directory1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Internal.InstallRootCerts::PrintCert(Firebase.LogLevel,System.Security.Cryptography.X509Certificates.X509Certificate2,System.Security.Cryptography.X509Certificates.X509ChainStatus[],System.String)
extern "C"  void InstallRootCerts_PrintCert_m2117415721 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, X509Certificate2_t4056456767 * ___cert1, X509ChainStatusU5BU5D_t830390908* ___chainElementStatus2, String_t* ___chainElementInformation3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection Firebase.Internal.InstallRootCerts::Process(Firebase.FirebaseApp)
extern "C"  X509CertificateCollection_t1197680765 * InstallRootCerts_Process_m1398406616 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Internal.InstallRootCerts::.cctor()
extern "C"  void InstallRootCerts__cctor_m516906450 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
