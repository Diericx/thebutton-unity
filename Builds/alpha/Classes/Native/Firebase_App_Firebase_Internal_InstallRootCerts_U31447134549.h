﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t2756269959;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Internal.InstallRootCerts/<Process>c__AnonStorey0
struct  U3CProcessU3Ec__AnonStorey0_t1447134549  : public Il2CppObject
{
public:
	// System.Net.Security.RemoteCertificateValidationCallback Firebase.Internal.InstallRootCerts/<Process>c__AnonStorey0::originalCallback
	RemoteCertificateValidationCallback_t2756269959 * ___originalCallback_0;

public:
	inline static int32_t get_offset_of_originalCallback_0() { return static_cast<int32_t>(offsetof(U3CProcessU3Ec__AnonStorey0_t1447134549, ___originalCallback_0)); }
	inline RemoteCertificateValidationCallback_t2756269959 * get_originalCallback_0() const { return ___originalCallback_0; }
	inline RemoteCertificateValidationCallback_t2756269959 ** get_address_of_originalCallback_0() { return &___originalCallback_0; }
	inline void set_originalCallback_0(RemoteCertificateValidationCallback_t2756269959 * value)
	{
		___originalCallback_0 = value;
		Il2CppCodeGenWriteBarrier(&___originalCallback_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
