﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Internal.InstallRootCerts/<Process>c__AnonStorey0
struct U3CProcessU3Ec__AnonStorey0_t1447134549;
// System.Object
struct Il2CppObject;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t283079845;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t777637347;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Security_Cryptography_X509Certifica283079845.h"
#include "System_System_Security_Cryptography_X509Certificate777637347.h"
#include "System_System_Net_Security_SslPolicyErrors1928581431.h"

// System.Void Firebase.Internal.InstallRootCerts/<Process>c__AnonStorey0::.ctor()
extern "C"  void U3CProcessU3Ec__AnonStorey0__ctor_m1025331492 (U3CProcessU3Ec__AnonStorey0_t1447134549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.Internal.InstallRootCerts/<Process>c__AnonStorey0::<>m__0(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern "C"  bool U3CProcessU3Ec__AnonStorey0_U3CU3Em__0_m2591264586 (U3CProcessU3Ec__AnonStorey0_t1447134549 * __this, Il2CppObject * ___sender0, X509Certificate_t283079845 * ___certificate1, X509Chain_t777637347 * ___chain2, int32_t ___sslPolicyErrors3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
