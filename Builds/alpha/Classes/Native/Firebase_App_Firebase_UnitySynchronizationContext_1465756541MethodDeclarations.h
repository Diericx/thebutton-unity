﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.UnitySynchronizationContext/<Send>c__AnonStorey4
struct U3CSendU3Ec__AnonStorey4_t1465756541;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.UnitySynchronizationContext/<Send>c__AnonStorey4::.ctor()
extern "C"  void U3CSendU3Ec__AnonStorey4__ctor_m2424407232 (U3CSendU3Ec__AnonStorey4_t1465756541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
