﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.UnitySynchronizationContext/<Send>c__AnonStorey3
struct U3CSendU3Ec__AnonStorey3_t1465756544;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.UnitySynchronizationContext/<Send>c__AnonStorey3::.ctor()
extern "C"  void U3CSendU3Ec__AnonStorey3__ctor_m1310654673 (U3CSendU3Ec__AnonStorey3_t1465756544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
