﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// Firebase.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey1
struct U3CSendCoroutineU3Ec__AnonStorey1_t3854140196;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey2
struct  U3CSendCoroutineU3Ec__AnonStorey2_t3450855669  : public Il2CppObject
{
public:
	// System.Threading.ManualResetEvent Firebase.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey2::newSignal
	ManualResetEvent_t926074657 * ___newSignal_0;
	// Firebase.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey1 Firebase.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey2::<>f__ref$1
	U3CSendCoroutineU3Ec__AnonStorey1_t3854140196 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_newSignal_0() { return static_cast<int32_t>(offsetof(U3CSendCoroutineU3Ec__AnonStorey2_t3450855669, ___newSignal_0)); }
	inline ManualResetEvent_t926074657 * get_newSignal_0() const { return ___newSignal_0; }
	inline ManualResetEvent_t926074657 ** get_address_of_newSignal_0() { return &___newSignal_0; }
	inline void set_newSignal_0(ManualResetEvent_t926074657 * value)
	{
		___newSignal_0 = value;
		Il2CppCodeGenWriteBarrier(&___newSignal_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3CSendCoroutineU3Ec__AnonStorey2_t3450855669, ___U3CU3Ef__refU241_1)); }
	inline U3CSendCoroutineU3Ec__AnonStorey1_t3854140196 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3CSendCoroutineU3Ec__AnonStorey1_t3854140196 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3CSendCoroutineU3Ec__AnonStorey1_t3854140196 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
