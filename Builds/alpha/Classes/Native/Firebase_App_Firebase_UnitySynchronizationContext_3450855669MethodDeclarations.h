﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey2
struct U3CSendCoroutineU3Ec__AnonStorey2_t3450855669;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey2::.ctor()
extern "C"  void U3CSendCoroutineU3Ec__AnonStorey2__ctor_m568898540 (U3CSendCoroutineU3Ec__AnonStorey2_t3450855669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Firebase.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey2::<>m__0()
extern "C"  Il2CppObject * U3CSendCoroutineU3Ec__AnonStorey2_U3CU3Em__0_m2167246975 (U3CSendCoroutineU3Ec__AnonStorey2_t3450855669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
