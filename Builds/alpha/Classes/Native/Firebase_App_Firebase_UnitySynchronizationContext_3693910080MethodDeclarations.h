﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0
struct U3CSignaledCoroutineU3Ec__Iterator0_t3693910080;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::.ctor()
extern "C"  void U3CSignaledCoroutineU3Ec__Iterator0__ctor_m3075902269 (U3CSignaledCoroutineU3Ec__Iterator0_t3693910080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::MoveNext()
extern "C"  bool U3CSignaledCoroutineU3Ec__Iterator0_MoveNext_m53105779 (U3CSignaledCoroutineU3Ec__Iterator0_t3693910080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Firebase.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSignaledCoroutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4036536747 (U3CSignaledCoroutineU3Ec__Iterator0_t3693910080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Firebase.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSignaledCoroutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3163236739 (U3CSignaledCoroutineU3Ec__Iterator0_t3693910080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::Dispose()
extern "C"  void U3CSignaledCoroutineU3Ec__Iterator0_Dispose_m777660278 (U3CSignaledCoroutineU3Ec__Iterator0_t3693910080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.UnitySynchronizationContext/<SignaledCoroutine>c__Iterator0::Reset()
extern "C"  void U3CSignaledCoroutineU3Ec__Iterator0_Reset_m1946727928 (U3CSignaledCoroutineU3Ec__Iterator0_t3693910080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
