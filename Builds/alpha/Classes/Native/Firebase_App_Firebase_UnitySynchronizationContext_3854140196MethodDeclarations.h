﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey1
struct U3CSendCoroutineU3Ec__AnonStorey1_t3854140196;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.UnitySynchronizationContext/<SendCoroutine>c__AnonStorey1::.ctor()
extern "C"  void U3CSendCoroutineU3Ec__AnonStorey1__ctor_m567649193 (U3CSendCoroutineU3Ec__AnonStorey1_t3854140196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
