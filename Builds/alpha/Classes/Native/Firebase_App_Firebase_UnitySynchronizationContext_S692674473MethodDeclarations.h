﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir
struct SynchronizationContextBehavoir_t692674473;
// System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>>
struct Queue_1_t279829387;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir::.ctor()
extern "C"  void SynchronizationContextBehavoir__ctor_m3448616242 (SynchronizationContextBehavoir_t692674473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>> Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir::get_CallbackQueue()
extern "C"  Queue_1_t279829387 * SynchronizationContextBehavoir_get_CallbackQueue_m2312663759 (SynchronizationContextBehavoir_t692674473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Firebase.UnitySynchronizationContext/SynchronizationContextBehavoir::Start()
extern "C"  Il2CppObject * SynchronizationContextBehavoir_Start_m3359373602 (SynchronizationContextBehavoir_t692674473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
