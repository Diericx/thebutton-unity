﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper
struct SWIGStringHelper_t1725176893;
// Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate
struct SWIGStringDelegate_t553428412;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE_SWIGStr553428412.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper::.cctor()
extern "C"  void SWIGStringHelper__cctor_m1662264752 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper::.ctor()
extern "C"  void SWIGStringHelper__ctor_m1377274003 (SWIGStringHelper_t1725176893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper::SWIGRegisterStringCallback_AuthUtil(Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate)
extern "C"  void SWIGStringHelper_SWIGRegisterStringCallback_AuthUtil_m2804163847 (Il2CppObject * __this /* static, unused */, SWIGStringDelegate_t553428412 * ___stringDelegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper::CreateString(System.String)
extern "C"  String_t* SWIGStringHelper_CreateString_m881484445 (Il2CppObject * __this /* static, unused */, String_t* ___cString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" char* DEFAULT_CALL ReversePInvokeWrapper_SWIGStringHelper_CreateString_m881484445(char* ___cString0);
