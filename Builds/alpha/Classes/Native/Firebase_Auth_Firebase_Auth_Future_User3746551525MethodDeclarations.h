﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Auth.Future_User
struct Future_User_t3746551525;
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>
struct Task_1_t3166995609;
// Firebase.Auth.Future_User/Action
struct Action_t1614918345;
// Firebase.Auth.Future_User/SWIG_CompletionDelegate
struct SWIG_CompletionDelegate_t2830873745;
// Firebase.Auth.FirebaseUser
struct FirebaseUser_t4046966602;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "Firebase_Auth_Firebase_Auth_Future_User3746551525.h"
#include "Firebase_Auth_Firebase_Auth_Future_User_Action1614918345.h"
#include "Firebase_Auth_Firebase_Auth_Future_User_SWIG_Compl2830873745.h"

// System.Void Firebase.Auth.Future_User::.ctor(System.IntPtr,System.Boolean)
extern "C"  void Future_User__ctor_m341523963 (Future_User_t3746551525 * __this, IntPtr_t ___cPtr0, bool ___cMemoryOwn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Auth.Future_User::Finalize()
extern "C"  void Future_User_Finalize_m2443298420 (Future_User_t3746551525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Auth.Future_User::Dispose()
extern "C"  void Future_User_Dispose_m3386840133 (Future_User_t3746551525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.Future_User::GetTask(Firebase.Auth.Future_User)
extern "C"  Task_1_t3166995609 * Future_User_GetTask_m1069196464 (Il2CppObject * __this /* static, unused */, Future_User_t3746551525 * ___fu0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Auth.Future_User::SetOnCompletionCallback(Firebase.Auth.Future_User/Action)
extern "C"  void Future_User_SetOnCompletionCallback_m2991677111 (Future_User_t3746551525 * __this, Action_t1614918345 * ___userCompletionCallback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Auth.Future_User::SetCompletionData(System.IntPtr)
extern "C"  void Future_User_SetCompletionData_m2358975060 (Future_User_t3746551525 * __this, IntPtr_t ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Auth.Future_User::SWIG_CompletionDispatcher(System.Int32)
extern "C"  void Future_User_SWIG_CompletionDispatcher_m1755402693 (Il2CppObject * __this /* static, unused */, int32_t ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Future_User_SWIG_CompletionDispatcher_m1755402693(int32_t ___key0);
// System.IntPtr Firebase.Auth.Future_User::SWIG_OnCompletion(Firebase.Auth.Future_User/SWIG_CompletionDelegate,System.Int32)
extern "C"  IntPtr_t Future_User_SWIG_OnCompletion_m2671901145 (Future_User_t3746551525 * __this, SWIG_CompletionDelegate_t2830873745 * ___cs_callback0, int32_t ___cs_key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Auth.Future_User::SWIG_FreeCompletionData(System.IntPtr)
extern "C"  void Future_User_SWIG_FreeCompletionData_m2921042785 (Future_User_t3746551525 * __this, IntPtr_t ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Auth.FirebaseUser Firebase.Auth.Future_User::Result()
extern "C"  FirebaseUser_t4046966602 * Future_User_Result_m1430738177 (Future_User_t3746551525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Auth.Future_User::.cctor()
extern "C"  void Future_User__cctor_m3764872609 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
