﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Auth.Future_User/<GetTask>c__AnonStorey0
struct U3CGetTaskU3Ec__AnonStorey0_t2356161773;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Auth.Future_User/<GetTask>c__AnonStorey0::.ctor()
extern "C"  void U3CGetTaskU3Ec__AnonStorey0__ctor_m3797510095 (U3CGetTaskU3Ec__AnonStorey0_t2356161773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Auth.Future_User/<GetTask>c__AnonStorey0::<>m__0()
extern "C"  void U3CGetTaskU3Ec__AnonStorey0_U3CU3Em__0_m618574514 (U3CGetTaskU3Ec__AnonStorey0_t2356161773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
