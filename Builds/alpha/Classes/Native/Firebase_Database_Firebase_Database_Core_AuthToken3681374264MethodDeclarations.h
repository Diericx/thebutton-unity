﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Core.AuthTokenProvider
struct AuthTokenProvider_t3681374264;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Core.AuthTokenProvider::.ctor()
extern "C"  void AuthTokenProvider__ctor_m3484254436 (AuthTokenProvider_t3681374264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
