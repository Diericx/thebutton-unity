﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<Firebase.FirebaseApp,Firebase.Database.DotNet.DotNetPlatform>
struct Func_2_t3961743794;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Database.Core.FirebaseConfigExtensions
struct  FirebaseConfigExtensions_t1046005831  : public Il2CppObject
{
public:

public:
};

struct FirebaseConfigExtensions_t1046005831_StaticFields
{
public:
	// System.Func`2<Firebase.FirebaseApp,Firebase.Database.DotNet.DotNetPlatform> Firebase.Database.Core.FirebaseConfigExtensions::_platformFactory
	Func_2_t3961743794 * ____platformFactory_0;

public:
	inline static int32_t get_offset_of__platformFactory_0() { return static_cast<int32_t>(offsetof(FirebaseConfigExtensions_t1046005831_StaticFields, ____platformFactory_0)); }
	inline Func_2_t3961743794 * get__platformFactory_0() const { return ____platformFactory_0; }
	inline Func_2_t3961743794 ** get_address_of__platformFactory_0() { return &____platformFactory_0; }
	inline void set__platformFactory_0(Func_2_t3961743794 * value)
	{
		____platformFactory_0 = value;
		Il2CppCodeGenWriteBarrier(&____platformFactory_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
