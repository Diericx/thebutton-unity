﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Core.FirebaseConfigExtensions/<CreatePlatform>c__AnonStorey1
struct U3CCreatePlatformU3Ec__AnonStorey1_t1927240834;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Core.FirebaseConfigExtensions/<CreatePlatform>c__AnonStorey1::.ctor()
extern "C"  void U3CCreatePlatformU3Ec__AnonStorey1__ctor_m4007955861 (U3CCreatePlatformU3Ec__AnonStorey1_t1927240834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
