﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// Firebase.Database.Core.FirebaseConfigExtensions/<CreatePlatform>c__AnonStorey1
struct U3CCreatePlatformU3Ec__AnonStorey1_t1927240834;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Database.Core.FirebaseConfigExtensions/<CreatePlatform>c__AnonStorey0
struct  U3CCreatePlatformU3Ec__AnonStorey0_t3493324775  : public Il2CppObject
{
public:
	// System.Type Firebase.Database.Core.FirebaseConfigExtensions/<CreatePlatform>c__AnonStorey0::unityType
	Type_t * ___unityType_0;
	// Firebase.Database.Core.FirebaseConfigExtensions/<CreatePlatform>c__AnonStorey1 Firebase.Database.Core.FirebaseConfigExtensions/<CreatePlatform>c__AnonStorey0::<>f__ref$1
	U3CCreatePlatformU3Ec__AnonStorey1_t1927240834 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_unityType_0() { return static_cast<int32_t>(offsetof(U3CCreatePlatformU3Ec__AnonStorey0_t3493324775, ___unityType_0)); }
	inline Type_t * get_unityType_0() const { return ___unityType_0; }
	inline Type_t ** get_address_of_unityType_0() { return &___unityType_0; }
	inline void set_unityType_0(Type_t * value)
	{
		___unityType_0 = value;
		Il2CppCodeGenWriteBarrier(&___unityType_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3CCreatePlatformU3Ec__AnonStorey0_t3493324775, ___U3CU3Ef__refU241_1)); }
	inline U3CCreatePlatformU3Ec__AnonStorey1_t1927240834 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3CCreatePlatformU3Ec__AnonStorey1_t1927240834 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3CCreatePlatformU3Ec__AnonStorey1_t1927240834 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
