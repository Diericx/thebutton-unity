﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Core.FirebaseConfigExtensions/<CreatePlatform>c__AnonStorey0
struct U3CCreatePlatformU3Ec__AnonStorey0_t3493324775;
// Firebase.Database.DotNet.DotNetPlatform
struct DotNetPlatform_t2951135975;
// Firebase.FirebaseApp
struct FirebaseApp_t210707726;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_FirebaseApp210707726.h"

// System.Void Firebase.Database.Core.FirebaseConfigExtensions/<CreatePlatform>c__AnonStorey0::.ctor()
extern "C"  void U3CCreatePlatformU3Ec__AnonStorey0__ctor_m420972144 (U3CCreatePlatformU3Ec__AnonStorey0_t3493324775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Database.DotNet.DotNetPlatform Firebase.Database.Core.FirebaseConfigExtensions/<CreatePlatform>c__AnonStorey0::<>m__0(Firebase.FirebaseApp)
extern "C"  DotNetPlatform_t2951135975 * U3CCreatePlatformU3Ec__AnonStorey0_U3CU3Em__0_m1230665814 (U3CCreatePlatformU3Ec__AnonStorey0_t3493324775 * __this, FirebaseApp_t210707726 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
