﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.DatabaseReference/Runnable226
struct Runnable226_t2944834882;
// Firebase.Database.DatabaseReference
struct DatabaseReference_t1167676104;
// Firebase.Database.Internal.Snapshot.Node
struct Node_t2640059010;
// Firebase.Database.Internal.Utilities.Pair`2<System.Threading.Tasks.Task`1<System.Object>,Firebase.Database.DatabaseReference/CompletionListener>
struct Pair_2_t2408862168;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_DatabaseRefere1167676104.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps2640059010.h"

// System.Void Firebase.Database.DatabaseReference/Runnable226::.ctor(Firebase.Database.DatabaseReference,Firebase.Database.Internal.Snapshot.Node,Firebase.Database.Internal.Utilities.Pair`2<System.Threading.Tasks.Task`1<System.Object>,Firebase.Database.DatabaseReference/CompletionListener>)
extern "C"  void Runnable226__ctor_m2988636269 (Runnable226_t2944834882 * __this, DatabaseReference_t1167676104 * ___enclosing0, Node_t2640059010 * ___node1, Pair_2_t2408862168 * ___wrapped2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.DatabaseReference/Runnable226::Run()
extern "C"  void Runnable226_Run_m1966938924 (Runnable226_t2944834882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
