﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.DotNet.DotNetPlatform/SynchronizationContextTarget/<PostEvent>c__AnonStorey0
struct U3CPostEventU3Ec__AnonStorey0_t1624051538;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Firebase.Database.DotNet.DotNetPlatform/SynchronizationContextTarget/<PostEvent>c__AnonStorey0::.ctor()
extern "C"  void U3CPostEventU3Ec__AnonStorey0__ctor_m2341392877 (U3CPostEventU3Ec__AnonStorey0_t1624051538 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.DotNet.DotNetPlatform/SynchronizationContextTarget/<PostEvent>c__AnonStorey0::<>m__0(System.Object)
extern "C"  void U3CPostEventU3Ec__AnonStorey0_U3CU3Em__0_m1247582084 (U3CPostEventU3Ec__AnonStorey0_t1624051538 * __this, Il2CppObject * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
