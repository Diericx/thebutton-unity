﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.DotNet.DotNetPlatform/DefaultRunLoop40
struct DefaultRunLoop40_t4147568072;
// Firebase.Database.Internal.Logging.LogWrapper
struct LogWrapper_t438307305;
// System.Exception
struct Exception_t1927440687;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Loggin438307305.h"
#include "mscorlib_System_Exception1927440687.h"

// System.Void Firebase.Database.DotNet.DotNetPlatform/DefaultRunLoop40::.ctor(Firebase.Database.Internal.Logging.LogWrapper)
extern "C"  void DefaultRunLoop40__ctor_m2671039806 (DefaultRunLoop40_t4147568072 * __this, LogWrapper_t438307305 * ___logger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.DotNet.DotNetPlatform/DefaultRunLoop40::HandleException(System.Exception)
extern "C"  void DefaultRunLoop40_HandleException_m4023187246 (DefaultRunLoop40_t4147568072 * __this, Exception_t1927440687 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
