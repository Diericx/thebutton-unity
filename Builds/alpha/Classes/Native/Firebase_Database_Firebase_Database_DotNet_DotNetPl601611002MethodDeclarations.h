﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.DotNet.DotNetPlatform/SynchronizationContextTarget
struct SynchronizationContextTarget_t601611002;
// Google.Sharpen.Runnable
struct Runnable_t1446984663;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.DotNet.DotNetPlatform/SynchronizationContextTarget::.ctor()
extern "C"  void SynchronizationContextTarget__ctor_m401898139 (SynchronizationContextTarget_t601611002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.DotNet.DotNetPlatform/SynchronizationContextTarget::PostEvent(Google.Sharpen.Runnable)
extern "C"  void SynchronizationContextTarget_PostEvent_m1797652624 (SynchronizationContextTarget_t601611002 * __this, Il2CppObject * ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.DotNet.DotNetPlatform/SynchronizationContextTarget::Restart()
extern "C"  void SynchronizationContextTarget_Restart_m3641902610 (SynchronizationContextTarget_t601611002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
