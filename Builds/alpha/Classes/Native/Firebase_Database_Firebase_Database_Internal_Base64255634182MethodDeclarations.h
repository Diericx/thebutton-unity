﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Base64/Coder
struct Coder_t4255634182;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Base64/Coder::.ctor()
extern "C"  void Coder__ctor_m924679955 (Coder_t4255634182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
