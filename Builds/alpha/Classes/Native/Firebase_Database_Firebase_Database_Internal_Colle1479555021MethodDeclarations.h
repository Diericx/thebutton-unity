﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Collection.ArraySortedMap`2/Enumerator134<System.Object,System.Object>
struct Enumerator134_t1479555021;
// Firebase.Database.Internal.Collection.ArraySortedMap`2<System.Object,System.Object>
struct ArraySortedMap_2_t3398066313;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"

// System.Void Firebase.Database.Internal.Collection.ArraySortedMap`2/Enumerator134<System.Object,System.Object>::.ctor(Firebase.Database.Internal.Collection.ArraySortedMap`2<TK,TV>,System.Int32,System.Boolean)
extern "C"  void Enumerator134__ctor_m2329147759_gshared (Enumerator134_t1479555021 * __this, ArraySortedMap_2_t3398066313 * ___enclosing0, int32_t ___pos1, bool ___reverse2, const MethodInfo* method);
#define Enumerator134__ctor_m2329147759(__this, ___enclosing0, ___pos1, ___reverse2, method) ((  void (*) (Enumerator134_t1479555021 *, ArraySortedMap_2_t3398066313 *, int32_t, bool, const MethodInfo*))Enumerator134__ctor_m2329147759_gshared)(__this, ___enclosing0, ___pos1, ___reverse2, method)
// System.Boolean Firebase.Database.Internal.Collection.ArraySortedMap`2/Enumerator134<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator134_MoveNext_m3794421025_gshared (Enumerator134_t1479555021 * __this, const MethodInfo* method);
#define Enumerator134_MoveNext_m3794421025(__this, method) ((  bool (*) (Enumerator134_t1479555021 *, const MethodInfo*))Enumerator134_MoveNext_m3794421025_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TK,TV> Firebase.Database.Internal.Collection.ArraySortedMap`2/Enumerator134<System.Object,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t38854645  Enumerator134_get_Current_m751914458_gshared (Enumerator134_t1479555021 * __this, const MethodInfo* method);
#define Enumerator134_get_Current_m751914458(__this, method) ((  KeyValuePair_2_t38854645  (*) (Enumerator134_t1479555021 *, const MethodInfo*))Enumerator134_get_Current_m751914458_gshared)(__this, method)
// System.Void Firebase.Database.Internal.Collection.ArraySortedMap`2/Enumerator134<System.Object,System.Object>::set_Current(System.Collections.Generic.KeyValuePair`2<TK,TV>)
extern "C"  void Enumerator134_set_Current_m627319475_gshared (Enumerator134_t1479555021 * __this, KeyValuePair_2_t38854645  ___value0, const MethodInfo* method);
#define Enumerator134_set_Current_m627319475(__this, ___value0, method) ((  void (*) (Enumerator134_t1479555021 *, KeyValuePair_2_t38854645 , const MethodInfo*))Enumerator134_set_Current_m627319475_gshared)(__this, ___value0, method)
// System.Object Firebase.Database.Internal.Collection.ArraySortedMap`2/Enumerator134<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator134_System_Collections_IEnumerator_get_Current_m3695877741_gshared (Enumerator134_t1479555021 * __this, const MethodInfo* method);
#define Enumerator134_System_Collections_IEnumerator_get_Current_m3695877741(__this, method) ((  Il2CppObject * (*) (Enumerator134_t1479555021 *, const MethodInfo*))Enumerator134_System_Collections_IEnumerator_get_Current_m3695877741_gshared)(__this, method)
// System.Void Firebase.Database.Internal.Collection.ArraySortedMap`2/Enumerator134<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator134_Dispose_m3049192278_gshared (Enumerator134_t1479555021 * __this, const MethodInfo* method);
#define Enumerator134_Dispose_m3049192278(__this, method) ((  void (*) (Enumerator134_t1479555021 *, const MethodInfo*))Enumerator134_Dispose_m3049192278_gshared)(__this, method)
// System.Void Firebase.Database.Internal.Collection.ArraySortedMap`2/Enumerator134<System.Object,System.Object>::Reset()
extern "C"  void Enumerator134_Reset_m2928477348_gshared (Enumerator134_t1479555021 * __this, const MethodInfo* method);
#define Enumerator134_Reset_m2928477348(__this, method) ((  void (*) (Enumerator134_t1479555021 *, const MethodInfo*))Enumerator134_Reset_m2928477348_gshared)(__this, method)
