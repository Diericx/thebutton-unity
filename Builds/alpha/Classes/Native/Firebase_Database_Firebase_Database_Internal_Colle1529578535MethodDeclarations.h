﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>
struct ImmutableSortedMap_2_t3217094540;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t643912417;
// Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder/IKeyTranslator<System.Object,System.Object>
struct IKeyTranslator_t1428849306;

#include "codegen/il2cpp-codegen.h"

// Firebase.Database.Internal.Collection.ImmutableSortedMap`2<TK,TV> Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder<System.Object,System.Object>::EmptyMap(System.Collections.Generic.IComparer`1<TK>)
extern "C"  ImmutableSortedMap_2_t3217094540 * Builder_EmptyMap_m3930589567_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___comparator0, const MethodInfo* method);
#define Builder_EmptyMap_m3930589567(__this /* static, unused */, ___comparator0, method) ((  ImmutableSortedMap_2_t3217094540 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Builder_EmptyMap_m3930589567_gshared)(__this /* static, unused */, ___comparator0, method)
// Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder/IKeyTranslator<TK,TV> Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder<System.Object,System.Object>::IdentityTranslator()
extern "C"  Il2CppObject* Builder_IdentityTranslator_m224979022_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Builder_IdentityTranslator_m224979022(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Builder_IdentityTranslator_m224979022_gshared)(__this /* static, unused */, method)
// System.Void Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder<System.Object,System.Object>::.cctor()
extern "C"  void Builder__cctor_m1907675492_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Builder__cctor_m1907675492(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Builder__cctor_m1907675492_gshared)(__this /* static, unused */, method)
