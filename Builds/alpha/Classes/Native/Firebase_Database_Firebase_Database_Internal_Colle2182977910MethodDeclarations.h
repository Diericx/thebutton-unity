﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Collection.StandardComparator`1<System.Object>
struct StandardComparator_1_t2182977910;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Firebase.Database.Internal.Collection.StandardComparator`1<System.Object>::.ctor()
extern "C"  void StandardComparator_1__ctor_m3122475602_gshared (StandardComparator_1_t2182977910 * __this, const MethodInfo* method);
#define StandardComparator_1__ctor_m3122475602(__this, method) ((  void (*) (StandardComparator_1_t2182977910 *, const MethodInfo*))StandardComparator_1__ctor_m3122475602_gshared)(__this, method)
// System.Int32 Firebase.Database.Internal.Collection.StandardComparator`1<System.Object>::Compare(TA,TA)
extern "C"  int32_t StandardComparator_1_Compare_m517984725_gshared (StandardComparator_1_t2182977910 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define StandardComparator_1_Compare_m517984725(__this, ___x0, ___y1, method) ((  int32_t (*) (StandardComparator_1_t2182977910 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))StandardComparator_1_Compare_m517984725_gshared)(__this, ___x0, ___y1, method)
// Firebase.Database.Internal.Collection.StandardComparator`1<TA> Firebase.Database.Internal.Collection.StandardComparator`1<System.Object>::GetComparator()
extern "C"  StandardComparator_1_t2182977910 * StandardComparator_1_GetComparator_m3420190789_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define StandardComparator_1_GetComparator_m3420190789(__this /* static, unused */, method) ((  StandardComparator_1_t2182977910 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))StandardComparator_1_GetComparator_m3420190789_gshared)(__this /* static, unused */, method)
// System.Void Firebase.Database.Internal.Collection.StandardComparator`1<System.Object>::.cctor()
extern "C"  void StandardComparator_1__cctor_m3768252471_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define StandardComparator_1__cctor_m3768252471(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))StandardComparator_1__cctor_m3768252471_gshared)(__this /* static, unused */, method)
