﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>
struct ImmutableSortedSet_1_t2348759144;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3230389896;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t643912417;
// Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>
struct ImmutableSortedMap_2_t3217094540;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>::.ctor(System.Collections.Generic.IList`1<T>,System.Collections.Generic.IComparer`1<T>)
extern "C"  void ImmutableSortedSet_1__ctor_m1945539485_gshared (ImmutableSortedSet_1_t2348759144 * __this, Il2CppObject* ___elems0, Il2CppObject* ___comparator1, const MethodInfo* method);
#define ImmutableSortedSet_1__ctor_m1945539485(__this, ___elems0, ___comparator1, method) ((  void (*) (ImmutableSortedSet_1_t2348759144 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))ImmutableSortedSet_1__ctor_m1945539485_gshared)(__this, ___elems0, ___comparator1, method)
// System.Void Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>::.ctor(Firebase.Database.Internal.Collection.ImmutableSortedMap`2<T,System.Object>)
extern "C"  void ImmutableSortedSet_1__ctor_m1960942718_gshared (ImmutableSortedSet_1_t2348759144 * __this, ImmutableSortedMap_2_t3217094540 * ___map0, const MethodInfo* method);
#define ImmutableSortedSet_1__ctor_m1960942718(__this, ___map0, method) ((  void (*) (ImmutableSortedSet_1_t2348759144 *, ImmutableSortedMap_2_t3217094540 *, const MethodInfo*))ImmutableSortedSet_1__ctor_m1960942718_gshared)(__this, ___map0, method)
// System.Collections.Generic.IEnumerator`1<T> Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ImmutableSortedSet_1_GetEnumerator_m3228214528_gshared (ImmutableSortedSet_1_t2348759144 * __this, const MethodInfo* method);
#define ImmutableSortedSet_1_GetEnumerator_m3228214528(__this, method) ((  Il2CppObject* (*) (ImmutableSortedSet_1_t2348759144 *, const MethodInfo*))ImmutableSortedSet_1_GetEnumerator_m3228214528_gshared)(__this, method)
// System.Collections.IEnumerator Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ImmutableSortedSet_1_System_Collections_IEnumerable_GetEnumerator_m16219723_gshared (ImmutableSortedSet_1_t2348759144 * __this, const MethodInfo* method);
#define ImmutableSortedSet_1_System_Collections_IEnumerable_GetEnumerator_m16219723(__this, method) ((  Il2CppObject * (*) (ImmutableSortedSet_1_t2348759144 *, const MethodInfo*))ImmutableSortedSet_1_System_Collections_IEnumerable_GetEnumerator_m16219723_gshared)(__this, method)
// Firebase.Database.Internal.Collection.ImmutableSortedSet`1<T> Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>::Remove(T)
extern "C"  ImmutableSortedSet_1_t2348759144 * ImmutableSortedSet_1_Remove_m3750122640_gshared (ImmutableSortedSet_1_t2348759144 * __this, Il2CppObject * ___entry0, const MethodInfo* method);
#define ImmutableSortedSet_1_Remove_m3750122640(__this, ___entry0, method) ((  ImmutableSortedSet_1_t2348759144 * (*) (ImmutableSortedSet_1_t2348759144 *, Il2CppObject *, const MethodInfo*))ImmutableSortedSet_1_Remove_m3750122640_gshared)(__this, ___entry0, method)
// Firebase.Database.Internal.Collection.ImmutableSortedSet`1<T> Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>::Insert(T)
extern "C"  ImmutableSortedSet_1_t2348759144 * ImmutableSortedSet_1_Insert_m3179399611_gshared (ImmutableSortedSet_1_t2348759144 * __this, Il2CppObject * ___entry0, const MethodInfo* method);
#define ImmutableSortedSet_1_Insert_m3179399611(__this, ___entry0, method) ((  ImmutableSortedSet_1_t2348759144 * (*) (ImmutableSortedSet_1_t2348759144 *, Il2CppObject *, const MethodInfo*))ImmutableSortedSet_1_Insert_m3179399611_gshared)(__this, ___entry0, method)
// T Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>::GetMinEntry()
extern "C"  Il2CppObject * ImmutableSortedSet_1_GetMinEntry_m4219155093_gshared (ImmutableSortedSet_1_t2348759144 * __this, const MethodInfo* method);
#define ImmutableSortedSet_1_GetMinEntry_m4219155093(__this, method) ((  Il2CppObject * (*) (ImmutableSortedSet_1_t2348759144 *, const MethodInfo*))ImmutableSortedSet_1_GetMinEntry_m4219155093_gshared)(__this, method)
// T Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>::GetMaxEntry()
extern "C"  Il2CppObject * ImmutableSortedSet_1_GetMaxEntry_m2387621655_gshared (ImmutableSortedSet_1_t2348759144 * __this, const MethodInfo* method);
#define ImmutableSortedSet_1_GetMaxEntry_m2387621655(__this, method) ((  Il2CppObject * (*) (ImmutableSortedSet_1_t2348759144 *, const MethodInfo*))ImmutableSortedSet_1_GetMaxEntry_m2387621655_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>::ReverseIterator()
extern "C"  Il2CppObject* ImmutableSortedSet_1_ReverseIterator_m1480254674_gshared (ImmutableSortedSet_1_t2348759144 * __this, const MethodInfo* method);
#define ImmutableSortedSet_1_ReverseIterator_m1480254674(__this, method) ((  Il2CppObject* (*) (ImmutableSortedSet_1_t2348759144 *, const MethodInfo*))ImmutableSortedSet_1_ReverseIterator_m1480254674_gshared)(__this, method)
// T Firebase.Database.Internal.Collection.ImmutableSortedSet`1<System.Object>::GetPredecessorEntry(T)
extern "C"  Il2CppObject * ImmutableSortedSet_1_GetPredecessorEntry_m1208315696_gshared (ImmutableSortedSet_1_t2348759144 * __this, Il2CppObject * ___entry0, const MethodInfo* method);
#define ImmutableSortedSet_1_GetPredecessorEntry_m1208315696(__this, ___entry0, method) ((  Il2CppObject * (*) (ImmutableSortedSet_1_t2348759144 *, Il2CppObject *, const MethodInfo*))ImmutableSortedSet_1_GetPredecessorEntry_m1208315696_gshared)(__this, ___entry0, method)
