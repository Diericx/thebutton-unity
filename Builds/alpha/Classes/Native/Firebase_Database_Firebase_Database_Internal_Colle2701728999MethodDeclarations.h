﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle1529578535MethodDeclarations.h"

// Firebase.Database.Internal.Collection.ImmutableSortedMap`2<TK,TV> Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::EmptyMap(System.Collections.Generic.IComparer`1<TK>)
#define Builder_EmptyMap_m3858564147(__this /* static, unused */, ___comparator0, method) ((  ImmutableSortedMap_2_t94277708 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Builder_EmptyMap_m3930589567_gshared)(__this /* static, unused */, ___comparator0, method)
// Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder/IKeyTranslator<TK,TV> Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::IdentityTranslator()
#define Builder_IdentityTranslator_m1830302292(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Builder_IdentityTranslator_m224979022_gshared)(__this /* static, unused */, method)
// System.Void Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::.cctor()
#define Builder__cctor_m2225950228(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Builder__cctor_m1907675492_gshared)(__this /* static, unused */, method)
