﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder/KeyTranslator109<System.Object,System.Object>
struct KeyTranslator109_t2828396717;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder/KeyTranslator109<System.Object,System.Object>::.ctor()
extern "C"  void KeyTranslator109__ctor_m3806668953_gshared (KeyTranslator109_t2828396717 * __this, const MethodInfo* method);
#define KeyTranslator109__ctor_m3806668953(__this, method) ((  void (*) (KeyTranslator109_t2828396717 *, const MethodInfo*))KeyTranslator109__ctor_m3806668953_gshared)(__this, method)
// System.Object Firebase.Database.Internal.Collection.ImmutableSortedMap`2/Builder/KeyTranslator109<System.Object,System.Object>::Translate(System.Object)
extern "C"  Il2CppObject * KeyTranslator109_Translate_m4195723552_gshared (KeyTranslator109_t2828396717 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define KeyTranslator109_Translate_m4195723552(__this, ___key0, method) ((  Il2CppObject * (*) (KeyTranslator109_t2828396717 *, Il2CppObject *, const MethodInfo*))KeyTranslator109_Translate_m4195723552_gshared)(__this, ___key0, method)
