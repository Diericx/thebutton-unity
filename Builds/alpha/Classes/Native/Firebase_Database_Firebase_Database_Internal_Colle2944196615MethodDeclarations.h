﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Collection.LlrbNode`2/NodeVisitor<System.Object,System.Object>
struct NodeVisitor_t2944196615;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Collection.LlrbNode`2/NodeVisitor<System.Object,System.Object>::.ctor()
extern "C"  void NodeVisitor__ctor_m3743962795_gshared (NodeVisitor_t2944196615 * __this, const MethodInfo* method);
#define NodeVisitor__ctor_m3743962795(__this, method) ((  void (*) (NodeVisitor_t2944196615 *, const MethodInfo*))NodeVisitor__ctor_m3743962795_gshared)(__this, method)
