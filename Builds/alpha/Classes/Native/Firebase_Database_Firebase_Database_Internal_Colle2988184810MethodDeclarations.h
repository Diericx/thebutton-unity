﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12<System.Object,System.Object,System.Object,System.Object,System.Object>
struct Base12_t2988184810;
// System.Collections.Generic.IEnumerator`1<Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/BooleanChunk<System.Object,System.Object,System.Object,System.Object,System.Object>>
struct IEnumerator_1_t512382140;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(System.Int32)
extern "C"  void Base12__ctor_m440061024_gshared (Base12_t2988184810 * __this, int32_t ___size0, const MethodInfo* method);
#define Base12__ctor_m440061024(__this, ___size0, method) ((  void (*) (Base12_t2988184810 *, int32_t, const MethodInfo*))Base12__ctor_m440061024_gshared)(__this, ___size0, method)
// System.Collections.Generic.IEnumerator`1<Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/BooleanChunk<TK,TV,TA,TB,TC>> Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12<System.Object,System.Object,System.Object,System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* Base12_GetEnumerator_m3539400435_gshared (Base12_t2988184810 * __this, const MethodInfo* method);
#define Base12_GetEnumerator_m3539400435(__this, method) ((  Il2CppObject* (*) (Base12_t2988184810 *, const MethodInfo*))Base12_GetEnumerator_m3539400435_gshared)(__this, method)
// System.Collections.IEnumerator Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12<System.Object,System.Object,System.Object,System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Base12_System_Collections_IEnumerable_GetEnumerator_m158844324_gshared (Base12_t2988184810 * __this, const MethodInfo* method);
#define Base12_System_Collections_IEnumerable_GetEnumerator_m158844324(__this, method) ((  Il2CppObject * (*) (Base12_t2988184810 *, const MethodInfo*))Base12_System_Collections_IEnumerable_GetEnumerator_m158844324_gshared)(__this, method)
