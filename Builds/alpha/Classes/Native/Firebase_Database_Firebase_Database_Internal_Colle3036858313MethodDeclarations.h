﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/BooleanChunk<System.Object,System.Object,System.Object,System.Object,System.Object>
struct BooleanChunk_t3036858313;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/BooleanChunk<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void BooleanChunk__ctor_m1163001482_gshared (BooleanChunk_t3036858313 * __this, const MethodInfo* method);
#define BooleanChunk__ctor_m1163001482(__this, method) ((  void (*) (BooleanChunk_t3036858313 *, const MethodInfo*))BooleanChunk__ctor_m1163001482_gshared)(__this, method)
