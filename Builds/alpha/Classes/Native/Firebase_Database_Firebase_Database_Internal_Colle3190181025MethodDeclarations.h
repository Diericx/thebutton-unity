﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Collection.ImmutableSortedSet`1/WrappedEntryIterator<System.Object>
struct WrappedEntryIterator_t3190181025;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t1809345768;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Collection.ImmutableSortedSet`1/WrappedEntryIterator<System.Object>::.ctor(System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<T,System.Object>>)
extern "C"  void WrappedEntryIterator__ctor_m2946463310_gshared (WrappedEntryIterator_t3190181025 * __this, Il2CppObject* ___iterator0, const MethodInfo* method);
#define WrappedEntryIterator__ctor_m2946463310(__this, ___iterator0, method) ((  void (*) (WrappedEntryIterator_t3190181025 *, Il2CppObject*, const MethodInfo*))WrappedEntryIterator__ctor_m2946463310_gshared)(__this, ___iterator0, method)
// System.Boolean Firebase.Database.Internal.Collection.ImmutableSortedSet`1/WrappedEntryIterator<System.Object>::MoveNext()
extern "C"  bool WrappedEntryIterator_MoveNext_m1318140214_gshared (WrappedEntryIterator_t3190181025 * __this, const MethodInfo* method);
#define WrappedEntryIterator_MoveNext_m1318140214(__this, method) ((  bool (*) (WrappedEntryIterator_t3190181025 *, const MethodInfo*))WrappedEntryIterator_MoveNext_m1318140214_gshared)(__this, method)
// T Firebase.Database.Internal.Collection.ImmutableSortedSet`1/WrappedEntryIterator<System.Object>::get_Current()
extern "C"  Il2CppObject * WrappedEntryIterator_get_Current_m2584468257_gshared (WrappedEntryIterator_t3190181025 * __this, const MethodInfo* method);
#define WrappedEntryIterator_get_Current_m2584468257(__this, method) ((  Il2CppObject * (*) (WrappedEntryIterator_t3190181025 *, const MethodInfo*))WrappedEntryIterator_get_Current_m2584468257_gshared)(__this, method)
// System.Object Firebase.Database.Internal.Collection.ImmutableSortedSet`1/WrappedEntryIterator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * WrappedEntryIterator_System_Collections_IEnumerator_get_Current_m3843867690_gshared (WrappedEntryIterator_t3190181025 * __this, const MethodInfo* method);
#define WrappedEntryIterator_System_Collections_IEnumerator_get_Current_m3843867690(__this, method) ((  Il2CppObject * (*) (WrappedEntryIterator_t3190181025 *, const MethodInfo*))WrappedEntryIterator_System_Collections_IEnumerator_get_Current_m3843867690_gshared)(__this, method)
// System.Void Firebase.Database.Internal.Collection.ImmutableSortedSet`1/WrappedEntryIterator<System.Object>::Dispose()
extern "C"  void WrappedEntryIterator_Dispose_m1881631145_gshared (WrappedEntryIterator_t3190181025 * __this, const MethodInfo* method);
#define WrappedEntryIterator_Dispose_m1881631145(__this, method) ((  void (*) (WrappedEntryIterator_t3190181025 *, const MethodInfo*))WrappedEntryIterator_Dispose_m1881631145_gshared)(__this, method)
// System.Void Firebase.Database.Internal.Collection.ImmutableSortedSet`1/WrappedEntryIterator<System.Object>::Reset()
extern "C"  void WrappedEntryIterator_Reset_m2058005523_gshared (WrappedEntryIterator_t3190181025 * __this, const MethodInfo* method);
#define WrappedEntryIterator_Reset_m2058005523(__this, method) ((  void (*) (WrappedEntryIterator_t3190181025 *, const MethodInfo*))WrappedEntryIterator_Reset_m2058005523_gshared)(__this, method)
