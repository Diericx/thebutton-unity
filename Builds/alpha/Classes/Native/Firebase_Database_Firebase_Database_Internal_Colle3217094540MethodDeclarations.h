﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>
struct ImmutableSortedMap_2_t3217094540;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::.ctor()
extern "C"  void ImmutableSortedMap_2__ctor_m2147415669_gshared (ImmutableSortedMap_2_t3217094540 * __this, const MethodInfo* method);
#define ImmutableSortedMap_2__ctor_m2147415669(__this, method) ((  void (*) (ImmutableSortedMap_2_t3217094540 *, const MethodInfo*))ImmutableSortedMap_2__ctor_m2147415669_gshared)(__this, method)
// System.Collections.IEnumerator Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ImmutableSortedMap_2_System_Collections_IEnumerable_GetEnumerator_m1935600564_gshared (ImmutableSortedMap_2_t3217094540 * __this, const MethodInfo* method);
#define ImmutableSortedMap_2_System_Collections_IEnumerable_GetEnumerator_m1935600564(__this, method) ((  Il2CppObject * (*) (ImmutableSortedMap_2_t3217094540 *, const MethodInfo*))ImmutableSortedMap_2_System_Collections_IEnumerable_GetEnumerator_m1935600564_gshared)(__this, method)
// System.Boolean Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool ImmutableSortedMap_2_Equals_m525750466_gshared (ImmutableSortedMap_2_t3217094540 * __this, Il2CppObject * ___o0, const MethodInfo* method);
#define ImmutableSortedMap_2_Equals_m525750466(__this, ___o0, method) ((  bool (*) (ImmutableSortedMap_2_t3217094540 *, Il2CppObject *, const MethodInfo*))ImmutableSortedMap_2_Equals_m525750466_gshared)(__this, ___o0, method)
// System.Int32 Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t ImmutableSortedMap_2_GetHashCode_m567452986_gshared (ImmutableSortedMap_2_t3217094540 * __this, const MethodInfo* method);
#define ImmutableSortedMap_2_GetHashCode_m567452986(__this, method) ((  int32_t (*) (ImmutableSortedMap_2_t3217094540 *, const MethodInfo*))ImmutableSortedMap_2_GetHashCode_m567452986_gshared)(__this, method)
// System.String Firebase.Database.Internal.Collection.ImmutableSortedMap`2<System.Object,System.Object>::ToString()
extern "C"  String_t* ImmutableSortedMap_2_ToString_m1520722834_gshared (ImmutableSortedMap_2_t3217094540 * __this, const MethodInfo* method);
#define ImmutableSortedMap_2_ToString_m1520722834(__this, method) ((  String_t* (*) (ImmutableSortedMap_2_t3217094540 *, const MethodInfo*))ImmutableSortedMap_2_ToString_m1520722834_gshared)(__this, method)
