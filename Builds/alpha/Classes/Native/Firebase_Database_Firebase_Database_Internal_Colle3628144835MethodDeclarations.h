﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Collection.LlrbRedValueNode`2<System.Object,System.Object>
struct LlrbRedValueNode_2_t3628144835;
// System.Object
struct Il2CppObject;
// Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>
struct LlrbNode_2_t4262869811;
// Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>
struct LlrbValueNode_2_t1296857666;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle1714721308.h"

// System.Void Firebase.Database.Internal.Collection.LlrbRedValueNode`2<System.Object,System.Object>::.ctor(TK,TV)
extern "C"  void LlrbRedValueNode_2__ctor_m2387469861_gshared (LlrbRedValueNode_2_t3628144835 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define LlrbRedValueNode_2__ctor_m2387469861(__this, ___key0, ___value1, method) ((  void (*) (LlrbRedValueNode_2_t3628144835 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))LlrbRedValueNode_2__ctor_m2387469861_gshared)(__this, ___key0, ___value1, method)
// System.Void Firebase.Database.Internal.Collection.LlrbRedValueNode`2<System.Object,System.Object>::.ctor(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>)
extern "C"  void LlrbRedValueNode_2__ctor_m1169553381_gshared (LlrbRedValueNode_2_t3628144835 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, LlrbNode_2_t4262869811 * ___left2, LlrbNode_2_t4262869811 * ___right3, const MethodInfo* method);
#define LlrbRedValueNode_2__ctor_m1169553381(__this, ___key0, ___value1, ___left2, ___right3, method) ((  void (*) (LlrbRedValueNode_2_t3628144835 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 *, const MethodInfo*))LlrbRedValueNode_2__ctor_m1169553381_gshared)(__this, ___key0, ___value1, ___left2, ___right3, method)
// Firebase.Database.Internal.Collection.LlrbNode`2/Color<TK,TV> Firebase.Database.Internal.Collection.LlrbRedValueNode`2<System.Object,System.Object>::GetColor()
extern "C"  int32_t LlrbRedValueNode_2_GetColor_m2296677748_gshared (LlrbRedValueNode_2_t3628144835 * __this, const MethodInfo* method);
#define LlrbRedValueNode_2_GetColor_m2296677748(__this, method) ((  int32_t (*) (LlrbRedValueNode_2_t3628144835 *, const MethodInfo*))LlrbRedValueNode_2_GetColor_m2296677748_gshared)(__this, method)
// System.Boolean Firebase.Database.Internal.Collection.LlrbRedValueNode`2<System.Object,System.Object>::IsRed()
extern "C"  bool LlrbRedValueNode_2_IsRed_m4110529481_gshared (LlrbRedValueNode_2_t3628144835 * __this, const MethodInfo* method);
#define LlrbRedValueNode_2_IsRed_m4110529481(__this, method) ((  bool (*) (LlrbRedValueNode_2_t3628144835 *, const MethodInfo*))LlrbRedValueNode_2_IsRed_m4110529481_gshared)(__this, method)
// Firebase.Database.Internal.Collection.LlrbValueNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbRedValueNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>)
extern "C"  LlrbValueNode_2_t1296857666 * LlrbRedValueNode_2_Copy_m554805106_gshared (LlrbRedValueNode_2_t3628144835 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, LlrbNode_2_t4262869811 * ___left2, LlrbNode_2_t4262869811 * ___right3, const MethodInfo* method);
#define LlrbRedValueNode_2_Copy_m554805106(__this, ___key0, ___value1, ___left2, ___right3, method) ((  LlrbValueNode_2_t1296857666 * (*) (LlrbRedValueNode_2_t3628144835 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 *, const MethodInfo*))LlrbRedValueNode_2_Copy_m554805106_gshared)(__this, ___key0, ___value1, ___left2, ___right3, method)
