﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12/Enumerator216<System.Object,System.Object,System.Object,System.Object,System.Object>
struct Enumerator216_t3810636662;
// Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12<System.Object,System.Object,System.Object,System.Object,System.Object>
struct Base12_t2988184810;
// Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/BooleanChunk<System.Object,System.Object,System.Object,System.Object,System.Object>
struct BooleanChunk_t3036858313;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12/Enumerator216<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12<TK,TV,TA,TB,TC>)
extern "C"  void Enumerator216__ctor_m3035470894_gshared (Enumerator216_t3810636662 * __this, Base12_t2988184810 * ___enclosing0, const MethodInfo* method);
#define Enumerator216__ctor_m3035470894(__this, ___enclosing0, method) ((  void (*) (Enumerator216_t3810636662 *, Base12_t2988184810 *, const MethodInfo*))Enumerator216__ctor_m3035470894_gshared)(__this, ___enclosing0, method)
// System.Boolean Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12/Enumerator216<System.Object,System.Object,System.Object,System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator216_MoveNext_m1132866179_gshared (Enumerator216_t3810636662 * __this, const MethodInfo* method);
#define Enumerator216_MoveNext_m1132866179(__this, method) ((  bool (*) (Enumerator216_t3810636662 *, const MethodInfo*))Enumerator216_MoveNext_m1132866179_gshared)(__this, method)
// Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/BooleanChunk<TK,TV,TA,TB,TC> Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12/Enumerator216<System.Object,System.Object,System.Object,System.Object,System.Object>::System.Collections.Generic.IEnumerator<Firebase.Database.Internal.Collection.RbTreeSortedMap<TK,TV>.BuilderAbc<TA,TB,TC>.BooleanChunk>.get_Current()
extern "C"  BooleanChunk_t3036858313 * Enumerator216_System_Collections_Generic_IEnumeratorU3CFirebase_Database_Internal_Collection_RbTreeSortedMapU3CTKU2CTVU3E_BuilderAbcU3CTAU2CTBU2CTCU3E_BooleanChunkU3E_get_Current_m4007318139_gshared (Enumerator216_t3810636662 * __this, const MethodInfo* method);
#define Enumerator216_System_Collections_Generic_IEnumeratorU3CFirebase_Database_Internal_Collection_RbTreeSortedMapU3CTKU2CTVU3E_BuilderAbcU3CTAU2CTBU2CTCU3E_BooleanChunkU3E_get_Current_m4007318139(__this, method) ((  BooleanChunk_t3036858313 * (*) (Enumerator216_t3810636662 *, const MethodInfo*))Enumerator216_System_Collections_Generic_IEnumeratorU3CFirebase_Database_Internal_Collection_RbTreeSortedMapU3CTKU2CTVU3E_BuilderAbcU3CTAU2CTBU2CTCU3E_BooleanChunkU3E_get_Current_m4007318139_gshared)(__this, method)
// System.Object Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12/Enumerator216<System.Object,System.Object,System.Object,System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator216_System_Collections_IEnumerator_get_Current_m1327421699_gshared (Enumerator216_t3810636662 * __this, const MethodInfo* method);
#define Enumerator216_System_Collections_IEnumerator_get_Current_m1327421699(__this, method) ((  Il2CppObject * (*) (Enumerator216_t3810636662 *, const MethodInfo*))Enumerator216_System_Collections_IEnumerator_get_Current_m1327421699_gshared)(__this, method)
// System.Void Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12/Enumerator216<System.Object,System.Object,System.Object,System.Object,System.Object>::Dispose()
extern "C"  void Enumerator216_Dispose_m3119943996_gshared (Enumerator216_t3810636662 * __this, const MethodInfo* method);
#define Enumerator216_Dispose_m3119943996(__this, method) ((  void (*) (Enumerator216_t3810636662 *, const MethodInfo*))Enumerator216_Dispose_m3119943996_gshared)(__this, method)
// System.Void Firebase.Database.Internal.Collection.RbTreeSortedMap`2/BuilderAbc`3/Base12/Enumerator216<System.Object,System.Object,System.Object,System.Object,System.Object>::Reset()
extern "C"  void Enumerator216_Reset_m679673586_gshared (Enumerator216_t3810636662 * __this, const MethodInfo* method);
#define Enumerator216_Reset_m679673586(__this, method) ((  void (*) (Enumerator216_t3810636662 *, const MethodInfo*))Enumerator216_Reset_m679673586_gshared)(__this, method)
