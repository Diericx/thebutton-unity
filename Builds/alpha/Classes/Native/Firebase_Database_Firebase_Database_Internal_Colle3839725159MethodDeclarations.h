﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Collection.LlrbBlackValueNode`2<System.Object,System.Object>
struct LlrbBlackValueNode_2_t3839725159;
// System.Object
struct Il2CppObject;
// Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>
struct LlrbNode_2_t4262869811;
// Firebase.Database.Internal.Collection.LlrbValueNode`2<System.Object,System.Object>
struct LlrbValueNode_2_t1296857666;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle1714721308.h"

// System.Void Firebase.Database.Internal.Collection.LlrbBlackValueNode`2<System.Object,System.Object>::.ctor(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>)
extern "C"  void LlrbBlackValueNode_2__ctor_m2300250781_gshared (LlrbBlackValueNode_2_t3839725159 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, LlrbNode_2_t4262869811 * ___left2, LlrbNode_2_t4262869811 * ___right3, const MethodInfo* method);
#define LlrbBlackValueNode_2__ctor_m2300250781(__this, ___key0, ___value1, ___left2, ___right3, method) ((  void (*) (LlrbBlackValueNode_2_t3839725159 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 *, const MethodInfo*))LlrbBlackValueNode_2__ctor_m2300250781_gshared)(__this, ___key0, ___value1, ___left2, ___right3, method)
// Firebase.Database.Internal.Collection.LlrbNode`2/Color<TK,TV> Firebase.Database.Internal.Collection.LlrbBlackValueNode`2<System.Object,System.Object>::GetColor()
extern "C"  int32_t LlrbBlackValueNode_2_GetColor_m422302376_gshared (LlrbBlackValueNode_2_t3839725159 * __this, const MethodInfo* method);
#define LlrbBlackValueNode_2_GetColor_m422302376(__this, method) ((  int32_t (*) (LlrbBlackValueNode_2_t3839725159 *, const MethodInfo*))LlrbBlackValueNode_2_GetColor_m422302376_gshared)(__this, method)
// System.Boolean Firebase.Database.Internal.Collection.LlrbBlackValueNode`2<System.Object,System.Object>::IsRed()
extern "C"  bool LlrbBlackValueNode_2_IsRed_m1597543809_gshared (LlrbBlackValueNode_2_t3839725159 * __this, const MethodInfo* method);
#define LlrbBlackValueNode_2_IsRed_m1597543809(__this, method) ((  bool (*) (LlrbBlackValueNode_2_t3839725159 *, const MethodInfo*))LlrbBlackValueNode_2_IsRed_m1597543809_gshared)(__this, method)
// Firebase.Database.Internal.Collection.LlrbValueNode`2<TK,TV> Firebase.Database.Internal.Collection.LlrbBlackValueNode`2<System.Object,System.Object>::Copy(TK,TV,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>,Firebase.Database.Internal.Collection.LlrbNode`2<TK,TV>)
extern "C"  LlrbValueNode_2_t1296857666 * LlrbBlackValueNode_2_Copy_m4069924962_gshared (LlrbBlackValueNode_2_t3839725159 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, LlrbNode_2_t4262869811 * ___left2, LlrbNode_2_t4262869811 * ___right3, const MethodInfo* method);
#define LlrbBlackValueNode_2_Copy_m4069924962(__this, ___key0, ___value1, ___left2, ___right3, method) ((  LlrbValueNode_2_t1296857666 * (*) (LlrbBlackValueNode_2_t3839725159 *, Il2CppObject *, Il2CppObject *, LlrbNode_2_t4262869811 *, LlrbNode_2_t4262869811 *, const MethodInfo*))LlrbBlackValueNode_2_Copy_m4069924962_gshared)(__this, ___key0, ___value1, ___left2, ___right3, method)
