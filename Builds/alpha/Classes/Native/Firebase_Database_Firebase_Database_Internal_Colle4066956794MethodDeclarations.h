﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle2944196615MethodDeclarations.h"

// System.Void Firebase.Database.Internal.Collection.LlrbNode`2/NodeVisitor<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Snapshot.Node>::.ctor()
#define NodeVisitor__ctor_m3533439296(__this, method) ((  void (*) (NodeVisitor_t4066956794 *, const MethodInfo*))NodeVisitor__ctor_m3743962795_gshared)(__this, method)
