﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>
struct LlrbNode_2_t4262869811;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Collection.LlrbNode`2<System.Object,System.Object>::.ctor()
extern "C"  void LlrbNode_2__ctor_m2809819688_gshared (LlrbNode_2_t4262869811 * __this, const MethodInfo* method);
#define LlrbNode_2__ctor_m2809819688(__this, method) ((  void (*) (LlrbNode_2_t4262869811 *, const MethodInfo*))LlrbNode_2__ctor_m2809819688_gshared)(__this, method)
