﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle2348759144MethodDeclarations.h"

// System.Void Firebase.Database.Internal.Collection.ImmutableSortedSet`1<Firebase.Database.Internal.Snapshot.NamedNode>::.ctor(System.Collections.Generic.IList`1<T>,System.Collections.Generic.IComparer`1<T>)
#define ImmutableSortedSet_1__ctor_m1982522199(__this, ___elems0, ___comparator1, method) ((  void (*) (ImmutableSortedSet_1_t447195634 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))ImmutableSortedSet_1__ctor_m1945539485_gshared)(__this, ___elems0, ___comparator1, method)
// System.Void Firebase.Database.Internal.Collection.ImmutableSortedSet`1<Firebase.Database.Internal.Snapshot.NamedNode>::.ctor(Firebase.Database.Internal.Collection.ImmutableSortedMap`2<T,System.Object>)
#define ImmutableSortedSet_1__ctor_m210976960(__this, ___map0, method) ((  void (*) (ImmutableSortedSet_1_t447195634 *, ImmutableSortedMap_2_t1517776026 *, const MethodInfo*))ImmutableSortedSet_1__ctor_m1960942718_gshared)(__this, ___map0, method)
// System.Collections.Generic.IEnumerator`1<T> Firebase.Database.Internal.Collection.ImmutableSortedSet`1<Firebase.Database.Internal.Snapshot.NamedNode>::GetEnumerator()
#define ImmutableSortedSet_1_GetEnumerator_m3684371306(__this, method) ((  Il2CppObject* (*) (ImmutableSortedSet_1_t447195634 *, const MethodInfo*))ImmutableSortedSet_1_GetEnumerator_m3228214528_gshared)(__this, method)
// System.Collections.IEnumerator Firebase.Database.Internal.Collection.ImmutableSortedSet`1<Firebase.Database.Internal.Snapshot.NamedNode>::System.Collections.IEnumerable.GetEnumerator()
#define ImmutableSortedSet_1_System_Collections_IEnumerable_GetEnumerator_m2313056373(__this, method) ((  Il2CppObject * (*) (ImmutableSortedSet_1_t447195634 *, const MethodInfo*))ImmutableSortedSet_1_System_Collections_IEnumerable_GetEnumerator_m16219723_gshared)(__this, method)
// Firebase.Database.Internal.Collection.ImmutableSortedSet`1<T> Firebase.Database.Internal.Collection.ImmutableSortedSet`1<Firebase.Database.Internal.Snapshot.NamedNode>::Remove(T)
#define ImmutableSortedSet_1_Remove_m2167717238(__this, ___entry0, method) ((  ImmutableSortedSet_1_t447195634 * (*) (ImmutableSortedSet_1_t447195634 *, NamedNode_t787885785 *, const MethodInfo*))ImmutableSortedSet_1_Remove_m3750122640_gshared)(__this, ___entry0, method)
// Firebase.Database.Internal.Collection.ImmutableSortedSet`1<T> Firebase.Database.Internal.Collection.ImmutableSortedSet`1<Firebase.Database.Internal.Snapshot.NamedNode>::Insert(T)
#define ImmutableSortedSet_1_Insert_m3076137505(__this, ___entry0, method) ((  ImmutableSortedSet_1_t447195634 * (*) (ImmutableSortedSet_1_t447195634 *, NamedNode_t787885785 *, const MethodInfo*))ImmutableSortedSet_1_Insert_m3179399611_gshared)(__this, ___entry0, method)
// T Firebase.Database.Internal.Collection.ImmutableSortedSet`1<Firebase.Database.Internal.Snapshot.NamedNode>::GetMinEntry()
#define ImmutableSortedSet_1_GetMinEntry_m2542502359(__this, method) ((  NamedNode_t787885785 * (*) (ImmutableSortedSet_1_t447195634 *, const MethodInfo*))ImmutableSortedSet_1_GetMinEntry_m4219155093_gshared)(__this, method)
// T Firebase.Database.Internal.Collection.ImmutableSortedSet`1<Firebase.Database.Internal.Snapshot.NamedNode>::GetMaxEntry()
#define ImmutableSortedSet_1_GetMaxEntry_m80495157(__this, method) ((  NamedNode_t787885785 * (*) (ImmutableSortedSet_1_t447195634 *, const MethodInfo*))ImmutableSortedSet_1_GetMaxEntry_m2387621655_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> Firebase.Database.Internal.Collection.ImmutableSortedSet`1<Firebase.Database.Internal.Snapshot.NamedNode>::ReverseIterator()
#define ImmutableSortedSet_1_ReverseIterator_m2895997528(__this, method) ((  Il2CppObject* (*) (ImmutableSortedSet_1_t447195634 *, const MethodInfo*))ImmutableSortedSet_1_ReverseIterator_m1480254674_gshared)(__this, method)
// T Firebase.Database.Internal.Collection.ImmutableSortedSet`1<Firebase.Database.Internal.Snapshot.NamedNode>::GetPredecessorEntry(T)
#define ImmutableSortedSet_1_GetPredecessorEntry_m2381162926(__this, ___entry0, method) ((  NamedNode_t787885785 * (*) (ImmutableSortedSet_1_t447195634 *, NamedNode_t787885785 *, const MethodInfo*))ImmutableSortedSet_1_GetPredecessorEntry_m1208315696_gshared)(__this, ___entry0, method)
