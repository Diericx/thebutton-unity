﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle2182977910MethodDeclarations.h"

// System.Void Firebase.Database.Internal.Collection.StandardComparator`1<Firebase.Database.Internal.Snapshot.ChildKey>::.ctor()
#define StandardComparator_1__ctor_m3139831772(__this, method) ((  void (*) (StandardComparator_1_t691330998 *, const MethodInfo*))StandardComparator_1__ctor_m3122475602_gshared)(__this, method)
// System.Int32 Firebase.Database.Internal.Collection.StandardComparator`1<Firebase.Database.Internal.Snapshot.ChildKey>::Compare(TA,TA)
#define StandardComparator_1_Compare_m2382457873(__this, ___x0, ___y1, method) ((  int32_t (*) (StandardComparator_1_t691330998 *, ChildKey_t1197802383 *, ChildKey_t1197802383 *, const MethodInfo*))StandardComparator_1_Compare_m517984725_gshared)(__this, ___x0, ___y1, method)
// Firebase.Database.Internal.Collection.StandardComparator`1<TA> Firebase.Database.Internal.Collection.StandardComparator`1<Firebase.Database.Internal.Snapshot.ChildKey>::GetComparator()
#define StandardComparator_1_GetComparator_m2621741865(__this /* static, unused */, method) ((  StandardComparator_1_t691330998 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))StandardComparator_1_GetComparator_m3420190789_gshared)(__this /* static, unused */, method)
// System.Void Firebase.Database.Internal.Collection.StandardComparator`1<Firebase.Database.Internal.Snapshot.ChildKey>::.cctor()
#define StandardComparator_1__cctor_m994556287(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))StandardComparator_1__cctor_m3768252471_gshared)(__this /* static, unused */, method)
