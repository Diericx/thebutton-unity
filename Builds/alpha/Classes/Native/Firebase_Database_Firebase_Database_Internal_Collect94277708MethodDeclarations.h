﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Colle3217094540MethodDeclarations.h"

// System.Void Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::.ctor()
#define ImmutableSortedMap_2__ctor_m3484987717(__this, method) ((  void (*) (ImmutableSortedMap_2_t94277708 *, const MethodInfo*))ImmutableSortedMap_2__ctor_m2147415669_gshared)(__this, method)
// System.Collections.IEnumerator Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::System.Collections.IEnumerable.GetEnumerator()
#define ImmutableSortedMap_2_System_Collections_IEnumerable_GetEnumerator_m206197652(__this, method) ((  Il2CppObject * (*) (ImmutableSortedMap_2_t94277708 *, const MethodInfo*))ImmutableSortedMap_2_System_Collections_IEnumerable_GetEnumerator_m1935600564_gshared)(__this, method)
// System.Boolean Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Equals(System.Object)
#define ImmutableSortedMap_2_Equals_m2749657646(__this, ___o0, method) ((  bool (*) (ImmutableSortedMap_2_t94277708 *, Il2CppObject *, const MethodInfo*))ImmutableSortedMap_2_Equals_m525750466_gshared)(__this, ___o0, method)
// System.Int32 Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::GetHashCode()
#define ImmutableSortedMap_2_GetHashCode_m1358866466(__this, method) ((  int32_t (*) (ImmutableSortedMap_2_t94277708 *, const MethodInfo*))ImmutableSortedMap_2_GetHashCode_m567452986_gshared)(__this, method)
// System.String Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::ToString()
#define ImmutableSortedMap_2_ToString_m3651263904(__this, method) ((  String_t* (*) (ImmutableSortedMap_2_t94277708 *, const MethodInfo*))ImmutableSortedMap_2_ToString_m1520722834_gshared)(__this, method)
