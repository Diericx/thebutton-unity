﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Connection.Util.StringListReader
struct StringListReader_t1703758004;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.Database.Internal.Connection.Util.StringListReader::.ctor()
extern "C"  void StringListReader__ctor_m3107172584 (StringListReader_t1703758004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Connection.Util.StringListReader::AddString(System.String)
extern "C"  void StringListReader_AddString_m3661328932 (StringListReader_t1703758004 * __this, String_t* ___string0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Connection.Util.StringListReader::Freeze()
extern "C"  void StringListReader_Freeze_m3520175511 (StringListReader_t1703758004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Database.Internal.Connection.Util.StringListReader::ToString()
extern "C"  String_t* StringListReader_ToString_m1312861979 (StringListReader_t1703758004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
