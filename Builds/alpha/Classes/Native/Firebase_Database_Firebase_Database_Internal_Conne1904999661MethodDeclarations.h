﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Connection.PersistentConnection
struct PersistentConnection_t1904999661;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Connection.PersistentConnection::.ctor()
extern "C"  void PersistentConnection__ctor_m314149483 (PersistentConnection_t1904999661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
