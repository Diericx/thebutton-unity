﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Connection.WebsocketConnection/WsClientTubesock/Runnable49
struct Runnable49_t1917770995;
// Firebase.Database.Internal.Connection.WebsocketConnection/WsClientTubesock
struct WsClientTubesock_t1111414532;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1111414532.h"

// System.Void Firebase.Database.Internal.Connection.WebsocketConnection/WsClientTubesock/Runnable49::.ctor(Firebase.Database.Internal.Connection.WebsocketConnection/WsClientTubesock)
extern "C"  void Runnable49__ctor_m3382587070 (Runnable49_t1917770995 * __this, WsClientTubesock_t1111414532 * ___enclosing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Connection.WebsocketConnection/WsClientTubesock/Runnable49::Run()
extern "C"  void Runnable49_Run_m3074234837 (Runnable49_t1917770995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
