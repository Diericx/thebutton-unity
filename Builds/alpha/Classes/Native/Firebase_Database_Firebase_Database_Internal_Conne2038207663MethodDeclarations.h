﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Connection.Util.RetryHelper/Runnable53
struct Runnable53_t2038207663;
// Firebase.Database.Internal.Connection.Util.RetryHelper
struct RetryHelper_t2341283196;
// Google.Sharpen.Runnable
struct Runnable_t1446984663;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne2341283196.h"

// System.Void Firebase.Database.Internal.Connection.Util.RetryHelper/Runnable53::.ctor(Firebase.Database.Internal.Connection.Util.RetryHelper,Google.Sharpen.Runnable)
extern "C"  void Runnable53__ctor_m455912054 (Runnable53_t2038207663 * __this, RetryHelper_t2341283196 * ___enclosing0, Il2CppObject * ___runnable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Connection.Util.RetryHelper/Runnable53::Run()
extern "C"  void Runnable53_Run_m3147406169 (Runnable53_t2038207663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
