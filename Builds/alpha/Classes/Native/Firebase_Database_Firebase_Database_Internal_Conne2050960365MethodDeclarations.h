﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec
struct ListenQuerySpec_t2050960365;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_t2570160834;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t2603311978;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec::.ctor(System.Collections.Generic.IList`1<System.String>,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern "C"  void ListenQuerySpec__ctor_m3825376161 (ListenQuerySpec_t2050960365 * __this, Il2CppObject* ___path0, Il2CppObject* ___queryParams1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec::Equals(System.Object)
extern "C"  bool ListenQuerySpec_Equals_m2790645535 (ListenQuerySpec_t2050960365 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec::GetHashCode()
extern "C"  int32_t ListenQuerySpec_GetHashCode_m825482621 (ListenQuerySpec_t2050960365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec::ToString()
extern "C"  String_t* ListenQuerySpec_ToString_m149019009 (ListenQuerySpec_t2050960365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
