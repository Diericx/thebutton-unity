﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Connection.PersistentConnectionImpl/ConnectionRequestCallback629
struct ConnectionRequestCallback629_t2407603808;
// Firebase.Database.Internal.Connection.IRequestResultCallback
struct IRequestResultCallback_t1513452486;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t2603311978;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Connection.PersistentConnectionImpl/ConnectionRequestCallback629::.ctor(Firebase.Database.Internal.Connection.IRequestResultCallback)
extern "C"  void ConnectionRequestCallback629__ctor_m949727436 (ConnectionRequestCallback629_t2407603808 * __this, Il2CppObject * ___onComplete0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Connection.PersistentConnectionImpl/ConnectionRequestCallback629::OnResponse(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern "C"  void ConnectionRequestCallback629_OnResponse_m3573215451 (ConnectionRequestCallback629_t2407603808 * __this, Il2CppObject* ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
