﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Connection.PersistentConnectionImpl/Runnable1097
struct Runnable1097_t2533788837;
// Firebase.Database.Internal.Connection.PersistentConnectionImpl
struct PersistentConnectionImpl_t1099507887;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1099507887.h"

// System.Void Firebase.Database.Internal.Connection.PersistentConnectionImpl/Runnable1097::.ctor(Firebase.Database.Internal.Connection.PersistentConnectionImpl)
extern "C"  void Runnable1097__ctor_m764292048 (Runnable1097_t2533788837 * __this, PersistentConnectionImpl_t1099507887 * ___enclosing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Connection.PersistentConnectionImpl/Runnable1097::Run()
extern "C"  void Runnable1097_Run_m141780231 (Runnable1097_t2533788837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
