﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingDisconnect
struct OutstandingDisconnect_t3017763353;
// System.String
struct String_t;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_t2570160834;
// System.Object
struct Il2CppObject;
// Firebase.Database.Internal.Connection.IRequestResultCallback
struct IRequestResultCallback_t1513452486;

#include "codegen/il2cpp-codegen.h"

// System.String Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingDisconnect::GetAction()
extern "C"  String_t* OutstandingDisconnect_GetAction_m3631694627 (OutstandingDisconnect_t3017763353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<System.String> Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingDisconnect::GetPath()
extern "C"  Il2CppObject* OutstandingDisconnect_GetPath_m2519423331 (OutstandingDisconnect_t3017763353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingDisconnect::GetData()
extern "C"  Il2CppObject * OutstandingDisconnect_GetData_m3294737289 (OutstandingDisconnect_t3017763353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Database.Internal.Connection.IRequestResultCallback Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingDisconnect::GetOnComplete()
extern "C"  Il2CppObject * OutstandingDisconnect_GetOnComplete_m1446250026 (OutstandingDisconnect_t3017763353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
