﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t2603311978;

#include "codegen/il2cpp-codegen.h"

// System.Boolean Firebase.Database.Internal.Connection.Util.DictionaryHelper::IsEqivalentTo(System.Collections.Generic.IDictionary`2<System.String,System.Object>,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern "C"  bool DictionaryHelper_IsEqivalentTo_m3921497542 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___d10, Il2CppObject* ___d21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
