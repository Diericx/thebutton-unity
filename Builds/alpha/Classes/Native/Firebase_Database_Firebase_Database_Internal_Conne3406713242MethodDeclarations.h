﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Connection.ConnectionAuthTokenProvider
struct ConnectionAuthTokenProvider_t3406713242;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Connection.ConnectionAuthTokenProvider::.ctor()
extern "C"  void ConnectionAuthTokenProvider__ctor_m1234060630 (ConnectionAuthTokenProvider_t3406713242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
