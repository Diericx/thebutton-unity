﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Connection.PersistentConnectionImpl/Runnable557
struct Runnable557_t3816656261;
// Firebase.Database.Internal.Connection.PersistentConnectionImpl
struct PersistentConnectionImpl_t1099507887;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1099507887.h"

// System.Void Firebase.Database.Internal.Connection.PersistentConnectionImpl/Runnable557::.ctor(Firebase.Database.Internal.Connection.PersistentConnectionImpl,System.Boolean)
extern "C"  void Runnable557__ctor_m2209972715 (Runnable557_t3816656261 * __this, PersistentConnectionImpl_t1099507887 * ___enclosing0, bool ___forceRefresh1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Connection.PersistentConnectionImpl/Runnable557::Run()
extern "C"  void Runnable557_Run_m1547413399 (Runnable557_t3816656261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
