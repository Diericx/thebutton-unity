﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Connection.WebsocketConnection/WsClientTubesock/Runnable86
struct Runnable86_t3887139478;
// Firebase.Database.Internal.Connection.WebsocketConnection/WsClientTubesock
struct WsClientTubesock_t1111414532;
// Firebase.Database.Internal.TubeSock.WebSocketException
struct WebSocketException_t780773228;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1111414532.h"
#include "Firebase_Database_Firebase_Database_Internal_TubeSo780773228.h"

// System.Void Firebase.Database.Internal.Connection.WebsocketConnection/WsClientTubesock/Runnable86::.ctor(Firebase.Database.Internal.Connection.WebsocketConnection/WsClientTubesock,Firebase.Database.Internal.TubeSock.WebSocketException)
extern "C"  void Runnable86__ctor_m3874306662 (Runnable86_t3887139478 * __this, WsClientTubesock_t1111414532 * ___enclosing0, WebSocketException_t780773228 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Connection.WebsocketConnection/WsClientTubesock/Runnable86::Run()
extern "C"  void Runnable86_Run_m1216596032 (Runnable86_t3887139478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
