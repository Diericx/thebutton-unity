﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Connection.PersistentConnectionImpl/Runnable557/GetTokenCallback567
struct GetTokenCallback567_t164694114;
// Firebase.Database.Internal.Connection.PersistentConnectionImpl/Runnable557
struct Runnable557_t3816656261;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne3816656261.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.Database.Internal.Connection.PersistentConnectionImpl/Runnable557/GetTokenCallback567::.ctor(Firebase.Database.Internal.Connection.PersistentConnectionImpl/Runnable557,System.Int64)
extern "C"  void GetTokenCallback567__ctor_m1333632698 (GetTokenCallback567_t164694114 * __this, Runnable557_t3816656261 * ___enclosing0, int64_t ___thisGetTokenAttempt1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Connection.PersistentConnectionImpl/Runnable557/GetTokenCallback567::OnSuccess(System.String)
extern "C"  void GetTokenCallback567_OnSuccess_m3959139137 (GetTokenCallback567_t164694114 * __this, String_t* ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
