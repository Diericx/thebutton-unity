﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Connection.WebsocketConnection/Runnable290
struct Runnable290_t265678593;
// Firebase.Database.Internal.Connection.WebsocketConnection
struct WebsocketConnection_t1372375221;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1372375221.h"

// System.Void Firebase.Database.Internal.Connection.WebsocketConnection/Runnable290::.ctor(Firebase.Database.Internal.Connection.WebsocketConnection)
extern "C"  void Runnable290__ctor_m3904218712 (Runnable290_t265678593 * __this, WebsocketConnection_t1372375221 * ___enclosing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Connection.WebsocketConnection/Runnable290::Run()
extern "C"  void Runnable290_Run_m1876191011 (Runnable290_t265678593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
