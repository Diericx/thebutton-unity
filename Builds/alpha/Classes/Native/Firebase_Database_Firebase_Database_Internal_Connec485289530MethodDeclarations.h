﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Connection.PersistentConnectionImpl/ConnectionRequestCallback797
struct ConnectionRequestCallback797_t485289530;
// Firebase.Database.Internal.Connection.PersistentConnectionImpl
struct PersistentConnectionImpl_t1099507887;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t2603311978;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1099507887.h"

// System.Void Firebase.Database.Internal.Connection.PersistentConnectionImpl/ConnectionRequestCallback797::.ctor(Firebase.Database.Internal.Connection.PersistentConnectionImpl,System.Boolean)
extern "C"  void ConnectionRequestCallback797__ctor_m3454443184 (ConnectionRequestCallback797_t485289530 * __this, PersistentConnectionImpl_t1099507887 * ___enclosing0, bool ___restoreStateAfterComplete1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Connection.PersistentConnectionImpl/ConnectionRequestCallback797::OnResponse(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern "C"  void ConnectionRequestCallback797_OnResponse_m254856491 (ConnectionRequestCallback797_t485289530 * __this, Il2CppObject* ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
