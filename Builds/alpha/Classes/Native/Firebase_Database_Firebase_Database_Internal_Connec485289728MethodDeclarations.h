﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Connection.PersistentConnectionImpl/ConnectionRequestCallback993
struct ConnectionRequestCallback993_t485289728;
// Firebase.Database.Internal.Connection.PersistentConnectionImpl
struct PersistentConnectionImpl_t1099507887;
// Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen
struct OutstandingListen_t67058648;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t2603311978;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1099507887.h"
#include "Firebase_Database_Firebase_Database_Internal_Connect67058648.h"

// System.Void Firebase.Database.Internal.Connection.PersistentConnectionImpl/ConnectionRequestCallback993::.ctor(Firebase.Database.Internal.Connection.PersistentConnectionImpl,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen)
extern "C"  void ConnectionRequestCallback993__ctor_m3045931981 (ConnectionRequestCallback993_t485289728 * __this, PersistentConnectionImpl_t1099507887 * ___enclosing0, OutstandingListen_t67058648 * ___listen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Connection.PersistentConnectionImpl/ConnectionRequestCallback993::OnResponse(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern "C"  void ConnectionRequestCallback993_OnResponse_m3502974625 (ConnectionRequestCallback993_t485289728 * __this, Il2CppObject* ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
