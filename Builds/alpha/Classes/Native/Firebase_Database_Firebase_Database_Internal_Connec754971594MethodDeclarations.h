﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Connection.WebsocketConnection/WsClientTubesock/Runnable64
struct Runnable64_t754971594;
// Firebase.Database.Internal.Connection.WebsocketConnection/WsClientTubesock
struct WsClientTubesock_t1111414532;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1111414532.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.Database.Internal.Connection.WebsocketConnection/WsClientTubesock/Runnable64::.ctor(Firebase.Database.Internal.Connection.WebsocketConnection/WsClientTubesock,System.String)
extern "C"  void Runnable64__ctor_m4109102567 (Runnable64_t754971594 * __this, WsClientTubesock_t1111414532 * ___enclosing0, String_t* ___str1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Connection.WebsocketConnection/WsClientTubesock/Runnable64::Run()
extern "C"  void Runnable64_Run_m2597082508 (Runnable64_t754971594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
