﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_U969358333MethodDeclarations.h"

// System.Void Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::.ctor(T,Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>)
#define ImmutableTree_1__ctor_m1431106565(__this, ___value0, ___children1, method) ((  void (*) (ImmutableTree_1_t1000466367 *, SyncPoint_t2720557329 *, ImmutableSortedMap_2_t94277708 *, const MethodInfo*))ImmutableTree_1__ctor_m4028204070_gshared)(__this, ___value0, ___children1, method)
// System.Void Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::.ctor(T)
#define ImmutableTree_1__ctor_m596749977(__this, ___value0, method) ((  void (*) (ImmutableTree_1_t1000466367 *, SyncPoint_t2720557329 *, const MethodInfo*))ImmutableTree_1__ctor_m2496271442_gshared)(__this, ___value0, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,T>> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::GetEnumerator()
#define ImmutableTree_1_GetEnumerator_m3927411661(__this, method) ((  Il2CppObject* (*) (ImmutableTree_1_t1000466367 *, const MethodInfo*))ImmutableTree_1_GetEnumerator_m1374165662_gshared)(__this, method)
// System.Collections.IEnumerator Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::System.Collections.IEnumerable.GetEnumerator()
#define ImmutableTree_1_System_Collections_IEnumerable_GetEnumerator_m3344220082(__this, method) ((  Il2CppObject * (*) (ImmutableTree_1_t1000466367 *, const MethodInfo*))ImmutableTree_1_System_Collections_IEnumerable_GetEnumerator_m1780813397_gshared)(__this, method)
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::EmptyInstance()
#define ImmutableTree_1_EmptyInstance_m204585065(__this /* static, unused */, method) ((  ImmutableTree_1_t1000466367 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ImmutableTree_1_EmptyInstance_m3642173572_gshared)(__this /* static, unused */, method)
// T Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::GetValue()
#define ImmutableTree_1_GetValue_m1773109367(__this, method) ((  SyncPoint_t2720557329 * (*) (ImmutableTree_1_t1000466367 *, const MethodInfo*))ImmutableTree_1_GetValue_m3705018054_gshared)(__this, method)
// Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::GetChildren()
#define ImmutableTree_1_GetChildren_m834371739(__this, method) ((  ImmutableSortedMap_2_t94277708 * (*) (ImmutableTree_1_t1000466367 *, const MethodInfo*))ImmutableTree_1_GetChildren_m4256937032_gshared)(__this, method)
// System.Boolean Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::IsEmpty()
#define ImmutableTree_1_IsEmpty_m885920084(__this, method) ((  bool (*) (ImmutableTree_1_t1000466367 *, const MethodInfo*))ImmutableTree_1_IsEmpty_m4143570191_gshared)(__this, method)
// Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::FindRootMostMatchingPath(Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.Utilities.Predicate`1<T>)
#define ImmutableTree_1_FindRootMostMatchingPath_m4151183874(__this, ___relativePath0, ___predicate1, method) ((  Path_t2568473163 * (*) (ImmutableTree_1_t1000466367 *, Path_t2568473163 *, Predicate_1_t1237722006 *, const MethodInfo*))ImmutableTree_1_FindRootMostMatchingPath_m1844098277_gshared)(__this, ___relativePath0, ___predicate1, method)
// Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::FindRootMostPathWithValue(Firebase.Database.Internal.Core.Path)
#define ImmutableTree_1_FindRootMostPathWithValue_m2289554912(__this, ___relativePath0, method) ((  Path_t2568473163 * (*) (ImmutableTree_1_t1000466367 *, Path_t2568473163 *, const MethodInfo*))ImmutableTree_1_FindRootMostPathWithValue_m318551461_gshared)(__this, ___relativePath0, method)
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::GetChild(Firebase.Database.Internal.Snapshot.ChildKey)
#define ImmutableTree_1_GetChild_m2593905891(__this, ___child0, method) ((  ImmutableTree_1_t1000466367 * (*) (ImmutableTree_1_t1000466367 *, ChildKey_t1197802383 *, const MethodInfo*))ImmutableTree_1_GetChild_m4207950176_gshared)(__this, ___child0, method)
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::Subtree(Firebase.Database.Internal.Core.Path)
#define ImmutableTree_1_Subtree_m1875967456(__this, ___relativePath0, method) ((  ImmutableTree_1_t1000466367 * (*) (ImmutableTree_1_t1000466367 *, Path_t2568473163 *, const MethodInfo*))ImmutableTree_1_Subtree_m2292476291_gshared)(__this, ___relativePath0, method)
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::Set(Firebase.Database.Internal.Core.Path,T)
#define ImmutableTree_1_Set_m4090092186(__this, ___relativePath0, ___value1, method) ((  ImmutableTree_1_t1000466367 * (*) (ImmutableTree_1_t1000466367 *, Path_t2568473163 *, SyncPoint_t2720557329 *, const MethodInfo*))ImmutableTree_1_Set_m1462385863_gshared)(__this, ___relativePath0, ___value1, method)
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::Remove(Firebase.Database.Internal.Core.Path)
#define ImmutableTree_1_Remove_m2817104134(__this, ___relativePath0, method) ((  ImmutableTree_1_t1000466367 * (*) (ImmutableTree_1_t1000466367 *, Path_t2568473163 *, const MethodInfo*))ImmutableTree_1_Remove_m3691997035_gshared)(__this, ___relativePath0, method)
// T Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::Get(Firebase.Database.Internal.Core.Path)
#define ImmutableTree_1_Get_m4025149709(__this, ___relativePath0, method) ((  SyncPoint_t2720557329 * (*) (ImmutableTree_1_t1000466367 *, Path_t2568473163 *, const MethodInfo*))ImmutableTree_1_Get_m3285009340_gshared)(__this, ___relativePath0, method)
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::SetTree(Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T>)
#define ImmutableTree_1_SetTree_m1241112935(__this, ___relativePath0, ___newTree1, method) ((  ImmutableTree_1_t1000466367 * (*) (ImmutableTree_1_t1000466367 *, Path_t2568473163 *, ImmutableTree_1_t1000466367 *, const MethodInfo*))ImmutableTree_1_SetTree_m730507020_gshared)(__this, ___relativePath0, ___newTree1, method)
// System.Void Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::Foreach(Firebase.Database.Internal.Core.Utilities.ImmutableTree`1/ITreeVisitor`1<T,System.Object>)
#define ImmutableTree_1_Foreach_m2171106893(__this, ___visitor0, method) ((  void (*) (ImmutableTree_1_t1000466367 *, Il2CppObject*, const MethodInfo*))ImmutableTree_1_Foreach_m2205888316_gshared)(__this, ___visitor0, method)
// System.String Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::ToString()
#define ImmutableTree_1_ToString_m734981054(__this, method) ((  String_t* (*) (ImmutableTree_1_t1000466367 *, const MethodInfo*))ImmutableTree_1_ToString_m1893070879_gshared)(__this, method)
// System.Boolean Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::Equals(System.Object)
#define ImmutableTree_1_Equals_m1346859144(__this, ___o0, method) ((  bool (*) (ImmutableTree_1_t1000466367 *, Il2CppObject *, const MethodInfo*))ImmutableTree_1_Equals_m2384379909_gshared)(__this, ___o0, method)
// System.Int32 Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::GetHashCode()
#define ImmutableTree_1_GetHashCode_m4111251492(__this, method) ((  int32_t (*) (ImmutableTree_1_t1000466367 *, const MethodInfo*))ImmutableTree_1_GetHashCode_m2582451103_gshared)(__this, method)
// System.Void Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Core.SyncPoint>::.cctor()
#define ImmutableTree_1__cctor_m1828184326(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ImmutableTree_1__cctor_m171993079_gshared)(__this /* static, unused */, method)
