﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.Path/Enumerator174
struct Enumerator174_t1007998148;
// Firebase.Database.Internal.Core.Path
struct Path_t2568473163;
// System.Object
struct Il2CppObject;
// Firebase.Database.Internal.Snapshot.ChildKey
struct ChildKey_t1197802383;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2568473163.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps1197802383.h"

// System.Void Firebase.Database.Internal.Core.Path/Enumerator174::.ctor(Firebase.Database.Internal.Core.Path)
extern "C"  void Enumerator174__ctor_m2580862332 (Enumerator174_t1007998148 * __this, Path_t2568473163 * ___enclosing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.Database.Internal.Core.Path/Enumerator174::MoveNext()
extern "C"  bool Enumerator174_MoveNext_m368514115 (Enumerator174_t1007998148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Firebase.Database.Internal.Core.Path/Enumerator174::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator174_System_Collections_IEnumerator_get_Current_m2709869603 (Enumerator174_t1007998148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Database.Internal.Snapshot.ChildKey Firebase.Database.Internal.Core.Path/Enumerator174::get_Current()
extern "C"  ChildKey_t1197802383 * Enumerator174_get_Current_m942875142 (Enumerator174_t1007998148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Core.Path/Enumerator174::set_Current(Firebase.Database.Internal.Snapshot.ChildKey)
extern "C"  void Enumerator174_set_Current_m2937457235 (Enumerator174_t1007998148 * __this, ChildKey_t1197802383 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Core.Path/Enumerator174::Dispose()
extern "C"  void Enumerator174_Dispose_m4227094002 (Enumerator174_t1007998148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Core.Path/Enumerator174::Reset()
extern "C"  void Enumerator174_Reset_m1696285324 (Enumerator174_t1007998148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
