﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.Repo/Runnable326
struct Runnable326_t1011990335;
// Firebase.Database.DatabaseReference/CompletionListener
struct CompletionListener_t93014473;
// Firebase.Database.DatabaseError
struct DatabaseError_t1067746743;
// Firebase.Database.DatabaseReference
struct DatabaseReference_t1167676104;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_DatabaseReferenc93014473.h"
#include "Firebase_Database_Firebase_Database_DatabaseError1067746743.h"
#include "Firebase_Database_Firebase_Database_DatabaseRefere1167676104.h"

// System.Void Firebase.Database.Internal.Core.Repo/Runnable326::.ctor(Firebase.Database.DatabaseReference/CompletionListener,Firebase.Database.DatabaseError,Firebase.Database.DatabaseReference)
extern "C"  void Runnable326__ctor_m1929827046 (Runnable326_t1011990335 * __this, CompletionListener_t93014473 * ___onComplete0, DatabaseError_t1067746743 * ___error1, DatabaseReference_t1167676104 * ___ref2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Core.Repo/Runnable326::Run()
extern "C"  void Runnable326_Run_m3427492553 (Runnable326_t1011990335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
