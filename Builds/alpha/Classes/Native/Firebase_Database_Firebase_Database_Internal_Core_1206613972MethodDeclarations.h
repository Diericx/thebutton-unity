﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.Utilities.Predicate`1<System.Object>
struct Predicate_1_t1206613972;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Core.Utilities.Predicate`1<System.Object>::.ctor()
extern "C"  void Predicate_1__ctor_m2995668689_gshared (Predicate_1_t1206613972 * __this, const MethodInfo* method);
#define Predicate_1__ctor_m2995668689(__this, method) ((  void (*) (Predicate_1_t1206613972 *, const MethodInfo*))Predicate_1__ctor_m2995668689_gshared)(__this, method)
// System.Void Firebase.Database.Internal.Core.Utilities.Predicate`1<System.Object>::.cctor()
extern "C"  void Predicate_1__cctor_m1504777504_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Predicate_1__cctor_m1504777504(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Predicate_1__cctor_m1504777504_gshared)(__this /* static, unused */, method)
