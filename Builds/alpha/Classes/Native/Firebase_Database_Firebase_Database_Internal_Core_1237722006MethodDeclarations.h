﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1206613972MethodDeclarations.h"

// System.Void Firebase.Database.Internal.Core.Utilities.Predicate`1<Firebase.Database.Internal.Core.SyncPoint>::.ctor()
#define Predicate_1__ctor_m314610906(__this, method) ((  void (*) (Predicate_1_t1237722006 *, const MethodInfo*))Predicate_1__ctor_m2995668689_gshared)(__this, method)
// System.Void Firebase.Database.Internal.Core.Utilities.Predicate`1<Firebase.Database.Internal.Core.SyncPoint>::.cctor()
#define Predicate_1__cctor_m3426269191(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Predicate_1__cctor_m1504777504_gshared)(__this /* static, unused */, method)
