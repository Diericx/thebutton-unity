﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.View.Filter.NodeFilter
struct NodeFilter_t1410970950;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Core.View.Filter.NodeFilter::.ctor()
extern "C"  void NodeFilter__ctor_m3209034546 (NodeFilter_t1410970950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
