﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.Utilities.Predicate`1/Predicate6<System.Object>
struct Predicate6_t1443174078;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Firebase.Database.Internal.Core.Utilities.Predicate`1/Predicate6<System.Object>::.ctor()
extern "C"  void Predicate6__ctor_m2242370669_gshared (Predicate6_t1443174078 * __this, const MethodInfo* method);
#define Predicate6__ctor_m2242370669(__this, method) ((  void (*) (Predicate6_t1443174078 *, const MethodInfo*))Predicate6__ctor_m2242370669_gshared)(__this, method)
// System.Boolean Firebase.Database.Internal.Core.Utilities.Predicate`1/Predicate6<System.Object>::Evaluate(T)
extern "C"  bool Predicate6_Evaluate_m1553685562_gshared (Predicate6_t1443174078 * __this, Il2CppObject * ___object0, const MethodInfo* method);
#define Predicate6_Evaluate_m1553685562(__this, ___object0, method) ((  bool (*) (Predicate6_t1443174078 *, Il2CppObject *, const MethodInfo*))Predicate6_Evaluate_m1553685562_gshared)(__this, ___object0, method)
