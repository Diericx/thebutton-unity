﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.Repo/TreeVisitor778
struct TreeVisitor778_t1464745291;
// Firebase.Database.Internal.Core.Repo
struct Repo_t1244308462;
// Firebase.Database.Internal.Core.Utilities.Tree`1<System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.Repo/TransactionData>>
struct Tree_1_t3109747774;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1244308462.h"

// System.Void Firebase.Database.Internal.Core.Repo/TreeVisitor778::.ctor(Firebase.Database.Internal.Core.Repo)
extern "C"  void TreeVisitor778__ctor_m106163860 (TreeVisitor778_t1464745291 * __this, Repo_t1244308462 * ___enclosing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Core.Repo/TreeVisitor778::VisitTree(Firebase.Database.Internal.Core.Utilities.Tree`1<System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.Repo/TransactionData>>)
extern "C"  void TreeVisitor778_VisitTree_m3260297024 (TreeVisitor778_t1464745291 * __this, Tree_1_t3109747774 * ___tree0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
