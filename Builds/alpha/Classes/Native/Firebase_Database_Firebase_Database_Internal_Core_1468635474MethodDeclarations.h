﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.Context/ConnectionAuthTokenProvider258/GetTokenCompletionListener261
struct GetTokenCompletionListener261_t1468635474;
// Firebase.Database.Internal.Connection.ConnectionAuthTokenProvider/IGetTokenCallback
struct IGetTokenCallback_t3391042181;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.Database.Internal.Core.Context/ConnectionAuthTokenProvider258/GetTokenCompletionListener261::.ctor(Firebase.Database.Internal.Connection.ConnectionAuthTokenProvider/IGetTokenCallback)
extern "C"  void GetTokenCompletionListener261__ctor_m1822589882 (GetTokenCompletionListener261_t1468635474 * __this, Il2CppObject * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Core.Context/ConnectionAuthTokenProvider258/GetTokenCompletionListener261::OnSuccess(System.String)
extern "C"  void GetTokenCompletionListener261_OnSuccess_m1942071935 (GetTokenCompletionListener261_t1468635474 * __this, String_t* ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
