﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.Utilities.Tree`1/TreeVisitor111<System.Object>
struct TreeVisitor111_t1617732397;
// Firebase.Database.Internal.Core.Utilities.Tree`1/ITreeVisitor<System.Object>
struct ITreeVisitor_t512772653;
// Firebase.Database.Internal.Core.Utilities.Tree`1<System.Object>
struct Tree_1_t3114744003;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Core.Utilities.Tree`1/TreeVisitor111<System.Object>::.ctor(Firebase.Database.Internal.Core.Utilities.Tree`1/ITreeVisitor<T>,System.Boolean)
extern "C"  void TreeVisitor111__ctor_m2621602448_gshared (TreeVisitor111_t1617732397 * __this, Il2CppObject* ___visitor0, bool ___childrenFirst1, const MethodInfo* method);
#define TreeVisitor111__ctor_m2621602448(__this, ___visitor0, ___childrenFirst1, method) ((  void (*) (TreeVisitor111_t1617732397 *, Il2CppObject*, bool, const MethodInfo*))TreeVisitor111__ctor_m2621602448_gshared)(__this, ___visitor0, ___childrenFirst1, method)
// System.Void Firebase.Database.Internal.Core.Utilities.Tree`1/TreeVisitor111<System.Object>::VisitTree(Firebase.Database.Internal.Core.Utilities.Tree`1<T>)
extern "C"  void TreeVisitor111_VisitTree_m3204516422_gshared (TreeVisitor111_t1617732397 * __this, Tree_1_t3114744003 * ___tree0, const MethodInfo* method);
#define TreeVisitor111_VisitTree_m3204516422(__this, ___tree0, method) ((  void (*) (TreeVisitor111_t1617732397 *, Tree_1_t3114744003 *, const MethodInfo*))TreeVisitor111_VisitTree_m3204516422_gshared)(__this, ___tree0, method)
