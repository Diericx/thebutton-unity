﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.WriteTree/Predicate372
struct Predicate372_t1781886473;
// Firebase.Database.Internal.Core.UserWriteRecord
struct UserWriteRecord_t388677579;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_U388677579.h"

// System.Void Firebase.Database.Internal.Core.WriteTree/Predicate372::.ctor()
extern "C"  void Predicate372__ctor_m606799772 (Predicate372_t1781886473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.Database.Internal.Core.WriteTree/Predicate372::Evaluate(Firebase.Database.Internal.Core.UserWriteRecord)
extern "C"  bool Predicate372_Evaluate_m3134379240 (Predicate372_t1781886473 * __this, UserWriteRecord_t388677579 * ___write0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
