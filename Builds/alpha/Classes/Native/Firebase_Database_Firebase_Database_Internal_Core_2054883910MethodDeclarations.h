﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.ThreadInitializer/ThreadInitializer7
struct ThreadInitializer7_t2054883910;
// Google.Sharpen.Thread
struct Thread_t1322377586;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Google_Sharpen_Google_Sharpen_Thread1322377586.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.Database.Internal.Core.ThreadInitializer/ThreadInitializer7::.ctor()
extern "C"  void ThreadInitializer7__ctor_m297748665 (ThreadInitializer7_t2054883910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Core.ThreadInitializer/ThreadInitializer7::SetName(Google.Sharpen.Thread,System.String)
extern "C"  void ThreadInitializer7_SetName_m7078872 (ThreadInitializer7_t2054883910 * __this, Thread_t1322377586 * ___t0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Core.ThreadInitializer/ThreadInitializer7::SetDaemon(Google.Sharpen.Thread,System.Boolean)
extern "C"  void ThreadInitializer7_SetDaemon_m4148543428 (ThreadInitializer7_t2054883910 * __this, Thread_t1322377586 * ___t0, bool ___isDaemon1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
