﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.Repo/TransactionData
struct TransactionData_t2143512465;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2143512465.h"

// System.Int32 Firebase.Database.Internal.Core.Repo/TransactionData::CompareTo(Firebase.Database.Internal.Core.Repo/TransactionData)
extern "C"  int32_t TransactionData_CompareTo_m1676081351 (TransactionData_t2143512465 * __this, TransactionData_t2143512465 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
