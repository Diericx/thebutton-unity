﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.SyncTree/Callable399
struct Callable399_t2358426799;
// Firebase.Database.Internal.Core.SyncTree
struct SyncTree_t528142079;
// Firebase.Database.Internal.Core.EventRegistration
struct EventRegistration_t4222917807;
// System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.DataEvent>
struct IList_1_t3871132203;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_S528142079.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_4222917807.h"

// System.Void Firebase.Database.Internal.Core.SyncTree/Callable399::.ctor(Firebase.Database.Internal.Core.SyncTree,Firebase.Database.Internal.Core.EventRegistration)
extern "C"  void Callable399__ctor_m2603645378 (Callable399_t2358426799 * __this, SyncTree_t528142079 * ___enclosing0, EventRegistration_t4222917807 * ___eventRegistration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.DataEvent> Firebase.Database.Internal.Core.SyncTree/Callable399::Call()
extern "C"  Il2CppObject* Callable399_Call_m1217683901 (Callable399_t2358426799 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
