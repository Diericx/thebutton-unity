﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.Tag
struct Tag_t2439924210;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Firebase.Database.Internal.Core.Tag::.ctor(System.Int64)
extern "C"  void Tag__ctor_m3114662961 (Tag_t2439924210 * __this, int64_t ___tagNumber0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Firebase.Database.Internal.Core.Tag::GetTagNumber()
extern "C"  int64_t Tag_GetTagNumber_m761908269 (Tag_t2439924210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Database.Internal.Core.Tag::ToString()
extern "C"  String_t* Tag_ToString_m221230566 (Tag_t2439924210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.Database.Internal.Core.Tag::Equals(System.Object)
extern "C"  bool Tag_Equals_m464528122 (Tag_t2439924210 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.Database.Internal.Core.Tag::GetHashCode()
extern "C"  int32_t Tag_GetHashCode_m822896086 (Tag_t2439924210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
