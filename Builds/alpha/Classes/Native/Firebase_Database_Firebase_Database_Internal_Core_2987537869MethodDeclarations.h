﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.Repo/Runnable1019
struct Runnable1019_t2987537869;
// Firebase.Database.Internal.Core.Repo
struct Repo_t1244308462;
// Firebase.Database.Internal.Core.Repo/TransactionData
struct TransactionData_t2143512465;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1244308462.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2143512465.h"

// System.Void Firebase.Database.Internal.Core.Repo/Runnable1019::.ctor(Firebase.Database.Internal.Core.Repo,Firebase.Database.Internal.Core.Repo/TransactionData)
extern "C"  void Runnable1019__ctor_m2009729825 (Runnable1019_t2987537869 * __this, Repo_t1244308462 * ___enclosing0, TransactionData_t2143512465 * ___transaction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Core.Repo/Runnable1019::Run()
extern "C"  void Runnable1019_Run_m1893186351 (Runnable1019_t2987537869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
