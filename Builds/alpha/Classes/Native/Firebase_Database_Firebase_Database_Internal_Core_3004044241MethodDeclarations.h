﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.DatabaseConfig
struct DatabaseConfig_t3004044241;
// Firebase.FirebaseApp
struct FirebaseApp_t210707726;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_FirebaseApp210707726.h"

// System.Void Firebase.Database.Internal.Core.DatabaseConfig::.ctor()
extern "C"  void DatabaseConfig__ctor_m3792622928 (DatabaseConfig_t3004044241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Core.DatabaseConfig::SetFirebaseApp(Firebase.FirebaseApp)
extern "C"  void DatabaseConfig_SetFirebaseApp_m54784199 (DatabaseConfig_t3004044241 * __this, FirebaseApp_t210707726 * ___app0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
