﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.Repo/TreeVisitor907
struct TreeVisitor907_t3030829553;
// Firebase.Database.Internal.Core.Repo
struct Repo_t1244308462;
// Firebase.Database.Internal.Core.Utilities.Tree`1<System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.Repo/TransactionData>>
struct Tree_1_t3109747774;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1244308462.h"

// System.Void Firebase.Database.Internal.Core.Repo/TreeVisitor907::.ctor(Firebase.Database.Internal.Core.Repo)
extern "C"  void TreeVisitor907__ctor_m429732432 (TreeVisitor907_t3030829553 * __this, Repo_t1244308462 * ___enclosing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Core.Repo/TreeVisitor907::VisitTree(Firebase.Database.Internal.Core.Utilities.Tree`1<System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.Repo/TransactionData>>)
extern "C"  void TreeVisitor907_VisitTree_m3591607472 (TreeVisitor907_t3030829553 * __this, Tree_1_t3109747774 * ___tree0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
