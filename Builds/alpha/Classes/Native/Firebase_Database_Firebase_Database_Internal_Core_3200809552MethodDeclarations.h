﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1206613972MethodDeclarations.h"

// System.Void Firebase.Database.Internal.Core.Utilities.Predicate`1<Firebase.Database.Internal.Core.UserWriteRecord>::.ctor()
#define Predicate_1__ctor_m682497130(__this, method) ((  void (*) (Predicate_1_t3200809552 *, const MethodInfo*))Predicate_1__ctor_m2995668689_gshared)(__this, method)
// System.Void Firebase.Database.Internal.Core.Utilities.Predicate`1<Firebase.Database.Internal.Core.UserWriteRecord>::.cctor()
#define Predicate_1__cctor_m1512613769(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Predicate_1__cctor_m1504777504_gshared)(__this /* static, unused */, method)
