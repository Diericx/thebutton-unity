﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.View.EventGenerator/Comparator70
struct Comparator70_t3660935884;
// Firebase.Database.Internal.Core.View.EventGenerator
struct EventGenerator_t3304926575;
// Firebase.Database.Internal.Core.View.Change
struct Change_t639587248;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3304926575.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_V639587248.h"

// System.Void Firebase.Database.Internal.Core.View.EventGenerator/Comparator70::.ctor(Firebase.Database.Internal.Core.View.EventGenerator)
extern "C"  void Comparator70__ctor_m2830954055 (Comparator70_t3660935884 * __this, EventGenerator_t3304926575 * ___enclosing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.Database.Internal.Core.View.EventGenerator/Comparator70::Compare(Firebase.Database.Internal.Core.View.Change,Firebase.Database.Internal.Core.View.Change)
extern "C"  int32_t Comparator70_Compare_m65355560 (Comparator70_t3660935884 * __this, Change_t639587248 * ___a0, Change_t639587248 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
