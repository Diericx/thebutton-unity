﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.Repo/Runnable1144
struct Runnable1144_t3747052656;
// Firebase.Database.Internal.Core.Repo/TransactionData
struct TransactionData_t2143512465;
// Firebase.Database.DatabaseError
struct DatabaseError_t1067746743;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2143512465.h"
#include "Firebase_Database_Firebase_Database_DatabaseError1067746743.h"

// System.Void Firebase.Database.Internal.Core.Repo/Runnable1144::.ctor(Firebase.Database.Internal.Core.Repo/TransactionData,Firebase.Database.DatabaseError)
extern "C"  void Runnable1144__ctor_m1578225325 (Runnable1144_t3747052656 * __this, TransactionData_t2143512465 * ___transaction0, DatabaseError_t1067746743 * ___abortError1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Core.Repo/Runnable1144::Run()
extern "C"  void Runnable1144_Run_m3251072506 (Runnable1144_t3747052656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
