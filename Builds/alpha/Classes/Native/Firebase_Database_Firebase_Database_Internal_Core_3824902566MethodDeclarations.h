﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3829898795MethodDeclarations.h"

// System.Void Firebase.Database.Internal.Core.Utilities.TreeNode`1<System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.Repo/TransactionData>>::.ctor()
#define TreeNode_1__ctor_m578994537(__this, method) ((  void (*) (TreeNode_1_t3824902566 *, const MethodInfo*))TreeNode_1__ctor_m4209293980_gshared)(__this, method)
// System.String Firebase.Database.Internal.Core.Utilities.TreeNode`1<System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.Repo/TransactionData>>::ToStringExpr(System.String)
#define TreeNode_1_ToStringExpr_m3555516624(__this, ___prefix0, method) ((  String_t* (*) (TreeNode_1_t3824902566 *, String_t*, const MethodInfo*))TreeNode_1_ToStringExpr_m422485072_gshared)(__this, ___prefix0, method)
