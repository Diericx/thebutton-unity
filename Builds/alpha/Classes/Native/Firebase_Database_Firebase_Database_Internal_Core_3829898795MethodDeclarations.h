﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.Utilities.TreeNode`1<System.Object>
struct TreeNode_1_t3829898795;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.Database.Internal.Core.Utilities.TreeNode`1<System.Object>::.ctor()
extern "C"  void TreeNode_1__ctor_m4209293980_gshared (TreeNode_1_t3829898795 * __this, const MethodInfo* method);
#define TreeNode_1__ctor_m4209293980(__this, method) ((  void (*) (TreeNode_1_t3829898795 *, const MethodInfo*))TreeNode_1__ctor_m4209293980_gshared)(__this, method)
// System.String Firebase.Database.Internal.Core.Utilities.TreeNode`1<System.Object>::ToStringExpr(System.String)
extern "C"  String_t* TreeNode_1_ToStringExpr_m422485072_gshared (TreeNode_1_t3829898795 * __this, String_t* ___prefix0, const MethodInfo* method);
#define TreeNode_1_ToStringExpr_m422485072(__this, ___prefix0, method) ((  String_t* (*) (TreeNode_1_t3829898795 *, String_t*, const MethodInfo*))TreeNode_1_ToStringExpr_m422485072_gshared)(__this, ___prefix0, method)
