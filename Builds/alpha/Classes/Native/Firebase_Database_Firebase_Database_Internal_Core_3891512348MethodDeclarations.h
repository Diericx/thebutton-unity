﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.View.ViewProcessor/ProcessorResult
struct ProcessorResult_t3891512348;
// Firebase.Database.Internal.Core.View.ViewCache
struct ViewCache_t983378685;
// System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.Change>
struct IList_1_t1180527849;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_V983378685.h"

// System.Void Firebase.Database.Internal.Core.View.ViewProcessor/ProcessorResult::.ctor(Firebase.Database.Internal.Core.View.ViewCache,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.Change>)
extern "C"  void ProcessorResult__ctor_m525128489 (ProcessorResult_t3891512348 * __this, ViewCache_t983378685 * ___viewCache0, Il2CppObject* ___changes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
