﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.Repo/TreeVisitor1074
struct TreeVisitor1074_t3961001445;
// Firebase.Database.Internal.Core.Repo
struct Repo_t1244308462;
// System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.Repo/TransactionData>
struct IList_1_t2684453066;
// Firebase.Database.Internal.Core.Utilities.Tree`1<System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.Repo/TransactionData>>
struct Tree_1_t3109747774;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1244308462.h"

// System.Void Firebase.Database.Internal.Core.Repo/TreeVisitor1074::.ctor(Firebase.Database.Internal.Core.Repo,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.Repo/TransactionData>)
extern "C"  void TreeVisitor1074__ctor_m1666967138 (TreeVisitor1074_t3961001445 * __this, Repo_t1244308462 * ___enclosing0, Il2CppObject* ___queue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Core.Repo/TreeVisitor1074::VisitTree(Firebase.Database.Internal.Core.Utilities.Tree`1<System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.Repo/TransactionData>>)
extern "C"  void TreeVisitor1074_VisitTree_m2940661134 (TreeVisitor1074_t3961001445 * __this, Tree_1_t3109747774 * ___tree0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
