﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.Repo/TokenChangeListener79
struct TokenChangeListener79_t3977814488;
// Firebase.Database.Internal.Core.Repo
struct Repo_t1244308462;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1244308462.h"

// System.Void Firebase.Database.Internal.Core.Repo/TokenChangeListener79::.ctor(Firebase.Database.Internal.Core.Repo)
extern "C"  void TokenChangeListener79__ctor_m3512283955 (TokenChangeListener79_t3977814488 * __this, Repo_t1244308462 * ___enclosing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Core.Repo/TokenChangeListener79::OnTokenChange()
extern "C"  void TokenChangeListener79_OnTokenChange_m1252074463 (TokenChangeListener79_t3977814488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
