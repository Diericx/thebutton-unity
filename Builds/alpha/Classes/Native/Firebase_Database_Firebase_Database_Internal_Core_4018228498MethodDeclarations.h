﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.ThreadInitializer
struct ThreadInitializer_t4018228498;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Core.ThreadInitializer::.ctor()
extern "C"  void ThreadInitializer__ctor_m1777708885 (ThreadInitializer_t4018228498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Core.ThreadInitializer::.cctor()
extern "C"  void ThreadInitializer__cctor_m3675758128 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
