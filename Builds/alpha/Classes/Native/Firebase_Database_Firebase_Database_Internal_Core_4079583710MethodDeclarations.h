﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.RepoInfo
struct RepoInfo_t4079583710;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Core.RepoInfo::.ctor()
extern "C"  void RepoInfo__ctor_m3758972803 (RepoInfo_t4079583710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Database.Internal.Core.RepoInfo::ToString()
extern "C"  String_t* RepoInfo_ToString_m2079388828 (RepoInfo_t4079583710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
