﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.Repo/RequestResultCallback817/Runnable838
struct Runnable838_t456743155;
// Firebase.Database.Internal.Core.Repo/TransactionData
struct TransactionData_t2143512465;
// Firebase.Database.DataSnapshot
struct DataSnapshot_t1287895350;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2143512465.h"
#include "Firebase_Database_Firebase_Database_DataSnapshot1287895350.h"

// System.Void Firebase.Database.Internal.Core.Repo/RequestResultCallback817/Runnable838::.ctor(Firebase.Database.Internal.Core.Repo/TransactionData,Firebase.Database.DataSnapshot)
extern "C"  void Runnable838__ctor_m72475675 (Runnable838_t456743155 * __this, TransactionData_t2143512465 * ___txn0, DataSnapshot_t1287895350 * ___snap1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Core.Repo/RequestResultCallback817/Runnable838::Run()
extern "C"  void Runnable838_Run_m1691358965 (Runnable838_t456743155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
