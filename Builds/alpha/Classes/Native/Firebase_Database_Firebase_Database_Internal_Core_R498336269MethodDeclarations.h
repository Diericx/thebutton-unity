﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.Repo/ListenProvider106/Runnable111
struct Runnable111_t498336269;
// Firebase.Database.Internal.Core.Repo/ListenProvider106
struct ListenProvider106_t3500234454;
// Firebase.Database.Internal.Core.View.QuerySpec
struct QuerySpec_t377558711;
// Firebase.Database.Internal.Core.SyncTree/ICompletionListener
struct ICompletionListener_t1189386635;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3500234454.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_V377558711.h"

// System.Void Firebase.Database.Internal.Core.Repo/ListenProvider106/Runnable111::.ctor(Firebase.Database.Internal.Core.Repo/ListenProvider106,Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.SyncTree/ICompletionListener)
extern "C"  void Runnable111__ctor_m3570599847 (Runnable111_t498336269 * __this, ListenProvider106_t3500234454 * ___enclosing0, QuerySpec_t377558711 * ___query1, Il2CppObject * ___onComplete2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Core.Repo/ListenProvider106/Runnable111::Run()
extern "C"  void Runnable111_Run_m2050681335 (Runnable111_t498336269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
