﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.Repo/TreeFilter1088
struct TreeFilter1088_t949104308;
// Firebase.Database.Internal.Core.Repo
struct Repo_t1244308462;
// Firebase.Database.Internal.Core.Utilities.Tree`1<System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.Repo/TransactionData>>
struct Tree_1_t3109747774;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_1244308462.h"

// System.Void Firebase.Database.Internal.Core.Repo/TreeFilter1088::.ctor(Firebase.Database.Internal.Core.Repo,System.Int32)
extern "C"  void TreeFilter1088__ctor_m1112895652 (TreeFilter1088_t949104308 * __this, Repo_t1244308462 * ___enclosing0, int32_t ___reason1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.Database.Internal.Core.Repo/TreeFilter1088::FilterTreeNode(Firebase.Database.Internal.Core.Utilities.Tree`1<System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.Repo/TransactionData>>)
extern "C"  bool TreeFilter1088_FilterTreeNode_m1587368830 (TreeFilter1088_t949104308 * __this, Tree_1_t3109747774 * ___tree0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
