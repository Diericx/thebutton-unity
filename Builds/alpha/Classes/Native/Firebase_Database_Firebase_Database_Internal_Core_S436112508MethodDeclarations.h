﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.SyncTree/Callable330
struct Callable330_t436112508;
// Firebase.Database.Internal.Core.SyncTree
struct SyncTree_t528142079;
// Firebase.Database.Internal.Core.Tag
struct Tag_t2439924210;
// System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.Event>
struct IList_1_t1273747003;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_S528142079.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2439924210.h"

// System.Void Firebase.Database.Internal.Core.SyncTree/Callable330::.ctor(Firebase.Database.Internal.Core.SyncTree,Firebase.Database.Internal.Core.Tag)
extern "C"  void Callable330__ctor_m2953103958 (Callable330_t436112508 * __this, SyncTree_t528142079 * ___enclosing0, Tag_t2439924210 * ___tag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.Event> Firebase.Database.Internal.Core.SyncTree/Callable330::Call()
extern "C"  Il2CppObject* Callable330_Call_m1235943094 (Callable330_t436112508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
