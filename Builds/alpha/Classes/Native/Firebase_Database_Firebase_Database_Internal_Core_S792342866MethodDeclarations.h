﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.SyncTree/Callable318
struct Callable318_t792342866;
// Firebase.Database.Internal.Core.SyncTree
struct SyncTree_t528142079;
// Firebase.Database.Internal.Core.Path
struct Path_t2568473163;
// System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.Event>
struct IList_1_t1273747003;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_S528142079.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2568473163.h"

// System.Void Firebase.Database.Internal.Core.SyncTree/Callable318::.ctor(Firebase.Database.Internal.Core.SyncTree,Firebase.Database.Internal.Core.Path)
extern "C"  void Callable318__ctor_m506390437 (Callable318_t792342866 * __this, SyncTree_t528142079 * ___enclosing0, Path_t2568473163 * ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.Event> Firebase.Database.Internal.Core.SyncTree/Callable318::Call()
extern "C"  Il2CppObject* Callable318_Call_m202893284 (Callable318_t792342866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
