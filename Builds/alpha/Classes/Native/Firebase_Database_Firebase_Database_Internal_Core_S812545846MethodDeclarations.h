﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.SyncTree/Callable259
struct Callable259_t812545846;
// Firebase.Database.Internal.Core.SyncTree
struct SyncTree_t528142079;
// System.Collections.Generic.IDictionary`2<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Snapshot.Node>
struct IDictionary_2_t4243616403;
// Firebase.Database.Internal.Core.Path
struct Path_t2568473163;
// System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.Event>
struct IList_1_t1273747003;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_S528142079.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2568473163.h"

// System.Void Firebase.Database.Internal.Core.SyncTree/Callable259::.ctor(Firebase.Database.Internal.Core.SyncTree,System.Collections.Generic.IDictionary`2<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Snapshot.Node>,Firebase.Database.Internal.Core.Path)
extern "C"  void Callable259__ctor_m888935777 (Callable259_t812545846 * __this, SyncTree_t528142079 * ___enclosing0, Il2CppObject* ___changedChildren1, Path_t2568473163 * ___path2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.Event> Firebase.Database.Internal.Core.SyncTree/Callable259::Call()
extern "C"  Il2CppObject* Callable259_Call_m3337232856 (Callable259_t812545846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
