﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.Utilities.Predicate`1<System.Nullable`1<System.Boolean>>
struct Predicate_1_t605805710;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Core.Utilities.Predicate`1<System.Nullable`1<System.Boolean>>::.ctor()
extern "C"  void Predicate_1__ctor_m3549290457_gshared (Predicate_1_t605805710 * __this, const MethodInfo* method);
#define Predicate_1__ctor_m3549290457(__this, method) ((  void (*) (Predicate_1_t605805710 *, const MethodInfo*))Predicate_1__ctor_m3549290457_gshared)(__this, method)
// System.Void Firebase.Database.Internal.Core.Utilities.Predicate`1<System.Nullable`1<System.Boolean>>::.cctor()
extern "C"  void Predicate_1__cctor_m612371706_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Predicate_1__cctor_m612371706(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Predicate_1__cctor_m612371706_gshared)(__this /* static, unused */, method)
