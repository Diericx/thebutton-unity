﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.Utilities.Predicate`1/Predicate6<System.Nullable`1<System.Boolean>>
struct Predicate6_t842365816;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"

// System.Void Firebase.Database.Internal.Core.Utilities.Predicate`1/Predicate6<System.Nullable`1<System.Boolean>>::.ctor()
extern "C"  void Predicate6__ctor_m3289419165_gshared (Predicate6_t842365816 * __this, const MethodInfo* method);
#define Predicate6__ctor_m3289419165(__this, method) ((  void (*) (Predicate6_t842365816 *, const MethodInfo*))Predicate6__ctor_m3289419165_gshared)(__this, method)
// System.Boolean Firebase.Database.Internal.Core.Utilities.Predicate`1/Predicate6<System.Nullable`1<System.Boolean>>::Evaluate(T)
extern "C"  bool Predicate6_Evaluate_m2929706496_gshared (Predicate6_t842365816 * __this, Nullable_1_t2088641033  ___object0, const MethodInfo* method);
#define Predicate6_Evaluate_m2929706496(__this, ___object0, method) ((  bool (*) (Predicate6_t842365816 *, Nullable_1_t2088641033 , const MethodInfo*))Predicate6_Evaluate_m2929706496_gshared)(__this, ___object0, method)
