﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_U969358333MethodDeclarations.h"

// System.Void Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::.ctor(T,Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>)
#define ImmutableTree_1__ctor_m3196449309(__this, ___value0, ___children1, method) ((  void (*) (ImmutableTree_1_t919968048 *, Node_t2640059010 *, ImmutableSortedMap_2_t94277708 *, const MethodInfo*))ImmutableTree_1__ctor_m4028204070_gshared)(__this, ___value0, ___children1, method)
// System.Void Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::.ctor(T)
#define ImmutableTree_1__ctor_m1189041185(__this, ___value0, method) ((  void (*) (ImmutableTree_1_t919968048 *, Node_t2640059010 *, const MethodInfo*))ImmutableTree_1__ctor_m2496271442_gshared)(__this, ___value0, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,T>> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::GetEnumerator()
#define ImmutableTree_1_GetEnumerator_m2143999977(__this, method) ((  Il2CppObject* (*) (ImmutableTree_1_t919968048 *, const MethodInfo*))ImmutableTree_1_GetEnumerator_m1374165662_gshared)(__this, method)
// System.Collections.IEnumerator Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::System.Collections.IEnumerable.GetEnumerator()
#define ImmutableTree_1_System_Collections_IEnumerable_GetEnumerator_m3423873794(__this, method) ((  Il2CppObject * (*) (ImmutableTree_1_t919968048 *, const MethodInfo*))ImmutableTree_1_System_Collections_IEnumerable_GetEnumerator_m1780813397_gshared)(__this, method)
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::EmptyInstance()
#define ImmutableTree_1_EmptyInstance_m1624522393(__this /* static, unused */, method) ((  ImmutableTree_1_t919968048 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ImmutableTree_1_EmptyInstance_m3642173572_gshared)(__this /* static, unused */, method)
// T Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::GetValue()
#define ImmutableTree_1_GetValue_m691323203(__this, method) ((  Node_t2640059010 * (*) (ImmutableTree_1_t919968048 *, const MethodInfo*))ImmutableTree_1_GetValue_m3705018054_gshared)(__this, method)
// Firebase.Database.Internal.Collection.ImmutableSortedMap`2<Firebase.Database.Internal.Snapshot.ChildKey,System.Object> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::GetChildren()
#define ImmutableTree_1_GetChildren_m1500800967(__this, method) ((  ImmutableSortedMap_2_t94277708 * (*) (ImmutableTree_1_t919968048 *, const MethodInfo*))ImmutableTree_1_GetChildren_m4256937032_gshared)(__this, method)
// System.Boolean Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::IsEmpty()
#define ImmutableTree_1_IsEmpty_m1329460504(__this, method) ((  bool (*) (ImmutableTree_1_t919968048 *, const MethodInfo*))ImmutableTree_1_IsEmpty_m4143570191_gshared)(__this, method)
// Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::FindRootMostMatchingPath(Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.Utilities.Predicate`1<T>)
#define ImmutableTree_1_FindRootMostMatchingPath_m3514645938(__this, ___relativePath0, ___predicate1, method) ((  Path_t2568473163 * (*) (ImmutableTree_1_t919968048 *, Path_t2568473163 *, Predicate_1_t1157223687 *, const MethodInfo*))ImmutableTree_1_FindRootMostMatchingPath_m1844098277_gshared)(__this, ___relativePath0, ___predicate1, method)
// Firebase.Database.Internal.Core.Path Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::FindRootMostPathWithValue(Firebase.Database.Internal.Core.Path)
#define ImmutableTree_1_FindRootMostPathWithValue_m3605983576(__this, ___relativePath0, method) ((  Path_t2568473163 * (*) (ImmutableTree_1_t919968048 *, Path_t2568473163 *, const MethodInfo*))ImmutableTree_1_FindRootMostPathWithValue_m318551461_gshared)(__this, ___relativePath0, method)
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::GetChild(Firebase.Database.Internal.Snapshot.ChildKey)
#define ImmutableTree_1_GetChild_m2186985867(__this, ___child0, method) ((  ImmutableTree_1_t919968048 * (*) (ImmutableTree_1_t919968048 *, ChildKey_t1197802383 *, const MethodInfo*))ImmutableTree_1_GetChild_m4207950176_gshared)(__this, ___child0, method)
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::Subtree(Firebase.Database.Internal.Core.Path)
#define ImmutableTree_1_Subtree_m1093537556(__this, ___relativePath0, method) ((  ImmutableTree_1_t919968048 * (*) (ImmutableTree_1_t919968048 *, Path_t2568473163 *, const MethodInfo*))ImmutableTree_1_Subtree_m2292476291_gshared)(__this, ___relativePath0, method)
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::Set(Firebase.Database.Internal.Core.Path,T)
#define ImmutableTree_1_Set_m3937561142(__this, ___relativePath0, ___value1, method) ((  ImmutableTree_1_t919968048 * (*) (ImmutableTree_1_t919968048 *, Path_t2568473163 *, Node_t2640059010 *, const MethodInfo*))ImmutableTree_1_Set_m1462385863_gshared)(__this, ___relativePath0, ___value1, method)
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::Remove(Firebase.Database.Internal.Core.Path)
#define ImmutableTree_1_Remove_m2207505754(__this, ___relativePath0, method) ((  ImmutableTree_1_t919968048 * (*) (ImmutableTree_1_t919968048 *, Path_t2568473163 *, const MethodInfo*))ImmutableTree_1_Remove_m3691997035_gshared)(__this, ___relativePath0, method)
// T Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::Get(Firebase.Database.Internal.Core.Path)
#define ImmutableTree_1_Get_m4065592413(__this, ___relativePath0, method) ((  Node_t2640059010 * (*) (ImmutableTree_1_t919968048 *, Path_t2568473163 *, const MethodInfo*))ImmutableTree_1_Get_m3285009340_gshared)(__this, ___relativePath0, method)
// Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T> Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::SetTree(Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<T>)
#define ImmutableTree_1_SetTree_m1463435607(__this, ___relativePath0, ___newTree1, method) ((  ImmutableTree_1_t919968048 * (*) (ImmutableTree_1_t919968048 *, Path_t2568473163 *, ImmutableTree_1_t919968048 *, const MethodInfo*))ImmutableTree_1_SetTree_m730507020_gshared)(__this, ___relativePath0, ___newTree1, method)
// System.Void Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::Foreach(Firebase.Database.Internal.Core.Utilities.ImmutableTree`1/ITreeVisitor`1<T,System.Object>)
#define ImmutableTree_1_Foreach_m773024997(__this, ___visitor0, method) ((  void (*) (ImmutableTree_1_t919968048 *, Il2CppObject*, const MethodInfo*))ImmutableTree_1_Foreach_m2205888316_gshared)(__this, ___visitor0, method)
// System.String Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::ToString()
#define ImmutableTree_1_ToString_m1822242184(__this, method) ((  String_t* (*) (ImmutableTree_1_t919968048 *, const MethodInfo*))ImmutableTree_1_ToString_m1893070879_gshared)(__this, method)
// System.Boolean Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::Equals(System.Object)
#define ImmutableTree_1_Equals_m2514675748(__this, ___o0, method) ((  bool (*) (ImmutableTree_1_t919968048 *, Il2CppObject *, const MethodInfo*))ImmutableTree_1_Equals_m2384379909_gshared)(__this, ___o0, method)
// System.Int32 Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::GetHashCode()
#define ImmutableTree_1_GetHashCode_m2369558064(__this, method) ((  int32_t (*) (ImmutableTree_1_t919968048 *, const MethodInfo*))ImmutableTree_1_GetHashCode_m2582451103_gshared)(__this, method)
// System.Void Firebase.Database.Internal.Core.Utilities.ImmutableTree`1<Firebase.Database.Internal.Snapshot.Node>::.cctor()
#define ImmutableTree_1__cctor_m1707208378(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ImmutableTree_1__cctor_m171993079_gshared)(__this /* static, unused */, method)
