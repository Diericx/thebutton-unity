﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.View.View/OperationResult
struct OperationResult_t335720575;
// System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.DataEvent>
struct IList_1_t3871132203;
// System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.Change>
struct IList_1_t1180527849;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Core.View.View/OperationResult::.ctor(System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.DataEvent>,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.Change>)
extern "C"  void OperationResult__ctor_m3821273556 (OperationResult_t335720575 * __this, Il2CppObject* ___events0, Il2CppObject* ___changes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
