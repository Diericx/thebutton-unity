﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.View.EventRaiser/Runnable30
struct Runnable30_t656865134;
// Firebase.Database.Internal.Core.View.EventRaiser
struct EventRaiser_t2009560;
// System.Collections.Generic.List`1<Firebase.Database.Internal.Core.View.Event>
struct List_1_t101927534;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_Vie2009560.h"

// System.Void Firebase.Database.Internal.Core.View.EventRaiser/Runnable30::.ctor(Firebase.Database.Internal.Core.View.EventRaiser,System.Collections.Generic.List`1<Firebase.Database.Internal.Core.View.Event>)
extern "C"  void Runnable30__ctor_m1969263961 (Runnable30_t656865134 * __this, EventRaiser_t2009560 * ___enclosing0, List_1_t101927534 * ___eventsClone1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Core.View.EventRaiser/Runnable30::Run()
extern "C"  void Runnable30_Run_m1041402256 (Runnable30_t656865134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
