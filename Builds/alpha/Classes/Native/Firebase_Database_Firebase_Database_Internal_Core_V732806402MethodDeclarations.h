﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.View.Event
struct Event_t732806402;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Core.View.Event::.ctor()
extern "C"  void Event__ctor_m3403214800 (Event_t732806402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
