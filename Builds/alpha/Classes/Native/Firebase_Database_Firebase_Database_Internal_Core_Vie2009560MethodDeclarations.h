﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Core.View.EventRaiser
struct EventRaiser_t2009560;
// Firebase.Database.Internal.Core.Context
struct Context_t3486154757;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_3486154757.h"

// System.Void Firebase.Database.Internal.Core.View.EventRaiser::.ctor(Firebase.Database.Internal.Core.Context)
extern "C"  void EventRaiser__ctor_m2170893191 (EventRaiser_t2009560 * __this, Context_t3486154757 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
