﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Snapshot.CompoundHash/ChildVisitor206
struct ChildVisitor206_t1476446587;
// Firebase.Database.Internal.Snapshot.CompoundHash/CompoundHashBuilder
struct CompoundHashBuilder_t1995558443;
// Firebase.Database.Internal.Snapshot.ChildKey
struct ChildKey_t1197802383;
// Firebase.Database.Internal.Snapshot.Node
struct Node_t2640059010;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps1995558443.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps1197802383.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps2640059010.h"

// System.Void Firebase.Database.Internal.Snapshot.CompoundHash/ChildVisitor206::.ctor(Firebase.Database.Internal.Snapshot.CompoundHash/CompoundHashBuilder)
extern "C"  void ChildVisitor206__ctor_m3220775809 (ChildVisitor206_t1476446587 * __this, CompoundHashBuilder_t1995558443 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Snapshot.CompoundHash/ChildVisitor206::VisitChild(Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Snapshot.Node)
extern "C"  void ChildVisitor206_VisitChild_m1696122090 (ChildVisitor206_t1476446587 * __this, ChildKey_t1197802383 * ___name0, Node_t2640059010 * ___child1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
