﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Snapshot.Node/ChildrenNode56
struct ChildrenNode56_t1691052188;
// Firebase.Database.Internal.Snapshot.Node
struct Node_t2640059010;
// Firebase.Database.Internal.Snapshot.ChildKey
struct ChildKey_t1197802383;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps2640059010.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps1197802383.h"

// System.Void Firebase.Database.Internal.Snapshot.Node/ChildrenNode56::.ctor()
extern "C"  void ChildrenNode56__ctor_m951607689 (ChildrenNode56_t1691052188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.Database.Internal.Snapshot.Node/ChildrenNode56::CompareTo(Firebase.Database.Internal.Snapshot.Node)
extern "C"  int32_t ChildrenNode56_CompareTo_m2718675718 (ChildrenNode56_t1691052188 * __this, Node_t2640059010 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Database.Internal.Snapshot.Node Firebase.Database.Internal.Snapshot.Node/ChildrenNode56::GetPriority()
extern "C"  Node_t2640059010 * ChildrenNode56_GetPriority_m1976567789 (ChildrenNode56_t1691052188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.Database.Internal.Snapshot.Node/ChildrenNode56::IsEmpty()
extern "C"  bool ChildrenNode56_IsEmpty_m893559004 (ChildrenNode56_t1691052188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.Database.Internal.Snapshot.Node/ChildrenNode56::HasChild(Firebase.Database.Internal.Snapshot.ChildKey)
extern "C"  bool ChildrenNode56_HasChild_m1569600243 (ChildrenNode56_t1691052188 * __this, ChildKey_t1197802383 * ___childKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Database.Internal.Snapshot.Node Firebase.Database.Internal.Snapshot.Node/ChildrenNode56::GetImmediateChild(Firebase.Database.Internal.Snapshot.ChildKey)
extern "C"  Node_t2640059010 * ChildrenNode56_GetImmediateChild_m1940996406 (ChildrenNode56_t1691052188 * __this, ChildKey_t1197802383 * ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Database.Internal.Snapshot.Node/ChildrenNode56::ToString()
extern "C"  String_t* ChildrenNode56_ToString_m2262219356 (ChildrenNode56_t1691052188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
