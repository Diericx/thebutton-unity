﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Snapshot.ChildrenNode/Comparator17
struct Comparator17_t2321959569;
// Firebase.Database.Internal.Snapshot.ChildKey
struct ChildKey_t1197802383;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps1197802383.h"

// System.Void Firebase.Database.Internal.Snapshot.ChildrenNode/Comparator17::.ctor()
extern "C"  void Comparator17__ctor_m1359409856 (Comparator17_t2321959569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.Database.Internal.Snapshot.ChildrenNode/Comparator17::Compare(Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Snapshot.ChildKey)
extern "C"  int32_t Comparator17_Compare_m1446388295 (Comparator17_t2321959569 * __this, ChildKey_t1197802383 * ___o10, ChildKey_t1197802383 * ___o21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
