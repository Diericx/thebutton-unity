﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Snapshot.Node
struct Node_t2640059010;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Snapshot.Node::.ctor()
extern "C"  void Node__ctor_m77508930 (Node_t2640059010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Firebase.Database.Internal.Snapshot.Node::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Node_System_Collections_IEnumerable_GetEnumerator_m1037343087 (Node_t2640059010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Snapshot.Node::.cctor()
extern "C"  void Node__cctor_m1586403041 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
