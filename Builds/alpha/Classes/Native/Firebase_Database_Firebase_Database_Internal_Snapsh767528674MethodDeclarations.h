﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Snapshot.ChildKey/IntegerChildKey
struct IntegerChildKey_t767528674;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.Database.Internal.Snapshot.ChildKey/IntegerChildKey::.ctor(System.String,System.Int32)
extern "C"  void IntegerChildKey__ctor_m1316847892 (IntegerChildKey_t767528674 * __this, String_t* ___name0, int32_t ___intValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.Database.Internal.Snapshot.ChildKey/IntegerChildKey::IsInt()
extern "C"  bool IntegerChildKey_IsInt_m974218190 (IntegerChildKey_t767528674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Firebase.Database.Internal.Snapshot.ChildKey/IntegerChildKey::IntValue()
extern "C"  int32_t IntegerChildKey_IntValue_m1544067069 (IntegerChildKey_t767528674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Database.Internal.Snapshot.ChildKey/IntegerChildKey::ToString()
extern "C"  String_t* IntegerChildKey_ToString_m2427447852 (IntegerChildKey_t767528674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
