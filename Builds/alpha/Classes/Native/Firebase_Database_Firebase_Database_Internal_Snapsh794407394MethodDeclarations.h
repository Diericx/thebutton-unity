﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Snapshot.ChildrenNode/NodeVisitor245
struct NodeVisitor245_t794407394;
// Firebase.Database.Internal.Snapshot.ChildrenNode
struct ChildrenNode_t1490646017;
// Firebase.Database.Internal.Snapshot.ChildrenNode/ChildVisitor
struct ChildVisitor_t394951299;
// Firebase.Database.Internal.Snapshot.ChildKey
struct ChildKey_t1197802383;
// Firebase.Database.Internal.Snapshot.Node
struct Node_t2640059010;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps1490646017.h"
#include "Firebase_Database_Firebase_Database_Internal_Snapsh394951299.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps1197802383.h"
#include "Firebase_Database_Firebase_Database_Internal_Snaps2640059010.h"

// System.Void Firebase.Database.Internal.Snapshot.ChildrenNode/NodeVisitor245::.ctor(Firebase.Database.Internal.Snapshot.ChildrenNode,Firebase.Database.Internal.Snapshot.ChildrenNode/ChildVisitor)
extern "C"  void NodeVisitor245__ctor_m2003236690 (NodeVisitor245_t794407394 * __this, ChildrenNode_t1490646017 * ___enclosing0, ChildVisitor_t394951299 * ___visitor1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Snapshot.ChildrenNode/NodeVisitor245::VisitEntry(Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Snapshot.Node)
extern "C"  void NodeVisitor245_VisitEntry_m3442044381 (NodeVisitor245_t794407394 * __this, ChildKey_t1197802383 * ___key0, Node_t2640059010 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
