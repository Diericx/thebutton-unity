﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Snapshot.ChildrenNode/NamedNodeIterator
struct NamedNodeIterator_t932129784;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Snapshot.Node>>
struct IEnumerator_1_t2932105947;
// Firebase.Database.Internal.Snapshot.NamedNode
struct NamedNode_t787885785;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Snapshot.ChildrenNode/NamedNodeIterator::.ctor(System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Snapshot.Node>>)
extern "C"  void NamedNodeIterator__ctor_m3924137134 (NamedNodeIterator_t932129784 * __this, Il2CppObject* ___iterator0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.Database.Internal.Snapshot.ChildrenNode/NamedNodeIterator::MoveNext()
extern "C"  bool NamedNodeIterator_MoveNext_m3407389365 (NamedNodeIterator_t932129784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Database.Internal.Snapshot.NamedNode Firebase.Database.Internal.Snapshot.ChildrenNode/NamedNodeIterator::get_Current()
extern "C"  NamedNode_t787885785 * NamedNodeIterator_get_Current_m1324997170 (NamedNodeIterator_t932129784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Firebase.Database.Internal.Snapshot.ChildrenNode/NamedNodeIterator::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * NamedNodeIterator_System_Collections_IEnumerator_get_Current_m1254702917 (NamedNodeIterator_t932129784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Snapshot.ChildrenNode/NamedNodeIterator::Dispose()
extern "C"  void NamedNodeIterator_Dispose_m2928486790 (NamedNodeIterator_t932129784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Snapshot.ChildrenNode/NamedNodeIterator::Reset()
extern "C"  void NamedNodeIterator_Reset_m2339217804 (NamedNodeIterator_t932129784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
