﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.TubeSock.WebSocketWriter/Runnable33
struct Runnable33_t4002296479;
// Firebase.Database.Internal.TubeSock.WebSocketWriter
struct WebSocketWriter_t3581815132;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_TubeS3581815132.h"

// System.Void Firebase.Database.Internal.TubeSock.WebSocketWriter/Runnable33::.ctor(Firebase.Database.Internal.TubeSock.WebSocketWriter)
extern "C"  void Runnable33__ctor_m646113135 (Runnable33_t4002296479 * __this, WebSocketWriter_t3581815132 * ___enclosing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.TubeSock.WebSocketWriter/Runnable33::Run()
extern "C"  void Runnable33_Run_m2578351065 (Runnable33_t4002296479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
