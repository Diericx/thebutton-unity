﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.TubeSock.MessageBuilderFactory/IBuilder
struct IBuilder_t3782858154;

#include "codegen/il2cpp-codegen.h"

// Firebase.Database.Internal.TubeSock.MessageBuilderFactory/IBuilder Firebase.Database.Internal.TubeSock.MessageBuilderFactory::Builder(System.Byte)
extern "C"  Il2CppObject * MessageBuilderFactory_Builder_m1540265049 (Il2CppObject * __this /* static, unused */, uint8_t ___opcode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
