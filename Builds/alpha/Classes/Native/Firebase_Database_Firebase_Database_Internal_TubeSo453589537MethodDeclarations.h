﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.TubeSock.WebSocket/ThreadInitializer55
struct ThreadInitializer55_t453589537;
// Google.Sharpen.Thread
struct Thread_t1322377586;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Google_Sharpen_Google_Sharpen_Thread1322377586.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.Database.Internal.TubeSock.WebSocket/ThreadInitializer55::.ctor()
extern "C"  void ThreadInitializer55__ctor_m3688447840 (ThreadInitializer55_t453589537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.TubeSock.WebSocket/ThreadInitializer55::SetName(Google.Sharpen.Thread,System.String)
extern "C"  void ThreadInitializer55_SetName_m1620103481 (ThreadInitializer55_t453589537 * __this, Thread_t1322377586 * ___t0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
