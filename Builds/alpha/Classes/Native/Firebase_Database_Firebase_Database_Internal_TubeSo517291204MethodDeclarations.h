﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.TubeSock.WebSocket/Runnable101
struct Runnable101_t517291204;
// Firebase.Database.Internal.TubeSock.WebSocket
struct WebSocket_t2930208447;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_TubeS2930208447.h"

// System.Void Firebase.Database.Internal.TubeSock.WebSocket/Runnable101::.ctor(Firebase.Database.Internal.TubeSock.WebSocket)
extern "C"  void Runnable101__ctor_m126059551 (Runnable101_t517291204 * __this, WebSocket_t2930208447 * ___enclosing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.TubeSock.WebSocket/Runnable101::Run()
extern "C"  void Runnable101_Run_m1907819022 (Runnable101_t517291204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
