﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Utilities.ParsedUrl
struct ParsedUrl_t1255537880;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Utilities.ParsedUrl::.ctor()
extern "C"  void ParsedUrl__ctor_m3182488512 (ParsedUrl_t1255537880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
