﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Utilities.Pair`2<System.Object,System.Object>
struct Pair_2_t1259711689;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Firebase.Database.Internal.Utilities.Pair`2<System.Object,System.Object>::.ctor(T,TU)
extern "C"  void Pair_2__ctor_m1137640203_gshared (Pair_2_t1259711689 * __this, Il2CppObject * ___first0, Il2CppObject * ___second1, const MethodInfo* method);
#define Pair_2__ctor_m1137640203(__this, ___first0, ___second1, method) ((  void (*) (Pair_2_t1259711689 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Pair_2__ctor_m1137640203_gshared)(__this, ___first0, ___second1, method)
// T Firebase.Database.Internal.Utilities.Pair`2<System.Object,System.Object>::GetFirst()
extern "C"  Il2CppObject * Pair_2_GetFirst_m3897815769_gshared (Pair_2_t1259711689 * __this, const MethodInfo* method);
#define Pair_2_GetFirst_m3897815769(__this, method) ((  Il2CppObject * (*) (Pair_2_t1259711689 *, const MethodInfo*))Pair_2_GetFirst_m3897815769_gshared)(__this, method)
// TU Firebase.Database.Internal.Utilities.Pair`2<System.Object,System.Object>::GetSecond()
extern "C"  Il2CppObject * Pair_2_GetSecond_m1675024216_gshared (Pair_2_t1259711689 * __this, const MethodInfo* method);
#define Pair_2_GetSecond_m1675024216(__this, method) ((  Il2CppObject * (*) (Pair_2_t1259711689 *, const MethodInfo*))Pair_2_GetSecond_m1675024216_gshared)(__this, method)
// System.Boolean Firebase.Database.Internal.Utilities.Pair`2<System.Object,System.Object>::Equals(System.Object)
extern "C"  bool Pair_2_Equals_m45448119_gshared (Pair_2_t1259711689 * __this, Il2CppObject * ___o0, const MethodInfo* method);
#define Pair_2_Equals_m45448119(__this, ___o0, method) ((  bool (*) (Pair_2_t1259711689 *, Il2CppObject *, const MethodInfo*))Pair_2_Equals_m45448119_gshared)(__this, ___o0, method)
// System.Int32 Firebase.Database.Internal.Utilities.Pair`2<System.Object,System.Object>::GetHashCode()
extern "C"  int32_t Pair_2_GetHashCode_m2815204653_gshared (Pair_2_t1259711689 * __this, const MethodInfo* method);
#define Pair_2_GetHashCode_m2815204653(__this, method) ((  int32_t (*) (Pair_2_t1259711689 *, const MethodInfo*))Pair_2_GetHashCode_m2815204653_gshared)(__this, method)
// System.String Firebase.Database.Internal.Utilities.Pair`2<System.Object,System.Object>::ToString()
extern "C"  String_t* Pair_2_ToString_m2900895833_gshared (Pair_2_t1259711689 * __this, const MethodInfo* method);
#define Pair_2_ToString_m2900895833(__this, method) ((  String_t* (*) (Pair_2_t1259711689 *, const MethodInfo*))Pair_2_ToString_m2900895833_gshared)(__this, method)
