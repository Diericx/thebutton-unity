﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Utili1259711689MethodDeclarations.h"

// System.Void Firebase.Database.Internal.Utilities.Pair`2<System.Threading.Tasks.Task`1<System.Object>,Firebase.Database.DatabaseReference/CompletionListener>::.ctor(T,TU)
#define Pair_2__ctor_m1800988919(__this, ___first0, ___second1, method) ((  void (*) (Pair_2_t2408862168 *, Task_1_t1809478302 *, CompletionListener_t93014473 *, const MethodInfo*))Pair_2__ctor_m1137640203_gshared)(__this, ___first0, ___second1, method)
// T Firebase.Database.Internal.Utilities.Pair`2<System.Threading.Tasks.Task`1<System.Object>,Firebase.Database.DatabaseReference/CompletionListener>::GetFirst()
#define Pair_2_GetFirst_m1833292153(__this, method) ((  Task_1_t1809478302 * (*) (Pair_2_t2408862168 *, const MethodInfo*))Pair_2_GetFirst_m3897815769_gshared)(__this, method)
// TU Firebase.Database.Internal.Utilities.Pair`2<System.Threading.Tasks.Task`1<System.Object>,Firebase.Database.DatabaseReference/CompletionListener>::GetSecond()
#define Pair_2_GetSecond_m1786699082(__this, method) ((  CompletionListener_t93014473 * (*) (Pair_2_t2408862168 *, const MethodInfo*))Pair_2_GetSecond_m1675024216_gshared)(__this, method)
// System.Boolean Firebase.Database.Internal.Utilities.Pair`2<System.Threading.Tasks.Task`1<System.Object>,Firebase.Database.DatabaseReference/CompletionListener>::Equals(System.Object)
#define Pair_2_Equals_m1989313707(__this, ___o0, method) ((  bool (*) (Pair_2_t2408862168 *, Il2CppObject *, const MethodInfo*))Pair_2_Equals_m45448119_gshared)(__this, ___o0, method)
// System.Int32 Firebase.Database.Internal.Utilities.Pair`2<System.Threading.Tasks.Task`1<System.Object>,Firebase.Database.DatabaseReference/CompletionListener>::GetHashCode()
#define Pair_2_GetHashCode_m434212341(__this, method) ((  int32_t (*) (Pair_2_t2408862168 *, const MethodInfo*))Pair_2_GetHashCode_m2815204653_gshared)(__this, method)
// System.String Firebase.Database.Internal.Utilities.Pair`2<System.Threading.Tasks.Task`1<System.Object>,Firebase.Database.DatabaseReference/CompletionListener>::ToString()
#define Pair_2_ToString_m485046281(__this, method) ((  String_t* (*) (Pair_2_t2408862168 *, const MethodInfo*))Pair_2_ToString_m2900895833_gshared)(__this, method)
