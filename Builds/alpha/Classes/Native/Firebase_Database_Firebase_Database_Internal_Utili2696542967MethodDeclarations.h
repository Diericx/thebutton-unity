﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Utilities.DefaultClock
struct DefaultClock_t2696542967;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Internal.Utilities.DefaultClock::.ctor()
extern "C"  void DefaultClock__ctor_m276465299 (DefaultClock_t2696542967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Firebase.Database.Internal.Utilities.DefaultClock::Millis()
extern "C"  int64_t DefaultClock_Millis_m4218847848 (DefaultClock_t2696542967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
