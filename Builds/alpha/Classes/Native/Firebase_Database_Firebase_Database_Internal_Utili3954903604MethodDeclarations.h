﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Utilities.DefaultRunLoop/ScheduledThreadPoolExecutor42
struct ScheduledThreadPoolExecutor42_t3954903604;
// Google.Sharpen.ThreadFactory
struct ThreadFactory_t1392637388;

#include "codegen/il2cpp-codegen.h"
#include "Google_Sharpen_Google_Sharpen_ThreadFactory1392637388.h"

// System.Void Firebase.Database.Internal.Utilities.DefaultRunLoop/ScheduledThreadPoolExecutor42::.ctor(System.Int32,Google.Sharpen.ThreadFactory)
extern "C"  void ScheduledThreadPoolExecutor42__ctor_m4194695398 (ScheduledThreadPoolExecutor42_t3954903604 * __this, int32_t ___baseArg10, ThreadFactory_t1392637388 * ___baseArg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
