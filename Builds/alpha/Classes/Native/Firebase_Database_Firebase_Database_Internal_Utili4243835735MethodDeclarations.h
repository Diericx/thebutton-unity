﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Utilities.DefaultRunLoop/FirebaseThreadFactory/ExceptionCatcher
struct ExceptionCatcher_t4243835735;
// Firebase.Database.Internal.Utilities.DefaultRunLoop/FirebaseThreadFactory
struct FirebaseThreadFactory_t699088793;
// Google.Sharpen.Runnable
struct Runnable_t1446984663;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Utilit699088793.h"

// System.Void Firebase.Database.Internal.Utilities.DefaultRunLoop/FirebaseThreadFactory/ExceptionCatcher::.ctor(Firebase.Database.Internal.Utilities.DefaultRunLoop/FirebaseThreadFactory,Google.Sharpen.Runnable)
extern "C"  void ExceptionCatcher__ctor_m413455174 (ExceptionCatcher_t4243835735 * __this, FirebaseThreadFactory_t699088793 * ___enclosing0, Il2CppObject * ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Utilities.DefaultRunLoop/FirebaseThreadFactory/ExceptionCatcher::Run()
extern "C"  void ExceptionCatcher_Run_m380011129 (ExceptionCatcher_t4243835735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
