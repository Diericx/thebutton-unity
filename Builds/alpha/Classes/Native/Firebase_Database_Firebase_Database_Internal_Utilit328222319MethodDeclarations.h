﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Internal.Utilities.Utilities/<WrapOnComplete>c__AnonStorey0
struct U3CWrapOnCompleteU3Ec__AnonStorey0_t328222319;
// Firebase.Database.DatabaseError
struct DatabaseError_t1067746743;
// Firebase.Database.DatabaseReference
struct DatabaseReference_t1167676104;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_DatabaseError1067746743.h"
#include "Firebase_Database_Firebase_Database_DatabaseRefere1167676104.h"

// System.Void Firebase.Database.Internal.Utilities.Utilities/<WrapOnComplete>c__AnonStorey0::.ctor()
extern "C"  void U3CWrapOnCompleteU3Ec__AnonStorey0__ctor_m569461262 (U3CWrapOnCompleteU3Ec__AnonStorey0_t328222319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Internal.Utilities.Utilities/<WrapOnComplete>c__AnonStorey0::<>m__0(Firebase.Database.DatabaseError,Firebase.Database.DatabaseReference)
extern "C"  void U3CWrapOnCompleteU3Ec__AnonStorey0_U3CU3Em__0_m2603106156 (U3CWrapOnCompleteU3Ec__AnonStorey0_t328222319 * __this, DatabaseError_t1067746743 * ___error0, DatabaseReference_t1167676104 * ___ref1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
