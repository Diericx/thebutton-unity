﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Utili1259711689MethodDeclarations.h"

// System.Void Firebase.Database.Internal.Utilities.Pair`2<System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.QuerySpec>,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.Event>>::.ctor(T,TU)
#define Pair_2__ctor_m1505600488(__this, ___first0, ___second1, method) ((  void (*) (Pair_2_t25474928 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Pair_2__ctor_m1137640203_gshared)(__this, ___first0, ___second1, method)
// T Firebase.Database.Internal.Utilities.Pair`2<System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.QuerySpec>,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.Event>>::GetFirst()
#define Pair_2_GetFirst_m1319713158(__this, method) ((  Il2CppObject* (*) (Pair_2_t25474928 *, const MethodInfo*))Pair_2_GetFirst_m3897815769_gshared)(__this, method)
// TU Firebase.Database.Internal.Utilities.Pair`2<System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.QuerySpec>,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.Event>>::GetSecond()
#define Pair_2_GetSecond_m2628354125(__this, method) ((  Il2CppObject* (*) (Pair_2_t25474928 *, const MethodInfo*))Pair_2_GetSecond_m1675024216_gshared)(__this, method)
// System.Boolean Firebase.Database.Internal.Utilities.Pair`2<System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.QuerySpec>,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.Event>>::Equals(System.Object)
#define Pair_2_Equals_m50267006(__this, ___o0, method) ((  bool (*) (Pair_2_t25474928 *, Il2CppObject *, const MethodInfo*))Pair_2_Equals_m45448119_gshared)(__this, ___o0, method)
// System.Int32 Firebase.Database.Internal.Utilities.Pair`2<System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.QuerySpec>,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.Event>>::GetHashCode()
#define Pair_2_GetHashCode_m1801508982(__this, method) ((  int32_t (*) (Pair_2_t25474928 *, const MethodInfo*))Pair_2_GetHashCode_m2815204653_gshared)(__this, method)
// System.String Firebase.Database.Internal.Utilities.Pair`2<System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.QuerySpec>,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.View.Event>>::ToString()
#define Pair_2_ToString_m498106812(__this, method) ((  String_t* (*) (Pair_2_t25474928 *, const MethodInfo*))Pair_2_ToString_m2900895833_gshared)(__this, method)
