﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Logger
struct Logger_t225270238;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Logger::.ctor()
extern "C"  void Logger__ctor_m3319557657 (Logger_t225270238 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
