﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Query/Runnable182
struct Runnable182_t1164582213;
// Firebase.Database.Query
struct Query_t2792659010;
// Firebase.Database.Internal.Core.EventRegistration
struct EventRegistration_t4222917807;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Query2792659010.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_4222917807.h"

// System.Void Firebase.Database.Query/Runnable182::.ctor(Firebase.Database.Query,Firebase.Database.Internal.Core.EventRegistration)
extern "C"  void Runnable182__ctor_m1545244145 (Runnable182_t1164582213 * __this, Query_t2792659010 * ___enclosing0, EventRegistration_t4222917807 * ___registration1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Query/Runnable182::Run()
extern "C"  void Runnable182_Run_m2630489479 (Runnable182_t1164582213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
