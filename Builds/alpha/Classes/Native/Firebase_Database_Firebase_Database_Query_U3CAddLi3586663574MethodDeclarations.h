﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Query/<AddListenerForSingleValueEvent>c__AnonStorey1
struct U3CAddListenerForSingleValueEventU3Ec__AnonStorey1_t3586663574;
// System.Object
struct Il2CppObject;
// Firebase.Database.ValueChangedEventArgs
struct ValueChangedEventArgs_t929877234;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "Firebase_Database_Firebase_Database_ValueChangedEve929877234.h"

// System.Void Firebase.Database.Query/<AddListenerForSingleValueEvent>c__AnonStorey1::.ctor()
extern "C"  void U3CAddListenerForSingleValueEventU3Ec__AnonStorey1__ctor_m1855234649 (U3CAddListenerForSingleValueEventU3Ec__AnonStorey1_t3586663574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Query/<AddListenerForSingleValueEvent>c__AnonStorey1::<>m__0(System.Object,Firebase.Database.ValueChangedEventArgs)
extern "C"  void U3CAddListenerForSingleValueEventU3Ec__AnonStorey1_U3CU3Em__0_m2029154126 (U3CAddListenerForSingleValueEventU3Ec__AnonStorey1_t3586663574 * __this, Il2CppObject * ___o0, ValueChangedEventArgs_t929877234 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
