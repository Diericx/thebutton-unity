﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Query/<GetValueAsync>c__AnonStorey0
struct U3CGetValueAsyncU3Ec__AnonStorey0_t2125080077;
// System.Object
struct Il2CppObject;
// Firebase.Database.ValueChangedEventArgs
struct ValueChangedEventArgs_t929877234;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "Firebase_Database_Firebase_Database_ValueChangedEve929877234.h"

// System.Void Firebase.Database.Query/<GetValueAsync>c__AnonStorey0::.ctor()
extern "C"  void U3CGetValueAsyncU3Ec__AnonStorey0__ctor_m109643228 (U3CGetValueAsyncU3Ec__AnonStorey0_t2125080077 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Query/<GetValueAsync>c__AnonStorey0::<>m__0(System.Object,Firebase.Database.ValueChangedEventArgs)
extern "C"  void U3CGetValueAsyncU3Ec__AnonStorey0_U3CU3Em__0_m3530687369 (U3CGetValueAsyncU3Ec__AnonStorey0_t2125080077 * __this, Il2CppObject * ___o0, ValueChangedEventArgs_t929877234 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
