﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "Firebase_Database_U3CPrivateImplementationDetailsU3424485092.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{c72063f3-67cc-4a85-b2d6-47e59e04d581}
struct  U3CPrivateImplementationDetailsU3EU7Bc72063f3U2D67ccU2D4a85U2Db2d6U2D47e59e04d581U7D_t2517675689  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3EU7Bc72063f3U2D67ccU2D4a85U2Db2d6U2D47e59e04d581U7D_t2517675689_StaticFields
{
public:
	// <PrivateImplementationDetails>{c72063f3-67cc-4a85-b2d6-47e59e04d581}/$ArrayType=64 <PrivateImplementationDetails>{c72063f3-67cc-4a85-b2d6-47e59e04d581}::$field-2
	U24ArrayTypeU3D64_t3424485092  ___U24fieldU2D2_0;
	// <PrivateImplementationDetails>{c72063f3-67cc-4a85-b2d6-47e59e04d581}/$ArrayType=64 <PrivateImplementationDetails>{c72063f3-67cc-4a85-b2d6-47e59e04d581}::$field-3
	U24ArrayTypeU3D64_t3424485092  ___U24fieldU2D3_1;

public:
	inline static int32_t get_offset_of_U24fieldU2D2_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7Bc72063f3U2D67ccU2D4a85U2Db2d6U2D47e59e04d581U7D_t2517675689_StaticFields, ___U24fieldU2D2_0)); }
	inline U24ArrayTypeU3D64_t3424485092  get_U24fieldU2D2_0() const { return ___U24fieldU2D2_0; }
	inline U24ArrayTypeU3D64_t3424485092 * get_address_of_U24fieldU2D2_0() { return &___U24fieldU2D2_0; }
	inline void set_U24fieldU2D2_0(U24ArrayTypeU3D64_t3424485092  value)
	{
		___U24fieldU2D2_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3EU7Bc72063f3U2D67ccU2D4a85U2Db2d6U2D47e59e04d581U7D_t2517675689_StaticFields, ___U24fieldU2D3_1)); }
	inline U24ArrayTypeU3D64_t3424485092  get_U24fieldU2D3_1() const { return ___U24fieldU2D3_1; }
	inline U24ArrayTypeU3D64_t3424485092 * get_address_of_U24fieldU2D3_1() { return &___U24fieldU2D3_1; }
	inline void set_U24fieldU2D3_1(U24ArrayTypeU3D64_t3424485092  value)
	{
		___U24fieldU2D3_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
