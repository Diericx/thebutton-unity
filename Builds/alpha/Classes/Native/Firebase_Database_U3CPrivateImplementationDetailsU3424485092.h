﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>{c72063f3-67cc-4a85-b2d6-47e59e04d581}/$ArrayType=64
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D64_t3424485092 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D64_t3424485092__padding[64];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
