﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Unity.PreserveAttribute
struct PreserveAttribute_t2023989938;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Unity.PreserveAttribute::.ctor()
extern "C"  void PreserveAttribute__ctor_m94944701 (PreserveAttribute_t2023989938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
