﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Net.WebClient
struct WebClient_t1432723993;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t3047564564;
// Firebase.Database.Unity.ServiceAccountCredential/OAuthRequest
struct OAuthRequest_t3497136277;
// Firebase.Database.Unity.ServiceAccountCredential
struct ServiceAccountCredential_t3398163339;
// System.Object
struct Il2CppObject;
// Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0/<SendOAuth>c__AnonStorey1
struct U3CSendOAuthU3Ec__AnonStorey1_t520895791;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0
struct  U3CSendOAuthU3Ec__Iterator0_t1559836771  : public Il2CppObject
{
public:
	// System.Net.WebClient Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::<client>__0
	WebClient_t1432723993 * ___U3CclientU3E__0_0;
	// System.Collections.Specialized.NameValueCollection Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::<reqparm>__1
	NameValueCollection_t3047564564 * ___U3CreqparmU3E__1_1;
	// Firebase.Database.Unity.ServiceAccountCredential/OAuthRequest Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::request
	OAuthRequest_t3497136277 * ___request_2;
	// Firebase.Database.Unity.ServiceAccountCredential Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::$this
	ServiceAccountCredential_t3398163339 * ___U24this_3;
	// System.Object Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::$PC
	int32_t ___U24PC_6;
	// Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0/<SendOAuth>c__AnonStorey1 Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::$locvar0
	U3CSendOAuthU3Ec__AnonStorey1_t520895791 * ___U24locvar0_7;

public:
	inline static int32_t get_offset_of_U3CclientU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSendOAuthU3Ec__Iterator0_t1559836771, ___U3CclientU3E__0_0)); }
	inline WebClient_t1432723993 * get_U3CclientU3E__0_0() const { return ___U3CclientU3E__0_0; }
	inline WebClient_t1432723993 ** get_address_of_U3CclientU3E__0_0() { return &___U3CclientU3E__0_0; }
	inline void set_U3CclientU3E__0_0(WebClient_t1432723993 * value)
	{
		___U3CclientU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CclientU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CreqparmU3E__1_1() { return static_cast<int32_t>(offsetof(U3CSendOAuthU3Ec__Iterator0_t1559836771, ___U3CreqparmU3E__1_1)); }
	inline NameValueCollection_t3047564564 * get_U3CreqparmU3E__1_1() const { return ___U3CreqparmU3E__1_1; }
	inline NameValueCollection_t3047564564 ** get_address_of_U3CreqparmU3E__1_1() { return &___U3CreqparmU3E__1_1; }
	inline void set_U3CreqparmU3E__1_1(NameValueCollection_t3047564564 * value)
	{
		___U3CreqparmU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CreqparmU3E__1_1, value);
	}

	inline static int32_t get_offset_of_request_2() { return static_cast<int32_t>(offsetof(U3CSendOAuthU3Ec__Iterator0_t1559836771, ___request_2)); }
	inline OAuthRequest_t3497136277 * get_request_2() const { return ___request_2; }
	inline OAuthRequest_t3497136277 ** get_address_of_request_2() { return &___request_2; }
	inline void set_request_2(OAuthRequest_t3497136277 * value)
	{
		___request_2 = value;
		Il2CppCodeGenWriteBarrier(&___request_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CSendOAuthU3Ec__Iterator0_t1559836771, ___U24this_3)); }
	inline ServiceAccountCredential_t3398163339 * get_U24this_3() const { return ___U24this_3; }
	inline ServiceAccountCredential_t3398163339 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ServiceAccountCredential_t3398163339 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CSendOAuthU3Ec__Iterator0_t1559836771, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CSendOAuthU3Ec__Iterator0_t1559836771, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CSendOAuthU3Ec__Iterator0_t1559836771, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_7() { return static_cast<int32_t>(offsetof(U3CSendOAuthU3Ec__Iterator0_t1559836771, ___U24locvar0_7)); }
	inline U3CSendOAuthU3Ec__AnonStorey1_t520895791 * get_U24locvar0_7() const { return ___U24locvar0_7; }
	inline U3CSendOAuthU3Ec__AnonStorey1_t520895791 ** get_address_of_U24locvar0_7() { return &___U24locvar0_7; }
	inline void set_U24locvar0_7(U3CSendOAuthU3Ec__AnonStorey1_t520895791 * value)
	{
		___U24locvar0_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
