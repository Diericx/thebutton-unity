﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0
struct U3CSendOAuthU3Ec__Iterator0_t1559836771;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::.ctor()
extern "C"  void U3CSendOAuthU3Ec__Iterator0__ctor_m1996092573 (U3CSendOAuthU3Ec__Iterator0_t1559836771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::MoveNext()
extern "C"  bool U3CSendOAuthU3Ec__Iterator0_MoveNext_m4174874303 (U3CSendOAuthU3Ec__Iterator0_t1559836771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendOAuthU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2896288715 (U3CSendOAuthU3Ec__Iterator0_t1559836771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendOAuthU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1171917123 (U3CSendOAuthU3Ec__Iterator0_t1559836771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::Dispose()
extern "C"  void U3CSendOAuthU3Ec__Iterator0_Dispose_m1858233370 (U3CSendOAuthU3Ec__Iterator0_t1559836771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::Reset()
extern "C"  void U3CSendOAuthU3Ec__Iterator0_Reset_m1165723380 (U3CSendOAuthU3Ec__Iterator0_t1559836771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0::<>__Finally0()
extern "C"  void U3CSendOAuthU3Ec__Iterator0_U3CU3E__Finally0_m788646374 (U3CSendOAuthU3Ec__Iterator0_t1559836771 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
