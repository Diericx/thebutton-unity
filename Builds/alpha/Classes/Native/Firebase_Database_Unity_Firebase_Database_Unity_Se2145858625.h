﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Net.UploadValuesCompletedEventArgs
struct UploadValuesCompletedEventArgs_t3564452537;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Database.Unity.ServiceAccountCredential/UploadCompleted
struct  UploadCompleted_t2145858625  : public Il2CppObject
{
public:
	// System.Net.UploadValuesCompletedEventArgs Firebase.Database.Unity.ServiceAccountCredential/UploadCompleted::args
	UploadValuesCompletedEventArgs_t3564452537 * ___args_0;

public:
	inline static int32_t get_offset_of_args_0() { return static_cast<int32_t>(offsetof(UploadCompleted_t2145858625, ___args_0)); }
	inline UploadValuesCompletedEventArgs_t3564452537 * get_args_0() const { return ___args_0; }
	inline UploadValuesCompletedEventArgs_t3564452537 ** get_address_of_args_0() { return &___args_0; }
	inline void set_args_0(UploadValuesCompletedEventArgs_t3564452537 * value)
	{
		___args_0 = value;
		Il2CppCodeGenWriteBarrier(&___args_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
