﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Unity.ServiceAccountCredential/UploadCompleted
struct UploadCompleted_t2145858625;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Unity.ServiceAccountCredential/UploadCompleted::.ctor()
extern "C"  void UploadCompleted__ctor_m182388035 (UploadCompleted_t2145858625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
