﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Unity.ServiceCredential/Initializer
struct Initializer_t2915825156;
// System.String
struct String_t;
// Firebase.Database.Unity.IClock
struct IClock_t1761322331;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.Database.Unity.ServiceCredential/Initializer::.ctor(System.String)
extern "C"  void Initializer__ctor_m2870913516 (Initializer_t2915825156 * __this, String_t* ___tokenServerUrl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Database.Unity.ServiceCredential/Initializer::get_TokenServerUrl()
extern "C"  String_t* Initializer_get_TokenServerUrl_m1876759603 (Initializer_t2915825156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Unity.ServiceCredential/Initializer::set_TokenServerUrl(System.String)
extern "C"  void Initializer_set_TokenServerUrl_m210258324 (Initializer_t2915825156 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Firebase.Database.Unity.IClock Firebase.Database.Unity.ServiceCredential/Initializer::get_Clock()
extern "C"  Il2CppObject * Initializer_get_Clock_m3276295110 (Initializer_t2915825156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Unity.ServiceCredential/Initializer::set_Clock(Firebase.Database.Unity.IClock)
extern "C"  void Initializer_set_Clock_m1598159275 (Initializer_t2915825156 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
