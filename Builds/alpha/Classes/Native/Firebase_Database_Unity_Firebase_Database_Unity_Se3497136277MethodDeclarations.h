﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Unity.ServiceAccountCredential/OAuthRequest
struct OAuthRequest_t3497136277;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Unity.ServiceAccountCredential/OAuthRequest::.ctor()
extern "C"  void OAuthRequest__ctor_m398011043 (OAuthRequest_t3497136277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
