﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Unity.ServiceAccountCredential/<GetAccessTokenForRequestSync>c__AnonStorey2
struct U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_t3624071070;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Unity.ServiceAccountCredential/<GetAccessTokenForRequestSync>c__AnonStorey2::.ctor()
extern "C"  void U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2__ctor_m3845533516 (U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_t3624071070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Firebase.Database.Unity.ServiceAccountCredential/<GetAccessTokenForRequestSync>c__AnonStorey2::<>m__0()
extern "C"  Il2CppObject * U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_U3CU3Em__0_m991289755 (U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_t3624071070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
