﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Firebase.Database.Unity.ServiceAccountCredential/UploadCompleted
struct UploadCompleted_t2145858625;
// Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0
struct U3CSendOAuthU3Ec__Iterator0_t1559836771;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0/<SendOAuth>c__AnonStorey1
struct  U3CSendOAuthU3Ec__AnonStorey1_t520895791  : public Il2CppObject
{
public:
	// Firebase.Database.Unity.ServiceAccountCredential/UploadCompleted Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0/<SendOAuth>c__AnonStorey1::uploadCompleted
	UploadCompleted_t2145858625 * ___uploadCompleted_0;
	// Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0 Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0/<SendOAuth>c__AnonStorey1::<>f__ref$0
	U3CSendOAuthU3Ec__Iterator0_t1559836771 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_uploadCompleted_0() { return static_cast<int32_t>(offsetof(U3CSendOAuthU3Ec__AnonStorey1_t520895791, ___uploadCompleted_0)); }
	inline UploadCompleted_t2145858625 * get_uploadCompleted_0() const { return ___uploadCompleted_0; }
	inline UploadCompleted_t2145858625 ** get_address_of_uploadCompleted_0() { return &___uploadCompleted_0; }
	inline void set_uploadCompleted_0(UploadCompleted_t2145858625 * value)
	{
		___uploadCompleted_0 = value;
		Il2CppCodeGenWriteBarrier(&___uploadCompleted_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CSendOAuthU3Ec__AnonStorey1_t520895791, ___U3CU3Ef__refU240_1)); }
	inline U3CSendOAuthU3Ec__Iterator0_t1559836771 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CSendOAuthU3Ec__Iterator0_t1559836771 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CSendOAuthU3Ec__Iterator0_t1559836771 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU240_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
