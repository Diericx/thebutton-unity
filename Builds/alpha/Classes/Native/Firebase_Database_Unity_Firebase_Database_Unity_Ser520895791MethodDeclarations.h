﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0/<SendOAuth>c__AnonStorey1
struct U3CSendOAuthU3Ec__AnonStorey1_t520895791;
// System.Object
struct Il2CppObject;
// System.Net.UploadValuesCompletedEventArgs
struct UploadValuesCompletedEventArgs_t3564452537;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_System_Net_UploadValuesCompletedEventArgs3564452537.h"

// System.Void Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0/<SendOAuth>c__AnonStorey1::.ctor()
extern "C"  void U3CSendOAuthU3Ec__AnonStorey1__ctor_m1929189873 (U3CSendOAuthU3Ec__AnonStorey1_t520895791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Unity.ServiceAccountCredential/<SendOAuth>c__Iterator0/<SendOAuth>c__AnonStorey1::<>m__0(System.Object,System.Net.UploadValuesCompletedEventArgs)
extern "C"  void U3CSendOAuthU3Ec__AnonStorey1_U3CU3Em__0_m3378572595 (U3CSendOAuthU3Ec__AnonStorey1_t520895791 * __this, Il2CppObject * ___sender0, UploadValuesCompletedEventArgs_t3564452537 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
