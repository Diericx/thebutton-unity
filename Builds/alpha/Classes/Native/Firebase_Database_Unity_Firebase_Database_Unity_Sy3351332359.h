﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Firebase.Database.Unity.IClock
struct IClock_t1761322331;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Database.Unity.SystemClock
struct  SystemClock_t3351332359  : public Il2CppObject
{
public:

public:
};

struct SystemClock_t3351332359_StaticFields
{
public:
	// Firebase.Database.Unity.IClock Firebase.Database.Unity.SystemClock::Default
	Il2CppObject * ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(SystemClock_t3351332359_StaticFields, ___Default_0)); }
	inline Il2CppObject * get_Default_0() const { return ___Default_0; }
	inline Il2CppObject ** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(Il2CppObject * value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier(&___Default_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
