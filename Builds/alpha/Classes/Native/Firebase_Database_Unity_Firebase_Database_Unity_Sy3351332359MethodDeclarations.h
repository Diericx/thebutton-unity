﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Unity.SystemClock
struct SystemClock_t3351332359;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Void Firebase.Database.Unity.SystemClock::.ctor()
extern "C"  void SystemClock__ctor_m2043626562 (SystemClock_t3351332359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Firebase.Database.Unity.SystemClock::get_Now()
extern "C"  DateTime_t693205669  SystemClock_get_Now_m797233368 (SystemClock_t3351332359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Firebase.Database.Unity.SystemClock::get_UtcNow()
extern "C"  DateTime_t693205669  SystemClock_get_UtcNow_m3188797936 (SystemClock_t3351332359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Unity.SystemClock::.cctor()
extern "C"  void SystemClock__cctor_m529698727 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
