﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "Firebase_Database_Firebase_Database_Internal_Core_3824902566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Database.Unity.TreeNodeIListTransactionData
struct  TreeNodeIListTransactionData_t390449921  : public TreeNode_1_t3824902566
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
