﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Unity.TreeNodeIListTransactionData
struct TreeNodeIListTransactionData_t390449921;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Unity.TreeNodeIListTransactionData::.ctor()
extern "C"  void TreeNodeIListTransactionData__ctor_m3485790242 (TreeNodeIListTransactionData_t390449921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
