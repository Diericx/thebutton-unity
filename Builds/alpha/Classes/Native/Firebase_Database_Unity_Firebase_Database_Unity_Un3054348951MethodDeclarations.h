﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Unity.UnityAuthTokenProvider/<AddTokenChangeListener>c__AnonStorey1
struct U3CAddTokenChangeListenerU3Ec__AnonStorey1_t3054348951;
// System.Object
struct Il2CppObject;
// System.EventArgs
struct EventArgs_t3289624707;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_EventArgs3289624707.h"

// System.Void Firebase.Database.Unity.UnityAuthTokenProvider/<AddTokenChangeListener>c__AnonStorey1::.ctor()
extern "C"  void U3CAddTokenChangeListenerU3Ec__AnonStorey1__ctor_m2451425437 (U3CAddTokenChangeListenerU3Ec__AnonStorey1_t3054348951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Firebase.Database.Unity.UnityAuthTokenProvider/<AddTokenChangeListener>c__AnonStorey1::<>m__0(System.Object,System.EventArgs)
extern "C"  void U3CAddTokenChangeListenerU3Ec__AnonStorey1_U3CU3Em__0_m364880740 (U3CAddTokenChangeListenerU3Ec__AnonStorey1_t3054348951 * __this, Il2CppObject * ___sender0, EventArgs_t3289624707 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
