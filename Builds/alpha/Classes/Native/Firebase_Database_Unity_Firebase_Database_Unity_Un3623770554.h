﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<Firebase.FirebaseApp,Firebase.Database.DotNet.DotNetPlatform>
struct Func_2_t3961743794;

#include "Firebase_Database_Firebase_Database_DotNet_DotNetP2951135975.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Firebase.Database.Unity.UnityPlatform
struct  UnityPlatform_t3623770554  : public DotNetPlatform_t2951135975
{
public:

public:
};

struct UnityPlatform_t3623770554_StaticFields
{
public:
	// System.Func`2<Firebase.FirebaseApp,Firebase.Database.DotNet.DotNetPlatform> Firebase.Database.Unity.UnityPlatform::<>f__am$cache0
	Func_2_t3961743794 * ___U3CU3Ef__amU24cache0_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(UnityPlatform_t3623770554_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Func_2_t3961743794 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Func_2_t3961743794 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Func_2_t3961743794 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
