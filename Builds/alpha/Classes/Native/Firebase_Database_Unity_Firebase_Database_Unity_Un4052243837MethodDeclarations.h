﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.Database.Unity.UnityAuthTokenProvider/<GetToken>c__AnonStorey0
struct U3CGetTokenU3Ec__AnonStorey0_t4052243837;
// System.String
struct String_t;
// System.Threading.Tasks.Task`1<System.String>
struct Task_1_t1149249240;

#include "codegen/il2cpp-codegen.h"

// System.Void Firebase.Database.Unity.UnityAuthTokenProvider/<GetToken>c__AnonStorey0::.ctor()
extern "C"  void U3CGetTokenU3Ec__AnonStorey0__ctor_m1307976171 (U3CGetTokenU3Ec__AnonStorey0_t4052243837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Database.Unity.UnityAuthTokenProvider/<GetToken>c__AnonStorey0::<>m__0(System.Threading.Tasks.Task`1<System.String>)
extern "C"  String_t* U3CGetTokenU3Ec__AnonStorey0_U3CU3Em__0_m1504785580 (U3CGetTokenU3Ec__AnonStorey0_t4052243837 * __this, Task_1_t1149249240 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
