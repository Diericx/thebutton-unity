﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Firebase.FirebaseApp
struct FirebaseApp_t210707726;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "Firebase_App_Firebase_FirebaseApp210707726.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Firebase.Unity.Editor.FirebaseEditorExtensions::SetEditorDatabaseUrl(Firebase.FirebaseApp,System.String)
extern "C"  void FirebaseEditorExtensions_SetEditorDatabaseUrl_m3984445699 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, String_t* ___databaseUrl1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Unity.Editor.FirebaseEditorExtensions::GetEditorP12Password(Firebase.FirebaseApp)
extern "C"  String_t* FirebaseEditorExtensions_GetEditorP12Password_m3642378088 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Unity.Editor.FirebaseEditorExtensions::GetEditorP12FileName(Firebase.FirebaseApp)
extern "C"  String_t* FirebaseEditorExtensions_GetEditorP12FileName_m1066698760 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Unity.Editor.FirebaseEditorExtensions::GetEditorServiceAccountEmail(Firebase.FirebaseApp)
extern "C"  String_t* FirebaseEditorExtensions_GetEditorServiceAccountEmail_m2686895772 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Firebase.Unity.Editor.FirebaseEditorExtensions::GetEditorAuthUserId(Firebase.FirebaseApp)
extern "C"  String_t* FirebaseEditorExtensions_GetEditorAuthUserId_m1692323114 (Il2CppObject * __this /* static, unused */, FirebaseApp_t210707726 * ___app0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
