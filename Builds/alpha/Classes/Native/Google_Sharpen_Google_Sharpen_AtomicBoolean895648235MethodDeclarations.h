﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Google.Sharpen.AtomicBoolean
struct AtomicBoolean_t895648235;

#include "codegen/il2cpp-codegen.h"

// System.Void Google.Sharpen.AtomicBoolean::.ctor(System.Boolean)
extern "C"  void AtomicBoolean__ctor_m534715259 (AtomicBoolean_t895648235 * __this, bool ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Google.Sharpen.AtomicBoolean::Get()
extern "C"  bool AtomicBoolean_Get_m1348647586 (AtomicBoolean_t895648235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Google.Sharpen.AtomicBoolean::CompareAndSet(System.Boolean,System.Boolean)
extern "C"  bool AtomicBoolean_CompareAndSet_m1610432222 (AtomicBoolean_t895648235 * __this, bool ___v10, bool ___v21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
