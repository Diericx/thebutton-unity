﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Google.Sharpen.AtomicInteger
struct AtomicInteger_t4174655693;

#include "codegen/il2cpp-codegen.h"

// System.Void Google.Sharpen.AtomicInteger::.ctor(System.Int32)
extern "C"  void AtomicInteger__ctor_m1335232787 (AtomicInteger_t4174655693 * __this, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Google.Sharpen.AtomicInteger::IncrementAndGet()
extern "C"  int32_t AtomicInteger_IncrementAndGet_m2936054554 (AtomicInteger_t4174655693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
