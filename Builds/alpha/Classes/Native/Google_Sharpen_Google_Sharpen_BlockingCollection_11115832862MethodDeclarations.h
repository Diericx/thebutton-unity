﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Google_Sharpen_Google_Sharpen_BlockingCollection_13061287978MethodDeclarations.h"

// System.Void Google.Sharpen.BlockingCollection`1<System.IO.MemoryStream>::.ctor()
#define BlockingCollection_1__ctor_m2257943285(__this, method) ((  void (*) (BlockingCollection_1_t1115832862 *, const MethodInfo*))BlockingCollection_1__ctor_m225173119_gshared)(__this, method)
// System.Int32 Google.Sharpen.BlockingCollection`1<System.IO.MemoryStream>::get_Count()
#define BlockingCollection_1_get_Count_m1767940729(__this, method) ((  int32_t (*) (BlockingCollection_1_t1115832862 *, const MethodInfo*))BlockingCollection_1_get_Count_m472184031_gshared)(__this, method)
// System.Void Google.Sharpen.BlockingCollection`1<System.IO.MemoryStream>::Dispose()
#define BlockingCollection_1_Dispose_m3916934944(__this, method) ((  void (*) (BlockingCollection_1_t1115832862 *, const MethodInfo*))BlockingCollection_1_Dispose_m1813179332_gshared)(__this, method)
// System.Void Google.Sharpen.BlockingCollection`1<System.IO.MemoryStream>::Add(T)
#define BlockingCollection_1_Add_m392549001(__this, ___item0, method) ((  void (*) (BlockingCollection_1_t1115832862 *, MemoryStream_t743994179 *, const MethodInfo*))BlockingCollection_1_Add_m2390167760_gshared)(__this, ___item0, method)
// T Google.Sharpen.BlockingCollection`1<System.IO.MemoryStream>::Take()
#define BlockingCollection_1_Take_m66727740(__this, method) ((  MemoryStream_t743994179 * (*) (BlockingCollection_1_t1115832862 *, const MethodInfo*))BlockingCollection_1_Take_m3350786155_gshared)(__this, method)
