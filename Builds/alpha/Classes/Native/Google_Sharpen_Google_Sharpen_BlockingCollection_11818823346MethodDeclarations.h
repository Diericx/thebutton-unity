﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Google_Sharpen_Google_Sharpen_BlockingCollection_13061287978MethodDeclarations.h"

// System.Void Google.Sharpen.BlockingCollection`1<Google.Sharpen.Runnable>::.ctor()
#define BlockingCollection_1__ctor_m944931090(__this, method) ((  void (*) (BlockingCollection_1_t1818823346 *, const MethodInfo*))BlockingCollection_1__ctor_m225173119_gshared)(__this, method)
// System.Int32 Google.Sharpen.BlockingCollection`1<Google.Sharpen.Runnable>::get_Count()
#define BlockingCollection_1_get_Count_m2408182408(__this, method) ((  int32_t (*) (BlockingCollection_1_t1818823346 *, const MethodInfo*))BlockingCollection_1_get_Count_m472184031_gshared)(__this, method)
// System.Void Google.Sharpen.BlockingCollection`1<Google.Sharpen.Runnable>::Dispose()
#define BlockingCollection_1_Dispose_m2655011373(__this, method) ((  void (*) (BlockingCollection_1_t1818823346 *, const MethodInfo*))BlockingCollection_1_Dispose_m1813179332_gshared)(__this, method)
// System.Void Google.Sharpen.BlockingCollection`1<Google.Sharpen.Runnable>::Add(T)
#define BlockingCollection_1_Add_m793065891(__this, ___item0, method) ((  void (*) (BlockingCollection_1_t1818823346 *, Il2CppObject *, const MethodInfo*))BlockingCollection_1_Add_m2390167760_gshared)(__this, ___item0, method)
// T Google.Sharpen.BlockingCollection`1<Google.Sharpen.Runnable>::Take()
#define BlockingCollection_1_Take_m3805406950(__this, method) ((  Il2CppObject * (*) (BlockingCollection_1_t1818823346 *, const MethodInfo*))BlockingCollection_1_Take_m3350786155_gshared)(__this, method)
