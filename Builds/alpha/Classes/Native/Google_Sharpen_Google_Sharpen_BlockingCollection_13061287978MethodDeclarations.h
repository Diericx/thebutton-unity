﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Google.Sharpen.BlockingCollection`1<System.Object>
struct BlockingCollection_1_t3061287978;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void Google.Sharpen.BlockingCollection`1<System.Object>::.ctor()
extern "C"  void BlockingCollection_1__ctor_m225173119_gshared (BlockingCollection_1_t3061287978 * __this, const MethodInfo* method);
#define BlockingCollection_1__ctor_m225173119(__this, method) ((  void (*) (BlockingCollection_1_t3061287978 *, const MethodInfo*))BlockingCollection_1__ctor_m225173119_gshared)(__this, method)
// System.Int32 Google.Sharpen.BlockingCollection`1<System.Object>::get_Count()
extern "C"  int32_t BlockingCollection_1_get_Count_m472184031_gshared (BlockingCollection_1_t3061287978 * __this, const MethodInfo* method);
#define BlockingCollection_1_get_Count_m472184031(__this, method) ((  int32_t (*) (BlockingCollection_1_t3061287978 *, const MethodInfo*))BlockingCollection_1_get_Count_m472184031_gshared)(__this, method)
// System.Void Google.Sharpen.BlockingCollection`1<System.Object>::Dispose()
extern "C"  void BlockingCollection_1_Dispose_m1813179332_gshared (BlockingCollection_1_t3061287978 * __this, const MethodInfo* method);
#define BlockingCollection_1_Dispose_m1813179332(__this, method) ((  void (*) (BlockingCollection_1_t3061287978 *, const MethodInfo*))BlockingCollection_1_Dispose_m1813179332_gshared)(__this, method)
// System.Void Google.Sharpen.BlockingCollection`1<System.Object>::Add(T)
extern "C"  void BlockingCollection_1_Add_m2390167760_gshared (BlockingCollection_1_t3061287978 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define BlockingCollection_1_Add_m2390167760(__this, ___item0, method) ((  void (*) (BlockingCollection_1_t3061287978 *, Il2CppObject *, const MethodInfo*))BlockingCollection_1_Add_m2390167760_gshared)(__this, ___item0, method)
// T Google.Sharpen.BlockingCollection`1<System.Object>::Take()
extern "C"  Il2CppObject * BlockingCollection_1_Take_m3350786155_gshared (BlockingCollection_1_t3061287978 * __this, const MethodInfo* method);
#define BlockingCollection_1_Take_m3350786155(__this, method) ((  Il2CppObject * (*) (BlockingCollection_1_t3061287978 *, const MethodInfo*))BlockingCollection_1_Take_m3350786155_gshared)(__this, method)
