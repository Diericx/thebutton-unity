﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Google.Sharpen.CharacterCodingException
struct CharacterCodingException_t3867687652;

#include "codegen/il2cpp-codegen.h"

// System.Void Google.Sharpen.CharacterCodingException::.ctor()
extern "C"  void CharacterCodingException__ctor_m3849126603 (CharacterCodingException_t3867687652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
