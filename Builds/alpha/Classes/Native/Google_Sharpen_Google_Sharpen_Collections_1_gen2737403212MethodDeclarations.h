﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3230389896;

#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.IList`1<T> Google.Sharpen.Collections`1<System.Object>::get_EMPTY_SET()
extern "C"  Il2CppObject* Collections_1_get_EMPTY_SET_m4198404202_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Collections_1_get_EMPTY_SET_m4198404202(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Collections_1_get_EMPTY_SET_m4198404202_gshared)(__this /* static, unused */, method)
// System.Void Google.Sharpen.Collections`1<System.Object>::.cctor()
extern "C"  void Collections_1__cctor_m3636824690_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Collections_1__cctor_m3636824690(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Collections_1__cctor_m3636824690_gshared)(__this /* static, unused */, method)
