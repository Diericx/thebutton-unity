﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.IList`1<System.Int64>
struct IList_1_t1450018638;

#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.IList`1<T> Google.Sharpen.Collections`1<System.Int64>::get_EMPTY_SET()
extern "C"  Il2CppObject* Collections_1_get_EMPTY_SET_m1324405184_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Collections_1_get_EMPTY_SET_m1324405184(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Collections_1_get_EMPTY_SET_m1324405184_gshared)(__this /* static, unused */, method)
// System.Void Google.Sharpen.Collections`1<System.Int64>::.cctor()
extern "C"  void Collections_1__cctor_m3923826052_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Collections_1__cctor_m3923826052(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Collections_1__cctor_m3923826052_gshared)(__this /* static, unused */, method)
