﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Google.Sharpen.ThreadFactory
struct ThreadFactory_t1392637388;

#include "codegen/il2cpp-codegen.h"

// Google.Sharpen.ThreadFactory Google.Sharpen.Executors::DefaultThreadFactory()
extern "C"  ThreadFactory_t1392637388 * Executors_DefaultThreadFactory_m2872455255 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Google.Sharpen.Executors::.cctor()
extern "C"  void Executors__cctor_m3969193582 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
