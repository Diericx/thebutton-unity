﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Google.Sharpen.MessageDigest
struct MessageDigest_t1820469897;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Google.Sharpen.MessageDigest::.ctor()
extern "C"  void MessageDigest__ctor_m4036998384 (MessageDigest_t1820469897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Google.Sharpen.MessageDigest Google.Sharpen.MessageDigest::GetInstance(System.String)
extern "C"  MessageDigest_t1820469897 * MessageDigest_GetInstance_m1231012073 (Il2CppObject * __this /* static, unused */, String_t* ___algorithm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
