﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Google.Sharpen.MessageDigest`1<System.Object>
struct MessageDigest_1_t1512921346;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"

// System.Void Google.Sharpen.MessageDigest`1<System.Object>::.ctor()
extern "C"  void MessageDigest_1__ctor_m1278820287_gshared (MessageDigest_1_t1512921346 * __this, const MethodInfo* method);
#define MessageDigest_1__ctor_m1278820287(__this, method) ((  void (*) (MessageDigest_1_t1512921346 *, const MethodInfo*))MessageDigest_1__ctor_m1278820287_gshared)(__this, method)
// System.Byte[] Google.Sharpen.MessageDigest`1<System.Object>::Digest()
extern "C"  ByteU5BU5D_t3397334013* MessageDigest_1_Digest_m142710311_gshared (MessageDigest_1_t1512921346 * __this, const MethodInfo* method);
#define MessageDigest_1_Digest_m142710311(__this, method) ((  ByteU5BU5D_t3397334013* (*) (MessageDigest_1_t1512921346 *, const MethodInfo*))MessageDigest_1_Digest_m142710311_gshared)(__this, method)
// System.Void Google.Sharpen.MessageDigest`1<System.Object>::Dispose()
extern "C"  void MessageDigest_1_Dispose_m1405832584_gshared (MessageDigest_1_t1512921346 * __this, const MethodInfo* method);
#define MessageDigest_1_Dispose_m1405832584(__this, method) ((  void (*) (MessageDigest_1_t1512921346 *, const MethodInfo*))MessageDigest_1_Dispose_m1405832584_gshared)(__this, method)
// System.Void Google.Sharpen.MessageDigest`1<System.Object>::Init()
extern "C"  void MessageDigest_1_Init_m3249709105_gshared (MessageDigest_1_t1512921346 * __this, const MethodInfo* method);
#define MessageDigest_1_Init_m3249709105(__this, method) ((  void (*) (MessageDigest_1_t1512921346 *, const MethodInfo*))MessageDigest_1_Init_m3249709105_gshared)(__this, method)
// System.Void Google.Sharpen.MessageDigest`1<System.Object>::Reset()
extern "C"  void MessageDigest_1_Reset_m3892685886_gshared (MessageDigest_1_t1512921346 * __this, const MethodInfo* method);
#define MessageDigest_1_Reset_m3892685886(__this, method) ((  void (*) (MessageDigest_1_t1512921346 *, const MethodInfo*))MessageDigest_1_Reset_m3892685886_gshared)(__this, method)
// System.Void Google.Sharpen.MessageDigest`1<System.Object>::Update(System.Byte[])
extern "C"  void MessageDigest_1_Update_m2121830137_gshared (MessageDigest_1_t1512921346 * __this, ByteU5BU5D_t3397334013* ___input0, const MethodInfo* method);
#define MessageDigest_1_Update_m2121830137(__this, ___input0, method) ((  void (*) (MessageDigest_1_t1512921346 *, ByteU5BU5D_t3397334013*, const MethodInfo*))MessageDigest_1_Update_m2121830137_gshared)(__this, ___input0, method)
