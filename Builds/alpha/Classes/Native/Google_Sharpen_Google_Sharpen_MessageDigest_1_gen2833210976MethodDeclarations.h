﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Google_Sharpen_Google_Sharpen_MessageDigest_1_gen1512921346MethodDeclarations.h"

// System.Void Google.Sharpen.MessageDigest`1<System.Security.Cryptography.MD5CryptoServiceProvider>::.ctor()
#define MessageDigest_1__ctor_m4132279685(__this, method) ((  void (*) (MessageDigest_1_t2833210976 *, const MethodInfo*))MessageDigest_1__ctor_m1278820287_gshared)(__this, method)
// System.Byte[] Google.Sharpen.MessageDigest`1<System.Security.Cryptography.MD5CryptoServiceProvider>::Digest()
#define MessageDigest_1_Digest_m1014362913(__this, method) ((  ByteU5BU5D_t3397334013* (*) (MessageDigest_1_t2833210976 *, const MethodInfo*))MessageDigest_1_Digest_m142710311_gshared)(__this, method)
// System.Void Google.Sharpen.MessageDigest`1<System.Security.Cryptography.MD5CryptoServiceProvider>::Dispose()
#define MessageDigest_1_Dispose_m3533491938(__this, method) ((  void (*) (MessageDigest_1_t2833210976 *, const MethodInfo*))MessageDigest_1_Dispose_m1405832584_gshared)(__this, method)
// System.Void Google.Sharpen.MessageDigest`1<System.Security.Cryptography.MD5CryptoServiceProvider>::Init()
#define MessageDigest_1_Init_m2604214235(__this, method) ((  void (*) (MessageDigest_1_t2833210976 *, const MethodInfo*))MessageDigest_1_Init_m3249709105_gshared)(__this, method)
// System.Void Google.Sharpen.MessageDigest`1<System.Security.Cryptography.MD5CryptoServiceProvider>::Reset()
#define MessageDigest_1_Reset_m3299246700(__this, method) ((  void (*) (MessageDigest_1_t2833210976 *, const MethodInfo*))MessageDigest_1_Reset_m3892685886_gshared)(__this, method)
// System.Void Google.Sharpen.MessageDigest`1<System.Security.Cryptography.MD5CryptoServiceProvider>::Update(System.Byte[])
#define MessageDigest_1_Update_m2015642315(__this, ___input0, method) ((  void (*) (MessageDigest_1_t2833210976 *, ByteU5BU5D_t3397334013*, const MethodInfo*))MessageDigest_1_Update_m2121830137_gshared)(__this, ___input0, method)
