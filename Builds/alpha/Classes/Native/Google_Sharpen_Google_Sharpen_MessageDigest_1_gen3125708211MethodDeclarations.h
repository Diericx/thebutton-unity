﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Google_Sharpen_Google_Sharpen_MessageDigest_1_gen1512921346MethodDeclarations.h"

// System.Void Google.Sharpen.MessageDigest`1<System.Security.Cryptography.SHA1Managed>::.ctor()
#define MessageDigest_1__ctor_m1096675898(__this, method) ((  void (*) (MessageDigest_1_t3125708211 *, const MethodInfo*))MessageDigest_1__ctor_m1278820287_gshared)(__this, method)
// System.Byte[] Google.Sharpen.MessageDigest`1<System.Security.Cryptography.SHA1Managed>::Digest()
#define MessageDigest_1_Digest_m3417442868(__this, method) ((  ByteU5BU5D_t3397334013* (*) (MessageDigest_1_t3125708211 *, const MethodInfo*))MessageDigest_1_Digest_m142710311_gshared)(__this, method)
// System.Void Google.Sharpen.MessageDigest`1<System.Security.Cryptography.SHA1Managed>::Dispose()
#define MessageDigest_1_Dispose_m1896682679(__this, method) ((  void (*) (MessageDigest_1_t3125708211 *, const MethodInfo*))MessageDigest_1_Dispose_m1405832584_gshared)(__this, method)
// System.Void Google.Sharpen.MessageDigest`1<System.Security.Cryptography.SHA1Managed>::Init()
#define MessageDigest_1_Init_m3358470050(__this, method) ((  void (*) (MessageDigest_1_t3125708211 *, const MethodInfo*))MessageDigest_1_Init_m3249709105_gshared)(__this, method)
// System.Void Google.Sharpen.MessageDigest`1<System.Security.Cryptography.SHA1Managed>::Reset()
#define MessageDigest_1_Reset_m1709720901(__this, method) ((  void (*) (MessageDigest_1_t3125708211 *, const MethodInfo*))MessageDigest_1_Reset_m3892685886_gshared)(__this, method)
// System.Void Google.Sharpen.MessageDigest`1<System.Security.Cryptography.SHA1Managed>::Update(System.Byte[])
#define MessageDigest_1_Update_m324136476(__this, ___input0, method) ((  void (*) (MessageDigest_1_t3125708211 *, ByteU5BU5D_t3397334013*, const MethodInfo*))MessageDigest_1_Update_m2121830137_gshared)(__this, ___input0, method)
