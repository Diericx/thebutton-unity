﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Google.Sharpen.RunnableAction
struct RunnableAction_t2631350173;
// System.Action
struct Action_t3226471752;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action3226471752.h"

// System.Void Google.Sharpen.RunnableAction::.ctor(System.Action)
extern "C"  void RunnableAction__ctor_m2165871567 (RunnableAction_t2631350173 * __this, Action_t3226471752 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Google.Sharpen.RunnableAction::Run()
extern "C"  void RunnableAction_Run_m1116151679 (RunnableAction_t2631350173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
