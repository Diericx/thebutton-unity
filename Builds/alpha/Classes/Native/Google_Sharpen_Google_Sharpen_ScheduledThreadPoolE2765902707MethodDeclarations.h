﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Google.Sharpen.ScheduledThreadPoolExecutor/Task`1<System.Object>
struct Task_1_t2765902707;
// Google.Sharpen.ScheduledThreadPoolExecutor
struct ScheduledThreadPoolExecutor_t2537379786;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime693205669.h"
#include "Google_Sharpen_Google_Sharpen_ScheduledThreadPoolE2537379786.h"

// System.Void Google.Sharpen.ScheduledThreadPoolExecutor/Task`1<System.Object>::.ctor()
extern "C"  void Task_1__ctor_m1873130930_gshared (Task_1_t2765902707 * __this, const MethodInfo* method);
#define Task_1__ctor_m1873130930(__this, method) ((  void (*) (Task_1_t2765902707 *, const MethodInfo*))Task_1__ctor_m1873130930_gshared)(__this, method)
// System.DateTime Google.Sharpen.ScheduledThreadPoolExecutor/Task`1<System.Object>::get_DueTime()
extern "C"  DateTime_t693205669  Task_1_get_DueTime_m817386727_gshared (Task_1_t2765902707 * __this, const MethodInfo* method);
#define Task_1_get_DueTime_m817386727(__this, method) ((  DateTime_t693205669  (*) (Task_1_t2765902707 *, const MethodInfo*))Task_1_get_DueTime_m817386727_gshared)(__this, method)
// System.Void Google.Sharpen.ScheduledThreadPoolExecutor/Task`1<System.Object>::set_DueTime(System.DateTime)
extern "C"  void Task_1_set_DueTime_m745370016_gshared (Task_1_t2765902707 * __this, DateTime_t693205669  ___value0, const MethodInfo* method);
#define Task_1_set_DueTime_m745370016(__this, ___value0, method) ((  void (*) (Task_1_t2765902707 *, DateTime_t693205669 , const MethodInfo*))Task_1_set_DueTime_m745370016_gshared)(__this, ___value0, method)
// Google.Sharpen.ScheduledThreadPoolExecutor Google.Sharpen.ScheduledThreadPoolExecutor/Task`1<System.Object>::get_Executor()
extern "C"  ScheduledThreadPoolExecutor_t2537379786 * Task_1_get_Executor_m231682525_gshared (Task_1_t2765902707 * __this, const MethodInfo* method);
#define Task_1_get_Executor_m231682525(__this, method) ((  ScheduledThreadPoolExecutor_t2537379786 * (*) (Task_1_t2765902707 *, const MethodInfo*))Task_1_get_Executor_m231682525_gshared)(__this, method)
// System.Void Google.Sharpen.ScheduledThreadPoolExecutor/Task`1<System.Object>::set_Executor(Google.Sharpen.ScheduledThreadPoolExecutor)
extern "C"  void Task_1_set_Executor_m2168903056_gshared (Task_1_t2765902707 * __this, ScheduledThreadPoolExecutor_t2537379786 * ___value0, const MethodInfo* method);
#define Task_1_set_Executor_m2168903056(__this, ___value0, method) ((  void (*) (Task_1_t2765902707 *, ScheduledThreadPoolExecutor_t2537379786 *, const MethodInfo*))Task_1_set_Executor_m2168903056_gshared)(__this, ___value0, method)
// System.Object Google.Sharpen.ScheduledThreadPoolExecutor/Task`1<System.Object>::get_Owner()
extern "C"  Il2CppObject * Task_1_get_Owner_m4193204519_gshared (Task_1_t2765902707 * __this, const MethodInfo* method);
#define Task_1_get_Owner_m4193204519(__this, method) ((  Il2CppObject * (*) (Task_1_t2765902707 *, const MethodInfo*))Task_1_get_Owner_m4193204519_gshared)(__this, method)
// System.Void Google.Sharpen.ScheduledThreadPoolExecutor/Task`1<System.Object>::Start()
extern "C"  void Task_1_Start_m3221464038_gshared (Task_1_t2765902707 * __this, const MethodInfo* method);
#define Task_1_Start_m3221464038(__this, method) ((  void (*) (Task_1_t2765902707 *, const MethodInfo*))Task_1_Start_m3221464038_gshared)(__this, method)
// System.Void Google.Sharpen.ScheduledThreadPoolExecutor/Task`1<System.Object>::Run()
extern "C"  void Task_1_Run_m4247771159_gshared (Task_1_t2765902707 * __this, const MethodInfo* method);
#define Task_1_Run_m4247771159(__this, method) ((  void (*) (Task_1_t2765902707 *, const MethodInfo*))Task_1_Run_m4247771159_gshared)(__this, method)
// System.Boolean Google.Sharpen.ScheduledThreadPoolExecutor/Task`1<System.Object>::Cancel(System.Boolean)
extern "C"  bool Task_1_Cancel_m2715282611_gshared (Task_1_t2765902707 * __this, bool ___mayInterruptIfRunning0, const MethodInfo* method);
#define Task_1_Cancel_m2715282611(__this, ___mayInterruptIfRunning0, method) ((  bool (*) (Task_1_t2765902707 *, bool, const MethodInfo*))Task_1_Cancel_m2715282611_gshared)(__this, ___mayInterruptIfRunning0, method)
