﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Google.Sharpen.Scheduler/<HasTasks>c__AnonStorey0
struct U3CHasTasksU3Ec__AnonStorey0_t451012389;
// Google.Sharpen.IScheduledITask
struct IScheduledITask_t3129349074;

#include "codegen/il2cpp-codegen.h"

// System.Void Google.Sharpen.Scheduler/<HasTasks>c__AnonStorey0::.ctor()
extern "C"  void U3CHasTasksU3Ec__AnonStorey0__ctor_m1271780390 (U3CHasTasksU3Ec__AnonStorey0_t451012389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Google.Sharpen.Scheduler/<HasTasks>c__AnonStorey0::<>m__0(Google.Sharpen.IScheduledITask)
extern "C"  bool U3CHasTasksU3Ec__AnonStorey0_U3CU3Em__0_m2540386877 (U3CHasTasksU3Ec__AnonStorey0_t451012389 * __this, Il2CppObject * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
