﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Google.Sharpen.ThreadGroup
struct ThreadGroup_t2181833315;
// Google.Sharpen.Thread
struct Thread_t1322377586;

#include "codegen/il2cpp-codegen.h"
#include "Google_Sharpen_Google_Sharpen_Thread1322377586.h"

// System.Void Google.Sharpen.ThreadGroup::.ctor()
extern "C"  void ThreadGroup__ctor_m911405404 (ThreadGroup_t2181833315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Google.Sharpen.ThreadGroup::Add(Google.Sharpen.Thread)
extern "C"  void ThreadGroup_Add_m1453551097 (ThreadGroup_t2181833315 * __this, Thread_t1322377586 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Google.Sharpen.ThreadGroup::Remove(Google.Sharpen.Thread)
extern "C"  void ThreadGroup_Remove_m581850860 (ThreadGroup_t2181833315 * __this, Thread_t1322377586 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
