﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Google_Sharpen_Google_Sharpen_TimeUnit4006728025.h"

// System.Int64 Google.Sharpen.TimeUnitExtensions::Convert(Google.Sharpen.TimeUnit,System.Int64,Google.Sharpen.TimeUnit)
extern "C"  int64_t TimeUnitExtensions_Convert_m3092584112 (Il2CppObject * __this /* static, unused */, int64_t ___thisUnit0, int64_t ___duration1, int64_t ___targetUnit2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
