﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Google.Sharpen.URISyntaxException
struct URISyntaxException_t3236900932;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Google.Sharpen.URISyntaxException::.ctor(System.String,System.String)
extern "C"  void URISyntaxException__ctor_m1394743853 (URISyntaxException_t3236900932 * __this, String_t* ___s0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
