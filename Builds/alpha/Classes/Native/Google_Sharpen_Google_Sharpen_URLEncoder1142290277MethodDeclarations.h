﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String Google.Sharpen.URLEncoder::Encode(System.String,System.String)
extern "C"  String_t* URLEncoder_Encode_m524824813 (Il2CppObject * __this /* static, unused */, String_t* ___str0, String_t* ___encoding1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
