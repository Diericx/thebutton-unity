﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Specialized_ListDictiona3458713452.h"
#include "System_System_Collections_Specialized_ListDictiona2725637098.h"
#include "System_System_Collections_Specialized_ListDictiona1923170152.h"
#include "System_System_Collections_Specialized_ListDictionar528898270.h"
#include "System_System_Collections_Specialized_ListDictiona2037848305.h"
#include "System_System_Collections_Specialized_NameObjectCo2034248631.h"
#include "System_System_Collections_Specialized_NameObjectCo3244489099.h"
#include "System_System_Collections_Specialized_NameObjectCo1718269396.h"
#include "System_System_Collections_Specialized_NameObjectCol633582367.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564.h"
#include "System_System_Collections_Specialized_StringCollect352985975.h"
#include "System_System_Collections_Specialized_StringDictio1070889667.h"
#include "System_System_Collections_Specialized_StringEnumera441637433.h"
#include "System_System_ComponentModel_ArrayConverter2804512129.h"
#include "System_System_ComponentModel_ArrayConverter_ArrayPr599180064.h"
#include "System_System_ComponentModel_AsyncCompletedEventArgs83270938.h"
#include "System_System_ComponentModel_AttributeCollection1925812292.h"
#include "System_System_ComponentModel_BaseNumberConverter1130358776.h"
#include "System_System_ComponentModel_BooleanConverter284715810.h"
#include "System_System_ComponentModel_BrowsableAttribute2487167291.h"
#include "System_System_ComponentModel_ByteConverter1265255600.h"
#include "System_System_ComponentModel_CharConverter437233350.h"
#include "System_System_ComponentModel_CollectionConverter2459375096.h"
#include "System_System_ComponentModel_ComponentCollection737017907.h"
#include "System_System_ComponentModel_ComponentConverter3121608223.h"
#include "System_System_ComponentModel_Component2826673791.h"
#include "System_System_ComponentModel_CultureInfoConverter2239982248.h"
#include "System_System_ComponentModel_CultureInfoConverter_2714931249.h"
#include "System_System_ComponentModel_DateTimeConverter2436647419.h"
#include "System_System_ComponentModel_DecimalConverter1618403211.h"
#include "System_System_ComponentModel_DefaultEventAttribute1079704873.h"
#include "System_System_ComponentModel_DefaultPropertyAttrib1962767338.h"
#include "System_System_ComponentModel_DefaultValueAttribute1302720498.h"
#include "System_System_ComponentModel_DesignerAttribute2778719479.h"
#include "System_System_ComponentModel_DesignerCategoryAttri1270090451.h"
#include "System_System_ComponentModel_DesignerSerialization2980019899.h"
#include "System_System_ComponentModel_DesignerSerialization3751360903.h"
#include "System_System_ComponentModel_Design_Serialization_2188593799.h"
#include "System_System_ComponentModel_Design_Serialization_1404033120.h"
#include "System_System_ComponentModel_Design_Serialization_1162957127.h"
#include "System_System_ComponentModel_DoubleConverter864652623.h"
#include "System_System_ComponentModel_EditorAttribute3559776959.h"
#include "System_System_ComponentModel_EditorBrowsableAttrib1050682502.h"
#include "System_System_ComponentModel_EditorBrowsableState373498655.h"
#include "System_System_ComponentModel_EnumConverter2538808523.h"
#include "System_System_ComponentModel_EnumConverter_EnumCom2635778853.h"
#include "System_System_ComponentModel_EventDescriptorCollec3053042509.h"
#include "System_System_ComponentModel_EventDescriptor962731901.h"
#include "System_System_ComponentModel_ListEntry935815304.h"
#include "System_System_ComponentModel_EventHandlerList1298116880.h"
#include "System_System_ComponentModel_GuidConverter1547586607.h"
#include "System_System_ComponentModel_Int16Converter903627590.h"
#include "System_System_ComponentModel_Int32Converter957938388.h"
#include "System_System_ComponentModel_Int64Converter3186343659.h"
#include "System_System_ComponentModel_MemberDescriptor3749827553.h"
#include "System_System_ComponentModel_MemberDescriptor_Membe856477673.h"
#include "System_System_ComponentModel_NullableConverter1941973167.h"
#include "System_System_ComponentModel_PropertyChangedEventA1689446432.h"
#include "System_System_ComponentModel_PropertyDescriptorCol3166009492.h"
#include "System_System_ComponentModel_PropertyDescriptor4250402154.h"
#include "System_System_ComponentModel_ReadOnlyAttribute4102148880.h"
#include "System_System_ComponentModel_ReferenceConverter3131270729.h"
#include "System_System_ComponentModel_ReflectionEventDescri3740223310.h"
#include "System_System_ComponentModel_ReflectionPropertyDes2570125387.h"
#include "System_System_ComponentModel_RefreshEventArgs2077477224.h"
#include "System_System_ComponentModel_SByteConverter4003686519.h"
#include "System_System_ComponentModel_SingleConverter3693313828.h"
#include "System_System_ComponentModel_StringConverter3749524419.h"
#include "System_System_ComponentModel_TimeSpanConverter2149358279.h"
#include "System_System_ComponentModel_TypeConverterAttribute252469870.h"
#include "System_System_ComponentModel_TypeConverter745995970.h"
#include "System_System_ComponentModel_TypeConverter_Standard191679357.h"
#include "System_System_ComponentModel_TypeDescriptionProvid2438624375.h"
#include "System_System_ComponentModel_TypeDescriptor3595688691.h"
#include "System_System_ComponentModel_TypeDescriptor_Attrib1066710002.h"
#include "System_System_ComponentModel_TypeDescriptor_Wrappe1420150273.h"
#include "System_System_ComponentModel_TypeDescriptor_Defaul2227823135.h"
#include "System_System_ComponentModel_Info42715200.h"
#include "System_System_ComponentModel_ComponentInfo2742875487.h"
#include "System_System_ComponentModel_TypeInfo1029530608.h"
#include "System_System_ComponentModel_UInt16Converter1747783369.h"
#include "System_System_ComponentModel_UInt32Converter1748821239.h"
#include "System_System_ComponentModel_UInt64Converter977523578.h"
#include "System_System_ComponentModel_WeakObjectWrapper2012978780.h"
#include "System_System_ComponentModel_WeakObjectWrapperComp3891611113.h"
#include "System_System_ComponentModel_Win32Exception1708275760.h"
#include "System_System_Configuration_ConfigurationException3814184945.h"
#include "System_System_Configuration_ConfigurationSettings1600776263.h"
#include "System_System_Configuration_DefaultConfig320482295.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1100 = { sizeof (ListDictionary_t3458713452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1100[4] = 
{
	ListDictionary_t3458713452::get_offset_of_count_0(),
	ListDictionary_t3458713452::get_offset_of_version_1(),
	ListDictionary_t3458713452::get_offset_of_head_2(),
	ListDictionary_t3458713452::get_offset_of_comparer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1101 = { sizeof (DictionaryNode_t2725637098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1101[3] = 
{
	DictionaryNode_t2725637098::get_offset_of_key_0(),
	DictionaryNode_t2725637098::get_offset_of_value_1(),
	DictionaryNode_t2725637098::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1102 = { sizeof (DictionaryNodeEnumerator_t1923170152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1102[4] = 
{
	DictionaryNodeEnumerator_t1923170152::get_offset_of_dict_0(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_isAtStart_1(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_current_2(),
	DictionaryNodeEnumerator_t1923170152::get_offset_of_version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1103 = { sizeof (DictionaryNodeCollection_t528898270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1103[2] = 
{
	DictionaryNodeCollection_t528898270::get_offset_of_dict_0(),
	DictionaryNodeCollection_t528898270::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1104 = { sizeof (DictionaryNodeCollectionEnumerator_t2037848305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1104[2] = 
{
	DictionaryNodeCollectionEnumerator_t2037848305::get_offset_of_inner_0(),
	DictionaryNodeCollectionEnumerator_t2037848305::get_offset_of_isKeyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1105 = { sizeof (NameObjectCollectionBase_t2034248631), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1105[10] = 
{
	NameObjectCollectionBase_t2034248631::get_offset_of_m_ItemsContainer_0(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_NullKeyItem_1(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_ItemsArray_2(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_hashprovider_3(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_comparer_4(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_defCapacity_5(),
	NameObjectCollectionBase_t2034248631::get_offset_of_m_readonly_6(),
	NameObjectCollectionBase_t2034248631::get_offset_of_infoCopy_7(),
	NameObjectCollectionBase_t2034248631::get_offset_of_keyscoll_8(),
	NameObjectCollectionBase_t2034248631::get_offset_of_equality_comparer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1106 = { sizeof (_Item_t3244489099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1106[2] = 
{
	_Item_t3244489099::get_offset_of_key_0(),
	_Item_t3244489099::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1107 = { sizeof (_KeysEnumerator_t1718269396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1107[2] = 
{
	_KeysEnumerator_t1718269396::get_offset_of_m_collection_0(),
	_KeysEnumerator_t1718269396::get_offset_of_m_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1108 = { sizeof (KeysCollection_t633582367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1108[1] = 
{
	KeysCollection_t633582367::get_offset_of_m_collection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1109 = { sizeof (NameValueCollection_t3047564564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1109[2] = 
{
	NameValueCollection_t3047564564::get_offset_of_cachedAllKeys_10(),
	NameValueCollection_t3047564564::get_offset_of_cachedAll_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1110 = { sizeof (StringCollection_t352985975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1110[1] = 
{
	StringCollection_t352985975::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1111 = { sizeof (StringDictionary_t1070889667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1111[1] = 
{
	StringDictionary_t1070889667::get_offset_of_contents_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1112 = { sizeof (StringEnumerator_t441637433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1112[1] = 
{
	StringEnumerator_t441637433::get_offset_of_enumerable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1113 = { sizeof (ArrayConverter_t2804512129), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1114 = { sizeof (ArrayPropertyDescriptor_t599180064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1114[2] = 
{
	ArrayPropertyDescriptor_t599180064::get_offset_of_index_4(),
	ArrayPropertyDescriptor_t599180064::get_offset_of_array_type_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1115 = { sizeof (AsyncCompletedEventArgs_t83270938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1115[3] = 
{
	AsyncCompletedEventArgs_t83270938::get_offset_of__error_1(),
	AsyncCompletedEventArgs_t83270938::get_offset_of__cancelled_2(),
	AsyncCompletedEventArgs_t83270938::get_offset_of__userState_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1116 = { sizeof (AttributeCollection_t1925812292), -1, sizeof(AttributeCollection_t1925812292_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1116[2] = 
{
	AttributeCollection_t1925812292::get_offset_of_attrList_0(),
	AttributeCollection_t1925812292_StaticFields::get_offset_of_Empty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1117 = { sizeof (BaseNumberConverter_t1130358776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1117[1] = 
{
	BaseNumberConverter_t1130358776::get_offset_of_InnerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1118 = { sizeof (BooleanConverter_t284715810), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1119 = { sizeof (BrowsableAttribute_t2487167291), -1, sizeof(BrowsableAttribute_t2487167291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1119[4] = 
{
	BrowsableAttribute_t2487167291::get_offset_of_browsable_0(),
	BrowsableAttribute_t2487167291_StaticFields::get_offset_of_Default_1(),
	BrowsableAttribute_t2487167291_StaticFields::get_offset_of_No_2(),
	BrowsableAttribute_t2487167291_StaticFields::get_offset_of_Yes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1120 = { sizeof (ByteConverter_t1265255600), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1121 = { sizeof (CharConverter_t437233350), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1122 = { sizeof (CollectionConverter_t2459375096), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1123 = { sizeof (ComponentCollection_t737017907), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1124 = { sizeof (ComponentConverter_t3121608223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1125 = { sizeof (Component_t2826673791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1125[3] = 
{
	Component_t2826673791::get_offset_of_event_handlers_1(),
	Component_t2826673791::get_offset_of_mySite_2(),
	Component_t2826673791::get_offset_of_disposedEvent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1126 = { sizeof (CultureInfoConverter_t2239982248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1126[1] = 
{
	CultureInfoConverter_t2239982248::get_offset_of__standardValues_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1127 = { sizeof (CultureInfoComparer_t2714931249), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1128 = { sizeof (DateTimeConverter_t2436647419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1129 = { sizeof (DecimalConverter_t1618403211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1130 = { sizeof (DefaultEventAttribute_t1079704873), -1, sizeof(DefaultEventAttribute_t1079704873_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1130[2] = 
{
	DefaultEventAttribute_t1079704873::get_offset_of_eventName_0(),
	DefaultEventAttribute_t1079704873_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1131 = { sizeof (DefaultPropertyAttribute_t1962767338), -1, sizeof(DefaultPropertyAttribute_t1962767338_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1131[2] = 
{
	DefaultPropertyAttribute_t1962767338::get_offset_of_property_name_0(),
	DefaultPropertyAttribute_t1962767338_StaticFields::get_offset_of_Default_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1132 = { sizeof (DefaultValueAttribute_t1302720498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1132[1] = 
{
	DefaultValueAttribute_t1302720498::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1133 = { sizeof (DesignerAttribute_t2778719479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1133[2] = 
{
	DesignerAttribute_t2778719479::get_offset_of_name_0(),
	DesignerAttribute_t2778719479::get_offset_of_basetypename_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1134 = { sizeof (DesignerCategoryAttribute_t1270090451), -1, sizeof(DesignerCategoryAttribute_t1270090451_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1134[5] = 
{
	DesignerCategoryAttribute_t1270090451::get_offset_of_category_0(),
	DesignerCategoryAttribute_t1270090451_StaticFields::get_offset_of_Component_1(),
	DesignerCategoryAttribute_t1270090451_StaticFields::get_offset_of_Form_2(),
	DesignerCategoryAttribute_t1270090451_StaticFields::get_offset_of_Generic_3(),
	DesignerCategoryAttribute_t1270090451_StaticFields::get_offset_of_Default_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1135 = { sizeof (DesignerSerializationVisibilityAttribute_t2980019899), -1, sizeof(DesignerSerializationVisibilityAttribute_t2980019899_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1135[5] = 
{
	DesignerSerializationVisibilityAttribute_t2980019899::get_offset_of_visibility_0(),
	DesignerSerializationVisibilityAttribute_t2980019899_StaticFields::get_offset_of_Default_1(),
	DesignerSerializationVisibilityAttribute_t2980019899_StaticFields::get_offset_of_Content_2(),
	DesignerSerializationVisibilityAttribute_t2980019899_StaticFields::get_offset_of_Hidden_3(),
	DesignerSerializationVisibilityAttribute_t2980019899_StaticFields::get_offset_of_Visible_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1136 = { sizeof (DesignerSerializationVisibility_t3751360903)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1136[4] = 
{
	DesignerSerializationVisibility_t3751360903::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1137 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1138 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1139 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1140 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1141 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1142 = { sizeof (DesignerSerializerAttribute_t2188593799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1142[2] = 
{
	DesignerSerializerAttribute_t2188593799::get_offset_of_serializerTypeName_0(),
	DesignerSerializerAttribute_t2188593799::get_offset_of_baseSerializerTypeName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1143 = { sizeof (InstanceDescriptor_t1404033120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1143[3] = 
{
	InstanceDescriptor_t1404033120::get_offset_of_member_0(),
	InstanceDescriptor_t1404033120::get_offset_of_arguments_1(),
	InstanceDescriptor_t1404033120::get_offset_of_isComplete_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1144 = { sizeof (RootDesignerSerializerAttribute_t1162957127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1144[3] = 
{
	RootDesignerSerializerAttribute_t1162957127::get_offset_of_serializer_0(),
	RootDesignerSerializerAttribute_t1162957127::get_offset_of_baseserializer_1(),
	RootDesignerSerializerAttribute_t1162957127::get_offset_of_reload_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1145 = { sizeof (DoubleConverter_t864652623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1146 = { sizeof (EditorAttribute_t3559776959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1146[2] = 
{
	EditorAttribute_t3559776959::get_offset_of_name_0(),
	EditorAttribute_t3559776959::get_offset_of_basename_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1147 = { sizeof (EditorBrowsableAttribute_t1050682502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1147[1] = 
{
	EditorBrowsableAttribute_t1050682502::get_offset_of_state_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1148 = { sizeof (EditorBrowsableState_t373498655)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1148[4] = 
{
	EditorBrowsableState_t373498655::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1149 = { sizeof (EnumConverter_t2538808523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1149[2] = 
{
	EnumConverter_t2538808523::get_offset_of_type_0(),
	EnumConverter_t2538808523::get_offset_of_stdValues_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1150 = { sizeof (EnumComparer_t2635778853), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1151 = { sizeof (EventDescriptorCollection_t3053042509), -1, sizeof(EventDescriptorCollection_t3053042509_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1151[3] = 
{
	EventDescriptorCollection_t3053042509::get_offset_of_eventList_0(),
	EventDescriptorCollection_t3053042509::get_offset_of_isReadOnly_1(),
	EventDescriptorCollection_t3053042509_StaticFields::get_offset_of_Empty_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1152 = { sizeof (EventDescriptor_t962731901), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1153 = { sizeof (ListEntry_t935815304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1153[3] = 
{
	ListEntry_t935815304::get_offset_of_key_0(),
	ListEntry_t935815304::get_offset_of_value_1(),
	ListEntry_t935815304::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1154 = { sizeof (EventHandlerList_t1298116880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1154[2] = 
{
	EventHandlerList_t1298116880::get_offset_of_entries_0(),
	EventHandlerList_t1298116880::get_offset_of_null_entry_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1155 = { sizeof (GuidConverter_t1547586607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1156 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1157 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1158 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1159 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1160 = { sizeof (Int16Converter_t903627590), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1161 = { sizeof (Int32Converter_t957938388), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1162 = { sizeof (Int64Converter_t3186343659), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1163 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1164 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1165 = { sizeof (MemberDescriptor_t3749827553), -1, sizeof(MemberDescriptor_t3749827553_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1165[4] = 
{
	MemberDescriptor_t3749827553::get_offset_of_name_0(),
	MemberDescriptor_t3749827553::get_offset_of_attrs_1(),
	MemberDescriptor_t3749827553::get_offset_of_attrCollection_2(),
	MemberDescriptor_t3749827553_StaticFields::get_offset_of_default_comparer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1166 = { sizeof (MemberDescriptorComparer_t856477673), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1167 = { sizeof (NullableConverter_t1941973167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1167[3] = 
{
	NullableConverter_t1941973167::get_offset_of_nullableType_0(),
	NullableConverter_t1941973167::get_offset_of_underlyingType_1(),
	NullableConverter_t1941973167::get_offset_of_underlyingTypeConverter_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1168 = { sizeof (PropertyChangedEventArgs_t1689446432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1168[1] = 
{
	PropertyChangedEventArgs_t1689446432::get_offset_of_propertyName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1169 = { sizeof (PropertyDescriptorCollection_t3166009492), -1, sizeof(PropertyDescriptorCollection_t3166009492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1169[3] = 
{
	PropertyDescriptorCollection_t3166009492_StaticFields::get_offset_of_Empty_0(),
	PropertyDescriptorCollection_t3166009492::get_offset_of_properties_1(),
	PropertyDescriptorCollection_t3166009492::get_offset_of_readOnly_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1170 = { sizeof (PropertyDescriptor_t4250402154), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1171 = { sizeof (ReadOnlyAttribute_t4102148880), -1, sizeof(ReadOnlyAttribute_t4102148880_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1171[4] = 
{
	ReadOnlyAttribute_t4102148880::get_offset_of_read_only_0(),
	ReadOnlyAttribute_t4102148880_StaticFields::get_offset_of_No_1(),
	ReadOnlyAttribute_t4102148880_StaticFields::get_offset_of_Yes_2(),
	ReadOnlyAttribute_t4102148880_StaticFields::get_offset_of_Default_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1172 = { sizeof (ReferenceConverter_t3131270729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1172[1] = 
{
	ReferenceConverter_t3131270729::get_offset_of_reference_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1173 = { sizeof (ReflectionEventDescriptor_t3740223310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1173[5] = 
{
	ReflectionEventDescriptor_t3740223310::get_offset_of__eventType_4(),
	ReflectionEventDescriptor_t3740223310::get_offset_of__componentType_5(),
	ReflectionEventDescriptor_t3740223310::get_offset_of__eventInfo_6(),
	ReflectionEventDescriptor_t3740223310::get_offset_of_add_method_7(),
	ReflectionEventDescriptor_t3740223310::get_offset_of_remove_method_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1174 = { sizeof (ReflectionPropertyDescriptor_t2570125387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1174[3] = 
{
	ReflectionPropertyDescriptor_t2570125387::get_offset_of__member_4(),
	ReflectionPropertyDescriptor_t2570125387::get_offset_of__componentType_5(),
	ReflectionPropertyDescriptor_t2570125387::get_offset_of__propertyType_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1175 = { sizeof (RefreshEventArgs_t2077477224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1175[2] = 
{
	RefreshEventArgs_t2077477224::get_offset_of_component_1(),
	RefreshEventArgs_t2077477224::get_offset_of_type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1176 = { sizeof (SByteConverter_t4003686519), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1177 = { sizeof (SingleConverter_t3693313828), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1178 = { sizeof (StringConverter_t3749524419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1179 = { sizeof (TimeSpanConverter_t2149358279), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1180 = { sizeof (TypeConverterAttribute_t252469870), -1, sizeof(TypeConverterAttribute_t252469870_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1180[2] = 
{
	TypeConverterAttribute_t252469870_StaticFields::get_offset_of_Default_0(),
	TypeConverterAttribute_t252469870::get_offset_of_converter_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1181 = { sizeof (TypeConverter_t745995970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1182 = { sizeof (StandardValuesCollection_t191679357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1182[1] = 
{
	StandardValuesCollection_t191679357::get_offset_of_values_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1183 = { sizeof (TypeDescriptionProvider_t2438624375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1183[1] = 
{
	TypeDescriptionProvider_t2438624375::get_offset_of__parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1184 = { sizeof (TypeDescriptor_t3595688691), -1, sizeof(TypeDescriptor_t3595688691_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1184[12] = 
{
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_creatingDefaultConverters_0(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_defaultConverters_1(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_descriptorHandler_2(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_componentTable_3(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_typeTable_4(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_editors_5(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_typeDescriptionProvidersLock_6(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_typeDescriptionProviders_7(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_componentDescriptionProvidersLock_8(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_componentDescriptionProviders_9(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_onDispose_10(),
	TypeDescriptor_t3595688691_StaticFields::get_offset_of_Refreshed_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1185 = { sizeof (AttributeProvider_t1066710002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1185[1] = 
{
	AttributeProvider_t1066710002::get_offset_of_attributes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1186 = { sizeof (WrappedTypeDescriptionProvider_t1420150273), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1186[1] = 
{
	WrappedTypeDescriptionProvider_t1420150273::get_offset_of_U3CWrappedU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1187 = { sizeof (DefaultTypeDescriptionProvider_t2227823135), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1188 = { sizeof (Info_t42715200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1188[6] = 
{
	Info_t42715200::get_offset_of__infoType_0(),
	Info_t42715200::get_offset_of__defaultEvent_1(),
	Info_t42715200::get_offset_of__gotDefaultEvent_2(),
	Info_t42715200::get_offset_of__defaultProperty_3(),
	Info_t42715200::get_offset_of__gotDefaultProperty_4(),
	Info_t42715200::get_offset_of__attributes_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1189 = { sizeof (ComponentInfo_t2742875487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1189[3] = 
{
	ComponentInfo_t2742875487::get_offset_of__component_6(),
	ComponentInfo_t2742875487::get_offset_of__events_7(),
	ComponentInfo_t2742875487::get_offset_of__properties_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1190 = { sizeof (TypeInfo_t1029530608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1190[2] = 
{
	TypeInfo_t1029530608::get_offset_of__events_6(),
	TypeInfo_t1029530608::get_offset_of__properties_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1191 = { sizeof (UInt16Converter_t1747783369), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1192 = { sizeof (UInt32Converter_t1748821239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1193 = { sizeof (UInt64Converter_t977523578), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1194 = { sizeof (WeakObjectWrapper_t2012978780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1194[2] = 
{
	WeakObjectWrapper_t2012978780::get_offset_of_U3CTargetHashCodeU3Ek__BackingField_0(),
	WeakObjectWrapper_t2012978780::get_offset_of_U3CWeakU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1195 = { sizeof (WeakObjectWrapperComparer_t3891611113), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1196 = { sizeof (Win32Exception_t1708275760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1196[1] = 
{
	Win32Exception_t1708275760::get_offset_of_native_error_code_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1197 = { sizeof (ConfigurationException_t3814184945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1197[2] = 
{
	ConfigurationException_t3814184945::get_offset_of_filename_11(),
	ConfigurationException_t3814184945::get_offset_of_line_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1198 = { sizeof (ConfigurationSettings_t1600776263), -1, sizeof(ConfigurationSettings_t1600776263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1198[2] = 
{
	ConfigurationSettings_t1600776263_StaticFields::get_offset_of_config_0(),
	ConfigurationSettings_t1600776263_StaticFields::get_offset_of_lockobj_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1199 = { sizeof (DefaultConfig_t320482295), -1, sizeof(DefaultConfig_t320482295_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1199[1] = 
{
	DefaultConfig_t320482295_StaticFields::get_offset_of_instance_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
