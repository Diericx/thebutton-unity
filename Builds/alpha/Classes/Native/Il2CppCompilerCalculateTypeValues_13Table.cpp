﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_IPAddress1399971723.h"
#include "System_System_Net_IPEndPoint2615413766.h"
#include "System_System_Net_IPHostEntry994738509.h"
#include "System_System_Net_IPv6Address2596635879.h"
#include "System_System_Net_NetConfig1609737885.h"
#include "System_System_Net_NetworkCredential1714133953.h"
#include "System_System_Net_NetworkInformation_IPInterfacePr3986609851.h"
#include "System_System_Net_NetworkInformation_Win32IPInterf3641679752.h"
#include "System_System_Net_NetworkInformation_IPv4Interface3946458365.h"
#include "System_System_Net_NetworkInformation_Win32IPv4Inte2652275954.h"
#include "System_System_Net_NetworkInformation_ifa_ifu602722385.h"
#include "System_System_Net_NetworkInformation_ifaddrs2532459533.h"
#include "System_System_Net_NetworkInformation_sockaddr_in1277740973.h"
#include "System_System_Net_NetworkInformation_sockaddr_in62899168071.h"
#include "System_System_Net_NetworkInformation_in6_addr4035827331.h"
#include "System_System_Net_NetworkInformation_sockaddr_ll1681025498.h"
#include "System_System_Net_NetworkInformation_LinuxArpHardw4257672401.h"
#include "System_System_Net_NetworkInformation_MacOsStructs_i937751619.h"
#include "System_System_Net_NetworkInformation_MacOsStructs_3329842375.h"
#include "System_System_Net_NetworkInformation_MacOsStructs_2792443317.h"
#include "System_System_Net_NetworkInformation_MacOsStructs_1014645363.h"
#include "System_System_Net_NetworkInformation_MacOsStructs_s834146887.h"
#include "System_System_Net_NetworkInformation_MacOsStructs_3955242742.h"
#include "System_System_Net_NetworkInformation_MacOsArpHardwa650784048.h"
#include "System_System_Net_NetworkInformation_NetworkInform1863186723.h"
#include "System_System_Net_NetworkInformation_NetworkInterf2376191102.h"
#include "System_System_Net_NetworkInformation_NetworkInterfac63927633.h"
#include "System_System_Net_NetworkInformation_UnixNetworkIn1000704527.h"
#include "System_System_Net_NetworkInformation_LinuxNetworkI3864470295.h"
#include "System_System_Net_NetworkInformation_MacOsNetworkI1454185290.h"
#include "System_System_Net_NetworkInformation_Win32NetworkIn482839970.h"
#include "System_System_Net_NetworkInformation_NetworkInterf4226883065.h"
#include "System_System_Net_NetworkInformation_OperationalSt2833345236.h"
#include "System_System_Net_NetworkInformation_AlignmentUnion707470070.h"
#include "System_System_Net_NetworkInformation_Win32_IP_ADAPT680756680.h"
#include "System_System_Net_NetworkInformation_Win32_MIB_IFR4215928996.h"
#include "System_System_Net_ProtocolViolationException4263317570.h"
#include "System_System_Net_Security_AuthenticatedStream1183414097.h"
#include "System_System_Net_Security_AuthenticationLevel2424130044.h"
#include "System_System_Net_SecurityProtocolType3099771628.h"
#include "System_System_Net_Security_SslStream1853163792.h"
#include "System_System_Net_Security_SslStream_U3CBeginAuthe1358332250.h"
#include "System_System_Net_Security_SslPolicyErrors1928581431.h"
#include "System_System_Net_ServicePoint2765344313.h"
#include "System_System_Net_ServicePointManager745663000.h"
#include "System_System_Net_ServicePointManager_SPKey1552752485.h"
#include "System_System_Net_ServicePointManager_ChainValidat1155887809.h"
#include "System_System_Net_SocketAddress838303055.h"
#include "System_System_Net_Sockets_AddressFamily303362630.h"
#include "System_System_Net_Sockets_LingerOption1165263720.h"
#include "System_System_Net_Sockets_MulticastOption2505469155.h"
#include "System_System_Net_Sockets_NetworkStream581172200.h"
#include "System_System_Net_Sockets_ProtocolType2178963134.h"
#include "System_System_Net_Sockets_SelectMode3413969319.h"
#include "System_System_Net_Sockets_Socket3821512045.h"
#include "System_System_Net_Sockets_Socket_SocketOperation3328960782.h"
#include "System_System_Net_Sockets_Socket_SocketAsyncResult2959281146.h"
#include "System_System_Net_Sockets_Socket_Worker1317165246.h"
#include "System_System_Net_Sockets_Socket_SocketAsyncCall3737776727.h"
#include "System_System_Net_Sockets_SocketError307542793.h"
#include "System_System_Net_Sockets_SocketException1618573604.h"
#include "System_System_Net_Sockets_SocketFlags2353657790.h"
#include "System_System_Net_Sockets_SocketOptionLevel1505247880.h"
#include "System_System_Net_Sockets_SocketOptionName1089121285.h"
#include "System_System_Net_Sockets_SocketShutdown3247039417.h"
#include "System_System_Net_Sockets_SocketType1143498533.h"
#include "System_System_Net_Sockets_TcpClient408947970.h"
#include "System_System_Net_WebAsyncResult905414499.h"
#include "System_System_Net_WebClient1432723993.h"
#include "System_System_Net_ReadState657568301.h"
#include "System_System_Net_WebConnection324679648.h"
#include "System_System_Net_WebConnection_AbortHelper2895113041.h"
#include "System_System_Net_WebConnectionData3550260432.h"
#include "System_System_Net_WebConnectionGroup3242458773.h"
#include "System_System_Net_WebConnectionStream1922483508.h"
#include "System_System_Net_WebException3368933679.h"
#include "System_System_Net_WebExceptionStatus1169373531.h"
#include "System_System_Net_WebHeaderCollection3028142837.h"
#include "System_System_Net_WebProxy1169192840.h"
#include "System_System_Net_WebRequest1365124353.h"
#include "System_System_Net_WebResponse1895226051.h"
#include "System_System_Security_Authentication_SslProtocols894678499.h"
#include "System_System_Security_Cryptography_AsnDecodeStatu1962003286.h"
#include "System_System_Security_Cryptography_AsnEncodedData463456204.h"
#include "System_System_Security_Cryptography_OidCollection3790243618.h"
#include "System_System_Security_Cryptography_Oid3221867120.h"
#include "System_System_Security_Cryptography_OidEnumerator3674631724.h"
#include "System_System_Security_Cryptography_X509Certificat2370524385.h"
#include "System_Mono_Security_X509_OSX509Certificates3584809896.h"
#include "System_Mono_Security_X509_OSX509Certificates_SecTr1984565408.h"
#include "System_System_Security_Cryptography_X509Certificates_P870392.h"
#include "System_System_Security_Cryptography_X509Certificat1570828128.h"
#include "System_System_Security_Cryptography_X509Certificat2183514610.h"
#include "System_System_Security_Cryptography_X509Certificate452415348.h"
#include "System_System_Security_Cryptography_X509Certificat2005802885.h"
#include "System_System_Security_Cryptography_X509Certificat1562873317.h"
#include "System_System_Security_Cryptography_X509Certificat1108969367.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1300 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1301 = { sizeof (IPAddress_t1399971723), -1, sizeof(IPAddress_t1399971723_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1301[11] = 
{
	IPAddress_t1399971723::get_offset_of_m_Address_0(),
	IPAddress_t1399971723::get_offset_of_m_Family_1(),
	IPAddress_t1399971723::get_offset_of_m_Numbers_2(),
	IPAddress_t1399971723::get_offset_of_m_ScopeId_3(),
	IPAddress_t1399971723_StaticFields::get_offset_of_Any_4(),
	IPAddress_t1399971723_StaticFields::get_offset_of_Broadcast_5(),
	IPAddress_t1399971723_StaticFields::get_offset_of_Loopback_6(),
	IPAddress_t1399971723_StaticFields::get_offset_of_None_7(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6Any_8(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6Loopback_9(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6None_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1302 = { sizeof (IPEndPoint_t2615413766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1302[2] = 
{
	IPEndPoint_t2615413766::get_offset_of_address_0(),
	IPEndPoint_t2615413766::get_offset_of_port_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1303 = { sizeof (IPHostEntry_t994738509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1303[3] = 
{
	IPHostEntry_t994738509::get_offset_of_addressList_0(),
	IPHostEntry_t994738509::get_offset_of_aliases_1(),
	IPHostEntry_t994738509::get_offset_of_hostName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1304 = { sizeof (IPv6Address_t2596635879), -1, sizeof(IPv6Address_t2596635879_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1304[5] = 
{
	IPv6Address_t2596635879::get_offset_of_address_0(),
	IPv6Address_t2596635879::get_offset_of_prefixLength_1(),
	IPv6Address_t2596635879::get_offset_of_scopeId_2(),
	IPv6Address_t2596635879_StaticFields::get_offset_of_Loopback_3(),
	IPv6Address_t2596635879_StaticFields::get_offset_of_Unspecified_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1305 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1306 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1307 = { sizeof (NetConfig_t1609737885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1307[2] = 
{
	NetConfig_t1609737885::get_offset_of_ipv6Enabled_0(),
	NetConfig_t1609737885::get_offset_of_MaxResponseHeadersLength_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1308 = { sizeof (NetworkCredential_t1714133953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1308[3] = 
{
	NetworkCredential_t1714133953::get_offset_of_userName_0(),
	NetworkCredential_t1714133953::get_offset_of_password_1(),
	NetworkCredential_t1714133953::get_offset_of_domain_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1309 = { sizeof (IPInterfaceProperties_t3986609851), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1310 = { sizeof (Win32IPInterfaceProperties2_t3641679752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1310[3] = 
{
	Win32IPInterfaceProperties2_t3641679752::get_offset_of_addr_0(),
	Win32IPInterfaceProperties2_t3641679752::get_offset_of_mib4_1(),
	Win32IPInterfaceProperties2_t3641679752::get_offset_of_mib6_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1311 = { sizeof (IPv4InterfaceStatistics_t3946458365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1312 = { sizeof (Win32IPv4InterfaceStatistics_t2652275954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1312[1] = 
{
	Win32IPv4InterfaceStatistics_t2652275954::get_offset_of_info_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1313 = { sizeof (ifa_ifu_t602722385)+ sizeof (Il2CppObject), sizeof(ifa_ifu_t602722385 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1313[2] = 
{
	ifa_ifu_t602722385::get_offset_of_ifu_broadaddr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifa_ifu_t602722385::get_offset_of_ifu_dstaddr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1314 = { sizeof (ifaddrs_t2532459533)+ sizeof (Il2CppObject), sizeof(ifaddrs_t2532459533_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1314[7] = 
{
	ifaddrs_t2532459533::get_offset_of_ifa_next_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t2532459533::get_offset_of_ifa_name_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t2532459533::get_offset_of_ifa_flags_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t2532459533::get_offset_of_ifa_addr_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t2532459533::get_offset_of_ifa_netmask_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t2532459533::get_offset_of_ifa_ifu_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t2532459533::get_offset_of_ifa_data_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1315 = { sizeof (sockaddr_in_t1277740973)+ sizeof (Il2CppObject), sizeof(sockaddr_in_t1277740973 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1315[3] = 
{
	sockaddr_in_t1277740973::get_offset_of_sin_family_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in_t1277740973::get_offset_of_sin_port_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in_t1277740973::get_offset_of_sin_addr_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1316 = { sizeof (sockaddr_in6_t2899168071)+ sizeof (Il2CppObject), sizeof(sockaddr_in6_t2899168071_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1316[5] = 
{
	sockaddr_in6_t2899168071::get_offset_of_sin6_family_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t2899168071::get_offset_of_sin6_port_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t2899168071::get_offset_of_sin6_flowinfo_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t2899168071::get_offset_of_sin6_addr_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t2899168071::get_offset_of_sin6_scope_id_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1317 = { sizeof (in6_addr_t4035827331)+ sizeof (Il2CppObject), sizeof(in6_addr_t4035827331_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1317[1] = 
{
	in6_addr_t4035827331::get_offset_of_u6_addr8_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1318 = { sizeof (sockaddr_ll_t1681025498)+ sizeof (Il2CppObject), sizeof(sockaddr_ll_t1681025498_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1318[7] = 
{
	sockaddr_ll_t1681025498::get_offset_of_sll_family_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_ll_t1681025498::get_offset_of_sll_protocol_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_ll_t1681025498::get_offset_of_sll_ifindex_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_ll_t1681025498::get_offset_of_sll_hatype_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_ll_t1681025498::get_offset_of_sll_pkttype_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_ll_t1681025498::get_offset_of_sll_halen_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_ll_t1681025498::get_offset_of_sll_addr_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1319 = { sizeof (LinuxArpHardware_t4257672401)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1319[11] = 
{
	LinuxArpHardware_t4257672401::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1320 = { sizeof (ifaddrs_t937751619)+ sizeof (Il2CppObject), sizeof(ifaddrs_t937751619_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1320[7] = 
{
	ifaddrs_t937751619::get_offset_of_ifa_next_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t937751619::get_offset_of_ifa_name_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t937751619::get_offset_of_ifa_flags_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t937751619::get_offset_of_ifa_addr_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t937751619::get_offset_of_ifa_netmask_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t937751619::get_offset_of_ifa_dstaddr_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ifaddrs_t937751619::get_offset_of_ifa_data_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1321 = { sizeof (sockaddr_t3329842375)+ sizeof (Il2CppObject), sizeof(sockaddr_t3329842375 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1321[2] = 
{
	sockaddr_t3329842375::get_offset_of_sa_len_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_t3329842375::get_offset_of_sa_family_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1322 = { sizeof (sockaddr_in_t2792443317)+ sizeof (Il2CppObject), sizeof(sockaddr_in_t2792443317 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1322[4] = 
{
	sockaddr_in_t2792443317::get_offset_of_sin_len_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in_t2792443317::get_offset_of_sin_family_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in_t2792443317::get_offset_of_sin_port_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in_t2792443317::get_offset_of_sin_addr_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1323 = { sizeof (in6_addr_t1014645363)+ sizeof (Il2CppObject), sizeof(in6_addr_t1014645363_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1323[1] = 
{
	in6_addr_t1014645363::get_offset_of_u6_addr8_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1324 = { sizeof (sockaddr_in6_t834146887)+ sizeof (Il2CppObject), sizeof(sockaddr_in6_t834146887_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1324[6] = 
{
	sockaddr_in6_t834146887::get_offset_of_sin6_len_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t834146887::get_offset_of_sin6_family_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t834146887::get_offset_of_sin6_port_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t834146887::get_offset_of_sin6_flowinfo_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t834146887::get_offset_of_sin6_addr_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_in6_t834146887::get_offset_of_sin6_scope_id_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1325 = { sizeof (sockaddr_dl_t3955242742)+ sizeof (Il2CppObject), sizeof(sockaddr_dl_t3955242742_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1325[8] = 
{
	sockaddr_dl_t3955242742::get_offset_of_sdl_len_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t3955242742::get_offset_of_sdl_family_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t3955242742::get_offset_of_sdl_index_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t3955242742::get_offset_of_sdl_type_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t3955242742::get_offset_of_sdl_nlen_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t3955242742::get_offset_of_sdl_alen_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t3955242742::get_offset_of_sdl_slen_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	sockaddr_dl_t3955242742::get_offset_of_sdl_data_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1326 = { sizeof (MacOsArpHardware_t650784048)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1326[7] = 
{
	MacOsArpHardware_t650784048::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1327 = { sizeof (NetworkInformationException_t1863186723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1327[1] = 
{
	NetworkInformationException_t1863186723::get_offset_of_error_code_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1328 = { sizeof (NetworkInterfaceComponent_t2376191102)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1328[3] = 
{
	NetworkInterfaceComponent_t2376191102::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1329 = { sizeof (NetworkInterface_t63927633), -1, sizeof(NetworkInterface_t63927633_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1329[2] = 
{
	NetworkInterface_t63927633_StaticFields::get_offset_of_windowsVer51_0(),
	NetworkInterface_t63927633_StaticFields::get_offset_of_runningOnUnix_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1330 = { sizeof (UnixNetworkInterface_t1000704527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1330[5] = 
{
	UnixNetworkInterface_t1000704527::get_offset_of_name_2(),
	UnixNetworkInterface_t1000704527::get_offset_of_index_3(),
	UnixNetworkInterface_t1000704527::get_offset_of_addresses_4(),
	UnixNetworkInterface_t1000704527::get_offset_of_macAddress_5(),
	UnixNetworkInterface_t1000704527::get_offset_of_type_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1331 = { sizeof (LinuxNetworkInterface_t3864470295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1331[3] = 
{
	LinuxNetworkInterface_t3864470295::get_offset_of_iface_path_7(),
	LinuxNetworkInterface_t3864470295::get_offset_of_iface_operstate_path_8(),
	LinuxNetworkInterface_t3864470295::get_offset_of_iface_flags_path_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1332 = { sizeof (MacOsNetworkInterface_t1454185290), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1333 = { sizeof (Win32NetworkInterface2_t482839970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1333[5] = 
{
	Win32NetworkInterface2_t482839970::get_offset_of_addr_2(),
	Win32NetworkInterface2_t482839970::get_offset_of_mib4_3(),
	Win32NetworkInterface2_t482839970::get_offset_of_mib6_4(),
	Win32NetworkInterface2_t482839970::get_offset_of_ip4stats_5(),
	Win32NetworkInterface2_t482839970::get_offset_of_ip_if_props_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1334 = { sizeof (NetworkInterfaceType_t4226883065)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1334[26] = 
{
	NetworkInterfaceType_t4226883065::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1335 = { sizeof (OperationalStatus_t2833345236)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1335[8] = 
{
	OperationalStatus_t2833345236::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1336 = { sizeof (AlignmentUnion_t707470070)+ sizeof (Il2CppObject), sizeof(AlignmentUnion_t707470070 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1336[3] = 
{
	AlignmentUnion_t707470070::get_offset_of_Alignment_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AlignmentUnion_t707470070::get_offset_of_Length_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AlignmentUnion_t707470070::get_offset_of_IfIndex_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1337 = { sizeof (Win32_IP_ADAPTER_ADDRESSES_t680756680), sizeof(Win32_IP_ADAPTER_ADDRESSES_t680756680_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1337[18] = 
{
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_Alignment_0(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_Next_1(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_AdapterName_2(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_FirstUnicastAddress_3(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_FirstAnycastAddress_4(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_FirstMulticastAddress_5(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_FirstDnsServerAddress_6(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_DnsSuffix_7(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_Description_8(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_FriendlyName_9(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_PhysicalAddress_10(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_PhysicalAddressLength_11(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_Flags_12(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_Mtu_13(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_IfType_14(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_OperStatus_15(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_Ipv6IfIndex_16(),
	Win32_IP_ADAPTER_ADDRESSES_t680756680::get_offset_of_ZoneIndices_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1338 = { sizeof (Win32_MIB_IFROW_t4215928996)+ sizeof (Il2CppObject), sizeof(Win32_MIB_IFROW_t4215928996_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1338[24] = 
{
	Win32_MIB_IFROW_t4215928996::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_Index_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_Type_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_Mtu_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_Speed_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_PhysAddrLen_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_PhysAddr_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_AdminStatus_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_OperStatus_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_LastChange_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_InOctets_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_InUcastPkts_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_InNUcastPkts_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_InDiscards_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_InErrors_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_InUnknownProtos_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_OutOctets_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_OutUcastPkts_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_OutNUcastPkts_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_OutDiscards_19() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_OutErrors_20() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_OutQLen_21() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_DescrLen_22() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Win32_MIB_IFROW_t4215928996::get_offset_of_Descr_23() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1339 = { sizeof (ProtocolViolationException_t4263317570), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1340 = { sizeof (AuthenticatedStream_t1183414097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1340[2] = 
{
	AuthenticatedStream_t1183414097::get_offset_of_innerStream_2(),
	AuthenticatedStream_t1183414097::get_offset_of_leaveStreamOpen_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1341 = { sizeof (AuthenticationLevel_t2424130044)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1341[4] = 
{
	AuthenticationLevel_t2424130044::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1342 = { sizeof (SecurityProtocolType_t3099771628)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1342[3] = 
{
	SecurityProtocolType_t3099771628::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1343 = { sizeof (SslStream_t1853163792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1343[3] = 
{
	SslStream_t1853163792::get_offset_of_ssl_stream_4(),
	SslStream_t1853163792::get_offset_of_validation_callback_5(),
	SslStream_t1853163792::get_offset_of_selection_callback_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1344 = { sizeof (U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t1358332250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1344[2] = 
{
	U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t1358332250::get_offset_of_clientCertificates_0(),
	U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t1358332250::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1345 = { sizeof (SslPolicyErrors_t1928581431)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1345[5] = 
{
	SslPolicyErrors_t1928581431::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1346 = { sizeof (ServicePoint_t2765344313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1346[17] = 
{
	ServicePoint_t2765344313::get_offset_of_uri_0(),
	ServicePoint_t2765344313::get_offset_of_connectionLimit_1(),
	ServicePoint_t2765344313::get_offset_of_maxIdleTime_2(),
	ServicePoint_t2765344313::get_offset_of_currentConnections_3(),
	ServicePoint_t2765344313::get_offset_of_idleSince_4(),
	ServicePoint_t2765344313::get_offset_of_protocolVersion_5(),
	ServicePoint_t2765344313::get_offset_of_certificate_6(),
	ServicePoint_t2765344313::get_offset_of_clientCertificate_7(),
	ServicePoint_t2765344313::get_offset_of_host_8(),
	ServicePoint_t2765344313::get_offset_of_usesProxy_9(),
	ServicePoint_t2765344313::get_offset_of_groups_10(),
	ServicePoint_t2765344313::get_offset_of_sendContinue_11(),
	ServicePoint_t2765344313::get_offset_of_useConnect_12(),
	ServicePoint_t2765344313::get_offset_of_locker_13(),
	ServicePoint_t2765344313::get_offset_of_hostE_14(),
	ServicePoint_t2765344313::get_offset_of_useNagle_15(),
	ServicePoint_t2765344313::get_offset_of_endPointCallback_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1347 = { sizeof (ServicePointManager_t745663000), -1, sizeof(ServicePointManager_t745663000_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1347[11] = 
{
	ServicePointManager_t745663000_StaticFields::get_offset_of_servicePoints_0(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_policy_1(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_defaultConnectionLimit_2(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_maxServicePointIdleTime_3(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_maxServicePoints_4(),
	ServicePointManager_t745663000_StaticFields::get_offset_of__checkCRL_5(),
	ServicePointManager_t745663000_StaticFields::get_offset_of__securityProtocol_6(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_expectContinue_7(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_useNagle_8(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_server_cert_cb_9(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_manager_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1348 = { sizeof (SPKey_t1552752485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1348[2] = 
{
	SPKey_t1552752485::get_offset_of_uri_0(),
	SPKey_t1552752485::get_offset_of_use_connect_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1349 = { sizeof (ChainValidationHelper_t1155887809), -1, sizeof(ChainValidationHelper_t1155887809_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1349[4] = 
{
	ChainValidationHelper_t1155887809::get_offset_of_sender_0(),
	ChainValidationHelper_t1155887809::get_offset_of_host_1(),
	ChainValidationHelper_t1155887809_StaticFields::get_offset_of_is_macosx_2(),
	ChainValidationHelper_t1155887809_StaticFields::get_offset_of_s_flags_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1350 = { sizeof (SocketAddress_t838303055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1350[1] = 
{
	SocketAddress_t838303055::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1351 = { sizeof (AddressFamily_t303362630)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1351[32] = 
{
	AddressFamily_t303362630::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1352 = { sizeof (LingerOption_t1165263720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1352[2] = 
{
	LingerOption_t1165263720::get_offset_of_enabled_0(),
	LingerOption_t1165263720::get_offset_of_seconds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1353 = { sizeof (MulticastOption_t2505469155), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1354 = { sizeof (NetworkStream_t581172200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1354[6] = 
{
	NetworkStream_t581172200::get_offset_of_access_2(),
	NetworkStream_t581172200::get_offset_of_socket_3(),
	NetworkStream_t581172200::get_offset_of_owns_socket_4(),
	NetworkStream_t581172200::get_offset_of_readable_5(),
	NetworkStream_t581172200::get_offset_of_writeable_6(),
	NetworkStream_t581172200::get_offset_of_disposed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1355 = { sizeof (ProtocolType_t2178963134)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1355[26] = 
{
	ProtocolType_t2178963134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1356 = { sizeof (SelectMode_t3413969319)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1356[4] = 
{
	SelectMode_t3413969319::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1357 = { sizeof (Socket_t3821512045), -1, sizeof(Socket_t3821512045_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1357[22] = 
{
	Socket_t3821512045::get_offset_of_readQ_0(),
	Socket_t3821512045::get_offset_of_writeQ_1(),
	Socket_t3821512045::get_offset_of_islistening_2(),
	Socket_t3821512045::get_offset_of_MinListenPort_3(),
	Socket_t3821512045::get_offset_of_MaxListenPort_4(),
	Socket_t3821512045_StaticFields::get_offset_of_ipv4Supported_5(),
	Socket_t3821512045_StaticFields::get_offset_of_ipv6Supported_6(),
	Socket_t3821512045::get_offset_of_linger_timeout_7(),
	Socket_t3821512045::get_offset_of_socket_8(),
	Socket_t3821512045::get_offset_of_address_family_9(),
	Socket_t3821512045::get_offset_of_socket_type_10(),
	Socket_t3821512045::get_offset_of_protocol_type_11(),
	Socket_t3821512045::get_offset_of_blocking_12(),
	Socket_t3821512045::get_offset_of_blocking_thread_13(),
	Socket_t3821512045::get_offset_of_isbound_14(),
	Socket_t3821512045_StaticFields::get_offset_of_current_bind_count_15(),
	Socket_t3821512045::get_offset_of_max_bind_count_16(),
	Socket_t3821512045::get_offset_of_connected_17(),
	Socket_t3821512045::get_offset_of_closed_18(),
	Socket_t3821512045::get_offset_of_disposed_19(),
	Socket_t3821512045::get_offset_of_seed_endpoint_20(),
	Socket_t3821512045_StaticFields::get_offset_of_check_socket_policy_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1358 = { sizeof (SocketOperation_t3328960782)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1358[15] = 
{
	SocketOperation_t3328960782::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1359 = { sizeof (SocketAsyncResult_t2959281146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1359[25] = 
{
	SocketAsyncResult_t2959281146::get_offset_of_Sock_0(),
	SocketAsyncResult_t2959281146::get_offset_of_handle_1(),
	SocketAsyncResult_t2959281146::get_offset_of_state_2(),
	SocketAsyncResult_t2959281146::get_offset_of_callback_3(),
	SocketAsyncResult_t2959281146::get_offset_of_waithandle_4(),
	SocketAsyncResult_t2959281146::get_offset_of_delayedException_5(),
	SocketAsyncResult_t2959281146::get_offset_of_EndPoint_6(),
	SocketAsyncResult_t2959281146::get_offset_of_Buffer_7(),
	SocketAsyncResult_t2959281146::get_offset_of_Offset_8(),
	SocketAsyncResult_t2959281146::get_offset_of_Size_9(),
	SocketAsyncResult_t2959281146::get_offset_of_SockFlags_10(),
	SocketAsyncResult_t2959281146::get_offset_of_AcceptSocket_11(),
	SocketAsyncResult_t2959281146::get_offset_of_Addresses_12(),
	SocketAsyncResult_t2959281146::get_offset_of_Port_13(),
	SocketAsyncResult_t2959281146::get_offset_of_Buffers_14(),
	SocketAsyncResult_t2959281146::get_offset_of_ReuseSocket_15(),
	SocketAsyncResult_t2959281146::get_offset_of_acc_socket_16(),
	SocketAsyncResult_t2959281146::get_offset_of_total_17(),
	SocketAsyncResult_t2959281146::get_offset_of_completed_sync_18(),
	SocketAsyncResult_t2959281146::get_offset_of_completed_19(),
	SocketAsyncResult_t2959281146::get_offset_of_blocking_20(),
	SocketAsyncResult_t2959281146::get_offset_of_error_21(),
	SocketAsyncResult_t2959281146::get_offset_of_operation_22(),
	SocketAsyncResult_t2959281146::get_offset_of_ares_23(),
	SocketAsyncResult_t2959281146::get_offset_of_EndCalled_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1360 = { sizeof (Worker_t1317165246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1360[3] = 
{
	Worker_t1317165246::get_offset_of_result_0(),
	Worker_t1317165246::get_offset_of_requireSocketSecurity_1(),
	Worker_t1317165246::get_offset_of_send_so_far_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1361 = { sizeof (SocketAsyncCall_t3737776727), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1362 = { sizeof (SocketError_t307542793)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1362[48] = 
{
	SocketError_t307542793::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1363 = { sizeof (SocketException_t1618573604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1364 = { sizeof (SocketFlags_t2353657790)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1364[11] = 
{
	SocketFlags_t2353657790::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1365 = { sizeof (SocketOptionLevel_t1505247880)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1365[6] = 
{
	SocketOptionLevel_t1505247880::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1366 = { sizeof (SocketOptionName_t1089121285)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1366[44] = 
{
	SocketOptionName_t1089121285::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1367 = { sizeof (SocketShutdown_t3247039417)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1367[4] = 
{
	SocketShutdown_t3247039417::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1368 = { sizeof (SocketType_t1143498533)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1368[7] = 
{
	SocketType_t1143498533::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1369 = { sizeof (TcpClient_t408947970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1369[4] = 
{
	TcpClient_t408947970::get_offset_of_stream_0(),
	TcpClient_t408947970::get_offset_of_active_1(),
	TcpClient_t408947970::get_offset_of_client_2(),
	TcpClient_t408947970::get_offset_of_disposed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1370 = { sizeof (WebAsyncResult_t905414499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1370[17] = 
{
	WebAsyncResult_t905414499::get_offset_of_handle_0(),
	WebAsyncResult_t905414499::get_offset_of_synch_1(),
	WebAsyncResult_t905414499::get_offset_of_isCompleted_2(),
	WebAsyncResult_t905414499::get_offset_of_cb_3(),
	WebAsyncResult_t905414499::get_offset_of_state_4(),
	WebAsyncResult_t905414499::get_offset_of_nbytes_5(),
	WebAsyncResult_t905414499::get_offset_of_innerAsyncResult_6(),
	WebAsyncResult_t905414499::get_offset_of_callbackDone_7(),
	WebAsyncResult_t905414499::get_offset_of_exc_8(),
	WebAsyncResult_t905414499::get_offset_of_response_9(),
	WebAsyncResult_t905414499::get_offset_of_writeStream_10(),
	WebAsyncResult_t905414499::get_offset_of_buffer_11(),
	WebAsyncResult_t905414499::get_offset_of_offset_12(),
	WebAsyncResult_t905414499::get_offset_of_size_13(),
	WebAsyncResult_t905414499::get_offset_of_locker_14(),
	WebAsyncResult_t905414499::get_offset_of_EndCalled_15(),
	WebAsyncResult_t905414499::get_offset_of_AsyncWriteAll_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1371 = { sizeof (WebClient_t1432723993), -1, sizeof(WebClient_t1432723993_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1371[12] = 
{
	WebClient_t1432723993_StaticFields::get_offset_of_urlEncodedCType_4(),
	WebClient_t1432723993_StaticFields::get_offset_of_hexBytes_5(),
	WebClient_t1432723993::get_offset_of_credentials_6(),
	WebClient_t1432723993::get_offset_of_headers_7(),
	WebClient_t1432723993::get_offset_of_responseHeaders_8(),
	WebClient_t1432723993::get_offset_of_is_busy_9(),
	WebClient_t1432723993::get_offset_of_async_10(),
	WebClient_t1432723993::get_offset_of_async_thread_11(),
	WebClient_t1432723993::get_offset_of_encoding_12(),
	WebClient_t1432723993::get_offset_of_proxy_13(),
	WebClient_t1432723993::get_offset_of_DownloadProgressChanged_14(),
	WebClient_t1432723993::get_offset_of_UploadValuesCompleted_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1372 = { sizeof (ReadState_t657568301)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1372[5] = 
{
	ReadState_t657568301::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1373 = { sizeof (WebConnection_t324679648), -1, sizeof(WebConnection_t324679648_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1373[32] = 
{
	WebConnection_t324679648::get_offset_of_sPoint_0(),
	WebConnection_t324679648::get_offset_of_nstream_1(),
	WebConnection_t324679648::get_offset_of_socket_2(),
	WebConnection_t324679648::get_offset_of_socketLock_3(),
	WebConnection_t324679648::get_offset_of_status_4(),
	WebConnection_t324679648::get_offset_of_initConn_5(),
	WebConnection_t324679648::get_offset_of_keepAlive_6(),
	WebConnection_t324679648::get_offset_of_buffer_7(),
	WebConnection_t324679648_StaticFields::get_offset_of_readDoneDelegate_8(),
	WebConnection_t324679648::get_offset_of_abortHandler_9(),
	WebConnection_t324679648::get_offset_of_abortHelper_10(),
	WebConnection_t324679648::get_offset_of_readState_11(),
	WebConnection_t324679648::get_offset_of_Data_12(),
	WebConnection_t324679648::get_offset_of_chunkedRead_13(),
	WebConnection_t324679648::get_offset_of_chunkStream_14(),
	WebConnection_t324679648::get_offset_of_queue_15(),
	WebConnection_t324679648::get_offset_of_reused_16(),
	WebConnection_t324679648::get_offset_of_position_17(),
	WebConnection_t324679648::get_offset_of_busy_18(),
	WebConnection_t324679648::get_offset_of_priority_request_19(),
	WebConnection_t324679648::get_offset_of_ntlm_credentials_20(),
	WebConnection_t324679648::get_offset_of_ntlm_authenticated_21(),
	WebConnection_t324679648::get_offset_of_unsafe_sharing_22(),
	WebConnection_t324679648::get_offset_of_ssl_23(),
	WebConnection_t324679648::get_offset_of_certsAvailable_24(),
	WebConnection_t324679648::get_offset_of_connect_exception_25(),
	WebConnection_t324679648_StaticFields::get_offset_of_classLock_26(),
	WebConnection_t324679648_StaticFields::get_offset_of_sslStream_27(),
	WebConnection_t324679648_StaticFields::get_offset_of_piClient_28(),
	WebConnection_t324679648_StaticFields::get_offset_of_piServer_29(),
	WebConnection_t324679648_StaticFields::get_offset_of_piTrustFailure_30(),
	WebConnection_t324679648_StaticFields::get_offset_of_method_GetSecurityPolicyFromNonMainThread_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1374 = { sizeof (AbortHelper_t2895113041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1374[1] = 
{
	AbortHelper_t2895113041::get_offset_of_Connection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1375 = { sizeof (WebConnectionData_t3550260432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1375[7] = 
{
	WebConnectionData_t3550260432::get_offset_of_request_0(),
	WebConnectionData_t3550260432::get_offset_of_StatusCode_1(),
	WebConnectionData_t3550260432::get_offset_of_StatusDescription_2(),
	WebConnectionData_t3550260432::get_offset_of_Headers_3(),
	WebConnectionData_t3550260432::get_offset_of_Version_4(),
	WebConnectionData_t3550260432::get_offset_of_stream_5(),
	WebConnectionData_t3550260432::get_offset_of_Challenge_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1376 = { sizeof (WebConnectionGroup_t3242458773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1376[5] = 
{
	WebConnectionGroup_t3242458773::get_offset_of_sPoint_0(),
	WebConnectionGroup_t3242458773::get_offset_of_name_1(),
	WebConnectionGroup_t3242458773::get_offset_of_connections_2(),
	WebConnectionGroup_t3242458773::get_offset_of_rnd_3(),
	WebConnectionGroup_t3242458773::get_offset_of_queue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1377 = { sizeof (WebConnectionStream_t1922483508), -1, sizeof(WebConnectionStream_t1922483508_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1377[27] = 
{
	WebConnectionStream_t1922483508_StaticFields::get_offset_of_crlf_2(),
	WebConnectionStream_t1922483508::get_offset_of_isRead_3(),
	WebConnectionStream_t1922483508::get_offset_of_cnc_4(),
	WebConnectionStream_t1922483508::get_offset_of_request_5(),
	WebConnectionStream_t1922483508::get_offset_of_readBuffer_6(),
	WebConnectionStream_t1922483508::get_offset_of_readBufferOffset_7(),
	WebConnectionStream_t1922483508::get_offset_of_readBufferSize_8(),
	WebConnectionStream_t1922483508::get_offset_of_contentLength_9(),
	WebConnectionStream_t1922483508::get_offset_of_totalRead_10(),
	WebConnectionStream_t1922483508::get_offset_of_totalWritten_11(),
	WebConnectionStream_t1922483508::get_offset_of_nextReadCalled_12(),
	WebConnectionStream_t1922483508::get_offset_of_pendingReads_13(),
	WebConnectionStream_t1922483508::get_offset_of_pendingWrites_14(),
	WebConnectionStream_t1922483508::get_offset_of_pending_15(),
	WebConnectionStream_t1922483508::get_offset_of_allowBuffering_16(),
	WebConnectionStream_t1922483508::get_offset_of_sendChunked_17(),
	WebConnectionStream_t1922483508::get_offset_of_writeBuffer_18(),
	WebConnectionStream_t1922483508::get_offset_of_requestWritten_19(),
	WebConnectionStream_t1922483508::get_offset_of_headers_20(),
	WebConnectionStream_t1922483508::get_offset_of_disposed_21(),
	WebConnectionStream_t1922483508::get_offset_of_headersSent_22(),
	WebConnectionStream_t1922483508::get_offset_of_locker_23(),
	WebConnectionStream_t1922483508::get_offset_of_initRead_24(),
	WebConnectionStream_t1922483508::get_offset_of_read_eof_25(),
	WebConnectionStream_t1922483508::get_offset_of_complete_request_written_26(),
	WebConnectionStream_t1922483508::get_offset_of_read_timeout_27(),
	WebConnectionStream_t1922483508::get_offset_of_write_timeout_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1378 = { sizeof (WebException_t3368933679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1378[2] = 
{
	WebException_t3368933679::get_offset_of_response_12(),
	WebException_t3368933679::get_offset_of_status_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1379 = { sizeof (WebExceptionStatus_t1169373531)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1379[22] = 
{
	WebExceptionStatus_t1169373531::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1380 = { sizeof (WebHeaderCollection_t3028142837), -1, sizeof(WebHeaderCollection_t3028142837_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1380[5] = 
{
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_restricted_12(),
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_multiValue_13(),
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_restricted_response_14(),
	WebHeaderCollection_t3028142837::get_offset_of_internallyCreated_15(),
	WebHeaderCollection_t3028142837_StaticFields::get_offset_of_allowed_chars_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1381 = { sizeof (WebProxy_t1169192840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1381[5] = 
{
	WebProxy_t1169192840::get_offset_of_address_0(),
	WebProxy_t1169192840::get_offset_of_bypassOnLocal_1(),
	WebProxy_t1169192840::get_offset_of_bypassList_2(),
	WebProxy_t1169192840::get_offset_of_credentials_3(),
	WebProxy_t1169192840::get_offset_of_useDefaultCredentials_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1382 = { sizeof (WebRequest_t1365124353), -1, sizeof(WebRequest_t1365124353_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1382[5] = 
{
	WebRequest_t1365124353_StaticFields::get_offset_of_prefixes_1(),
	WebRequest_t1365124353_StaticFields::get_offset_of_isDefaultWebProxySet_2(),
	WebRequest_t1365124353_StaticFields::get_offset_of_defaultWebProxy_3(),
	WebRequest_t1365124353::get_offset_of_authentication_level_4(),
	WebRequest_t1365124353_StaticFields::get_offset_of_lockobj_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1383 = { sizeof (WebResponse_t1895226051), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1384 = { sizeof (SslProtocols_t894678499)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1384[6] = 
{
	SslProtocols_t894678499::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1385 = { sizeof (AsnDecodeStatus_t1962003286)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1385[7] = 
{
	AsnDecodeStatus_t1962003286::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1386 = { sizeof (AsnEncodedData_t463456204), -1, sizeof(AsnEncodedData_t463456204_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1386[3] = 
{
	AsnEncodedData_t463456204::get_offset_of__oid_0(),
	AsnEncodedData_t463456204::get_offset_of__raw_1(),
	AsnEncodedData_t463456204_StaticFields::get_offset_of_U3CU3Ef__switchU24map13_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1387 = { sizeof (OidCollection_t3790243618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1387[2] = 
{
	OidCollection_t3790243618::get_offset_of__list_0(),
	OidCollection_t3790243618::get_offset_of__readOnly_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1388 = { sizeof (Oid_t3221867120), -1, sizeof(Oid_t3221867120_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1388[3] = 
{
	Oid_t3221867120::get_offset_of__value_0(),
	Oid_t3221867120::get_offset_of__name_1(),
	Oid_t3221867120_StaticFields::get_offset_of_U3CU3Ef__switchU24map14_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1389 = { sizeof (OidEnumerator_t3674631724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1389[2] = 
{
	OidEnumerator_t3674631724::get_offset_of__collection_0(),
	OidEnumerator_t3674631724::get_offset_of__position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1390 = { sizeof (OpenFlags_t2370524385)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1390[6] = 
{
	OpenFlags_t2370524385::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1391 = { sizeof (OSX509Certificates_t3584809896), -1, sizeof(OSX509Certificates_t3584809896_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1391[1] = 
{
	OSX509Certificates_t3584809896_StaticFields::get_offset_of_sslsecpolicy_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1392 = { sizeof (SecTrustResult_t1984565408)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1392[9] = 
{
	SecTrustResult_t1984565408::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1393 = { sizeof (PublicKey_t870392), -1, sizeof(PublicKey_t870392_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1393[5] = 
{
	PublicKey_t870392::get_offset_of__key_0(),
	PublicKey_t870392::get_offset_of__keyValue_1(),
	PublicKey_t870392::get_offset_of__params_2(),
	PublicKey_t870392::get_offset_of__oid_3(),
	PublicKey_t870392_StaticFields::get_offset_of_U3CU3Ef__switchU24map16_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1394 = { sizeof (StoreLocation_t1570828128)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1394[3] = 
{
	StoreLocation_t1570828128::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1395 = { sizeof (StoreName_t2183514610)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1395[9] = 
{
	StoreName_t2183514610::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1396 = { sizeof (X500DistinguishedName_t452415348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1396[1] = 
{
	X500DistinguishedName_t452415348::get_offset_of_name_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1397 = { sizeof (X500DistinguishedNameFlags_t2005802885)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1397[11] = 
{
	X500DistinguishedNameFlags_t2005802885::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1398 = { sizeof (X509BasicConstraintsExtension_t1562873317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1398[6] = 
{
	0,
	0,
	X509BasicConstraintsExtension_t1562873317::get_offset_of__certificateAuthority_6(),
	X509BasicConstraintsExtension_t1562873317::get_offset_of__hasPathLengthConstraint_7(),
	X509BasicConstraintsExtension_t1562873317::get_offset_of__pathLengthConstraint_8(),
	X509BasicConstraintsExtension_t1562873317::get_offset_of__status_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1399 = { sizeof (X509Certificate2Collection_t1108969367), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
