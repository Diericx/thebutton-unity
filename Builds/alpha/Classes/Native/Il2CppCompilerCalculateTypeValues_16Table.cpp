﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordProt173216930.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RSASslSign389653629.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RSASslSig1282301050.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityC3722381418.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityP2290372928.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityPr155967584.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ServerCon3823737132.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Validatio1782558132.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslClient3918817353.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslCipher1404755603.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslHandsh3044322977.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStreamB934199321.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStream1610391122.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsCipherS396038680.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsClient2311449551.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsExcepti583514812.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsServerS403340211.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsStream4089752859.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake4001384466.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3938752374.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2540099417.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2537917473.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake4150496570.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3939745042.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2939633944.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3808761250.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_905088469.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2187269356.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1869592958.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1289300668.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_530021076.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2172608670.h"
#include "Mono_Security_Mono_Math_Prime_PrimalityTest572679901.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certificat989458295.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certifica3318447433.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certifica3721235490.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKe1663566523.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1892466092.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2038352954.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2540610921.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3672778804.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2896841275.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2866209745.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3672778802.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1703410334.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1957337331.h"
#include "System_Configuration_U3CModuleU3E3783534214.h"
#include "System_Configuration_System_Configuration_Provider2882126354.h"
#include "System_Configuration_System_Configuration_Provider2548499159.h"
#include "System_Configuration_System_Configuration_ClientCo4294641134.h"
#include "System_Configuration_System_Configuration_ConfigNa2395569530.h"
#include "System_Configuration_System_Configuration_ConfigInf546730838.h"
#include "System_Configuration_System_Configuration_Configur3335372970.h"
#include "System_Configuration_System_Configuration_Configur3250313246.h"
#include "System_Configuration_System_Configuration_Configur3860111898.h"
#include "System_Configuration_System_Configuration_Configur2811353736.h"
#include "System_Configuration_System_Configuration_Configur1776195828.h"
#include "System_Configuration_System_Configuration_ElementMa997038224.h"
#include "System_Configuration_System_Configuration_Configur1911180302.h"
#include "System_Configuration_System_Configuration_Configur3305291330.h"
#include "System_Configuration_System_Configuration_Configur1806001494.h"
#include "System_Configuration_System_Configuration_Configur1362721126.h"
#include "System_Configuration_System_Configuration_Configur2625210096.h"
#include "System_Configuration_System_Configuration_Configur1895107553.h"
#include "System_Configuration_System_Configuration_Configur1903842989.h"
#include "System_Configuration_System_Configuration_Configura131834733.h"
#include "System_Configuration_System_Configuration_Configur1011762925.h"
#include "System_Configuration_System_Configuration_Configur2608608455.h"
#include "System_Configuration_System_Configuration_Configur2048066811.h"
#include "System_Configuration_System_Configuration_Configur3655647199.h"
#include "System_Configuration_System_Configuration_Configur3473514151.h"
#include "System_Configuration_System_Configuration_Configur3219689025.h"
#include "System_Configuration_System_Configuration_Configura700320212.h"
#include "System_Configuration_System_Configuration_Configur2600766927.h"
#include "System_Configuration_System_Configuration_Configur4261113299.h"
#include "System_Configuration_System_Configuration_Configur1795270620.h"
#include "System_Configuration_System_Configuration_Configur2230982736.h"
#include "System_Configuration_System_Configuration_Configura575145286.h"
#include "System_Configuration_System_Configuration_Configur1204907851.h"
#include "System_Configuration_System_Configuration_Configur1007519140.h"
#include "System_Configuration_System_Configuration_Configura210547623.h"
#include "System_Configuration_ConfigXmlTextReader3212066157.h"
#include "System_Configuration_System_Configuration_DefaultS3840532724.h"
#include "System_Configuration_System_Configuration_DefaultVa300527515.h"
#include "System_Configuration_System_Configuration_ElementI3165583784.h"
#include "System_Configuration_System_Configuration_ExeConfi1419586304.h"
#include "System_Configuration_System_Configuration_IgnoreSec681509237.h"
#include "System_Configuration_System_Configuration_Internal3846641927.h"
#include "System_Configuration_System_Configuration_Internal2108740756.h"
#include "System_Configuration_System_Configuration_InternalC547577555.h"
#include "System_Configuration_System_Configuration_ExeConfi2778769322.h"
#include "System_Configuration_System_Configuration_InternalC547578517.h"
#include "System_Configuration_System_Configuration_Property2089433965.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (SendRecordAsyncResult_t173216930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1600[7] = 
{
	SendRecordAsyncResult_t173216930::get_offset_of_locker_0(),
	SendRecordAsyncResult_t173216930::get_offset_of__userCallback_1(),
	SendRecordAsyncResult_t173216930::get_offset_of__userState_2(),
	SendRecordAsyncResult_t173216930::get_offset_of__asyncException_3(),
	SendRecordAsyncResult_t173216930::get_offset_of_handle_4(),
	SendRecordAsyncResult_t173216930::get_offset_of__message_5(),
	SendRecordAsyncResult_t173216930::get_offset_of_completed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (RSASslSignatureDeformatter_t389653629), -1, sizeof(RSASslSignatureDeformatter_t389653629_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1601[3] = 
{
	RSASslSignatureDeformatter_t389653629::get_offset_of_key_0(),
	RSASslSignatureDeformatter_t389653629::get_offset_of_hash_1(),
	RSASslSignatureDeformatter_t389653629_StaticFields::get_offset_of_U3CU3Ef__switchU24map15_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (RSASslSignatureFormatter_t1282301050), -1, sizeof(RSASslSignatureFormatter_t1282301050_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1602[3] = 
{
	RSASslSignatureFormatter_t1282301050::get_offset_of_key_0(),
	RSASslSignatureFormatter_t1282301050::get_offset_of_hash_1(),
	RSASslSignatureFormatter_t1282301050_StaticFields::get_offset_of_U3CU3Ef__switchU24map16_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { sizeof (SecurityCompressionType_t3722381418)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1603[3] = 
{
	SecurityCompressionType_t3722381418::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (SecurityParameters_t2290372928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1604[3] = 
{
	SecurityParameters_t2290372928::get_offset_of_cipher_0(),
	SecurityParameters_t2290372928::get_offset_of_clientWriteMAC_1(),
	SecurityParameters_t2290372928::get_offset_of_serverWriteMAC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (SecurityProtocolType_t155967584)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1605[5] = 
{
	SecurityProtocolType_t155967584::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (ServerContext_t3823737132), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (ValidationResult_t1782558132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1607[3] = 
{
	ValidationResult_t1782558132::get_offset_of_trusted_0(),
	ValidationResult_t1782558132::get_offset_of_user_denied_1(),
	ValidationResult_t1782558132::get_offset_of_error_code_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (SslClientStream_t3918817353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1608[4] = 
{
	SslClientStream_t3918817353::get_offset_of_ServerCertValidation_17(),
	SslClientStream_t3918817353::get_offset_of_ClientCertSelection_18(),
	SslClientStream_t3918817353::get_offset_of_PrivateKeySelection_19(),
	SslClientStream_t3918817353::get_offset_of_ServerCertValidation2_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (SslCipherSuite_t1404755603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1609[3] = 
{
	SslCipherSuite_t1404755603::get_offset_of_pad1_21(),
	SslCipherSuite_t1404755603::get_offset_of_pad2_22(),
	SslCipherSuite_t1404755603::get_offset_of_header_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (SslHandshakeHash_t3044322977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1610[8] = 
{
	SslHandshakeHash_t3044322977::get_offset_of_md5_4(),
	SslHandshakeHash_t3044322977::get_offset_of_sha_5(),
	SslHandshakeHash_t3044322977::get_offset_of_hashing_6(),
	SslHandshakeHash_t3044322977::get_offset_of_secret_7(),
	SslHandshakeHash_t3044322977::get_offset_of_innerPadMD5_8(),
	SslHandshakeHash_t3044322977::get_offset_of_outerPadMD5_9(),
	SslHandshakeHash_t3044322977::get_offset_of_innerPadSHA_10(),
	SslHandshakeHash_t3044322977::get_offset_of_outerPadSHA_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (SslStreamBase_t934199321), -1, sizeof(SslStreamBase_t934199321_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1611[15] = 
{
	0,
	SslStreamBase_t934199321_StaticFields::get_offset_of_record_processing_3(),
	SslStreamBase_t934199321::get_offset_of_innerStream_4(),
	SslStreamBase_t934199321::get_offset_of_inputBuffer_5(),
	SslStreamBase_t934199321::get_offset_of_context_6(),
	SslStreamBase_t934199321::get_offset_of_protocol_7(),
	SslStreamBase_t934199321::get_offset_of_ownsStream_8(),
	SslStreamBase_t934199321::get_offset_of_disposed_9(),
	SslStreamBase_t934199321::get_offset_of_checkCertRevocationStatus_10(),
	SslStreamBase_t934199321::get_offset_of_negotiate_11(),
	SslStreamBase_t934199321::get_offset_of_read_12(),
	SslStreamBase_t934199321::get_offset_of_write_13(),
	SslStreamBase_t934199321::get_offset_of_negotiationComplete_14(),
	SslStreamBase_t934199321::get_offset_of_recbuf_15(),
	SslStreamBase_t934199321::get_offset_of_recordStream_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (InternalAsyncResult_t1610391122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1612[12] = 
{
	InternalAsyncResult_t1610391122::get_offset_of_locker_0(),
	InternalAsyncResult_t1610391122::get_offset_of__userCallback_1(),
	InternalAsyncResult_t1610391122::get_offset_of__userState_2(),
	InternalAsyncResult_t1610391122::get_offset_of__asyncException_3(),
	InternalAsyncResult_t1610391122::get_offset_of_handle_4(),
	InternalAsyncResult_t1610391122::get_offset_of_completed_5(),
	InternalAsyncResult_t1610391122::get_offset_of__bytesRead_6(),
	InternalAsyncResult_t1610391122::get_offset_of__fromWrite_7(),
	InternalAsyncResult_t1610391122::get_offset_of__proceedAfterHandshake_8(),
	InternalAsyncResult_t1610391122::get_offset_of__buffer_9(),
	InternalAsyncResult_t1610391122::get_offset_of__offset_10(),
	InternalAsyncResult_t1610391122::get_offset_of__count_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (TlsCipherSuite_t396038680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1613[2] = 
{
	TlsCipherSuite_t396038680::get_offset_of_header_21(),
	TlsCipherSuite_t396038680::get_offset_of_headerLock_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (TlsClientSettings_t2311449551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1614[4] = 
{
	TlsClientSettings_t2311449551::get_offset_of_targetHost_0(),
	TlsClientSettings_t2311449551::get_offset_of_certificates_1(),
	TlsClientSettings_t2311449551::get_offset_of_clientCertificate_2(),
	TlsClientSettings_t2311449551::get_offset_of_certificateRSA_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (TlsException_t583514812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1615[1] = 
{
	TlsException_t583514812::get_offset_of_alert_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (TlsServerSettings_t403340211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1616[8] = 
{
	TlsServerSettings_t403340211::get_offset_of_certificates_0(),
	TlsServerSettings_t403340211::get_offset_of_certificateRSA_1(),
	TlsServerSettings_t403340211::get_offset_of_rsaParameters_2(),
	TlsServerSettings_t403340211::get_offset_of_signedParams_3(),
	TlsServerSettings_t403340211::get_offset_of_distinguisedNames_4(),
	TlsServerSettings_t403340211::get_offset_of_serverKeyExchange_5(),
	TlsServerSettings_t403340211::get_offset_of_certificateRequest_6(),
	TlsServerSettings_t403340211::get_offset_of_certificateTypes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (TlsStream_t4089752859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1617[4] = 
{
	TlsStream_t4089752859::get_offset_of_canRead_2(),
	TlsStream_t4089752859::get_offset_of_canWrite_3(),
	TlsStream_t4089752859::get_offset_of_buffer_4(),
	TlsStream_t4089752859::get_offset_of_temp_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (ClientCertificateType_t4001384466)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1618[6] = 
{
	ClientCertificateType_t4001384466::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (HandshakeMessage_t3938752374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1619[4] = 
{
	HandshakeMessage_t3938752374::get_offset_of_context_6(),
	HandshakeMessage_t3938752374::get_offset_of_handshakeType_7(),
	HandshakeMessage_t3938752374::get_offset_of_contentType_8(),
	HandshakeMessage_t3938752374::get_offset_of_cache_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (HandshakeType_t2540099417)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1620[12] = 
{
	HandshakeType_t2540099417::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (TlsClientCertificate_t2537917473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1621[2] = 
{
	TlsClientCertificate_t2537917473::get_offset_of_clientCertSelected_10(),
	TlsClientCertificate_t2537917473::get_offset_of_clientCert_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (TlsClientCertificateVerify_t4150496570), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (TlsClientFinished_t3939745042), -1, sizeof(TlsClientFinished_t3939745042_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1623[1] = 
{
	TlsClientFinished_t3939745042_StaticFields::get_offset_of_Ssl3Marker_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (TlsClientHello_t2939633944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1624[1] = 
{
	TlsClientHello_t2939633944::get_offset_of_random_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (TlsClientKeyExchange_t3808761250), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (TlsServerCertificate_t905088469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1626[1] = 
{
	TlsServerCertificate_t905088469::get_offset_of_certificates_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (TlsServerCertificateRequest_t2187269356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1627[2] = 
{
	TlsServerCertificateRequest_t2187269356::get_offset_of_certificateTypes_10(),
	TlsServerCertificateRequest_t2187269356::get_offset_of_distinguisedNames_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (TlsServerFinished_t1869592958), -1, sizeof(TlsServerFinished_t1869592958_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1628[1] = 
{
	TlsServerFinished_t1869592958_StaticFields::get_offset_of_Ssl3Marker_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (TlsServerHello_t1289300668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1629[4] = 
{
	TlsServerHello_t1289300668::get_offset_of_compressionMethod_10(),
	TlsServerHello_t1289300668::get_offset_of_random_11(),
	TlsServerHello_t1289300668::get_offset_of_sessionId_12(),
	TlsServerHello_t1289300668::get_offset_of_cipherSuite_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (TlsServerHelloDone_t530021076), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (TlsServerKeyExchange_t2172608670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1631[2] = 
{
	TlsServerKeyExchange_t2172608670::get_offset_of_rsaParams_10(),
	TlsServerKeyExchange_t2172608670::get_offset_of_signedParams_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (PrimalityTest_t572679902), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (CertificateValidationCallback_t989458295), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (CertificateValidationCallback2_t3318447433), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (CertificateSelectionCallback_t3721235490), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (PrivateKeySelectionCallback_t1663566523), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305139), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1637[15] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D5_1(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D6_2(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D7_3(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D8_4(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D9_5(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D11_6(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D12_7(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D13_8(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D14_9(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D15_10(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D16_11(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D17_12(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D21_13(),
	U3CPrivateImplementationDetailsU3E_t1486305139_StaticFields::get_offset_of_U24U24fieldU2D22_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (U24ArrayTypeU243132_t1892466093)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU243132_t1892466093 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (U24ArrayTypeU24256_t2038352955)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t2038352955 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (U24ArrayTypeU2420_t540610922)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2420_t540610922 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { sizeof (U24ArrayTypeU2432_t3672778805)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2432_t3672778805 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { sizeof (U24ArrayTypeU2448_t896841276)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2448_t896841276 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { sizeof (U24ArrayTypeU2464_t2866209746)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2464_t2866209746 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { sizeof (U24ArrayTypeU2412_t3672778806)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3672778806 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (U24ArrayTypeU2416_t1703410336)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t1703410336 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (U24ArrayTypeU244_t1957337331)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU244_t1957337331 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (U3CModuleU3E_t3783534218), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { sizeof (ProviderBase_t2882126354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1654[3] = 
{
	ProviderBase_t2882126354::get_offset_of_alreadyInitialized_0(),
	ProviderBase_t2882126354::get_offset_of__description_1(),
	ProviderBase_t2882126354::get_offset_of__name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (ProviderCollection_t2548499159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1655[3] = 
{
	ProviderCollection_t2548499159::get_offset_of_lookup_0(),
	ProviderCollection_t2548499159::get_offset_of_readOnly_1(),
	ProviderCollection_t2548499159::get_offset_of_values_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (ClientConfigurationSystem_t4294641134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1656[1] = 
{
	ClientConfigurationSystem_t4294641134::get_offset_of_cfg_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (ConfigNameValueCollection_t2395569530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1657[1] = 
{
	ConfigNameValueCollection_t2395569530::get_offset_of_modified_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (ConfigInfo_t546730838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1658[6] = 
{
	ConfigInfo_t546730838::get_offset_of_Name_0(),
	ConfigInfo_t546730838::get_offset_of_TypeName_1(),
	ConfigInfo_t546730838::get_offset_of_Type_2(),
	ConfigInfo_t546730838::get_offset_of_streamName_3(),
	ConfigInfo_t546730838::get_offset_of_Parent_4(),
	ConfigInfo_t546730838::get_offset_of_ConfigHost_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { sizeof (Configuration_t3335372970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1659[12] = 
{
	Configuration_t3335372970::get_offset_of_parent_0(),
	Configuration_t3335372970::get_offset_of_elementData_1(),
	Configuration_t3335372970::get_offset_of_streamName_2(),
	Configuration_t3335372970::get_offset_of_rootSectionGroup_3(),
	Configuration_t3335372970::get_offset_of_locations_4(),
	Configuration_t3335372970::get_offset_of_rootGroup_5(),
	Configuration_t3335372970::get_offset_of_system_6(),
	Configuration_t3335372970::get_offset_of_hasFile_7(),
	Configuration_t3335372970::get_offset_of_rootNamespace_8(),
	Configuration_t3335372970::get_offset_of_configPath_9(),
	Configuration_t3335372970::get_offset_of_locationConfigPath_10(),
	Configuration_t3335372970::get_offset_of_locationSubPath_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { sizeof (ConfigurationAllowDefinition_t3250313246)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1660[5] = 
{
	ConfigurationAllowDefinition_t3250313246::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (ConfigurationAllowExeDefinition_t3860111898)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1661[5] = 
{
	ConfigurationAllowExeDefinition_t3860111898::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (ConfigurationCollectionAttribute_t2811353736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1662[5] = 
{
	ConfigurationCollectionAttribute_t2811353736::get_offset_of_addItemName_0(),
	ConfigurationCollectionAttribute_t2811353736::get_offset_of_clearItemsName_1(),
	ConfigurationCollectionAttribute_t2811353736::get_offset_of_removeItemName_2(),
	ConfigurationCollectionAttribute_t2811353736::get_offset_of_collectionType_3(),
	ConfigurationCollectionAttribute_t2811353736::get_offset_of_itemType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (ConfigurationElement_t1776195828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1663[13] = 
{
	ConfigurationElement_t1776195828::get_offset_of_rawXml_0(),
	ConfigurationElement_t1776195828::get_offset_of_modified_1(),
	ConfigurationElement_t1776195828::get_offset_of_map_2(),
	ConfigurationElement_t1776195828::get_offset_of_keyProps_3(),
	ConfigurationElement_t1776195828::get_offset_of_defaultCollection_4(),
	ConfigurationElement_t1776195828::get_offset_of_readOnly_5(),
	ConfigurationElement_t1776195828::get_offset_of_elementInfo_6(),
	ConfigurationElement_t1776195828::get_offset_of__configuration_7(),
	ConfigurationElement_t1776195828::get_offset_of_lockAllAttributesExcept_8(),
	ConfigurationElement_t1776195828::get_offset_of_lockAllElementsExcept_9(),
	ConfigurationElement_t1776195828::get_offset_of_lockAttributes_10(),
	ConfigurationElement_t1776195828::get_offset_of_lockElements_11(),
	ConfigurationElement_t1776195828::get_offset_of_lockItem_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (ElementMap_t997038224), -1, sizeof(ElementMap_t997038224_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1664[3] = 
{
	ElementMap_t997038224_StaticFields::get_offset_of_elementMaps_0(),
	ElementMap_t997038224::get_offset_of_properties_1(),
	ElementMap_t997038224::get_offset_of_collectionAttribute_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (ConfigurationElementCollection_t1911180302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1665[10] = 
{
	ConfigurationElementCollection_t1911180302::get_offset_of_list_13(),
	ConfigurationElementCollection_t1911180302::get_offset_of_removed_14(),
	ConfigurationElementCollection_t1911180302::get_offset_of_inherited_15(),
	ConfigurationElementCollection_t1911180302::get_offset_of_emitClear_16(),
	ConfigurationElementCollection_t1911180302::get_offset_of_modified_17(),
	ConfigurationElementCollection_t1911180302::get_offset_of_comparer_18(),
	ConfigurationElementCollection_t1911180302::get_offset_of_inheritedLimitIndex_19(),
	ConfigurationElementCollection_t1911180302::get_offset_of_addElementName_20(),
	ConfigurationElementCollection_t1911180302::get_offset_of_clearElementName_21(),
	ConfigurationElementCollection_t1911180302::get_offset_of_removeElementName_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (ConfigurationRemoveElement_t3305291330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1666[3] = 
{
	ConfigurationRemoveElement_t3305291330::get_offset_of_properties_13(),
	ConfigurationRemoveElement_t3305291330::get_offset_of__origElement_14(),
	ConfigurationRemoveElement_t3305291330::get_offset_of__origCollection_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (ConfigurationElementCollectionType_t1806001494)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1667[5] = 
{
	ConfigurationElementCollectionType_t1806001494::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (ConfigurationErrorsException_t1362721126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1668[2] = 
{
	ConfigurationErrorsException_t1362721126::get_offset_of_filename_13(),
	ConfigurationErrorsException_t1362721126::get_offset_of_line_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (ConfigurationFileMap_t2625210096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1669[1] = 
{
	ConfigurationFileMap_t2625210096::get_offset_of_machineConfigFilename_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (ConfigurationLocation_t1895107553), -1, sizeof(ConfigurationLocation_t1895107553_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1670[7] = 
{
	ConfigurationLocation_t1895107553_StaticFields::get_offset_of_pathTrimChars_0(),
	ConfigurationLocation_t1895107553::get_offset_of_path_1(),
	ConfigurationLocation_t1895107553::get_offset_of_configuration_2(),
	ConfigurationLocation_t1895107553::get_offset_of_parent_3(),
	ConfigurationLocation_t1895107553::get_offset_of_xmlContent_4(),
	ConfigurationLocation_t1895107553::get_offset_of_parentResolved_5(),
	ConfigurationLocation_t1895107553::get_offset_of_allowOverride_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (ConfigurationLocationCollection_t1903842989), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (ConfigurationLockType_t131834733)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1672[4] = 
{
	ConfigurationLockType_t131834733::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (ConfigurationLockCollection_t1011762925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1673[6] = 
{
	ConfigurationLockCollection_t1011762925::get_offset_of_names_0(),
	ConfigurationLockCollection_t1011762925::get_offset_of_element_1(),
	ConfigurationLockCollection_t1011762925::get_offset_of_lockType_2(),
	ConfigurationLockCollection_t1011762925::get_offset_of_is_modified_3(),
	ConfigurationLockCollection_t1011762925::get_offset_of_valid_name_hash_4(),
	ConfigurationLockCollection_t1011762925::get_offset_of_valid_names_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (ConfigurationManager_t2608608455), -1, sizeof(ConfigurationManager_t2608608455_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1674[3] = 
{
	ConfigurationManager_t2608608455_StaticFields::get_offset_of_configFactory_0(),
	ConfigurationManager_t2608608455_StaticFields::get_offset_of_configSystem_1(),
	ConfigurationManager_t2608608455_StaticFields::get_offset_of_lockobj_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (ConfigurationProperty_t2048066811), -1, sizeof(ConfigurationProperty_t2048066811_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1675[9] = 
{
	ConfigurationProperty_t2048066811_StaticFields::get_offset_of_NoDefaultValue_0(),
	ConfigurationProperty_t2048066811::get_offset_of_name_1(),
	ConfigurationProperty_t2048066811::get_offset_of_type_2(),
	ConfigurationProperty_t2048066811::get_offset_of_default_value_3(),
	ConfigurationProperty_t2048066811::get_offset_of_converter_4(),
	ConfigurationProperty_t2048066811::get_offset_of_validation_5(),
	ConfigurationProperty_t2048066811::get_offset_of_flags_6(),
	ConfigurationProperty_t2048066811::get_offset_of_description_7(),
	ConfigurationProperty_t2048066811::get_offset_of_collectionAttribute_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (ConfigurationPropertyAttribute_t3655647199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1676[3] = 
{
	ConfigurationPropertyAttribute_t3655647199::get_offset_of_name_0(),
	ConfigurationPropertyAttribute_t3655647199::get_offset_of_default_value_1(),
	ConfigurationPropertyAttribute_t3655647199::get_offset_of_flags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (ConfigurationPropertyCollection_t3473514151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1677[1] = 
{
	ConfigurationPropertyCollection_t3473514151::get_offset_of_collection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (ConfigurationPropertyOptions_t3219689025)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1678[5] = 
{
	ConfigurationPropertyOptions_t3219689025::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (ConfigurationSaveMode_t700320212)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1679[4] = 
{
	ConfigurationSaveMode_t700320212::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (ConfigurationSection_t2600766927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1680[4] = 
{
	ConfigurationSection_t2600766927::get_offset_of_sectionInformation_13(),
	ConfigurationSection_t2600766927::get_offset_of_section_handler_14(),
	ConfigurationSection_t2600766927::get_offset_of_externalDataXml_15(),
	ConfigurationSection_t2600766927::get_offset_of__configContext_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (ConfigurationSectionCollection_t4261113299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1681[2] = 
{
	ConfigurationSectionCollection_t4261113299::get_offset_of_group_10(),
	ConfigurationSectionCollection_t4261113299::get_offset_of_config_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { sizeof (U3CGetEnumeratorU3Ec__Iterator0_t1795270620), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1682[5] = 
{
	U3CGetEnumeratorU3Ec__Iterator0_t1795270620::get_offset_of_U3CU24s_32U3E__0_0(),
	U3CGetEnumeratorU3Ec__Iterator0_t1795270620::get_offset_of_U3CkeyU3E__1_1(),
	U3CGetEnumeratorU3Ec__Iterator0_t1795270620::get_offset_of_U24PC_2(),
	U3CGetEnumeratorU3Ec__Iterator0_t1795270620::get_offset_of_U24current_3(),
	U3CGetEnumeratorU3Ec__Iterator0_t1795270620::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { sizeof (ConfigurationSectionGroup_t2230982736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1683[5] = 
{
	ConfigurationSectionGroup_t2230982736::get_offset_of_sections_0(),
	ConfigurationSectionGroup_t2230982736::get_offset_of_groups_1(),
	ConfigurationSectionGroup_t2230982736::get_offset_of_config_2(),
	ConfigurationSectionGroup_t2230982736::get_offset_of_group_3(),
	ConfigurationSectionGroup_t2230982736::get_offset_of_initialized_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (ConfigurationSectionGroupCollection_t575145286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1684[2] = 
{
	ConfigurationSectionGroupCollection_t575145286::get_offset_of_group_10(),
	ConfigurationSectionGroupCollection_t575145286::get_offset_of_config_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (ConfigurationUserLevel_t1204907851)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1685[4] = 
{
	ConfigurationUserLevel_t1204907851::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (ConfigurationValidatorAttribute_t1007519140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1686[2] = 
{
	ConfigurationValidatorAttribute_t1007519140::get_offset_of_validatorType_0(),
	ConfigurationValidatorAttribute_t1007519140::get_offset_of_instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (ConfigurationValidatorBase_t210547623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (ConfigXmlTextReader_t3212066157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1688[1] = 
{
	ConfigXmlTextReader_t3212066157::get_offset_of_fileName_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { sizeof (DefaultSection_t3840532724), -1, sizeof(DefaultSection_t3840532724_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1689[1] = 
{
	DefaultSection_t3840532724_StaticFields::get_offset_of_properties_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { sizeof (DefaultValidator_t300527515), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { sizeof (ElementInformation_t3165583784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1691[3] = 
{
	ElementInformation_t3165583784::get_offset_of_propertyInfo_0(),
	ElementInformation_t3165583784::get_offset_of_owner_1(),
	ElementInformation_t3165583784::get_offset_of_properties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { sizeof (ExeConfigurationFileMap_t1419586304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1692[3] = 
{
	ExeConfigurationFileMap_t1419586304::get_offset_of_exeConfigFilename_1(),
	ExeConfigurationFileMap_t1419586304::get_offset_of_localUserConfigFilename_2(),
	ExeConfigurationFileMap_t1419586304::get_offset_of_roamingUserConfigFilename_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { sizeof (IgnoreSection_t681509237), -1, sizeof(IgnoreSection_t681509237_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1693[2] = 
{
	IgnoreSection_t681509237::get_offset_of_xml_17(),
	IgnoreSection_t681509237_StaticFields::get_offset_of_properties_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { sizeof (InternalConfigurationFactory_t3846641927), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { sizeof (InternalConfigurationSystem_t2108740756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1695[3] = 
{
	InternalConfigurationSystem_t2108740756::get_offset_of_host_0(),
	InternalConfigurationSystem_t2108740756::get_offset_of_root_1(),
	InternalConfigurationSystem_t2108740756::get_offset_of_hostInitParams_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { sizeof (InternalConfigurationHost_t547577555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { sizeof (ExeConfigurationHost_t2778769322), -1, sizeof(ExeConfigurationHost_t2778769322_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1697[3] = 
{
	ExeConfigurationHost_t2778769322::get_offset_of_map_0(),
	ExeConfigurationHost_t2778769322::get_offset_of_level_1(),
	ExeConfigurationHost_t2778769322_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (InternalConfigurationRoot_t547578517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1698[2] = 
{
	InternalConfigurationRoot_t547578517::get_offset_of_host_0(),
	InternalConfigurationRoot_t547578517::get_offset_of_isDesignTime_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (PropertyInformation_t2089433965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1699[5] = 
{
	PropertyInformation_t2089433965::get_offset_of_isModified_0(),
	PropertyInformation_t2089433965::get_offset_of_val_1(),
	PropertyInformation_t2089433965::get_offset_of_origin_2(),
	PropertyInformation_t2089433965::get_offset_of_owner_3(),
	PropertyInformation_t2089433965::get_offset_of_property_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
