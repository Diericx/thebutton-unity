﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Configuration_System_Configuration_PropertyI954922393.h"
#include "System_Configuration_System_Configuration_Property1453501302.h"
#include "System_Configuration_System_Configuration_Property1217826846.h"
#include "System_Configuration_System_Configuration_Protecte1807950812.h"
#include "System_Configuration_System_Configuration_Protecte3971982415.h"
#include "System_Configuration_System_Configuration_Protected388338823.h"
#include "System_Configuration_System_Configuration_Protecte3541826375.h"
#include "System_Configuration_System_Configuration_ProviderS873049714.h"
#include "System_Configuration_System_Configuration_ProviderS585304908.h"
#include "System_Configuration_System_Configuration_SectionI1739019515.h"
#include "System_Configuration_System_Configuration_SectionG2346323570.h"
#include "System_Configuration_System_Configuration_ConfigIn3264723080.h"
#include "System_Configuration_System_Configuration_SectionI2754609709.h"
#include "System_Configuration_System_MonoTODOAttribute3487514019.h"
#include "System_Configuration_System_MonoInternalNoteAttrib4192790486.h"
#include "System_Xml_U3CModuleU3E3783534214.h"
#include "System_Xml_System_MonoTODOAttribute3487514019.h"
#include "System_Xml_Mono_Xml_Schema_XsdIdentitySelector185499482.h"
#include "System_Xml_Mono_Xml_Schema_XsdIdentityField2563516441.h"
#include "System_Xml_Mono_Xml_Schema_XsdIdentityPath2037874.h"
#include "System_Xml_Mono_Xml_Schema_XsdIdentityStep452377251.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyEntryField3632833996.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyEntryFieldCollect2946985218.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyEntry3532015222.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyEntryCollection1714053544.h"
#include "System_Xml_Mono_Xml_Schema_XsdKeyTable2980902100.h"
#include "System_Xml_Mono_Xml_Schema_XsdParticleStateManager4119977941.h"
#include "System_Xml_Mono_Xml_Schema_XsdValidationState3545965403.h"
#include "System_Xml_Mono_Xml_Schema_XsdElementValidationStat152111323.h"
#include "System_Xml_Mono_Xml_Schema_XsdSequenceValidationSt3542030006.h"
#include "System_Xml_Mono_Xml_Schema_XsdChoiceValidationStat1506407122.h"
#include "System_Xml_Mono_Xml_Schema_XsdAllValidationState1028212608.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnyValidationState204702461.h"
#include "System_Xml_Mono_Xml_Schema_XsdAppendedValidationSt3724408090.h"
#include "System_Xml_Mono_Xml_Schema_XsdEmptyValidationState1914323912.h"
#include "System_Xml_Mono_Xml_Schema_XsdInvalidValidationSta1112885736.h"
#include "System_Xml_Mono_Xml_Schema_XsdValidatingReader1704923617.h"
#include "System_Xml_Mono_Xml_Schema_XsdValidationContext3720679969.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDManager3860002335.h"
#include "System_Xml_Mono_Xml_Schema_XsdWildcard625524157.h"
#include "System_Xml_System_Xml_ConformanceLevel3761201363.h"
#include "System_Xml_Mono_Xml_DTDAutomataFactory3605390810.h"
#include "System_Xml_Mono_Xml_DTDAutomata545990600.h"
#include "System_Xml_Mono_Xml_DTDElementAutomata2864881036.h"
#include "System_Xml_Mono_Xml_DTDChoiceAutomata2810241733.h"
#include "System_Xml_Mono_Xml_DTDSequenceAutomata1228770437.h"
#include "System_Xml_Mono_Xml_DTDOneOrMoreAutomata1559764132.h"
#include "System_Xml_Mono_Xml_DTDEmptyAutomata411530619.h"
#include "System_Xml_Mono_Xml_DTDAnyAutomata146446906.h"
#include "System_Xml_Mono_Xml_DTDInvalidAutomata247674167.h"
#include "System_Xml_Mono_Xml_DTDObjectModel1113953282.h"
#include "System_Xml_Mono_Xml_DictionaryBase1005937181.h"
#include "System_Xml_Mono_Xml_DictionaryBase_U3CU3Ec__Iterat3518389200.h"
#include "System_Xml_Mono_Xml_DTDCollectionBase2621362935.h"
#include "System_Xml_Mono_Xml_DTDElementDeclarationCollectio2224069626.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclarationCollection243645429.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclarationCollection1212505713.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclarationCollectio228085060.h"
#include "System_Xml_Mono_Xml_DTDContentModel445576364.h"
#include "System_Xml_Mono_Xml_DTDContentModelCollection3164170484.h"
#include "System_Xml_Mono_Xml_DTDNode1758286970.h"
#include "System_Xml_Mono_Xml_DTDElementDeclaration8748002.h"
#include "System_Xml_Mono_Xml_DTDAttributeDefinition3692870749.h"
#include "System_Xml_Mono_Xml_DTDAttListDeclaration2272374839.h"
#include "System_Xml_Mono_Xml_DTDEntityBase2353758560.h"
#include "System_Xml_Mono_Xml_DTDEntityDeclaration4283284771.h"
#include "System_Xml_Mono_Xml_DTDNotationDeclaration1758408116.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclarationC3496720022.h"
#include "System_Xml_Mono_Xml_DTDParameterEntityDeclaration252230634.h"
#include "System_Xml_Mono_Xml_DTDContentOrderType3150259539.h"
#include "System_Xml_Mono_Xml_DTDAttributeOccurenceType2819881069.h"
#include "System_Xml_Mono_Xml_DTDOccurence99371501.h"
#include "System_Xml_System_Xml_DTDReader2453137441.h"
#include "System_Xml_Mono_Xml_DTDValidatingReader4120969348.h"
#include "System_Xml_Mono_Xml_DTDValidatingReader_AttributeS1499247213.h"
#include "System_Xml_Mono_Xml_EntityResolvingXmlReader2086920314.h"
#include "System_Xml_System_Xml_EntityHandling3960499440.h"
#include "System_Xml_System_Xml_Formatting1126649075.h"
#include "System_Xml_System_Xml_NameTable594386929.h"
#include "System_Xml_System_Xml_NameTable_Entry2583369454.h"
#include "System_Xml_System_Xml_NamespaceHandling1452270444.h"
#include "System_Xml_System_Xml_NewLineHandling1737195169.h"
#include "System_Xml_System_Xml_ReadState3138905245.h"
#include "System_Xml_System_Xml_ValidationType1401987383.h"
#include "System_Xml_System_Xml_WhitespaceHandling3754063142.h"
#include "System_Xml_System_Xml_WriteState1534871862.h"
#include "System_Xml_System_Xml_XmlEntity4027255380.h"
#include "System_Xml_System_Xml_XmlAttribute175731005.h"
#include "System_Xml_System_Xml_XmlAttributeCollection3359885287.h"
#include "System_Xml_System_Xml_XmlCDataSection1124775823.h"
#include "System_Xml_System_Xml_XmlChar1369421061.h"
#include "System_Xml_System_Xml_XmlCharacterData575748506.h"
#include "System_Xml_System_Xml_XmlComment3999331572.h"
#include "System_Xml_System_Xml_XmlNotation206561061.h"
#include "System_Xml_System_Xml_XmlDeclaration1545359137.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (PropertyInformationCollection_t954922393), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (PropertyInformationEnumerator_t1453501302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1701[2] = 
{
	PropertyInformationEnumerator_t1453501302::get_offset_of_collection_0(),
	PropertyInformationEnumerator_t1453501302::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (PropertyValueOrigin_t1217826846)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1702[4] = 
{
	PropertyValueOrigin_t1217826846::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (ProtectedConfiguration_t1807950812), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (ProtectedConfigurationProvider_t3971982415), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (ProtectedConfigurationProviderCollection_t388338823), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (ProtectedConfigurationSection_t3541826375), -1, sizeof(ProtectedConfigurationSection_t3541826375_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1706[4] = 
{
	ProtectedConfigurationSection_t3541826375_StaticFields::get_offset_of_defaultProviderProp_17(),
	ProtectedConfigurationSection_t3541826375_StaticFields::get_offset_of_providersProp_18(),
	ProtectedConfigurationSection_t3541826375_StaticFields::get_offset_of_properties_19(),
	ProtectedConfigurationSection_t3541826375::get_offset_of_providers_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (ProviderSettings_t873049714), -1, sizeof(ProviderSettings_t873049714_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1707[4] = 
{
	ProviderSettings_t873049714::get_offset_of_parameters_13(),
	ProviderSettings_t873049714_StaticFields::get_offset_of_nameProp_14(),
	ProviderSettings_t873049714_StaticFields::get_offset_of_typeProp_15(),
	ProviderSettings_t873049714_StaticFields::get_offset_of_properties_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (ProviderSettingsCollection_t585304908), -1, sizeof(ProviderSettingsCollection_t585304908_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1708[1] = 
{
	ProviderSettingsCollection_t585304908_StaticFields::get_offset_of_props_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (SectionInfo_t1739019515), -1, sizeof(SectionInfo_t1739019515_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1709[6] = 
{
	SectionInfo_t1739019515::get_offset_of_allowLocation_6(),
	SectionInfo_t1739019515::get_offset_of_requirePermission_7(),
	SectionInfo_t1739019515::get_offset_of_restartOnExternalChanges_8(),
	SectionInfo_t1739019515::get_offset_of_allowDefinition_9(),
	SectionInfo_t1739019515::get_offset_of_allowExeDefinition_10(),
	SectionInfo_t1739019515_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (SectionGroupInfo_t2346323570), -1, sizeof(SectionGroupInfo_t2346323570_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1710[3] = 
{
	SectionGroupInfo_t2346323570::get_offset_of_sections_6(),
	SectionGroupInfo_t2346323570::get_offset_of_groups_7(),
	SectionGroupInfo_t2346323570_StaticFields::get_offset_of_emptyList_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (ConfigInfoCollection_t3264723080), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (SectionInformation_t2754609709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1712[13] = 
{
	SectionInformation_t2754609709::get_offset_of_parent_0(),
	SectionInformation_t2754609709::get_offset_of_allow_definition_1(),
	SectionInformation_t2754609709::get_offset_of_allow_exe_definition_2(),
	SectionInformation_t2754609709::get_offset_of_allow_location_3(),
	SectionInformation_t2754609709::get_offset_of_allow_override_4(),
	SectionInformation_t2754609709::get_offset_of_inherit_on_child_apps_5(),
	SectionInformation_t2754609709::get_offset_of_restart_on_external_changes_6(),
	SectionInformation_t2754609709::get_offset_of_require_permission_7(),
	SectionInformation_t2754609709::get_offset_of_config_source_8(),
	SectionInformation_t2754609709::get_offset_of_name_9(),
	SectionInformation_t2754609709::get_offset_of_raw_xml_10(),
	SectionInformation_t2754609709::get_offset_of_protection_provider_11(),
	SectionInformation_t2754609709::get_offset_of_U3CConfigFilePathU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (MonoTODOAttribute_t3487514021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1713[1] = 
{
	MonoTODOAttribute_t3487514021::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (MonoInternalNoteAttribute_t4192790486), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (U3CModuleU3E_t3783534219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (MonoTODOAttribute_t3487514022), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (XsdIdentitySelector_t185499482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1717[3] = 
{
	XsdIdentitySelector_t185499482::get_offset_of_selectorPaths_0(),
	XsdIdentitySelector_t185499482::get_offset_of_fields_1(),
	XsdIdentitySelector_t185499482::get_offset_of_cachedFields_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (XsdIdentityField_t2563516441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1718[2] = 
{
	XsdIdentityField_t2563516441::get_offset_of_fieldPaths_0(),
	XsdIdentityField_t2563516441::get_offset_of_index_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (XsdIdentityPath_t2037874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1719[2] = 
{
	XsdIdentityPath_t2037874::get_offset_of_OrderedSteps_0(),
	XsdIdentityPath_t2037874::get_offset_of_Descendants_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (XsdIdentityStep_t452377251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1720[6] = 
{
	XsdIdentityStep_t452377251::get_offset_of_IsCurrent_0(),
	XsdIdentityStep_t452377251::get_offset_of_IsAttribute_1(),
	XsdIdentityStep_t452377251::get_offset_of_IsAnyName_2(),
	XsdIdentityStep_t452377251::get_offset_of_NsName_3(),
	XsdIdentityStep_t452377251::get_offset_of_Name_4(),
	XsdIdentityStep_t452377251::get_offset_of_Namespace_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (XsdKeyEntryField_t3632833996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1721[13] = 
{
	XsdKeyEntryField_t3632833996::get_offset_of_entry_0(),
	XsdKeyEntryField_t3632833996::get_offset_of_field_1(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldFound_2(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldLineNumber_3(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldLinePosition_4(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldHasLineInfo_5(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldType_6(),
	XsdKeyEntryField_t3632833996::get_offset_of_Identity_7(),
	XsdKeyEntryField_t3632833996::get_offset_of_IsXsiNil_8(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldFoundDepth_9(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldFoundPath_10(),
	XsdKeyEntryField_t3632833996::get_offset_of_Consuming_11(),
	XsdKeyEntryField_t3632833996::get_offset_of_Consumed_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (XsdKeyEntryFieldCollection_t2946985218), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (XsdKeyEntry_t3532015222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1723[8] = 
{
	XsdKeyEntry_t3532015222::get_offset_of_StartDepth_0(),
	XsdKeyEntry_t3532015222::get_offset_of_SelectorLineNumber_1(),
	XsdKeyEntry_t3532015222::get_offset_of_SelectorLinePosition_2(),
	XsdKeyEntry_t3532015222::get_offset_of_SelectorHasLineInfo_3(),
	XsdKeyEntry_t3532015222::get_offset_of_KeyFields_4(),
	XsdKeyEntry_t3532015222::get_offset_of_KeyRefFound_5(),
	XsdKeyEntry_t3532015222::get_offset_of_OwnerSequence_6(),
	XsdKeyEntry_t3532015222::get_offset_of_keyFound_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (XsdKeyEntryCollection_t1714053544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (XsdKeyTable_t2980902100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1725[9] = 
{
	XsdKeyTable_t2980902100::get_offset_of_alwaysTrue_0(),
	XsdKeyTable_t2980902100::get_offset_of_selector_1(),
	XsdKeyTable_t2980902100::get_offset_of_source_2(),
	XsdKeyTable_t2980902100::get_offset_of_qname_3(),
	XsdKeyTable_t2980902100::get_offset_of_refKeyName_4(),
	XsdKeyTable_t2980902100::get_offset_of_Entries_5(),
	XsdKeyTable_t2980902100::get_offset_of_FinishedEntries_6(),
	XsdKeyTable_t2980902100::get_offset_of_StartDepth_7(),
	XsdKeyTable_t2980902100::get_offset_of_ReferencedKey_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (XsdParticleStateManager_t4119977941), -1, sizeof(XsdParticleStateManager_t4119977941_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1726[6] = 
{
	XsdParticleStateManager_t4119977941::get_offset_of_table_0(),
	XsdParticleStateManager_t4119977941::get_offset_of_processContents_1(),
	XsdParticleStateManager_t4119977941::get_offset_of_CurrentElement_2(),
	XsdParticleStateManager_t4119977941::get_offset_of_ContextStack_3(),
	XsdParticleStateManager_t4119977941::get_offset_of_Context_4(),
	XsdParticleStateManager_t4119977941_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (XsdValidationState_t3545965403), -1, sizeof(XsdValidationState_t3545965403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1727[3] = 
{
	XsdValidationState_t3545965403_StaticFields::get_offset_of_invalid_0(),
	XsdValidationState_t3545965403::get_offset_of_occured_1(),
	XsdValidationState_t3545965403::get_offset_of_manager_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (XsdElementValidationState_t152111323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1728[1] = 
{
	XsdElementValidationState_t152111323::get_offset_of_element_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (XsdSequenceValidationState_t3542030006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1729[4] = 
{
	XsdSequenceValidationState_t3542030006::get_offset_of_seq_3(),
	XsdSequenceValidationState_t3542030006::get_offset_of_current_4(),
	XsdSequenceValidationState_t3542030006::get_offset_of_currentAutomata_5(),
	XsdSequenceValidationState_t3542030006::get_offset_of_emptiable_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (XsdChoiceValidationState_t1506407122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1730[3] = 
{
	XsdChoiceValidationState_t1506407122::get_offset_of_choice_3(),
	XsdChoiceValidationState_t1506407122::get_offset_of_emptiable_4(),
	XsdChoiceValidationState_t1506407122::get_offset_of_emptiableComputed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (XsdAllValidationState_t1028212608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1731[2] = 
{
	XsdAllValidationState_t1028212608::get_offset_of_all_3(),
	XsdAllValidationState_t1028212608::get_offset_of_consumed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (XsdAnyValidationState_t204702461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1732[1] = 
{
	XsdAnyValidationState_t204702461::get_offset_of_any_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (XsdAppendedValidationState_t3724408090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1733[2] = 
{
	XsdAppendedValidationState_t3724408090::get_offset_of_head_3(),
	XsdAppendedValidationState_t3724408090::get_offset_of_rest_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (XsdEmptyValidationState_t1914323912), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (XsdInvalidValidationState_t1112885736), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (XsdValidatingReader_t1704923617), -1, sizeof(XsdValidatingReader_t1704923617_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1736[30] = 
{
	XsdValidatingReader_t1704923617_StaticFields::get_offset_of_emptyAttributeArray_2(),
	XsdValidatingReader_t1704923617::get_offset_of_reader_3(),
	XsdValidatingReader_t1704923617::get_offset_of_resolver_4(),
	XsdValidatingReader_t1704923617::get_offset_of_sourceReaderSchemaInfo_5(),
	XsdValidatingReader_t1704923617::get_offset_of_readerLineInfo_6(),
	XsdValidatingReader_t1704923617::get_offset_of_validationType_7(),
	XsdValidatingReader_t1704923617::get_offset_of_schemas_8(),
	XsdValidatingReader_t1704923617::get_offset_of_namespaces_9(),
	XsdValidatingReader_t1704923617::get_offset_of_validationStarted_10(),
	XsdValidatingReader_t1704923617::get_offset_of_checkIdentity_11(),
	XsdValidatingReader_t1704923617::get_offset_of_idManager_12(),
	XsdValidatingReader_t1704923617::get_offset_of_checkKeyConstraints_13(),
	XsdValidatingReader_t1704923617::get_offset_of_keyTables_14(),
	XsdValidatingReader_t1704923617::get_offset_of_currentKeyFieldConsumers_15(),
	XsdValidatingReader_t1704923617::get_offset_of_tmpKeyrefPool_16(),
	XsdValidatingReader_t1704923617::get_offset_of_elementQNameStack_17(),
	XsdValidatingReader_t1704923617::get_offset_of_state_18(),
	XsdValidatingReader_t1704923617::get_offset_of_skipValidationDepth_19(),
	XsdValidatingReader_t1704923617::get_offset_of_xsiNilDepth_20(),
	XsdValidatingReader_t1704923617::get_offset_of_storedCharacters_21(),
	XsdValidatingReader_t1704923617::get_offset_of_shouldValidateCharacters_22(),
	XsdValidatingReader_t1704923617::get_offset_of_defaultAttributes_23(),
	XsdValidatingReader_t1704923617::get_offset_of_currentDefaultAttribute_24(),
	XsdValidatingReader_t1704923617::get_offset_of_defaultAttributesCache_25(),
	XsdValidatingReader_t1704923617::get_offset_of_defaultAttributeConsumed_26(),
	XsdValidatingReader_t1704923617::get_offset_of_currentAttrType_27(),
	XsdValidatingReader_t1704923617::get_offset_of_ValidationEventHandler_28(),
	XsdValidatingReader_t1704923617_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_29(),
	XsdValidatingReader_t1704923617_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_30(),
	XsdValidatingReader_t1704923617_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (XsdValidationContext_t3720679969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1737[3] = 
{
	XsdValidationContext_t3720679969::get_offset_of_xsi_type_0(),
	XsdValidationContext_t3720679969::get_offset_of_State_1(),
	XsdValidationContext_t3720679969::get_offset_of_element_stack_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (XsdIDManager_t3860002335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1738[3] = 
{
	XsdIDManager_t3860002335::get_offset_of_idList_0(),
	XsdIDManager_t3860002335::get_offset_of_missingIDReferences_1(),
	XsdIDManager_t3860002335::get_offset_of_thisElementId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (XsdWildcard_t625524157), -1, sizeof(XsdWildcard_t625524157_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1739[10] = 
{
	XsdWildcard_t625524157::get_offset_of_xsobj_0(),
	XsdWildcard_t625524157::get_offset_of_ResolvedProcessing_1(),
	XsdWildcard_t625524157::get_offset_of_TargetNamespace_2(),
	XsdWildcard_t625524157::get_offset_of_SkipCompile_3(),
	XsdWildcard_t625524157::get_offset_of_HasValueAny_4(),
	XsdWildcard_t625524157::get_offset_of_HasValueLocal_5(),
	XsdWildcard_t625524157::get_offset_of_HasValueOther_6(),
	XsdWildcard_t625524157::get_offset_of_HasValueTargetNamespace_7(),
	XsdWildcard_t625524157::get_offset_of_ResolvedNamespaces_8(),
	XsdWildcard_t625524157_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (ConformanceLevel_t3761201363)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1742[4] = 
{
	ConformanceLevel_t3761201363::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (DTDAutomataFactory_t3605390810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1743[3] = 
{
	DTDAutomataFactory_t3605390810::get_offset_of_root_0(),
	DTDAutomataFactory_t3605390810::get_offset_of_choiceTable_1(),
	DTDAutomataFactory_t3605390810::get_offset_of_sequenceTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (DTDAutomata_t545990600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1744[1] = 
{
	DTDAutomata_t545990600::get_offset_of_root_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (DTDElementAutomata_t2864881036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1745[1] = 
{
	DTDElementAutomata_t2864881036::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (DTDChoiceAutomata_t2810241733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1746[4] = 
{
	DTDChoiceAutomata_t2810241733::get_offset_of_left_1(),
	DTDChoiceAutomata_t2810241733::get_offset_of_right_2(),
	DTDChoiceAutomata_t2810241733::get_offset_of_hasComputedEmptiable_3(),
	DTDChoiceAutomata_t2810241733::get_offset_of_cachedEmptiable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (DTDSequenceAutomata_t1228770437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1747[4] = 
{
	DTDSequenceAutomata_t1228770437::get_offset_of_left_1(),
	DTDSequenceAutomata_t1228770437::get_offset_of_right_2(),
	DTDSequenceAutomata_t1228770437::get_offset_of_hasComputedEmptiable_3(),
	DTDSequenceAutomata_t1228770437::get_offset_of_cachedEmptiable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (DTDOneOrMoreAutomata_t1559764132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1748[1] = 
{
	DTDOneOrMoreAutomata_t1559764132::get_offset_of_children_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (DTDEmptyAutomata_t411530619), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (DTDAnyAutomata_t146446906), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (DTDInvalidAutomata_t247674167), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (DTDObjectModel_t1113953282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1752[23] = 
{
	DTDObjectModel_t1113953282::get_offset_of_factory_0(),
	DTDObjectModel_t1113953282::get_offset_of_rootAutomata_1(),
	DTDObjectModel_t1113953282::get_offset_of_emptyAutomata_2(),
	DTDObjectModel_t1113953282::get_offset_of_anyAutomata_3(),
	DTDObjectModel_t1113953282::get_offset_of_invalidAutomata_4(),
	DTDObjectModel_t1113953282::get_offset_of_elementDecls_5(),
	DTDObjectModel_t1113953282::get_offset_of_attListDecls_6(),
	DTDObjectModel_t1113953282::get_offset_of_peDecls_7(),
	DTDObjectModel_t1113953282::get_offset_of_entityDecls_8(),
	DTDObjectModel_t1113953282::get_offset_of_notationDecls_9(),
	DTDObjectModel_t1113953282::get_offset_of_validationErrors_10(),
	DTDObjectModel_t1113953282::get_offset_of_resolver_11(),
	DTDObjectModel_t1113953282::get_offset_of_nameTable_12(),
	DTDObjectModel_t1113953282::get_offset_of_externalResources_13(),
	DTDObjectModel_t1113953282::get_offset_of_baseURI_14(),
	DTDObjectModel_t1113953282::get_offset_of_name_15(),
	DTDObjectModel_t1113953282::get_offset_of_publicId_16(),
	DTDObjectModel_t1113953282::get_offset_of_systemId_17(),
	DTDObjectModel_t1113953282::get_offset_of_intSubset_18(),
	DTDObjectModel_t1113953282::get_offset_of_intSubsetHasPERef_19(),
	DTDObjectModel_t1113953282::get_offset_of_isStandalone_20(),
	DTDObjectModel_t1113953282::get_offset_of_lineNumber_21(),
	DTDObjectModel_t1113953282::get_offset_of_linePosition_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (DictionaryBase_t1005937181), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (U3CU3Ec__Iterator3_t3518389200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1754[5] = 
{
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U3CU24s_50U3E__0_0(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U3CpU3E__1_1(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U24PC_2(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (DTDCollectionBase_t2621362935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1755[1] = 
{
	DTDCollectionBase_t2621362935::get_offset_of_root_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (DTDElementDeclarationCollection_t2224069626), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (DTDAttListDeclarationCollection_t243645429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (DTDEntityDeclarationCollection_t1212505713), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (DTDNotationDeclarationCollection_t228085060), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (DTDContentModel_t445576364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1760[7] = 
{
	DTDContentModel_t445576364::get_offset_of_root_5(),
	DTDContentModel_t445576364::get_offset_of_compiledAutomata_6(),
	DTDContentModel_t445576364::get_offset_of_ownerElementName_7(),
	DTDContentModel_t445576364::get_offset_of_elementName_8(),
	DTDContentModel_t445576364::get_offset_of_orderType_9(),
	DTDContentModel_t445576364::get_offset_of_childModels_10(),
	DTDContentModel_t445576364::get_offset_of_occurence_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (DTDContentModelCollection_t3164170484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1761[1] = 
{
	DTDContentModelCollection_t3164170484::get_offset_of_contentModel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (DTDNode_t1758286970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1762[5] = 
{
	DTDNode_t1758286970::get_offset_of_root_0(),
	DTDNode_t1758286970::get_offset_of_isInternalSubset_1(),
	DTDNode_t1758286970::get_offset_of_baseURI_2(),
	DTDNode_t1758286970::get_offset_of_lineNumber_3(),
	DTDNode_t1758286970::get_offset_of_linePosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (DTDElementDeclaration_t8748002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1763[6] = 
{
	DTDElementDeclaration_t8748002::get_offset_of_root_5(),
	DTDElementDeclaration_t8748002::get_offset_of_contentModel_6(),
	DTDElementDeclaration_t8748002::get_offset_of_name_7(),
	DTDElementDeclaration_t8748002::get_offset_of_isEmpty_8(),
	DTDElementDeclaration_t8748002::get_offset_of_isAny_9(),
	DTDElementDeclaration_t8748002::get_offset_of_isMixedContent_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (DTDAttributeDefinition_t3692870749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1764[7] = 
{
	DTDAttributeDefinition_t3692870749::get_offset_of_name_5(),
	DTDAttributeDefinition_t3692870749::get_offset_of_datatype_6(),
	DTDAttributeDefinition_t3692870749::get_offset_of_enumeratedLiterals_7(),
	DTDAttributeDefinition_t3692870749::get_offset_of_unresolvedDefault_8(),
	DTDAttributeDefinition_t3692870749::get_offset_of_enumeratedNotations_9(),
	DTDAttributeDefinition_t3692870749::get_offset_of_occurenceType_10(),
	DTDAttributeDefinition_t3692870749::get_offset_of_resolvedDefaultValue_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (DTDAttListDeclaration_t2272374839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1765[3] = 
{
	DTDAttListDeclaration_t2272374839::get_offset_of_name_5(),
	DTDAttListDeclaration_t2272374839::get_offset_of_attributeOrders_6(),
	DTDAttListDeclaration_t2272374839::get_offset_of_attributes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (DTDEntityBase_t2353758560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1766[10] = 
{
	DTDEntityBase_t2353758560::get_offset_of_name_5(),
	DTDEntityBase_t2353758560::get_offset_of_publicId_6(),
	DTDEntityBase_t2353758560::get_offset_of_systemId_7(),
	DTDEntityBase_t2353758560::get_offset_of_literalValue_8(),
	DTDEntityBase_t2353758560::get_offset_of_replacementText_9(),
	DTDEntityBase_t2353758560::get_offset_of_uriString_10(),
	DTDEntityBase_t2353758560::get_offset_of_absUri_11(),
	DTDEntityBase_t2353758560::get_offset_of_isInvalid_12(),
	DTDEntityBase_t2353758560::get_offset_of_loadFailed_13(),
	DTDEntityBase_t2353758560::get_offset_of_resolver_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (DTDEntityDeclaration_t4283284771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1767[6] = 
{
	DTDEntityDeclaration_t4283284771::get_offset_of_entityValue_15(),
	DTDEntityDeclaration_t4283284771::get_offset_of_notationName_16(),
	DTDEntityDeclaration_t4283284771::get_offset_of_ReferencingEntities_17(),
	DTDEntityDeclaration_t4283284771::get_offset_of_scanned_18(),
	DTDEntityDeclaration_t4283284771::get_offset_of_recursed_19(),
	DTDEntityDeclaration_t4283284771::get_offset_of_hasExternalReference_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (DTDNotationDeclaration_t1758408116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1768[5] = 
{
	DTDNotationDeclaration_t1758408116::get_offset_of_name_5(),
	DTDNotationDeclaration_t1758408116::get_offset_of_localName_6(),
	DTDNotationDeclaration_t1758408116::get_offset_of_prefix_7(),
	DTDNotationDeclaration_t1758408116::get_offset_of_publicId_8(),
	DTDNotationDeclaration_t1758408116::get_offset_of_systemId_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (DTDParameterEntityDeclarationCollection_t3496720022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1769[2] = 
{
	DTDParameterEntityDeclarationCollection_t3496720022::get_offset_of_peDecls_0(),
	DTDParameterEntityDeclarationCollection_t3496720022::get_offset_of_root_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (DTDParameterEntityDeclaration_t252230634), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (DTDContentOrderType_t3150259539)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1771[4] = 
{
	DTDContentOrderType_t3150259539::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (DTDAttributeOccurenceType_t2819881069)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1772[5] = 
{
	DTDAttributeOccurenceType_t2819881069::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (DTDOccurence_t99371501)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1773[5] = 
{
	DTDOccurence_t99371501::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (DTDReader_t2453137441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1774[14] = 
{
	DTDReader_t2453137441::get_offset_of_currentInput_0(),
	DTDReader_t2453137441::get_offset_of_parserInputStack_1(),
	DTDReader_t2453137441::get_offset_of_nameBuffer_2(),
	DTDReader_t2453137441::get_offset_of_nameLength_3(),
	DTDReader_t2453137441::get_offset_of_nameCapacity_4(),
	DTDReader_t2453137441::get_offset_of_valueBuffer_5(),
	DTDReader_t2453137441::get_offset_of_currentLinkedNodeLineNumber_6(),
	DTDReader_t2453137441::get_offset_of_currentLinkedNodeLinePosition_7(),
	DTDReader_t2453137441::get_offset_of_dtdIncludeSect_8(),
	DTDReader_t2453137441::get_offset_of_normalization_9(),
	DTDReader_t2453137441::get_offset_of_processingInternalSubset_10(),
	DTDReader_t2453137441::get_offset_of_cachedPublicId_11(),
	DTDReader_t2453137441::get_offset_of_cachedSystemId_12(),
	DTDReader_t2453137441::get_offset_of_DTD_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (DTDValidatingReader_t4120969348), -1, sizeof(DTDValidatingReader_t4120969348_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1775[29] = 
{
	DTDValidatingReader_t4120969348::get_offset_of_reader_2(),
	DTDValidatingReader_t4120969348::get_offset_of_sourceTextReader_3(),
	DTDValidatingReader_t4120969348::get_offset_of_validatingReader_4(),
	DTDValidatingReader_t4120969348::get_offset_of_dtd_5(),
	DTDValidatingReader_t4120969348::get_offset_of_resolver_6(),
	DTDValidatingReader_t4120969348::get_offset_of_currentElement_7(),
	DTDValidatingReader_t4120969348::get_offset_of_attributes_8(),
	DTDValidatingReader_t4120969348::get_offset_of_attributeCount_9(),
	DTDValidatingReader_t4120969348::get_offset_of_currentAttribute_10(),
	DTDValidatingReader_t4120969348::get_offset_of_consumedAttribute_11(),
	DTDValidatingReader_t4120969348::get_offset_of_elementStack_12(),
	DTDValidatingReader_t4120969348::get_offset_of_automataStack_13(),
	DTDValidatingReader_t4120969348::get_offset_of_popScope_14(),
	DTDValidatingReader_t4120969348::get_offset_of_isStandalone_15(),
	DTDValidatingReader_t4120969348::get_offset_of_currentAutomata_16(),
	DTDValidatingReader_t4120969348::get_offset_of_previousAutomata_17(),
	DTDValidatingReader_t4120969348::get_offset_of_idList_18(),
	DTDValidatingReader_t4120969348::get_offset_of_missingIDReferences_19(),
	DTDValidatingReader_t4120969348::get_offset_of_nsmgr_20(),
	DTDValidatingReader_t4120969348::get_offset_of_currentTextValue_21(),
	DTDValidatingReader_t4120969348::get_offset_of_constructingTextValue_22(),
	DTDValidatingReader_t4120969348::get_offset_of_shouldResetCurrentTextValue_23(),
	DTDValidatingReader_t4120969348::get_offset_of_isSignificantWhitespace_24(),
	DTDValidatingReader_t4120969348::get_offset_of_isWhitespace_25(),
	DTDValidatingReader_t4120969348::get_offset_of_isText_26(),
	DTDValidatingReader_t4120969348::get_offset_of_attributeValueEntityStack_27(),
	DTDValidatingReader_t4120969348::get_offset_of_valueBuilder_28(),
	DTDValidatingReader_t4120969348::get_offset_of_whitespaceChars_29(),
	DTDValidatingReader_t4120969348_StaticFields::get_offset_of_U3CU3Ef__switchU24map2A_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (AttributeSlot_t1499247213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1776[6] = 
{
	AttributeSlot_t1499247213::get_offset_of_Name_0(),
	AttributeSlot_t1499247213::get_offset_of_LocalName_1(),
	AttributeSlot_t1499247213::get_offset_of_NS_2(),
	AttributeSlot_t1499247213::get_offset_of_Prefix_3(),
	AttributeSlot_t1499247213::get_offset_of_Value_4(),
	AttributeSlot_t1499247213::get_offset_of_IsDefault_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (EntityResolvingXmlReader_t2086920314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1777[8] = 
{
	EntityResolvingXmlReader_t2086920314::get_offset_of_entity_2(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_source_3(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_context_4(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_resolver_5(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_entity_handling_6(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_entity_inside_attr_7(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_inside_attr_8(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_do_resolve_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (EntityHandling_t3960499440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1778[3] = 
{
	EntityHandling_t3960499440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (Formatting_t1126649075)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1779[3] = 
{
	Formatting_t1126649075::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (NameTable_t594386929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1783[3] = 
{
	NameTable_t594386929::get_offset_of_count_0(),
	NameTable_t594386929::get_offset_of_buckets_1(),
	NameTable_t594386929::get_offset_of_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (Entry_t2583369454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1784[4] = 
{
	Entry_t2583369454::get_offset_of_str_0(),
	Entry_t2583369454::get_offset_of_hash_1(),
	Entry_t2583369454::get_offset_of_len_2(),
	Entry_t2583369454::get_offset_of_next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (NamespaceHandling_t1452270444)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1785[3] = 
{
	NamespaceHandling_t1452270444::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (NewLineHandling_t1737195169)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1786[4] = 
{
	NewLineHandling_t1737195169::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (ReadState_t3138905245)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1787[6] = 
{
	ReadState_t3138905245::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (ValidationType_t1401987383)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1788[6] = 
{
	ValidationType_t1401987383::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (WhitespaceHandling_t3754063142)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1789[4] = 
{
	WhitespaceHandling_t3754063142::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (WriteState_t1534871862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1790[8] = 
{
	WriteState_t1534871862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (XmlEntity_t4027255380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1791[7] = 
{
	XmlEntity_t4027255380::get_offset_of_name_5(),
	XmlEntity_t4027255380::get_offset_of_NDATA_6(),
	XmlEntity_t4027255380::get_offset_of_publicId_7(),
	XmlEntity_t4027255380::get_offset_of_systemId_8(),
	XmlEntity_t4027255380::get_offset_of_baseUri_9(),
	XmlEntity_t4027255380::get_offset_of_lastLinkedChild_10(),
	XmlEntity_t4027255380::get_offset_of_contentAlreadySet_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (XmlAttribute_t175731005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1792[4] = 
{
	XmlAttribute_t175731005::get_offset_of_name_5(),
	XmlAttribute_t175731005::get_offset_of_isDefault_6(),
	XmlAttribute_t175731005::get_offset_of_lastLinkedChild_7(),
	XmlAttribute_t175731005::get_offset_of_schemaInfo_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (XmlAttributeCollection_t3359885287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1793[2] = 
{
	XmlAttributeCollection_t3359885287::get_offset_of_ownerElement_4(),
	XmlAttributeCollection_t3359885287::get_offset_of_ownerDocument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (XmlCDataSection_t1124775823), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (XmlChar_t1369421061), -1, sizeof(XmlChar_t1369421061_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1795[5] = 
{
	XmlChar_t1369421061_StaticFields::get_offset_of_WhitespaceChars_0(),
	XmlChar_t1369421061_StaticFields::get_offset_of_firstNamePages_1(),
	XmlChar_t1369421061_StaticFields::get_offset_of_namePages_2(),
	XmlChar_t1369421061_StaticFields::get_offset_of_nameBitmap_3(),
	XmlChar_t1369421061_StaticFields::get_offset_of_U3CU3Ef__switchU24map2F_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (XmlCharacterData_t575748506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1796[1] = 
{
	XmlCharacterData_t575748506::get_offset_of_data_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (XmlComment_t3999331572), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (XmlNotation_t206561061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1798[4] = 
{
	XmlNotation_t206561061::get_offset_of_localName_5(),
	XmlNotation_t206561061::get_offset_of_publicId_6(),
	XmlNotation_t206561061::get_offset_of_systemId_7(),
	XmlNotation_t206561061::get_offset_of_prefix_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (XmlDeclaration_t1545359137), -1, sizeof(XmlDeclaration_t1545359137_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1799[4] = 
{
	XmlDeclaration_t1545359137::get_offset_of_encoding_6(),
	XmlDeclaration_t1545359137::get_offset_of_standalone_7(),
	XmlDeclaration_t1545359137::get_offset_of_version_8(),
	XmlDeclaration_t1545359137_StaticFields::get_offset_of_U3CU3Ef__switchU24map30_9(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
