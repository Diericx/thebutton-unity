﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlDocument3649534162.h"
#include "System_Xml_System_Xml_XmlDocumentFragment3083262362.h"
#include "System_Xml_System_Xml_XmlDocumentType824160610.h"
#include "System_Xml_System_Xml_XmlElement2877111883.h"
#include "System_Xml_System_Xml_XmlEntityReference3053868353.h"
#include "System_Xml_System_Xml_XmlException4188277960.h"
#include "System_Xml_System_Xml_XmlImplementation1664517635.h"
#include "System_Xml_System_Xml_XmlConvert1936105738.h"
#include "System_Xml_System_Xml_XmlLinkedNode1287616130.h"
#include "System_Xml_System_Xml_XmlNameEntry3745551716.h"
#include "System_Xml_System_Xml_XmlNameEntryCache3855584002.h"
#include "System_Xml_System_Xml_XmlNameTable1345805268.h"
#include "System_Xml_System_Xml_XmlNamedNodeMap145210370.h"
#include "System_Xml_System_Xml_XmlNamespaceManager486731501.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsDecl3210081295.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsScope2513625351.h"
#include "System_Xml_System_Xml_XmlNode616554813.h"
#include "System_Xml_System_Xml_XmlNode_EmptyNodeList1718403287.h"
#include "System_Xml_System_Xml_XmlNodeChangedAction1188489541.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventArgs4036174778.h"
#include "System_Xml_System_Xml_XmlNodeList497326455.h"
#include "System_Xml_System_Xml_XmlNodeListChildren2811458520.h"
#include "System_Xml_System_Xml_XmlNodeListChildren_Enumerato569056069.h"
#include "System_Xml_System_Xml_XmlNodeReader1022603664.h"
#include "System_Xml_System_Xml_XmlNodeReaderImpl2982135230.h"
#include "System_Xml_System_Xml_XmlNodeType739504597.h"
#include "System_Xml_System_Xml_XmlParserContext2728039553.h"
#include "System_Xml_System_Xml_XmlParserContext_ContextItem1262420678.h"
#include "System_Xml_System_Xml_XmlProcessingInstruction431557540.h"
#include "System_Xml_System_Xml_XmlQualifiedName1944712516.h"
#include "System_Xml_System_Xml_XmlReader3675626668.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport1548133672.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_Comma1644897369.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_CharG1955031820.h"
#include "System_Xml_System_Xml_XmlReaderSettings1578612233.h"
#include "System_Xml_System_Xml_XmlResolver2024571559.h"
#include "System_Xml_System_Xml_XmlSignificantWhitespace1224054391.h"
#include "System_Xml_System_Xml_XmlSpace2880376877.h"
#include "System_Xml_System_Xml_XmlText4111601336.h"
#include "System_Xml_Mono_Xml2_XmlTextReader511376973.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlTokenInfo254587324.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlAttributeTok3353594030.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName2340974457.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputState3313602765.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputStateSt3023928423.h"
#include "System_Xml_System_Xml_XmlTextReader3514170725.h"
#include "System_Xml_System_Xml_XmlTokenizedType1619571710.h"
#include "System_Xml_System_Xml_XmlUrlResolver896669594.h"
#include "System_Xml_System_Xml_XmlValidatingReader3416770767.h"
#include "System_Xml_System_Xml_XmlWhitespace2557770518.h"
#include "System_Xml_System_Xml_XmlWriter1048088568.h"
#include "System_Xml_System_Xml_XmlTextWriter2527250655.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlNodeInfo3709371029.h"
#include "System_Xml_System_Xml_XmlTextWriter_StringUtil2068578019.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlDeclState3530111136.h"
#include "System_Xml_System_Xml_XmlStreamReader2725532304.h"
#include "System_Xml_System_Xml_NonBlockingStreamReader3963211903.h"
#include "System_Xml_System_Xml_XmlInputStream2650744719.h"
#include "System_Xml_System_Xml_XmlParserInput2366782760.h"
#include "System_Xml_System_Xml_XmlParserInput_XmlParserInputS25800784.h"
#include "System_Xml_Mono_Xml_Schema_XsdWhitespaceFacet2758097827.h"
#include "System_Xml_Mono_Xml_Schema_XsdOrdering3741887935.h"
#include "System_Xml_Mono_Xml_Schema_XsdAnySimpleType1096449895.h"
#include "System_Xml_Mono_Xml_Schema_XdtAnyAtomicType3359210273.h"
#include "System_Xml_Mono_Xml_Schema_XdtUntypedAtomic2904699188.h"
#include "System_Xml_Mono_Xml_Schema_XsdString263933896.h"
#include "System_Xml_Mono_Xml_Schema_XsdNormalizedString2132216291.h"
#include "System_Xml_Mono_Xml_Schema_XsdToken2902215462.h"
#include "System_Xml_Mono_Xml_Schema_XsdLanguage3897851897.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMToken547135877.h"
#include "System_Xml_Mono_Xml_Schema_XsdNMTokens1753890866.h"
#include "System_Xml_Mono_Xml_Schema_XsdName1409945702.h"
#include "System_Xml_Mono_Xml_Schema_XsdNCName414304927.h"
#include "System_Xml_Mono_Xml_Schema_XsdID1067193160.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRef1377311049.h"
#include "System_Xml_Mono_Xml_Schema_XsdIDRefs763119546.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntity2664305022.h"
#include "System_Xml_Mono_Xml_Schema_XsdEntities1103053540.h"
#include "System_Xml_Mono_Xml_Schema_XsdNotation720379093.h"
#include "System_Xml_Mono_Xml_Schema_XsdDecimal2932251550.h"
#include "System_Xml_Mono_Xml_Schema_XsdInteger1330502157.h"
#include "System_Xml_Mono_Xml_Schema_XsdLong4179235589.h"
#include "System_Xml_Mono_Xml_Schema_XsdInt1488443894.h"
#include "System_Xml_Mono_Xml_Schema_XsdShort1778530041.h"
#include "System_Xml_Mono_Xml_Schema_XsdByte1120972221.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonNegativeInteger3587933853.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedLong137890294.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedInt2956447959.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedShort3693774826.h"
#include "System_Xml_Mono_Xml_Schema_XsdUnsignedByte3216355454.h"
#include "System_Xml_Mono_Xml_Schema_XsdPositiveInteger1896481288.h"
#include "System_Xml_Mono_Xml_Schema_XsdNonPositiveInteger409343009.h"
#include "System_Xml_Mono_Xml_Schema_XsdNegativeInteger399444136.h"
#include "System_Xml_Mono_Xml_Schema_XsdFloat386143221.h"
#include "System_Xml_Mono_Xml_Schema_XsdDouble2510112208.h"
#include "System_Xml_Mono_Xml_Schema_XsdBase64Binary1094704629.h"
#include "System_Xml_Mono_Xml_Schema_XsdHexBinary3496718151.h"
#include "System_Xml_Mono_Xml_Schema_XsdQName930779123.h"
#include "System_Xml_Mono_Xml_Schema_XsdBoolean4126353587.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (XmlDocument_t3649534162), -1, sizeof(XmlDocument_t3649534162_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1800[20] = 
{
	XmlDocument_t3649534162_StaticFields::get_offset_of_optimal_create_types_5(),
	XmlDocument_t3649534162::get_offset_of_optimal_create_element_6(),
	XmlDocument_t3649534162::get_offset_of_optimal_create_attribute_7(),
	XmlDocument_t3649534162::get_offset_of_nameTable_8(),
	XmlDocument_t3649534162::get_offset_of_baseURI_9(),
	XmlDocument_t3649534162::get_offset_of_implementation_10(),
	XmlDocument_t3649534162::get_offset_of_preserveWhitespace_11(),
	XmlDocument_t3649534162::get_offset_of_resolver_12(),
	XmlDocument_t3649534162::get_offset_of_idTable_13(),
	XmlDocument_t3649534162::get_offset_of_nameCache_14(),
	XmlDocument_t3649534162::get_offset_of_lastLinkedChild_15(),
	XmlDocument_t3649534162::get_offset_of_schemas_16(),
	XmlDocument_t3649534162::get_offset_of_schemaInfo_17(),
	XmlDocument_t3649534162::get_offset_of_loadMode_18(),
	XmlDocument_t3649534162::get_offset_of_NodeChanged_19(),
	XmlDocument_t3649534162::get_offset_of_NodeChanging_20(),
	XmlDocument_t3649534162::get_offset_of_NodeInserted_21(),
	XmlDocument_t3649534162::get_offset_of_NodeInserting_22(),
	XmlDocument_t3649534162::get_offset_of_NodeRemoved_23(),
	XmlDocument_t3649534162::get_offset_of_NodeRemoving_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (XmlDocumentFragment_t3083262362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[1] = 
{
	XmlDocumentFragment_t3083262362::get_offset_of_lastLinkedChild_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (XmlDocumentType_t824160610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[3] = 
{
	XmlDocumentType_t824160610::get_offset_of_entities_6(),
	XmlDocumentType_t824160610::get_offset_of_notations_7(),
	XmlDocumentType_t824160610::get_offset_of_dtd_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (XmlElement_t2877111883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[5] = 
{
	XmlElement_t2877111883::get_offset_of_attributes_6(),
	XmlElement_t2877111883::get_offset_of_name_7(),
	XmlElement_t2877111883::get_offset_of_lastLinkedChild_8(),
	XmlElement_t2877111883::get_offset_of_isNotEmpty_9(),
	XmlElement_t2877111883::get_offset_of_schemaInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (XmlEntityReference_t3053868353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1804[2] = 
{
	XmlEntityReference_t3053868353::get_offset_of_entityName_6(),
	XmlEntityReference_t3053868353::get_offset_of_lastLinkedChild_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (XmlException_t4188277960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[5] = 
{
	XmlException_t4188277960::get_offset_of_lineNumber_11(),
	XmlException_t4188277960::get_offset_of_linePosition_12(),
	XmlException_t4188277960::get_offset_of_sourceUri_13(),
	XmlException_t4188277960::get_offset_of_res_14(),
	XmlException_t4188277960::get_offset_of_messages_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (XmlImplementation_t1664517635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1806[1] = 
{
	XmlImplementation_t1664517635::get_offset_of_InternalNameTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (XmlConvert_t1936105738), -1, sizeof(XmlConvert_t1936105738_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1807[8] = 
{
	XmlConvert_t1936105738_StaticFields::get_offset_of_datetimeFormats_0(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_defaultDateTimeFormats_1(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_roundtripDateTimeFormats_2(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_localDateTimeFormats_3(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_utcDateTimeFormats_4(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_unspecifiedDateTimeFormats_5(),
	XmlConvert_t1936105738_StaticFields::get_offset_of__defaultStyle_6(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_U3CU3Ef__switchU24map33_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (XmlLinkedNode_t1287616130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[1] = 
{
	XmlLinkedNode_t1287616130::get_offset_of_nextSibling_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (XmlNameEntry_t3745551716), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[5] = 
{
	XmlNameEntry_t3745551716::get_offset_of_Prefix_0(),
	XmlNameEntry_t3745551716::get_offset_of_LocalName_1(),
	XmlNameEntry_t3745551716::get_offset_of_NS_2(),
	XmlNameEntry_t3745551716::get_offset_of_Hash_3(),
	XmlNameEntry_t3745551716::get_offset_of_prefixed_name_cache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (XmlNameEntryCache_t3855584002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1810[4] = 
{
	XmlNameEntryCache_t3855584002::get_offset_of_table_0(),
	XmlNameEntryCache_t3855584002::get_offset_of_nameTable_1(),
	XmlNameEntryCache_t3855584002::get_offset_of_dummy_2(),
	XmlNameEntryCache_t3855584002::get_offset_of_cacheBuffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (XmlNameTable_t1345805268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (XmlNamedNodeMap_t145210370), -1, sizeof(XmlNamedNodeMap_t145210370_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1812[4] = 
{
	XmlNamedNodeMap_t145210370_StaticFields::get_offset_of_emptyEnumerator_0(),
	XmlNamedNodeMap_t145210370::get_offset_of_parent_1(),
	XmlNamedNodeMap_t145210370::get_offset_of_nodeList_2(),
	XmlNamedNodeMap_t145210370::get_offset_of_readOnly_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (XmlNamespaceManager_t486731501), -1, sizeof(XmlNamespaceManager_t486731501_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1813[9] = 
{
	XmlNamespaceManager_t486731501::get_offset_of_decls_0(),
	XmlNamespaceManager_t486731501::get_offset_of_declPos_1(),
	XmlNamespaceManager_t486731501::get_offset_of_scopes_2(),
	XmlNamespaceManager_t486731501::get_offset_of_scopePos_3(),
	XmlNamespaceManager_t486731501::get_offset_of_defaultNamespace_4(),
	XmlNamespaceManager_t486731501::get_offset_of_count_5(),
	XmlNamespaceManager_t486731501::get_offset_of_nameTable_6(),
	XmlNamespaceManager_t486731501::get_offset_of_internalAtomizedNames_7(),
	XmlNamespaceManager_t486731501_StaticFields::get_offset_of_U3CU3Ef__switchU24map25_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (NsDecl_t3210081295)+ sizeof (Il2CppObject), sizeof(NsDecl_t3210081295_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1814[2] = 
{
	NsDecl_t3210081295::get_offset_of_Prefix_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NsDecl_t3210081295::get_offset_of_Uri_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (NsScope_t2513625351)+ sizeof (Il2CppObject), sizeof(NsScope_t2513625351_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1815[2] = 
{
	NsScope_t2513625351::get_offset_of_DeclCount_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NsScope_t2513625351::get_offset_of_DefaultNamespace_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (XmlNode_t616554813), -1, sizeof(XmlNode_t616554813_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1816[5] = 
{
	XmlNode_t616554813_StaticFields::get_offset_of_emptyList_0(),
	XmlNode_t616554813::get_offset_of_ownerDocument_1(),
	XmlNode_t616554813::get_offset_of_parentNode_2(),
	XmlNode_t616554813::get_offset_of_childNodes_3(),
	XmlNode_t616554813_StaticFields::get_offset_of_U3CU3Ef__switchU24map2B_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (EmptyNodeList_t1718403287), -1, sizeof(EmptyNodeList_t1718403287_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1817[1] = 
{
	EmptyNodeList_t1718403287_StaticFields::get_offset_of_emptyEnumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (XmlNodeChangedAction_t1188489541)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1818[4] = 
{
	XmlNodeChangedAction_t1188489541::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (XmlNodeChangedEventArgs_t4036174778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[6] = 
{
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__oldParent_1(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__newParent_2(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__action_3(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__node_4(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__oldValue_5(),
	XmlNodeChangedEventArgs_t4036174778::get_offset_of__newValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (XmlNodeList_t497326455), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (XmlNodeListChildren_t2811458520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1821[1] = 
{
	XmlNodeListChildren_t2811458520::get_offset_of_parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (Enumerator_t569056069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1822[3] = 
{
	Enumerator_t569056069::get_offset_of_parent_0(),
	Enumerator_t569056069::get_offset_of_currentChild_1(),
	Enumerator_t569056069::get_offset_of_passedLastNode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (XmlNodeReader_t1022603664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1823[4] = 
{
	XmlNodeReader_t1022603664::get_offset_of_entity_2(),
	XmlNodeReader_t1022603664::get_offset_of_source_3(),
	XmlNodeReader_t1022603664::get_offset_of_entityInsideAttribute_4(),
	XmlNodeReader_t1022603664::get_offset_of_insideAttribute_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (XmlNodeReaderImpl_t2982135230), -1, sizeof(XmlNodeReaderImpl_t2982135230_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1824[11] = 
{
	XmlNodeReaderImpl_t2982135230::get_offset_of_document_2(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_startNode_3(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_current_4(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_ownerLinkedNode_5(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_state_6(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_depth_7(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_isEndElement_8(),
	XmlNodeReaderImpl_t2982135230::get_offset_of_ignoreStartNode_9(),
	XmlNodeReaderImpl_t2982135230_StaticFields::get_offset_of_U3CU3Ef__switchU24map34_10(),
	XmlNodeReaderImpl_t2982135230_StaticFields::get_offset_of_U3CU3Ef__switchU24map35_11(),
	XmlNodeReaderImpl_t2982135230_StaticFields::get_offset_of_U3CU3Ef__switchU24map36_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (XmlNodeType_t739504597)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1825[19] = 
{
	XmlNodeType_t739504597::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (XmlParserContext_t2728039553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1826[13] = 
{
	XmlParserContext_t2728039553::get_offset_of_baseURI_0(),
	XmlParserContext_t2728039553::get_offset_of_docTypeName_1(),
	XmlParserContext_t2728039553::get_offset_of_encoding_2(),
	XmlParserContext_t2728039553::get_offset_of_internalSubset_3(),
	XmlParserContext_t2728039553::get_offset_of_namespaceManager_4(),
	XmlParserContext_t2728039553::get_offset_of_nameTable_5(),
	XmlParserContext_t2728039553::get_offset_of_publicID_6(),
	XmlParserContext_t2728039553::get_offset_of_systemID_7(),
	XmlParserContext_t2728039553::get_offset_of_xmlLang_8(),
	XmlParserContext_t2728039553::get_offset_of_xmlSpace_9(),
	XmlParserContext_t2728039553::get_offset_of_contextItems_10(),
	XmlParserContext_t2728039553::get_offset_of_contextItemCount_11(),
	XmlParserContext_t2728039553::get_offset_of_dtd_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (ContextItem_t1262420678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1827[3] = 
{
	ContextItem_t1262420678::get_offset_of_BaseURI_0(),
	ContextItem_t1262420678::get_offset_of_XmlLang_1(),
	ContextItem_t1262420678::get_offset_of_XmlSpace_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (XmlProcessingInstruction_t431557540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1828[2] = 
{
	XmlProcessingInstruction_t431557540::get_offset_of_target_6(),
	XmlProcessingInstruction_t431557540::get_offset_of_data_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (XmlQualifiedName_t1944712516), -1, sizeof(XmlQualifiedName_t1944712516_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1829[4] = 
{
	XmlQualifiedName_t1944712516_StaticFields::get_offset_of_Empty_0(),
	XmlQualifiedName_t1944712516::get_offset_of_name_1(),
	XmlQualifiedName_t1944712516::get_offset_of_ns_2(),
	XmlQualifiedName_t1944712516::get_offset_of_hash_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (XmlReader_t3675626668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1830[2] = 
{
	XmlReader_t3675626668::get_offset_of_binary_0(),
	XmlReader_t3675626668::get_offset_of_settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (XmlReaderBinarySupport_t1548133672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1831[5] = 
{
	XmlReaderBinarySupport_t1548133672::get_offset_of_reader_0(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_base64CacheStartsAt_1(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_state_2(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_hasCache_3(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_dontReset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (CommandState_t1644897369)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1832[6] = 
{
	CommandState_t1644897369::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (CharGetter_t1955031820), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (XmlReaderSettings_t1578612233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1834[5] = 
{
	XmlReaderSettings_t1578612233::get_offset_of_checkCharacters_0(),
	XmlReaderSettings_t1578612233::get_offset_of_conformance_1(),
	XmlReaderSettings_t1578612233::get_offset_of_schemas_2(),
	XmlReaderSettings_t1578612233::get_offset_of_schemasNeedsInitialization_3(),
	XmlReaderSettings_t1578612233::get_offset_of_validationFlags_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (XmlResolver_t2024571559), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (XmlSignificantWhitespace_t1224054391), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (XmlSpace_t2880376877)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1837[4] = 
{
	XmlSpace_t2880376877::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (XmlText_t4111601336), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (XmlTextReader_t511376973), -1, sizeof(XmlTextReader_t511376973_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1839[54] = 
{
	XmlTextReader_t511376973::get_offset_of_cursorToken_2(),
	XmlTextReader_t511376973::get_offset_of_currentToken_3(),
	XmlTextReader_t511376973::get_offset_of_currentAttributeToken_4(),
	XmlTextReader_t511376973::get_offset_of_currentAttributeValueToken_5(),
	XmlTextReader_t511376973::get_offset_of_attributeTokens_6(),
	XmlTextReader_t511376973::get_offset_of_attributeValueTokens_7(),
	XmlTextReader_t511376973::get_offset_of_currentAttribute_8(),
	XmlTextReader_t511376973::get_offset_of_currentAttributeValue_9(),
	XmlTextReader_t511376973::get_offset_of_attributeCount_10(),
	XmlTextReader_t511376973::get_offset_of_parserContext_11(),
	XmlTextReader_t511376973::get_offset_of_nameTable_12(),
	XmlTextReader_t511376973::get_offset_of_nsmgr_13(),
	XmlTextReader_t511376973::get_offset_of_readState_14(),
	XmlTextReader_t511376973::get_offset_of_disallowReset_15(),
	XmlTextReader_t511376973::get_offset_of_depth_16(),
	XmlTextReader_t511376973::get_offset_of_elementDepth_17(),
	XmlTextReader_t511376973::get_offset_of_depthUp_18(),
	XmlTextReader_t511376973::get_offset_of_popScope_19(),
	XmlTextReader_t511376973::get_offset_of_elementNames_20(),
	XmlTextReader_t511376973::get_offset_of_elementNameStackPos_21(),
	XmlTextReader_t511376973::get_offset_of_allowMultipleRoot_22(),
	XmlTextReader_t511376973::get_offset_of_isStandalone_23(),
	XmlTextReader_t511376973::get_offset_of_returnEntityReference_24(),
	XmlTextReader_t511376973::get_offset_of_entityReferenceName_25(),
	XmlTextReader_t511376973::get_offset_of_valueBuffer_26(),
	XmlTextReader_t511376973::get_offset_of_reader_27(),
	XmlTextReader_t511376973::get_offset_of_peekChars_28(),
	XmlTextReader_t511376973::get_offset_of_peekCharsIndex_29(),
	XmlTextReader_t511376973::get_offset_of_peekCharsLength_30(),
	XmlTextReader_t511376973::get_offset_of_curNodePeekIndex_31(),
	XmlTextReader_t511376973::get_offset_of_preserveCurrentTag_32(),
	XmlTextReader_t511376973::get_offset_of_line_33(),
	XmlTextReader_t511376973::get_offset_of_column_34(),
	XmlTextReader_t511376973::get_offset_of_currentLinkedNodeLineNumber_35(),
	XmlTextReader_t511376973::get_offset_of_currentLinkedNodeLinePosition_36(),
	XmlTextReader_t511376973::get_offset_of_useProceedingLineInfo_37(),
	XmlTextReader_t511376973::get_offset_of_startNodeType_38(),
	XmlTextReader_t511376973::get_offset_of_currentState_39(),
	XmlTextReader_t511376973::get_offset_of_nestLevel_40(),
	XmlTextReader_t511376973::get_offset_of_readCharsInProgress_41(),
	XmlTextReader_t511376973::get_offset_of_binaryCharGetter_42(),
	XmlTextReader_t511376973::get_offset_of_namespaces_43(),
	XmlTextReader_t511376973::get_offset_of_whitespaceHandling_44(),
	XmlTextReader_t511376973::get_offset_of_resolver_45(),
	XmlTextReader_t511376973::get_offset_of_normalization_46(),
	XmlTextReader_t511376973::get_offset_of_checkCharacters_47(),
	XmlTextReader_t511376973::get_offset_of_prohibitDtd_48(),
	XmlTextReader_t511376973::get_offset_of_closeInput_49(),
	XmlTextReader_t511376973::get_offset_of_entityHandling_50(),
	XmlTextReader_t511376973::get_offset_of_whitespacePool_51(),
	XmlTextReader_t511376973::get_offset_of_whitespaceCache_52(),
	XmlTextReader_t511376973::get_offset_of_stateStack_53(),
	XmlTextReader_t511376973_StaticFields::get_offset_of_U3CU3Ef__switchU24map38_54(),
	XmlTextReader_t511376973_StaticFields::get_offset_of_U3CU3Ef__switchU24map39_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (XmlTokenInfo_t254587324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1840[13] = 
{
	XmlTokenInfo_t254587324::get_offset_of_valueCache_0(),
	XmlTokenInfo_t254587324::get_offset_of_Reader_1(),
	XmlTokenInfo_t254587324::get_offset_of_Name_2(),
	XmlTokenInfo_t254587324::get_offset_of_LocalName_3(),
	XmlTokenInfo_t254587324::get_offset_of_Prefix_4(),
	XmlTokenInfo_t254587324::get_offset_of_NamespaceURI_5(),
	XmlTokenInfo_t254587324::get_offset_of_IsEmptyElement_6(),
	XmlTokenInfo_t254587324::get_offset_of_QuoteChar_7(),
	XmlTokenInfo_t254587324::get_offset_of_LineNumber_8(),
	XmlTokenInfo_t254587324::get_offset_of_LinePosition_9(),
	XmlTokenInfo_t254587324::get_offset_of_ValueBufferStart_10(),
	XmlTokenInfo_t254587324::get_offset_of_ValueBufferEnd_11(),
	XmlTokenInfo_t254587324::get_offset_of_NodeType_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (XmlAttributeTokenInfo_t3353594030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1841[4] = 
{
	XmlAttributeTokenInfo_t3353594030::get_offset_of_ValueTokenStartIndex_13(),
	XmlAttributeTokenInfo_t3353594030::get_offset_of_ValueTokenEndIndex_14(),
	XmlAttributeTokenInfo_t3353594030::get_offset_of_valueCache_15(),
	XmlAttributeTokenInfo_t3353594030::get_offset_of_tmpBuilder_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (TagName_t2340974457)+ sizeof (Il2CppObject), sizeof(TagName_t2340974457_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1842[3] = 
{
	TagName_t2340974457::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagName_t2340974457::get_offset_of_LocalName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagName_t2340974457::get_offset_of_Prefix_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (DtdInputState_t3313602765)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1843[10] = 
{
	DtdInputState_t3313602765::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (DtdInputStateStack_t3023928423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1844[1] = 
{
	DtdInputStateStack_t3023928423::get_offset_of_intern_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (XmlTextReader_t3514170725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1845[5] = 
{
	XmlTextReader_t3514170725::get_offset_of_entity_2(),
	XmlTextReader_t3514170725::get_offset_of_source_3(),
	XmlTextReader_t3514170725::get_offset_of_entityInsideAttribute_4(),
	XmlTextReader_t3514170725::get_offset_of_insideAttribute_5(),
	XmlTextReader_t3514170725::get_offset_of_entityNameStack_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (XmlTokenizedType_t1619571710)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1846[14] = 
{
	XmlTokenizedType_t1619571710::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (XmlUrlResolver_t896669594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1847[1] = 
{
	XmlUrlResolver_t896669594::get_offset_of_credential_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (XmlValidatingReader_t3416770767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1848[12] = 
{
	XmlValidatingReader_t3416770767::get_offset_of_entityHandling_2(),
	XmlValidatingReader_t3416770767::get_offset_of_sourceReader_3(),
	XmlValidatingReader_t3416770767::get_offset_of_xmlTextReader_4(),
	XmlValidatingReader_t3416770767::get_offset_of_validatingReader_5(),
	XmlValidatingReader_t3416770767::get_offset_of_resolver_6(),
	XmlValidatingReader_t3416770767::get_offset_of_resolverSpecified_7(),
	XmlValidatingReader_t3416770767::get_offset_of_validationType_8(),
	XmlValidatingReader_t3416770767::get_offset_of_schemas_9(),
	XmlValidatingReader_t3416770767::get_offset_of_dtdReader_10(),
	XmlValidatingReader_t3416770767::get_offset_of_schemaInfo_11(),
	XmlValidatingReader_t3416770767::get_offset_of_storedCharacters_12(),
	XmlValidatingReader_t3416770767::get_offset_of_ValidationEventHandler_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (XmlWhitespace_t2557770518), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (XmlWriter_t1048088568), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (XmlTextWriter_t2527250655), -1, sizeof(XmlTextWriter_t2527250655_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1851[35] = 
{
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_unmarked_utf8encoding_0(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_escaped_text_chars_1(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_escaped_attr_chars_2(),
	XmlTextWriter_t2527250655::get_offset_of_base_stream_3(),
	XmlTextWriter_t2527250655::get_offset_of_source_4(),
	XmlTextWriter_t2527250655::get_offset_of_writer_5(),
	XmlTextWriter_t2527250655::get_offset_of_preserver_6(),
	XmlTextWriter_t2527250655::get_offset_of_preserved_name_7(),
	XmlTextWriter_t2527250655::get_offset_of_is_preserved_xmlns_8(),
	XmlTextWriter_t2527250655::get_offset_of_allow_doc_fragment_9(),
	XmlTextWriter_t2527250655::get_offset_of_close_output_stream_10(),
	XmlTextWriter_t2527250655::get_offset_of_ignore_encoding_11(),
	XmlTextWriter_t2527250655::get_offset_of_namespaces_12(),
	XmlTextWriter_t2527250655::get_offset_of_xmldecl_state_13(),
	XmlTextWriter_t2527250655::get_offset_of_check_character_validity_14(),
	XmlTextWriter_t2527250655::get_offset_of_newline_handling_15(),
	XmlTextWriter_t2527250655::get_offset_of_is_document_entity_16(),
	XmlTextWriter_t2527250655::get_offset_of_state_17(),
	XmlTextWriter_t2527250655::get_offset_of_node_state_18(),
	XmlTextWriter_t2527250655::get_offset_of_nsmanager_19(),
	XmlTextWriter_t2527250655::get_offset_of_open_count_20(),
	XmlTextWriter_t2527250655::get_offset_of_elements_21(),
	XmlTextWriter_t2527250655::get_offset_of_new_local_namespaces_22(),
	XmlTextWriter_t2527250655::get_offset_of_explicit_nsdecls_23(),
	XmlTextWriter_t2527250655::get_offset_of_namespace_handling_24(),
	XmlTextWriter_t2527250655::get_offset_of_indent_25(),
	XmlTextWriter_t2527250655::get_offset_of_indent_count_26(),
	XmlTextWriter_t2527250655::get_offset_of_indent_char_27(),
	XmlTextWriter_t2527250655::get_offset_of_indent_string_28(),
	XmlTextWriter_t2527250655::get_offset_of_newline_29(),
	XmlTextWriter_t2527250655::get_offset_of_indent_attributes_30(),
	XmlTextWriter_t2527250655::get_offset_of_quote_char_31(),
	XmlTextWriter_t2527250655::get_offset_of_v2_32(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_U3CU3Ef__switchU24map3A_33(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_U3CU3Ef__switchU24map3B_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (XmlNodeInfo_t3709371029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1852[7] = 
{
	XmlNodeInfo_t3709371029::get_offset_of_Prefix_0(),
	XmlNodeInfo_t3709371029::get_offset_of_LocalName_1(),
	XmlNodeInfo_t3709371029::get_offset_of_NS_2(),
	XmlNodeInfo_t3709371029::get_offset_of_HasSimple_3(),
	XmlNodeInfo_t3709371029::get_offset_of_HasElements_4(),
	XmlNodeInfo_t3709371029::get_offset_of_XmlLang_5(),
	XmlNodeInfo_t3709371029::get_offset_of_XmlSpace_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (StringUtil_t2068578019), -1, sizeof(StringUtil_t2068578019_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1853[2] = 
{
	StringUtil_t2068578019_StaticFields::get_offset_of_cul_0(),
	StringUtil_t2068578019_StaticFields::get_offset_of_cmp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (XmlDeclState_t3530111136)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1854[5] = 
{
	XmlDeclState_t3530111136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (XmlStreamReader_t2725532304), -1, sizeof(XmlStreamReader_t2725532304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1855[2] = 
{
	XmlStreamReader_t2725532304::get_offset_of_input_13(),
	XmlStreamReader_t2725532304_StaticFields::get_offset_of_invalidDataException_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (NonBlockingStreamReader_t3963211903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1856[11] = 
{
	NonBlockingStreamReader_t3963211903::get_offset_of_input_buffer_2(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoded_buffer_3(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoded_count_4(),
	NonBlockingStreamReader_t3963211903::get_offset_of_pos_5(),
	NonBlockingStreamReader_t3963211903::get_offset_of_buffer_size_6(),
	NonBlockingStreamReader_t3963211903::get_offset_of_encoding_7(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoder_8(),
	NonBlockingStreamReader_t3963211903::get_offset_of_base_stream_9(),
	NonBlockingStreamReader_t3963211903::get_offset_of_mayBlock_10(),
	NonBlockingStreamReader_t3963211903::get_offset_of_line_builder_11(),
	NonBlockingStreamReader_t3963211903::get_offset_of_foundCR_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (XmlInputStream_t2650744719), -1, sizeof(XmlInputStream_t2650744719_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1857[7] = 
{
	XmlInputStream_t2650744719_StaticFields::get_offset_of_StrictUTF8_2(),
	XmlInputStream_t2650744719::get_offset_of_enc_3(),
	XmlInputStream_t2650744719::get_offset_of_stream_4(),
	XmlInputStream_t2650744719::get_offset_of_buffer_5(),
	XmlInputStream_t2650744719::get_offset_of_bufLength_6(),
	XmlInputStream_t2650744719::get_offset_of_bufPos_7(),
	XmlInputStream_t2650744719_StaticFields::get_offset_of_encodingException_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (XmlParserInput_t2366782760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1858[5] = 
{
	XmlParserInput_t2366782760::get_offset_of_sourceStack_0(),
	XmlParserInput_t2366782760::get_offset_of_source_1(),
	XmlParserInput_t2366782760::get_offset_of_has_peek_2(),
	XmlParserInput_t2366782760::get_offset_of_peek_char_3(),
	XmlParserInput_t2366782760::get_offset_of_allowTextDecl_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (XmlParserInputSource_t25800784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1859[6] = 
{
	XmlParserInputSource_t25800784::get_offset_of_BaseURI_0(),
	XmlParserInputSource_t25800784::get_offset_of_reader_1(),
	XmlParserInputSource_t25800784::get_offset_of_state_2(),
	XmlParserInputSource_t25800784::get_offset_of_isPE_3(),
	XmlParserInputSource_t25800784::get_offset_of_line_4(),
	XmlParserInputSource_t25800784::get_offset_of_column_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (XsdWhitespaceFacet_t2758097827)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1861[4] = 
{
	XsdWhitespaceFacet_t2758097827::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (XsdOrdering_t3741887935)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1862[5] = 
{
	XsdOrdering_t3741887935::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (XsdAnySimpleType_t1096449895), -1, sizeof(XsdAnySimpleType_t1096449895_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1863[6] = 
{
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_instance_55(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_whitespaceArray_56(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_booleanAllowedFacets_57(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_decimalAllowedFacets_58(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_durationAllowedFacets_59(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_stringAllowedFacets_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (XdtAnyAtomicType_t3359210273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (XdtUntypedAtomic_t2904699188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (XsdString_t263933896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (XsdNormalizedString_t2132216291), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (XsdToken_t2902215462), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (XsdLanguage_t3897851897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (XsdNMToken_t547135877), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (XsdNMTokens_t1753890866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (XsdName_t1409945702), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (XsdNCName_t414304927), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (XsdID_t1067193160), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (XsdIDRef_t1377311049), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (XsdIDRefs_t763119546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (XsdEntity_t2664305022), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (XsdEntities_t1103053540), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (XsdNotation_t720379093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (XsdDecimal_t2932251550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (XsdInteger_t1330502157), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (XsdLong_t4179235589), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (XsdInt_t1488443894), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (XsdShort_t1778530041), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (XsdByte_t1120972221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (XsdNonNegativeInteger_t3587933853), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (XsdUnsignedLong_t137890294), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (XsdUnsignedInt_t2956447959), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (XsdUnsignedShort_t3693774826), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (XsdUnsignedByte_t3216355454), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (XsdPositiveInteger_t1896481288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (XsdNonPositiveInteger_t409343009), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (XsdNegativeInteger_t399444136), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (XsdFloat_t386143221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (XsdDouble_t2510112208), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (XsdBase64Binary_t1094704629), -1, sizeof(XsdBase64Binary_t1094704629_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1896[2] = 
{
	XsdBase64Binary_t1094704629_StaticFields::get_offset_of_ALPHABET_61(),
	XsdBase64Binary_t1094704629_StaticFields::get_offset_of_decodeTable_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (XsdHexBinary_t3496718151), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (XsdQName_t930779123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (XsdBoolean_t4126353587), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
