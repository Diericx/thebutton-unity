﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_WaitForSecondsRealtime2105307154.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1808633952.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram1120735295.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttri1027170048.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAtt665825653.h"
#include "UnityEngine_UnityEngine_Logger3328995178.h"
#include "UnityEngine_UnityEngine_Scripting_PreserveAttribut4182602970.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3212052468.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative1913052472.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri3673080018.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules1810425448.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1390152093.h"
#include "UnityEngine_UnityEngineInternal_GenericStack3718539591.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4275971970.h"
#include "Firebase_App_U3CModuleU3E3783534214.h"
#include "Firebase_App_MonoPInvokeCallbackAttribute1970456718.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE3515465473.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGException1412130252.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGException2876249339.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGException2443153790.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGPendingEx4193433529.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGStringHelp549488518.h"
#include "Firebase_App_Firebase_AppUtilPINVOKE_SWIGStringHelpe18001273.h"
#include "Firebase_App_Firebase_AppUtil2859790353.h"
#include "Firebase_App_Firebase_LogLevel543421840.h"
#include "Firebase_App_Firebase_FirebaseApp210707726.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler2907300047.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler_1988210674.h"
#include "Firebase_App_Firebase_FirebaseApp_FirebaseHandler_M323097478.h"
#include "Firebase_App_Firebase_FirebaseApp_DestroyDelegate3635929227.h"
#include "Firebase_App_Firebase_FirebaseApp_CreateDelegate413676709.h"
#include "Firebase_App_Firebase_AppOptions1641189195.h"
#include "Firebase_App_Firebase_InitResult105293995.h"
#include "Firebase_App_Firebase_FutureString4225986006.h"
#include "Firebase_App_Firebase_FutureString_Action3062064994.h"
#include "Firebase_App_Firebase_FutureString_MonoPInvokeCallb507919426.h"
#include "Firebase_App_Firebase_FutureString_SWIG_Completion1819656562.h"
#include "Firebase_App_Firebase_FutureString_U3CGetTaskU3Ec_4099292678.h"
#include "Firebase_App_Firebase_FutureBase2698306134.h"
#include "Firebase_App_Firebase_FutureStatus4011176069.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext1051733884.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_S692674473.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_1310454857.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_3693910080.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_3854140196.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_3450855669.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_1465756544.h"
#include "Firebase_App_Firebase_UnitySynchronizationContext_1465756541.h"
#include "Firebase_App_Firebase_Internal_InstallRootCerts1106242208.h"
#include "Firebase_App_Firebase_Internal_InstallRootCerts_U31447134549.h"
#include "Firebase_App_Firebase_InitializationException1438959983.h"
#include "Firebase_App_Firebase_FirebaseException2567272216.h"
#include "Firebase_App_Firebase_Internal_FirebaseConfigExtens617662701.h"
#include "Firebase_App_Firebase_Internal_FirebaseEditorExten1010167188.h"
#include "Google_MiniJson_U3CModuleU3E3783534214.h"
#include "Google_MiniJson_Google_MiniJSON_Json3279401392.h"
#include "Google_MiniJson_Google_MiniJSON_Json_Parser1529573252.h"
#include "Google_MiniJson_Google_MiniJSON_Json_Parser_TOKEN3628901966.h"
#include "Google_MiniJson_Google_MiniJSON_Json_Serializer3538635535.h"
#include "Google_Sharpen_U3CModuleU3E3783534214.h"
#include "Google_Sharpen_Google_Sharpen_WrappedSystemStream831474186.h"
#include "Google_Sharpen_Google_Sharpen_URLEncoder1142290277.h"
#include "Google_Sharpen_Google_Sharpen_TimeUnit4006728025.h"
#include "Google_Sharpen_Google_Sharpen_TimeUnitExtensions1386558595.h"
#include "Google_Sharpen_Google_Sharpen_ThreadPoolExecutor376308723.h"
#include "Google_Sharpen_Google_Sharpen_RunnableAction2631350173.h"
#include "Google_Sharpen_Google_Sharpen_ThreadFactory1392637388.h"
#include "Google_Sharpen_Google_Sharpen_Thread1322377586.h"
#include "Google_Sharpen_Google_Sharpen_ThreadGroup2181833315.h"
#include "Google_Sharpen_Google_Sharpen_ScheduledThreadPoolE2537379786.h"
#include "Google_Sharpen_Google_Sharpen_Scheduler2347715885.h"
#include "Google_Sharpen_Google_Sharpen_Scheduler_U3CHasTasks451012389.h"
#include "Google_Sharpen_Google_Sharpen_Runtime423218882.h"
#include "Google_Sharpen_Google_Sharpen_OutputStream3965982961.h"
#include "Google_Sharpen_Google_Sharpen_MessageDigest1820469897.h"
#include "Google_Sharpen_Google_Sharpen_InputStream39831546.h"
#include "Google_Sharpen_Google_Sharpen_Extensions996338116.h"
#include "Google_Sharpen_Google_Sharpen_Executors2516589264.h"
#include "Google_Sharpen_Google_Sharpen_CharacterCodingExcep3867687652.h"
#include "Google_Sharpen_Google_Sharpen_NoSuchElementExceptio335811679.h"
#include "Google_Sharpen_Google_Sharpen_UnsupportedEncodingEx551349759.h"
#include "Google_Sharpen_Google_Sharpen_URISyntaxException3236900932.h"
#include "Google_Sharpen_Google_Sharpen_Collections4125780067.h"
#include "Google_Sharpen_Google_Sharpen_AtomicLong1771203261.h"
#include "Google_Sharpen_Google_Sharpen_AtomicInteger4174655693.h"
#include "Google_Sharpen_Google_Sharpen_AtomicBoolean895648235.h"
#include "Google_Sharpen_Google_Sharpen_Arrays2916779344.h"
#include "Boo_Lang_U3CModuleU3E3783534214.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (Vector2_t2243707579)+ sizeof (Il2CppObject), sizeof(Vector2_t2243707579 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2400[3] = 
{
	Vector2_t2243707579::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector2_t2243707579::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (Vector4_t2243707581)+ sizeof (Il2CppObject), sizeof(Vector4_t2243707581 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2401[5] = 
{
	0,
	Vector4_t2243707581::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_z_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t2243707581::get_offset_of_w_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (WaitForSecondsRealtime_t2105307154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2402[1] = 
{
	WaitForSecondsRealtime_t2105307154::get_offset_of_waitTime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (AnimationPlayableUtilities_t1808633952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (FrameData_t1120735295)+ sizeof (Il2CppObject), sizeof(FrameData_t1120735295 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2404[4] = 
{
	FrameData_t1120735295::get_offset_of_m_UpdateId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_Time_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_LastTime_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t1120735295::get_offset_of_m_TimeScale_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (DefaultValueAttribute_t1027170048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2405[1] = 
{
	DefaultValueAttribute_t1027170048::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (ExcludeFromDocsAttribute_t665825653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (Logger_t3328995178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2409[3] = 
{
	Logger_t3328995178::get_offset_of_U3ClogHandlerU3Ek__BackingField_0(),
	Logger_t3328995178::get_offset_of_U3ClogEnabledU3Ek__BackingField_1(),
	Logger_t3328995178::get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (PreserveAttribute_t4182602970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (UsedByNativeCodeAttribute_t3212052468), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (RequiredByNativeCodeAttribute_t1913052472), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (FormerlySerializedAsAttribute_t3673080018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2413[1] = 
{
	FormerlySerializedAsAttribute_t3673080018::get_offset_of_m_oldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (TypeInferenceRules_t1810425448)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2414[5] = 
{
	TypeInferenceRules_t1810425448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (TypeInferenceRuleAttribute_t1390152093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2415[1] = 
{
	TypeInferenceRuleAttribute_t1390152093::get_offset_of__rule_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (GenericStack_t3718539591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (NetFxCoreExtensions_t4275971970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (U3CModuleU3E_t3783534225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (MonoPInvokeCallbackAttribute_t1970456718), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (AppUtilPINVOKE_t3515465473), -1, sizeof(AppUtilPINVOKE_t3515465473_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2420[2] = 
{
	AppUtilPINVOKE_t3515465473_StaticFields::get_offset_of_swigExceptionHelper_0(),
	AppUtilPINVOKE_t3515465473_StaticFields::get_offset_of_swigStringHelper_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (SWIGExceptionHelper_t1412130252), -1, sizeof(SWIGExceptionHelper_t1412130252_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2421[14] = 
{
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_applicationDelegate_0(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_arithmeticDelegate_1(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_divideByZeroDelegate_2(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_indexOutOfRangeDelegate_3(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_invalidCastDelegate_4(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_invalidOperationDelegate_5(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_ioDelegate_6(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_nullReferenceDelegate_7(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_outOfMemoryDelegate_8(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_overflowDelegate_9(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_systemDelegate_10(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_argumentDelegate_11(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_argumentNullDelegate_12(),
	SWIGExceptionHelper_t1412130252_StaticFields::get_offset_of_argumentOutOfRangeDelegate_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (ExceptionDelegate_t2876249339), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (ExceptionArgumentDelegate_t2443153790), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (SWIGPendingException_t4193433529), -1, sizeof(SWIGPendingException_t4193433529_StaticFields), sizeof(SWIGPendingException_t4193433529_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable2424[2] = 
{
	THREAD_STATIC_FIELD_OFFSET,
	SWIGPendingException_t4193433529_StaticFields::get_offset_of_numExceptionsPending_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (SWIGStringHelper_t549488518), -1, sizeof(SWIGStringHelper_t549488518_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2425[1] = 
{
	SWIGStringHelper_t549488518_StaticFields::get_offset_of_stringDelegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (SWIGStringDelegate_t18001273), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (AppUtil_t2859790353), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (LogLevel_t543421840)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2428[7] = 
{
	LogLevel_t543421840::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (FirebaseApp_t210707726), -1, sizeof(FirebaseApp_t210707726_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2429[7] = 
{
	FirebaseApp_t210707726::get_offset_of_swigCPtr_0(),
	FirebaseApp_t210707726::get_offset_of_swigCMemOwn_1(),
	FirebaseApp_t210707726_StaticFields::get_offset_of_nameToProxy_2(),
	FirebaseApp_t210707726_StaticFields::get_offset_of_cPtrToProxy_3(),
	FirebaseApp_t210707726_StaticFields::get_offset_of_cPtrRefCount_4(),
	FirebaseApp_t210707726::get_offset_of_destroy_5(),
	FirebaseApp_t210707726_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (FirebaseHandler_t2907300047), -1, sizeof(FirebaseHandler_t2907300047_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2430[2] = 
{
	FirebaseHandler_t2907300047_StaticFields::get_offset_of_firebaseHandler_2(),
	FirebaseHandler_t2907300047_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (LogMessageDelegate_t1988210674), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (MonoPInvokeCallbackAttribute_t323097478), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (DestroyDelegate_t3635929227), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (CreateDelegate_t413676709), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (AppOptions_t1641189195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2435[2] = 
{
	AppOptions_t1641189195::get_offset_of_swigCPtr_0(),
	AppOptions_t1641189195::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (InitResult_t105293995)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2436[3] = 
{
	InitResult_t105293995::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (FutureString_t4225986006), -1, sizeof(FutureString_t4225986006_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2437[6] = 
{
	FutureString_t4225986006::get_offset_of_swigCPtr_2(),
	FutureString_t4225986006_StaticFields::get_offset_of_Callbacks_3(),
	FutureString_t4225986006_StaticFields::get_offset_of_CallbackIndex_4(),
	FutureString_t4225986006_StaticFields::get_offset_of_CallbackLock_5(),
	FutureString_t4225986006::get_offset_of_callbackData_6(),
	FutureString_t4225986006::get_offset_of_SWIG_CompletionCB_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (Action_t3062064994), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (MonoPInvokeCallbackAttribute_t507919426), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (SWIG_CompletionDelegate_t1819656562), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (U3CGetTaskU3Ec__AnonStorey0_t4099292678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2441[2] = 
{
	U3CGetTaskU3Ec__AnonStorey0_t4099292678::get_offset_of_fu_0(),
	U3CGetTaskU3Ec__AnonStorey0_t4099292678::get_offset_of_tcs_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (FutureBase_t2698306134), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2442[2] = 
{
	FutureBase_t2698306134::get_offset_of_swigCPtr_0(),
	FutureBase_t2698306134::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (FutureStatus_t4011176069)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2443[4] = 
{
	FutureStatus_t4011176069::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (UnitySynchronizationContext_t1051733884), -1, sizeof(UnitySynchronizationContext_t1051733884_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2444[5] = 
{
	UnitySynchronizationContext_t1051733884_StaticFields::get_offset_of__instance_1(),
	UnitySynchronizationContext_t1051733884::get_offset_of_queue_2(),
	UnitySynchronizationContext_t1051733884::get_offset_of_behavior_3(),
	UnitySynchronizationContext_t1051733884::get_offset_of_mainThreadId_4(),
	UnitySynchronizationContext_t1051733884_StaticFields::get_offset_of_signalDictionary_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (SynchronizationContextBehavoir_t692674473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2445[1] = 
{
	SynchronizationContextBehavoir_t692674473::get_offset_of_callbackQueue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (U3CStartU3Ec__Iterator0_t1310454857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2446[6] = 
{
	U3CStartU3Ec__Iterator0_t1310454857::get_offset_of_U3CentryU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1310454857::get_offset_of_U24locvar0_1(),
	U3CStartU3Ec__Iterator0_t1310454857::get_offset_of_U24this_2(),
	U3CStartU3Ec__Iterator0_t1310454857::get_offset_of_U24current_3(),
	U3CStartU3Ec__Iterator0_t1310454857::get_offset_of_U24disposing_4(),
	U3CStartU3Ec__Iterator0_t1310454857::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (U3CSignaledCoroutineU3Ec__Iterator0_t3693910080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2447[5] = 
{
	U3CSignaledCoroutineU3Ec__Iterator0_t3693910080::get_offset_of_coroutine_0(),
	U3CSignaledCoroutineU3Ec__Iterator0_t3693910080::get_offset_of_newSignal_1(),
	U3CSignaledCoroutineU3Ec__Iterator0_t3693910080::get_offset_of_U24current_2(),
	U3CSignaledCoroutineU3Ec__Iterator0_t3693910080::get_offset_of_U24disposing_3(),
	U3CSignaledCoroutineU3Ec__Iterator0_t3693910080::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (U3CSendCoroutineU3Ec__AnonStorey1_t3854140196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2448[2] = 
{
	U3CSendCoroutineU3Ec__AnonStorey1_t3854140196::get_offset_of_coroutine_0(),
	U3CSendCoroutineU3Ec__AnonStorey1_t3854140196::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (U3CSendCoroutineU3Ec__AnonStorey2_t3450855669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2449[2] = 
{
	U3CSendCoroutineU3Ec__AnonStorey2_t3450855669::get_offset_of_newSignal_0(),
	U3CSendCoroutineU3Ec__AnonStorey2_t3450855669::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (U3CSendU3Ec__AnonStorey3_t1465756544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (U3CSendU3Ec__AnonStorey4_t1465756541), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (InstallRootCerts_t1106242208), -1, sizeof(InstallRootCerts_t1106242208_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2452[4] = 
{
	InstallRootCerts_t1106242208_StaticFields::get_offset_of_Sync_0(),
	InstallRootCerts_t1106242208_StaticFields::get_offset_of__installedRoots_1(),
	InstallRootCerts_t1106242208_StaticFields::get_offset_of_TrustedRoot_2(),
	InstallRootCerts_t1106242208_StaticFields::get_offset_of_IntermediateCA_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (U3CProcessU3Ec__AnonStorey0_t1447134549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2453[1] = 
{
	U3CProcessU3Ec__AnonStorey0_t1447134549::get_offset_of_originalCallback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (InitializationException_t1438959983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2454[1] = 
{
	InitializationException_t1438959983::get_offset_of_U3CInitResultU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (FirebaseException_t2567272216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2455[1] = 
{
	FirebaseException_t2567272216::get_offset_of_U3CErrorCodeU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (FirebaseConfigExtensions_t617662701), -1, sizeof(FirebaseConfigExtensions_t617662701_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2456[3] = 
{
	FirebaseConfigExtensions_t617662701_StaticFields::get_offset_of_Default_0(),
	FirebaseConfigExtensions_t617662701_StaticFields::get_offset_of_Sync_1(),
	FirebaseConfigExtensions_t617662701_StaticFields::get_offset_of_SStringState_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (FirebaseEditorExtensions_t1010167188), -1, sizeof(FirebaseEditorExtensions_t1010167188_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2457[1] = 
{
	FirebaseEditorExtensions_t1010167188_StaticFields::get_offset_of_SStringState_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (U3CModuleU3E_t3783534226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (Json_t3279401392), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (Parser_t1529573252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2460[1] = 
{
	Parser_t1529573252::get_offset_of_json_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (TOKEN_t3628901966)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2461[13] = 
{
	TOKEN_t3628901966::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (Serializer_t3538635535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2462[1] = 
{
	Serializer_t3538635535::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (U3CModuleU3E_t3783534227), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (WrappedSystemStream_t831474186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2464[4] = 
{
	WrappedSystemStream_t831474186::get_offset_of_ist_2(),
	WrappedSystemStream_t831474186::get_offset_of_ost_3(),
	WrappedSystemStream_t831474186::get_offset_of_position_4(),
	WrappedSystemStream_t831474186::get_offset_of_markedPosition_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (URLEncoder_t1142290277), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (TimeUnit_t4006728025)+ sizeof (Il2CppObject), sizeof(int64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2466[3] = 
{
	TimeUnit_t4006728025::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (TimeUnitExtensions_t1386558595), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (ThreadPoolExecutor_t376308723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2468[8] = 
{
	ThreadPoolExecutor_t376308723::get_offset_of__corePoolSize_0(),
	ThreadPoolExecutor_t376308723::get_offset_of__pendingTasks_1(),
	ThreadPoolExecutor_t376308723::get_offset_of__pool_2(),
	ThreadPoolExecutor_t376308723::get_offset_of__factory_3(),
	ThreadPoolExecutor_t376308723::get_offset_of__freeThreads_4(),
	ThreadPoolExecutor_t376308723::get_offset_of__maxPoolSize_5(),
	ThreadPoolExecutor_t376308723::get_offset_of__runningThreads_6(),
	ThreadPoolExecutor_t376308723::get_offset_of__shutdown_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (RunnableAction_t2631350173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2469[1] = 
{
	RunnableAction_t2631350173::get_offset_of__action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (ThreadFactory_t1392637388), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (Thread_t1322377586), -1, sizeof(Thread_t1322377586_StaticFields), sizeof(Thread_t1322377586_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable2471[6] = 
{
	Thread_t1322377586_StaticFields::get_offset_of_defaultGroup_0(),
	Thread_t1322377586::get_offset_of_interrupted_1(),
	Thread_t1322377586::get_offset_of_runnable_2(),
	Thread_t1322377586::get_offset_of_tgroup_3(),
	Thread_t1322377586::get_offset_of_thread_4(),
	THREAD_STATIC_FIELD_OFFSET,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (ThreadGroup_t2181833315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2472[1] = 
{
	ThreadGroup_t2181833315::get_offset_of_threads_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (ScheduledThreadPoolExecutor_t2537379786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2473[1] = 
{
	ScheduledThreadPoolExecutor_t2537379786::get_offset_of_executeExistingDelayedTasksAfterShutdownPolicy_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2474[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (Scheduler_t2347715885), -1, sizeof(Scheduler_t2347715885_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2476[4] = 
{
	Scheduler_t2347715885_StaticFields::get_offset_of_Instance_0(),
	Scheduler_t2347715885::get_offset_of_tasks_1(),
	Scheduler_t2347715885::get_offset_of_scheduler_2(),
	Scheduler_t2347715885::get_offset_of_newTask_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (U3CHasTasksU3Ec__AnonStorey0_t451012389), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2477[1] = 
{
	U3CHasTasksU3Ec__AnonStorey0_t451012389::get_offset_of_owner_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (Runtime_t423218882), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (OutputStream_t3965982961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2480[1] = 
{
	OutputStream_t3965982961::get_offset_of_Wrapped_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (MessageDigest_t1820469897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2482[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (InputStream_t39831546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2483[2] = 
{
	InputStream_t39831546::get_offset_of_mark_0(),
	InputStream_t39831546::get_offset_of_Wrapped_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (Extensions_t996338116), -1, sizeof(Extensions_t996338116_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2485[2] = 
{
	Extensions_t996338116_StaticFields::get_offset_of_EpochTicks_0(),
	Extensions_t996338116_StaticFields::get_offset_of_Utf8Encoder_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (Executors_t2516589264), -1, sizeof(Executors_t2516589264_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2486[1] = 
{
	Executors_t2516589264_StaticFields::get_offset_of_defaultThreadFactory_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (CharacterCodingException_t3867687652), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (NoSuchElementException_t335811679), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (UnsupportedEncodingException_t551349759), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (URISyntaxException_t3236900932), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2491[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (Collections_t4125780067), -1, sizeof(Collections_t4125780067_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2492[1] = 
{
	Collections_t4125780067_StaticFields::get_offset_of_empty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2494[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (AtomicLong_t1771203261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2495[1] = 
{
	AtomicLong_t1771203261::get_offset_of_val_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (AtomicInteger_t4174655693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2496[1] = 
{
	AtomicInteger_t4174655693::get_offset_of_val_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (AtomicBoolean_t895648235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2497[1] = 
{
	AtomicBoolean_t895648235::get_offset_of__actualValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (Arrays_t2916779344), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (U3CModuleU3E_t3783534228), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
