﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Boo_Lang_Boo_Lang_Builtins3763248930.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Dispa2227862569.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Dispa1307565918.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Dispat708950850.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Dispa1344382164.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Numer3345193737.h"
#include "Boo_Lang_Boo_Lang_Runtime_ExtensionRegistry1402255936.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices1910041954.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices_U3CGetEx1600278906.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices_U3CCoerce752649758.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices_U3CEmitIm187060723.h"
#include "Boo_Lang_Boo_Lang_Runtime_DynamicDispatching_Dispa2240407071.h"
#include "Firebase_Auth_U3CModuleU3E3783534214.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE2761888122.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE_SWIGExc326262631.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE_SWIGEx4033223266.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE_SWIGEx1735378451.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE_SWIGPe3156587018.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE_SWIGSt1725176893.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtilPINVOKE_SWIGStr553428412.h"
#include "Firebase_Auth_Firebase_Auth_AuthUtil2343916418.h"
#include "Firebase_Auth_Firebase_Auth_FirebaseAuth3105883899.h"
#include "Firebase_Auth_Firebase_Auth_FirebaseAuth_StateChan3080659151.h"
#include "Firebase_Auth_Firebase_Auth_FirebaseAuth_MonoPInvok403298259.h"
#include "Firebase_Auth_Firebase_Auth_FirebaseUser4046966602.h"
#include "Firebase_Auth_Firebase_Auth_Future_User3746551525.h"
#include "Firebase_Auth_Firebase_Auth_Future_User_Action1614918345.h"
#include "Firebase_Auth_Firebase_Auth_Future_User_MonoPInvoke629937593.h"
#include "Firebase_Auth_Firebase_Auth_Future_User_SWIG_Compl2830873745.h"
#include "Firebase_Auth_Firebase_Auth_Future_User_U3CGetTask2356161773.h"
#include "Firebase_Database_U3CModuleU3E3783534214.h"
#include "Firebase_Database_Firebase_Database_DatabaseError1067746743.h"
#include "Firebase_Database_Firebase_Database_DatabaseExcept1509032028.h"
#include "Firebase_Database_Firebase_Database_DatabaseRefere1167676104.h"
#include "Firebase_Database_Firebase_Database_DatabaseReferenc93014473.h"
#include "Firebase_Database_Firebase_Database_DatabaseRefere2944834882.h"
#include "Firebase_Database_Firebase_Database_DataSnapshot1287895350.h"
#include "Firebase_Database_Firebase_Database_InternalHelpers118531028.h"
#include "Firebase_Database_Firebase_Database_MutableData1171022152.h"
#include "Firebase_Database_Firebase_Database_Query2792659010.h"
#include "Firebase_Database_Firebase_Database_Query_Runnable1164582213.h"
#include "Firebase_Database_Firebase_Database_Query_Runnable2730666154.h"
#include "Firebase_Database_Firebase_Database_Query_U3CGetVa2125080077.h"
#include "Firebase_Database_Firebase_Database_Query_U3CAddLi3586663574.h"
#include "Firebase_Database_Firebase_Database_TransactionRes3107513211.h"
#include "Firebase_Database_Firebase_Database_ValueChangedEve929877234.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1429582413.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne2339493528.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1942506794.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1747391121.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne3406713242.h"
#include "Firebase_Database_Firebase_Database_Internal_Connec671917741.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1016574279.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne2292118836.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1904999661.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne1099507887.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne2050960365.h"
#include "Firebase_Database_Firebase_Database_Internal_Connect67058648.h"
#include "Firebase_Database_Firebase_Database_Internal_Connec774816424.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne3017763353.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne3450828338.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne3816656261.h"
#include "Firebase_Database_Firebase_Database_Internal_Connec164694114.h"
#include "Firebase_Database_Firebase_Database_Internal_Conne2407603808.h"
#include "Firebase_Database_Firebase_Database_Internal_Connec485289530.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (Builtins_t3763248930), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2502[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2503[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2504[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (DispatcherCache_t2227862569), -1, sizeof(DispatcherCache_t2227862569_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2505[1] = 
{
	DispatcherCache_t2227862569_StaticFields::get_offset_of__cache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (DispatcherFactory_t1307565918), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (DispatcherKey_t708950850), -1, sizeof(DispatcherKey_t708950850_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2507[4] = 
{
	DispatcherKey_t708950850_StaticFields::get_offset_of_EqualityComparer_0(),
	DispatcherKey_t708950850::get_offset_of__type_1(),
	DispatcherKey_t708950850::get_offset_of__name_2(),
	DispatcherKey_t708950850::get_offset_of__arguments_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (_EqualityComparer_t1344382164), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (NumericPromotions_t3345193737), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (ExtensionRegistry_t1402255936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[2] = 
{
	ExtensionRegistry_t1402255936::get_offset_of__extensions_0(),
	ExtensionRegistry_t1402255936::get_offset_of__classLock_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (RuntimeServices_t1910041954), -1, sizeof(RuntimeServices_t1910041954_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2512[5] = 
{
	RuntimeServices_t1910041954_StaticFields::get_offset_of_NoArguments_0(),
	RuntimeServices_t1910041954_StaticFields::get_offset_of_RuntimeServicesType_1(),
	RuntimeServices_t1910041954_StaticFields::get_offset_of__cache_2(),
	RuntimeServices_t1910041954_StaticFields::get_offset_of__extensions_3(),
	RuntimeServices_t1910041954_StaticFields::get_offset_of_True_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (U3CGetExtensionMethodsU3Ec__IteratorC_t1600278906), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2513[4] = 
{
	U3CGetExtensionMethodsU3Ec__IteratorC_t1600278906::get_offset_of_U3CU24s_49U3E__0_0(),
	U3CGetExtensionMethodsU3Ec__IteratorC_t1600278906::get_offset_of_U3CmemberU3E__1_1(),
	U3CGetExtensionMethodsU3Ec__IteratorC_t1600278906::get_offset_of_U24PC_2(),
	U3CGetExtensionMethodsU3Ec__IteratorC_t1600278906::get_offset_of_U24current_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (U3CCoerceU3Ec__AnonStorey1D_t752649758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2514[2] = 
{
	U3CCoerceU3Ec__AnonStorey1D_t752649758::get_offset_of_value_0(),
	U3CCoerceU3Ec__AnonStorey1D_t752649758::get_offset_of_toType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t187060723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2515[1] = 
{
	U3CEmitImplicitConversionDispatcherU3Ec__AnonStorey1E_t187060723::get_offset_of_method_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (Dispatcher_t2240407071), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (U3CModuleU3E_t3783534229), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (AuthUtilPINVOKE_t2761888122), -1, sizeof(AuthUtilPINVOKE_t2761888122_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2518[2] = 
{
	AuthUtilPINVOKE_t2761888122_StaticFields::get_offset_of_swigExceptionHelper_0(),
	AuthUtilPINVOKE_t2761888122_StaticFields::get_offset_of_swigStringHelper_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (SWIGExceptionHelper_t326262631), -1, sizeof(SWIGExceptionHelper_t326262631_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2519[14] = 
{
	SWIGExceptionHelper_t326262631_StaticFields::get_offset_of_applicationDelegate_0(),
	SWIGExceptionHelper_t326262631_StaticFields::get_offset_of_arithmeticDelegate_1(),
	SWIGExceptionHelper_t326262631_StaticFields::get_offset_of_divideByZeroDelegate_2(),
	SWIGExceptionHelper_t326262631_StaticFields::get_offset_of_indexOutOfRangeDelegate_3(),
	SWIGExceptionHelper_t326262631_StaticFields::get_offset_of_invalidCastDelegate_4(),
	SWIGExceptionHelper_t326262631_StaticFields::get_offset_of_invalidOperationDelegate_5(),
	SWIGExceptionHelper_t326262631_StaticFields::get_offset_of_ioDelegate_6(),
	SWIGExceptionHelper_t326262631_StaticFields::get_offset_of_nullReferenceDelegate_7(),
	SWIGExceptionHelper_t326262631_StaticFields::get_offset_of_outOfMemoryDelegate_8(),
	SWIGExceptionHelper_t326262631_StaticFields::get_offset_of_overflowDelegate_9(),
	SWIGExceptionHelper_t326262631_StaticFields::get_offset_of_systemDelegate_10(),
	SWIGExceptionHelper_t326262631_StaticFields::get_offset_of_argumentDelegate_11(),
	SWIGExceptionHelper_t326262631_StaticFields::get_offset_of_argumentNullDelegate_12(),
	SWIGExceptionHelper_t326262631_StaticFields::get_offset_of_argumentOutOfRangeDelegate_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (ExceptionDelegate_t4033223266), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (ExceptionArgumentDelegate_t1735378451), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (SWIGPendingException_t3156587018), -1, sizeof(SWIGPendingException_t3156587018_StaticFields), sizeof(SWIGPendingException_t3156587018_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable2522[2] = 
{
	THREAD_STATIC_FIELD_OFFSET,
	SWIGPendingException_t3156587018_StaticFields::get_offset_of_numExceptionsPending_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (SWIGStringHelper_t1725176893), -1, sizeof(SWIGStringHelper_t1725176893_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2523[1] = 
{
	SWIGStringHelper_t1725176893_StaticFields::get_offset_of_stringDelegate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (SWIGStringDelegate_t553428412), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (AuthUtil_t2343916418), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (FirebaseAuth_t3105883899), -1, sizeof(FirebaseAuth_t3105883899_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2526[8] = 
{
	FirebaseAuth_t3105883899::get_offset_of_swigCPtr_0(),
	FirebaseAuth_t3105883899::get_offset_of_swigCMemOwn_1(),
	FirebaseAuth_t3105883899::get_offset_of_appProxy_2(),
	FirebaseAuth_t3105883899::get_offset_of_appCPtr_3(),
	FirebaseAuth_t3105883899::get_offset_of_authStateListener_4(),
	FirebaseAuth_t3105883899::get_offset_of_StateChanged_5(),
	FirebaseAuth_t3105883899_StaticFields::get_offset_of_appCPtrToAuth_6(),
	FirebaseAuth_t3105883899_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (StateChangedDelegate_t3080659151), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (MonoPInvokeCallbackAttribute_t403298259), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (FirebaseUser_t4046966602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2529[2] = 
{
	FirebaseUser_t4046966602::get_offset_of_swigCMemOwn_0(),
	FirebaseUser_t4046966602::get_offset_of_swigCPtr_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (Future_User_t3746551525), -1, sizeof(Future_User_t3746551525_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2530[6] = 
{
	Future_User_t3746551525::get_offset_of_swigCPtr_2(),
	Future_User_t3746551525_StaticFields::get_offset_of_Callbacks_3(),
	Future_User_t3746551525_StaticFields::get_offset_of_CallbackIndex_4(),
	Future_User_t3746551525_StaticFields::get_offset_of_CallbackLock_5(),
	Future_User_t3746551525::get_offset_of_callbackData_6(),
	Future_User_t3746551525::get_offset_of_SWIG_CompletionCB_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (Action_t1614918345), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (MonoPInvokeCallbackAttribute_t629937593), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (SWIG_CompletionDelegate_t2830873745), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (U3CGetTaskU3Ec__AnonStorey0_t2356161773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2534[2] = 
{
	U3CGetTaskU3Ec__AnonStorey0_t2356161773::get_offset_of_fu_0(),
	U3CGetTaskU3Ec__AnonStorey0_t2356161773::get_offset_of_tcs_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (U3CModuleU3E_t3783534230), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (DatabaseError_t1067746743), -1, sizeof(DatabaseError_t1067746743_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2537[18] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	DatabaseError_t1067746743_StaticFields::get_offset_of_ErrorReasons_13(),
	DatabaseError_t1067746743_StaticFields::get_offset_of_ErrorCodes_14(),
	DatabaseError_t1067746743::get_offset_of_U3CCodeU3Ek__BackingField_15(),
	DatabaseError_t1067746743::get_offset_of_U3CMessageU3Ek__BackingField_16(),
	DatabaseError_t1067746743::get_offset_of_U3CDetailsU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (DatabaseException_t1509032028), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (DatabaseReference_t1167676104), -1, sizeof(DatabaseReference_t1167676104_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2539[2] = 
{
	DatabaseReference_t1167676104_StaticFields::get_offset_of__defaultConfig_4(),
	DatabaseReference_t1167676104_StaticFields::get_offset_of_SSync_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (CompletionListener_t93014473), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (Runnable226_t2944834882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2541[3] = 
{
	Runnable226_t2944834882::get_offset_of__enclosing_0(),
	Runnable226_t2944834882::get_offset_of__node_1(),
	Runnable226_t2944834882::get_offset_of__wrapped_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (DataSnapshot_t1287895350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2542[2] = 
{
	DataSnapshot_t1287895350::get_offset_of__node_0(),
	DataSnapshot_t1287895350::get_offset_of_U3CReferenceU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (InternalHelpers_t118531028), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (MutableData_t1171022152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2544[2] = 
{
	MutableData_t1171022152::get_offset_of__holder_0(),
	MutableData_t1171022152::get_offset_of__prefixPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (Query_t2792659010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[4] = 
{
	Query_t2792659010::get_offset_of__orderByCalled_0(),
	Query_t2792659010::get_offset_of_Params_1(),
	Query_t2792659010::get_offset_of_Path_2(),
	Query_t2792659010::get_offset_of_Repo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (Runnable182_t1164582213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2546[2] = 
{
	Runnable182_t1164582213::get_offset_of__enclosing_0(),
	Runnable182_t1164582213::get_offset_of__registration_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (Runnable192_t2730666154), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2547[2] = 
{
	Runnable192_t2730666154::get_offset_of__enclosing_0(),
	Runnable192_t2730666154::get_offset_of__listener_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (U3CGetValueAsyncU3Ec__AnonStorey0_t2125080077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2548[1] = 
{
	U3CGetValueAsyncU3Ec__AnonStorey0_t2125080077::get_offset_of_completionSource_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (U3CAddListenerForSingleValueEventU3Ec__AnonStorey1_t3586663574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2549[3] = 
{
	U3CAddListenerForSingleValueEventU3Ec__AnonStorey1_t3586663574::get_offset_of_wrapper_0(),
	U3CAddListenerForSingleValueEventU3Ec__AnonStorey1_t3586663574::get_offset_of_handler_1(),
	U3CAddListenerForSingleValueEventU3Ec__AnonStorey1_t3586663574::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (TransactionResult_t3107513211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2550[2] = 
{
	TransactionResult_t3107513211::get_offset_of__data_0(),
	TransactionResult_t3107513211::get_offset_of_U3CIsSuccessU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (ValueChangedEventArgs_t929877234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2552[2] = 
{
	ValueChangedEventArgs_t929877234::get_offset_of_U3CSnapshotU3Ek__BackingField_1(),
	ValueChangedEventArgs_t929877234::get_offset_of_U3CDatabaseErrorU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2553[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2554[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2556[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2559[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2560[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2561[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2563[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2565[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2569[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2570[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2571[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2572[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2573[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2574[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2575[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (CompoundHash_t1429582413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2576[2] = 
{
	CompoundHash_t1429582413::get_offset_of__hashes_0(),
	CompoundHash_t1429582413::get_offset_of__posts_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (Connection_t2339493528), -1, sizeof(Connection_t2339493528_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2577[6] = 
{
	Connection_t2339493528_StaticFields::get_offset_of__connectionIds_0(),
	Connection_t2339493528::get_offset_of__delegate_1(),
	Connection_t2339493528::get_offset_of__hostInfo_2(),
	Connection_t2339493528::get_offset_of__logger_3(),
	Connection_t2339493528::get_offset_of__conn_4(),
	Connection_t2339493528::get_offset_of__state_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (DisconnectReason_t1942506794)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2578[3] = 
{
	DisconnectReason_t1942506794::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (State_t1747391121)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2580[4] = 
{
	State_t1747391121::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (ConnectionAuthTokenProvider_t3406713242), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (ConnectionContext_t671917741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2583[6] = 
{
	ConnectionContext_t671917741::get_offset_of__authTokenProvider_0(),
	ConnectionContext_t671917741::get_offset_of__clientSdkVersion_1(),
	ConnectionContext_t671917741::get_offset_of__executorService_2(),
	ConnectionContext_t671917741::get_offset_of__logger_3(),
	ConnectionContext_t671917741::get_offset_of__persistenceEnabled_4(),
	ConnectionContext_t671917741::get_offset_of__userAgent_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (ConnectionUtils_t1016574279), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (HostInfo_t2292118836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2585[3] = 
{
	HostInfo_t2292118836::get_offset_of__host_0(),
	HostInfo_t2292118836::get_offset_of__namespace_1(),
	HostInfo_t2292118836::get_offset_of__secure_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (PersistentConnection_t1904999661), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (PersistentConnectionImpl_t1099507887), -1, sizeof(PersistentConnectionImpl_t1099507887_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2589[28] = 
{
	PersistentConnectionImpl_t1099507887_StaticFields::get_offset_of__connectionIds_0(),
	PersistentConnectionImpl_t1099507887::get_offset_of__authTokenProvider_1(),
	PersistentConnectionImpl_t1099507887::get_offset_of__context_2(),
	PersistentConnectionImpl_t1099507887::get_offset_of__delegate_3(),
	PersistentConnectionImpl_t1099507887::get_offset_of__executorService_4(),
	PersistentConnectionImpl_t1099507887::get_offset_of__hostInfo_5(),
	PersistentConnectionImpl_t1099507887::get_offset_of__interruptReasons_6(),
	PersistentConnectionImpl_t1099507887::get_offset_of__listens_7(),
	PersistentConnectionImpl_t1099507887::get_offset_of__logger_8(),
	PersistentConnectionImpl_t1099507887::get_offset_of__onDisconnectRequestQueue_9(),
	PersistentConnectionImpl_t1099507887::get_offset_of__outstandingPuts_10(),
	PersistentConnectionImpl_t1099507887::get_offset_of__requestCbHash_11(),
	PersistentConnectionImpl_t1099507887::get_offset_of__retryHelper_12(),
	PersistentConnectionImpl_t1099507887::get_offset_of__authToken_13(),
	PersistentConnectionImpl_t1099507887::get_offset_of__cachedHost_14(),
	PersistentConnectionImpl_t1099507887::get_offset_of__connectionState_15(),
	PersistentConnectionImpl_t1099507887::get_offset_of__currentGetTokenAttempt_16(),
	PersistentConnectionImpl_t1099507887::get_offset_of__firstConnection_17(),
	PersistentConnectionImpl_t1099507887::get_offset_of__forceAuthTokenRefresh_18(),
	PersistentConnectionImpl_t1099507887::get_offset_of__hasOnDisconnects_19(),
	PersistentConnectionImpl_t1099507887::get_offset_of__inactivityTimer_20(),
	PersistentConnectionImpl_t1099507887::get_offset_of__invalidAuthTokenCount_21(),
	PersistentConnectionImpl_t1099507887::get_offset_of__lastConnectionEstablishedTime_22(),
	PersistentConnectionImpl_t1099507887::get_offset_of__lastSessionId_23(),
	PersistentConnectionImpl_t1099507887::get_offset_of__lastWriteTimestamp_24(),
	PersistentConnectionImpl_t1099507887::get_offset_of__realtime_25(),
	PersistentConnectionImpl_t1099507887::get_offset_of__requestCounter_26(),
	PersistentConnectionImpl_t1099507887::get_offset_of__writeCounter_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (ListenQuerySpec_t2050960365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2591[3] = 
{
	ListenQuerySpec_t2050960365::get_offset_of_Path_0(),
	ListenQuerySpec_t2050960365::get_offset_of__pathAsString_1(),
	ListenQuerySpec_t2050960365::get_offset_of_QueryParams_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (OutstandingListen_t67058648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2592[4] = 
{
	OutstandingListen_t67058648::get_offset_of__hashFunction_0(),
	OutstandingListen_t67058648::get_offset_of_Query_1(),
	OutstandingListen_t67058648::get_offset_of_ResultCallback_2(),
	OutstandingListen_t67058648::get_offset_of__tag_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (OutstandingPut_t774816424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2593[4] = 
{
	OutstandingPut_t774816424::get_offset_of__action_0(),
	OutstandingPut_t774816424::get_offset_of_OnComplete_1(),
	OutstandingPut_t774816424::get_offset_of__request_2(),
	OutstandingPut_t774816424::get_offset_of__sent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (OutstandingDisconnect_t3017763353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2594[4] = 
{
	OutstandingDisconnect_t3017763353::get_offset_of__action_0(),
	OutstandingDisconnect_t3017763353::get_offset_of__data_1(),
	OutstandingDisconnect_t3017763353::get_offset_of__path_2(),
	OutstandingDisconnect_t3017763353::get_offset_of_OnComplete_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (ConnectionState_t3450828338)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2595[6] = 
{
	ConnectionState_t3450828338::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (Runnable557_t3816656261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2596[2] = 
{
	Runnable557_t3816656261::get_offset_of__enclosing_0(),
	Runnable557_t3816656261::get_offset_of__forceRefresh_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (GetTokenCallback567_t164694114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2597[2] = 
{
	GetTokenCallback567_t164694114::get_offset_of__enclosing_0(),
	GetTokenCallback567_t164694114::get_offset_of__thisGetTokenAttempt_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (ConnectionRequestCallback629_t2407603808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2598[1] = 
{
	ConnectionRequestCallback629_t2407603808::get_offset_of__onComplete_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (ConnectionRequestCallback797_t485289530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2599[2] = 
{
	ConnectionRequestCallback797_t485289530::get_offset_of__enclosing_0(),
	ConnectionRequestCallback797_t485289530::get_offset_of__restoreStateAfterComplete_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
