﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Firebase_Database_Firebase_Database_Internal_Utili1255537880.h"
#include "Firebase_Database_Firebase_Database_Internal_Utili2243316744.h"
#include "Firebase_Database_Firebase_Database_Internal_Utilit328222319.h"
#include "Firebase_Database_Firebase_Database_Internal_Utili1722631061.h"
#include "Firebase_Database_Firebase_Database_Core_AuthToken3681374264.h"
#include "Firebase_Database_Firebase_Database_Core_Platform739508161.h"
#include "Firebase_Database_Firebase_Database_Logger225270238.h"
#include "Firebase_Database_Firebase_Database_Logger_Level2798387899.h"
#include "Firebase_Database_Firebase_Database_DotNet_DotNetP2951135975.h"
#include "Firebase_Database_Firebase_Database_DotNet_DotNetPl601611002.h"
#include "Firebase_Database_Firebase_Database_DotNet_DotNetP1624051538.h"
#include "Firebase_Database_Firebase_Database_DotNet_DotNetP4147568072.h"
#include "Firebase_Database_Firebase_Database_Core_GAuthToke2345784186.h"
#include "Firebase_Database_Firebase_Database_Core_FirebaseC1046005831.h"
#include "Firebase_Database_Firebase_Database_Core_FirebaseC1927240834.h"
#include "Firebase_Database_Firebase_Database_Core_FirebaseC3493324775.h"
#include "Firebase_Database_Firebase_Database_FirebaseDataba1318758358.h"
#include "Firebase_Database_Firebase_Database_Internal_Base64124663375.h"
#include "Firebase_Database_Firebase_Database_Internal_Base64255634182.h"
#include "Firebase_Database_Firebase_Database_Internal_Base63204051639.h"
#include "Firebase_Database_U3CPrivateImplementationDetailsU2517675689.h"
#include "Firebase_Database_U3CPrivateImplementationDetailsU3424485092.h"
#include "MarkerMetro_Unity_WinLegacy_U3CModuleU3E3783534214.h"
#include "Firebase_Database_Unity_U3CModuleU3E3783534214.h"
#include "Firebase_Database_Unity_Firebase_Unity_Editor_Fireba83728876.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Go3945198801.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Sy3351332359.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se3398163339.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se2145858625.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se3497136277.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se1955556897.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se1559836771.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Ser520895791.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se3624071070.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se2286218796.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Se2915825156.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Tre390449921.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Pr2023989938.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Un3623770554.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Un2258131283.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Uni371495853.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Un4052243837.h"
#include "Firebase_Database_Unity_Firebase_Database_Unity_Un3054348951.h"
#include "JsonFx_Json_U3CModuleU3E3783534214.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonI1226473646.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonN2109979655.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonR2021823321.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonR1410336530.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonS3292412897.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonD1886537714.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonT1571553219.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonS4080780537.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonT2143717411.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonWr446744171.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonW2950311970.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonC4092422604.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_TypeC3996825458.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonMe797088438.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonO2568113370.h"
#include "UnityEngine_UI_U3CModuleU3E3783534214.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandle942672932.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg1967201810.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3959312622.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3365010046.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg2524067914.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1693084770.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirect1406276862.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycasterM3179336627.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou3960014691.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventD1524870173.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AbstractEv1333959294.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (ParsedUrl_t1255537880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2800[2] = 
{
	ParsedUrl_t1255537880::get_offset_of_Path_0(),
	ParsedUrl_t1255537880::get_offset_of_RepoInfo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (Utilities_t2243316744), -1, sizeof(Utilities_t2243316744_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2801[1] = 
{
	Utilities_t2243316744_StaticFields::get_offset_of_HexCharacters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (U3CWrapOnCompleteU3Ec__AnonStorey0_t328222319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2802[1] = 
{
	U3CWrapOnCompleteU3Ec__AnonStorey0_t328222319::get_offset_of_source_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (Validation_t1722631061), -1, sizeof(Validation_t1722631061_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2803[2] = 
{
	Validation_t1722631061_StaticFields::get_offset_of_InvalidPathRegex_0(),
	Validation_t1722631061_StaticFields::get_offset_of_InvalidKeyRegex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (AuthTokenProvider_t3681374264), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (Platform_t739508161), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (Logger_t225270238), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (Level_t2798387899)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2809[6] = 
{
	Level_t2798387899::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (DotNetPlatform_t2951135975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2812[1] = 
{
	DotNetPlatform_t2951135975::get_offset_of_U3CFirebaseAppU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (SynchronizationContextTarget_t601611002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2813[2] = 
{
	SynchronizationContextTarget_t601611002::get_offset_of__context_0(),
	SynchronizationContextTarget_t601611002::get_offset_of__executor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (U3CPostEventU3Ec__AnonStorey0_t1624051538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2814[1] = 
{
	U3CPostEventU3Ec__AnonStorey0_t1624051538::get_offset_of_r_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (DefaultRunLoop40_t4147568072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2815[1] = 
{
	DefaultRunLoop40_t4147568072::get_offset_of__logger_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (GAuthToken_t2345784186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2816[2] = 
{
	GAuthToken_t2345784186::get_offset_of__auth_0(),
	GAuthToken_t2345784186::get_offset_of__token_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (FirebaseConfigExtensions_t1046005831), -1, sizeof(FirebaseConfigExtensions_t1046005831_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2817[1] = 
{
	FirebaseConfigExtensions_t1046005831_StaticFields::get_offset_of__platformFactory_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (U3CCreatePlatformU3Ec__AnonStorey1_t1927240834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2818[1] = 
{
	U3CCreatePlatformU3Ec__AnonStorey1_t1927240834::get_offset_of_app_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (U3CCreatePlatformU3Ec__AnonStorey0_t3493324775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2819[2] = 
{
	U3CCreatePlatformU3Ec__AnonStorey0_t3493324775::get_offset_of_unityType_0(),
	U3CCreatePlatformU3Ec__AnonStorey0_t3493324775::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (FirebaseDatabase_t1318758358), -1, sizeof(FirebaseDatabase_t1318758358_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2820[8] = 
{
	0,
	FirebaseDatabase_t1318758358_StaticFields::get_offset_of_DatabaseInstances_1(),
	FirebaseDatabase_t1318758358_StaticFields::get_offset_of_SSync_2(),
	FirebaseDatabase_t1318758358::get_offset_of__config_3(),
	FirebaseDatabase_t1318758358::get_offset_of__repoInfo_4(),
	FirebaseDatabase_t1318758358::get_offset_of__sync_5(),
	FirebaseDatabase_t1318758358::get_offset_of__repo_6(),
	FirebaseDatabase_t1318758358::get_offset_of_U3CAppU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (Base64_t124663375), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (Coder_t4255634182), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2822[2] = 
{
	Coder_t4255634182::get_offset_of_Op_0(),
	Coder_t4255634182::get_offset_of_Output_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (Encoder_t3204051639), -1, sizeof(Encoder_t3204051639_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2823[9] = 
{
	Encoder_t3204051639_StaticFields::get_offset_of_Encode_2(),
	Encoder_t3204051639_StaticFields::get_offset_of_EncodeWebsafe_3(),
	Encoder_t3204051639::get_offset_of__alphabet_4(),
	Encoder_t3204051639::get_offset_of__tail_5(),
	Encoder_t3204051639::get_offset_of_DoCr_6(),
	Encoder_t3204051639::get_offset_of_DoNewline_7(),
	Encoder_t3204051639::get_offset_of_DoPadding_8(),
	Encoder_t3204051639::get_offset_of__count_9(),
	Encoder_t3204051639::get_offset_of_TailLen_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (U3CPrivateImplementationDetailsU3EU7Bc72063f3U2D67ccU2D4a85U2Db2d6U2D47e59e04d581U7D_t2517675689), -1, sizeof(U3CPrivateImplementationDetailsU3EU7Bc72063f3U2D67ccU2D4a85U2Db2d6U2D47e59e04d581U7D_t2517675689_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2824[2] = 
{
	U3CPrivateImplementationDetailsU3EU7Bc72063f3U2D67ccU2D4a85U2Db2d6U2D47e59e04d581U7D_t2517675689_StaticFields::get_offset_of_U24fieldU2D2_0(),
	U3CPrivateImplementationDetailsU3EU7Bc72063f3U2D67ccU2D4a85U2Db2d6U2D47e59e04d581U7D_t2517675689_StaticFields::get_offset_of_U24fieldU2D3_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (U24ArrayTypeU3D64_t3424485092)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D64_t3424485092 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (U3CModuleU3E_t3783534231), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (U3CModuleU3E_t3783534232), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (FirebaseEditorExtensions_t83728876), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (GoogleAuthConsts_t3945198801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2829[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (SystemClock_t3351332359), -1, sizeof(SystemClock_t3351332359_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2831[1] = 
{
	SystemClock_t3351332359_StaticFields::get_offset_of_Default_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (ServiceAccountCredential_t3398163339), -1, sizeof(ServiceAccountCredential_t3398163339_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2833[5] = 
{
	ServiceAccountCredential_t3398163339::get_offset_of_U3CIdU3Ek__BackingField_2(),
	ServiceAccountCredential_t3398163339::get_offset_of_U3CUserU3Ek__BackingField_3(),
	ServiceAccountCredential_t3398163339::get_offset_of_U3CScopesU3Ek__BackingField_4(),
	ServiceAccountCredential_t3398163339::get_offset_of_U3CKeyU3Ek__BackingField_5(),
	ServiceAccountCredential_t3398163339_StaticFields::get_offset_of_UnixEpoch_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (UploadCompleted_t2145858625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2834[1] = 
{
	UploadCompleted_t2145858625::get_offset_of_args_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (OAuthRequest_t3497136277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2835[2] = 
{
	OAuthRequest_t3497136277::get_offset_of_Assertion_0(),
	OAuthRequest_t3497136277::get_offset_of_ResponseBody_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (Initializer_t1955556897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2836[4] = 
{
	Initializer_t1955556897::get_offset_of_U3CIdU3Ek__BackingField_2(),
	Initializer_t1955556897::get_offset_of_U3CUserU3Ek__BackingField_3(),
	Initializer_t1955556897::get_offset_of_U3CScopesU3Ek__BackingField_4(),
	Initializer_t1955556897::get_offset_of_U3CKeyU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (U3CSendOAuthU3Ec__Iterator0_t1559836771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2837[8] = 
{
	U3CSendOAuthU3Ec__Iterator0_t1559836771::get_offset_of_U3CclientU3E__0_0(),
	U3CSendOAuthU3Ec__Iterator0_t1559836771::get_offset_of_U3CreqparmU3E__1_1(),
	U3CSendOAuthU3Ec__Iterator0_t1559836771::get_offset_of_request_2(),
	U3CSendOAuthU3Ec__Iterator0_t1559836771::get_offset_of_U24this_3(),
	U3CSendOAuthU3Ec__Iterator0_t1559836771::get_offset_of_U24current_4(),
	U3CSendOAuthU3Ec__Iterator0_t1559836771::get_offset_of_U24disposing_5(),
	U3CSendOAuthU3Ec__Iterator0_t1559836771::get_offset_of_U24PC_6(),
	U3CSendOAuthU3Ec__Iterator0_t1559836771::get_offset_of_U24locvar0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (U3CSendOAuthU3Ec__AnonStorey1_t520895791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2838[2] = 
{
	U3CSendOAuthU3Ec__AnonStorey1_t520895791::get_offset_of_uploadCompleted_0(),
	U3CSendOAuthU3Ec__AnonStorey1_t520895791::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_t3624071070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2839[2] = 
{
	U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_t3624071070::get_offset_of_request_0(),
	U3CGetAccessTokenForRequestSyncU3Ec__AnonStorey2_t3624071070::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (ServiceCredential_t2286218796), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2840[2] = 
{
	ServiceCredential_t2286218796::get_offset_of_U3CTokenServerUrlU3Ek__BackingField_0(),
	ServiceCredential_t2286218796::get_offset_of_U3CClockU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (Initializer_t2915825156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2841[2] = 
{
	Initializer_t2915825156::get_offset_of_U3CTokenServerUrlU3Ek__BackingField_0(),
	Initializer_t2915825156::get_offset_of_U3CClockU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (TreeNodeIListTransactionData_t390449921), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (PreserveAttribute_t2023989938), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (UnityPlatform_t3623770554), -1, sizeof(UnityPlatform_t3623770554_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2844[1] = 
{
	UnityPlatform_t3623770554_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (UnityLogger_t2258131283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2845[1] = 
{
	UnityLogger_t2258131283::get_offset_of__loglevel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (UnityAuthTokenProvider_t371495853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2846[1] = 
{
	UnityAuthTokenProvider_t371495853::get_offset_of__firebaseApp_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { sizeof (U3CGetTokenU3Ec__AnonStorey0_t4052243837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2847[1] = 
{
	U3CGetTokenU3Ec__AnonStorey0_t4052243837::get_offset_of_listener_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { sizeof (U3CAddTokenChangeListenerU3Ec__AnonStorey1_t3054348951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2848[1] = 
{
	U3CAddTokenChangeListenerU3Ec__AnonStorey1_t3054348951::get_offset_of_listener_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (U3CModuleU3E_t3783534233), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (JsonIgnoreAttribute_t1226473646), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { sizeof (JsonNameAttribute_t2109979655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2852[1] = 
{
	JsonNameAttribute_t2109979655::get_offset_of_jsonName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { sizeof (JsonReader_t2021823321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2853[6] = 
{
	JsonReader_t2021823321::get_offset_of_Settings_0(),
	JsonReader_t2021823321::get_offset_of_Source_1(),
	JsonReader_t2021823321::get_offset_of_SourceLength_2(),
	JsonReader_t2021823321::get_offset_of_index_3(),
	JsonReader_t2021823321::get_offset_of_previouslyDeserialized_4(),
	JsonReader_t2021823321::get_offset_of_jsArrays_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (JsonReaderSettings_t1410336530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2854[5] = 
{
	JsonReaderSettings_t1410336530::get_offset_of_Coercion_0(),
	JsonReaderSettings_t1410336530::get_offset_of_allowUnquotedObjectKeys_1(),
	JsonReaderSettings_t1410336530::get_offset_of_typeHintName_2(),
	JsonReaderSettings_t1410336530::get_offset_of_U3CHandleCyclicReferencesU3Ek__BackingField_3(),
	JsonReaderSettings_t1410336530::get_offset_of_converters_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (JsonSerializationException_t3292412897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (JsonDeserializationException_t1886537714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2856[1] = 
{
	JsonDeserializationException_t1886537714::get_offset_of_index_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (JsonTypeCoercionException_t1571553219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (JsonSpecifiedPropertyAttribute_t4080780537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2858[1] = 
{
	JsonSpecifiedPropertyAttribute_t4080780537::get_offset_of_specifiedProperty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (JsonToken_t2143717411)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2859[18] = 
{
	JsonToken_t2143717411::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (JsonWriter_t446744171), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2860[4] = 
{
	JsonWriter_t446744171::get_offset_of_Writer_0(),
	JsonWriter_t446744171::get_offset_of_settings_1(),
	JsonWriter_t446744171::get_offset_of_depth_2(),
	JsonWriter_t446744171::get_offset_of_previouslySerializedObjects_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (JsonWriterSettings_t2950311970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2862[10] = 
{
	JsonWriterSettings_t2950311970::get_offset_of_dateTimeSerializer_0(),
	JsonWriterSettings_t2950311970::get_offset_of_maxDepth_1(),
	JsonWriterSettings_t2950311970::get_offset_of_newLine_2(),
	JsonWriterSettings_t2950311970::get_offset_of_prettyPrint_3(),
	JsonWriterSettings_t2950311970::get_offset_of_tab_4(),
	JsonWriterSettings_t2950311970::get_offset_of_typeHintName_5(),
	JsonWriterSettings_t2950311970::get_offset_of_useXmlSerializationAttributes_6(),
	JsonWriterSettings_t2950311970::get_offset_of_U3CHandleCyclicReferencesU3Ek__BackingField_7(),
	JsonWriterSettings_t2950311970::get_offset_of_U3CDebugModeU3Ek__BackingField_8(),
	JsonWriterSettings_t2950311970::get_offset_of_converters_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (JsonConverter_t4092422604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (TypeCoercionUtility_t3996825458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2864[2] = 
{
	TypeCoercionUtility_t3996825458::get_offset_of_memberMapCache_0(),
	TypeCoercionUtility_t3996825458::get_offset_of_allowNullValueTypes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (JsonMemberAttribute_t797088438), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (JsonOptInAttribute_t2568113370), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (U3CModuleU3E_t3783534234), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (EventHandle_t942672932)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2868[3] = 
{
	EventHandle_t942672932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { sizeof (EventSystem_t3466835263), -1, sizeof(EventSystem_t3466835263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2887[12] = 
{
	EventSystem_t3466835263::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t3466835263::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CcurrentU3Ek__BackingField_4(),
	EventSystem_t3466835263::get_offset_of_m_FirstSelected_5(),
	EventSystem_t3466835263::get_offset_of_m_sendNavigationEvents_6(),
	EventSystem_t3466835263::get_offset_of_m_DragThreshold_7(),
	EventSystem_t3466835263::get_offset_of_m_CurrentSelected_8(),
	EventSystem_t3466835263::get_offset_of_m_Paused_9(),
	EventSystem_t3466835263::get_offset_of_m_SelectionGuard_10(),
	EventSystem_t3466835263::get_offset_of_m_DummyData_11(),
	EventSystem_t3466835263_StaticFields::get_offset_of_s_RaycastComparer_12(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { sizeof (EventTrigger_t1967201810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2888[2] = 
{
	EventTrigger_t1967201810::get_offset_of_m_Delegates_2(),
	EventTrigger_t1967201810::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { sizeof (TriggerEvent_t3959312622), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { sizeof (Entry_t3365010046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2890[2] = 
{
	Entry_t3365010046::get_offset_of_eventID_0(),
	Entry_t3365010046::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { sizeof (EventTriggerType_t2524067914)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2891[18] = 
{
	EventTriggerType_t2524067914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { sizeof (ExecuteEvents_t1693084770), -1, sizeof(ExecuteEvents_t1693084770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2892[36] = 
{
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_19(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_20(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_21(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_22(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_23(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_24(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_25(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_26(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_27(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_28(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_29(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_30(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_31(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_32(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_33(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_34(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { sizeof (MoveDirection_t1406276862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2894[6] = 
{
	MoveDirection_t1406276862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { sizeof (RaycasterManager_t3179336627), -1, sizeof(RaycasterManager_t3179336627_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2895[1] = 
{
	RaycasterManager_t3179336627_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2896 = { sizeof (RaycastResult_t21186376)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2896[10] = 
{
	RaycastResult_t21186376::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_module_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_index_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2897 = { sizeof (UIBehaviour_t3960014691), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2898 = { sizeof (AxisEventData_t1524870173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2898[2] = 
{
	AxisEventData_t1524870173::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t1524870173::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2899 = { sizeof (AbstractEventData_t1333959294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2899[1] = 
{
	AbstractEventData_t1333959294::get_offset_of_m_Used_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
