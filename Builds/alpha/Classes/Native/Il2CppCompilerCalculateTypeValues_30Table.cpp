﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransit1114673831.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent1896830814.h"
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup1030026315.h"
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistry1349564894.h"
#include "UnityEngine_UI_UnityEngine_UI_Clipping223789604.h"
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexCli3349113845.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter3114550109.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_As1166448724.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler2574720772.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMod987318053.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenM1916789528.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit3220761768.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter1325211874.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_Fi4030874534.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup1515633077.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corn1077473318.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis1431825778.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Cons3558160636.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou2875670365.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVertical1968298610.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup_U3CDelay3228926346.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder2155218138.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility4076838048.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup2468316403.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "UnityScript_Lang_U3CModuleU3E3783534214.h"
#include "UnityScript_Lang_UnityScript_Lang_Extensions2406697300.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_PubnubExample3094214262.h"
#include "AssemblyU2DCSharp_PubnubExample_PubnubState2523872396.h"
#include "AssemblyU2DCSharp_PubnubExample_U3CPublishMultiple1336100689.h"
#include "AssemblyU2DCSharp_PubnubExample_U3CRunCoroutineU3Ec579498094.h"
#include "AssemblyU2DCSharp_AuthUIHandler2288525084.h"
#include "AssemblyU2DCSharp_ButtonClickedText439765454.h"
#include "AssemblyU2DCSharp_FirebaseHandler1500858051.h"
#include "AssemblyU2DCSharp_User719925459.h"
#include "AssemblyU2DCSharp_ImageCapture1449800373.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_BuildReques3968482310.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_EventExtens4132974392.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CoroutineCl2476503358.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubWebRes647984363.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubWebRe3863823607.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ExceptionHa3031412530.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_Counter3542383138.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_Helpers2866752347.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_Helpers_U3C2641118174.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_JSONSeriali2861936230.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_JsonFxUnity4175252755.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_LoggingMetho447643578.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_LoggingMeth2240454990.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubErrorSe59688891.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubMessa1890139634.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubClient110317921.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubError3669959072.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubErrorF397889342.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubError2074922397.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubError2349065665.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3000 = { sizeof (ToggleTransition_t1114673831)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3000[3] = 
{
	ToggleTransition_t1114673831::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3001 = { sizeof (ToggleEvent_t1896830814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3002 = { sizeof (ToggleGroup_t1030026315), -1, sizeof(ToggleGroup_t1030026315_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3002[4] = 
{
	ToggleGroup_t1030026315::get_offset_of_m_AllowSwitchOff_2(),
	ToggleGroup_t1030026315::get_offset_of_m_Toggles_3(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3003 = { sizeof (ClipperRegistry_t1349564894), -1, sizeof(ClipperRegistry_t1349564894_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3003[2] = 
{
	ClipperRegistry_t1349564894_StaticFields::get_offset_of_s_Instance_0(),
	ClipperRegistry_t1349564894::get_offset_of_m_Clippers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3004 = { sizeof (Clipping_t223789604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3005 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3006 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3007 = { sizeof (RectangularVertexClipper_t3349113845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3007[2] = 
{
	RectangularVertexClipper_t3349113845::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t3349113845::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3008 = { sizeof (AspectRatioFitter_t3114550109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3008[4] = 
{
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3009 = { sizeof (AspectMode_t1166448724)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3009[6] = 
{
	AspectMode_t1166448724::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3010 = { sizeof (CanvasScaler_t2574720772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3010[14] = 
{
	CanvasScaler_t2574720772::get_offset_of_m_UiScaleMode_2(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferencePixelsPerUnit_3(),
	CanvasScaler_t2574720772::get_offset_of_m_ScaleFactor_4(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferenceResolution_5(),
	CanvasScaler_t2574720772::get_offset_of_m_ScreenMatchMode_6(),
	CanvasScaler_t2574720772::get_offset_of_m_MatchWidthOrHeight_7(),
	0,
	CanvasScaler_t2574720772::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2574720772::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2574720772::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2574720772::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2574720772::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3011 = { sizeof (ScaleMode_t987318053)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3011[4] = 
{
	ScaleMode_t987318053::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3012 = { sizeof (ScreenMatchMode_t1916789528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3012[4] = 
{
	ScreenMatchMode_t1916789528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3013 = { sizeof (Unit_t3220761768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3013[6] = 
{
	Unit_t3220761768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3014 = { sizeof (ContentSizeFitter_t1325211874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3014[4] = 
{
	ContentSizeFitter_t1325211874::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t1325211874::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3015 = { sizeof (FitMode_t4030874534)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3015[4] = 
{
	FitMode_t4030874534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3016 = { sizeof (GridLayoutGroup_t1515633077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3016[6] = 
{
	GridLayoutGroup_t1515633077::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t1515633077::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t1515633077::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t1515633077::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3017 = { sizeof (Corner_t1077473318)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3017[5] = 
{
	Corner_t1077473318::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3018 = { sizeof (Axis_t1431825778)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3018[3] = 
{
	Axis_t1431825778::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3019 = { sizeof (Constraint_t3558160636)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3019[4] = 
{
	Constraint_t3558160636::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3020 = { sizeof (HorizontalLayoutGroup_t2875670365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3021 = { sizeof (HorizontalOrVerticalLayoutGroup_t1968298610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3021[5] = 
{
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandHeight_12(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlWidth_13(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3022 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3023 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3024 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3025 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3026 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3027 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3027[7] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3028 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3028[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3029 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3029[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3030 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3030[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3031 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3031[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3032 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3033 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3034 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3034[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3035 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3035[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3036 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3036[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3037 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3037[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3038 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3039 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3040 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3041 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3042 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3042[9] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Normals_4(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_5(),
	VertexHelper_t385374196::get_offset_of_m_Indices_6(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3043 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3044 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3044[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3045 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3046 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3047 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3048 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3049 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3049[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3050 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3050[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3051 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3052 = { sizeof (U3CModuleU3E_t3783534235), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3053 = { sizeof (Extensions_t2406697300), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3054 = { sizeof (U3CModuleU3E_t3783534236), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3055 = { sizeof (PubnubExample_t3094214262), -1, sizeof(PubnubExample_t3094214262_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3055[58] = 
{
	PubnubExample_t3094214262::get_offset_of_ssl_2(),
	PubnubExample_t3094214262::get_offset_of_resumeOnReconnect_3(),
	PubnubExample_t3094214262::get_offset_of_cipherKey_4(),
	PubnubExample_t3094214262::get_offset_of_secretKey_5(),
	PubnubExample_t3094214262::get_offset_of_publishKey_6(),
	PubnubExample_t3094214262::get_offset_of_subscribeKey_7(),
	PubnubExample_t3094214262::get_offset_of_uuid_8(),
	PubnubExample_t3094214262::get_offset_of_subscribeTimeoutInSeconds_9(),
	PubnubExample_t3094214262::get_offset_of_operationTimeoutInSeconds_10(),
	PubnubExample_t3094214262::get_offset_of_networkMaxRetries_11(),
	PubnubExample_t3094214262::get_offset_of_networkRetryIntervalInSeconds_12(),
	PubnubExample_t3094214262::get_offset_of_heartbeatIntervalInSeconds_13(),
	PubnubExample_t3094214262_StaticFields::get_offset_of_showErrorMessageSegments_14(),
	PubnubExample_t3094214262::get_offset_of_channel_15(),
	PubnubExample_t3094214262::get_offset_of_publishedMessage_16(),
	PubnubExample_t3094214262::get_offset_of_publishedMetadataKey_17(),
	PubnubExample_t3094214262::get_offset_of_publishedMetadataValue_18(),
	PubnubExample_t3094214262::get_offset_of_pubChannel_19(),
	PubnubExample_t3094214262::get_offset_of_input_20(),
	PubnubExample_t3094214262_StaticFields::get_offset_of_pubnub_21(),
	PubnubExample_t3094214262::get_offset_of_state_22(),
	PubnubExample_t3094214262_StaticFields::get_offset_of_recordQueue_23(),
	PubnubExample_t3094214262::get_offset_of_scrollPosition_24(),
	PubnubExample_t3094214262::get_offset_of_pubnubApiResult_25(),
	PubnubExample_t3094214262::get_offset_of_requestInProcess_26(),
	PubnubExample_t3094214262::get_offset_of_showPublishPopupWindow_27(),
	PubnubExample_t3094214262::get_offset_of_showGrantWindow_28(),
	PubnubExample_t3094214262::get_offset_of_showAuthWindow_29(),
	PubnubExample_t3094214262::get_offset_of_showTextWindow_30(),
	PubnubExample_t3094214262::get_offset_of_showActionsPopupWindow_31(),
	PubnubExample_t3094214262::get_offset_of_showCGPopupWindow_32(),
	PubnubExample_t3094214262::get_offset_of_showPamPopupWindow_33(),
	PubnubExample_t3094214262::get_offset_of_toggle1_34(),
	PubnubExample_t3094214262::get_offset_of_toggle2_35(),
	PubnubExample_t3094214262::get_offset_of_valueToSet_36(),
	PubnubExample_t3094214262::get_offset_of_valueToSetSubs_37(),
	PubnubExample_t3094214262::get_offset_of_valueToSetAuthKey_38(),
	PubnubExample_t3094214262::get_offset_of_text1_39(),
	PubnubExample_t3094214262::get_offset_of_text2_40(),
	PubnubExample_t3094214262::get_offset_of_text3_41(),
	PubnubExample_t3094214262::get_offset_of_text4_42(),
	PubnubExample_t3094214262::get_offset_of_storeInHistory_43(),
	PubnubExample_t3094214262::get_offset_of_publishWindowRect_44(),
	PubnubExample_t3094214262::get_offset_of_authWindowRect_45(),
	PubnubExample_t3094214262::get_offset_of_textWindowRect_46(),
	PubnubExample_t3094214262::get_offset_of_textWindowRect2_47(),
	PubnubExample_t3094214262::get_offset_of_allowUserSettingsChange_48(),
	PubnubExample_t3094214262::get_offset_of_fLeft_49(),
	PubnubExample_t3094214262::get_offset_of_fLeftInit_50(),
	PubnubExample_t3094214262::get_offset_of_fTop_51(),
	PubnubExample_t3094214262::get_offset_of_fTopInit_52(),
	PubnubExample_t3094214262::get_offset_of_fRowHeight_53(),
	PubnubExample_t3094214262::get_offset_of_fHeight_54(),
	PubnubExample_t3094214262::get_offset_of_fButtonHeight_55(),
	PubnubExample_t3094214262::get_offset_of_fButtonWidth_56(),
	PubnubExample_t3094214262::get_offset_of_currentRTT_57(),
	PubnubExample_t3094214262_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_58(),
	PubnubExample_t3094214262_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_59(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3056 = { sizeof (PubnubState_t2523872396)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3056[42] = 
{
	PubnubState_t2523872396::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3057 = { sizeof (U3CPublishMultipleU3Ec__Iterator0_t1336100689), -1, sizeof(U3CPublishMultipleU3Ec__Iterator0_t1336100689_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3057[6] = 
{
	U3CPublishMultipleU3Ec__Iterator0_t1336100689::get_offset_of_U3CiU3E__0_0(),
	U3CPublishMultipleU3Ec__Iterator0_t1336100689::get_offset_of_U24this_1(),
	U3CPublishMultipleU3Ec__Iterator0_t1336100689::get_offset_of_U24current_2(),
	U3CPublishMultipleU3Ec__Iterator0_t1336100689::get_offset_of_U24disposing_3(),
	U3CPublishMultipleU3Ec__Iterator0_t1336100689::get_offset_of_U24PC_4(),
	U3CPublishMultipleU3Ec__Iterator0_t1336100689_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3058 = { sizeof (U3CRunCoroutineU3Ec__Iterator1_t579498094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3058[4] = 
{
	U3CRunCoroutineU3Ec__Iterator1_t579498094::get_offset_of_U24this_0(),
	U3CRunCoroutineU3Ec__Iterator1_t579498094::get_offset_of_U24current_1(),
	U3CRunCoroutineU3Ec__Iterator1_t579498094::get_offset_of_U24disposing_2(),
	U3CRunCoroutineU3Ec__Iterator1_t579498094::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3059 = { sizeof (AuthUIHandler_t2288525084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3059[3] = 
{
	AuthUIHandler_t2288525084::get_offset_of_mainMenu_2(),
	AuthUIHandler_t2288525084::get_offset_of_loginForm_3(),
	AuthUIHandler_t2288525084::get_offset_of_signUpForm_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3060 = { sizeof (ButtonClickedText_t439765454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3060[2] = 
{
	ButtonClickedText_t439765454::get_offset_of_posDelta_2(),
	ButtonClickedText_t439765454::get_offset_of_alphaDelta_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3061 = { sizeof (FirebaseHandler_t1500858051), -1, sizeof(FirebaseHandler_t1500858051_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3061[10] = 
{
	FirebaseHandler_t1500858051_StaticFields::get_offset_of_auth_2(),
	FirebaseHandler_t1500858051::get_offset_of_reference_3(),
	FirebaseHandler_t1500858051::get_offset_of_passwordInput_4(),
	FirebaseHandler_t1500858051::get_offset_of_emailInput_5(),
	FirebaseHandler_t1500858051::get_offset_of_usernameInput_6(),
	FirebaseHandler_t1500858051::get_offset_of_DEFAULT_EMAIL_7(),
	FirebaseHandler_t1500858051::get_offset_of_DEFAULT_PASS_8(),
	FirebaseHandler_t1500858051_StaticFields::get_offset_of_username_9(),
	FirebaseHandler_t1500858051_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
	FirebaseHandler_t1500858051_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3062 = { sizeof (User_t719925459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3062[2] = 
{
	User_t719925459::get_offset_of_username_0(),
	User_t719925459::get_offset_of_email_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3063 = { sizeof (ImageCapture_t1449800373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3063[3] = 
{
	ImageCapture_t1449800373::get_offset_of_webCamTexture_2(),
	ImageCapture_t1449800373::get_offset_of_stop_3(),
	ImageCapture_t1449800373::get_offset_of_photoCount_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3064 = { sizeof (BuildRequests_t3968482310), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3065 = { sizeof (EventExtensions_t4132974392), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3066 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3066[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3067 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3067[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3068 = { sizeof (CurrentRequestType_t3250227986)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3068[5] = 
{
	CurrentRequestType_t3250227986::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3069 = { sizeof (CoroutineClass_t2476503358), -1, sizeof(CoroutineClass_t2476503358_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3069[47] = 
{
	CoroutineClass_t2476503358_StaticFields::get_offset_of__unused_2(),
	CoroutineClass_t2476503358_StaticFields::get_offset_of__unused2_3(),
	CoroutineClass_t2476503358_StaticFields::get_offset_of__unused3_4(),
	CoroutineClass_t2476503358_StaticFields::get_offset_of__unused4_5(),
	CoroutineClass_t2476503358_StaticFields::get_offset_of__unused5_6(),
	CoroutineClass_t2476503358_StaticFields::get_offset_of__unused6_7(),
	CoroutineClass_t2476503358_StaticFields::get_offset_of__unused7_8(),
	CoroutineClass_t2476503358_StaticFields::get_offset_of__unused8_9(),
	CoroutineClass_t2476503358_StaticFields::get_offset_of__unused9_10(),
	CoroutineClass_t2476503358_StaticFields::get_offset_of__unused91_11(),
	CoroutineClass_t2476503358_StaticFields::get_offset_of__unused92_12(),
	CoroutineClass_t2476503358_StaticFields::get_offset_of__unused10_13(),
	CoroutineClass_t2476503358_StaticFields::get_offset_of__unused11_14(),
	CoroutineClass_t2476503358_StaticFields::get_offset_of__unused12_15(),
	CoroutineClass_t2476503358_StaticFields::get_offset_of__unused13_16(),
	CoroutineClass_t2476503358_StaticFields::get_offset_of__unused14_17(),
	CoroutineClass_t2476503358_StaticFields::get_offset_of__unused15_18(),
	CoroutineClass_t2476503358::get_offset_of_isHearbeatComplete_19(),
	CoroutineClass_t2476503358::get_offset_of_isPresenceHeartbeatComplete_20(),
	CoroutineClass_t2476503358::get_offset_of_isSubscribeComplete_21(),
	CoroutineClass_t2476503358::get_offset_of_isNonSubscribeComplete_22(),
	CoroutineClass_t2476503358::get_offset_of_SubCoroutine_23(),
	CoroutineClass_t2476503358::get_offset_of_SubTimeoutCoroutine_24(),
	CoroutineClass_t2476503358::get_offset_of_NonSubCoroutine_25(),
	CoroutineClass_t2476503358::get_offset_of_NonSubTimeoutCoroutine_26(),
	CoroutineClass_t2476503358::get_offset_of_PresenceHeartbeatCoroutine_27(),
	CoroutineClass_t2476503358::get_offset_of_PresenceHeartbeatTimeoutCoroutine_28(),
	CoroutineClass_t2476503358::get_offset_of_HeartbeatCoroutine_29(),
	CoroutineClass_t2476503358::get_offset_of_HeartbeatTimeoutCoroutine_30(),
	CoroutineClass_t2476503358::get_offset_of_DelayRequestCoroutineHB_31(),
	CoroutineClass_t2476503358::get_offset_of_DelayRequestCoroutinePHB_32(),
	CoroutineClass_t2476503358::get_offset_of_DelayRequestCoroutineSub_33(),
	CoroutineClass_t2476503358::get_offset_of_subscribeWww_34(),
	CoroutineClass_t2476503358::get_offset_of_heartbeatWww_35(),
	CoroutineClass_t2476503358::get_offset_of_presenceHeartbeatWww_36(),
	CoroutineClass_t2476503358::get_offset_of_nonSubscribeWww_37(),
	CoroutineClass_t2476503358::get_offset_of_subCoroutineComplete_38(),
	CoroutineClass_t2476503358::get_offset_of_nonSubCoroutineComplete_39(),
	CoroutineClass_t2476503358::get_offset_of_presenceHeartbeatCoroutineComplete_40(),
	CoroutineClass_t2476503358::get_offset_of_heartbeatCoroutineComplete_41(),
	CoroutineClass_t2476503358::get_offset_of_subscribeTimer_42(),
	CoroutineClass_t2476503358::get_offset_of_heartbeatTimer_43(),
	CoroutineClass_t2476503358::get_offset_of_presenceHeartbeatTimer_44(),
	CoroutineClass_t2476503358::get_offset_of_nonSubscribeTimer_45(),
	CoroutineClass_t2476503358::get_offset_of_heartbeatPauseTimer_46(),
	CoroutineClass_t2476503358::get_offset_of_presenceHeartbeatPauseTimer_47(),
	CoroutineClass_t2476503358::get_offset_of_subscribePauseTimer_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3070 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3070[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3071 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3071[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3072 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3072[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3073 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3073[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3074 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3074[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3075 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3075[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3076 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3076[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3077 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3077[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3078 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3078[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3079 = { sizeof (PubnubWebResponse_t647984363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3079[1] = 
{
	PubnubWebResponse_t647984363::get_offset_of_www_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3080 = { sizeof (PubnubWebRequest_t3863823607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3080[1] = 
{
	PubnubWebRequest_t3863823607::get_offset_of_www_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3081 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3081[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3082 = { sizeof (ExceptionHandlers_t3031412530), -1, sizeof(ExceptionHandlers_t3031412530_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3082[1] = 
{
	ExceptionHandlers_t3031412530_StaticFields::get_offset_of_multiplexException_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3083 = { sizeof (Counter_t3542383138), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3083[2] = 
{
	Counter_t3542383138::get_offset_of_current_0(),
	Counter_t3542383138::get_offset_of_syncRoot_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3084 = { sizeof (Helpers_t2866752347), -1, sizeof(Helpers_t2866752347_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3084[3] = 
{
	Helpers_t2866752347_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	Helpers_t2866752347_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	Helpers_t2866752347_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3085 = { sizeof (U3CGetDuplicatesU3Ec__Iterator0_t2641118174), -1, sizeof(U3CGetDuplicatesU3Ec__Iterator0_t2641118174_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3085[11] = 
{
	U3CGetDuplicatesU3Ec__Iterator0_t2641118174::get_offset_of_rawChannels_0(),
	U3CGetDuplicatesU3Ec__Iterator0_t2641118174::get_offset_of_U3CresultsU3E__0_1(),
	U3CGetDuplicatesU3Ec__Iterator0_t2641118174::get_offset_of_U24locvar0_2(),
	U3CGetDuplicatesU3Ec__Iterator0_t2641118174::get_offset_of_U3CgroupU3E__1_3(),
	U3CGetDuplicatesU3Ec__Iterator0_t2641118174::get_offset_of_U24locvar1_4(),
	U3CGetDuplicatesU3Ec__Iterator0_t2641118174::get_offset_of_U3CitemU3E__2_5(),
	U3CGetDuplicatesU3Ec__Iterator0_t2641118174::get_offset_of_U24current_6(),
	U3CGetDuplicatesU3Ec__Iterator0_t2641118174::get_offset_of_U24disposing_7(),
	U3CGetDuplicatesU3Ec__Iterator0_t2641118174::get_offset_of_U24PC_8(),
	U3CGetDuplicatesU3Ec__Iterator0_t2641118174_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
	U3CGetDuplicatesU3Ec__Iterator0_t2641118174_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3086 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3086[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3087 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3088 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3089 = { sizeof (JSONSerializer_t2861936230), -1, sizeof(JSONSerializer_t2861936230_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3089[1] = 
{
	JSONSerializer_t2861936230_StaticFields::get_offset_of_jsonPluggableLibrary_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3090 = { sizeof (JsonFxUnitySerializer_t4175252755), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3091 = { sizeof (LoggingMethod_t447643578), -1, sizeof(LoggingMethod_t447643578_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3091[1] = 
{
	LoggingMethod_t447643578_StaticFields::get_offset_of_logLevel_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3092 = { sizeof (Level_t2240454990)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3092[6] = 
{
	Level_t2240454990::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3093 = { sizeof (PubnubErrorSeverity_t59688891)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3093[4] = 
{
	PubnubErrorSeverity_t59688891::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3094 = { sizeof (PubnubMessageSource_t1890139634)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3094[3] = 
{
	PubnubMessageSource_t1890139634::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3095 = { sizeof (PubnubClientError_t110317921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3095[12] = 
{
	PubnubClientError_t110317921::get_offset_of__statusCode_0(),
	PubnubClientError_t110317921::get_offset_of__errorSeverity_1(),
	PubnubClientError_t110317921::get_offset_of__isDotNetException_2(),
	PubnubClientError_t110317921::get_offset_of__messageSource_3(),
	PubnubClientError_t110317921::get_offset_of__message_4(),
	PubnubClientError_t110317921::get_offset_of__channel_5(),
	PubnubClientError_t110317921::get_offset_of__channelGroup_6(),
	PubnubClientError_t110317921::get_offset_of__detailedDotNetException_7(),
	PubnubClientError_t110317921::get_offset_of__pubnubWebRequest_8(),
	PubnubClientError_t110317921::get_offset_of__pubnubWebResponse_9(),
	PubnubClientError_t110317921::get_offset_of__description_10(),
	PubnubClientError_t110317921::get_offset_of__dateTimeGMT_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3096 = { sizeof (PubnubErrorFilter_t3669959072), -1, sizeof(PubnubErrorFilter_t3669959072_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3096[1] = 
{
	PubnubErrorFilter_t3669959072_StaticFields::get_offset_of_errorLevel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3097 = { sizeof (Level_t397889342)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3097[4] = 
{
	Level_t397889342::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3098 = { sizeof (PubnubErrorCodeHelper_t2074922397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3099 = { sizeof (PubnubErrorCode_t2349065665)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3099[56] = 
{
	PubnubErrorCode_t2349065665::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
