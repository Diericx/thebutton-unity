﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubError3917390315.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_MD5CryptoSe1077286933.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_MD53967221866.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubCrypt3627838878.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_Pubnub2451529532.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CallbackTyp1848089183.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubCallba275514186.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubCrypt3412661541.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PubnubUnity2122492879.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ResponseTyp1248099179.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_StoredReques340008349.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_StoredReques255256256.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_SubscribeEn1077520116.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PNPresenceE3577011079.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PNMessageRes756120242.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PNPresenceEv939915784.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_SubscribeMe3739430639.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_TimetokenMe3476199713.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelIden1147162267.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelParam547936593.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelEnti3266154606.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_Subscriptio3881812953.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_PushTypeSer3531847941.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_Utility1684717438.h"
#include "AssemblyU2DCSharp_PubnubHandler3174799440.h"
#include "AssemblyU2DCSharp_ButtonPressPacket3076793663.h"
#include "AssemblyU2DCSharp_PhotoPacket2217737632.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CustomClas1119119311.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_PubnubDemo1053894922.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_PubnubDemoM254076932.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInte1691354350.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInter324141773.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInte3351738869.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInte2929920653.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInte3589972445.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInte4236681917.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInte1103676223.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInte1103676222.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInte1103676225.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInte3156701289.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInte1119155813.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInte4166525517.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInterg47555547.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInte2341681674.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInte1290049279.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInte4098566886.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInte3800429020.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInter612063986.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInte3639947701.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInte4018124012.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInte1364000944.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInte4086914571.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInte2438945764.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_CommonInter574709483.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestAbortS2694853646.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestAbortS1406312695.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestAlread3135694410.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestAlread1936135251.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestBounce3845500086.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestBounce1345070543.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestBounce4134311263.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestBounce3197359886.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestBounce1324311378.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestBounce2645351515.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestBounce3713701440.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestBounce3456075201.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCGAddL2353600463.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCGAddL4195216218.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCGCHAd3525210006.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCGCHAd3787879071.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCGCHAd3130264171.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCGCHAd3975013560.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestConnec4278862304.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestConnec2884833369.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorouti911951813.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorout3468753608.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorout1479832066.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorout3466280779.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorouti277379567.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorout1932954230.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorout1442572721.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorout2012502852.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorout2070611827.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorouti799381618.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorouti786852867.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorout3430217958.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorout3597817705.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorouti602355608.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorout3898362486.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorouti103610343.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorout3041896496.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorout2900771665.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorout3459747169.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorout1382381424.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorout1944684459.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { sizeof (PubnubErrorCodeDescription_t3917390315), -1, sizeof(PubnubErrorCodeDescription_t3917390315_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3100[1] = 
{
	PubnubErrorCodeDescription_t3917390315_StaticFields::get_offset_of_dictionaryCodes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { sizeof (MD5CryptoServiceProvider_t1077286933), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { sizeof (MD5_t3967221866), -1, sizeof(MD5_t3967221866_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3102[23] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	MD5_t3967221866_StaticFields::get_offset_of_PADDING_16(),
	MD5_t3967221866::get_offset_of_state_17(),
	MD5_t3967221866::get_offset_of_count_18(),
	MD5_t3967221866::get_offset_of_buffer_19(),
	MD5_t3967221866::get_offset_of_HashValue_20(),
	MD5_t3967221866::get_offset_of_State_21(),
	MD5_t3967221866::get_offset_of_HashSizeValue_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { sizeof (PubnubCryptoBase_t3627838878), -1, sizeof(PubnubCryptoBase_t3627838878_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3103[2] = 
{
	PubnubCryptoBase_t3627838878::get_offset_of_cipherKey_0(),
	PubnubCryptoBase_t3627838878_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { sizeof (Pubnub_t2451529532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3104[1] = 
{
	Pubnub_t2451529532::get_offset_of_pubnub_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3105[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { sizeof (CallbackType_t1848089183)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3106[7] = 
{
	CallbackType_t1848089183::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { sizeof (PubnubCallbacks_t275514186), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { sizeof (PubnubCrypto_t3412661541), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { sizeof (PubnubUnity_t2122492879), -1, sizeof(PubnubUnity_t2122492879_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3109[46] = 
{
	PubnubUnity_t2122492879::get_offset_of_PropertyChanged_0(),
	PubnubUnity_t2122492879_StaticFields::get_offset_of_gobj_1(),
	PubnubUnity_t2122492879::get_offset_of_localGobj_2(),
	PubnubUnity_t2122492879::get_offset_of_publishMessageCounter_3(),
	PubnubUnity_t2122492879::get_offset_of_coroutine_4(),
	PubnubUnity_t2122492879::get_offset_of_origin_5(),
	PubnubUnity_t2122492879::get_offset_of_publishKey_6(),
	PubnubUnity_t2122492879::get_offset_of_subscribeKey_7(),
	PubnubUnity_t2122492879::get_offset_of_secretKey_8(),
	PubnubUnity_t2122492879::get_offset_of_cipherKey_9(),
	PubnubUnity_t2122492879::get_offset_of_ssl_10(),
	PubnubUnity_t2122492879_StaticFields::get_offset_of_lastSubscribeTimetoken_11(),
	PubnubUnity_t2122492879_StaticFields::get_offset_of_lastSubscribeTimetokenForNewMultiplex_12(),
	0,
	PubnubUnity_t2122492879_StaticFields::get_offset_of_pnsdkVersion_14(),
	PubnubUnity_t2122492879::get_offset_of_pubnubWebRequestCallbackIntervalInSeconds_15(),
	PubnubUnity_t2122492879::get_offset_of_pubnubOperationTimeoutIntervalInSeconds_16(),
	PubnubUnity_t2122492879::get_offset_of_pubnubHeartbeatTimeoutIntervalInSeconds_17(),
	PubnubUnity_t2122492879::get_offset_of_pubnubNetworkTcpCheckIntervalInSeconds_18(),
	PubnubUnity_t2122492879::get_offset_of_pubnubNetworkCheckRetries_19(),
	PubnubUnity_t2122492879::get_offset_of_pubnubWebRequestRetryIntervalInSeconds_20(),
	PubnubUnity_t2122492879::get_offset_of_pubnubPresenceHeartbeatInSeconds_21(),
	PubnubUnity_t2122492879::get_offset_of_presenceHeartbeatIntervalInSeconds_22(),
	PubnubUnity_t2122492879::get_offset_of_requestDelayTime_23(),
	PubnubUnity_t2122492879::get_offset_of_enableResumeOnReconnect_24(),
	PubnubUnity_t2122492879::get_offset_of_uuidChanged_25(),
	PubnubUnity_t2122492879::get_offset_of_overrideTcpKeepAlive_26(),
	PubnubUnity_t2122492879::get_offset_of_enableJsonEncodingForPublish_27(),
	PubnubUnity_t2122492879::get_offset_of_pubnubLogLevel_28(),
	PubnubUnity_t2122492879::get_offset_of_errorLevel_29(),
	PubnubUnity_t2122492879::get_offset_of_resetTimetoken_30(),
	PubnubUnity_t2122492879::get_offset_of_keepHearbeatRunning_31(),
	PubnubUnity_t2122492879::get_offset_of_isHearbeatRunning_32(),
	PubnubUnity_t2122492879::get_offset_of_keepPresenceHearbeatRunning_33(),
	PubnubUnity_t2122492879::get_offset_of_isPresenceHearbeatRunning_34(),
	PubnubUnity_t2122492879::get_offset_of_internetStatus_35(),
	PubnubUnity_t2122492879::get_offset_of_retriesExceeded_36(),
	PubnubUnity_t2122492879::get_offset_of_retryCount_37(),
	PubnubUnity_t2122492879::get_offset_of_history_38(),
	PubnubUnity_t2122492879::get_offset_of_authenticationKey_39(),
	PubnubUnity_t2122492879::get_offset_of_pubnubUnitTest_40(),
	PubnubUnity_t2122492879_StaticFields::get_offset_of_jsonPluggableLibrary_41(),
	PubnubUnity_t2122492879::get_offset_of_sessionUUID_42(),
	PubnubUnity_t2122492879::get_offset_of_filterExpr_43(),
	PubnubUnity_t2122492879::get_offset_of_U3CRegionU3Ek__BackingField_44(),
	PubnubUnity_t2122492879_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_45(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3110[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { sizeof (ResponseType_t1248099179)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3111[31] = 
{
	ResponseType_t1248099179::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3112[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { sizeof (StoredRequestState_t340008349), -1, sizeof(StoredRequestState_t340008349_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3113[3] = 
{
	StoredRequestState_t340008349_StaticFields::get_offset_of_instance_0(),
	StoredRequestState_t340008349_StaticFields::get_offset_of_syncRoot_1(),
	StoredRequestState_t340008349::get_offset_of_requestStates_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { sizeof (U3CSetRequestStateU3Ec__AnonStorey0_t255256256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3114[1] = 
{
	U3CSetRequestStateU3Ec__AnonStorey0_t255256256::get_offset_of_reqState_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3115[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3116[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { sizeof (SubscribeEnvelope_t1077520116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3117[2] = 
{
	SubscribeEnvelope_t1077520116::get_offset_of_U3CmU3Ek__BackingField_0(),
	SubscribeEnvelope_t1077520116::get_offset_of_U3CtU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3118 = { sizeof (PNPresenceEvent_t3577011079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3118[5] = 
{
	PNPresenceEvent_t3577011079::get_offset_of_U3CActionU3Ek__BackingField_0(),
	PNPresenceEvent_t3577011079::get_offset_of_U3CUUIDU3Ek__BackingField_1(),
	PNPresenceEvent_t3577011079::get_offset_of_U3COccupancyU3Ek__BackingField_2(),
	PNPresenceEvent_t3577011079::get_offset_of_U3CTimestampU3Ek__BackingField_3(),
	PNPresenceEvent_t3577011079::get_offset_of_U3CStateU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3119 = { sizeof (PNMessageResult_t756120242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3119[7] = 
{
	PNMessageResult_t756120242::get_offset_of_U3CPayloadU3Ek__BackingField_0(),
	PNMessageResult_t756120242::get_offset_of_U3CSubscriptionU3Ek__BackingField_1(),
	PNMessageResult_t756120242::get_offset_of_U3CChannelU3Ek__BackingField_2(),
	PNMessageResult_t756120242::get_offset_of_U3CTimetokenU3Ek__BackingField_3(),
	PNMessageResult_t756120242::get_offset_of_U3COriginatingTimetokenU3Ek__BackingField_4(),
	PNMessageResult_t756120242::get_offset_of_U3CUserMetadataU3Ek__BackingField_5(),
	PNMessageResult_t756120242::get_offset_of_U3CIssuingClientIdU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3120 = { sizeof (PNPresenceEventResult_t939915784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3120[10] = 
{
	PNPresenceEventResult_t939915784::get_offset_of_U3CEventU3Ek__BackingField_0(),
	PNPresenceEventResult_t939915784::get_offset_of_U3CSubscriptionU3Ek__BackingField_1(),
	PNPresenceEventResult_t939915784::get_offset_of_U3CChannelU3Ek__BackingField_2(),
	PNPresenceEventResult_t939915784::get_offset_of_U3CUUIDU3Ek__BackingField_3(),
	PNPresenceEventResult_t939915784::get_offset_of_U3CTimestampU3Ek__BackingField_4(),
	PNPresenceEventResult_t939915784::get_offset_of_U3CTimetokenU3Ek__BackingField_5(),
	PNPresenceEventResult_t939915784::get_offset_of_U3COccupancyU3Ek__BackingField_6(),
	PNPresenceEventResult_t939915784::get_offset_of_U3CStateU3Ek__BackingField_7(),
	PNPresenceEventResult_t939915784::get_offset_of_U3CUserMetadataU3Ek__BackingField_8(),
	PNPresenceEventResult_t939915784::get_offset_of_U3CIssuingClientIdU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3121 = { sizeof (SubscribeMessage_t3739430639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3121[11] = 
{
	SubscribeMessage_t3739430639::get_offset_of_U3CaU3Ek__BackingField_0(),
	SubscribeMessage_t3739430639::get_offset_of_U3CbU3Ek__BackingField_1(),
	SubscribeMessage_t3739430639::get_offset_of_U3CcU3Ek__BackingField_2(),
	SubscribeMessage_t3739430639::get_offset_of_U3CdU3Ek__BackingField_3(),
	SubscribeMessage_t3739430639::get_offset_of_U3CfU3Ek__BackingField_4(),
	SubscribeMessage_t3739430639::get_offset_of_U3CiU3Ek__BackingField_5(),
	SubscribeMessage_t3739430639::get_offset_of_U3CkU3Ek__BackingField_6(),
	SubscribeMessage_t3739430639::get_offset_of_U3CsU3Ek__BackingField_7(),
	SubscribeMessage_t3739430639::get_offset_of_U3CoU3Ek__BackingField_8(),
	SubscribeMessage_t3739430639::get_offset_of_U3CpU3Ek__BackingField_9(),
	SubscribeMessage_t3739430639::get_offset_of_U3CuU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3122 = { sizeof (TimetokenMetadata_t3476199713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3122[2] = 
{
	TimetokenMetadata_t3476199713::get_offset_of_U3CtU3Ek__BackingField_0(),
	TimetokenMetadata_t3476199713::get_offset_of_U3CrU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3123 = { sizeof (ChannelIdentity_t1147162267)+ sizeof (Il2CppObject), sizeof(ChannelIdentity_t1147162267_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3123[3] = 
{
	ChannelIdentity_t1147162267::get_offset_of_U3CChannelOrChannelGroupNameU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ChannelIdentity_t1147162267::get_offset_of_U3CIsChannelGroupU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ChannelIdentity_t1147162267::get_offset_of_U3CIsPresenceChannelU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3124 = { sizeof (ChannelParameters_t547936593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3124[5] = 
{
	ChannelParameters_t547936593::get_offset_of_U3CIsAwaitingConnectCallbackU3Ek__BackingField_0(),
	ChannelParameters_t547936593::get_offset_of_U3CIsSubscribedU3Ek__BackingField_1(),
	ChannelParameters_t547936593::get_offset_of_U3CCallbacksU3Ek__BackingField_2(),
	ChannelParameters_t547936593::get_offset_of_U3CUserStateU3Ek__BackingField_3(),
	ChannelParameters_t547936593::get_offset_of_U3CTypeParameterTypeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3125 = { sizeof (ChannelEntity_t3266154606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3125[2] = 
{
	ChannelEntity_t3266154606::get_offset_of_ChannelID_0(),
	ChannelEntity_t3266154606::get_offset_of_ChannelParams_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3126 = { sizeof (Subscription_t3881812953), -1, sizeof(Subscription_t3881812953_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3126[17] = 
{
	Subscription_t3881812953_StaticFields::get_offset_of_instance_0(),
	Subscription_t3881812953_StaticFields::get_offset_of_syncRoot_1(),
	Subscription_t3881812953::get_offset_of_U3CHasChannelGroupsU3Ek__BackingField_2(),
	Subscription_t3881812953::get_offset_of_U3CHasPresenceChannelsU3Ek__BackingField_3(),
	Subscription_t3881812953::get_offset_of_U3CHasChannelsOrChannelGroupsU3Ek__BackingField_4(),
	Subscription_t3881812953::get_offset_of_U3CHasChannelsU3Ek__BackingField_5(),
	Subscription_t3881812953::get_offset_of_U3CCurrentSubscribedChannelsCountU3Ek__BackingField_6(),
	Subscription_t3881812953::get_offset_of_U3CCurrentSubscribedChannelGroupsCountU3Ek__BackingField_7(),
	Subscription_t3881812953::get_offset_of_U3CChannelsAndChannelGroupsAwaitingConnectCallbackU3Ek__BackingField_8(),
	Subscription_t3881812953::get_offset_of_U3CAllChannelsU3Ek__BackingField_9(),
	Subscription_t3881812953::get_offset_of_U3CAllChannelGroupsU3Ek__BackingField_10(),
	Subscription_t3881812953::get_offset_of_U3CAllSubscribedChannelsAndChannelGroupsU3Ek__BackingField_11(),
	Subscription_t3881812953::get_offset_of_U3CAllPresenceChannelsOrChannelGroupsU3Ek__BackingField_12(),
	Subscription_t3881812953::get_offset_of_U3CAllNonPresenceChannelsOrChannelGroupsU3Ek__BackingField_13(),
	Subscription_t3881812953::get_offset_of_U3CCompiledUserStateU3Ek__BackingField_14(),
	Subscription_t3881812953::get_offset_of_U3CConnectCallbackSentU3Ek__BackingField_15(),
	Subscription_t3881812953::get_offset_of_channelEntitiesDictionary_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3127 = { sizeof (PushTypeService_t3531847941)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3127[6] = 
{
	PushTypeService_t3531847941::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3128 = { sizeof (Utility_t1684717438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3128[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3129 = { sizeof (PubnubHandler_t3174799440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3129[19] = 
{
	PubnubHandler_t3174799440::get_offset_of_pubnub_2(),
	PubnubHandler_t3174799440::get_offset_of_textPrefab_3(),
	PubnubHandler_t3174799440::get_offset_of_canvas_4(),
	PubnubHandler_t3174799440::get_offset_of_theButton_5(),
	PubnubHandler_t3174799440::get_offset_of_theButtonRect_6(),
	PubnubHandler_t3174799440::get_offset_of_fireworksPrefabs_7(),
	PubnubHandler_t3174799440::get_offset_of_blackoutCube_8(),
	PubnubHandler_t3174799440::get_offset_of_winnerTxtObj_9(),
	PubnubHandler_t3174799440::get_offset_of_imageCaptureHolderObj_10(),
	PubnubHandler_t3174799440::get_offset_of_winnerPhotoObj_11(),
	PubnubHandler_t3174799440::get_offset_of_showWinnerTime_12(),
	PubnubHandler_t3174799440::get_offset_of_showWinnerTimeMax_13(),
	PubnubHandler_t3174799440::get_offset_of_minSpawnTime_14(),
	PubnubHandler_t3174799440::get_offset_of_maxSpawnTime_15(),
	PubnubHandler_t3174799440::get_offset_of_timer_16(),
	PubnubHandler_t3174799440::get_offset_of_nextTime_17(),
	PubnubHandler_t3174799440::get_offset_of_winner_18(),
	PubnubHandler_t3174799440::get_offset_of_auth_19(),
	PubnubHandler_t3174799440::get_offset_of_user_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3130 = { sizeof (ButtonPressPacket_t3076793663)+ sizeof (Il2CppObject), sizeof(ButtonPressPacket_t3076793663_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3130[2] = 
{
	ButtonPressPacket_t3076793663::get_offset_of_action_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ButtonPressPacket_t3076793663::get_offset_of_name_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3131 = { sizeof (PhotoPacket_t2217737632)+ sizeof (Il2CppObject), sizeof(PhotoPacket_t2217737632_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3131[2] = 
{
	PhotoPacket_t2217737632::get_offset_of_action_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PhotoPacket_t2217737632::get_offset_of_photo_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3132 = { sizeof (CustomClass_t1119119311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3132[2] = 
{
	CustomClass_t1119119311::get_offset_of_foo_0(),
	CustomClass_t1119119311::get_offset_of_bar_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3133 = { sizeof (PubnubDemoObject_t1053894922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3133[7] = 
{
	PubnubDemoObject_t1053894922::get_offset_of_VersionID_0(),
	PubnubDemoObject_t1053894922::get_offset_of_Timetoken_1(),
	PubnubDemoObject_t1053894922::get_offset_of_OperationName_2(),
	PubnubDemoObject_t1053894922::get_offset_of_Channels_3(),
	PubnubDemoObject_t1053894922::get_offset_of_DemoMessage_4(),
	PubnubDemoObject_t1053894922::get_offset_of_CustomMessage_5(),
	PubnubDemoObject_t1053894922::get_offset_of_SampleXml_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3134 = { sizeof (PubnubDemoMessage_t254076932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3134[1] = 
{
	PubnubDemoMessage_t254076932::get_offset_of_DefaultMessage_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3135 = { sizeof (CommonIntergrationTests_t1691354350), -1, sizeof(CommonIntergrationTests_t1691354350_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3135[22] = 
{
	CommonIntergrationTests_t1691354350_StaticFields::get_offset_of_PublishKey_0(),
	CommonIntergrationTests_t1691354350_StaticFields::get_offset_of_SubscribeKey_1(),
	CommonIntergrationTests_t1691354350_StaticFields::get_offset_of_SecretKey_2(),
	CommonIntergrationTests_t1691354350_StaticFields::get_offset_of_WaitTimeBetweenCalls_3(),
	CommonIntergrationTests_t1691354350_StaticFields::get_offset_of_WaitTimeBetweenCallsLow_4(),
	CommonIntergrationTests_t1691354350_StaticFields::get_offset_of_WaitTimeToReadResponse_5(),
	CommonIntergrationTests_t1691354350_StaticFields::get_offset_of_WaitTime_6(),
	CommonIntergrationTests_t1691354350::get_offset_of_pubnub_7(),
	CommonIntergrationTests_t1691354350::get_offset_of_U3CTimedOutU3Ek__BackingField_8(),
	CommonIntergrationTests_t1691354350::get_offset_of_U3CResponseU3Ek__BackingField_9(),
	CommonIntergrationTests_t1691354350::get_offset_of_U3CResponseStringU3Ek__BackingField_10(),
	CommonIntergrationTests_t1691354350::get_offset_of_U3CSubChannelU3Ek__BackingField_11(),
	CommonIntergrationTests_t1691354350::get_offset_of_U3CNameU3Ek__BackingField_12(),
	CommonIntergrationTests_t1691354350::get_offset_of_U3CErrorResponseU3Ek__BackingField_13(),
	CommonIntergrationTests_t1691354350::get_offset_of_U3CDeliveryStatusU3Ek__BackingField_14(),
	CommonIntergrationTests_t1691354350::get_offset_of_ExpectedMessage_15(),
	CommonIntergrationTests_t1691354350::get_offset_of_ExpectedChannels_16(),
	CommonIntergrationTests_t1691354350::get_offset_of_IsError_17(),
	CommonIntergrationTests_t1691354350::get_offset_of_IsTimeout_18(),
	CommonIntergrationTests_t1691354350::get_offset_of_Crt_19(),
	CommonIntergrationTests_t1691354350::get_offset_of_RespType_20(),
	CommonIntergrationTests_t1691354350_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3136 = { sizeof (U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3136[14] = 
{
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773::get_offset_of_testName_0(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773::get_offset_of_ssl_1(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773::get_offset_of_withCipher_2(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773::get_offset_of_U3CrU3E__1_3(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773::get_offset_of_U3CbGetAllCGU3E__5_4(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773::get_offset_of_U3CstrLogU3E__7_5(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773::get_offset_of_U3CbRemoveChU3E__13_6(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773::get_offset_of_U3CbGetChannel2U3E__14_7(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773::get_offset_of_U3CstrLog2U3E__15_8(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773::get_offset_of_U24this_9(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773::get_offset_of_U24current_10(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773::get_offset_of_U24disposing_11(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773::get_offset_of_U24PC_12(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator0_t324141773::get_offset_of_U24locvar0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3137 = { sizeof (U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3137[18] = 
{
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869::get_offset_of_testName_0(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869::get_offset_of_bAddChannel_1(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869::get_offset_of_cg_2(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869::get_offset_of_channel_3(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869::get_offset_of_bGetChannel_4(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869::get_offset_of_pubMessage_5(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869::get_offset_of_bSubMessage_6(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869::get_offset_of_bSubConnected_7(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869::get_offset_of_state_8(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869::get_offset_of_bSetState_9(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869::get_offset_of_bGetState_10(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869::get_offset_of_uuid_11(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869::get_offset_of_bHereNow_12(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869::get_offset_of_bHereNowState_13(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869::get_offset_of_bSetUserState2_14(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869::get_offset_of_bGetUserState2_15(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869::get_offset_of_bUnsub_16(),
	U3CDoCGAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey15_t3351738869::get_offset_of_U3CU3Ef__refU240_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3138 = { sizeof (U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3138[13] = 
{
	U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653::get_offset_of_testName_0(),
	U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653::get_offset_of_ssl_1(),
	U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653::get_offset_of_withCipher_2(),
	U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653::get_offset_of_U3CchannelU3E__0_3(),
	U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653::get_offset_of_asObject_4(),
	U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653::get_offset_of_U3CcommonPublishU3E__1_5(),
	U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653::get_offset_of_message_6(),
	U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653::get_offset_of_expectedStringResponse_7(),
	U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653::get_offset_of_matchExpectedStringResponse_8(),
	U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653::get_offset_of_U24this_9(),
	U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653::get_offset_of_U24current_10(),
	U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653::get_offset_of_U24disposing_11(),
	U3CDoSubscribeThenPublishAndParseU3Ec__Iterator1_t2929920653::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3139 = { sizeof (U3CDoTimeAndParseU3Ec__Iterator2_t3589972445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3139[7] = 
{
	U3CDoTimeAndParseU3Ec__Iterator2_t3589972445::get_offset_of_testName_0(),
	U3CDoTimeAndParseU3Ec__Iterator2_t3589972445::get_offset_of_ssl_1(),
	U3CDoTimeAndParseU3Ec__Iterator2_t3589972445::get_offset_of_asObject_2(),
	U3CDoTimeAndParseU3Ec__Iterator2_t3589972445::get_offset_of_U24this_3(),
	U3CDoTimeAndParseU3Ec__Iterator2_t3589972445::get_offset_of_U24current_4(),
	U3CDoTimeAndParseU3Ec__Iterator2_t3589972445::get_offset_of_U24disposing_5(),
	U3CDoTimeAndParseU3Ec__Iterator2_t3589972445::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3140 = { sizeof (U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3140[26] = 
{
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_testName_0(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_ssl_1(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_withCipher_2(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_U3CchannelU3E__0_3(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_U3CcommonPublishU3E__1_4(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_U3CstoreInHistoryU3E__2_5(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_noStore_6(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_U3CfieldsU3E__3_7(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_U3CstarttimeU3E__4_8(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_U3CmidtimeU3E__5_9(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_U3CcountU3E__6_10(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_numberOfMessages_11(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_asObject_12(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_messages_13(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_U24locvar0_14(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_U24locvar1_15(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_U3CmessageU3E__7_16(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_U24locvar2_17(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_U24locvar3_18(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_U3CmessageU3E__8_19(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_isParamsTest_20(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_U3CpassedU3E__9_21(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_U24this_22(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_U24current_23(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_U24disposing_24(),
	U3CDoPublishThenDetailedHistoryAndParseU3Ec__Iterator3_t4236681917::get_offset_of_U24PC_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3141 = { sizeof (U3CDoPublishAndParseU3Ec__Iterator4_t1103676223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3141[10] = 
{
	U3CDoPublishAndParseU3Ec__Iterator4_t1103676223::get_offset_of_testName_0(),
	U3CDoPublishAndParseU3Ec__Iterator4_t1103676223::get_offset_of_ssl_1(),
	U3CDoPublishAndParseU3Ec__Iterator4_t1103676223::get_offset_of_U3CchannelU3E__0_2(),
	U3CDoPublishAndParseU3Ec__Iterator4_t1103676223::get_offset_of_asObject_3(),
	U3CDoPublishAndParseU3Ec__Iterator4_t1103676223::get_offset_of_message_4(),
	U3CDoPublishAndParseU3Ec__Iterator4_t1103676223::get_offset_of_expected_5(),
	U3CDoPublishAndParseU3Ec__Iterator4_t1103676223::get_offset_of_U24this_6(),
	U3CDoPublishAndParseU3Ec__Iterator4_t1103676223::get_offset_of_U24current_7(),
	U3CDoPublishAndParseU3Ec__Iterator4_t1103676223::get_offset_of_U24disposing_8(),
	U3CDoPublishAndParseU3Ec__Iterator4_t1103676223::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3142 = { sizeof (U3CDoPublishAndParseU3Ec__Iterator5_t1103676222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3142[12] = 
{
	U3CDoPublishAndParseU3Ec__Iterator5_t1103676222::get_offset_of_U3CchannelU3E__0_0(),
	U3CDoPublishAndParseU3Ec__Iterator5_t1103676222::get_offset_of_noSecretKey_1(),
	U3CDoPublishAndParseU3Ec__Iterator5_t1103676222::get_offset_of_testName_2(),
	U3CDoPublishAndParseU3Ec__Iterator5_t1103676222::get_offset_of_ssl_3(),
	U3CDoPublishAndParseU3Ec__Iterator5_t1103676222::get_offset_of_withCipher_4(),
	U3CDoPublishAndParseU3Ec__Iterator5_t1103676222::get_offset_of_asObject_5(),
	U3CDoPublishAndParseU3Ec__Iterator5_t1103676222::get_offset_of_message_6(),
	U3CDoPublishAndParseU3Ec__Iterator5_t1103676222::get_offset_of_expected_7(),
	U3CDoPublishAndParseU3Ec__Iterator5_t1103676222::get_offset_of_U24this_8(),
	U3CDoPublishAndParseU3Ec__Iterator5_t1103676222::get_offset_of_U24current_9(),
	U3CDoPublishAndParseU3Ec__Iterator5_t1103676222::get_offset_of_U24disposing_10(),
	U3CDoPublishAndParseU3Ec__Iterator5_t1103676222::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3143 = { sizeof (U3CDoPublishAndParseU3Ec__Iterator6_t1103676225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3143[11] = 
{
	U3CDoPublishAndParseU3Ec__Iterator6_t1103676225::get_offset_of_testName_0(),
	U3CDoPublishAndParseU3Ec__Iterator6_t1103676225::get_offset_of_ssl_1(),
	U3CDoPublishAndParseU3Ec__Iterator6_t1103676225::get_offset_of_withCipher_2(),
	U3CDoPublishAndParseU3Ec__Iterator6_t1103676225::get_offset_of_U3CchannelU3E__0_3(),
	U3CDoPublishAndParseU3Ec__Iterator6_t1103676225::get_offset_of_asObject_4(),
	U3CDoPublishAndParseU3Ec__Iterator6_t1103676225::get_offset_of_message_5(),
	U3CDoPublishAndParseU3Ec__Iterator6_t1103676225::get_offset_of_expected_6(),
	U3CDoPublishAndParseU3Ec__Iterator6_t1103676225::get_offset_of_U24this_7(),
	U3CDoPublishAndParseU3Ec__Iterator6_t1103676225::get_offset_of_U24current_8(),
	U3CDoPublishAndParseU3Ec__Iterator6_t1103676225::get_offset_of_U24disposing_9(),
	U3CDoPublishAndParseU3Ec__Iterator6_t1103676225::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3144 = { sizeof (U3CSetAndDeleteStateAndParseU3Ec__Iterator7_t3156701289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3144[10] = 
{
	U3CSetAndDeleteStateAndParseU3Ec__Iterator7_t3156701289::get_offset_of_testName_0(),
	U3CSetAndDeleteStateAndParseU3Ec__Iterator7_t3156701289::get_offset_of_ssl_1(),
	U3CSetAndDeleteStateAndParseU3Ec__Iterator7_t3156701289::get_offset_of_U3CchannelU3E__0_2(),
	U3CSetAndDeleteStateAndParseU3Ec__Iterator7_t3156701289::get_offset_of_U3CkvpU3E__1_3(),
	U3CSetAndDeleteStateAndParseU3Ec__Iterator7_t3156701289::get_offset_of_U3CcommonStateU3E__2_4(),
	U3CSetAndDeleteStateAndParseU3Ec__Iterator7_t3156701289::get_offset_of_U3Ckvp2U3E__3_5(),
	U3CSetAndDeleteStateAndParseU3Ec__Iterator7_t3156701289::get_offset_of_U24this_6(),
	U3CSetAndDeleteStateAndParseU3Ec__Iterator7_t3156701289::get_offset_of_U24current_7(),
	U3CSetAndDeleteStateAndParseU3Ec__Iterator7_t3156701289::get_offset_of_U24disposing_8(),
	U3CSetAndDeleteStateAndParseU3Ec__Iterator7_t3156701289::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3145 = { sizeof (U3CSetAndGetStateAndParseU3Ec__Iterator8_t1119155813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3145[8] = 
{
	U3CSetAndGetStateAndParseU3Ec__Iterator8_t1119155813::get_offset_of_testName_0(),
	U3CSetAndGetStateAndParseU3Ec__Iterator8_t1119155813::get_offset_of_ssl_1(),
	U3CSetAndGetStateAndParseU3Ec__Iterator8_t1119155813::get_offset_of_U3CchannelU3E__0_2(),
	U3CSetAndGetStateAndParseU3Ec__Iterator8_t1119155813::get_offset_of_U3CstateU3E__1_3(),
	U3CSetAndGetStateAndParseU3Ec__Iterator8_t1119155813::get_offset_of_U24this_4(),
	U3CSetAndGetStateAndParseU3Ec__Iterator8_t1119155813::get_offset_of_U24current_5(),
	U3CSetAndGetStateAndParseU3Ec__Iterator8_t1119155813::get_offset_of_U24disposing_6(),
	U3CSetAndGetStateAndParseU3Ec__Iterator8_t1119155813::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3146 = { sizeof (U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3146[8] = 
{
	U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517::get_offset_of_testName_0(),
	U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517::get_offset_of_ssl_1(),
	U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517::get_offset_of_U3CchannelU3E__0_2(),
	U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517::get_offset_of_U24this_3(),
	U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517::get_offset_of_U24current_4(),
	U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517::get_offset_of_U24disposing_5(),
	U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517::get_offset_of_U24PC_6(),
	U3CSetAndGetStateAndParseUUIDU3Ec__Iterator9_t4166525517::get_offset_of_U24locvar0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3147 = { sizeof (U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_t47555547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3147[7] = 
{
	U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_t47555547::get_offset_of_state1_0(),
	U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_t47555547::get_offset_of_b1_1(),
	U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_t47555547::get_offset_of_state2_2(),
	U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_t47555547::get_offset_of_b2_3(),
	U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_t47555547::get_offset_of_b3_4(),
	U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_t47555547::get_offset_of_b4_5(),
	U3CSetAndGetStateAndParseUUIDU3Ec__AnonStorey16_t47555547::get_offset_of_U3CU3Ef__refU249_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3148 = { sizeof (U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_t2341681674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3148[8] = 
{
	U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_t2341681674::get_offset_of_testName_0(),
	U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_t2341681674::get_offset_of_ssl_1(),
	U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_t2341681674::get_offset_of_U3CchannelU3E__0_2(),
	U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_t2341681674::get_offset_of_parseAsString_3(),
	U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_t2341681674::get_offset_of_U24this_4(),
	U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_t2341681674::get_offset_of_U24current_5(),
	U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_t2341681674::get_offset_of_U24disposing_6(),
	U3CDoSubscribeThenDoGlobalHereNowAndParseU3Ec__IteratorA_t2341681674::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3149 = { sizeof (U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3149[8] = 
{
	U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279::get_offset_of_testName_0(),
	U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279::get_offset_of_ssl_1(),
	U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279::get_offset_of_U3CchannelU3E__0_2(),
	U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279::get_offset_of_parseAsString_3(),
	U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279::get_offset_of_U24this_4(),
	U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279::get_offset_of_U24current_5(),
	U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279::get_offset_of_U24disposing_6(),
	U3CDoSubscribeThenDoWhereNowAndParseU3Ec__IteratorB_t1290049279::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3150 = { sizeof (U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3150[12] = 
{
	U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886::get_offset_of_testName_0(),
	U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886::get_offset_of_ssl_1(),
	U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886::get_offset_of_U3CchannelU3E__0_2(),
	U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886::get_offset_of_U3CmatchUUIDU3E__1_3(),
	U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886::get_offset_of_customUUID_4(),
	U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886::get_offset_of_U3CstateU3E__2_5(),
	U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886::get_offset_of_doWithState_6(),
	U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886::get_offset_of_parseAsString_7(),
	U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886::get_offset_of_U24this_8(),
	U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886::get_offset_of_U24current_9(),
	U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886::get_offset_of_U24disposing_10(),
	U3CDoSubscribeThenHereNowAndParseU3Ec__IteratorC_t4098566886::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3151 = { sizeof (U3CDoSubscribeU3Ec__IteratorD_t3800429020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3151[7] = 
{
	U3CDoSubscribeU3Ec__IteratorD_t3800429020::get_offset_of_U3CcommonSubscribeU3E__0_0(),
	U3CDoSubscribeU3Ec__IteratorD_t3800429020::get_offset_of_testName_1(),
	U3CDoSubscribeU3Ec__IteratorD_t3800429020::get_offset_of_pn_2(),
	U3CDoSubscribeU3Ec__IteratorD_t3800429020::get_offset_of_channel_3(),
	U3CDoSubscribeU3Ec__IteratorD_t3800429020::get_offset_of_U24current_4(),
	U3CDoSubscribeU3Ec__IteratorD_t3800429020::get_offset_of_U24disposing_5(),
	U3CDoSubscribeU3Ec__IteratorD_t3800429020::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3152 = { sizeof (U3CDoConnectedTestU3Ec__IteratorE_t612063986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3152[9] = 
{
	U3CDoConnectedTestU3Ec__IteratorE_t612063986::get_offset_of_testName_0(),
	U3CDoConnectedTestU3Ec__IteratorE_t612063986::get_offset_of_ssl_1(),
	U3CDoConnectedTestU3Ec__IteratorE_t612063986::get_offset_of_U3CchannelU3E__0_2(),
	U3CDoConnectedTestU3Ec__IteratorE_t612063986::get_offset_of_asObject_3(),
	U3CDoConnectedTestU3Ec__IteratorE_t612063986::get_offset_of_isPresence_4(),
	U3CDoConnectedTestU3Ec__IteratorE_t612063986::get_offset_of_U24this_5(),
	U3CDoConnectedTestU3Ec__IteratorE_t612063986::get_offset_of_U24current_6(),
	U3CDoConnectedTestU3Ec__IteratorE_t612063986::get_offset_of_U24disposing_7(),
	U3CDoConnectedTestU3Ec__IteratorE_t612063986::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3153 = { sizeof (U3CDoNotSubscribedTestU3Ec__IteratorF_t3639947701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3153[9] = 
{
	U3CDoNotSubscribedTestU3Ec__IteratorF_t3639947701::get_offset_of_testName_0(),
	U3CDoNotSubscribedTestU3Ec__IteratorF_t3639947701::get_offset_of_ssl_1(),
	U3CDoNotSubscribedTestU3Ec__IteratorF_t3639947701::get_offset_of_U3CchannelU3E__0_2(),
	U3CDoNotSubscribedTestU3Ec__IteratorF_t3639947701::get_offset_of_asObject_3(),
	U3CDoNotSubscribedTestU3Ec__IteratorF_t3639947701::get_offset_of_isPresence_4(),
	U3CDoNotSubscribedTestU3Ec__IteratorF_t3639947701::get_offset_of_U24this_5(),
	U3CDoNotSubscribedTestU3Ec__IteratorF_t3639947701::get_offset_of_U24current_6(),
	U3CDoNotSubscribedTestU3Ec__IteratorF_t3639947701::get_offset_of_U24disposing_7(),
	U3CDoNotSubscribedTestU3Ec__IteratorF_t3639947701::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3154 = { sizeof (U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3154[9] = 
{
	U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012::get_offset_of_testName_0(),
	U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012::get_offset_of_ssl_1(),
	U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012::get_offset_of_U3CchannelU3E__0_2(),
	U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012::get_offset_of_asObject_3(),
	U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012::get_offset_of_isPresence_4(),
	U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012::get_offset_of_U24this_5(),
	U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012::get_offset_of_U24current_6(),
	U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012::get_offset_of_U24disposing_7(),
	U3CDoUnsubscribeTestU3Ec__Iterator10_t4018124012::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3155 = { sizeof (U3CDoAlreadySubscribeTestU3Ec__Iterator11_t1364000944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3155[9] = 
{
	U3CDoAlreadySubscribeTestU3Ec__Iterator11_t1364000944::get_offset_of_testName_0(),
	U3CDoAlreadySubscribeTestU3Ec__Iterator11_t1364000944::get_offset_of_ssl_1(),
	U3CDoAlreadySubscribeTestU3Ec__Iterator11_t1364000944::get_offset_of_U3CchannelU3E__0_2(),
	U3CDoAlreadySubscribeTestU3Ec__Iterator11_t1364000944::get_offset_of_asObject_3(),
	U3CDoAlreadySubscribeTestU3Ec__Iterator11_t1364000944::get_offset_of_isPresence_4(),
	U3CDoAlreadySubscribeTestU3Ec__Iterator11_t1364000944::get_offset_of_U24this_5(),
	U3CDoAlreadySubscribeTestU3Ec__Iterator11_t1364000944::get_offset_of_U24current_6(),
	U3CDoAlreadySubscribeTestU3Ec__Iterator11_t1364000944::get_offset_of_U24disposing_7(),
	U3CDoAlreadySubscribeTestU3Ec__Iterator11_t1364000944::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3156 = { sizeof (U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_t4086914571), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3156[8] = 
{
	U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_t4086914571::get_offset_of_testName_0(),
	U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_t4086914571::get_offset_of_ssl_1(),
	U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_t4086914571::get_offset_of_U3CchannelU3E__0_2(),
	U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_t4086914571::get_offset_of_asObject_3(),
	U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_t4086914571::get_offset_of_U24this_4(),
	U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_t4086914571::get_offset_of_U24current_5(),
	U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_t4086914571::get_offset_of_U24disposing_6(),
	U3CDoPresenceThenSubscribeAndParseU3Ec__Iterator12_t4086914571::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3157 = { sizeof (U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3157[16] = 
{
	U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764::get_offset_of_url_0(),
	U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764::get_offset_of_U3CwwwU3E__0_1(),
	U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764::get_offset_of_expectedMessage_2(),
	U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764::get_offset_of_channels_3(),
	U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764::get_offset_of_isError_4(),
	U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764::get_offset_of_isTimeout_5(),
	U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764::get_offset_of_crt_6(),
	U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764::get_offset_of_respType_7(),
	U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764::get_offset_of_asObject_8(),
	U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764::get_offset_of_timeout_9(),
	U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764::get_offset_of_pause_10(),
	U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764::get_offset_of_timetoken_11(),
	U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764::get_offset_of_U24this_12(),
	U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764::get_offset_of_U24current_13(),
	U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764::get_offset_of_U24disposing_14(),
	U3CTestCoroutineRunProcessResponseU3Ec__Iterator13_t2438945764::get_offset_of_U24PC_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3158 = { sizeof (U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3158[16] = 
{
	U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483::get_offset_of_url_0(),
	U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483::get_offset_of_U3CwwwU3E__0_1(),
	U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483::get_offset_of_expectedMessage_2(),
	U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483::get_offset_of_channels_3(),
	U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483::get_offset_of_isError_4(),
	U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483::get_offset_of_isTimeout_5(),
	U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483::get_offset_of_crt_6(),
	U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483::get_offset_of_respType_7(),
	U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483::get_offset_of_asObject_8(),
	U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483::get_offset_of_timeout_9(),
	U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483::get_offset_of_pause_10(),
	U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483::get_offset_of_timetoken_11(),
	U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483::get_offset_of_U24this_12(),
	U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483::get_offset_of_U24current_13(),
	U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483::get_offset_of_U24disposing_14(),
	U3CTestCoroutineRunErrorU3Ec__Iterator14_t574709483::get_offset_of_U24PC_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3159 = { sizeof (TestAbortSubscribe_t2694853646), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3160 = { sizeof (U3CStartU3Ec__Iterator0_t1406312695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3160[15] = 
{
	U3CStartU3Ec__Iterator0_t1406312695::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1406312695::get_offset_of_U3CrU3E__1_1(),
	U3CStartU3Ec__Iterator0_t1406312695::get_offset_of_U3CchannelU3E__2_2(),
	U3CStartU3Ec__Iterator0_t1406312695::get_offset_of_U3CmultiChannelU3E__3_3(),
	U3CStartU3Ec__Iterator0_t1406312695::get_offset_of_U3CpubnubU3E__4_4(),
	U3CStartU3Ec__Iterator0_t1406312695::get_offset_of_U3CcrtU3E__5_5(),
	U3CStartU3Ec__Iterator0_t1406312695::get_offset_of_U3CexpectedMessageU3E__6_6(),
	U3CStartU3Ec__Iterator0_t1406312695::get_offset_of_U3CexpectedChannelsU3E__7_7(),
	U3CStartU3Ec__Iterator0_t1406312695::get_offset_of_U3CnanoSecondTimeU3E__8_8(),
	U3CStartU3Ec__Iterator0_t1406312695::get_offset_of_U3CurlU3E__9_9(),
	U3CStartU3Ec__Iterator0_t1406312695::get_offset_of_U3CrespTypeU3E__A_10(),
	U3CStartU3Ec__Iterator0_t1406312695::get_offset_of_U24this_11(),
	U3CStartU3Ec__Iterator0_t1406312695::get_offset_of_U24current_12(),
	U3CStartU3Ec__Iterator0_t1406312695::get_offset_of_U24disposing_13(),
	U3CStartU3Ec__Iterator0_t1406312695::get_offset_of_U24PC_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3161 = { sizeof (TestAlreadySubscribed_t3135694410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3161[3] = 
{
	TestAlreadySubscribed_t3135694410::get_offset_of_SslOn_2(),
	TestAlreadySubscribed_t3135694410::get_offset_of_AsObject_3(),
	TestAlreadySubscribed_t3135694410::get_offset_of_IsPresence_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3162 = { sizeof (U3CStartU3Ec__Iterator0_t1936135251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3162[5] = 
{
	U3CStartU3Ec__Iterator0_t1936135251::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1936135251::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t1936135251::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t1936135251::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t1936135251::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3163 = { sizeof (TestBounceHB_t3845500086), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3164 = { sizeof (U3CStartU3Ec__Iterator0_t1345070543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3164[15] = 
{
	U3CStartU3Ec__Iterator0_t1345070543::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1345070543::get_offset_of_U3CrU3E__1_1(),
	U3CStartU3Ec__Iterator0_t1345070543::get_offset_of_U3CchannelU3E__2_2(),
	U3CStartU3Ec__Iterator0_t1345070543::get_offset_of_U3CmultiChannelU3E__3_3(),
	U3CStartU3Ec__Iterator0_t1345070543::get_offset_of_U3CpubnubU3E__4_4(),
	U3CStartU3Ec__Iterator0_t1345070543::get_offset_of_U3CcrtU3E__5_5(),
	U3CStartU3Ec__Iterator0_t1345070543::get_offset_of_U3CexpectedMessageU3E__6_6(),
	U3CStartU3Ec__Iterator0_t1345070543::get_offset_of_U3CexpectedChannelsU3E__7_7(),
	U3CStartU3Ec__Iterator0_t1345070543::get_offset_of_U3CnanoSecondTimeU3E__8_8(),
	U3CStartU3Ec__Iterator0_t1345070543::get_offset_of_U3CurlU3E__9_9(),
	U3CStartU3Ec__Iterator0_t1345070543::get_offset_of_U3CrespTypeU3E__A_10(),
	U3CStartU3Ec__Iterator0_t1345070543::get_offset_of_U24this_11(),
	U3CStartU3Ec__Iterator0_t1345070543::get_offset_of_U24current_12(),
	U3CStartU3Ec__Iterator0_t1345070543::get_offset_of_U24disposing_13(),
	U3CStartU3Ec__Iterator0_t1345070543::get_offset_of_U24PC_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3165 = { sizeof (TestBounceNonSub_t4134311263), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3166 = { sizeof (U3CStartU3Ec__Iterator0_t3197359886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3166[15] = 
{
	U3CStartU3Ec__Iterator0_t3197359886::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3197359886::get_offset_of_U3CrU3E__1_1(),
	U3CStartU3Ec__Iterator0_t3197359886::get_offset_of_U3CchannelU3E__2_2(),
	U3CStartU3Ec__Iterator0_t3197359886::get_offset_of_U3CmultiChannelU3E__3_3(),
	U3CStartU3Ec__Iterator0_t3197359886::get_offset_of_U3CpubnubU3E__4_4(),
	U3CStartU3Ec__Iterator0_t3197359886::get_offset_of_U3CcrtU3E__5_5(),
	U3CStartU3Ec__Iterator0_t3197359886::get_offset_of_U3CexpectedMessageU3E__6_6(),
	U3CStartU3Ec__Iterator0_t3197359886::get_offset_of_U3CexpectedChannelsU3E__7_7(),
	U3CStartU3Ec__Iterator0_t3197359886::get_offset_of_U3CnanoSecondTimeU3E__8_8(),
	U3CStartU3Ec__Iterator0_t3197359886::get_offset_of_U3CurlU3E__9_9(),
	U3CStartU3Ec__Iterator0_t3197359886::get_offset_of_U3CrespTypeU3E__A_10(),
	U3CStartU3Ec__Iterator0_t3197359886::get_offset_of_U24this_11(),
	U3CStartU3Ec__Iterator0_t3197359886::get_offset_of_U24current_12(),
	U3CStartU3Ec__Iterator0_t3197359886::get_offset_of_U24disposing_13(),
	U3CStartU3Ec__Iterator0_t3197359886::get_offset_of_U24PC_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3167 = { sizeof (TestBouncePHB_t1324311378), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3168 = { sizeof (U3CStartU3Ec__Iterator0_t2645351515), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3168[15] = 
{
	U3CStartU3Ec__Iterator0_t2645351515::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t2645351515::get_offset_of_U3CrU3E__1_1(),
	U3CStartU3Ec__Iterator0_t2645351515::get_offset_of_U3CchannelU3E__2_2(),
	U3CStartU3Ec__Iterator0_t2645351515::get_offset_of_U3CmultiChannelU3E__3_3(),
	U3CStartU3Ec__Iterator0_t2645351515::get_offset_of_U3CpubnubU3E__4_4(),
	U3CStartU3Ec__Iterator0_t2645351515::get_offset_of_U3CcrtU3E__5_5(),
	U3CStartU3Ec__Iterator0_t2645351515::get_offset_of_U3CexpectedMessageU3E__6_6(),
	U3CStartU3Ec__Iterator0_t2645351515::get_offset_of_U3CexpectedChannelsU3E__7_7(),
	U3CStartU3Ec__Iterator0_t2645351515::get_offset_of_U3CnanoSecondTimeU3E__8_8(),
	U3CStartU3Ec__Iterator0_t2645351515::get_offset_of_U3CurlU3E__9_9(),
	U3CStartU3Ec__Iterator0_t2645351515::get_offset_of_U3CrespTypeU3E__A_10(),
	U3CStartU3Ec__Iterator0_t2645351515::get_offset_of_U24this_11(),
	U3CStartU3Ec__Iterator0_t2645351515::get_offset_of_U24current_12(),
	U3CStartU3Ec__Iterator0_t2645351515::get_offset_of_U24disposing_13(),
	U3CStartU3Ec__Iterator0_t2645351515::get_offset_of_U24PC_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3169 = { sizeof (TestBounceSubscribe_t3713701440), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3170 = { sizeof (U3CStartU3Ec__Iterator0_t3456075201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3170[15] = 
{
	U3CStartU3Ec__Iterator0_t3456075201::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3456075201::get_offset_of_U3CrU3E__1_1(),
	U3CStartU3Ec__Iterator0_t3456075201::get_offset_of_U3CchannelU3E__2_2(),
	U3CStartU3Ec__Iterator0_t3456075201::get_offset_of_U3CmultiChannelU3E__3_3(),
	U3CStartU3Ec__Iterator0_t3456075201::get_offset_of_U3CpubnubU3E__4_4(),
	U3CStartU3Ec__Iterator0_t3456075201::get_offset_of_U3CcrtU3E__5_5(),
	U3CStartU3Ec__Iterator0_t3456075201::get_offset_of_U3CexpectedMessageU3E__6_6(),
	U3CStartU3Ec__Iterator0_t3456075201::get_offset_of_U3CexpectedChannelsU3E__7_7(),
	U3CStartU3Ec__Iterator0_t3456075201::get_offset_of_U3CnanoSecondTimeU3E__8_8(),
	U3CStartU3Ec__Iterator0_t3456075201::get_offset_of_U3CurlU3E__9_9(),
	U3CStartU3Ec__Iterator0_t3456075201::get_offset_of_U3CrespTypeU3E__A_10(),
	U3CStartU3Ec__Iterator0_t3456075201::get_offset_of_U24this_11(),
	U3CStartU3Ec__Iterator0_t3456075201::get_offset_of_U24current_12(),
	U3CStartU3Ec__Iterator0_t3456075201::get_offset_of_U24disposing_13(),
	U3CStartU3Ec__Iterator0_t3456075201::get_offset_of_U24PC_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3171 = { sizeof (TestCGAddListRemoveSubscribeStateHereNowUnsub_t2353600463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3171[4] = 
{
	TestCGAddListRemoveSubscribeStateHereNowUnsub_t2353600463::get_offset_of_SslOn_2(),
	TestCGAddListRemoveSubscribeStateHereNowUnsub_t2353600463::get_offset_of_CipherOn_3(),
	TestCGAddListRemoveSubscribeStateHereNowUnsub_t2353600463::get_offset_of_AsObject_4(),
	TestCGAddListRemoveSubscribeStateHereNowUnsub_t2353600463::get_offset_of_BothString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3172 = { sizeof (U3CStartU3Ec__Iterator0_t4195216218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3172[9] = 
{
	U3CStartU3Ec__Iterator0_t4195216218::get_offset_of_U3CMessage1U3E__0_0(),
	U3CStartU3Ec__Iterator0_t4195216218::get_offset_of_U3CMessage2U3E__1_1(),
	U3CStartU3Ec__Iterator0_t4195216218::get_offset_of_U3CMessageU3E__2_2(),
	U3CStartU3Ec__Iterator0_t4195216218::get_offset_of_U3CexpectedMessageU3E__3_3(),
	U3CStartU3Ec__Iterator0_t4195216218::get_offset_of_U3CcommonU3E__4_4(),
	U3CStartU3Ec__Iterator0_t4195216218::get_offset_of_U24this_5(),
	U3CStartU3Ec__Iterator0_t4195216218::get_offset_of_U24current_6(),
	U3CStartU3Ec__Iterator0_t4195216218::get_offset_of_U24disposing_7(),
	U3CStartU3Ec__Iterator0_t4195216218::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3173 = { sizeof (TestCGCHAddListRemoveSubscribeStateHereNowUnsub_t3525210006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3173[5] = 
{
	TestCGCHAddListRemoveSubscribeStateHereNowUnsub_t3525210006::get_offset_of_SslOn_2(),
	TestCGCHAddListRemoveSubscribeStateHereNowUnsub_t3525210006::get_offset_of_CipherOn_3(),
	TestCGCHAddListRemoveSubscribeStateHereNowUnsub_t3525210006::get_offset_of_AsObject_4(),
	TestCGCHAddListRemoveSubscribeStateHereNowUnsub_t3525210006::get_offset_of_BothString_5(),
	TestCGCHAddListRemoveSubscribeStateHereNowUnsub_t3525210006::get_offset_of_pubnub_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3174 = { sizeof (U3CStartU3Ec__Iterator0_t3787879071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3174[8] = 
{
	U3CStartU3Ec__Iterator0_t3787879071::get_offset_of_U3CMessage1U3E__0_0(),
	U3CStartU3Ec__Iterator0_t3787879071::get_offset_of_U3CMessage2U3E__1_1(),
	U3CStartU3Ec__Iterator0_t3787879071::get_offset_of_U3CMessageU3E__2_2(),
	U3CStartU3Ec__Iterator0_t3787879071::get_offset_of_U3CexpectedMessageU3E__3_3(),
	U3CStartU3Ec__Iterator0_t3787879071::get_offset_of_U24this_4(),
	U3CStartU3Ec__Iterator0_t3787879071::get_offset_of_U24current_5(),
	U3CStartU3Ec__Iterator0_t3787879071::get_offset_of_U24disposing_6(),
	U3CStartU3Ec__Iterator0_t3787879071::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3175 = { sizeof (U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3175[16] = 
{
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171::get_offset_of_U3CrU3E__0_0(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171::get_offset_of_testName_1(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171::get_offset_of_U3CbGetAllCGU3E__6_2(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171::get_offset_of_U3CstrLogU3E__8_3(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171::get_offset_of_U3CbSubPublishedU3E__C_4(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171::get_offset_of_U3CbGetStateU3E__F_5(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171::get_offset_of_U3CbSetUserState2U3E__13_6(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171::get_offset_of_U3CbGetUserState2U3E__14_7(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171::get_offset_of_U3CbRemoveChU3E__16_8(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171::get_offset_of_U3CbGetChannel2U3E__17_9(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171::get_offset_of_U3CstrLog2U3E__18_10(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171::get_offset_of_U24this_11(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171::get_offset_of_U24current_12(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171::get_offset_of_U24disposing_13(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171::get_offset_of_U24PC_14(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__Iterator1_t3130264171::get_offset_of_U24locvar0_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3176 = { sizeof (U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3176[17] = 
{
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560::get_offset_of_testName_0(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560::get_offset_of_bAddChannel_1(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560::get_offset_of_cg_2(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560::get_offset_of_channel_3(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560::get_offset_of_bGetChannel_4(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560::get_offset_of_pubMessage_5(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560::get_offset_of_bSubMessage_6(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560::get_offset_of_ch_7(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560::get_offset_of_bSubMessage2_8(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560::get_offset_of_bSubConnected_9(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560::get_offset_of_state_10(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560::get_offset_of_bSetState_11(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560::get_offset_of_uuid_12(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560::get_offset_of_bHereNow_13(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560::get_offset_of_bHereNowState_14(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560::get_offset_of_bUnsub_15(),
	U3CDoCGCHAddListRemoveSubscribeStateHereNowUnsubU3Ec__AnonStorey2_t3975013560::get_offset_of_U3CU3Ef__refU241_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3177 = { sizeof (TestConnectedMessage_t4278862304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3177[3] = 
{
	TestConnectedMessage_t4278862304::get_offset_of_SslOn_2(),
	TestConnectedMessage_t4278862304::get_offset_of_AsObject_3(),
	TestConnectedMessage_t4278862304::get_offset_of_IsPresence_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3178 = { sizeof (U3CStartU3Ec__Iterator0_t2884833369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3178[5] = 
{
	U3CStartU3Ec__Iterator0_t2884833369::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t2884833369::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2884833369::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2884833369::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2884833369::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3179 = { sizeof (TestCoroutineRunIntegerationNonSubError_t911951813), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3180 = { sizeof (U3CStartU3Ec__Iterator0_t3468753608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3180[12] = 
{
	U3CStartU3Ec__Iterator0_t3468753608::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3468753608::get_offset_of_U3CurlU3E__1_1(),
	U3CStartU3Ec__Iterator0_t3468753608::get_offset_of_U3CmultiChannelU3E__2_2(),
	U3CStartU3Ec__Iterator0_t3468753608::get_offset_of_U3CcrtU3E__3_3(),
	U3CStartU3Ec__Iterator0_t3468753608::get_offset_of_U3CexpectedMessageU3E__4_4(),
	U3CStartU3Ec__Iterator0_t3468753608::get_offset_of_U3CexpectedChannelsU3E__5_5(),
	U3CStartU3Ec__Iterator0_t3468753608::get_offset_of_U3CrespTypeU3E__6_6(),
	U3CStartU3Ec__Iterator0_t3468753608::get_offset_of_U3CienumU3E__7_7(),
	U3CStartU3Ec__Iterator0_t3468753608::get_offset_of_U24this_8(),
	U3CStartU3Ec__Iterator0_t3468753608::get_offset_of_U24current_9(),
	U3CStartU3Ec__Iterator0_t3468753608::get_offset_of_U24disposing_10(),
	U3CStartU3Ec__Iterator0_t3468753608::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3181 = { sizeof (TestCoroutineRunIntegerationPHBError_t1479832066), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3182 = { sizeof (U3CStartU3Ec__Iterator0_t3466280779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3182[12] = 
{
	U3CStartU3Ec__Iterator0_t3466280779::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3466280779::get_offset_of_U3CurlU3E__1_1(),
	U3CStartU3Ec__Iterator0_t3466280779::get_offset_of_U3CmultiChannelU3E__2_2(),
	U3CStartU3Ec__Iterator0_t3466280779::get_offset_of_U3CcrtU3E__3_3(),
	U3CStartU3Ec__Iterator0_t3466280779::get_offset_of_U3CexpectedMessageU3E__4_4(),
	U3CStartU3Ec__Iterator0_t3466280779::get_offset_of_U3CexpectedChannelsU3E__5_5(),
	U3CStartU3Ec__Iterator0_t3466280779::get_offset_of_U3CrespTypeU3E__6_6(),
	U3CStartU3Ec__Iterator0_t3466280779::get_offset_of_U3CienumU3E__7_7(),
	U3CStartU3Ec__Iterator0_t3466280779::get_offset_of_U24this_8(),
	U3CStartU3Ec__Iterator0_t3466280779::get_offset_of_U24current_9(),
	U3CStartU3Ec__Iterator0_t3466280779::get_offset_of_U24disposing_10(),
	U3CStartU3Ec__Iterator0_t3466280779::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3183 = { sizeof (TestCoroutineRunIntegrationHB_t277379567), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3184 = { sizeof (U3CStartU3Ec__Iterator0_t1932954230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3184[12] = 
{
	U3CStartU3Ec__Iterator0_t1932954230::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1932954230::get_offset_of_U3CurlU3E__1_1(),
	U3CStartU3Ec__Iterator0_t1932954230::get_offset_of_U3CmultiChannelU3E__2_2(),
	U3CStartU3Ec__Iterator0_t1932954230::get_offset_of_U3CcrtU3E__3_3(),
	U3CStartU3Ec__Iterator0_t1932954230::get_offset_of_U3CexpectedMessageU3E__4_4(),
	U3CStartU3Ec__Iterator0_t1932954230::get_offset_of_U3CexpectedChannelsU3E__5_5(),
	U3CStartU3Ec__Iterator0_t1932954230::get_offset_of_U3CrespTypeU3E__6_6(),
	U3CStartU3Ec__Iterator0_t1932954230::get_offset_of_U3CienumU3E__7_7(),
	U3CStartU3Ec__Iterator0_t1932954230::get_offset_of_U24this_8(),
	U3CStartU3Ec__Iterator0_t1932954230::get_offset_of_U24current_9(),
	U3CStartU3Ec__Iterator0_t1932954230::get_offset_of_U24disposing_10(),
	U3CStartU3Ec__Iterator0_t1932954230::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3185 = { sizeof (TestCoroutineRunIntegrationHBError_t1442572721), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3186 = { sizeof (U3CStartU3Ec__Iterator0_t2012502852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3186[12] = 
{
	U3CStartU3Ec__Iterator0_t2012502852::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t2012502852::get_offset_of_U3CurlU3E__1_1(),
	U3CStartU3Ec__Iterator0_t2012502852::get_offset_of_U3CmultiChannelU3E__2_2(),
	U3CStartU3Ec__Iterator0_t2012502852::get_offset_of_U3CcrtU3E__3_3(),
	U3CStartU3Ec__Iterator0_t2012502852::get_offset_of_U3CexpectedMessageU3E__4_4(),
	U3CStartU3Ec__Iterator0_t2012502852::get_offset_of_U3CexpectedChannelsU3E__5_5(),
	U3CStartU3Ec__Iterator0_t2012502852::get_offset_of_U3CrespTypeU3E__6_6(),
	U3CStartU3Ec__Iterator0_t2012502852::get_offset_of_U3CienumU3E__7_7(),
	U3CStartU3Ec__Iterator0_t2012502852::get_offset_of_U24this_8(),
	U3CStartU3Ec__Iterator0_t2012502852::get_offset_of_U24current_9(),
	U3CStartU3Ec__Iterator0_t2012502852::get_offset_of_U24disposing_10(),
	U3CStartU3Ec__Iterator0_t2012502852::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3187 = { sizeof (TestCoroutineRunIntegrationPHB_t2070611827), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3188 = { sizeof (U3CStartU3Ec__Iterator0_t799381618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3188[13] = 
{
	U3CStartU3Ec__Iterator0_t799381618::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t799381618::get_offset_of_U3CmultiChannelU3E__1_1(),
	U3CStartU3Ec__Iterator0_t799381618::get_offset_of_U3CpubnubU3E__2_2(),
	U3CStartU3Ec__Iterator0_t799381618::get_offset_of_U3CcrtU3E__3_3(),
	U3CStartU3Ec__Iterator0_t799381618::get_offset_of_U3CexpectedMessageU3E__4_4(),
	U3CStartU3Ec__Iterator0_t799381618::get_offset_of_U3CexpectedChannelsU3E__5_5(),
	U3CStartU3Ec__Iterator0_t799381618::get_offset_of_U3CurlU3E__6_6(),
	U3CStartU3Ec__Iterator0_t799381618::get_offset_of_U3CrespTypeU3E__7_7(),
	U3CStartU3Ec__Iterator0_t799381618::get_offset_of_U3CienumU3E__8_8(),
	U3CStartU3Ec__Iterator0_t799381618::get_offset_of_U24this_9(),
	U3CStartU3Ec__Iterator0_t799381618::get_offset_of_U24current_10(),
	U3CStartU3Ec__Iterator0_t799381618::get_offset_of_U24disposing_11(),
	U3CStartU3Ec__Iterator0_t799381618::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3189 = { sizeof (TestCoroutineRunIntegrationSub_t786852867), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3190 = { sizeof (U3CStartU3Ec__Iterator0_t3430217958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3190[14] = 
{
	U3CStartU3Ec__Iterator0_t3430217958::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3430217958::get_offset_of_U3CmultiChannelU3E__1_1(),
	U3CStartU3Ec__Iterator0_t3430217958::get_offset_of_U3CcrtU3E__2_2(),
	U3CStartU3Ec__Iterator0_t3430217958::get_offset_of_U3CexpectedMessageU3E__3_3(),
	U3CStartU3Ec__Iterator0_t3430217958::get_offset_of_U3CexpectedChannelsU3E__4_4(),
	U3CStartU3Ec__Iterator0_t3430217958::get_offset_of_U3CrespTypeU3E__5_5(),
	U3CStartU3Ec__Iterator0_t3430217958::get_offset_of_U3CpubnubU3E__6_6(),
	U3CStartU3Ec__Iterator0_t3430217958::get_offset_of_U3CnanoSecondTimeU3E__7_7(),
	U3CStartU3Ec__Iterator0_t3430217958::get_offset_of_U3CurlU3E__8_8(),
	U3CStartU3Ec__Iterator0_t3430217958::get_offset_of_U3CienumU3E__9_9(),
	U3CStartU3Ec__Iterator0_t3430217958::get_offset_of_U24this_10(),
	U3CStartU3Ec__Iterator0_t3430217958::get_offset_of_U24current_11(),
	U3CStartU3Ec__Iterator0_t3430217958::get_offset_of_U24disposing_12(),
	U3CStartU3Ec__Iterator0_t3430217958::get_offset_of_U24PC_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3191 = { sizeof (TestCoroutineRunIntegrationSubError_t3597817705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3192 = { sizeof (U3CStartU3Ec__Iterator0_t602355608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3192[12] = 
{
	U3CStartU3Ec__Iterator0_t602355608::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t602355608::get_offset_of_U3CurlU3E__1_1(),
	U3CStartU3Ec__Iterator0_t602355608::get_offset_of_U3CmultiChannelU3E__2_2(),
	U3CStartU3Ec__Iterator0_t602355608::get_offset_of_U3CcrtU3E__3_3(),
	U3CStartU3Ec__Iterator0_t602355608::get_offset_of_U3CexpectedMessageU3E__4_4(),
	U3CStartU3Ec__Iterator0_t602355608::get_offset_of_U3CexpectedChannelsU3E__5_5(),
	U3CStartU3Ec__Iterator0_t602355608::get_offset_of_U3CrespTypeU3E__6_6(),
	U3CStartU3Ec__Iterator0_t602355608::get_offset_of_U3CienumU3E__7_7(),
	U3CStartU3Ec__Iterator0_t602355608::get_offset_of_U24this_8(),
	U3CStartU3Ec__Iterator0_t602355608::get_offset_of_U24current_9(),
	U3CStartU3Ec__Iterator0_t602355608::get_offset_of_U24disposing_10(),
	U3CStartU3Ec__Iterator0_t602355608::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3193 = { sizeof (TestCoroutineRunIntegrationSubErrorTimeout_t3898362486), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3194 = { sizeof (U3CStartU3Ec__Iterator0_t103610343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3194[3] = 
{
	U3CStartU3Ec__Iterator0_t103610343::get_offset_of_U24current_0(),
	U3CStartU3Ec__Iterator0_t103610343::get_offset_of_U24disposing_1(),
	U3CStartU3Ec__Iterator0_t103610343::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3195 = { sizeof (TestCoroutineRunIntegrationTests_t3041896496), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3196 = { sizeof (U3CStartU3Ec__Iterator0_t2900771665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3196[12] = 
{
	U3CStartU3Ec__Iterator0_t2900771665::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t2900771665::get_offset_of_U3CurlU3E__1_1(),
	U3CStartU3Ec__Iterator0_t2900771665::get_offset_of_U3CmultiChannelU3E__2_2(),
	U3CStartU3Ec__Iterator0_t2900771665::get_offset_of_U3CcrtU3E__3_3(),
	U3CStartU3Ec__Iterator0_t2900771665::get_offset_of_U3CexpectedMessageU3E__4_4(),
	U3CStartU3Ec__Iterator0_t2900771665::get_offset_of_U3CexpectedChannelsU3E__5_5(),
	U3CStartU3Ec__Iterator0_t2900771665::get_offset_of_U3CrespTypeU3E__6_6(),
	U3CStartU3Ec__Iterator0_t2900771665::get_offset_of_U3CienumU3E__7_7(),
	U3CStartU3Ec__Iterator0_t2900771665::get_offset_of_U24this_8(),
	U3CStartU3Ec__Iterator0_t2900771665::get_offset_of_U24current_9(),
	U3CStartU3Ec__Iterator0_t2900771665::get_offset_of_U24disposing_10(),
	U3CStartU3Ec__Iterator0_t2900771665::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3197 = { sizeof (TestCoroutineRunSubscribeAbort_t3459747169), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3198 = { sizeof (U3CStartU3Ec__Iterator0_t1382381424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3198[4] = 
{
	U3CStartU3Ec__Iterator0_t1382381424::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t1382381424::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t1382381424::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t1382381424::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3199 = { sizeof (TestCoroutineRunSubscribeMultiple_t1944684459), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
