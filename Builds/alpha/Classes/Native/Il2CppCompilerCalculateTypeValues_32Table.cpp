﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestCorout3216771218.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestDelUse3399981929.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestDelUser147422324.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestDelUse1475431263.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestDelUse1373182319.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestDetail3933854162.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestDetail3853365259.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestDetaile206760270.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestDetail4124774743.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestGlobal1271793713.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestGlobal2035977652.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestHBTime1340712739.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestHBTime1504804398.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestHereNow236862272.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestHereNo1125967569.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestNonSub4015492332.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestNonSub1346351357.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestNotSub4150449091.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestNotSubs381706958.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestNullAsE398058888.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestNullAs3908806913.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPHBTime475805337.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPHBTim3001609288.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPresen3679081271.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPresen3034679634.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPresen2161520313.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPresenc855549685.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPresence57511345.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPresen1507632776.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestProces1733042336.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestProces3504551233.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPublis3915767209.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPublis1117630120.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPublis2825332490.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPublis4164458955.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPublis2456351671.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPublis3478324138.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPublis3500303089.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPublishN39973772.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPublis3340936991.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPublishN56554715.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPublis2553593815.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPublis3354114646.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPublis1537834550.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPublis1472748615.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPublis2348182496.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPublis2022132156.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPublish257393974.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPublish498973711.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPublis2226981140.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestPublis3776434096.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestRemoveC911744142.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestRemoveC438671695.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestRemove3240521343.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestRemoveC816457458.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSecret4164925165.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSecretK363530616.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSetAnd2908412544.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSetAnd3772056065.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSetAnd3454205617.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSetAnd2502183628.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSetAndG667934322.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSetAnd3258107483.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr1078578741.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscri196500632.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr3825317694.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr2394687319.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr3441415060.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr2179167133.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr2305244543.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscri639752250.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr2788072960.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr2348082985.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscri256670965.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscri870106880.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr3684075257.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr2686287252.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscri499442242.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscri998415939.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscri135406318.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr3090456015.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscri256365834.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr1035946611.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr1054294135.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscri751811486.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscri425182959.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr2593074586.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscri460277727.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr2364701085.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr3284876308.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr2380756133.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr1797587694.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr1093487688.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr3268774053.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr2756860596.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr1290748520.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr1257108233.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr2535103227.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr2176127986.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr1329469586.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3200 = { sizeof (U3CStartU3Ec__Iterator0_t3216771218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3200[4] = 
{
	U3CStartU3Ec__Iterator0_t3216771218::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t3216771218::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t3216771218::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t3216771218::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3201 = { sizeof (TestDelUserStateCG_t3399981929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3201[5] = 
{
	TestDelUserStateCG_t3399981929::get_offset_of_SslOn_2(),
	TestDelUserStateCG_t3399981929::get_offset_of_CipherOn_3(),
	TestDelUserStateCG_t3399981929::get_offset_of_AsObject_4(),
	TestDelUserStateCG_t3399981929::get_offset_of_BothString_5(),
	TestDelUserStateCG_t3399981929::get_offset_of_pubnub_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3202 = { sizeof (U3CStartU3Ec__Iterator0_t147422324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3202[8] = 
{
	U3CStartU3Ec__Iterator0_t147422324::get_offset_of_U3CMessage1U3E__0_0(),
	U3CStartU3Ec__Iterator0_t147422324::get_offset_of_U3CMessage2U3E__1_1(),
	U3CStartU3Ec__Iterator0_t147422324::get_offset_of_U3CMessageU3E__2_2(),
	U3CStartU3Ec__Iterator0_t147422324::get_offset_of_U3CexpectedMessageU3E__3_3(),
	U3CStartU3Ec__Iterator0_t147422324::get_offset_of_U24this_4(),
	U3CStartU3Ec__Iterator0_t147422324::get_offset_of_U24current_5(),
	U3CStartU3Ec__Iterator0_t147422324::get_offset_of_U24disposing_6(),
	U3CStartU3Ec__Iterator0_t147422324::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3203 = { sizeof (U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3203[11] = 
{
	U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263::get_offset_of_U3CrU3E__0_0(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263::get_offset_of_testName_1(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263::get_offset_of_U3CuuidU3E__5_2(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263::get_offset_of_U3CstrLogU3E__6_3(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263::get_offset_of_U3CbGetStateU3E__9_4(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263::get_offset_of_U3CstrLog2U3E__E_5(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263::get_offset_of_U24this_6(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263::get_offset_of_U24current_7(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263::get_offset_of_U24disposing_8(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263::get_offset_of_U24PC_9(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__Iterator1_t1475431263::get_offset_of_U24locvar0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3204 = { sizeof (U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3204[12] = 
{
	U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319::get_offset_of_testName_0(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319::get_offset_of_bAddChannel_1(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319::get_offset_of_bSubConnected_2(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319::get_offset_of_ch_3(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319::get_offset_of_cg_4(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319::get_offset_of_state_5(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319::get_offset_of_bSetState_6(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319::get_offset_of_channel_7(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319::get_offset_of_bSetUserState2_8(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319::get_offset_of_bGetUserState2_9(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319::get_offset_of_bRemoveAll_10(),
	U3CDoSubscribeSetStateDelStateCGU3Ec__AnonStorey2_t1373182319::get_offset_of_U3CU3Ef__refU241_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3205 = { sizeof (TestDetailedHistory_t3933854162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3205[4] = 
{
	TestDetailedHistory_t3933854162::get_offset_of_SslOn_2(),
	TestDetailedHistory_t3933854162::get_offset_of_CipherOn_3(),
	TestDetailedHistory_t3933854162::get_offset_of_AsObject_4(),
	TestDetailedHistory_t3933854162::get_offset_of_NoStore_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3206 = { sizeof (U3CStartU3Ec__Iterator0_t3853365259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3206[6] = 
{
	U3CStartU3Ec__Iterator0_t3853365259::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3853365259::get_offset_of_U3CmessageU3E__1_1(),
	U3CStartU3Ec__Iterator0_t3853365259::get_offset_of_U24this_2(),
	U3CStartU3Ec__Iterator0_t3853365259::get_offset_of_U24current_3(),
	U3CStartU3Ec__Iterator0_t3853365259::get_offset_of_U24disposing_4(),
	U3CStartU3Ec__Iterator0_t3853365259::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3207 = { sizeof (TestDetailedHistoryParams_t206760270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3207[4] = 
{
	TestDetailedHistoryParams_t206760270::get_offset_of_SslOn_2(),
	TestDetailedHistoryParams_t206760270::get_offset_of_CipherOn_3(),
	TestDetailedHistoryParams_t206760270::get_offset_of_AsObject_4(),
	TestDetailedHistoryParams_t206760270::get_offset_of_NoStore_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3208 = { sizeof (U3CStartU3Ec__Iterator0_t4124774743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3208[6] = 
{
	U3CStartU3Ec__Iterator0_t4124774743::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t4124774743::get_offset_of_U3CmessageU3E__1_1(),
	U3CStartU3Ec__Iterator0_t4124774743::get_offset_of_U24this_2(),
	U3CStartU3Ec__Iterator0_t4124774743::get_offset_of_U24current_3(),
	U3CStartU3Ec__Iterator0_t4124774743::get_offset_of_U24disposing_4(),
	U3CStartU3Ec__Iterator0_t4124774743::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3209 = { sizeof (TestGlobalHereNow_t1271793713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3209[3] = 
{
	TestGlobalHereNow_t1271793713::get_offset_of_SslOn_2(),
	TestGlobalHereNow_t1271793713::get_offset_of_CipherOn_3(),
	TestGlobalHereNow_t1271793713::get_offset_of_AsObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3210 = { sizeof (U3CStartU3Ec__Iterator0_t2035977652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3210[5] = 
{
	U3CStartU3Ec__Iterator0_t2035977652::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t2035977652::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2035977652::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2035977652::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2035977652::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3211 = { sizeof (TestHBTimeout_t1340712739), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3212 = { sizeof (U3CStartU3Ec__Iterator0_t1504804398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3212[15] = 
{
	U3CStartU3Ec__Iterator0_t1504804398::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1504804398::get_offset_of_U3CrU3E__1_1(),
	U3CStartU3Ec__Iterator0_t1504804398::get_offset_of_U3CchannelU3E__2_2(),
	U3CStartU3Ec__Iterator0_t1504804398::get_offset_of_U3CmultiChannelU3E__3_3(),
	U3CStartU3Ec__Iterator0_t1504804398::get_offset_of_U3CpubnubU3E__4_4(),
	U3CStartU3Ec__Iterator0_t1504804398::get_offset_of_U3CcrtU3E__5_5(),
	U3CStartU3Ec__Iterator0_t1504804398::get_offset_of_U3CexpectedMessageU3E__6_6(),
	U3CStartU3Ec__Iterator0_t1504804398::get_offset_of_U3CexpectedChannelsU3E__7_7(),
	U3CStartU3Ec__Iterator0_t1504804398::get_offset_of_U3CnanoSecondTimeU3E__8_8(),
	U3CStartU3Ec__Iterator0_t1504804398::get_offset_of_U3CurlU3E__9_9(),
	U3CStartU3Ec__Iterator0_t1504804398::get_offset_of_U3CrespTypeU3E__A_10(),
	U3CStartU3Ec__Iterator0_t1504804398::get_offset_of_U24this_11(),
	U3CStartU3Ec__Iterator0_t1504804398::get_offset_of_U24current_12(),
	U3CStartU3Ec__Iterator0_t1504804398::get_offset_of_U24disposing_13(),
	U3CStartU3Ec__Iterator0_t1504804398::get_offset_of_U24PC_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3213 = { sizeof (TestHereNow_t236862272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3213[5] = 
{
	TestHereNow_t236862272::get_offset_of_SslOn_2(),
	TestHereNow_t236862272::get_offset_of_CipherOn_3(),
	TestHereNow_t236862272::get_offset_of_AsObject_4(),
	TestHereNow_t236862272::get_offset_of_WithState_5(),
	TestHereNow_t236862272::get_offset_of_CustomUUID_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3214 = { sizeof (U3CStartU3Ec__Iterator0_t1125967569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3214[5] = 
{
	U3CStartU3Ec__Iterator0_t1125967569::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1125967569::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t1125967569::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t1125967569::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t1125967569::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3215 = { sizeof (TestNonSubTimeout_t4015492332), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3216 = { sizeof (U3CStartU3Ec__Iterator0_t1346351357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3216[15] = 
{
	U3CStartU3Ec__Iterator0_t1346351357::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1346351357::get_offset_of_U3CrU3E__1_1(),
	U3CStartU3Ec__Iterator0_t1346351357::get_offset_of_U3CchannelU3E__2_2(),
	U3CStartU3Ec__Iterator0_t1346351357::get_offset_of_U3CmultiChannelU3E__3_3(),
	U3CStartU3Ec__Iterator0_t1346351357::get_offset_of_U3CpubnubU3E__4_4(),
	U3CStartU3Ec__Iterator0_t1346351357::get_offset_of_U3CcrtU3E__5_5(),
	U3CStartU3Ec__Iterator0_t1346351357::get_offset_of_U3CexpectedMessageU3E__6_6(),
	U3CStartU3Ec__Iterator0_t1346351357::get_offset_of_U3CexpectedChannelsU3E__7_7(),
	U3CStartU3Ec__Iterator0_t1346351357::get_offset_of_U3CnanoSecondTimeU3E__8_8(),
	U3CStartU3Ec__Iterator0_t1346351357::get_offset_of_U3CurlU3E__9_9(),
	U3CStartU3Ec__Iterator0_t1346351357::get_offset_of_U3CrespTypeU3E__A_10(),
	U3CStartU3Ec__Iterator0_t1346351357::get_offset_of_U24this_11(),
	U3CStartU3Ec__Iterator0_t1346351357::get_offset_of_U24current_12(),
	U3CStartU3Ec__Iterator0_t1346351357::get_offset_of_U24disposing_13(),
	U3CStartU3Ec__Iterator0_t1346351357::get_offset_of_U24PC_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3217 = { sizeof (TestNotSubscribed_t4150449091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3217[3] = 
{
	TestNotSubscribed_t4150449091::get_offset_of_SslOn_2(),
	TestNotSubscribed_t4150449091::get_offset_of_AsObject_3(),
	TestNotSubscribed_t4150449091::get_offset_of_IsPresence_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3218 = { sizeof (U3CStartU3Ec__Iterator0_t381706958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3218[5] = 
{
	U3CStartU3Ec__Iterator0_t381706958::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t381706958::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t381706958::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t381706958::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t381706958::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3219 = { sizeof (TestNullAsEmptyOnPublish_t398058888), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3220 = { sizeof (U3CStartU3Ec__Iterator0_t3908806913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3220[6] = 
{
	U3CStartU3Ec__Iterator0_t3908806913::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3908806913::get_offset_of_U3CTestNameU3E__1_1(),
	U3CStartU3Ec__Iterator0_t3908806913::get_offset_of_U3CpubnubU3E__2_2(),
	U3CStartU3Ec__Iterator0_t3908806913::get_offset_of_U24current_3(),
	U3CStartU3Ec__Iterator0_t3908806913::get_offset_of_U24disposing_4(),
	U3CStartU3Ec__Iterator0_t3908806913::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3221 = { sizeof (TestPHBTimeout_t475805337), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3222 = { sizeof (U3CStartU3Ec__Iterator0_t3001609288), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3222[15] = 
{
	U3CStartU3Ec__Iterator0_t3001609288::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3001609288::get_offset_of_U3CrU3E__1_1(),
	U3CStartU3Ec__Iterator0_t3001609288::get_offset_of_U3CchannelU3E__2_2(),
	U3CStartU3Ec__Iterator0_t3001609288::get_offset_of_U3CmultiChannelU3E__3_3(),
	U3CStartU3Ec__Iterator0_t3001609288::get_offset_of_U3CpubnubU3E__4_4(),
	U3CStartU3Ec__Iterator0_t3001609288::get_offset_of_U3CcrtU3E__5_5(),
	U3CStartU3Ec__Iterator0_t3001609288::get_offset_of_U3CexpectedMessageU3E__6_6(),
	U3CStartU3Ec__Iterator0_t3001609288::get_offset_of_U3CexpectedChannelsU3E__7_7(),
	U3CStartU3Ec__Iterator0_t3001609288::get_offset_of_U3CnanoSecondTimeU3E__8_8(),
	U3CStartU3Ec__Iterator0_t3001609288::get_offset_of_U3CurlU3E__9_9(),
	U3CStartU3Ec__Iterator0_t3001609288::get_offset_of_U3CrespTypeU3E__A_10(),
	U3CStartU3Ec__Iterator0_t3001609288::get_offset_of_U24this_11(),
	U3CStartU3Ec__Iterator0_t3001609288::get_offset_of_U24current_12(),
	U3CStartU3Ec__Iterator0_t3001609288::get_offset_of_U24disposing_13(),
	U3CStartU3Ec__Iterator0_t3001609288::get_offset_of_U24PC_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3223 = { sizeof (TestPresenceCG_t3679081271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3223[5] = 
{
	TestPresenceCG_t3679081271::get_offset_of_SslOn_2(),
	TestPresenceCG_t3679081271::get_offset_of_CipherOn_3(),
	TestPresenceCG_t3679081271::get_offset_of_AsObject_4(),
	TestPresenceCG_t3679081271::get_offset_of_BothString_5(),
	TestPresenceCG_t3679081271::get_offset_of_pubnub_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3224 = { sizeof (U3CStartU3Ec__Iterator0_t3034679634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3224[4] = 
{
	U3CStartU3Ec__Iterator0_t3034679634::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t3034679634::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t3034679634::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t3034679634::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3225 = { sizeof (U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3225[11] = 
{
	U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313::get_offset_of_U3CrU3E__0_0(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313::get_offset_of_U3CchU3E__2_1(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313::get_offset_of_testName_2(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313::get_offset_of_U3CuuidU3E__4_3(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313::get_offset_of_U3CpubMessageU3E__6_4(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313::get_offset_of_U3CchToSubU3E__7_5(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313::get_offset_of_U24this_6(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313::get_offset_of_U24current_7(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313::get_offset_of_U24disposing_8(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313::get_offset_of_U24PC_9(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2161520313::get_offset_of_U24locvar0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3226 = { sizeof (U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3226[8] = 
{
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685::get_offset_of_testName_0(),
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685::get_offset_of_bAddChannel_1(),
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685::get_offset_of_cg_2(),
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685::get_offset_of_channel_3(),
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685::get_offset_of_bGetChannel_4(),
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685::get_offset_of_bSubConnect_5(),
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685::get_offset_of_bUnsub_6(),
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t855549685::get_offset_of_U3CU3Ef__refU241_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3227 = { sizeof (TestPresenceHeartbeat_t57511345), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3228 = { sizeof (U3CStartU3Ec__Iterator0_t1507632776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3228[4] = 
{
	U3CStartU3Ec__Iterator0_t1507632776::get_offset_of_U3CTestNameU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1507632776::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t1507632776::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t1507632776::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3229 = { sizeof (TestProcessTimeout_t1733042336), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3230 = { sizeof (U3CStartU3Ec__Iterator0_t3504551233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3230[3] = 
{
	U3CStartU3Ec__Iterator0_t3504551233::get_offset_of_U24current_0(),
	U3CStartU3Ec__Iterator0_t3504551233::get_offset_of_U24disposing_1(),
	U3CStartU3Ec__Iterator0_t3504551233::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3231 = { sizeof (TestPublishComplex_t3915767209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3231[3] = 
{
	TestPublishComplex_t3915767209::get_offset_of_SslOn_2(),
	TestPublishComplex_t3915767209::get_offset_of_AsObject_3(),
	TestPublishComplex_t3915767209::get_offset_of_WithCipher_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3232 = { sizeof (U3CStartU3Ec__Iterator0_t1117630120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3232[6] = 
{
	U3CStartU3Ec__Iterator0_t1117630120::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1117630120::get_offset_of_U3CmessageU3E__1_1(),
	U3CStartU3Ec__Iterator0_t1117630120::get_offset_of_U24this_2(),
	U3CStartU3Ec__Iterator0_t1117630120::get_offset_of_U24current_3(),
	U3CStartU3Ec__Iterator0_t1117630120::get_offset_of_U24disposing_4(),
	U3CStartU3Ec__Iterator0_t1117630120::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3233 = { sizeof (TestPublishKeyOverride_t2825332490), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3234 = { sizeof (U3CStartU3Ec__Iterator0_t4164458955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3234[7] = 
{
	U3CStartU3Ec__Iterator0_t4164458955::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t4164458955::get_offset_of_U3CTestNameU3E__1_1(),
	U3CStartU3Ec__Iterator0_t4164458955::get_offset_of_U3CpubnubU3E__2_2(),
	U3CStartU3Ec__Iterator0_t4164458955::get_offset_of_U24this_3(),
	U3CStartU3Ec__Iterator0_t4164458955::get_offset_of_U24current_4(),
	U3CStartU3Ec__Iterator0_t4164458955::get_offset_of_U24disposing_5(),
	U3CStartU3Ec__Iterator0_t4164458955::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3235 = { sizeof (TestPublishKeyPresent_t2456351671), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3236 = { sizeof (U3CStartU3Ec__Iterator0_t3478324138), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3236[6] = 
{
	U3CStartU3Ec__Iterator0_t3478324138::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3478324138::get_offset_of_U3CTestNameU3E__1_1(),
	U3CStartU3Ec__Iterator0_t3478324138::get_offset_of_U3CpubnubU3E__2_2(),
	U3CStartU3Ec__Iterator0_t3478324138::get_offset_of_U24current_3(),
	U3CStartU3Ec__Iterator0_t3478324138::get_offset_of_U24disposing_4(),
	U3CStartU3Ec__Iterator0_t3478324138::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3237 = { sizeof (TestPublishNoStore_t3500303089), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3237[5] = 
{
	TestPublishNoStore_t3500303089::get_offset_of_SslOn_2(),
	TestPublishNoStore_t3500303089::get_offset_of_CipherOn_3(),
	TestPublishNoStore_t3500303089::get_offset_of_AsObject_4(),
	TestPublishNoStore_t3500303089::get_offset_of_BothString_5(),
	TestPublishNoStore_t3500303089::get_offset_of_pubnub_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3238 = { sizeof (U3CStartU3Ec__Iterator0_t39973772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3238[4] = 
{
	U3CStartU3Ec__Iterator0_t39973772::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t39973772::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t39973772::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t39973772::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3239 = { sizeof (U3CDoPublishNoStoreU3Ec__Iterator1_t3340936991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3239[7] = 
{
	U3CDoPublishNoStoreU3Ec__Iterator1_t3340936991::get_offset_of_U3CrU3E__0_0(),
	U3CDoPublishNoStoreU3Ec__Iterator1_t3340936991::get_offset_of_U3CuuidU3E__2_1(),
	U3CDoPublishNoStoreU3Ec__Iterator1_t3340936991::get_offset_of_U24this_2(),
	U3CDoPublishNoStoreU3Ec__Iterator1_t3340936991::get_offset_of_U24current_3(),
	U3CDoPublishNoStoreU3Ec__Iterator1_t3340936991::get_offset_of_U24disposing_4(),
	U3CDoPublishNoStoreU3Ec__Iterator1_t3340936991::get_offset_of_U24PC_5(),
	U3CDoPublishNoStoreU3Ec__Iterator1_t3340936991::get_offset_of_U24locvar0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3240 = { sizeof (U3CDoPublishNoStoreU3Ec__AnonStorey2_t56554715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3240[3] = 
{
	U3CDoPublishNoStoreU3Ec__AnonStorey2_t56554715::get_offset_of_ch_0(),
	U3CDoPublishNoStoreU3Ec__AnonStorey2_t56554715::get_offset_of_pubMessage_1(),
	U3CDoPublishNoStoreU3Ec__AnonStorey2_t56554715::get_offset_of_U3CU3Ef__refU241_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3241 = { sizeof (TestPublishSimple_t2553593815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3241[3] = 
{
	TestPublishSimple_t2553593815::get_offset_of_SslOn_2(),
	TestPublishSimple_t2553593815::get_offset_of_AsObject_3(),
	TestPublishSimple_t2553593815::get_offset_of_WithCipher_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3242 = { sizeof (U3CStartU3Ec__Iterator0_t3354114646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3242[5] = 
{
	U3CStartU3Ec__Iterator0_t3354114646::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3354114646::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t3354114646::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t3354114646::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t3354114646::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3243 = { sizeof (TestPublishWithMeta_t1537834550), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3243[5] = 
{
	TestPublishWithMeta_t1537834550::get_offset_of_SslOn_2(),
	TestPublishWithMeta_t1537834550::get_offset_of_CipherOn_3(),
	TestPublishWithMeta_t1537834550::get_offset_of_AsObject_4(),
	TestPublishWithMeta_t1537834550::get_offset_of_BothString_5(),
	TestPublishWithMeta_t1537834550::get_offset_of_pubnub_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3244 = { sizeof (U3CStartU3Ec__Iterator0_t1472748615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3244[4] = 
{
	U3CStartU3Ec__Iterator0_t1472748615::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t1472748615::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t1472748615::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t1472748615::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3245 = { sizeof (U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3245[9] = 
{
	U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496::get_offset_of_U3CrU3E__0_0(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496::get_offset_of_testName_1(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496::get_offset_of_U3CuuidU3E__2_2(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496::get_offset_of_U3CstrLog2U3E__7_3(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496::get_offset_of_U24this_4(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496::get_offset_of_U24current_5(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496::get_offset_of_U24disposing_6(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496::get_offset_of_U24PC_7(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2348182496::get_offset_of_U24locvar0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3246 = { sizeof (U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3246[7] = 
{
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156::get_offset_of_testName_0(),
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156::get_offset_of_pubMessage_1(),
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156::get_offset_of_bSubConnect_2(),
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156::get_offset_of_ch_3(),
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156::get_offset_of_dict_4(),
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156::get_offset_of_bUnsub_5(),
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t2022132156::get_offset_of_U3CU3Ef__refU241_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3247 = { sizeof (TestPublishWithMetaNeg_t257393974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3247[5] = 
{
	TestPublishWithMetaNeg_t257393974::get_offset_of_SslOn_2(),
	TestPublishWithMetaNeg_t257393974::get_offset_of_CipherOn_3(),
	TestPublishWithMetaNeg_t257393974::get_offset_of_AsObject_4(),
	TestPublishWithMetaNeg_t257393974::get_offset_of_BothString_5(),
	TestPublishWithMetaNeg_t257393974::get_offset_of_pubnub_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3248 = { sizeof (U3CStartU3Ec__Iterator0_t498973711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3248[4] = 
{
	U3CStartU3Ec__Iterator0_t498973711::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t498973711::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t498973711::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t498973711::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3249 = { sizeof (U3CDoTestPresenceCGU3Ec__Iterator1_t2226981140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3249[9] = 
{
	U3CDoTestPresenceCGU3Ec__Iterator1_t2226981140::get_offset_of_U3CrU3E__0_0(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2226981140::get_offset_of_testName_1(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2226981140::get_offset_of_U3CuuidU3E__2_2(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2226981140::get_offset_of_U3CstrLog2U3E__8_3(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2226981140::get_offset_of_U24this_4(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2226981140::get_offset_of_U24current_5(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2226981140::get_offset_of_U24disposing_6(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2226981140::get_offset_of_U24PC_7(),
	U3CDoTestPresenceCGU3Ec__Iterator1_t2226981140::get_offset_of_U24locvar0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3250 = { sizeof (U3CDoTestPresenceCGU3Ec__AnonStorey2_t3776434096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3250[8] = 
{
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t3776434096::get_offset_of_testName_0(),
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t3776434096::get_offset_of_pubMessage_1(),
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t3776434096::get_offset_of_bSubConnect_2(),
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t3776434096::get_offset_of_ch_3(),
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t3776434096::get_offset_of_dict_4(),
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t3776434096::get_offset_of_bDetailedHistory_5(),
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t3776434096::get_offset_of_bUnsub_6(),
	U3CDoTestPresenceCGU3Ec__AnonStorey2_t3776434096::get_offset_of_U3CU3Ef__refU241_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3251 = { sizeof (TestRemoveCGAndRemoveAllCG_t911744142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3251[5] = 
{
	TestRemoveCGAndRemoveAllCG_t911744142::get_offset_of_SslOn_2(),
	TestRemoveCGAndRemoveAllCG_t911744142::get_offset_of_CipherOn_3(),
	TestRemoveCGAndRemoveAllCG_t911744142::get_offset_of_AsObject_4(),
	TestRemoveCGAndRemoveAllCG_t911744142::get_offset_of_BothString_5(),
	TestRemoveCGAndRemoveAllCG_t911744142::get_offset_of_pubnub_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3252 = { sizeof (U3CStartU3Ec__Iterator0_t438671695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3252[8] = 
{
	U3CStartU3Ec__Iterator0_t438671695::get_offset_of_U3CMessage1U3E__0_0(),
	U3CStartU3Ec__Iterator0_t438671695::get_offset_of_U3CMessage2U3E__1_1(),
	U3CStartU3Ec__Iterator0_t438671695::get_offset_of_U3CMessageU3E__2_2(),
	U3CStartU3Ec__Iterator0_t438671695::get_offset_of_U3CexpectedMessageU3E__3_3(),
	U3CStartU3Ec__Iterator0_t438671695::get_offset_of_U24this_4(),
	U3CStartU3Ec__Iterator0_t438671695::get_offset_of_U24current_5(),
	U3CStartU3Ec__Iterator0_t438671695::get_offset_of_U24disposing_6(),
	U3CStartU3Ec__Iterator0_t438671695::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3253 = { sizeof (U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3253[12] = 
{
	U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343::get_offset_of_U3CrU3E__0_0(),
	U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343::get_offset_of_U3CchU3E__2_1(),
	U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343::get_offset_of_testName_2(),
	U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343::get_offset_of_U3CbGetAllCGU3E__6_3(),
	U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343::get_offset_of_U3CuuidU3E__7_4(),
	U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343::get_offset_of_U3CstrLogU3E__8_5(),
	U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343::get_offset_of_U3CstrLog2U3E__C_6(),
	U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343::get_offset_of_U24this_7(),
	U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343::get_offset_of_U24current_8(),
	U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343::get_offset_of_U24disposing_9(),
	U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343::get_offset_of_U24PC_10(),
	U3CDoRemoveCGAndRemoveAllCGU3Ec__Iterator1_t3240521343::get_offset_of_U24locvar0_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3254 = { sizeof (U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3254[9] = 
{
	U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458::get_offset_of_testName_0(),
	U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458::get_offset_of_bAddChannel_1(),
	U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458::get_offset_of_cg_2(),
	U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458::get_offset_of_channel_3(),
	U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458::get_offset_of_bGetChannel_4(),
	U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458::get_offset_of_bRemoveCh_5(),
	U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458::get_offset_of_bGetChannel2_6(),
	U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458::get_offset_of_bRemoveAll_7(),
	U3CDoRemoveCGAndRemoveAllCGU3Ec__AnonStorey2_t816457458::get_offset_of_U3CU3Ef__refU241_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3255 = { sizeof (TestSecretKeyOptional_t4164925165), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3256 = { sizeof (U3CStartU3Ec__Iterator0_t363530616), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3256[6] = 
{
	U3CStartU3Ec__Iterator0_t363530616::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t363530616::get_offset_of_U3CTestNameU3E__1_1(),
	U3CStartU3Ec__Iterator0_t363530616::get_offset_of_U24this_2(),
	U3CStartU3Ec__Iterator0_t363530616::get_offset_of_U24current_3(),
	U3CStartU3Ec__Iterator0_t363530616::get_offset_of_U24disposing_4(),
	U3CStartU3Ec__Iterator0_t363530616::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3257 = { sizeof (TestSetAndDeleteGlobalState_t2908412544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3258 = { sizeof (U3CStartU3Ec__Iterator0_t3772056065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3258[6] = 
{
	U3CStartU3Ec__Iterator0_t3772056065::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3772056065::get_offset_of_U3CtestNameU3E__1_1(),
	U3CStartU3Ec__Iterator0_t3772056065::get_offset_of_U24this_2(),
	U3CStartU3Ec__Iterator0_t3772056065::get_offset_of_U24current_3(),
	U3CStartU3Ec__Iterator0_t3772056065::get_offset_of_U24disposing_4(),
	U3CStartU3Ec__Iterator0_t3772056065::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3259 = { sizeof (TestSetAndGetGlobalState_t3454205617), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3260 = { sizeof (U3CStartU3Ec__Iterator0_t2502183628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3260[6] = 
{
	U3CStartU3Ec__Iterator0_t2502183628::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t2502183628::get_offset_of_U3CtestNameU3E__1_1(),
	U3CStartU3Ec__Iterator0_t2502183628::get_offset_of_U24this_2(),
	U3CStartU3Ec__Iterator0_t2502183628::get_offset_of_U24current_3(),
	U3CStartU3Ec__Iterator0_t2502183628::get_offset_of_U24disposing_4(),
	U3CStartU3Ec__Iterator0_t2502183628::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3261 = { sizeof (TestSetAndGetGlobalStateUUID_t667934322), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3262 = { sizeof (U3CStartU3Ec__Iterator0_t3258107483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3262[6] = 
{
	U3CStartU3Ec__Iterator0_t3258107483::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3258107483::get_offset_of_U3CtestNameU3E__1_1(),
	U3CStartU3Ec__Iterator0_t3258107483::get_offset_of_U24this_2(),
	U3CStartU3Ec__Iterator0_t3258107483::get_offset_of_U24current_3(),
	U3CStartU3Ec__Iterator0_t3258107483::get_offset_of_U24disposing_4(),
	U3CStartU3Ec__Iterator0_t3258107483::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3263 = { sizeof (TestSubscribeComplexMessage_t1078578741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3263[3] = 
{
	TestSubscribeComplexMessage_t1078578741::get_offset_of_SslOn_2(),
	TestSubscribeComplexMessage_t1078578741::get_offset_of_CipherOn_3(),
	TestSubscribeComplexMessage_t1078578741::get_offset_of_AsObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3264 = { sizeof (U3CStartU3Ec__Iterator0_t196500632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3264[6] = 
{
	U3CStartU3Ec__Iterator0_t196500632::get_offset_of_U3CMessageU3E__0_0(),
	U3CStartU3Ec__Iterator0_t196500632::get_offset_of_U3CcommonU3E__1_1(),
	U3CStartU3Ec__Iterator0_t196500632::get_offset_of_U24this_2(),
	U3CStartU3Ec__Iterator0_t196500632::get_offset_of_U24current_3(),
	U3CStartU3Ec__Iterator0_t196500632::get_offset_of_U24disposing_4(),
	U3CStartU3Ec__Iterator0_t196500632::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3265 = { sizeof (TestSubscribeDict_t3825317694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3265[4] = 
{
	TestSubscribeDict_t3825317694::get_offset_of_SslOn_2(),
	TestSubscribeDict_t3825317694::get_offset_of_CipherOn_3(),
	TestSubscribeDict_t3825317694::get_offset_of_AsObject_4(),
	TestSubscribeDict_t3825317694::get_offset_of_BothString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3266 = { sizeof (U3CStartU3Ec__Iterator0_t2394687319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3266[9] = 
{
	U3CStartU3Ec__Iterator0_t2394687319::get_offset_of_U3CMessage1U3E__0_0(),
	U3CStartU3Ec__Iterator0_t2394687319::get_offset_of_U3CMessage2U3E__1_1(),
	U3CStartU3Ec__Iterator0_t2394687319::get_offset_of_U3CMessageU3E__2_2(),
	U3CStartU3Ec__Iterator0_t2394687319::get_offset_of_U3CexpectedMessageU3E__3_3(),
	U3CStartU3Ec__Iterator0_t2394687319::get_offset_of_U3CcommonU3E__4_4(),
	U3CStartU3Ec__Iterator0_t2394687319::get_offset_of_U24this_5(),
	U3CStartU3Ec__Iterator0_t2394687319::get_offset_of_U24current_6(),
	U3CStartU3Ec__Iterator0_t2394687319::get_offset_of_U24disposing_7(),
	U3CStartU3Ec__Iterator0_t2394687319::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3267 = { sizeof (TestSubscribeDoubeArray_t3441415060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3267[4] = 
{
	TestSubscribeDoubeArray_t3441415060::get_offset_of_Message_2(),
	TestSubscribeDoubeArray_t3441415060::get_offset_of_SslOn_3(),
	TestSubscribeDoubeArray_t3441415060::get_offset_of_CipherOn_4(),
	TestSubscribeDoubeArray_t3441415060::get_offset_of_AsObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3268 = { sizeof (U3CStartU3Ec__Iterator0_t2179167133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3268[5] = 
{
	U3CStartU3Ec__Iterator0_t2179167133::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t2179167133::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2179167133::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2179167133::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2179167133::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3269 = { sizeof (TestSubscribeDouble_t2305244543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3269[4] = 
{
	TestSubscribeDouble_t2305244543::get_offset_of_Message_2(),
	TestSubscribeDouble_t2305244543::get_offset_of_SslOn_3(),
	TestSubscribeDouble_t2305244543::get_offset_of_CipherOn_4(),
	TestSubscribeDouble_t2305244543::get_offset_of_AsObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3270 = { sizeof (U3CStartU3Ec__Iterator0_t639752250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3270[5] = 
{
	U3CStartU3Ec__Iterator0_t639752250::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t639752250::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t639752250::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t639752250::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t639752250::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3271 = { sizeof (TestSubscribeEmptyArray_t2788072960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3271[4] = 
{
	TestSubscribeEmptyArray_t2788072960::get_offset_of_Message_2(),
	TestSubscribeEmptyArray_t2788072960::get_offset_of_SslOn_3(),
	TestSubscribeEmptyArray_t2788072960::get_offset_of_CipherOn_4(),
	TestSubscribeEmptyArray_t2788072960::get_offset_of_AsObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3272 = { sizeof (U3CStartU3Ec__Iterator0_t2348082985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3272[5] = 
{
	U3CStartU3Ec__Iterator0_t2348082985::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t2348082985::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2348082985::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2348082985::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2348082985::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3273 = { sizeof (TestSubscribeEmptyDict_t256670965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3273[3] = 
{
	TestSubscribeEmptyDict_t256670965::get_offset_of_SslOn_2(),
	TestSubscribeEmptyDict_t256670965::get_offset_of_CipherOn_3(),
	TestSubscribeEmptyDict_t256670965::get_offset_of_AsObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3274 = { sizeof (U3CStartU3Ec__Iterator0_t870106880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3274[6] = 
{
	U3CStartU3Ec__Iterator0_t870106880::get_offset_of_U3CMessageU3E__0_0(),
	U3CStartU3Ec__Iterator0_t870106880::get_offset_of_U3CcommonU3E__1_1(),
	U3CStartU3Ec__Iterator0_t870106880::get_offset_of_U24this_2(),
	U3CStartU3Ec__Iterator0_t870106880::get_offset_of_U24current_3(),
	U3CStartU3Ec__Iterator0_t870106880::get_offset_of_U24disposing_4(),
	U3CStartU3Ec__Iterator0_t870106880::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3275 = { sizeof (TestSubscribeInt_t3684075257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3275[4] = 
{
	TestSubscribeInt_t3684075257::get_offset_of_Message_2(),
	TestSubscribeInt_t3684075257::get_offset_of_SslOn_3(),
	TestSubscribeInt_t3684075257::get_offset_of_CipherOn_4(),
	TestSubscribeInt_t3684075257::get_offset_of_AsObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3276 = { sizeof (U3CStartU3Ec__Iterator0_t2686287252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3276[5] = 
{
	U3CStartU3Ec__Iterator0_t2686287252::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t2686287252::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2686287252::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2686287252::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2686287252::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3277 = { sizeof (TestSubscribeIntArray_t499442242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3277[4] = 
{
	TestSubscribeIntArray_t499442242::get_offset_of_Message_2(),
	TestSubscribeIntArray_t499442242::get_offset_of_SslOn_3(),
	TestSubscribeIntArray_t499442242::get_offset_of_CipherOn_4(),
	TestSubscribeIntArray_t499442242::get_offset_of_AsObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3278 = { sizeof (U3CStartU3Ec__Iterator0_t998415939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3278[5] = 
{
	U3CStartU3Ec__Iterator0_t998415939::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t998415939::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t998415939::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t998415939::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t998415939::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3279 = { sizeof (TestSubscribeJoin_t135406318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3279[3] = 
{
	TestSubscribeJoin_t135406318::get_offset_of_common_2(),
	TestSubscribeJoin_t135406318::get_offset_of_SslOn_3(),
	TestSubscribeJoin_t135406318::get_offset_of_AsObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3280 = { sizeof (U3CStartU3Ec__Iterator0_t3090456015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3280[4] = 
{
	U3CStartU3Ec__Iterator0_t3090456015::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t3090456015::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t3090456015::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t3090456015::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3281 = { sizeof (TestSubscribeLong_t256365834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3281[4] = 
{
	TestSubscribeLong_t256365834::get_offset_of_Message_2(),
	TestSubscribeLong_t256365834::get_offset_of_SslOn_3(),
	TestSubscribeLong_t256365834::get_offset_of_CipherOn_4(),
	TestSubscribeLong_t256365834::get_offset_of_AsObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3282 = { sizeof (U3CStartU3Ec__Iterator0_t1035946611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3282[5] = 
{
	U3CStartU3Ec__Iterator0_t1035946611::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1035946611::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t1035946611::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t1035946611::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t1035946611::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3283 = { sizeof (TestSubscribeLongArray_t1054294135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3283[4] = 
{
	TestSubscribeLongArray_t1054294135::get_offset_of_Message_2(),
	TestSubscribeLongArray_t1054294135::get_offset_of_SslOn_3(),
	TestSubscribeLongArray_t1054294135::get_offset_of_CipherOn_4(),
	TestSubscribeLongArray_t1054294135::get_offset_of_AsObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3284 = { sizeof (U3CStartU3Ec__Iterator0_t751811486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3284[5] = 
{
	U3CStartU3Ec__Iterator0_t751811486::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t751811486::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t751811486::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t751811486::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t751811486::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3285 = { sizeof (TestSubscribePubSubV2PresenceResponse_t425182959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3285[5] = 
{
	TestSubscribePubSubV2PresenceResponse_t425182959::get_offset_of_SslOn_2(),
	TestSubscribePubSubV2PresenceResponse_t425182959::get_offset_of_CipherOn_3(),
	TestSubscribePubSubV2PresenceResponse_t425182959::get_offset_of_AsObject_4(),
	TestSubscribePubSubV2PresenceResponse_t425182959::get_offset_of_BothString_5(),
	TestSubscribePubSubV2PresenceResponse_t425182959::get_offset_of_pubnub_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3286 = { sizeof (U3CStartU3Ec__Iterator0_t2593074586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3286[4] = 
{
	U3CStartU3Ec__Iterator0_t2593074586::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t2593074586::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t2593074586::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t2593074586::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3287 = { sizeof (U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727), -1, sizeof(U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3287[17] = 
{
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727::get_offset_of_U3CrU3E__0_0(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727::get_offset_of_testName_1(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727::get_offset_of_U3CuuidU3E__2_2(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727::get_offset_of_U3CpubMessageU3E__5_3(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727::get_offset_of_U3CbUnsubU3E__6_4(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727::get_offset_of_U3CstrLog2U3E__7_5(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727::get_offset_of_U24this_6(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727::get_offset_of_U24current_7(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727::get_offset_of_U24disposing_8(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727::get_offset_of_U24PC_9(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727::get_offset_of_U24locvar0_10(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_11(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_12(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_13(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_14(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_15(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t460277727_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3288 = { sizeof (U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t2364701085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3288[5] = 
{
	U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t2364701085::get_offset_of_testName_0(),
	U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t2364701085::get_offset_of_ch_1(),
	U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t2364701085::get_offset_of_bSubMessage2_2(),
	U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t2364701085::get_offset_of_bSubConnect_3(),
	U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t2364701085::get_offset_of_U3CU3Ef__refU241_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3289 = { sizeof (TestSubscribePubSubV2Response_t3284876308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3289[5] = 
{
	TestSubscribePubSubV2Response_t3284876308::get_offset_of_SslOn_2(),
	TestSubscribePubSubV2Response_t3284876308::get_offset_of_CipherOn_3(),
	TestSubscribePubSubV2Response_t3284876308::get_offset_of_AsObject_4(),
	TestSubscribePubSubV2Response_t3284876308::get_offset_of_BothString_5(),
	TestSubscribePubSubV2Response_t3284876308::get_offset_of_pubnub_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3290 = { sizeof (U3CStartU3Ec__Iterator0_t2380756133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3290[4] = 
{
	U3CStartU3Ec__Iterator0_t2380756133::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t2380756133::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t2380756133::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t2380756133::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3291 = { sizeof (U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3291[10] = 
{
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694::get_offset_of_U3CrU3E__0_0(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694::get_offset_of_testName_1(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694::get_offset_of_U3CuuidU3E__2_2(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694::get_offset_of_U3CbUnsubU3E__6_3(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694::get_offset_of_U3CstrLog2U3E__7_4(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694::get_offset_of_U24this_5(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694::get_offset_of_U24current_6(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694::get_offset_of_U24disposing_7(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694::get_offset_of_U24PC_8(),
	U3CDoTestSubscribePSV2U3Ec__Iterator1_t1797587694::get_offset_of_U24locvar0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3292 = { sizeof (U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3292[6] = 
{
	U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688::get_offset_of_testName_0(),
	U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688::get_offset_of_pubMessage_1(),
	U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688::get_offset_of_ch_2(),
	U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688::get_offset_of_bSubMessage2_3(),
	U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688::get_offset_of_bSubConnect_4(),
	U3CDoTestSubscribePSV2U3Ec__AnonStorey2_t1093487688::get_offset_of_U3CU3Ef__refU241_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3293 = { sizeof (TestSubscribeSimpleMessage_t3268774053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3293[4] = 
{
	TestSubscribeSimpleMessage_t3268774053::get_offset_of_Message_2(),
	TestSubscribeSimpleMessage_t3268774053::get_offset_of_SslOn_3(),
	TestSubscribeSimpleMessage_t3268774053::get_offset_of_CipherOn_4(),
	TestSubscribeSimpleMessage_t3268774053::get_offset_of_AsObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3294 = { sizeof (U3CStartU3Ec__Iterator0_t2756860596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3294[5] = 
{
	U3CStartU3Ec__Iterator0_t2756860596::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t2756860596::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2756860596::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2756860596::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2756860596::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3295 = { sizeof (TestSubscribeStringArray_t1290748520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3295[4] = 
{
	TestSubscribeStringArray_t1290748520::get_offset_of_Message_2(),
	TestSubscribeStringArray_t1290748520::get_offset_of_SslOn_3(),
	TestSubscribeStringArray_t1290748520::get_offset_of_CipherOn_4(),
	TestSubscribeStringArray_t1290748520::get_offset_of_AsObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3296 = { sizeof (U3CStartU3Ec__Iterator0_t1257108233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3296[5] = 
{
	U3CStartU3Ec__Iterator0_t1257108233::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1257108233::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t1257108233::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t1257108233::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t1257108233::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3297 = { sizeof (TestSubscribeTimeout_t2535103227), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3298 = { sizeof (U3CStartU3Ec__Iterator0_t2176127986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3298[15] = 
{
	U3CStartU3Ec__Iterator0_t2176127986::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t2176127986::get_offset_of_U3CrU3E__1_1(),
	U3CStartU3Ec__Iterator0_t2176127986::get_offset_of_U3CchannelU3E__2_2(),
	U3CStartU3Ec__Iterator0_t2176127986::get_offset_of_U3CmultiChannelU3E__3_3(),
	U3CStartU3Ec__Iterator0_t2176127986::get_offset_of_U3CpubnubU3E__4_4(),
	U3CStartU3Ec__Iterator0_t2176127986::get_offset_of_U3CcrtU3E__5_5(),
	U3CStartU3Ec__Iterator0_t2176127986::get_offset_of_U3CexpectedMessageU3E__6_6(),
	U3CStartU3Ec__Iterator0_t2176127986::get_offset_of_U3CexpectedChannelsU3E__7_7(),
	U3CStartU3Ec__Iterator0_t2176127986::get_offset_of_U3CnanoSecondTimeU3E__8_8(),
	U3CStartU3Ec__Iterator0_t2176127986::get_offset_of_U3CurlU3E__9_9(),
	U3CStartU3Ec__Iterator0_t2176127986::get_offset_of_U3CrespTypeU3E__A_10(),
	U3CStartU3Ec__Iterator0_t2176127986::get_offset_of_U24this_11(),
	U3CStartU3Ec__Iterator0_t2176127986::get_offset_of_U24current_12(),
	U3CStartU3Ec__Iterator0_t2176127986::get_offset_of_U24disposing_13(),
	U3CStartU3Ec__Iterator0_t2176127986::get_offset_of_U24PC_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3299 = { sizeof (TestSubscribeWildcard_t1329469586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3299[5] = 
{
	TestSubscribeWildcard_t1329469586::get_offset_of_SslOn_2(),
	TestSubscribeWildcard_t1329469586::get_offset_of_CipherOn_3(),
	TestSubscribeWildcard_t1329469586::get_offset_of_AsObject_4(),
	TestSubscribeWildcard_t1329469586::get_offset_of_BothString_5(),
	TestSubscribeWildcard_t1329469586::get_offset_of_pubnub_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
