﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr1105815419.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr1988850785.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscri747002922.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr2144859056.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscri402349705.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscrib72015321.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestSubscr2576590760.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestTimeRe2677318202.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestTimeRe3449820275.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestUnsubs1211443923.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestUnsubsc171600722.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestUnsubs2019897593.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestUnsubsc675432200.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestUnsubsc492747701.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestUnsubsc351596005.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestWhereNo234485433.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Tests_TestWhereN2538041892.h"
#include "AssemblyU2DCSharp_SelfDestruct3722248020.h"
#include "AssemblyU2DCSharp_MiniJSON_Json4020371770.h"
#include "AssemblyU2DCSharp_MiniJSON_Json_Parser1915358011.h"
#include "AssemblyU2DCSharp_MiniJSON_Json_Parser_TOKEN2182318091.h"
#include "AssemblyU2DCSharp_MiniJSON_Json_Serializer4088787656.h"
#include "AssemblyU2DCSharp_UnityTest_AssertionComponent3962419315.h"
#include "AssemblyU2DCSharp_UnityTest_AssertionComponent_U3C1299100433.h"
#include "AssemblyU2DCSharp_UnityTest_AssertionException516184885.h"
#include "AssemblyU2DCSharp_UnityTest_Assertions3702573905.h"
#include "AssemblyU2DCSharp_UnityTest_CheckMethod3605307967.h"
#include "AssemblyU2DCSharp_UnityTest_ActionBase1823832315.h"
#include "AssemblyU2DCSharp_UnityTest_BoolComparer1826668123.h"
#include "AssemblyU2DCSharp_UnityTest_ColliderComparer340179063.h"
#include "AssemblyU2DCSharp_UnityTest_ColliderComparer_Compa3910007866.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBase429484414.h"
#include "AssemblyU2DCSharp_UnityTest_ComparerBase_CompareTo3768054886.h"
#include "AssemblyU2DCSharp_UnityTest_FloatComparer4094246107.h"
#include "AssemblyU2DCSharp_UnityTest_FloatComparer_CompareTy553951299.h"
#include "AssemblyU2DCSharp_UnityTest_GeneralComparer3787245533.h"
#include "AssemblyU2DCSharp_UnityTest_GeneralComparer_Compar3001481952.h"
#include "AssemblyU2DCSharp_UnityTest_IntComparer2524366688.h"
#include "AssemblyU2DCSharp_UnityTest_IntComparer_CompareTyp1355928499.h"
#include "AssemblyU2DCSharp_UnityTest_IsRenderedByCamera1258299123.h"
#include "AssemblyU2DCSharp_UnityTest_IsRenderedByCamera_Com3273652030.h"
#include "AssemblyU2DCSharp_UnityTest_StringComparer3025937606.h"
#include "AssemblyU2DCSharp_UnityTest_StringComparer_Compare2214952917.h"
#include "AssemblyU2DCSharp_UnityTest_TransformComparer561999009.h"
#include "AssemblyU2DCSharp_UnityTest_TransformComparer_Comp3894166528.h"
#include "AssemblyU2DCSharp_UnityTest_ValueDoesNotChange1999561901.h"
#include "AssemblyU2DCSharp_UnityTest_Vector2Comparer1532566132.h"
#include "AssemblyU2DCSharp_UnityTest_Vector2Comparer_Compar3262516535.h"
#include "AssemblyU2DCSharp_UnityTest_Vector3Comparer1524406581.h"
#include "AssemblyU2DCSharp_UnityTest_Vector3Comparer_Compar1743505528.h"
#include "AssemblyU2DCSharp_UnityTest_Vector4Comparer1534646382.h"
#include "AssemblyU2DCSharp_UnityTest_Vector4Comparer_Compar1036948413.h"
#include "AssemblyU2DCSharp_UnityTest_InvalidPathException173188949.h"
#include "AssemblyU2DCSharp_UnityTest_MemberResolver2688551152.h"
#include "AssemblyU2DCSharp_UnityTest_TestResultState1620411406.h"
#include "AssemblyU2DCSharp_GameScore3436013872.h"
#include "AssemblyU2DCSharp_CodeBasedAssertionExample389301950.h"
#include "AssemblyU2DCSharp_UnityTest_CustomComponent1909660778.h"
#include "AssemblyU2DCSharp_DynamicIntegrationTest1420509291.h"
#include "AssemblyU2DCSharp_ThrowCustomException959516388.h"
#include "AssemblyU2DCSharp_ThrowCustomException_CustomExcept564769231.h"
#include "AssemblyU2DCSharp_UnityTest_DTOFormatter1172541681.h"
#include "AssemblyU2DCSharp_UnityTest_DTOFormatter_Writer3158935662.h"
#include "AssemblyU2DCSharp_UnityTest_DTOFormatter_Reader3361724802.h"
#include "AssemblyU2DCSharp_IntegrationTest1033695346.h"
#include "AssemblyU2DCSharp_IntegrationTest_ExcludePlatformA4117182796.h"
#include "AssemblyU2DCSharp_IntegrationTest_ExpectExceptions1126243032.h"
#include "AssemblyU2DCSharp_IntegrationTest_IgnoreAttribute323259989.h"
#include "AssemblyU2DCSharp_IntegrationTest_DynamicTestAttri2184122240.h"
#include "AssemblyU2DCSharp_IntegrationTest_SucceedWithAsserti84104168.h"
#include "AssemblyU2DCSharp_IntegrationTest_TimeoutAttribute3752972774.h"
#include "AssemblyU2DCSharp_IntegrationTestAttribute3722261662.h"
#include "AssemblyU2DCSharp_UnityTest_IntegrationTestRunner_1666533096.h"
#include "AssemblyU2DCSharp_UnityTest_NetworkResultSender2246312250.h"
#include "AssemblyU2DCSharp_UnityTest_ResultDTO2310469256.h"
#include "AssemblyU2DCSharp_UnityTest_ResultDTO_MessageType2559459879.h"
#include "AssemblyU2DCSharp_UnityTest_SerializableTestResult3764870664.h"
#include "AssemblyU2DCSharp_UnityTest_TestComponent2516511601.h"
#include "AssemblyU2DCSharp_UnityTest_TestComponent_Included3258709055.h"
#include "AssemblyU2DCSharp_UnityTest_TestComponent_NullTest2842545593.h"
#include "AssemblyU2DCSharp_UnityTest_TestComponent_U3CGetTy2811533093.h"
#include "AssemblyU2DCSharp_UnityTest_TestComponent_U3CGetTyp722069460.h"
#include "AssemblyU2DCSharp_UnityTest_TestResult490498461.h"
#include "AssemblyU2DCSharp_UnityTest_TestResult_ResultType581735912.h"
#include "AssemblyU2DCSharp_TestResultRenderer1986929490.h"
#include "AssemblyU2DCSharp_TestResultRenderer_Styles986091095.h"
#include "AssemblyU2DCSharp_UnityTest_TestRunner1304041832.h"
#include "AssemblyU2DCSharp_UnityTest_TestRunner_TestState193983825.h"
#include "AssemblyU2DCSharp_UnityTest_TestRunner_U3CParseLis3712145952.h"
#include "AssemblyU2DCSharp_UnityTest_TestRunner_U3CStateMach817978249.h"
#include "AssemblyU2DCSharp_UnityTest_IntegrationTestRunner_3689696405.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3300 = { sizeof (U3CStartU3Ec__Iterator0_t1105815419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3300[4] = 
{
	U3CStartU3Ec__Iterator0_t1105815419::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t1105815419::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t1105815419::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t1105815419::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3301 = { sizeof (U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3301[9] = 
{
	U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785::get_offset_of_U3CrU3E__0_0(),
	U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785::get_offset_of_testName_1(),
	U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785::get_offset_of_U3CbUnsubU3E__A_2(),
	U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785::get_offset_of_U3CstrLog2U3E__B_3(),
	U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785::get_offset_of_U24this_4(),
	U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785::get_offset_of_U24current_5(),
	U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785::get_offset_of_U24disposing_6(),
	U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785::get_offset_of_U24PC_7(),
	U3CDoTestSubscribeWildcardU3Ec__Iterator1_t1988850785::get_offset_of_U24locvar0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3302 = { sizeof (U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3302[11] = 
{
	U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922::get_offset_of_testName_0(),
	U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922::get_offset_of_pubMessage_1(),
	U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922::get_offset_of_channel_2(),
	U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922::get_offset_of_bSubMessage_3(),
	U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922::get_offset_of_ch_4(),
	U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922::get_offset_of_bSubMessage2_5(),
	U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922::get_offset_of_bSubConnect_6(),
	U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922::get_offset_of_uuid_7(),
	U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922::get_offset_of_chToSub_8(),
	U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922::get_offset_of_bSubWC_9(),
	U3CDoTestSubscribeWildcardU3Ec__AnonStorey2_t747002922::get_offset_of_U3CU3Ef__refU241_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3303 = { sizeof (TestSubscribeWithTimetoken_t2144859056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3303[5] = 
{
	TestSubscribeWithTimetoken_t2144859056::get_offset_of_SslOn_2(),
	TestSubscribeWithTimetoken_t2144859056::get_offset_of_CipherOn_3(),
	TestSubscribeWithTimetoken_t2144859056::get_offset_of_AsObject_4(),
	TestSubscribeWithTimetoken_t2144859056::get_offset_of_BothString_5(),
	TestSubscribeWithTimetoken_t2144859056::get_offset_of_pubnub_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3304 = { sizeof (U3CStartU3Ec__Iterator0_t402349705), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3304[8] = 
{
	U3CStartU3Ec__Iterator0_t402349705::get_offset_of_U3CMessage1U3E__0_0(),
	U3CStartU3Ec__Iterator0_t402349705::get_offset_of_U3CMessage2U3E__1_1(),
	U3CStartU3Ec__Iterator0_t402349705::get_offset_of_U3CMessageU3E__2_2(),
	U3CStartU3Ec__Iterator0_t402349705::get_offset_of_U3CexpectedMessageU3E__3_3(),
	U3CStartU3Ec__Iterator0_t402349705::get_offset_of_U24this_4(),
	U3CStartU3Ec__Iterator0_t402349705::get_offset_of_U24current_5(),
	U3CStartU3Ec__Iterator0_t402349705::get_offset_of_U24disposing_6(),
	U3CStartU3Ec__Iterator0_t402349705::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3305 = { sizeof (U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3305[13] = 
{
	U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321::get_offset_of_U3CrU3E__0_0(),
	U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321::get_offset_of_U3CchU3E__2_1(),
	U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321::get_offset_of_testName_2(),
	U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321::get_offset_of_U3CbGetAllCGU3E__6_3(),
	U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321::get_offset_of_U3CuuidU3E__7_4(),
	U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321::get_offset_of_U3CstrLogU3E__8_5(),
	U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321::get_offset_of_U3CbRemoveChU3E__D_6(),
	U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321::get_offset_of_U3CstrLog2U3E__E_7(),
	U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321::get_offset_of_U24this_8(),
	U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321::get_offset_of_U24current_9(),
	U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321::get_offset_of_U24disposing_10(),
	U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321::get_offset_of_U24PC_11(),
	U3CDoTestSubscribeWithTimetokenU3Ec__Iterator1_t72015321::get_offset_of_U24locvar0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3306 = { sizeof (U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3306[10] = 
{
	U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760::get_offset_of_testName_0(),
	U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760::get_offset_of_bAddChannel_1(),
	U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760::get_offset_of_cg_2(),
	U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760::get_offset_of_channel_3(),
	U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760::get_offset_of_bGetChannel_4(),
	U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760::get_offset_of_tt_5(),
	U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760::get_offset_of_pubMessage_6(),
	U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760::get_offset_of_bSubMessage_7(),
	U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760::get_offset_of_bUnsub_8(),
	U3CDoTestSubscribeWithTimetokenU3Ec__AnonStorey2_t2576590760::get_offset_of_U3CU3Ef__refU241_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3307 = { sizeof (TestTimeResponse_t2677318202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3307[3] = 
{
	TestTimeResponse_t2677318202::get_offset_of_SslOn_2(),
	TestTimeResponse_t2677318202::get_offset_of_CipherOn_3(),
	TestTimeResponse_t2677318202::get_offset_of_AsObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3308 = { sizeof (U3CStartU3Ec__Iterator0_t3449820275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3308[5] = 
{
	U3CStartU3Ec__Iterator0_t3449820275::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3449820275::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t3449820275::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t3449820275::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t3449820275::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3309 = { sizeof (TestUnsubscribe_t1211443923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3309[3] = 
{
	TestUnsubscribe_t1211443923::get_offset_of_SslOn_2(),
	TestUnsubscribe_t1211443923::get_offset_of_AsObject_3(),
	TestUnsubscribe_t1211443923::get_offset_of_IsPresence_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3310 = { sizeof (U3CStartU3Ec__Iterator0_t171600722), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3310[5] = 
{
	U3CStartU3Ec__Iterator0_t171600722::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t171600722::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t171600722::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t171600722::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t171600722::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3311 = { sizeof (TestUnsubscribeWildcard_t2019897593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3311[5] = 
{
	TestUnsubscribeWildcard_t2019897593::get_offset_of_SslOn_2(),
	TestUnsubscribeWildcard_t2019897593::get_offset_of_CipherOn_3(),
	TestUnsubscribeWildcard_t2019897593::get_offset_of_AsObject_4(),
	TestUnsubscribeWildcard_t2019897593::get_offset_of_BothString_5(),
	TestUnsubscribeWildcard_t2019897593::get_offset_of_pubnub_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3312 = { sizeof (U3CStartU3Ec__Iterator0_t675432200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3312[4] = 
{
	U3CStartU3Ec__Iterator0_t675432200::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t675432200::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t675432200::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t675432200::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3313 = { sizeof (U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3313[10] = 
{
	U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701::get_offset_of_U3CrU3E__0_0(),
	U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701::get_offset_of_U3CchU3E__1_1(),
	U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701::get_offset_of_U3CchannelU3E__2_2(),
	U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701::get_offset_of_testName_3(),
	U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701::get_offset_of_U3CpubMessageU3E__6_4(),
	U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701::get_offset_of_U24this_5(),
	U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701::get_offset_of_U24current_6(),
	U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701::get_offset_of_U24disposing_7(),
	U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701::get_offset_of_U24PC_8(),
	U3CDoTestUnsubscribeWildcardU3Ec__Iterator1_t492747701::get_offset_of_U24locvar0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3314 = { sizeof (U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3314[7] = 
{
	U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005::get_offset_of_testName_0(),
	U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005::get_offset_of_bSubConnect_1(),
	U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005::get_offset_of_uuid_2(),
	U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005::get_offset_of_chToSub_3(),
	U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005::get_offset_of_bSubWC_4(),
	U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005::get_offset_of_bUnsub_5(),
	U3CDoTestUnsubscribeWildcardU3Ec__AnonStorey2_t351596005::get_offset_of_U3CU3Ef__refU241_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3315 = { sizeof (TestWhereNow_t234485433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3315[3] = 
{
	TestWhereNow_t234485433::get_offset_of_SslOn_2(),
	TestWhereNow_t234485433::get_offset_of_CipherOn_3(),
	TestWhereNow_t234485433::get_offset_of_AsObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3316 = { sizeof (U3CStartU3Ec__Iterator0_t2538041892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3316[5] = 
{
	U3CStartU3Ec__Iterator0_t2538041892::get_offset_of_U3CcommonU3E__0_0(),
	U3CStartU3Ec__Iterator0_t2538041892::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2538041892::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2538041892::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2538041892::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3317 = { sizeof (SelfDestruct_t3722248020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3317[1] = 
{
	SelfDestruct_t3722248020::get_offset_of_time_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3318 = { sizeof (Json_t4020371770), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3319 = { sizeof (Parser_t1915358011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3319[2] = 
{
	0,
	Parser_t1915358011::get_offset_of_json_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3320 = { sizeof (TOKEN_t2182318091)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3320[13] = 
{
	TOKEN_t2182318091::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3321 = { sizeof (Serializer_t4088787656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3321[1] = 
{
	Serializer_t4088787656::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3322 = { sizeof (AssertionComponent_t3962419315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3322[13] = 
{
	AssertionComponent_t3962419315::get_offset_of_checkAfterTime_2(),
	AssertionComponent_t3962419315::get_offset_of_repeatCheckTime_3(),
	AssertionComponent_t3962419315::get_offset_of_repeatEveryTime_4(),
	AssertionComponent_t3962419315::get_offset_of_checkAfterFrames_5(),
	AssertionComponent_t3962419315::get_offset_of_repeatCheckFrame_6(),
	AssertionComponent_t3962419315::get_offset_of_repeatEveryFrame_7(),
	AssertionComponent_t3962419315::get_offset_of_hasFailed_8(),
	AssertionComponent_t3962419315::get_offset_of_checkMethods_9(),
	AssertionComponent_t3962419315::get_offset_of_m_ActionBase_10(),
	AssertionComponent_t3962419315::get_offset_of_checksPerformed_11(),
	AssertionComponent_t3962419315::get_offset_of_m_CheckOnFrame_12(),
	AssertionComponent_t3962419315::get_offset_of_m_CreatedInFilePath_13(),
	AssertionComponent_t3962419315::get_offset_of_m_CreatedInFileLine_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3323 = { sizeof (U3CCheckPeriodicallyU3Ec__Iterator0_t1299100433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3323[4] = 
{
	U3CCheckPeriodicallyU3Ec__Iterator0_t1299100433::get_offset_of_U24this_0(),
	U3CCheckPeriodicallyU3Ec__Iterator0_t1299100433::get_offset_of_U24current_1(),
	U3CCheckPeriodicallyU3Ec__Iterator0_t1299100433::get_offset_of_U24disposing_2(),
	U3CCheckPeriodicallyU3Ec__Iterator0_t1299100433::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3324 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3325 = { sizeof (AssertionException_t516184885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3325[1] = 
{
	AssertionException_t516184885::get_offset_of_m_Assertion_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3326 = { sizeof (Assertions_t3702573905), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3327 = { sizeof (CheckMethod_t3605307967)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3327[26] = 
{
	CheckMethod_t3605307967::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3328 = { sizeof (ActionBase_t1823832315), -1, sizeof(ActionBase_t1823832315_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3328[5] = 
{
	ActionBase_t1823832315::get_offset_of_go_2(),
	ActionBase_t1823832315::get_offset_of_m_ObjVal_3(),
	ActionBase_t1823832315::get_offset_of_m_MemberResolver_4(),
	ActionBase_t1823832315::get_offset_of_thisPropertyPath_5(),
	ActionBase_t1823832315_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3329 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3330 = { sizeof (BoolComparer_t1826668123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3331 = { sizeof (ColliderComparer_t340179063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3331[1] = 
{
	ColliderComparer_t340179063::get_offset_of_compareType_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3332 = { sizeof (CompareType_t3910007866)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3332[3] = 
{
	CompareType_t3910007866::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3333 = { sizeof (ComparerBase_t429484414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3333[6] = 
{
	ComparerBase_t429484414::get_offset_of_compareToType_7(),
	ComparerBase_t429484414::get_offset_of_other_8(),
	ComparerBase_t429484414::get_offset_of_m_ObjOtherVal_9(),
	ComparerBase_t429484414::get_offset_of_otherPropertyPath_10(),
	ComparerBase_t429484414::get_offset_of_m_MemberResolverB_11(),
	ComparerBase_t429484414::get_offset_of_U3CConstValueU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3334 = { sizeof (CompareToType_t3768054886)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3334[4] = 
{
	CompareToType_t3768054886::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3335 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3336 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3336[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3337 = { sizeof (FloatComparer_t4094246107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3337[2] = 
{
	FloatComparer_t4094246107::get_offset_of_compareTypes_14(),
	FloatComparer_t4094246107::get_offset_of_floatingPointError_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3338 = { sizeof (CompareTypes_t553951299)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3338[5] = 
{
	CompareTypes_t553951299::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3339 = { sizeof (GeneralComparer_t3787245533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3339[1] = 
{
	GeneralComparer_t3787245533::get_offset_of_compareType_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3340 = { sizeof (CompareType_t3001481952)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3340[3] = 
{
	CompareType_t3001481952::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3341 = { sizeof (IntComparer_t2524366688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3341[1] = 
{
	IntComparer_t2524366688::get_offset_of_compareType_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3342 = { sizeof (CompareType_t1355928499)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3342[7] = 
{
	CompareType_t1355928499::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3343 = { sizeof (IsRenderedByCamera_t1258299123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3343[1] = 
{
	IsRenderedByCamera_t1258299123::get_offset_of_compareType_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3344 = { sizeof (CompareType_t3273652030)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3344[3] = 
{
	CompareType_t3273652030::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3345 = { sizeof (StringComparer_t3025937606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3345[3] = 
{
	StringComparer_t3025937606::get_offset_of_compareType_14(),
	StringComparer_t3025937606::get_offset_of_comparisonType_15(),
	StringComparer_t3025937606::get_offset_of_ignoreCase_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3346 = { sizeof (CompareType_t2214952917)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3346[5] = 
{
	CompareType_t2214952917::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3347 = { sizeof (TransformComparer_t561999009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3347[1] = 
{
	TransformComparer_t561999009::get_offset_of_compareType_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3348 = { sizeof (CompareType_t3894166528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3348[3] = 
{
	CompareType_t3894166528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3349 = { sizeof (ValueDoesNotChange_t1999561901), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3349[1] = 
{
	ValueDoesNotChange_t1999561901::get_offset_of_m_Value_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3350 = { sizeof (Vector2Comparer_t1532566132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3350[2] = 
{
	Vector2Comparer_t1532566132::get_offset_of_compareType_14(),
	Vector2Comparer_t1532566132::get_offset_of_floatingPointError_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3351 = { sizeof (CompareType_t3262516535)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3351[3] = 
{
	CompareType_t3262516535::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3352 = { sizeof (Vector3Comparer_t1524406581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3352[2] = 
{
	Vector3Comparer_t1524406581::get_offset_of_compareType_14(),
	Vector3Comparer_t1524406581::get_offset_of_floatingPointError_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3353 = { sizeof (CompareType_t1743505528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3353[3] = 
{
	CompareType_t1743505528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3354 = { sizeof (Vector4Comparer_t1534646382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3354[2] = 
{
	Vector4Comparer_t1534646382::get_offset_of_compareType_14(),
	Vector4Comparer_t1534646382::get_offset_of_floatingPointError_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3355 = { sizeof (CompareType_t1036948413)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3355[3] = 
{
	CompareType_t1036948413::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3356 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3357 = { sizeof (InvalidPathException_t173188949), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3358 = { sizeof (MemberResolver_t2688551152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3358[4] = 
{
	MemberResolver_t2688551152::get_offset_of_m_CallingObjectRef_0(),
	MemberResolver_t2688551152::get_offset_of_m_Callstack_1(),
	MemberResolver_t2688551152::get_offset_of_m_GameObject_2(),
	MemberResolver_t2688551152::get_offset_of_m_Path_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3359 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3360 = { sizeof (TestResultState_t1620411406)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3360[9] = 
{
	TestResultState_t1620411406::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3361 = { sizeof (GameScore_t3436013872), -1, sizeof(GameScore_t3436013872_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3361[6] = 
{
	GameScore_t3436013872_StaticFields::get_offset_of_s_Instance_2(),
	GameScore_t3436013872::get_offset_of_playerLayerName_3(),
	GameScore_t3436013872::get_offset_of_enemyLayerName_4(),
	GameScore_t3436013872::get_offset_of_m_Deaths_5(),
	GameScore_t3436013872::get_offset_of_m_Kills_6(),
	GameScore_t3436013872::get_offset_of_m_StartTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3362 = { sizeof (CodeBasedAssertionExample_t389301950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3362[2] = 
{
	CodeBasedAssertionExample_t389301950::get_offset_of_FloatField_2(),
	CodeBasedAssertionExample_t389301950::get_offset_of_goReference_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3363 = { sizeof (CustomComponent_t1909660778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3363[2] = 
{
	CustomComponent_t1909660778::get_offset_of_U3CMyFloatPropU3Ek__BackingField_2(),
	CustomComponent_t1909660778::get_offset_of_MyFloatField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3364 = { sizeof (DynamicIntegrationTest_t1420509291), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3365 = { sizeof (ThrowCustomException_t959516388), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3366 = { sizeof (CustomException_t564769231), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3367 = { sizeof (DTOFormatter_t1172541681), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3368 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3369 = { sizeof (Writer_t3158935662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3369[1] = 
{
	Writer_t3158935662::get_offset_of__stream_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3370 = { sizeof (Reader_t3361724802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3370[1] = 
{
	Reader_t3361724802::get_offset_of__stream_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3371 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3372 = { sizeof (IntegrationTest_t1033695346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3372[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3373 = { sizeof (ExcludePlatformAttribute_t4117182796), -1, sizeof(ExcludePlatformAttribute_t4117182796_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3373[2] = 
{
	ExcludePlatformAttribute_t4117182796::get_offset_of_platformsToExclude_0(),
	ExcludePlatformAttribute_t4117182796_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3374 = { sizeof (ExpectExceptions_t1126243032), -1, sizeof(ExpectExceptions_t1126243032_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3374[3] = 
{
	ExpectExceptions_t1126243032::get_offset_of_exceptionTypeNames_0(),
	ExpectExceptions_t1126243032::get_offset_of_succeedOnException_1(),
	ExpectExceptions_t1126243032_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3375 = { sizeof (IgnoreAttribute_t323259989), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3376 = { sizeof (DynamicTestAttribute_t2184122240), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3376[1] = 
{
	DynamicTestAttribute_t2184122240::get_offset_of_m_SceneName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3377 = { sizeof (SucceedWithAssertions_t84104168), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3378 = { sizeof (TimeoutAttribute_t3752972774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3378[1] = 
{
	TimeoutAttribute_t3752972774::get_offset_of_timeout_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3379 = { sizeof (IntegrationTestAttribute_t3722261662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3379[1] = 
{
	IntegrationTestAttribute_t3722261662::get_offset_of_m_Path_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3380 = { sizeof (IntegrationTestsProvider_t1666533096), -1, sizeof(IntegrationTestsProvider_t1666533096_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3380[4] = 
{
	IntegrationTestsProvider_t1666533096::get_offset_of_testCollection_0(),
	IntegrationTestsProvider_t1666533096::get_offset_of_currentTestGroup_1(),
	IntegrationTestsProvider_t1666533096::get_offset_of_testToRun_2(),
	IntegrationTestsProvider_t1666533096_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3381 = { sizeof (NetworkResultSender_t2246312250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3381[1] = 
{
	NetworkResultSender_t2246312250::get_offset_of_m_LostConnection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3382 = { sizeof (ResultDTO_t2310469256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3382[7] = 
{
	ResultDTO_t2310469256::get_offset_of_messageType_0(),
	ResultDTO_t2310469256::get_offset_of_levelCount_1(),
	ResultDTO_t2310469256::get_offset_of_loadedLevel_2(),
	ResultDTO_t2310469256::get_offset_of_loadedLevelName_3(),
	ResultDTO_t2310469256::get_offset_of_testName_4(),
	ResultDTO_t2310469256::get_offset_of_testTimeout_5(),
	ResultDTO_t2310469256::get_offset_of_testResult_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3383 = { sizeof (MessageType_t2559459879)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3383[8] = 
{
	MessageType_t2559459879::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3384 = { sizeof (SerializableTestResult_t3764870664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3384[10] = 
{
	SerializableTestResult_t3764870664::get_offset_of_resultState_0(),
	SerializableTestResult_t3764870664::get_offset_of_message_1(),
	SerializableTestResult_t3764870664::get_offset_of_executed_2(),
	SerializableTestResult_t3764870664::get_offset_of_name_3(),
	SerializableTestResult_t3764870664::get_offset_of_fullName_4(),
	SerializableTestResult_t3764870664::get_offset_of_id_5(),
	SerializableTestResult_t3764870664::get_offset_of_isSuccess_6(),
	SerializableTestResult_t3764870664::get_offset_of_duration_7(),
	SerializableTestResult_t3764870664::get_offset_of_stackTrace_8(),
	SerializableTestResult_t3764870664::get_offset_of_isIgnored_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3385 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3386 = { sizeof (TestComponent_t2516511601), -1, sizeof(TestComponent_t2516511601_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3386[16] = 
{
	TestComponent_t2516511601_StaticFields::get_offset_of_NullTestComponent_2(),
	TestComponent_t2516511601::get_offset_of_timeout_3(),
	TestComponent_t2516511601::get_offset_of_ignored_4(),
	TestComponent_t2516511601::get_offset_of_succeedAfterAllAssertionsAreExecuted_5(),
	TestComponent_t2516511601::get_offset_of_expectException_6(),
	TestComponent_t2516511601::get_offset_of_expectedExceptionList_7(),
	TestComponent_t2516511601::get_offset_of_succeedWhenExceptionIsThrown_8(),
	TestComponent_t2516511601::get_offset_of_includedPlatforms_9(),
	TestComponent_t2516511601::get_offset_of_platformsToIgnore_10(),
	TestComponent_t2516511601::get_offset_of_dynamic_11(),
	TestComponent_t2516511601::get_offset_of_dynamicTypeName_12(),
	TestComponent_t2516511601_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_13(),
	TestComponent_t2516511601_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_14(),
	TestComponent_t2516511601_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_15(),
	TestComponent_t2516511601_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_16(),
	TestComponent_t2516511601_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3387 = { sizeof (IncludedPlatforms_t3258709055)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3387[24] = 
{
	IncludedPlatforms_t3258709055::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3388 = { sizeof (NullTestComponentImpl_t2842545593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3388[1] = 
{
	NullTestComponentImpl_t2842545593::get_offset_of_U3CgameObjectU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3389 = { sizeof (U3CGetTypeByNameU3Ec__AnonStorey1_t2811533093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3389[1] = 
{
	U3CGetTypeByNameU3Ec__AnonStorey1_t2811533093::get_offset_of_className_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3390 = { sizeof (U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3390[13] = 
{
	U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460::get_offset_of_U24locvar0_0(),
	U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460::get_offset_of_U24locvar1_1(),
	U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460::get_offset_of_U3CassemblyU3E__0_2(),
	U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460::get_offset_of_U3CtypesU3E__1_3(),
	U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460::get_offset_of_U24locvar4_4(),
	U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460::get_offset_of_U24locvar5_5(),
	U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460::get_offset_of_U3CtypeU3E__2_6(),
	U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460::get_offset_of_U3CattributesU3E__3_7(),
	U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460::get_offset_of_U3CaU3E__4_8(),
	U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460::get_offset_of_sceneName_9(),
	U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460::get_offset_of_U24current_10(),
	U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460::get_offset_of_U24disposing_11(),
	U3CGetTypesWithHelpAttributeU3Ec__Iterator0_t722069460::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3391 = { sizeof (TestResult_t490498461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3391[9] = 
{
	TestResult_t490498461::get_offset_of_m_Go_0(),
	TestResult_t490498461::get_offset_of_m_Name_1(),
	TestResult_t490498461::get_offset_of_resultType_2(),
	TestResult_t490498461::get_offset_of_duration_3(),
	TestResult_t490498461::get_offset_of_messages_4(),
	TestResult_t490498461::get_offset_of_stacktrace_5(),
	TestResult_t490498461::get_offset_of_id_6(),
	TestResult_t490498461::get_offset_of_dynamicTest_7(),
	TestResult_t490498461::get_offset_of_TestComponent_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3392 = { sizeof (ResultType_t581735912)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3392[7] = 
{
	ResultType_t581735912::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3393 = { sizeof (TestResultRenderer_t1986929490), -1, sizeof(TestResultRenderer_t1986929490_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3393[7] = 
{
	TestResultRenderer_t1986929490::get_offset_of_m_TestCollection_0(),
	TestResultRenderer_t1986929490::get_offset_of_m_ShowResults_1(),
	TestResultRenderer_t1986929490::get_offset_of_m_ScrollPosition_2(),
	TestResultRenderer_t1986929490::get_offset_of_m_FailureCount_3(),
	TestResultRenderer_t1986929490_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	TestResultRenderer_t1986929490_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	TestResultRenderer_t1986929490_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3394 = { sizeof (Styles_t986091095), -1, sizeof(Styles_t986091095_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3394[3] = 
{
	Styles_t986091095_StaticFields::get_offset_of_SucceedLabelStyle_0(),
	Styles_t986091095_StaticFields::get_offset_of_FailedLabelStyle_1(),
	Styles_t986091095_StaticFields::get_offset_of_FailedMessagesStyle_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3395 = { sizeof (TestRunner_t1304041832), -1, sizeof(TestRunner_t1304041832_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3395[33] = 
{
	TestRunner_t1304041832_StaticFields::get_offset_of_TestSceneNumber_2(),
	TestRunner_t1304041832_StaticFields::get_offset_of_k_ResultRenderer_3(),
	TestRunner_t1304041832::get_offset_of_currentTest_4(),
	TestRunner_t1304041832::get_offset_of_m_ResultList_5(),
	TestRunner_t1304041832::get_offset_of_m_TestComponents_6(),
	TestRunner_t1304041832::get_offset_of_m_StartTime_7(),
	TestRunner_t1304041832::get_offset_of_m_ReadyToRun_8(),
	TestRunner_t1304041832::get_offset_of_m_TestMessages_9(),
	TestRunner_t1304041832::get_offset_of_m_Stacktrace_10(),
	TestRunner_t1304041832::get_offset_of_m_TestState_11(),
	TestRunner_t1304041832::get_offset_of_m_Configurator_12(),
	TestRunner_t1304041832::get_offset_of_TestRunnerCallback_13(),
	TestRunner_t1304041832::get_offset_of_m_TestsProvider_14(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	TestRunner_t1304041832_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_23(),
	TestRunner_t1304041832_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_24(),
	TestRunner_t1304041832_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_25(),
	TestRunner_t1304041832_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_26(),
	TestRunner_t1304041832_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_27(),
	TestRunner_t1304041832_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_28(),
	TestRunner_t1304041832_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_29(),
	TestRunner_t1304041832_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_30(),
	TestRunner_t1304041832_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_31(),
	TestRunner_t1304041832_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_32(),
	TestRunner_t1304041832_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_33(),
	TestRunner_t1304041832_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3396 = { sizeof (TestState_t193983825)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3396[7] = 
{
	TestState_t193983825::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3397 = { sizeof (U3CParseListForGroupsU3Ec__AnonStorey1_t3712145952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3397[1] = 
{
	U3CParseListForGroupsU3Ec__AnonStorey1_t3712145952::get_offset_of_testResult_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3398 = { sizeof (U3CStateMachineU3Ec__Iterator0_t817978249), -1, sizeof(U3CStateMachineU3Ec__Iterator0_t817978249_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3398[6] = 
{
	U3CStateMachineU3Ec__Iterator0_t817978249::get_offset_of_U24this_0(),
	U3CStateMachineU3Ec__Iterator0_t817978249::get_offset_of_U24current_1(),
	U3CStateMachineU3Ec__Iterator0_t817978249::get_offset_of_U24disposing_2(),
	U3CStateMachineU3Ec__Iterator0_t817978249::get_offset_of_U24PC_3(),
	U3CStateMachineU3Ec__Iterator0_t817978249_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	U3CStateMachineU3Ec__Iterator0_t817978249_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3399 = { sizeof (TestRunnerCallbackList_t3689696405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3399[1] = 
{
	TestRunnerCallbackList_t3689696405::get_offset_of_m_CallbackList_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
