﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UnityTest_TestRunnerConfigurator1966496711.h"
#include "AssemblyU2DCSharp_UnityTest_CallTesting1988766430.h"
#include "AssemblyU2DCSharp_UnityTest_CallTesting_Functions3020774339.h"
#include "AssemblyU2DCSharp_UnityTest_CallTesting_Method2536378837.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2731437132.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScript_ExplosionPart2473625634.h"
#include "AssemblyU2DUnityScript_EffectSequencer194314474.h"
#include "AssemblyU2DUnityScript_EffectSequencer_U24StartU241610517444.h"
#include "AssemblyU2DUnityScript_EffectSequencer_U24StartU242409507311.h"
#include "AssemblyU2DUnityScript_EffectSequencer_U24Instanti1298213366.h"
#include "AssemblyU2DUnityScript_EffectSequencer_U24Instantia814366785.h"
#include "AssemblyU2DUnityScript_MuzzleFlashAnimate122292636.h"
#include "AssemblyU2DUnityScript_AI2476083018.h"
#include "AssemblyU2DUnityScript_DisableOutsideRadius3052300191.h"
#include "AssemblyU2DUnityScript_PatrolPoint2056099796.h"
#include "AssemblyU2DUnityScript_SpiderAttackMoveController1979379996.h"
#include "AssemblyU2DUnityScript_SpiderReturnMoveController1990791070.h"
#include "AssemblyU2DUnityScript_FanRotate1574437886.h"
#include "AssemblyU2DUnityScript_FootType3548411356.h"
#include "AssemblyU2DUnityScript_FootstepHandler2514543022.h"
#include "AssemblyU2DUnityScript_MechAnimation783079135.h"
#include "AssemblyU2DUnityScript_MechAnimationTest372588337.h"
#include "AssemblyU2DUnityScript_MoveAnimation3061523661.h"
#include "AssemblyU2DUnityScript_PlayerAnimation4286826603.h"
#include "AssemblyU2DUnityScript_SpiderAnimation75583237.h"
#include "AssemblyU2DUnityScript_SpiderAnimationTest3642224283.h"
#include "AssemblyU2DUnityScript_conveyorBelt3030136244.h"
#include "AssemblyU2DUnityScript_GlowPlane2106059055.h"
#include "AssemblyU2DUnityScript_GlowPlaneAngleFade239948010.h"
#include "AssemblyU2DUnityScript_LaserScope1872580701.h"
#include "AssemblyU2DUnityScript_LaserScope_U24ChoseNewAnimat330810268.h"
#include "AssemblyU2DUnityScript_LaserScope_U24ChoseNewAnima1966044057.h"
#include "AssemblyU2DUnityScript_SelfIlluminationBlink1392230251.h"
#include "AssemblyU2DUnityScript_ObjectCache960934699.h"
#include "AssemblyU2DUnityScript_Spawner534830648.h"
#include "AssemblyU2DUnityScript_DestroyObject260685093.h"
#include "AssemblyU2DUnityScript_PlaySound4257033973.h"
#include "AssemblyU2DUnityScript_ReceiverItem169526838.h"
#include "AssemblyU2DUnityScript_ReceiverItem_U24SendWithDel3482389000.h"
#include "AssemblyU2DUnityScript_ReceiverItem_U24SendWithDel4172685971.h"
#include "AssemblyU2DUnityScript_SignalSender1204926691.h"
#include "AssemblyU2DUnityScript_SpawnAtCheckpoint58124256.h"
#include "AssemblyU2DUnityScript_SpawnObject1174160328.h"
#include "AssemblyU2DUnityScript_TriggerOnMouseOrJoystick2646678581.h"
#include "AssemblyU2DUnityScript_FreeMovementMotor1633549708.h"
#include "AssemblyU2DUnityScript_Boundary1794889402.h"
#include "AssemblyU2DUnityScript_Joystick549888914.h"
#include "AssemblyU2DUnityScript_MovementMotor1612021398.h"
#include "AssemblyU2DUnityScript_PlayerMoveController2811056080.h"
#include "AssemblyU2DUnityScript_AutoFire2657208557.h"
#include "AssemblyU2DUnityScript_Health2683907638.h"
#include "AssemblyU2DUnityScript_Health_U24RegenerateU24911949600641.h"
#include "AssemblyU2DUnityScript_Health_U24RegenerateU2491_U3039949752.h"
#include "AssemblyU2DUnityScript_HealthFlash1223120616.h"
#include "AssemblyU2DUnityScript_PerFrameRaycast2437905999.h"
#include "AssemblyU2DUnityScript_SimpleBullet2114900010.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3400 = { sizeof (TestRunnerConfigurator_t1966496711), -1, sizeof(TestRunnerConfigurator_t1966496711_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3400[6] = 
{
	TestRunnerConfigurator_t1966496711_StaticFields::get_offset_of_integrationTestsNetwork_0(),
	TestRunnerConfigurator_t1966496711_StaticFields::get_offset_of_batchRunFileMarker_1(),
	TestRunnerConfigurator_t1966496711_StaticFields::get_offset_of_testScenesToRun_2(),
	TestRunnerConfigurator_t1966496711_StaticFields::get_offset_of_previousScenes_3(),
	TestRunnerConfigurator_t1966496711::get_offset_of_U3CisBatchRunU3Ek__BackingField_4(),
	TestRunnerConfigurator_t1966496711::get_offset_of_U3CsendResultsOverNetworkU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3401 = { sizeof (CallTesting_t1988766430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3401[6] = 
{
	CallTesting_t1988766430::get_offset_of_afterFrames_2(),
	CallTesting_t1988766430::get_offset_of_afterSeconds_3(),
	CallTesting_t1988766430::get_offset_of_callOnMethod_4(),
	CallTesting_t1988766430::get_offset_of_methodToCall_5(),
	CallTesting_t1988766430::get_offset_of_m_StartFrame_6(),
	CallTesting_t1988766430::get_offset_of_m_StartTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3402 = { sizeof (Functions_t3020774339)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3402[27] = 
{
	Functions_t3020774339::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3403 = { sizeof (Method_t2536378837)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3403[3] = 
{
	Method_t2536378837::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3404 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3404[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D1036C5F8EF306104BD582D73E555F4DAE8EECB24_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3405 = { sizeof (U24ArrayTypeU3D20_t2731437132)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D20_t2731437132 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3406 = { sizeof (U3CModuleU3E_t3783534237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3407 = { sizeof (ExplosionPart_t2473625634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3407[4] = 
{
	ExplosionPart_t2473625634::get_offset_of_gameObject_0(),
	ExplosionPart_t2473625634::get_offset_of_delay_1(),
	ExplosionPart_t2473625634::get_offset_of_hqOnly_2(),
	ExplosionPart_t2473625634::get_offset_of_yOffset_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3408 = { sizeof (EffectSequencer_t194314474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3408[4] = 
{
	EffectSequencer_t194314474::get_offset_of_ambientEmitters_2(),
	EffectSequencer_t194314474::get_offset_of_explosionEmitters_3(),
	EffectSequencer_t194314474::get_offset_of_smokeEmitters_4(),
	EffectSequencer_t194314474::get_offset_of_miscSpecialEffects_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3409 = { sizeof (U24StartU2469_t1610517444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3409[1] = 
{
	U24StartU2469_t1610517444::get_offset_of_U24self_U2477_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3410 = { sizeof (U24_t2409507311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3410[7] = 
{
	U24_t2409507311::get_offset_of_U24goU2470_2(),
	U24_t2409507311::get_offset_of_U24maxTimeU2471_3(),
	U24_t2409507311::get_offset_of_U24U24iteratorU2465U2472_4(),
	U24_t2409507311::get_offset_of_U24U24iteratorU2466U2473_5(),
	U24_t2409507311::get_offset_of_U24U24iteratorU2467U2474_6(),
	U24_t2409507311::get_offset_of_U24U24iteratorU2468U2475_7(),
	U24_t2409507311::get_offset_of_U24self_U2476_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3411 = { sizeof (U24InstantiateDelayedU2478_t1298213366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3411[2] = 
{
	U24InstantiateDelayedU2478_t1298213366::get_offset_of_U24goU2481_0(),
	U24InstantiateDelayedU2478_t1298213366::get_offset_of_U24self_U2482_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3412 = { sizeof (U24_t814366785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3412[2] = 
{
	U24_t814366785::get_offset_of_U24goU2479_2(),
	U24_t814366785::get_offset_of_U24self_U2480_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3413 = { sizeof (MuzzleFlashAnimate_t122292636), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3414 = { sizeof (AI_t2476083018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3414[6] = 
{
	AI_t2476083018::get_offset_of_behaviourOnSpotted_2(),
	AI_t2476083018::get_offset_of_soundOnSpotted_3(),
	AI_t2476083018::get_offset_of_behaviourOnLostTrack_4(),
	AI_t2476083018::get_offset_of_character_5(),
	AI_t2476083018::get_offset_of_player_6(),
	AI_t2476083018::get_offset_of_insideInterestArea_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3415 = { sizeof (DisableOutsideRadius_t3052300191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3415[3] = 
{
	DisableOutsideRadius_t3052300191::get_offset_of_target_2(),
	DisableOutsideRadius_t3052300191::get_offset_of_sphereCollider_3(),
	DisableOutsideRadius_t3052300191::get_offset_of_activeRadius_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3416 = { sizeof (PatrolPoint_t2056099796), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3416[1] = 
{
	PatrolPoint_t2056099796::get_offset_of_position_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3417 = { sizeof (SpiderAttackMoveController_t1979379996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3417[23] = 
{
	SpiderAttackMoveController_t1979379996::get_offset_of_motor_2(),
	SpiderAttackMoveController_t1979379996::get_offset_of_targetDistanceMin_3(),
	SpiderAttackMoveController_t1979379996::get_offset_of_targetDistanceMax_4(),
	SpiderAttackMoveController_t1979379996::get_offset_of_proximityDistance_5(),
	SpiderAttackMoveController_t1979379996::get_offset_of_damageRadius_6(),
	SpiderAttackMoveController_t1979379996::get_offset_of_proximityBuildupTime_7(),
	SpiderAttackMoveController_t1979379996::get_offset_of_proximityOfNoReturn_8(),
	SpiderAttackMoveController_t1979379996::get_offset_of_damageAmount_9(),
	SpiderAttackMoveController_t1979379996::get_offset_of_proximityRenderer_10(),
	SpiderAttackMoveController_t1979379996::get_offset_of_audioSource_11(),
	SpiderAttackMoveController_t1979379996::get_offset_of_blinkComponents_12(),
	SpiderAttackMoveController_t1979379996::get_offset_of_blinkPlane_13(),
	SpiderAttackMoveController_t1979379996::get_offset_of_intentionalExplosion_14(),
	SpiderAttackMoveController_t1979379996::get_offset_of_animationBehaviour_15(),
	SpiderAttackMoveController_t1979379996::get_offset_of_ai_16(),
	SpiderAttackMoveController_t1979379996::get_offset_of_character_17(),
	SpiderAttackMoveController_t1979379996::get_offset_of_player_18(),
	SpiderAttackMoveController_t1979379996::get_offset_of_inRange_19(),
	SpiderAttackMoveController_t1979379996::get_offset_of_nextRaycastTime_20(),
	SpiderAttackMoveController_t1979379996::get_offset_of_lastRaycastSuccessfulTime_21(),
	SpiderAttackMoveController_t1979379996::get_offset_of_proximityLevel_22(),
	SpiderAttackMoveController_t1979379996::get_offset_of_lastBlinkTime_23(),
	SpiderAttackMoveController_t1979379996::get_offset_of_noticeTime_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3418 = { sizeof (SpiderReturnMoveController_t1990791070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3418[5] = 
{
	SpiderReturnMoveController_t1990791070::get_offset_of_motor_2(),
	SpiderReturnMoveController_t1990791070::get_offset_of_ai_3(),
	SpiderReturnMoveController_t1990791070::get_offset_of_character_4(),
	SpiderReturnMoveController_t1990791070::get_offset_of_spawnPos_5(),
	SpiderReturnMoveController_t1990791070::get_offset_of_animationBehaviour_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3419 = { sizeof (FanRotate_t1574437886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3419[2] = 
{
	FanRotate_t1574437886::get_offset_of_thisMesh_2(),
	FanRotate_t1574437886::get_offset_of_uvs_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3420 = { sizeof (FootType_t3548411356)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3420[4] = 
{
	FootType_t3548411356::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3421 = { sizeof (FootstepHandler_t2514543022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3421[3] = 
{
	FootstepHandler_t2514543022::get_offset_of_audioSource_2(),
	FootstepHandler_t2514543022::get_offset_of_footType_3(),
	FootstepHandler_t2514543022::get_offset_of_physicMaterial_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3422 = { sizeof (MechAnimation_t783079135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3422[9] = 
{
	MechAnimation_t783079135::get_offset_of_rigid_2(),
	MechAnimation_t783079135::get_offset_of_idle_3(),
	MechAnimation_t783079135::get_offset_of_walk_4(),
	MechAnimation_t783079135::get_offset_of_turnLeft_5(),
	MechAnimation_t783079135::get_offset_of_turnRight_6(),
	MechAnimation_t783079135::get_offset_of_footstepSignals_7(),
	MechAnimation_t783079135::get_offset_of_tr_8(),
	MechAnimation_t783079135::get_offset_of_lastFootstepTime_9(),
	MechAnimation_t783079135::get_offset_of_lastAnimTime_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3423 = { sizeof (MechAnimationTest_t372588337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3423[9] = 
{
	MechAnimationTest_t372588337::get_offset_of_turning_2(),
	MechAnimationTest_t372588337::get_offset_of_walking_3(),
	MechAnimationTest_t372588337::get_offset_of_turnOffset_4(),
	MechAnimationTest_t372588337::get_offset_of_rigid_5(),
	MechAnimationTest_t372588337::get_offset_of_idle_6(),
	MechAnimationTest_t372588337::get_offset_of_walk_7(),
	MechAnimationTest_t372588337::get_offset_of_turnLeft_8(),
	MechAnimationTest_t372588337::get_offset_of_turnRight_9(),
	MechAnimationTest_t372588337::get_offset_of_footstepSignals_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3424 = { sizeof (MoveAnimation_t3061523661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3424[6] = 
{
	MoveAnimation_t3061523661::get_offset_of_clip_0(),
	MoveAnimation_t3061523661::get_offset_of_velocity_1(),
	MoveAnimation_t3061523661::get_offset_of_weight_2(),
	MoveAnimation_t3061523661::get_offset_of_currentBest_3(),
	MoveAnimation_t3061523661::get_offset_of_speed_4(),
	MoveAnimation_t3061523661::get_offset_of_angle_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3425 = { sizeof (PlayerAnimation_t4286826603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3425[24] = 
{
	PlayerAnimation_t4286826603::get_offset_of_rigid_2(),
	PlayerAnimation_t4286826603::get_offset_of_rootBone_3(),
	PlayerAnimation_t4286826603::get_offset_of_upperBodyBone_4(),
	PlayerAnimation_t4286826603::get_offset_of_maxIdleSpeed_5(),
	PlayerAnimation_t4286826603::get_offset_of_minWalkSpeed_6(),
	PlayerAnimation_t4286826603::get_offset_of_idle_7(),
	PlayerAnimation_t4286826603::get_offset_of_turn_8(),
	PlayerAnimation_t4286826603::get_offset_of_shootAdditive_9(),
	PlayerAnimation_t4286826603::get_offset_of_moveAnimations_10(),
	PlayerAnimation_t4286826603::get_offset_of_footstepSignals_11(),
	PlayerAnimation_t4286826603::get_offset_of_tr_12(),
	PlayerAnimation_t4286826603::get_offset_of_lastPosition_13(),
	PlayerAnimation_t4286826603::get_offset_of_velocity_14(),
	PlayerAnimation_t4286826603::get_offset_of_localVelocity_15(),
	PlayerAnimation_t4286826603::get_offset_of_speed_16(),
	PlayerAnimation_t4286826603::get_offset_of_angle_17(),
	PlayerAnimation_t4286826603::get_offset_of_lowerBodyDeltaAngle_18(),
	PlayerAnimation_t4286826603::get_offset_of_idleWeight_19(),
	PlayerAnimation_t4286826603::get_offset_of_lowerBodyForwardTarget_20(),
	PlayerAnimation_t4286826603::get_offset_of_lowerBodyForward_21(),
	PlayerAnimation_t4286826603::get_offset_of_bestAnimation_22(),
	PlayerAnimation_t4286826603::get_offset_of_lastFootstepTime_23(),
	PlayerAnimation_t4286826603::get_offset_of_lastAnimTime_24(),
	PlayerAnimation_t4286826603::get_offset_of_animationComponent_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3426 = { sizeof (SpiderAnimation_t75583237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3426[13] = 
{
	SpiderAnimation_t75583237::get_offset_of_motor_2(),
	SpiderAnimation_t75583237::get_offset_of_activateAnim_3(),
	SpiderAnimation_t75583237::get_offset_of_forwardAnim_4(),
	SpiderAnimation_t75583237::get_offset_of_backAnim_5(),
	SpiderAnimation_t75583237::get_offset_of_leftAnim_6(),
	SpiderAnimation_t75583237::get_offset_of_rightAnim_7(),
	SpiderAnimation_t75583237::get_offset_of_audioSource_8(),
	SpiderAnimation_t75583237::get_offset_of_footstepSignals_9(),
	SpiderAnimation_t75583237::get_offset_of_skiddingSounds_10(),
	SpiderAnimation_t75583237::get_offset_of_footstepSounds_11(),
	SpiderAnimation_t75583237::get_offset_of_tr_12(),
	SpiderAnimation_t75583237::get_offset_of_lastFootstepTime_13(),
	SpiderAnimation_t75583237::get_offset_of_lastAnimTime_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3427 = { sizeof (SpiderAnimationTest_t3642224283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3427[8] = 
{
	SpiderAnimationTest_t3642224283::get_offset_of_rigid_2(),
	SpiderAnimationTest_t3642224283::get_offset_of_forwardAnim_3(),
	SpiderAnimationTest_t3642224283::get_offset_of_backAnim_4(),
	SpiderAnimationTest_t3642224283::get_offset_of_leftAnim_5(),
	SpiderAnimationTest_t3642224283::get_offset_of_rightAnim_6(),
	SpiderAnimationTest_t3642224283::get_offset_of_walking_7(),
	SpiderAnimationTest_t3642224283::get_offset_of_angle_8(),
	SpiderAnimationTest_t3642224283::get_offset_of_tr_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3428 = { sizeof (conveyorBelt_t3030136244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3428[2] = 
{
	conveyorBelt_t3030136244::get_offset_of_scrollSpeed_2(),
	conveyorBelt_t3030136244::get_offset_of_mat_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3429 = { sizeof (GlowPlane_t2106059055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3429[7] = 
{
	GlowPlane_t2106059055::get_offset_of_playerTransform_2(),
	GlowPlane_t2106059055::get_offset_of_pos_3(),
	GlowPlane_t2106059055::get_offset_of_scale_4(),
	GlowPlane_t2106059055::get_offset_of_minGlow_5(),
	GlowPlane_t2106059055::get_offset_of_maxGlow_6(),
	GlowPlane_t2106059055::get_offset_of_glowColor_7(),
	GlowPlane_t2106059055::get_offset_of_mat_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3430 = { sizeof (GlowPlaneAngleFade_t239948010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3430[3] = 
{
	GlowPlaneAngleFade_t239948010::get_offset_of_cameraTransform_2(),
	GlowPlaneAngleFade_t239948010::get_offset_of_glowColor_3(),
	GlowPlaneAngleFade_t239948010::get_offset_of_dot_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3431 = { sizeof (LaserScope_t1872580701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3431[10] = 
{
	LaserScope_t1872580701::get_offset_of_scrollSpeed_2(),
	LaserScope_t1872580701::get_offset_of_pulseSpeed_3(),
	LaserScope_t1872580701::get_offset_of_noiseSize_4(),
	LaserScope_t1872580701::get_offset_of_maxWidth_5(),
	LaserScope_t1872580701::get_offset_of_minWidth_6(),
	LaserScope_t1872580701::get_offset_of_pointer_7(),
	LaserScope_t1872580701::get_offset_of_lRenderer_8(),
	LaserScope_t1872580701::get_offset_of_aniTime_9(),
	LaserScope_t1872580701::get_offset_of_aniDir_10(),
	LaserScope_t1872580701::get_offset_of_raycast_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3432 = { sizeof (U24ChoseNewAnimationTargetCoroutineU2483_t330810268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3432[1] = 
{
	U24ChoseNewAnimationTargetCoroutineU2483_t330810268::get_offset_of_U24self_U2485_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3433 = { sizeof (U24_t1966044057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3433[1] = 
{
	U24_t1966044057::get_offset_of_U24self_U2484_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3434 = { sizeof (SelfIlluminationBlink_t1392230251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3434[1] = 
{
	SelfIlluminationBlink_t1392230251::get_offset_of_blink_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3435 = { sizeof (ObjectCache_t960934699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3435[4] = 
{
	ObjectCache_t960934699::get_offset_of_prefab_0(),
	ObjectCache_t960934699::get_offset_of_cacheSize_1(),
	ObjectCache_t960934699::get_offset_of_objects_2(),
	ObjectCache_t960934699::get_offset_of_cacheIndex_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3436 = { sizeof (Spawner_t534830648), -1, sizeof(Spawner_t534830648_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3436[3] = 
{
	Spawner_t534830648_StaticFields::get_offset_of_spawner_2(),
	Spawner_t534830648::get_offset_of_caches_3(),
	Spawner_t534830648::get_offset_of_activeCachedObjects_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3437 = { sizeof (DestroyObject_t260685093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3437[1] = 
{
	DestroyObject_t260685093::get_offset_of_objectToDestroy_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3438 = { sizeof (PlaySound_t4257033973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3438[2] = 
{
	PlaySound_t4257033973::get_offset_of_audioSource_2(),
	PlaySound_t4257033973::get_offset_of_sound_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3439 = { sizeof (ReceiverItem_t169526838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3439[3] = 
{
	ReceiverItem_t169526838::get_offset_of_receiver_0(),
	ReceiverItem_t169526838::get_offset_of_action_1(),
	ReceiverItem_t169526838::get_offset_of_delay_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3440 = { sizeof (U24SendWithDelayU2486_t3482389000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3440[2] = 
{
	U24SendWithDelayU2486_t3482389000::get_offset_of_U24senderU2489_0(),
	U24SendWithDelayU2486_t3482389000::get_offset_of_U24self_U2490_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3441 = { sizeof (U24_t4172685971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3441[2] = 
{
	U24_t4172685971::get_offset_of_U24senderU2487_2(),
	U24_t4172685971::get_offset_of_U24self_U2488_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3442 = { sizeof (SignalSender_t1204926691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3442[3] = 
{
	SignalSender_t1204926691::get_offset_of_onlyOnce_0(),
	SignalSender_t1204926691::get_offset_of_receivers_1(),
	SignalSender_t1204926691::get_offset_of_hasFired_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3443 = { sizeof (SpawnAtCheckpoint_t58124256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3443[1] = 
{
	SpawnAtCheckpoint_t58124256::get_offset_of_checkpoint_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3444 = { sizeof (SpawnObject_t1174160328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3444[3] = 
{
	SpawnObject_t1174160328::get_offset_of_objectToSpawn_2(),
	SpawnObject_t1174160328::get_offset_of_onDestroyedSignals_3(),
	SpawnObject_t1174160328::get_offset_of_spawned_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3445 = { sizeof (TriggerOnMouseOrJoystick_t2646678581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3445[4] = 
{
	TriggerOnMouseOrJoystick_t2646678581::get_offset_of_mouseDownSignals_2(),
	TriggerOnMouseOrJoystick_t2646678581::get_offset_of_mouseUpSignals_3(),
	TriggerOnMouseOrJoystick_t2646678581::get_offset_of_state_4(),
	TriggerOnMouseOrJoystick_t2646678581::get_offset_of_joysticks_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3446 = { sizeof (FreeMovementMotor_t1633549708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3446[3] = 
{
	FreeMovementMotor_t1633549708::get_offset_of_walkingSpeed_5(),
	FreeMovementMotor_t1633549708::get_offset_of_walkingSnappyness_6(),
	FreeMovementMotor_t1633549708::get_offset_of_turningSmoothing_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3447 = { sizeof (Boundary_t1794889402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3447[2] = 
{
	Boundary_t1794889402::get_offset_of_min_0(),
	Boundary_t1794889402::get_offset_of_max_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3448 = { sizeof (Joystick_t549888914), -1, sizeof(Joystick_t549888914_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3448[19] = 
{
	Joystick_t549888914_StaticFields::get_offset_of_joysticks_2(),
	Joystick_t549888914_StaticFields::get_offset_of_enumeratedJoysticks_3(),
	Joystick_t549888914_StaticFields::get_offset_of_tapTimeDelta_4(),
	Joystick_t549888914::get_offset_of_touchPad_5(),
	Joystick_t549888914::get_offset_of_touchZone_6(),
	Joystick_t549888914::get_offset_of_deadZone_7(),
	Joystick_t549888914::get_offset_of_normalize_8(),
	Joystick_t549888914::get_offset_of_position_9(),
	Joystick_t549888914::get_offset_of_tapCount_10(),
	Joystick_t549888914::get_offset_of_lastFingerId_11(),
	Joystick_t549888914::get_offset_of_tapTimeWindow_12(),
	Joystick_t549888914::get_offset_of_fingerDownPos_13(),
	Joystick_t549888914::get_offset_of_fingerDownTime_14(),
	Joystick_t549888914::get_offset_of_firstDeltaTime_15(),
	Joystick_t549888914::get_offset_of_gui_16(),
	Joystick_t549888914::get_offset_of_defaultRect_17(),
	Joystick_t549888914::get_offset_of_guiBoundary_18(),
	Joystick_t549888914::get_offset_of_guiTouchOffset_19(),
	Joystick_t549888914::get_offset_of_guiCenter_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3449 = { sizeof (MovementMotor_t1612021398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3449[3] = 
{
	MovementMotor_t1612021398::get_offset_of_movementDirection_2(),
	MovementMotor_t1612021398::get_offset_of_movementTarget_3(),
	MovementMotor_t1612021398::get_offset_of_facingDirection_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3450 = { sizeof (PlayerMoveController_t2811056080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3450[24] = 
{
	PlayerMoveController_t2811056080::get_offset_of_motor_2(),
	PlayerMoveController_t2811056080::get_offset_of_character_3(),
	PlayerMoveController_t2811056080::get_offset_of_cursorPrefab_4(),
	PlayerMoveController_t2811056080::get_offset_of_joystickPrefab_5(),
	PlayerMoveController_t2811056080::get_offset_of_cameraSmoothing_6(),
	PlayerMoveController_t2811056080::get_offset_of_cameraPreview_7(),
	PlayerMoveController_t2811056080::get_offset_of_cursorPlaneHeight_8(),
	PlayerMoveController_t2811056080::get_offset_of_cursorFacingCamera_9(),
	PlayerMoveController_t2811056080::get_offset_of_cursorSmallerWithDistance_10(),
	PlayerMoveController_t2811056080::get_offset_of_cursorSmallerWhenClose_11(),
	PlayerMoveController_t2811056080::get_offset_of_mainCamera_12(),
	PlayerMoveController_t2811056080::get_offset_of_cursorObject_13(),
	PlayerMoveController_t2811056080::get_offset_of_joystickLeft_14(),
	PlayerMoveController_t2811056080::get_offset_of_joystickRight_15(),
	PlayerMoveController_t2811056080::get_offset_of_mainCameraTransform_16(),
	PlayerMoveController_t2811056080::get_offset_of_cameraVelocity_17(),
	PlayerMoveController_t2811056080::get_offset_of_cameraOffset_18(),
	PlayerMoveController_t2811056080::get_offset_of_initOffsetToPlayer_19(),
	PlayerMoveController_t2811056080::get_offset_of_cursorScreenPosition_20(),
	PlayerMoveController_t2811056080::get_offset_of_playerMovementPlane_21(),
	PlayerMoveController_t2811056080::get_offset_of_joystickRightGO_22(),
	PlayerMoveController_t2811056080::get_offset_of_screenMovementSpace_23(),
	PlayerMoveController_t2811056080::get_offset_of_screenMovementForward_24(),
	PlayerMoveController_t2811056080::get_offset_of_screenMovementRight_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3451 = { sizeof (AutoFire_t2657208557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3451[11] = 
{
	AutoFire_t2657208557::get_offset_of_bulletPrefab_2(),
	AutoFire_t2657208557::get_offset_of_spawnPoint_3(),
	AutoFire_t2657208557::get_offset_of_frequency_4(),
	AutoFire_t2657208557::get_offset_of_coneAngle_5(),
	AutoFire_t2657208557::get_offset_of_firing_6(),
	AutoFire_t2657208557::get_offset_of_damagePerSecond_7(),
	AutoFire_t2657208557::get_offset_of_forcePerSecond_8(),
	AutoFire_t2657208557::get_offset_of_hitSoundVolume_9(),
	AutoFire_t2657208557::get_offset_of_muzzleFlashFront_10(),
	AutoFire_t2657208557::get_offset_of_lastFireTime_11(),
	AutoFire_t2657208557::get_offset_of_raycast_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3452 = { sizeof (Health_t2683907638), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3452[16] = 
{
	Health_t2683907638::get_offset_of_maxHealth_2(),
	Health_t2683907638::get_offset_of_health_3(),
	Health_t2683907638::get_offset_of_regenerateSpeed_4(),
	Health_t2683907638::get_offset_of_invincible_5(),
	Health_t2683907638::get_offset_of_dead_6(),
	Health_t2683907638::get_offset_of_damagePrefab_7(),
	Health_t2683907638::get_offset_of_damageEffectTransform_8(),
	Health_t2683907638::get_offset_of_damageEffectMultiplier_9(),
	Health_t2683907638::get_offset_of_damageEffectCentered_10(),
	Health_t2683907638::get_offset_of_scorchMarkPrefab_11(),
	Health_t2683907638::get_offset_of_scorchMark_12(),
	Health_t2683907638::get_offset_of_damageSignals_13(),
	Health_t2683907638::get_offset_of_dieSignals_14(),
	Health_t2683907638::get_offset_of_lastDamageTime_15(),
	Health_t2683907638::get_offset_of_damageEffectCenterYOffset_16(),
	Health_t2683907638::get_offset_of_colliderRadiusHeuristic_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3453 = { sizeof (U24RegenerateU2491_t1949600641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3453[1] = 
{
	U24RegenerateU2491_t1949600641::get_offset_of_U24self_U2493_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3454 = { sizeof (U24_t3039949752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3454[1] = 
{
	U24_t3039949752::get_offset_of_U24self_U2492_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3455 = { sizeof (HealthFlash_t1223120616), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3455[4] = 
{
	HealthFlash_t1223120616::get_offset_of_playerHealth_2(),
	HealthFlash_t1223120616::get_offset_of_healthMaterial_3(),
	HealthFlash_t1223120616::get_offset_of_healthBlink_4(),
	HealthFlash_t1223120616::get_offset_of_oneOverMaxHealth_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3456 = { sizeof (PerFrameRaycast_t2437905999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3456[2] = 
{
	PerFrameRaycast_t2437905999::get_offset_of_hitInfo_2(),
	PerFrameRaycast_t2437905999::get_offset_of_tr_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3457 = { sizeof (SimpleBullet_t2114900010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3457[5] = 
{
	SimpleBullet_t2114900010::get_offset_of_speed_2(),
	SimpleBullet_t2114900010::get_offset_of_lifeTime_3(),
	SimpleBullet_t2114900010::get_offset_of_dist_4(),
	SimpleBullet_t2114900010::get_offset_of_spawnTime_5(),
	SimpleBullet_t2114900010::get_offset_of_tr_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
