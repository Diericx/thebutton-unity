﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_SecurityException887327375.h"
#include "mscorlib_System_Security_RuntimeDeclSecurityAction4265870207.h"
#include "mscorlib_System_Security_SecurityManager3191249573.h"
#include "mscorlib_System_Security_SecuritySafeCriticalAttrib372031554.h"
#include "mscorlib_System_Security_SecurityZone140334334.h"
#include "mscorlib_System_Security_SuppressUnmanagedCodeSecuri39244474.h"
#include "mscorlib_System_Security_UnverifiableCodeAttribute765455733.h"
#include "mscorlib_System_Security_XmlSyntaxException2895742617.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricAlg784058677.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricKe3339648384.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricSi3580832979.h"
#include "mscorlib_System_Security_Cryptography_AsymmetricSi4058014248.h"
#include "mscorlib_System_Security_Cryptography_Base64Consta4112722312.h"
#include "mscorlib_System_Security_Cryptography_CipherMode162592484.h"
#include "mscorlib_System_Security_Cryptography_CryptoConfig896479599.h"
#include "mscorlib_System_Security_Cryptography_CryptoConfig2096418190.h"
#include "mscorlib_System_Security_Cryptography_Cryptographi3349726436.h"
#include "mscorlib_System_Security_Cryptography_Cryptographi4184064416.h"
#include "mscorlib_System_Security_Cryptography_CryptoStream3531341937.h"
#include "mscorlib_System_Security_Cryptography_CryptoStream1337713182.h"
#include "mscorlib_System_Security_Cryptography_CspParameters46065560.h"
#include "mscorlib_System_Security_Cryptography_CspProviderFl105264000.h"
#include "mscorlib_System_Security_Cryptography_DES1353513560.h"
#include "mscorlib_System_Security_Cryptography_DESTransform179750796.h"
#include "mscorlib_System_Security_Cryptography_DESCryptoServ933603253.h"
#include "mscorlib_System_Security_Cryptography_DSA903174880.h"
#include "mscorlib_System_Security_Cryptography_DSACryptoSer2915171657.h"
#include "mscorlib_System_Security_Cryptography_DSAParameter1872138834.h"
#include "mscorlib_System_Security_Cryptography_DSASignature2187578719.h"
#include "mscorlib_System_Security_Cryptography_DSASignature1065727064.h"
#include "mscorlib_System_Security_Cryptography_FromBase64Tr2331250892.h"
#include "mscorlib_System_Security_Cryptography_FromBase64Tr3227039347.h"
#include "mscorlib_System_Security_Cryptography_HashAlgorith2624936259.h"
#include "mscorlib_System_Security_Cryptography_HMAC130461695.h"
#include "mscorlib_System_Security_Cryptography_HMACMD52214610803.h"
#include "mscorlib_System_Security_Cryptography_HMACRIPEMD160131410643.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA11958407246.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA2562622794722.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA3843026079344.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA512653426285.h"
#include "mscorlib_System_Security_Cryptography_KeyedHashAlg1374150027.h"
#include "mscorlib_System_Security_Cryptography_KeySizes3144736271.h"
#include "mscorlib_System_Security_Cryptography_MACTripleDES442445873.h"
#include "mscorlib_System_Security_Cryptography_MD51507972490.h"
#include "mscorlib_System_Security_Cryptography_MD5CryptoSer4009738925.h"
#include "mscorlib_System_Security_Cryptography_PaddingMode3032142640.h"
#include "mscorlib_System_Security_Cryptography_RandomNumber2510243513.h"
#include "mscorlib_System_Security_Cryptography_RC23410342145.h"
#include "mscorlib_System_Security_Cryptography_RC2CryptoServ663781682.h"
#include "mscorlib_System_Security_Cryptography_RC2Transform93681661.h"
#include "mscorlib_System_Security_Cryptography_Rijndael2154803531.h"
#include "mscorlib_System_Security_Cryptography_RijndaelMana1034060848.h"
#include "mscorlib_System_Security_Cryptography_RijndaelTran1067822295.h"
#include "mscorlib_System_Security_Cryptography_RijndaelManag135163252.h"
#include "mscorlib_System_Security_Cryptography_RIPEMD1601732039966.h"
#include "mscorlib_System_Security_Cryptography_RIPEMD160Man1613307429.h"
#include "mscorlib_System_Security_Cryptography_RNGCryptoSer2688843926.h"
#include "mscorlib_System_Security_Cryptography_RSA3719518354.h"
#include "mscorlib_System_Security_Cryptography_RSACryptoSer4229286967.h"
#include "mscorlib_System_Security_Cryptography_RSAParameter1462703416.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1KeyE4167037264.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1Signa145198701.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1Signa925064184.h"
#include "mscorlib_System_Security_Cryptography_SHA13336793149.h"
#include "mscorlib_System_Security_Cryptography_SHA1Internal3873507626.h"
#include "mscorlib_System_Security_Cryptography_SHA1CryptoSe3913997830.h"
#include "mscorlib_System_Security_Cryptography_SHA1Managed7268864.h"
#include "mscorlib_System_Security_Cryptography_SHA256582564463.h"
#include "mscorlib_System_Security_Cryptography_SHA256Manage2029745292.h"
#include "mscorlib_System_Security_Cryptography_SHA384535510267.h"
#include "mscorlib_System_Security_Cryptography_SHA384Managed741627254.h"
#include "mscorlib_System_Security_Cryptography_SHA5122908163326.h"
#include "mscorlib_System_Security_Cryptography_SHA512Manage3949709369.h"
#include "mscorlib_System_Security_Cryptography_SHAConstants3712871299.h"
#include "mscorlib_System_Security_Cryptography_SignatureDescr89145500.h"
#include "mscorlib_System_Security_Cryptography_DSASignature1998527418.h"
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SHA12477284625.h"
#include "mscorlib_System_Security_Cryptography_SymmetricAlg1108166522.h"
#include "mscorlib_System_Security_Cryptography_ToBase64Trans625739466.h"
#include "mscorlib_System_Security_Cryptography_TripleDES243950698.h"
#include "mscorlib_System_Security_Cryptography_TripleDESCry2380467305.h"
#include "mscorlib_System_Security_Cryptography_TripleDESTra2384411462.h"
#include "mscorlib_System_Security_Cryptography_X509Certifica283079845.h"
#include "mscorlib_System_Security_Cryptography_X509Certific1216946873.h"
#include "mscorlib_System_Security_Permissions_EnvironmentPer935902940.h"
#include "mscorlib_System_Security_Permissions_EnvironmentPe3182667772.h"
#include "mscorlib_System_Security_Permissions_FileDialogPer3088110531.h"
#include "mscorlib_System_Security_Permissions_FileDialogPer4165817673.h"
#include "mscorlib_System_Security_Permissions_FileIOPermiss3702443043.h"
#include "mscorlib_System_Security_Permissions_FileIOPermiss1079931101.h"
#include "mscorlib_System_Security_Permissions_GacIdentityPer944690662.h"
#include "mscorlib_System_Security_Permissions_IsolatedStorag174977552.h"
#include "mscorlib_System_Security_Permissions_IsolatedStora3424948193.h"
#include "mscorlib_System_Security_Permissions_IsolatedStora3796409103.h"
#include "mscorlib_System_Security_Permissions_KeyContainerP3643383499.h"
#include "mscorlib_System_Security_Permissions_KeyContainerPer41069825.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize800 = { sizeof (SecurityException_t887327375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable800[8] = 
{
	SecurityException_t887327375::get_offset_of_permissionState_11(),
	SecurityException_t887327375::get_offset_of_permissionType_12(),
	SecurityException_t887327375::get_offset_of__granted_13(),
	SecurityException_t887327375::get_offset_of__refused_14(),
	SecurityException_t887327375::get_offset_of__demanded_15(),
	SecurityException_t887327375::get_offset_of__firstperm_16(),
	SecurityException_t887327375::get_offset_of__method_17(),
	SecurityException_t887327375::get_offset_of__evidence_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize801 = { sizeof (RuntimeDeclSecurityActions_t4265870207)+ sizeof (Il2CppObject), sizeof(RuntimeDeclSecurityActions_t4265870207 ), 0, 0 };
extern const int32_t g_FieldOffsetTable801[3] = 
{
	RuntimeDeclSecurityActions_t4265870207::get_offset_of_cas_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RuntimeDeclSecurityActions_t4265870207::get_offset_of_noncas_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RuntimeDeclSecurityActions_t4265870207::get_offset_of_choice_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize802 = { sizeof (SecurityManager_t3191249573), -1, sizeof(SecurityManager_t3191249573_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable802[5] = 
{
	SecurityManager_t3191249573_StaticFields::get_offset_of__lockObject_0(),
	SecurityManager_t3191249573_StaticFields::get_offset_of__hierarchy_1(),
	SecurityManager_t3191249573_StaticFields::get_offset_of__declsecCache_2(),
	SecurityManager_t3191249573_StaticFields::get_offset_of__level_3(),
	SecurityManager_t3191249573_StaticFields::get_offset_of__execution_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize803 = { sizeof (SecuritySafeCriticalAttribute_t372031554), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize804 = { sizeof (SecurityZone_t140334334)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable804[7] = 
{
	SecurityZone_t140334334::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize805 = { sizeof (SuppressUnmanagedCodeSecurityAttribute_t39244474), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize806 = { sizeof (UnverifiableCodeAttribute_t765455733), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize807 = { sizeof (XmlSyntaxException_t2895742617), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize808 = { sizeof (AsymmetricAlgorithm_t784058677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable808[2] = 
{
	AsymmetricAlgorithm_t784058677::get_offset_of_KeySizeValue_0(),
	AsymmetricAlgorithm_t784058677::get_offset_of_LegalKeySizesValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize809 = { sizeof (AsymmetricKeyExchangeFormatter_t3339648384), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize810 = { sizeof (AsymmetricSignatureDeformatter_t3580832979), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize811 = { sizeof (AsymmetricSignatureFormatter_t4058014248), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize812 = { sizeof (Base64Constants_t4112722312), -1, sizeof(Base64Constants_t4112722312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable812[2] = 
{
	Base64Constants_t4112722312_StaticFields::get_offset_of_EncodeTable_0(),
	Base64Constants_t4112722312_StaticFields::get_offset_of_DecodeTable_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize813 = { sizeof (CipherMode_t162592484)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable813[6] = 
{
	CipherMode_t162592484::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize814 = { sizeof (CryptoConfig_t896479599), -1, sizeof(CryptoConfig_t896479599_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable814[3] = 
{
	CryptoConfig_t896479599_StaticFields::get_offset_of_lockObject_0(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_algorithms_1(),
	CryptoConfig_t896479599_StaticFields::get_offset_of_oid_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize815 = { sizeof (CryptoHandler_t2096418190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable815[5] = 
{
	CryptoHandler_t2096418190::get_offset_of_algorithms_0(),
	CryptoHandler_t2096418190::get_offset_of_oid_1(),
	CryptoHandler_t2096418190::get_offset_of_names_2(),
	CryptoHandler_t2096418190::get_offset_of_classnames_3(),
	CryptoHandler_t2096418190::get_offset_of_level_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize816 = { sizeof (CryptographicException_t3349726436), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize817 = { sizeof (CryptographicUnexpectedOperationException_t4184064416), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize818 = { sizeof (CryptoStream_t3531341937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable818[15] = 
{
	CryptoStream_t3531341937::get_offset_of__stream_2(),
	CryptoStream_t3531341937::get_offset_of__transform_3(),
	CryptoStream_t3531341937::get_offset_of__mode_4(),
	CryptoStream_t3531341937::get_offset_of__currentBlock_5(),
	CryptoStream_t3531341937::get_offset_of__disposed_6(),
	CryptoStream_t3531341937::get_offset_of__flushedFinalBlock_7(),
	CryptoStream_t3531341937::get_offset_of__partialCount_8(),
	CryptoStream_t3531341937::get_offset_of__endOfStream_9(),
	CryptoStream_t3531341937::get_offset_of__waitingBlock_10(),
	CryptoStream_t3531341937::get_offset_of__waitingCount_11(),
	CryptoStream_t3531341937::get_offset_of__transformedBlock_12(),
	CryptoStream_t3531341937::get_offset_of__transformedPos_13(),
	CryptoStream_t3531341937::get_offset_of__transformedCount_14(),
	CryptoStream_t3531341937::get_offset_of__workingBlock_15(),
	CryptoStream_t3531341937::get_offset_of__workingCount_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize819 = { sizeof (CryptoStreamMode_t1337713182)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable819[3] = 
{
	CryptoStreamMode_t1337713182::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize820 = { sizeof (CspParameters_t46065560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable820[5] = 
{
	CspParameters_t46065560::get_offset_of__Flags_0(),
	CspParameters_t46065560::get_offset_of_KeyContainerName_1(),
	CspParameters_t46065560::get_offset_of_KeyNumber_2(),
	CspParameters_t46065560::get_offset_of_ProviderName_3(),
	CspParameters_t46065560::get_offset_of_ProviderType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize821 = { sizeof (CspProviderFlags_t105264000)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable821[9] = 
{
	CspProviderFlags_t105264000::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize822 = { sizeof (DES_t1353513560), -1, sizeof(DES_t1353513560_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable822[2] = 
{
	DES_t1353513560_StaticFields::get_offset_of_weakKeys_10(),
	DES_t1353513560_StaticFields::get_offset_of_semiWeakKeys_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize823 = { sizeof (DESTransform_t179750796), -1, sizeof(DESTransform_t179750796_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable823[13] = 
{
	DESTransform_t179750796_StaticFields::get_offset_of_KEY_BIT_SIZE_12(),
	DESTransform_t179750796_StaticFields::get_offset_of_KEY_BYTE_SIZE_13(),
	DESTransform_t179750796_StaticFields::get_offset_of_BLOCK_BIT_SIZE_14(),
	DESTransform_t179750796_StaticFields::get_offset_of_BLOCK_BYTE_SIZE_15(),
	DESTransform_t179750796::get_offset_of_keySchedule_16(),
	DESTransform_t179750796::get_offset_of_byteBuff_17(),
	DESTransform_t179750796::get_offset_of_dwordBuff_18(),
	DESTransform_t179750796_StaticFields::get_offset_of_spBoxes_19(),
	DESTransform_t179750796_StaticFields::get_offset_of_PC1_20(),
	DESTransform_t179750796_StaticFields::get_offset_of_leftRotTotal_21(),
	DESTransform_t179750796_StaticFields::get_offset_of_PC2_22(),
	DESTransform_t179750796_StaticFields::get_offset_of_ipTab_23(),
	DESTransform_t179750796_StaticFields::get_offset_of_fpTab_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize824 = { sizeof (DESCryptoServiceProvider_t933603253), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize825 = { sizeof (DSA_t903174880), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize826 = { sizeof (DSACryptoServiceProvider_t2915171657), -1, sizeof(DSACryptoServiceProvider_t2915171657_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable826[7] = 
{
	DSACryptoServiceProvider_t2915171657::get_offset_of_store_2(),
	DSACryptoServiceProvider_t2915171657::get_offset_of_persistKey_3(),
	DSACryptoServiceProvider_t2915171657::get_offset_of_persisted_4(),
	DSACryptoServiceProvider_t2915171657::get_offset_of_privateKeyExportable_5(),
	DSACryptoServiceProvider_t2915171657::get_offset_of_m_disposed_6(),
	DSACryptoServiceProvider_t2915171657::get_offset_of_dsa_7(),
	DSACryptoServiceProvider_t2915171657_StaticFields::get_offset_of_useMachineKeyStore_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize827 = { sizeof (DSAParameters_t1872138834)+ sizeof (Il2CppObject), sizeof(DSAParameters_t1872138834_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable827[8] = 
{
	DSAParameters_t1872138834::get_offset_of_Counter_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1872138834::get_offset_of_G_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1872138834::get_offset_of_J_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1872138834::get_offset_of_P_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1872138834::get_offset_of_Q_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1872138834::get_offset_of_Seed_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1872138834::get_offset_of_X_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DSAParameters_t1872138834::get_offset_of_Y_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize828 = { sizeof (DSASignatureDeformatter_t2187578719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable828[1] = 
{
	DSASignatureDeformatter_t2187578719::get_offset_of_dsa_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize829 = { sizeof (DSASignatureFormatter_t1065727064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable829[1] = 
{
	DSASignatureFormatter_t1065727064::get_offset_of_dsa_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize830 = { sizeof (FromBase64TransformMode_t2331250892)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable830[3] = 
{
	FromBase64TransformMode_t2331250892::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize831 = { sizeof (FromBase64Transform_t3227039347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable831[5] = 
{
	FromBase64Transform_t3227039347::get_offset_of_mode_0(),
	FromBase64Transform_t3227039347::get_offset_of_accumulator_1(),
	FromBase64Transform_t3227039347::get_offset_of_accPtr_2(),
	FromBase64Transform_t3227039347::get_offset_of_m_disposed_3(),
	FromBase64Transform_t3227039347::get_offset_of_lookupTable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize832 = { sizeof (HashAlgorithm_t2624936259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable832[4] = 
{
	HashAlgorithm_t2624936259::get_offset_of_HashValue_0(),
	HashAlgorithm_t2624936259::get_offset_of_HashSizeValue_1(),
	HashAlgorithm_t2624936259::get_offset_of_State_2(),
	HashAlgorithm_t2624936259::get_offset_of_disposed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize833 = { sizeof (HMAC_t130461695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable833[5] = 
{
	HMAC_t130461695::get_offset_of__disposed_5(),
	HMAC_t130461695::get_offset_of__hashName_6(),
	HMAC_t130461695::get_offset_of__algo_7(),
	HMAC_t130461695::get_offset_of__block_8(),
	HMAC_t130461695::get_offset_of__blockSizeValue_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize834 = { sizeof (HMACMD5_t2214610803), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize835 = { sizeof (HMACRIPEMD160_t131410643), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize836 = { sizeof (HMACSHA1_t1958407246), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize837 = { sizeof (HMACSHA256_t2622794722), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize838 = { sizeof (HMACSHA384_t3026079344), -1, sizeof(HMACSHA384_t3026079344_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable838[2] = 
{
	HMACSHA384_t3026079344_StaticFields::get_offset_of_legacy_mode_10(),
	HMACSHA384_t3026079344::get_offset_of_legacy_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize839 = { sizeof (HMACSHA512_t653426285), -1, sizeof(HMACSHA512_t653426285_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable839[2] = 
{
	HMACSHA512_t653426285_StaticFields::get_offset_of_legacy_mode_10(),
	HMACSHA512_t653426285::get_offset_of_legacy_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize840 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize841 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize842 = { sizeof (KeyedHashAlgorithm_t1374150027), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable842[1] = 
{
	KeyedHashAlgorithm_t1374150027::get_offset_of_KeyValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize843 = { sizeof (KeySizes_t3144736271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable843[3] = 
{
	KeySizes_t3144736271::get_offset_of__maxSize_0(),
	KeySizes_t3144736271::get_offset_of__minSize_1(),
	KeySizes_t3144736271::get_offset_of__skipSize_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize844 = { sizeof (MACTripleDES_t442445873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable844[3] = 
{
	MACTripleDES_t442445873::get_offset_of_tdes_5(),
	MACTripleDES_t442445873::get_offset_of_mac_6(),
	MACTripleDES_t442445873::get_offset_of_m_disposed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize845 = { sizeof (MD5_t1507972490), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize846 = { sizeof (MD5CryptoServiceProvider_t4009738925), -1, sizeof(MD5CryptoServiceProvider_t4009738925_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable846[6] = 
{
	MD5CryptoServiceProvider_t4009738925::get_offset_of__H_4(),
	MD5CryptoServiceProvider_t4009738925::get_offset_of_buff_5(),
	MD5CryptoServiceProvider_t4009738925::get_offset_of_count_6(),
	MD5CryptoServiceProvider_t4009738925::get_offset_of__ProcessingBuffer_7(),
	MD5CryptoServiceProvider_t4009738925::get_offset_of__ProcessingBufferCount_8(),
	MD5CryptoServiceProvider_t4009738925_StaticFields::get_offset_of_K_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize847 = { sizeof (PaddingMode_t3032142640)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable847[6] = 
{
	PaddingMode_t3032142640::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize848 = { sizeof (RandomNumberGenerator_t2510243513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize849 = { sizeof (RC2_t3410342145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable849[1] = 
{
	RC2_t3410342145::get_offset_of_EffectiveKeySizeValue_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize850 = { sizeof (RC2CryptoServiceProvider_t663781682), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize851 = { sizeof (RC2Transform_t93681661), -1, sizeof(RC2Transform_t93681661_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable851[7] = 
{
	RC2Transform_t93681661::get_offset_of_R0_12(),
	RC2Transform_t93681661::get_offset_of_R1_13(),
	RC2Transform_t93681661::get_offset_of_R2_14(),
	RC2Transform_t93681661::get_offset_of_R3_15(),
	RC2Transform_t93681661::get_offset_of_K_16(),
	RC2Transform_t93681661::get_offset_of_j_17(),
	RC2Transform_t93681661_StaticFields::get_offset_of_pitable_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize852 = { sizeof (Rijndael_t2154803531), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize853 = { sizeof (RijndaelManaged_t1034060848), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize854 = { sizeof (RijndaelTransform_t1067822295), -1, sizeof(RijndaelTransform_t1067822295_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable854[15] = 
{
	RijndaelTransform_t1067822295::get_offset_of_expandedKey_12(),
	RijndaelTransform_t1067822295::get_offset_of_Nb_13(),
	RijndaelTransform_t1067822295::get_offset_of_Nk_14(),
	RijndaelTransform_t1067822295::get_offset_of_Nr_15(),
	RijndaelTransform_t1067822295_StaticFields::get_offset_of_Rcon_16(),
	RijndaelTransform_t1067822295_StaticFields::get_offset_of_SBox_17(),
	RijndaelTransform_t1067822295_StaticFields::get_offset_of_iSBox_18(),
	RijndaelTransform_t1067822295_StaticFields::get_offset_of_T0_19(),
	RijndaelTransform_t1067822295_StaticFields::get_offset_of_T1_20(),
	RijndaelTransform_t1067822295_StaticFields::get_offset_of_T2_21(),
	RijndaelTransform_t1067822295_StaticFields::get_offset_of_T3_22(),
	RijndaelTransform_t1067822295_StaticFields::get_offset_of_iT0_23(),
	RijndaelTransform_t1067822295_StaticFields::get_offset_of_iT1_24(),
	RijndaelTransform_t1067822295_StaticFields::get_offset_of_iT2_25(),
	RijndaelTransform_t1067822295_StaticFields::get_offset_of_iT3_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize855 = { sizeof (RijndaelManagedTransform_t135163252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable855[2] = 
{
	RijndaelManagedTransform_t135163252::get_offset_of__st_0(),
	RijndaelManagedTransform_t135163252::get_offset_of__bs_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize856 = { sizeof (RIPEMD160_t1732039966), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize857 = { sizeof (RIPEMD160Managed_t1613307429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable857[5] = 
{
	RIPEMD160Managed_t1613307429::get_offset_of__ProcessingBuffer_4(),
	RIPEMD160Managed_t1613307429::get_offset_of__X_5(),
	RIPEMD160Managed_t1613307429::get_offset_of__HashValue_6(),
	RIPEMD160Managed_t1613307429::get_offset_of__Length_7(),
	RIPEMD160Managed_t1613307429::get_offset_of__ProcessingBufferCount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize858 = { sizeof (RNGCryptoServiceProvider_t2688843926), -1, sizeof(RNGCryptoServiceProvider_t2688843926_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable858[2] = 
{
	RNGCryptoServiceProvider_t2688843926_StaticFields::get_offset_of__lock_0(),
	RNGCryptoServiceProvider_t2688843926::get_offset_of__handle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize859 = { sizeof (RSA_t3719518354), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize860 = { sizeof (RSACryptoServiceProvider_t4229286967), -1, sizeof(RSACryptoServiceProvider_t4229286967_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable860[8] = 
{
	RSACryptoServiceProvider_t4229286967::get_offset_of_store_2(),
	RSACryptoServiceProvider_t4229286967::get_offset_of_persistKey_3(),
	RSACryptoServiceProvider_t4229286967::get_offset_of_persisted_4(),
	RSACryptoServiceProvider_t4229286967::get_offset_of_privateKeyExportable_5(),
	RSACryptoServiceProvider_t4229286967::get_offset_of_m_disposed_6(),
	RSACryptoServiceProvider_t4229286967::get_offset_of_rsa_7(),
	RSACryptoServiceProvider_t4229286967_StaticFields::get_offset_of_useMachineKeyStore_8(),
	RSACryptoServiceProvider_t4229286967_StaticFields::get_offset_of_U3CU3Ef__switchU24map2D_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize861 = { sizeof (RSAParameters_t1462703416)+ sizeof (Il2CppObject), sizeof(RSAParameters_t1462703416_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable861[8] = 
{
	RSAParameters_t1462703416::get_offset_of_P_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1462703416::get_offset_of_Q_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1462703416::get_offset_of_D_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1462703416::get_offset_of_DP_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1462703416::get_offset_of_DQ_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1462703416::get_offset_of_InverseQ_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1462703416::get_offset_of_Modulus_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RSAParameters_t1462703416::get_offset_of_Exponent_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize862 = { sizeof (RSAPKCS1KeyExchangeFormatter_t4167037264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable862[2] = 
{
	RSAPKCS1KeyExchangeFormatter_t4167037264::get_offset_of_rsa_0(),
	RSAPKCS1KeyExchangeFormatter_t4167037264::get_offset_of_random_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize863 = { sizeof (RSAPKCS1SignatureDeformatter_t145198701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable863[2] = 
{
	RSAPKCS1SignatureDeformatter_t145198701::get_offset_of_rsa_0(),
	RSAPKCS1SignatureDeformatter_t145198701::get_offset_of_hashName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize864 = { sizeof (RSAPKCS1SignatureFormatter_t925064184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable864[2] = 
{
	RSAPKCS1SignatureFormatter_t925064184::get_offset_of_rsa_0(),
	RSAPKCS1SignatureFormatter_t925064184::get_offset_of_hash_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize865 = { sizeof (SHA1_t3336793149), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize866 = { sizeof (SHA1Internal_t3873507626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable866[5] = 
{
	SHA1Internal_t3873507626::get_offset_of__H_0(),
	SHA1Internal_t3873507626::get_offset_of_count_1(),
	SHA1Internal_t3873507626::get_offset_of__ProcessingBuffer_2(),
	SHA1Internal_t3873507626::get_offset_of__ProcessingBufferCount_3(),
	SHA1Internal_t3873507626::get_offset_of_buff_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize867 = { sizeof (SHA1CryptoServiceProvider_t3913997830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable867[1] = 
{
	SHA1CryptoServiceProvider_t3913997830::get_offset_of_sha_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize868 = { sizeof (SHA1Managed_t7268864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable868[1] = 
{
	SHA1Managed_t7268864::get_offset_of_sha_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize869 = { sizeof (SHA256_t582564463), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize870 = { sizeof (SHA256Managed_t2029745292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable870[5] = 
{
	SHA256Managed_t2029745292::get_offset_of__H_4(),
	SHA256Managed_t2029745292::get_offset_of_count_5(),
	SHA256Managed_t2029745292::get_offset_of__ProcessingBuffer_6(),
	SHA256Managed_t2029745292::get_offset_of__ProcessingBufferCount_7(),
	SHA256Managed_t2029745292::get_offset_of_buff_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize871 = { sizeof (SHA384_t535510267), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize872 = { sizeof (SHA384Managed_t741627254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable872[14] = 
{
	SHA384Managed_t741627254::get_offset_of_xBuf_4(),
	SHA384Managed_t741627254::get_offset_of_xBufOff_5(),
	SHA384Managed_t741627254::get_offset_of_byteCount1_6(),
	SHA384Managed_t741627254::get_offset_of_byteCount2_7(),
	SHA384Managed_t741627254::get_offset_of_H1_8(),
	SHA384Managed_t741627254::get_offset_of_H2_9(),
	SHA384Managed_t741627254::get_offset_of_H3_10(),
	SHA384Managed_t741627254::get_offset_of_H4_11(),
	SHA384Managed_t741627254::get_offset_of_H5_12(),
	SHA384Managed_t741627254::get_offset_of_H6_13(),
	SHA384Managed_t741627254::get_offset_of_H7_14(),
	SHA384Managed_t741627254::get_offset_of_H8_15(),
	SHA384Managed_t741627254::get_offset_of_W_16(),
	SHA384Managed_t741627254::get_offset_of_wOff_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize873 = { sizeof (SHA512_t2908163326), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize874 = { sizeof (SHA512Managed_t3949709369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable874[14] = 
{
	SHA512Managed_t3949709369::get_offset_of_xBuf_4(),
	SHA512Managed_t3949709369::get_offset_of_xBufOff_5(),
	SHA512Managed_t3949709369::get_offset_of_byteCount1_6(),
	SHA512Managed_t3949709369::get_offset_of_byteCount2_7(),
	SHA512Managed_t3949709369::get_offset_of_H1_8(),
	SHA512Managed_t3949709369::get_offset_of_H2_9(),
	SHA512Managed_t3949709369::get_offset_of_H3_10(),
	SHA512Managed_t3949709369::get_offset_of_H4_11(),
	SHA512Managed_t3949709369::get_offset_of_H5_12(),
	SHA512Managed_t3949709369::get_offset_of_H6_13(),
	SHA512Managed_t3949709369::get_offset_of_H7_14(),
	SHA512Managed_t3949709369::get_offset_of_H8_15(),
	SHA512Managed_t3949709369::get_offset_of_W_16(),
	SHA512Managed_t3949709369::get_offset_of_wOff_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize875 = { sizeof (SHAConstants_t3712871299), -1, sizeof(SHAConstants_t3712871299_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable875[2] = 
{
	SHAConstants_t3712871299_StaticFields::get_offset_of_K1_0(),
	SHAConstants_t3712871299_StaticFields::get_offset_of_K2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize876 = { sizeof (SignatureDescription_t89145500), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable876[4] = 
{
	SignatureDescription_t89145500::get_offset_of__DeformatterAlgorithm_0(),
	SignatureDescription_t89145500::get_offset_of__DigestAlgorithm_1(),
	SignatureDescription_t89145500::get_offset_of__FormatterAlgorithm_2(),
	SignatureDescription_t89145500::get_offset_of__KeyAlgorithm_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize877 = { sizeof (DSASignatureDescription_t1998527418), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize878 = { sizeof (RSAPKCS1SHA1SignatureDescription_t2477284625), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize879 = { sizeof (SymmetricAlgorithm_t1108166522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable879[10] = 
{
	SymmetricAlgorithm_t1108166522::get_offset_of_BlockSizeValue_0(),
	SymmetricAlgorithm_t1108166522::get_offset_of_IVValue_1(),
	SymmetricAlgorithm_t1108166522::get_offset_of_KeySizeValue_2(),
	SymmetricAlgorithm_t1108166522::get_offset_of_KeyValue_3(),
	SymmetricAlgorithm_t1108166522::get_offset_of_LegalBlockSizesValue_4(),
	SymmetricAlgorithm_t1108166522::get_offset_of_LegalKeySizesValue_5(),
	SymmetricAlgorithm_t1108166522::get_offset_of_FeedbackSizeValue_6(),
	SymmetricAlgorithm_t1108166522::get_offset_of_ModeValue_7(),
	SymmetricAlgorithm_t1108166522::get_offset_of_PaddingValue_8(),
	SymmetricAlgorithm_t1108166522::get_offset_of_m_disposed_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize880 = { sizeof (ToBase64Transform_t625739466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable880[1] = 
{
	ToBase64Transform_t625739466::get_offset_of_m_disposed_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize881 = { sizeof (TripleDES_t243950698), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize882 = { sizeof (TripleDESCryptoServiceProvider_t2380467305), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize883 = { sizeof (TripleDESTransform_t2384411462), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable883[6] = 
{
	TripleDESTransform_t2384411462::get_offset_of_E1_12(),
	TripleDESTransform_t2384411462::get_offset_of_D2_13(),
	TripleDESTransform_t2384411462::get_offset_of_E3_14(),
	TripleDESTransform_t2384411462::get_offset_of_D1_15(),
	TripleDESTransform_t2384411462::get_offset_of_E2_16(),
	TripleDESTransform_t2384411462::get_offset_of_D3_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize884 = { sizeof (X509Certificate_t283079845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable884[5] = 
{
	X509Certificate_t283079845::get_offset_of_x509_0(),
	X509Certificate_t283079845::get_offset_of_hideDates_1(),
	X509Certificate_t283079845::get_offset_of_cachedCertificateHash_2(),
	X509Certificate_t283079845::get_offset_of_issuer_name_3(),
	X509Certificate_t283079845::get_offset_of_subject_name_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize885 = { sizeof (X509KeyStorageFlags_t1216946873)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable885[7] = 
{
	X509KeyStorageFlags_t1216946873::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize886 = { sizeof (EnvironmentPermission_t935902940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable886[3] = 
{
	EnvironmentPermission_t935902940::get_offset_of__state_0(),
	EnvironmentPermission_t935902940::get_offset_of_readList_1(),
	EnvironmentPermission_t935902940::get_offset_of_writeList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize887 = { sizeof (EnvironmentPermissionAccess_t3182667772)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable887[5] = 
{
	EnvironmentPermissionAccess_t3182667772::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize888 = { sizeof (FileDialogPermission_t3088110531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable888[1] = 
{
	FileDialogPermission_t3088110531::get_offset_of__access_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize889 = { sizeof (FileDialogPermissionAccess_t4165817673)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable889[5] = 
{
	FileDialogPermissionAccess_t4165817673::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize890 = { sizeof (FileIOPermission_t3702443043), -1, sizeof(FileIOPermission_t3702443043_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable890[9] = 
{
	FileIOPermission_t3702443043_StaticFields::get_offset_of_BadPathNameCharacters_0(),
	FileIOPermission_t3702443043_StaticFields::get_offset_of_BadFileNameCharacters_1(),
	FileIOPermission_t3702443043::get_offset_of_m_Unrestricted_2(),
	FileIOPermission_t3702443043::get_offset_of_m_AllFilesAccess_3(),
	FileIOPermission_t3702443043::get_offset_of_m_AllLocalFilesAccess_4(),
	FileIOPermission_t3702443043::get_offset_of_readList_5(),
	FileIOPermission_t3702443043::get_offset_of_writeList_6(),
	FileIOPermission_t3702443043::get_offset_of_appendList_7(),
	FileIOPermission_t3702443043::get_offset_of_pathList_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize891 = { sizeof (FileIOPermissionAccess_t1079931101)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable891[7] = 
{
	FileIOPermissionAccess_t1079931101::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize892 = { sizeof (GacIdentityPermission_t944690662), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize893 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize894 = { sizeof (IsolatedStorageContainment_t174977552)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable894[13] = 
{
	IsolatedStorageContainment_t174977552::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize895 = { sizeof (IsolatedStorageFilePermission_t3424948193), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize896 = { sizeof (IsolatedStoragePermission_t3796409103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable896[5] = 
{
	IsolatedStoragePermission_t3796409103::get_offset_of_m_userQuota_0(),
	IsolatedStoragePermission_t3796409103::get_offset_of_m_machineQuota_1(),
	IsolatedStoragePermission_t3796409103::get_offset_of_m_expirationDays_2(),
	IsolatedStoragePermission_t3796409103::get_offset_of_m_permanentData_3(),
	IsolatedStoragePermission_t3796409103::get_offset_of_m_allowed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize897 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize898 = { sizeof (KeyContainerPermission_t3643383499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable898[2] = 
{
	KeyContainerPermission_t3643383499::get_offset_of__accessEntries_0(),
	KeyContainerPermission_t3643383499::get_offset_of__flags_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize899 = { sizeof (KeyContainerPermissionAccessEntry_t41069825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable899[6] = 
{
	KeyContainerPermissionAccessEntry_t41069825::get_offset_of__flags_0(),
	KeyContainerPermissionAccessEntry_t41069825::get_offset_of__containerName_1(),
	KeyContainerPermissionAccessEntry_t41069825::get_offset_of__spec_2(),
	KeyContainerPermissionAccessEntry_t41069825::get_offset_of__store_3(),
	KeyContainerPermissionAccessEntry_t41069825::get_offset_of__providerName_4(),
	KeyContainerPermissionAccessEntry_t41069825::get_offset_of__type_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
