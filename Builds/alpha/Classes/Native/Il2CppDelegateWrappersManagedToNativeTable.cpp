﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t3898244613 ();
extern "C" void DelegatePInvokeWrapper_Swapper_t2637371637 ();
extern "C" void DelegatePInvokeWrapper_InternalCancelHandler_t2895223116 ();
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t3184826381 ();
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t489908132 ();
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t754146990 ();
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t362827733 ();
extern "C" void DelegatePInvokeWrapper_ThreadStart_t3437517264 ();
extern "C" void DelegatePInvokeWrapper_ReadMethod_t3362229488 ();
extern "C" void DelegatePInvokeWrapper_UnmanagedReadOrWrite_t1990215745 ();
extern "C" void DelegatePInvokeWrapper_WriteMethod_t1894833619 ();
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t1559754630 ();
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t888270799 ();
extern "C" void DelegatePInvokeWrapper_SocketAsyncCall_t3737776727 ();
extern "C" void DelegatePInvokeWrapper_CostDelegate_t1824458113 ();
extern "C" void DelegatePInvokeWrapper_CharGetter_t1955031820 ();
extern "C" void DelegatePInvokeWrapper_Action_t3226471752 ();
extern "C" void DelegatePInvokeWrapper_LogCallback_t1867914413 ();
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t3007145346 ();
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t421863554 ();
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t3743753033 ();
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t3522132132 ();
extern "C" void DelegatePInvokeWrapper_StateChanged_t2480912210 ();
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t3423469815 ();
extern "C" void DelegatePInvokeWrapper_UnityAction_t4025899511 ();
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t1272078033 ();
extern "C" void DelegatePInvokeWrapper_WindowFunction_t3486805455 ();
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t3594822336 ();
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t3033456180 ();
extern "C" void DelegatePInvokeWrapper_ExceptionArgumentDelegate_t2443153790 ();
extern "C" void DelegatePInvokeWrapper_ExceptionDelegate_t2876249339 ();
extern "C" void DelegatePInvokeWrapper_SWIGStringDelegate_t18001273 ();
extern "C" void DelegatePInvokeWrapper_DestroyDelegate_t3635929227 ();
extern "C" void DelegatePInvokeWrapper_LogMessageDelegate_t1988210674 ();
extern "C" void DelegatePInvokeWrapper_Action_t3062064994 ();
extern "C" void DelegatePInvokeWrapper_SWIG_CompletionDelegate_t1819656562 ();
extern "C" void DelegatePInvokeWrapper_DispatcherFactory_t1307565918 ();
extern "C" void DelegatePInvokeWrapper_ExceptionArgumentDelegate_t1735378451 ();
extern "C" void DelegatePInvokeWrapper_ExceptionDelegate_t4033223266 ();
extern "C" void DelegatePInvokeWrapper_SWIGStringDelegate_t553428412 ();
extern "C" void DelegatePInvokeWrapper_StateChangedDelegate_t3080659151 ();
extern "C" void DelegatePInvokeWrapper_Action_t1614918345 ();
extern "C" void DelegatePInvokeWrapper_SWIG_CompletionDelegate_t2830873745 ();
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t1946318473 ();
extern const Il2CppMethodPointer g_DelegateWrappersManagedToNative[44] = 
{
	DelegatePInvokeWrapper_AppDomainInitializer_t3898244613,
	DelegatePInvokeWrapper_Swapper_t2637371637,
	DelegatePInvokeWrapper_InternalCancelHandler_t2895223116,
	DelegatePInvokeWrapper_ReadDelegate_t3184826381,
	DelegatePInvokeWrapper_WriteDelegate_t489908132,
	DelegatePInvokeWrapper_CrossContextDelegate_t754146990,
	DelegatePInvokeWrapper_CallbackHandler_t362827733,
	DelegatePInvokeWrapper_ThreadStart_t3437517264,
	DelegatePInvokeWrapper_ReadMethod_t3362229488,
	DelegatePInvokeWrapper_UnmanagedReadOrWrite_t1990215745,
	DelegatePInvokeWrapper_WriteMethod_t1894833619,
	DelegatePInvokeWrapper_ReadDelegate_t1559754630,
	DelegatePInvokeWrapper_WriteDelegate_t888270799,
	DelegatePInvokeWrapper_SocketAsyncCall_t3737776727,
	DelegatePInvokeWrapper_CostDelegate_t1824458113,
	DelegatePInvokeWrapper_CharGetter_t1955031820,
	DelegatePInvokeWrapper_Action_t3226471752,
	DelegatePInvokeWrapper_LogCallback_t1867914413,
	DelegatePInvokeWrapper_PCMReaderCallback_t3007145346,
	DelegatePInvokeWrapper_PCMSetPositionCallback_t421863554,
	DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t3743753033,
	DelegatePInvokeWrapper_WillRenderCanvases_t3522132132,
	DelegatePInvokeWrapper_StateChanged_t2480912210,
	DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t3423469815,
	DelegatePInvokeWrapper_UnityAction_t4025899511,
	DelegatePInvokeWrapper_FontTextureRebuildCallback_t1272078033,
	DelegatePInvokeWrapper_WindowFunction_t3486805455,
	DelegatePInvokeWrapper_SkinChangedDelegate_t3594822336,
	DelegatePInvokeWrapper_UpdatedEventHandler_t3033456180,
	DelegatePInvokeWrapper_ExceptionArgumentDelegate_t2443153790,
	DelegatePInvokeWrapper_ExceptionDelegate_t2876249339,
	DelegatePInvokeWrapper_SWIGStringDelegate_t18001273,
	DelegatePInvokeWrapper_DestroyDelegate_t3635929227,
	DelegatePInvokeWrapper_LogMessageDelegate_t1988210674,
	DelegatePInvokeWrapper_Action_t3062064994,
	DelegatePInvokeWrapper_SWIG_CompletionDelegate_t1819656562,
	DelegatePInvokeWrapper_DispatcherFactory_t1307565918,
	DelegatePInvokeWrapper_ExceptionArgumentDelegate_t1735378451,
	DelegatePInvokeWrapper_ExceptionDelegate_t4033223266,
	DelegatePInvokeWrapper_SWIGStringDelegate_t553428412,
	DelegatePInvokeWrapper_StateChangedDelegate_t3080659151,
	DelegatePInvokeWrapper_Action_t1614918345,
	DelegatePInvokeWrapper_SWIG_CompletionDelegate_t2830873745,
	DelegatePInvokeWrapper_OnValidateInput_t1946318473,
};
