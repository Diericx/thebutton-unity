﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern const Il2CppType Il2CppObject_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0 = { 1, GenInst_Il2CppObject_0_0_0_Types };
extern const Il2CppType Int32_t2071877448_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0 = { 1, GenInst_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType Char_t3454481338_0_0_0;
static const Il2CppType* GenInst_Char_t3454481338_0_0_0_Types[] = { &Char_t3454481338_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0 = { 1, GenInst_Char_t3454481338_0_0_0_Types };
extern const Il2CppType IConvertible_t908092482_0_0_0;
static const Il2CppType* GenInst_IConvertible_t908092482_0_0_0_Types[] = { &IConvertible_t908092482_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t908092482_0_0_0 = { 1, GenInst_IConvertible_t908092482_0_0_0_Types };
extern const Il2CppType IComparable_t1857082765_0_0_0;
static const Il2CppType* GenInst_IComparable_t1857082765_0_0_0_Types[] = { &IComparable_t1857082765_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t1857082765_0_0_0 = { 1, GenInst_IComparable_t1857082765_0_0_0_Types };
extern const Il2CppType IComparable_1_t991353265_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t991353265_0_0_0_Types[] = { &IComparable_1_t991353265_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t991353265_0_0_0 = { 1, GenInst_IComparable_1_t991353265_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1363496211_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1363496211_0_0_0_Types[] = { &IEquatable_1_t1363496211_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1363496211_0_0_0 = { 1, GenInst_IEquatable_1_t1363496211_0_0_0_Types };
extern const Il2CppType ValueType_t3507792607_0_0_0;
static const Il2CppType* GenInst_ValueType_t3507792607_0_0_0_Types[] = { &ValueType_t3507792607_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t3507792607_0_0_0 = { 1, GenInst_ValueType_t3507792607_0_0_0_Types };
extern const Il2CppType Int64_t909078037_0_0_0;
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Types[] = { &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0 = { 1, GenInst_Int64_t909078037_0_0_0_Types };
extern const Il2CppType UInt32_t2149682021_0_0_0;
static const Il2CppType* GenInst_UInt32_t2149682021_0_0_0_Types[] = { &UInt32_t2149682021_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t2149682021_0_0_0 = { 1, GenInst_UInt32_t2149682021_0_0_0_Types };
extern const Il2CppType UInt64_t2909196914_0_0_0;
static const Il2CppType* GenInst_UInt64_t2909196914_0_0_0_Types[] = { &UInt64_t2909196914_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0 = { 1, GenInst_UInt64_t2909196914_0_0_0_Types };
extern const Il2CppType Byte_t3683104436_0_0_0;
static const Il2CppType* GenInst_Byte_t3683104436_0_0_0_Types[] = { &Byte_t3683104436_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t3683104436_0_0_0 = { 1, GenInst_Byte_t3683104436_0_0_0_Types };
extern const Il2CppType SByte_t454417549_0_0_0;
static const Il2CppType* GenInst_SByte_t454417549_0_0_0_Types[] = { &SByte_t454417549_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t454417549_0_0_0 = { 1, GenInst_SByte_t454417549_0_0_0_Types };
extern const Il2CppType Int16_t4041245914_0_0_0;
static const Il2CppType* GenInst_Int16_t4041245914_0_0_0_Types[] = { &Int16_t4041245914_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t4041245914_0_0_0 = { 1, GenInst_Int16_t4041245914_0_0_0_Types };
extern const Il2CppType UInt16_t986882611_0_0_0;
static const Il2CppType* GenInst_UInt16_t986882611_0_0_0_Types[] = { &UInt16_t986882611_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t986882611_0_0_0 = { 1, GenInst_UInt16_t986882611_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType IEnumerable_t2911409499_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t2911409499_0_0_0_Types[] = { &IEnumerable_t2911409499_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t2911409499_0_0_0 = { 1, GenInst_IEnumerable_t2911409499_0_0_0_Types };
extern const Il2CppType ICloneable_t3853279282_0_0_0;
static const Il2CppType* GenInst_ICloneable_t3853279282_0_0_0_Types[] = { &ICloneable_t3853279282_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t3853279282_0_0_0 = { 1, GenInst_ICloneable_t3853279282_0_0_0_Types };
extern const Il2CppType IComparable_1_t3861059456_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3861059456_0_0_0_Types[] = { &IComparable_1_t3861059456_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3861059456_0_0_0 = { 1, GenInst_IComparable_1_t3861059456_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4233202402_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4233202402_0_0_0_Types[] = { &IEquatable_1_t4233202402_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4233202402_0_0_0 = { 1, GenInst_IEquatable_1_t4233202402_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType IReflect_t3412036974_0_0_0;
static const Il2CppType* GenInst_IReflect_t3412036974_0_0_0_Types[] = { &IReflect_t3412036974_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t3412036974_0_0_0 = { 1, GenInst_IReflect_t3412036974_0_0_0_Types };
extern const Il2CppType _Type_t102776839_0_0_0;
static const Il2CppType* GenInst__Type_t102776839_0_0_0_Types[] = { &_Type_t102776839_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t102776839_0_0_0 = { 1, GenInst__Type_t102776839_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t502202687_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_Types[] = { &ICustomAttributeProvider_t502202687_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t502202687_0_0_0_Types };
extern const Il2CppType _MemberInfo_t332722161_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t332722161_0_0_0_Types[] = { &_MemberInfo_t332722161_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t332722161_0_0_0 = { 1, GenInst__MemberInfo_t332722161_0_0_0_Types };
extern const Il2CppType IFormattable_t1523031934_0_0_0;
static const Il2CppType* GenInst_IFormattable_t1523031934_0_0_0_Types[] = { &IFormattable_t1523031934_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t1523031934_0_0_0 = { 1, GenInst_IFormattable_t1523031934_0_0_0_Types };
extern const Il2CppType IComparable_1_t3903716671_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3903716671_0_0_0_Types[] = { &IComparable_1_t3903716671_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3903716671_0_0_0 = { 1, GenInst_IComparable_1_t3903716671_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4275859617_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4275859617_0_0_0_Types[] = { &IEquatable_1_t4275859617_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4275859617_0_0_0 = { 1, GenInst_IEquatable_1_t4275859617_0_0_0_Types };
extern const Il2CppType Double_t4078015681_0_0_0;
static const Il2CppType* GenInst_Double_t4078015681_0_0_0_Types[] = { &Double_t4078015681_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t4078015681_0_0_0 = { 1, GenInst_Double_t4078015681_0_0_0_Types };
extern const Il2CppType IComparable_1_t1614887608_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1614887608_0_0_0_Types[] = { &IComparable_1_t1614887608_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1614887608_0_0_0 = { 1, GenInst_IComparable_1_t1614887608_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1987030554_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1987030554_0_0_0_Types[] = { &IEquatable_1_t1987030554_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1987030554_0_0_0 = { 1, GenInst_IEquatable_1_t1987030554_0_0_0_Types };
extern const Il2CppType IComparable_1_t3981521244_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3981521244_0_0_0_Types[] = { &IComparable_1_t3981521244_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3981521244_0_0_0 = { 1, GenInst_IComparable_1_t3981521244_0_0_0_Types };
extern const Il2CppType IEquatable_1_t58696894_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t58696894_0_0_0_Types[] = { &IEquatable_1_t58696894_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t58696894_0_0_0 = { 1, GenInst_IEquatable_1_t58696894_0_0_0_Types };
extern const Il2CppType IComparable_1_t1219976363_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1219976363_0_0_0_Types[] = { &IComparable_1_t1219976363_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1219976363_0_0_0 = { 1, GenInst_IComparable_1_t1219976363_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1592119309_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1592119309_0_0_0_Types[] = { &IEquatable_1_t1592119309_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1592119309_0_0_0 = { 1, GenInst_IEquatable_1_t1592119309_0_0_0_Types };
extern const Il2CppType Single_t2076509932_0_0_0;
static const Il2CppType* GenInst_Single_t2076509932_0_0_0_Types[] = { &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0 = { 1, GenInst_Single_t2076509932_0_0_0_Types };
extern const Il2CppType IComparable_1_t3908349155_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3908349155_0_0_0_Types[] = { &IComparable_1_t3908349155_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3908349155_0_0_0 = { 1, GenInst_IComparable_1_t3908349155_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4280492101_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4280492101_0_0_0_Types[] = { &IEquatable_1_t4280492101_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4280492101_0_0_0 = { 1, GenInst_IEquatable_1_t4280492101_0_0_0_Types };
extern const Il2CppType Decimal_t724701077_0_0_0;
static const Il2CppType* GenInst_Decimal_t724701077_0_0_0_Types[] = { &Decimal_t724701077_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t724701077_0_0_0 = { 1, GenInst_Decimal_t724701077_0_0_0_Types };
extern const Il2CppType Boolean_t3825574718_0_0_0;
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0 = { 1, GenInst_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Delegate_t3022476291_0_0_0;
static const Il2CppType* GenInst_Delegate_t3022476291_0_0_0_Types[] = { &Delegate_t3022476291_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t3022476291_0_0_0 = { 1, GenInst_Delegate_t3022476291_0_0_0_Types };
extern const Il2CppType ISerializable_t1245643778_0_0_0;
static const Il2CppType* GenInst_ISerializable_t1245643778_0_0_0_Types[] = { &ISerializable_t1245643778_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t1245643778_0_0_0 = { 1, GenInst_ISerializable_t1245643778_0_0_0_Types };
extern const Il2CppType ParameterInfo_t2249040075_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t2249040075_0_0_0_Types[] = { &ParameterInfo_t2249040075_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2249040075_0_0_0 = { 1, GenInst_ParameterInfo_t2249040075_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t470209990_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t470209990_0_0_0_Types[] = { &_ParameterInfo_t470209990_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t470209990_0_0_0 = { 1, GenInst__ParameterInfo_t470209990_0_0_0_Types };
extern const Il2CppType ParameterModifier_t1820634920_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t1820634920_0_0_0_Types[] = { &ParameterModifier_t1820634920_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1820634920_0_0_0 = { 1, GenInst_ParameterModifier_t1820634920_0_0_0_Types };
extern const Il2CppType IComparable_1_t2818721834_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2818721834_0_0_0_Types[] = { &IComparable_1_t2818721834_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2818721834_0_0_0 = { 1, GenInst_IComparable_1_t2818721834_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3190864780_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3190864780_0_0_0_Types[] = { &IEquatable_1_t3190864780_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3190864780_0_0_0 = { 1, GenInst_IEquatable_1_t3190864780_0_0_0_Types };
extern const Il2CppType IComparable_1_t446068841_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t446068841_0_0_0_Types[] = { &IComparable_1_t446068841_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t446068841_0_0_0 = { 1, GenInst_IComparable_1_t446068841_0_0_0_Types };
extern const Il2CppType IEquatable_1_t818211787_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t818211787_0_0_0_Types[] = { &IEquatable_1_t818211787_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t818211787_0_0_0 = { 1, GenInst_IEquatable_1_t818211787_0_0_0_Types };
extern const Il2CppType IComparable_1_t1578117841_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1578117841_0_0_0_Types[] = { &IComparable_1_t1578117841_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1578117841_0_0_0 = { 1, GenInst_IComparable_1_t1578117841_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1950260787_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1950260787_0_0_0_Types[] = { &IEquatable_1_t1950260787_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1950260787_0_0_0 = { 1, GenInst_IEquatable_1_t1950260787_0_0_0_Types };
extern const Il2CppType IComparable_1_t2286256772_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2286256772_0_0_0_Types[] = { &IComparable_1_t2286256772_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2286256772_0_0_0 = { 1, GenInst_IComparable_1_t2286256772_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2658399718_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2658399718_0_0_0_Types[] = { &IEquatable_1_t2658399718_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2658399718_0_0_0 = { 1, GenInst_IEquatable_1_t2658399718_0_0_0_Types };
extern const Il2CppType IComparable_1_t2740917260_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2740917260_0_0_0_Types[] = { &IComparable_1_t2740917260_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2740917260_0_0_0 = { 1, GenInst_IComparable_1_t2740917260_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3113060206_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3113060206_0_0_0_Types[] = { &IEquatable_1_t3113060206_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3113060206_0_0_0 = { 1, GenInst_IEquatable_1_t3113060206_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType EventInfo_t_0_0_0;
static const Il2CppType* GenInst_EventInfo_t_0_0_0_Types[] = { &EventInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_EventInfo_t_0_0_0 = { 1, GenInst_EventInfo_t_0_0_0_Types };
extern const Il2CppType _EventInfo_t2430923913_0_0_0;
static const Il2CppType* GenInst__EventInfo_t2430923913_0_0_0_Types[] = { &_EventInfo_t2430923913_0_0_0 };
extern const Il2CppGenericInst GenInst__EventInfo_t2430923913_0_0_0 = { 1, GenInst__EventInfo_t2430923913_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t2511231167_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t2511231167_0_0_0_Types[] = { &_FieldInfo_t2511231167_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t2511231167_0_0_0 = { 1, GenInst__FieldInfo_t2511231167_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType _MethodInfo_t3642518830_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t3642518830_0_0_0_Types[] = { &_MethodInfo_t3642518830_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t3642518830_0_0_0 = { 1, GenInst__MethodInfo_t3642518830_0_0_0_Types };
extern const Il2CppType MethodBase_t904190842_0_0_0;
static const Il2CppType* GenInst_MethodBase_t904190842_0_0_0_Types[] = { &MethodBase_t904190842_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t904190842_0_0_0 = { 1, GenInst_MethodBase_t904190842_0_0_0_Types };
extern const Il2CppType _MethodBase_t1935530873_0_0_0;
static const Il2CppType* GenInst__MethodBase_t1935530873_0_0_0_Types[] = { &_MethodBase_t1935530873_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t1935530873_0_0_0 = { 1, GenInst__MethodBase_t1935530873_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t1567586598_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t1567586598_0_0_0_Types[] = { &_PropertyInfo_t1567586598_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t1567586598_0_0_0 = { 1, GenInst__PropertyInfo_t1567586598_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t2851816542_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t2851816542_0_0_0_Types[] = { &ConstructorInfo_t2851816542_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t2851816542_0_0_0 = { 1, GenInst_ConstructorInfo_t2851816542_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t3269099341_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t3269099341_0_0_0_Types[] = { &_ConstructorInfo_t3269099341_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t3269099341_0_0_0 = { 1, GenInst__ConstructorInfo_t3269099341_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3716250094_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0 = { 1, GenInst_KeyValuePair_2_t3716250094_0_0_0_Types };
extern const Il2CppType Link_t2723257478_0_0_0;
static const Il2CppType* GenInst_Link_t2723257478_0_0_0_Types[] = { &Link_t2723257478_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t2723257478_0_0_0 = { 1, GenInst_Link_t2723257478_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t3048875398_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t3048875398_0_0_0_Types[] = { &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0 = { 1, GenInst_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1744001932_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1744001932_0_0_0_Types[] = { &KeyValuePair_2_t1744001932_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1744001932_0_0_0 = { 1, GenInst_KeyValuePair_2_t1744001932_0_0_0_Types };
extern const Il2CppType TableRange_t2011406615_0_0_0;
static const Il2CppType* GenInst_TableRange_t2011406615_0_0_0_Types[] = { &TableRange_t2011406615_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t2011406615_0_0_0 = { 1, GenInst_TableRange_t2011406615_0_0_0_Types };
extern const Il2CppType TailoringInfo_t1449609243_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t1449609243_0_0_0_Types[] = { &TailoringInfo_t1449609243_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1449609243_0_0_0 = { 1, GenInst_TailoringInfo_t1449609243_0_0_0_Types };
extern const Il2CppType Contraction_t1673853792_0_0_0;
static const Il2CppType* GenInst_Contraction_t1673853792_0_0_0_Types[] = { &Contraction_t1673853792_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t1673853792_0_0_0 = { 1, GenInst_Contraction_t1673853792_0_0_0_Types };
extern const Il2CppType Level2Map_t3322505726_0_0_0;
static const Il2CppType* GenInst_Level2Map_t3322505726_0_0_0_Types[] = { &Level2Map_t3322505726_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t3322505726_0_0_0 = { 1, GenInst_Level2Map_t3322505726_0_0_0_Types };
extern const Il2CppType BigInteger_t925946152_0_0_0;
static const Il2CppType* GenInst_BigInteger_t925946152_0_0_0_Types[] = { &BigInteger_t925946152_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t925946152_0_0_0 = { 1, GenInst_BigInteger_t925946152_0_0_0_Types };
extern const Il2CppType UriScheme_t683497865_0_0_0;
static const Il2CppType* GenInst_UriScheme_t683497865_0_0_0_Types[] = { &UriScheme_t683497865_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t683497865_0_0_0 = { 1, GenInst_UriScheme_t683497865_0_0_0_Types };
extern const Il2CppType KeySizes_t3144736271_0_0_0;
static const Il2CppType* GenInst_KeySizes_t3144736271_0_0_0_Types[] = { &KeySizes_t3144736271_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t3144736271_0_0_0 = { 1, GenInst_KeySizes_t3144736271_0_0_0_Types };
extern const Il2CppType Assembly_t4268412390_0_0_0;
static const Il2CppType* GenInst_Assembly_t4268412390_0_0_0_Types[] = { &Assembly_t4268412390_0_0_0 };
extern const Il2CppGenericInst GenInst_Assembly_t4268412390_0_0_0 = { 1, GenInst_Assembly_t4268412390_0_0_0_Types };
extern const Il2CppType _Assembly_t2937922309_0_0_0;
static const Il2CppType* GenInst__Assembly_t2937922309_0_0_0_Types[] = { &_Assembly_t2937922309_0_0_0 };
extern const Il2CppGenericInst GenInst__Assembly_t2937922309_0_0_0 = { 1, GenInst__Assembly_t2937922309_0_0_0_Types };
extern const Il2CppType IEvidenceFactory_t1747265420_0_0_0;
static const Il2CppType* GenInst_IEvidenceFactory_t1747265420_0_0_0_Types[] = { &IEvidenceFactory_t1747265420_0_0_0 };
extern const Il2CppGenericInst GenInst_IEvidenceFactory_t1747265420_0_0_0 = { 1, GenInst_IEvidenceFactory_t1747265420_0_0_0_Types };
extern const Il2CppType DateTime_t693205669_0_0_0;
static const Il2CppType* GenInst_DateTime_t693205669_0_0_0_Types[] = { &DateTime_t693205669_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t693205669_0_0_0 = { 1, GenInst_DateTime_t693205669_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t1362988906_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t1362988906_0_0_0_Types[] = { &DateTimeOffset_t1362988906_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t1362988906_0_0_0 = { 1, GenInst_DateTimeOffset_t1362988906_0_0_0_Types };
extern const Il2CppType TimeSpan_t3430258949_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t3430258949_0_0_0_Types[] = { &TimeSpan_t3430258949_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t3430258949_0_0_0 = { 1, GenInst_TimeSpan_t3430258949_0_0_0_Types };
extern const Il2CppType Guid_t2533601593_0_0_0;
static const Il2CppType* GenInst_Guid_t2533601593_0_0_0_Types[] = { &Guid_t2533601593_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t2533601593_0_0_0 = { 1, GenInst_Guid_t2533601593_0_0_0_Types };
extern const Il2CppType IComparable_1_t1362446645_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1362446645_0_0_0_Types[] = { &IComparable_1_t1362446645_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1362446645_0_0_0 = { 1, GenInst_IComparable_1_t1362446645_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1734589591_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1734589591_0_0_0_Types[] = { &IEquatable_1_t1734589591_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1734589591_0_0_0 = { 1, GenInst_IEquatable_1_t1734589591_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t3093286891_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t3093286891_0_0_0_Types[] = { &CustomAttributeData_t3093286891_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t3093286891_0_0_0 = { 1, GenInst_CustomAttributeData_t3093286891_0_0_0_Types };
extern const Il2CppType TermInfoStrings_t1425267120_0_0_0;
static const Il2CppType* GenInst_TermInfoStrings_t1425267120_0_0_0_Types[] = { &TermInfoStrings_t1425267120_0_0_0 };
extern const Il2CppGenericInst GenInst_TermInfoStrings_t1425267120_0_0_0 = { 1, GenInst_TermInfoStrings_t1425267120_0_0_0_Types };
extern const Il2CppType Enum_t2459695545_0_0_0;
static const Il2CppType* GenInst_Enum_t2459695545_0_0_0_Types[] = { &Enum_t2459695545_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t2459695545_0_0_0 = { 1, GenInst_Enum_t2459695545_0_0_0_Types };
extern const Il2CppType Version_t1755874712_0_0_0;
static const Il2CppType* GenInst_Version_t1755874712_0_0_0_Types[] = { &Version_t1755874712_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t1755874712_0_0_0 = { 1, GenInst_Version_t1755874712_0_0_0_Types };
extern const Il2CppType Slot_t2022531261_0_0_0;
static const Il2CppType* GenInst_Slot_t2022531261_0_0_0_Types[] = { &Slot_t2022531261_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2022531261_0_0_0 = { 1, GenInst_Slot_t2022531261_0_0_0_Types };
extern const Il2CppType Slot_t2267560602_0_0_0;
static const Il2CppType* GenInst_Slot_t2267560602_0_0_0_Types[] = { &Slot_t2267560602_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2267560602_0_0_0 = { 1, GenInst_Slot_t2267560602_0_0_0_Types };
extern const Il2CppType StackFrame_t2050294881_0_0_0;
static const Il2CppType* GenInst_StackFrame_t2050294881_0_0_0_Types[] = { &StackFrame_t2050294881_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t2050294881_0_0_0 = { 1, GenInst_StackFrame_t2050294881_0_0_0_Types };
extern const Il2CppType Calendar_t585061108_0_0_0;
static const Il2CppType* GenInst_Calendar_t585061108_0_0_0_Types[] = { &Calendar_t585061108_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t585061108_0_0_0 = { 1, GenInst_Calendar_t585061108_0_0_0_Types };
extern const Il2CppType CultureInfo_t3500843524_0_0_0;
static const Il2CppType* GenInst_CultureInfo_t3500843524_0_0_0_Types[] = { &CultureInfo_t3500843524_0_0_0 };
extern const Il2CppGenericInst GenInst_CultureInfo_t3500843524_0_0_0 = { 1, GenInst_CultureInfo_t3500843524_0_0_0_Types };
extern const Il2CppType IFormatProvider_t2849799027_0_0_0;
static const Il2CppType* GenInst_IFormatProvider_t2849799027_0_0_0_Types[] = { &IFormatProvider_t2849799027_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormatProvider_t2849799027_0_0_0 = { 1, GenInst_IFormatProvider_t2849799027_0_0_0_Types };
extern const Il2CppType Module_t4282841206_0_0_0;
static const Il2CppType* GenInst_Module_t4282841206_0_0_0_Types[] = { &Module_t4282841206_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t4282841206_0_0_0 = { 1, GenInst_Module_t4282841206_0_0_0_Types };
extern const Il2CppType _Module_t2144668161_0_0_0;
static const Il2CppType* GenInst__Module_t2144668161_0_0_0_Types[] = { &_Module_t2144668161_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t2144668161_0_0_0 = { 1, GenInst__Module_t2144668161_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t1498197914_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1498197914_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t94157543_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_Types[] = { &CustomAttributeNamedArgument_t94157543_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t94157543_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_Types };
extern const Il2CppType Exception_t1927440687_0_0_0;
static const Il2CppType* GenInst_Exception_t1927440687_0_0_0_Types[] = { &Exception_t1927440687_0_0_0 };
extern const Il2CppGenericInst GenInst_Exception_t1927440687_0_0_0 = { 1, GenInst_Exception_t1927440687_0_0_0_Types };
extern const Il2CppType _Exception_t3026971024_0_0_0;
static const Il2CppType* GenInst__Exception_t3026971024_0_0_0_Types[] = { &_Exception_t3026971024_0_0_0 };
extern const Il2CppGenericInst GenInst__Exception_t3026971024_0_0_0 = { 1, GenInst__Exception_t3026971024_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t4156028127_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t4156028127_0_0_0_Types[] = { &ModuleBuilder_t4156028127_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t4156028127_0_0_0 = { 1, GenInst_ModuleBuilder_t4156028127_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t1075102050_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t1075102050_0_0_0_Types[] = { &_ModuleBuilder_t1075102050_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t1075102050_0_0_0 = { 1, GenInst__ModuleBuilder_t1075102050_0_0_0_Types };
extern const Il2CppType MonoResource_t3127387157_0_0_0;
static const Il2CppType* GenInst_MonoResource_t3127387157_0_0_0_Types[] = { &MonoResource_t3127387157_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoResource_t3127387157_0_0_0 = { 1, GenInst_MonoResource_t3127387157_0_0_0_Types };
extern const Il2CppType RefEmitPermissionSet_t2708608433_0_0_0;
static const Il2CppType* GenInst_RefEmitPermissionSet_t2708608433_0_0_0_Types[] = { &RefEmitPermissionSet_t2708608433_0_0_0 };
extern const Il2CppGenericInst GenInst_RefEmitPermissionSet_t2708608433_0_0_0 = { 1, GenInst_RefEmitPermissionSet_t2708608433_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t3344728474_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t3344728474_0_0_0_Types[] = { &ParameterBuilder_t3344728474_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t3344728474_0_0_0 = { 1, GenInst_ParameterBuilder_t3344728474_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t2251638747_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t2251638747_0_0_0_Types[] = { &_ParameterBuilder_t2251638747_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t2251638747_0_0_0 = { 1, GenInst__ParameterBuilder_t2251638747_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t1664964607_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t1664964607_0_0_0_Types[] = { &TypeU5BU5D_t1664964607_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t1664964607_0_0_0 = { 1, GenInst_TypeU5BU5D_t1664964607_0_0_0_Types };
extern const Il2CppType Il2CppArray_0_0_0;
static const Il2CppType* GenInst_Il2CppArray_0_0_0_Types[] = { &Il2CppArray_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppArray_0_0_0 = { 1, GenInst_Il2CppArray_0_0_0_Types };
extern const Il2CppType ICollection_t91669223_0_0_0;
static const Il2CppType* GenInst_ICollection_t91669223_0_0_0_Types[] = { &ICollection_t91669223_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t91669223_0_0_0 = { 1, GenInst_ICollection_t91669223_0_0_0_Types };
extern const Il2CppType IList_t3321498491_0_0_0;
static const Il2CppType* GenInst_IList_t3321498491_0_0_0_Types[] = { &IList_t3321498491_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t3321498491_0_0_0 = { 1, GenInst_IList_t3321498491_0_0_0_Types };
extern const Il2CppType LocalBuilder_t2116499186_0_0_0;
static const Il2CppType* GenInst_LocalBuilder_t2116499186_0_0_0_Types[] = { &LocalBuilder_t2116499186_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalBuilder_t2116499186_0_0_0 = { 1, GenInst_LocalBuilder_t2116499186_0_0_0_Types };
extern const Il2CppType _LocalBuilder_t61912499_0_0_0;
static const Il2CppType* GenInst__LocalBuilder_t61912499_0_0_0_Types[] = { &_LocalBuilder_t61912499_0_0_0 };
extern const Il2CppGenericInst GenInst__LocalBuilder_t61912499_0_0_0 = { 1, GenInst__LocalBuilder_t61912499_0_0_0_Types };
extern const Il2CppType LocalVariableInfo_t1749284021_0_0_0;
static const Il2CppType* GenInst_LocalVariableInfo_t1749284021_0_0_0_Types[] = { &LocalVariableInfo_t1749284021_0_0_0 };
extern const Il2CppGenericInst GenInst_LocalVariableInfo_t1749284021_0_0_0 = { 1, GenInst_LocalVariableInfo_t1749284021_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t149559338_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t149559338_0_0_0_Types[] = { &ILTokenInfo_t149559338_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t149559338_0_0_0 = { 1, GenInst_ILTokenInfo_t149559338_0_0_0_Types };
extern const Il2CppType LabelData_t3712112744_0_0_0;
static const Il2CppType* GenInst_LabelData_t3712112744_0_0_0_Types[] = { &LabelData_t3712112744_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t3712112744_0_0_0 = { 1, GenInst_LabelData_t3712112744_0_0_0_Types };
extern const Il2CppType LabelFixup_t4090909514_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t4090909514_0_0_0_Types[] = { &LabelFixup_t4090909514_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t4090909514_0_0_0 = { 1, GenInst_LabelFixup_t4090909514_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t1370236603_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0_Types[] = { &GenericTypeParameterBuilder_t1370236603_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0_Types };
extern const Il2CppType TypeBuilder_t3308873219_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t3308873219_0_0_0_Types[] = { &TypeBuilder_t3308873219_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t3308873219_0_0_0 = { 1, GenInst_TypeBuilder_t3308873219_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t2783404358_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t2783404358_0_0_0_Types[] = { &_TypeBuilder_t2783404358_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t2783404358_0_0_0 = { 1, GenInst__TypeBuilder_t2783404358_0_0_0_Types };
extern const Il2CppType MethodBuilder_t644187984_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t644187984_0_0_0_Types[] = { &MethodBuilder_t644187984_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t644187984_0_0_0 = { 1, GenInst_MethodBuilder_t644187984_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t3932949077_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t3932949077_0_0_0_Types[] = { &_MethodBuilder_t3932949077_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t3932949077_0_0_0 = { 1, GenInst__MethodBuilder_t3932949077_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t700974433_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t700974433_0_0_0_Types[] = { &ConstructorBuilder_t700974433_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t700974433_0_0_0 = { 1, GenInst_ConstructorBuilder_t700974433_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t1236878896_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t1236878896_0_0_0_Types[] = { &_ConstructorBuilder_t1236878896_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t1236878896_0_0_0 = { 1, GenInst__ConstructorBuilder_t1236878896_0_0_0_Types };
extern const Il2CppType PropertyBuilder_t3694255912_0_0_0;
static const Il2CppType* GenInst_PropertyBuilder_t3694255912_0_0_0_Types[] = { &PropertyBuilder_t3694255912_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t3694255912_0_0_0 = { 1, GenInst_PropertyBuilder_t3694255912_0_0_0_Types };
extern const Il2CppType _PropertyBuilder_t3341912621_0_0_0;
static const Il2CppType* GenInst__PropertyBuilder_t3341912621_0_0_0_Types[] = { &_PropertyBuilder_t3341912621_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t3341912621_0_0_0 = { 1, GenInst__PropertyBuilder_t3341912621_0_0_0_Types };
extern const Il2CppType FieldBuilder_t2784804005_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t2784804005_0_0_0_Types[] = { &FieldBuilder_t2784804005_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t2784804005_0_0_0 = { 1, GenInst_FieldBuilder_t2784804005_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t1895266044_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t1895266044_0_0_0_Types[] = { &_FieldBuilder_t1895266044_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t1895266044_0_0_0 = { 1, GenInst__FieldBuilder_t1895266044_0_0_0_Types };
extern const Il2CppType ResourceInfo_t3933049236_0_0_0;
static const Il2CppType* GenInst_ResourceInfo_t3933049236_0_0_0_Types[] = { &ResourceInfo_t3933049236_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceInfo_t3933049236_0_0_0 = { 1, GenInst_ResourceInfo_t3933049236_0_0_0_Types };
extern const Il2CppType ResourceCacheItem_t333236149_0_0_0;
static const Il2CppType* GenInst_ResourceCacheItem_t333236149_0_0_0_Types[] = { &ResourceCacheItem_t333236149_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t333236149_0_0_0 = { 1, GenInst_ResourceCacheItem_t333236149_0_0_0_Types };
extern const Il2CppType IContextAttribute_t2439121372_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t2439121372_0_0_0_Types[] = { &IContextAttribute_t2439121372_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t2439121372_0_0_0 = { 1, GenInst_IContextAttribute_t2439121372_0_0_0_Types };
extern const Il2CppType IContextProperty_t287246399_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t287246399_0_0_0_Types[] = { &IContextProperty_t287246399_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t287246399_0_0_0 = { 1, GenInst_IContextProperty_t287246399_0_0_0_Types };
extern const Il2CppType Header_t2756440555_0_0_0;
static const Il2CppType* GenInst_Header_t2756440555_0_0_0_Types[] = { &Header_t2756440555_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t2756440555_0_0_0 = { 1, GenInst_Header_t2756440555_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t2759960940_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t2759960940_0_0_0_Types[] = { &ITrackingHandler_t2759960940_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2759960940_0_0_0 = { 1, GenInst_ITrackingHandler_t2759960940_0_0_0_Types };
extern const Il2CppType IComparable_1_t2525044892_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2525044892_0_0_0_Types[] = { &IComparable_1_t2525044892_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2525044892_0_0_0 = { 1, GenInst_IComparable_1_t2525044892_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2897187838_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2897187838_0_0_0_Types[] = { &IEquatable_1_t2897187838_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2897187838_0_0_0 = { 1, GenInst_IEquatable_1_t2897187838_0_0_0_Types };
extern const Il2CppType IComparable_1_t2556540300_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2556540300_0_0_0_Types[] = { &IComparable_1_t2556540300_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2556540300_0_0_0 = { 1, GenInst_IComparable_1_t2556540300_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2928683246_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2928683246_0_0_0_Types[] = { &IEquatable_1_t2928683246_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2928683246_0_0_0 = { 1, GenInst_IEquatable_1_t2928683246_0_0_0_Types };
extern const Il2CppType IComparable_1_t967130876_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t967130876_0_0_0_Types[] = { &IComparable_1_t967130876_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t967130876_0_0_0 = { 1, GenInst_IComparable_1_t967130876_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1339273822_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1339273822_0_0_0_Types[] = { &IEquatable_1_t1339273822_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1339273822_0_0_0 = { 1, GenInst_IEquatable_1_t1339273822_0_0_0_Types };
extern const Il2CppType TypeTag_t141209596_0_0_0;
static const Il2CppType* GenInst_TypeTag_t141209596_0_0_0_Types[] = { &TypeTag_t141209596_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t141209596_0_0_0 = { 1, GenInst_TypeTag_t141209596_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType KeyContainerPermissionAccessEntry_t41069825_0_0_0;
static const Il2CppType* GenInst_KeyContainerPermissionAccessEntry_t41069825_0_0_0_Types[] = { &KeyContainerPermissionAccessEntry_t41069825_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyContainerPermissionAccessEntry_t41069825_0_0_0 = { 1, GenInst_KeyContainerPermissionAccessEntry_t41069825_0_0_0_Types };
extern const Il2CppType StrongName_t2988747270_0_0_0;
static const Il2CppType* GenInst_StrongName_t2988747270_0_0_0_Types[] = { &StrongName_t2988747270_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t2988747270_0_0_0 = { 1, GenInst_StrongName_t2988747270_0_0_0_Types };
extern const Il2CppType CodeConnectAccess_t3638993531_0_0_0;
static const Il2CppType* GenInst_CodeConnectAccess_t3638993531_0_0_0_Types[] = { &CodeConnectAccess_t3638993531_0_0_0 };
extern const Il2CppGenericInst GenInst_CodeConnectAccess_t3638993531_0_0_0 = { 1, GenInst_CodeConnectAccess_t3638993531_0_0_0_Types };
extern const Il2CppType WaitHandle_t677569169_0_0_0;
static const Il2CppType* GenInst_WaitHandle_t677569169_0_0_0_Types[] = { &WaitHandle_t677569169_0_0_0 };
extern const Il2CppGenericInst GenInst_WaitHandle_t677569169_0_0_0 = { 1, GenInst_WaitHandle_t677569169_0_0_0_Types };
extern const Il2CppType IDisposable_t2427283555_0_0_0;
static const Il2CppType* GenInst_IDisposable_t2427283555_0_0_0_Types[] = { &IDisposable_t2427283555_0_0_0 };
extern const Il2CppGenericInst GenInst_IDisposable_t2427283555_0_0_0 = { 1, GenInst_IDisposable_t2427283555_0_0_0_Types };
extern const Il2CppType MarshalByRefObject_t1285298191_0_0_0;
static const Il2CppType* GenInst_MarshalByRefObject_t1285298191_0_0_0_Types[] = { &MarshalByRefObject_t1285298191_0_0_0 };
extern const Il2CppGenericInst GenInst_MarshalByRefObject_t1285298191_0_0_0 = { 1, GenInst_MarshalByRefObject_t1285298191_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t38854645_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0 = { 1, GenInst_KeyValuePair_2_t38854645_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
extern const Il2CppType Attribute_t542643598_0_0_0;
static const Il2CppType* GenInst_Attribute_t542643598_0_0_0_Types[] = { &Attribute_t542643598_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t542643598_0_0_0 = { 1, GenInst_Attribute_t542643598_0_0_0_Types };
extern const Il2CppType _Attribute_t1557664299_0_0_0;
static const Il2CppType* GenInst__Attribute_t1557664299_0_0_0_Types[] = { &_Attribute_t1557664299_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t1557664299_0_0_0 = { 1, GenInst__Attribute_t1557664299_0_0_0_Types };
extern const Il2CppType PropertyDescriptor_t4250402154_0_0_0;
static const Il2CppType* GenInst_PropertyDescriptor_t4250402154_0_0_0_Types[] = { &PropertyDescriptor_t4250402154_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyDescriptor_t4250402154_0_0_0 = { 1, GenInst_PropertyDescriptor_t4250402154_0_0_0_Types };
extern const Il2CppType MemberDescriptor_t3749827553_0_0_0;
static const Il2CppType* GenInst_MemberDescriptor_t3749827553_0_0_0_Types[] = { &MemberDescriptor_t3749827553_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberDescriptor_t3749827553_0_0_0 = { 1, GenInst_MemberDescriptor_t3749827553_0_0_0_Types };
extern const Il2CppType EventDescriptor_t962731901_0_0_0;
static const Il2CppType* GenInst_EventDescriptor_t962731901_0_0_0_Types[] = { &EventDescriptor_t962731901_0_0_0 };
extern const Il2CppGenericInst GenInst_EventDescriptor_t962731901_0_0_0 = { 1, GenInst_EventDescriptor_t962731901_0_0_0_Types };
extern const Il2CppType AttributeU5BU5D_t4255796347_0_0_0;
static const Il2CppType* GenInst_AttributeU5BU5D_t4255796347_0_0_0_Types[] = { &AttributeU5BU5D_t4255796347_0_0_0 };
extern const Il2CppGenericInst GenInst_AttributeU5BU5D_t4255796347_0_0_0 = { 1, GenInst_AttributeU5BU5D_t4255796347_0_0_0_Types };
extern const Il2CppType LinkedList_1_t2743332604_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_Types[] = { &Type_t_0_0_0, &LinkedList_1_t2743332604_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0 = { 2, GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_Types };
extern const Il2CppType TypeDescriptionProvider_t2438624375_0_0_0;
static const Il2CppType* GenInst_TypeDescriptionProvider_t2438624375_0_0_0_Types[] = { &TypeDescriptionProvider_t2438624375_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeDescriptionProvider_t2438624375_0_0_0 = { 1, GenInst_TypeDescriptionProvider_t2438624375_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &LinkedList_1_t2743332604_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_LinkedList_1_t2743332604_0_0_0_Types[] = { &LinkedList_1_t2743332604_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t2743332604_0_0_0 = { 1, GenInst_LinkedList_1_t2743332604_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2438035723_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2438035723_0_0_0_Types[] = { &KeyValuePair_2_t2438035723_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2438035723_0_0_0 = { 1, GenInst_KeyValuePair_2_t2438035723_0_0_0_Types };
extern const Il2CppType WeakObjectWrapper_t2012978780_0_0_0;
static const Il2CppType* GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_Types[] = { &WeakObjectWrapper_t2012978780_0_0_0, &LinkedList_1_t2743332604_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0 = { 2, GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_Types };
static const Il2CppType* GenInst_WeakObjectWrapper_t2012978780_0_0_0_Types[] = { &WeakObjectWrapper_t2012978780_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t2012978780_0_0_0 = { 1, GenInst_WeakObjectWrapper_t2012978780_0_0_0_Types };
static const Il2CppType* GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &WeakObjectWrapper_t2012978780_0_0_0, &LinkedList_1_t2743332604_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3261256129_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3261256129_0_0_0_Types[] = { &KeyValuePair_2_t3261256129_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3261256129_0_0_0 = { 1, GenInst_KeyValuePair_2_t3261256129_0_0_0_Types };
extern const Il2CppType Cookie_t3154017544_0_0_0;
static const Il2CppType* GenInst_Cookie_t3154017544_0_0_0_Types[] = { &Cookie_t3154017544_0_0_0 };
extern const Il2CppGenericInst GenInst_Cookie_t3154017544_0_0_0 = { 1, GenInst_Cookie_t3154017544_0_0_0_Types };
extern const Il2CppType IPAddress_t1399971723_0_0_0;
static const Il2CppType* GenInst_IPAddress_t1399971723_0_0_0_Types[] = { &IPAddress_t1399971723_0_0_0 };
extern const Il2CppGenericInst GenInst_IPAddress_t1399971723_0_0_0 = { 1, GenInst_IPAddress_t1399971723_0_0_0_Types };
extern const Il2CppType NetworkInterface_t63927633_0_0_0;
static const Il2CppType* GenInst_NetworkInterface_t63927633_0_0_0_Types[] = { &NetworkInterface_t63927633_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkInterface_t63927633_0_0_0 = { 1, GenInst_NetworkInterface_t63927633_0_0_0_Types };
extern const Il2CppType LinuxNetworkInterface_t3864470295_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_Types[] = { &String_t_0_0_0, &LinuxNetworkInterface_t3864470295_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0 = { 2, GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &LinuxNetworkInterface_t3864470295_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_LinuxNetworkInterface_t3864470295_0_0_0_Types[] = { &LinuxNetworkInterface_t3864470295_0_0_0 };
extern const Il2CppGenericInst GenInst_LinuxNetworkInterface_t3864470295_0_0_0 = { 1, GenInst_LinuxNetworkInterface_t3864470295_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3536594779_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3536594779_0_0_0_Types[] = { &KeyValuePair_2_t3536594779_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3536594779_0_0_0 = { 1, GenInst_KeyValuePair_2_t3536594779_0_0_0_Types };
extern const Il2CppType MacOsNetworkInterface_t1454185290_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_Types[] = { &String_t_0_0_0, &MacOsNetworkInterface_t1454185290_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0 = { 2, GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &MacOsNetworkInterface_t1454185290_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_MacOsNetworkInterface_t1454185290_0_0_0_Types[] = { &MacOsNetworkInterface_t1454185290_0_0_0 };
extern const Il2CppGenericInst GenInst_MacOsNetworkInterface_t1454185290_0_0_0 = { 1, GenInst_MacOsNetworkInterface_t1454185290_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1126309774_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1126309774_0_0_0_Types[] = { &KeyValuePair_2_t1126309774_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1126309774_0_0_0 = { 1, GenInst_KeyValuePair_2_t1126309774_0_0_0_Types };
extern const Il2CppType Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0;
static const Il2CppType* GenInst_Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0_Types[] = { &Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0 };
extern const Il2CppGenericInst GenInst_Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0 = { 1, GenInst_Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t4278378721_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t4278378721_0_0_0_Types[] = { &X509ChainStatus_t4278378721_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t4278378721_0_0_0 = { 1, GenInst_X509ChainStatus_t4278378721_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t2594217482_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t2594217482_0_0_0_Types[] = { &ArraySegment_1_t2594217482_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t2594217482_0_0_0 = { 1, GenInst_ArraySegment_1_t2594217482_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1174980068_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0 = { 1, GenInst_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t3825574718_0_0_0, &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t3825574718_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3497699202_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3497699202_0_0_0_Types[] = { &KeyValuePair_2_t3497699202_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3497699202_0_0_0 = { 1, GenInst_KeyValuePair_2_t3497699202_0_0_0_Types };
extern const Il2CppType X509Certificate_t283079845_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t283079845_0_0_0_Types[] = { &X509Certificate_t283079845_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t283079845_0_0_0 = { 1, GenInst_X509Certificate_t283079845_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t327125377_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t327125377_0_0_0_Types[] = { &IDeserializationCallback_t327125377_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t327125377_0_0_0 = { 1, GenInst_IDeserializationCallback_t327125377_0_0_0_Types };
extern const Il2CppType Capture_t4157900610_0_0_0;
static const Il2CppType* GenInst_Capture_t4157900610_0_0_0_Types[] = { &Capture_t4157900610_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t4157900610_0_0_0 = { 1, GenInst_Capture_t4157900610_0_0_0_Types };
extern const Il2CppType DynamicMethod_t3307743052_0_0_0;
static const Il2CppType* GenInst_DynamicMethod_t3307743052_0_0_0_Types[] = { &DynamicMethod_t3307743052_0_0_0 };
extern const Il2CppGenericInst GenInst_DynamicMethod_t3307743052_0_0_0 = { 1, GenInst_DynamicMethod_t3307743052_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3132015601_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0 = { 1, GenInst_KeyValuePair_2_t3132015601_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types };
extern const Il2CppType Label_t4243202660_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Label_t4243202660_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1008373517_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1008373517_0_0_0_Types[] = { &KeyValuePair_2_t1008373517_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1008373517_0_0_0 = { 1, GenInst_KeyValuePair_2_t1008373517_0_0_0_Types };
static const Il2CppType* GenInst_Label_t4243202660_0_0_0_Types[] = { &Label_t4243202660_0_0_0 };
extern const Il2CppGenericInst GenInst_Label_t4243202660_0_0_0 = { 1, GenInst_Label_t4243202660_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Label_t4243202660_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_Label_t4243202660_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Label_t4243202660_0_0_0, &Label_t4243202660_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_Label_t4243202660_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_Label_t4243202660_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Label_t4243202660_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_KeyValuePair_2_t1008373517_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Label_t4243202660_0_0_0, &KeyValuePair_2_t1008373517_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_KeyValuePair_2_t1008373517_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_KeyValuePair_2_t1008373517_0_0_0_Types };
extern const Il2CppType Group_t3761430853_0_0_0;
static const Il2CppType* GenInst_Group_t3761430853_0_0_0_Types[] = { &Group_t3761430853_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t3761430853_0_0_0 = { 1, GenInst_Group_t3761430853_0_0_0_Types };
extern const Il2CppType Mark_t2724874473_0_0_0;
static const Il2CppType* GenInst_Mark_t2724874473_0_0_0_Types[] = { &Mark_t2724874473_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t2724874473_0_0_0 = { 1, GenInst_Mark_t2724874473_0_0_0_Types };
extern const Il2CppType UriScheme_t1876590943_0_0_0;
static const Il2CppType* GenInst_UriScheme_t1876590943_0_0_0_Types[] = { &UriScheme_t1876590943_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t1876590943_0_0_0 = { 1, GenInst_UriScheme_t1876590943_0_0_0_Types };
extern const Il2CppType BigInteger_t925946153_0_0_0;
static const Il2CppType* GenInst_BigInteger_t925946153_0_0_0_Types[] = { &BigInteger_t925946153_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t925946153_0_0_0 = { 1, GenInst_BigInteger_t925946153_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t3397334013_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t3397334013_0_0_0_Types[] = { &ByteU5BU5D_t3397334013_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t3397334013_0_0_0 = { 1, GenInst_ByteU5BU5D_t3397334013_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t4001384466_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t4001384466_0_0_0_Types[] = { &ClientCertificateType_t4001384466_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t4001384466_0_0_0 = { 1, GenInst_ClientCertificateType_t4001384466_0_0_0_Types };
extern const Il2CppType ConfigurationProperty_t2048066811_0_0_0;
static const Il2CppType* GenInst_ConfigurationProperty_t2048066811_0_0_0_Types[] = { &ConfigurationProperty_t2048066811_0_0_0 };
extern const Il2CppGenericInst GenInst_ConfigurationProperty_t2048066811_0_0_0 = { 1, GenInst_ConfigurationProperty_t2048066811_0_0_0_Types };
extern const Il2CppType XsdIdentityPath_t2037874_0_0_0;
static const Il2CppType* GenInst_XsdIdentityPath_t2037874_0_0_0_Types[] = { &XsdIdentityPath_t2037874_0_0_0 };
extern const Il2CppGenericInst GenInst_XsdIdentityPath_t2037874_0_0_0 = { 1, GenInst_XsdIdentityPath_t2037874_0_0_0_Types };
extern const Il2CppType XsdIdentityField_t2563516441_0_0_0;
static const Il2CppType* GenInst_XsdIdentityField_t2563516441_0_0_0_Types[] = { &XsdIdentityField_t2563516441_0_0_0 };
extern const Il2CppGenericInst GenInst_XsdIdentityField_t2563516441_0_0_0 = { 1, GenInst_XsdIdentityField_t2563516441_0_0_0_Types };
extern const Il2CppType XsdIdentityStep_t452377251_0_0_0;
static const Il2CppType* GenInst_XsdIdentityStep_t452377251_0_0_0_Types[] = { &XsdIdentityStep_t452377251_0_0_0 };
extern const Il2CppGenericInst GenInst_XsdIdentityStep_t452377251_0_0_0 = { 1, GenInst_XsdIdentityStep_t452377251_0_0_0_Types };
extern const Il2CppType XmlSchemaAttribute_t4015859774_0_0_0;
static const Il2CppType* GenInst_XmlSchemaAttribute_t4015859774_0_0_0_Types[] = { &XmlSchemaAttribute_t4015859774_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaAttribute_t4015859774_0_0_0 = { 1, GenInst_XmlSchemaAttribute_t4015859774_0_0_0_Types };
extern const Il2CppType XmlSchemaAnnotated_t2082486936_0_0_0;
static const Il2CppType* GenInst_XmlSchemaAnnotated_t2082486936_0_0_0_Types[] = { &XmlSchemaAnnotated_t2082486936_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaAnnotated_t2082486936_0_0_0 = { 1, GenInst_XmlSchemaAnnotated_t2082486936_0_0_0_Types };
extern const Il2CppType XmlSchemaObject_t2050913741_0_0_0;
static const Il2CppType* GenInst_XmlSchemaObject_t2050913741_0_0_0_Types[] = { &XmlSchemaObject_t2050913741_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaObject_t2050913741_0_0_0 = { 1, GenInst_XmlSchemaObject_t2050913741_0_0_0_Types };
extern const Il2CppType XmlSchemaException_t4082200141_0_0_0;
static const Il2CppType* GenInst_XmlSchemaException_t4082200141_0_0_0_Types[] = { &XmlSchemaException_t4082200141_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaException_t4082200141_0_0_0 = { 1, GenInst_XmlSchemaException_t4082200141_0_0_0_Types };
extern const Il2CppType SystemException_t3877406272_0_0_0;
static const Il2CppType* GenInst_SystemException_t3877406272_0_0_0_Types[] = { &SystemException_t3877406272_0_0_0 };
extern const Il2CppGenericInst GenInst_SystemException_t3877406272_0_0_0 = { 1, GenInst_SystemException_t3877406272_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1430411454_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1430411454_0_0_0_Types[] = { &KeyValuePair_2_t1430411454_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1430411454_0_0_0 = { 1, GenInst_KeyValuePair_2_t1430411454_0_0_0_Types };
extern const Il2CppType DTDNode_t1758286970_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_DTDNode_t1758286970_0_0_0_Types[] = { &String_t_0_0_0, &DTDNode_t1758286970_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DTDNode_t1758286970_0_0_0 = { 2, GenInst_String_t_0_0_0_DTDNode_t1758286970_0_0_0_Types };
static const Il2CppType* GenInst_DTDNode_t1758286970_0_0_0_Types[] = { &DTDNode_t1758286970_0_0_0 };
extern const Il2CppGenericInst GenInst_DTDNode_t1758286970_0_0_0 = { 1, GenInst_DTDNode_t1758286970_0_0_0_Types };
extern const Il2CppType AttributeSlot_t1499247213_0_0_0;
static const Il2CppType* GenInst_AttributeSlot_t1499247213_0_0_0_Types[] = { &AttributeSlot_t1499247213_0_0_0 };
extern const Il2CppGenericInst GenInst_AttributeSlot_t1499247213_0_0_0 = { 1, GenInst_AttributeSlot_t1499247213_0_0_0_Types };
extern const Il2CppType Entry_t2583369454_0_0_0;
static const Il2CppType* GenInst_Entry_t2583369454_0_0_0_Types[] = { &Entry_t2583369454_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t2583369454_0_0_0 = { 1, GenInst_Entry_t2583369454_0_0_0_Types };
extern const Il2CppType XmlNode_t616554813_0_0_0;
static const Il2CppType* GenInst_XmlNode_t616554813_0_0_0_Types[] = { &XmlNode_t616554813_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlNode_t616554813_0_0_0 = { 1, GenInst_XmlNode_t616554813_0_0_0_Types };
extern const Il2CppType IXPathNavigable_t845515791_0_0_0;
static const Il2CppType* GenInst_IXPathNavigable_t845515791_0_0_0_Types[] = { &IXPathNavigable_t845515791_0_0_0 };
extern const Il2CppGenericInst GenInst_IXPathNavigable_t845515791_0_0_0 = { 1, GenInst_IXPathNavigable_t845515791_0_0_0_Types };
extern const Il2CppType NsDecl_t3210081295_0_0_0;
static const Il2CppType* GenInst_NsDecl_t3210081295_0_0_0_Types[] = { &NsDecl_t3210081295_0_0_0 };
extern const Il2CppGenericInst GenInst_NsDecl_t3210081295_0_0_0 = { 1, GenInst_NsDecl_t3210081295_0_0_0_Types };
extern const Il2CppType NsScope_t2513625351_0_0_0;
static const Il2CppType* GenInst_NsScope_t2513625351_0_0_0_Types[] = { &NsScope_t2513625351_0_0_0 };
extern const Il2CppGenericInst GenInst_NsScope_t2513625351_0_0_0 = { 1, GenInst_NsScope_t2513625351_0_0_0_Types };
extern const Il2CppType XmlAttributeTokenInfo_t3353594030_0_0_0;
static const Il2CppType* GenInst_XmlAttributeTokenInfo_t3353594030_0_0_0_Types[] = { &XmlAttributeTokenInfo_t3353594030_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlAttributeTokenInfo_t3353594030_0_0_0 = { 1, GenInst_XmlAttributeTokenInfo_t3353594030_0_0_0_Types };
extern const Il2CppType XmlTokenInfo_t254587324_0_0_0;
static const Il2CppType* GenInst_XmlTokenInfo_t254587324_0_0_0_Types[] = { &XmlTokenInfo_t254587324_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlTokenInfo_t254587324_0_0_0 = { 1, GenInst_XmlTokenInfo_t254587324_0_0_0_Types };
extern const Il2CppType TagName_t2340974457_0_0_0;
static const Il2CppType* GenInst_TagName_t2340974457_0_0_0_Types[] = { &TagName_t2340974457_0_0_0 };
extern const Il2CppGenericInst GenInst_TagName_t2340974457_0_0_0 = { 1, GenInst_TagName_t2340974457_0_0_0_Types };
extern const Il2CppType XmlNodeInfo_t3709371029_0_0_0;
static const Il2CppType* GenInst_XmlNodeInfo_t3709371029_0_0_0_Types[] = { &XmlNodeInfo_t3709371029_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlNodeInfo_t3709371029_0_0_0 = { 1, GenInst_XmlNodeInfo_t3709371029_0_0_0_Types };
extern const Il2CppType XmlAttribute_t175731005_0_0_0;
static const Il2CppType* GenInst_XmlAttribute_t175731005_0_0_0_Types[] = { &XmlAttribute_t175731005_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlAttribute_t175731005_0_0_0 = { 1, GenInst_XmlAttribute_t175731005_0_0_0_Types };
extern const Il2CppType IHasXmlChildNode_t2048545686_0_0_0;
static const Il2CppType* GenInst_IHasXmlChildNode_t2048545686_0_0_0_Types[] = { &IHasXmlChildNode_t2048545686_0_0_0 };
extern const Il2CppGenericInst GenInst_IHasXmlChildNode_t2048545686_0_0_0 = { 1, GenInst_IHasXmlChildNode_t2048545686_0_0_0_Types };
extern const Il2CppType XmlQualifiedName_t1944712516_0_0_0;
static const Il2CppType* GenInst_XmlQualifiedName_t1944712516_0_0_0_Types[] = { &XmlQualifiedName_t1944712516_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t1944712516_0_0_0 = { 1, GenInst_XmlQualifiedName_t1944712516_0_0_0_Types };
extern const Il2CppType Regex_t1803876613_0_0_0;
static const Il2CppType* GenInst_Regex_t1803876613_0_0_0_Types[] = { &Regex_t1803876613_0_0_0 };
extern const Il2CppGenericInst GenInst_Regex_t1803876613_0_0_0 = { 1, GenInst_Regex_t1803876613_0_0_0_Types };
extern const Il2CppType XmlSchemaSimpleType_t248156492_0_0_0;
static const Il2CppType* GenInst_XmlSchemaSimpleType_t248156492_0_0_0_Types[] = { &XmlSchemaSimpleType_t248156492_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaSimpleType_t248156492_0_0_0 = { 1, GenInst_XmlSchemaSimpleType_t248156492_0_0_0_Types };
extern const Il2CppType XmlSchemaType_t1795078578_0_0_0;
static const Il2CppType* GenInst_XmlSchemaType_t1795078578_0_0_0_Types[] = { &XmlSchemaType_t1795078578_0_0_0 };
extern const Il2CppGenericInst GenInst_XmlSchemaType_t1795078578_0_0_0 = { 1, GenInst_XmlSchemaType_t1795078578_0_0_0_Types };
extern const Il2CppType Link_t865133271_0_0_0;
static const Il2CppType* GenInst_Link_t865133271_0_0_0_Types[] = { &Link_t865133271_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t865133271_0_0_0 = { 1, GenInst_Link_t865133271_0_0_0_Types };
extern const Il2CppType List_1_t2058570427_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_List_1_t2058570427_0_0_0_Types[] = { &Il2CppObject_0_0_0, &List_1_t2058570427_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_List_1_t2058570427_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_List_1_t2058570427_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_List_1_t2058570427_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &List_1_t2058570427_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_List_1_t2058570427_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_List_1_t2058570427_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t2058570427_0_0_0_Types[] = { &List_1_t2058570427_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2058570427_0_0_0 = { 1, GenInst_List_1_t2058570427_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3702943074_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3702943074_0_0_0_Types[] = { &KeyValuePair_2_t3702943074_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3702943074_0_0_0 = { 1, GenInst_KeyValuePair_2_t3702943074_0_0_0_Types };
extern const Il2CppType IGrouping_2_t906937361_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t906937361_0_0_0_Types[] = { &IGrouping_2_t906937361_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t906937361_0_0_0 = { 1, GenInst_IGrouping_2_t906937361_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2981576340_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_IEnumerable_1_t2981576340_0_0_0_Types[] = { &Il2CppObject_0_0_0, &IEnumerable_1_t2981576340_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_IEnumerable_1_t2981576340_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_IEnumerable_1_t2981576340_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1436312919_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1436312919_0_0_0_Types[] = { &KeyValuePair_2_t1436312919_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1436312919_0_0_0 = { 1, GenInst_KeyValuePair_2_t1436312919_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Il2CppObject_0_0_0, &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Int64_t909078037_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1436312919_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t1436312919_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1436312919_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1436312919_0_0_0_Types };
extern const Il2CppType WeakReference_t1077405567_0_0_0;
static const Il2CppType* GenInst_WeakReference_t1077405567_0_0_0_Types[] = { &WeakReference_t1077405567_0_0_0 };
extern const Il2CppGenericInst GenInst_WeakReference_t1077405567_0_0_0 = { 1, GenInst_WeakReference_t1077405567_0_0_0_Types };
extern const Il2CppType Task_1_t1809478302_0_0_0;
static const Il2CppType* GenInst_Task_1_t1809478302_0_0_0_Types[] = { &Task_1_t1809478302_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t1809478302_0_0_0 = { 1, GenInst_Task_1_t1809478302_0_0_0_Types };
extern const Il2CppType Task_t1843236107_0_0_0;
static const Il2CppType* GenInst_Task_t1843236107_0_0_0_Types[] = { &Task_t1843236107_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_t1843236107_0_0_0 = { 1, GenInst_Task_t1843236107_0_0_0_Types };
extern const Il2CppType Action_1_t1645035489_0_0_0;
static const Il2CppType* GenInst_Action_1_t1645035489_0_0_0_Types[] = { &Action_1_t1645035489_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t1645035489_0_0_0 = { 1, GenInst_Action_1_t1645035489_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t818741072_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t818741072_0_0_0_Types[] = { &KeyValuePair_2_t818741072_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t818741072_0_0_0 = { 1, GenInst_KeyValuePair_2_t818741072_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Int32_t2071877448_0_0_0, &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t818741072_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t818741072_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t818741072_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t818741072_0_0_0_Types };
extern const Il2CppType Action_t3226471752_0_0_0;
static const Il2CppType* GenInst_Action_t3226471752_0_0_0_Types[] = { &Action_t3226471752_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_t3226471752_0_0_0 = { 1, GenInst_Action_t3226471752_0_0_0_Types };
static const Il2CppType* GenInst_Task_t1843236107_0_0_0_Il2CppObject_0_0_0_Types[] = { &Task_t1843236107_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_t1843236107_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Task_t1843236107_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Task_t1843236107_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Task_t1843236107_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_t1843236107_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Task_t1843236107_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType Task_1_t1191906455_0_0_0;
static const Il2CppType* GenInst_Task_1_t1191906455_0_0_0_Types[] = { &Task_1_t1191906455_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t1191906455_0_0_0 = { 1, GenInst_Task_1_t1191906455_0_0_0_Types };
static const Il2CppType* GenInst_Task_1_t1809478302_0_0_0_Il2CppObject_0_0_0_Types[] = { &Task_1_t1809478302_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t1809478302_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Task_1_t1809478302_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType Object_t1021602117_0_0_0;
static const Il2CppType* GenInst_Object_t1021602117_0_0_0_Types[] = { &Object_t1021602117_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t1021602117_0_0_0 = { 1, GenInst_Object_t1021602117_0_0_0_Types };
extern const Il2CppType Camera_t189460977_0_0_0;
static const Il2CppType* GenInst_Camera_t189460977_0_0_0_Types[] = { &Camera_t189460977_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t189460977_0_0_0 = { 1, GenInst_Camera_t189460977_0_0_0_Types };
extern const Il2CppType Behaviour_t955675639_0_0_0;
static const Il2CppType* GenInst_Behaviour_t955675639_0_0_0_Types[] = { &Behaviour_t955675639_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t955675639_0_0_0 = { 1, GenInst_Behaviour_t955675639_0_0_0_Types };
extern const Il2CppType Component_t3819376471_0_0_0;
static const Il2CppType* GenInst_Component_t3819376471_0_0_0_Types[] = { &Component_t3819376471_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t3819376471_0_0_0 = { 1, GenInst_Component_t3819376471_0_0_0_Types };
extern const Il2CppType Display_t3666191348_0_0_0;
static const Il2CppType* GenInst_Display_t3666191348_0_0_0_Types[] = { &Display_t3666191348_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t3666191348_0_0_0 = { 1, GenInst_Display_t3666191348_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType AchievementDescription_t3110978151_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t3110978151_0_0_0_Types[] = { &AchievementDescription_t3110978151_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t3110978151_0_0_0 = { 1, GenInst_AchievementDescription_t3110978151_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t3498529102_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t3498529102_0_0_0_Types[] = { &IAchievementDescription_t3498529102_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t3498529102_0_0_0 = { 1, GenInst_IAchievementDescription_t3498529102_0_0_0_Types };
extern const Il2CppType UserProfile_t3365630962_0_0_0;
static const Il2CppType* GenInst_UserProfile_t3365630962_0_0_0_Types[] = { &UserProfile_t3365630962_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t3365630962_0_0_0 = { 1, GenInst_UserProfile_t3365630962_0_0_0_Types };
extern const Il2CppType IUserProfile_t4108565527_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t4108565527_0_0_0_Types[] = { &IUserProfile_t4108565527_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t4108565527_0_0_0 = { 1, GenInst_IUserProfile_t4108565527_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t453887929_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t453887929_0_0_0_Types[] = { &GcLeaderboard_t453887929_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t453887929_0_0_0 = { 1, GenInst_GcLeaderboard_t453887929_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t4083280315_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t4083280315_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t2709554645_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t2709554645_0_0_0_Types[] = { &IAchievementU5BU5D_t2709554645_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t2709554645_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t2709554645_0_0_0_Types };
extern const Il2CppType IAchievement_t1752291260_0_0_0;
static const Il2CppType* GenInst_IAchievement_t1752291260_0_0_0_Types[] = { &IAchievement_t1752291260_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t1752291260_0_0_0 = { 1, GenInst_IAchievement_t1752291260_0_0_0_Types };
extern const Il2CppType GcAchievementData_t1754866149_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t1754866149_0_0_0_Types[] = { &GcAchievementData_t1754866149_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t1754866149_0_0_0 = { 1, GenInst_GcAchievementData_t1754866149_0_0_0_Types };
extern const Il2CppType Achievement_t1333316625_0_0_0;
static const Il2CppType* GenInst_Achievement_t1333316625_0_0_0_Types[] = { &Achievement_t1333316625_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t1333316625_0_0_0 = { 1, GenInst_Achievement_t1333316625_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t3237304636_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t3237304636_0_0_0_Types[] = { &IScoreU5BU5D_t3237304636_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t3237304636_0_0_0 = { 1, GenInst_IScoreU5BU5D_t3237304636_0_0_0_Types };
extern const Il2CppType IScore_t513966369_0_0_0;
static const Il2CppType* GenInst_IScore_t513966369_0_0_0_Types[] = { &IScore_t513966369_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t513966369_0_0_0 = { 1, GenInst_IScore_t513966369_0_0_0_Types };
extern const Il2CppType GcScoreData_t3676783238_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t3676783238_0_0_0_Types[] = { &GcScoreData_t3676783238_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t3676783238_0_0_0 = { 1, GenInst_GcScoreData_t3676783238_0_0_0_Types };
extern const Il2CppType Score_t2307748940_0_0_0;
static const Il2CppType* GenInst_Score_t2307748940_0_0_0_Types[] = { &Score_t2307748940_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t2307748940_0_0_0 = { 1, GenInst_Score_t2307748940_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t3461248430_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t3461248430_0_0_0_Types[] = { &IUserProfileU5BU5D_t3461248430_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t3461248430_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t3461248430_0_0_0_Types };
extern const Il2CppType Plane_t3727654732_0_0_0;
static const Il2CppType* GenInst_Plane_t3727654732_0_0_0_Types[] = { &Plane_t3727654732_0_0_0 };
extern const Il2CppGenericInst GenInst_Plane_t3727654732_0_0_0 = { 1, GenInst_Plane_t3727654732_0_0_0_Types };
extern const Il2CppType Keyframe_t1449471340_0_0_0;
static const Il2CppType* GenInst_Keyframe_t1449471340_0_0_0_Types[] = { &Keyframe_t1449471340_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t1449471340_0_0_0 = { 1, GenInst_Keyframe_t1449471340_0_0_0_Types };
extern const Il2CppType Vector3_t2243707580_0_0_0;
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0 = { 1, GenInst_Vector3_t2243707580_0_0_0_Types };
extern const Il2CppType Vector4_t2243707581_0_0_0;
static const Il2CppType* GenInst_Vector4_t2243707581_0_0_0_Types[] = { &Vector4_t2243707581_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t2243707581_0_0_0 = { 1, GenInst_Vector4_t2243707581_0_0_0_Types };
extern const Il2CppType Vector2_t2243707579_0_0_0;
static const Il2CppType* GenInst_Vector2_t2243707579_0_0_0_Types[] = { &Vector2_t2243707579_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0 = { 1, GenInst_Vector2_t2243707579_0_0_0_Types };
extern const Il2CppType Color32_t874517518_0_0_0;
static const Il2CppType* GenInst_Color32_t874517518_0_0_0_Types[] = { &Color32_t874517518_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t874517518_0_0_0 = { 1, GenInst_Color32_t874517518_0_0_0_Types };
extern const Il2CppType Color_t2020392075_0_0_0;
static const Il2CppType* GenInst_Color_t2020392075_0_0_0_Types[] = { &Color_t2020392075_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t2020392075_0_0_0 = { 1, GenInst_Color_t2020392075_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0 = { 2, GenInst_String_t_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1701344717_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1701344717_0_0_0_Types[] = { &KeyValuePair_2_t1701344717_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1701344717_0_0_0 = { 1, GenInst_KeyValuePair_2_t1701344717_0_0_0_Types };
extern const Il2CppType Playable_t3667545548_0_0_0;
static const Il2CppType* GenInst_Playable_t3667545548_0_0_0_Types[] = { &Playable_t3667545548_0_0_0 };
extern const Il2CppGenericInst GenInst_Playable_t3667545548_0_0_0 = { 1, GenInst_Playable_t3667545548_0_0_0_Types };
extern const Il2CppType Scene_t1684909666_0_0_0;
extern const Il2CppType LoadSceneMode_t2981886439_0_0_0;
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0_Types[] = { &Scene_t1684909666_0_0_0, &LoadSceneMode_t2981886439_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0 = { 2, GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_Types[] = { &Scene_t1684909666_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0 = { 1, GenInst_Scene_t1684909666_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0_Types[] = { &Scene_t1684909666_0_0_0, &Scene_t1684909666_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0 = { 2, GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0_Types };
extern const Il2CppType ContactPoint_t1376425630_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t1376425630_0_0_0_Types[] = { &ContactPoint_t1376425630_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t1376425630_0_0_0 = { 1, GenInst_ContactPoint_t1376425630_0_0_0_Types };
extern const Il2CppType RaycastHit_t87180320_0_0_0;
static const Il2CppType* GenInst_RaycastHit_t87180320_0_0_0_Types[] = { &RaycastHit_t87180320_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit_t87180320_0_0_0 = { 1, GenInst_RaycastHit_t87180320_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t502193897_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t502193897_0_0_0_Types[] = { &Rigidbody2D_t502193897_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t502193897_0_0_0 = { 1, GenInst_Rigidbody2D_t502193897_0_0_0_Types };
extern const Il2CppType RaycastHit2D_t4063908774_0_0_0;
static const Il2CppType* GenInst_RaycastHit2D_t4063908774_0_0_0_Types[] = { &RaycastHit2D_t4063908774_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t4063908774_0_0_0 = { 1, GenInst_RaycastHit2D_t4063908774_0_0_0_Types };
extern const Il2CppType ContactPoint2D_t3659330976_0_0_0;
static const Il2CppType* GenInst_ContactPoint2D_t3659330976_0_0_0_Types[] = { &ContactPoint2D_t3659330976_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t3659330976_0_0_0 = { 1, GenInst_ContactPoint2D_t3659330976_0_0_0_Types };
extern const Il2CppType UIVertex_t1204258818_0_0_0;
static const Il2CppType* GenInst_UIVertex_t1204258818_0_0_0_Types[] = { &UIVertex_t1204258818_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t1204258818_0_0_0 = { 1, GenInst_UIVertex_t1204258818_0_0_0_Types };
extern const Il2CppType UICharInfo_t3056636800_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t3056636800_0_0_0_Types[] = { &UICharInfo_t3056636800_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t3056636800_0_0_0 = { 1, GenInst_UICharInfo_t3056636800_0_0_0_Types };
extern const Il2CppType UILineInfo_t3621277874_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t3621277874_0_0_0_Types[] = { &UILineInfo_t3621277874_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t3621277874_0_0_0 = { 1, GenInst_UILineInfo_t3621277874_0_0_0_Types };
extern const Il2CppType Font_t4239498691_0_0_0;
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_Types[] = { &Font_t4239498691_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0 = { 1, GenInst_Font_t4239498691_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t4183744904_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t4183744904_0_0_0_Types[] = { &GUILayoutOption_t4183744904_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t4183744904_0_0_0 = { 1, GenInst_GUILayoutOption_t4183744904_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t3828586629_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t3828586629_0_0_0_Types[] = { &GUILayoutEntry_t3828586629_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t3828586629_0_0_0 = { 1, GenInst_GUILayoutEntry_t3828586629_0_0_0_Types };
extern const Il2CppType LayoutCache_t3120781045_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &LayoutCache_t3120781045_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3749587448_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0 = { 1, GenInst_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &LayoutCache_t3120781045_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_LayoutCache_t3120781045_0_0_0_Types[] = { &LayoutCache_t3120781045_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutCache_t3120781045_0_0_0 = { 1, GenInst_LayoutCache_t3120781045_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4180919198_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4180919198_0_0_0_Types[] = { &KeyValuePair_2_t4180919198_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4180919198_0_0_0 = { 1, GenInst_KeyValuePair_2_t4180919198_0_0_0_Types };
extern const Il2CppType GUIStyle_t1799908754_0_0_0;
static const Il2CppType* GenInst_GUIStyle_t1799908754_0_0_0_Types[] = { &GUIStyle_t1799908754_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t1799908754_0_0_0 = { 1, GenInst_GUIStyle_t1799908754_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1799908754_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1799908754_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1472033238_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1472033238_0_0_0_Types[] = { &KeyValuePair_2_t1472033238_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1472033238_0_0_0 = { 1, GenInst_KeyValuePair_2_t1472033238_0_0_0_Types };
extern const Il2CppType Event_t3028476042_0_0_0;
extern const Il2CppType TextEditOp_t3138797698_0_0_0;
static const Il2CppType* GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &Event_t3028476042_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0 = { 2, GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t488203048_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t488203048_0_0_0_Types[] = { &KeyValuePair_2_t488203048_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t488203048_0_0_0 = { 1, GenInst_KeyValuePair_2_t488203048_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t3138797698_0_0_0_Types[] = { &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t3138797698_0_0_0 = { 1, GenInst_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types[] = { &Il2CppObject_0_0_0, &TextEditOp_t3138797698_0_0_0, &KeyValuePair_2_t488203048_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types };
static const Il2CppType* GenInst_Event_t3028476042_0_0_0_Types[] = { &Event_t3028476042_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t3028476042_0_0_0 = { 1, GenInst_Event_t3028476042_0_0_0_Types };
static const Il2CppType* GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Event_t3028476042_0_0_0, &TextEditOp_t3138797698_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3799506081_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3799506081_0_0_0_Types[] = { &KeyValuePair_2_t3799506081_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3799506081_0_0_0 = { 1, GenInst_KeyValuePair_2_t3799506081_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t2656950_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t2656950_0_0_0_Types[] = { &DisallowMultipleComponent_t2656950_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t2656950_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t2656950_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t3043633143_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t3043633143_0_0_0_Types[] = { &ExecuteInEditMode_t3043633143_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t3043633143_0_0_0 = { 1, GenInst_ExecuteInEditMode_t3043633143_0_0_0_Types };
extern const Il2CppType RequireComponent_t864575032_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t864575032_0_0_0_Types[] = { &RequireComponent_t864575032_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t864575032_0_0_0 = { 1, GenInst_RequireComponent_t864575032_0_0_0_Types };
extern const Il2CppType HitInfo_t1761367055_0_0_0;
static const Il2CppType* GenInst_HitInfo_t1761367055_0_0_0_Types[] = { &HitInfo_t1761367055_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t1761367055_0_0_0 = { 1, GenInst_HitInfo_t1761367055_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType PersistentCall_t3793436469_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t3793436469_0_0_0_Types[] = { &PersistentCall_t3793436469_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t3793436469_0_0_0 = { 1, GenInst_PersistentCall_t3793436469_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t2229564840_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t2229564840_0_0_0_Types[] = { &BaseInvokableCall_t2229564840_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t2229564840_0_0_0 = { 1, GenInst_BaseInvokableCall_t2229564840_0_0_0_Types };
extern const Il2CppType FirebaseApp_t210707726_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_FirebaseApp_t210707726_0_0_0_Types[] = { &String_t_0_0_0, &FirebaseApp_t210707726_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FirebaseApp_t210707726_0_0_0 = { 2, GenInst_String_t_0_0_0_FirebaseApp_t210707726_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_FirebaseApp_t210707726_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &FirebaseApp_t210707726_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FirebaseApp_t210707726_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_FirebaseApp_t210707726_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_FirebaseApp_t210707726_0_0_0_Types[] = { &FirebaseApp_t210707726_0_0_0 };
extern const Il2CppGenericInst GenInst_FirebaseApp_t210707726_0_0_0 = { 1, GenInst_FirebaseApp_t210707726_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4177799506_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4177799506_0_0_0_Types[] = { &KeyValuePair_2_t4177799506_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4177799506_0_0_0 = { 1, GenInst_KeyValuePair_2_t4177799506_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FirebaseApp_t210707726_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FirebaseApp_t210707726_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FirebaseApp_t210707726_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_FirebaseApp_t210707726_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t888819835_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t888819835_0_0_0_Types[] = { &KeyValuePair_2_t888819835_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t888819835_0_0_0 = { 1, GenInst_KeyValuePair_2_t888819835_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Il2CppObject_0_0_0, &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_IntPtr_t_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_IntPtr_t_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t888819835_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t888819835_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t888819835_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t888819835_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FirebaseApp_t210707726_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FirebaseApp_t210707726_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FirebaseApp_t210707726_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_FirebaseApp_t210707726_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2705045562_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2705045562_0_0_0_Types[] = { &KeyValuePair_2_t2705045562_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2705045562_0_0_0 = { 1, GenInst_KeyValuePair_2_t2705045562_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t271247988_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t271247988_0_0_0_Types[] = { &KeyValuePair_2_t271247988_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t271247988_0_0_0 = { 1, GenInst_KeyValuePair_2_t271247988_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Int32_t2071877448_0_0_0, &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_IntPtr_t_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_IntPtr_t_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Int32_t2071877448_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t271247988_0_0_0_Types[] = { &IntPtr_t_0_0_0, &Int32_t2071877448_0_0_0, &KeyValuePair_2_t271247988_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t271247988_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t271247988_0_0_0_Types };
extern const Il2CppType Action_t3062064994_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Action_t3062064994_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Action_t3062064994_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Action_t3062064994_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Action_t3062064994_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Action_t3062064994_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Action_t3062064994_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Action_t3062064994_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Action_t3062064994_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Action_t3062064994_0_0_0_Types[] = { &Action_t3062064994_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_t3062064994_0_0_0 = { 1, GenInst_Action_t3062064994_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4122203147_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4122203147_0_0_0_Types[] = { &KeyValuePair_2_t4122203147_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4122203147_0_0_0 = { 1, GenInst_KeyValuePair_2_t4122203147_0_0_0_Types };
extern const Il2CppType Task_1_t1149249240_0_0_0;
static const Il2CppType* GenInst_Task_1_t1149249240_0_0_0_Types[] = { &Task_1_t1149249240_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t1149249240_0_0_0 = { 1, GenInst_Task_1_t1149249240_0_0_0_Types };
extern const Il2CppType Tuple_2_t460172552_0_0_0;
static const Il2CppType* GenInst_Tuple_2_t460172552_0_0_0_Types[] = { &Tuple_2_t460172552_0_0_0 };
extern const Il2CppGenericInst GenInst_Tuple_2_t460172552_0_0_0 = { 1, GenInst_Tuple_2_t460172552_0_0_0_Types };
extern const Il2CppType SendOrPostCallback_t296893742_0_0_0;
static const Il2CppType* GenInst_SendOrPostCallback_t296893742_0_0_0_Il2CppObject_0_0_0_Types[] = { &SendOrPostCallback_t296893742_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_SendOrPostCallback_t296893742_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_SendOrPostCallback_t296893742_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType ManualResetEvent_t926074657_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_ManualResetEvent_t926074657_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &ManualResetEvent_t926074657_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_ManualResetEvent_t926074657_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_ManualResetEvent_t926074657_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_ManualResetEvent_t926074657_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &ManualResetEvent_t926074657_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_ManualResetEvent_t926074657_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_ManualResetEvent_t926074657_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_ManualResetEvent_t926074657_0_0_0_Types[] = { &ManualResetEvent_t926074657_0_0_0 };
extern const Il2CppGenericInst GenInst_ManualResetEvent_t926074657_0_0_0 = { 1, GenInst_ManualResetEvent_t926074657_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1986212810_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1986212810_0_0_0_Types[] = { &KeyValuePair_2_t1986212810_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1986212810_0_0_0 = { 1, GenInst_KeyValuePair_2_t1986212810_0_0_0_Types };
extern const Il2CppType IEnumerator_t1466026749_0_0_0;
static const Il2CppType* GenInst_IEnumerator_t1466026749_0_0_0_Types[] = { &IEnumerator_t1466026749_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerator_t1466026749_0_0_0 = { 1, GenInst_IEnumerator_t1466026749_0_0_0_Types };
extern const Il2CppType X509CertificateCollection_t1197680765_0_0_0;
static const Il2CppType* GenInst_FirebaseApp_t210707726_0_0_0_X509CertificateCollection_t1197680765_0_0_0_Types[] = { &FirebaseApp_t210707726_0_0_0, &X509CertificateCollection_t1197680765_0_0_0 };
extern const Il2CppGenericInst GenInst_FirebaseApp_t210707726_0_0_0_X509CertificateCollection_t1197680765_0_0_0 = { 2, GenInst_FirebaseApp_t210707726_0_0_0_X509CertificateCollection_t1197680765_0_0_0_Types };
static const Il2CppType* GenInst_FirebaseApp_t210707726_0_0_0_X509CertificateCollection_t1197680765_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &FirebaseApp_t210707726_0_0_0, &X509CertificateCollection_t1197680765_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_FirebaseApp_t210707726_0_0_0_X509CertificateCollection_t1197680765_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_FirebaseApp_t210707726_0_0_0_X509CertificateCollection_t1197680765_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_X509CertificateCollection_t1197680765_0_0_0_Types[] = { &X509CertificateCollection_t1197680765_0_0_0 };
extern const Il2CppGenericInst GenInst_X509CertificateCollection_t1197680765_0_0_0 = { 1, GenInst_X509CertificateCollection_t1197680765_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3716606344_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3716606344_0_0_0_Types[] = { &KeyValuePair_2_t3716606344_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716606344_0_0_0 = { 1, GenInst_KeyValuePair_2_t3716606344_0_0_0_Types };
extern const Il2CppType Dictionary_2_t3943999495_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t3943999495_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Dictionary_2_t3943999495_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t3943999495_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t3943999495_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t3943999495_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Dictionary_2_t3943999495_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t3943999495_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t3943999495_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t3943999495_0_0_0_Types[] = { &Dictionary_2_t3943999495_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3943999495_0_0_0 = { 1, GenInst_Dictionary_2_t3943999495_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t709170352_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t709170352_0_0_0_Types[] = { &KeyValuePair_2_t709170352_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t709170352_0_0_0 = { 1, GenInst_KeyValuePair_2_t709170352_0_0_0_Types };
extern const Il2CppType Dictionary_2_t309261261_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t309261261_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Dictionary_2_t309261261_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t309261261_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t309261261_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2361573779_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2361573779_0_0_0_Types[] = { &KeyValuePair_2_t2361573779_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2361573779_0_0_0 = { 1, GenInst_KeyValuePair_2_t2361573779_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t309261261_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Dictionary_2_t309261261_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t309261261_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t309261261_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t309261261_0_0_0_Types[] = { &Dictionary_2_t309261261_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t309261261_0_0_0 = { 1, GenInst_Dictionary_2_t309261261_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1369399414_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1369399414_0_0_0_Types[] = { &KeyValuePair_2_t1369399414_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1369399414_0_0_0 = { 1, GenInst_KeyValuePair_2_t1369399414_0_0_0_Types };
extern const Il2CppType Runnable_t1446984663_0_0_0;
static const Il2CppType* GenInst_Runnable_t1446984663_0_0_0_Types[] = { &Runnable_t1446984663_0_0_0 };
extern const Il2CppGenericInst GenInst_Runnable_t1446984663_0_0_0 = { 1, GenInst_Runnable_t1446984663_0_0_0_Types };
extern const Il2CppType Thread_t1322377586_0_0_0;
static const Il2CppType* GenInst_Thread_t1322377586_0_0_0_Types[] = { &Thread_t1322377586_0_0_0 };
extern const Il2CppGenericInst GenInst_Thread_t1322377586_0_0_0 = { 1, GenInst_Thread_t1322377586_0_0_0_Types };
extern const Il2CppType IScheduledITask_t3129349074_0_0_0;
static const Il2CppType* GenInst_IScheduledITask_t3129349074_0_0_0_Types[] = { &IScheduledITask_t3129349074_0_0_0 };
extern const Il2CppGenericInst GenInst_IScheduledITask_t3129349074_0_0_0 = { 1, GenInst_IScheduledITask_t3129349074_0_0_0_Types };
static const Il2CppType* GenInst_IScheduledITask_t3129349074_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &IScheduledITask_t3129349074_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_IScheduledITask_t3129349074_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_IScheduledITask_t3129349074_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType SHA1Managed_t7268864_0_0_0;
static const Il2CppType* GenInst_SHA1Managed_t7268864_0_0_0_Types[] = { &SHA1Managed_t7268864_0_0_0 };
extern const Il2CppGenericInst GenInst_SHA1Managed_t7268864_0_0_0 = { 1, GenInst_SHA1Managed_t7268864_0_0_0_Types };
extern const Il2CppType MD5CryptoServiceProvider_t4009738925_0_0_0;
static const Il2CppType* GenInst_MD5CryptoServiceProvider_t4009738925_0_0_0_Types[] = { &MD5CryptoServiceProvider_t4009738925_0_0_0 };
extern const Il2CppGenericInst GenInst_MD5CryptoServiceProvider_t4009738925_0_0_0 = { 1, GenInst_MD5CryptoServiceProvider_t4009738925_0_0_0_Types };
extern const Il2CppType List_1_t61287617_0_0_0;
static const Il2CppType* GenInst_List_1_t61287617_0_0_0_Types[] = { &List_1_t61287617_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t61287617_0_0_0 = { 1, GenInst_List_1_t61287617_0_0_0_Types };
extern const Il2CppType DispatcherKey_t708950850_0_0_0;
extern const Il2CppType Dispatcher_t2240407071_0_0_0;
static const Il2CppType* GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_Types[] = { &DispatcherKey_t708950850_0_0_0, &Dispatcher_t2240407071_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0 = { 2, GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_Types };
static const Il2CppType* GenInst_DispatcherKey_t708950850_0_0_0_Types[] = { &DispatcherKey_t708950850_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatcherKey_t708950850_0_0_0 = { 1, GenInst_DispatcherKey_t708950850_0_0_0_Types };
static const Il2CppType* GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &DispatcherKey_t708950850_0_0_0, &Dispatcher_t2240407071_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Dispatcher_t2240407071_0_0_0_Types[] = { &Dispatcher_t2240407071_0_0_0 };
extern const Il2CppGenericInst GenInst_Dispatcher_t2240407071_0_0_0 = { 1, GenInst_Dispatcher_t2240407071_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3846770086_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3846770086_0_0_0_Types[] = { &KeyValuePair_2_t3846770086_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3846770086_0_0_0 = { 1, GenInst_KeyValuePair_2_t3846770086_0_0_0_Types };
extern const Il2CppType FirebaseAuth_t3105883899_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FirebaseAuth_t3105883899_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FirebaseAuth_t3105883899_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FirebaseAuth_t3105883899_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_FirebaseAuth_t3105883899_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_FirebaseAuth_t3105883899_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &IntPtr_t_0_0_0, &FirebaseAuth_t3105883899_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_FirebaseAuth_t3105883899_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_FirebaseAuth_t3105883899_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_FirebaseAuth_t3105883899_0_0_0_Types[] = { &FirebaseAuth_t3105883899_0_0_0 };
extern const Il2CppGenericInst GenInst_FirebaseAuth_t3105883899_0_0_0 = { 1, GenInst_FirebaseAuth_t3105883899_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1305254439_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1305254439_0_0_0_Types[] = { &KeyValuePair_2_t1305254439_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1305254439_0_0_0 = { 1, GenInst_KeyValuePair_2_t1305254439_0_0_0_Types };
extern const Il2CppType FirebaseUser_t4046966602_0_0_0;
static const Il2CppType* GenInst_FirebaseUser_t4046966602_0_0_0_Types[] = { &FirebaseUser_t4046966602_0_0_0 };
extern const Il2CppGenericInst GenInst_FirebaseUser_t4046966602_0_0_0 = { 1, GenInst_FirebaseUser_t4046966602_0_0_0_Types };
extern const Il2CppType Task_1_t3166995609_0_0_0;
static const Il2CppType* GenInst_Task_1_t3166995609_0_0_0_Types[] = { &Task_1_t3166995609_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t3166995609_0_0_0 = { 1, GenInst_Task_1_t3166995609_0_0_0_Types };
extern const Il2CppType Action_t1614918345_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Action_t1614918345_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Action_t1614918345_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Action_t1614918345_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Action_t1614918345_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Action_t1614918345_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Action_t1614918345_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Action_t1614918345_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Action_t1614918345_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Action_t1614918345_0_0_0_Types[] = { &Action_t1614918345_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_t1614918345_0_0_0 = { 1, GenInst_Action_t1614918345_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2675056498_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2675056498_0_0_0_Types[] = { &KeyValuePair_2_t2675056498_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2675056498_0_0_0 = { 1, GenInst_KeyValuePair_2_t2675056498_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType Nullable_1_t334943763_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Nullable_1_t334943763_0_0_0_Types[] = { &String_t_0_0_0, &Nullable_1_t334943763_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Nullable_1_t334943763_0_0_0 = { 2, GenInst_String_t_0_0_0_Nullable_1_t334943763_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Nullable_1_t334943763_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Nullable_1_t334943763_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Nullable_1_t334943763_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Nullable_1_t334943763_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1979316409_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1979316409_0_0_0_Types[] = { &KeyValuePair_2_t1979316409_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1979316409_0_0_0 = { 1, GenInst_KeyValuePair_2_t1979316409_0_0_0_Types };
static const Il2CppType* GenInst_Nullable_1_t334943763_0_0_0_Types[] = { &Nullable_1_t334943763_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t334943763_0_0_0 = { 1, GenInst_Nullable_1_t334943763_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &String_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3089358386_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3089358386_0_0_0_Types[] = { &KeyValuePair_2_t3089358386_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3089358386_0_0_0 = { 1, GenInst_KeyValuePair_2_t3089358386_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Nullable_1_t334943763_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Nullable_1_t334943763_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Nullable_1_t334943763_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Nullable_1_t334943763_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Nullable_1_t334943763_0_0_0_Nullable_1_t334943763_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Nullable_1_t334943763_0_0_0, &Nullable_1_t334943763_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Nullable_1_t334943763_0_0_0_Nullable_1_t334943763_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Nullable_1_t334943763_0_0_0_Nullable_1_t334943763_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Nullable_1_t334943763_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Nullable_1_t334943763_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Nullable_1_t334943763_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Nullable_1_t334943763_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Nullable_1_t334943763_0_0_0_KeyValuePair_2_t1979316409_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Nullable_1_t334943763_0_0_0, &KeyValuePair_2_t1979316409_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Nullable_1_t334943763_0_0_0_KeyValuePair_2_t1979316409_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Nullable_1_t334943763_0_0_0_KeyValuePair_2_t1979316409_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Nullable_1_t334943763_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Nullable_1_t334943763_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Nullable_1_t334943763_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Nullable_1_t334943763_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t7068247_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t7068247_0_0_0_Types[] = { &KeyValuePair_2_t7068247_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t7068247_0_0_0 = { 1, GenInst_KeyValuePair_2_t7068247_0_0_0_Types };
extern const Il2CppType CompletionListener_t93014473_0_0_0;
static const Il2CppType* GenInst_Task_1_t1809478302_0_0_0_CompletionListener_t93014473_0_0_0_Types[] = { &Task_1_t1809478302_0_0_0, &CompletionListener_t93014473_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t1809478302_0_0_0_CompletionListener_t93014473_0_0_0 = { 2, GenInst_Task_1_t1809478302_0_0_0_CompletionListener_t93014473_0_0_0_Types };
extern const Il2CppType ValueChangedEventArgs_t929877234_0_0_0;
static const Il2CppType* GenInst_ValueChangedEventArgs_t929877234_0_0_0_Types[] = { &ValueChangedEventArgs_t929877234_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueChangedEventArgs_t929877234_0_0_0 = { 1, GenInst_ValueChangedEventArgs_t929877234_0_0_0_Types };
extern const Il2CppType DataSnapshot_t1287895350_0_0_0;
static const Il2CppType* GenInst_DataSnapshot_t1287895350_0_0_0_Types[] = { &DataSnapshot_t1287895350_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSnapshot_t1287895350_0_0_0 = { 1, GenInst_DataSnapshot_t1287895350_0_0_0_Types };
extern const Il2CppType Task_1_t407924357_0_0_0;
static const Il2CppType* GenInst_Task_1_t407924357_0_0_0_Types[] = { &Task_1_t407924357_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t407924357_0_0_0 = { 1, GenInst_Task_1_t407924357_0_0_0_Types };
extern const Il2CppType LlrbValueNode_2_t1296857666_0_0_0;
static const Il2CppType* GenInst_LlrbValueNode_2_t1296857666_0_0_0_Types[] = { &LlrbValueNode_2_t1296857666_0_0_0 };
extern const Il2CppGenericInst GenInst_LlrbValueNode_2_t1296857666_0_0_0 = { 1, GenInst_LlrbValueNode_2_t1296857666_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 5, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType BooleanChunk_t3036858313_0_0_0;
static const Il2CppType* GenInst_BooleanChunk_t3036858313_0_0_0_Types[] = { &BooleanChunk_t3036858313_0_0_0 };
extern const Il2CppGenericInst GenInst_BooleanChunk_t3036858313_0_0_0 = { 1, GenInst_BooleanChunk_t3036858313_0_0_0_Types };
extern const Il2CppType IList_1_t2570160834_0_0_0;
static const Il2CppType* GenInst_IList_1_t2570160834_0_0_0_Types[] = { &IList_1_t2570160834_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t2570160834_0_0_0 = { 1, GenInst_IList_1_t2570160834_0_0_0_Types };
extern const Il2CppType RangeMerge_t98025219_0_0_0;
static const Il2CppType* GenInst_RangeMerge_t98025219_0_0_0_Types[] = { &RangeMerge_t98025219_0_0_0 };
extern const Il2CppGenericInst GenInst_RangeMerge_t98025219_0_0_0 = { 1, GenInst_RangeMerge_t98025219_0_0_0_Types };
extern const Il2CppType ListenQuerySpec_t2050960365_0_0_0;
extern const Il2CppType OutstandingListen_t67058648_0_0_0;
static const Il2CppType* GenInst_ListenQuerySpec_t2050960365_0_0_0_OutstandingListen_t67058648_0_0_0_Types[] = { &ListenQuerySpec_t2050960365_0_0_0, &OutstandingListen_t67058648_0_0_0 };
extern const Il2CppGenericInst GenInst_ListenQuerySpec_t2050960365_0_0_0_OutstandingListen_t67058648_0_0_0 = { 2, GenInst_ListenQuerySpec_t2050960365_0_0_0_OutstandingListen_t67058648_0_0_0_Types };
static const Il2CppType* GenInst_ListenQuerySpec_t2050960365_0_0_0_Types[] = { &ListenQuerySpec_t2050960365_0_0_0 };
extern const Il2CppGenericInst GenInst_ListenQuerySpec_t2050960365_0_0_0 = { 1, GenInst_ListenQuerySpec_t2050960365_0_0_0_Types };
static const Il2CppType* GenInst_OutstandingListen_t67058648_0_0_0_Types[] = { &OutstandingListen_t67058648_0_0_0 };
extern const Il2CppGenericInst GenInst_OutstandingListen_t67058648_0_0_0 = { 1, GenInst_OutstandingListen_t67058648_0_0_0_Types };
extern const Il2CppType OutstandingDisconnect_t3017763353_0_0_0;
static const Il2CppType* GenInst_OutstandingDisconnect_t3017763353_0_0_0_Types[] = { &OutstandingDisconnect_t3017763353_0_0_0 };
extern const Il2CppGenericInst GenInst_OutstandingDisconnect_t3017763353_0_0_0 = { 1, GenInst_OutstandingDisconnect_t3017763353_0_0_0_Types };
extern const Il2CppType OutstandingPut_t774816424_0_0_0;
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_OutstandingPut_t774816424_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &OutstandingPut_t774816424_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_OutstandingPut_t774816424_0_0_0 = { 2, GenInst_Int64_t909078037_0_0_0_OutstandingPut_t774816424_0_0_0_Types };
static const Il2CppType* GenInst_OutstandingPut_t774816424_0_0_0_Types[] = { &OutstandingPut_t774816424_0_0_0 };
extern const Il2CppGenericInst GenInst_OutstandingPut_t774816424_0_0_0 = { 1, GenInst_OutstandingPut_t774816424_0_0_0_Types };
extern const Il2CppType IConnectionRequestCallback_t2235793546_0_0_0;
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_IConnectionRequestCallback_t2235793546_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &IConnectionRequestCallback_t2235793546_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_IConnectionRequestCallback_t2235793546_0_0_0 = { 2, GenInst_Int64_t909078037_0_0_0_IConnectionRequestCallback_t2235793546_0_0_0_Types };
static const Il2CppType* GenInst_IConnectionRequestCallback_t2235793546_0_0_0_Types[] = { &IConnectionRequestCallback_t2235793546_0_0_0 };
extern const Il2CppGenericInst GenInst_IConnectionRequestCallback_t2235793546_0_0_0 = { 1, GenInst_IConnectionRequestCallback_t2235793546_0_0_0_Types };
static const Il2CppType* GenInst_ListenQuerySpec_t2050960365_0_0_0_OutstandingListen_t67058648_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ListenQuerySpec_t2050960365_0_0_0, &OutstandingListen_t67058648_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ListenQuerySpec_t2050960365_0_0_0_OutstandingListen_t67058648_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ListenQuerySpec_t2050960365_0_0_0_OutstandingListen_t67058648_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3990354856_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3990354856_0_0_0_Types[] = { &KeyValuePair_2_t3990354856_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3990354856_0_0_0 = { 1, GenInst_KeyValuePair_2_t3990354856_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_IConnectionRequestCallback_t2235793546_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &IConnectionRequestCallback_t2235793546_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_IConnectionRequestCallback_t2235793546_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_IConnectionRequestCallback_t2235793546_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t982657170_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t982657170_0_0_0_Types[] = { &KeyValuePair_2_t982657170_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t982657170_0_0_0 = { 1, GenInst_KeyValuePair_2_t982657170_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_OutstandingPut_t774816424_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &OutstandingPut_t774816424_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_OutstandingPut_t774816424_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_OutstandingPut_t774816424_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3816647344_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3816647344_0_0_0_Types[] = { &KeyValuePair_2_t3816647344_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3816647344_0_0_0 = { 1, GenInst_KeyValuePair_2_t3816647344_0_0_0_Types };
extern const Il2CppType Link_t3379729309_0_0_0;
static const Il2CppType* GenInst_Link_t3379729309_0_0_0_Types[] = { &Link_t3379729309_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t3379729309_0_0_0 = { 1, GenInst_Link_t3379729309_0_0_0_Types };
extern const Il2CppType IDictionary_2_t2603311978_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t2603311978_0_0_0_Types[] = { &IDictionary_2_t2603311978_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t2603311978_0_0_0 = { 1, GenInst_IDictionary_2_t2603311978_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4001878204_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4001878204_0_0_0_Types[] = { &KeyValuePair_2_t4001878204_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4001878204_0_0_0 = { 1, GenInst_KeyValuePair_2_t4001878204_0_0_0_Types };
extern const Il2CppType Path_t2568473163_0_0_0;
extern const Il2CppType Node_t2640059010_0_0_0;
static const Il2CppType* GenInst_Path_t2568473163_0_0_0_Node_t2640059010_0_0_0_Types[] = { &Path_t2568473163_0_0_0, &Node_t2640059010_0_0_0 };
extern const Il2CppGenericInst GenInst_Path_t2568473163_0_0_0_Node_t2640059010_0_0_0 = { 2, GenInst_Path_t2568473163_0_0_0_Node_t2640059010_0_0_0_Types };
static const Il2CppType* GenInst_Node_t2640059010_0_0_0_Types[] = { &Node_t2640059010_0_0_0 };
extern const Il2CppGenericInst GenInst_Node_t2640059010_0_0_0 = { 1, GenInst_Node_t2640059010_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4051268489_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4051268489_0_0_0_Types[] = { &KeyValuePair_2_t4051268489_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4051268489_0_0_0 = { 1, GenInst_KeyValuePair_2_t4051268489_0_0_0_Types };
static const Il2CppType* GenInst_Path_t2568473163_0_0_0_Il2CppObject_0_0_0_Types[] = { &Path_t2568473163_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Path_t2568473163_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Path_t2568473163_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType ChildKey_t1197802383_0_0_0;
static const Il2CppType* GenInst_ChildKey_t1197802383_0_0_0_Il2CppObject_0_0_0_Types[] = { &ChildKey_t1197802383_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ChildKey_t1197802383_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ChildKey_t1197802383_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1211005109_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1211005109_0_0_0_Types[] = { &KeyValuePair_2_t1211005109_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1211005109_0_0_0 = { 1, GenInst_KeyValuePair_2_t1211005109_0_0_0_Types };
static const Il2CppType* GenInst_ChildKey_t1197802383_0_0_0_Types[] = { &ChildKey_t1197802383_0_0_0 };
extern const Il2CppGenericInst GenInst_ChildKey_t1197802383_0_0_0 = { 1, GenInst_ChildKey_t1197802383_0_0_0_Types };
extern const Il2CppType IComparable_1_t3029641606_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3029641606_0_0_0_Types[] = { &IComparable_1_t3029641606_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3029641606_0_0_0 = { 1, GenInst_IComparable_1_t3029641606_0_0_0_Types };
static const Il2CppType* GenInst_Node_t2640059010_0_0_0_Il2CppObject_0_0_0_Types[] = { &Node_t2640059010_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Node_t2640059010_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Node_t2640059010_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Path_t2568473163_0_0_0_Types[] = { &Path_t2568473163_0_0_0 };
extern const Il2CppGenericInst GenInst_Path_t2568473163_0_0_0 = { 1, GenInst_Path_t2568473163_0_0_0_Types };
extern const Il2CppType CompoundWrite_t496419158_0_0_0;
static const Il2CppType* GenInst_Node_t2640059010_0_0_0_CompoundWrite_t496419158_0_0_0_Types[] = { &Node_t2640059010_0_0_0, &CompoundWrite_t496419158_0_0_0 };
extern const Il2CppGenericInst GenInst_Node_t2640059010_0_0_0_CompoundWrite_t496419158_0_0_0 = { 2, GenInst_Node_t2640059010_0_0_0_CompoundWrite_t496419158_0_0_0_Types };
extern const Il2CppType NamedNode_t787885785_0_0_0;
static const Il2CppType* GenInst_NamedNode_t787885785_0_0_0_Types[] = { &NamedNode_t787885785_0_0_0 };
extern const Il2CppGenericInst GenInst_NamedNode_t787885785_0_0_0 = { 1, GenInst_NamedNode_t787885785_0_0_0_Types };
static const Il2CppType* GenInst_ChildKey_t1197802383_0_0_0_CompoundWrite_t496419158_0_0_0_Types[] = { &ChildKey_t1197802383_0_0_0, &CompoundWrite_t496419158_0_0_0 };
extern const Il2CppGenericInst GenInst_ChildKey_t1197802383_0_0_0_CompoundWrite_t496419158_0_0_0 = { 2, GenInst_ChildKey_t1197802383_0_0_0_CompoundWrite_t496419158_0_0_0_Types };
static const Il2CppType* GenInst_CompoundWrite_t496419158_0_0_0_Types[] = { &CompoundWrite_t496419158_0_0_0 };
extern const Il2CppGenericInst GenInst_CompoundWrite_t496419158_0_0_0 = { 1, GenInst_CompoundWrite_t496419158_0_0_0_Types };
static const Il2CppType* GenInst_ChildKey_t1197802383_0_0_0_CompoundWrite_t496419158_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ChildKey_t1197802383_0_0_0, &CompoundWrite_t496419158_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ChildKey_t1197802383_0_0_0_CompoundWrite_t496419158_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ChildKey_t1197802383_0_0_0_CompoundWrite_t496419158_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3312942268_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3312942268_0_0_0_Types[] = { &KeyValuePair_2_t3312942268_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3312942268_0_0_0 = { 1, GenInst_KeyValuePair_2_t3312942268_0_0_0_Types };
extern const Il2CppType IList_1_t2684453066_0_0_0;
static const Il2CppType* GenInst_IList_1_t2684453066_0_0_0_Types[] = { &IList_1_t2684453066_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t2684453066_0_0_0 = { 1, GenInst_IList_1_t2684453066_0_0_0_Types };
extern const Il2CppType TransactionData_t2143512465_0_0_0;
static const Il2CppType* GenInst_TransactionData_t2143512465_0_0_0_Types[] = { &TransactionData_t2143512465_0_0_0 };
extern const Il2CppGenericInst GenInst_TransactionData_t2143512465_0_0_0 = { 1, GenInst_TransactionData_t2143512465_0_0_0_Types };
static const Il2CppType* GenInst_ChildKey_t1197802383_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ChildKey_t1197802383_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ChildKey_t1197802383_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ChildKey_t1197802383_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType Event_t732806402_0_0_0;
static const Il2CppType* GenInst_Event_t732806402_0_0_0_Types[] = { &Event_t732806402_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t732806402_0_0_0 = { 1, GenInst_Event_t732806402_0_0_0_Types };
static const Il2CppType* GenInst_Path_t2568473163_0_0_0_Node_t2640059010_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Path_t2568473163_0_0_0, &Node_t2640059010_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Path_t2568473163_0_0_0_Node_t2640059010_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Path_t2568473163_0_0_0_Node_t2640059010_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType RangeMerge_t2434126241_0_0_0;
static const Il2CppType* GenInst_RangeMerge_t2434126241_0_0_0_Types[] = { &RangeMerge_t2434126241_0_0_0 };
extern const Il2CppGenericInst GenInst_RangeMerge_t2434126241_0_0_0 = { 1, GenInst_RangeMerge_t2434126241_0_0_0_Types };
extern const Il2CppType UserWriteRecord_t388677579_0_0_0;
static const Il2CppType* GenInst_UserWriteRecord_t388677579_0_0_0_Types[] = { &UserWriteRecord_t388677579_0_0_0 };
extern const Il2CppGenericInst GenInst_UserWriteRecord_t388677579_0_0_0 = { 1, GenInst_UserWriteRecord_t388677579_0_0_0_Types };
extern const Il2CppType DataEvent_t3330191602_0_0_0;
static const Il2CppType* GenInst_DataEvent_t3330191602_0_0_0_Types[] = { &DataEvent_t3330191602_0_0_0 };
extern const Il2CppGenericInst GenInst_DataEvent_t3330191602_0_0_0 = { 1, GenInst_DataEvent_t3330191602_0_0_0_Types };
extern const Il2CppType Context_t3486154757_0_0_0;
extern const Il2CppType IDictionary_2_t1158171145_0_0_0;
static const Il2CppType* GenInst_Context_t3486154757_0_0_0_IDictionary_2_t1158171145_0_0_0_Types[] = { &Context_t3486154757_0_0_0, &IDictionary_2_t1158171145_0_0_0 };
extern const Il2CppGenericInst GenInst_Context_t3486154757_0_0_0_IDictionary_2_t1158171145_0_0_0 = { 2, GenInst_Context_t3486154757_0_0_0_IDictionary_2_t1158171145_0_0_0_Types };
extern const Il2CppType Repo_t1244308462_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Repo_t1244308462_0_0_0_Types[] = { &String_t_0_0_0, &Repo_t1244308462_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Repo_t1244308462_0_0_0 = { 2, GenInst_String_t_0_0_0_Repo_t1244308462_0_0_0_Types };
static const Il2CppType* GenInst_Repo_t1244308462_0_0_0_Types[] = { &Repo_t1244308462_0_0_0 };
extern const Il2CppGenericInst GenInst_Repo_t1244308462_0_0_0 = { 1, GenInst_Repo_t1244308462_0_0_0_Types };
static const Il2CppType* GenInst_Context_t3486154757_0_0_0_Types[] = { &Context_t3486154757_0_0_0 };
extern const Il2CppGenericInst GenInst_Context_t3486154757_0_0_0 = { 1, GenInst_Context_t3486154757_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t1158171145_0_0_0_Types[] = { &IDictionary_2_t1158171145_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t1158171145_0_0_0 = { 1, GenInst_IDictionary_2_t1158171145_0_0_0_Types };
static const Il2CppType* GenInst_Context_t3486154757_0_0_0_IDictionary_2_t1158171145_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Context_t3486154757_0_0_0, &IDictionary_2_t1158171145_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Context_t3486154757_0_0_0_IDictionary_2_t1158171145_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Context_t3486154757_0_0_0_IDictionary_2_t1158171145_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1203317345_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1203317345_0_0_0_Types[] = { &KeyValuePair_2_t1203317345_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1203317345_0_0_0 = { 1, GenInst_KeyValuePair_2_t1203317345_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Repo_t1244308462_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Repo_t1244308462_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Repo_t1244308462_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Repo_t1244308462_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t916432946_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t916432946_0_0_0_Types[] = { &KeyValuePair_2_t916432946_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t916432946_0_0_0 = { 1, GenInst_KeyValuePair_2_t916432946_0_0_0_Types };
extern const Il2CppType SparseSnapshotTree_t504080338_0_0_0;
static const Il2CppType* GenInst_ChildKey_t1197802383_0_0_0_SparseSnapshotTree_t504080338_0_0_0_Types[] = { &ChildKey_t1197802383_0_0_0, &SparseSnapshotTree_t504080338_0_0_0 };
extern const Il2CppGenericInst GenInst_ChildKey_t1197802383_0_0_0_SparseSnapshotTree_t504080338_0_0_0 = { 2, GenInst_ChildKey_t1197802383_0_0_0_SparseSnapshotTree_t504080338_0_0_0_Types };
static const Il2CppType* GenInst_SparseSnapshotTree_t504080338_0_0_0_Types[] = { &SparseSnapshotTree_t504080338_0_0_0 };
extern const Il2CppGenericInst GenInst_SparseSnapshotTree_t504080338_0_0_0 = { 1, GenInst_SparseSnapshotTree_t504080338_0_0_0_Types };
static const Il2CppType* GenInst_ChildKey_t1197802383_0_0_0_SparseSnapshotTree_t504080338_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ChildKey_t1197802383_0_0_0, &SparseSnapshotTree_t504080338_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ChildKey_t1197802383_0_0_0_SparseSnapshotTree_t504080338_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ChildKey_t1197802383_0_0_0_SparseSnapshotTree_t504080338_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3320603448_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3320603448_0_0_0_Types[] = { &KeyValuePair_2_t3320603448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3320603448_0_0_0 = { 1, GenInst_KeyValuePair_2_t3320603448_0_0_0_Types };
extern const Il2CppType QueryParams_t526937568_0_0_0;
extern const Il2CppType View_t798282663_0_0_0;
static const Il2CppType* GenInst_QueryParams_t526937568_0_0_0_View_t798282663_0_0_0_Types[] = { &QueryParams_t526937568_0_0_0, &View_t798282663_0_0_0 };
extern const Il2CppGenericInst GenInst_QueryParams_t526937568_0_0_0_View_t798282663_0_0_0 = { 2, GenInst_QueryParams_t526937568_0_0_0_View_t798282663_0_0_0_Types };
static const Il2CppType* GenInst_QueryParams_t526937568_0_0_0_Types[] = { &QueryParams_t526937568_0_0_0 };
extern const Il2CppGenericInst GenInst_QueryParams_t526937568_0_0_0 = { 1, GenInst_QueryParams_t526937568_0_0_0_Types };
static const Il2CppType* GenInst_View_t798282663_0_0_0_Types[] = { &View_t798282663_0_0_0 };
extern const Il2CppGenericInst GenInst_View_t798282663_0_0_0 = { 1, GenInst_View_t798282663_0_0_0_Types };
static const Il2CppType* GenInst_QueryParams_t526937568_0_0_0_View_t798282663_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &QueryParams_t526937568_0_0_0, &View_t798282663_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_QueryParams_t526937568_0_0_0_View_t798282663_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_QueryParams_t526937568_0_0_0_View_t798282663_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2059584600_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2059584600_0_0_0_Types[] = { &KeyValuePair_2_t2059584600_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2059584600_0_0_0 = { 1, GenInst_KeyValuePair_2_t2059584600_0_0_0_Types };
extern const Il2CppType Change_t639587248_0_0_0;
static const Il2CppType* GenInst_Change_t639587248_0_0_0_Types[] = { &Change_t639587248_0_0_0 };
extern const Il2CppGenericInst GenInst_Change_t639587248_0_0_0 = { 1, GenInst_Change_t639587248_0_0_0_Types };
extern const Il2CppType IList_1_t918499312_0_0_0;
extern const Il2CppType IList_1_t1273747003_0_0_0;
static const Il2CppType* GenInst_IList_1_t918499312_0_0_0_IList_1_t1273747003_0_0_0_Types[] = { &IList_1_t918499312_0_0_0, &IList_1_t1273747003_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t918499312_0_0_0_IList_1_t1273747003_0_0_0 = { 2, GenInst_IList_1_t918499312_0_0_0_IList_1_t1273747003_0_0_0_Types };
extern const Il2CppType QuerySpec_t377558711_0_0_0;
static const Il2CppType* GenInst_QuerySpec_t377558711_0_0_0_Types[] = { &QuerySpec_t377558711_0_0_0 };
extern const Il2CppGenericInst GenInst_QuerySpec_t377558711_0_0_0 = { 1, GenInst_QuerySpec_t377558711_0_0_0_Types };
extern const Il2CppType Tag_t2439924210_0_0_0;
static const Il2CppType* GenInst_QuerySpec_t377558711_0_0_0_Tag_t2439924210_0_0_0_Types[] = { &QuerySpec_t377558711_0_0_0, &Tag_t2439924210_0_0_0 };
extern const Il2CppGenericInst GenInst_QuerySpec_t377558711_0_0_0_Tag_t2439924210_0_0_0 = { 2, GenInst_QuerySpec_t377558711_0_0_0_Tag_t2439924210_0_0_0_Types };
static const Il2CppType* GenInst_Tag_t2439924210_0_0_0_Types[] = { &Tag_t2439924210_0_0_0 };
extern const Il2CppGenericInst GenInst_Tag_t2439924210_0_0_0 = { 1, GenInst_Tag_t2439924210_0_0_0_Types };
static const Il2CppType* GenInst_Tag_t2439924210_0_0_0_QuerySpec_t377558711_0_0_0_Types[] = { &Tag_t2439924210_0_0_0, &QuerySpec_t377558711_0_0_0 };
extern const Il2CppGenericInst GenInst_Tag_t2439924210_0_0_0_QuerySpec_t377558711_0_0_0 = { 2, GenInst_Tag_t2439924210_0_0_0_QuerySpec_t377558711_0_0_0_Types };
extern const Il2CppType SyncPoint_t2720557329_0_0_0;
static const Il2CppType* GenInst_SyncPoint_t2720557329_0_0_0_Types[] = { &SyncPoint_t2720557329_0_0_0 };
extern const Il2CppGenericInst GenInst_SyncPoint_t2720557329_0_0_0 = { 1, GenInst_SyncPoint_t2720557329_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4082376523_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4082376523_0_0_0_Types[] = { &KeyValuePair_2_t4082376523_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4082376523_0_0_0 = { 1, GenInst_KeyValuePair_2_t4082376523_0_0_0_Types };
static const Il2CppType* GenInst_Path_t2568473163_0_0_0_SyncPoint_t2720557329_0_0_0_Types[] = { &Path_t2568473163_0_0_0, &SyncPoint_t2720557329_0_0_0 };
extern const Il2CppGenericInst GenInst_Path_t2568473163_0_0_0_SyncPoint_t2720557329_0_0_0 = { 2, GenInst_Path_t2568473163_0_0_0_SyncPoint_t2720557329_0_0_0_Types };
static const Il2CppType* GenInst_SyncPoint_t2720557329_0_0_0_Il2CppObject_0_0_0_Types[] = { &SyncPoint_t2720557329_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_SyncPoint_t2720557329_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_SyncPoint_t2720557329_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Tag_t2439924210_0_0_0_QuerySpec_t377558711_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Tag_t2439924210_0_0_0, &QuerySpec_t377558711_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Tag_t2439924210_0_0_0_QuerySpec_t377558711_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Tag_t2439924210_0_0_0_QuerySpec_t377558711_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2206838478_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2206838478_0_0_0_Types[] = { &KeyValuePair_2_t2206838478_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2206838478_0_0_0 = { 1, GenInst_KeyValuePair_2_t2206838478_0_0_0_Types };
static const Il2CppType* GenInst_QuerySpec_t377558711_0_0_0_Tag_t2439924210_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &QuerySpec_t377558711_0_0_0, &Tag_t2439924210_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_QuerySpec_t377558711_0_0_0_Tag_t2439924210_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_QuerySpec_t377558711_0_0_0_Tag_t2439924210_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1065531536_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1065531536_0_0_0_Types[] = { &KeyValuePair_2_t1065531536_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1065531536_0_0_0 = { 1, GenInst_KeyValuePair_2_t1065531536_0_0_0_Types };
static const Il2CppType* GenInst_IList_1_t1273747003_0_0_0_Types[] = { &IList_1_t1273747003_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1273747003_0_0_0 = { 1, GenInst_IList_1_t1273747003_0_0_0_Types };
extern const Il2CppType IList_1_t3871132203_0_0_0;
static const Il2CppType* GenInst_IList_1_t3871132203_0_0_0_Types[] = { &IList_1_t3871132203_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3871132203_0_0_0 = { 1, GenInst_IList_1_t3871132203_0_0_0_Types };
extern const Il2CppType Nullable_1_t2088641033_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t2088641033_0_0_0_Types[] = { &Nullable_1_t2088641033_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t2088641033_0_0_0 = { 1, GenInst_Nullable_1_t2088641033_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3450460227_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3450460227_0_0_0_Types[] = { &KeyValuePair_2_t3450460227_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3450460227_0_0_0 = { 1, GenInst_KeyValuePair_2_t3450460227_0_0_0_Types };
static const Il2CppType* GenInst_Path_t2568473163_0_0_0_Nullable_1_t2088641033_0_0_0_Types[] = { &Path_t2568473163_0_0_0, &Nullable_1_t2088641033_0_0_0 };
extern const Il2CppGenericInst GenInst_Path_t2568473163_0_0_0_Nullable_1_t2088641033_0_0_0 = { 2, GenInst_Path_t2568473163_0_0_0_Nullable_1_t2088641033_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3733013679_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3733013679_0_0_0_Types[] = { &KeyValuePair_2_t3733013679_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3733013679_0_0_0 = { 1, GenInst_KeyValuePair_2_t3733013679_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Nullable_1_t2088641033_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Nullable_1_t2088641033_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Nullable_1_t2088641033_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Nullable_1_t2088641033_0_0_0_Types };
static const Il2CppType* GenInst_Nullable_1_t2088641033_0_0_0_Il2CppObject_0_0_0_Types[] = { &Nullable_1_t2088641033_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t2088641033_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Nullable_1_t2088641033_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType EventRegistration_t4222917807_0_0_0;
extern const Il2CppType IList_1_t468891112_0_0_0;
static const Il2CppType* GenInst_EventRegistration_t4222917807_0_0_0_IList_1_t468891112_0_0_0_Types[] = { &EventRegistration_t4222917807_0_0_0, &IList_1_t468891112_0_0_0 };
extern const Il2CppGenericInst GenInst_EventRegistration_t4222917807_0_0_0_IList_1_t468891112_0_0_0 = { 2, GenInst_EventRegistration_t4222917807_0_0_0_IList_1_t468891112_0_0_0_Types };
static const Il2CppType* GenInst_EventRegistration_t4222917807_0_0_0_Types[] = { &EventRegistration_t4222917807_0_0_0 };
extern const Il2CppGenericInst GenInst_EventRegistration_t4222917807_0_0_0 = { 1, GenInst_EventRegistration_t4222917807_0_0_0_Types };
static const Il2CppType* GenInst_EventRegistration_t4222917807_0_0_0_IList_1_t468891112_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &EventRegistration_t4222917807_0_0_0, &IList_1_t468891112_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_EventRegistration_t4222917807_0_0_0_IList_1_t468891112_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_EventRegistration_t4222917807_0_0_0_IList_1_t468891112_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_IList_1_t468891112_0_0_0_Types[] = { &IList_1_t468891112_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t468891112_0_0_0 = { 1, GenInst_IList_1_t468891112_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1813098414_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1813098414_0_0_0_Types[] = { &KeyValuePair_2_t1813098414_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1813098414_0_0_0 = { 1, GenInst_KeyValuePair_2_t1813098414_0_0_0_Types };
extern const Il2CppType ViewFrom_t2537511179_0_0_0;
static const Il2CppType* GenInst_ViewFrom_t2537511179_0_0_0_Types[] = { &ViewFrom_t2537511179_0_0_0 };
extern const Il2CppGenericInst GenInst_ViewFrom_t2537511179_0_0_0 = { 1, GenInst_ViewFrom_t2537511179_0_0_0_Types };
static const Il2CppType* GenInst_ChildKey_t1197802383_0_0_0_Change_t639587248_0_0_0_Types[] = { &ChildKey_t1197802383_0_0_0, &Change_t639587248_0_0_0 };
extern const Il2CppGenericInst GenInst_ChildKey_t1197802383_0_0_0_Change_t639587248_0_0_0 = { 2, GenInst_ChildKey_t1197802383_0_0_0_Change_t639587248_0_0_0_Types };
static const Il2CppType* GenInst_ChildKey_t1197802383_0_0_0_Change_t639587248_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ChildKey_t1197802383_0_0_0, &Change_t639587248_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ChildKey_t1197802383_0_0_0_Change_t639587248_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ChildKey_t1197802383_0_0_0_Change_t639587248_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3456110358_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3456110358_0_0_0_Types[] = { &KeyValuePair_2_t3456110358_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3456110358_0_0_0 = { 1, GenInst_KeyValuePair_2_t3456110358_0_0_0_Types };
static const Il2CppType* GenInst_ChildKey_t1197802383_0_0_0_Node_t2640059010_0_0_0_Types[] = { &ChildKey_t1197802383_0_0_0, &Node_t2640059010_0_0_0 };
extern const Il2CppGenericInst GenInst_ChildKey_t1197802383_0_0_0_Node_t2640059010_0_0_0 = { 2, GenInst_ChildKey_t1197802383_0_0_0_Node_t2640059010_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1161614824_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1161614824_0_0_0_Types[] = { &KeyValuePair_2_t1161614824_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1161614824_0_0_0 = { 1, GenInst_KeyValuePair_2_t1161614824_0_0_0_Types };
static const Il2CppType* GenInst_NamedNode_t787885785_0_0_0_Il2CppObject_0_0_0_Types[] = { &NamedNode_t787885785_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_NamedNode_t787885785_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_NamedNode_t787885785_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ChildKey_t1197802383_0_0_0_Node_t2640059010_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ChildKey_t1197802383_0_0_0, &Node_t2640059010_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ChildKey_t1197802383_0_0_0_Node_t2640059010_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ChildKey_t1197802383_0_0_0_Node_t2640059010_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType MemoryStream_t743994179_0_0_0;
static const Il2CppType* GenInst_MemoryStream_t743994179_0_0_0_Types[] = { &MemoryStream_t743994179_0_0_0 };
extern const Il2CppGenericInst GenInst_MemoryStream_t743994179_0_0_0 = { 1, GenInst_MemoryStream_t743994179_0_0_0_Types };
extern const Il2CppType DotNetPlatform_t2951135975_0_0_0;
static const Il2CppType* GenInst_FirebaseApp_t210707726_0_0_0_DotNetPlatform_t2951135975_0_0_0_Types[] = { &FirebaseApp_t210707726_0_0_0, &DotNetPlatform_t2951135975_0_0_0 };
extern const Il2CppGenericInst GenInst_FirebaseApp_t210707726_0_0_0_DotNetPlatform_t2951135975_0_0_0 = { 2, GenInst_FirebaseApp_t210707726_0_0_0_DotNetPlatform_t2951135975_0_0_0_Types };
extern const Il2CppType FirebaseDatabase_t1318758358_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_FirebaseDatabase_t1318758358_0_0_0_Types[] = { &String_t_0_0_0, &FirebaseDatabase_t1318758358_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FirebaseDatabase_t1318758358_0_0_0 = { 2, GenInst_String_t_0_0_0_FirebaseDatabase_t1318758358_0_0_0_Types };
static const Il2CppType* GenInst_FirebaseDatabase_t1318758358_0_0_0_Types[] = { &FirebaseDatabase_t1318758358_0_0_0 };
extern const Il2CppGenericInst GenInst_FirebaseDatabase_t1318758358_0_0_0 = { 1, GenInst_FirebaseDatabase_t1318758358_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_FirebaseDatabase_t1318758358_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &FirebaseDatabase_t1318758358_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FirebaseDatabase_t1318758358_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_FirebaseDatabase_t1318758358_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t990882842_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t990882842_0_0_0_Types[] = { &KeyValuePair_2_t990882842_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t990882842_0_0_0 = { 1, GenInst_KeyValuePair_2_t990882842_0_0_0_Types };
static const Il2CppType* GenInst_Task_1_t1149249240_0_0_0_String_t_0_0_0_Types[] = { &Task_1_t1149249240_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t1149249240_0_0_0_String_t_0_0_0 = { 2, GenInst_Task_1_t1149249240_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType ArrayList_t4252133567_0_0_0;
static const Il2CppType* GenInst_ArrayList_t4252133567_0_0_0_Types[] = { &ArrayList_t4252133567_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayList_t4252133567_0_0_0 = { 1, GenInst_ArrayList_t4252133567_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MemberInfo_t_0_0_0_Types[] = { &String_t_0_0_0, &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MemberInfo_t_0_0_0 = { 2, GenInst_String_t_0_0_0_MemberInfo_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_MemberInfo_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &MemberInfo_t_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_MemberInfo_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_MemberInfo_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3715221744_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3715221744_0_0_0_Types[] = { &KeyValuePair_2_t3715221744_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3715221744_0_0_0 = { 1, GenInst_KeyValuePair_2_t3715221744_0_0_0_Types };
extern const Il2CppType JsonConverter_t4092422604_0_0_0;
static const Il2CppType* GenInst_JsonConverter_t4092422604_0_0_0_Types[] = { &JsonConverter_t4092422604_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonConverter_t4092422604_0_0_0 = { 1, GenInst_JsonConverter_t4092422604_0_0_0_Types };
extern const Il2CppType Dictionary_2_t1662909226_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Dictionary_2_t1662909226_0_0_0_Types[] = { &Type_t_0_0_0, &Dictionary_2_t1662909226_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Dictionary_2_t1662909226_0_0_0 = { 2, GenInst_Type_t_0_0_0_Dictionary_2_t1662909226_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Dictionary_2_t1662909226_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Type_t_0_0_0, &Dictionary_2_t1662909226_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Dictionary_2_t1662909226_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_Dictionary_2_t1662909226_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t1662909226_0_0_0_Types[] = { &Dictionary_2_t1662909226_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1662909226_0_0_0 = { 1, GenInst_Dictionary_2_t1662909226_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1357612345_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1357612345_0_0_0_Types[] = { &KeyValuePair_2_t1357612345_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1357612345_0_0_0 = { 1, GenInst_KeyValuePair_2_t1357612345_0_0_0_Types };
extern const Il2CppType BaseInputModule_t1295781545_0_0_0;
static const Il2CppType* GenInst_BaseInputModule_t1295781545_0_0_0_Types[] = { &BaseInputModule_t1295781545_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInputModule_t1295781545_0_0_0 = { 1, GenInst_BaseInputModule_t1295781545_0_0_0_Types };
extern const Il2CppType RaycastResult_t21186376_0_0_0;
static const Il2CppType* GenInst_RaycastResult_t21186376_0_0_0_Types[] = { &RaycastResult_t21186376_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t21186376_0_0_0 = { 1, GenInst_RaycastResult_t21186376_0_0_0_Types };
extern const Il2CppType IDeselectHandler_t3182198310_0_0_0;
static const Il2CppType* GenInst_IDeselectHandler_t3182198310_0_0_0_Types[] = { &IDeselectHandler_t3182198310_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t3182198310_0_0_0 = { 1, GenInst_IDeselectHandler_t3182198310_0_0_0_Types };
extern const Il2CppType IEventSystemHandler_t2741188318_0_0_0;
static const Il2CppType* GenInst_IEventSystemHandler_t2741188318_0_0_0_Types[] = { &IEventSystemHandler_t2741188318_0_0_0 };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t2741188318_0_0_0 = { 1, GenInst_IEventSystemHandler_t2741188318_0_0_0_Types };
extern const Il2CppType List_1_t2110309450_0_0_0;
static const Il2CppType* GenInst_List_1_t2110309450_0_0_0_Types[] = { &List_1_t2110309450_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2110309450_0_0_0 = { 1, GenInst_List_1_t2110309450_0_0_0_Types };
extern const Il2CppType List_1_t3188497603_0_0_0;
static const Il2CppType* GenInst_List_1_t3188497603_0_0_0_Types[] = { &List_1_t3188497603_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3188497603_0_0_0 = { 1, GenInst_List_1_t3188497603_0_0_0_Types };
extern const Il2CppType ISelectHandler_t2812555161_0_0_0;
static const Il2CppType* GenInst_ISelectHandler_t2812555161_0_0_0_Types[] = { &ISelectHandler_t2812555161_0_0_0 };
extern const Il2CppGenericInst GenInst_ISelectHandler_t2812555161_0_0_0 = { 1, GenInst_ISelectHandler_t2812555161_0_0_0_Types };
extern const Il2CppType BaseRaycaster_t2336171397_0_0_0;
static const Il2CppType* GenInst_BaseRaycaster_t2336171397_0_0_0_Types[] = { &BaseRaycaster_t2336171397_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t2336171397_0_0_0 = { 1, GenInst_BaseRaycaster_t2336171397_0_0_0_Types };
extern const Il2CppType Entry_t3365010046_0_0_0;
static const Il2CppType* GenInst_Entry_t3365010046_0_0_0_Types[] = { &Entry_t3365010046_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t3365010046_0_0_0 = { 1, GenInst_Entry_t3365010046_0_0_0_Types };
extern const Il2CppType BaseEventData_t2681005625_0_0_0;
static const Il2CppType* GenInst_BaseEventData_t2681005625_0_0_0_Types[] = { &BaseEventData_t2681005625_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseEventData_t2681005625_0_0_0 = { 1, GenInst_BaseEventData_t2681005625_0_0_0_Types };
extern const Il2CppType IPointerEnterHandler_t193164956_0_0_0;
static const Il2CppType* GenInst_IPointerEnterHandler_t193164956_0_0_0_Types[] = { &IPointerEnterHandler_t193164956_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t193164956_0_0_0 = { 1, GenInst_IPointerEnterHandler_t193164956_0_0_0_Types };
extern const Il2CppType IPointerExitHandler_t461019860_0_0_0;
static const Il2CppType* GenInst_IPointerExitHandler_t461019860_0_0_0_Types[] = { &IPointerExitHandler_t461019860_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t461019860_0_0_0 = { 1, GenInst_IPointerExitHandler_t461019860_0_0_0_Types };
extern const Il2CppType IPointerDownHandler_t3929046918_0_0_0;
static const Il2CppType* GenInst_IPointerDownHandler_t3929046918_0_0_0_Types[] = { &IPointerDownHandler_t3929046918_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t3929046918_0_0_0 = { 1, GenInst_IPointerDownHandler_t3929046918_0_0_0_Types };
extern const Il2CppType IPointerUpHandler_t1847764461_0_0_0;
static const Il2CppType* GenInst_IPointerUpHandler_t1847764461_0_0_0_Types[] = { &IPointerUpHandler_t1847764461_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t1847764461_0_0_0 = { 1, GenInst_IPointerUpHandler_t1847764461_0_0_0_Types };
extern const Il2CppType IPointerClickHandler_t96169666_0_0_0;
static const Il2CppType* GenInst_IPointerClickHandler_t96169666_0_0_0_Types[] = { &IPointerClickHandler_t96169666_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t96169666_0_0_0 = { 1, GenInst_IPointerClickHandler_t96169666_0_0_0_Types };
extern const Il2CppType IInitializePotentialDragHandler_t3350809087_0_0_0;
static const Il2CppType* GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0_Types[] = { &IInitializePotentialDragHandler_t3350809087_0_0_0 };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0_Types };
extern const Il2CppType IBeginDragHandler_t3135127860_0_0_0;
static const Il2CppType* GenInst_IBeginDragHandler_t3135127860_0_0_0_Types[] = { &IBeginDragHandler_t3135127860_0_0_0 };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t3135127860_0_0_0 = { 1, GenInst_IBeginDragHandler_t3135127860_0_0_0_Types };
extern const Il2CppType IDragHandler_t2583993319_0_0_0;
static const Il2CppType* GenInst_IDragHandler_t2583993319_0_0_0_Types[] = { &IDragHandler_t2583993319_0_0_0 };
extern const Il2CppGenericInst GenInst_IDragHandler_t2583993319_0_0_0 = { 1, GenInst_IDragHandler_t2583993319_0_0_0_Types };
extern const Il2CppType IEndDragHandler_t1349123600_0_0_0;
static const Il2CppType* GenInst_IEndDragHandler_t1349123600_0_0_0_Types[] = { &IEndDragHandler_t1349123600_0_0_0 };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t1349123600_0_0_0 = { 1, GenInst_IEndDragHandler_t1349123600_0_0_0_Types };
extern const Il2CppType IDropHandler_t2390101210_0_0_0;
static const Il2CppType* GenInst_IDropHandler_t2390101210_0_0_0_Types[] = { &IDropHandler_t2390101210_0_0_0 };
extern const Il2CppGenericInst GenInst_IDropHandler_t2390101210_0_0_0 = { 1, GenInst_IDropHandler_t2390101210_0_0_0_Types };
extern const Il2CppType IScrollHandler_t3834677510_0_0_0;
static const Il2CppType* GenInst_IScrollHandler_t3834677510_0_0_0_Types[] = { &IScrollHandler_t3834677510_0_0_0 };
extern const Il2CppGenericInst GenInst_IScrollHandler_t3834677510_0_0_0 = { 1, GenInst_IScrollHandler_t3834677510_0_0_0_Types };
extern const Il2CppType IUpdateSelectedHandler_t3778909353_0_0_0;
static const Il2CppType* GenInst_IUpdateSelectedHandler_t3778909353_0_0_0_Types[] = { &IUpdateSelectedHandler_t3778909353_0_0_0 };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t3778909353_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t3778909353_0_0_0_Types };
extern const Il2CppType IMoveHandler_t2611925506_0_0_0;
static const Il2CppType* GenInst_IMoveHandler_t2611925506_0_0_0_Types[] = { &IMoveHandler_t2611925506_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoveHandler_t2611925506_0_0_0 = { 1, GenInst_IMoveHandler_t2611925506_0_0_0_Types };
extern const Il2CppType ISubmitHandler_t525803901_0_0_0;
static const Il2CppType* GenInst_ISubmitHandler_t525803901_0_0_0_Types[] = { &ISubmitHandler_t525803901_0_0_0 };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t525803901_0_0_0 = { 1, GenInst_ISubmitHandler_t525803901_0_0_0_Types };
extern const Il2CppType ICancelHandler_t1980319651_0_0_0;
static const Il2CppType* GenInst_ICancelHandler_t1980319651_0_0_0_Types[] = { &ICancelHandler_t1980319651_0_0_0 };
extern const Il2CppGenericInst GenInst_ICancelHandler_t1980319651_0_0_0 = { 1, GenInst_ICancelHandler_t1980319651_0_0_0_Types };
extern const Il2CppType Transform_t3275118058_0_0_0;
static const Il2CppType* GenInst_Transform_t3275118058_0_0_0_Types[] = { &Transform_t3275118058_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t3275118058_0_0_0 = { 1, GenInst_Transform_t3275118058_0_0_0_Types };
extern const Il2CppType GameObject_t1756533147_0_0_0;
static const Il2CppType* GenInst_GameObject_t1756533147_0_0_0_Types[] = { &GameObject_t1756533147_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0 = { 1, GenInst_GameObject_t1756533147_0_0_0_Types };
extern const Il2CppType BaseInput_t621514313_0_0_0;
static const Il2CppType* GenInst_BaseInput_t621514313_0_0_0_Types[] = { &BaseInput_t621514313_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInput_t621514313_0_0_0 = { 1, GenInst_BaseInput_t621514313_0_0_0_Types };
extern const Il2CppType UIBehaviour_t3960014691_0_0_0;
static const Il2CppType* GenInst_UIBehaviour_t3960014691_0_0_0_Types[] = { &UIBehaviour_t3960014691_0_0_0 };
extern const Il2CppGenericInst GenInst_UIBehaviour_t3960014691_0_0_0 = { 1, GenInst_UIBehaviour_t3960014691_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t1158329972_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t1158329972_0_0_0_Types[] = { &MonoBehaviour_t1158329972_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t1158329972_0_0_0 = { 1, GenInst_MonoBehaviour_t1158329972_0_0_0_Types };
extern const Il2CppType PointerEventData_t1599784723_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &PointerEventData_t1599784723_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &PointerEventData_t1599784723_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_PointerEventData_t1599784723_0_0_0_Types[] = { &PointerEventData_t1599784723_0_0_0 };
extern const Il2CppGenericInst GenInst_PointerEventData_t1599784723_0_0_0 = { 1, GenInst_PointerEventData_t1599784723_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2659922876_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2659922876_0_0_0_Types[] = { &KeyValuePair_2_t2659922876_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2659922876_0_0_0 = { 1, GenInst_KeyValuePair_2_t2659922876_0_0_0_Types };
extern const Il2CppType ButtonState_t2688375492_0_0_0;
static const Il2CppType* GenInst_ButtonState_t2688375492_0_0_0_Types[] = { &ButtonState_t2688375492_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonState_t2688375492_0_0_0 = { 1, GenInst_ButtonState_t2688375492_0_0_0_Types };
extern const Il2CppType ICanvasElement_t986520779_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0 = { 1, GenInst_ICanvasElement_t986520779_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ICanvasElement_t986520779_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType ColorBlock_t2652774230_0_0_0;
static const Il2CppType* GenInst_ColorBlock_t2652774230_0_0_0_Types[] = { &ColorBlock_t2652774230_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorBlock_t2652774230_0_0_0 = { 1, GenInst_ColorBlock_t2652774230_0_0_0_Types };
extern const Il2CppType OptionData_t2420267500_0_0_0;
static const Il2CppType* GenInst_OptionData_t2420267500_0_0_0_Types[] = { &OptionData_t2420267500_0_0_0 };
extern const Il2CppGenericInst GenInst_OptionData_t2420267500_0_0_0 = { 1, GenInst_OptionData_t2420267500_0_0_0_Types };
extern const Il2CppType DropdownItem_t4139978805_0_0_0;
static const Il2CppType* GenInst_DropdownItem_t4139978805_0_0_0_Types[] = { &DropdownItem_t4139978805_0_0_0 };
extern const Il2CppGenericInst GenInst_DropdownItem_t4139978805_0_0_0 = { 1, GenInst_DropdownItem_t4139978805_0_0_0_Types };
extern const Il2CppType FloatTween_t2986189219_0_0_0;
static const Il2CppType* GenInst_FloatTween_t2986189219_0_0_0_Types[] = { &FloatTween_t2986189219_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatTween_t2986189219_0_0_0 = { 1, GenInst_FloatTween_t2986189219_0_0_0_Types };
extern const Il2CppType Sprite_t309593783_0_0_0;
static const Il2CppType* GenInst_Sprite_t309593783_0_0_0_Types[] = { &Sprite_t309593783_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t309593783_0_0_0 = { 1, GenInst_Sprite_t309593783_0_0_0_Types };
extern const Il2CppType Canvas_t209405766_0_0_0;
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_Types[] = { &Canvas_t209405766_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0 = { 1, GenInst_Canvas_t209405766_0_0_0_Types };
extern const Il2CppType List_1_t3873494194_0_0_0;
static const Il2CppType* GenInst_List_1_t3873494194_0_0_0_Types[] = { &List_1_t3873494194_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3873494194_0_0_0 = { 1, GenInst_List_1_t3873494194_0_0_0_Types };
extern const Il2CppType HashSet_1_t2984649583_0_0_0;
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_Types[] = { &Font_t4239498691_0_0_0, &HashSet_1_t2984649583_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0 = { 2, GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_Types };
extern const Il2CppType Text_t356221433_0_0_0;
static const Il2CppType* GenInst_Text_t356221433_0_0_0_Types[] = { &Text_t356221433_0_0_0 };
extern const Il2CppGenericInst GenInst_Text_t356221433_0_0_0 = { 1, GenInst_Text_t356221433_0_0_0_Types };
static const Il2CppType* GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Font_t4239498691_0_0_0, &HashSet_1_t2984649583_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_HashSet_1_t2984649583_0_0_0_Types[] = { &HashSet_1_t2984649583_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t2984649583_0_0_0 = { 1, GenInst_HashSet_1_t2984649583_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t850112849_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t850112849_0_0_0_Types[] = { &KeyValuePair_2_t850112849_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t850112849_0_0_0 = { 1, GenInst_KeyValuePair_2_t850112849_0_0_0_Types };
extern const Il2CppType ColorTween_t3438117476_0_0_0;
static const Il2CppType* GenInst_ColorTween_t3438117476_0_0_0_Types[] = { &ColorTween_t3438117476_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorTween_t3438117476_0_0_0 = { 1, GenInst_ColorTween_t3438117476_0_0_0_Types };
extern const Il2CppType Graphic_t2426225576_0_0_0;
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0 = { 1, GenInst_Graphic_t2426225576_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t286373651_0_0_0;
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_Types[] = { &Canvas_t209405766_0_0_0, &IndexedSet_1_t286373651_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0 = { 2, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Graphic_t2426225576_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Canvas_t209405766_0_0_0, &IndexedSet_1_t286373651_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t286373651_0_0_0_Types[] = { &IndexedSet_1_t286373651_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t286373651_0_0_0 = { 1, GenInst_IndexedSet_1_t286373651_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2391682566_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2391682566_0_0_0_Types[] = { &KeyValuePair_2_t2391682566_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2391682566_0_0_0 = { 1, GenInst_KeyValuePair_2_t2391682566_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3010968081_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3010968081_0_0_0_Types[] = { &KeyValuePair_2_t3010968081_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3010968081_0_0_0 = { 1, GenInst_KeyValuePair_2_t3010968081_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1912381698_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1912381698_0_0_0_Types[] = { &KeyValuePair_2_t1912381698_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1912381698_0_0_0 = { 1, GenInst_KeyValuePair_2_t1912381698_0_0_0_Types };
extern const Il2CppType Type_t3352948571_0_0_0;
static const Il2CppType* GenInst_Type_t3352948571_0_0_0_Types[] = { &Type_t3352948571_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t3352948571_0_0_0 = { 1, GenInst_Type_t3352948571_0_0_0_Types };
extern const Il2CppType FillMethod_t1640962579_0_0_0;
static const Il2CppType* GenInst_FillMethod_t1640962579_0_0_0_Types[] = { &FillMethod_t1640962579_0_0_0 };
extern const Il2CppGenericInst GenInst_FillMethod_t1640962579_0_0_0 = { 1, GenInst_FillMethod_t1640962579_0_0_0_Types };
extern const Il2CppType ContentType_t1028629049_0_0_0;
static const Il2CppType* GenInst_ContentType_t1028629049_0_0_0_Types[] = { &ContentType_t1028629049_0_0_0 };
extern const Il2CppGenericInst GenInst_ContentType_t1028629049_0_0_0 = { 1, GenInst_ContentType_t1028629049_0_0_0_Types };
extern const Il2CppType LineType_t2931319356_0_0_0;
static const Il2CppType* GenInst_LineType_t2931319356_0_0_0_Types[] = { &LineType_t2931319356_0_0_0 };
extern const Il2CppGenericInst GenInst_LineType_t2931319356_0_0_0 = { 1, GenInst_LineType_t2931319356_0_0_0_Types };
extern const Il2CppType InputType_t1274231802_0_0_0;
static const Il2CppType* GenInst_InputType_t1274231802_0_0_0_Types[] = { &InputType_t1274231802_0_0_0 };
extern const Il2CppGenericInst GenInst_InputType_t1274231802_0_0_0 = { 1, GenInst_InputType_t1274231802_0_0_0_Types };
extern const Il2CppType TouchScreenKeyboardType_t875112366_0_0_0;
static const Il2CppType* GenInst_TouchScreenKeyboardType_t875112366_0_0_0_Types[] = { &TouchScreenKeyboardType_t875112366_0_0_0 };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t875112366_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t875112366_0_0_0_Types };
extern const Il2CppType CharacterValidation_t3437478890_0_0_0;
static const Il2CppType* GenInst_CharacterValidation_t3437478890_0_0_0_Types[] = { &CharacterValidation_t3437478890_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterValidation_t3437478890_0_0_0 = { 1, GenInst_CharacterValidation_t3437478890_0_0_0_Types };
extern const Il2CppType Mask_t2977958238_0_0_0;
static const Il2CppType* GenInst_Mask_t2977958238_0_0_0_Types[] = { &Mask_t2977958238_0_0_0 };
extern const Il2CppGenericInst GenInst_Mask_t2977958238_0_0_0 = { 1, GenInst_Mask_t2977958238_0_0_0_Types };
extern const Il2CppType List_1_t2347079370_0_0_0;
static const Il2CppType* GenInst_List_1_t2347079370_0_0_0_Types[] = { &List_1_t2347079370_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2347079370_0_0_0 = { 1, GenInst_List_1_t2347079370_0_0_0_Types };
extern const Il2CppType RectMask2D_t1156185964_0_0_0;
static const Il2CppType* GenInst_RectMask2D_t1156185964_0_0_0_Types[] = { &RectMask2D_t1156185964_0_0_0 };
extern const Il2CppGenericInst GenInst_RectMask2D_t1156185964_0_0_0 = { 1, GenInst_RectMask2D_t1156185964_0_0_0_Types };
extern const Il2CppType List_1_t525307096_0_0_0;
static const Il2CppType* GenInst_List_1_t525307096_0_0_0_Types[] = { &List_1_t525307096_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t525307096_0_0_0 = { 1, GenInst_List_1_t525307096_0_0_0_Types };
extern const Il2CppType Navigation_t1571958496_0_0_0;
static const Il2CppType* GenInst_Navigation_t1571958496_0_0_0_Types[] = { &Navigation_t1571958496_0_0_0 };
extern const Il2CppGenericInst GenInst_Navigation_t1571958496_0_0_0 = { 1, GenInst_Navigation_t1571958496_0_0_0_Types };
extern const Il2CppType IClippable_t1941276057_0_0_0;
static const Il2CppType* GenInst_IClippable_t1941276057_0_0_0_Types[] = { &IClippable_t1941276057_0_0_0 };
extern const Il2CppGenericInst GenInst_IClippable_t1941276057_0_0_0 = { 1, GenInst_IClippable_t1941276057_0_0_0_Types };
extern const Il2CppType Direction_t3696775921_0_0_0;
static const Il2CppType* GenInst_Direction_t3696775921_0_0_0_Types[] = { &Direction_t3696775921_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t3696775921_0_0_0 = { 1, GenInst_Direction_t3696775921_0_0_0_Types };
extern const Il2CppType Selectable_t1490392188_0_0_0;
static const Il2CppType* GenInst_Selectable_t1490392188_0_0_0_Types[] = { &Selectable_t1490392188_0_0_0 };
extern const Il2CppGenericInst GenInst_Selectable_t1490392188_0_0_0 = { 1, GenInst_Selectable_t1490392188_0_0_0_Types };
extern const Il2CppType Transition_t605142169_0_0_0;
static const Il2CppType* GenInst_Transition_t605142169_0_0_0_Types[] = { &Transition_t605142169_0_0_0 };
extern const Il2CppGenericInst GenInst_Transition_t605142169_0_0_0 = { 1, GenInst_Transition_t605142169_0_0_0_Types };
extern const Il2CppType SpriteState_t1353336012_0_0_0;
static const Il2CppType* GenInst_SpriteState_t1353336012_0_0_0_Types[] = { &SpriteState_t1353336012_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteState_t1353336012_0_0_0 = { 1, GenInst_SpriteState_t1353336012_0_0_0_Types };
extern const Il2CppType CanvasGroup_t3296560743_0_0_0;
static const Il2CppType* GenInst_CanvasGroup_t3296560743_0_0_0_Types[] = { &CanvasGroup_t3296560743_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasGroup_t3296560743_0_0_0 = { 1, GenInst_CanvasGroup_t3296560743_0_0_0_Types };
extern const Il2CppType Direction_t1525323322_0_0_0;
static const Il2CppType* GenInst_Direction_t1525323322_0_0_0_Types[] = { &Direction_t1525323322_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t1525323322_0_0_0 = { 1, GenInst_Direction_t1525323322_0_0_0_Types };
extern const Il2CppType MatEntry_t3157325053_0_0_0;
static const Il2CppType* GenInst_MatEntry_t3157325053_0_0_0_Types[] = { &MatEntry_t3157325053_0_0_0 };
extern const Il2CppGenericInst GenInst_MatEntry_t3157325053_0_0_0 = { 1, GenInst_MatEntry_t3157325053_0_0_0_Types };
extern const Il2CppType Toggle_t3976754468_0_0_0;
static const Il2CppType* GenInst_Toggle_t3976754468_0_0_0_Types[] = { &Toggle_t3976754468_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t3976754468_0_0_0 = { 1, GenInst_Toggle_t3976754468_0_0_0_Types };
static const Il2CppType* GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Toggle_t3976754468_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType IClipper_t900477982_0_0_0;
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Types[] = { &IClipper_t900477982_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0 = { 1, GenInst_IClipper_t900477982_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &IClipper_t900477982_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &IClipper_t900477982_0_0_0, &Int32_t2071877448_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t379984643_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t379984643_0_0_0_Types[] = { &KeyValuePair_2_t379984643_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t379984643_0_0_0 = { 1, GenInst_KeyValuePair_2_t379984643_0_0_0_Types };
extern const Il2CppType AspectMode_t1166448724_0_0_0;
static const Il2CppType* GenInst_AspectMode_t1166448724_0_0_0_Types[] = { &AspectMode_t1166448724_0_0_0 };
extern const Il2CppGenericInst GenInst_AspectMode_t1166448724_0_0_0 = { 1, GenInst_AspectMode_t1166448724_0_0_0_Types };
extern const Il2CppType FitMode_t4030874534_0_0_0;
static const Il2CppType* GenInst_FitMode_t4030874534_0_0_0_Types[] = { &FitMode_t4030874534_0_0_0 };
extern const Il2CppGenericInst GenInst_FitMode_t4030874534_0_0_0 = { 1, GenInst_FitMode_t4030874534_0_0_0_Types };
extern const Il2CppType RectTransform_t3349966182_0_0_0;
static const Il2CppType* GenInst_RectTransform_t3349966182_0_0_0_Types[] = { &RectTransform_t3349966182_0_0_0 };
extern const Il2CppGenericInst GenInst_RectTransform_t3349966182_0_0_0 = { 1, GenInst_RectTransform_t3349966182_0_0_0_Types };
extern const Il2CppType LayoutRebuilder_t2155218138_0_0_0;
static const Il2CppType* GenInst_LayoutRebuilder_t2155218138_0_0_0_Types[] = { &LayoutRebuilder_t2155218138_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t2155218138_0_0_0 = { 1, GenInst_LayoutRebuilder_t2155218138_0_0_0_Types };
extern const Il2CppType ILayoutElement_t1975293769_0_0_0;
static const Il2CppType* GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0_Types[] = { &ILayoutElement_t1975293769_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0_Types };
extern const Il2CppType List_1_t1612828712_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828712_0_0_0_Types[] = { &List_1_t1612828712_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828712_0_0_0 = { 1, GenInst_List_1_t1612828712_0_0_0_Types };
extern const Il2CppType List_1_t243638650_0_0_0;
static const Il2CppType* GenInst_List_1_t243638650_0_0_0_Types[] = { &List_1_t243638650_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t243638650_0_0_0 = { 1, GenInst_List_1_t243638650_0_0_0_Types };
extern const Il2CppType List_1_t1612828711_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828711_0_0_0_Types[] = { &List_1_t1612828711_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828711_0_0_0 = { 1, GenInst_List_1_t1612828711_0_0_0_Types };
extern const Il2CppType List_1_t1612828713_0_0_0;
static const Il2CppType* GenInst_List_1_t1612828713_0_0_0_Types[] = { &List_1_t1612828713_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1612828713_0_0_0 = { 1, GenInst_List_1_t1612828713_0_0_0_Types };
extern const Il2CppType List_1_t1440998580_0_0_0;
static const Il2CppType* GenInst_List_1_t1440998580_0_0_0_Types[] = { &List_1_t1440998580_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1440998580_0_0_0 = { 1, GenInst_List_1_t1440998580_0_0_0_Types };
extern const Il2CppType List_1_t573379950_0_0_0;
static const Il2CppType* GenInst_List_1_t573379950_0_0_0_Types[] = { &List_1_t573379950_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t573379950_0_0_0 = { 1, GenInst_List_1_t573379950_0_0_0_Types };
extern const Il2CppType PubnubClientError_t110317921_0_0_0;
static const Il2CppType* GenInst_PubnubClientError_t110317921_0_0_0_Types[] = { &PubnubClientError_t110317921_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubClientError_t110317921_0_0_0 = { 1, GenInst_PubnubClientError_t110317921_0_0_0_Types };
extern const Il2CppType ChannelEntity_t3266154606_0_0_0;
static const Il2CppType* GenInst_ChannelEntity_t3266154606_0_0_0_Types[] = { &ChannelEntity_t3266154606_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelEntity_t3266154606_0_0_0 = { 1, GenInst_ChannelEntity_t3266154606_0_0_0_Types };
extern const Il2CppType PNMessageResult_t756120242_0_0_0;
static const Il2CppType* GenInst_PNMessageResult_t756120242_0_0_0_Types[] = { &PNMessageResult_t756120242_0_0_0 };
extern const Il2CppGenericInst GenInst_PNMessageResult_t756120242_0_0_0 = { 1, GenInst_PNMessageResult_t756120242_0_0_0_Types };
extern const Il2CppType ChannelIdentity_t1147162267_0_0_0;
extern const Il2CppType ChannelParameters_t547936593_0_0_0;
static const Il2CppType* GenInst_ChannelIdentity_t1147162267_0_0_0_ChannelParameters_t547936593_0_0_0_Types[] = { &ChannelIdentity_t1147162267_0_0_0, &ChannelParameters_t547936593_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelIdentity_t1147162267_0_0_0_ChannelParameters_t547936593_0_0_0 = { 2, GenInst_ChannelIdentity_t1147162267_0_0_0_ChannelParameters_t547936593_0_0_0_Types };
static const Il2CppType* GenInst_ChannelIdentity_t1147162267_0_0_0_Il2CppObject_0_0_0_Types[] = { &ChannelIdentity_t1147162267_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelIdentity_t1147162267_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ChannelIdentity_t1147162267_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1381843961_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1381843961_0_0_0_Types[] = { &KeyValuePair_2_t1381843961_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1381843961_0_0_0 = { 1, GenInst_KeyValuePair_2_t1381843961_0_0_0_Types };
static const Il2CppType* GenInst_ChannelIdentity_t1147162267_0_0_0_Types[] = { &ChannelIdentity_t1147162267_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelIdentity_t1147162267_0_0_0 = { 1, GenInst_ChannelIdentity_t1147162267_0_0_0_Types };
static const Il2CppType* GenInst_ChannelIdentity_t1147162267_0_0_0_Il2CppObject_0_0_0_ChannelIdentity_t1147162267_0_0_0_Types[] = { &ChannelIdentity_t1147162267_0_0_0, &Il2CppObject_0_0_0, &ChannelIdentity_t1147162267_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelIdentity_t1147162267_0_0_0_Il2CppObject_0_0_0_ChannelIdentity_t1147162267_0_0_0 = { 3, GenInst_ChannelIdentity_t1147162267_0_0_0_Il2CppObject_0_0_0_ChannelIdentity_t1147162267_0_0_0_Types };
static const Il2CppType* GenInst_ChannelIdentity_t1147162267_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &ChannelIdentity_t1147162267_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelIdentity_t1147162267_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_ChannelIdentity_t1147162267_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ChannelIdentity_t1147162267_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ChannelIdentity_t1147162267_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelIdentity_t1147162267_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ChannelIdentity_t1147162267_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_ChannelIdentity_t1147162267_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1381843961_0_0_0_Types[] = { &ChannelIdentity_t1147162267_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t1381843961_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelIdentity_t1147162267_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1381843961_0_0_0 = { 3, GenInst_ChannelIdentity_t1147162267_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1381843961_0_0_0_Types };
static const Il2CppType* GenInst_ChannelIdentity_t1147162267_0_0_0_ChannelParameters_t547936593_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ChannelIdentity_t1147162267_0_0_0, &ChannelParameters_t547936593_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelIdentity_t1147162267_0_0_0_ChannelParameters_t547936593_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ChannelIdentity_t1147162267_0_0_0_ChannelParameters_t547936593_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_ChannelIdentity_t1147162267_0_0_0_ChannelParameters_t547936593_0_0_0_ChannelParameters_t547936593_0_0_0_Types[] = { &ChannelIdentity_t1147162267_0_0_0, &ChannelParameters_t547936593_0_0_0, &ChannelParameters_t547936593_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelIdentity_t1147162267_0_0_0_ChannelParameters_t547936593_0_0_0_ChannelParameters_t547936593_0_0_0 = { 3, GenInst_ChannelIdentity_t1147162267_0_0_0_ChannelParameters_t547936593_0_0_0_ChannelParameters_t547936593_0_0_0_Types };
static const Il2CppType* GenInst_ChannelParameters_t547936593_0_0_0_Types[] = { &ChannelParameters_t547936593_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelParameters_t547936593_0_0_0 = { 1, GenInst_ChannelParameters_t547936593_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3535298555_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3535298555_0_0_0_Types[] = { &KeyValuePair_2_t3535298555_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3535298555_0_0_0 = { 1, GenInst_KeyValuePair_2_t3535298555_0_0_0_Types };
extern const Il2CppType EventArgs_t3289624707_0_0_0;
static const Il2CppType* GenInst_EventArgs_t3289624707_0_0_0_Types[] = { &EventArgs_t3289624707_0_0_0 };
extern const Il2CppGenericInst GenInst_EventArgs_t3289624707_0_0_0 = { 1, GenInst_EventArgs_t3289624707_0_0_0_Types };
extern const Il2CppType SubscribeMessage_t3739430639_0_0_0;
static const Il2CppType* GenInst_SubscribeMessage_t3739430639_0_0_0_Types[] = { &SubscribeMessage_t3739430639_0_0_0 };
extern const Il2CppGenericInst GenInst_SubscribeMessage_t3739430639_0_0_0 = { 1, GenInst_SubscribeMessage_t3739430639_0_0_0_Types };
extern const Il2CppType StringU5BU5D_t1642385972_0_0_0;
static const Il2CppType* GenInst_StringU5BU5D_t1642385972_0_0_0_Types[] = { &StringU5BU5D_t1642385972_0_0_0 };
extern const Il2CppGenericInst GenInst_StringU5BU5D_t1642385972_0_0_0 = { 1, GenInst_StringU5BU5D_t1642385972_0_0_0_Types };
extern const Il2CppType Int32U5BU5D_t3030399641_0_0_0;
static const Il2CppType* GenInst_Int32U5BU5D_t3030399641_0_0_0_Types[] = { &Int32U5BU5D_t3030399641_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32U5BU5D_t3030399641_0_0_0 = { 1, GenInst_Int32U5BU5D_t3030399641_0_0_0_Types };
extern const Il2CppType Int64U5BU5D_t717125112_0_0_0;
static const Il2CppType* GenInst_Int64U5BU5D_t717125112_0_0_0_Types[] = { &Int64U5BU5D_t717125112_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64U5BU5D_t717125112_0_0_0 = { 1, GenInst_Int64U5BU5D_t717125112_0_0_0_Types };
extern const Il2CppType Int16U5BU5D_t3104283263_0_0_0;
static const Il2CppType* GenInst_Int16U5BU5D_t3104283263_0_0_0_Types[] = { &Int16U5BU5D_t3104283263_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16U5BU5D_t3104283263_0_0_0 = { 1, GenInst_Int16U5BU5D_t3104283263_0_0_0_Types };
extern const Il2CppType UInt16U5BU5D_t2527266722_0_0_0;
static const Il2CppType* GenInst_UInt16U5BU5D_t2527266722_0_0_0_Types[] = { &UInt16U5BU5D_t2527266722_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16U5BU5D_t2527266722_0_0_0 = { 1, GenInst_UInt16U5BU5D_t2527266722_0_0_0_Types };
extern const Il2CppType UInt64U5BU5D_t1668688775_0_0_0;
static const Il2CppType* GenInst_UInt64U5BU5D_t1668688775_0_0_0_Types[] = { &UInt64U5BU5D_t1668688775_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64U5BU5D_t1668688775_0_0_0 = { 1, GenInst_UInt64U5BU5D_t1668688775_0_0_0_Types };
extern const Il2CppType UInt32U5BU5D_t59386216_0_0_0;
static const Il2CppType* GenInst_UInt32U5BU5D_t59386216_0_0_0_Types[] = { &UInt32U5BU5D_t59386216_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32U5BU5D_t59386216_0_0_0 = { 1, GenInst_UInt32U5BU5D_t59386216_0_0_0_Types };
extern const Il2CppType DecimalU5BU5D_t624008824_0_0_0;
static const Il2CppType* GenInst_DecimalU5BU5D_t624008824_0_0_0_Types[] = { &DecimalU5BU5D_t624008824_0_0_0 };
extern const Il2CppGenericInst GenInst_DecimalU5BU5D_t624008824_0_0_0 = { 1, GenInst_DecimalU5BU5D_t624008824_0_0_0_Types };
extern const Il2CppType DoubleU5BU5D_t1889952540_0_0_0;
static const Il2CppType* GenInst_DoubleU5BU5D_t1889952540_0_0_0_Types[] = { &DoubleU5BU5D_t1889952540_0_0_0 };
extern const Il2CppGenericInst GenInst_DoubleU5BU5D_t1889952540_0_0_0 = { 1, GenInst_DoubleU5BU5D_t1889952540_0_0_0_Types };
extern const Il2CppType BooleanU5BU5D_t3568034315_0_0_0;
static const Il2CppType* GenInst_BooleanU5BU5D_t3568034315_0_0_0_Types[] = { &BooleanU5BU5D_t3568034315_0_0_0 };
extern const Il2CppGenericInst GenInst_BooleanU5BU5D_t3568034315_0_0_0 = { 1, GenInst_BooleanU5BU5D_t3568034315_0_0_0_Types };
extern const Il2CppType ObjectU5BU5D_t3614634134_0_0_0;
static const Il2CppType* GenInst_ObjectU5BU5D_t3614634134_0_0_0_Types[] = { &ObjectU5BU5D_t3614634134_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectU5BU5D_t3614634134_0_0_0 = { 1, GenInst_ObjectU5BU5D_t3614634134_0_0_0_Types };
extern const Il2CppType SingleU5BU5D_t577127397_0_0_0;
static const Il2CppType* GenInst_SingleU5BU5D_t577127397_0_0_0_Types[] = { &SingleU5BU5D_t577127397_0_0_0 };
extern const Il2CppGenericInst GenInst_SingleU5BU5D_t577127397_0_0_0 = { 1, GenInst_SingleU5BU5D_t577127397_0_0_0_Types };
extern const Il2CppType IGrouping_2_t2569427433_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t2569427433_0_0_0_Types[] = { &IGrouping_2_t2569427433_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t2569427433_0_0_0 = { 1, GenInst_IGrouping_2_t2569427433_0_0_0_Types };
static const Il2CppType* GenInst_IGrouping_2_t2569427433_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &IGrouping_2_t2569427433_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t2569427433_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_IGrouping_2_t2569427433_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType CurrentRequestType_t3250227986_0_0_0;
static const Il2CppType* GenInst_CurrentRequestType_t3250227986_0_0_0_Il2CppObject_0_0_0_Types[] = { &CurrentRequestType_t3250227986_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_CurrentRequestType_t3250227986_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_CurrentRequestType_t3250227986_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3104328646_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3104328646_0_0_0_Types[] = { &KeyValuePair_2_t3104328646_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3104328646_0_0_0 = { 1, GenInst_KeyValuePair_2_t3104328646_0_0_0_Types };
static const Il2CppType* GenInst_CurrentRequestType_t3250227986_0_0_0_Types[] = { &CurrentRequestType_t3250227986_0_0_0 };
extern const Il2CppGenericInst GenInst_CurrentRequestType_t3250227986_0_0_0 = { 1, GenInst_CurrentRequestType_t3250227986_0_0_0_Types };
static const Il2CppType* GenInst_CurrentRequestType_t3250227986_0_0_0_Il2CppObject_0_0_0_CurrentRequestType_t3250227986_0_0_0_Types[] = { &CurrentRequestType_t3250227986_0_0_0, &Il2CppObject_0_0_0, &CurrentRequestType_t3250227986_0_0_0 };
extern const Il2CppGenericInst GenInst_CurrentRequestType_t3250227986_0_0_0_Il2CppObject_0_0_0_CurrentRequestType_t3250227986_0_0_0 = { 3, GenInst_CurrentRequestType_t3250227986_0_0_0_Il2CppObject_0_0_0_CurrentRequestType_t3250227986_0_0_0_Types };
static const Il2CppType* GenInst_CurrentRequestType_t3250227986_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &CurrentRequestType_t3250227986_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_CurrentRequestType_t3250227986_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_CurrentRequestType_t3250227986_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_CurrentRequestType_t3250227986_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &CurrentRequestType_t3250227986_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_CurrentRequestType_t3250227986_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_CurrentRequestType_t3250227986_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_CurrentRequestType_t3250227986_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3104328646_0_0_0_Types[] = { &CurrentRequestType_t3250227986_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t3104328646_0_0_0 };
extern const Il2CppGenericInst GenInst_CurrentRequestType_t3250227986_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3104328646_0_0_0 = { 3, GenInst_CurrentRequestType_t3250227986_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3104328646_0_0_0_Types };
extern const Il2CppType IDictionary_t596158605_0_0_0;
static const Il2CppType* GenInst_IDictionary_t596158605_0_0_0_Types[] = { &IDictionary_t596158605_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_t596158605_0_0_0 = { 1, GenInst_IDictionary_t596158605_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int64_t909078037_0_0_0_Types[] = { &String_t_0_0_0, &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int64_t909078037_0_0_0 = { 2, GenInst_String_t_0_0_0_Int64_t909078037_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2553450683_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2553450683_0_0_0_Types[] = { &KeyValuePair_2_t2553450683_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2553450683_0_0_0 = { 1, GenInst_KeyValuePair_2_t2553450683_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int64_t909078037_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int64_t909078037_0_0_0, &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int64_t909078037_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_KeyValuePair_2_t2553450683_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int64_t909078037_0_0_0, &KeyValuePair_2_t2553450683_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_KeyValuePair_2_t2553450683_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_KeyValuePair_2_t2553450683_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int64_t909078037_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &Int64_t909078037_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int64_t909078037_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Int64_t909078037_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t581202521_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t581202521_0_0_0_Types[] = { &KeyValuePair_2_t581202521_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t581202521_0_0_0 = { 1, GenInst_KeyValuePair_2_t581202521_0_0_0_Types };
static const Il2CppType* GenInst_Object_t1021602117_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Object_t1021602117_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t1021602117_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Object_t1021602117_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType AssertionComponent_t3962419315_0_0_0;
static const Il2CppType* GenInst_AssertionComponent_t3962419315_0_0_0_Types[] = { &AssertionComponent_t3962419315_0_0_0 };
extern const Il2CppGenericInst GenInst_AssertionComponent_t3962419315_0_0_0 = { 1, GenInst_AssertionComponent_t3962419315_0_0_0_Types };
extern const Il2CppType IAssertionComponentConfigurator_t3050464039_0_0_0;
static const Il2CppType* GenInst_IAssertionComponentConfigurator_t3050464039_0_0_0_Types[] = { &IAssertionComponentConfigurator_t3050464039_0_0_0 };
extern const Il2CppGenericInst GenInst_IAssertionComponentConfigurator_t3050464039_0_0_0 = { 1, GenInst_IAssertionComponentConfigurator_t3050464039_0_0_0_Types };
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &FieldInfo_t_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_FieldInfo_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Boolean_t3825574718_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Bounds_t3033363703_0_0_0;
static const Il2CppType* GenInst_Bounds_t3033363703_0_0_0_Types[] = { &Bounds_t3033363703_0_0_0 };
extern const Il2CppGenericInst GenInst_Bounds_t3033363703_0_0_0 = { 1, GenInst_Bounds_t3033363703_0_0_0_Types };
static const Il2CppType* GenInst_Bounds_t3033363703_0_0_0_Bounds_t3033363703_0_0_0_Types[] = { &Bounds_t3033363703_0_0_0, &Bounds_t3033363703_0_0_0 };
extern const Il2CppGenericInst GenInst_Bounds_t3033363703_0_0_0_Bounds_t3033363703_0_0_0 = { 2, GenInst_Bounds_t3033363703_0_0_0_Bounds_t3033363703_0_0_0_Types };
static const Il2CppType* GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types[] = { &Single_t2076509932_0_0_0, &Single_t2076509932_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types };
extern const Il2CppType Renderer_t257310565_0_0_0;
static const Il2CppType* GenInst_Renderer_t257310565_0_0_0_Camera_t189460977_0_0_0_Types[] = { &Renderer_t257310565_0_0_0, &Camera_t189460977_0_0_0 };
extern const Il2CppGenericInst GenInst_Renderer_t257310565_0_0_0_Camera_t189460977_0_0_0 = { 2, GenInst_Renderer_t257310565_0_0_0_Camera_t189460977_0_0_0_Types };
static const Il2CppType* GenInst_Transform_t3275118058_0_0_0_Transform_t3275118058_0_0_0_Types[] = { &Transform_t3275118058_0_0_0, &Transform_t3275118058_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t3275118058_0_0_0_Transform_t3275118058_0_0_0 = { 2, GenInst_Transform_t3275118058_0_0_0_Transform_t3275118058_0_0_0_Types };
static const Il2CppType* GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_Types[] = { &Vector2_t2243707579_0_0_0, &Vector2_t2243707579_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0 = { 2, GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types[] = { &Vector3_t2243707580_0_0_0, &Vector3_t2243707580_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0 = { 2, GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types };
static const Il2CppType* GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_Types[] = { &Vector4_t2243707581_0_0_0, &Vector4_t2243707581_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0 = { 2, GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_Types };
extern const Il2CppType RuntimePlatform_t1869584967_0_0_0;
static const Il2CppType* GenInst_RuntimePlatform_t1869584967_0_0_0_Types[] = { &RuntimePlatform_t1869584967_0_0_0 };
extern const Il2CppGenericInst GenInst_RuntimePlatform_t1869584967_0_0_0 = { 1, GenInst_RuntimePlatform_t1869584967_0_0_0_Types };
extern const Il2CppType TestComponent_t2516511601_0_0_0;
static const Il2CppType* GenInst_TestComponent_t2516511601_0_0_0_Types[] = { &TestComponent_t2516511601_0_0_0 };
extern const Il2CppGenericInst GenInst_TestComponent_t2516511601_0_0_0 = { 1, GenInst_TestComponent_t2516511601_0_0_0_Types };
extern const Il2CppType TestResult_t490498461_0_0_0;
static const Il2CppType* GenInst_TestResult_t490498461_0_0_0_Types[] = { &TestResult_t490498461_0_0_0 };
extern const Il2CppGenericInst GenInst_TestResult_t490498461_0_0_0 = { 1, GenInst_TestResult_t490498461_0_0_0_Types };
extern const Il2CppType ITestComponent_t2920761518_0_0_0;
static const Il2CppType* GenInst_ITestComponent_t2920761518_0_0_0_Types[] = { &ITestComponent_t2920761518_0_0_0 };
extern const Il2CppGenericInst GenInst_ITestComponent_t2920761518_0_0_0 = { 1, GenInst_ITestComponent_t2920761518_0_0_0_Types };
static const Il2CppType* GenInst_RuntimePlatform_t1869584967_0_0_0_String_t_0_0_0_Types[] = { &RuntimePlatform_t1869584967_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_RuntimePlatform_t1869584967_0_0_0_String_t_0_0_0 = { 2, GenInst_RuntimePlatform_t1869584967_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_RuntimePlatform_t1869584967_0_0_0_Il2CppObject_0_0_0_Types[] = { &RuntimePlatform_t1869584967_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_RuntimePlatform_t1869584967_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_RuntimePlatform_t1869584967_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_String_t_0_0_0_Types[] = { &Type_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_String_t_0_0_0 = { 2, GenInst_Type_t_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType HashSet_1_t1254222372_0_0_0;
static const Il2CppType* GenInst_ITestComponent_t2920761518_0_0_0_HashSet_1_t1254222372_0_0_0_Types[] = { &ITestComponent_t2920761518_0_0_0, &HashSet_1_t1254222372_0_0_0 };
extern const Il2CppGenericInst GenInst_ITestComponent_t2920761518_0_0_0_HashSet_1_t1254222372_0_0_0 = { 2, GenInst_ITestComponent_t2920761518_0_0_0_HashSet_1_t1254222372_0_0_0_Types };
static const Il2CppType* GenInst_ITestComponent_t2920761518_0_0_0_HashSet_1_t1254222372_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &ITestComponent_t2920761518_0_0_0, &HashSet_1_t1254222372_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_ITestComponent_t2920761518_0_0_0_HashSet_1_t1254222372_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ITestComponent_t2920761518_0_0_0_HashSet_1_t1254222372_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_HashSet_1_t1254222372_0_0_0_Types[] = { &HashSet_1_t1254222372_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t1254222372_0_0_0 = { 1, GenInst_HashSet_1_t1254222372_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2166842639_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2166842639_0_0_0_Types[] = { &KeyValuePair_2_t2166842639_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2166842639_0_0_0 = { 1, GenInst_KeyValuePair_2_t2166842639_0_0_0_Types };
static const Il2CppType* GenInst_ITestComponent_t2920761518_0_0_0_ITestComponent_t2920761518_0_0_0_Types[] = { &ITestComponent_t2920761518_0_0_0, &ITestComponent_t2920761518_0_0_0 };
extern const Il2CppGenericInst GenInst_ITestComponent_t2920761518_0_0_0_ITestComponent_t2920761518_0_0_0 = { 2, GenInst_ITestComponent_t2920761518_0_0_0_ITestComponent_t2920761518_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1595930271_0_0_0;
static const Il2CppType* GenInst_Assembly_t4268412390_0_0_0_IEnumerable_1_t1595930271_0_0_0_Types[] = { &Assembly_t4268412390_0_0_0, &IEnumerable_1_t1595930271_0_0_0 };
extern const Il2CppGenericInst GenInst_Assembly_t4268412390_0_0_0_IEnumerable_1_t1595930271_0_0_0 = { 2, GenInst_Assembly_t4268412390_0_0_0_IEnumerable_1_t1595930271_0_0_0_Types };
static const Il2CppType* GenInst_TestComponent_t2516511601_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &TestComponent_t2516511601_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_TestComponent_t2516511601_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_TestComponent_t2516511601_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Type_t_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Type_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType List_1_t2625464602_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t2625464602_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t2625464602_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t2625464602_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t2625464602_0_0_0_Types };
extern const Il2CppType ITestResult_t3256343470_0_0_0;
static const Il2CppType* GenInst_ITestResult_t3256343470_0_0_0_Types[] = { &ITestResult_t3256343470_0_0_0 };
extern const Il2CppGenericInst GenInst_ITestResult_t3256343470_0_0_0 = { 1, GenInst_ITestResult_t3256343470_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t2625464602_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t2625464602_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t2625464602_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t2625464602_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t2625464602_0_0_0_Types[] = { &List_1_t2625464602_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2625464602_0_0_0 = { 1, GenInst_List_1_t2625464602_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2297589086_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2297589086_0_0_0_Types[] = { &KeyValuePair_2_t2297589086_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2297589086_0_0_0 = { 1, GenInst_KeyValuePair_2_t2297589086_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2297589086_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &KeyValuePair_2_t2297589086_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2297589086_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_KeyValuePair_2_t2297589086_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_Int32_t2071877448_0_0_0_Types };
static const Il2CppType* GenInst_ITestResult_t3256343470_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &ITestResult_t3256343470_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_ITestResult_t3256343470_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_ITestResult_t3256343470_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_ITestResult_t3256343470_0_0_0_String_t_0_0_0_Types[] = { &ITestResult_t3256343470_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_ITestResult_t3256343470_0_0_0_String_t_0_0_0 = { 2, GenInst_ITestResult_t3256343470_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_TestComponent_t2516511601_0_0_0_TestResult_t490498461_0_0_0_Types[] = { &TestComponent_t2516511601_0_0_0, &TestResult_t490498461_0_0_0 };
extern const Il2CppGenericInst GenInst_TestComponent_t2516511601_0_0_0_TestResult_t490498461_0_0_0 = { 2, GenInst_TestComponent_t2516511601_0_0_0_TestResult_t490498461_0_0_0_Types };
static const Il2CppType* GenInst_TestResult_t490498461_0_0_0_ITestComponent_t2920761518_0_0_0_Types[] = { &TestResult_t490498461_0_0_0, &ITestComponent_t2920761518_0_0_0 };
extern const Il2CppGenericInst GenInst_TestResult_t490498461_0_0_0_ITestComponent_t2920761518_0_0_0 = { 2, GenInst_TestResult_t490498461_0_0_0_ITestComponent_t2920761518_0_0_0_Types };
static const Il2CppType* GenInst_TestResult_t490498461_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &TestResult_t490498461_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_TestResult_t490498461_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_TestResult_t490498461_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_TestResult_t490498461_0_0_0_String_t_0_0_0_Types[] = { &TestResult_t490498461_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_TestResult_t490498461_0_0_0_String_t_0_0_0 = { 2, GenInst_TestResult_t490498461_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Component_t3819376471_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Component_t3819376471_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t3819376471_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Component_t3819376471_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const Il2CppType* GenInst_AssertionComponent_t3962419315_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &AssertionComponent_t3962419315_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_AssertionComponent_t3962419315_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_AssertionComponent_t3962419315_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType ITestRunnerCallback_t327193412_0_0_0;
static const Il2CppType* GenInst_ITestRunnerCallback_t327193412_0_0_0_Types[] = { &ITestRunnerCallback_t327193412_0_0_0 };
extern const Il2CppGenericInst GenInst_ITestRunnerCallback_t327193412_0_0_0 = { 1, GenInst_ITestRunnerCallback_t327193412_0_0_0_Types };
extern const Il2CppType ExplosionPart_t2473625634_0_0_0;
static const Il2CppType* GenInst_ExplosionPart_t2473625634_0_0_0_Types[] = { &ExplosionPart_t2473625634_0_0_0 };
extern const Il2CppGenericInst GenInst_ExplosionPart_t2473625634_0_0_0 = { 1, GenInst_ExplosionPart_t2473625634_0_0_0_Types };
extern const Il2CppType WaitForSeconds_t3839502067_0_0_0;
static const Il2CppType* GenInst_WaitForSeconds_t3839502067_0_0_0_Types[] = { &WaitForSeconds_t3839502067_0_0_0 };
extern const Il2CppGenericInst GenInst_WaitForSeconds_t3839502067_0_0_0 = { 1, GenInst_WaitForSeconds_t3839502067_0_0_0_Types };
extern const Il2CppType SelfIlluminationBlink_t1392230251_0_0_0;
static const Il2CppType* GenInst_SelfIlluminationBlink_t1392230251_0_0_0_Types[] = { &SelfIlluminationBlink_t1392230251_0_0_0 };
extern const Il2CppGenericInst GenInst_SelfIlluminationBlink_t1392230251_0_0_0 = { 1, GenInst_SelfIlluminationBlink_t1392230251_0_0_0_Types };
extern const Il2CppType MoveAnimation_t3061523661_0_0_0;
static const Il2CppType* GenInst_MoveAnimation_t3061523661_0_0_0_Types[] = { &MoveAnimation_t3061523661_0_0_0 };
extern const Il2CppGenericInst GenInst_MoveAnimation_t3061523661_0_0_0 = { 1, GenInst_MoveAnimation_t3061523661_0_0_0_Types };
extern const Il2CppType ObjectCache_t960934699_0_0_0;
static const Il2CppType* GenInst_ObjectCache_t960934699_0_0_0_Types[] = { &ObjectCache_t960934699_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectCache_t960934699_0_0_0 = { 1, GenInst_ObjectCache_t960934699_0_0_0_Types };
extern const Il2CppType ReceiverItem_t169526838_0_0_0;
static const Il2CppType* GenInst_ReceiverItem_t169526838_0_0_0_Types[] = { &ReceiverItem_t169526838_0_0_0 };
extern const Il2CppGenericInst GenInst_ReceiverItem_t169526838_0_0_0 = { 1, GenInst_ReceiverItem_t169526838_0_0_0_Types };
extern const Il2CppType Health_t2683907638_0_0_0;
static const Il2CppType* GenInst_Health_t2683907638_0_0_0_Types[] = { &Health_t2683907638_0_0_0 };
extern const Il2CppGenericInst GenInst_Health_t2683907638_0_0_0 = { 1, GenInst_Health_t2683907638_0_0_0_Types };
extern const Il2CppType Joystick_t549888914_0_0_0;
static const Il2CppType* GenInst_Joystick_t549888914_0_0_0_Types[] = { &Joystick_t549888914_0_0_0 };
extern const Il2CppGenericInst GenInst_Joystick_t549888914_0_0_0 = { 1, GenInst_Joystick_t549888914_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t4048664256_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0_Types[] = { &IEnumerable_1_t4048664256_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1730553742_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0_Types[] = { &Array_Sort_m1730553742_gp_0_0_0_0, &Array_Sort_m1730553742_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3106198730_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3106198730_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0_Types[] = { &Array_Sort_m3106198730_gp_0_0_0_0, &Array_Sort_m3106198730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2090966156_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Types[] = { &Array_Sort_m2090966156_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2090966156_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0_Types[] = { &Array_Sort_m2090966156_gp_0_0_0_0, &Array_Sort_m2090966156_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1985772939_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Types[] = { &Array_Sort_m1985772939_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1985772939_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1985772939_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0_Types[] = { &Array_Sort_m1985772939_gp_0_0_0_0, &Array_Sort_m1985772939_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2736815140_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0_Types[] = { &Array_Sort_m2736815140_gp_0_0_0_0, &Array_Sort_m2736815140_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m2468799988_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2468799988_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0_Types[] = { &Array_Sort_m2468799988_gp_0_0_0_0, &Array_Sort_m2468799988_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2587948790_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Types[] = { &Array_Sort_m2587948790_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2587948790_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0_Types[] = { &Array_Sort_m2587948790_gp_0_0_0_0, &Array_Sort_m2587948790_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1279015767_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1279015767_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_1_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m1279015767_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0_Types[] = { &Array_Sort_m1279015767_gp_0_0_0_0, &Array_Sort_m1279015767_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m52621935_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m52621935_gp_0_0_0_0_Types[] = { &Array_Sort_m52621935_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m52621935_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m52621935_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3546416104_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3546416104_gp_0_0_0_0_Types[] = { &Array_Sort_m3546416104_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3546416104_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3546416104_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m533480027_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m533480027_gp_0_0_0_0_Types[] = { &Array_qsort_m533480027_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m533480027_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m533480027_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m533480027_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0_Types[] = { &Array_qsort_m533480027_gp_0_0_0_0, &Array_qsort_m533480027_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m940423571_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m940423571_gp_0_0_0_0_Types[] = { &Array_compare_m940423571_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m940423571_gp_0_0_0_0 = { 1, GenInst_Array_compare_m940423571_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m565008110_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m565008110_gp_0_0_0_0_Types[] = { &Array_qsort_m565008110_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m565008110_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m565008110_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m1201602141_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m1201602141_gp_0_0_0_0_Types[] = { &Array_Resize_m1201602141_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m1201602141_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m1201602141_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m2783802133_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m2783802133_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m3775633118_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m3775633118_gp_0_0_0_0_Types[] = { &Array_ForEach_m3775633118_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m3775633118_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m3775633118_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m1734974082_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m1734974082_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m1734974082_gp_0_0_0_0, &Array_ConvertAll_m1734974082_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m934773128_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m934773128_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m3202023711_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m3202023711_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m352384762_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m352384762_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1593955424_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1593955424_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1546138173_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1546138173_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1082322798_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1082322798_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m525402987_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m525402987_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3577113407_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3577113407_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1033585031_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1033585031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3052238307_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3052238307_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m1306290405_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0_Types[] = { &Array_IndexOf_m1306290405_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m2825795862_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0_Types[] = { &Array_IndexOf_m2825795862_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m2841140625_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0_Types[] = { &Array_IndexOf_m2841140625_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m3304283431_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m3304283431_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m3860096562_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m3860096562_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m2100440379_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m2100440379_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m982349212_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m982349212_gp_0_0_0_0_Types[] = { &Array_FindAll_m982349212_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m982349212_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m982349212_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m1825464757_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m1825464757_gp_0_0_0_0_Types[] = { &Array_Exists_m1825464757_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m1825464757_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m1825464757_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m1258056624_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m1258056624_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m2529971459_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m2529971459_gp_0_0_0_0_Types[] = { &Array_Find_m2529971459_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m2529971459_gp_0_0_0_0 = { 1, GenInst_Array_Find_m2529971459_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m3929249453_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m3929249453_gp_0_0_0_0_Types[] = { &Array_FindLast_m3929249453_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m3929249453_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m3929249453_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t3582267753_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t3582267753_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t3737699284_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t3737699284_gp_0_0_0_0_Types[] = { &IList_1_t3737699284_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3737699284_gp_0_0_0_0 = { 1, GenInst_IList_1_t3737699284_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t1552160836_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1552160836_gp_0_0_0_0_Types[] = { &ICollection_1_t1552160836_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1552160836_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1552160836_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t1398937014_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t1398937014_gp_0_0_0_0_Types[] = { &Nullable_1_t1398937014_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t1398937014_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t1398937014_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2276497324_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2276497324_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_1_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t2276497324_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3180694294_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3180694294_0_0_0 = { 1, GenInst_KeyValuePair_2_t3180694294_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t3895203923_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3895203923_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0_Types[] = { &ShimEnumerator_t3895203923_gp_0_0_0_0, &ShimEnumerator_t3895203923_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t2089681430_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2089681430_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0_Types[] = { &Enumerator_t2089681430_gp_0_0_0_0, &Enumerator_t2089681430_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3434615342_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3434615342_0_0_0_Types[] = { &KeyValuePair_2_t3434615342_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3434615342_0_0_0 = { 1, GenInst_KeyValuePair_2_t3434615342_0_0_0_Types };
extern const Il2CppType KeyCollection_t1229212677_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t1229212677_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t83320710_gp_0_0_0_0;
extern const Il2CppType Enumerator_t83320710_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0_Types[] = { &Enumerator_t83320710_gp_0_0_0_0, &Enumerator_t83320710_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0 = { 2, GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t83320710_gp_0_0_0_0_Types[] = { &Enumerator_t83320710_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t83320710_gp_0_0_0_0 = { 1, GenInst_Enumerator_t83320710_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_1_0_0_0, &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { &KeyCollection_t1229212677_gp_0_0_0_0, &KeyCollection_t1229212677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types };
extern const Il2CppType ValueCollection_t2262344653_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2262344653_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_0_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2262344653_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t3111723616_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3111723616_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0_Types[] = { &Enumerator_t3111723616_gp_0_0_0_0, &Enumerator_t3111723616_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t3111723616_gp_1_0_0_0_Types[] = { &Enumerator_t3111723616_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3111723616_gp_1_0_0_0 = { 1, GenInst_Enumerator_t3111723616_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_0_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { &ValueCollection_t2262344653_gp_1_0_0_0, &ValueCollection_t2262344653_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { &DictionaryEntry_t3048875398_0_0_0, &DictionaryEntry_t3048875398_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 2, GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &Dictionary_2_t2276497324_gp_0_0_0_0, &Dictionary_2_t2276497324_gp_1_0_0_0, &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types[] = { &KeyValuePair_2_t3180694294_0_0_0, &KeyValuePair_2_t3180694294_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0 = { 2, GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3502329323_gp_0_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_0_0_0_0 = { 1, GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3502329323_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_1_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_1_0_0_0 = { 1, GenInst_IDictionary_2_t3502329323_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4174120762_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4174120762_0_0_0_Types[] = { &KeyValuePair_2_t4174120762_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4174120762_0_0_0 = { 1, GenInst_KeyValuePair_2_t4174120762_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0_Types[] = { &IDictionary_2_t3502329323_gp_0_0_0_0, &IDictionary_2_t3502329323_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1988958766_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t1988958766_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t1988958766_gp_0_0_0_0, &KeyValuePair_2_t1988958766_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t2066709010_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t2066709010_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t1766400012_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t1766400012_gp_0_0_0_0_Types[] = { &DefaultComparer_t1766400012_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t1766400012_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t1766400012_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t2202941003_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t2202941003_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t1169184319_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t1169184319_gp_0_0_0_0_Types[] = { &List_1_t1169184319_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1169184319_gp_0_0_0_0 = { 1, GenInst_List_1_t1169184319_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1292967705_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1292967705_gp_0_0_0_0_Types[] = { &Enumerator_t1292967705_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1292967705_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1292967705_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t686054069_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t686054069_gp_0_0_0_0_Types[] = { &Collection_1_t686054069_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t686054069_gp_0_0_0_0 = { 1, GenInst_Collection_1_t686054069_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t3540981679_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t3540981679_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0_Types };
extern const Il2CppType ArraySegment_1_t1001032761_gp_0_0_0_0;
static const Il2CppType* GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0_Types[] = { &ArraySegment_1_t1001032761_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0 = { 1, GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t1036860714_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t1036860714_gp_0_0_0_0_Types[] = { &Comparer_1_t1036860714_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t1036860714_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t1036860714_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t3074655092_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t3074655092_gp_0_0_0_0_Types[] = { &DefaultComparer_t3074655092_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3074655092_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3074655092_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t1787398723_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0_Types[] = { &GenericComparer_1_t1787398723_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0_Types };
extern const Il2CppType LinkedList_1_t3556217344_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedList_1_t3556217344_gp_0_0_0_0_Types[] = { &LinkedList_1_t3556217344_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t3556217344_gp_0_0_0_0 = { 1, GenInst_LinkedList_1_t3556217344_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4145643798_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4145643798_gp_0_0_0_0_Types[] = { &Enumerator_t4145643798_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4145643798_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4145643798_gp_0_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t2172356692_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0_Types[] = { &LinkedListNode_1_t2172356692_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0 = { 1, GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0_Types };
extern const Il2CppType Queue_1_t1458930734_gp_0_0_0_0;
static const Il2CppType* GenInst_Queue_1_t1458930734_gp_0_0_0_0_Types[] = { &Queue_1_t1458930734_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Queue_1_t1458930734_gp_0_0_0_0 = { 1, GenInst_Queue_1_t1458930734_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t4000919638_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t4000919638_gp_0_0_0_0_Types[] = { &Enumerator_t4000919638_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t4000919638_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4000919638_gp_0_0_0_0_Types };
extern const Il2CppType Stack_1_t4016656541_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t4016656541_gp_0_0_0_0_Types[] = { &Stack_1_t4016656541_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t4016656541_gp_0_0_0_0 = { 1, GenInst_Stack_1_t4016656541_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t546412149_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t546412149_gp_0_0_0_0_Types[] = { &Enumerator_t546412149_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t546412149_gp_0_0_0_0 = { 1, GenInst_Enumerator_t546412149_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_All_m2363499768_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_All_m2363499768_gp_0_0_0_0_Types[] = { &Enumerable_All_m2363499768_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_All_m2363499768_gp_0_0_0_0 = { 1, GenInst_Enumerable_All_m2363499768_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_All_m2363499768_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_All_m2363499768_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_All_m2363499768_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_All_m2363499768_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m665396702_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m665396702_gp_0_0_0_0_Types[] = { &Enumerable_Any_m665396702_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m665396702_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m665396702_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m2739389357_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Types[] = { &Enumerable_Any_m2739389357_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_Any_m2739389357_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_Cast_m2974870241_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Cast_m2974870241_gp_0_0_0_0_Types[] = { &Enumerable_Cast_m2974870241_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Cast_m2974870241_gp_0_0_0_0 = { 1, GenInst_Enumerable_Cast_m2974870241_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateCastIterator_m3836917259_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateCastIterator_m3836917259_gp_0_0_0_0_Types[] = { &Enumerable_CreateCastIterator_m3836917259_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateCastIterator_m3836917259_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateCastIterator_m3836917259_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m1284016302_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m1284016302_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m1284016302_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m1284016302_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m1284016302_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m4622279_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m4622279_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m4622279_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m4622279_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m4622279_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Count_m1561720045_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Count_m1561720045_gp_0_0_0_0_Types[] = { &Enumerable_Count_m1561720045_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Count_m1561720045_gp_0_0_0_0 = { 1, GenInst_Enumerable_Count_m1561720045_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Count_m136242780_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Count_m136242780_gp_0_0_0_0_Types[] = { &Enumerable_Count_m136242780_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Count_m136242780_gp_0_0_0_0 = { 1, GenInst_Enumerable_Count_m136242780_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Count_m136242780_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_Count_m136242780_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Count_m136242780_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Count_m136242780_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_Distinct_m4171187007_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Distinct_m4171187007_gp_0_0_0_0_Types[] = { &Enumerable_Distinct_m4171187007_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Distinct_m4171187007_gp_0_0_0_0 = { 1, GenInst_Enumerable_Distinct_m4171187007_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Distinct_m522457978_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Distinct_m522457978_gp_0_0_0_0_Types[] = { &Enumerable_Distinct_m522457978_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Distinct_m522457978_gp_0_0_0_0 = { 1, GenInst_Enumerable_Distinct_m522457978_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateDistinctIterator_m503395888_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateDistinctIterator_m503395888_gp_0_0_0_0_Types[] = { &Enumerable_CreateDistinctIterator_m503395888_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateDistinctIterator_m503395888_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateDistinctIterator_m503395888_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ElementAt_m258442918_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ElementAt_m258442918_gp_0_0_0_0_Types[] = { &Enumerable_ElementAt_m258442918_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ElementAt_m258442918_gp_0_0_0_0 = { 1, GenInst_Enumerable_ElementAt_m258442918_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ElementAt_m714932326_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ElementAt_m714932326_gp_0_0_0_0_Types[] = { &Enumerable_ElementAt_m714932326_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ElementAt_m714932326_gp_0_0_0_0 = { 1, GenInst_Enumerable_ElementAt_m714932326_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_First_m4120844597_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m4120844597_gp_0_0_0_0_Types[] = { &Enumerable_First_m4120844597_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m4120844597_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m4120844597_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_First_m4120844597_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_First_m4120844597_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m4120844597_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_First_m4120844597_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_First_m1693250038_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m1693250038_gp_0_0_0_0_Types[] = { &Enumerable_First_m1693250038_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m1693250038_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m1693250038_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0_Types[] = { &Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0 = { 1, GenInst_Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0;
extern const Il2CppType List_1_t2888087137_0_0_0;
static const Il2CppType* GenInst_Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0_List_1_t2888087137_0_0_0_Types[] = { &Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0, &List_1_t2888087137_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0_List_1_t2888087137_0_0_0 = { 2, GenInst_Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0_List_1_t2888087137_0_0_0_Types };
extern const Il2CppType Enumerable_ContainsGroup_m1146262421_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_ContainsGroup_m1146262421_gp_1_0_0_0_Types[] = { &Enumerable_ContainsGroup_m1146262421_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ContainsGroup_m1146262421_gp_1_0_0_0 = { 1, GenInst_Enumerable_ContainsGroup_m1146262421_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0_Types[] = { &Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0 = { 1, GenInst_Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_GroupBy_m1544473094_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_GroupBy_m1544473094_gp_0_0_0_0_Types[] = { &Enumerable_GroupBy_m1544473094_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1544473094_gp_0_0_0_0 = { 1, GenInst_Enumerable_GroupBy_m1544473094_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_GroupBy_m1544473094_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_GroupBy_m1544473094_gp_0_0_0_0_Enumerable_GroupBy_m1544473094_gp_1_0_0_0_Types[] = { &Enumerable_GroupBy_m1544473094_gp_0_0_0_0, &Enumerable_GroupBy_m1544473094_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1544473094_gp_0_0_0_0_Enumerable_GroupBy_m1544473094_gp_1_0_0_0 = { 2, GenInst_Enumerable_GroupBy_m1544473094_gp_0_0_0_0_Enumerable_GroupBy_m1544473094_gp_1_0_0_0_Types };
extern const Il2CppType IGrouping_2_t3129334088_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t3129334088_0_0_0_Types[] = { &IGrouping_2_t3129334088_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t3129334088_0_0_0 = { 1, GenInst_IGrouping_2_t3129334088_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_GroupBy_m1544473094_gp_1_0_0_0_Enumerable_GroupBy_m1544473094_gp_0_0_0_0_Types[] = { &Enumerable_GroupBy_m1544473094_gp_1_0_0_0, &Enumerable_GroupBy_m1544473094_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1544473094_gp_1_0_0_0_Enumerable_GroupBy_m1544473094_gp_0_0_0_0 = { 2, GenInst_Enumerable_GroupBy_m1544473094_gp_1_0_0_0_Enumerable_GroupBy_m1544473094_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_GroupBy_m1259221415_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_GroupBy_m1259221415_gp_0_0_0_0_Types[] = { &Enumerable_GroupBy_m1259221415_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1259221415_gp_0_0_0_0 = { 1, GenInst_Enumerable_GroupBy_m1259221415_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_GroupBy_m1259221415_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_GroupBy_m1259221415_gp_0_0_0_0_Enumerable_GroupBy_m1259221415_gp_1_0_0_0_Types[] = { &Enumerable_GroupBy_m1259221415_gp_0_0_0_0, &Enumerable_GroupBy_m1259221415_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1259221415_gp_0_0_0_0_Enumerable_GroupBy_m1259221415_gp_1_0_0_0 = { 2, GenInst_Enumerable_GroupBy_m1259221415_gp_0_0_0_0_Enumerable_GroupBy_m1259221415_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_GroupBy_m1259221415_gp_1_0_0_0_Types[] = { &Enumerable_GroupBy_m1259221415_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1259221415_gp_1_0_0_0 = { 1, GenInst_Enumerable_GroupBy_m1259221415_gp_1_0_0_0_Types };
extern const Il2CppType IGrouping_2_t3129334089_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t3129334089_0_0_0_Types[] = { &IGrouping_2_t3129334089_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t3129334089_0_0_0 = { 1, GenInst_IGrouping_2_t3129334089_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_GroupBy_m1259221415_gp_1_0_0_0_Enumerable_GroupBy_m1259221415_gp_0_0_0_0_Types[] = { &Enumerable_GroupBy_m1259221415_gp_1_0_0_0, &Enumerable_GroupBy_m1259221415_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_GroupBy_m1259221415_gp_1_0_0_0_Enumerable_GroupBy_m1259221415_gp_0_0_0_0 = { 2, GenInst_Enumerable_GroupBy_m1259221415_gp_1_0_0_0_Enumerable_GroupBy_m1259221415_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0_Types[] = { &Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0_Types[] = { &Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0, &Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0_Types[] = { &Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0_Types };
extern const Il2CppType IGrouping_2_t2588622000_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t2588622000_0_0_0_Types[] = { &IGrouping_2_t2588622000_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t2588622000_0_0_0 = { 1, GenInst_IGrouping_2_t2588622000_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0_Types[] = { &Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0, &Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0 = { 2, GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OfType_m192007909_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OfType_m192007909_gp_0_0_0_0_Types[] = { &Enumerable_OfType_m192007909_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OfType_m192007909_gp_0_0_0_0 = { 1, GenInst_Enumerable_OfType_m192007909_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateOfTypeIterator_m1172444379_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateOfTypeIterator_m1172444379_gp_0_0_0_0_Types[] = { &Enumerable_CreateOfTypeIterator_m1172444379_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateOfTypeIterator_m1172444379_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateOfTypeIterator_m1172444379_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m920500904_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Types[] = { &Enumerable_OrderBy_m920500904_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m920500904_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m920500904_gp_0_0_0_0, &Enumerable_OrderBy_m920500904_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0 = { 2, GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m4222740363_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Types[] = { &Enumerable_OrderBy_m4222740363_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m4222740363_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m4222740363_gp_0_0_0_0, &Enumerable_OrderBy_m4222740363_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0 = { 2, GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m4222740363_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m2459603006_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0 = { 1, GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m2459603006_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_0_0_0_0, &Enumerable_Select_m2459603006_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0 = { 2, GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0_Types[] = { &Enumerable_Select_m2459603006_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0 = { 1, GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0, &Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_SelectMany_m608128666_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0_Types[] = { &Enumerable_SelectMany_m608128666_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0 = { 1, GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t380606227_0_0_0;
static const Il2CppType* GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0_IEnumerable_1_t380606227_0_0_0_Types[] = { &Enumerable_SelectMany_m608128666_gp_0_0_0_0, &IEnumerable_1_t380606227_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0_IEnumerable_1_t380606227_0_0_0 = { 2, GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0_IEnumerable_1_t380606227_0_0_0_Types };
extern const Il2CppType Enumerable_SelectMany_m608128666_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_SelectMany_m608128666_gp_1_0_0_0_Types[] = { &Enumerable_SelectMany_m608128666_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SelectMany_m608128666_gp_1_0_0_0 = { 1, GenInst_Enumerable_SelectMany_m608128666_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0_Enumerable_SelectMany_m608128666_gp_1_0_0_0_Types[] = { &Enumerable_SelectMany_m608128666_gp_0_0_0_0, &Enumerable_SelectMany_m608128666_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0_Enumerable_SelectMany_m608128666_gp_1_0_0_0 = { 2, GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0_Enumerable_SelectMany_m608128666_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0_Types[] = { &Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t567845041_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0_IEnumerable_1_t567845041_0_0_0_Types[] = { &Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0, &IEnumerable_1_t567845041_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0_IEnumerable_1_t567845041_0_0_0 = { 2, GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0_IEnumerable_1_t567845041_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0_Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0, &Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0_Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0_Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_Single_m1979188101_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0_Types[] = { &Enumerable_Single_m1979188101_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0 = { 1, GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_Single_m1979188101_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_Single_m2156153962_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Single_m2156153962_gp_0_0_0_0_Types[] = { &Enumerable_Single_m2156153962_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m2156153962_gp_0_0_0_0 = { 1, GenInst_Enumerable_Single_m2156153962_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Single_m2776017211_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Single_m2776017211_gp_0_0_0_0_Types[] = { &Enumerable_Single_m2776017211_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m2776017211_gp_0_0_0_0 = { 1, GenInst_Enumerable_Single_m2776017211_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Single_m2776017211_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_Single_m2776017211_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m2776017211_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Single_m2776017211_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_Sum_m3284247776_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Sum_m3284247776_gp_0_0_0_0_Types[] = { &Enumerable_Sum_m3284247776_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Sum_m3284247776_gp_0_0_0_0 = { 1, GenInst_Enumerable_Sum_m3284247776_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Sum_m3284247776_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &Enumerable_Sum_m3284247776_gp_0_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Sum_m3284247776_gp_0_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Enumerable_Sum_m3284247776_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType Enumerable_ToArray_m2343256994_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0_Types[] = { &Enumerable_ToArray_m2343256994_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToList_m261161385_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0_Types[] = { &Enumerable_ToList_m261161385_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Where_m2409552823_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Types[] = { &Enumerable_Where_m2409552823_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_Where_m2409552823_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t54777913_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t54777913_gp_0_0_0_0_Types[] = { &U3CCreateCastIteratorU3Ec__Iterator0_1_t54777913_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t54777913_gp_0_0_0_0 = { 1, GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t54777913_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3711089181_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3711089181_gp_0_0_0_0_Types[] = { &U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3711089181_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3711089181_gp_0_0_0_0 = { 1, GenInst_U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3711089181_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0;
extern const Il2CppType U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0_Types[] = { &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0, &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0 = { 2, GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0_Types };
extern const Il2CppType IGrouping_2_t1824977620_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t1824977620_0_0_0_Types[] = { &IGrouping_2_t1824977620_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t1824977620_0_0_0 = { 1, GenInst_IGrouping_2_t1824977620_0_0_0_Types };
extern const Il2CppType List_1_t3797843219_0_0_0;
static const Il2CppType* GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_List_1_t3797843219_0_0_0_Types[] = { &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0, &List_1_t3797843219_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_List_1_t3797843219_0_0_0 = { 2, GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_List_1_t3797843219_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0_Types[] = { &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0 = { 1, GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_Types[] = { &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0, &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0 = { 2, GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_Types[] = { &U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0 = { 1, GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t3057470804_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t3057470804_gp_0_0_0_0_Types[] = { &U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t3057470804_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t3057470804_gp_0_0_0_0 = { 1, GenInst_U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t3057470804_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 = { 2, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0_Types[] = { &U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0 = { 1, GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0_Types[] = { &U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0 = { 1, GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3792789022_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0_IEnumerable_1_t3792789022_0_0_0_Types[] = { &U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0, &IEnumerable_1_t3792789022_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0_IEnumerable_1_t3792789022_0_0_0 = { 2, GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0_IEnumerable_1_t3792789022_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0_Types[] = { &U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0, &U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0 = { 2, GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0, &Boolean_t3825574718_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
extern const Il2CppType Grouping_2_t288249757_gp_1_0_0_0;
static const Il2CppType* GenInst_Grouping_2_t288249757_gp_1_0_0_0_Types[] = { &Grouping_2_t288249757_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Grouping_2_t288249757_gp_1_0_0_0 = { 1, GenInst_Grouping_2_t288249757_gp_1_0_0_0_Types };
extern const Il2CppType Grouping_2_t288249757_gp_0_0_0_0;
static const Il2CppType* GenInst_Grouping_2_t288249757_gp_0_0_0_0_Grouping_2_t288249757_gp_1_0_0_0_Types[] = { &Grouping_2_t288249757_gp_0_0_0_0, &Grouping_2_t288249757_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Grouping_2_t288249757_gp_0_0_0_0_Grouping_2_t288249757_gp_1_0_0_0 = { 2, GenInst_Grouping_2_t288249757_gp_0_0_0_0_Grouping_2_t288249757_gp_1_0_0_0_Types };
extern const Il2CppType IGrouping_2_t845301090_gp_1_0_0_0;
static const Il2CppType* GenInst_IGrouping_2_t845301090_gp_1_0_0_0_Types[] = { &IGrouping_2_t845301090_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IGrouping_2_t845301090_gp_1_0_0_0 = { 1, GenInst_IGrouping_2_t845301090_gp_1_0_0_0_Types };
extern const Il2CppType IOrderedEnumerable_1_t641749975_gp_0_0_0_0;
static const Il2CppType* GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_Types[] = { &IOrderedEnumerable_1_t641749975_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0 = { 1, GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_Types };
extern const Il2CppType OrderedEnumerable_1_t753306046_gp_0_0_0_0;
static const Il2CppType* GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_Types[] = { &OrderedEnumerable_1_t753306046_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0 = { 1, GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_Types };
extern const Il2CppType OrderedSequence_2_t1023848160_gp_0_0_0_0;
static const Il2CppType* GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_Types[] = { &OrderedSequence_2_t1023848160_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0 = { 1, GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_Types };
extern const Il2CppType OrderedSequence_2_t1023848160_gp_1_0_0_0;
static const Il2CppType* GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types[] = { &OrderedSequence_2_t1023848160_gp_0_0_0_0, &OrderedSequence_2_t1023848160_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0 = { 2, GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types[] = { &OrderedSequence_2_t1023848160_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0 = { 1, GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types };
extern const Il2CppType QuickSort_1_t1290221672_gp_0_0_0_0;
static const Il2CppType* GenInst_QuickSort_1_t1290221672_gp_0_0_0_0_Types[] = { &QuickSort_1_t1290221672_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_QuickSort_1_t1290221672_gp_0_0_0_0 = { 1, GenInst_QuickSort_1_t1290221672_gp_0_0_0_0_Types };
extern const Il2CppType U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0_Types[] = { &U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0 = { 1, GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0_Types };
extern const Il2CppType SortContext_1_t4088581714_gp_0_0_0_0;
static const Il2CppType* GenInst_SortContext_1_t4088581714_gp_0_0_0_0_Types[] = { &SortContext_1_t4088581714_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortContext_1_t4088581714_gp_0_0_0_0 = { 1, GenInst_SortContext_1_t4088581714_gp_0_0_0_0_Types };
extern const Il2CppType SortSequenceContext_2_t3419387730_gp_0_0_0_0;
static const Il2CppType* GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_Types[] = { &SortSequenceContext_2_t3419387730_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0 = { 1, GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_Types };
extern const Il2CppType SortSequenceContext_2_t3419387730_gp_1_0_0_0;
static const Il2CppType* GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types[] = { &SortSequenceContext_2_t3419387730_gp_0_0_0_0, &SortSequenceContext_2_t3419387730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0 = { 2, GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types[] = { &SortSequenceContext_2_t3419387730_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0 = { 1, GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types };
extern const Il2CppType HashSet_1_t2624254809_gp_0_0_0_0;
static const Il2CppType* GenInst_HashSet_1_t2624254809_gp_0_0_0_0_Types[] = { &HashSet_1_t2624254809_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t2624254809_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t2624254809_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2109956843_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2109956843_gp_0_0_0_0_Types[] = { &Enumerator_t2109956843_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2109956843_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2109956843_gp_0_0_0_0_Types };
extern const Il2CppType PrimeHelper_t3424417428_gp_0_0_0_0;
static const Il2CppType* GenInst_PrimeHelper_t3424417428_gp_0_0_0_0_Types[] = { &PrimeHelper_t3424417428_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PrimeHelper_t3424417428_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t3424417428_gp_0_0_0_0_Types };
extern const Il2CppType Tuple_2_t1951933832_gp_0_0_0_0;
extern const Il2CppType Tuple_2_t1951933832_gp_1_0_0_0;
static const Il2CppType* GenInst_Tuple_2_t1951933832_gp_0_0_0_0_Tuple_2_t1951933832_gp_1_0_0_0_Types[] = { &Tuple_2_t1951933832_gp_0_0_0_0, &Tuple_2_t1951933832_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Tuple_2_t1951933832_gp_0_0_0_0_Tuple_2_t1951933832_gp_1_0_0_0 = { 2, GenInst_Tuple_2_t1951933832_gp_0_0_0_0_Tuple_2_t1951933832_gp_1_0_0_0_Types };
extern const Il2CppType ThreadLocal_1_t1818133092_gp_0_0_0_0;
static const Il2CppType* GenInst_ThreadLocal_1_t1818133092_gp_0_0_0_0_Types[] = { &ThreadLocal_1_t1818133092_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadLocal_1_t1818133092_gp_0_0_0_0 = { 1, GenInst_ThreadLocal_1_t1818133092_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_ThreadLocal_1_t1818133092_gp_0_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &ThreadLocal_1_t1818133092_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_ThreadLocal_1_t1818133092_gp_0_0_0_0 = { 2, GenInst_Int64_t909078037_0_0_0_ThreadLocal_1_t1818133092_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ThreadLocal_1_t1818133092_gp_0_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ThreadLocal_1_t1818133092_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ThreadLocal_1_t1818133092_gp_0_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ThreadLocal_1_t1818133092_gp_0_0_0_0_Types };
extern const Il2CppType TaskCompletionSource_1_t1185916947_gp_0_0_0_0;
static const Il2CppType* GenInst_TaskCompletionSource_1_t1185916947_gp_0_0_0_0_Types[] = { &TaskCompletionSource_1_t1185916947_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TaskCompletionSource_1_t1185916947_gp_0_0_0_0 = { 1, GenInst_TaskCompletionSource_1_t1185916947_gp_0_0_0_0_Types };
extern const Il2CppType Task_ContinueWith_m340847350_gp_0_0_0_0;
static const Il2CppType* GenInst_Task_t1843236107_0_0_0_Task_ContinueWith_m340847350_gp_0_0_0_0_Types[] = { &Task_t1843236107_0_0_0, &Task_ContinueWith_m340847350_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_t1843236107_0_0_0_Task_ContinueWith_m340847350_gp_0_0_0_0 = { 2, GenInst_Task_t1843236107_0_0_0_Task_ContinueWith_m340847350_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Task_ContinueWith_m340847350_gp_0_0_0_0_Types[] = { &Task_ContinueWith_m340847350_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_ContinueWith_m340847350_gp_0_0_0_0 = { 1, GenInst_Task_ContinueWith_m340847350_gp_0_0_0_0_Types };
extern const Il2CppType Task_ContinueWith_m4241933427_gp_0_0_0_0;
static const Il2CppType* GenInst_Task_t1843236107_0_0_0_Task_ContinueWith_m4241933427_gp_0_0_0_0_Types[] = { &Task_t1843236107_0_0_0, &Task_ContinueWith_m4241933427_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_t1843236107_0_0_0_Task_ContinueWith_m4241933427_gp_0_0_0_0 = { 2, GenInst_Task_t1843236107_0_0_0_Task_ContinueWith_m4241933427_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Task_ContinueWith_m4241933427_gp_0_0_0_0_Types[] = { &Task_ContinueWith_m4241933427_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_ContinueWith_m4241933427_gp_0_0_0_0 = { 1, GenInst_Task_ContinueWith_m4241933427_gp_0_0_0_0_Types };
extern const Il2CppType U3CContinueWithU3Ec__AnonStorey0_1_t290911130_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CContinueWithU3Ec__AnonStorey0_1_t290911130_gp_0_0_0_0_Types[] = { &U3CContinueWithU3Ec__AnonStorey0_1_t290911130_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CContinueWithU3Ec__AnonStorey0_1_t290911130_gp_0_0_0_0 = { 1, GenInst_U3CContinueWithU3Ec__AnonStorey0_1_t290911130_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Task_t1843236107_0_0_0_U3CContinueWithU3Ec__AnonStorey0_1_t290911130_gp_0_0_0_0_Types[] = { &Task_t1843236107_0_0_0, &U3CContinueWithU3Ec__AnonStorey0_1_t290911130_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_t1843236107_0_0_0_U3CContinueWithU3Ec__AnonStorey0_1_t290911130_gp_0_0_0_0 = { 2, GenInst_Task_t1843236107_0_0_0_U3CContinueWithU3Ec__AnonStorey0_1_t290911130_gp_0_0_0_0_Types };
extern const Il2CppType U3CContinueWithU3Ec__AnonStorey1_t2941869902_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CContinueWithU3Ec__AnonStorey1_t2941869902_gp_0_0_0_0_Types[] = { &U3CContinueWithU3Ec__AnonStorey1_t2941869902_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CContinueWithU3Ec__AnonStorey1_t2941869902_gp_0_0_0_0 = { 1, GenInst_U3CContinueWithU3Ec__AnonStorey1_t2941869902_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Task_t1843236107_0_0_0_U3CContinueWithU3Ec__AnonStorey1_t2941869902_gp_0_0_0_0_Types[] = { &Task_t1843236107_0_0_0, &U3CContinueWithU3Ec__AnonStorey1_t2941869902_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_t1843236107_0_0_0_U3CContinueWithU3Ec__AnonStorey1_t2941869902_gp_0_0_0_0 = { 2, GenInst_Task_t1843236107_0_0_0_U3CContinueWithU3Ec__AnonStorey1_t2941869902_gp_0_0_0_0_Types };
extern const Il2CppType Task_1_t2975743850_0_0_0;
static const Il2CppType* GenInst_Task_1_t2975743850_0_0_0_Types[] = { &Task_1_t2975743850_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t2975743850_0_0_0 = { 1, GenInst_Task_1_t2975743850_0_0_0_Types };
extern const Il2CppType Task_1_t3208082394_gp_0_0_0_0;
static const Il2CppType* GenInst_Task_1_t3208082394_gp_0_0_0_0_Types[] = { &Task_1_t3208082394_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t3208082394_gp_0_0_0_0 = { 1, GenInst_Task_1_t3208082394_gp_0_0_0_0_Types };
extern const Il2CppType Task_1_ContinueWith_m1882222058_gp_0_0_0_0;
static const Il2CppType* GenInst_Task_1_t2975743850_0_0_0_Task_1_ContinueWith_m1882222058_gp_0_0_0_0_Types[] = { &Task_1_t2975743850_0_0_0, &Task_1_ContinueWith_m1882222058_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t2975743850_0_0_0_Task_1_ContinueWith_m1882222058_gp_0_0_0_0 = { 2, GenInst_Task_1_t2975743850_0_0_0_Task_1_ContinueWith_m1882222058_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Task_1_ContinueWith_m1882222058_gp_0_0_0_0_Types[] = { &Task_1_ContinueWith_m1882222058_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_ContinueWith_m1882222058_gp_0_0_0_0 = { 1, GenInst_Task_1_ContinueWith_m1882222058_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Task_1_t3208082394_gp_0_0_0_0_Task_1_ContinueWith_m1882222058_gp_0_0_0_0_Types[] = { &Task_1_t3208082394_gp_0_0_0_0, &Task_1_ContinueWith_m1882222058_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t3208082394_gp_0_0_0_0_Task_1_ContinueWith_m1882222058_gp_0_0_0_0 = { 2, GenInst_Task_1_t3208082394_gp_0_0_0_0_Task_1_ContinueWith_m1882222058_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Task_t1843236107_0_0_0_Task_1_ContinueWith_m1882222058_gp_0_0_0_0_Types[] = { &Task_t1843236107_0_0_0, &Task_1_ContinueWith_m1882222058_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_t1843236107_0_0_0_Task_1_ContinueWith_m1882222058_gp_0_0_0_0 = { 2, GenInst_Task_t1843236107_0_0_0_Task_1_ContinueWith_m1882222058_gp_0_0_0_0_Types };
extern const Il2CppType Task_1_t1425623332_0_0_0;
static const Il2CppType* GenInst_Task_1_t1425623332_0_0_0_Types[] = { &Task_1_t1425623332_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t1425623332_0_0_0 = { 1, GenInst_Task_1_t1425623332_0_0_0_Types };
extern const Il2CppType U3CContinueWithU3Ec__AnonStorey0_t1657961876_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CContinueWithU3Ec__AnonStorey0_t1657961876_gp_0_0_0_0_Types[] = { &U3CContinueWithU3Ec__AnonStorey0_t1657961876_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CContinueWithU3Ec__AnonStorey0_t1657961876_gp_0_0_0_0 = { 1, GenInst_U3CContinueWithU3Ec__AnonStorey0_t1657961876_gp_0_0_0_0_Types };
extern const Il2CppType Task_1_t1202168276_0_0_0;
extern const Il2CppType U3CContinueWithU3Ec__AnonStorey1_1_t1434506820_gp_1_0_0_0;
static const Il2CppType* GenInst_Task_1_t1202168276_0_0_0_U3CContinueWithU3Ec__AnonStorey1_1_t1434506820_gp_1_0_0_0_Types[] = { &Task_1_t1202168276_0_0_0, &U3CContinueWithU3Ec__AnonStorey1_1_t1434506820_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t1202168276_0_0_0_U3CContinueWithU3Ec__AnonStorey1_1_t1434506820_gp_1_0_0_0 = { 2, GenInst_Task_1_t1202168276_0_0_0_U3CContinueWithU3Ec__AnonStorey1_1_t1434506820_gp_1_0_0_0_Types };
extern const Il2CppType U3CContinueWithU3Ec__AnonStorey1_1_t1434506820_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CContinueWithU3Ec__AnonStorey1_1_t1434506820_gp_0_0_0_0_Types[] = { &U3CContinueWithU3Ec__AnonStorey1_1_t1434506820_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CContinueWithU3Ec__AnonStorey1_1_t1434506820_gp_0_0_0_0 = { 1, GenInst_U3CContinueWithU3Ec__AnonStorey1_1_t1434506820_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentInChildren_m3417738402_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0_Types[] = { &Component_GetComponentInChildren_m3417738402_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m825036157_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m825036157_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m3873375864_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m3873375864_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m1600202230_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m1600202230_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m3990064736_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0_Types[] = { &Component_GetComponents_m3990064736_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m2051523689_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0_Types[] = { &Component_GetComponents_m2051523689_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0_Types[] = { &GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponents_m2621570726_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0_Types[] = { &GameObject_GetComponents_m2621570726_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0_Types[] = { &Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0 = { 1, GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SafeLength_m3101579087_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0_Types[] = { &Mesh_SafeLength_m3101579087_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0 = { 1, GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetListForChannel_m3999848894_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0_Types[] = { &Mesh_SetListForChannel_m3999848894_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetListForChannel_m4171325764_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0_Types[] = { &Mesh_SetListForChannel_m4171325764_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0_Types[] = { &Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0 = { 1, GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0_Types };
extern const Il2CppType Object_Instantiate_m2530741872_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0_Types[] = { &Object_Instantiate_m2530741872_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0 = { 1, GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0_Types };
extern const Il2CppType Object_FindObjectsOfType_m894835059_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0_Types[] = { &Object_FindObjectsOfType_m894835059_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0_Types };
extern const Il2CppType GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0_Types[] = { &GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0 = { 1, GenInst_GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0_Types };
extern const Il2CppType AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0;
static const Il2CppType* GenInst_AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0_Types[] = { &AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0 = { 1, GenInst_AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0_Types };
extern const Il2CppType CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0;
static const Il2CppType* GenInst_CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0_Types[] = { &CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0 = { 1, GenInst_CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_1_t476640868_gp_0_0_0_0;
static const Il2CppType* GenInst_InvokableCall_1_t476640868_gp_0_0_0_0_Types[] = { &InvokableCall_1_t476640868_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t476640868_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t476640868_gp_0_0_0_0_Types };
extern const Il2CppType UnityAction_1_t2490859068_0_0_0;
static const Il2CppType* GenInst_UnityAction_1_t2490859068_0_0_0_Types[] = { &UnityAction_1_t2490859068_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityAction_1_t2490859068_0_0_0 = { 1, GenInst_UnityAction_1_t2490859068_0_0_0_Types };
extern const Il2CppType InvokableCall_2_t2042724809_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t2042724809_gp_1_0_0_0;
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_0_0_0_0, &InvokableCall_2_t2042724809_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2042724809_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0_Types };
extern const Il2CppType InvokableCall_3_t3608808750_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_2_0_0_0;
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_0_0_0_0, &InvokableCall_3_t3608808750_gp_1_0_0_0, &InvokableCall_3_t3608808750_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3608808750_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0_Types };
extern const Il2CppType InvokableCall_4_t879925395_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_3_0_0_0;
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_0_0_0_0, &InvokableCall_4_t879925395_gp_1_0_0_0, &InvokableCall_4_t879925395_gp_2_0_0_0, &InvokableCall_4_t879925395_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_1_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_2_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t879925395_gp_3_0_0_0_Types[] = { &InvokableCall_4_t879925395_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_3_0_0_0_Types };
extern const Il2CppType CachedInvokableCall_1_t224769006_gp_0_0_0_0;
static const Il2CppType* GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0_Types[] = { &CachedInvokableCall_1_t224769006_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_1_t4075366602_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0_Types[] = { &UnityEvent_1_t4075366602_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_2_t4075366599_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t4075366599_gp_1_0_0_0;
static const Il2CppType* GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0_Types[] = { &UnityEvent_2_t4075366599_gp_0_0_0_0, &UnityEvent_2_t4075366599_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0_Types };
extern const Il2CppType UnityEvent_3_t4075366600_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_2_0_0_0;
static const Il2CppType* GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0_Types[] = { &UnityEvent_3_t4075366600_gp_0_0_0_0, &UnityEvent_3_t4075366600_gp_1_0_0_0, &UnityEvent_3_t4075366600_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0_Types };
extern const Il2CppType UnityEvent_4_t4075366597_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_3_0_0_0;
static const Il2CppType* GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0_Types[] = { &UnityEvent_4_t4075366597_gp_0_0_0_0, &UnityEvent_4_t4075366597_gp_1_0_0_0, &UnityEvent_4_t4075366597_gp_2_0_0_0, &UnityEvent_4_t4075366597_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0_Types };
extern const Il2CppType Dictionary_2_t3757567216_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t3757567216_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Dictionary_2_t3757567216_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t3757567216_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t3757567216_0_0_0_Types };
extern const Il2CppType FirebaseConfigExtensions_GetState_m2920295301_gp_0_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_FirebaseConfigExtensions_GetState_m2920295301_gp_0_0_0_0_Types[] = { &String_t_0_0_0, &FirebaseConfigExtensions_GetState_m2920295301_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FirebaseConfigExtensions_GetState_m2920295301_gp_0_0_0_0 = { 2, GenInst_String_t_0_0_0_FirebaseConfigExtensions_GetState_m2920295301_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t3758243676_0_0_0;
static const Il2CppType* GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t3758243676_0_0_0_Types[] = { &Int32_t2071877448_0_0_0, &Dictionary_2_t3758243676_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t3758243676_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t3758243676_0_0_0_Types };
extern const Il2CppType FirebaseConfigExtensions_SetState_m771261708_gp_0_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_FirebaseConfigExtensions_SetState_m771261708_gp_0_0_0_0_Types[] = { &String_t_0_0_0, &FirebaseConfigExtensions_SetState_m771261708_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_FirebaseConfigExtensions_SetState_m771261708_gp_0_0_0_0 = { 2, GenInst_String_t_0_0_0_FirebaseConfigExtensions_SetState_m771261708_gp_0_0_0_0_Types };
extern const Il2CppType ScheduledThreadPoolExecutor_Schedule_m1378582659_gp_0_0_0_0;
static const Il2CppType* GenInst_ScheduledThreadPoolExecutor_Schedule_m1378582659_gp_0_0_0_0_Types[] = { &ScheduledThreadPoolExecutor_Schedule_m1378582659_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ScheduledThreadPoolExecutor_Schedule_m1378582659_gp_0_0_0_0 = { 1, GenInst_ScheduledThreadPoolExecutor_Schedule_m1378582659_gp_0_0_0_0_Types };
extern const Il2CppType Task_1_t1733150135_gp_0_0_0_0;
static const Il2CppType* GenInst_Task_1_t1733150135_gp_0_0_0_0_Types[] = { &Task_1_t1733150135_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Task_1_t1733150135_gp_0_0_0_0 = { 1, GenInst_Task_1_t1733150135_gp_0_0_0_0_Types };
extern const Il2CppType MessageDigest_1_t895280350_gp_0_0_0_0;
static const Il2CppType* GenInst_MessageDigest_1_t895280350_gp_0_0_0_0_Types[] = { &MessageDigest_1_t895280350_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageDigest_1_t895280350_gp_0_0_0_0 = { 1, GenInst_MessageDigest_1_t895280350_gp_0_0_0_0_Types };
extern const Il2CppType Extensions_Get_m2190249218_gp_0_0_0_0;
extern const Il2CppType Extensions_Get_m2190249218_gp_1_0_0_0;
static const Il2CppType* GenInst_Extensions_Get_m2190249218_gp_0_0_0_0_Extensions_Get_m2190249218_gp_1_0_0_0_Types[] = { &Extensions_Get_m2190249218_gp_0_0_0_0, &Extensions_Get_m2190249218_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Extensions_Get_m2190249218_gp_0_0_0_0_Extensions_Get_m2190249218_gp_1_0_0_0 = { 2, GenInst_Extensions_Get_m2190249218_gp_0_0_0_0_Extensions_Get_m2190249218_gp_1_0_0_0_Types };
extern const Il2CppType Extensions_IsEmpty_m18954539_gp_0_0_0_0;
static const Il2CppType* GenInst_Extensions_IsEmpty_m18954539_gp_0_0_0_0_Types[] = { &Extensions_IsEmpty_m18954539_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Extensions_IsEmpty_m18954539_gp_0_0_0_0 = { 1, GenInst_Extensions_IsEmpty_m18954539_gp_0_0_0_0_Types };
extern const Il2CppType Extensions_Remove_m1109595446_gp_0_0_0_0;
static const Il2CppType* GenInst_Extensions_Remove_m1109595446_gp_0_0_0_0_Types[] = { &Extensions_Remove_m1109595446_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Extensions_Remove_m1109595446_gp_0_0_0_0 = { 1, GenInst_Extensions_Remove_m1109595446_gp_0_0_0_0_Types };
extern const Il2CppType Extensions_Contains_m1742031028_gp_0_0_0_0;
extern const Il2CppType Extensions_Contains_m1742031028_gp_1_0_0_0;
static const Il2CppType* GenInst_Extensions_Contains_m1742031028_gp_0_0_0_0_Extensions_Contains_m1742031028_gp_1_0_0_0_Types[] = { &Extensions_Contains_m1742031028_gp_0_0_0_0, &Extensions_Contains_m1742031028_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Extensions_Contains_m1742031028_gp_0_0_0_0_Extensions_Contains_m1742031028_gp_1_0_0_0 = { 2, GenInst_Extensions_Contains_m1742031028_gp_0_0_0_0_Extensions_Contains_m1742031028_gp_1_0_0_0_Types };
extern const Il2CppType Extensions_Sort_m1675725510_gp_0_0_0_0;
static const Il2CppType* GenInst_Extensions_Sort_m1675725510_gp_0_0_0_0_Types[] = { &Extensions_Sort_m1675725510_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Extensions_Sort_m1675725510_gp_0_0_0_0 = { 1, GenInst_Extensions_Sort_m1675725510_gp_0_0_0_0_Types };
extern const Il2CppType Extensions_Sort_m434348948_gp_0_0_0_0;
static const Il2CppType* GenInst_Extensions_Sort_m434348948_gp_0_0_0_0_Types[] = { &Extensions_Sort_m434348948_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Extensions_Sort_m434348948_gp_0_0_0_0 = { 1, GenInst_Extensions_Sort_m434348948_gp_0_0_0_0_Types };
extern const Il2CppType Extensions_SubList_m3529398206_gp_0_0_0_0;
static const Il2CppType* GenInst_Extensions_SubList_m3529398206_gp_0_0_0_0_Types[] = { &Extensions_SubList_m3529398206_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Extensions_SubList_m3529398206_gp_0_0_0_0 = { 1, GenInst_Extensions_SubList_m3529398206_gp_0_0_0_0_Types };
extern const Il2CppType Collections_1_t2510947384_gp_0_0_0_0;
static const Il2CppType* GenInst_Collections_1_t2510947384_gp_0_0_0_0_Types[] = { &Collections_1_t2510947384_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collections_1_t2510947384_gp_0_0_0_0 = { 1, GenInst_Collections_1_t2510947384_gp_0_0_0_0_Types };
extern const Il2CppType Collections_AddAll_m1767255324_gp_0_0_0_0;
static const Il2CppType* GenInst_Collections_AddAll_m1767255324_gp_0_0_0_0_Types[] = { &Collections_AddAll_m1767255324_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collections_AddAll_m1767255324_gp_0_0_0_0 = { 1, GenInst_Collections_AddAll_m1767255324_gp_0_0_0_0_Types };
extern const Il2CppType Collections_Remove_m1549833231_gp_0_0_0_0;
extern const Il2CppType Collections_Remove_m1549833231_gp_1_0_0_0;
static const Il2CppType* GenInst_Collections_Remove_m1549833231_gp_0_0_0_0_Collections_Remove_m1549833231_gp_1_0_0_0_Types[] = { &Collections_Remove_m1549833231_gp_0_0_0_0, &Collections_Remove_m1549833231_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Collections_Remove_m1549833231_gp_0_0_0_0_Collections_Remove_m1549833231_gp_1_0_0_0 = { 2, GenInst_Collections_Remove_m1549833231_gp_0_0_0_0_Collections_Remove_m1549833231_gp_1_0_0_0_Types };
extern const Il2CppType Collections_ToArray_m3354275710_gp_0_0_0_0;
static const Il2CppType* GenInst_Collections_ToArray_m3354275710_gp_0_0_0_0_Types[] = { &Collections_ToArray_m3354275710_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collections_ToArray_m3354275710_gp_0_0_0_0 = { 1, GenInst_Collections_ToArray_m3354275710_gp_0_0_0_0_Types };
extern const Il2CppType Collections_EmptyMap_m1916522621_gp_0_0_0_0;
extern const Il2CppType Collections_EmptyMap_m1916522621_gp_1_0_0_0;
static const Il2CppType* GenInst_Collections_EmptyMap_m1916522621_gp_0_0_0_0_Collections_EmptyMap_m1916522621_gp_1_0_0_0_Types[] = { &Collections_EmptyMap_m1916522621_gp_0_0_0_0, &Collections_EmptyMap_m1916522621_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Collections_EmptyMap_m1916522621_gp_0_0_0_0_Collections_EmptyMap_m1916522621_gp_1_0_0_0 = { 2, GenInst_Collections_EmptyMap_m1916522621_gp_0_0_0_0_Collections_EmptyMap_m1916522621_gp_1_0_0_0_Types };
extern const Il2CppType Collections_EmptyList_m2155311715_gp_0_0_0_0;
static const Il2CppType* GenInst_Collections_EmptyList_m2155311715_gp_0_0_0_0_Types[] = { &Collections_EmptyList_m2155311715_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collections_EmptyList_m2155311715_gp_0_0_0_0 = { 1, GenInst_Collections_EmptyList_m2155311715_gp_0_0_0_0_Types };
extern const Il2CppType Collections_GetEmptyList_m994968909_gp_0_0_0_0;
static const Il2CppType* GenInst_Collections_GetEmptyList_m994968909_gp_0_0_0_0_Types[] = { &Collections_GetEmptyList_m994968909_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collections_GetEmptyList_m994968909_gp_0_0_0_0 = { 1, GenInst_Collections_GetEmptyList_m994968909_gp_0_0_0_0_Types };
extern const Il2CppType Collections_SingletonList_m1687744405_gp_0_0_0_0;
static const Il2CppType* GenInst_Collections_SingletonList_m1687744405_gp_0_0_0_0_Types[] = { &Collections_SingletonList_m1687744405_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collections_SingletonList_m1687744405_gp_0_0_0_0 = { 1, GenInst_Collections_SingletonList_m1687744405_gp_0_0_0_0_Types };
extern const Il2CppType Collections_UnmodifiableList_m1396560940_gp_0_0_0_0;
static const Il2CppType* GenInst_Collections_UnmodifiableList_m1396560940_gp_0_0_0_0_Types[] = { &Collections_UnmodifiableList_m1396560940_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collections_UnmodifiableList_m1396560940_gp_0_0_0_0 = { 1, GenInst_Collections_UnmodifiableList_m1396560940_gp_0_0_0_0_Types };
extern const Il2CppType BlockingCollection_1_t1344468038_gp_0_0_0_0;
static const Il2CppType* GenInst_BlockingCollection_1_t1344468038_gp_0_0_0_0_Types[] = { &BlockingCollection_1_t1344468038_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BlockingCollection_1_t1344468038_gp_0_0_0_0 = { 1, GenInst_BlockingCollection_1_t1344468038_gp_0_0_0_0_Types };
extern const Il2CppType Arrays_AsList_m1664585568_gp_0_0_0_0;
static const Il2CppType* GenInst_Arrays_AsList_m1664585568_gp_0_0_0_0_Types[] = { &Arrays_AsList_m1664585568_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Arrays_AsList_m1664585568_gp_0_0_0_0 = { 1, GenInst_Arrays_AsList_m1664585568_gp_0_0_0_0_Types };
extern const Il2CppType GenericGenerator_1_t2598493761_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericGenerator_1_t2598493761_gp_0_0_0_0_Types[] = { &GenericGenerator_1_t2598493761_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericGenerator_1_t2598493761_gp_0_0_0_0 = { 1, GenInst_GenericGenerator_1_t2598493761_gp_0_0_0_0_Types };
extern const Il2CppType GenericGeneratorEnumerator_1_t484336861_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericGeneratorEnumerator_1_t484336861_gp_0_0_0_0_Types[] = { &GenericGeneratorEnumerator_1_t484336861_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericGeneratorEnumerator_1_t484336861_gp_0_0_0_0 = { 1, GenInst_GenericGeneratorEnumerator_1_t484336861_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t2244783541_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t2244783541_gp_0_0_0_0_Types[] = { &List_1_t2244783541_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2244783541_gp_0_0_0_0 = { 1, GenInst_List_1_t2244783541_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t264254312_0_0_0;
static const Il2CppType* GenInst_List_1_t264254312_0_0_0_Types[] = { &List_1_t264254312_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t264254312_0_0_0 = { 1, GenInst_List_1_t264254312_0_0_0_Types };
extern const Il2CppType ArraySortedMap_2_t2832718474_gp_0_0_0_0;
extern const Il2CppType ArraySortedMap_2_t2832718474_gp_1_0_0_0;
static const Il2CppType* GenInst_ArraySortedMap_2_t2832718474_gp_0_0_0_0_ArraySortedMap_2_t2832718474_gp_1_0_0_0_Types[] = { &ArraySortedMap_2_t2832718474_gp_0_0_0_0, &ArraySortedMap_2_t2832718474_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySortedMap_2_t2832718474_gp_0_0_0_0_ArraySortedMap_2_t2832718474_gp_1_0_0_0 = { 2, GenInst_ArraySortedMap_2_t2832718474_gp_0_0_0_0_ArraySortedMap_2_t2832718474_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ArraySortedMap_2_t2832718474_gp_0_0_0_0_Types[] = { &ArraySortedMap_2_t2832718474_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySortedMap_2_t2832718474_gp_0_0_0_0 = { 1, GenInst_ArraySortedMap_2_t2832718474_gp_0_0_0_0_Types };
extern const Il2CppType ArraySortedMap_2_BuildFrom_m271241364_gp_0_0_0_0;
static const Il2CppType* GenInst_ArraySortedMap_2_BuildFrom_m271241364_gp_0_0_0_0_Types[] = { &ArraySortedMap_2_BuildFrom_m271241364_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySortedMap_2_BuildFrom_m271241364_gp_0_0_0_0 = { 1, GenInst_ArraySortedMap_2_BuildFrom_m271241364_gp_0_0_0_0_Types };
extern const Il2CppType ArraySortedMap_2_BuildFrom_m271241364_gp_1_0_0_0;
extern const Il2CppType ArraySortedMap_2_BuildFrom_m271241364_gp_2_0_0_0;
static const Il2CppType* GenInst_ArraySortedMap_2_BuildFrom_m271241364_gp_1_0_0_0_ArraySortedMap_2_BuildFrom_m271241364_gp_2_0_0_0_Types[] = { &ArraySortedMap_2_BuildFrom_m271241364_gp_1_0_0_0, &ArraySortedMap_2_BuildFrom_m271241364_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySortedMap_2_BuildFrom_m271241364_gp_1_0_0_0_ArraySortedMap_2_BuildFrom_m271241364_gp_2_0_0_0 = { 2, GenInst_ArraySortedMap_2_BuildFrom_m271241364_gp_1_0_0_0_ArraySortedMap_2_BuildFrom_m271241364_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_ArraySortedMap_2_BuildFrom_m271241364_gp_0_0_0_0_ArraySortedMap_2_BuildFrom_m271241364_gp_2_0_0_0_Types[] = { &ArraySortedMap_2_BuildFrom_m271241364_gp_0_0_0_0, &ArraySortedMap_2_BuildFrom_m271241364_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySortedMap_2_BuildFrom_m271241364_gp_0_0_0_0_ArraySortedMap_2_BuildFrom_m271241364_gp_2_0_0_0 = { 2, GenInst_ArraySortedMap_2_BuildFrom_m271241364_gp_0_0_0_0_ArraySortedMap_2_BuildFrom_m271241364_gp_2_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2825879006_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2825879006_0_0_0_Types[] = { &KeyValuePair_2_t2825879006_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2825879006_0_0_0 = { 1, GenInst_KeyValuePair_2_t2825879006_0_0_0_Types };
extern const Il2CppType Enumerator134_t1654254710_gp_0_0_0_0;
extern const Il2CppType Enumerator134_t1654254710_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator134_t1654254710_gp_0_0_0_0_Enumerator134_t1654254710_gp_1_0_0_0_Types[] = { &Enumerator134_t1654254710_gp_0_0_0_0, &Enumerator134_t1654254710_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator134_t1654254710_gp_0_0_0_0_Enumerator134_t1654254710_gp_1_0_0_0 = { 2, GenInst_Enumerator134_t1654254710_gp_0_0_0_0_Enumerator134_t1654254710_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1795570862_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1795570862_0_0_0_Types[] = { &KeyValuePair_2_t1795570862_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1795570862_0_0_0 = { 1, GenInst_KeyValuePair_2_t1795570862_0_0_0_Types };
static const Il2CppType* GenInst_ArraySortedMap_2_t2832718474_gp_0_0_0_0_ArraySortedMap_2_t2832718474_gp_0_0_0_0_ArraySortedMap_2_t2832718474_gp_1_0_0_0_Types[] = { &ArraySortedMap_2_t2832718474_gp_0_0_0_0, &ArraySortedMap_2_t2832718474_gp_0_0_0_0, &ArraySortedMap_2_t2832718474_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySortedMap_2_t2832718474_gp_0_0_0_0_ArraySortedMap_2_t2832718474_gp_0_0_0_0_ArraySortedMap_2_t2832718474_gp_1_0_0_0 = { 3, GenInst_ArraySortedMap_2_t2832718474_gp_0_0_0_0_ArraySortedMap_2_t2832718474_gp_0_0_0_0_ArraySortedMap_2_t2832718474_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ArraySortedMap_2_t2832718474_gp_1_0_0_0_Types[] = { &ArraySortedMap_2_t2832718474_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ArraySortedMap_2_t2832718474_gp_1_0_0_0 = { 1, GenInst_ArraySortedMap_2_t2832718474_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t467411514_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t467411514_0_0_0_Types[] = { &KeyValuePair_2_t467411514_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t467411514_0_0_0 = { 1, GenInst_KeyValuePair_2_t467411514_0_0_0_Types };
extern const Il2CppType ImmutableSortedMap_2_t1726168363_gp_0_0_0_0;
extern const Il2CppType ImmutableSortedMap_2_t1726168363_gp_1_0_0_0;
static const Il2CppType* GenInst_ImmutableSortedMap_2_t1726168363_gp_0_0_0_0_ImmutableSortedMap_2_t1726168363_gp_1_0_0_0_Types[] = { &ImmutableSortedMap_2_t1726168363_gp_0_0_0_0, &ImmutableSortedMap_2_t1726168363_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ImmutableSortedMap_2_t1726168363_gp_0_0_0_0_ImmutableSortedMap_2_t1726168363_gp_1_0_0_0 = { 2, GenInst_ImmutableSortedMap_2_t1726168363_gp_0_0_0_0_ImmutableSortedMap_2_t1726168363_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ImmutableSortedMap_2_t1726168363_gp_0_0_0_0_Types[] = { &ImmutableSortedMap_2_t1726168363_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ImmutableSortedMap_2_t1726168363_gp_0_0_0_0 = { 1, GenInst_ImmutableSortedMap_2_t1726168363_gp_0_0_0_0_Types };
extern const Il2CppType Builder_t4024572244_gp_0_0_0_0;
static const Il2CppType* GenInst_Builder_t4024572244_gp_0_0_0_0_Types[] = { &Builder_t4024572244_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Builder_t4024572244_gp_0_0_0_0 = { 1, GenInst_Builder_t4024572244_gp_0_0_0_0_Types };
extern const Il2CppType Builder_t4024572244_gp_1_0_0_0;
static const Il2CppType* GenInst_Builder_t4024572244_gp_0_0_0_0_Builder_t4024572244_gp_1_0_0_0_Types[] = { &Builder_t4024572244_gp_0_0_0_0, &Builder_t4024572244_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Builder_t4024572244_gp_0_0_0_0_Builder_t4024572244_gp_1_0_0_0 = { 2, GenInst_Builder_t4024572244_gp_0_0_0_0_Builder_t4024572244_gp_1_0_0_0_Types };
extern const Il2CppType Builder_FromMap_m4216204579_gp_0_0_0_0;
extern const Il2CppType Builder_FromMap_m4216204579_gp_1_0_0_0;
static const Il2CppType* GenInst_Builder_FromMap_m4216204579_gp_0_0_0_0_Builder_FromMap_m4216204579_gp_1_0_0_0_Types[] = { &Builder_FromMap_m4216204579_gp_0_0_0_0, &Builder_FromMap_m4216204579_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Builder_FromMap_m4216204579_gp_0_0_0_0_Builder_FromMap_m4216204579_gp_1_0_0_0 = { 2, GenInst_Builder_FromMap_m4216204579_gp_0_0_0_0_Builder_FromMap_m4216204579_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Builder_FromMap_m4216204579_gp_0_0_0_0_Types[] = { &Builder_FromMap_m4216204579_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Builder_FromMap_m4216204579_gp_0_0_0_0 = { 1, GenInst_Builder_FromMap_m4216204579_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3291920818_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3291920818_0_0_0_Types[] = { &KeyValuePair_2_t3291920818_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3291920818_0_0_0 = { 1, GenInst_KeyValuePair_2_t3291920818_0_0_0_Types };
extern const Il2CppType Builder_BuildFrom_m3982510148_gp_0_0_0_0;
static const Il2CppType* GenInst_Builder_BuildFrom_m3982510148_gp_0_0_0_0_Types[] = { &Builder_BuildFrom_m3982510148_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Builder_BuildFrom_m3982510148_gp_0_0_0_0 = { 1, GenInst_Builder_BuildFrom_m3982510148_gp_0_0_0_0_Types };
extern const Il2CppType Builder_BuildFrom_m3982510148_gp_1_0_0_0;
extern const Il2CppType Builder_BuildFrom_m3982510148_gp_2_0_0_0;
static const Il2CppType* GenInst_Builder_BuildFrom_m3982510148_gp_1_0_0_0_Builder_BuildFrom_m3982510148_gp_2_0_0_0_Types[] = { &Builder_BuildFrom_m3982510148_gp_1_0_0_0, &Builder_BuildFrom_m3982510148_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Builder_BuildFrom_m3982510148_gp_1_0_0_0_Builder_BuildFrom_m3982510148_gp_2_0_0_0 = { 2, GenInst_Builder_BuildFrom_m3982510148_gp_1_0_0_0_Builder_BuildFrom_m3982510148_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_Builder_BuildFrom_m3982510148_gp_0_0_0_0_Builder_BuildFrom_m3982510148_gp_2_0_0_0_Types[] = { &Builder_BuildFrom_m3982510148_gp_0_0_0_0, &Builder_BuildFrom_m3982510148_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Builder_BuildFrom_m3982510148_gp_0_0_0_0_Builder_BuildFrom_m3982510148_gp_2_0_0_0 = { 2, GenInst_Builder_BuildFrom_m3982510148_gp_0_0_0_0_Builder_BuildFrom_m3982510148_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_Builder_BuildFrom_m3982510148_gp_0_0_0_0_Builder_BuildFrom_m3982510148_gp_1_0_0_0_Builder_BuildFrom_m3982510148_gp_2_0_0_0_Types[] = { &Builder_BuildFrom_m3982510148_gp_0_0_0_0, &Builder_BuildFrom_m3982510148_gp_1_0_0_0, &Builder_BuildFrom_m3982510148_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_Builder_BuildFrom_m3982510148_gp_0_0_0_0_Builder_BuildFrom_m3982510148_gp_1_0_0_0_Builder_BuildFrom_m3982510148_gp_2_0_0_0 = { 3, GenInst_Builder_BuildFrom_m3982510148_gp_0_0_0_0_Builder_BuildFrom_m3982510148_gp_1_0_0_0_Builder_BuildFrom_m3982510148_gp_2_0_0_0_Types };
extern const Il2CppType KeyTranslator109_t3529733654_gp_0_0_0_0;
extern const Il2CppType KeyTranslator109_t3529733654_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyTranslator109_t3529733654_gp_0_0_0_0_KeyTranslator109_t3529733654_gp_1_0_0_0_Types[] = { &KeyTranslator109_t3529733654_gp_0_0_0_0, &KeyTranslator109_t3529733654_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyTranslator109_t3529733654_gp_0_0_0_0_KeyTranslator109_t3529733654_gp_1_0_0_0 = { 2, GenInst_KeyTranslator109_t3529733654_gp_0_0_0_0_KeyTranslator109_t3529733654_gp_1_0_0_0_Types };
extern const Il2CppType ImmutableSortedMapIterator_2_t1801858573_gp_0_0_0_0;
extern const Il2CppType ImmutableSortedMapIterator_2_t1801858573_gp_1_0_0_0;
static const Il2CppType* GenInst_ImmutableSortedMapIterator_2_t1801858573_gp_0_0_0_0_ImmutableSortedMapIterator_2_t1801858573_gp_1_0_0_0_Types[] = { &ImmutableSortedMapIterator_2_t1801858573_gp_0_0_0_0, &ImmutableSortedMapIterator_2_t1801858573_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ImmutableSortedMapIterator_2_t1801858573_gp_0_0_0_0_ImmutableSortedMapIterator_2_t1801858573_gp_1_0_0_0 = { 2, GenInst_ImmutableSortedMapIterator_2_t1801858573_gp_0_0_0_0_ImmutableSortedMapIterator_2_t1801858573_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ImmutableSortedMapIterator_2_t1801858573_gp_0_0_0_0_Types[] = { &ImmutableSortedMapIterator_2_t1801858573_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ImmutableSortedMapIterator_2_t1801858573_gp_0_0_0_0 = { 1, GenInst_ImmutableSortedMapIterator_2_t1801858573_gp_0_0_0_0_Types };
extern const Il2CppType LlrbValueNode_2_t411157183_0_0_0;
static const Il2CppType* GenInst_LlrbValueNode_2_t411157183_0_0_0_Types[] = { &LlrbValueNode_2_t411157183_0_0_0 };
extern const Il2CppGenericInst GenInst_LlrbValueNode_2_t411157183_0_0_0 = { 1, GenInst_LlrbValueNode_2_t411157183_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3448121458_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3448121458_0_0_0_Types[] = { &KeyValuePair_2_t3448121458_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3448121458_0_0_0 = { 1, GenInst_KeyValuePair_2_t3448121458_0_0_0_Types };
extern const Il2CppType ImmutableSortedSet_1_t3924744372_gp_0_0_0_0;
static const Il2CppType* GenInst_ImmutableSortedSet_1_t3924744372_gp_0_0_0_0_Types[] = { &ImmutableSortedSet_1_t3924744372_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ImmutableSortedSet_1_t3924744372_gp_0_0_0_0 = { 1, GenInst_ImmutableSortedSet_1_t3924744372_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_ImmutableSortedSet_1_t3924744372_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &ImmutableSortedSet_1_t3924744372_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ImmutableSortedSet_1_t3924744372_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ImmutableSortedSet_1_t3924744372_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2148234354_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2148234354_0_0_0_Types[] = { &KeyValuePair_2_t2148234354_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2148234354_0_0_0 = { 1, GenInst_KeyValuePair_2_t2148234354_0_0_0_Types };
extern const Il2CppType WrappedEntryIterator_t2059579541_gp_0_0_0_0;
static const Il2CppType* GenInst_WrappedEntryIterator_t2059579541_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &WrappedEntryIterator_t2059579541_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_WrappedEntryIterator_t2059579541_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_WrappedEntryIterator_t2059579541_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_WrappedEntryIterator_t2059579541_gp_0_0_0_0_Types[] = { &WrappedEntryIterator_t2059579541_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_WrappedEntryIterator_t2059579541_gp_0_0_0_0 = { 1, GenInst_WrappedEntryIterator_t2059579541_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_ImmutableSortedSet_1_t3924744372_gp_0_0_0_0_ImmutableSortedSet_1_t3924744372_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &ImmutableSortedSet_1_t3924744372_gp_0_0_0_0, &ImmutableSortedSet_1_t3924744372_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ImmutableSortedSet_1_t3924744372_gp_0_0_0_0_ImmutableSortedSet_1_t3924744372_gp_0_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_ImmutableSortedSet_1_t3924744372_gp_0_0_0_0_ImmutableSortedSet_1_t3924744372_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType LlrbBlackValueNode_2_t2021173780_gp_0_0_0_0;
extern const Il2CppType LlrbBlackValueNode_2_t2021173780_gp_1_0_0_0;
static const Il2CppType* GenInst_LlrbBlackValueNode_2_t2021173780_gp_0_0_0_0_LlrbBlackValueNode_2_t2021173780_gp_1_0_0_0_Types[] = { &LlrbBlackValueNode_2_t2021173780_gp_0_0_0_0, &LlrbBlackValueNode_2_t2021173780_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_LlrbBlackValueNode_2_t2021173780_gp_0_0_0_0_LlrbBlackValueNode_2_t2021173780_gp_1_0_0_0 = { 2, GenInst_LlrbBlackValueNode_2_t2021173780_gp_0_0_0_0_LlrbBlackValueNode_2_t2021173780_gp_1_0_0_0_Types };
extern const Il2CppType LlrbEmptyNode_2_t339901673_gp_0_0_0_0;
extern const Il2CppType LlrbEmptyNode_2_t339901673_gp_1_0_0_0;
static const Il2CppType* GenInst_LlrbEmptyNode_2_t339901673_gp_0_0_0_0_LlrbEmptyNode_2_t339901673_gp_1_0_0_0_Types[] = { &LlrbEmptyNode_2_t339901673_gp_0_0_0_0, &LlrbEmptyNode_2_t339901673_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_LlrbEmptyNode_2_t339901673_gp_0_0_0_0_LlrbEmptyNode_2_t339901673_gp_1_0_0_0 = { 2, GenInst_LlrbEmptyNode_2_t339901673_gp_0_0_0_0_LlrbEmptyNode_2_t339901673_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_LlrbEmptyNode_2_t339901673_gp_0_0_0_0_Types[] = { &LlrbEmptyNode_2_t339901673_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LlrbEmptyNode_2_t339901673_gp_0_0_0_0 = { 1, GenInst_LlrbEmptyNode_2_t339901673_gp_0_0_0_0_Types };
extern const Il2CppType LlrbNode_2_t745868504_gp_0_0_0_0;
extern const Il2CppType LlrbNode_2_t745868504_gp_1_0_0_0;
static const Il2CppType* GenInst_LlrbNode_2_t745868504_gp_0_0_0_0_LlrbNode_2_t745868504_gp_1_0_0_0_Types[] = { &LlrbNode_2_t745868504_gp_0_0_0_0, &LlrbNode_2_t745868504_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_LlrbNode_2_t745868504_gp_0_0_0_0_LlrbNode_2_t745868504_gp_1_0_0_0 = { 2, GenInst_LlrbNode_2_t745868504_gp_0_0_0_0_LlrbNode_2_t745868504_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_LlrbNode_2_t745868504_gp_0_0_0_0_Types[] = { &LlrbNode_2_t745868504_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LlrbNode_2_t745868504_gp_0_0_0_0 = { 1, GenInst_LlrbNode_2_t745868504_gp_0_0_0_0_Types };
extern const Il2CppType Color_t3892504923_gp_0_0_0_0;
extern const Il2CppType Color_t3892504923_gp_1_0_0_0;
static const Il2CppType* GenInst_Color_t3892504923_gp_0_0_0_0_Color_t3892504923_gp_1_0_0_0_Types[] = { &Color_t3892504923_gp_0_0_0_0, &Color_t3892504923_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t3892504923_gp_0_0_0_0_Color_t3892504923_gp_1_0_0_0 = { 2, GenInst_Color_t3892504923_gp_0_0_0_0_Color_t3892504923_gp_1_0_0_0_Types };
extern const Il2CppType NodeVisitor_t367361012_gp_0_0_0_0;
extern const Il2CppType NodeVisitor_t367361012_gp_1_0_0_0;
static const Il2CppType* GenInst_NodeVisitor_t367361012_gp_0_0_0_0_NodeVisitor_t367361012_gp_1_0_0_0_Types[] = { &NodeVisitor_t367361012_gp_0_0_0_0, &NodeVisitor_t367361012_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_NodeVisitor_t367361012_gp_0_0_0_0_NodeVisitor_t367361012_gp_1_0_0_0 = { 2, GenInst_NodeVisitor_t367361012_gp_0_0_0_0_NodeVisitor_t367361012_gp_1_0_0_0_Types };
extern const Il2CppType LlrbRedValueNode_2_t2625247752_gp_0_0_0_0;
extern const Il2CppType LlrbRedValueNode_2_t2625247752_gp_1_0_0_0;
static const Il2CppType* GenInst_LlrbRedValueNode_2_t2625247752_gp_0_0_0_0_LlrbRedValueNode_2_t2625247752_gp_1_0_0_0_Types[] = { &LlrbRedValueNode_2_t2625247752_gp_0_0_0_0, &LlrbRedValueNode_2_t2625247752_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_LlrbRedValueNode_2_t2625247752_gp_0_0_0_0_LlrbRedValueNode_2_t2625247752_gp_1_0_0_0 = { 2, GenInst_LlrbRedValueNode_2_t2625247752_gp_0_0_0_0_LlrbRedValueNode_2_t2625247752_gp_1_0_0_0_Types };
extern const Il2CppType LlrbValueNode_2_t3934141949_gp_0_0_0_0;
extern const Il2CppType LlrbValueNode_2_t3934141949_gp_1_0_0_0;
static const Il2CppType* GenInst_LlrbValueNode_2_t3934141949_gp_0_0_0_0_LlrbValueNode_2_t3934141949_gp_1_0_0_0_Types[] = { &LlrbValueNode_2_t3934141949_gp_0_0_0_0, &LlrbValueNode_2_t3934141949_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_LlrbValueNode_2_t3934141949_gp_0_0_0_0_LlrbValueNode_2_t3934141949_gp_1_0_0_0 = { 2, GenInst_LlrbValueNode_2_t3934141949_gp_0_0_0_0_LlrbValueNode_2_t3934141949_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_LlrbValueNode_2_t3934141949_gp_0_0_0_0_Types[] = { &LlrbValueNode_2_t3934141949_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LlrbValueNode_2_t3934141949_gp_0_0_0_0 = { 1, GenInst_LlrbValueNode_2_t3934141949_gp_0_0_0_0_Types };
extern const Il2CppType RbTreeSortedMap_2_t470035893_gp_0_0_0_0;
extern const Il2CppType RbTreeSortedMap_2_t470035893_gp_1_0_0_0;
static const Il2CppType* GenInst_RbTreeSortedMap_2_t470035893_gp_0_0_0_0_RbTreeSortedMap_2_t470035893_gp_1_0_0_0_Types[] = { &RbTreeSortedMap_2_t470035893_gp_0_0_0_0, &RbTreeSortedMap_2_t470035893_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_RbTreeSortedMap_2_t470035893_gp_0_0_0_0_RbTreeSortedMap_2_t470035893_gp_1_0_0_0 = { 2, GenInst_RbTreeSortedMap_2_t470035893_gp_0_0_0_0_RbTreeSortedMap_2_t470035893_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_RbTreeSortedMap_2_t470035893_gp_0_0_0_0_Types[] = { &RbTreeSortedMap_2_t470035893_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RbTreeSortedMap_2_t470035893_gp_0_0_0_0 = { 1, GenInst_RbTreeSortedMap_2_t470035893_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2602970322_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2602970322_0_0_0_Types[] = { &KeyValuePair_2_t2602970322_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2602970322_0_0_0 = { 1, GenInst_KeyValuePair_2_t2602970322_0_0_0_Types };
extern const Il2CppType RbTreeSortedMap_2_BuildFrom_m3368602716_gp_0_0_0_0;
static const Il2CppType* GenInst_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_0_0_0_0_Types[] = { &RbTreeSortedMap_2_BuildFrom_m3368602716_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_0_0_0_0 = { 1, GenInst_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_0_0_0_0_Types };
extern const Il2CppType RbTreeSortedMap_2_BuildFrom_m3368602716_gp_1_0_0_0;
extern const Il2CppType RbTreeSortedMap_2_BuildFrom_m3368602716_gp_2_0_0_0;
static const Il2CppType* GenInst_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_1_0_0_0_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_2_0_0_0_Types[] = { &RbTreeSortedMap_2_BuildFrom_m3368602716_gp_1_0_0_0, &RbTreeSortedMap_2_BuildFrom_m3368602716_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_1_0_0_0_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_2_0_0_0 = { 2, GenInst_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_1_0_0_0_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_0_0_0_0_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_2_0_0_0_Types[] = { &RbTreeSortedMap_2_BuildFrom_m3368602716_gp_0_0_0_0, &RbTreeSortedMap_2_BuildFrom_m3368602716_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_0_0_0_0_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_2_0_0_0 = { 2, GenInst_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_0_0_0_0_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_RbTreeSortedMap_2_t470035893_gp_0_0_0_0_RbTreeSortedMap_2_t470035893_gp_1_0_0_0_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_0_0_0_0_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_1_0_0_0_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_2_0_0_0_Types[] = { &RbTreeSortedMap_2_t470035893_gp_0_0_0_0, &RbTreeSortedMap_2_t470035893_gp_1_0_0_0, &RbTreeSortedMap_2_BuildFrom_m3368602716_gp_0_0_0_0, &RbTreeSortedMap_2_BuildFrom_m3368602716_gp_1_0_0_0, &RbTreeSortedMap_2_BuildFrom_m3368602716_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_RbTreeSortedMap_2_t470035893_gp_0_0_0_0_RbTreeSortedMap_2_t470035893_gp_1_0_0_0_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_0_0_0_0_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_1_0_0_0_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_2_0_0_0 = { 5, GenInst_RbTreeSortedMap_2_t470035893_gp_0_0_0_0_RbTreeSortedMap_2_t470035893_gp_1_0_0_0_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_0_0_0_0_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_1_0_0_0_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_2_0_0_0_Types };
extern const Il2CppType RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0;
extern const Il2CppType RbTreeSortedMap_2_FromMap_m1608270809_gp_1_0_0_0;
static const Il2CppType* GenInst_RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0_RbTreeSortedMap_2_FromMap_m1608270809_gp_1_0_0_0_Types[] = { &RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0, &RbTreeSortedMap_2_FromMap_m1608270809_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0_RbTreeSortedMap_2_FromMap_m1608270809_gp_1_0_0_0 = { 2, GenInst_RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0_RbTreeSortedMap_2_FromMap_m1608270809_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0_Types[] = { &RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0 = { 1, GenInst_RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_RbTreeSortedMap_2_t470035893_gp_0_0_0_0_RbTreeSortedMap_2_t470035893_gp_1_0_0_0_RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0_RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0_RbTreeSortedMap_2_FromMap_m1608270809_gp_1_0_0_0_Types[] = { &RbTreeSortedMap_2_t470035893_gp_0_0_0_0, &RbTreeSortedMap_2_t470035893_gp_1_0_0_0, &RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0, &RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0, &RbTreeSortedMap_2_FromMap_m1608270809_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_RbTreeSortedMap_2_t470035893_gp_0_0_0_0_RbTreeSortedMap_2_t470035893_gp_1_0_0_0_RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0_RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0_RbTreeSortedMap_2_FromMap_m1608270809_gp_1_0_0_0 = { 5, GenInst_RbTreeSortedMap_2_t470035893_gp_0_0_0_0_RbTreeSortedMap_2_t470035893_gp_1_0_0_0_RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0_RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0_RbTreeSortedMap_2_FromMap_m1608270809_gp_1_0_0_0_Types };
extern const Il2CppType BuilderAbc_3_t2723596885_gp_2_0_0_0;
static const Il2CppType* GenInst_BuilderAbc_3_t2723596885_gp_2_0_0_0_Types[] = { &BuilderAbc_3_t2723596885_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_BuilderAbc_3_t2723596885_gp_2_0_0_0 = { 1, GenInst_BuilderAbc_3_t2723596885_gp_2_0_0_0_Types };
extern const Il2CppType BuilderAbc_3_t2723596885_gp_3_0_0_0;
extern const Il2CppType BuilderAbc_3_t2723596885_gp_4_0_0_0;
static const Il2CppType* GenInst_BuilderAbc_3_t2723596885_gp_3_0_0_0_BuilderAbc_3_t2723596885_gp_4_0_0_0_Types[] = { &BuilderAbc_3_t2723596885_gp_3_0_0_0, &BuilderAbc_3_t2723596885_gp_4_0_0_0 };
extern const Il2CppGenericInst GenInst_BuilderAbc_3_t2723596885_gp_3_0_0_0_BuilderAbc_3_t2723596885_gp_4_0_0_0 = { 2, GenInst_BuilderAbc_3_t2723596885_gp_3_0_0_0_BuilderAbc_3_t2723596885_gp_4_0_0_0_Types };
static const Il2CppType* GenInst_BuilderAbc_3_t2723596885_gp_2_0_0_0_BuilderAbc_3_t2723596885_gp_4_0_0_0_Types[] = { &BuilderAbc_3_t2723596885_gp_2_0_0_0, &BuilderAbc_3_t2723596885_gp_4_0_0_0 };
extern const Il2CppGenericInst GenInst_BuilderAbc_3_t2723596885_gp_2_0_0_0_BuilderAbc_3_t2723596885_gp_4_0_0_0 = { 2, GenInst_BuilderAbc_3_t2723596885_gp_2_0_0_0_BuilderAbc_3_t2723596885_gp_4_0_0_0_Types };
extern const Il2CppType BooleanChunk_t3755014355_0_0_0;
static const Il2CppType* GenInst_BooleanChunk_t3755014355_0_0_0_Types[] = { &BooleanChunk_t3755014355_0_0_0 };
extern const Il2CppGenericInst GenInst_BooleanChunk_t3755014355_0_0_0 = { 1, GenInst_BooleanChunk_t3755014355_0_0_0_Types };
extern const Il2CppType Base12_t182897086_gp_0_0_0_0;
extern const Il2CppType Base12_t182897086_gp_1_0_0_0;
extern const Il2CppType Base12_t182897086_gp_2_0_0_0;
extern const Il2CppType Base12_t182897086_gp_3_0_0_0;
extern const Il2CppType Base12_t182897086_gp_4_0_0_0;
static const Il2CppType* GenInst_Base12_t182897086_gp_0_0_0_0_Base12_t182897086_gp_1_0_0_0_Base12_t182897086_gp_2_0_0_0_Base12_t182897086_gp_3_0_0_0_Base12_t182897086_gp_4_0_0_0_Types[] = { &Base12_t182897086_gp_0_0_0_0, &Base12_t182897086_gp_1_0_0_0, &Base12_t182897086_gp_2_0_0_0, &Base12_t182897086_gp_3_0_0_0, &Base12_t182897086_gp_4_0_0_0 };
extern const Il2CppGenericInst GenInst_Base12_t182897086_gp_0_0_0_0_Base12_t182897086_gp_1_0_0_0_Base12_t182897086_gp_2_0_0_0_Base12_t182897086_gp_3_0_0_0_Base12_t182897086_gp_4_0_0_0 = { 5, GenInst_Base12_t182897086_gp_0_0_0_0_Base12_t182897086_gp_1_0_0_0_Base12_t182897086_gp_2_0_0_0_Base12_t182897086_gp_3_0_0_0_Base12_t182897086_gp_4_0_0_0_Types };
extern const Il2CppType Enumerator216_t1286610410_gp_0_0_0_0;
extern const Il2CppType Enumerator216_t1286610410_gp_1_0_0_0;
extern const Il2CppType Enumerator216_t1286610410_gp_2_0_0_0;
extern const Il2CppType Enumerator216_t1286610410_gp_3_0_0_0;
extern const Il2CppType Enumerator216_t1286610410_gp_4_0_0_0;
static const Il2CppType* GenInst_Enumerator216_t1286610410_gp_0_0_0_0_Enumerator216_t1286610410_gp_1_0_0_0_Enumerator216_t1286610410_gp_2_0_0_0_Enumerator216_t1286610410_gp_3_0_0_0_Enumerator216_t1286610410_gp_4_0_0_0_Types[] = { &Enumerator216_t1286610410_gp_0_0_0_0, &Enumerator216_t1286610410_gp_1_0_0_0, &Enumerator216_t1286610410_gp_2_0_0_0, &Enumerator216_t1286610410_gp_3_0_0_0, &Enumerator216_t1286610410_gp_4_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator216_t1286610410_gp_0_0_0_0_Enumerator216_t1286610410_gp_1_0_0_0_Enumerator216_t1286610410_gp_2_0_0_0_Enumerator216_t1286610410_gp_3_0_0_0_Enumerator216_t1286610410_gp_4_0_0_0 = { 5, GenInst_Enumerator216_t1286610410_gp_0_0_0_0_Enumerator216_t1286610410_gp_1_0_0_0_Enumerator216_t1286610410_gp_2_0_0_0_Enumerator216_t1286610410_gp_3_0_0_0_Enumerator216_t1286610410_gp_4_0_0_0_Types };
extern const Il2CppType BooleanChunk_t1065636191_0_0_0;
static const Il2CppType* GenInst_BooleanChunk_t1065636191_0_0_0_Types[] = { &BooleanChunk_t1065636191_0_0_0 };
extern const Il2CppGenericInst GenInst_BooleanChunk_t1065636191_0_0_0 = { 1, GenInst_BooleanChunk_t1065636191_0_0_0_Types };
extern const Il2CppType BuilderAbc_3_t2723596885_gp_0_0_0_0;
extern const Il2CppType BuilderAbc_3_t2723596885_gp_1_0_0_0;
static const Il2CppType* GenInst_BuilderAbc_3_t2723596885_gp_0_0_0_0_BuilderAbc_3_t2723596885_gp_1_0_0_0_BuilderAbc_3_t2723596885_gp_2_0_0_0_BuilderAbc_3_t2723596885_gp_3_0_0_0_BuilderAbc_3_t2723596885_gp_4_0_0_0_Types[] = { &BuilderAbc_3_t2723596885_gp_0_0_0_0, &BuilderAbc_3_t2723596885_gp_1_0_0_0, &BuilderAbc_3_t2723596885_gp_2_0_0_0, &BuilderAbc_3_t2723596885_gp_3_0_0_0, &BuilderAbc_3_t2723596885_gp_4_0_0_0 };
extern const Il2CppGenericInst GenInst_BuilderAbc_3_t2723596885_gp_0_0_0_0_BuilderAbc_3_t2723596885_gp_1_0_0_0_BuilderAbc_3_t2723596885_gp_2_0_0_0_BuilderAbc_3_t2723596885_gp_3_0_0_0_BuilderAbc_3_t2723596885_gp_4_0_0_0 = { 5, GenInst_BuilderAbc_3_t2723596885_gp_0_0_0_0_BuilderAbc_3_t2723596885_gp_1_0_0_0_BuilderAbc_3_t2723596885_gp_2_0_0_0_BuilderAbc_3_t2723596885_gp_3_0_0_0_BuilderAbc_3_t2723596885_gp_4_0_0_0_Types };
static const Il2CppType* GenInst_BuilderAbc_3_t2723596885_gp_2_0_0_0_BuilderAbc_3_t2723596885_gp_4_0_0_0_BuilderAbc_3_t2723596885_gp_2_0_0_0_BuilderAbc_3_t2723596885_gp_3_0_0_0_BuilderAbc_3_t2723596885_gp_4_0_0_0_Types[] = { &BuilderAbc_3_t2723596885_gp_2_0_0_0, &BuilderAbc_3_t2723596885_gp_4_0_0_0, &BuilderAbc_3_t2723596885_gp_2_0_0_0, &BuilderAbc_3_t2723596885_gp_3_0_0_0, &BuilderAbc_3_t2723596885_gp_4_0_0_0 };
extern const Il2CppGenericInst GenInst_BuilderAbc_3_t2723596885_gp_2_0_0_0_BuilderAbc_3_t2723596885_gp_4_0_0_0_BuilderAbc_3_t2723596885_gp_2_0_0_0_BuilderAbc_3_t2723596885_gp_3_0_0_0_BuilderAbc_3_t2723596885_gp_4_0_0_0 = { 5, GenInst_BuilderAbc_3_t2723596885_gp_2_0_0_0_BuilderAbc_3_t2723596885_gp_4_0_0_0_BuilderAbc_3_t2723596885_gp_2_0_0_0_BuilderAbc_3_t2723596885_gp_3_0_0_0_BuilderAbc_3_t2723596885_gp_4_0_0_0_Types };
extern const Il2CppType BooleanChunk_t3968502773_0_0_0;
static const Il2CppType* GenInst_BooleanChunk_t3968502773_0_0_0_Types[] = { &BooleanChunk_t3968502773_0_0_0 };
extern const Il2CppGenericInst GenInst_BooleanChunk_t3968502773_0_0_0 = { 1, GenInst_BooleanChunk_t3968502773_0_0_0_Types };
extern const Il2CppType StandardComparator_1_t225988210_gp_0_0_0_0;
static const Il2CppType* GenInst_StandardComparator_1_t225988210_gp_0_0_0_0_Types[] = { &StandardComparator_1_t225988210_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_StandardComparator_1_t225988210_gp_0_0_0_0 = { 1, GenInst_StandardComparator_1_t225988210_gp_0_0_0_0_Types };
extern const Il2CppType Repo_PostEvents_m1913162193_gp_0_0_0_0;
static const Il2CppType* GenInst_Repo_PostEvents_m1913162193_gp_0_0_0_0_Types[] = { &Repo_PostEvents_m1913162193_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Repo_PostEvents_m1913162193_gp_0_0_0_0 = { 1, GenInst_Repo_PostEvents_m1913162193_gp_0_0_0_0_Types };
extern const Il2CppType NoopPersistenceManager_RunInTransaction_m1482428390_gp_0_0_0_0;
static const Il2CppType* GenInst_NoopPersistenceManager_RunInTransaction_m1482428390_gp_0_0_0_0_Types[] = { &NoopPersistenceManager_RunInTransaction_m1482428390_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NoopPersistenceManager_RunInTransaction_m1482428390_gp_0_0_0_0 = { 1, GenInst_NoopPersistenceManager_RunInTransaction_m1482428390_gp_0_0_0_0_Types };
extern const Il2CppType IPersistenceManager_RunInTransaction_m3062294953_gp_0_0_0_0;
static const Il2CppType* GenInst_IPersistenceManager_RunInTransaction_m3062294953_gp_0_0_0_0_Types[] = { &IPersistenceManager_RunInTransaction_m3062294953_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IPersistenceManager_RunInTransaction_m3062294953_gp_0_0_0_0 = { 1, GenInst_IPersistenceManager_RunInTransaction_m3062294953_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2759104460_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2759104460_0_0_0_Types[] = { &KeyValuePair_2_t2759104460_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2759104460_0_0_0 = { 1, GenInst_KeyValuePair_2_t2759104460_0_0_0_Types };
extern const Il2CppType ImmutableTree_1_t749652817_gp_0_0_0_0;
static const Il2CppType* GenInst_Path_t2568473163_0_0_0_ImmutableTree_1_t749652817_gp_0_0_0_0_Types[] = { &Path_t2568473163_0_0_0, &ImmutableTree_1_t749652817_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Path_t2568473163_0_0_0_ImmutableTree_1_t749652817_gp_0_0_0_0 = { 2, GenInst_Path_t2568473163_0_0_0_ImmutableTree_1_t749652817_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_ImmutableTree_1_t749652817_gp_0_0_0_0_Types[] = { &ImmutableTree_1_t749652817_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ImmutableTree_1_t749652817_gp_0_0_0_0 = { 1, GenInst_ImmutableTree_1_t749652817_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_ImmutableTree_1_t749652817_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &ImmutableTree_1_t749652817_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ImmutableTree_1_t749652817_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ImmutableTree_1_t749652817_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType ImmutableTree_1_Fold_m1045730212_gp_0_0_0_0;
static const Il2CppType* GenInst_ImmutableTree_1_t749652817_gp_0_0_0_0_ImmutableTree_1_Fold_m1045730212_gp_0_0_0_0_Types[] = { &ImmutableTree_1_t749652817_gp_0_0_0_0, &ImmutableTree_1_Fold_m1045730212_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ImmutableTree_1_t749652817_gp_0_0_0_0_ImmutableTree_1_Fold_m1045730212_gp_0_0_0_0 = { 2, GenInst_ImmutableTree_1_t749652817_gp_0_0_0_0_ImmutableTree_1_Fold_m1045730212_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_ImmutableTree_1_Fold_m1045730212_gp_0_0_0_0_Types[] = { &ImmutableTree_1_Fold_m1045730212_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ImmutableTree_1_Fold_m1045730212_gp_0_0_0_0 = { 1, GenInst_ImmutableTree_1_Fold_m1045730212_gp_0_0_0_0_Types };
extern const Il2CppType ImmutableTree_1_Fold_m2866300079_gp_0_0_0_0;
static const Il2CppType* GenInst_ImmutableTree_1_t749652817_gp_0_0_0_0_ImmutableTree_1_Fold_m2866300079_gp_0_0_0_0_Types[] = { &ImmutableTree_1_t749652817_gp_0_0_0_0, &ImmutableTree_1_Fold_m2866300079_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ImmutableTree_1_t749652817_gp_0_0_0_0_ImmutableTree_1_Fold_m2866300079_gp_0_0_0_0 = { 2, GenInst_ImmutableTree_1_t749652817_gp_0_0_0_0_ImmutableTree_1_Fold_m2866300079_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_ImmutableTree_1_Fold_m2866300079_gp_0_0_0_0_Types[] = { &ImmutableTree_1_Fold_m2866300079_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ImmutableTree_1_Fold_m2866300079_gp_0_0_0_0 = { 1, GenInst_ImmutableTree_1_Fold_m2866300079_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2631867154_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2631867154_0_0_0_Types[] = { &KeyValuePair_2_t2631867154_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2631867154_0_0_0 = { 1, GenInst_KeyValuePair_2_t2631867154_0_0_0_Types };
extern const Il2CppType TreeVisitor278_t622415511_gp_0_0_0_0;
static const Il2CppType* GenInst_Path_t2568473163_0_0_0_TreeVisitor278_t622415511_gp_0_0_0_0_Types[] = { &Path_t2568473163_0_0_0, &TreeVisitor278_t622415511_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Path_t2568473163_0_0_0_TreeVisitor278_t622415511_gp_0_0_0_0 = { 2, GenInst_Path_t2568473163_0_0_0_TreeVisitor278_t622415511_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_TreeVisitor278_t622415511_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &TreeVisitor278_t622415511_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TreeVisitor278_t622415511_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_TreeVisitor278_t622415511_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType Predicate_1_t382063104_gp_0_0_0_0;
static const Il2CppType* GenInst_Predicate_1_t382063104_gp_0_0_0_0_Types[] = { &Predicate_1_t382063104_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Predicate_1_t382063104_gp_0_0_0_0 = { 1, GenInst_Predicate_1_t382063104_gp_0_0_0_0_Types };
extern const Il2CppType Predicate6_t1686426874_gp_0_0_0_0;
static const Il2CppType* GenInst_Predicate6_t1686426874_gp_0_0_0_0_Types[] = { &Predicate6_t1686426874_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Predicate6_t1686426874_gp_0_0_0_0 = { 1, GenInst_Predicate6_t1686426874_gp_0_0_0_0_Types };
extern const Il2CppType Tree_1_t1820556167_gp_0_0_0_0;
static const Il2CppType* GenInst_Tree_1_t1820556167_gp_0_0_0_0_Types[] = { &Tree_1_t1820556167_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Tree_1_t1820556167_gp_0_0_0_0 = { 1, GenInst_Tree_1_t1820556167_gp_0_0_0_0_Types };
extern const Il2CppType ITreeVisitor_t753513729_gp_0_0_0_0;
static const Il2CppType* GenInst_ITreeVisitor_t753513729_gp_0_0_0_0_Types[] = { &ITreeVisitor_t753513729_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ITreeVisitor_t753513729_gp_0_0_0_0 = { 1, GenInst_ITreeVisitor_t753513729_gp_0_0_0_0_Types };
extern const Il2CppType ITreeFilter_t3097072013_gp_0_0_0_0;
static const Il2CppType* GenInst_ITreeFilter_t3097072013_gp_0_0_0_0_Types[] = { &ITreeFilter_t3097072013_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ITreeFilter_t3097072013_gp_0_0_0_0 = { 1, GenInst_ITreeFilter_t3097072013_gp_0_0_0_0_Types };
extern const Il2CppType TreeVisitor111_t2665920001_gp_0_0_0_0;
static const Il2CppType* GenInst_TreeVisitor111_t2665920001_gp_0_0_0_0_Types[] = { &TreeVisitor111_t2665920001_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TreeVisitor111_t2665920001_gp_0_0_0_0 = { 1, GenInst_TreeVisitor111_t2665920001_gp_0_0_0_0_Types };
extern const Il2CppType TreeNode_1_t2853783343_gp_0_0_0_0;
static const Il2CppType* GenInst_TreeNode_1_t2853783343_gp_0_0_0_0_Types[] = { &TreeNode_1_t2853783343_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TreeNode_1_t2853783343_gp_0_0_0_0 = { 1, GenInst_TreeNode_1_t2853783343_gp_0_0_0_0_Types };
extern const Il2CppType EventRaiser_RaiseEvents_m1759473118_gp_0_0_0_0;
static const Il2CppType* GenInst_EventRaiser_RaiseEvents_m1759473118_gp_0_0_0_0_Types[] = { &EventRaiser_RaiseEvents_m1759473118_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EventRaiser_RaiseEvents_m1759473118_gp_0_0_0_0 = { 1, GenInst_EventRaiser_RaiseEvents_m1759473118_gp_0_0_0_0_Types };
extern const Il2CppType Pair_2_t3489481034_gp_0_0_0_0;
extern const Il2CppType Pair_2_t3489481034_gp_1_0_0_0;
static const Il2CppType* GenInst_Pair_2_t3489481034_gp_0_0_0_0_Pair_2_t3489481034_gp_1_0_0_0_Types[] = { &Pair_2_t3489481034_gp_0_0_0_0, &Pair_2_t3489481034_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Pair_2_t3489481034_gp_0_0_0_0_Pair_2_t3489481034_gp_1_0_0_0 = { 2, GenInst_Pair_2_t3489481034_gp_0_0_0_0_Pair_2_t3489481034_gp_1_0_0_0_Types };
extern const Il2CppType ExecuteEvents_Execute_m1961163955_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0_Types[] = { &ExecuteEvents_Execute_m1961163955_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0_Types[] = { &ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0_Types[] = { &ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0_Types };
extern const Il2CppType TweenRunner_1_t2584777480_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0_Types[] = { &TweenRunner_1_t2584777480_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0_Types };
extern const Il2CppType Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0;
static const Il2CppType* GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0_Types[] = { &Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0_Types };
extern const Il2CppType SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0;
static const Il2CppType* GenInst_SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0_Types[] = { &SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0 = { 1, GenInst_SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t573160278_gp_0_0_0_0;
static const Il2CppType* GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Types[] = { &IndexedSet_1_t573160278_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t573160278_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types[] = { &IndexedSet_1_t573160278_gp_0_0_0_0, &Int32_t2071877448_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types };
extern const Il2CppType ListPool_1_t1984115411_gp_0_0_0_0;
static const Il2CppType* GenInst_ListPool_1_t1984115411_gp_0_0_0_0_Types[] = { &ListPool_1_t1984115411_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListPool_1_t1984115411_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t1984115411_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t2000868992_0_0_0;
static const Il2CppType* GenInst_List_1_t2000868992_0_0_0_Types[] = { &List_1_t2000868992_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2000868992_0_0_0 = { 1, GenInst_List_1_t2000868992_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t4265859154_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0_Types[] = { &ObjectPool_1_t4265859154_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0_Types };
extern const Il2CppType BuildRequests_BuildRequestState_m127437801_gp_0_0_0_0;
static const Il2CppType* GenInst_BuildRequests_BuildRequestState_m127437801_gp_0_0_0_0_Types[] = { &BuildRequests_BuildRequestState_m127437801_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BuildRequests_BuildRequestState_m127437801_gp_0_0_0_0 = { 1, GenInst_BuildRequests_BuildRequestState_m127437801_gp_0_0_0_0_Types };
extern const Il2CppType BuildRequests_BuildRequestState_m1406497467_gp_0_0_0_0;
static const Il2CppType* GenInst_BuildRequests_BuildRequestState_m1406497467_gp_0_0_0_0_Types[] = { &BuildRequests_BuildRequestState_m1406497467_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BuildRequests_BuildRequestState_m1406497467_gp_0_0_0_0 = { 1, GenInst_BuildRequests_BuildRequestState_m1406497467_gp_0_0_0_0_Types };
extern const Il2CppType BuildRequests_BuildRequestState_m274720804_gp_0_0_0_0;
static const Il2CppType* GenInst_BuildRequests_BuildRequestState_m274720804_gp_0_0_0_0_Types[] = { &BuildRequests_BuildRequestState_m274720804_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BuildRequests_BuildRequestState_m274720804_gp_0_0_0_0 = { 1, GenInst_BuildRequests_BuildRequestState_m274720804_gp_0_0_0_0_Types };
extern const Il2CppType BuildRequests_BuildRestApiRequest_m1350603643_gp_0_0_0_0;
static const Il2CppType* GenInst_BuildRequests_BuildRestApiRequest_m1350603643_gp_0_0_0_0_Types[] = { &BuildRequests_BuildRestApiRequest_m1350603643_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_BuildRequests_BuildRestApiRequest_m1350603643_gp_0_0_0_0 = { 1, GenInst_BuildRequests_BuildRestApiRequest_m1350603643_gp_0_0_0_0_Types };
extern const Il2CppType EventExtensions_Raise_m516954942_gp_0_0_0_0;
static const Il2CppType* GenInst_EventExtensions_Raise_m516954942_gp_0_0_0_0_Types[] = { &EventExtensions_Raise_m516954942_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EventExtensions_Raise_m516954942_gp_0_0_0_0 = { 1, GenInst_EventExtensions_Raise_m516954942_gp_0_0_0_0_Types };
extern const Il2CppType CustomEventArgs_1_t1643748401_gp_0_0_0_0;
static const Il2CppType* GenInst_CustomEventArgs_1_t1643748401_gp_0_0_0_0_Types[] = { &CustomEventArgs_1_t1643748401_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomEventArgs_1_t1643748401_gp_0_0_0_0 = { 1, GenInst_CustomEventArgs_1_t1643748401_gp_0_0_0_0_Types };
extern const Il2CppType CoroutineParams_1_t1685449857_gp_0_0_0_0;
static const Il2CppType* GenInst_CoroutineParams_1_t1685449857_gp_0_0_0_0_Types[] = { &CoroutineParams_1_t1685449857_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CoroutineParams_1_t1685449857_gp_0_0_0_0 = { 1, GenInst_CoroutineParams_1_t1685449857_gp_0_0_0_0_Types };
extern const Il2CppType CoroutineClass_DelayStartCoroutine_m2034096782_gp_0_0_0_0;
static const Il2CppType* GenInst_CoroutineClass_DelayStartCoroutine_m2034096782_gp_0_0_0_0_Types[] = { &CoroutineClass_DelayStartCoroutine_m2034096782_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CoroutineClass_DelayStartCoroutine_m2034096782_gp_0_0_0_0 = { 1, GenInst_CoroutineClass_DelayStartCoroutine_m2034096782_gp_0_0_0_0_Types };
extern const Il2CppType CoroutineClass_Run_m3622408249_gp_0_0_0_0;
static const Il2CppType* GenInst_CoroutineClass_Run_m3622408249_gp_0_0_0_0_Types[] = { &CoroutineClass_Run_m3622408249_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CoroutineClass_Run_m3622408249_gp_0_0_0_0 = { 1, GenInst_CoroutineClass_Run_m3622408249_gp_0_0_0_0_Types };
extern const Il2CppType CoroutineClass_StartCoroutinesByName_m12538590_gp_0_0_0_0;
static const Il2CppType* GenInst_CoroutineClass_StartCoroutinesByName_m12538590_gp_0_0_0_0_Types[] = { &CoroutineClass_StartCoroutinesByName_m12538590_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CoroutineClass_StartCoroutinesByName_m12538590_gp_0_0_0_0 = { 1, GenInst_CoroutineClass_StartCoroutinesByName_m12538590_gp_0_0_0_0_Types };
extern const Il2CppType CoroutineClass_DelayRequest_m2804562785_gp_0_0_0_0;
static const Il2CppType* GenInst_CoroutineClass_DelayRequest_m2804562785_gp_0_0_0_0_Types[] = { &CoroutineClass_DelayRequest_m2804562785_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CoroutineClass_DelayRequest_m2804562785_gp_0_0_0_0 = { 1, GenInst_CoroutineClass_DelayRequest_m2804562785_gp_0_0_0_0_Types };
extern const Il2CppType CoroutineClass_ProcessResponse_m8201378_gp_0_0_0_0;
static const Il2CppType* GenInst_CoroutineClass_ProcessResponse_m8201378_gp_0_0_0_0_Types[] = { &CoroutineClass_ProcessResponse_m8201378_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CoroutineClass_ProcessResponse_m8201378_gp_0_0_0_0 = { 1, GenInst_CoroutineClass_ProcessResponse_m8201378_gp_0_0_0_0_Types };
extern const Il2CppType CoroutineClass_SendRequestSub_m176625721_gp_0_0_0_0;
static const Il2CppType* GenInst_CoroutineClass_SendRequestSub_m176625721_gp_0_0_0_0_Types[] = { &CoroutineClass_SendRequestSub_m176625721_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CoroutineClass_SendRequestSub_m176625721_gp_0_0_0_0 = { 1, GenInst_CoroutineClass_SendRequestSub_m176625721_gp_0_0_0_0_Types };
extern const Il2CppType CoroutineClass_SendRequestNonSub_m602791090_gp_0_0_0_0;
static const Il2CppType* GenInst_CoroutineClass_SendRequestNonSub_m602791090_gp_0_0_0_0_Types[] = { &CoroutineClass_SendRequestNonSub_m602791090_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CoroutineClass_SendRequestNonSub_m602791090_gp_0_0_0_0 = { 1, GenInst_CoroutineClass_SendRequestNonSub_m602791090_gp_0_0_0_0_Types };
extern const Il2CppType CoroutineClass_SendRequestPresenceHeartbeat_m3831291074_gp_0_0_0_0;
static const Il2CppType* GenInst_CoroutineClass_SendRequestPresenceHeartbeat_m3831291074_gp_0_0_0_0_Types[] = { &CoroutineClass_SendRequestPresenceHeartbeat_m3831291074_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CoroutineClass_SendRequestPresenceHeartbeat_m3831291074_gp_0_0_0_0 = { 1, GenInst_CoroutineClass_SendRequestPresenceHeartbeat_m3831291074_gp_0_0_0_0_Types };
extern const Il2CppType CoroutineClass_SendRequestHeartbeat_m3055755353_gp_0_0_0_0;
static const Il2CppType* GenInst_CoroutineClass_SendRequestHeartbeat_m3055755353_gp_0_0_0_0_Types[] = { &CoroutineClass_SendRequestHeartbeat_m3055755353_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CoroutineClass_SendRequestHeartbeat_m3055755353_gp_0_0_0_0 = { 1, GenInst_CoroutineClass_SendRequestHeartbeat_m3055755353_gp_0_0_0_0_Types };
extern const Il2CppType CoroutineClass_BounceRequest_m2366396667_gp_0_0_0_0;
static const Il2CppType* GenInst_CoroutineClass_BounceRequest_m2366396667_gp_0_0_0_0_Types[] = { &CoroutineClass_BounceRequest_m2366396667_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CoroutineClass_BounceRequest_m2366396667_gp_0_0_0_0 = { 1, GenInst_CoroutineClass_BounceRequest_m2366396667_gp_0_0_0_0_Types };
extern const Il2CppType CoroutineClass_ProcessTimeout_m4043344492_gp_0_0_0_0;
static const Il2CppType* GenInst_CoroutineClass_ProcessTimeout_m4043344492_gp_0_0_0_0_Types[] = { &CoroutineClass_ProcessTimeout_m4043344492_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CoroutineClass_ProcessTimeout_m4043344492_gp_0_0_0_0 = { 1, GenInst_CoroutineClass_ProcessTimeout_m4043344492_gp_0_0_0_0_Types };
extern const Il2CppType CoroutineClass_CheckTimeoutSub_m386401961_gp_0_0_0_0;
static const Il2CppType* GenInst_CoroutineClass_CheckTimeoutSub_m386401961_gp_0_0_0_0_Types[] = { &CoroutineClass_CheckTimeoutSub_m386401961_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CoroutineClass_CheckTimeoutSub_m386401961_gp_0_0_0_0 = { 1, GenInst_CoroutineClass_CheckTimeoutSub_m386401961_gp_0_0_0_0_Types };
extern const Il2CppType CoroutineClass_CheckTimeoutNonSub_m1320448384_gp_0_0_0_0;
static const Il2CppType* GenInst_CoroutineClass_CheckTimeoutNonSub_m1320448384_gp_0_0_0_0_Types[] = { &CoroutineClass_CheckTimeoutNonSub_m1320448384_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CoroutineClass_CheckTimeoutNonSub_m1320448384_gp_0_0_0_0 = { 1, GenInst_CoroutineClass_CheckTimeoutNonSub_m1320448384_gp_0_0_0_0_Types };
extern const Il2CppType CoroutineClass_CheckTimeoutPresenceHeartbeat_m3595108732_gp_0_0_0_0;
static const Il2CppType* GenInst_CoroutineClass_CheckTimeoutPresenceHeartbeat_m3595108732_gp_0_0_0_0_Types[] = { &CoroutineClass_CheckTimeoutPresenceHeartbeat_m3595108732_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CoroutineClass_CheckTimeoutPresenceHeartbeat_m3595108732_gp_0_0_0_0 = { 1, GenInst_CoroutineClass_CheckTimeoutPresenceHeartbeat_m3595108732_gp_0_0_0_0_Types };
extern const Il2CppType CoroutineClass_CheckTimeoutHeartbeat_m3060509961_gp_0_0_0_0;
static const Il2CppType* GenInst_CoroutineClass_CheckTimeoutHeartbeat_m3060509961_gp_0_0_0_0_Types[] = { &CoroutineClass_CheckTimeoutHeartbeat_m3060509961_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CoroutineClass_CheckTimeoutHeartbeat_m3060509961_gp_0_0_0_0 = { 1, GenInst_CoroutineClass_CheckTimeoutHeartbeat_m3060509961_gp_0_0_0_0_Types };
extern const Il2CppType CoroutineClass_FireEvent_m2778758263_gp_0_0_0_0;
static const Il2CppType* GenInst_CoroutineClass_FireEvent_m2778758263_gp_0_0_0_0_Types[] = { &CoroutineClass_FireEvent_m2778758263_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CoroutineClass_FireEvent_m2778758263_gp_0_0_0_0 = { 1, GenInst_CoroutineClass_FireEvent_m2778758263_gp_0_0_0_0_Types };
extern const Il2CppType U3CDelayRequestU3Ec__Iterator0_1_t1472438566_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CDelayRequestU3Ec__Iterator0_1_t1472438566_gp_0_0_0_0_Types[] = { &U3CDelayRequestU3Ec__Iterator0_1_t1472438566_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CDelayRequestU3Ec__Iterator0_1_t1472438566_gp_0_0_0_0 = { 1, GenInst_U3CDelayRequestU3Ec__Iterator0_1_t1472438566_gp_0_0_0_0_Types };
extern const Il2CppType U3CSendRequestSubU3Ec__Iterator1_1_t3370355612_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CSendRequestSubU3Ec__Iterator1_1_t3370355612_gp_0_0_0_0_Types[] = { &U3CSendRequestSubU3Ec__Iterator1_1_t3370355612_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CSendRequestSubU3Ec__Iterator1_1_t3370355612_gp_0_0_0_0 = { 1, GenInst_U3CSendRequestSubU3Ec__Iterator1_1_t3370355612_gp_0_0_0_0_Types };
extern const Il2CppType U3CSendRequestNonSubU3Ec__Iterator2_1_t909973264_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CSendRequestNonSubU3Ec__Iterator2_1_t909973264_gp_0_0_0_0_Types[] = { &U3CSendRequestNonSubU3Ec__Iterator2_1_t909973264_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CSendRequestNonSubU3Ec__Iterator2_1_t909973264_gp_0_0_0_0 = { 1, GenInst_U3CSendRequestNonSubU3Ec__Iterator2_1_t909973264_gp_0_0_0_0_Types };
extern const Il2CppType U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1863681071_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1863681071_gp_0_0_0_0_Types[] = { &U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1863681071_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1863681071_gp_0_0_0_0 = { 1, GenInst_U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1863681071_gp_0_0_0_0_Types };
extern const Il2CppType U3CSendRequestHeartbeatU3Ec__Iterator4_1_t3389641949_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CSendRequestHeartbeatU3Ec__Iterator4_1_t3389641949_gp_0_0_0_0_Types[] = { &U3CSendRequestHeartbeatU3Ec__Iterator4_1_t3389641949_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CSendRequestHeartbeatU3Ec__Iterator4_1_t3389641949_gp_0_0_0_0 = { 1, GenInst_U3CSendRequestHeartbeatU3Ec__Iterator4_1_t3389641949_gp_0_0_0_0_Types };
extern const Il2CppType U3CCheckTimeoutSubU3Ec__Iterator5_1_t1773310952_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCheckTimeoutSubU3Ec__Iterator5_1_t1773310952_gp_0_0_0_0_Types[] = { &U3CCheckTimeoutSubU3Ec__Iterator5_1_t1773310952_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCheckTimeoutSubU3Ec__Iterator5_1_t1773310952_gp_0_0_0_0 = { 1, GenInst_U3CCheckTimeoutSubU3Ec__Iterator5_1_t1773310952_gp_0_0_0_0_Types };
extern const Il2CppType U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t367276060_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t367276060_gp_0_0_0_0_Types[] = { &U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t367276060_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t367276060_gp_0_0_0_0 = { 1, GenInst_U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t367276060_gp_0_0_0_0_Types };
extern const Il2CppType U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t981046395_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t981046395_gp_0_0_0_0_Types[] = { &U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t981046395_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t981046395_gp_0_0_0_0 = { 1, GenInst_U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t981046395_gp_0_0_0_0_Types };
extern const Il2CppType U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t886287453_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t886287453_gp_0_0_0_0_Types[] = { &U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t886287453_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t886287453_gp_0_0_0_0 = { 1, GenInst_U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t886287453_gp_0_0_0_0_Types };
extern const Il2CppType ExceptionHandlers_CreateErrorCodeAndCallErrorCallback_m362467824_gp_0_0_0_0;
static const Il2CppType* GenInst_ExceptionHandlers_CreateErrorCodeAndCallErrorCallback_m362467824_gp_0_0_0_0_Types[] = { &ExceptionHandlers_CreateErrorCodeAndCallErrorCallback_m362467824_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExceptionHandlers_CreateErrorCodeAndCallErrorCallback_m362467824_gp_0_0_0_0 = { 1, GenInst_ExceptionHandlers_CreateErrorCodeAndCallErrorCallback_m362467824_gp_0_0_0_0_Types };
extern const Il2CppType ExceptionHandlers_ResponseCallbackErrorOrTimeoutHandler_m1440915304_gp_0_0_0_0;
static const Il2CppType* GenInst_ExceptionHandlers_ResponseCallbackErrorOrTimeoutHandler_m1440915304_gp_0_0_0_0_Types[] = { &ExceptionHandlers_ResponseCallbackErrorOrTimeoutHandler_m1440915304_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExceptionHandlers_ResponseCallbackErrorOrTimeoutHandler_m1440915304_gp_0_0_0_0 = { 1, GenInst_ExceptionHandlers_ResponseCallbackErrorOrTimeoutHandler_m1440915304_gp_0_0_0_0_Types };
extern const Il2CppType ExceptionHandlers_ResponseCallbackWebExceptionHandler_m2921193952_gp_0_0_0_0;
static const Il2CppType* GenInst_ExceptionHandlers_ResponseCallbackWebExceptionHandler_m2921193952_gp_0_0_0_0_Types[] = { &ExceptionHandlers_ResponseCallbackWebExceptionHandler_m2921193952_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExceptionHandlers_ResponseCallbackWebExceptionHandler_m2921193952_gp_0_0_0_0 = { 1, GenInst_ExceptionHandlers_ResponseCallbackWebExceptionHandler_m2921193952_gp_0_0_0_0_Types };
extern const Il2CppType ExceptionHandlers_ResponseCallbackExceptionHandler_m2979300171_gp_0_0_0_0;
static const Il2CppType* GenInst_ExceptionHandlers_ResponseCallbackExceptionHandler_m2979300171_gp_0_0_0_0_Types[] = { &ExceptionHandlers_ResponseCallbackExceptionHandler_m2979300171_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExceptionHandlers_ResponseCallbackExceptionHandler_m2979300171_gp_0_0_0_0 = { 1, GenInst_ExceptionHandlers_ResponseCallbackExceptionHandler_m2979300171_gp_0_0_0_0_Types };
extern const Il2CppType ExceptionHandlers_ProcessResponseCallbackExceptionHandler_m3077749814_gp_0_0_0_0;
static const Il2CppType* GenInst_ExceptionHandlers_ProcessResponseCallbackExceptionHandler_m3077749814_gp_0_0_0_0_Types[] = { &ExceptionHandlers_ProcessResponseCallbackExceptionHandler_m3077749814_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExceptionHandlers_ProcessResponseCallbackExceptionHandler_m3077749814_gp_0_0_0_0 = { 1, GenInst_ExceptionHandlers_ProcessResponseCallbackExceptionHandler_m3077749814_gp_0_0_0_0_Types };
extern const Il2CppType ExceptionHandlers_ProcessResponseCallbackWebExceptionHandler_m3558871925_gp_0_0_0_0;
static const Il2CppType* GenInst_ExceptionHandlers_ProcessResponseCallbackWebExceptionHandler_m3558871925_gp_0_0_0_0_Types[] = { &ExceptionHandlers_ProcessResponseCallbackWebExceptionHandler_m3558871925_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExceptionHandlers_ProcessResponseCallbackWebExceptionHandler_m3558871925_gp_0_0_0_0 = { 1, GenInst_ExceptionHandlers_ProcessResponseCallbackWebExceptionHandler_m3558871925_gp_0_0_0_0_Types };
extern const Il2CppType ExceptionHandlers_FireMultiplexException_m3941725088_gp_0_0_0_0;
static const Il2CppType* GenInst_ExceptionHandlers_FireMultiplexException_m3941725088_gp_0_0_0_0_Types[] = { &ExceptionHandlers_FireMultiplexException_m3941725088_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExceptionHandlers_FireMultiplexException_m3941725088_gp_0_0_0_0 = { 1, GenInst_ExceptionHandlers_FireMultiplexException_m3941725088_gp_0_0_0_0_Types };
extern const Il2CppType ExceptionHandlers_UrlRequestCommonExceptionHandler_m4124436988_gp_0_0_0_0;
static const Il2CppType* GenInst_ExceptionHandlers_UrlRequestCommonExceptionHandler_m4124436988_gp_0_0_0_0_Types[] = { &ExceptionHandlers_UrlRequestCommonExceptionHandler_m4124436988_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExceptionHandlers_UrlRequestCommonExceptionHandler_m4124436988_gp_0_0_0_0 = { 1, GenInst_ExceptionHandlers_UrlRequestCommonExceptionHandler_m4124436988_gp_0_0_0_0_Types };
extern const Il2CppType ExceptionHandlers_PushNotificationExceptionHandler_m133924597_gp_0_0_0_0;
static const Il2CppType* GenInst_ExceptionHandlers_PushNotificationExceptionHandler_m133924597_gp_0_0_0_0_Types[] = { &ExceptionHandlers_PushNotificationExceptionHandler_m133924597_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExceptionHandlers_PushNotificationExceptionHandler_m133924597_gp_0_0_0_0 = { 1, GenInst_ExceptionHandlers_PushNotificationExceptionHandler_m133924597_gp_0_0_0_0_Types };
extern const Il2CppType ExceptionHandlers_ChannelGroupExceptionHandler_m568856718_gp_0_0_0_0;
static const Il2CppType* GenInst_ExceptionHandlers_ChannelGroupExceptionHandler_m568856718_gp_0_0_0_0_Types[] = { &ExceptionHandlers_ChannelGroupExceptionHandler_m568856718_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExceptionHandlers_ChannelGroupExceptionHandler_m568856718_gp_0_0_0_0 = { 1, GenInst_ExceptionHandlers_ChannelGroupExceptionHandler_m568856718_gp_0_0_0_0_Types };
extern const Il2CppType ExceptionHandlers_CommonExceptionHandler_m537430337_gp_0_0_0_0;
static const Il2CppType* GenInst_ExceptionHandlers_CommonExceptionHandler_m537430337_gp_0_0_0_0_Types[] = { &ExceptionHandlers_CommonExceptionHandler_m537430337_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExceptionHandlers_CommonExceptionHandler_m537430337_gp_0_0_0_0 = { 1, GenInst_ExceptionHandlers_CommonExceptionHandler_m537430337_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_UpdateOrAddUserStateOfEntity_m3328797157_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_UpdateOrAddUserStateOfEntity_m3328797157_gp_0_0_0_0_Types[] = { &Helpers_UpdateOrAddUserStateOfEntity_m3328797157_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_UpdateOrAddUserStateOfEntity_m3328797157_gp_0_0_0_0 = { 1, GenInst_Helpers_UpdateOrAddUserStateOfEntity_m3328797157_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_CheckAndAddExistingUserState_m1851326739_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_CheckAndAddExistingUserState_m1851326739_gp_0_0_0_0_Types[] = { &Helpers_CheckAndAddExistingUserState_m1851326739_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_CheckAndAddExistingUserState_m1851326739_gp_0_0_0_0 = { 1, GenInst_Helpers_CheckAndAddExistingUserState_m1851326739_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_CreateChannelEntity_m1336794432_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_CreateChannelEntity_m1336794432_gp_0_0_0_0_Types[] = { &Helpers_CreateChannelEntity_m1336794432_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_CreateChannelEntity_m1336794432_gp_0_0_0_0 = { 1, GenInst_Helpers_CreateChannelEntity_m1336794432_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_CreateChannelEntity_m140253032_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_CreateChannelEntity_m140253032_gp_0_0_0_0_Types[] = { &Helpers_CreateChannelEntity_m140253032_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_CreateChannelEntity_m140253032_gp_0_0_0_0 = { 1, GenInst_Helpers_CreateChannelEntity_m140253032_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_CreateChannelEntityAndAddToSubscribe_m1556276701_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_CreateChannelEntityAndAddToSubscribe_m1556276701_gp_0_0_0_0_Types[] = { &Helpers_CreateChannelEntityAndAddToSubscribe_m1556276701_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_CreateChannelEntityAndAddToSubscribe_m1556276701_gp_0_0_0_0 = { 1, GenInst_Helpers_CreateChannelEntityAndAddToSubscribe_m1556276701_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_RemoveDuplicatesCheckAlreadySubscribedAndGetChannelsCommon_m2858836678_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_RemoveDuplicatesCheckAlreadySubscribedAndGetChannelsCommon_m2858836678_gp_0_0_0_0_Types[] = { &Helpers_RemoveDuplicatesCheckAlreadySubscribedAndGetChannelsCommon_m2858836678_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_RemoveDuplicatesCheckAlreadySubscribedAndGetChannelsCommon_m2858836678_gp_0_0_0_0 = { 1, GenInst_Helpers_RemoveDuplicatesCheckAlreadySubscribedAndGetChannelsCommon_m2858836678_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_RemoveDuplicatesCheckAlreadySubscribedAndGetChannels_m837243240_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_RemoveDuplicatesCheckAlreadySubscribedAndGetChannels_m837243240_gp_0_0_0_0_Types[] = { &Helpers_RemoveDuplicatesCheckAlreadySubscribedAndGetChannels_m837243240_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_RemoveDuplicatesCheckAlreadySubscribedAndGetChannels_m837243240_gp_0_0_0_0 = { 1, GenInst_Helpers_RemoveDuplicatesCheckAlreadySubscribedAndGetChannels_m837243240_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_ProcessResponseCallbacksV2_m907462966_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_ProcessResponseCallbacksV2_m907462966_gp_0_0_0_0_Types[] = { &Helpers_ProcessResponseCallbacksV2_m907462966_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_ProcessResponseCallbacksV2_m907462966_gp_0_0_0_0 = { 1, GenInst_Helpers_ProcessResponseCallbacksV2_m907462966_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_ProcessResponseCallbacks_m100484875_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_ProcessResponseCallbacks_m100484875_gp_0_0_0_0_Types[] = { &Helpers_ProcessResponseCallbacks_m100484875_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_ProcessResponseCallbacks_m100484875_gp_0_0_0_0 = { 1, GenInst_Helpers_ProcessResponseCallbacks_m100484875_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_CheckSubscribedChannelsAndSendCallbacks_m434503918_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_CheckSubscribedChannelsAndSendCallbacks_m434503918_gp_0_0_0_0_Types[] = { &Helpers_CheckSubscribedChannelsAndSendCallbacks_m434503918_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_CheckSubscribedChannelsAndSendCallbacks_m434503918_gp_0_0_0_0 = { 1, GenInst_Helpers_CheckSubscribedChannelsAndSendCallbacks_m434503918_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_WrapResultBasedOnResponseType_m2395420773_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_WrapResultBasedOnResponseType_m2395420773_gp_0_0_0_0_Types[] = { &Helpers_WrapResultBasedOnResponseType_m2395420773_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_WrapResultBasedOnResponseType_m2395420773_gp_0_0_0_0 = { 1, GenInst_Helpers_WrapResultBasedOnResponseType_m2395420773_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_ProcessWrapResultBasedOnResponseTypeException_m2995813353_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_ProcessWrapResultBasedOnResponseTypeException_m2995813353_gp_0_0_0_0_Types[] = { &Helpers_ProcessWrapResultBasedOnResponseTypeException_m2995813353_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_ProcessWrapResultBasedOnResponseTypeException_m2995813353_gp_0_0_0_0 = { 1, GenInst_Helpers_ProcessWrapResultBasedOnResponseTypeException_m2995813353_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_FindChannelEntityAndCallback_m4007496225_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_FindChannelEntityAndCallback_m4007496225_gp_0_0_0_0_Types[] = { &Helpers_FindChannelEntityAndCallback_m4007496225_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_FindChannelEntityAndCallback_m4007496225_gp_0_0_0_0 = { 1, GenInst_Helpers_FindChannelEntityAndCallback_m4007496225_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_ResponseToUserCallbackForSubscribeV2_m321604969_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_ResponseToUserCallbackForSubscribeV2_m321604969_gp_0_0_0_0_Types[] = { &Helpers_ResponseToUserCallbackForSubscribeV2_m321604969_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_ResponseToUserCallbackForSubscribeV2_m321604969_gp_0_0_0_0 = { 1, GenInst_Helpers_ResponseToUserCallbackForSubscribeV2_m321604969_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_ResponseToUserCallback_m1132470759_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_ResponseToUserCallback_m1132470759_gp_0_0_0_0_Types[] = { &Helpers_ResponseToUserCallback_m1132470759_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_ResponseToUserCallback_m1132470759_gp_0_0_0_0 = { 1, GenInst_Helpers_ResponseToUserCallback_m1132470759_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_ResponseToConnectCallback_m3049791096_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_ResponseToConnectCallback_m3049791096_gp_0_0_0_0_Types[] = { &Helpers_ResponseToConnectCallback_m3049791096_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_ResponseToConnectCallback_m3049791096_gp_0_0_0_0 = { 1, GenInst_Helpers_ResponseToConnectCallback_m3049791096_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_CreatePubnubClientError_m1606993622_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_CreatePubnubClientError_m1606993622_gp_0_0_0_0_Types[] = { &Helpers_CreatePubnubClientError_m1606993622_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_CreatePubnubClientError_m1606993622_gp_0_0_0_0 = { 1, GenInst_Helpers_CreatePubnubClientError_m1606993622_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_CreatePubnubClientError_m3026826283_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_CreatePubnubClientError_m3026826283_gp_0_0_0_0_Types[] = { &Helpers_CreatePubnubClientError_m3026826283_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_CreatePubnubClientError_m3026826283_gp_0_0_0_0 = { 1, GenInst_Helpers_CreatePubnubClientError_m3026826283_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_CreatePubnubClientError_m3384800101_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_CreatePubnubClientError_m3384800101_gp_0_0_0_0_Types[] = { &Helpers_CreatePubnubClientError_m3384800101_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_CreatePubnubClientError_m3384800101_gp_0_0_0_0 = { 1, GenInst_Helpers_CreatePubnubClientError_m3384800101_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_CreatePubnubClientError_m2221030080_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_CreatePubnubClientError_m2221030080_gp_0_0_0_0_Types[] = { &Helpers_CreatePubnubClientError_m2221030080_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_CreatePubnubClientError_m2221030080_gp_0_0_0_0 = { 1, GenInst_Helpers_CreatePubnubClientError_m2221030080_gp_0_0_0_0_Types };
extern const Il2CppType Helpers_CreatePubnubClientError_m2783450996_gp_0_0_0_0;
static const Il2CppType* GenInst_Helpers_CreatePubnubClientError_m2783450996_gp_0_0_0_0_Types[] = { &Helpers_CreatePubnubClientError_m2783450996_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Helpers_CreatePubnubClientError_m2783450996_gp_0_0_0_0 = { 1, GenInst_Helpers_CreatePubnubClientError_m2783450996_gp_0_0_0_0_Types };
extern const Il2CppType JsonFxUnitySerializer_Deserialize_m1336013203_gp_0_0_0_0;
static const Il2CppType* GenInst_JsonFxUnitySerializer_Deserialize_m1336013203_gp_0_0_0_0_Types[] = { &JsonFxUnitySerializer_Deserialize_m1336013203_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_JsonFxUnitySerializer_Deserialize_m1336013203_gp_0_0_0_0 = { 1, GenInst_JsonFxUnitySerializer_Deserialize_m1336013203_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_Subscribe_m4173062036_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_Subscribe_m4173062036_gp_0_0_0_0_Types[] = { &Pubnub_Subscribe_m4173062036_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_Subscribe_m4173062036_gp_0_0_0_0 = { 1, GenInst_Pubnub_Subscribe_m4173062036_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_Subscribe_m1063884376_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_Subscribe_m1063884376_gp_0_0_0_0_Types[] = { &Pubnub_Subscribe_m1063884376_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_Subscribe_m1063884376_gp_0_0_0_0 = { 1, GenInst_Pubnub_Subscribe_m1063884376_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_Subscribe_m938123644_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_Subscribe_m938123644_gp_0_0_0_0_Types[] = { &Pubnub_Subscribe_m938123644_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_Subscribe_m938123644_gp_0_0_0_0 = { 1, GenInst_Pubnub_Subscribe_m938123644_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_Subscribe_m2991922720_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_Subscribe_m2991922720_gp_0_0_0_0_Types[] = { &Pubnub_Subscribe_m2991922720_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_Subscribe_m2991922720_gp_0_0_0_0 = { 1, GenInst_Pubnub_Subscribe_m2991922720_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_Subscribe_m2176482236_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_Subscribe_m2176482236_gp_0_0_0_0_Types[] = { &Pubnub_Subscribe_m2176482236_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_Subscribe_m2176482236_gp_0_0_0_0 = { 1, GenInst_Pubnub_Subscribe_m2176482236_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_Publish_m3308409727_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_Publish_m3308409727_gp_0_0_0_0_Types[] = { &Pubnub_Publish_m3308409727_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_Publish_m3308409727_gp_0_0_0_0 = { 1, GenInst_Pubnub_Publish_m3308409727_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_Publish_m1125539792_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_Publish_m1125539792_gp_0_0_0_0_Types[] = { &Pubnub_Publish_m1125539792_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_Publish_m1125539792_gp_0_0_0_0 = { 1, GenInst_Pubnub_Publish_m1125539792_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_Publish_m1659297476_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_Publish_m1659297476_gp_0_0_0_0_Types[] = { &Pubnub_Publish_m1659297476_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_Publish_m1659297476_gp_0_0_0_0 = { 1, GenInst_Pubnub_Publish_m1659297476_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_Publish_m695990871_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_Publish_m695990871_gp_0_0_0_0_Types[] = { &Pubnub_Publish_m695990871_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_Publish_m695990871_gp_0_0_0_0 = { 1, GenInst_Pubnub_Publish_m695990871_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_Publish_m1535867746_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_Publish_m1535867746_gp_0_0_0_0_Types[] = { &Pubnub_Publish_m1535867746_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_Publish_m1535867746_gp_0_0_0_0 = { 1, GenInst_Pubnub_Publish_m1535867746_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_Presence_m1987549737_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_Presence_m1987549737_gp_0_0_0_0_Types[] = { &Pubnub_Presence_m1987549737_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_Presence_m1987549737_gp_0_0_0_0 = { 1, GenInst_Pubnub_Presence_m1987549737_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_Presence_m4203253203_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_Presence_m4203253203_gp_0_0_0_0_Types[] = { &Pubnub_Presence_m4203253203_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_Presence_m4203253203_gp_0_0_0_0 = { 1, GenInst_Pubnub_Presence_m4203253203_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_Presence_m3139762387_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_Presence_m3139762387_gp_0_0_0_0_Types[] = { &Pubnub_Presence_m3139762387_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_Presence_m3139762387_gp_0_0_0_0 = { 1, GenInst_Pubnub_Presence_m3139762387_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_Presence_m2275364137_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_Presence_m2275364137_gp_0_0_0_0_Types[] = { &Pubnub_Presence_m2275364137_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_Presence_m2275364137_gp_0_0_0_0 = { 1, GenInst_Pubnub_Presence_m2275364137_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_DetailedHistory_m2786197292_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_DetailedHistory_m2786197292_gp_0_0_0_0_Types[] = { &Pubnub_DetailedHistory_m2786197292_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_DetailedHistory_m2786197292_gp_0_0_0_0 = { 1, GenInst_Pubnub_DetailedHistory_m2786197292_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_DetailedHistory_m1609992699_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_DetailedHistory_m1609992699_gp_0_0_0_0_Types[] = { &Pubnub_DetailedHistory_m1609992699_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_DetailedHistory_m1609992699_gp_0_0_0_0 = { 1, GenInst_Pubnub_DetailedHistory_m1609992699_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_DetailedHistory_m932243879_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_DetailedHistory_m932243879_gp_0_0_0_0_Types[] = { &Pubnub_DetailedHistory_m932243879_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_DetailedHistory_m932243879_gp_0_0_0_0 = { 1, GenInst_Pubnub_DetailedHistory_m932243879_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_DetailedHistory_m3061495195_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_DetailedHistory_m3061495195_gp_0_0_0_0_Types[] = { &Pubnub_DetailedHistory_m3061495195_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_DetailedHistory_m3061495195_gp_0_0_0_0 = { 1, GenInst_Pubnub_DetailedHistory_m3061495195_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_DetailedHistory_m4040590188_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_DetailedHistory_m4040590188_gp_0_0_0_0_Types[] = { &Pubnub_DetailedHistory_m4040590188_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_DetailedHistory_m4040590188_gp_0_0_0_0 = { 1, GenInst_Pubnub_DetailedHistory_m4040590188_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_DetailedHistory_m1522726056_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_DetailedHistory_m1522726056_gp_0_0_0_0_Types[] = { &Pubnub_DetailedHistory_m1522726056_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_DetailedHistory_m1522726056_gp_0_0_0_0 = { 1, GenInst_Pubnub_DetailedHistory_m1522726056_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_HereNow_m3852130836_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_HereNow_m3852130836_gp_0_0_0_0_Types[] = { &Pubnub_HereNow_m3852130836_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_HereNow_m3852130836_gp_0_0_0_0 = { 1, GenInst_Pubnub_HereNow_m3852130836_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_HereNow_m3312200294_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_HereNow_m3312200294_gp_0_0_0_0_Types[] = { &Pubnub_HereNow_m3312200294_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_HereNow_m3312200294_gp_0_0_0_0 = { 1, GenInst_Pubnub_HereNow_m3312200294_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_HereNow_m3915400198_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_HereNow_m3915400198_gp_0_0_0_0_Types[] = { &Pubnub_HereNow_m3915400198_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_HereNow_m3915400198_gp_0_0_0_0 = { 1, GenInst_Pubnub_HereNow_m3915400198_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_GlobalHereNow_m2431920201_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_GlobalHereNow_m2431920201_gp_0_0_0_0_Types[] = { &Pubnub_GlobalHereNow_m2431920201_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_GlobalHereNow_m2431920201_gp_0_0_0_0 = { 1, GenInst_Pubnub_GlobalHereNow_m2431920201_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_GlobalHereNow_m4166942753_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_GlobalHereNow_m4166942753_gp_0_0_0_0_Types[] = { &Pubnub_GlobalHereNow_m4166942753_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_GlobalHereNow_m4166942753_gp_0_0_0_0 = { 1, GenInst_Pubnub_GlobalHereNow_m4166942753_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_WhereNow_m667813399_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_WhereNow_m667813399_gp_0_0_0_0_Types[] = { &Pubnub_WhereNow_m667813399_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_WhereNow_m667813399_gp_0_0_0_0 = { 1, GenInst_Pubnub_WhereNow_m667813399_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_Unsubscribe_m742992275_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_Unsubscribe_m742992275_gp_0_0_0_0_Types[] = { &Pubnub_Unsubscribe_m742992275_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_Unsubscribe_m742992275_gp_0_0_0_0 = { 1, GenInst_Pubnub_Unsubscribe_m742992275_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_Unsubscribe_m2334922711_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_Unsubscribe_m2334922711_gp_0_0_0_0_Types[] = { &Pubnub_Unsubscribe_m2334922711_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_Unsubscribe_m2334922711_gp_0_0_0_0 = { 1, GenInst_Pubnub_Unsubscribe_m2334922711_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_Unsubscribe_m1059268865_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_Unsubscribe_m1059268865_gp_0_0_0_0_Types[] = { &Pubnub_Unsubscribe_m1059268865_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_Unsubscribe_m1059268865_gp_0_0_0_0 = { 1, GenInst_Pubnub_Unsubscribe_m1059268865_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_PresenceUnsubscribe_m841331202_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_PresenceUnsubscribe_m841331202_gp_0_0_0_0_Types[] = { &Pubnub_PresenceUnsubscribe_m841331202_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_PresenceUnsubscribe_m841331202_gp_0_0_0_0 = { 1, GenInst_Pubnub_PresenceUnsubscribe_m841331202_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_PresenceUnsubscribe_m2247745402_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_PresenceUnsubscribe_m2247745402_gp_0_0_0_0_Types[] = { &Pubnub_PresenceUnsubscribe_m2247745402_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_PresenceUnsubscribe_m2247745402_gp_0_0_0_0 = { 1, GenInst_Pubnub_PresenceUnsubscribe_m2247745402_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_Time_m4208398435_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_Time_m4208398435_gp_0_0_0_0_Types[] = { &Pubnub_Time_m4208398435_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_Time_m4208398435_gp_0_0_0_0 = { 1, GenInst_Pubnub_Time_m4208398435_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_AuditAccess_m1919842191_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_AuditAccess_m1919842191_gp_0_0_0_0_Types[] = { &Pubnub_AuditAccess_m1919842191_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_AuditAccess_m1919842191_gp_0_0_0_0 = { 1, GenInst_Pubnub_AuditAccess_m1919842191_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_AuditAccess_m255406457_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_AuditAccess_m255406457_gp_0_0_0_0_Types[] = { &Pubnub_AuditAccess_m255406457_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_AuditAccess_m255406457_gp_0_0_0_0 = { 1, GenInst_Pubnub_AuditAccess_m255406457_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_AuditAccess_m2182157455_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_AuditAccess_m2182157455_gp_0_0_0_0_Types[] = { &Pubnub_AuditAccess_m2182157455_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_AuditAccess_m2182157455_gp_0_0_0_0 = { 1, GenInst_Pubnub_AuditAccess_m2182157455_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_ChannelGroupAuditAccess_m3730454487_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_ChannelGroupAuditAccess_m3730454487_gp_0_0_0_0_Types[] = { &Pubnub_ChannelGroupAuditAccess_m3730454487_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_ChannelGroupAuditAccess_m3730454487_gp_0_0_0_0 = { 1, GenInst_Pubnub_ChannelGroupAuditAccess_m3730454487_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_ChannelGroupAuditAccess_m1789403085_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_ChannelGroupAuditAccess_m1789403085_gp_0_0_0_0_Types[] = { &Pubnub_ChannelGroupAuditAccess_m1789403085_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_ChannelGroupAuditAccess_m1789403085_gp_0_0_0_0 = { 1, GenInst_Pubnub_ChannelGroupAuditAccess_m1789403085_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_ChannelGroupAuditAccess_m1285632727_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_ChannelGroupAuditAccess_m1285632727_gp_0_0_0_0_Types[] = { &Pubnub_ChannelGroupAuditAccess_m1285632727_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_ChannelGroupAuditAccess_m1285632727_gp_0_0_0_0 = { 1, GenInst_Pubnub_ChannelGroupAuditAccess_m1285632727_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_ChannelGroupAuditPresenceAccess_m2375812858_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_ChannelGroupAuditPresenceAccess_m2375812858_gp_0_0_0_0_Types[] = { &Pubnub_ChannelGroupAuditPresenceAccess_m2375812858_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_ChannelGroupAuditPresenceAccess_m2375812858_gp_0_0_0_0 = { 1, GenInst_Pubnub_ChannelGroupAuditPresenceAccess_m2375812858_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_ChannelGroupAuditPresenceAccess_m568156134_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_ChannelGroupAuditPresenceAccess_m568156134_gp_0_0_0_0_Types[] = { &Pubnub_ChannelGroupAuditPresenceAccess_m568156134_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_ChannelGroupAuditPresenceAccess_m568156134_gp_0_0_0_0 = { 1, GenInst_Pubnub_ChannelGroupAuditPresenceAccess_m568156134_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_ChannelGroupGrantAccess_m1178498835_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_ChannelGroupGrantAccess_m1178498835_gp_0_0_0_0_Types[] = { &Pubnub_ChannelGroupGrantAccess_m1178498835_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_ChannelGroupGrantAccess_m1178498835_gp_0_0_0_0 = { 1, GenInst_Pubnub_ChannelGroupGrantAccess_m1178498835_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_ChannelGroupGrantAccess_m3460332902_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_ChannelGroupGrantAccess_m3460332902_gp_0_0_0_0_Types[] = { &Pubnub_ChannelGroupGrantAccess_m3460332902_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_ChannelGroupGrantAccess_m3460332902_gp_0_0_0_0 = { 1, GenInst_Pubnub_ChannelGroupGrantAccess_m3460332902_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_ChannelGroupGrantAccess_m1384537745_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_ChannelGroupGrantAccess_m1384537745_gp_0_0_0_0_Types[] = { &Pubnub_ChannelGroupGrantAccess_m1384537745_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_ChannelGroupGrantAccess_m1384537745_gp_0_0_0_0 = { 1, GenInst_Pubnub_ChannelGroupGrantAccess_m1384537745_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_ChannelGroupGrantAccess_m518994066_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_ChannelGroupGrantAccess_m518994066_gp_0_0_0_0_Types[] = { &Pubnub_ChannelGroupGrantAccess_m518994066_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_ChannelGroupGrantAccess_m518994066_gp_0_0_0_0 = { 1, GenInst_Pubnub_ChannelGroupGrantAccess_m518994066_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_ChannelGroupGrantPresenceAccess_m155900683_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_ChannelGroupGrantPresenceAccess_m155900683_gp_0_0_0_0_Types[] = { &Pubnub_ChannelGroupGrantPresenceAccess_m155900683_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_ChannelGroupGrantPresenceAccess_m155900683_gp_0_0_0_0 = { 1, GenInst_Pubnub_ChannelGroupGrantPresenceAccess_m155900683_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_ChannelGroupGrantPresenceAccess_m4121007672_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_ChannelGroupGrantPresenceAccess_m4121007672_gp_0_0_0_0_Types[] = { &Pubnub_ChannelGroupGrantPresenceAccess_m4121007672_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_ChannelGroupGrantPresenceAccess_m4121007672_gp_0_0_0_0 = { 1, GenInst_Pubnub_ChannelGroupGrantPresenceAccess_m4121007672_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_ChannelGroupGrantPresenceAccess_m2808607265_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_ChannelGroupGrantPresenceAccess_m2808607265_gp_0_0_0_0_Types[] = { &Pubnub_ChannelGroupGrantPresenceAccess_m2808607265_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_ChannelGroupGrantPresenceAccess_m2808607265_gp_0_0_0_0 = { 1, GenInst_Pubnub_ChannelGroupGrantPresenceAccess_m2808607265_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_ChannelGroupGrantPresenceAccess_m3359245880_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_ChannelGroupGrantPresenceAccess_m3359245880_gp_0_0_0_0_Types[] = { &Pubnub_ChannelGroupGrantPresenceAccess_m3359245880_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_ChannelGroupGrantPresenceAccess_m3359245880_gp_0_0_0_0 = { 1, GenInst_Pubnub_ChannelGroupGrantPresenceAccess_m3359245880_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_AuditPresenceAccess_m4091230300_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_AuditPresenceAccess_m4091230300_gp_0_0_0_0_Types[] = { &Pubnub_AuditPresenceAccess_m4091230300_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_AuditPresenceAccess_m4091230300_gp_0_0_0_0 = { 1, GenInst_Pubnub_AuditPresenceAccess_m4091230300_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_AuditPresenceAccess_m1880498832_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_AuditPresenceAccess_m1880498832_gp_0_0_0_0_Types[] = { &Pubnub_AuditPresenceAccess_m1880498832_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_AuditPresenceAccess_m1880498832_gp_0_0_0_0 = { 1, GenInst_Pubnub_AuditPresenceAccess_m1880498832_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_GrantAccess_m3752519459_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_GrantAccess_m3752519459_gp_0_0_0_0_Types[] = { &Pubnub_GrantAccess_m3752519459_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_GrantAccess_m3752519459_gp_0_0_0_0 = { 1, GenInst_Pubnub_GrantAccess_m3752519459_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_GrantAccess_m2877601888_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_GrantAccess_m2877601888_gp_0_0_0_0_Types[] = { &Pubnub_GrantAccess_m2877601888_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_GrantAccess_m2877601888_gp_0_0_0_0 = { 1, GenInst_Pubnub_GrantAccess_m2877601888_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_GrantAccess_m2509597765_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_GrantAccess_m2509597765_gp_0_0_0_0_Types[] = { &Pubnub_GrantAccess_m2509597765_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_GrantAccess_m2509597765_gp_0_0_0_0 = { 1, GenInst_Pubnub_GrantAccess_m2509597765_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_GrantAccess_m982193236_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_GrantAccess_m982193236_gp_0_0_0_0_Types[] = { &Pubnub_GrantAccess_m982193236_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_GrantAccess_m982193236_gp_0_0_0_0 = { 1, GenInst_Pubnub_GrantAccess_m982193236_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_GrantPresenceAccess_m2650027371_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_GrantPresenceAccess_m2650027371_gp_0_0_0_0_Types[] = { &Pubnub_GrantPresenceAccess_m2650027371_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_GrantPresenceAccess_m2650027371_gp_0_0_0_0 = { 1, GenInst_Pubnub_GrantPresenceAccess_m2650027371_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_GrantPresenceAccess_m300154446_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_GrantPresenceAccess_m300154446_gp_0_0_0_0_Types[] = { &Pubnub_GrantPresenceAccess_m300154446_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_GrantPresenceAccess_m300154446_gp_0_0_0_0 = { 1, GenInst_Pubnub_GrantPresenceAccess_m300154446_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_GrantPresenceAccess_m2743920757_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_GrantPresenceAccess_m2743920757_gp_0_0_0_0_Types[] = { &Pubnub_GrantPresenceAccess_m2743920757_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_GrantPresenceAccess_m2743920757_gp_0_0_0_0 = { 1, GenInst_Pubnub_GrantPresenceAccess_m2743920757_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_GrantPresenceAccess_m1724514030_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_GrantPresenceAccess_m1724514030_gp_0_0_0_0_Types[] = { &Pubnub_GrantPresenceAccess_m1724514030_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_GrantPresenceAccess_m1724514030_gp_0_0_0_0 = { 1, GenInst_Pubnub_GrantPresenceAccess_m1724514030_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_SetUserState_m4120570694_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_SetUserState_m4120570694_gp_0_0_0_0_Types[] = { &Pubnub_SetUserState_m4120570694_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_SetUserState_m4120570694_gp_0_0_0_0 = { 1, GenInst_Pubnub_SetUserState_m4120570694_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_SetUserState_m1721356594_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_SetUserState_m1721356594_gp_0_0_0_0_Types[] = { &Pubnub_SetUserState_m1721356594_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_SetUserState_m1721356594_gp_0_0_0_0 = { 1, GenInst_Pubnub_SetUserState_m1721356594_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_SetUserState_m198699590_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_SetUserState_m198699590_gp_0_0_0_0_Types[] = { &Pubnub_SetUserState_m198699590_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_SetUserState_m198699590_gp_0_0_0_0 = { 1, GenInst_Pubnub_SetUserState_m198699590_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_SetUserState_m2667328267_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_SetUserState_m2667328267_gp_0_0_0_0_Types[] = { &Pubnub_SetUserState_m2667328267_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_SetUserState_m2667328267_gp_0_0_0_0 = { 1, GenInst_Pubnub_SetUserState_m2667328267_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_SetUserState_m3214533505_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_SetUserState_m3214533505_gp_0_0_0_0_Types[] = { &Pubnub_SetUserState_m3214533505_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_SetUserState_m3214533505_gp_0_0_0_0 = { 1, GenInst_Pubnub_SetUserState_m3214533505_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_SetUserState_m2796272193_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_SetUserState_m2796272193_gp_0_0_0_0_Types[] = { &Pubnub_SetUserState_m2796272193_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_SetUserState_m2796272193_gp_0_0_0_0 = { 1, GenInst_Pubnub_SetUserState_m2796272193_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_GetUserState_m1950946474_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_GetUserState_m1950946474_gp_0_0_0_0_Types[] = { &Pubnub_GetUserState_m1950946474_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_GetUserState_m1950946474_gp_0_0_0_0 = { 1, GenInst_Pubnub_GetUserState_m1950946474_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_GetUserState_m496349206_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_GetUserState_m496349206_gp_0_0_0_0_Types[] = { &Pubnub_GetUserState_m496349206_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_GetUserState_m496349206_gp_0_0_0_0 = { 1, GenInst_Pubnub_GetUserState_m496349206_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_GetUserState_m1227177878_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_GetUserState_m1227177878_gp_0_0_0_0_Types[] = { &Pubnub_GetUserState_m1227177878_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_GetUserState_m1227177878_gp_0_0_0_0 = { 1, GenInst_Pubnub_GetUserState_m1227177878_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_RegisterDeviceForPush_m1562716200_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_RegisterDeviceForPush_m1562716200_gp_0_0_0_0_Types[] = { &Pubnub_RegisterDeviceForPush_m1562716200_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_RegisterDeviceForPush_m1562716200_gp_0_0_0_0 = { 1, GenInst_Pubnub_RegisterDeviceForPush_m1562716200_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_UnregisterDeviceForPush_m3670068759_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_UnregisterDeviceForPush_m3670068759_gp_0_0_0_0_Types[] = { &Pubnub_UnregisterDeviceForPush_m3670068759_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_UnregisterDeviceForPush_m3670068759_gp_0_0_0_0 = { 1, GenInst_Pubnub_UnregisterDeviceForPush_m3670068759_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_RemoveChannelForDevicePush_m3529259676_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_RemoveChannelForDevicePush_m3529259676_gp_0_0_0_0_Types[] = { &Pubnub_RemoveChannelForDevicePush_m3529259676_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_RemoveChannelForDevicePush_m3529259676_gp_0_0_0_0 = { 1, GenInst_Pubnub_RemoveChannelForDevicePush_m3529259676_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_GetChannelsForDevicePush_m2905882887_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_GetChannelsForDevicePush_m2905882887_gp_0_0_0_0_Types[] = { &Pubnub_GetChannelsForDevicePush_m2905882887_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_GetChannelsForDevicePush_m2905882887_gp_0_0_0_0 = { 1, GenInst_Pubnub_GetChannelsForDevicePush_m2905882887_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_AddChannelsToChannelGroup_m2030738952_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_AddChannelsToChannelGroup_m2030738952_gp_0_0_0_0_Types[] = { &Pubnub_AddChannelsToChannelGroup_m2030738952_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_AddChannelsToChannelGroup_m2030738952_gp_0_0_0_0 = { 1, GenInst_Pubnub_AddChannelsToChannelGroup_m2030738952_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_RemoveChannelsFromChannelGroup_m3440325472_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_RemoveChannelsFromChannelGroup_m3440325472_gp_0_0_0_0_Types[] = { &Pubnub_RemoveChannelsFromChannelGroup_m3440325472_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_RemoveChannelsFromChannelGroup_m3440325472_gp_0_0_0_0 = { 1, GenInst_Pubnub_RemoveChannelsFromChannelGroup_m3440325472_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_RemoveChannelGroup_m3630438968_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_RemoveChannelGroup_m3630438968_gp_0_0_0_0_Types[] = { &Pubnub_RemoveChannelGroup_m3630438968_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_RemoveChannelGroup_m3630438968_gp_0_0_0_0 = { 1, GenInst_Pubnub_RemoveChannelGroup_m3630438968_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_GetChannelsForChannelGroup_m2818075329_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_GetChannelsForChannelGroup_m2818075329_gp_0_0_0_0_Types[] = { &Pubnub_GetChannelsForChannelGroup_m2818075329_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_GetChannelsForChannelGroup_m2818075329_gp_0_0_0_0 = { 1, GenInst_Pubnub_GetChannelsForChannelGroup_m2818075329_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_GetAllChannelGroups_m1267467130_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_GetAllChannelGroups_m1267467130_gp_0_0_0_0_Types[] = { &Pubnub_GetAllChannelGroups_m1267467130_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_GetAllChannelGroups_m1267467130_gp_0_0_0_0 = { 1, GenInst_Pubnub_GetAllChannelGroups_m1267467130_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_TerminateCurrentSubscriberRequest_m3329387326_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_TerminateCurrentSubscriberRequest_m3329387326_gp_0_0_0_0_Types[] = { &Pubnub_TerminateCurrentSubscriberRequest_m3329387326_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_TerminateCurrentSubscriberRequest_m3329387326_gp_0_0_0_0 = { 1, GenInst_Pubnub_TerminateCurrentSubscriberRequest_m3329387326_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_EndPendingRequests_m2437671125_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_EndPendingRequests_m2437671125_gp_0_0_0_0_Types[] = { &Pubnub_EndPendingRequests_m2437671125_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_EndPendingRequests_m2437671125_gp_0_0_0_0 = { 1, GenInst_Pubnub_EndPendingRequests_m2437671125_gp_0_0_0_0_Types };
extern const Il2CppType Pubnub_ChangeUUID_m1807290336_gp_0_0_0_0;
static const Il2CppType* GenInst_Pubnub_ChangeUUID_m1807290336_gp_0_0_0_0_Types[] = { &Pubnub_ChangeUUID_m1807290336_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Pubnub_ChangeUUID_m1807290336_gp_0_0_0_0 = { 1, GenInst_Pubnub_ChangeUUID_m1807290336_gp_0_0_0_0_Types };
extern const Il2CppType PubnubChannelCallback_1_t1317012685_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubChannelCallback_1_t1317012685_gp_0_0_0_0_Types[] = { &PubnubChannelCallback_1_t1317012685_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubChannelCallback_1_t1317012685_gp_0_0_0_0 = { 1, GenInst_PubnubChannelCallback_1_t1317012685_gp_0_0_0_0_Types };
extern const Il2CppType PubnubCallbacks_SendCallbacks_m1716641792_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubCallbacks_SendCallbacks_m1716641792_gp_0_0_0_0_Types[] = { &PubnubCallbacks_SendCallbacks_m1716641792_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubCallbacks_SendCallbacks_m1716641792_gp_0_0_0_0 = { 1, GenInst_PubnubCallbacks_SendCallbacks_m1716641792_gp_0_0_0_0_Types };
extern const Il2CppType PubnubCallbacks_SendCallbackChannelEntity_m569285385_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubCallbacks_SendCallbackChannelEntity_m569285385_gp_0_0_0_0_Types[] = { &PubnubCallbacks_SendCallbackChannelEntity_m569285385_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubCallbacks_SendCallbackChannelEntity_m569285385_gp_0_0_0_0 = { 1, GenInst_PubnubCallbacks_SendCallbackChannelEntity_m569285385_gp_0_0_0_0_Types };
extern const Il2CppType PubnubCallbacks_SendCallbacks_m1184457907_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubCallbacks_SendCallbacks_m1184457907_gp_0_0_0_0_Types[] = { &PubnubCallbacks_SendCallbacks_m1184457907_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubCallbacks_SendCallbacks_m1184457907_gp_0_0_0_0 = { 1, GenInst_PubnubCallbacks_SendCallbacks_m1184457907_gp_0_0_0_0_Types };
extern const Il2CppType PubnubCallbacks_SendCallbackBasedOnType_m2834347013_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubCallbacks_SendCallbackBasedOnType_m2834347013_gp_0_0_0_0_Types[] = { &PubnubCallbacks_SendCallbackBasedOnType_m2834347013_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubCallbacks_SendCallbackBasedOnType_m2834347013_gp_0_0_0_0 = { 1, GenInst_PubnubCallbacks_SendCallbackBasedOnType_m2834347013_gp_0_0_0_0_Types };
extern const Il2CppType PubnubCallbacks_SendCallbacks_m1731919200_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubCallbacks_SendCallbacks_m1731919200_gp_0_0_0_0_Types[] = { &PubnubCallbacks_SendCallbacks_m1731919200_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubCallbacks_SendCallbacks_m1731919200_gp_0_0_0_0 = { 1, GenInst_PubnubCallbacks_SendCallbacks_m1731919200_gp_0_0_0_0_Types };
extern const Il2CppType PubnubCallbacks_SendCallback_m417186584_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubCallbacks_SendCallback_m417186584_gp_0_0_0_0_Types[] = { &PubnubCallbacks_SendCallback_m417186584_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubCallbacks_SendCallback_m417186584_gp_0_0_0_0 = { 1, GenInst_PubnubCallbacks_SendCallback_m417186584_gp_0_0_0_0_Types };
extern const Il2CppType PubnubCallbacks_FireErrorCallbacksForAllChannels_m3526528221_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubCallbacks_FireErrorCallbacksForAllChannels_m3526528221_gp_0_0_0_0_Types[] = { &PubnubCallbacks_FireErrorCallbacksForAllChannels_m3526528221_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubCallbacks_FireErrorCallbacksForAllChannels_m3526528221_gp_0_0_0_0 = { 1, GenInst_PubnubCallbacks_FireErrorCallbacksForAllChannels_m3526528221_gp_0_0_0_0_Types };
extern const Il2CppType PubnubCallbacks_FireErrorCallbacksForAllChannels_m1593768736_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubCallbacks_FireErrorCallbacksForAllChannels_m1593768736_gp_0_0_0_0_Types[] = { &PubnubCallbacks_FireErrorCallbacksForAllChannels_m1593768736_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubCallbacks_FireErrorCallbacksForAllChannels_m1593768736_gp_0_0_0_0 = { 1, GenInst_PubnubCallbacks_FireErrorCallbacksForAllChannels_m1593768736_gp_0_0_0_0_Types };
extern const Il2CppType PubnubCallbacks_FireErrorCallbacksForAllChannelsCommon_m3626991610_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubCallbacks_FireErrorCallbacksForAllChannelsCommon_m3626991610_gp_0_0_0_0_Types[] = { &PubnubCallbacks_FireErrorCallbacksForAllChannelsCommon_m3626991610_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubCallbacks_FireErrorCallbacksForAllChannelsCommon_m3626991610_gp_0_0_0_0 = { 1, GenInst_PubnubCallbacks_FireErrorCallbacksForAllChannelsCommon_m3626991610_gp_0_0_0_0_Types };
extern const Il2CppType PubnubCallbacks_FireErrorCallbacksForAllChannels_m1778702368_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubCallbacks_FireErrorCallbacksForAllChannels_m1778702368_gp_0_0_0_0_Types[] = { &PubnubCallbacks_FireErrorCallbacksForAllChannels_m1778702368_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubCallbacks_FireErrorCallbacksForAllChannels_m1778702368_gp_0_0_0_0 = { 1, GenInst_PubnubCallbacks_FireErrorCallbacksForAllChannels_m1778702368_gp_0_0_0_0_Types };
extern const Il2CppType PubnubCallbacks_FireErrorCallback_m122111588_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubCallbacks_FireErrorCallback_m122111588_gp_0_0_0_0_Types[] = { &PubnubCallbacks_FireErrorCallback_m122111588_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubCallbacks_FireErrorCallback_m122111588_gp_0_0_0_0 = { 1, GenInst_PubnubCallbacks_FireErrorCallback_m122111588_gp_0_0_0_0_Types };
extern const Il2CppType PubnubCallbacks_GetPubnubChannelCallback_m285047123_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubCallbacks_GetPubnubChannelCallback_m285047123_gp_0_0_0_0_Types[] = { &PubnubCallbacks_GetPubnubChannelCallback_m285047123_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubCallbacks_GetPubnubChannelCallback_m285047123_gp_0_0_0_0 = { 1, GenInst_PubnubCallbacks_GetPubnubChannelCallback_m285047123_gp_0_0_0_0_Types };
extern const Il2CppType PubnubCallbacks_CallErrorCallback_m4008086708_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubCallbacks_CallErrorCallback_m4008086708_gp_0_0_0_0_Types[] = { &PubnubCallbacks_CallErrorCallback_m4008086708_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubCallbacks_CallErrorCallback_m4008086708_gp_0_0_0_0 = { 1, GenInst_PubnubCallbacks_CallErrorCallback_m4008086708_gp_0_0_0_0_Types };
extern const Il2CppType PubnubCallbacks_CallErrorCallback_m3396799529_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubCallbacks_CallErrorCallback_m3396799529_gp_0_0_0_0_Types[] = { &PubnubCallbacks_CallErrorCallback_m3396799529_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubCallbacks_CallErrorCallback_m3396799529_gp_0_0_0_0 = { 1, GenInst_PubnubCallbacks_CallErrorCallback_m3396799529_gp_0_0_0_0_Types };
extern const Il2CppType PubnubCallbacks_CallErrorCallback_m2481956824_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubCallbacks_CallErrorCallback_m2481956824_gp_0_0_0_0_Types[] = { &PubnubCallbacks_CallErrorCallback_m2481956824_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubCallbacks_CallErrorCallback_m2481956824_gp_0_0_0_0 = { 1, GenInst_PubnubCallbacks_CallErrorCallback_m2481956824_gp_0_0_0_0_Types };
extern const Il2CppType PubnubCallbacks_CallCallbackForEachChannelEntity_m3119983772_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubCallbacks_CallCallbackForEachChannelEntity_m3119983772_gp_0_0_0_0_Types[] = { &PubnubCallbacks_CallCallbackForEachChannelEntity_m3119983772_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubCallbacks_CallCallbackForEachChannelEntity_m3119983772_gp_0_0_0_0 = { 1, GenInst_PubnubCallbacks_CallCallbackForEachChannelEntity_m3119983772_gp_0_0_0_0_Types };
extern const Il2CppType PubnubCallbacks_CallErrorCallback_m3337762012_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubCallbacks_CallErrorCallback_m3337762012_gp_0_0_0_0_Types[] = { &PubnubCallbacks_CallErrorCallback_m3337762012_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubCallbacks_CallErrorCallback_m3337762012_gp_0_0_0_0 = { 1, GenInst_PubnubCallbacks_CallErrorCallback_m3337762012_gp_0_0_0_0_Types };
extern const Il2CppType PubnubCallbacks_JsonResponseToCallback_m772386789_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubCallbacks_JsonResponseToCallback_m772386789_gp_0_0_0_0_Types[] = { &PubnubCallbacks_JsonResponseToCallback_m772386789_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubCallbacks_JsonResponseToCallback_m772386789_gp_0_0_0_0 = { 1, GenInst_PubnubCallbacks_JsonResponseToCallback_m772386789_gp_0_0_0_0_Types };
extern const Il2CppType PubnubCallbacks_JsonResponseToCallback_m2546918935_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubCallbacks_JsonResponseToCallback_m2546918935_gp_0_0_0_0_Types[] = { &PubnubCallbacks_JsonResponseToCallback_m2546918935_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubCallbacks_JsonResponseToCallback_m2546918935_gp_0_0_0_0 = { 1, GenInst_PubnubCallbacks_JsonResponseToCallback_m2546918935_gp_0_0_0_0_Types };
extern const Il2CppType PubnubCallbacks_GoToCallback_m3707684152_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubCallbacks_GoToCallback_m3707684152_gp_0_0_0_0_Types[] = { &PubnubCallbacks_GoToCallback_m3707684152_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubCallbacks_GoToCallback_m3707684152_gp_0_0_0_0 = { 1, GenInst_PubnubCallbacks_GoToCallback_m3707684152_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_Subscribe_m2468939927_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_Subscribe_m2468939927_gp_0_0_0_0_Types[] = { &PubnubUnity_Subscribe_m2468939927_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_Subscribe_m2468939927_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_Subscribe_m2468939927_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_Publish_m970715889_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_Publish_m970715889_gp_0_0_0_0_Types[] = { &PubnubUnity_Publish_m970715889_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_Publish_m970715889_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_Publish_m970715889_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_Presence_m4218118026_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_Presence_m4218118026_gp_0_0_0_0_Types[] = { &PubnubUnity_Presence_m4218118026_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_Presence_m4218118026_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_Presence_m4218118026_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_DetailedHistory_m1201134702_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_DetailedHistory_m1201134702_gp_0_0_0_0_Types[] = { &PubnubUnity_DetailedHistory_m1201134702_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_DetailedHistory_m1201134702_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_DetailedHistory_m1201134702_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_HereNow_m4073893281_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_HereNow_m4073893281_gp_0_0_0_0_Types[] = { &PubnubUnity_HereNow_m4073893281_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_HereNow_m4073893281_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_HereNow_m4073893281_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_GlobalHereNow_m1913427906_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_GlobalHereNow_m1913427906_gp_0_0_0_0_Types[] = { &PubnubUnity_GlobalHereNow_m1913427906_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_GlobalHereNow_m1913427906_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_GlobalHereNow_m1913427906_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_WhereNow_m3016916584_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_WhereNow_m3016916584_gp_0_0_0_0_Types[] = { &PubnubUnity_WhereNow_m3016916584_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_WhereNow_m3016916584_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_WhereNow_m3016916584_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_PresenceUnsubscribe_m1129371395_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_PresenceUnsubscribe_m1129371395_gp_0_0_0_0_Types[] = { &PubnubUnity_PresenceUnsubscribe_m1129371395_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_PresenceUnsubscribe_m1129371395_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_PresenceUnsubscribe_m1129371395_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_Unsubscribe_m3864006068_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_Unsubscribe_m3864006068_gp_0_0_0_0_Types[] = { &PubnubUnity_Unsubscribe_m3864006068_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_Unsubscribe_m3864006068_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_Unsubscribe_m3864006068_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_Time_m3989694962_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_Time_m3989694962_gp_0_0_0_0_Types[] = { &PubnubUnity_Time_m3989694962_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_Time_m3989694962_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_Time_m3989694962_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_GrantAccess_m1374897058_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_GrantAccess_m1374897058_gp_0_0_0_0_Types[] = { &PubnubUnity_GrantAccess_m1374897058_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_GrantAccess_m1374897058_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_GrantAccess_m1374897058_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_GrantPresenceAccess_m2725868595_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_GrantPresenceAccess_m2725868595_gp_0_0_0_0_Types[] = { &PubnubUnity_GrantPresenceAccess_m2725868595_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_GrantPresenceAccess_m2725868595_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_GrantPresenceAccess_m2725868595_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_AuditAccess_m2295512540_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_AuditAccess_m2295512540_gp_0_0_0_0_Types[] = { &PubnubUnity_AuditAccess_m2295512540_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_AuditAccess_m2295512540_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_AuditAccess_m2295512540_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_AuditPresenceAccess_m2308049845_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_AuditPresenceAccess_m2308049845_gp_0_0_0_0_Types[] = { &PubnubUnity_AuditPresenceAccess_m2308049845_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_AuditPresenceAccess_m2308049845_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_AuditPresenceAccess_m2308049845_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_SetUserState_m3661404517_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_SetUserState_m3661404517_gp_0_0_0_0_Types[] = { &PubnubUnity_SetUserState_m3661404517_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_SetUserState_m3661404517_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_SetUserState_m3661404517_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_SetUserState_m3414125520_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_SetUserState_m3414125520_gp_0_0_0_0_Types[] = { &PubnubUnity_SetUserState_m3414125520_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_SetUserState_m3414125520_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_SetUserState_m3414125520_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_SharedSetUserState_m2517404725_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_SharedSetUserState_m2517404725_gp_0_0_0_0_Types[] = { &PubnubUnity_SharedSetUserState_m2517404725_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_SharedSetUserState_m2517404725_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_SharedSetUserState_m2517404725_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_GetUserState_m1048009635_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_GetUserState_m1048009635_gp_0_0_0_0_Types[] = { &PubnubUnity_GetUserState_m1048009635_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_GetUserState_m1048009635_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_GetUserState_m1048009635_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_RegisterDeviceForPush_m2758890001_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_RegisterDeviceForPush_m2758890001_gp_0_0_0_0_Types[] = { &PubnubUnity_RegisterDeviceForPush_m2758890001_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_RegisterDeviceForPush_m2758890001_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_RegisterDeviceForPush_m2758890001_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_UnregisterDeviceForPush_m1203487220_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_UnregisterDeviceForPush_m1203487220_gp_0_0_0_0_Types[] = { &PubnubUnity_UnregisterDeviceForPush_m1203487220_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_UnregisterDeviceForPush_m1203487220_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_UnregisterDeviceForPush_m1203487220_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_RemoveChannelForDevicePush_m1412460385_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_RemoveChannelForDevicePush_m1412460385_gp_0_0_0_0_Types[] = { &PubnubUnity_RemoveChannelForDevicePush_m1412460385_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_RemoveChannelForDevicePush_m1412460385_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_RemoveChannelForDevicePush_m1412460385_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_GetChannelsForDevicePush_m1033807426_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_GetChannelsForDevicePush_m1033807426_gp_0_0_0_0_Types[] = { &PubnubUnity_GetChannelsForDevicePush_m1033807426_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_GetChannelsForDevicePush_m1033807426_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_GetChannelsForDevicePush_m1033807426_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_AddChannelsToChannelGroup_m2931888159_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_AddChannelsToChannelGroup_m2931888159_gp_0_0_0_0_Types[] = { &PubnubUnity_AddChannelsToChannelGroup_m2931888159_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_AddChannelsToChannelGroup_m2931888159_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_AddChannelsToChannelGroup_m2931888159_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_RemoveChannelsFromChannelGroup_m4235633181_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_RemoveChannelsFromChannelGroup_m4235633181_gp_0_0_0_0_Types[] = { &PubnubUnity_RemoveChannelsFromChannelGroup_m4235633181_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_RemoveChannelsFromChannelGroup_m4235633181_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_RemoveChannelsFromChannelGroup_m4235633181_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_RemoveChannelGroup_m4155625375_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_RemoveChannelGroup_m4155625375_gp_0_0_0_0_Types[] = { &PubnubUnity_RemoveChannelGroup_m4155625375_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_RemoveChannelGroup_m4155625375_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_RemoveChannelGroup_m4155625375_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_GetChannelsForChannelGroup_m1635924752_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_GetChannelsForChannelGroup_m1635924752_gp_0_0_0_0_Types[] = { &PubnubUnity_GetChannelsForChannelGroup_m1635924752_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_GetChannelsForChannelGroup_m1635924752_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_GetChannelsForChannelGroup_m1635924752_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_GetAllChannelGroups_m323957427_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_GetAllChannelGroups_m323957427_gp_0_0_0_0_Types[] = { &PubnubUnity_GetAllChannelGroups_m323957427_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_GetAllChannelGroups_m323957427_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_GetAllChannelGroups_m323957427_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_ChannelGroupAuditAccess_m1374637066_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_ChannelGroupAuditAccess_m1374637066_gp_0_0_0_0_Types[] = { &PubnubUnity_ChannelGroupAuditAccess_m1374637066_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_ChannelGroupAuditAccess_m1374637066_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_ChannelGroupAuditAccess_m1374637066_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_ChannelGroupGrantAccess_m224916673_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_ChannelGroupGrantAccess_m224916673_gp_0_0_0_0_Types[] = { &PubnubUnity_ChannelGroupGrantAccess_m224916673_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_ChannelGroupGrantAccess_m224916673_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_ChannelGroupGrantAccess_m224916673_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_TerminateCurrentSubscriberRequest_m1519878411_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_TerminateCurrentSubscriberRequest_m1519878411_gp_0_0_0_0_Types[] = { &PubnubUnity_TerminateCurrentSubscriberRequest_m1519878411_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_TerminateCurrentSubscriberRequest_m1519878411_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_TerminateCurrentSubscriberRequest_m1519878411_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_EndPendingRequests_m2955742683_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_EndPendingRequests_m2955742683_gp_0_0_0_0_Types[] = { &PubnubUnity_EndPendingRequests_m2955742683_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_EndPendingRequests_m2955742683_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_EndPendingRequests_m2955742683_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_ChangeUUID_m3033551624_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_ChangeUUID_m3033551624_gp_0_0_0_0_Types[] = { &PubnubUnity_ChangeUUID_m3033551624_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_ChangeUUID_m3033551624_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_ChangeUUID_m3033551624_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_StopHeartbeat_m115641262_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_StopHeartbeat_m115641262_gp_0_0_0_0_Types[] = { &PubnubUnity_StopHeartbeat_m115641262_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_StopHeartbeat_m115641262_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_StopHeartbeat_m115641262_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_StopPresenceHeartbeat_m824786707_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_StopPresenceHeartbeat_m824786707_gp_0_0_0_0_Types[] = { &PubnubUnity_StopPresenceHeartbeat_m824786707_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_StopPresenceHeartbeat_m824786707_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_StopPresenceHeartbeat_m824786707_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_StartPresenceHeartbeat_m734069910_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_StartPresenceHeartbeat_m734069910_gp_0_0_0_0_Types[] = { &PubnubUnity_StartPresenceHeartbeat_m734069910_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_StartPresenceHeartbeat_m734069910_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_StartPresenceHeartbeat_m734069910_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_RunPresenceHeartbeat_m3606858653_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_RunPresenceHeartbeat_m3606858653_gp_0_0_0_0_Types[] = { &PubnubUnity_RunPresenceHeartbeat_m3606858653_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_RunPresenceHeartbeat_m3606858653_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_RunPresenceHeartbeat_m3606858653_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_StartHeartbeat_m2311398285_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_StartHeartbeat_m2311398285_gp_0_0_0_0_Types[] = { &PubnubUnity_StartHeartbeat_m2311398285_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_StartHeartbeat_m2311398285_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_StartHeartbeat_m2311398285_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_RunHeartbeat_m212101030_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_RunHeartbeat_m212101030_gp_0_0_0_0_Types[] = { &PubnubUnity_RunHeartbeat_m212101030_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_RunHeartbeat_m212101030_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_RunHeartbeat_m212101030_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_InternetConnectionAvailableHandler_m2273761285_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_InternetConnectionAvailableHandler_m2273761285_gp_0_0_0_0_Types[] = { &PubnubUnity_InternetConnectionAvailableHandler_m2273761285_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_InternetConnectionAvailableHandler_m2273761285_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_InternetConnectionAvailableHandler_m2273761285_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_HeartbeatHandler_m4185383501_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_HeartbeatHandler_m4185383501_gp_0_0_0_0_Types[] = { &PubnubUnity_HeartbeatHandler_m4185383501_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_HeartbeatHandler_m4185383501_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_HeartbeatHandler_m4185383501_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_PresenceHeartbeatHandler_m1687634496_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_PresenceHeartbeatHandler_m1687634496_gp_0_0_0_0_Types[] = { &PubnubUnity_PresenceHeartbeatHandler_m1687634496_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_PresenceHeartbeatHandler_m1687634496_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_PresenceHeartbeatHandler_m1687634496_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_SubscribePresenceHanlder_m1008753418_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_SubscribePresenceHanlder_m1008753418_gp_0_0_0_0_Types[] = { &PubnubUnity_SubscribePresenceHanlder_m1008753418_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_SubscribePresenceHanlder_m1008753418_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_SubscribePresenceHanlder_m1008753418_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_NonSubscribeHandler_m744339322_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_NonSubscribeHandler_m744339322_gp_0_0_0_0_Types[] = { &PubnubUnity_NonSubscribeHandler_m744339322_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_NonSubscribeHandler_m744339322_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_NonSubscribeHandler_m744339322_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_ProcessCoroutineCompleteResponse_m1108104348_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_ProcessCoroutineCompleteResponse_m1108104348_gp_0_0_0_0_Types[] = { &PubnubUnity_ProcessCoroutineCompleteResponse_m1108104348_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_ProcessCoroutineCompleteResponse_m1108104348_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_ProcessCoroutineCompleteResponse_m1108104348_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_CoroutineCompleteHandler_m1901552286_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_CoroutineCompleteHandler_m1901552286_gp_0_0_0_0_Types[] = { &PubnubUnity_CoroutineCompleteHandler_m1901552286_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_CoroutineCompleteHandler_m1901552286_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_CoroutineCompleteHandler_m1901552286_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_ResponseCallbackNonErrorHandler_m2985386212_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_ResponseCallbackNonErrorHandler_m2985386212_gp_0_0_0_0_Types[] = { &PubnubUnity_ResponseCallbackNonErrorHandler_m2985386212_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_ResponseCallbackNonErrorHandler_m2985386212_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_ResponseCallbackNonErrorHandler_m2985386212_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_UrlProcessResponseCallbackNonAsync_m4100457174_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_UrlProcessResponseCallbackNonAsync_m4100457174_gp_0_0_0_0_Types[] = { &PubnubUnity_UrlProcessResponseCallbackNonAsync_m4100457174_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_UrlProcessResponseCallbackNonAsync_m4100457174_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_UrlProcessResponseCallbackNonAsync_m4100457174_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_MultiplexExceptionHandler_m3843920276_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_MultiplexExceptionHandler_m3843920276_gp_0_0_0_0_Types[] = { &PubnubUnity_MultiplexExceptionHandler_m3843920276_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_MultiplexExceptionHandler_m3843920276_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_MultiplexExceptionHandler_m3843920276_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_HandleMultiplexException_m884452366_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_HandleMultiplexException_m884452366_gp_0_0_0_0_Types[] = { &PubnubUnity_HandleMultiplexException_m884452366_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_HandleMultiplexException_m884452366_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_HandleMultiplexException_m884452366_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_ParseReceiedJSONV2_m3877215119_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_ParseReceiedJSONV2_m3877215119_gp_0_0_0_0_Types[] = { &PubnubUnity_ParseReceiedJSONV2_m3877215119_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_ParseReceiedJSONV2_m3877215119_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_ParseReceiedJSONV2_m3877215119_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_ParseReceiedTimetoken_m286559067_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_ParseReceiedTimetoken_m286559067_gp_0_0_0_0_Types[] = { &PubnubUnity_ParseReceiedTimetoken_m286559067_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_ParseReceiedTimetoken_m286559067_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_ParseReceiedTimetoken_m286559067_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_RunRequests_m1850633377_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_RunRequests_m1850633377_gp_0_0_0_0_Types[] = { &PubnubUnity_RunRequests_m1850633377_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_RunRequests_m1850633377_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_RunRequests_m1850633377_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_UrlProcessRequest_m2222622787_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_UrlProcessRequest_m2222622787_gp_0_0_0_0_Types[] = { &PubnubUnity_UrlProcessRequest_m2222622787_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_UrlProcessRequest_m2222622787_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_UrlProcessRequest_m2222622787_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_AbortPreviousRequest_m555711142_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_AbortPreviousRequest_m555711142_gp_0_0_0_0_Types[] = { &PubnubUnity_AbortPreviousRequest_m555711142_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_AbortPreviousRequest_m555711142_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_AbortPreviousRequest_m555711142_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_RemoveUnsubscribedChannelsAndDeleteUserState_m4250063849_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_RemoveUnsubscribedChannelsAndDeleteUserState_m4250063849_gp_0_0_0_0_Types[] = { &PubnubUnity_RemoveUnsubscribedChannelsAndDeleteUserState_m4250063849_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_RemoveUnsubscribedChannelsAndDeleteUserState_m4250063849_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_RemoveUnsubscribedChannelsAndDeleteUserState_m4250063849_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_ContinueToSubscribeRestOfChannels_m2580276620_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_ContinueToSubscribeRestOfChannels_m2580276620_gp_0_0_0_0_Types[] = { &PubnubUnity_ContinueToSubscribeRestOfChannels_m2580276620_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_ContinueToSubscribeRestOfChannels_m2580276620_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_ContinueToSubscribeRestOfChannels_m2580276620_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_MultiChannelUnsubscribeInit_m4205939232_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_MultiChannelUnsubscribeInit_m4205939232_gp_0_0_0_0_Types[] = { &PubnubUnity_MultiChannelUnsubscribeInit_m4205939232_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_MultiChannelUnsubscribeInit_m4205939232_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_MultiChannelUnsubscribeInit_m4205939232_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_MultiChannelSubscribeInit_m584995115_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_MultiChannelSubscribeInit_m584995115_gp_0_0_0_0_Types[] = { &PubnubUnity_MultiChannelSubscribeInit_m584995115_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_MultiChannelSubscribeInit_m584995115_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_MultiChannelSubscribeInit_m584995115_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_CheckAllChannelsAreUnsubscribed_m1395504951_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_CheckAllChannelsAreUnsubscribed_m1395504951_gp_0_0_0_0_Types[] = { &PubnubUnity_CheckAllChannelsAreUnsubscribed_m1395504951_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_CheckAllChannelsAreUnsubscribed_m1395504951_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_CheckAllChannelsAreUnsubscribed_m1395504951_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_MultiChannelSubscribeRequest_m1765398455_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_MultiChannelSubscribeRequest_m1765398455_gp_0_0_0_0_Types[] = { &PubnubUnity_MultiChannelSubscribeRequest_m1765398455_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_MultiChannelSubscribeRequest_m1765398455_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_MultiChannelSubscribeRequest_m1765398455_gp_0_0_0_0_Types };
extern const Il2CppType PubnubUnity_RetryLoop_m2862844403_gp_0_0_0_0;
static const Il2CppType* GenInst_PubnubUnity_RetryLoop_m2862844403_gp_0_0_0_0_Types[] = { &PubnubUnity_RetryLoop_m2862844403_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PubnubUnity_RetryLoop_m2862844403_gp_0_0_0_0 = { 1, GenInst_PubnubUnity_RetryLoop_m2862844403_gp_0_0_0_0_Types };
extern const Il2CppType ReconnectState_1_t3360909985_gp_0_0_0_0;
static const Il2CppType* GenInst_ReconnectState_1_t3360909985_gp_0_0_0_0_Types[] = { &ReconnectState_1_t3360909985_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconnectState_1_t3360909985_gp_0_0_0_0 = { 1, GenInst_ReconnectState_1_t3360909985_gp_0_0_0_0_Types };
extern const Il2CppType RequestState_1_t1134074457_gp_0_0_0_0;
static const Il2CppType* GenInst_RequestState_1_t1134074457_gp_0_0_0_0_Types[] = { &RequestState_1_t1134074457_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_RequestState_1_t1134074457_gp_0_0_0_0 = { 1, GenInst_RequestState_1_t1134074457_gp_0_0_0_0_Types };
extern const Il2CppType SafeDictionary_2_t977272055_gp_0_0_0_0;
extern const Il2CppType SafeDictionary_2_t977272055_gp_1_0_0_0;
static const Il2CppType* GenInst_SafeDictionary_2_t977272055_gp_0_0_0_0_SafeDictionary_2_t977272055_gp_1_0_0_0_SafeDictionary_2_t977272055_gp_1_0_0_0_Types[] = { &SafeDictionary_2_t977272055_gp_0_0_0_0, &SafeDictionary_2_t977272055_gp_1_0_0_0, &SafeDictionary_2_t977272055_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SafeDictionary_2_t977272055_gp_0_0_0_0_SafeDictionary_2_t977272055_gp_1_0_0_0_SafeDictionary_2_t977272055_gp_1_0_0_0 = { 3, GenInst_SafeDictionary_2_t977272055_gp_0_0_0_0_SafeDictionary_2_t977272055_gp_1_0_0_0_SafeDictionary_2_t977272055_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_SafeDictionary_2_t977272055_gp_0_0_0_0_Types[] = { &SafeDictionary_2_t977272055_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SafeDictionary_2_t977272055_gp_0_0_0_0 = { 1, GenInst_SafeDictionary_2_t977272055_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_SafeDictionary_2_t977272055_gp_1_0_0_0_Types[] = { &SafeDictionary_2_t977272055_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SafeDictionary_2_t977272055_gp_1_0_0_0 = { 1, GenInst_SafeDictionary_2_t977272055_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_SafeDictionary_2_t977272055_gp_0_0_0_0_SafeDictionary_2_t977272055_gp_1_0_0_0_Types[] = { &SafeDictionary_2_t977272055_gp_0_0_0_0, &SafeDictionary_2_t977272055_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SafeDictionary_2_t977272055_gp_0_0_0_0_SafeDictionary_2_t977272055_gp_1_0_0_0 = { 2, GenInst_SafeDictionary_2_t977272055_gp_0_0_0_0_SafeDictionary_2_t977272055_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t237505930_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t237505930_0_0_0_Types[] = { &KeyValuePair_2_t237505930_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t237505930_0_0_0 = { 1, GenInst_KeyValuePair_2_t237505930_0_0_0_Types };
extern const Il2CppType Utility_CheckCallback_m3672287171_gp_0_0_0_0;
static const Il2CppType* GenInst_Utility_CheckCallback_m3672287171_gp_0_0_0_0_Types[] = { &Utility_CheckCallback_m3672287171_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Utility_CheckCallback_m3672287171_gp_0_0_0_0 = { 1, GenInst_Utility_CheckCallback_m3672287171_gp_0_0_0_0_Types };
extern const Il2CppType Utility_CheckRequestTimeoutMessageInError_m3861899226_gp_0_0_0_0;
static const Il2CppType* GenInst_Utility_CheckRequestTimeoutMessageInError_m3861899226_gp_0_0_0_0_Types[] = { &Utility_CheckRequestTimeoutMessageInError_m3861899226_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Utility_CheckRequestTimeoutMessageInError_m3861899226_gp_0_0_0_0 = { 1, GenInst_Utility_CheckRequestTimeoutMessageInError_m3861899226_gp_0_0_0_0_Types };
extern const Il2CppType CommonIntergrationTests_Deserialize_m3606102474_gp_0_0_0_0;
static const Il2CppType* GenInst_CommonIntergrationTests_Deserialize_m3606102474_gp_0_0_0_0_Types[] = { &CommonIntergrationTests_Deserialize_m3606102474_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CommonIntergrationTests_Deserialize_m3606102474_gp_0_0_0_0 = { 1, GenInst_CommonIntergrationTests_Deserialize_m3606102474_gp_0_0_0_0_Types };
extern const Il2CppType CommonIntergrationTests_TestRun_m3883849331_gp_0_0_0_0;
static const Il2CppType* GenInst_CommonIntergrationTests_TestRun_m3883849331_gp_0_0_0_0_Types[] = { &CommonIntergrationTests_TestRun_m3883849331_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CommonIntergrationTests_TestRun_m3883849331_gp_0_0_0_0 = { 1, GenInst_CommonIntergrationTests_TestRun_m3883849331_gp_0_0_0_0_Types };
extern const Il2CppType CommonIntergrationTests_TestProcessResponse_m769602744_gp_0_0_0_0;
static const Il2CppType* GenInst_CommonIntergrationTests_TestProcessResponse_m769602744_gp_0_0_0_0_Types[] = { &CommonIntergrationTests_TestProcessResponse_m769602744_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CommonIntergrationTests_TestProcessResponse_m769602744_gp_0_0_0_0 = { 1, GenInst_CommonIntergrationTests_TestProcessResponse_m769602744_gp_0_0_0_0_Types };
extern const Il2CppType CommonIntergrationTests_CcCoroutineComplete_m3907901951_gp_0_0_0_0;
static const Il2CppType* GenInst_CommonIntergrationTests_CcCoroutineComplete_m3907901951_gp_0_0_0_0_Types[] = { &CommonIntergrationTests_CcCoroutineComplete_m3907901951_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CommonIntergrationTests_CcCoroutineComplete_m3907901951_gp_0_0_0_0 = { 1, GenInst_CommonIntergrationTests_CcCoroutineComplete_m3907901951_gp_0_0_0_0_Types };
extern const Il2CppType CommonIntergrationTests_TestProcessResponseError_m1490096072_gp_0_0_0_0;
static const Il2CppType* GenInst_CommonIntergrationTests_TestProcessResponseError_m1490096072_gp_0_0_0_0_Types[] = { &CommonIntergrationTests_TestProcessResponseError_m1490096072_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CommonIntergrationTests_TestProcessResponseError_m1490096072_gp_0_0_0_0 = { 1, GenInst_CommonIntergrationTests_TestProcessResponseError_m1490096072_gp_0_0_0_0_Types };
extern const Il2CppType CommonIntergrationTests_CcCoroutineCompleteError_m2550140263_gp_0_0_0_0;
static const Il2CppType* GenInst_CommonIntergrationTests_CcCoroutineCompleteError_m2550140263_gp_0_0_0_0_Types[] = { &CommonIntergrationTests_CcCoroutineCompleteError_m2550140263_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CommonIntergrationTests_CcCoroutineCompleteError_m2550140263_gp_0_0_0_0 = { 1, GenInst_CommonIntergrationTests_CcCoroutineCompleteError_m2550140263_gp_0_0_0_0_Types };
extern const Il2CppType CommonIntergrationTests_TestBounce_m1012596302_gp_0_0_0_0;
static const Il2CppType* GenInst_CommonIntergrationTests_TestBounce_m1012596302_gp_0_0_0_0_Types[] = { &CommonIntergrationTests_TestBounce_m1012596302_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CommonIntergrationTests_TestBounce_m1012596302_gp_0_0_0_0 = { 1, GenInst_CommonIntergrationTests_TestBounce_m1012596302_gp_0_0_0_0_Types };
extern const Il2CppType AssertionComponent_Create_m2540130672_gp_0_0_0_0;
static const Il2CppType* GenInst_AssertionComponent_Create_m2540130672_gp_0_0_0_0_Types[] = { &AssertionComponent_Create_m2540130672_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AssertionComponent_Create_m2540130672_gp_0_0_0_0 = { 1, GenInst_AssertionComponent_Create_m2540130672_gp_0_0_0_0_Types };
extern const Il2CppType AssertionComponent_Create_m1225255230_gp_0_0_0_0;
static const Il2CppType* GenInst_AssertionComponent_Create_m1225255230_gp_0_0_0_0_Types[] = { &AssertionComponent_Create_m1225255230_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AssertionComponent_Create_m1225255230_gp_0_0_0_0 = { 1, GenInst_AssertionComponent_Create_m1225255230_gp_0_0_0_0_Types };
extern const Il2CppType AssertionComponent_Create_m4038277768_gp_0_0_0_0;
static const Il2CppType* GenInst_AssertionComponent_Create_m4038277768_gp_0_0_0_0_Types[] = { &AssertionComponent_Create_m4038277768_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AssertionComponent_Create_m4038277768_gp_0_0_0_0 = { 1, GenInst_AssertionComponent_Create_m4038277768_gp_0_0_0_0_Types };
extern const Il2CppType AssertionComponent_Create_m1403296970_gp_0_0_0_0;
static const Il2CppType* GenInst_AssertionComponent_Create_m1403296970_gp_0_0_0_0_Types[] = { &AssertionComponent_Create_m1403296970_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AssertionComponent_Create_m1403296970_gp_0_0_0_0 = { 1, GenInst_AssertionComponent_Create_m1403296970_gp_0_0_0_0_Types };
extern const Il2CppType AssertionComponent_Create_m2455923606_gp_0_0_0_0;
static const Il2CppType* GenInst_AssertionComponent_Create_m2455923606_gp_0_0_0_0_Types[] = { &AssertionComponent_Create_m2455923606_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AssertionComponent_Create_m2455923606_gp_0_0_0_0 = { 1, GenInst_AssertionComponent_Create_m2455923606_gp_0_0_0_0_Types };
extern const Il2CppType AssertionComponent_Create_m2411304588_gp_0_0_0_0;
static const Il2CppType* GenInst_AssertionComponent_Create_m2411304588_gp_0_0_0_0_Types[] = { &AssertionComponent_Create_m2411304588_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AssertionComponent_Create_m2411304588_gp_0_0_0_0 = { 1, GenInst_AssertionComponent_Create_m2411304588_gp_0_0_0_0_Types };
extern const Il2CppType AssertionComponent_CreateAssertionComponent_m888969327_gp_0_0_0_0;
static const Il2CppType* GenInst_AssertionComponent_CreateAssertionComponent_m888969327_gp_0_0_0_0_Types[] = { &AssertionComponent_CreateAssertionComponent_m888969327_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AssertionComponent_CreateAssertionComponent_m888969327_gp_0_0_0_0 = { 1, GenInst_AssertionComponent_CreateAssertionComponent_m888969327_gp_0_0_0_0_Types };
extern const Il2CppType ActionBaseGeneric_1_t2640614817_gp_0_0_0_0;
static const Il2CppType* GenInst_ActionBaseGeneric_1_t2640614817_gp_0_0_0_0_Types[] = { &ActionBaseGeneric_1_t2640614817_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ActionBaseGeneric_1_t2640614817_gp_0_0_0_0 = { 1, GenInst_ActionBaseGeneric_1_t2640614817_gp_0_0_0_0_Types };
extern const Il2CppType ComparerBaseGeneric_1_t3426043530_gp_0_0_0_0;
static const Il2CppType* GenInst_ComparerBaseGeneric_1_t3426043530_gp_0_0_0_0_ComparerBaseGeneric_1_t3426043530_gp_0_0_0_0_Types[] = { &ComparerBaseGeneric_1_t3426043530_gp_0_0_0_0, &ComparerBaseGeneric_1_t3426043530_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ComparerBaseGeneric_1_t3426043530_gp_0_0_0_0_ComparerBaseGeneric_1_t3426043530_gp_0_0_0_0 = { 2, GenInst_ComparerBaseGeneric_1_t3426043530_gp_0_0_0_0_ComparerBaseGeneric_1_t3426043530_gp_0_0_0_0_Types };
extern const Il2CppType ComparerBaseGeneric_2_t3426043527_gp_0_0_0_0;
extern const Il2CppType ComparerBaseGeneric_2_t3426043527_gp_1_0_0_0;
static const Il2CppType* GenInst_ComparerBaseGeneric_2_t3426043527_gp_0_0_0_0_ComparerBaseGeneric_2_t3426043527_gp_1_0_0_0_Types[] = { &ComparerBaseGeneric_2_t3426043527_gp_0_0_0_0, &ComparerBaseGeneric_2_t3426043527_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ComparerBaseGeneric_2_t3426043527_gp_0_0_0_0_ComparerBaseGeneric_2_t3426043527_gp_1_0_0_0 = { 2, GenInst_ComparerBaseGeneric_2_t3426043527_gp_0_0_0_0_ComparerBaseGeneric_2_t3426043527_gp_1_0_0_0_Types };
extern const Il2CppType VectorComparerBase_1_t2826517438_gp_0_0_0_0;
static const Il2CppType* GenInst_VectorComparerBase_1_t2826517438_gp_0_0_0_0_Types[] = { &VectorComparerBase_1_t2826517438_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_VectorComparerBase_1_t2826517438_gp_0_0_0_0 = { 1, GenInst_VectorComparerBase_1_t2826517438_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_VectorComparerBase_1_t2826517438_gp_0_0_0_0_VectorComparerBase_1_t2826517438_gp_0_0_0_0_Types[] = { &VectorComparerBase_1_t2826517438_gp_0_0_0_0, &VectorComparerBase_1_t2826517438_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_VectorComparerBase_1_t2826517438_gp_0_0_0_0_VectorComparerBase_1_t2826517438_gp_0_0_0_0 = { 2, GenInst_VectorComparerBase_1_t2826517438_gp_0_0_0_0_VectorComparerBase_1_t2826517438_gp_0_0_0_0_Types };
extern const Il2CppType DefaultExecutionOrder_t2717914595_0_0_0;
static const Il2CppType* GenInst_DefaultExecutionOrder_t2717914595_0_0_0_Types[] = { &DefaultExecutionOrder_t2717914595_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultExecutionOrder_t2717914595_0_0_0 = { 1, GenInst_DefaultExecutionOrder_t2717914595_0_0_0_Types };
extern const Il2CppType GUILayer_t3254902478_0_0_0;
static const Il2CppType* GenInst_GUILayer_t3254902478_0_0_0_Types[] = { &GUILayer_t3254902478_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t3254902478_0_0_0 = { 1, GenInst_GUILayer_t3254902478_0_0_0_Types };
extern const Il2CppType FirebaseHandler_t2907300047_0_0_0;
static const Il2CppType* GenInst_FirebaseHandler_t2907300047_0_0_0_Types[] = { &FirebaseHandler_t2907300047_0_0_0 };
extern const Il2CppGenericInst GenInst_FirebaseHandler_t2907300047_0_0_0 = { 1, GenInst_FirebaseHandler_t2907300047_0_0_0_Types };
extern const Il2CppType SynchronizationContextBehavoir_t692674473_0_0_0;
static const Il2CppType* GenInst_SynchronizationContextBehavoir_t692674473_0_0_0_Types[] = { &SynchronizationContextBehavoir_t692674473_0_0_0 };
extern const Il2CppGenericInst GenInst_SynchronizationContextBehavoir_t692674473_0_0_0 = { 1, GenInst_SynchronizationContextBehavoir_t692674473_0_0_0_Types };
extern const Il2CppType EventSystem_t3466835263_0_0_0;
static const Il2CppType* GenInst_EventSystem_t3466835263_0_0_0_Types[] = { &EventSystem_t3466835263_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSystem_t3466835263_0_0_0 = { 1, GenInst_EventSystem_t3466835263_0_0_0_Types };
extern const Il2CppType AxisEventData_t1524870173_0_0_0;
static const Il2CppType* GenInst_AxisEventData_t1524870173_0_0_0_Types[] = { &AxisEventData_t1524870173_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisEventData_t1524870173_0_0_0 = { 1, GenInst_AxisEventData_t1524870173_0_0_0_Types };
extern const Il2CppType SpriteRenderer_t1209076198_0_0_0;
static const Il2CppType* GenInst_SpriteRenderer_t1209076198_0_0_0_Types[] = { &SpriteRenderer_t1209076198_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t1209076198_0_0_0 = { 1, GenInst_SpriteRenderer_t1209076198_0_0_0_Types };
extern const Il2CppType Image_t2042527209_0_0_0;
static const Il2CppType* GenInst_Image_t2042527209_0_0_0_Types[] = { &Image_t2042527209_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t2042527209_0_0_0 = { 1, GenInst_Image_t2042527209_0_0_0_Types };
extern const Il2CppType Button_t2872111280_0_0_0;
static const Il2CppType* GenInst_Button_t2872111280_0_0_0_Types[] = { &Button_t2872111280_0_0_0 };
extern const Il2CppGenericInst GenInst_Button_t2872111280_0_0_0 = { 1, GenInst_Button_t2872111280_0_0_0_Types };
extern const Il2CppType RawImage_t2749640213_0_0_0;
static const Il2CppType* GenInst_RawImage_t2749640213_0_0_0_Types[] = { &RawImage_t2749640213_0_0_0 };
extern const Il2CppGenericInst GenInst_RawImage_t2749640213_0_0_0 = { 1, GenInst_RawImage_t2749640213_0_0_0_Types };
extern const Il2CppType Slider_t297367283_0_0_0;
static const Il2CppType* GenInst_Slider_t297367283_0_0_0_Types[] = { &Slider_t297367283_0_0_0 };
extern const Il2CppGenericInst GenInst_Slider_t297367283_0_0_0 = { 1, GenInst_Slider_t297367283_0_0_0_Types };
extern const Il2CppType Scrollbar_t3248359358_0_0_0;
static const Il2CppType* GenInst_Scrollbar_t3248359358_0_0_0_Types[] = { &Scrollbar_t3248359358_0_0_0 };
extern const Il2CppGenericInst GenInst_Scrollbar_t3248359358_0_0_0 = { 1, GenInst_Scrollbar_t3248359358_0_0_0_Types };
extern const Il2CppType InputField_t1631627530_0_0_0;
static const Il2CppType* GenInst_InputField_t1631627530_0_0_0_Types[] = { &InputField_t1631627530_0_0_0 };
extern const Il2CppGenericInst GenInst_InputField_t1631627530_0_0_0 = { 1, GenInst_InputField_t1631627530_0_0_0_Types };
extern const Il2CppType ScrollRect_t1199013257_0_0_0;
static const Il2CppType* GenInst_ScrollRect_t1199013257_0_0_0_Types[] = { &ScrollRect_t1199013257_0_0_0 };
extern const Il2CppGenericInst GenInst_ScrollRect_t1199013257_0_0_0 = { 1, GenInst_ScrollRect_t1199013257_0_0_0_Types };
extern const Il2CppType Dropdown_t1985816271_0_0_0;
static const Il2CppType* GenInst_Dropdown_t1985816271_0_0_0_Types[] = { &Dropdown_t1985816271_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_t1985816271_0_0_0 = { 1, GenInst_Dropdown_t1985816271_0_0_0_Types };
extern const Il2CppType GraphicRaycaster_t410733016_0_0_0;
static const Il2CppType* GenInst_GraphicRaycaster_t410733016_0_0_0_Types[] = { &GraphicRaycaster_t410733016_0_0_0 };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t410733016_0_0_0 = { 1, GenInst_GraphicRaycaster_t410733016_0_0_0_Types };
extern const Il2CppType CanvasRenderer_t261436805_0_0_0;
static const Il2CppType* GenInst_CanvasRenderer_t261436805_0_0_0_Types[] = { &CanvasRenderer_t261436805_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t261436805_0_0_0 = { 1, GenInst_CanvasRenderer_t261436805_0_0_0_Types };
extern const Il2CppType Corner_t1077473318_0_0_0;
static const Il2CppType* GenInst_Corner_t1077473318_0_0_0_Types[] = { &Corner_t1077473318_0_0_0 };
extern const Il2CppGenericInst GenInst_Corner_t1077473318_0_0_0 = { 1, GenInst_Corner_t1077473318_0_0_0_Types };
extern const Il2CppType Axis_t1431825778_0_0_0;
static const Il2CppType* GenInst_Axis_t1431825778_0_0_0_Types[] = { &Axis_t1431825778_0_0_0 };
extern const Il2CppGenericInst GenInst_Axis_t1431825778_0_0_0 = { 1, GenInst_Axis_t1431825778_0_0_0_Types };
extern const Il2CppType Constraint_t3558160636_0_0_0;
static const Il2CppType* GenInst_Constraint_t3558160636_0_0_0_Types[] = { &Constraint_t3558160636_0_0_0 };
extern const Il2CppGenericInst GenInst_Constraint_t3558160636_0_0_0 = { 1, GenInst_Constraint_t3558160636_0_0_0_Types };
extern const Il2CppType SubmitEvent_t907918422_0_0_0;
static const Il2CppType* GenInst_SubmitEvent_t907918422_0_0_0_Types[] = { &SubmitEvent_t907918422_0_0_0 };
extern const Il2CppGenericInst GenInst_SubmitEvent_t907918422_0_0_0 = { 1, GenInst_SubmitEvent_t907918422_0_0_0_Types };
extern const Il2CppType OnChangeEvent_t2863344003_0_0_0;
static const Il2CppType* GenInst_OnChangeEvent_t2863344003_0_0_0_Types[] = { &OnChangeEvent_t2863344003_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t2863344003_0_0_0 = { 1, GenInst_OnChangeEvent_t2863344003_0_0_0_Types };
extern const Il2CppType OnValidateInput_t1946318473_0_0_0;
static const Il2CppType* GenInst_OnValidateInput_t1946318473_0_0_0_Types[] = { &OnValidateInput_t1946318473_0_0_0 };
extern const Il2CppGenericInst GenInst_OnValidateInput_t1946318473_0_0_0 = { 1, GenInst_OnValidateInput_t1946318473_0_0_0_Types };
extern const Il2CppType LayoutElement_t2808691390_0_0_0;
static const Il2CppType* GenInst_LayoutElement_t2808691390_0_0_0_Types[] = { &LayoutElement_t2808691390_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutElement_t2808691390_0_0_0 = { 1, GenInst_LayoutElement_t2808691390_0_0_0_Types };
extern const Il2CppType RectOffset_t3387826427_0_0_0;
static const Il2CppType* GenInst_RectOffset_t3387826427_0_0_0_Types[] = { &RectOffset_t3387826427_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t3387826427_0_0_0 = { 1, GenInst_RectOffset_t3387826427_0_0_0_Types };
extern const Il2CppType TextAnchor_t112990806_0_0_0;
static const Il2CppType* GenInst_TextAnchor_t112990806_0_0_0_Types[] = { &TextAnchor_t112990806_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAnchor_t112990806_0_0_0 = { 1, GenInst_TextAnchor_t112990806_0_0_0_Types };
extern const Il2CppType AnimationTriggers_t3244928895_0_0_0;
static const Il2CppType* GenInst_AnimationTriggers_t3244928895_0_0_0_Types[] = { &AnimationTriggers_t3244928895_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t3244928895_0_0_0 = { 1, GenInst_AnimationTriggers_t3244928895_0_0_0_Types };
extern const Il2CppType Animator_t69676727_0_0_0;
static const Il2CppType* GenInst_Animator_t69676727_0_0_0_Types[] = { &Animator_t69676727_0_0_0 };
extern const Il2CppGenericInst GenInst_Animator_t69676727_0_0_0 = { 1, GenInst_Animator_t69676727_0_0_0_Types };
extern const Il2CppType FloatComparer_t4094246107_0_0_0;
static const Il2CppType* GenInst_FloatComparer_t4094246107_0_0_0_Types[] = { &FloatComparer_t4094246107_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatComparer_t4094246107_0_0_0 = { 1, GenInst_FloatComparer_t4094246107_0_0_0_Types };
extern const Il2CppType ValueDoesNotChange_t1999561901_0_0_0;
static const Il2CppType* GenInst_ValueDoesNotChange_t1999561901_0_0_0_Types[] = { &ValueDoesNotChange_t1999561901_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueDoesNotChange_t1999561901_0_0_0 = { 1, GenInst_ValueDoesNotChange_t1999561901_0_0_0_Types };
extern const Il2CppType GeneralComparer_t3787245533_0_0_0;
static const Il2CppType* GenInst_GeneralComparer_t3787245533_0_0_0_Types[] = { &GeneralComparer_t3787245533_0_0_0 };
extern const Il2CppGenericInst GenInst_GeneralComparer_t3787245533_0_0_0 = { 1, GenInst_GeneralComparer_t3787245533_0_0_0_Types };
extern const Il2CppType ImageCapture_t1449800373_0_0_0;
static const Il2CppType* GenInst_ImageCapture_t1449800373_0_0_0_Types[] = { &ImageCapture_t1449800373_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageCapture_t1449800373_0_0_0 = { 1, GenInst_ImageCapture_t1449800373_0_0_0_Types };
extern const Il2CppType Uri_t19570940_0_0_0;
static const Il2CppType* GenInst_Uri_t19570940_0_0_0_Types[] = { &Uri_t19570940_0_0_0 };
extern const Il2CppGenericInst GenInst_Uri_t19570940_0_0_0 = { 1, GenInst_Uri_t19570940_0_0_0_Types };
extern const Il2CppType CoroutineClass_t2476503358_0_0_0;
static const Il2CppType* GenInst_CoroutineClass_t2476503358_0_0_0_Types[] = { &CoroutineClass_t2476503358_0_0_0 };
extern const Il2CppGenericInst GenInst_CoroutineClass_t2476503358_0_0_0 = { 1, GenInst_CoroutineClass_t2476503358_0_0_0_Types };
static const Il2CppType* GenInst_Assembly_t4268412390_0_0_0_Type_t_0_0_0_Types[] = { &Assembly_t4268412390_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Assembly_t4268412390_0_0_0_Type_t_0_0_0 = { 2, GenInst_Assembly_t4268412390_0_0_0_Type_t_0_0_0_Types };
extern const Il2CppType TestRunner_t1304041832_0_0_0;
static const Il2CppType* GenInst_TestRunner_t1304041832_0_0_0_Types[] = { &TestRunner_t1304041832_0_0_0 };
extern const Il2CppGenericInst GenInst_TestRunner_t1304041832_0_0_0 = { 1, GenInst_TestRunner_t1304041832_0_0_0_Types };
extern const Il2CppType AudioSource_t1135106623_0_0_0;
static const Il2CppType* GenInst_AudioSource_t1135106623_0_0_0_Types[] = { &AudioSource_t1135106623_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioSource_t1135106623_0_0_0 = { 1, GenInst_AudioSource_t1135106623_0_0_0_Types };
extern const Il2CppType PerFrameRaycast_t2437905999_0_0_0;
static const Il2CppType* GenInst_PerFrameRaycast_t2437905999_0_0_0_Types[] = { &PerFrameRaycast_t2437905999_0_0_0 };
extern const Il2CppGenericInst GenInst_PerFrameRaycast_t2437905999_0_0_0 = { 1, GenInst_PerFrameRaycast_t2437905999_0_0_0_Types };
extern const Il2CppType SimpleBullet_t2114900010_0_0_0;
static const Il2CppType* GenInst_SimpleBullet_t2114900010_0_0_0_Types[] = { &SimpleBullet_t2114900010_0_0_0 };
extern const Il2CppGenericInst GenInst_SimpleBullet_t2114900010_0_0_0 = { 1, GenInst_SimpleBullet_t2114900010_0_0_0_Types };
extern const Il2CppType SphereCollider_t1662511355_0_0_0;
static const Il2CppType* GenInst_SphereCollider_t1662511355_0_0_0_Types[] = { &SphereCollider_t1662511355_0_0_0 };
extern const Il2CppGenericInst GenInst_SphereCollider_t1662511355_0_0_0 = { 1, GenInst_SphereCollider_t1662511355_0_0_0_Types };
extern const Il2CppType ParticleEmitter_t4099167268_0_0_0;
static const Il2CppType* GenInst_ParticleEmitter_t4099167268_0_0_0_Types[] = { &ParticleEmitter_t4099167268_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticleEmitter_t4099167268_0_0_0 = { 1, GenInst_ParticleEmitter_t4099167268_0_0_0_Types };
extern const Il2CppType Rigidbody_t4233889191_0_0_0;
static const Il2CppType* GenInst_Rigidbody_t4233889191_0_0_0_Types[] = { &Rigidbody_t4233889191_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody_t4233889191_0_0_0 = { 1, GenInst_Rigidbody_t4233889191_0_0_0_Types };
static const Il2CppType* GenInst_Renderer_t257310565_0_0_0_Types[] = { &Renderer_t257310565_0_0_0 };
extern const Il2CppGenericInst GenInst_Renderer_t257310565_0_0_0 = { 1, GenInst_Renderer_t257310565_0_0_0_Types };
extern const Il2CppType Collider_t3497673348_0_0_0;
static const Il2CppType* GenInst_Collider_t3497673348_0_0_0_Types[] = { &Collider_t3497673348_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider_t3497673348_0_0_0 = { 1, GenInst_Collider_t3497673348_0_0_0_Types };
extern const Il2CppType GUITexture_t1909122990_0_0_0;
static const Il2CppType* GenInst_GUITexture_t1909122990_0_0_0_Types[] = { &GUITexture_t1909122990_0_0_0 };
extern const Il2CppGenericInst GenInst_GUITexture_t1909122990_0_0_0 = { 1, GenInst_GUITexture_t1909122990_0_0_0_Types };
extern const Il2CppType Animation_t2068071072_0_0_0;
static const Il2CppType* GenInst_Animation_t2068071072_0_0_0_Types[] = { &Animation_t2068071072_0_0_0 };
extern const Il2CppGenericInst GenInst_Animation_t2068071072_0_0_0 = { 1, GenInst_Animation_t2068071072_0_0_0_Types };
extern const Il2CppType AI_t2476083018_0_0_0;
static const Il2CppType* GenInst_AI_t2476083018_0_0_0_Types[] = { &AI_t2476083018_0_0_0 };
extern const Il2CppGenericInst GenInst_AI_t2476083018_0_0_0 = { 1, GenInst_AI_t2476083018_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3733013679_0_0_0_KeyValuePair_2_t3733013679_0_0_0_Types[] = { &KeyValuePair_2_t3733013679_0_0_0, &KeyValuePair_2_t3733013679_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3733013679_0_0_0_KeyValuePair_2_t3733013679_0_0_0 = { 2, GenInst_KeyValuePair_2_t3733013679_0_0_0_KeyValuePair_2_t3733013679_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0, &KeyValuePair_2_t38854645_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_Types[] = { &Int64_t909078037_0_0_0, &Int64_t909078037_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0 = { 2, GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0_Types[] = { &CustomAttributeNamedArgument_t94157543_0_0_0, &CustomAttributeNamedArgument_t94157543_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1498197914_0_0_0, &CustomAttributeTypedArgument_t1498197914_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0_Types };
static const Il2CppType* GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0_Types[] = { &Color32_t874517518_0_0_0, &Color32_t874517518_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0 = { 2, GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0_Types };
static const Il2CppType* GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0_Types[] = { &RaycastResult_t21186376_0_0_0, &RaycastResult_t21186376_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0 = { 2, GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0_Types };
static const Il2CppType* GenInst_Playable_t3667545548_0_0_0_Playable_t3667545548_0_0_0_Types[] = { &Playable_t3667545548_0_0_0, &Playable_t3667545548_0_0_0 };
extern const Il2CppGenericInst GenInst_Playable_t3667545548_0_0_0_Playable_t3667545548_0_0_0 = { 2, GenInst_Playable_t3667545548_0_0_0_Playable_t3667545548_0_0_0_Types };
static const Il2CppType* GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0_Types[] = { &UICharInfo_t3056636800_0_0_0, &UICharInfo_t3056636800_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0 = { 2, GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0_Types };
static const Il2CppType* GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0_Types[] = { &UILineInfo_t3621277874_0_0_0, &UILineInfo_t3621277874_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0 = { 2, GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0_Types };
static const Il2CppType* GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0_Types[] = { &UIVertex_t1204258818_0_0_0, &UIVertex_t1204258818_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0 = { 2, GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0_Types };
static const Il2CppType* GenInst_ChannelIdentity_t1147162267_0_0_0_ChannelIdentity_t1147162267_0_0_0_Types[] = { &ChannelIdentity_t1147162267_0_0_0, &ChannelIdentity_t1147162267_0_0_0 };
extern const Il2CppGenericInst GenInst_ChannelIdentity_t1147162267_0_0_0_ChannelIdentity_t1147162267_0_0_0 = { 2, GenInst_ChannelIdentity_t1147162267_0_0_0_ChannelIdentity_t1147162267_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1381843961_0_0_0_KeyValuePair_2_t1381843961_0_0_0_Types[] = { &KeyValuePair_2_t1381843961_0_0_0, &KeyValuePair_2_t1381843961_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1381843961_0_0_0_KeyValuePair_2_t1381843961_0_0_0 = { 2, GenInst_KeyValuePair_2_t1381843961_0_0_0_KeyValuePair_2_t1381843961_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1381843961_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1381843961_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1381843961_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1381843961_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_CurrentRequestType_t3250227986_0_0_0_CurrentRequestType_t3250227986_0_0_0_Types[] = { &CurrentRequestType_t3250227986_0_0_0, &CurrentRequestType_t3250227986_0_0_0 };
extern const Il2CppGenericInst GenInst_CurrentRequestType_t3250227986_0_0_0_CurrentRequestType_t3250227986_0_0_0 = { 2, GenInst_CurrentRequestType_t3250227986_0_0_0_CurrentRequestType_t3250227986_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3104328646_0_0_0_KeyValuePair_2_t3104328646_0_0_0_Types[] = { &KeyValuePair_2_t3104328646_0_0_0, &KeyValuePair_2_t3104328646_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3104328646_0_0_0_KeyValuePair_2_t3104328646_0_0_0 = { 2, GenInst_KeyValuePair_2_t3104328646_0_0_0_KeyValuePair_2_t3104328646_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3104328646_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3104328646_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3104328646_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3104328646_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0, &KeyValuePair_2_t3132015601_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0 = { 2, GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3132015601_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0, &KeyValuePair_2_t3749587448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0 = { 2, GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3749587448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1008373517_0_0_0_KeyValuePair_2_t1008373517_0_0_0_Types[] = { &KeyValuePair_2_t1008373517_0_0_0, &KeyValuePair_2_t1008373517_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1008373517_0_0_0_KeyValuePair_2_t1008373517_0_0_0 = { 2, GenInst_KeyValuePair_2_t1008373517_0_0_0_KeyValuePair_2_t1008373517_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1008373517_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1008373517_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1008373517_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1008373517_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Label_t4243202660_0_0_0_Il2CppObject_0_0_0_Types[] = { &Label_t4243202660_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Label_t4243202660_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Label_t4243202660_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Label_t4243202660_0_0_0_Label_t4243202660_0_0_0_Types[] = { &Label_t4243202660_0_0_0, &Label_t4243202660_0_0_0 };
extern const Il2CppGenericInst GenInst_Label_t4243202660_0_0_0_Label_t4243202660_0_0_0 = { 2, GenInst_Label_t4243202660_0_0_0_Label_t4243202660_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t818741072_0_0_0_KeyValuePair_2_t818741072_0_0_0_Types[] = { &KeyValuePair_2_t818741072_0_0_0, &KeyValuePair_2_t818741072_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t818741072_0_0_0_KeyValuePair_2_t818741072_0_0_0 = { 2, GenInst_KeyValuePair_2_t818741072_0_0_0_KeyValuePair_2_t818741072_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t818741072_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t818741072_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t818741072_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t818741072_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1436312919_0_0_0_KeyValuePair_2_t1436312919_0_0_0_Types[] = { &KeyValuePair_2_t1436312919_0_0_0, &KeyValuePair_2_t1436312919_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1436312919_0_0_0_KeyValuePair_2_t1436312919_0_0_0 = { 2, GenInst_KeyValuePair_2_t1436312919_0_0_0_KeyValuePair_2_t1436312919_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1436312919_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1436312919_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1436312919_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1436312919_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t271247988_0_0_0_KeyValuePair_2_t271247988_0_0_0_Types[] = { &KeyValuePair_2_t271247988_0_0_0, &KeyValuePair_2_t271247988_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t271247988_0_0_0_KeyValuePair_2_t271247988_0_0_0 = { 2, GenInst_KeyValuePair_2_t271247988_0_0_0_KeyValuePair_2_t271247988_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t271247988_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t271247988_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t271247988_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t271247988_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0, &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t888819835_0_0_0_KeyValuePair_2_t888819835_0_0_0_Types[] = { &KeyValuePair_2_t888819835_0_0_0, &KeyValuePair_2_t888819835_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t888819835_0_0_0_KeyValuePair_2_t888819835_0_0_0 = { 2, GenInst_KeyValuePair_2_t888819835_0_0_0_KeyValuePair_2_t888819835_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t888819835_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t888819835_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t888819835_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t888819835_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0, &KeyValuePair_2_t1174980068_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0 = { 2, GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1174980068_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0, &KeyValuePair_2_t3716250094_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0 = { 2, GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3716250094_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2553450683_0_0_0_KeyValuePair_2_t2553450683_0_0_0_Types[] = { &KeyValuePair_2_t2553450683_0_0_0, &KeyValuePair_2_t2553450683_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2553450683_0_0_0_KeyValuePair_2_t2553450683_0_0_0 = { 2, GenInst_KeyValuePair_2_t2553450683_0_0_0_KeyValuePair_2_t2553450683_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2553450683_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2553450683_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2553450683_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2553450683_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1979316409_0_0_0_KeyValuePair_2_t1979316409_0_0_0_Types[] = { &KeyValuePair_2_t1979316409_0_0_0, &KeyValuePair_2_t1979316409_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1979316409_0_0_0_KeyValuePair_2_t1979316409_0_0_0 = { 2, GenInst_KeyValuePair_2_t1979316409_0_0_0_KeyValuePair_2_t1979316409_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1979316409_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1979316409_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1979316409_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1979316409_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Nullable_1_t334943763_0_0_0_Nullable_1_t334943763_0_0_0_Types[] = { &Nullable_1_t334943763_0_0_0, &Nullable_1_t334943763_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t334943763_0_0_0_Nullable_1_t334943763_0_0_0 = { 2, GenInst_Nullable_1_t334943763_0_0_0_Nullable_1_t334943763_0_0_0_Types };
static const Il2CppType* GenInst_Nullable_1_t334943763_0_0_0_Il2CppObject_0_0_0_Types[] = { &Nullable_1_t334943763_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t334943763_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Nullable_1_t334943763_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t38854645_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types[] = { &KeyValuePair_2_t488203048_0_0_0, &KeyValuePair_2_t488203048_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0 = { 2, GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t488203048_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types[] = { &TextEditOp_t3138797698_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types[] = { &TextEditOp_t3138797698_0_0_0, &TextEditOp_t3138797698_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0 = { 2, GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[1515] = 
{
	&GenInst_Il2CppObject_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0,
	&GenInst_Char_t3454481338_0_0_0,
	&GenInst_IConvertible_t908092482_0_0_0,
	&GenInst_IComparable_t1857082765_0_0_0,
	&GenInst_IComparable_1_t991353265_0_0_0,
	&GenInst_IEquatable_1_t1363496211_0_0_0,
	&GenInst_ValueType_t3507792607_0_0_0,
	&GenInst_Int64_t909078037_0_0_0,
	&GenInst_UInt32_t2149682021_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0,
	&GenInst_Byte_t3683104436_0_0_0,
	&GenInst_SByte_t454417549_0_0_0,
	&GenInst_Int16_t4041245914_0_0_0,
	&GenInst_UInt16_t986882611_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IEnumerable_t2911409499_0_0_0,
	&GenInst_ICloneable_t3853279282_0_0_0,
	&GenInst_IComparable_1_t3861059456_0_0_0,
	&GenInst_IEquatable_1_t4233202402_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t3412036974_0_0_0,
	&GenInst__Type_t102776839_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0,
	&GenInst__MemberInfo_t332722161_0_0_0,
	&GenInst_IFormattable_t1523031934_0_0_0,
	&GenInst_IComparable_1_t3903716671_0_0_0,
	&GenInst_IEquatable_1_t4275859617_0_0_0,
	&GenInst_Double_t4078015681_0_0_0,
	&GenInst_IComparable_1_t1614887608_0_0_0,
	&GenInst_IEquatable_1_t1987030554_0_0_0,
	&GenInst_IComparable_1_t3981521244_0_0_0,
	&GenInst_IEquatable_1_t58696894_0_0_0,
	&GenInst_IComparable_1_t1219976363_0_0_0,
	&GenInst_IEquatable_1_t1592119309_0_0_0,
	&GenInst_Single_t2076509932_0_0_0,
	&GenInst_IComparable_1_t3908349155_0_0_0,
	&GenInst_IEquatable_1_t4280492101_0_0_0,
	&GenInst_Decimal_t724701077_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0,
	&GenInst_Delegate_t3022476291_0_0_0,
	&GenInst_ISerializable_t1245643778_0_0_0,
	&GenInst_ParameterInfo_t2249040075_0_0_0,
	&GenInst__ParameterInfo_t470209990_0_0_0,
	&GenInst_ParameterModifier_t1820634920_0_0_0,
	&GenInst_IComparable_1_t2818721834_0_0_0,
	&GenInst_IEquatable_1_t3190864780_0_0_0,
	&GenInst_IComparable_1_t446068841_0_0_0,
	&GenInst_IEquatable_1_t818211787_0_0_0,
	&GenInst_IComparable_1_t1578117841_0_0_0,
	&GenInst_IEquatable_1_t1950260787_0_0_0,
	&GenInst_IComparable_1_t2286256772_0_0_0,
	&GenInst_IEquatable_1_t2658399718_0_0_0,
	&GenInst_IComparable_1_t2740917260_0_0_0,
	&GenInst_IEquatable_1_t3113060206_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_EventInfo_t_0_0_0,
	&GenInst__EventInfo_t2430923913_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t2511231167_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t3642518830_0_0_0,
	&GenInst_MethodBase_t904190842_0_0_0,
	&GenInst__MethodBase_t1935530873_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t1567586598_0_0_0,
	&GenInst_ConstructorInfo_t2851816542_0_0_0,
	&GenInst__ConstructorInfo_t3269099341_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_Link_t2723257478_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1744001932_0_0_0,
	&GenInst_TableRange_t2011406615_0_0_0,
	&GenInst_TailoringInfo_t1449609243_0_0_0,
	&GenInst_Contraction_t1673853792_0_0_0,
	&GenInst_Level2Map_t3322505726_0_0_0,
	&GenInst_BigInteger_t925946152_0_0_0,
	&GenInst_UriScheme_t683497865_0_0_0,
	&GenInst_KeySizes_t3144736271_0_0_0,
	&GenInst_Assembly_t4268412390_0_0_0,
	&GenInst__Assembly_t2937922309_0_0_0,
	&GenInst_IEvidenceFactory_t1747265420_0_0_0,
	&GenInst_DateTime_t693205669_0_0_0,
	&GenInst_DateTimeOffset_t1362988906_0_0_0,
	&GenInst_TimeSpan_t3430258949_0_0_0,
	&GenInst_Guid_t2533601593_0_0_0,
	&GenInst_IComparable_1_t1362446645_0_0_0,
	&GenInst_IEquatable_1_t1734589591_0_0_0,
	&GenInst_CustomAttributeData_t3093286891_0_0_0,
	&GenInst_TermInfoStrings_t1425267120_0_0_0,
	&GenInst_Enum_t2459695545_0_0_0,
	&GenInst_Version_t1755874712_0_0_0,
	&GenInst_Slot_t2022531261_0_0_0,
	&GenInst_Slot_t2267560602_0_0_0,
	&GenInst_StackFrame_t2050294881_0_0_0,
	&GenInst_Calendar_t585061108_0_0_0,
	&GenInst_CultureInfo_t3500843524_0_0_0,
	&GenInst_IFormatProvider_t2849799027_0_0_0,
	&GenInst_Module_t4282841206_0_0_0,
	&GenInst__Module_t2144668161_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t94157543_0_0_0,
	&GenInst_Exception_t1927440687_0_0_0,
	&GenInst__Exception_t3026971024_0_0_0,
	&GenInst_ModuleBuilder_t4156028127_0_0_0,
	&GenInst__ModuleBuilder_t1075102050_0_0_0,
	&GenInst_MonoResource_t3127387157_0_0_0,
	&GenInst_RefEmitPermissionSet_t2708608433_0_0_0,
	&GenInst_ParameterBuilder_t3344728474_0_0_0,
	&GenInst__ParameterBuilder_t2251638747_0_0_0,
	&GenInst_TypeU5BU5D_t1664964607_0_0_0,
	&GenInst_Il2CppArray_0_0_0,
	&GenInst_ICollection_t91669223_0_0_0,
	&GenInst_IList_t3321498491_0_0_0,
	&GenInst_LocalBuilder_t2116499186_0_0_0,
	&GenInst__LocalBuilder_t61912499_0_0_0,
	&GenInst_LocalVariableInfo_t1749284021_0_0_0,
	&GenInst_ILTokenInfo_t149559338_0_0_0,
	&GenInst_LabelData_t3712112744_0_0_0,
	&GenInst_LabelFixup_t4090909514_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0,
	&GenInst_TypeBuilder_t3308873219_0_0_0,
	&GenInst__TypeBuilder_t2783404358_0_0_0,
	&GenInst_MethodBuilder_t644187984_0_0_0,
	&GenInst__MethodBuilder_t3932949077_0_0_0,
	&GenInst_ConstructorBuilder_t700974433_0_0_0,
	&GenInst__ConstructorBuilder_t1236878896_0_0_0,
	&GenInst_PropertyBuilder_t3694255912_0_0_0,
	&GenInst__PropertyBuilder_t3341912621_0_0_0,
	&GenInst_FieldBuilder_t2784804005_0_0_0,
	&GenInst__FieldBuilder_t1895266044_0_0_0,
	&GenInst_ResourceInfo_t3933049236_0_0_0,
	&GenInst_ResourceCacheItem_t333236149_0_0_0,
	&GenInst_IContextAttribute_t2439121372_0_0_0,
	&GenInst_IContextProperty_t287246399_0_0_0,
	&GenInst_Header_t2756440555_0_0_0,
	&GenInst_ITrackingHandler_t2759960940_0_0_0,
	&GenInst_IComparable_1_t2525044892_0_0_0,
	&GenInst_IEquatable_1_t2897187838_0_0_0,
	&GenInst_IComparable_1_t2556540300_0_0_0,
	&GenInst_IEquatable_1_t2928683246_0_0_0,
	&GenInst_IComparable_1_t967130876_0_0_0,
	&GenInst_IEquatable_1_t1339273822_0_0_0,
	&GenInst_TypeTag_t141209596_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_KeyContainerPermissionAccessEntry_t41069825_0_0_0,
	&GenInst_StrongName_t2988747270_0_0_0,
	&GenInst_CodeConnectAccess_t3638993531_0_0_0,
	&GenInst_WaitHandle_t677569169_0_0_0,
	&GenInst_IDisposable_t2427283555_0_0_0,
	&GenInst_MarshalByRefObject_t1285298191_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_Attribute_t542643598_0_0_0,
	&GenInst__Attribute_t1557664299_0_0_0,
	&GenInst_PropertyDescriptor_t4250402154_0_0_0,
	&GenInst_MemberDescriptor_t3749827553_0_0_0,
	&GenInst_EventDescriptor_t962731901_0_0_0,
	&GenInst_AttributeU5BU5D_t4255796347_0_0_0,
	&GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0,
	&GenInst_TypeDescriptionProvider_t2438624375_0_0_0,
	&GenInst_Type_t_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_LinkedList_1_t2743332604_0_0_0,
	&GenInst_KeyValuePair_2_t2438035723_0_0_0,
	&GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0,
	&GenInst_WeakObjectWrapper_t2012978780_0_0_0,
	&GenInst_WeakObjectWrapper_t2012978780_0_0_0_LinkedList_1_t2743332604_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3261256129_0_0_0,
	&GenInst_Cookie_t3154017544_0_0_0,
	&GenInst_IPAddress_t1399971723_0_0_0,
	&GenInst_NetworkInterface_t63927633_0_0_0,
	&GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0,
	&GenInst_String_t_0_0_0_LinuxNetworkInterface_t3864470295_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_LinuxNetworkInterface_t3864470295_0_0_0,
	&GenInst_KeyValuePair_2_t3536594779_0_0_0,
	&GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0,
	&GenInst_String_t_0_0_0_MacOsNetworkInterface_t1454185290_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_MacOsNetworkInterface_t1454185290_0_0_0,
	&GenInst_KeyValuePair_2_t1126309774_0_0_0,
	&GenInst_Win32_IP_ADAPTER_ADDRESSES_t680756680_0_0_0,
	&GenInst_X509ChainStatus_t4278378721_0_0_0,
	&GenInst_ArraySegment_1_t2594217482_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3497699202_0_0_0,
	&GenInst_X509Certificate_t283079845_0_0_0,
	&GenInst_IDeserializationCallback_t327125377_0_0_0,
	&GenInst_Capture_t4157900610_0_0_0,
	&GenInst_DynamicMethod_t3307743052_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0,
	&GenInst_KeyValuePair_2_t1008373517_0_0_0,
	&GenInst_Label_t4243202660_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_Label_t4243202660_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Label_t4243202660_0_0_0_KeyValuePair_2_t1008373517_0_0_0,
	&GenInst_Group_t3761430853_0_0_0,
	&GenInst_Mark_t2724874473_0_0_0,
	&GenInst_UriScheme_t1876590943_0_0_0,
	&GenInst_BigInteger_t925946153_0_0_0,
	&GenInst_ByteU5BU5D_t3397334013_0_0_0,
	&GenInst_ClientCertificateType_t4001384466_0_0_0,
	&GenInst_ConfigurationProperty_t2048066811_0_0_0,
	&GenInst_XsdIdentityPath_t2037874_0_0_0,
	&GenInst_XsdIdentityField_t2563516441_0_0_0,
	&GenInst_XsdIdentityStep_t452377251_0_0_0,
	&GenInst_XmlSchemaAttribute_t4015859774_0_0_0,
	&GenInst_XmlSchemaAnnotated_t2082486936_0_0_0,
	&GenInst_XmlSchemaObject_t2050913741_0_0_0,
	&GenInst_XmlSchemaException_t4082200141_0_0_0,
	&GenInst_SystemException_t3877406272_0_0_0,
	&GenInst_KeyValuePair_2_t1430411454_0_0_0,
	&GenInst_String_t_0_0_0_DTDNode_t1758286970_0_0_0,
	&GenInst_DTDNode_t1758286970_0_0_0,
	&GenInst_AttributeSlot_t1499247213_0_0_0,
	&GenInst_Entry_t2583369454_0_0_0,
	&GenInst_XmlNode_t616554813_0_0_0,
	&GenInst_IXPathNavigable_t845515791_0_0_0,
	&GenInst_NsDecl_t3210081295_0_0_0,
	&GenInst_NsScope_t2513625351_0_0_0,
	&GenInst_XmlAttributeTokenInfo_t3353594030_0_0_0,
	&GenInst_XmlTokenInfo_t254587324_0_0_0,
	&GenInst_TagName_t2340974457_0_0_0,
	&GenInst_XmlNodeInfo_t3709371029_0_0_0,
	&GenInst_XmlAttribute_t175731005_0_0_0,
	&GenInst_IHasXmlChildNode_t2048545686_0_0_0,
	&GenInst_XmlQualifiedName_t1944712516_0_0_0,
	&GenInst_Regex_t1803876613_0_0_0,
	&GenInst_XmlSchemaSimpleType_t248156492_0_0_0,
	&GenInst_XmlSchemaType_t1795078578_0_0_0,
	&GenInst_Link_t865133271_0_0_0,
	&GenInst_Il2CppObject_0_0_0_List_1_t2058570427_0_0_0,
	&GenInst_Il2CppObject_0_0_0_List_1_t2058570427_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_List_1_t2058570427_0_0_0,
	&GenInst_KeyValuePair_2_t3702943074_0_0_0,
	&GenInst_IGrouping_2_t906937361_0_0_0,
	&GenInst_Il2CppObject_0_0_0_IEnumerable_1_t2981576340_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1436312919_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Int64_t909078037_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1436312919_0_0_0,
	&GenInst_WeakReference_t1077405567_0_0_0,
	&GenInst_Task_1_t1809478302_0_0_0,
	&GenInst_Task_t1843236107_0_0_0,
	&GenInst_Action_1_t1645035489_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t818741072_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t818741072_0_0_0,
	&GenInst_Action_t3226471752_0_0_0,
	&GenInst_Task_t1843236107_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Task_t1843236107_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Task_1_t1191906455_0_0_0,
	&GenInst_Task_1_t1809478302_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Object_t1021602117_0_0_0,
	&GenInst_Camera_t189460977_0_0_0,
	&GenInst_Behaviour_t955675639_0_0_0,
	&GenInst_Component_t3819376471_0_0_0,
	&GenInst_Display_t3666191348_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Il2CppObject_0_0_0,
	&GenInst_AchievementDescription_t3110978151_0_0_0,
	&GenInst_IAchievementDescription_t3498529102_0_0_0,
	&GenInst_UserProfile_t3365630962_0_0_0,
	&GenInst_IUserProfile_t4108565527_0_0_0,
	&GenInst_GcLeaderboard_t453887929_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0,
	&GenInst_IAchievementU5BU5D_t2709554645_0_0_0,
	&GenInst_IAchievement_t1752291260_0_0_0,
	&GenInst_GcAchievementData_t1754866149_0_0_0,
	&GenInst_Achievement_t1333316625_0_0_0,
	&GenInst_IScoreU5BU5D_t3237304636_0_0_0,
	&GenInst_IScore_t513966369_0_0_0,
	&GenInst_GcScoreData_t3676783238_0_0_0,
	&GenInst_Score_t2307748940_0_0_0,
	&GenInst_IUserProfileU5BU5D_t3461248430_0_0_0,
	&GenInst_Plane_t3727654732_0_0_0,
	&GenInst_Keyframe_t1449471340_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0,
	&GenInst_Vector4_t2243707581_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0,
	&GenInst_Color32_t874517518_0_0_0,
	&GenInst_Color_t2020392075_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1701344717_0_0_0,
	&GenInst_Playable_t3667545548_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0,
	&GenInst_ContactPoint_t1376425630_0_0_0,
	&GenInst_RaycastHit_t87180320_0_0_0,
	&GenInst_Rigidbody2D_t502193897_0_0_0,
	&GenInst_RaycastHit2D_t4063908774_0_0_0,
	&GenInst_ContactPoint2D_t3659330976_0_0_0,
	&GenInst_UIVertex_t1204258818_0_0_0,
	&GenInst_UICharInfo_t3056636800_0_0_0,
	&GenInst_UILineInfo_t3621277874_0_0_0,
	&GenInst_Font_t4239498691_0_0_0,
	&GenInst_GUILayoutOption_t4183744904_0_0_0,
	&GenInst_GUILayoutEntry_t3828586629_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_LayoutCache_t3120781045_0_0_0,
	&GenInst_KeyValuePair_2_t4180919198_0_0_0,
	&GenInst_GUIStyle_t1799908754_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1472033238_0_0_0,
	&GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_KeyValuePair_2_t488203048_0_0_0,
	&GenInst_TextEditOp_t3138797698_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_TextEditOp_t3138797698_0_0_0_KeyValuePair_2_t488203048_0_0_0,
	&GenInst_Event_t3028476042_0_0_0,
	&GenInst_Event_t3028476042_0_0_0_TextEditOp_t3138797698_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3799506081_0_0_0,
	&GenInst_DisallowMultipleComponent_t2656950_0_0_0,
	&GenInst_ExecuteInEditMode_t3043633143_0_0_0,
	&GenInst_RequireComponent_t864575032_0_0_0,
	&GenInst_HitInfo_t1761367055_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PersistentCall_t3793436469_0_0_0,
	&GenInst_BaseInvokableCall_t2229564840_0_0_0,
	&GenInst_String_t_0_0_0_FirebaseApp_t210707726_0_0_0,
	&GenInst_String_t_0_0_0_FirebaseApp_t210707726_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_FirebaseApp_t210707726_0_0_0,
	&GenInst_KeyValuePair_2_t4177799506_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FirebaseApp_t210707726_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t888819835_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_IntPtr_t_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t888819835_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FirebaseApp_t210707726_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2705045562_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t271247988_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_IntPtr_t_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t271247988_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Action_t3062064994_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Action_t3062064994_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Action_t3062064994_0_0_0,
	&GenInst_KeyValuePair_2_t4122203147_0_0_0,
	&GenInst_Task_1_t1149249240_0_0_0,
	&GenInst_Tuple_2_t460172552_0_0_0,
	&GenInst_SendOrPostCallback_t296893742_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_ManualResetEvent_t926074657_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_ManualResetEvent_t926074657_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ManualResetEvent_t926074657_0_0_0,
	&GenInst_KeyValuePair_2_t1986212810_0_0_0,
	&GenInst_IEnumerator_t1466026749_0_0_0,
	&GenInst_FirebaseApp_t210707726_0_0_0_X509CertificateCollection_t1197680765_0_0_0,
	&GenInst_FirebaseApp_t210707726_0_0_0_X509CertificateCollection_t1197680765_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_X509CertificateCollection_t1197680765_0_0_0,
	&GenInst_KeyValuePair_2_t3716606344_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t3943999495_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t3943999495_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Dictionary_2_t3943999495_0_0_0,
	&GenInst_KeyValuePair_2_t709170352_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t309261261_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0,
	&GenInst_String_t_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2361573779_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t309261261_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Dictionary_2_t309261261_0_0_0,
	&GenInst_KeyValuePair_2_t1369399414_0_0_0,
	&GenInst_Runnable_t1446984663_0_0_0,
	&GenInst_Thread_t1322377586_0_0_0,
	&GenInst_IScheduledITask_t3129349074_0_0_0,
	&GenInst_IScheduledITask_t3129349074_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_SHA1Managed_t7268864_0_0_0,
	&GenInst_MD5CryptoServiceProvider_t4009738925_0_0_0,
	&GenInst_List_1_t61287617_0_0_0,
	&GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0,
	&GenInst_DispatcherKey_t708950850_0_0_0,
	&GenInst_DispatcherKey_t708950850_0_0_0_Dispatcher_t2240407071_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Dispatcher_t2240407071_0_0_0,
	&GenInst_KeyValuePair_2_t3846770086_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FirebaseAuth_t3105883899_0_0_0,
	&GenInst_IntPtr_t_0_0_0_FirebaseAuth_t3105883899_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_FirebaseAuth_t3105883899_0_0_0,
	&GenInst_KeyValuePair_2_t1305254439_0_0_0,
	&GenInst_FirebaseUser_t4046966602_0_0_0,
	&GenInst_Task_1_t3166995609_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Action_t1614918345_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Action_t1614918345_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Action_t1614918345_0_0_0,
	&GenInst_KeyValuePair_2_t2675056498_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_Nullable_1_t334943763_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Nullable_1_t334943763_0_0_0,
	&GenInst_KeyValuePair_2_t1979316409_0_0_0,
	&GenInst_Nullable_1_t334943763_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3089358386_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Nullable_1_t334943763_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Nullable_1_t334943763_0_0_0_Nullable_1_t334943763_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Nullable_1_t334943763_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Nullable_1_t334943763_0_0_0_KeyValuePair_2_t1979316409_0_0_0,
	&GenInst_String_t_0_0_0_Nullable_1_t334943763_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t7068247_0_0_0,
	&GenInst_Task_1_t1809478302_0_0_0_CompletionListener_t93014473_0_0_0,
	&GenInst_ValueChangedEventArgs_t929877234_0_0_0,
	&GenInst_DataSnapshot_t1287895350_0_0_0,
	&GenInst_Task_1_t407924357_0_0_0,
	&GenInst_LlrbValueNode_2_t1296857666_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_BooleanChunk_t3036858313_0_0_0,
	&GenInst_IList_1_t2570160834_0_0_0,
	&GenInst_RangeMerge_t98025219_0_0_0,
	&GenInst_ListenQuerySpec_t2050960365_0_0_0_OutstandingListen_t67058648_0_0_0,
	&GenInst_ListenQuerySpec_t2050960365_0_0_0,
	&GenInst_OutstandingListen_t67058648_0_0_0,
	&GenInst_OutstandingDisconnect_t3017763353_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_OutstandingPut_t774816424_0_0_0,
	&GenInst_OutstandingPut_t774816424_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_IConnectionRequestCallback_t2235793546_0_0_0,
	&GenInst_IConnectionRequestCallback_t2235793546_0_0_0,
	&GenInst_ListenQuerySpec_t2050960365_0_0_0_OutstandingListen_t67058648_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3990354856_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_IConnectionRequestCallback_t2235793546_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t982657170_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_OutstandingPut_t774816424_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3816647344_0_0_0,
	&GenInst_Link_t3379729309_0_0_0,
	&GenInst_IDictionary_2_t2603311978_0_0_0,
	&GenInst_KeyValuePair_2_t4001878204_0_0_0,
	&GenInst_Path_t2568473163_0_0_0_Node_t2640059010_0_0_0,
	&GenInst_Node_t2640059010_0_0_0,
	&GenInst_KeyValuePair_2_t4051268489_0_0_0,
	&GenInst_Path_t2568473163_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ChildKey_t1197802383_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1211005109_0_0_0,
	&GenInst_ChildKey_t1197802383_0_0_0,
	&GenInst_IComparable_1_t3029641606_0_0_0,
	&GenInst_Node_t2640059010_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Path_t2568473163_0_0_0,
	&GenInst_Node_t2640059010_0_0_0_CompoundWrite_t496419158_0_0_0,
	&GenInst_NamedNode_t787885785_0_0_0,
	&GenInst_ChildKey_t1197802383_0_0_0_CompoundWrite_t496419158_0_0_0,
	&GenInst_CompoundWrite_t496419158_0_0_0,
	&GenInst_ChildKey_t1197802383_0_0_0_CompoundWrite_t496419158_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3312942268_0_0_0,
	&GenInst_IList_1_t2684453066_0_0_0,
	&GenInst_TransactionData_t2143512465_0_0_0,
	&GenInst_ChildKey_t1197802383_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Event_t732806402_0_0_0,
	&GenInst_Path_t2568473163_0_0_0_Node_t2640059010_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_RangeMerge_t2434126241_0_0_0,
	&GenInst_UserWriteRecord_t388677579_0_0_0,
	&GenInst_DataEvent_t3330191602_0_0_0,
	&GenInst_Context_t3486154757_0_0_0_IDictionary_2_t1158171145_0_0_0,
	&GenInst_String_t_0_0_0_Repo_t1244308462_0_0_0,
	&GenInst_Repo_t1244308462_0_0_0,
	&GenInst_Context_t3486154757_0_0_0,
	&GenInst_IDictionary_2_t1158171145_0_0_0,
	&GenInst_Context_t3486154757_0_0_0_IDictionary_2_t1158171145_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1203317345_0_0_0,
	&GenInst_String_t_0_0_0_Repo_t1244308462_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t916432946_0_0_0,
	&GenInst_ChildKey_t1197802383_0_0_0_SparseSnapshotTree_t504080338_0_0_0,
	&GenInst_SparseSnapshotTree_t504080338_0_0_0,
	&GenInst_ChildKey_t1197802383_0_0_0_SparseSnapshotTree_t504080338_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3320603448_0_0_0,
	&GenInst_QueryParams_t526937568_0_0_0_View_t798282663_0_0_0,
	&GenInst_QueryParams_t526937568_0_0_0,
	&GenInst_View_t798282663_0_0_0,
	&GenInst_QueryParams_t526937568_0_0_0_View_t798282663_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2059584600_0_0_0,
	&GenInst_Change_t639587248_0_0_0,
	&GenInst_IList_1_t918499312_0_0_0_IList_1_t1273747003_0_0_0,
	&GenInst_QuerySpec_t377558711_0_0_0,
	&GenInst_QuerySpec_t377558711_0_0_0_Tag_t2439924210_0_0_0,
	&GenInst_Tag_t2439924210_0_0_0,
	&GenInst_Tag_t2439924210_0_0_0_QuerySpec_t377558711_0_0_0,
	&GenInst_SyncPoint_t2720557329_0_0_0,
	&GenInst_KeyValuePair_2_t4082376523_0_0_0,
	&GenInst_Path_t2568473163_0_0_0_SyncPoint_t2720557329_0_0_0,
	&GenInst_SyncPoint_t2720557329_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Tag_t2439924210_0_0_0_QuerySpec_t377558711_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2206838478_0_0_0,
	&GenInst_QuerySpec_t377558711_0_0_0_Tag_t2439924210_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1065531536_0_0_0,
	&GenInst_IList_1_t1273747003_0_0_0,
	&GenInst_IList_1_t3871132203_0_0_0,
	&GenInst_Nullable_1_t2088641033_0_0_0,
	&GenInst_KeyValuePair_2_t3450460227_0_0_0,
	&GenInst_Path_t2568473163_0_0_0_Nullable_1_t2088641033_0_0_0,
	&GenInst_KeyValuePair_2_t3733013679_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Nullable_1_t2088641033_0_0_0,
	&GenInst_Nullable_1_t2088641033_0_0_0_Il2CppObject_0_0_0,
	&GenInst_EventRegistration_t4222917807_0_0_0_IList_1_t468891112_0_0_0,
	&GenInst_EventRegistration_t4222917807_0_0_0,
	&GenInst_EventRegistration_t4222917807_0_0_0_IList_1_t468891112_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IList_1_t468891112_0_0_0,
	&GenInst_KeyValuePair_2_t1813098414_0_0_0,
	&GenInst_ViewFrom_t2537511179_0_0_0,
	&GenInst_ChildKey_t1197802383_0_0_0_Change_t639587248_0_0_0,
	&GenInst_ChildKey_t1197802383_0_0_0_Change_t639587248_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3456110358_0_0_0,
	&GenInst_ChildKey_t1197802383_0_0_0_Node_t2640059010_0_0_0,
	&GenInst_KeyValuePair_2_t1161614824_0_0_0,
	&GenInst_NamedNode_t787885785_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ChildKey_t1197802383_0_0_0_Node_t2640059010_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_MemoryStream_t743994179_0_0_0,
	&GenInst_FirebaseApp_t210707726_0_0_0_DotNetPlatform_t2951135975_0_0_0,
	&GenInst_String_t_0_0_0_FirebaseDatabase_t1318758358_0_0_0,
	&GenInst_FirebaseDatabase_t1318758358_0_0_0,
	&GenInst_String_t_0_0_0_FirebaseDatabase_t1318758358_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t990882842_0_0_0,
	&GenInst_Task_1_t1149249240_0_0_0_String_t_0_0_0,
	&GenInst_ArrayList_t4252133567_0_0_0,
	&GenInst_String_t_0_0_0_MemberInfo_t_0_0_0,
	&GenInst_String_t_0_0_0_MemberInfo_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3715221744_0_0_0,
	&GenInst_JsonConverter_t4092422604_0_0_0,
	&GenInst_Type_t_0_0_0_Dictionary_2_t1662909226_0_0_0,
	&GenInst_Type_t_0_0_0_Dictionary_2_t1662909226_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Dictionary_2_t1662909226_0_0_0,
	&GenInst_KeyValuePair_2_t1357612345_0_0_0,
	&GenInst_BaseInputModule_t1295781545_0_0_0,
	&GenInst_RaycastResult_t21186376_0_0_0,
	&GenInst_IDeselectHandler_t3182198310_0_0_0,
	&GenInst_IEventSystemHandler_t2741188318_0_0_0,
	&GenInst_List_1_t2110309450_0_0_0,
	&GenInst_List_1_t3188497603_0_0_0,
	&GenInst_ISelectHandler_t2812555161_0_0_0,
	&GenInst_BaseRaycaster_t2336171397_0_0_0,
	&GenInst_Entry_t3365010046_0_0_0,
	&GenInst_BaseEventData_t2681005625_0_0_0,
	&GenInst_IPointerEnterHandler_t193164956_0_0_0,
	&GenInst_IPointerExitHandler_t461019860_0_0_0,
	&GenInst_IPointerDownHandler_t3929046918_0_0_0,
	&GenInst_IPointerUpHandler_t1847764461_0_0_0,
	&GenInst_IPointerClickHandler_t96169666_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0,
	&GenInst_IBeginDragHandler_t3135127860_0_0_0,
	&GenInst_IDragHandler_t2583993319_0_0_0,
	&GenInst_IEndDragHandler_t1349123600_0_0_0,
	&GenInst_IDropHandler_t2390101210_0_0_0,
	&GenInst_IScrollHandler_t3834677510_0_0_0,
	&GenInst_IUpdateSelectedHandler_t3778909353_0_0_0,
	&GenInst_IMoveHandler_t2611925506_0_0_0,
	&GenInst_ISubmitHandler_t525803901_0_0_0,
	&GenInst_ICancelHandler_t1980319651_0_0_0,
	&GenInst_Transform_t3275118058_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0,
	&GenInst_BaseInput_t621514313_0_0_0,
	&GenInst_UIBehaviour_t3960014691_0_0_0,
	&GenInst_MonoBehaviour_t1158329972_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_PointerEventData_t1599784723_0_0_0,
	&GenInst_KeyValuePair_2_t2659922876_0_0_0,
	&GenInst_ButtonState_t2688375492_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ColorBlock_t2652774230_0_0_0,
	&GenInst_OptionData_t2420267500_0_0_0,
	&GenInst_DropdownItem_t4139978805_0_0_0,
	&GenInst_FloatTween_t2986189219_0_0_0,
	&GenInst_Sprite_t309593783_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0,
	&GenInst_List_1_t3873494194_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0,
	&GenInst_Text_t356221433_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_HashSet_1_t2984649583_0_0_0,
	&GenInst_KeyValuePair_2_t850112849_0_0_0,
	&GenInst_ColorTween_t3438117476_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IndexedSet_1_t286373651_0_0_0,
	&GenInst_KeyValuePair_2_t2391682566_0_0_0,
	&GenInst_KeyValuePair_2_t3010968081_0_0_0,
	&GenInst_KeyValuePair_2_t1912381698_0_0_0,
	&GenInst_Type_t3352948571_0_0_0,
	&GenInst_FillMethod_t1640962579_0_0_0,
	&GenInst_ContentType_t1028629049_0_0_0,
	&GenInst_LineType_t2931319356_0_0_0,
	&GenInst_InputType_t1274231802_0_0_0,
	&GenInst_TouchScreenKeyboardType_t875112366_0_0_0,
	&GenInst_CharacterValidation_t3437478890_0_0_0,
	&GenInst_Mask_t2977958238_0_0_0,
	&GenInst_List_1_t2347079370_0_0_0,
	&GenInst_RectMask2D_t1156185964_0_0_0,
	&GenInst_List_1_t525307096_0_0_0,
	&GenInst_Navigation_t1571958496_0_0_0,
	&GenInst_IClippable_t1941276057_0_0_0,
	&GenInst_Direction_t3696775921_0_0_0,
	&GenInst_Selectable_t1490392188_0_0_0,
	&GenInst_Transition_t605142169_0_0_0,
	&GenInst_SpriteState_t1353336012_0_0_0,
	&GenInst_CanvasGroup_t3296560743_0_0_0,
	&GenInst_Direction_t1525323322_0_0_0,
	&GenInst_MatEntry_t3157325053_0_0_0,
	&GenInst_Toggle_t3976754468_0_0_0,
	&GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t379984643_0_0_0,
	&GenInst_AspectMode_t1166448724_0_0_0,
	&GenInst_FitMode_t4030874534_0_0_0,
	&GenInst_RectTransform_t3349966182_0_0_0,
	&GenInst_LayoutRebuilder_t2155218138_0_0_0,
	&GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_List_1_t1612828712_0_0_0,
	&GenInst_List_1_t243638650_0_0_0,
	&GenInst_List_1_t1612828711_0_0_0,
	&GenInst_List_1_t1612828713_0_0_0,
	&GenInst_List_1_t1440998580_0_0_0,
	&GenInst_List_1_t573379950_0_0_0,
	&GenInst_PubnubClientError_t110317921_0_0_0,
	&GenInst_ChannelEntity_t3266154606_0_0_0,
	&GenInst_PNMessageResult_t756120242_0_0_0,
	&GenInst_ChannelIdentity_t1147162267_0_0_0_ChannelParameters_t547936593_0_0_0,
	&GenInst_ChannelIdentity_t1147162267_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1381843961_0_0_0,
	&GenInst_ChannelIdentity_t1147162267_0_0_0,
	&GenInst_ChannelIdentity_t1147162267_0_0_0_Il2CppObject_0_0_0_ChannelIdentity_t1147162267_0_0_0,
	&GenInst_ChannelIdentity_t1147162267_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ChannelIdentity_t1147162267_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ChannelIdentity_t1147162267_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t1381843961_0_0_0,
	&GenInst_ChannelIdentity_t1147162267_0_0_0_ChannelParameters_t547936593_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ChannelIdentity_t1147162267_0_0_0_ChannelParameters_t547936593_0_0_0_ChannelParameters_t547936593_0_0_0,
	&GenInst_ChannelParameters_t547936593_0_0_0,
	&GenInst_KeyValuePair_2_t3535298555_0_0_0,
	&GenInst_EventArgs_t3289624707_0_0_0,
	&GenInst_SubscribeMessage_t3739430639_0_0_0,
	&GenInst_StringU5BU5D_t1642385972_0_0_0,
	&GenInst_Int32U5BU5D_t3030399641_0_0_0,
	&GenInst_Int64U5BU5D_t717125112_0_0_0,
	&GenInst_Int16U5BU5D_t3104283263_0_0_0,
	&GenInst_UInt16U5BU5D_t2527266722_0_0_0,
	&GenInst_UInt64U5BU5D_t1668688775_0_0_0,
	&GenInst_UInt32U5BU5D_t59386216_0_0_0,
	&GenInst_DecimalU5BU5D_t624008824_0_0_0,
	&GenInst_DoubleU5BU5D_t1889952540_0_0_0,
	&GenInst_BooleanU5BU5D_t3568034315_0_0_0,
	&GenInst_ObjectU5BU5D_t3614634134_0_0_0,
	&GenInst_SingleU5BU5D_t577127397_0_0_0,
	&GenInst_IGrouping_2_t2569427433_0_0_0,
	&GenInst_IGrouping_2_t2569427433_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_CurrentRequestType_t3250227986_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3104328646_0_0_0,
	&GenInst_CurrentRequestType_t3250227986_0_0_0,
	&GenInst_CurrentRequestType_t3250227986_0_0_0_Il2CppObject_0_0_0_CurrentRequestType_t3250227986_0_0_0,
	&GenInst_CurrentRequestType_t3250227986_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_CurrentRequestType_t3250227986_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_CurrentRequestType_t3250227986_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3104328646_0_0_0,
	&GenInst_IDictionary_t596158605_0_0_0,
	&GenInst_String_t_0_0_0_Int64_t909078037_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0,
	&GenInst_KeyValuePair_2_t2553450683_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int64_t909078037_0_0_0_KeyValuePair_2_t2553450683_0_0_0,
	&GenInst_String_t_0_0_0_Int64_t909078037_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t581202521_0_0_0,
	&GenInst_Object_t1021602117_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_AssertionComponent_t3962419315_0_0_0,
	&GenInst_IAssertionComponentConfigurator_t3050464039_0_0_0,
	&GenInst_FieldInfo_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Bounds_t3033363703_0_0_0,
	&GenInst_Bounds_t3033363703_0_0_0_Bounds_t3033363703_0_0_0,
	&GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_Renderer_t257310565_0_0_0_Camera_t189460977_0_0_0,
	&GenInst_Transform_t3275118058_0_0_0_Transform_t3275118058_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0,
	&GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0,
	&GenInst_RuntimePlatform_t1869584967_0_0_0,
	&GenInst_TestComponent_t2516511601_0_0_0,
	&GenInst_TestResult_t490498461_0_0_0,
	&GenInst_ITestComponent_t2920761518_0_0_0,
	&GenInst_RuntimePlatform_t1869584967_0_0_0_String_t_0_0_0,
	&GenInst_RuntimePlatform_t1869584967_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Type_t_0_0_0_String_t_0_0_0,
	&GenInst_ITestComponent_t2920761518_0_0_0_HashSet_1_t1254222372_0_0_0,
	&GenInst_ITestComponent_t2920761518_0_0_0_HashSet_1_t1254222372_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_HashSet_1_t1254222372_0_0_0,
	&GenInst_KeyValuePair_2_t2166842639_0_0_0,
	&GenInst_ITestComponent_t2920761518_0_0_0_ITestComponent_t2920761518_0_0_0,
	&GenInst_Assembly_t4268412390_0_0_0_IEnumerable_1_t1595930271_0_0_0,
	&GenInst_TestComponent_t2516511601_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Type_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t2625464602_0_0_0,
	&GenInst_ITestResult_t3256343470_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t2625464602_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_List_1_t2625464602_0_0_0,
	&GenInst_KeyValuePair_2_t2297589086_0_0_0,
	&GenInst_KeyValuePair_2_t2297589086_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ITestResult_t3256343470_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_ITestResult_t3256343470_0_0_0_String_t_0_0_0,
	&GenInst_TestComponent_t2516511601_0_0_0_TestResult_t490498461_0_0_0,
	&GenInst_TestResult_t490498461_0_0_0_ITestComponent_t2920761518_0_0_0,
	&GenInst_TestResult_t490498461_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_TestResult_t490498461_0_0_0_String_t_0_0_0,
	&GenInst_Component_t3819376471_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_AssertionComponent_t3962419315_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_ITestRunnerCallback_t327193412_0_0_0,
	&GenInst_ExplosionPart_t2473625634_0_0_0,
	&GenInst_WaitForSeconds_t3839502067_0_0_0,
	&GenInst_SelfIlluminationBlink_t1392230251_0_0_0,
	&GenInst_MoveAnimation_t3061523661_0_0_0,
	&GenInst_ObjectCache_t960934699_0_0_0,
	&GenInst_ReceiverItem_t169526838_0_0_0,
	&GenInst_Health_t2683907638_0_0_0,
	&GenInst_Joystick_t549888914_0_0_0,
	&GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2949663298_gp_0_0_0_0,
	&GenInst_Array_Sort_m1730553742_gp_0_0_0_0_Array_Sort_m1730553742_gp_0_0_0_0,
	&GenInst_Array_Sort_m3106198730_gp_0_0_0_0_Array_Sort_m3106198730_gp_1_0_0_0,
	&GenInst_Array_Sort_m2090966156_gp_0_0_0_0,
	&GenInst_Array_Sort_m2090966156_gp_0_0_0_0_Array_Sort_m2090966156_gp_0_0_0_0,
	&GenInst_Array_Sort_m1985772939_gp_0_0_0_0,
	&GenInst_Array_Sort_m1985772939_gp_0_0_0_0_Array_Sort_m1985772939_gp_1_0_0_0,
	&GenInst_Array_Sort_m2736815140_gp_0_0_0_0_Array_Sort_m2736815140_gp_0_0_0_0,
	&GenInst_Array_Sort_m2468799988_gp_0_0_0_0_Array_Sort_m2468799988_gp_1_0_0_0,
	&GenInst_Array_Sort_m2587948790_gp_0_0_0_0,
	&GenInst_Array_Sort_m2587948790_gp_0_0_0_0_Array_Sort_m2587948790_gp_0_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_0_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_1_0_0_0,
	&GenInst_Array_Sort_m1279015767_gp_0_0_0_0_Array_Sort_m1279015767_gp_1_0_0_0,
	&GenInst_Array_Sort_m52621935_gp_0_0_0_0,
	&GenInst_Array_Sort_m3546416104_gp_0_0_0_0,
	&GenInst_Array_qsort_m533480027_gp_0_0_0_0,
	&GenInst_Array_qsort_m533480027_gp_0_0_0_0_Array_qsort_m533480027_gp_1_0_0_0,
	&GenInst_Array_compare_m940423571_gp_0_0_0_0,
	&GenInst_Array_qsort_m565008110_gp_0_0_0_0,
	&GenInst_Array_Resize_m1201602141_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m2783802133_gp_0_0_0_0,
	&GenInst_Array_ForEach_m3775633118_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m1734974082_gp_0_0_0_0_Array_ConvertAll_m1734974082_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m934773128_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m3202023711_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m352384762_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1593955424_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1546138173_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1082322798_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m525402987_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3577113407_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1033585031_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3052238307_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1306290405_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m2825795862_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m2841140625_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3304283431_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3860096562_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m2100440379_gp_0_0_0_0,
	&GenInst_Array_FindAll_m982349212_gp_0_0_0_0,
	&GenInst_Array_Exists_m1825464757_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m1258056624_gp_0_0_0_0,
	&GenInst_Array_Find_m2529971459_gp_0_0_0_0,
	&GenInst_Array_FindLast_m3929249453_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0,
	&GenInst_IList_1_t3737699284_gp_0_0_0_0,
	&GenInst_ICollection_1_t1552160836_gp_0_0_0_0,
	&GenInst_Nullable_1_t1398937014_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m4157835592_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m4157835592_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m1249362843_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_1_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m404672235_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m3599098477_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0,
	&GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3434615342_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0,
	&GenInst_Enumerator_t83320710_gp_0_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0,
	&GenInst_Enumerator_t3111723616_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_0_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4174120762_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0,
	&GenInst_DefaultComparer_t1766400012_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0,
	&GenInst_List_1_t1169184319_gp_0_0_0_0,
	&GenInst_Enumerator_t1292967705_gp_0_0_0_0,
	&GenInst_Collection_1_t686054069_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0,
	&GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0,
	&GenInst_Comparer_1_t1036860714_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3074655092_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0,
	&GenInst_LinkedList_1_t3556217344_gp_0_0_0_0,
	&GenInst_Enumerator_t4145643798_gp_0_0_0_0,
	&GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0,
	&GenInst_Queue_1_t1458930734_gp_0_0_0_0,
	&GenInst_Enumerator_t4000919638_gp_0_0_0_0,
	&GenInst_Stack_1_t4016656541_gp_0_0_0_0,
	&GenInst_Enumerator_t546412149_gp_0_0_0_0,
	&GenInst_Enumerable_All_m2363499768_gp_0_0_0_0,
	&GenInst_Enumerable_All_m2363499768_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_Any_m665396702_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m2739389357_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_Cast_m2974870241_gp_0_0_0_0,
	&GenInst_Enumerable_CreateCastIterator_m3836917259_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m1284016302_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m4622279_gp_0_0_0_0,
	&GenInst_Enumerable_Count_m1561720045_gp_0_0_0_0,
	&GenInst_Enumerable_Count_m136242780_gp_0_0_0_0,
	&GenInst_Enumerable_Count_m136242780_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_Distinct_m4171187007_gp_0_0_0_0,
	&GenInst_Enumerable_Distinct_m522457978_gp_0_0_0_0,
	&GenInst_Enumerable_CreateDistinctIterator_m503395888_gp_0_0_0_0,
	&GenInst_Enumerable_ElementAt_m258442918_gp_0_0_0_0,
	&GenInst_Enumerable_ElementAt_m714932326_gp_0_0_0_0,
	&GenInst_Enumerable_First_m4120844597_gp_0_0_0_0,
	&GenInst_Enumerable_First_m4120844597_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_First_m1693250038_gp_0_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m1924765929_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0_List_1_t2888087137_0_0_0,
	&GenInst_Enumerable_ContainsGroup_m1146262421_gp_1_0_0_0,
	&GenInst_Enumerable_ContainsGroup_m1146262421_gp_0_0_0_0,
	&GenInst_Enumerable_GroupBy_m1544473094_gp_0_0_0_0,
	&GenInst_Enumerable_GroupBy_m1544473094_gp_0_0_0_0_Enumerable_GroupBy_m1544473094_gp_1_0_0_0,
	&GenInst_IGrouping_2_t3129334088_0_0_0,
	&GenInst_Enumerable_GroupBy_m1544473094_gp_1_0_0_0_Enumerable_GroupBy_m1544473094_gp_0_0_0_0,
	&GenInst_Enumerable_GroupBy_m1259221415_gp_0_0_0_0,
	&GenInst_Enumerable_GroupBy_m1259221415_gp_0_0_0_0_Enumerable_GroupBy_m1259221415_gp_1_0_0_0,
	&GenInst_Enumerable_GroupBy_m1259221415_gp_1_0_0_0,
	&GenInst_IGrouping_2_t3129334089_0_0_0,
	&GenInst_Enumerable_GroupBy_m1259221415_gp_1_0_0_0_Enumerable_GroupBy_m1259221415_gp_0_0_0_0,
	&GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0,
	&GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0,
	&GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0,
	&GenInst_IGrouping_2_t2588622000_0_0_0,
	&GenInst_Enumerable_CreateGroupByIterator_m1843252285_gp_1_0_0_0_Enumerable_CreateGroupByIterator_m1843252285_gp_0_0_0_0,
	&GenInst_Enumerable_OfType_m192007909_gp_0_0_0_0,
	&GenInst_Enumerable_CreateOfTypeIterator_m1172444379_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m920500904_gp_0_0_0_0_Enumerable_OrderBy_m920500904_gp_1_0_0_0,
	&GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m4222740363_gp_0_0_0_0_Enumerable_OrderBy_m4222740363_gp_1_0_0_0,
	&GenInst_Enumerable_OrderBy_m4222740363_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_0_0_0_0_Enumerable_Select_m2459603006_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m2459603006_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_0_0_0_0_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m990489692_gp_1_0_0_0,
	&GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0,
	&GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0_IEnumerable_1_t380606227_0_0_0,
	&GenInst_Enumerable_SelectMany_m608128666_gp_1_0_0_0,
	&GenInst_Enumerable_SelectMany_m608128666_gp_0_0_0_0_Enumerable_SelectMany_m608128666_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0_IEnumerable_1_t567845041_0_0_0,
	&GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectManyIterator_m869034500_gp_0_0_0_0_Enumerable_CreateSelectManyIterator_m869034500_gp_1_0_0_0,
	&GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0,
	&GenInst_Enumerable_Single_m1979188101_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_Single_m2156153962_gp_0_0_0_0,
	&GenInst_Enumerable_Single_m2776017211_gp_0_0_0_0,
	&GenInst_Enumerable_Single_m2776017211_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_Sum_m3284247776_gp_0_0_0_0,
	&GenInst_Enumerable_Sum_m3284247776_gp_0_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Enumerable_ToArray_m2343256994_gp_0_0_0_0,
	&GenInst_Enumerable_ToList_m261161385_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2409552823_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m2714912225_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t54777913_gp_0_0_0_0,
	&GenInst_U3CCreateDistinctIteratorU3Ec__Iterator3_1_t3711089181_gp_0_0_0_0,
	&GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0,
	&GenInst_IGrouping_2_t1824977620_0_0_0,
	&GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0_List_1_t3797843219_0_0_0,
	&GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0,
	&GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_0_0_0_0_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0,
	&GenInst_U3CCreateGroupByIteratorU3Ec__Iterator5_2_t3781089638_gp_1_0_0_0,
	&GenInst_U3CCreateOfTypeIteratorU3Ec__IteratorC_1_t3057470804_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0,
	&GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0,
	&GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0,
	&GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0_IEnumerable_1_t3792789022_0_0_0,
	&GenInst_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_0_0_0_0_U3CCreateSelectManyIteratorU3Ec__Iterator12_2_t1186455359_gp_1_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Grouping_2_t288249757_gp_1_0_0_0,
	&GenInst_Grouping_2_t288249757_gp_0_0_0_0_Grouping_2_t288249757_gp_1_0_0_0,
	&GenInst_IGrouping_2_t845301090_gp_1_0_0_0,
	&GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0,
	&GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0,
	&GenInst_QuickSort_1_t1290221672_gp_0_0_0_0,
	&GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0,
	&GenInst_SortContext_1_t4088581714_gp_0_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0,
	&GenInst_HashSet_1_t2624254809_gp_0_0_0_0,
	&GenInst_Enumerator_t2109956843_gp_0_0_0_0,
	&GenInst_PrimeHelper_t3424417428_gp_0_0_0_0,
	&GenInst_Tuple_2_t1951933832_gp_0_0_0_0_Tuple_2_t1951933832_gp_1_0_0_0,
	&GenInst_ThreadLocal_1_t1818133092_gp_0_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_ThreadLocal_1_t1818133092_gp_0_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ThreadLocal_1_t1818133092_gp_0_0_0_0,
	&GenInst_TaskCompletionSource_1_t1185916947_gp_0_0_0_0,
	&GenInst_Task_t1843236107_0_0_0_Task_ContinueWith_m340847350_gp_0_0_0_0,
	&GenInst_Task_ContinueWith_m340847350_gp_0_0_0_0,
	&GenInst_Task_t1843236107_0_0_0_Task_ContinueWith_m4241933427_gp_0_0_0_0,
	&GenInst_Task_ContinueWith_m4241933427_gp_0_0_0_0,
	&GenInst_U3CContinueWithU3Ec__AnonStorey0_1_t290911130_gp_0_0_0_0,
	&GenInst_Task_t1843236107_0_0_0_U3CContinueWithU3Ec__AnonStorey0_1_t290911130_gp_0_0_0_0,
	&GenInst_U3CContinueWithU3Ec__AnonStorey1_t2941869902_gp_0_0_0_0,
	&GenInst_Task_t1843236107_0_0_0_U3CContinueWithU3Ec__AnonStorey1_t2941869902_gp_0_0_0_0,
	&GenInst_Task_1_t2975743850_0_0_0,
	&GenInst_Task_1_t3208082394_gp_0_0_0_0,
	&GenInst_Task_1_t2975743850_0_0_0_Task_1_ContinueWith_m1882222058_gp_0_0_0_0,
	&GenInst_Task_1_ContinueWith_m1882222058_gp_0_0_0_0,
	&GenInst_Task_1_t3208082394_gp_0_0_0_0_Task_1_ContinueWith_m1882222058_gp_0_0_0_0,
	&GenInst_Task_t1843236107_0_0_0_Task_1_ContinueWith_m1882222058_gp_0_0_0_0,
	&GenInst_Task_1_t1425623332_0_0_0,
	&GenInst_U3CContinueWithU3Ec__AnonStorey0_t1657961876_gp_0_0_0_0,
	&GenInst_Task_1_t1202168276_0_0_0_U3CContinueWithU3Ec__AnonStorey1_1_t1434506820_gp_1_0_0_0,
	&GenInst_U3CContinueWithU3Ec__AnonStorey1_1_t1434506820_gp_0_0_0_0,
	&GenInst_Component_GetComponentInChildren_m3417738402_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1286417916_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1989846061_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3998291033_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3421358420_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m825036157_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m3873375864_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m1600202230_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m3990064736_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m2051523689_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentInChildren_m3844288190_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m2621570726_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m1933507101_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m4124793869_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m4177085118_gp_0_0_0_0,
	&GenInst_Mesh_GetAllocArrayFromChannel_m3109360642_gp_0_0_0_0,
	&GenInst_Mesh_SafeLength_m3101579087_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m3999848894_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m4171325764_gp_0_0_0_0,
	&GenInst_Mesh_SetUvsImpl_m3197944684_gp_0_0_0_0,
	&GenInst_Object_Instantiate_m2530741872_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m894835059_gp_0_0_0_0,
	&GenInst_GenericMixerPlayable_CastTo_m984828924_gp_0_0_0_0,
	&GenInst_AnimationPlayable_CastTo_m2315139850_gp_0_0_0_0,
	&GenInst_CustomAnimationPlayable_CastTo_m2887957467_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t476640868_gp_0_0_0_0,
	&GenInst_UnityAction_1_t2490859068_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t3757567216_0_0_0,
	&GenInst_String_t_0_0_0_FirebaseConfigExtensions_GetState_m2920295301_gp_0_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Dictionary_2_t3758243676_0_0_0,
	&GenInst_String_t_0_0_0_FirebaseConfigExtensions_SetState_m771261708_gp_0_0_0_0,
	&GenInst_ScheduledThreadPoolExecutor_Schedule_m1378582659_gp_0_0_0_0,
	&GenInst_Task_1_t1733150135_gp_0_0_0_0,
	&GenInst_MessageDigest_1_t895280350_gp_0_0_0_0,
	&GenInst_Extensions_Get_m2190249218_gp_0_0_0_0_Extensions_Get_m2190249218_gp_1_0_0_0,
	&GenInst_Extensions_IsEmpty_m18954539_gp_0_0_0_0,
	&GenInst_Extensions_Remove_m1109595446_gp_0_0_0_0,
	&GenInst_Extensions_Contains_m1742031028_gp_0_0_0_0_Extensions_Contains_m1742031028_gp_1_0_0_0,
	&GenInst_Extensions_Sort_m1675725510_gp_0_0_0_0,
	&GenInst_Extensions_Sort_m434348948_gp_0_0_0_0,
	&GenInst_Extensions_SubList_m3529398206_gp_0_0_0_0,
	&GenInst_Collections_1_t2510947384_gp_0_0_0_0,
	&GenInst_Collections_AddAll_m1767255324_gp_0_0_0_0,
	&GenInst_Collections_Remove_m1549833231_gp_0_0_0_0_Collections_Remove_m1549833231_gp_1_0_0_0,
	&GenInst_Collections_ToArray_m3354275710_gp_0_0_0_0,
	&GenInst_Collections_EmptyMap_m1916522621_gp_0_0_0_0_Collections_EmptyMap_m1916522621_gp_1_0_0_0,
	&GenInst_Collections_EmptyList_m2155311715_gp_0_0_0_0,
	&GenInst_Collections_GetEmptyList_m994968909_gp_0_0_0_0,
	&GenInst_Collections_SingletonList_m1687744405_gp_0_0_0_0,
	&GenInst_Collections_UnmodifiableList_m1396560940_gp_0_0_0_0,
	&GenInst_BlockingCollection_1_t1344468038_gp_0_0_0_0,
	&GenInst_Arrays_AsList_m1664585568_gp_0_0_0_0,
	&GenInst_GenericGenerator_1_t2598493761_gp_0_0_0_0,
	&GenInst_GenericGeneratorEnumerator_1_t484336861_gp_0_0_0_0,
	&GenInst_List_1_t2244783541_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator6_t1336800315_gp_0_0_0_0,
	&GenInst_List_1_t264254312_0_0_0,
	&GenInst_ArraySortedMap_2_t2832718474_gp_0_0_0_0_ArraySortedMap_2_t2832718474_gp_1_0_0_0,
	&GenInst_ArraySortedMap_2_t2832718474_gp_0_0_0_0,
	&GenInst_ArraySortedMap_2_BuildFrom_m271241364_gp_0_0_0_0,
	&GenInst_ArraySortedMap_2_BuildFrom_m271241364_gp_1_0_0_0_ArraySortedMap_2_BuildFrom_m271241364_gp_2_0_0_0,
	&GenInst_ArraySortedMap_2_BuildFrom_m271241364_gp_0_0_0_0_ArraySortedMap_2_BuildFrom_m271241364_gp_2_0_0_0,
	&GenInst_KeyValuePair_2_t2825879006_0_0_0,
	&GenInst_Enumerator134_t1654254710_gp_0_0_0_0_Enumerator134_t1654254710_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1795570862_0_0_0,
	&GenInst_ArraySortedMap_2_t2832718474_gp_0_0_0_0_ArraySortedMap_2_t2832718474_gp_0_0_0_0_ArraySortedMap_2_t2832718474_gp_1_0_0_0,
	&GenInst_ArraySortedMap_2_t2832718474_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t467411514_0_0_0,
	&GenInst_ImmutableSortedMap_2_t1726168363_gp_0_0_0_0_ImmutableSortedMap_2_t1726168363_gp_1_0_0_0,
	&GenInst_ImmutableSortedMap_2_t1726168363_gp_0_0_0_0,
	&GenInst_Builder_t4024572244_gp_0_0_0_0,
	&GenInst_Builder_t4024572244_gp_0_0_0_0_Builder_t4024572244_gp_1_0_0_0,
	&GenInst_Builder_FromMap_m4216204579_gp_0_0_0_0_Builder_FromMap_m4216204579_gp_1_0_0_0,
	&GenInst_Builder_FromMap_m4216204579_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t3291920818_0_0_0,
	&GenInst_Builder_BuildFrom_m3982510148_gp_0_0_0_0,
	&GenInst_Builder_BuildFrom_m3982510148_gp_1_0_0_0_Builder_BuildFrom_m3982510148_gp_2_0_0_0,
	&GenInst_Builder_BuildFrom_m3982510148_gp_0_0_0_0_Builder_BuildFrom_m3982510148_gp_2_0_0_0,
	&GenInst_Builder_BuildFrom_m3982510148_gp_0_0_0_0_Builder_BuildFrom_m3982510148_gp_1_0_0_0_Builder_BuildFrom_m3982510148_gp_2_0_0_0,
	&GenInst_KeyTranslator109_t3529733654_gp_0_0_0_0_KeyTranslator109_t3529733654_gp_1_0_0_0,
	&GenInst_ImmutableSortedMapIterator_2_t1801858573_gp_0_0_0_0_ImmutableSortedMapIterator_2_t1801858573_gp_1_0_0_0,
	&GenInst_ImmutableSortedMapIterator_2_t1801858573_gp_0_0_0_0,
	&GenInst_LlrbValueNode_2_t411157183_0_0_0,
	&GenInst_KeyValuePair_2_t3448121458_0_0_0,
	&GenInst_ImmutableSortedSet_1_t3924744372_gp_0_0_0_0,
	&GenInst_ImmutableSortedSet_1_t3924744372_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2148234354_0_0_0,
	&GenInst_WrappedEntryIterator_t2059579541_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_WrappedEntryIterator_t2059579541_gp_0_0_0_0,
	&GenInst_ImmutableSortedSet_1_t3924744372_gp_0_0_0_0_ImmutableSortedSet_1_t3924744372_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_LlrbBlackValueNode_2_t2021173780_gp_0_0_0_0_LlrbBlackValueNode_2_t2021173780_gp_1_0_0_0,
	&GenInst_LlrbEmptyNode_2_t339901673_gp_0_0_0_0_LlrbEmptyNode_2_t339901673_gp_1_0_0_0,
	&GenInst_LlrbEmptyNode_2_t339901673_gp_0_0_0_0,
	&GenInst_LlrbNode_2_t745868504_gp_0_0_0_0_LlrbNode_2_t745868504_gp_1_0_0_0,
	&GenInst_LlrbNode_2_t745868504_gp_0_0_0_0,
	&GenInst_Color_t3892504923_gp_0_0_0_0_Color_t3892504923_gp_1_0_0_0,
	&GenInst_NodeVisitor_t367361012_gp_0_0_0_0_NodeVisitor_t367361012_gp_1_0_0_0,
	&GenInst_LlrbRedValueNode_2_t2625247752_gp_0_0_0_0_LlrbRedValueNode_2_t2625247752_gp_1_0_0_0,
	&GenInst_LlrbValueNode_2_t3934141949_gp_0_0_0_0_LlrbValueNode_2_t3934141949_gp_1_0_0_0,
	&GenInst_LlrbValueNode_2_t3934141949_gp_0_0_0_0,
	&GenInst_RbTreeSortedMap_2_t470035893_gp_0_0_0_0_RbTreeSortedMap_2_t470035893_gp_1_0_0_0,
	&GenInst_RbTreeSortedMap_2_t470035893_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t2602970322_0_0_0,
	&GenInst_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_0_0_0_0,
	&GenInst_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_1_0_0_0_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_2_0_0_0,
	&GenInst_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_0_0_0_0_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_2_0_0_0,
	&GenInst_RbTreeSortedMap_2_t470035893_gp_0_0_0_0_RbTreeSortedMap_2_t470035893_gp_1_0_0_0_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_0_0_0_0_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_1_0_0_0_RbTreeSortedMap_2_BuildFrom_m3368602716_gp_2_0_0_0,
	&GenInst_RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0_RbTreeSortedMap_2_FromMap_m1608270809_gp_1_0_0_0,
	&GenInst_RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0,
	&GenInst_RbTreeSortedMap_2_t470035893_gp_0_0_0_0_RbTreeSortedMap_2_t470035893_gp_1_0_0_0_RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0_RbTreeSortedMap_2_FromMap_m1608270809_gp_0_0_0_0_RbTreeSortedMap_2_FromMap_m1608270809_gp_1_0_0_0,
	&GenInst_BuilderAbc_3_t2723596885_gp_2_0_0_0,
	&GenInst_BuilderAbc_3_t2723596885_gp_3_0_0_0_BuilderAbc_3_t2723596885_gp_4_0_0_0,
	&GenInst_BuilderAbc_3_t2723596885_gp_2_0_0_0_BuilderAbc_3_t2723596885_gp_4_0_0_0,
	&GenInst_BooleanChunk_t3755014355_0_0_0,
	&GenInst_Base12_t182897086_gp_0_0_0_0_Base12_t182897086_gp_1_0_0_0_Base12_t182897086_gp_2_0_0_0_Base12_t182897086_gp_3_0_0_0_Base12_t182897086_gp_4_0_0_0,
	&GenInst_Enumerator216_t1286610410_gp_0_0_0_0_Enumerator216_t1286610410_gp_1_0_0_0_Enumerator216_t1286610410_gp_2_0_0_0_Enumerator216_t1286610410_gp_3_0_0_0_Enumerator216_t1286610410_gp_4_0_0_0,
	&GenInst_BooleanChunk_t1065636191_0_0_0,
	&GenInst_BuilderAbc_3_t2723596885_gp_0_0_0_0_BuilderAbc_3_t2723596885_gp_1_0_0_0_BuilderAbc_3_t2723596885_gp_2_0_0_0_BuilderAbc_3_t2723596885_gp_3_0_0_0_BuilderAbc_3_t2723596885_gp_4_0_0_0,
	&GenInst_BuilderAbc_3_t2723596885_gp_2_0_0_0_BuilderAbc_3_t2723596885_gp_4_0_0_0_BuilderAbc_3_t2723596885_gp_2_0_0_0_BuilderAbc_3_t2723596885_gp_3_0_0_0_BuilderAbc_3_t2723596885_gp_4_0_0_0,
	&GenInst_BooleanChunk_t3968502773_0_0_0,
	&GenInst_StandardComparator_1_t225988210_gp_0_0_0_0,
	&GenInst_Repo_PostEvents_m1913162193_gp_0_0_0_0,
	&GenInst_NoopPersistenceManager_RunInTransaction_m1482428390_gp_0_0_0_0,
	&GenInst_IPersistenceManager_RunInTransaction_m3062294953_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t2759104460_0_0_0,
	&GenInst_Path_t2568473163_0_0_0_ImmutableTree_1_t749652817_gp_0_0_0_0,
	&GenInst_ImmutableTree_1_t749652817_gp_0_0_0_0,
	&GenInst_ImmutableTree_1_t749652817_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ImmutableTree_1_t749652817_gp_0_0_0_0_ImmutableTree_1_Fold_m1045730212_gp_0_0_0_0,
	&GenInst_ImmutableTree_1_Fold_m1045730212_gp_0_0_0_0,
	&GenInst_ImmutableTree_1_t749652817_gp_0_0_0_0_ImmutableTree_1_Fold_m2866300079_gp_0_0_0_0,
	&GenInst_ImmutableTree_1_Fold_m2866300079_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t2631867154_0_0_0,
	&GenInst_Path_t2568473163_0_0_0_TreeVisitor278_t622415511_gp_0_0_0_0,
	&GenInst_TreeVisitor278_t622415511_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Predicate_1_t382063104_gp_0_0_0_0,
	&GenInst_Predicate6_t1686426874_gp_0_0_0_0,
	&GenInst_Tree_1_t1820556167_gp_0_0_0_0,
	&GenInst_ITreeVisitor_t753513729_gp_0_0_0_0,
	&GenInst_ITreeFilter_t3097072013_gp_0_0_0_0,
	&GenInst_TreeVisitor111_t2665920001_gp_0_0_0_0,
	&GenInst_TreeNode_1_t2853783343_gp_0_0_0_0,
	&GenInst_EventRaiser_RaiseEvents_m1759473118_gp_0_0_0_0,
	&GenInst_Pair_2_t3489481034_gp_0_0_0_0_Pair_2_t3489481034_gp_1_0_0_0,
	&GenInst_ExecuteEvents_Execute_m1961163955_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m1189839031_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m1741636447_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m3661808413_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m609328278_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m1533008304_gp_0_0_0_0,
	&GenInst_SetPropertyUtility_SetStruct_m1250080331_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t573160278_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ListPool_1_t1984115411_gp_0_0_0_0,
	&GenInst_List_1_t2000868992_0_0_0,
	&GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0,
	&GenInst_BuildRequests_BuildRequestState_m127437801_gp_0_0_0_0,
	&GenInst_BuildRequests_BuildRequestState_m1406497467_gp_0_0_0_0,
	&GenInst_BuildRequests_BuildRequestState_m274720804_gp_0_0_0_0,
	&GenInst_BuildRequests_BuildRestApiRequest_m1350603643_gp_0_0_0_0,
	&GenInst_EventExtensions_Raise_m516954942_gp_0_0_0_0,
	&GenInst_CustomEventArgs_1_t1643748401_gp_0_0_0_0,
	&GenInst_CoroutineParams_1_t1685449857_gp_0_0_0_0,
	&GenInst_CoroutineClass_DelayStartCoroutine_m2034096782_gp_0_0_0_0,
	&GenInst_CoroutineClass_Run_m3622408249_gp_0_0_0_0,
	&GenInst_CoroutineClass_StartCoroutinesByName_m12538590_gp_0_0_0_0,
	&GenInst_CoroutineClass_DelayRequest_m2804562785_gp_0_0_0_0,
	&GenInst_CoroutineClass_ProcessResponse_m8201378_gp_0_0_0_0,
	&GenInst_CoroutineClass_SendRequestSub_m176625721_gp_0_0_0_0,
	&GenInst_CoroutineClass_SendRequestNonSub_m602791090_gp_0_0_0_0,
	&GenInst_CoroutineClass_SendRequestPresenceHeartbeat_m3831291074_gp_0_0_0_0,
	&GenInst_CoroutineClass_SendRequestHeartbeat_m3055755353_gp_0_0_0_0,
	&GenInst_CoroutineClass_BounceRequest_m2366396667_gp_0_0_0_0,
	&GenInst_CoroutineClass_ProcessTimeout_m4043344492_gp_0_0_0_0,
	&GenInst_CoroutineClass_CheckTimeoutSub_m386401961_gp_0_0_0_0,
	&GenInst_CoroutineClass_CheckTimeoutNonSub_m1320448384_gp_0_0_0_0,
	&GenInst_CoroutineClass_CheckTimeoutPresenceHeartbeat_m3595108732_gp_0_0_0_0,
	&GenInst_CoroutineClass_CheckTimeoutHeartbeat_m3060509961_gp_0_0_0_0,
	&GenInst_CoroutineClass_FireEvent_m2778758263_gp_0_0_0_0,
	&GenInst_U3CDelayRequestU3Ec__Iterator0_1_t1472438566_gp_0_0_0_0,
	&GenInst_U3CSendRequestSubU3Ec__Iterator1_1_t3370355612_gp_0_0_0_0,
	&GenInst_U3CSendRequestNonSubU3Ec__Iterator2_1_t909973264_gp_0_0_0_0,
	&GenInst_U3CSendRequestPresenceHeartbeatU3Ec__Iterator3_1_t1863681071_gp_0_0_0_0,
	&GenInst_U3CSendRequestHeartbeatU3Ec__Iterator4_1_t3389641949_gp_0_0_0_0,
	&GenInst_U3CCheckTimeoutSubU3Ec__Iterator5_1_t1773310952_gp_0_0_0_0,
	&GenInst_U3CCheckTimeoutNonSubU3Ec__Iterator6_1_t367276060_gp_0_0_0_0,
	&GenInst_U3CCheckTimeoutPresenceHeartbeatU3Ec__Iterator7_1_t981046395_gp_0_0_0_0,
	&GenInst_U3CCheckTimeoutHeartbeatU3Ec__Iterator8_1_t886287453_gp_0_0_0_0,
	&GenInst_ExceptionHandlers_CreateErrorCodeAndCallErrorCallback_m362467824_gp_0_0_0_0,
	&GenInst_ExceptionHandlers_ResponseCallbackErrorOrTimeoutHandler_m1440915304_gp_0_0_0_0,
	&GenInst_ExceptionHandlers_ResponseCallbackWebExceptionHandler_m2921193952_gp_0_0_0_0,
	&GenInst_ExceptionHandlers_ResponseCallbackExceptionHandler_m2979300171_gp_0_0_0_0,
	&GenInst_ExceptionHandlers_ProcessResponseCallbackExceptionHandler_m3077749814_gp_0_0_0_0,
	&GenInst_ExceptionHandlers_ProcessResponseCallbackWebExceptionHandler_m3558871925_gp_0_0_0_0,
	&GenInst_ExceptionHandlers_FireMultiplexException_m3941725088_gp_0_0_0_0,
	&GenInst_ExceptionHandlers_UrlRequestCommonExceptionHandler_m4124436988_gp_0_0_0_0,
	&GenInst_ExceptionHandlers_PushNotificationExceptionHandler_m133924597_gp_0_0_0_0,
	&GenInst_ExceptionHandlers_ChannelGroupExceptionHandler_m568856718_gp_0_0_0_0,
	&GenInst_ExceptionHandlers_CommonExceptionHandler_m537430337_gp_0_0_0_0,
	&GenInst_Helpers_UpdateOrAddUserStateOfEntity_m3328797157_gp_0_0_0_0,
	&GenInst_Helpers_CheckAndAddExistingUserState_m1851326739_gp_0_0_0_0,
	&GenInst_Helpers_CreateChannelEntity_m1336794432_gp_0_0_0_0,
	&GenInst_Helpers_CreateChannelEntity_m140253032_gp_0_0_0_0,
	&GenInst_Helpers_CreateChannelEntityAndAddToSubscribe_m1556276701_gp_0_0_0_0,
	&GenInst_Helpers_RemoveDuplicatesCheckAlreadySubscribedAndGetChannelsCommon_m2858836678_gp_0_0_0_0,
	&GenInst_Helpers_RemoveDuplicatesCheckAlreadySubscribedAndGetChannels_m837243240_gp_0_0_0_0,
	&GenInst_Helpers_ProcessResponseCallbacksV2_m907462966_gp_0_0_0_0,
	&GenInst_Helpers_ProcessResponseCallbacks_m100484875_gp_0_0_0_0,
	&GenInst_Helpers_CheckSubscribedChannelsAndSendCallbacks_m434503918_gp_0_0_0_0,
	&GenInst_Helpers_WrapResultBasedOnResponseType_m2395420773_gp_0_0_0_0,
	&GenInst_Helpers_ProcessWrapResultBasedOnResponseTypeException_m2995813353_gp_0_0_0_0,
	&GenInst_Helpers_FindChannelEntityAndCallback_m4007496225_gp_0_0_0_0,
	&GenInst_Helpers_ResponseToUserCallbackForSubscribeV2_m321604969_gp_0_0_0_0,
	&GenInst_Helpers_ResponseToUserCallback_m1132470759_gp_0_0_0_0,
	&GenInst_Helpers_ResponseToConnectCallback_m3049791096_gp_0_0_0_0,
	&GenInst_Helpers_CreatePubnubClientError_m1606993622_gp_0_0_0_0,
	&GenInst_Helpers_CreatePubnubClientError_m3026826283_gp_0_0_0_0,
	&GenInst_Helpers_CreatePubnubClientError_m3384800101_gp_0_0_0_0,
	&GenInst_Helpers_CreatePubnubClientError_m2221030080_gp_0_0_0_0,
	&GenInst_Helpers_CreatePubnubClientError_m2783450996_gp_0_0_0_0,
	&GenInst_JsonFxUnitySerializer_Deserialize_m1336013203_gp_0_0_0_0,
	&GenInst_Pubnub_Subscribe_m4173062036_gp_0_0_0_0,
	&GenInst_Pubnub_Subscribe_m1063884376_gp_0_0_0_0,
	&GenInst_Pubnub_Subscribe_m938123644_gp_0_0_0_0,
	&GenInst_Pubnub_Subscribe_m2991922720_gp_0_0_0_0,
	&GenInst_Pubnub_Subscribe_m2176482236_gp_0_0_0_0,
	&GenInst_Pubnub_Publish_m3308409727_gp_0_0_0_0,
	&GenInst_Pubnub_Publish_m1125539792_gp_0_0_0_0,
	&GenInst_Pubnub_Publish_m1659297476_gp_0_0_0_0,
	&GenInst_Pubnub_Publish_m695990871_gp_0_0_0_0,
	&GenInst_Pubnub_Publish_m1535867746_gp_0_0_0_0,
	&GenInst_Pubnub_Presence_m1987549737_gp_0_0_0_0,
	&GenInst_Pubnub_Presence_m4203253203_gp_0_0_0_0,
	&GenInst_Pubnub_Presence_m3139762387_gp_0_0_0_0,
	&GenInst_Pubnub_Presence_m2275364137_gp_0_0_0_0,
	&GenInst_Pubnub_DetailedHistory_m2786197292_gp_0_0_0_0,
	&GenInst_Pubnub_DetailedHistory_m1609992699_gp_0_0_0_0,
	&GenInst_Pubnub_DetailedHistory_m932243879_gp_0_0_0_0,
	&GenInst_Pubnub_DetailedHistory_m3061495195_gp_0_0_0_0,
	&GenInst_Pubnub_DetailedHistory_m4040590188_gp_0_0_0_0,
	&GenInst_Pubnub_DetailedHistory_m1522726056_gp_0_0_0_0,
	&GenInst_Pubnub_HereNow_m3852130836_gp_0_0_0_0,
	&GenInst_Pubnub_HereNow_m3312200294_gp_0_0_0_0,
	&GenInst_Pubnub_HereNow_m3915400198_gp_0_0_0_0,
	&GenInst_Pubnub_GlobalHereNow_m2431920201_gp_0_0_0_0,
	&GenInst_Pubnub_GlobalHereNow_m4166942753_gp_0_0_0_0,
	&GenInst_Pubnub_WhereNow_m667813399_gp_0_0_0_0,
	&GenInst_Pubnub_Unsubscribe_m742992275_gp_0_0_0_0,
	&GenInst_Pubnub_Unsubscribe_m2334922711_gp_0_0_0_0,
	&GenInst_Pubnub_Unsubscribe_m1059268865_gp_0_0_0_0,
	&GenInst_Pubnub_PresenceUnsubscribe_m841331202_gp_0_0_0_0,
	&GenInst_Pubnub_PresenceUnsubscribe_m2247745402_gp_0_0_0_0,
	&GenInst_Pubnub_Time_m4208398435_gp_0_0_0_0,
	&GenInst_Pubnub_AuditAccess_m1919842191_gp_0_0_0_0,
	&GenInst_Pubnub_AuditAccess_m255406457_gp_0_0_0_0,
	&GenInst_Pubnub_AuditAccess_m2182157455_gp_0_0_0_0,
	&GenInst_Pubnub_ChannelGroupAuditAccess_m3730454487_gp_0_0_0_0,
	&GenInst_Pubnub_ChannelGroupAuditAccess_m1789403085_gp_0_0_0_0,
	&GenInst_Pubnub_ChannelGroupAuditAccess_m1285632727_gp_0_0_0_0,
	&GenInst_Pubnub_ChannelGroupAuditPresenceAccess_m2375812858_gp_0_0_0_0,
	&GenInst_Pubnub_ChannelGroupAuditPresenceAccess_m568156134_gp_0_0_0_0,
	&GenInst_Pubnub_ChannelGroupGrantAccess_m1178498835_gp_0_0_0_0,
	&GenInst_Pubnub_ChannelGroupGrantAccess_m3460332902_gp_0_0_0_0,
	&GenInst_Pubnub_ChannelGroupGrantAccess_m1384537745_gp_0_0_0_0,
	&GenInst_Pubnub_ChannelGroupGrantAccess_m518994066_gp_0_0_0_0,
	&GenInst_Pubnub_ChannelGroupGrantPresenceAccess_m155900683_gp_0_0_0_0,
	&GenInst_Pubnub_ChannelGroupGrantPresenceAccess_m4121007672_gp_0_0_0_0,
	&GenInst_Pubnub_ChannelGroupGrantPresenceAccess_m2808607265_gp_0_0_0_0,
	&GenInst_Pubnub_ChannelGroupGrantPresenceAccess_m3359245880_gp_0_0_0_0,
	&GenInst_Pubnub_AuditPresenceAccess_m4091230300_gp_0_0_0_0,
	&GenInst_Pubnub_AuditPresenceAccess_m1880498832_gp_0_0_0_0,
	&GenInst_Pubnub_GrantAccess_m3752519459_gp_0_0_0_0,
	&GenInst_Pubnub_GrantAccess_m2877601888_gp_0_0_0_0,
	&GenInst_Pubnub_GrantAccess_m2509597765_gp_0_0_0_0,
	&GenInst_Pubnub_GrantAccess_m982193236_gp_0_0_0_0,
	&GenInst_Pubnub_GrantPresenceAccess_m2650027371_gp_0_0_0_0,
	&GenInst_Pubnub_GrantPresenceAccess_m300154446_gp_0_0_0_0,
	&GenInst_Pubnub_GrantPresenceAccess_m2743920757_gp_0_0_0_0,
	&GenInst_Pubnub_GrantPresenceAccess_m1724514030_gp_0_0_0_0,
	&GenInst_Pubnub_SetUserState_m4120570694_gp_0_0_0_0,
	&GenInst_Pubnub_SetUserState_m1721356594_gp_0_0_0_0,
	&GenInst_Pubnub_SetUserState_m198699590_gp_0_0_0_0,
	&GenInst_Pubnub_SetUserState_m2667328267_gp_0_0_0_0,
	&GenInst_Pubnub_SetUserState_m3214533505_gp_0_0_0_0,
	&GenInst_Pubnub_SetUserState_m2796272193_gp_0_0_0_0,
	&GenInst_Pubnub_GetUserState_m1950946474_gp_0_0_0_0,
	&GenInst_Pubnub_GetUserState_m496349206_gp_0_0_0_0,
	&GenInst_Pubnub_GetUserState_m1227177878_gp_0_0_0_0,
	&GenInst_Pubnub_RegisterDeviceForPush_m1562716200_gp_0_0_0_0,
	&GenInst_Pubnub_UnregisterDeviceForPush_m3670068759_gp_0_0_0_0,
	&GenInst_Pubnub_RemoveChannelForDevicePush_m3529259676_gp_0_0_0_0,
	&GenInst_Pubnub_GetChannelsForDevicePush_m2905882887_gp_0_0_0_0,
	&GenInst_Pubnub_AddChannelsToChannelGroup_m2030738952_gp_0_0_0_0,
	&GenInst_Pubnub_RemoveChannelsFromChannelGroup_m3440325472_gp_0_0_0_0,
	&GenInst_Pubnub_RemoveChannelGroup_m3630438968_gp_0_0_0_0,
	&GenInst_Pubnub_GetChannelsForChannelGroup_m2818075329_gp_0_0_0_0,
	&GenInst_Pubnub_GetAllChannelGroups_m1267467130_gp_0_0_0_0,
	&GenInst_Pubnub_TerminateCurrentSubscriberRequest_m3329387326_gp_0_0_0_0,
	&GenInst_Pubnub_EndPendingRequests_m2437671125_gp_0_0_0_0,
	&GenInst_Pubnub_ChangeUUID_m1807290336_gp_0_0_0_0,
	&GenInst_PubnubChannelCallback_1_t1317012685_gp_0_0_0_0,
	&GenInst_PubnubCallbacks_SendCallbacks_m1716641792_gp_0_0_0_0,
	&GenInst_PubnubCallbacks_SendCallbackChannelEntity_m569285385_gp_0_0_0_0,
	&GenInst_PubnubCallbacks_SendCallbacks_m1184457907_gp_0_0_0_0,
	&GenInst_PubnubCallbacks_SendCallbackBasedOnType_m2834347013_gp_0_0_0_0,
	&GenInst_PubnubCallbacks_SendCallbacks_m1731919200_gp_0_0_0_0,
	&GenInst_PubnubCallbacks_SendCallback_m417186584_gp_0_0_0_0,
	&GenInst_PubnubCallbacks_FireErrorCallbacksForAllChannels_m3526528221_gp_0_0_0_0,
	&GenInst_PubnubCallbacks_FireErrorCallbacksForAllChannels_m1593768736_gp_0_0_0_0,
	&GenInst_PubnubCallbacks_FireErrorCallbacksForAllChannelsCommon_m3626991610_gp_0_0_0_0,
	&GenInst_PubnubCallbacks_FireErrorCallbacksForAllChannels_m1778702368_gp_0_0_0_0,
	&GenInst_PubnubCallbacks_FireErrorCallback_m122111588_gp_0_0_0_0,
	&GenInst_PubnubCallbacks_GetPubnubChannelCallback_m285047123_gp_0_0_0_0,
	&GenInst_PubnubCallbacks_CallErrorCallback_m4008086708_gp_0_0_0_0,
	&GenInst_PubnubCallbacks_CallErrorCallback_m3396799529_gp_0_0_0_0,
	&GenInst_PubnubCallbacks_CallErrorCallback_m2481956824_gp_0_0_0_0,
	&GenInst_PubnubCallbacks_CallCallbackForEachChannelEntity_m3119983772_gp_0_0_0_0,
	&GenInst_PubnubCallbacks_CallErrorCallback_m3337762012_gp_0_0_0_0,
	&GenInst_PubnubCallbacks_JsonResponseToCallback_m772386789_gp_0_0_0_0,
	&GenInst_PubnubCallbacks_JsonResponseToCallback_m2546918935_gp_0_0_0_0,
	&GenInst_PubnubCallbacks_GoToCallback_m3707684152_gp_0_0_0_0,
	&GenInst_PubnubUnity_Subscribe_m2468939927_gp_0_0_0_0,
	&GenInst_PubnubUnity_Publish_m970715889_gp_0_0_0_0,
	&GenInst_PubnubUnity_Presence_m4218118026_gp_0_0_0_0,
	&GenInst_PubnubUnity_DetailedHistory_m1201134702_gp_0_0_0_0,
	&GenInst_PubnubUnity_HereNow_m4073893281_gp_0_0_0_0,
	&GenInst_PubnubUnity_GlobalHereNow_m1913427906_gp_0_0_0_0,
	&GenInst_PubnubUnity_WhereNow_m3016916584_gp_0_0_0_0,
	&GenInst_PubnubUnity_PresenceUnsubscribe_m1129371395_gp_0_0_0_0,
	&GenInst_PubnubUnity_Unsubscribe_m3864006068_gp_0_0_0_0,
	&GenInst_PubnubUnity_Time_m3989694962_gp_0_0_0_0,
	&GenInst_PubnubUnity_GrantAccess_m1374897058_gp_0_0_0_0,
	&GenInst_PubnubUnity_GrantPresenceAccess_m2725868595_gp_0_0_0_0,
	&GenInst_PubnubUnity_AuditAccess_m2295512540_gp_0_0_0_0,
	&GenInst_PubnubUnity_AuditPresenceAccess_m2308049845_gp_0_0_0_0,
	&GenInst_PubnubUnity_SetUserState_m3661404517_gp_0_0_0_0,
	&GenInst_PubnubUnity_SetUserState_m3414125520_gp_0_0_0_0,
	&GenInst_PubnubUnity_SharedSetUserState_m2517404725_gp_0_0_0_0,
	&GenInst_PubnubUnity_GetUserState_m1048009635_gp_0_0_0_0,
	&GenInst_PubnubUnity_RegisterDeviceForPush_m2758890001_gp_0_0_0_0,
	&GenInst_PubnubUnity_UnregisterDeviceForPush_m1203487220_gp_0_0_0_0,
	&GenInst_PubnubUnity_RemoveChannelForDevicePush_m1412460385_gp_0_0_0_0,
	&GenInst_PubnubUnity_GetChannelsForDevicePush_m1033807426_gp_0_0_0_0,
	&GenInst_PubnubUnity_AddChannelsToChannelGroup_m2931888159_gp_0_0_0_0,
	&GenInst_PubnubUnity_RemoveChannelsFromChannelGroup_m4235633181_gp_0_0_0_0,
	&GenInst_PubnubUnity_RemoveChannelGroup_m4155625375_gp_0_0_0_0,
	&GenInst_PubnubUnity_GetChannelsForChannelGroup_m1635924752_gp_0_0_0_0,
	&GenInst_PubnubUnity_GetAllChannelGroups_m323957427_gp_0_0_0_0,
	&GenInst_PubnubUnity_ChannelGroupAuditAccess_m1374637066_gp_0_0_0_0,
	&GenInst_PubnubUnity_ChannelGroupGrantAccess_m224916673_gp_0_0_0_0,
	&GenInst_PubnubUnity_TerminateCurrentSubscriberRequest_m1519878411_gp_0_0_0_0,
	&GenInst_PubnubUnity_EndPendingRequests_m2955742683_gp_0_0_0_0,
	&GenInst_PubnubUnity_ChangeUUID_m3033551624_gp_0_0_0_0,
	&GenInst_PubnubUnity_StopHeartbeat_m115641262_gp_0_0_0_0,
	&GenInst_PubnubUnity_StopPresenceHeartbeat_m824786707_gp_0_0_0_0,
	&GenInst_PubnubUnity_StartPresenceHeartbeat_m734069910_gp_0_0_0_0,
	&GenInst_PubnubUnity_RunPresenceHeartbeat_m3606858653_gp_0_0_0_0,
	&GenInst_PubnubUnity_StartHeartbeat_m2311398285_gp_0_0_0_0,
	&GenInst_PubnubUnity_RunHeartbeat_m212101030_gp_0_0_0_0,
	&GenInst_PubnubUnity_InternetConnectionAvailableHandler_m2273761285_gp_0_0_0_0,
	&GenInst_PubnubUnity_HeartbeatHandler_m4185383501_gp_0_0_0_0,
	&GenInst_PubnubUnity_PresenceHeartbeatHandler_m1687634496_gp_0_0_0_0,
	&GenInst_PubnubUnity_SubscribePresenceHanlder_m1008753418_gp_0_0_0_0,
	&GenInst_PubnubUnity_NonSubscribeHandler_m744339322_gp_0_0_0_0,
	&GenInst_PubnubUnity_ProcessCoroutineCompleteResponse_m1108104348_gp_0_0_0_0,
	&GenInst_PubnubUnity_CoroutineCompleteHandler_m1901552286_gp_0_0_0_0,
	&GenInst_PubnubUnity_ResponseCallbackNonErrorHandler_m2985386212_gp_0_0_0_0,
	&GenInst_PubnubUnity_UrlProcessResponseCallbackNonAsync_m4100457174_gp_0_0_0_0,
	&GenInst_PubnubUnity_MultiplexExceptionHandler_m3843920276_gp_0_0_0_0,
	&GenInst_PubnubUnity_HandleMultiplexException_m884452366_gp_0_0_0_0,
	&GenInst_PubnubUnity_ParseReceiedJSONV2_m3877215119_gp_0_0_0_0,
	&GenInst_PubnubUnity_ParseReceiedTimetoken_m286559067_gp_0_0_0_0,
	&GenInst_PubnubUnity_RunRequests_m1850633377_gp_0_0_0_0,
	&GenInst_PubnubUnity_UrlProcessRequest_m2222622787_gp_0_0_0_0,
	&GenInst_PubnubUnity_AbortPreviousRequest_m555711142_gp_0_0_0_0,
	&GenInst_PubnubUnity_RemoveUnsubscribedChannelsAndDeleteUserState_m4250063849_gp_0_0_0_0,
	&GenInst_PubnubUnity_ContinueToSubscribeRestOfChannels_m2580276620_gp_0_0_0_0,
	&GenInst_PubnubUnity_MultiChannelUnsubscribeInit_m4205939232_gp_0_0_0_0,
	&GenInst_PubnubUnity_MultiChannelSubscribeInit_m584995115_gp_0_0_0_0,
	&GenInst_PubnubUnity_CheckAllChannelsAreUnsubscribed_m1395504951_gp_0_0_0_0,
	&GenInst_PubnubUnity_MultiChannelSubscribeRequest_m1765398455_gp_0_0_0_0,
	&GenInst_PubnubUnity_RetryLoop_m2862844403_gp_0_0_0_0,
	&GenInst_ReconnectState_1_t3360909985_gp_0_0_0_0,
	&GenInst_RequestState_1_t1134074457_gp_0_0_0_0,
	&GenInst_SafeDictionary_2_t977272055_gp_0_0_0_0_SafeDictionary_2_t977272055_gp_1_0_0_0_SafeDictionary_2_t977272055_gp_1_0_0_0,
	&GenInst_SafeDictionary_2_t977272055_gp_0_0_0_0,
	&GenInst_SafeDictionary_2_t977272055_gp_1_0_0_0,
	&GenInst_SafeDictionary_2_t977272055_gp_0_0_0_0_SafeDictionary_2_t977272055_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t237505930_0_0_0,
	&GenInst_Utility_CheckCallback_m3672287171_gp_0_0_0_0,
	&GenInst_Utility_CheckRequestTimeoutMessageInError_m3861899226_gp_0_0_0_0,
	&GenInst_CommonIntergrationTests_Deserialize_m3606102474_gp_0_0_0_0,
	&GenInst_CommonIntergrationTests_TestRun_m3883849331_gp_0_0_0_0,
	&GenInst_CommonIntergrationTests_TestProcessResponse_m769602744_gp_0_0_0_0,
	&GenInst_CommonIntergrationTests_CcCoroutineComplete_m3907901951_gp_0_0_0_0,
	&GenInst_CommonIntergrationTests_TestProcessResponseError_m1490096072_gp_0_0_0_0,
	&GenInst_CommonIntergrationTests_CcCoroutineCompleteError_m2550140263_gp_0_0_0_0,
	&GenInst_CommonIntergrationTests_TestBounce_m1012596302_gp_0_0_0_0,
	&GenInst_AssertionComponent_Create_m2540130672_gp_0_0_0_0,
	&GenInst_AssertionComponent_Create_m1225255230_gp_0_0_0_0,
	&GenInst_AssertionComponent_Create_m4038277768_gp_0_0_0_0,
	&GenInst_AssertionComponent_Create_m1403296970_gp_0_0_0_0,
	&GenInst_AssertionComponent_Create_m2455923606_gp_0_0_0_0,
	&GenInst_AssertionComponent_Create_m2411304588_gp_0_0_0_0,
	&GenInst_AssertionComponent_CreateAssertionComponent_m888969327_gp_0_0_0_0,
	&GenInst_ActionBaseGeneric_1_t2640614817_gp_0_0_0_0,
	&GenInst_ComparerBaseGeneric_1_t3426043530_gp_0_0_0_0_ComparerBaseGeneric_1_t3426043530_gp_0_0_0_0,
	&GenInst_ComparerBaseGeneric_2_t3426043527_gp_0_0_0_0_ComparerBaseGeneric_2_t3426043527_gp_1_0_0_0,
	&GenInst_VectorComparerBase_1_t2826517438_gp_0_0_0_0,
	&GenInst_VectorComparerBase_1_t2826517438_gp_0_0_0_0_VectorComparerBase_1_t2826517438_gp_0_0_0_0,
	&GenInst_DefaultExecutionOrder_t2717914595_0_0_0,
	&GenInst_GUILayer_t3254902478_0_0_0,
	&GenInst_FirebaseHandler_t2907300047_0_0_0,
	&GenInst_SynchronizationContextBehavoir_t692674473_0_0_0,
	&GenInst_EventSystem_t3466835263_0_0_0,
	&GenInst_AxisEventData_t1524870173_0_0_0,
	&GenInst_SpriteRenderer_t1209076198_0_0_0,
	&GenInst_Image_t2042527209_0_0_0,
	&GenInst_Button_t2872111280_0_0_0,
	&GenInst_RawImage_t2749640213_0_0_0,
	&GenInst_Slider_t297367283_0_0_0,
	&GenInst_Scrollbar_t3248359358_0_0_0,
	&GenInst_InputField_t1631627530_0_0_0,
	&GenInst_ScrollRect_t1199013257_0_0_0,
	&GenInst_Dropdown_t1985816271_0_0_0,
	&GenInst_GraphicRaycaster_t410733016_0_0_0,
	&GenInst_CanvasRenderer_t261436805_0_0_0,
	&GenInst_Corner_t1077473318_0_0_0,
	&GenInst_Axis_t1431825778_0_0_0,
	&GenInst_Constraint_t3558160636_0_0_0,
	&GenInst_SubmitEvent_t907918422_0_0_0,
	&GenInst_OnChangeEvent_t2863344003_0_0_0,
	&GenInst_OnValidateInput_t1946318473_0_0_0,
	&GenInst_LayoutElement_t2808691390_0_0_0,
	&GenInst_RectOffset_t3387826427_0_0_0,
	&GenInst_TextAnchor_t112990806_0_0_0,
	&GenInst_AnimationTriggers_t3244928895_0_0_0,
	&GenInst_Animator_t69676727_0_0_0,
	&GenInst_FloatComparer_t4094246107_0_0_0,
	&GenInst_ValueDoesNotChange_t1999561901_0_0_0,
	&GenInst_GeneralComparer_t3787245533_0_0_0,
	&GenInst_ImageCapture_t1449800373_0_0_0,
	&GenInst_Uri_t19570940_0_0_0,
	&GenInst_CoroutineClass_t2476503358_0_0_0,
	&GenInst_Assembly_t4268412390_0_0_0_Type_t_0_0_0,
	&GenInst_TestRunner_t1304041832_0_0_0,
	&GenInst_AudioSource_t1135106623_0_0_0,
	&GenInst_PerFrameRaycast_t2437905999_0_0_0,
	&GenInst_SimpleBullet_t2114900010_0_0_0,
	&GenInst_SphereCollider_t1662511355_0_0_0,
	&GenInst_ParticleEmitter_t4099167268_0_0_0,
	&GenInst_Rigidbody_t4233889191_0_0_0,
	&GenInst_Renderer_t257310565_0_0_0,
	&GenInst_Collider_t3497673348_0_0_0,
	&GenInst_GUITexture_t1909122990_0_0_0,
	&GenInst_Animation_t2068071072_0_0_0,
	&GenInst_AI_t2476083018_0_0_0,
	&GenInst_KeyValuePair_2_t3733013679_0_0_0_KeyValuePair_2_t3733013679_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0,
	&GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0,
	&GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0,
	&GenInst_Playable_t3667545548_0_0_0_Playable_t3667545548_0_0_0,
	&GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0,
	&GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0,
	&GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0,
	&GenInst_ChannelIdentity_t1147162267_0_0_0_ChannelIdentity_t1147162267_0_0_0,
	&GenInst_KeyValuePair_2_t1381843961_0_0_0_KeyValuePair_2_t1381843961_0_0_0,
	&GenInst_KeyValuePair_2_t1381843961_0_0_0_Il2CppObject_0_0_0,
	&GenInst_CurrentRequestType_t3250227986_0_0_0_CurrentRequestType_t3250227986_0_0_0,
	&GenInst_KeyValuePair_2_t3104328646_0_0_0_KeyValuePair_2_t3104328646_0_0_0,
	&GenInst_KeyValuePair_2_t3104328646_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1008373517_0_0_0_KeyValuePair_2_t1008373517_0_0_0,
	&GenInst_KeyValuePair_2_t1008373517_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Label_t4243202660_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Label_t4243202660_0_0_0_Label_t4243202660_0_0_0,
	&GenInst_KeyValuePair_2_t818741072_0_0_0_KeyValuePair_2_t818741072_0_0_0,
	&GenInst_KeyValuePair_2_t818741072_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1436312919_0_0_0_KeyValuePair_2_t1436312919_0_0_0,
	&GenInst_KeyValuePair_2_t1436312919_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t271247988_0_0_0_KeyValuePair_2_t271247988_0_0_0,
	&GenInst_KeyValuePair_2_t271247988_0_0_0_Il2CppObject_0_0_0,
	&GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0,
	&GenInst_KeyValuePair_2_t888819835_0_0_0_KeyValuePair_2_t888819835_0_0_0,
	&GenInst_KeyValuePair_2_t888819835_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2553450683_0_0_0_KeyValuePair_2_t2553450683_0_0_0,
	&GenInst_KeyValuePair_2_t2553450683_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1979316409_0_0_0_KeyValuePair_2_t1979316409_0_0_0,
	&GenInst_KeyValuePair_2_t1979316409_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Nullable_1_t334943763_0_0_0_Nullable_1_t334943763_0_0_0,
	&GenInst_Nullable_1_t334943763_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t488203048_0_0_0_KeyValuePair_2_t488203048_0_0_0,
	&GenInst_KeyValuePair_2_t488203048_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TextEditOp_t3138797698_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TextEditOp_t3138797698_0_0_0_TextEditOp_t3138797698_0_0_0,
};
