﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Serialization.JsonFx.JsonConverter
struct JsonConverter_t4092422604;
// Pathfinding.Serialization.JsonFx.JsonWriter
struct JsonWriter_t446744171;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// Pathfinding.Serialization.JsonFx.JsonReader
struct JsonReader_t2021823321;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;

#include "codegen/il2cpp-codegen.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonWr446744171.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Object2689449295.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonR2021823321.h"

// System.Void Pathfinding.Serialization.JsonFx.JsonConverter::Write(Pathfinding.Serialization.JsonFx.JsonWriter,System.Type,System.Object)
extern "C"  void JsonConverter_Write_m2427477930 (JsonConverter_t4092422604 * __this, JsonWriter_t446744171 * ___writer0, Type_t * ___type1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.Serialization.JsonFx.JsonConverter::Read(Pathfinding.Serialization.JsonFx.JsonReader,System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  Il2CppObject * JsonConverter_Read_m1009477653 (JsonConverter_t4092422604 * __this, JsonReader_t2021823321 * ___reader0, Type_t * ___type1, Dictionary_2_t309261261 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
