﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Serialization.JsonFx.JsonReaderSettings
struct JsonReaderSettings_t1410336530;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.Stack`1<System.Collections.ArrayList>
struct Stack_1_t1044894425;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Serialization.JsonFx.JsonReader
struct  JsonReader_t2021823321  : public Il2CppObject
{
public:
	// Pathfinding.Serialization.JsonFx.JsonReaderSettings Pathfinding.Serialization.JsonFx.JsonReader::Settings
	JsonReaderSettings_t1410336530 * ___Settings_0;
	// System.String Pathfinding.Serialization.JsonFx.JsonReader::Source
	String_t* ___Source_1;
	// System.Int32 Pathfinding.Serialization.JsonFx.JsonReader::SourceLength
	int32_t ___SourceLength_2;
	// System.Int32 Pathfinding.Serialization.JsonFx.JsonReader::index
	int32_t ___index_3;
	// System.Collections.Generic.List`1<System.Object> Pathfinding.Serialization.JsonFx.JsonReader::previouslyDeserialized
	List_1_t2058570427 * ___previouslyDeserialized_4;
	// System.Collections.Generic.Stack`1<System.Collections.ArrayList> Pathfinding.Serialization.JsonFx.JsonReader::jsArrays
	Stack_1_t1044894425 * ___jsArrays_5;

public:
	inline static int32_t get_offset_of_Settings_0() { return static_cast<int32_t>(offsetof(JsonReader_t2021823321, ___Settings_0)); }
	inline JsonReaderSettings_t1410336530 * get_Settings_0() const { return ___Settings_0; }
	inline JsonReaderSettings_t1410336530 ** get_address_of_Settings_0() { return &___Settings_0; }
	inline void set_Settings_0(JsonReaderSettings_t1410336530 * value)
	{
		___Settings_0 = value;
		Il2CppCodeGenWriteBarrier(&___Settings_0, value);
	}

	inline static int32_t get_offset_of_Source_1() { return static_cast<int32_t>(offsetof(JsonReader_t2021823321, ___Source_1)); }
	inline String_t* get_Source_1() const { return ___Source_1; }
	inline String_t** get_address_of_Source_1() { return &___Source_1; }
	inline void set_Source_1(String_t* value)
	{
		___Source_1 = value;
		Il2CppCodeGenWriteBarrier(&___Source_1, value);
	}

	inline static int32_t get_offset_of_SourceLength_2() { return static_cast<int32_t>(offsetof(JsonReader_t2021823321, ___SourceLength_2)); }
	inline int32_t get_SourceLength_2() const { return ___SourceLength_2; }
	inline int32_t* get_address_of_SourceLength_2() { return &___SourceLength_2; }
	inline void set_SourceLength_2(int32_t value)
	{
		___SourceLength_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(JsonReader_t2021823321, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_previouslyDeserialized_4() { return static_cast<int32_t>(offsetof(JsonReader_t2021823321, ___previouslyDeserialized_4)); }
	inline List_1_t2058570427 * get_previouslyDeserialized_4() const { return ___previouslyDeserialized_4; }
	inline List_1_t2058570427 ** get_address_of_previouslyDeserialized_4() { return &___previouslyDeserialized_4; }
	inline void set_previouslyDeserialized_4(List_1_t2058570427 * value)
	{
		___previouslyDeserialized_4 = value;
		Il2CppCodeGenWriteBarrier(&___previouslyDeserialized_4, value);
	}

	inline static int32_t get_offset_of_jsArrays_5() { return static_cast<int32_t>(offsetof(JsonReader_t2021823321, ___jsArrays_5)); }
	inline Stack_1_t1044894425 * get_jsArrays_5() const { return ___jsArrays_5; }
	inline Stack_1_t1044894425 ** get_address_of_jsArrays_5() { return &___jsArrays_5; }
	inline void set_jsArrays_5(Stack_1_t1044894425 * value)
	{
		___jsArrays_5 = value;
		Il2CppCodeGenWriteBarrier(&___jsArrays_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
