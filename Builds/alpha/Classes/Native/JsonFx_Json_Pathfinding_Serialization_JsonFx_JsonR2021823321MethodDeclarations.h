﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Serialization.JsonFx.JsonReader
struct JsonReader_t2021823321;
// System.String
struct String_t;
// Pathfinding.Serialization.JsonFx.JsonReaderSettings
struct JsonReaderSettings_t1410336530;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>
struct Dictionary_2_t1662909226;
// System.Collections.IEnumerable
struct IEnumerable_t2911409499;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonR1410336530.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Object2689449295.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonT2143717411.h"

// System.Void Pathfinding.Serialization.JsonFx.JsonReader::.ctor(System.String)
extern "C"  void JsonReader__ctor_m1860805928 (JsonReader_t2021823321 * __this, String_t* ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonReader::.ctor(System.String,Pathfinding.Serialization.JsonFx.JsonReaderSettings)
extern "C"  void JsonReader__ctor_m1313609786 (JsonReader_t2021823321 * __this, String_t* ___input0, JsonReaderSettings_t1410336530 * ___settings1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.Serialization.JsonFx.JsonReader::Deserialize(System.Int32,System.Type)
extern "C"  Il2CppObject * JsonReader_Deserialize_m1628683004 (JsonReader_t2021823321 * __this, int32_t ___start0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.Serialization.JsonFx.JsonReader::Read(System.Type,System.Boolean)
extern "C"  Il2CppObject * JsonReader_Read_m1526966819 (JsonReader_t2021823321 * __this, Type_t * ___expectedType0, bool ___typeIsHint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.Serialization.JsonFx.JsonReader::ReadObject(System.Type)
extern "C"  Il2CppObject * JsonReader_ReadObject_m1070998081 (JsonReader_t2021823321 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Pathfinding.Serialization.JsonFx.JsonReader::GetGenericDictionaryType(System.Type)
extern "C"  Type_t * JsonReader_GetGenericDictionaryType_m42840060 (JsonReader_t2021823321 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonReader::PopulateObject(System.Object&,System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>,System.Type)
extern "C"  void JsonReader_PopulateObject_m152476262 (JsonReader_t2021823321 * __this, Il2CppObject ** ___result0, Type_t * ___objectType1, Dictionary_2_t1662909226 * ___memberMap2, Type_t * ___genericDictionaryType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerable Pathfinding.Serialization.JsonFx.JsonReader::ReadArray(System.Type)
extern "C"  Il2CppObject * JsonReader_ReadArray_m2359650156 (JsonReader_t2021823321 * __this, Type_t * ___arrayType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Serialization.JsonFx.JsonReader::ReadUnquotedKey()
extern "C"  String_t* JsonReader_ReadUnquotedKey_m2689293933 (JsonReader_t2021823321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.Serialization.JsonFx.JsonReader::ReadString(System.Type)
extern "C"  Il2CppObject * JsonReader_ReadString_m2145598429 (JsonReader_t2021823321 * __this, Type_t * ___expectedType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.Serialization.JsonFx.JsonReader::ReadNumber(System.Type)
extern "C"  Il2CppObject * JsonReader_ReadNumber_m218548847 (JsonReader_t2021823321 * __this, Type_t * ___expectedType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.Serialization.JsonFx.JsonReader::Deserialize(System.String,System.Int32,System.Type)
extern "C"  Il2CppObject * JsonReader_Deserialize_m2812844944 (Il2CppObject * __this /* static, unused */, String_t* ___value0, int32_t ___start1, Type_t * ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Serialization.JsonFx.JsonToken Pathfinding.Serialization.JsonFx.JsonReader::Tokenize()
extern "C"  int32_t JsonReader_Tokenize_m3135568107 (JsonReader_t2021823321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Serialization.JsonFx.JsonToken Pathfinding.Serialization.JsonFx.JsonReader::Tokenize(System.Boolean)
extern "C"  int32_t JsonReader_Tokenize_m3812013816 (JsonReader_t2021823321 * __this, bool ___allowUnquotedString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Serialization.JsonFx.JsonReader::MatchLiteral(System.String)
extern "C"  bool JsonReader_MatchLiteral_m4215823740 (JsonReader_t2021823321 * __this, String_t* ___literal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
