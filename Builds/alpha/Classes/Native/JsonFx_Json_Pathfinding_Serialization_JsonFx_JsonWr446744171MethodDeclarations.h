﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Serialization.JsonFx.JsonWriter
struct JsonWriter_t446744171;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// Pathfinding.Serialization.JsonFx.JsonWriterSettings
struct JsonWriterSettings_t2950311970;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Enum
struct Enum_t2459695545;
// System.Uri
struct Uri_t19570940;
// System.Version
struct Version_t1755874712;
// System.Collections.IEnumerable
struct IEnumerable_t2911409499;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Type
struct Type_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Enum[]
struct EnumU5BU5D_t975156036;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonW2950311970.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Guid2533601593.h"
#include "mscorlib_System_Enum2459695545.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_Version1755874712.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"

// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::.ctor(System.Text.StringBuilder)
extern "C"  void JsonWriter__ctor_m2497554504 (JsonWriter_t446744171 * __this, StringBuilder_t1221177846 * ___output0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::.ctor(System.Text.StringBuilder,Pathfinding.Serialization.JsonFx.JsonWriterSettings)
extern "C"  void JsonWriter__ctor_m4251693306 (JsonWriter_t446744171 * __this, StringBuilder_t1221177846 * ___output0, JsonWriterSettings_t2950311970 * ___settings1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Serialization.JsonFx.JsonWriterSettings Pathfinding.Serialization.JsonFx.JsonWriter::get_Settings()
extern "C"  JsonWriterSettings_t2950311970 * JsonWriter_get_Settings_m3858231505 (JsonWriter_t446744171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.Serialization.JsonFx.JsonWriter::Serialize(System.Object)
extern "C"  String_t* JsonWriter_Serialize_m1912680015 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Object)
extern "C"  void JsonWriter_Write_m834573621 (JsonWriter_t446744171 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Object,System.Boolean)
extern "C"  void JsonWriter_Write_m1483103282 (JsonWriter_t446744171 * __this, Il2CppObject * ___value0, bool ___isProperty1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.DateTime)
extern "C"  void JsonWriter_Write_m2451896681 (JsonWriter_t446744171 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Guid)
extern "C"  void JsonWriter_Write_m377216803 (JsonWriter_t446744171 * __this, Guid_t2533601593  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Enum)
extern "C"  void JsonWriter_Write_m4262245103 (JsonWriter_t446744171 * __this, Enum_t2459695545 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.String)
extern "C"  void JsonWriter_Write_m1445053257 (JsonWriter_t446744171 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Boolean)
extern "C"  void JsonWriter_Write_m1363201044 (JsonWriter_t446744171 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Byte)
extern "C"  void JsonWriter_Write_m1734869828 (JsonWriter_t446744171 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.SByte)
extern "C"  void JsonWriter_Write_m3502806265 (JsonWriter_t446744171 * __this, int8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Int16)
extern "C"  void JsonWriter_Write_m2352567266 (JsonWriter_t446744171 * __this, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.UInt16)
extern "C"  void JsonWriter_Write_m1799894225 (JsonWriter_t446744171 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Int32)
extern "C"  void JsonWriter_Write_m2070242268 (JsonWriter_t446744171 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.UInt32)
extern "C"  void JsonWriter_Write_m4125492987 (JsonWriter_t446744171 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Int64)
extern "C"  void JsonWriter_Write_m2493729765 (JsonWriter_t446744171 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.UInt64)
extern "C"  void JsonWriter_Write_m2962693800 (JsonWriter_t446744171 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Single)
extern "C"  void JsonWriter_Write_m751860230 (JsonWriter_t446744171 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Double)
extern "C"  void JsonWriter_Write_m3571657501 (JsonWriter_t446744171 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Decimal)
extern "C"  void JsonWriter_Write_m432403331 (JsonWriter_t446744171 * __this, Decimal_t724701077  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Char)
extern "C"  void JsonWriter_Write_m2642460430 (JsonWriter_t446744171 * __this, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.TimeSpan)
extern "C"  void JsonWriter_Write_m1285670029 (JsonWriter_t446744171 * __this, TimeSpan_t3430258949  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Uri)
extern "C"  void JsonWriter_Write_m1759937626 (JsonWriter_t446744171 * __this, Uri_t19570940 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::Write(System.Version)
extern "C"  void JsonWriter_Write_m811580164 (JsonWriter_t446744171 * __this, Version_t1755874712 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteArray(System.Collections.IEnumerable)
extern "C"  void JsonWriter_WriteArray_m4192356609 (JsonWriter_t446744171 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteArrayItem(System.Object)
extern "C"  void JsonWriter_WriteArrayItem_m3308581641 (JsonWriter_t446744171 * __this, Il2CppObject * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObject(System.Collections.IDictionary)
extern "C"  void JsonWriter_WriteObject_m4159325587 (JsonWriter_t446744171 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteDictionary(System.Collections.IEnumerable)
extern "C"  void JsonWriter_WriteDictionary_m2019323386 (JsonWriter_t446744171 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectProperty(System.String,System.Object)
extern "C"  void JsonWriter_WriteObjectProperty_m1477974781 (JsonWriter_t446744171 * __this, String_t* ___key0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyName(System.String)
extern "C"  void JsonWriter_WriteObjectPropertyName_m4107500458 (JsonWriter_t446744171 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyValue(System.Object)
extern "C"  void JsonWriter_WriteObjectPropertyValue_m1955779618 (JsonWriter_t446744171 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObject(System.Object,System.Type)
extern "C"  void JsonWriter_WriteObject_m695765779 (JsonWriter_t446744171 * __this, Il2CppObject * ___value0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteArrayItemDelim()
extern "C"  void JsonWriter_WriteArrayItemDelim_m925569848 (JsonWriter_t446744171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteObjectPropertyDelim()
extern "C"  void JsonWriter_WriteObjectPropertyDelim_m2333488564 (JsonWriter_t446744171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::WriteLine()
extern "C"  void JsonWriter_WriteLine_m1547489601 (JsonWriter_t446744171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Serialization.JsonFx.JsonWriter::IsIgnored(System.Type,System.Reflection.MemberInfo,System.Object)
extern "C"  bool JsonWriter_IsIgnored_m1951341061 (JsonWriter_t446744171 * __this, Type_t * ___objType0, MemberInfo_t * ___member1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Serialization.JsonFx.JsonWriter::IsDefaultValue(System.Reflection.MemberInfo,System.Object)
extern "C"  bool JsonWriter_IsDefaultValue_m2413175006 (JsonWriter_t446744171 * __this, MemberInfo_t * ___member0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Enum[] Pathfinding.Serialization.JsonFx.JsonWriter::GetFlagList(System.Type,System.Object)
extern "C"  EnumU5BU5D_t975156036* JsonWriter_GetFlagList_m2306565768 (Il2CppObject * __this /* static, unused */, Type_t * ___enumType0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Serialization.JsonFx.JsonWriter::InvalidIeee754(System.Decimal)
extern "C"  bool JsonWriter_InvalidIeee754_m2621332041 (JsonWriter_t446744171 * __this, Decimal_t724701077  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Serialization.JsonFx.JsonWriter::System.IDisposable.Dispose()
extern "C"  void JsonWriter_System_IDisposable_Dispose_m341063705 (JsonWriter_t446744171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
