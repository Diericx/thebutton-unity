﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime>
struct WriteDelegate_1_t415313529;
// System.Object
struct Il2CppObject;
// Pathfinding.Serialization.JsonFx.JsonWriter
struct JsonWriter_t446744171;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "JsonFx_Json_Pathfinding_Serialization_JsonFx_JsonWr446744171.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime>::.ctor(System.Object,System.IntPtr)
extern "C"  void WriteDelegate_1__ctor_m1631716844_gshared (WriteDelegate_1_t415313529 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define WriteDelegate_1__ctor_m1631716844(__this, ___object0, ___method1, method) ((  void (*) (WriteDelegate_1_t415313529 *, Il2CppObject *, IntPtr_t, const MethodInfo*))WriteDelegate_1__ctor_m1631716844_gshared)(__this, ___object0, ___method1, method)
// System.Void Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime>::Invoke(Pathfinding.Serialization.JsonFx.JsonWriter,T)
extern "C"  void WriteDelegate_1_Invoke_m2787355611_gshared (WriteDelegate_1_t415313529 * __this, JsonWriter_t446744171 * ___writer0, DateTime_t693205669  ___value1, const MethodInfo* method);
#define WriteDelegate_1_Invoke_m2787355611(__this, ___writer0, ___value1, method) ((  void (*) (WriteDelegate_1_t415313529 *, JsonWriter_t446744171 *, DateTime_t693205669 , const MethodInfo*))WriteDelegate_1_Invoke_m2787355611_gshared)(__this, ___writer0, ___value1, method)
// System.IAsyncResult Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime>::BeginInvoke(Pathfinding.Serialization.JsonFx.JsonWriter,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * WriteDelegate_1_BeginInvoke_m1434329302_gshared (WriteDelegate_1_t415313529 * __this, JsonWriter_t446744171 * ___writer0, DateTime_t693205669  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define WriteDelegate_1_BeginInvoke_m1434329302(__this, ___writer0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (WriteDelegate_1_t415313529 *, JsonWriter_t446744171 *, DateTime_t693205669 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))WriteDelegate_1_BeginInvoke_m1434329302_gshared)(__this, ___writer0, ___value1, ___callback2, ___object3, method)
// System.Void Pathfinding.Serialization.JsonFx.WriteDelegate`1<System.DateTime>::EndInvoke(System.IAsyncResult)
extern "C"  void WriteDelegate_1_EndInvoke_m3986240798_gshared (WriteDelegate_1_t415313529 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define WriteDelegate_1_EndInvoke_m3986240798(__this, ___result0, method) ((  void (*) (WriteDelegate_1_t415313529 *, Il2CppObject *, const MethodInfo*))WriteDelegate_1_EndInvoke_m3986240798_gshared)(__this, ___result0, method)
