﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E3806193287MethodDeclarations.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec>::.ctor(System.Collections.Generic.HashSet`1<T>)
#define Enumerator__ctor_m1501195588(__this, ___hashset0, method) ((  void (*) (Enumerator_t1494302703 *, HashSet_1_t3005986861 *, const MethodInfo*))Enumerator__ctor_m1279102766_gshared)(__this, ___hashset0, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4134500582(__this, method) ((  Il2CppObject * (*) (Enumerator_t1494302703 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2899861010_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3693468586(__this, method) ((  void (*) (Enumerator_t1494302703 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2573763156_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec>::MoveNext()
#define Enumerator_MoveNext_m2889656960(__this, method) ((  bool (*) (Enumerator_t1494302703 *, const MethodInfo*))Enumerator_MoveNext_m2097560514_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec>::get_Current()
#define Enumerator_get_Current_m1411745383(__this, method) ((  QuerySpec_t377558711 * (*) (Enumerator_t1494302703 *, const MethodInfo*))Enumerator_get_Current_m3016104593_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec>::Dispose()
#define Enumerator_Dispose_m3829775823(__this, method) ((  void (*) (Enumerator_t1494302703 *, const MethodInfo*))Enumerator_Dispose_m2585752265_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec>::CheckState()
#define Enumerator_CheckState_m3868476013(__this, method) ((  void (*) (Enumerator_t1494302703 *, const MethodInfo*))Enumerator_CheckState_m1761755727_gshared)(__this, method)
