﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<System.Int64>
struct HashSet_1_t3537506187;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E2025822029.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Int64>::.ctor(System.Collections.Generic.HashSet`1<T>)
extern "C"  void Enumerator__ctor_m867359122_gshared (Enumerator_t2025822029 * __this, HashSet_1_t3537506187 * ___hashset0, const MethodInfo* method);
#define Enumerator__ctor_m867359122(__this, ___hashset0, method) ((  void (*) (Enumerator_t2025822029 *, HashSet_1_t3537506187 *, const MethodInfo*))Enumerator__ctor_m867359122_gshared)(__this, ___hashset0, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3738024532_gshared (Enumerator_t2025822029 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3738024532(__this, method) ((  Il2CppObject * (*) (Enumerator_t2025822029 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3738024532_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1440143100_gshared (Enumerator_t2025822029 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1440143100(__this, method) ((  void (*) (Enumerator_t2025822029 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1440143100_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Int64>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2183539996_gshared (Enumerator_t2025822029 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2183539996(__this, method) ((  bool (*) (Enumerator_t2025822029 *, const MethodInfo*))Enumerator_MoveNext_m2183539996_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<System.Int64>::get_Current()
extern "C"  int64_t Enumerator_get_Current_m2974072486_gshared (Enumerator_t2025822029 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2974072486(__this, method) ((  int64_t (*) (Enumerator_t2025822029 *, const MethodInfo*))Enumerator_get_Current_m2974072486_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Int64>::Dispose()
extern "C"  void Enumerator_Dispose_m37451469_gshared (Enumerator_t2025822029 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m37451469(__this, method) ((  void (*) (Enumerator_t2025822029 *, const MethodInfo*))Enumerator_Dispose_m37451469_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Int64>::CheckState()
extern "C"  void Enumerator_CheckState_m3368423215_gshared (Enumerator_t2025822029 * __this, const MethodInfo* method);
#define Enumerator_CheckState_m3368423215(__this, method) ((  void (*) (Enumerator_t2025822029 *, const MethodInfo*))Enumerator_CheckState_m3368423215_gshared)(__this, method)
