﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E3806193287MethodDeclarations.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey>::.ctor(System.Collections.Generic.HashSet`1<T>)
#define Enumerator__ctor_m989207316(__this, ___hashset0, method) ((  void (*) (Enumerator_t2314546375 *, HashSet_1_t3826230533 *, const MethodInfo*))Enumerator__ctor_m1279102766_gshared)(__this, ___hashset0, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1329206998(__this, method) ((  Il2CppObject * (*) (Enumerator_t2314546375 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2899861010_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1576337126(__this, method) ((  void (*) (Enumerator_t2314546375 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2573763156_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey>::MoveNext()
#define Enumerator_MoveNext_m772833144(__this, method) ((  bool (*) (Enumerator_t2314546375 *, const MethodInfo*))Enumerator_MoveNext_m2097560514_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey>::get_Current()
#define Enumerator_get_Current_m3498848821(__this, method) ((  ChildKey_t1197802383 * (*) (Enumerator_t2314546375 *, const MethodInfo*))Enumerator_get_Current_m3016104593_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey>::Dispose()
#define Enumerator_Dispose_m680405645(__this, method) ((  void (*) (Enumerator_t2314546375 *, const MethodInfo*))Enumerator_Dispose_m2585752265_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey>::CheckState()
#define Enumerator_CheckState_m956559567(__this, method) ((  void (*) (Enumerator_t2314546375 *, const MethodInfo*))Enumerator_CheckState_m1761755727_gshared)(__this, method)
