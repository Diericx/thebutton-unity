﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E3806193287MethodDeclarations.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityTest.TestComponent>::.ctor(System.Collections.Generic.HashSet`1<T>)
#define Enumerator__ctor_m2462029212(__this, ___hashset0, method) ((  void (*) (Enumerator_t3633255593 *, HashSet_1_t849972455 *, const MethodInfo*))Enumerator__ctor_m1279102766_gshared)(__this, ___hashset0, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<UnityTest.TestComponent>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m52590736(__this, method) ((  Il2CppObject * (*) (Enumerator_t3633255593 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2899861010_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityTest.TestComponent>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m883834454(__this, method) ((  void (*) (Enumerator_t3633255593 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2573763156_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<UnityTest.TestComponent>::MoveNext()
#define Enumerator_MoveNext_m2435716360(__this, method) ((  bool (*) (Enumerator_t3633255593 *, const MethodInfo*))Enumerator_MoveNext_m2097560514_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<UnityTest.TestComponent>::get_Current()
#define Enumerator_get_Current_m2277731569(__this, method) ((  TestComponent_t2516511601 * (*) (Enumerator_t3633255593 *, const MethodInfo*))Enumerator_get_Current_m3016104593_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityTest.TestComponent>::Dispose()
#define Enumerator_Dispose_m4020586433(__this, method) ((  void (*) (Enumerator_t3633255593 *, const MethodInfo*))Enumerator_Dispose_m2585752265_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityTest.TestComponent>::CheckState()
#define Enumerator_CheckState_m1206626355(__this, method) ((  void (*) (Enumerator_t3633255593 *, const MethodInfo*))Enumerator_CheckState_m1761755727_gshared)(__this, method)
