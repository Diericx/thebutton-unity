﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<System.Int64>
struct HashSet_1_t3537506187;
// System.Collections.Generic.IEqualityComparer`1<System.Int64>
struct IEqualityComparer_1_t121710815;
// System.Collections.Generic.IEnumerable`1<System.Int64>
struct IEnumerable_1_t1201205082;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.IEnumerator`1<System.Int64>
struct IEnumerator_1_t2679569160;
// System.Int64[]
struct Int64U5BU5D_t717125112;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E2025822029.h"

// System.Void System.Collections.Generic.HashSet`1<System.Int64>::.ctor()
extern "C"  void HashSet_1__ctor_m2390421239_gshared (HashSet_1_t3537506187 * __this, const MethodInfo* method);
#define HashSet_1__ctor_m2390421239(__this, method) ((  void (*) (HashSet_1_t3537506187 *, const MethodInfo*))HashSet_1__ctor_m2390421239_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int64>::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1__ctor_m1697362913_gshared (HashSet_1_t3537506187 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define HashSet_1__ctor_m1697362913(__this, ___comparer0, method) ((  void (*) (HashSet_1_t3537506187 *, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m1697362913_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int64>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void HashSet_1__ctor_m2494471460_gshared (HashSet_1_t3537506187 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define HashSet_1__ctor_m2494471460(__this, ___collection0, method) ((  void (*) (HashSet_1_t3537506187 *, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m2494471460_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int64>::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1__ctor_m2873660398_gshared (HashSet_1_t3537506187 * __this, Il2CppObject* ___collection0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define HashSet_1__ctor_m2873660398(__this, ___collection0, ___comparer1, method) ((  void (*) (HashSet_1_t3537506187 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m2873660398_gshared)(__this, ___collection0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int64>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HashSet_1__ctor_m568221588_gshared (HashSet_1_t3537506187 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define HashSet_1__ctor_m568221588(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t3537506187 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))HashSet_1__ctor_m568221588_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<System.Int64>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m802417033_gshared (HashSet_1_t3537506187 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m802417033(__this, method) ((  Il2CppObject* (*) (HashSet_1_t3537506187 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m802417033_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Int64>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m620302702_gshared (HashSet_1_t3537506187 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m620302702(__this, method) ((  bool (*) (HashSet_1_t3537506187 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m620302702_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int64>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
extern "C"  void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m1585428574_gshared (HashSet_1_t3537506187 * __this, Int64U5BU5D_t717125112* ___array0, int32_t ___index1, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m1585428574(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t3537506187 *, Int64U5BU5D_t717125112*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m1585428574_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int64>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3904556094_gshared (HashSet_1_t3537506187 * __this, int64_t ___item0, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3904556094(__this, ___item0, method) ((  void (*) (HashSet_1_t3537506187 *, int64_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3904556094_gshared)(__this, ___item0, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<System.Int64>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * HashSet_1_System_Collections_IEnumerable_GetEnumerator_m3956759738_gshared (HashSet_1_t3537506187 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m3956759738(__this, method) ((  Il2CppObject * (*) (HashSet_1_t3537506187 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m3956759738_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Int64>::get_Count()
extern "C"  int32_t HashSet_1_get_Count_m2517895809_gshared (HashSet_1_t3537506187 * __this, const MethodInfo* method);
#define HashSet_1_get_Count_m2517895809(__this, method) ((  int32_t (*) (HashSet_1_t3537506187 *, const MethodInfo*))HashSet_1_get_Count_m2517895809_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int64>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1_Init_m1090483318_gshared (HashSet_1_t3537506187 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define HashSet_1_Init_m1090483318(__this, ___capacity0, ___comparer1, method) ((  void (*) (HashSet_1_t3537506187 *, int32_t, Il2CppObject*, const MethodInfo*))HashSet_1_Init_m1090483318_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int64>::InitArrays(System.Int32)
extern "C"  void HashSet_1_InitArrays_m3060467878_gshared (HashSet_1_t3537506187 * __this, int32_t ___size0, const MethodInfo* method);
#define HashSet_1_InitArrays_m3060467878(__this, ___size0, method) ((  void (*) (HashSet_1_t3537506187 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m3060467878_gshared)(__this, ___size0, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Int64>::SlotsContainsAt(System.Int32,System.Int32,T)
extern "C"  bool HashSet_1_SlotsContainsAt_m2170624900_gshared (HashSet_1_t3537506187 * __this, int32_t ___index0, int32_t ___hash1, int64_t ___item2, const MethodInfo* method);
#define HashSet_1_SlotsContainsAt_m2170624900(__this, ___index0, ___hash1, ___item2, method) ((  bool (*) (HashSet_1_t3537506187 *, int32_t, int32_t, int64_t, const MethodInfo*))HashSet_1_SlotsContainsAt_m2170624900_gshared)(__this, ___index0, ___hash1, ___item2, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int64>::CopyTo(T[],System.Int32)
extern "C"  void HashSet_1_CopyTo_m257644762_gshared (HashSet_1_t3537506187 * __this, Int64U5BU5D_t717125112* ___array0, int32_t ___index1, const MethodInfo* method);
#define HashSet_1_CopyTo_m257644762(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t3537506187 *, Int64U5BU5D_t717125112*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m257644762_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int64>::CopyTo(T[],System.Int32,System.Int32)
extern "C"  void HashSet_1_CopyTo_m2849216225_gshared (HashSet_1_t3537506187 * __this, Int64U5BU5D_t717125112* ___array0, int32_t ___index1, int32_t ___count2, const MethodInfo* method);
#define HashSet_1_CopyTo_m2849216225(__this, ___array0, ___index1, ___count2, method) ((  void (*) (HashSet_1_t3537506187 *, Int64U5BU5D_t717125112*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m2849216225_gshared)(__this, ___array0, ___index1, ___count2, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int64>::Resize()
extern "C"  void HashSet_1_Resize_m2648490823_gshared (HashSet_1_t3537506187 * __this, const MethodInfo* method);
#define HashSet_1_Resize_m2648490823(__this, method) ((  void (*) (HashSet_1_t3537506187 *, const MethodInfo*))HashSet_1_Resize_m2648490823_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Int64>::GetLinkHashCode(System.Int32)
extern "C"  int32_t HashSet_1_GetLinkHashCode_m2626508935_gshared (HashSet_1_t3537506187 * __this, int32_t ___index0, const MethodInfo* method);
#define HashSet_1_GetLinkHashCode_m2626508935(__this, ___index0, method) ((  int32_t (*) (HashSet_1_t3537506187 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m2626508935_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Int64>::GetItemHashCode(T)
extern "C"  int32_t HashSet_1_GetItemHashCode_m4096580511_gshared (HashSet_1_t3537506187 * __this, int64_t ___item0, const MethodInfo* method);
#define HashSet_1_GetItemHashCode_m4096580511(__this, ___item0, method) ((  int32_t (*) (HashSet_1_t3537506187 *, int64_t, const MethodInfo*))HashSet_1_GetItemHashCode_m4096580511_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Int64>::Add(T)
extern "C"  bool HashSet_1_Add_m4055042979_gshared (HashSet_1_t3537506187 * __this, int64_t ___item0, const MethodInfo* method);
#define HashSet_1_Add_m4055042979(__this, ___item0, method) ((  bool (*) (HashSet_1_t3537506187 *, int64_t, const MethodInfo*))HashSet_1_Add_m4055042979_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int64>::Clear()
extern "C"  void HashSet_1_Clear_m1310801718_gshared (HashSet_1_t3537506187 * __this, const MethodInfo* method);
#define HashSet_1_Clear_m1310801718(__this, method) ((  void (*) (HashSet_1_t3537506187 *, const MethodInfo*))HashSet_1_Clear_m1310801718_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Int64>::Contains(T)
extern "C"  bool HashSet_1_Contains_m1801307682_gshared (HashSet_1_t3537506187 * __this, int64_t ___item0, const MethodInfo* method);
#define HashSet_1_Contains_m1801307682(__this, ___item0, method) ((  bool (*) (HashSet_1_t3537506187 *, int64_t, const MethodInfo*))HashSet_1_Contains_m1801307682_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Int64>::Remove(T)
extern "C"  bool HashSet_1_Remove_m3111638755_gshared (HashSet_1_t3537506187 * __this, int64_t ___item0, const MethodInfo* method);
#define HashSet_1_Remove_m3111638755(__this, ___item0, method) ((  bool (*) (HashSet_1_t3537506187 *, int64_t, const MethodInfo*))HashSet_1_Remove_m3111638755_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int64>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HashSet_1_GetObjectData_m3652143193_gshared (HashSet_1_t3537506187 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define HashSet_1_GetObjectData_m3652143193(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t3537506187 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))HashSet_1_GetObjectData_m3652143193_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.HashSet`1<System.Int64>::OnDeserialization(System.Object)
extern "C"  void HashSet_1_OnDeserialization_m871793021_gshared (HashSet_1_t3537506187 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define HashSet_1_OnDeserialization_m871793021(__this, ___sender0, method) ((  void (*) (HashSet_1_t3537506187 *, Il2CppObject *, const MethodInfo*))HashSet_1_OnDeserialization_m871793021_gshared)(__this, ___sender0, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<System.Int64>::GetEnumerator()
extern "C"  Enumerator_t2025822029  HashSet_1_GetEnumerator_m3634119530_gshared (HashSet_1_t3537506187 * __this, const MethodInfo* method);
#define HashSet_1_GetEnumerator_m3634119530(__this, method) ((  Enumerator_t2025822029  (*) (HashSet_1_t3537506187 *, const MethodInfo*))HashSet_1_GetEnumerator_m3634119530_gshared)(__this, method)
