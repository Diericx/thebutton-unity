﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g1022910149MethodDeclarations.h"

// System.Void System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::.ctor()
#define HashSet_1__ctor_m4169078287(__this, method) ((  void (*) (HashSet_1_t3826230533 *, const MethodInfo*))HashSet_1__ctor_m2858247305_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
#define HashSet_1__ctor_m318676217(__this, ___comparer0, method) ((  void (*) (HashSet_1_t3826230533 *, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m1683677701_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define HashSet_1__ctor_m1692734162(__this, ___collection0, method) ((  void (*) (HashSet_1_t3826230533 *, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m3869181306_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
#define HashSet_1__ctor_m1576178944(__this, ___collection0, ___comparer1, method) ((  void (*) (HashSet_1_t3826230533 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m1859650088_gshared)(__this, ___collection0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1__ctor_m4176802562(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t3826230533 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))HashSet_1__ctor_m3582855242_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1034713(__this, method) ((  Il2CppObject* (*) (HashSet_1_t3826230533 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m788997721_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3133573380(__this, method) ((  bool (*) (HashSet_1_t3826230533 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2633171492_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m3914973572(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t3826230533 *, ChildKeyU5BU5D_t491817302*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m1933244740_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::System.Collections.Generic.ICollection<T>.Add(T)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3862673764(__this, ___item0, method) ((  void (*) (HashSet_1_t3826230533 *, ChildKey_t1197802383 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3632050820_gshared)(__this, ___item0, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::System.Collections.IEnumerable.GetEnumerator()
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m3976991664(__this, method) ((  Il2CppObject * (*) (HashSet_1_t3826230533 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m2498631708_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::get_Count()
#define HashSet_1_get_Count_m36624361(__this, method) ((  int32_t (*) (HashSet_1_t3826230533 *, const MethodInfo*))HashSet_1_get_Count_m4103055329_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
#define HashSet_1_Init_m1631769960(__this, ___capacity0, ___comparer1, method) ((  void (*) (HashSet_1_t3826230533 *, int32_t, Il2CppObject*, const MethodInfo*))HashSet_1_Init_m1258286688_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::InitArrays(System.Int32)
#define HashSet_1_InitArrays_m2523554548(__this, ___size0, method) ((  void (*) (HashSet_1_t3826230533 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m1536879844_gshared)(__this, ___size0, method)
// System.Boolean System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::SlotsContainsAt(System.Int32,System.Int32,T)
#define HashSet_1_SlotsContainsAt_m1558882062(__this, ___index0, ___hash1, ___item2, method) ((  bool (*) (HashSet_1_t3826230533 *, int32_t, int32_t, ChildKey_t1197802383 *, const MethodInfo*))HashSet_1_SlotsContainsAt_m219342270_gshared)(__this, ___index0, ___hash1, ___item2, method)
// System.Void System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::CopyTo(T[],System.Int32)
#define HashSet_1_CopyTo_m820175112(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t3826230533 *, ChildKeyU5BU5D_t491817302*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m1750586488_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::CopyTo(T[],System.Int32,System.Int32)
#define HashSet_1_CopyTo_m1000800401(__this, ___array0, ___index1, ___count2, method) ((  void (*) (HashSet_1_t3826230533 *, ChildKeyU5BU5D_t491817302*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m4175866709_gshared)(__this, ___array0, ___index1, ___count2, method)
// System.Void System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::Resize()
#define HashSet_1_Resize_m4282510551(__this, method) ((  void (*) (HashSet_1_t3826230533 *, const MethodInfo*))HashSet_1_Resize_m1435308491_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::GetLinkHashCode(System.Int32)
#define HashSet_1_GetLinkHashCode_m3851148479(__this, ___index0, method) ((  int32_t (*) (HashSet_1_t3826230533 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m3972670595_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::GetItemHashCode(T)
#define HashSet_1_GetItemHashCode_m1727989143(__this, ___item0, method) ((  int32_t (*) (HashSet_1_t3826230533 *, ChildKey_t1197802383 *, const MethodInfo*))HashSet_1_GetItemHashCode_m433445195_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::Add(T)
#define HashSet_1_Add_m3616191874(__this, ___item0, method) ((  bool (*) (HashSet_1_t3826230533 *, ChildKey_t1197802383 *, const MethodInfo*))HashSet_1_Add_m2918921714_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::Clear()
#define HashSet_1_Clear_m2984613892(__this, method) ((  void (*) (HashSet_1_t3826230533 *, const MethodInfo*))HashSet_1_Clear_m350367572_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::Contains(T)
#define HashSet_1_Contains_m1136022540(__this, ___item0, method) ((  bool (*) (HashSet_1_t3826230533 *, ChildKey_t1197802383 *, const MethodInfo*))HashSet_1_Contains_m1075264948_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::Remove(T)
#define HashSet_1_Remove_m40983291(__this, ___item0, method) ((  bool (*) (HashSet_1_t3826230533 *, ChildKey_t1197802383 *, const MethodInfo*))HashSet_1_Remove_m4157587527_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1_GetObjectData_m1777996105(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t3826230533 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))HashSet_1_GetObjectData_m2935317189_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::OnDeserialization(System.Object)
#define HashSet_1_OnDeserialization_m1867586541(__this, ___sender0, method) ((  void (*) (HashSet_1_t3826230533 *, Il2CppObject *, const MethodInfo*))HashSet_1_OnDeserialization_m1222146673_gshared)(__this, ___sender0, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<Firebase.Database.Internal.Snapshot.ChildKey>::GetEnumerator()
#define HashSet_1_GetEnumerator_m577728635(__this, method) ((  Enumerator_t2314546375  (*) (HashSet_1_t3826230533 *, const MethodInfo*))HashSet_1_GetEnumerator_m623886159_gshared)(__this, method)
