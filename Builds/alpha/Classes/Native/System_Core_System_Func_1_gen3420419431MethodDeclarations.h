﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_1_gen348874681MethodDeclarations.h"

// System.Void System.Func`1<System.Collections.IEnumerator>::.ctor(System.Object,System.IntPtr)
#define Func_1__ctor_m3632084784(__this, ___object0, ___method1, method) ((  void (*) (Func_1_t3420419431 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_1__ctor_m3570736155_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`1<System.Collections.IEnumerator>::Invoke()
#define Func_1_Invoke_m3170726294(__this, method) ((  Il2CppObject * (*) (Func_1_t3420419431 *, const MethodInfo*))Func_1_Invoke_m846324009_gshared)(__this, method)
// System.IAsyncResult System.Func`1<System.Collections.IEnumerator>::BeginInvoke(System.AsyncCallback,System.Object)
#define Func_1_BeginInvoke_m3507072139(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (Func_1_t3420419431 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_1_BeginInvoke_m478378364_gshared)(__this, ___callback0, ___object1, method)
// TResult System.Func`1<System.Collections.IEnumerator>::EndInvoke(System.IAsyncResult)
#define Func_1_EndInvoke_m3585003378(__this, ___result0, method) ((  Il2CppObject * (*) (Func_1_t3420419431 *, Il2CppObject *, const MethodInfo*))Func_1_EndInvoke_m1965878547_gshared)(__this, ___result0, method)
