﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen1448218544MethodDeclarations.h"

// System.Void System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<ITestResult>>,System.Int32>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3053724412(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1384938115 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m2060014652_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<ITestResult>>,System.Int32>::Invoke(T)
#define Func_2_Invoke_m3284936492(__this, ___arg10, method) ((  int32_t (*) (Func_2_t1384938115 *, KeyValuePair_2_t2297589086 , const MethodInfo*))Func_2_Invoke_m696951934_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<ITestResult>>,System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m2277932363(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1384938115 *, KeyValuePair_2_t2297589086 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m3207457001_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<ITestResult>>,System.Int32>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3110564850(__this, ___result0, method) ((  int32_t (*) (Func_2_t1384938115 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m2053084796_gshared)(__this, ___result0, method)
