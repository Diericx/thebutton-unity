﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen845594077MethodDeclarations.h"

// System.Void System.Func`2<UnityEngine.RuntimePlatform,System.String>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m1519685866(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t185365015 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1146193114_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<UnityEngine.RuntimePlatform,System.String>::Invoke(T)
#define Func_2_Invoke_m1238463662(__this, ___arg10, method) ((  String_t* (*) (Func_2_t185365015 *, int32_t, const MethodInfo*))Func_2_Invoke_m2089686788_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<UnityEngine.RuntimePlatform,System.String>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m937811971(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t185365015 *, int32_t, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4117396615_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<UnityEngine.RuntimePlatform,System.String>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m991853560(__this, ___result0, method) ((  String_t* (*) (Func_2_t185365015 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1795508590_gshared)(__this, ___result0, method)
