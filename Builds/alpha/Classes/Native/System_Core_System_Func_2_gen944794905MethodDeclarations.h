﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2825504181MethodDeclarations.h"

// System.Void System.Func`2<UnityTest.TestComponent,UnityTest.TestResult>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2092387834(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t944794905 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3134197304_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<UnityTest.TestComponent,UnityTest.TestResult>::Invoke(T)
#define Func_2_Invoke_m117599298(__this, ___arg10, method) ((  TestResult_t490498461 * (*) (Func_2_t944794905 *, TestComponent_t2516511601 *, const MethodInfo*))Func_2_Invoke_m3288232740_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<UnityTest.TestComponent,UnityTest.TestResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m776315683(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t944794905 *, TestComponent_t2516511601 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4034295761_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<UnityTest.TestComponent,UnityTest.TestResult>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m1910356324(__this, ___result0, method) ((  TestResult_t490498461 * (*) (Func_2_t944794905 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1674435418_gshared)(__this, ___result0, method)
