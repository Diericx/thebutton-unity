﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_3_gen916471171MethodDeclarations.h"

// System.Void System.Func`3<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters,PubNubMessaging.Core.ChannelParameters>::.ctor(System.Object,System.IntPtr)
#define Func_3__ctor_m1567658589(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t3197735739 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m2548880321_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters,PubNubMessaging.Core.ChannelParameters>::Invoke(T1,T2)
#define Func_3_Invoke_m2974737484(__this, ___arg10, ___arg21, method) ((  ChannelParameters_t547936593 * (*) (Func_3_t3197735739 *, ChannelIdentity_t1147162267 , ChannelParameters_t547936593 *, const MethodInfo*))Func_3_Invoke_m3051459976_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters,PubNubMessaging.Core.ChannelParameters>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Func_3_BeginInvoke_m1036898581(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t3197735739 *, ChannelIdentity_t1147162267 , ChannelParameters_t547936593 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m380852609_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters,PubNubMessaging.Core.ChannelParameters>::EndInvoke(System.IAsyncResult)
#define Func_3_EndInvoke_m2528574781(__this, ___result0, method) ((  ChannelParameters_t547936593 * (*) (Func_3_t3197735739 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m2054238817_gshared)(__this, ___result0, method)
