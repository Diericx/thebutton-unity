﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen2509106130MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<Google.Sharpen.Runnable>::.ctor()
#define Queue_1__ctor_m3056173976(__this, method) ((  void (*) (Queue_1_t1266641498 *, const MethodInfo*))Queue_1__ctor_m1845245813_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<Google.Sharpen.Runnable>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define Queue_1__ctor_m4169742675(__this, ___collection0, method) ((  void (*) (Queue_1_t1266641498 *, Il2CppObject*, const MethodInfo*))Queue_1__ctor_m4205375284_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.Queue`1<Google.Sharpen.Runnable>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m127496776(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t1266641498 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m772787461_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<Google.Sharpen.Runnable>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m915672956(__this, method) ((  bool (*) (Queue_1_t1266641498 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m307125669_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<Google.Sharpen.Runnable>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m2518282046(__this, method) ((  Il2CppObject * (*) (Queue_1_t1266641498 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m311152041_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<Google.Sharpen.Runnable>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3562049266(__this, method) ((  Il2CppObject* (*) (Queue_1_t1266641498 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4179169157_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<Google.Sharpen.Runnable>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m1917506897(__this, method) ((  Il2CppObject * (*) (Queue_1_t1266641498 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m251608368_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<Google.Sharpen.Runnable>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m3398663997(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t1266641498 *, RunnableU5BU5D_t3219930862*, int32_t, const MethodInfo*))Queue_1_CopyTo_m1419617898_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<Google.Sharpen.Runnable>::Dequeue()
#define Queue_1_Dequeue_m2541751299(__this, method) ((  Il2CppObject * (*) (Queue_1_t1266641498 *, const MethodInfo*))Queue_1_Dequeue_m4118160228_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<Google.Sharpen.Runnable>::Peek()
#define Queue_1_Peek_m1133975174(__this, method) ((  Il2CppObject * (*) (Queue_1_t1266641498 *, const MethodInfo*))Queue_1_Peek_m1463479953_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<Google.Sharpen.Runnable>::Enqueue(T)
#define Queue_1_Enqueue_m3223077040(__this, ___item0, method) ((  void (*) (Queue_1_t1266641498 *, Il2CppObject *, const MethodInfo*))Queue_1_Enqueue_m2424099095_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<Google.Sharpen.Runnable>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m47723659(__this, ___new_size0, method) ((  void (*) (Queue_1_t1266641498 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m3858927842_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<Google.Sharpen.Runnable>::get_Count()
#define Queue_1_get_Count_m2459511200(__this, method) ((  int32_t (*) (Queue_1_t1266641498 *, const MethodInfo*))Queue_1_get_Count_m3795587777_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<Google.Sharpen.Runnable>::GetEnumerator()
#define Queue_1_GetEnumerator_m1147517227(__this, method) ((  Enumerator_t1776704578  (*) (Queue_1_t1266641498 *, const MethodInfo*))Queue_1_GetEnumerator_m2842671368_gshared)(__this, method)
