﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen2509106130MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<System.IO.MemoryStream>::.ctor()
#define Queue_1__ctor_m1477901073(__this, method) ((  void (*) (Queue_1_t563651014 *, const MethodInfo*))Queue_1__ctor_m1845245813_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.IO.MemoryStream>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define Queue_1__ctor_m2241201084(__this, ___collection0, method) ((  void (*) (Queue_1_t563651014 *, Il2CppObject*, const MethodInfo*))Queue_1__ctor_m4205375284_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.Queue`1<System.IO.MemoryStream>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m863866489(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t563651014 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m772787461_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<System.IO.MemoryStream>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m650274217(__this, method) ((  bool (*) (Queue_1_t563651014 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m307125669_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<System.IO.MemoryStream>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m1211983481(__this, method) ((  Il2CppObject * (*) (Queue_1_t563651014 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m311152041_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<System.IO.MemoryStream>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3197408965(__this, method) ((  Il2CppObject* (*) (Queue_1_t563651014 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4179169157_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<System.IO.MemoryStream>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m3223756452(__this, method) ((  Il2CppObject * (*) (Queue_1_t563651014 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m251608368_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.IO.MemoryStream>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m57931450(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t563651014 *, MemoryStreamU5BU5D_t3423141970*, int32_t, const MethodInfo*))Queue_1_CopyTo_m1419617898_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<System.IO.MemoryStream>::Dequeue()
#define Queue_1_Dequeue_m3756754110(__this, method) ((  MemoryStream_t743994179 * (*) (Queue_1_t563651014 *, const MethodInfo*))Queue_1_Dequeue_m4118160228_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<System.IO.MemoryStream>::Peek()
#define Queue_1_Peek_m2448217501(__this, method) ((  MemoryStream_t743994179 * (*) (Queue_1_t563651014 *, const MethodInfo*))Queue_1_Peek_m1463479953_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.IO.MemoryStream>::Enqueue(T)
#define Queue_1_Enqueue_m1155767683(__this, ___item0, method) ((  void (*) (Queue_1_t563651014 *, MemoryStream_t743994179 *, const MethodInfo*))Queue_1_Enqueue_m2424099095_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<System.IO.MemoryStream>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m2831163154(__this, ___new_size0, method) ((  void (*) (Queue_1_t563651014 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m3858927842_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<System.IO.MemoryStream>::get_Count()
#define Queue_1_get_Count_m2961853537(__this, method) ((  int32_t (*) (Queue_1_t563651014 *, const MethodInfo*))Queue_1_get_Count_m3795587777_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<System.IO.MemoryStream>::GetEnumerator()
#define Queue_1_GetEnumerator_m1132605242(__this, method) ((  Enumerator_t1073714094  (*) (Queue_1_t563651014 *, const MethodInfo*))Queue_1_GetEnumerator_m2842671368_gshared)(__this, method)
