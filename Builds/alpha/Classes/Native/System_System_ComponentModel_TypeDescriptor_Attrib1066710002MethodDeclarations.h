﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.TypeDescriptor/AttributeProvider
struct AttributeProvider_t1066710002;
// System.Attribute[]
struct AttributeU5BU5D_t4255796347;
// System.ComponentModel.TypeDescriptionProvider
struct TypeDescriptionProvider_t2438624375;

#include "codegen/il2cpp-codegen.h"
#include "System_System_ComponentModel_TypeDescriptionProvid2438624375.h"

// System.Void System.ComponentModel.TypeDescriptor/AttributeProvider::.ctor(System.Attribute[],System.ComponentModel.TypeDescriptionProvider)
extern "C"  void AttributeProvider__ctor_m2926712501 (AttributeProvider_t1066710002 * __this, AttributeU5BU5D_t4255796347* ___attributes0, TypeDescriptionProvider_t2438624375 * ___parent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
