﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.TypeDescriptor/WrappedTypeDescriptionProvider
struct WrappedTypeDescriptionProvider_t1420150273;
// System.ComponentModel.TypeDescriptionProvider
struct TypeDescriptionProvider_t2438624375;
// System.Object
struct Il2CppObject;
// System.IServiceProvider
struct IServiceProvider_t2397848447;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "System_System_ComponentModel_TypeDescriptionProvid2438624375.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void System.ComponentModel.TypeDescriptor/WrappedTypeDescriptionProvider::.ctor(System.ComponentModel.TypeDescriptionProvider)
extern "C"  void WrappedTypeDescriptionProvider__ctor_m25523371 (WrappedTypeDescriptionProvider_t1420150273 * __this, TypeDescriptionProvider_t2438624375 * ___wrapped0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.TypeDescriptionProvider System.ComponentModel.TypeDescriptor/WrappedTypeDescriptionProvider::get_Wrapped()
extern "C"  TypeDescriptionProvider_t2438624375 * WrappedTypeDescriptionProvider_get_Wrapped_m2865310604 (WrappedTypeDescriptionProvider_t1420150273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.TypeDescriptor/WrappedTypeDescriptionProvider::set_Wrapped(System.ComponentModel.TypeDescriptionProvider)
extern "C"  void WrappedTypeDescriptionProvider_set_Wrapped_m1337860563 (WrappedTypeDescriptionProvider_t1420150273 * __this, TypeDescriptionProvider_t2438624375 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.ComponentModel.TypeDescriptor/WrappedTypeDescriptionProvider::CreateInstance(System.IServiceProvider,System.Type,System.Type[],System.Object[])
extern "C"  Il2CppObject * WrappedTypeDescriptionProvider_CreateInstance_m3993888789 (WrappedTypeDescriptionProvider_t1420150273 * __this, Il2CppObject * ___provider0, Type_t * ___objectType1, TypeU5BU5D_t1664964607* ___argTypes2, ObjectU5BU5D_t3614634134* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
