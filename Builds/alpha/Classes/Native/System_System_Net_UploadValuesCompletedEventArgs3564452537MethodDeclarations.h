﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.UploadValuesCompletedEventArgs
struct UploadValuesCompletedEventArgs_t3564452537;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Exception
struct Exception_t1927440687;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Net.UploadValuesCompletedEventArgs::.ctor(System.Byte[],System.Exception,System.Boolean,System.Object)
extern "C"  void UploadValuesCompletedEventArgs__ctor_m1360548335 (UploadValuesCompletedEventArgs_t3564452537 * __this, ByteU5BU5D_t3397334013* ___result0, Exception_t1927440687 * ___error1, bool ___cancelled2, Il2CppObject * ___userState3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.UploadValuesCompletedEventArgs::get_Result()
extern "C"  ByteU5BU5D_t3397334013* UploadValuesCompletedEventArgs_get_Result_m3852338329 (UploadValuesCompletedEventArgs_t3564452537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
