﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Net.ICredentials
struct ICredentials_t3855617113;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t3028142837;
// System.Threading.Thread
struct Thread_t241561612;
// System.Text.Encoding
struct Encoding_t663144255;
// System.Net.IWebProxy
struct IWebProxy_t3916853445;
// System.Net.DownloadProgressChangedEventHandler
struct DownloadProgressChangedEventHandler_t3683154451;
// System.Net.UploadValuesCompletedEventHandler
struct UploadValuesCompletedEventHandler_t563858374;

#include "System_System_ComponentModel_Component2826673791.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.WebClient
struct  WebClient_t1432723993  : public Component_t2826673791
{
public:
	// System.Net.ICredentials System.Net.WebClient::credentials
	Il2CppObject * ___credentials_6;
	// System.Net.WebHeaderCollection System.Net.WebClient::headers
	WebHeaderCollection_t3028142837 * ___headers_7;
	// System.Net.WebHeaderCollection System.Net.WebClient::responseHeaders
	WebHeaderCollection_t3028142837 * ___responseHeaders_8;
	// System.Boolean System.Net.WebClient::is_busy
	bool ___is_busy_9;
	// System.Boolean System.Net.WebClient::async
	bool ___async_10;
	// System.Threading.Thread System.Net.WebClient::async_thread
	Thread_t241561612 * ___async_thread_11;
	// System.Text.Encoding System.Net.WebClient::encoding
	Encoding_t663144255 * ___encoding_12;
	// System.Net.IWebProxy System.Net.WebClient::proxy
	Il2CppObject * ___proxy_13;
	// System.Net.DownloadProgressChangedEventHandler System.Net.WebClient::DownloadProgressChanged
	DownloadProgressChangedEventHandler_t3683154451 * ___DownloadProgressChanged_14;
	// System.Net.UploadValuesCompletedEventHandler System.Net.WebClient::UploadValuesCompleted
	UploadValuesCompletedEventHandler_t563858374 * ___UploadValuesCompleted_15;

public:
	inline static int32_t get_offset_of_credentials_6() { return static_cast<int32_t>(offsetof(WebClient_t1432723993, ___credentials_6)); }
	inline Il2CppObject * get_credentials_6() const { return ___credentials_6; }
	inline Il2CppObject ** get_address_of_credentials_6() { return &___credentials_6; }
	inline void set_credentials_6(Il2CppObject * value)
	{
		___credentials_6 = value;
		Il2CppCodeGenWriteBarrier(&___credentials_6, value);
	}

	inline static int32_t get_offset_of_headers_7() { return static_cast<int32_t>(offsetof(WebClient_t1432723993, ___headers_7)); }
	inline WebHeaderCollection_t3028142837 * get_headers_7() const { return ___headers_7; }
	inline WebHeaderCollection_t3028142837 ** get_address_of_headers_7() { return &___headers_7; }
	inline void set_headers_7(WebHeaderCollection_t3028142837 * value)
	{
		___headers_7 = value;
		Il2CppCodeGenWriteBarrier(&___headers_7, value);
	}

	inline static int32_t get_offset_of_responseHeaders_8() { return static_cast<int32_t>(offsetof(WebClient_t1432723993, ___responseHeaders_8)); }
	inline WebHeaderCollection_t3028142837 * get_responseHeaders_8() const { return ___responseHeaders_8; }
	inline WebHeaderCollection_t3028142837 ** get_address_of_responseHeaders_8() { return &___responseHeaders_8; }
	inline void set_responseHeaders_8(WebHeaderCollection_t3028142837 * value)
	{
		___responseHeaders_8 = value;
		Il2CppCodeGenWriteBarrier(&___responseHeaders_8, value);
	}

	inline static int32_t get_offset_of_is_busy_9() { return static_cast<int32_t>(offsetof(WebClient_t1432723993, ___is_busy_9)); }
	inline bool get_is_busy_9() const { return ___is_busy_9; }
	inline bool* get_address_of_is_busy_9() { return &___is_busy_9; }
	inline void set_is_busy_9(bool value)
	{
		___is_busy_9 = value;
	}

	inline static int32_t get_offset_of_async_10() { return static_cast<int32_t>(offsetof(WebClient_t1432723993, ___async_10)); }
	inline bool get_async_10() const { return ___async_10; }
	inline bool* get_address_of_async_10() { return &___async_10; }
	inline void set_async_10(bool value)
	{
		___async_10 = value;
	}

	inline static int32_t get_offset_of_async_thread_11() { return static_cast<int32_t>(offsetof(WebClient_t1432723993, ___async_thread_11)); }
	inline Thread_t241561612 * get_async_thread_11() const { return ___async_thread_11; }
	inline Thread_t241561612 ** get_address_of_async_thread_11() { return &___async_thread_11; }
	inline void set_async_thread_11(Thread_t241561612 * value)
	{
		___async_thread_11 = value;
		Il2CppCodeGenWriteBarrier(&___async_thread_11, value);
	}

	inline static int32_t get_offset_of_encoding_12() { return static_cast<int32_t>(offsetof(WebClient_t1432723993, ___encoding_12)); }
	inline Encoding_t663144255 * get_encoding_12() const { return ___encoding_12; }
	inline Encoding_t663144255 ** get_address_of_encoding_12() { return &___encoding_12; }
	inline void set_encoding_12(Encoding_t663144255 * value)
	{
		___encoding_12 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_12, value);
	}

	inline static int32_t get_offset_of_proxy_13() { return static_cast<int32_t>(offsetof(WebClient_t1432723993, ___proxy_13)); }
	inline Il2CppObject * get_proxy_13() const { return ___proxy_13; }
	inline Il2CppObject ** get_address_of_proxy_13() { return &___proxy_13; }
	inline void set_proxy_13(Il2CppObject * value)
	{
		___proxy_13 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_13, value);
	}

	inline static int32_t get_offset_of_DownloadProgressChanged_14() { return static_cast<int32_t>(offsetof(WebClient_t1432723993, ___DownloadProgressChanged_14)); }
	inline DownloadProgressChangedEventHandler_t3683154451 * get_DownloadProgressChanged_14() const { return ___DownloadProgressChanged_14; }
	inline DownloadProgressChangedEventHandler_t3683154451 ** get_address_of_DownloadProgressChanged_14() { return &___DownloadProgressChanged_14; }
	inline void set_DownloadProgressChanged_14(DownloadProgressChangedEventHandler_t3683154451 * value)
	{
		___DownloadProgressChanged_14 = value;
		Il2CppCodeGenWriteBarrier(&___DownloadProgressChanged_14, value);
	}

	inline static int32_t get_offset_of_UploadValuesCompleted_15() { return static_cast<int32_t>(offsetof(WebClient_t1432723993, ___UploadValuesCompleted_15)); }
	inline UploadValuesCompletedEventHandler_t563858374 * get_UploadValuesCompleted_15() const { return ___UploadValuesCompleted_15; }
	inline UploadValuesCompletedEventHandler_t563858374 ** get_address_of_UploadValuesCompleted_15() { return &___UploadValuesCompleted_15; }
	inline void set_UploadValuesCompleted_15(UploadValuesCompletedEventHandler_t563858374 * value)
	{
		___UploadValuesCompleted_15 = value;
		Il2CppCodeGenWriteBarrier(&___UploadValuesCompleted_15, value);
	}
};

struct WebClient_t1432723993_StaticFields
{
public:
	// System.String System.Net.WebClient::urlEncodedCType
	String_t* ___urlEncodedCType_4;
	// System.Byte[] System.Net.WebClient::hexBytes
	ByteU5BU5D_t3397334013* ___hexBytes_5;

public:
	inline static int32_t get_offset_of_urlEncodedCType_4() { return static_cast<int32_t>(offsetof(WebClient_t1432723993_StaticFields, ___urlEncodedCType_4)); }
	inline String_t* get_urlEncodedCType_4() const { return ___urlEncodedCType_4; }
	inline String_t** get_address_of_urlEncodedCType_4() { return &___urlEncodedCType_4; }
	inline void set_urlEncodedCType_4(String_t* value)
	{
		___urlEncodedCType_4 = value;
		Il2CppCodeGenWriteBarrier(&___urlEncodedCType_4, value);
	}

	inline static int32_t get_offset_of_hexBytes_5() { return static_cast<int32_t>(offsetof(WebClient_t1432723993_StaticFields, ___hexBytes_5)); }
	inline ByteU5BU5D_t3397334013* get_hexBytes_5() const { return ___hexBytes_5; }
	inline ByteU5BU5D_t3397334013** get_address_of_hexBytes_5() { return &___hexBytes_5; }
	inline void set_hexBytes_5(ByteU5BU5D_t3397334013* value)
	{
		___hexBytes_5 = value;
		Il2CppCodeGenWriteBarrier(&___hexBytes_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
