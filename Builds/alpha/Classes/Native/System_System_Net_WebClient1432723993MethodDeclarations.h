﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.WebClient
struct WebClient_t1432723993;
// System.Net.UploadValuesCompletedEventHandler
struct UploadValuesCompletedEventHandler_t563858374;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t3028142837;
// System.Net.IWebProxy
struct IWebProxy_t3916853445;
// System.String
struct String_t;
// System.Uri
struct Uri_t19570940;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t3047564564;
// System.Object
struct Il2CppObject;
// System.Net.WebRequest
struct WebRequest_t1365124353;
// System.IO.Stream
struct Stream_t3255436806;
// System.Net.DownloadProgressChangedEventArgs
struct DownloadProgressChangedEventArgs_t3269688412;
// System.Net.UploadValuesCompletedEventArgs
struct UploadValuesCompletedEventArgs_t3564452537;
// System.Net.WebResponse
struct WebResponse_t1895226051;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_UploadValuesCompletedEventHandler563858374.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_String2029220233.h"
#include "System_System_Collections_Specialized_NameValueCol3047564564.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "System_System_Net_DownloadProgressChangedEventArgs3269688412.h"
#include "System_System_Net_UploadValuesCompletedEventArgs3564452537.h"
#include "System_System_Net_WebRequest1365124353.h"

// System.Void System.Net.WebClient::.ctor()
extern "C"  void WebClient__ctor_m660733025 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::.cctor()
extern "C"  void WebClient__cctor_m196213842 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::add_UploadValuesCompleted(System.Net.UploadValuesCompletedEventHandler)
extern "C"  void WebClient_add_UploadValuesCompleted_m3981342317 (WebClient_t1432723993 * __this, UploadValuesCompletedEventHandler_t563858374 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::remove_UploadValuesCompleted(System.Net.UploadValuesCompletedEventHandler)
extern "C"  void WebClient_remove_UploadValuesCompleted_m4255774786 (WebClient_t1432723993 * __this, UploadValuesCompletedEventHandler_t563858374 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebHeaderCollection System.Net.WebClient::get_Headers()
extern "C"  WebHeaderCollection_t3028142837 * WebClient_get_Headers_m2426120036 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IWebProxy System.Net.WebClient::get_Proxy()
extern "C"  Il2CppObject * WebClient_get_Proxy_m903524692 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebClient::get_IsBusy()
extern "C"  bool WebClient_get_IsBusy_m1483534531 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::CheckBusy()
extern "C"  void WebClient_CheckBusy_m3268863214 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::DetermineMethod(System.Uri,System.String,System.Boolean)
extern "C"  String_t* WebClient_DetermineMethod_m2758189946 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, bool ___is_upload2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::UploadValuesCore(System.Uri,System.String,System.Collections.Specialized.NameValueCollection,System.Object)
extern "C"  ByteU5BU5D_t3397334013* WebClient_UploadValuesCore_m1361004655 (WebClient_t1432723993 * __this, Uri_t19570940 * ___uri0, String_t* ___method1, NameValueCollection_t3047564564 * ___data2, Il2CppObject * ___userToken3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.WebClient::SetupRequest(System.Uri)
extern "C"  WebRequest_t1365124353 * WebClient_SetupRequest_m3399367600 (WebClient_t1432723993 * __this, Uri_t19570940 * ___uri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.WebClient::SetupRequest(System.Uri,System.String,System.Boolean)
extern "C"  WebRequest_t1365124353 * WebClient_SetupRequest_m1206909435 (WebClient_t1432723993 * __this, Uri_t19570940 * ___uri0, String_t* ___method1, bool ___is_upload2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::ReadAll(System.IO.Stream,System.Int32,System.Object)
extern "C"  ByteU5BU5D_t3397334013* WebClient_ReadAll_m1147384516 (WebClient_t1432723993 * __this, Stream_t3255436806 * ___stream0, int32_t ___length1, Il2CppObject * ___userToken2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UrlEncodeAndWrite(System.IO.Stream,System.Byte[])
extern "C"  void WebClient_UrlEncodeAndWrite_m3388978810 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::CompleteAsync()
extern "C"  void WebClient_CompleteAsync_m1173274810 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadValuesAsync(System.Uri,System.String,System.Collections.Specialized.NameValueCollection)
extern "C"  void WebClient_UploadValuesAsync_m1764200028 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, NameValueCollection_t3047564564 * ___values2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::UploadValuesAsync(System.Uri,System.String,System.Collections.Specialized.NameValueCollection,System.Object)
extern "C"  void WebClient_UploadValuesAsync_m3820210094 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, String_t* ___method1, NameValueCollection_t3047564564 * ___values2, Il2CppObject * ___userToken3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnDownloadProgressChanged(System.Net.DownloadProgressChangedEventArgs)
extern "C"  void WebClient_OnDownloadProgressChanged_m2431565293 (WebClient_t1432723993 * __this, DownloadProgressChangedEventArgs_t3269688412 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnUploadValuesCompleted(System.Net.UploadValuesCompletedEventArgs)
extern "C"  void WebClient_OnUploadValuesCompleted_m1965360313 (WebClient_t1432723993 * __this, UploadValuesCompletedEventArgs_t3564452537 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.WebClient::GetWebRequest(System.Uri)
extern "C"  WebRequest_t1365124353 * WebClient_GetWebRequest_m3663064103 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebResponse System.Net.WebClient::GetWebResponse(System.Net.WebRequest)
extern "C"  WebResponse_t1895226051 * WebClient_GetWebResponse_m247274309 (WebClient_t1432723993 * __this, WebRequest_t1365124353 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::<UploadValuesAsync>m__15(System.Object)
extern "C"  void WebClient_U3CUploadValuesAsyncU3Em__15_m827187413 (WebClient_t1432723993 * __this, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
