﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AnimationState
struct AnimationState_t1303741697;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AnimationBlendMode3402232318.h"

// System.Void UnityEngine.AnimationState::set_enabled(System.Boolean)
extern "C"  void AnimationState_set_enabled_m2079619927 (AnimationState_t1303741697 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimationState::get_weight()
extern "C"  float AnimationState_get_weight_m3266163847 (AnimationState_t1303741697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationState::set_weight(System.Single)
extern "C"  void AnimationState_set_weight_m2370306600 (AnimationState_t1303741697 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimationState::get_time()
extern "C"  float AnimationState_get_time_m2280025052 (AnimationState_t1303741697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationState::set_time(System.Single)
extern "C"  void AnimationState_set_time_m1882411177 (AnimationState_t1303741697 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimationState::get_normalizedTime()
extern "C"  float AnimationState_get_normalizedTime_m3003675227 (AnimationState_t1303741697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationState::set_normalizedTime(System.Single)
extern "C"  void AnimationState_set_normalizedTime_m3942659976 (AnimationState_t1303741697 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationState::set_speed(System.Single)
extern "C"  void AnimationState_set_speed_m465014523 (AnimationState_t1303741697 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimationState::get_length()
extern "C"  float AnimationState_get_length_m2895238571 (AnimationState_t1303741697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationState::set_layer(System.Int32)
extern "C"  void AnimationState_set_layer_m139053567 (AnimationState_t1303741697 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationState::set_blendMode(UnityEngine.AnimationBlendMode)
extern "C"  void AnimationState_set_blendMode_m3997078040 (AnimationState_t1303741697 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
