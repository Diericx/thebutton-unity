﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleEmitter
struct ParticleEmitter_t4099167268;

#include "codegen/il2cpp-codegen.h"

// System.Single UnityEngine.ParticleEmitter::get_maxEnergy()
extern "C"  float ParticleEmitter_get_maxEnergy_m4065204998 (ParticleEmitter_t4099167268 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
