﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.ThreadLocal`1<System.Object>
struct ThreadLocal_1_t241108632;
// System.Func`1<System.Object>
struct Func_1_t348874681;
// System.Collections.Generic.IDictionary`2<System.Int64,System.Object>
struct IDictionary_2_t1678051118;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Threading.ThreadLocal`1<System.Object>::.ctor(System.Func`1<T>)
extern "C"  void ThreadLocal_1__ctor_m3553355981_gshared (ThreadLocal_1_t241108632 * __this, Func_1_t348874681 * ___valueFactory0, const MethodInfo* method);
#define ThreadLocal_1__ctor_m3553355981(__this, ___valueFactory0, method) ((  void (*) (ThreadLocal_1_t241108632 *, Func_1_t348874681 *, const MethodInfo*))ThreadLocal_1__ctor_m3553355981_gshared)(__this, ___valueFactory0, method)
// System.Collections.Generic.IDictionary`2<System.Int64,T> System.Threading.ThreadLocal`1<System.Object>::get_ThreadLocalData()
extern "C"  Il2CppObject* ThreadLocal_1_get_ThreadLocalData_m1340242554_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ThreadLocal_1_get_ThreadLocalData_m1340242554(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ThreadLocal_1_get_ThreadLocalData_m1340242554_gshared)(__this /* static, unused */, method)
// T System.Threading.ThreadLocal`1<System.Object>::get_Value()
extern "C"  Il2CppObject * ThreadLocal_1_get_Value_m500997168_gshared (ThreadLocal_1_t241108632 * __this, const MethodInfo* method);
#define ThreadLocal_1_get_Value_m500997168(__this, method) ((  Il2CppObject * (*) (ThreadLocal_1_t241108632 *, const MethodInfo*))ThreadLocal_1_get_Value_m500997168_gshared)(__this, method)
// System.Void System.Threading.ThreadLocal`1<System.Object>::set_Value(T)
extern "C"  void ThreadLocal_1_set_Value_m3912689309_gshared (ThreadLocal_1_t241108632 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ThreadLocal_1_set_Value_m3912689309(__this, ___value0, method) ((  void (*) (ThreadLocal_1_t241108632 *, Il2CppObject *, const MethodInfo*))ThreadLocal_1_set_Value_m3912689309_gshared)(__this, ___value0, method)
// System.Void System.Threading.ThreadLocal`1<System.Object>::Finalize()
extern "C"  void ThreadLocal_1_Finalize_m1655992953_gshared (ThreadLocal_1_t241108632 * __this, const MethodInfo* method);
#define ThreadLocal_1_Finalize_m1655992953(__this, method) ((  void (*) (ThreadLocal_1_t241108632 *, const MethodInfo*))ThreadLocal_1_Finalize_m1655992953_gshared)(__this, method)
// System.Void System.Threading.ThreadLocal`1<System.Object>::CheckDisposed()
extern "C"  void ThreadLocal_1_CheckDisposed_m689976844_gshared (ThreadLocal_1_t241108632 * __this, const MethodInfo* method);
#define ThreadLocal_1_CheckDisposed_m689976844(__this, method) ((  void (*) (ThreadLocal_1_t241108632 *, const MethodInfo*))ThreadLocal_1_CheckDisposed_m689976844_gshared)(__this, method)
// System.Void System.Threading.ThreadLocal`1<System.Object>::Dispose()
extern "C"  void ThreadLocal_1_Dispose_m163491326_gshared (ThreadLocal_1_t241108632 * __this, const MethodInfo* method);
#define ThreadLocal_1_Dispose_m163491326(__this, method) ((  void (*) (ThreadLocal_1_t241108632 *, const MethodInfo*))ThreadLocal_1_Dispose_m163491326_gshared)(__this, method)
// System.Void System.Threading.ThreadLocal`1<System.Object>::.cctor()
extern "C"  void ThreadLocal_1__cctor_m1543018514_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ThreadLocal_1__cctor_m1543018514(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ThreadLocal_1__cctor_m1543018514_gshared)(__this /* static, unused */, method)
