﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.ThreadLocal`1<System.Int32>
struct ThreadLocal_1_t3918504081;
// System.Func`1<System.Int32>
struct Func_1_t4026270130;
// System.Collections.Generic.IDictionary`2<System.Int64,System.Int32>
struct IDictionary_2_t1060479271;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Threading.ThreadLocal`1<System.Int32>::.ctor(System.Func`1<T>)
extern "C"  void ThreadLocal_1__ctor_m1914502561_gshared (ThreadLocal_1_t3918504081 * __this, Func_1_t4026270130 * ___valueFactory0, const MethodInfo* method);
#define ThreadLocal_1__ctor_m1914502561(__this, ___valueFactory0, method) ((  void (*) (ThreadLocal_1_t3918504081 *, Func_1_t4026270130 *, const MethodInfo*))ThreadLocal_1__ctor_m1914502561_gshared)(__this, ___valueFactory0, method)
// System.Collections.Generic.IDictionary`2<System.Int64,T> System.Threading.ThreadLocal`1<System.Int32>::get_ThreadLocalData()
extern "C"  Il2CppObject* ThreadLocal_1_get_ThreadLocalData_m10338229_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ThreadLocal_1_get_ThreadLocalData_m10338229(__this /* static, unused */, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ThreadLocal_1_get_ThreadLocalData_m10338229_gshared)(__this /* static, unused */, method)
// T System.Threading.ThreadLocal`1<System.Int32>::get_Value()
extern "C"  int32_t ThreadLocal_1_get_Value_m1483883472_gshared (ThreadLocal_1_t3918504081 * __this, const MethodInfo* method);
#define ThreadLocal_1_get_Value_m1483883472(__this, method) ((  int32_t (*) (ThreadLocal_1_t3918504081 *, const MethodInfo*))ThreadLocal_1_get_Value_m1483883472_gshared)(__this, method)
// System.Void System.Threading.ThreadLocal`1<System.Int32>::set_Value(T)
extern "C"  void ThreadLocal_1_set_Value_m1952917285_gshared (ThreadLocal_1_t3918504081 * __this, int32_t ___value0, const MethodInfo* method);
#define ThreadLocal_1_set_Value_m1952917285(__this, ___value0, method) ((  void (*) (ThreadLocal_1_t3918504081 *, int32_t, const MethodInfo*))ThreadLocal_1_set_Value_m1952917285_gshared)(__this, ___value0, method)
// System.Void System.Threading.ThreadLocal`1<System.Int32>::Finalize()
extern "C"  void ThreadLocal_1_Finalize_m2096129366_gshared (ThreadLocal_1_t3918504081 * __this, const MethodInfo* method);
#define ThreadLocal_1_Finalize_m2096129366(__this, method) ((  void (*) (ThreadLocal_1_t3918504081 *, const MethodInfo*))ThreadLocal_1_Finalize_m2096129366_gshared)(__this, method)
// System.Void System.Threading.ThreadLocal`1<System.Int32>::CheckDisposed()
extern "C"  void ThreadLocal_1_CheckDisposed_m4187518017_gshared (ThreadLocal_1_t3918504081 * __this, const MethodInfo* method);
#define ThreadLocal_1_CheckDisposed_m4187518017(__this, method) ((  void (*) (ThreadLocal_1_t3918504081 *, const MethodInfo*))ThreadLocal_1_CheckDisposed_m4187518017_gshared)(__this, method)
// System.Void System.Threading.ThreadLocal`1<System.Int32>::Dispose()
extern "C"  void ThreadLocal_1_Dispose_m3582068371_gshared (ThreadLocal_1_t3918504081 * __this, const MethodInfo* method);
#define ThreadLocal_1_Dispose_m3582068371(__this, method) ((  void (*) (ThreadLocal_1_t3918504081 *, const MethodInfo*))ThreadLocal_1_Dispose_m3582068371_gshared)(__this, method)
// System.Void System.Threading.ThreadLocal`1<System.Int32>::.cctor()
extern "C"  void ThreadLocal_1__cctor_m3570105811_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ThreadLocal_1__cctor_m3570105811(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ThreadLocal_1__cctor_m3570105811_gshared)(__this /* static, unused */, method)
