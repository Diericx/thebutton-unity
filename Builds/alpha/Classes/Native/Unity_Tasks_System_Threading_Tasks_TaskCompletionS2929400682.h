﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>
struct Task_1_t3166995609;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser>
struct  TaskCompletionSource_1_t2929400682  : public Il2CppObject
{
public:
	// System.Threading.Tasks.Task`1<T> System.Threading.Tasks.TaskCompletionSource`1::<Task>k__BackingField
	Task_1_t3166995609 * ___U3CTaskU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CTaskU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TaskCompletionSource_1_t2929400682, ___U3CTaskU3Ek__BackingField_0)); }
	inline Task_1_t3166995609 * get_U3CTaskU3Ek__BackingField_0() const { return ___U3CTaskU3Ek__BackingField_0; }
	inline Task_1_t3166995609 ** get_address_of_U3CTaskU3Ek__BackingField_0() { return &___U3CTaskU3Ek__BackingField_0; }
	inline void set_U3CTaskU3Ek__BackingField_0(Task_1_t3166995609 * value)
	{
		___U3CTaskU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTaskU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
