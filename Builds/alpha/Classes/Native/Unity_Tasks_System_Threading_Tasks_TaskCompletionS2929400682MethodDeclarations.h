﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Unity_Tasks_System_Threading_Tasks_TaskCompletionS1571883375MethodDeclarations.h"

// System.Void System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser>::.ctor()
#define TaskCompletionSource_1__ctor_m1654415365(__this, method) ((  void (*) (TaskCompletionSource_1_t2929400682 *, const MethodInfo*))TaskCompletionSource_1__ctor_m2139207987_gshared)(__this, method)
// System.Threading.Tasks.Task`1<T> System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser>::get_Task()
#define TaskCompletionSource_1_get_Task_m601916200(__this, method) ((  Task_1_t3166995609 * (*) (TaskCompletionSource_1_t2929400682 *, const MethodInfo*))TaskCompletionSource_1_get_Task_m1340136548_gshared)(__this, method)
// System.Void System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser>::set_Task(System.Threading.Tasks.Task`1<T>)
#define TaskCompletionSource_1_set_Task_m2802833138(__this, ___value0, method) ((  void (*) (TaskCompletionSource_1_t2929400682 *, Task_1_t3166995609 *, const MethodInfo*))TaskCompletionSource_1_set_Task_m1591719211_gshared)(__this, ___value0, method)
// System.Boolean System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser>::TrySetResult(T)
#define TaskCompletionSource_1_TrySetResult_m1229405325(__this, ___result0, method) ((  bool (*) (TaskCompletionSource_1_t2929400682 *, FirebaseUser_t4046966602 *, const MethodInfo*))TaskCompletionSource_1_TrySetResult_m2533661502_gshared)(__this, ___result0, method)
// System.Boolean System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser>::TrySetException(System.Exception)
#define TaskCompletionSource_1_TrySetException_m2595843995(__this, ___exception0, method) ((  bool (*) (TaskCompletionSource_1_t2929400682 *, Exception_t1927440687 *, const MethodInfo*))TaskCompletionSource_1_TrySetException_m909130698_gshared)(__this, ___exception0, method)
// System.Boolean System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser>::TrySetCanceled()
#define TaskCompletionSource_1_TrySetCanceled_m3573582095(__this, method) ((  bool (*) (TaskCompletionSource_1_t2929400682 *, const MethodInfo*))TaskCompletionSource_1_TrySetCanceled_m2096289934_gshared)(__this, method)
// System.Void System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser>::SetResult(T)
#define TaskCompletionSource_1_SetResult_m210845803(__this, ___result0, method) ((  void (*) (TaskCompletionSource_1_t2929400682 *, FirebaseUser_t4046966602 *, const MethodInfo*))TaskCompletionSource_1_SetResult_m1977579665_gshared)(__this, ___result0, method)
// System.Void System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser>::SetException(System.Exception)
#define TaskCompletionSource_1_SetException_m872764268(__this, ___exception0, method) ((  void (*) (TaskCompletionSource_1_t2929400682 *, Exception_t1927440687 *, const MethodInfo*))TaskCompletionSource_1_SetException_m3931824926_gshared)(__this, ___exception0, method)
// System.Void System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser>::SetCanceled()
#define TaskCompletionSource_1_SetCanceled_m340886328(__this, method) ((  void (*) (TaskCompletionSource_1_t2929400682 *, const MethodInfo*))TaskCompletionSource_1_SetCanceled_m2805990535_gshared)(__this, method)
