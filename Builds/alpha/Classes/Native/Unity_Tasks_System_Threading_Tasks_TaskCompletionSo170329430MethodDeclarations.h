﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Unity_Tasks_System_Threading_Tasks_TaskCompletionS1571883375MethodDeclarations.h"

// System.Void System.Threading.Tasks.TaskCompletionSource`1<Firebase.Database.DataSnapshot>::.ctor()
#define TaskCompletionSource_1__ctor_m1367560829(__this, method) ((  void (*) (TaskCompletionSource_1_t170329430 *, const MethodInfo*))TaskCompletionSource_1__ctor_m2139207987_gshared)(__this, method)
// System.Threading.Tasks.Task`1<T> System.Threading.Tasks.TaskCompletionSource`1<Firebase.Database.DataSnapshot>::get_Task()
#define TaskCompletionSource_1_get_Task_m2444005770(__this, method) ((  Task_1_t407924357 * (*) (TaskCompletionSource_1_t170329430 *, const MethodInfo*))TaskCompletionSource_1_get_Task_m1340136548_gshared)(__this, method)
// System.Void System.Threading.Tasks.TaskCompletionSource`1<Firebase.Database.DataSnapshot>::set_Task(System.Threading.Tasks.Task`1<T>)
#define TaskCompletionSource_1_set_Task_m3111382589(__this, ___value0, method) ((  void (*) (TaskCompletionSource_1_t170329430 *, Task_1_t407924357 *, const MethodInfo*))TaskCompletionSource_1_set_Task_m1591719211_gshared)(__this, ___value0, method)
// System.Boolean System.Threading.Tasks.TaskCompletionSource`1<Firebase.Database.DataSnapshot>::TrySetResult(T)
#define TaskCompletionSource_1_TrySetResult_m3808833468(__this, ___result0, method) ((  bool (*) (TaskCompletionSource_1_t170329430 *, DataSnapshot_t1287895350 *, const MethodInfo*))TaskCompletionSource_1_TrySetResult_m2533661502_gshared)(__this, ___result0, method)
// System.Boolean System.Threading.Tasks.TaskCompletionSource`1<Firebase.Database.DataSnapshot>::TrySetException(System.Exception)
#define TaskCompletionSource_1_TrySetException_m1285839088(__this, ___exception0, method) ((  bool (*) (TaskCompletionSource_1_t170329430 *, Exception_t1927440687 *, const MethodInfo*))TaskCompletionSource_1_TrySetException_m909130698_gshared)(__this, ___exception0, method)
// System.Boolean System.Threading.Tasks.TaskCompletionSource`1<Firebase.Database.DataSnapshot>::TrySetCanceled()
#define TaskCompletionSource_1_TrySetCanceled_m4090866372(__this, method) ((  bool (*) (TaskCompletionSource_1_t170329430 *, const MethodInfo*))TaskCompletionSource_1_TrySetCanceled_m2096289934_gshared)(__this, method)
// System.Void System.Threading.Tasks.TaskCompletionSource`1<Firebase.Database.DataSnapshot>::SetResult(T)
#define TaskCompletionSource_1_SetResult_m2002934015(__this, ___result0, method) ((  void (*) (TaskCompletionSource_1_t170329430 *, DataSnapshot_t1287895350 *, const MethodInfo*))TaskCompletionSource_1_SetResult_m1977579665_gshared)(__this, ___result0, method)
// System.Void System.Threading.Tasks.TaskCompletionSource`1<Firebase.Database.DataSnapshot>::SetException(System.Exception)
#define TaskCompletionSource_1_SetException_m3358577470(__this, ___exception0, method) ((  void (*) (TaskCompletionSource_1_t170329430 *, Exception_t1927440687 *, const MethodInfo*))TaskCompletionSource_1_SetException_m3931824926_gshared)(__this, ___exception0, method)
// System.Void System.Threading.Tasks.TaskCompletionSource`1<Firebase.Database.DataSnapshot>::SetCanceled()
#define TaskCompletionSource_1_SetCanceled_m1495413129(__this, method) ((  void (*) (TaskCompletionSource_1_t170329430 *, const MethodInfo*))TaskCompletionSource_1_SetCanceled_m2805990535_gshared)(__this, method)
