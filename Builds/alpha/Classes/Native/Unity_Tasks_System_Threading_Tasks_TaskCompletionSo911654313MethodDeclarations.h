﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Unity_Tasks_System_Threading_Tasks_TaskCompletionS1571883375MethodDeclarations.h"

// System.Void System.Threading.Tasks.TaskCompletionSource`1<System.String>::.ctor()
#define TaskCompletionSource_1__ctor_m2461165479(__this, method) ((  void (*) (TaskCompletionSource_1_t911654313 *, const MethodInfo*))TaskCompletionSource_1__ctor_m2139207987_gshared)(__this, method)
// System.Threading.Tasks.Task`1<T> System.Threading.Tasks.TaskCompletionSource`1<System.String>::get_Task()
#define TaskCompletionSource_1_get_Task_m3400202330(__this, method) ((  Task_1_t1149249240 * (*) (TaskCompletionSource_1_t911654313 *, const MethodInfo*))TaskCompletionSource_1_get_Task_m1340136548_gshared)(__this, method)
// System.Void System.Threading.Tasks.TaskCompletionSource`1<System.String>::set_Task(System.Threading.Tasks.Task`1<T>)
#define TaskCompletionSource_1_set_Task_m2238247447(__this, ___value0, method) ((  void (*) (TaskCompletionSource_1_t911654313 *, Task_1_t1149249240 *, const MethodInfo*))TaskCompletionSource_1_set_Task_m1591719211_gshared)(__this, ___value0, method)
// System.Boolean System.Threading.Tasks.TaskCompletionSource`1<System.String>::TrySetResult(T)
#define TaskCompletionSource_1_TrySetResult_m3731728244(__this, ___result0, method) ((  bool (*) (TaskCompletionSource_1_t911654313 *, String_t*, const MethodInfo*))TaskCompletionSource_1_TrySetResult_m2533661502_gshared)(__this, ___result0, method)
// System.Boolean System.Threading.Tasks.TaskCompletionSource`1<System.String>::TrySetException(System.Exception)
#define TaskCompletionSource_1_TrySetException_m835211156(__this, ___exception0, method) ((  bool (*) (TaskCompletionSource_1_t911654313 *, Exception_t1927440687 *, const MethodInfo*))TaskCompletionSource_1_TrySetException_m909130698_gshared)(__this, ___exception0, method)
// System.Boolean System.Threading.Tasks.TaskCompletionSource`1<System.String>::TrySetCanceled()
#define TaskCompletionSource_1_TrySetCanceled_m709939496(__this, method) ((  bool (*) (TaskCompletionSource_1_t911654313 *, const MethodInfo*))TaskCompletionSource_1_TrySetCanceled_m2096289934_gshared)(__this, method)
// System.Void System.Threading.Tasks.TaskCompletionSource`1<System.String>::SetResult(T)
#define TaskCompletionSource_1_SetResult_m539871673(__this, ___result0, method) ((  void (*) (TaskCompletionSource_1_t911654313 *, String_t*, const MethodInfo*))TaskCompletionSource_1_SetResult_m1977579665_gshared)(__this, ___result0, method)
// System.Void System.Threading.Tasks.TaskCompletionSource`1<System.String>::SetException(System.Exception)
#define TaskCompletionSource_1_SetException_m3451240888(__this, ___exception0, method) ((  void (*) (TaskCompletionSource_1_t911654313 *, Exception_t1927440687 *, const MethodInfo*))TaskCompletionSource_1_SetException_m3931824926_gshared)(__this, ___exception0, method)
// System.Void System.Threading.Tasks.TaskCompletionSource`1<System.String>::SetCanceled()
#define TaskCompletionSource_1_SetCanceled_m2886845820(__this, method) ((  void (*) (TaskCompletionSource_1_t911654313 *, const MethodInfo*))TaskCompletionSource_1_SetCanceled_m2805990535_gshared)(__this, method)
