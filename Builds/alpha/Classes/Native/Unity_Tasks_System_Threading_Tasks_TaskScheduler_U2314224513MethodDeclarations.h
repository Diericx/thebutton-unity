﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.Tasks.TaskScheduler/<Post>c__AnonStorey0
struct U3CPostU3Ec__AnonStorey0_t2314224513;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Threading.Tasks.TaskScheduler/<Post>c__AnonStorey0::.ctor()
extern "C"  void U3CPostU3Ec__AnonStorey0__ctor_m324866787 (U3CPostU3Ec__AnonStorey0_t2314224513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Tasks.TaskScheduler/<Post>c__AnonStorey0::<>m__0(System.Object)
extern "C"  void U3CPostU3Ec__AnonStorey0_U3CU3Em__0_m3014510984 (U3CPostU3Ec__AnonStorey0_t2314224513 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
