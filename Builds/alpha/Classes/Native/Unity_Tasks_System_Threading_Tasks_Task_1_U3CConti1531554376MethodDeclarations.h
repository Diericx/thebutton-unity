﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.Tasks.Task`1/<ContinueWith>c__AnonStorey0<System.Object>
struct U3CContinueWithU3Ec__AnonStorey0_t1531554376;
// System.Threading.Tasks.Task
struct Task_t1843236107;

#include "codegen/il2cpp-codegen.h"
#include "Unity_Tasks_System_Threading_Tasks_Task1843236107.h"

// System.Void System.Threading.Tasks.Task`1/<ContinueWith>c__AnonStorey0<System.Object>::.ctor()
extern "C"  void U3CContinueWithU3Ec__AnonStorey0__ctor_m1357653564_gshared (U3CContinueWithU3Ec__AnonStorey0_t1531554376 * __this, const MethodInfo* method);
#define U3CContinueWithU3Ec__AnonStorey0__ctor_m1357653564(__this, method) ((  void (*) (U3CContinueWithU3Ec__AnonStorey0_t1531554376 *, const MethodInfo*))U3CContinueWithU3Ec__AnonStorey0__ctor_m1357653564_gshared)(__this, method)
// System.Void System.Threading.Tasks.Task`1/<ContinueWith>c__AnonStorey0<System.Object>::<>m__0(System.Threading.Tasks.Task)
extern "C"  void U3CContinueWithU3Ec__AnonStorey0_U3CU3Em__0_m3966895159_gshared (U3CContinueWithU3Ec__AnonStorey0_t1531554376 * __this, Task_t1843236107 * ___t0, const MethodInfo* method);
#define U3CContinueWithU3Ec__AnonStorey0_U3CU3Em__0_m3966895159(__this, ___t0, method) ((  void (*) (U3CContinueWithU3Ec__AnonStorey0_t1531554376 *, Task_t1843236107 *, const MethodInfo*))U3CContinueWithU3Ec__AnonStorey0_U3CU3Em__0_m3966895159_gshared)(__this, ___t0, method)
