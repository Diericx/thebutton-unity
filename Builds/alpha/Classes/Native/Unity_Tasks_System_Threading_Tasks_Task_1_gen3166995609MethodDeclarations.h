﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Unity_Tasks_System_Threading_Tasks_Task_1_gen1809478302MethodDeclarations.h"

// System.Void System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>::.ctor()
#define Task_1__ctor_m1961287864(__this, method) ((  void (*) (Task_1_t3166995609 *, const MethodInfo*))Task_1__ctor_m2185911839_gshared)(__this, method)
// T System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>::get_Result()
#define Task_1_get_Result_m2363781941(__this, method) ((  FirebaseUser_t4046966602 * (*) (Task_1_t3166995609 *, const MethodInfo*))Task_1_get_Result_m3345291210_gshared)(__this, method)
// System.Threading.Tasks.Task System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>::ContinueWith(System.Action`1<System.Threading.Tasks.Task`1<T>>)
#define Task_1_ContinueWith_m386333399(__this, ___continuation0, method) ((  Task_t1843236107 * (*) (Task_1_t3166995609 *, Action_1_t2968794991 *, const MethodInfo*))Task_1_ContinueWith_m2549062050_gshared)(__this, ___continuation0, method)
// System.Void System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>::RunContinuations()
#define Task_1_RunContinuations_m1073987643(__this, method) ((  void (*) (Task_1_t3166995609 *, const MethodInfo*))Task_1_RunContinuations_m3098339996_gshared)(__this, method)
// System.Boolean System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>::TrySetResult(T)
#define Task_1_TrySetResult_m813664210(__this, ___result0, method) ((  bool (*) (Task_1_t3166995609 *, FirebaseUser_t4046966602 *, const MethodInfo*))Task_1_TrySetResult_m3465015963_gshared)(__this, ___result0, method)
// System.Boolean System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>::TrySetCanceled()
#define Task_1_TrySetCanceled_m992876154(__this, method) ((  bool (*) (Task_1_t3166995609 *, const MethodInfo*))Task_1_TrySetCanceled_m2920752513_gshared)(__this, method)
// System.Boolean System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>::TrySetException(System.AggregateException)
#define Task_1_TrySetException_m1106594819(__this, ___exception0, method) ((  bool (*) (Task_1_t3166995609 *, AggregateException_t420812976 *, const MethodInfo*))Task_1_TrySetException_m2828599492_gshared)(__this, ___exception0, method)
