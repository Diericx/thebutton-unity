﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "Unity_Tasks_System_Threading_Tasks_Task_1_gen1809478302MethodDeclarations.h"

// System.Void System.Threading.Tasks.Task`1<Firebase.Database.DataSnapshot>::.ctor()
#define Task_1__ctor_m2257992917(__this, method) ((  void (*) (Task_1_t407924357 *, const MethodInfo*))Task_1__ctor_m2185911839_gshared)(__this, method)
// T System.Threading.Tasks.Task`1<Firebase.Database.DataSnapshot>::get_Result()
#define Task_1_get_Result_m487472270(__this, method) ((  DataSnapshot_t1287895350 * (*) (Task_1_t407924357 *, const MethodInfo*))Task_1_get_Result_m3345291210_gshared)(__this, method)
// System.Threading.Tasks.Task System.Threading.Tasks.Task`1<Firebase.Database.DataSnapshot>::ContinueWith(System.Action`1<System.Threading.Tasks.Task`1<T>>)
#define Task_1_ContinueWith_m1999459576(__this, ___continuation0, method) ((  Task_t1843236107 * (*) (Task_1_t407924357 *, Action_1_t209723739 *, const MethodInfo*))Task_1_ContinueWith_m2549062050_gshared)(__this, ___continuation0, method)
// System.Void System.Threading.Tasks.Task`1<Firebase.Database.DataSnapshot>::RunContinuations()
#define Task_1_RunContinuations_m3587726580(__this, method) ((  void (*) (Task_1_t407924357 *, const MethodInfo*))Task_1_RunContinuations_m3098339996_gshared)(__this, method)
// System.Boolean System.Threading.Tasks.Task`1<Firebase.Database.DataSnapshot>::TrySetResult(T)
#define Task_1_TrySetResult_m2268322409(__this, ___result0, method) ((  bool (*) (Task_1_t407924357 *, DataSnapshot_t1287895350 *, const MethodInfo*))Task_1_TrySetResult_m3465015963_gshared)(__this, ___result0, method)
// System.Boolean System.Threading.Tasks.Task`1<Firebase.Database.DataSnapshot>::TrySetCanceled()
#define Task_1_TrySetCanceled_m1976330487(__this, method) ((  bool (*) (Task_1_t407924357 *, const MethodInfo*))Task_1_TrySetCanceled_m2920752513_gshared)(__this, method)
// System.Boolean System.Threading.Tasks.Task`1<Firebase.Database.DataSnapshot>::TrySetException(System.AggregateException)
#define Task_1_TrySetException_m1746233584(__this, ___exception0, method) ((  bool (*) (Task_1_t407924357 *, AggregateException_t420812976 *, const MethodInfo*))Task_1_TrySetException_m2828599492_gshared)(__this, ___exception0, method)
