﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey0`1/<ContinueWith>c__AnonStorey1<System.Int32>
struct U3CContinueWithU3Ec__AnonStorey1_t1741657067;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey0`1/<ContinueWith>c__AnonStorey1<System.Int32>::.ctor()
extern "C"  void U3CContinueWithU3Ec__AnonStorey1__ctor_m4094585277_gshared (U3CContinueWithU3Ec__AnonStorey1_t1741657067 * __this, const MethodInfo* method);
#define U3CContinueWithU3Ec__AnonStorey1__ctor_m4094585277(__this, method) ((  void (*) (U3CContinueWithU3Ec__AnonStorey1_t1741657067 *, const MethodInfo*))U3CContinueWithU3Ec__AnonStorey1__ctor_m4094585277_gshared)(__this, method)
// System.Void System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey0`1/<ContinueWith>c__AnonStorey1<System.Int32>::<>m__0()
extern "C"  void U3CContinueWithU3Ec__AnonStorey1_U3CU3Em__0_m1282269014_gshared (U3CContinueWithU3Ec__AnonStorey1_t1741657067 * __this, const MethodInfo* method);
#define U3CContinueWithU3Ec__AnonStorey1_U3CU3Em__0_m1282269014(__this, method) ((  void (*) (U3CContinueWithU3Ec__AnonStorey1_t1741657067 *, const MethodInfo*))U3CContinueWithU3Ec__AnonStorey1_U3CU3Em__0_m1282269014_gshared)(__this, method)
