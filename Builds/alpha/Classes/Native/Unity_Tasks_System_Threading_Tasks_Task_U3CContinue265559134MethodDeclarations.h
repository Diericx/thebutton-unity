﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey0`1<System.Object>
struct U3CContinueWithU3Ec__AnonStorey0_1_t265559134;
// System.Threading.Tasks.Task
struct Task_t1843236107;

#include "codegen/il2cpp-codegen.h"
#include "Unity_Tasks_System_Threading_Tasks_Task1843236107.h"

// System.Void System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey0`1<System.Object>::.ctor()
extern "C"  void U3CContinueWithU3Ec__AnonStorey0_1__ctor_m3850180886_gshared (U3CContinueWithU3Ec__AnonStorey0_1_t265559134 * __this, const MethodInfo* method);
#define U3CContinueWithU3Ec__AnonStorey0_1__ctor_m3850180886(__this, method) ((  void (*) (U3CContinueWithU3Ec__AnonStorey0_1_t265559134 *, const MethodInfo*))U3CContinueWithU3Ec__AnonStorey0_1__ctor_m3850180886_gshared)(__this, method)
// System.Void System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey0`1<System.Object>::<>m__0()
extern "C"  void U3CContinueWithU3Ec__AnonStorey0_1_U3CU3Em__0_m250480761_gshared (U3CContinueWithU3Ec__AnonStorey0_1_t265559134 * __this, const MethodInfo* method);
#define U3CContinueWithU3Ec__AnonStorey0_1_U3CU3Em__0_m250480761(__this, method) ((  void (*) (U3CContinueWithU3Ec__AnonStorey0_1_t265559134 *, const MethodInfo*))U3CContinueWithU3Ec__AnonStorey0_1_U3CU3Em__0_m250480761_gshared)(__this, method)
// System.Void System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey0`1<System.Object>::<>m__1(System.Threading.Tasks.Task)
extern "C"  void U3CContinueWithU3Ec__AnonStorey0_1_U3CU3Em__1_m115454610_gshared (U3CContinueWithU3Ec__AnonStorey0_1_t265559134 * __this, Task_t1843236107 * ___t0, const MethodInfo* method);
#define U3CContinueWithU3Ec__AnonStorey0_1_U3CU3Em__1_m115454610(__this, ___t0, method) ((  void (*) (U3CContinueWithU3Ec__AnonStorey0_1_t265559134 *, Task_t1843236107 *, const MethodInfo*))U3CContinueWithU3Ec__AnonStorey0_1_U3CU3Em__1_m115454610_gshared)(__this, ___t0, method)
