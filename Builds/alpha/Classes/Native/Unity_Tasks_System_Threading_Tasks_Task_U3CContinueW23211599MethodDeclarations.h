﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey2
struct U3CContinueWithU3Ec__AnonStorey2_t23211599;
// System.Threading.Tasks.Task
struct Task_t1843236107;

#include "codegen/il2cpp-codegen.h"
#include "Unity_Tasks_System_Threading_Tasks_Task1843236107.h"

// System.Void System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey2::.ctor()
extern "C"  void U3CContinueWithU3Ec__AnonStorey2__ctor_m2420418137 (U3CContinueWithU3Ec__AnonStorey2_t23211599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Tasks.Task/<ContinueWith>c__AnonStorey2::<>m__0(System.Threading.Tasks.Task)
extern "C"  int32_t U3CContinueWithU3Ec__AnonStorey2_U3CU3Em__0_m2021650914 (U3CContinueWithU3Ec__AnonStorey2_t23211599 * __this, Task_t1843236107 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
