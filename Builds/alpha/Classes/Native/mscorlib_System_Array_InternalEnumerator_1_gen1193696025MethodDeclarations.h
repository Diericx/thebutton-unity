﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1193696025.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// System.Void System.Array/InternalEnumerator`1<System.Nullable`1<System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3215969007_gshared (InternalEnumerator_1_t1193696025 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3215969007(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1193696025 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3215969007_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Nullable`1<System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m74086415_gshared (InternalEnumerator_1_t1193696025 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m74086415(__this, method) ((  void (*) (InternalEnumerator_1_t1193696025 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m74086415_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Nullable`1<System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1865916591_gshared (InternalEnumerator_1_t1193696025 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1865916591(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1193696025 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1865916591_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Nullable`1<System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m6882692_gshared (InternalEnumerator_1_t1193696025 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m6882692(__this, method) ((  void (*) (InternalEnumerator_1_t1193696025 *, const MethodInfo*))InternalEnumerator_1_Dispose_m6882692_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Nullable`1<System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m253129691_gshared (InternalEnumerator_1_t1193696025 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m253129691(__this, method) ((  bool (*) (InternalEnumerator_1_t1193696025 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m253129691_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Nullable`1<System.Int32>>::get_Current()
extern "C"  Nullable_1_t334943763  InternalEnumerator_1_get_Current_m641925704_gshared (InternalEnumerator_1_t1193696025 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m641925704(__this, method) ((  Nullable_1_t334943763  (*) (InternalEnumerator_1_t1193696025 *, const MethodInfo*))InternalEnumerator_1_get_Current_m641925704_gshared)(__this, method)
