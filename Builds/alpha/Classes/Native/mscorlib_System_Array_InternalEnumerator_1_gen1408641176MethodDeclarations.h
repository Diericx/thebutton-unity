﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3548201557MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<Joystick>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m4248612633(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1408641176 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m853313801_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Joystick>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4122786317(__this, method) ((  void (*) (InternalEnumerator_1_t1408641176 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Joystick>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1033459069(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1408641176 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Joystick>::Dispose()
#define InternalEnumerator_1_Dispose_m618484858(__this, method) ((  void (*) (InternalEnumerator_1_t1408641176 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1636767846_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Joystick>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1414080581(__this, method) ((  bool (*) (InternalEnumerator_1_t1408641176 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1047150157_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Joystick>::get_Current()
#define InternalEnumerator_1_get_Current_m3094641048(__this, method) ((  Joystick_t549888914 * (*) (InternalEnumerator_1_t1408641176 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3206960238_gshared)(__this, method)
