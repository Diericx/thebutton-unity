﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2005914529.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelIden1147162267.h"

// System.Void System.Array/InternalEnumerator`1<PubNubMessaging.Core.ChannelIdentity>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2901382657_gshared (InternalEnumerator_1_t2005914529 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2901382657(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2005914529 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2901382657_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<PubNubMessaging.Core.ChannelIdentity>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m791631653_gshared (InternalEnumerator_1_t2005914529 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m791631653(__this, method) ((  void (*) (InternalEnumerator_1_t2005914529 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m791631653_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<PubNubMessaging.Core.ChannelIdentity>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1587478253_gshared (InternalEnumerator_1_t2005914529 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1587478253(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2005914529 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1587478253_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<PubNubMessaging.Core.ChannelIdentity>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1356736170_gshared (InternalEnumerator_1_t2005914529 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1356736170(__this, method) ((  void (*) (InternalEnumerator_1_t2005914529 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1356736170_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<PubNubMessaging.Core.ChannelIdentity>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4226233821_gshared (InternalEnumerator_1_t2005914529 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m4226233821(__this, method) ((  bool (*) (InternalEnumerator_1_t2005914529 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4226233821_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<PubNubMessaging.Core.ChannelIdentity>::get_Current()
extern "C"  ChannelIdentity_t1147162267  InternalEnumerator_1_get_Current_m2483800584_gshared (InternalEnumerator_1_t2005914529 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2483800584(__this, method) ((  ChannelIdentity_t1147162267  (*) (InternalEnumerator_1_t2005914529 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2483800584_gshared)(__this, method)
