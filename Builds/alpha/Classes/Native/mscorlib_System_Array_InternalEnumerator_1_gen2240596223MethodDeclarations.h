﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2240596223.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21381843961.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1147704010_gshared (InternalEnumerator_1_t2240596223 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1147704010(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2240596223 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1147704010_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2081471538_gshared (InternalEnumerator_1_t2240596223 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2081471538(__this, method) ((  void (*) (InternalEnumerator_1_t2240596223 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2081471538_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1797999628_gshared (InternalEnumerator_1_t2240596223 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1797999628(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2240596223 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1797999628_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2517319879_gshared (InternalEnumerator_1_t2240596223 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2517319879(__this, method) ((  void (*) (InternalEnumerator_1_t2240596223 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2517319879_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m417041794_gshared (InternalEnumerator_1_t2240596223 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m417041794(__this, method) ((  bool (*) (InternalEnumerator_1_t2240596223 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m417041794_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t1381843961  InternalEnumerator_1_get_Current_m3806426275_gshared (InternalEnumerator_1_t2240596223 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3806426275(__this, method) ((  KeyValuePair_2_t1381843961  (*) (InternalEnumerator_1_t2240596223 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3806426275_gshared)(__this, method)
