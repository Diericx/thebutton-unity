﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2838068671.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21979316409.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2244812422_gshared (InternalEnumerator_1_t2838068671 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2244812422(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2838068671 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2244812422_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m514316722_gshared (InternalEnumerator_1_t2838068671 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m514316722(__this, method) ((  void (*) (InternalEnumerator_1_t2838068671 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m514316722_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m821672514_gshared (InternalEnumerator_1_t2838068671 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m821672514(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2838068671 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m821672514_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1897373189_gshared (InternalEnumerator_1_t2838068671 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1897373189(__this, method) ((  void (*) (InternalEnumerator_1_t2838068671 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1897373189_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1006317814_gshared (InternalEnumerator_1_t2838068671 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1006317814(__this, method) ((  bool (*) (InternalEnumerator_1_t2838068671 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1006317814_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>>::get_Current()
extern "C"  KeyValuePair_2_t1979316409  InternalEnumerator_1_get_Current_m3208434877_gshared (InternalEnumerator_1_t2838068671 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3208434877(__this, method) ((  KeyValuePair_2_t1979316409  (*) (InternalEnumerator_1_t2838068671 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3208434877_gshared)(__this, method)
