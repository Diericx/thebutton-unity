﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3963080908.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23104328646.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2687081715_gshared (InternalEnumerator_1_t3963080908 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2687081715(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3963080908 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2687081715_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2644263763_gshared (InternalEnumerator_1_t3963080908 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2644263763(__this, method) ((  void (*) (InternalEnumerator_1_t3963080908 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2644263763_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1416189151_gshared (InternalEnumerator_1_t3963080908 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1416189151(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3963080908 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1416189151_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1132968068_gshared (InternalEnumerator_1_t3963080908 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1132968068(__this, method) ((  void (*) (InternalEnumerator_1_t3963080908 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1132968068_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m411980503_gshared (InternalEnumerator_1_t3963080908 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m411980503(__this, method) ((  bool (*) (InternalEnumerator_1_t3963080908 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m411980503_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t3104328646  InternalEnumerator_1_get_Current_m3054392282_gshared (InternalEnumerator_1_t3963080908 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3054392282(__this, method) ((  KeyValuePair_2_t3104328646  (*) (InternalEnumerator_1_t3963080908 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3054392282_gshared)(__this, method)
