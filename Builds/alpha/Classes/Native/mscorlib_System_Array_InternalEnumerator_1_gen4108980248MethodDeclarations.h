﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4108980248.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"

// System.Void System.Array/InternalEnumerator`1<PubNubMessaging.Core.CurrentRequestType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3953200560_gshared (InternalEnumerator_1_t4108980248 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3953200560(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4108980248 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3953200560_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<PubNubMessaging.Core.CurrentRequestType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m926743980_gshared (InternalEnumerator_1_t4108980248 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m926743980(__this, method) ((  void (*) (InternalEnumerator_1_t4108980248 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m926743980_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<PubNubMessaging.Core.CurrentRequestType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2975573170_gshared (InternalEnumerator_1_t4108980248 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2975573170(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4108980248 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2975573170_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<PubNubMessaging.Core.CurrentRequestType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4005504469_gshared (InternalEnumerator_1_t4108980248 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m4005504469(__this, method) ((  void (*) (InternalEnumerator_1_t4108980248 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4005504469_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<PubNubMessaging.Core.CurrentRequestType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1933234848_gshared (InternalEnumerator_1_t4108980248 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1933234848(__this, method) ((  bool (*) (InternalEnumerator_1_t4108980248 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1933234848_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<PubNubMessaging.Core.CurrentRequestType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3626705029_gshared (InternalEnumerator_1_t4108980248 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3626705029(__this, method) ((  int32_t (*) (InternalEnumerator_1_t4108980248 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3626705029_gshared)(__this, method)
