﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4238481571.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L3379729309.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int64>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1759383132_gshared (InternalEnumerator_1_t4238481571 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1759383132(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4238481571 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1759383132_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int64>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1446484396_gshared (InternalEnumerator_1_t4238481571 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1446484396(__this, method) ((  void (*) (InternalEnumerator_1_t4238481571 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1446484396_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int64>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1855157002_gshared (InternalEnumerator_1_t4238481571 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1855157002(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4238481571 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1855157002_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int64>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3225871575_gshared (InternalEnumerator_1_t4238481571 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3225871575(__this, method) ((  void (*) (InternalEnumerator_1_t4238481571 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3225871575_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int64>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1730139156_gshared (InternalEnumerator_1_t4238481571 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1730139156(__this, method) ((  bool (*) (InternalEnumerator_1_t4238481571 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1730139156_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int64>>::get_Current()
extern "C"  Link_t3379729309  InternalEnumerator_1_get_Current_m1112720851_gshared (InternalEnumerator_1_t4238481571 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1112720851(__this, method) ((  Link_t3379729309  (*) (InternalEnumerator_1_t4238481571 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1112720851_gshared)(__this, method)
