﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>
struct DefaultComparer_t2222283912;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23733013679.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::.ctor()
extern "C"  void DefaultComparer__ctor_m4118406113_gshared (DefaultComparer_t2222283912 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m4118406113(__this, method) ((  void (*) (DefaultComparer_t2222283912 *, const MethodInfo*))DefaultComparer__ctor_m4118406113_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3944517860_gshared (DefaultComparer_t2222283912 * __this, KeyValuePair_2_t3733013679  ___x0, KeyValuePair_2_t3733013679  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m3944517860(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t2222283912 *, KeyValuePair_2_t3733013679 , KeyValuePair_2_t3733013679 , const MethodInfo*))DefaultComparer_Compare_m3944517860_gshared)(__this, ___x0, ___y1, method)
