﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.EventRegistration,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.EventRegistration>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3966492154(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1080810598 *, Dictionary_2_t4055753192 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.EventRegistration,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.EventRegistration>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3879521013(__this, method) ((  Il2CppObject * (*) (Enumerator_t1080810598 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.EventRegistration,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.EventRegistration>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3952631229(__this, method) ((  void (*) (Enumerator_t1080810598 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.EventRegistration,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.EventRegistration>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m476209564(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1080810598 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.EventRegistration,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.EventRegistration>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2651780755(__this, method) ((  Il2CppObject * (*) (Enumerator_t1080810598 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.EventRegistration,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.EventRegistration>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2496526115(__this, method) ((  Il2CppObject * (*) (Enumerator_t1080810598 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.EventRegistration,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.EventRegistration>>::MoveNext()
#define Enumerator_MoveNext_m3273469989(__this, method) ((  bool (*) (Enumerator_t1080810598 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.EventRegistration,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.EventRegistration>>::get_Current()
#define Enumerator_get_Current_m1347274801(__this, method) ((  KeyValuePair_2_t1813098414  (*) (Enumerator_t1080810598 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.EventRegistration,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.EventRegistration>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1242460042(__this, method) ((  EventRegistration_t4222917807 * (*) (Enumerator_t1080810598 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.EventRegistration,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.EventRegistration>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2562591050(__this, method) ((  Il2CppObject* (*) (Enumerator_t1080810598 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.EventRegistration,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.EventRegistration>>::Reset()
#define Enumerator_Reset_m2709348916(__this, method) ((  void (*) (Enumerator_t1080810598 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.EventRegistration,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.EventRegistration>>::VerifyState()
#define Enumerator_VerifyState_m812201107(__this, method) ((  void (*) (Enumerator_t1080810598 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.EventRegistration,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.EventRegistration>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1925156429(__this, method) ((  void (*) (Enumerator_t1080810598 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.EventRegistration,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.EventRegistration>>::Dispose()
#define Enumerator_Dispose_m20990074(__this, method) ((  void (*) (Enumerator_t1080810598 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
