﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Nullable`1<System.Int32>>
struct Dictionary_2_t4221971187;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1247028593.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21979316409.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4026215401_gshared (Enumerator_t1247028593 * __this, Dictionary_2_t4221971187 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m4026215401(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1247028593 *, Dictionary_2_t4221971187 *, const MethodInfo*))Enumerator__ctor_m4026215401_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2974563846_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2974563846(__this, method) ((  Il2CppObject * (*) (Enumerator_t1247028593 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2974563846_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1209144614_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1209144614(__this, method) ((  void (*) (Enumerator_t1247028593 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1209144614_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3651401679_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3651401679(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1247028593 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3651401679_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3577756924_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3577756924(__this, method) ((  Il2CppObject * (*) (Enumerator_t1247028593 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3577756924_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2614586210_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2614586210(__this, method) ((  Il2CppObject * (*) (Enumerator_t1247028593 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2614586210_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m688389906_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m688389906(__this, method) ((  bool (*) (Enumerator_t1247028593 *, const MethodInfo*))Enumerator_MoveNext_m688389906_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::get_Current()
extern "C"  KeyValuePair_2_t1979316409  Enumerator_get_Current_m4069600302_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4069600302(__this, method) ((  KeyValuePair_2_t1979316409  (*) (Enumerator_t1247028593 *, const MethodInfo*))Enumerator_get_Current_m4069600302_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m672484517_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m672484517(__this, method) ((  Il2CppObject * (*) (Enumerator_t1247028593 *, const MethodInfo*))Enumerator_get_CurrentKey_m672484517_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::get_CurrentValue()
extern "C"  Nullable_1_t334943763  Enumerator_get_CurrentValue_m1012672069_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1012672069(__this, method) ((  Nullable_1_t334943763  (*) (Enumerator_t1247028593 *, const MethodInfo*))Enumerator_get_CurrentValue_m1012672069_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::Reset()
extern "C"  void Enumerator_Reset_m2206907019_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2206907019(__this, method) ((  void (*) (Enumerator_t1247028593 *, const MethodInfo*))Enumerator_Reset_m2206907019_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::VerifyState()
extern "C"  void Enumerator_VerifyState_m4272604834_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m4272604834(__this, method) ((  void (*) (Enumerator_t1247028593 *, const MethodInfo*))Enumerator_VerifyState_m4272604834_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3518573238_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3518573238(__this, method) ((  void (*) (Enumerator_t1247028593 *, const MethodInfo*))Enumerator_VerifyCurrent_m3518573238_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Nullable`1<System.Int32>>::Dispose()
extern "C"  void Enumerator_Dispose_m2832749025_gshared (Enumerator_t1247028593 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2832749025(__this, method) ((  void (*) (Enumerator_t1247028593 *, const MethodInfo*))Enumerator_Dispose_m2832749025_gshared)(__this, method)
