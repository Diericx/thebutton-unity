﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QueryParams,Firebase.Database.Internal.Core.View.View>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m832924872(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1327296784 *, Dictionary_2_t7272082 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QueryParams,Firebase.Database.Internal.Core.View.View>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1742603461(__this, method) ((  Il2CppObject * (*) (Enumerator_t1327296784 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QueryParams,Firebase.Database.Internal.Core.View.View>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m927501337(__this, method) ((  void (*) (Enumerator_t1327296784 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QueryParams,Firebase.Database.Internal.Core.View.View>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1562764816(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1327296784 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QueryParams,Firebase.Database.Internal.Core.View.View>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1986803627(__this, method) ((  Il2CppObject * (*) (Enumerator_t1327296784 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QueryParams,Firebase.Database.Internal.Core.View.View>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4044734347(__this, method) ((  Il2CppObject * (*) (Enumerator_t1327296784 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QueryParams,Firebase.Database.Internal.Core.View.View>::MoveNext()
#define Enumerator_MoveNext_m4099994057(__this, method) ((  bool (*) (Enumerator_t1327296784 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QueryParams,Firebase.Database.Internal.Core.View.View>::get_Current()
#define Enumerator_get_Current_m3240361777(__this, method) ((  KeyValuePair_2_t2059584600  (*) (Enumerator_t1327296784 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QueryParams,Firebase.Database.Internal.Core.View.View>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2713419536(__this, method) ((  QueryParams_t526937568 * (*) (Enumerator_t1327296784 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QueryParams,Firebase.Database.Internal.Core.View.View>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m4183793104(__this, method) ((  View_t798282663 * (*) (Enumerator_t1327296784 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QueryParams,Firebase.Database.Internal.Core.View.View>::Reset()
#define Enumerator_Reset_m3591699698(__this, method) ((  void (*) (Enumerator_t1327296784 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QueryParams,Firebase.Database.Internal.Core.View.View>::VerifyState()
#define Enumerator_VerifyState_m4116685247(__this, method) ((  void (*) (Enumerator_t1327296784 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QueryParams,Firebase.Database.Internal.Core.View.View>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1461204801(__this, method) ((  void (*) (Enumerator_t1327296784 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QueryParams,Firebase.Database.Internal.Core.View.View>::Dispose()
#define Enumerator_Dispose_m1141559216(__this, method) ((  void (*) (Enumerator_t1327296784 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
