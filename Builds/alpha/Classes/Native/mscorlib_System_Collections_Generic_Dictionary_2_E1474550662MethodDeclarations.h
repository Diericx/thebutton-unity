﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2133750241(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1474550662 *, Dictionary_2_t154525960 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3137202564(__this, method) ((  Il2CppObject * (*) (Enumerator_t1474550662 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2292792346(__this, method) ((  void (*) (Enumerator_t1474550662 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2891644039(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1474550662 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3471631510(__this, method) ((  Il2CppObject * (*) (Enumerator_t1474550662 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2606342240(__this, method) ((  Il2CppObject * (*) (Enumerator_t1474550662 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::MoveNext()
#define Enumerator_MoveNext_m2929964334(__this, method) ((  bool (*) (Enumerator_t1474550662 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::get_Current()
#define Enumerator_get_Current_m2946179758(__this, method) ((  KeyValuePair_2_t2206838478  (*) (Enumerator_t1474550662 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1439672333(__this, method) ((  Tag_t2439924210 * (*) (Enumerator_t1474550662 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1582305125(__this, method) ((  QuerySpec_t377558711 * (*) (Enumerator_t1474550662 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::Reset()
#define Enumerator_Reset_m2847047223(__this, method) ((  void (*) (Enumerator_t1474550662 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::VerifyState()
#define Enumerator_VerifyState_m1458178250(__this, method) ((  void (*) (Enumerator_t1474550662 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1881632458(__this, method) ((  void (*) (Enumerator_t1474550662 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::Dispose()
#define Enumerator_Dispose_m2509175833(__this, method) ((  void (*) (Enumerator_t1474550662 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
