﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ITestResult>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1362378281(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1565301270 *, Dictionary_2_t245276568 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ITestResult>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m535357174(__this, method) ((  Il2CppObject * (*) (Enumerator_t1565301270 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ITestResult>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2997083016(__this, method) ((  void (*) (Enumerator_t1565301270 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ITestResult>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m703920235(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1565301270 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ITestResult>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1067969068(__this, method) ((  Il2CppObject * (*) (Enumerator_t1565301270 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ITestResult>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m311792890(__this, method) ((  Il2CppObject * (*) (Enumerator_t1565301270 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ITestResult>>::MoveNext()
#define Enumerator_MoveNext_m2793313023(__this, method) ((  bool (*) (Enumerator_t1565301270 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ITestResult>>::get_Current()
#define Enumerator_get_Current_m3177567934(__this, method) ((  KeyValuePair_2_t2297589086  (*) (Enumerator_t1565301270 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ITestResult>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3849443901(__this, method) ((  String_t* (*) (Enumerator_t1565301270 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ITestResult>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m19837317(__this, method) ((  List_1_t2625464602 * (*) (Enumerator_t1565301270 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ITestResult>>::Reset()
#define Enumerator_Reset_m3518370315(__this, method) ((  void (*) (Enumerator_t1565301270 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ITestResult>>::VerifyState()
#define Enumerator_VerifyState_m903217536(__this, method) ((  void (*) (Enumerator_t1565301270 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ITestResult>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3972185016(__this, method) ((  void (*) (Enumerator_t1565301270 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<ITestResult>>::Dispose()
#define Enumerator_Dispose_m3568239513(__this, method) ((  void (*) (Enumerator_t1565301270 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
