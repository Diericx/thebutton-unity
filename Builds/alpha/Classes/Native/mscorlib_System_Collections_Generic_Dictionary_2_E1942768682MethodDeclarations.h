﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3017299632MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.Auth.Future_User/Action>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2380755185(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1942768682 *, Dictionary_2_t622743980 *, const MethodInfo*))Enumerator__ctor_m1702560852_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4120595844(__this, method) ((  Il2CppObject * (*) (Enumerator_t1942768682 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1631145297_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2526636700(__this, method) ((  void (*) (Enumerator_t1942768682 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2828524109_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2319289635(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1942768682 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m345330700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1701178758(__this, method) ((  Il2CppObject * (*) (Enumerator_t1942768682 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1330261287_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3600075176(__this, method) ((  Il2CppObject * (*) (Enumerator_t1942768682 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3853964719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.Auth.Future_User/Action>::MoveNext()
#define Enumerator_MoveNext_m2994816448(__this, method) ((  bool (*) (Enumerator_t1942768682 *, const MethodInfo*))Enumerator_MoveNext_m2770956757_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.Auth.Future_User/Action>::get_Current()
#define Enumerator_get_Current_m1715131028(__this, method) ((  KeyValuePair_2_t2675056498  (*) (Enumerator_t1942768682 *, const MethodInfo*))Enumerator_get_Current_m2230224741_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.Auth.Future_User/Action>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1723801653(__this, method) ((  int32_t (*) (Enumerator_t1942768682 *, const MethodInfo*))Enumerator_get_CurrentKey_m447338908_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.Auth.Future_User/Action>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m310645717(__this, method) ((  Action_t1614918345 * (*) (Enumerator_t1942768682 *, const MethodInfo*))Enumerator_get_CurrentValue_m3562053380_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.Auth.Future_User/Action>::Reset()
#define Enumerator_Reset_m3165349655(__this, method) ((  void (*) (Enumerator_t1942768682 *, const MethodInfo*))Enumerator_Reset_m761796566_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.Auth.Future_User/Action>::VerifyState()
#define Enumerator_VerifyState_m1961975672(__this, method) ((  void (*) (Enumerator_t1942768682 *, const MethodInfo*))Enumerator_VerifyState_m2118679243_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.Auth.Future_User/Action>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1529013836(__this, method) ((  void (*) (Enumerator_t1942768682 *, const MethodInfo*))Enumerator_VerifyCurrent_m4246196125_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Firebase.Auth.Future_User/Action>::Dispose()
#define Enumerator_Dispose_m3182447057(__this, method) ((  void (*) (Enumerator_t1942768682 *, const MethodInfo*))Enumerator_Dispose_m2243145188_gshared)(__this, method)
