﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>
struct Dictionary_2_t1052016128;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2372040830.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23104328646.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m276105898_gshared (Enumerator_t2372040830 * __this, Dictionary_2_t1052016128 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m276105898(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2372040830 *, Dictionary_2_t1052016128 *, const MethodInfo*))Enumerator__ctor_m276105898_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m517611691_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m517611691(__this, method) ((  Il2CppObject * (*) (Enumerator_t2372040830 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m517611691_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2745913687_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2745913687(__this, method) ((  void (*) (Enumerator_t2372040830 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2745913687_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m388892492_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m388892492(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2372040830 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m388892492_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3374398445_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3374398445(__this, method) ((  Il2CppObject * (*) (Enumerator_t2372040830 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3374398445_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2248508013_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2248508013(__this, method) ((  Il2CppObject * (*) (Enumerator_t2372040830 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2248508013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3717365715_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3717365715(__this, method) ((  bool (*) (Enumerator_t2372040830 *, const MethodInfo*))Enumerator_MoveNext_m3717365715_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t3104328646  Enumerator_get_Current_m2135551627_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2135551627(__this, method) ((  KeyValuePair_2_t3104328646  (*) (Enumerator_t2372040830 *, const MethodInfo*))Enumerator_get_Current_m2135551627_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m103879330_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m103879330(__this, method) ((  int32_t (*) (Enumerator_t2372040830 *, const MethodInfo*))Enumerator_get_CurrentKey_m103879330_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1777489826_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1777489826(__this, method) ((  Il2CppObject * (*) (Enumerator_t2372040830 *, const MethodInfo*))Enumerator_get_CurrentValue_m1777489826_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m1143911664_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1143911664(__this, method) ((  void (*) (Enumerator_t2372040830 *, const MethodInfo*))Enumerator_Reset_m1143911664_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1995051793_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1995051793(__this, method) ((  void (*) (Enumerator_t2372040830 *, const MethodInfo*))Enumerator_VerifyState_m1995051793_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3176930503_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3176930503(__this, method) ((  void (*) (Enumerator_t2372040830 *, const MethodInfo*))Enumerator_VerifyCurrent_m3176930503_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3484064310_gshared (Enumerator_t2372040830 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3484064310(__this, method) ((  void (*) (Enumerator_t2372040830 *, const MethodInfo*))Enumerator_Dispose_m3484064310_gshared)(__this, method)
