﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.View.Change>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2642733892(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2723822542 *, Dictionary_2_t1403797840 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.View.Change>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1717801953(__this, method) ((  Il2CppObject * (*) (Enumerator_t2723822542 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.View.Change>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m921515669(__this, method) ((  void (*) (Enumerator_t2723822542 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.View.Change>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2512012836(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2723822542 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.View.Change>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m825555023(__this, method) ((  Il2CppObject * (*) (Enumerator_t2723822542 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.View.Change>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1821947919(__this, method) ((  Il2CppObject * (*) (Enumerator_t2723822542 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.View.Change>::MoveNext()
#define Enumerator_MoveNext_m950819357(__this, method) ((  bool (*) (Enumerator_t2723822542 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.View.Change>::get_Current()
#define Enumerator_get_Current_m357640509(__this, method) ((  KeyValuePair_2_t3456110358  (*) (Enumerator_t2723822542 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.View.Change>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m699520476(__this, method) ((  ChildKey_t1197802383 * (*) (Enumerator_t2723822542 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.View.Change>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m705856388(__this, method) ((  Change_t639587248 * (*) (Enumerator_t2723822542 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.View.Change>::Reset()
#define Enumerator_Reset_m2750562286(__this, method) ((  void (*) (Enumerator_t2723822542 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.View.Change>::VerifyState()
#define Enumerator_VerifyState_m1317356731(__this, method) ((  void (*) (Enumerator_t2723822542 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.View.Change>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m613365701(__this, method) ((  void (*) (Enumerator_t2723822542 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.View.Change>::Dispose()
#define Enumerator_Dispose_m4020825196(__this, method) ((  void (*) (Enumerator_t2723822542 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
