﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4273254806(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2984318528 *, Dictionary_2_t1664293826 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4229400025(__this, method) ((  Il2CppObject * (*) (Enumerator_t2984318528 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2895509861(__this, method) ((  void (*) (Enumerator_t2984318528 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m413219158(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2984318528 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2904508843(__this, method) ((  Il2CppObject * (*) (Enumerator_t2984318528 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3860914027(__this, method) ((  Il2CppObject * (*) (Enumerator_t2984318528 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::MoveNext()
#define Enumerator_MoveNext_m1676418269(__this, method) ((  bool (*) (Enumerator_t2984318528 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::get_Current()
#define Enumerator_get_Current_m4038419229(__this, method) ((  KeyValuePair_2_t3716606344  (*) (Enumerator_t2984318528 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3637657630(__this, method) ((  FirebaseApp_t210707726 * (*) (Enumerator_t2984318528 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m479163574(__this, method) ((  X509CertificateCollection_t1197680765 * (*) (Enumerator_t2984318528 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::Reset()
#define Enumerator_Reset_m3612796204(__this, method) ((  void (*) (Enumerator_t2984318528 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::VerifyState()
#define Enumerator_VerifyState_m1886706015(__this, method) ((  void (*) (Enumerator_t2984318528 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1485300629(__this, method) ((  void (*) (Enumerator_t2984318528 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.FirebaseApp,System.Security.Cryptography.X509Certificates.X509CertificateCollection>::Dispose()
#define Enumerator_Dispose_m2328688622(__this, method) ((  void (*) (Enumerator_t2984318528 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
