﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2483095016(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3258067040 *, Dictionary_2_t1938042338 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m518133719(__this, method) ((  Il2CppObject * (*) (Enumerator_t3258067040 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2048864883(__this, method) ((  void (*) (Enumerator_t3258067040 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2742638810(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3258067040 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1671038281(__this, method) ((  Il2CppObject * (*) (Enumerator_t3258067040 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3760444321(__this, method) ((  Il2CppObject * (*) (Enumerator_t3258067040 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen>::MoveNext()
#define Enumerator_MoveNext_m3684073815(__this, method) ((  bool (*) (Enumerator_t3258067040 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen>::get_Current()
#define Enumerator_get_Current_m3895647231(__this, method) ((  KeyValuePair_2_t3990354856  (*) (Enumerator_t3258067040 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2560860208(__this, method) ((  ListenQuerySpec_t2050960365 * (*) (Enumerator_t3258067040 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2914377840(__this, method) ((  OutstandingListen_t67058648 * (*) (Enumerator_t3258067040 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen>::Reset()
#define Enumerator_Reset_m561622190(__this, method) ((  void (*) (Enumerator_t3258067040 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen>::VerifyState()
#define Enumerator_VerifyState_m1073774277(__this, method) ((  void (*) (Enumerator_t3258067040 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3781376099(__this, method) ((  void (*) (Enumerator_t3258067040 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen>::Dispose()
#define Enumerator_Dispose_m3556728228(__this, method) ((  void (*) (Enumerator_t3258067040 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
