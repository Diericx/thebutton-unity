﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Snapshot.Node>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2099418489(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3269590388 *, Dictionary_2_t1949565686 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Snapshot.Node>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m328091044(__this, method) ((  Il2CppObject * (*) (Enumerator_t3269590388 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Snapshot.Node>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4060490724(__this, method) ((  void (*) (Enumerator_t3269590388 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Snapshot.Node>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1568072287(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3269590388 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Snapshot.Node>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2158384338(__this, method) ((  Il2CppObject * (*) (Enumerator_t3269590388 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Snapshot.Node>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2396923732(__this, method) ((  Il2CppObject * (*) (Enumerator_t3269590388 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Snapshot.Node>::MoveNext()
#define Enumerator_MoveNext_m3761870600(__this, method) ((  bool (*) (Enumerator_t3269590388 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Snapshot.Node>::get_Current()
#define Enumerator_get_Current_m2789309508(__this, method) ((  KeyValuePair_2_t4001878204  (*) (Enumerator_t3269590388 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Snapshot.Node>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1468327829(__this, method) ((  Path_t2568473163 * (*) (Enumerator_t3269590388 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Snapshot.Node>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1564988405(__this, method) ((  Node_t2640059010 * (*) (Enumerator_t3269590388 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Snapshot.Node>::Reset()
#define Enumerator_Reset_m2648194587(__this, method) ((  void (*) (Enumerator_t3269590388 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Snapshot.Node>::VerifyState()
#define Enumerator_VerifyState_m1319426556(__this, method) ((  void (*) (Enumerator_t3269590388 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Snapshot.Node>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2407846124(__this, method) ((  void (*) (Enumerator_t3269590388 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Snapshot.Node>::Dispose()
#define Enumerator_Dispose_m3661190833(__this, method) ((  void (*) (Enumerator_t3269590388 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
