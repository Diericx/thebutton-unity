﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.FirebaseApp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m938085710(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3445511690 *, Dictionary_2_t2125486988 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.FirebaseApp>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3697767231(__this, method) ((  Il2CppObject * (*) (Enumerator_t3445511690 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.FirebaseApp>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4068611103(__this, method) ((  void (*) (Enumerator_t3445511690 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.FirebaseApp>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m205851678(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3445511690 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.FirebaseApp>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2857476469(__this, method) ((  Il2CppObject * (*) (Enumerator_t3445511690 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.FirebaseApp>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m630032981(__this, method) ((  Il2CppObject * (*) (Enumerator_t3445511690 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.FirebaseApp>::MoveNext()
#define Enumerator_MoveNext_m4189361259(__this, method) ((  bool (*) (Enumerator_t3445511690 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.FirebaseApp>::get_Current()
#define Enumerator_get_Current_m1101895267(__this, method) ((  KeyValuePair_2_t4177799506  (*) (Enumerator_t3445511690 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.FirebaseApp>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m93234286(__this, method) ((  String_t* (*) (Enumerator_t3445511690 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.FirebaseApp>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2082957022(__this, method) ((  FirebaseApp_t210707726 * (*) (Enumerator_t3445511690 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.FirebaseApp>::Reset()
#define Enumerator_Reset_m1191618616(__this, method) ((  void (*) (Enumerator_t3445511690 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.FirebaseApp>::VerifyState()
#define Enumerator_VerifyState_m329566725(__this, method) ((  void (*) (Enumerator_t3445511690 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.FirebaseApp>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2364255879(__this, method) ((  void (*) (Enumerator_t3445511690 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.FirebaseApp>::Dispose()
#define Enumerator_Dispose_m3516471082(__this, method) ((  void (*) (Enumerator_t3445511690 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
