﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1247028593MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Nullable`1<System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2328104933(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3569747727 *, Dictionary_2_t2249723025 *, const MethodInfo*))Enumerator__ctor_m4026215401_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Nullable`1<System.Int32>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3209771920(__this, method) ((  Il2CppObject * (*) (Enumerator_t3569747727 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2974563846_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Nullable`1<System.Int32>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1393670608(__this, method) ((  void (*) (Enumerator_t3569747727 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1209144614_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Nullable`1<System.Int32>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2762763107(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3569747727 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3651401679_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Nullable`1<System.Int32>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2719765670(__this, method) ((  Il2CppObject * (*) (Enumerator_t3569747727 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3577756924_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Nullable`1<System.Int32>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3742635400(__this, method) ((  Il2CppObject * (*) (Enumerator_t3569747727 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2614586210_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Nullable`1<System.Int32>>::MoveNext()
#define Enumerator_MoveNext_m2228587308(__this, method) ((  bool (*) (Enumerator_t3569747727 *, const MethodInfo*))Enumerator_MoveNext_m688389906_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Nullable`1<System.Int32>>::get_Current()
#define Enumerator_get_Current_m2236771368(__this, method) ((  KeyValuePair_2_t7068247  (*) (Enumerator_t3569747727 *, const MethodInfo*))Enumerator_get_Current_m4069600302_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Nullable`1<System.Int32>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m261490489(__this, method) ((  String_t* (*) (Enumerator_t3569747727 *, const MethodInfo*))Enumerator_get_CurrentKey_m672484517_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Nullable`1<System.Int32>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m4113315417(__this, method) ((  Nullable_1_t334943763  (*) (Enumerator_t3569747727 *, const MethodInfo*))Enumerator_get_CurrentValue_m1012672069_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Nullable`1<System.Int32>>::Reset()
#define Enumerator_Reset_m4057178759(__this, method) ((  void (*) (Enumerator_t3569747727 *, const MethodInfo*))Enumerator_Reset_m2206907019_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Nullable`1<System.Int32>>::VerifyState()
#define Enumerator_VerifyState_m1151436008(__this, method) ((  void (*) (Enumerator_t3569747727 *, const MethodInfo*))Enumerator_VerifyState_m4272604834_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Nullable`1<System.Int32>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1122324992(__this, method) ((  void (*) (Enumerator_t3569747727 *, const MethodInfo*))Enumerator_VerifyCurrent_m3518573238_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Nullable`1<System.Int32>>::Dispose()
#define Enumerator_Dispose_m2858669661(__this, method) ((  void (*) (Enumerator_t3569747727 *, const MethodInfo*))Enumerator_Dispose_m2832749025_gshared)(__this, method)
