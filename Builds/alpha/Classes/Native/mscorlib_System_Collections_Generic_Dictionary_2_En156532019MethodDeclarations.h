﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>
struct Dictionary_2_t3131474613;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En156532019.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_888819835.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1897170543_gshared (Enumerator_t156532019 * __this, Dictionary_2_t3131474613 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1897170543(__this, ___dictionary0, method) ((  void (*) (Enumerator_t156532019 *, Dictionary_2_t3131474613 *, const MethodInfo*))Enumerator__ctor_m1897170543_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2389891262_gshared (Enumerator_t156532019 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2389891262(__this, method) ((  Il2CppObject * (*) (Enumerator_t156532019 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2389891262_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3302454658_gshared (Enumerator_t156532019 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3302454658(__this, method) ((  void (*) (Enumerator_t156532019 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3302454658_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4077548725_gshared (Enumerator_t156532019 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4077548725(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t156532019 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4077548725_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2319419488_gshared (Enumerator_t156532019 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2319419488(__this, method) ((  Il2CppObject * (*) (Enumerator_t156532019 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2319419488_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m120250090_gshared (Enumerator_t156532019 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m120250090(__this, method) ((  Il2CppObject * (*) (Enumerator_t156532019 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m120250090_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m266619810_gshared (Enumerator_t156532019 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m266619810(__this, method) ((  bool (*) (Enumerator_t156532019 *, const MethodInfo*))Enumerator_MoveNext_m266619810_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t888819835  Enumerator_get_Current_m3216113338_gshared (Enumerator_t156532019 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3216113338(__this, method) ((  KeyValuePair_2_t888819835  (*) (Enumerator_t156532019 *, const MethodInfo*))Enumerator_get_Current_m3216113338_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::get_CurrentKey()
extern "C"  IntPtr_t Enumerator_get_CurrentKey_m2387752419_gshared (Enumerator_t156532019 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m2387752419(__this, method) ((  IntPtr_t (*) (Enumerator_t156532019 *, const MethodInfo*))Enumerator_get_CurrentKey_m2387752419_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3147984131_gshared (Enumerator_t156532019 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m3147984131(__this, method) ((  Il2CppObject * (*) (Enumerator_t156532019 *, const MethodInfo*))Enumerator_get_CurrentValue_m3147984131_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m1147555085_gshared (Enumerator_t156532019 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1147555085(__this, method) ((  void (*) (Enumerator_t156532019 *, const MethodInfo*))Enumerator_Reset_m1147555085_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1547102902_gshared (Enumerator_t156532019 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1547102902(__this, method) ((  void (*) (Enumerator_t156532019 *, const MethodInfo*))Enumerator_VerifyState_m1547102902_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m213906106_gshared (Enumerator_t156532019 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m213906106(__this, method) ((  void (*) (Enumerator_t156532019 *, const MethodInfo*))Enumerator_VerifyCurrent_m213906106_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2117437651_gshared (Enumerator_t156532019 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2117437651(__this, method) ((  void (*) (Enumerator_t156532019 *, const MethodInfo*))Enumerator_Dispose_m2117437651_gshared)(__this, method)
