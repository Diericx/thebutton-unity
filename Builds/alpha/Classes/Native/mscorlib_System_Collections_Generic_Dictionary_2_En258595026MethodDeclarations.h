﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.Database.FirebaseDatabase>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3251716397(__this, ___dictionary0, method) ((  void (*) (Enumerator_t258595026 *, Dictionary_2_t3233537620 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.Database.FirebaseDatabase>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4020477138(__this, method) ((  Il2CppObject * (*) (Enumerator_t258595026 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.Database.FirebaseDatabase>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4154334852(__this, method) ((  void (*) (Enumerator_t258595026 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.Database.FirebaseDatabase>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2930384731(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t258595026 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.Database.FirebaseDatabase>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4165254796(__this, method) ((  Il2CppObject * (*) (Enumerator_t258595026 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.Database.FirebaseDatabase>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m516115794(__this, method) ((  Il2CppObject * (*) (Enumerator_t258595026 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.Database.FirebaseDatabase>::MoveNext()
#define Enumerator_MoveNext_m3197857752(__this, method) ((  bool (*) (Enumerator_t258595026 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.Database.FirebaseDatabase>::get_Current()
#define Enumerator_get_Current_m3732040856(__this, method) ((  KeyValuePair_2_t990882842  (*) (Enumerator_t258595026 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.Database.FirebaseDatabase>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m629505777(__this, method) ((  String_t* (*) (Enumerator_t258595026 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.Database.FirebaseDatabase>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2913812921(__this, method) ((  FirebaseDatabase_t1318758358 * (*) (Enumerator_t258595026 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.Database.FirebaseDatabase>::Reset()
#define Enumerator_Reset_m1427638795(__this, method) ((  void (*) (Enumerator_t258595026 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.Database.FirebaseDatabase>::VerifyState()
#define Enumerator_VerifyState_m3875775384(__this, method) ((  void (*) (Enumerator_t258595026 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.Database.FirebaseDatabase>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m297066996(__this, method) ((  void (*) (Enumerator_t258595026 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Firebase.Database.FirebaseDatabase>::Dispose()
#define Enumerator_Dispose_m905682093(__this, method) ((  void (*) (Enumerator_t258595026 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
