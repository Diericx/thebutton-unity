﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m992296333(__this, ___dictionary0, method) ((  void (*) (Enumerator_t333243720 *, Dictionary_2_t3308186314 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3246606184(__this, method) ((  Il2CppObject * (*) (Enumerator_t333243720 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1291161206(__this, method) ((  void (*) (Enumerator_t333243720 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2835346175(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t333243720 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m95227502(__this, method) ((  Il2CppObject * (*) (Enumerator_t333243720 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m85956552(__this, method) ((  Il2CppObject * (*) (Enumerator_t333243720 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::MoveNext()
#define Enumerator_MoveNext_m593187202(__this, method) ((  bool (*) (Enumerator_t333243720 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::get_Current()
#define Enumerator_get_Current_m3760258050(__this, method) ((  KeyValuePair_2_t1065531536  (*) (Enumerator_t333243720 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3825101161(__this, method) ((  QuerySpec_t377558711 * (*) (Enumerator_t333243720 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m319859937(__this, method) ((  Tag_t2439924210 * (*) (Enumerator_t333243720 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::Reset()
#define Enumerator_Reset_m397313551(__this, method) ((  void (*) (Enumerator_t333243720 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::VerifyState()
#define Enumerator_VerifyState_m249557218(__this, method) ((  void (*) (Enumerator_t333243720 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2901590246(__this, method) ((  void (*) (Enumerator_t333243720 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::Dispose()
#define Enumerator_Dispose_m3791060525(__this, method) ((  void (*) (Enumerator_t333243720 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
