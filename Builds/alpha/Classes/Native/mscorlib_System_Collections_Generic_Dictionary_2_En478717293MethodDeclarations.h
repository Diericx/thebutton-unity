﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4262048411(__this, ___dictionary0, method) ((  void (*) (Enumerator_t478717293 *, Dictionary_2_t3453659887 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2198525804(__this, method) ((  Il2CppObject * (*) (Enumerator_t478717293 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2705040970(__this, method) ((  void (*) (Enumerator_t478717293 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4141194673(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t478717293 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2355644534(__this, method) ((  Il2CppObject * (*) (Enumerator_t478717293 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1048338740(__this, method) ((  Il2CppObject * (*) (Enumerator_t478717293 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::MoveNext()
#define Enumerator_MoveNext_m1256938234(__this, method) ((  bool (*) (Enumerator_t478717293 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::get_Current()
#define Enumerator_get_Current_m1368213154(__this, method) ((  KeyValuePair_2_t1211005109  (*) (Enumerator_t478717293 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1139247575(__this, method) ((  ChildKey_t1197802383 * (*) (Enumerator_t478717293 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3728301735(__this, method) ((  Il2CppObject * (*) (Enumerator_t478717293 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Reset()
#define Enumerator_Reset_m2548219409(__this, method) ((  void (*) (Enumerator_t478717293 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::VerifyState()
#define Enumerator_VerifyState_m1762732486(__this, method) ((  void (*) (Enumerator_t478717293 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2242200674(__this, method) ((  void (*) (Enumerator_t478717293 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Firebase.Database.Internal.Snapshot.ChildKey,System.Object>::Dispose()
#define Enumerator_Dispose_m3934094271(__this, method) ((  void (*) (Enumerator_t478717293 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
