﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En156532019MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.Auth.FirebaseAuth>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2475983685(__this, ___dictionary0, method) ((  void (*) (Enumerator_t572966623 *, Dictionary_2_t3547909217 *, const MethodInfo*))Enumerator__ctor_m1897170543_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m457957312(__this, method) ((  Il2CppObject * (*) (Enumerator_t572966623 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2389891262_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3024602134(__this, method) ((  void (*) (Enumerator_t572966623 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3302454658_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3969827103(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t572966623 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4077548725_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2102188366(__this, method) ((  Il2CppObject * (*) (Enumerator_t572966623 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2319419488_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2446513008(__this, method) ((  Il2CppObject * (*) (Enumerator_t572966623 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m120250090_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.Auth.FirebaseAuth>::MoveNext()
#define Enumerator_MoveNext_m32390082(__this, method) ((  bool (*) (Enumerator_t572966623 *, const MethodInfo*))Enumerator_MoveNext_m266619810_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.Auth.FirebaseAuth>::get_Current()
#define Enumerator_get_Current_m1944875778(__this, method) ((  KeyValuePair_2_t1305254439  (*) (Enumerator_t572966623 *, const MethodInfo*))Enumerator_get_Current_m3216113338_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.Auth.FirebaseAuth>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3957213665(__this, method) ((  IntPtr_t (*) (Enumerator_t572966623 *, const MethodInfo*))Enumerator_get_CurrentKey_m2387752419_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.Auth.FirebaseAuth>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1467217385(__this, method) ((  FirebaseAuth_t3105883899 * (*) (Enumerator_t572966623 *, const MethodInfo*))Enumerator_get_CurrentValue_m3147984131_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.Auth.FirebaseAuth>::Reset()
#define Enumerator_Reset_m2590355055(__this, method) ((  void (*) (Enumerator_t572966623 *, const MethodInfo*))Enumerator_Reset_m1147555085_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.Auth.FirebaseAuth>::VerifyState()
#define Enumerator_VerifyState_m2857707546(__this, method) ((  void (*) (Enumerator_t572966623 *, const MethodInfo*))Enumerator_VerifyState_m1547102902_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.Auth.FirebaseAuth>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m461456710(__this, method) ((  void (*) (Enumerator_t572966623 *, const MethodInfo*))Enumerator_VerifyCurrent_m213906106_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.IntPtr,Firebase.Auth.FirebaseAuth>::Dispose()
#define Enumerator_Dispose_m129376909(__this, method) ((  void (*) (Enumerator_t572966623 *, const MethodInfo*))Enumerator_Dispose_m2117437651_gshared)(__this, method)
