﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3554329481(__this, ___dictionary0, method) ((  void (*) (Enumerator_t625324529 *, Dictionary_2_t3600267123 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1546860180(__this, method) ((  Il2CppObject * (*) (Enumerator_t625324529 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3739444308(__this, method) ((  void (*) (Enumerator_t625324529 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m558619247(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t625324529 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2598277666(__this, method) ((  Il2CppObject * (*) (Enumerator_t625324529 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1278966500(__this, method) ((  Il2CppObject * (*) (Enumerator_t625324529 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>>::MoveNext()
#define Enumerator_MoveNext_m2074263096(__this, method) ((  bool (*) (Enumerator_t625324529 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>>::get_Current()
#define Enumerator_get_Current_m2957215124(__this, method) ((  KeyValuePair_2_t1357612345  (*) (Enumerator_t625324529 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3827359429(__this, method) ((  Type_t * (*) (Enumerator_t625324529 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m868327013(__this, method) ((  Dictionary_2_t1662909226 * (*) (Enumerator_t625324529 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>>::Reset()
#define Enumerator_Reset_m1738805643(__this, method) ((  void (*) (Enumerator_t625324529 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>>::VerifyState()
#define Enumerator_VerifyState_m3982404492(__this, method) ((  void (*) (Enumerator_t625324529 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m92079356(__this, method) ((  void (*) (Enumerator_t625324529 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Reflection.MemberInfo>>::Dispose()
#define Enumerator_Dispose_m2228435617(__this, method) ((  void (*) (Enumerator_t625324529 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
