﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>
struct Dictionary_2_t3624498739;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En649556145.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21381843961.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelIden1147162267.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3217945947_gshared (Enumerator_t649556145 * __this, Dictionary_2_t3624498739 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3217945947(__this, ___dictionary0, method) ((  void (*) (Enumerator_t649556145 *, Dictionary_2_t3624498739 *, const MethodInfo*))Enumerator__ctor_m3217945947_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1131689836_gshared (Enumerator_t649556145 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1131689836(__this, method) ((  Il2CppObject * (*) (Enumerator_t649556145 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1131689836_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1672675594_gshared (Enumerator_t649556145 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1672675594(__this, method) ((  void (*) (Enumerator_t649556145 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1672675594_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m636749937_gshared (Enumerator_t649556145 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m636749937(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t649556145 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m636749937_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m349546614_gshared (Enumerator_t649556145 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m349546614(__this, method) ((  Il2CppObject * (*) (Enumerator_t649556145 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m349546614_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3566584116_gshared (Enumerator_t649556145 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3566584116(__this, method) ((  Il2CppObject * (*) (Enumerator_t649556145 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3566584116_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2703835770_gshared (Enumerator_t649556145 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2703835770(__this, method) ((  bool (*) (Enumerator_t649556145 *, const MethodInfo*))Enumerator_MoveNext_m2703835770_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t1381843961  Enumerator_get_Current_m1373918882_gshared (Enumerator_t649556145 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1373918882(__this, method) ((  KeyValuePair_2_t1381843961  (*) (Enumerator_t649556145 *, const MethodInfo*))Enumerator_get_Current_m1373918882_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_CurrentKey()
extern "C"  ChannelIdentity_t1147162267  Enumerator_get_CurrentKey_m1881066775_gshared (Enumerator_t649556145 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1881066775(__this, method) ((  ChannelIdentity_t1147162267  (*) (Enumerator_t649556145 *, const MethodInfo*))Enumerator_get_CurrentKey_m1881066775_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3451592935_gshared (Enumerator_t649556145 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m3451592935(__this, method) ((  Il2CppObject * (*) (Enumerator_t649556145 *, const MethodInfo*))Enumerator_get_CurrentValue_m3451592935_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m87609553_gshared (Enumerator_t649556145 * __this, const MethodInfo* method);
#define Enumerator_Reset_m87609553(__this, method) ((  void (*) (Enumerator_t649556145 *, const MethodInfo*))Enumerator_Reset_m87609553_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1450064902_gshared (Enumerator_t649556145 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1450064902(__this, method) ((  void (*) (Enumerator_t649556145 *, const MethodInfo*))Enumerator_VerifyState_m1450064902_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m1085273890_gshared (Enumerator_t649556145 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1085273890(__this, method) ((  void (*) (Enumerator_t649556145 *, const MethodInfo*))Enumerator_VerifyCurrent_m1085273890_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m4288624127_gshared (Enumerator_t649556145 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m4288624127(__this, method) ((  void (*) (Enumerator_t649556145 *, const MethodInfo*))Enumerator_Dispose_m4288624127_gshared)(__this, method)
