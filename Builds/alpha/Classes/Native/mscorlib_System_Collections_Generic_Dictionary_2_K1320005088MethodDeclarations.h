﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>
struct KeyCollection_t1320005088;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>
struct Dictionary_2_t3131474613;
// System.Collections.Generic.IEnumerator`1<System.IntPtr>
struct IEnumerator_1_t4274551732;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1526010755.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m40025794_gshared (KeyCollection_t1320005088 * __this, Dictionary_2_t3131474613 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m40025794(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1320005088 *, Dictionary_2_t3131474613 *, const MethodInfo*))KeyCollection__ctor_m40025794_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m295062388_gshared (KeyCollection_t1320005088 * __this, IntPtr_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m295062388(__this, ___item0, method) ((  void (*) (KeyCollection_t1320005088 *, IntPtr_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m295062388_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1170030795_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1170030795(__this, method) ((  void (*) (KeyCollection_t1320005088 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1170030795_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m671614396_gshared (KeyCollection_t1320005088 * __this, IntPtr_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m671614396(__this, ___item0, method) ((  bool (*) (KeyCollection_t1320005088 *, IntPtr_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m671614396_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3118644019_gshared (KeyCollection_t1320005088 * __this, IntPtr_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3118644019(__this, ___item0, method) ((  bool (*) (KeyCollection_t1320005088 *, IntPtr_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3118644019_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2104224529_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2104224529(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1320005088 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2104224529_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m236144141_gshared (KeyCollection_t1320005088 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m236144141(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1320005088 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m236144141_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2864841844_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2864841844(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1320005088 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2864841844_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2924879015_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2924879015(__this, method) ((  bool (*) (KeyCollection_t1320005088 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2924879015_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1336146065_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1336146065(__this, method) ((  bool (*) (KeyCollection_t1320005088 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1336146065_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1967424173_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1967424173(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1320005088 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1967424173_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3820790603_gshared (KeyCollection_t1320005088 * __this, IntPtrU5BU5D_t169632028* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3820790603(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1320005088 *, IntPtrU5BU5D_t169632028*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3820790603_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1526010755  KeyCollection_GetEnumerator_m349130886_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m349130886(__this, method) ((  Enumerator_t1526010755  (*) (KeyCollection_t1320005088 *, const MethodInfo*))KeyCollection_GetEnumerator_m349130886_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1219588529_gshared (KeyCollection_t1320005088 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m1219588529(__this, method) ((  int32_t (*) (KeyCollection_t1320005088 *, const MethodInfo*))KeyCollection_get_Count_m1219588529_gshared)(__this, method)
