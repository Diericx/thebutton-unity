﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1867498172MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1402304130(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1413842423 *, Dictionary_2_t3225311948 *, const MethodInfo*))KeyCollection__ctor_m3885543862_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2974712452(__this, ___item0, method) ((  void (*) (KeyCollection_t1413842423 *, int64_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3319720888_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1194312591(__this, method) ((  void (*) (KeyCollection_t1413842423 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3601347139_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m276558260(__this, ___item0, method) ((  bool (*) (KeyCollection_t1413842423 *, int64_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2597166232_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m150317479(__this, ___item0, method) ((  bool (*) (KeyCollection_t1413842423 *, int64_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1060868683_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1375880897(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1413842423 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m152152421_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m3945018497(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1413842423 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1539094133_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1515299312(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1413842423 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1351799188_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3449294291(__this, method) ((  bool (*) (KeyCollection_t1413842423 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3534394359_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2208847525(__this, method) ((  bool (*) (KeyCollection_t1413842423 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3200643657_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3160941797(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1413842423 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1440087769_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m4113874911(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1413842423 *, Int64U5BU5D_t717125112*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1238003947_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::GetEnumerator()
#define KeyCollection_GetEnumerator_m364752148(__this, method) ((  Enumerator_t1619848090  (*) (KeyCollection_t1413842423 *, const MethodInfo*))KeyCollection_GetEnumerator_m3687526536_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::get_Count()
#define KeyCollection_get_Count_m1979530121(__this, method) ((  int32_t (*) (KeyCollection_t1413842423 *, const MethodInfo*))KeyCollection_get_Count_m972382317_gshared)(__this, method)
