﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1320005088MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m3205696640(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1736439692 *, Dictionary_2_t3547909217 *, const MethodInfo*))KeyCollection__ctor_m40025794_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4074298546(__this, ___item0, method) ((  void (*) (KeyCollection_t1736439692 *, IntPtr_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m295062388_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3909428505(__this, method) ((  void (*) (KeyCollection_t1736439692 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1170030795_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2368227014(__this, ___item0, method) ((  bool (*) (KeyCollection_t1736439692 *, IntPtr_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m671614396_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m623338525(__this, ___item0, method) ((  bool (*) (KeyCollection_t1736439692 *, IntPtr_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3118644019_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3930385943(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1736439692 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2104224529_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2933621615(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1736439692 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m236144141_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m338079174(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1736439692 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2864841844_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3599461289(__this, method) ((  bool (*) (KeyCollection_t1736439692 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2924879015_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3160141831(__this, method) ((  bool (*) (KeyCollection_t1736439692 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1336146065_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m4241486691(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1736439692 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1967424173_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m3072686693(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1736439692 *, IntPtrU5BU5D_t169632028*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3820790603_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::GetEnumerator()
#define KeyCollection_GetEnumerator_m3424617698(__this, method) ((  Enumerator_t1942445359  (*) (KeyCollection_t1736439692 *, const MethodInfo*))KeyCollection_GetEnumerator_m349130886_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::get_Count()
#define KeyCollection_get_Count_m1008410335(__this, method) ((  int32_t (*) (KeyCollection_t1736439692 *, const MethodInfo*))KeyCollection_get_Count_m1219588529_gshared)(__this, method)
