﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>
struct KeyCollection_t1813029214;
// System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>
struct Dictionary_2_t3624498739;
// System.Collections.Generic.IEnumerator`1<PubNubMessaging.Core.ChannelIdentity>
struct IEnumerator_1_t2917653390;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Core.ChannelIdentity[]
struct ChannelIdentityU5BU5D_t662656154;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelIden1147162267.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2019034881.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m3844954056_gshared (KeyCollection_t1813029214 * __this, Dictionary_2_t3624498739 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m3844954056(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1813029214 *, Dictionary_2_t3624498739 *, const MethodInfo*))KeyCollection__ctor_m3844954056_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1586359978_gshared (KeyCollection_t1813029214 * __this, ChannelIdentity_t1147162267  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1586359978(__this, ___item0, method) ((  void (*) (KeyCollection_t1813029214 *, ChannelIdentity_t1147162267 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1586359978_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m141495059_gshared (KeyCollection_t1813029214 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m141495059(__this, method) ((  void (*) (KeyCollection_t1813029214 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m141495059_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m618768654_gshared (KeyCollection_t1813029214 * __this, ChannelIdentity_t1147162267  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m618768654(__this, ___item0, method) ((  bool (*) (KeyCollection_t1813029214 *, ChannelIdentity_t1147162267 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m618768654_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m381423875_gshared (KeyCollection_t1813029214 * __this, ChannelIdentity_t1147162267  ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m381423875(__this, ___item0, method) ((  bool (*) (KeyCollection_t1813029214 *, ChannelIdentity_t1147162267 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m381423875_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1377390213_gshared (KeyCollection_t1813029214 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1377390213(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1813029214 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1377390213_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3390687461_gshared (KeyCollection_t1813029214 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3390687461(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1813029214 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3390687461_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m752030678_gshared (KeyCollection_t1813029214 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m752030678(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1813029214 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m752030678_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m49129367_gshared (KeyCollection_t1813029214 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m49129367(__this, method) ((  bool (*) (KeyCollection_t1813029214 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m49129367_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3616415873_gshared (KeyCollection_t1813029214 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3616415873(__this, method) ((  bool (*) (KeyCollection_t1813029214 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3616415873_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3690809537_gshared (KeyCollection_t1813029214 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3690809537(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1813029214 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3690809537_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2763756323_gshared (KeyCollection_t1813029214 * __this, ChannelIdentityU5BU5D_t662656154* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2763756323(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1813029214 *, ChannelIdentityU5BU5D_t662656154*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2763756323_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2019034881  KeyCollection_GetEnumerator_m2813588942_gshared (KeyCollection_t1813029214 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m2813588942(__this, method) ((  Enumerator_t2019034881  (*) (KeyCollection_t1813029214 *, const MethodInfo*))KeyCollection_GetEnumerator_m2813588942_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m136356229_gshared (KeyCollection_t1813029214 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m136356229(__this, method) ((  int32_t (*) (KeyCollection_t1813029214 *, const MethodInfo*))KeyCollection_get_Count_m136356229_gshared)(__this, method)
