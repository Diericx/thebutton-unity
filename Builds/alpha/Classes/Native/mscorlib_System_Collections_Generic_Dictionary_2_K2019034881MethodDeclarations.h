﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>
struct Dictionary_2_t3624498739;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2019034881.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelIden1147162267.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1552363313_gshared (Enumerator_t2019034881 * __this, Dictionary_2_t3624498739 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1552363313(__this, ___host0, method) ((  void (*) (Enumerator_t2019034881 *, Dictionary_2_t3624498739 *, const MethodInfo*))Enumerator__ctor_m1552363313_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2221565188_gshared (Enumerator_t2019034881 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2221565188(__this, method) ((  Il2CppObject * (*) (Enumerator_t2019034881 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2221565188_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3085171034_gshared (Enumerator_t2019034881 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3085171034(__this, method) ((  void (*) (Enumerator_t2019034881 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3085171034_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m223457449_gshared (Enumerator_t2019034881 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m223457449(__this, method) ((  void (*) (Enumerator_t2019034881 *, const MethodInfo*))Enumerator_Dispose_m223457449_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m365687422_gshared (Enumerator_t2019034881 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m365687422(__this, method) ((  bool (*) (Enumerator_t2019034881 *, const MethodInfo*))Enumerator_MoveNext_m365687422_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Current()
extern "C"  ChannelIdentity_t1147162267  Enumerator_get_Current_m1630057566_gshared (Enumerator_t2019034881 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1630057566(__this, method) ((  ChannelIdentity_t1147162267  (*) (Enumerator_t2019034881 *, const MethodInfo*))Enumerator_get_Current_m1630057566_gshared)(__this, method)
