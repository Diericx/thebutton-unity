﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Nullable`1<System.Int32>>
struct Dictionary_2_t4221971187;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2616507329.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m556353567_gshared (Enumerator_t2616507329 * __this, Dictionary_2_t4221971187 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m556353567(__this, ___host0, method) ((  void (*) (Enumerator_t2616507329 *, Dictionary_2_t4221971187 *, const MethodInfo*))Enumerator__ctor_m556353567_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3623312238_gshared (Enumerator_t2616507329 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3623312238(__this, method) ((  Il2CppObject * (*) (Enumerator_t2616507329 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3623312238_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3043893426_gshared (Enumerator_t2616507329 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3043893426(__this, method) ((  void (*) (Enumerator_t2616507329 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3043893426_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::Dispose()
extern "C"  void Enumerator_Dispose_m495764931_gshared (Enumerator_t2616507329 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m495764931(__this, method) ((  void (*) (Enumerator_t2616507329 *, const MethodInfo*))Enumerator_Dispose_m495764931_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m623691698_gshared (Enumerator_t2616507329 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m623691698(__this, method) ((  bool (*) (Enumerator_t2616507329 *, const MethodInfo*))Enumerator_MoveNext_m623691698_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1426407534_gshared (Enumerator_t2616507329 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1426407534(__this, method) ((  Il2CppObject * (*) (Enumerator_t2616507329 *, const MethodInfo*))Enumerator_get_Current_m1426407534_gshared)(__this, method)
