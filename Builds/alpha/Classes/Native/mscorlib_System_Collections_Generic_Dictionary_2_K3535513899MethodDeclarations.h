﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>
struct KeyCollection_t3535513899;
// System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>
struct Dictionary_2_t1052016128;
// System.Collections.Generic.IEnumerator`1<PubNubMessaging.Core.CurrentRequestType>
struct IEnumerator_1_t725751813;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// PubNubMessaging.Core.CurrentRequestType[]
struct CurrentRequestTypeU5BU5D_t2385140839;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3741519566.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m3637797411_gshared (KeyCollection_t3535513899 * __this, Dictionary_2_t1052016128 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m3637797411(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3535513899 *, Dictionary_2_t1052016128 *, const MethodInfo*))KeyCollection__ctor_m3637797411_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3195744869_gshared (KeyCollection_t3535513899 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3195744869(__this, ___item0, method) ((  void (*) (KeyCollection_t3535513899 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3195744869_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m487537122_gshared (KeyCollection_t3535513899 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m487537122(__this, method) ((  void (*) (KeyCollection_t3535513899 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m487537122_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1128639255_gshared (KeyCollection_t3535513899 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1128639255(__this, ___item0, method) ((  bool (*) (KeyCollection_t3535513899 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1128639255_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2379875470_gshared (KeyCollection_t3535513899 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2379875470(__this, ___item0, method) ((  bool (*) (KeyCollection_t3535513899 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2379875470_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3003424020_gshared (KeyCollection_t3535513899 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3003424020(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3535513899 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3003424020_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m456001620_gshared (KeyCollection_t3535513899 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m456001620(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3535513899 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m456001620_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4123609649_gshared (KeyCollection_t3535513899 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4123609649(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3535513899 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4123609649_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4118201988_gshared (KeyCollection_t3535513899 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4118201988(__this, method) ((  bool (*) (KeyCollection_t3535513899 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4118201988_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4232312780_gshared (KeyCollection_t3535513899 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4232312780(__this, method) ((  bool (*) (KeyCollection_t3535513899 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4232312780_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m4219179668_gshared (KeyCollection_t3535513899 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m4219179668(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3535513899 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m4219179668_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m3909347418_gshared (KeyCollection_t3535513899 * __this, CurrentRequestTypeU5BU5D_t2385140839* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m3909347418(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3535513899 *, CurrentRequestTypeU5BU5D_t2385140839*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3909347418_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3741519566  KeyCollection_GetEnumerator_m1163147327_gshared (KeyCollection_t3535513899 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1163147327(__this, method) ((  Enumerator_t3741519566  (*) (KeyCollection_t3535513899 *, const MethodInfo*))KeyCollection_GetEnumerator_m1163147327_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3302451028_gshared (KeyCollection_t3535513899 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m3302451028(__this, method) ((  int32_t (*) (KeyCollection_t3535513899 *, const MethodInfo*))KeyCollection_get_Count_m3302451028_gshared)(__this, method)
