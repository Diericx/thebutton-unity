﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>
struct Dictionary_2_t1052016128;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3741519566.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1498955086_gshared (Enumerator_t3741519566 * __this, Dictionary_2_t1052016128 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1498955086(__this, ___host0, method) ((  void (*) (Enumerator_t3741519566 *, Dictionary_2_t1052016128 *, const MethodInfo*))Enumerator__ctor_m1498955086_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2505623353_gshared (Enumerator_t3741519566 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2505623353(__this, method) ((  Il2CppObject * (*) (Enumerator_t3741519566 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2505623353_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4244405393_gshared (Enumerator_t3741519566 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4244405393(__this, method) ((  void (*) (Enumerator_t3741519566 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4244405393_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m535518462_gshared (Enumerator_t3741519566 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m535518462(__this, method) ((  void (*) (Enumerator_t3741519566 *, const MethodInfo*))Enumerator_Dispose_m535518462_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2361184961_gshared (Enumerator_t3741519566 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2361184961(__this, method) ((  bool (*) (Enumerator_t3741519566 *, const MethodInfo*))Enumerator_MoveNext_m2361184961_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m3083734335_gshared (Enumerator_t3741519566 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3083734335(__this, method) ((  int32_t (*) (Enumerator_t3741519566 *, const MethodInfo*))Enumerator_get_Current_m3083734335_gshared)(__this, method)
