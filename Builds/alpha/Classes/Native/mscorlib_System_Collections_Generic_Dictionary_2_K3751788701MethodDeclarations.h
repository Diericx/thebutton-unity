﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke470039898MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.SparseSnapshotTree>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m982579240(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3751788701 *, Dictionary_2_t1268290930 *, const MethodInfo*))KeyCollection__ctor_m4000691336_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.SparseSnapshotTree>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3142504674(__this, ___item0, method) ((  void (*) (KeyCollection_t3751788701 *, ChildKey_t1197802383 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m726860246_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.SparseSnapshotTree>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2042113669(__this, method) ((  void (*) (KeyCollection_t3751788701 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3185000447_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.SparseSnapshotTree>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2167354478(__this, ___item0, method) ((  bool (*) (KeyCollection_t3751788701 *, ChildKey_t1197802383 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m580889838_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.SparseSnapshotTree>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1206124513(__this, ___item0, method) ((  bool (*) (KeyCollection_t3751788701 *, ChildKey_t1197802383 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1818919095_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.SparseSnapshotTree>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2400843431(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3751788701 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m701895513_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.SparseSnapshotTree>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m239679987(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3751788701 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m201091229_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.SparseSnapshotTree>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3501947006(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3751788701 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1743416022_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.SparseSnapshotTree>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2342030741(__this, method) ((  bool (*) (KeyCollection_t3751788701 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m701366755_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.SparseSnapshotTree>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1721293123(__this, method) ((  bool (*) (KeyCollection_t3751788701 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4278618649_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.SparseSnapshotTree>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2005957587(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3751788701 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3348206461_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.SparseSnapshotTree>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m4246469297(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3751788701 *, ChildKeyU5BU5D_t491817302*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1469814847_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.SparseSnapshotTree>::GetEnumerator()
#define KeyCollection_GetEnumerator_m2994622540(__this, method) ((  Enumerator_t3957794368  (*) (KeyCollection_t3751788701 *, const MethodInfo*))KeyCollection_GetEnumerator_m3123493604_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.SparseSnapshotTree>::get_Count()
#define KeyCollection_get_Count_m708500219(__this, method) ((  int32_t (*) (KeyCollection_t3751788701 *, const MethodInfo*))KeyCollection_get_Count_m2913499705_gshared)(__this, method)
