﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1867498172MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingPut>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m2192395860(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t4247832597 *, Dictionary_2_t1764334826 *, const MethodInfo*))KeyCollection__ctor_m3885543862_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingPut>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1682559634(__this, ___item0, method) ((  void (*) (KeyCollection_t4247832597 *, int64_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3319720888_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingPut>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m899079015(__this, method) ((  void (*) (KeyCollection_t4247832597 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3601347139_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingPut>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3642932114(__this, ___item0, method) ((  bool (*) (KeyCollection_t4247832597 *, int64_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2597166232_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingPut>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3338526111(__this, ___item0, method) ((  bool (*) (KeyCollection_t4247832597 *, int64_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1060868683_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingPut>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2328488709(__this, method) ((  Il2CppObject* (*) (KeyCollection_t4247832597 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m152152421_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingPut>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2006341685(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t4247832597 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1539094133_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingPut>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4281452566(__this, method) ((  Il2CppObject * (*) (KeyCollection_t4247832597 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1351799188_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingPut>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2232200307(__this, method) ((  bool (*) (KeyCollection_t4247832597 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3534394359_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingPut>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m941711521(__this, method) ((  bool (*) (KeyCollection_t4247832597 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3200643657_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingPut>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m4186317873(__this, method) ((  Il2CppObject * (*) (KeyCollection_t4247832597 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1440087769_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingPut>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m3839586143(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t4247832597 *, Int64U5BU5D_t717125112*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1238003947_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingPut>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1438393474(__this, method) ((  Enumerator_t158870968  (*) (KeyCollection_t4247832597 *, const MethodInfo*))KeyCollection_GetEnumerator_m3687526536_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingPut>::get_Count()
#define KeyCollection_get_Count_m2983124733(__this, method) ((  int32_t (*) (KeyCollection_t4247832597 *, const MethodInfo*))KeyCollection_get_Count_m972382317_gshared)(__this, method)
