﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2410501662MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Nullable`1<System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m132629632(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t438253500 *, Dictionary_2_t2249723025 *, const MethodInfo*))KeyCollection__ctor_m2043260022_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Nullable`1<System.Int32>>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3056336258(__this, ___item0, method) ((  void (*) (KeyCollection_t438253500 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3428827944_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Nullable`1<System.Int32>>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3996695173(__this, method) ((  void (*) (KeyCollection_t438253500 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3627762673_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Nullable`1<System.Int32>>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3746542926(__this, ___item0, method) ((  bool (*) (KeyCollection_t438253500 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m679768904_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Nullable`1<System.Int32>>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3377088793(__this, ___item0, method) ((  bool (*) (KeyCollection_t438253500 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4140358365_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Nullable`1<System.Int32>>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m627075295(__this, method) ((  Il2CppObject* (*) (KeyCollection_t438253500 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4005946819_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Nullable`1<System.Int32>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1486619707(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t438253500 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3422803479_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Nullable`1<System.Int32>>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2556626438(__this, method) ((  Il2CppObject * (*) (KeyCollection_t438253500 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1277044908_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Nullable`1<System.Int32>>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3149697389(__this, method) ((  bool (*) (KeyCollection_t438253500 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2857207849_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Nullable`1<System.Int32>>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2027806963(__this, method) ((  bool (*) (KeyCollection_t438253500 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m492541175_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Nullable`1<System.Int32>>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m147632299(__this, method) ((  Il2CppObject * (*) (KeyCollection_t438253500 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m330716391_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Nullable`1<System.Int32>>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m2328359425(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t438253500 *, StringU5BU5D_t1642385972*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2605093053_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Nullable`1<System.Int32>>::GetEnumerator()
#define KeyCollection_GetEnumerator_m2287555260(__this, method) ((  Enumerator_t644259167  (*) (KeyCollection_t438253500 *, const MethodInfo*))KeyCollection_GetEnumerator_m1530211906_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Nullable`1<System.Int32>>::get_Count()
#define KeyCollection_get_Count_m1130453755(__this, method) ((  int32_t (*) (KeyCollection_t438253500 *, const MethodInfo*))KeyCollection_get_Count_m3837465343_gshared)(__this, method)
