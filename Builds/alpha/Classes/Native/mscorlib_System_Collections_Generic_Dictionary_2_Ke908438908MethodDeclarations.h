﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>
struct Dictionary_2_t2513902766;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke908438908.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1331665702_gshared (Enumerator_t908438908 * __this, Dictionary_2_t2513902766 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1331665702(__this, ___host0, method) ((  void (*) (Enumerator_t908438908 *, Dictionary_2_t2513902766 *, const MethodInfo*))Enumerator__ctor_m1331665702_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m565527927_gshared (Enumerator_t908438908 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m565527927(__this, method) ((  Il2CppObject * (*) (Enumerator_t908438908 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m565527927_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m955232903_gshared (Enumerator_t908438908 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m955232903(__this, method) ((  void (*) (Enumerator_t908438908 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m955232903_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m464596946_gshared (Enumerator_t908438908 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m464596946(__this, method) ((  void (*) (Enumerator_t908438908 *, const MethodInfo*))Enumerator_Dispose_m464596946_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4172265267_gshared (Enumerator_t908438908 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4172265267(__this, method) ((  bool (*) (Enumerator_t908438908 *, const MethodInfo*))Enumerator_MoveNext_m4172265267_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.IntPtr,System.Int32>::get_Current()
extern "C"  IntPtr_t Enumerator_get_Current_m2541778865_gshared (Enumerator_t908438908 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2541778865(__this, method) ((  IntPtr_t (*) (Enumerator_t908438908 *, const MethodInfo*))Enumerator_get_Current_m2541778865_gshared)(__this, method)
