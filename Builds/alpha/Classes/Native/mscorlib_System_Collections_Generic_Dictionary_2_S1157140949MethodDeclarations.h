﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>
struct ShimEnumerator_t1157140949;
// System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>
struct Dictionary_2_t1052016128;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m883854621_gshared (ShimEnumerator_t1157140949 * __this, Dictionary_2_t1052016128 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m883854621(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1157140949 *, Dictionary_2_t1052016128 *, const MethodInfo*))ShimEnumerator__ctor_m883854621_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2801184868_gshared (ShimEnumerator_t1157140949 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2801184868(__this, method) ((  bool (*) (ShimEnumerator_t1157140949 *, const MethodInfo*))ShimEnumerator_MoveNext_m2801184868_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m307455920_gshared (ShimEnumerator_t1157140949 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m307455920(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t1157140949 *, const MethodInfo*))ShimEnumerator_get_Entry_m307455920_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1543596449_gshared (ShimEnumerator_t1157140949 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1543596449(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1157140949 *, const MethodInfo*))ShimEnumerator_get_Key_m1543596449_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3123648713_gshared (ShimEnumerator_t1157140949 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m3123648713(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1157140949 *, const MethodInfo*))ShimEnumerator_get_Value_m3123648713_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3118454683_gshared (ShimEnumerator_t1157140949 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m3118454683(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1157140949 *, const MethodInfo*))ShimEnumerator_get_Current_m3118454683_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m4237967875_gshared (ShimEnumerator_t1157140949 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m4237967875(__this, method) ((  void (*) (ShimEnumerator_t1157140949 *, const MethodInfo*))ShimEnumerator_Reset_m4237967875_gshared)(__this, method)
