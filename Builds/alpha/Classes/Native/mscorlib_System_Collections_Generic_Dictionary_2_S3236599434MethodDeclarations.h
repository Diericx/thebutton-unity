﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>
struct ShimEnumerator_t3236599434;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>
struct Dictionary_2_t3131474613;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1293350960_gshared (ShimEnumerator_t3236599434 * __this, Dictionary_2_t3131474613 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1293350960(__this, ___host0, method) ((  void (*) (ShimEnumerator_t3236599434 *, Dictionary_2_t3131474613 *, const MethodInfo*))ShimEnumerator__ctor_m1293350960_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m3308190317_gshared (ShimEnumerator_t3236599434 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m3308190317(__this, method) ((  bool (*) (ShimEnumerator_t3236599434 *, const MethodInfo*))ShimEnumerator_MoveNext_m3308190317_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m3789806921_gshared (ShimEnumerator_t3236599434 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3789806921(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t3236599434 *, const MethodInfo*))ShimEnumerator_get_Entry_m3789806921_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1171725158_gshared (ShimEnumerator_t3236599434 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1171725158(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3236599434 *, const MethodInfo*))ShimEnumerator_get_Key_m1171725158_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m635365372_gshared (ShimEnumerator_t3236599434 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m635365372(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3236599434 *, const MethodInfo*))ShimEnumerator_get_Value_m635365372_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3610131784_gshared (ShimEnumerator_t3236599434 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m3610131784(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3236599434 *, const MethodInfo*))ShimEnumerator_get_Current_m3610131784_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.IntPtr,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m2611430510_gshared (ShimEnumerator_t3236599434 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m2611430510(__this, method) ((  void (*) (ShimEnumerator_t3236599434 *, const MethodInfo*))ShimEnumerator_Reset_m2611430510_gshared)(__this, method)
