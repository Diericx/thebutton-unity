﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>
struct ShimEnumerator_t3729623560;
// System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>
struct Dictionary_2_t3624498739;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1914092602_gshared (ShimEnumerator_t3729623560 * __this, Dictionary_2_t3624498739 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1914092602(__this, ___host0, method) ((  void (*) (ShimEnumerator_t3729623560 *, Dictionary_2_t3624498739 *, const MethodInfo*))ShimEnumerator__ctor_m1914092602_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2091134001_gshared (ShimEnumerator_t3729623560 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2091134001(__this, method) ((  bool (*) (ShimEnumerator_t3729623560 *, const MethodInfo*))ShimEnumerator_MoveNext_m2091134001_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m555340501_gshared (ShimEnumerator_t3729623560 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m555340501(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t3729623560 *, const MethodInfo*))ShimEnumerator_get_Entry_m555340501_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m4155475114_gshared (ShimEnumerator_t3729623560 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m4155475114(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3729623560 *, const MethodInfo*))ShimEnumerator_get_Key_m4155475114_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1980378472_gshared (ShimEnumerator_t3729623560 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1980378472(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3729623560 *, const MethodInfo*))ShimEnumerator_get_Value_m1980378472_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2412490000_gshared (ShimEnumerator_t3729623560 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2412490000(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t3729623560 *, const MethodInfo*))ShimEnumerator_get_Current_m2412490000_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m3465475748_gshared (ShimEnumerator_t3729623560 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3465475748(__this, method) ((  void (*) (ShimEnumerator_t3729623560 *, const MethodInfo*))ShimEnumerator_Reset_m3465475748_gshared)(__this, method)
