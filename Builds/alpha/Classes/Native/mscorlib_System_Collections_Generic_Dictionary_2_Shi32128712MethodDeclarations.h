﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Nullable`1<System.Int32>>
struct ShimEnumerator_t32128712;
// System.Collections.Generic.Dictionary`2<System.Object,System.Nullable`1<System.Int32>>
struct Dictionary_2_t4221971187;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Nullable`1<System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m904238124_gshared (ShimEnumerator_t32128712 * __this, Dictionary_2_t4221971187 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m904238124(__this, ___host0, method) ((  void (*) (ShimEnumerator_t32128712 *, Dictionary_2_t4221971187 *, const MethodInfo*))ShimEnumerator__ctor_m904238124_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Nullable`1<System.Int32>>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1815043987_gshared (ShimEnumerator_t32128712 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1815043987(__this, method) ((  bool (*) (ShimEnumerator_t32128712 *, const MethodInfo*))ShimEnumerator_MoveNext_m1815043987_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Nullable`1<System.Int32>>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m1435317939_gshared (ShimEnumerator_t32128712 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1435317939(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t32128712 *, const MethodInfo*))ShimEnumerator_get_Entry_m1435317939_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Nullable`1<System.Int32>>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1526017378_gshared (ShimEnumerator_t32128712 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1526017378(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t32128712 *, const MethodInfo*))ShimEnumerator_get_Key_m1526017378_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Nullable`1<System.Int32>>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1724190548_gshared (ShimEnumerator_t32128712 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1724190548(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t32128712 *, const MethodInfo*))ShimEnumerator_get_Value_m1724190548_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Nullable`1<System.Int32>>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3897818928_gshared (ShimEnumerator_t32128712 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m3897818928(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t32128712 *, const MethodInfo*))ShimEnumerator_get_Current_m3897818928_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Nullable`1<System.Int32>>::Reset()
extern "C"  void ShimEnumerator_Reset_m3158839854_gshared (ShimEnumerator_t32128712 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3158839854(__this, method) ((  void (*) (ShimEnumerator_t32128712 *, const MethodInfo*))ShimEnumerator_Reset_m3158839854_gshared)(__this, method)
