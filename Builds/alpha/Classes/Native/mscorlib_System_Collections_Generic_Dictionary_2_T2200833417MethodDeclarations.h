﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,PubNubMessaging.Core.ChannelIdentity>
struct Transform_1_t2200833417;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelIden1147162267.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,PubNubMessaging.Core.ChannelIdentity>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m901512951_gshared (Transform_1_t2200833417 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m901512951(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t2200833417 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m901512951_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,PubNubMessaging.Core.ChannelIdentity>::Invoke(TKey,TValue)
extern "C"  ChannelIdentity_t1147162267  Transform_1_Invoke_m3488829539_gshared (Transform_1_t2200833417 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m3488829539(__this, ___key0, ___value1, method) ((  ChannelIdentity_t1147162267  (*) (Transform_1_t2200833417 *, ChannelIdentity_t1147162267 , Il2CppObject *, const MethodInfo*))Transform_1_Invoke_m3488829539_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,PubNubMessaging.Core.ChannelIdentity>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1049470850_gshared (Transform_1_t2200833417 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m1049470850(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t2200833417 *, ChannelIdentity_t1147162267 , Il2CppObject *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m1049470850_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,System.Object,PubNubMessaging.Core.ChannelIdentity>::EndInvoke(System.IAsyncResult)
extern "C"  ChannelIdentity_t1147162267  Transform_1_EndInvoke_m1424772001_gshared (Transform_1_t2200833417 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m1424772001(__this, ___result0, method) ((  ChannelIdentity_t1147162267  (*) (Transform_1_t2200833417 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m1424772001_gshared)(__this, ___result0, method)
