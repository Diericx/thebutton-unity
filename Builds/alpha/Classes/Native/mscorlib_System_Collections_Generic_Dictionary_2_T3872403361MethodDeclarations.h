﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2260454664MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m3481643455(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3872403361 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m4161450529_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2025362331(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t3872403361 *, QuerySpec_t377558711 *, Tag_t2439924210 *, const MethodInfo*))Transform_1_Invoke_m2770612589_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m1592174016(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3872403361 *, QuerySpec_t377558711 *, Tag_t2439924210 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m3014766640_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m2393783921(__this, ___result0, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t3872403361 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m803975703_gshared)(__this, ___result0, method)
