﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4102546548MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m3838148381(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t4230356522 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m115431721_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3713239545(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t4230356522 *, ChannelIdentity_t1147162267 , ChannelParameters_t547936593 *, const MethodInfo*))Transform_1_Invoke_m443213181_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m2308362606(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t4230356522 *, ChannelIdentity_t1147162267 , ChannelParameters_t547936593 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m370790320_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m791479527(__this, ___result0, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t4230356522 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m2686178043_gshared)(__this, ___result0, method)
