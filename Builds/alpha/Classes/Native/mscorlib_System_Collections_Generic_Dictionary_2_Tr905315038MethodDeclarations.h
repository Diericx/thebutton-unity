﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr427295924MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Nullable`1<System.Int32>,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m1393259595(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t905315038 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m2129956639_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Nullable`1<System.Int32>,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m738709375(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t905315038 *, String_t*, Nullable_1_t334943763 , const MethodInfo*))Transform_1_Invoke_m2455579403_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Nullable`1<System.Int32>,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m237638996(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t905315038 *, String_t*, Nullable_1_t334943763 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m1988794314_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Nullable`1<System.Int32>,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m406993757(__this, ___result0, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t905315038 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m1232746697_gshared)(__this, ___result0, method)
