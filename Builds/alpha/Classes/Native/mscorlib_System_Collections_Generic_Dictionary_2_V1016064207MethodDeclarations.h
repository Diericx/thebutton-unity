﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>
struct Dictionary_2_t3624498739;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1016064207.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2611792153_gshared (Enumerator_t1016064207 * __this, Dictionary_2_t3624498739 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2611792153(__this, ___host0, method) ((  void (*) (Enumerator_t1016064207 *, Dictionary_2_t3624498739 *, const MethodInfo*))Enumerator__ctor_m2611792153_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3658774814_gshared (Enumerator_t1016064207 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3658774814(__this, method) ((  Il2CppObject * (*) (Enumerator_t1016064207 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3658774814_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3768658744_gshared (Enumerator_t1016064207 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3768658744(__this, method) ((  void (*) (Enumerator_t1016064207 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3768658744_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3582798049_gshared (Enumerator_t1016064207 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3582798049(__this, method) ((  void (*) (Enumerator_t1016064207 *, const MethodInfo*))Enumerator_Dispose_m3582798049_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3174207588_gshared (Enumerator_t1016064207 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3174207588(__this, method) ((  bool (*) (Enumerator_t1016064207 *, const MethodInfo*))Enumerator_MoveNext_m3174207588_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m244971966_gshared (Enumerator_t1016064207 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m244971966(__this, method) ((  Il2CppObject * (*) (Enumerator_t1016064207 *, const MethodInfo*))Enumerator_get_Current_m244971966_gshared)(__this, method)
