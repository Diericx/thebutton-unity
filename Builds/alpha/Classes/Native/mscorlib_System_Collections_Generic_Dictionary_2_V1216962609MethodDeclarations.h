﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Int32>
struct ValueCollection_t1216962609;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>
struct Dictionary_2_t2513902766;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3842368571;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4200435530.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m4182039553_gshared (ValueCollection_t1216962609 * __this, Dictionary_2_t2513902766 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m4182039553(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1216962609 *, Dictionary_2_t2513902766 *, const MethodInfo*))ValueCollection__ctor_m4182039553_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Int32>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1063996163_gshared (ValueCollection_t1216962609 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1063996163(__this, ___item0, method) ((  void (*) (ValueCollection_t1216962609 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1063996163_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Int32>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1361534360_gshared (ValueCollection_t1216962609 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1361534360(__this, method) ((  void (*) (ValueCollection_t1216962609 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1361534360_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Int32>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3215622577_gshared (ValueCollection_t1216962609 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3215622577(__this, ___item0, method) ((  bool (*) (ValueCollection_t1216962609 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3215622577_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Int32>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m347015730_gshared (ValueCollection_t1216962609 * __this, int32_t ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m347015730(__this, ___item0, method) ((  bool (*) (ValueCollection_t1216962609 *, int32_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m347015730_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Int32>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m389254962_gshared (ValueCollection_t1216962609 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m389254962(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1216962609 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m389254962_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m792557856_gshared (ValueCollection_t1216962609 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m792557856(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1216962609 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m792557856_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m378036739_gshared (ValueCollection_t1216962609 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m378036739(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1216962609 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m378036739_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Int32>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m4029687110_gshared (ValueCollection_t1216962609 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m4029687110(__this, method) ((  bool (*) (ValueCollection_t1216962609 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m4029687110_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3926650980_gshared (ValueCollection_t1216962609 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3926650980(__this, method) ((  bool (*) (ValueCollection_t1216962609 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3926650980_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3821389034_gshared (ValueCollection_t1216962609 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3821389034(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1216962609 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3821389034_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Int32>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m3913258068_gshared (ValueCollection_t1216962609 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m3913258068(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1216962609 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3913258068_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t4200435530  ValueCollection_GetEnumerator_m2417641577_gshared (ValueCollection_t1216962609 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m2417641577(__this, method) ((  Enumerator_t4200435530  (*) (ValueCollection_t1216962609 *, const MethodInfo*))ValueCollection_GetEnumerator_m2417641577_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,System.Int32>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m4197143260_gshared (ValueCollection_t1216962609 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m4197143260(__this, method) ((  int32_t (*) (ValueCollection_t1216962609 *, const MethodInfo*))ValueCollection_get_Count_m4197143260_gshared)(__this, method)
