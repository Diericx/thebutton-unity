﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Nullable`1<System.Int32>>
struct Dictionary_2_t4221971187;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1613536655.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1378921111_gshared (Enumerator_t1613536655 * __this, Dictionary_2_t4221971187 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1378921111(__this, ___host0, method) ((  void (*) (Enumerator_t1613536655 *, Dictionary_2_t4221971187 *, const MethodInfo*))Enumerator__ctor_m1378921111_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1317997224_gshared (Enumerator_t1613536655 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1317997224(__this, method) ((  Il2CppObject * (*) (Enumerator_t1613536655 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1317997224_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3216819908_gshared (Enumerator_t1613536655 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3216819908(__this, method) ((  void (*) (Enumerator_t1613536655 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3216819908_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::Dispose()
extern "C"  void Enumerator_Dispose_m1453665971_gshared (Enumerator_t1613536655 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1453665971(__this, method) ((  void (*) (Enumerator_t1613536655 *, const MethodInfo*))Enumerator_Dispose_m1453665971_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3793414252_gshared (Enumerator_t1613536655 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3793414252(__this, method) ((  bool (*) (Enumerator_t1613536655 *, const MethodInfo*))Enumerator_MoveNext_m3793414252_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Nullable`1<System.Int32>>::get_Current()
extern "C"  Nullable_1_t334943763  Enumerator_get_Current_m4077586382_gshared (Enumerator_t1613536655 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4077586382(__this, method) ((  Nullable_1_t334943763  (*) (Enumerator_t1613536655 *, const MethodInfo*))Enumerator_get_Current_m4077586382_gshared)(__this, method)
