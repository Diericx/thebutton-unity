﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2382027540MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m3122962996(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1928371791 *, Dictionary_2_t3225311948 *, const MethodInfo*))ValueCollection__ctor_m2077882560_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m996488870(__this, ___item0, method) ((  void (*) (ValueCollection_t1928371791 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m656178_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1610216351(__this, method) ((  void (*) (ValueCollection_t1928371791 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m979442795_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1021281506(__this, ___item0, method) ((  bool (*) (ValueCollection_t1928371791 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4058548678_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2107148975(__this, ___item0, method) ((  bool (*) (ValueCollection_t1928371791 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3259492947_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m509479489(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1928371791 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1223126429_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m993688193(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1928371791 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3768245709_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3360284370(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1928371791 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m674376046_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1982655899(__this, method) ((  bool (*) (ValueCollection_t1928371791 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3628342391_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2344656573(__this, method) ((  bool (*) (ValueCollection_t1928371791 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4219624793_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3986693165(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1928371791 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m722114041_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1507400031(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1928371791 *, IConnectionRequestCallbackU5BU5D_t1457146383*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1607943379_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1006021012(__this, method) ((  Enumerator_t616877416  (*) (ValueCollection_t1928371791 *, const MethodInfo*))ValueCollection_GetEnumerator_m1386936904_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Int64,Firebase.Database.Internal.Connection.PersistentConnectionImpl/IConnectionRequestCallback>::get_Count()
#define ValueCollection_get_Count_m1577885409(__this, method) ((  int32_t (*) (ValueCollection_t1928371791 *, const MethodInfo*))ValueCollection_get_Count_m2322833661_gshared)(__this, method)
