﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va984569266MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m3144370978(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2011246157 *, Dictionary_2_t3308186314 *, const MethodInfo*))ValueCollection__ctor_m1801851342_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1848352252(__this, ___item0, method) ((  void (*) (ValueCollection_t2011246157 *, Tag_t2439924210 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1477647540_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m502747289(__this, method) ((  void (*) (ValueCollection_t2011246157 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m573646175_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m213914420(__this, ___item0, method) ((  bool (*) (ValueCollection_t2011246157 *, Tag_t2439924210 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1598273024_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2754116733(__this, ___item0, method) ((  bool (*) (ValueCollection_t2011246157 *, Tag_t2439924210 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3764375695_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2307566951(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2011246157 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3036711881_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2660696895(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2011246157 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3792551117_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2531509112(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2011246157 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1773104428_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2952509625(__this, method) ((  bool (*) (ValueCollection_t2011246157 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1530798787_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m405629831(__this, method) ((  bool (*) (ValueCollection_t2011246157 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3044620153_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m867921819(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2011246157 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m919209341_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m3001416725(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2011246157 *, TagU5BU5D_t3799541255*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m927881183_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::GetEnumerator()
#define ValueCollection_GetEnumerator_m2625753306(__this, method) ((  Enumerator_t699751782  (*) (ValueCollection_t2011246157 *, const MethodInfo*))ValueCollection_GetEnumerator_m401908452_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.View.QuerySpec,Firebase.Database.Internal.Core.Tag>::get_Count()
#define ValueCollection_get_Count_m3551421247(__this, method) ((  int32_t (*) (ValueCollection_t2011246157 *, const MethodInfo*))ValueCollection_get_Count_m3718352161_gshared)(__this, method)
