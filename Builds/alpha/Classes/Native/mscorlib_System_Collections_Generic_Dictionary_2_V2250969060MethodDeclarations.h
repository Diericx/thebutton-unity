﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1834534456MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1955320986(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2250969060 *, Dictionary_2_t3547909217 *, const MethodInfo*))ValueCollection__ctor_m1760151388_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4049667036(__this, ___item0, method) ((  void (*) (ValueCollection_t2250969060 *, FirebaseAuth_t3105883899 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4281311350_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3647175825(__this, method) ((  void (*) (ValueCollection_t2250969060 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3471385195_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m507821972(__this, ___item0, method) ((  bool (*) (ValueCollection_t2250969060 *, FirebaseAuth_t3105883899 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3650367146_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3525419973(__this, ___item0, method) ((  bool (*) (ValueCollection_t2250969060 *, FirebaseAuth_t3105883899 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4188992987_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2387990751(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2250969060 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3930535201_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2462728503(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2250969060 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2212134845_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2378952624(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2250969060 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2109355710_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2169759321(__this, method) ((  bool (*) (ValueCollection_t2250969060 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3022741191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m603932391(__this, method) ((  bool (*) (ValueCollection_t2250969060 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2217081553_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1437503187(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2250969060 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3253749389_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m3597795357(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2250969060 *, FirebaseAuthU5BU5D_t3891187386*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m254858027_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::GetEnumerator()
#define ValueCollection_GetEnumerator_m999881058(__this, method) ((  Enumerator_t939474685  (*) (ValueCollection_t2250969060 *, const MethodInfo*))ValueCollection_GetEnumerator_m2859673798_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>::get_Count()
#define ValueCollection_get_Count_m3290189247(__this, method) ((  int32_t (*) (ValueCollection_t2250969060 *, const MethodInfo*))ValueCollection_get_Count_m3277366953_gshared)(__this, method)
