﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>
struct ValueCollection_t2327558582;
// System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>
struct Dictionary_2_t3624498739;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1016064207.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m3849807822_gshared (ValueCollection_t2327558582 * __this, Dictionary_2_t3624498739 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m3849807822(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2327558582 *, Dictionary_2_t3624498739 *, const MethodInfo*))ValueCollection__ctor_m3849807822_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1434557920_gshared (ValueCollection_t2327558582 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1434557920(__this, ___item0, method) ((  void (*) (ValueCollection_t2327558582 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1434557920_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1034920227_gshared (ValueCollection_t2327558582 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1034920227(__this, method) ((  void (*) (ValueCollection_t2327558582 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1034920227_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3358843880_gshared (ValueCollection_t2327558582 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3358843880(__this, ___item0, method) ((  bool (*) (ValueCollection_t2327558582 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3358843880_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3748986739_gshared (ValueCollection_t2327558582 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3748986739(__this, ___item0, method) ((  bool (*) (ValueCollection_t2327558582 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3748986739_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3314830813_gshared (ValueCollection_t2327558582 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3314830813(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2327558582 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3314830813_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2611748477_gshared (ValueCollection_t2327558582 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m2611748477(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2327558582 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2611748477_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2605800780_gshared (ValueCollection_t2327558582 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2605800780(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2327558582 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2605800780_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3625395231_gshared (ValueCollection_t2327558582 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3625395231(__this, method) ((  bool (*) (ValueCollection_t2327558582 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3625395231_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2096315201_gshared (ValueCollection_t2327558582 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2096315201(__this, method) ((  bool (*) (ValueCollection_t2327558582 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2096315201_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3102411441_gshared (ValueCollection_t2327558582 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3102411441(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2327558582 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3102411441_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m263058403_gshared (ValueCollection_t2327558582 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m263058403(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2327558582 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m263058403_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1016064207  ValueCollection_GetEnumerator_m4180477326_gshared (ValueCollection_t2327558582 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m4180477326(__this, method) ((  Enumerator_t1016064207  (*) (ValueCollection_t2327558582 *, const MethodInfo*))ValueCollection_GetEnumerator_m4180477326_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m2906919877_gshared (ValueCollection_t2327558582 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m2906919877(__this, method) ((  int32_t (*) (ValueCollection_t2327558582 *, const MethodInfo*))ValueCollection_get_Count_m2906919877_gshared)(__this, method)
