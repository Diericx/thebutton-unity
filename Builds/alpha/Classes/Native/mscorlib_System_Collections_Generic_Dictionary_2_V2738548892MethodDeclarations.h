﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>
struct Dictionary_2_t1052016128;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2738548892.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3907781068_gshared (Enumerator_t2738548892 * __this, Dictionary_2_t1052016128 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3907781068(__this, ___host0, method) ((  void (*) (Enumerator_t2738548892 *, Dictionary_2_t1052016128 *, const MethodInfo*))Enumerator__ctor_m3907781068_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4185473593_gshared (Enumerator_t2738548892 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m4185473593(__this, method) ((  Il2CppObject * (*) (Enumerator_t2738548892 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4185473593_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3715127049_gshared (Enumerator_t2738548892 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3715127049(__this, method) ((  void (*) (Enumerator_t2738548892 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3715127049_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1824472980_gshared (Enumerator_t2738548892 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1824472980(__this, method) ((  void (*) (Enumerator_t2738548892 *, const MethodInfo*))Enumerator_Dispose_m1824472980_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2888355561_gshared (Enumerator_t2738548892 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2888355561(__this, method) ((  bool (*) (Enumerator_t2738548892 *, const MethodInfo*))Enumerator_MoveNext_m2888355561_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1558578463_gshared (Enumerator_t2738548892 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1558578463(__this, method) ((  Il2CppObject * (*) (Enumerator_t2738548892 *, const MethodInfo*))Enumerator_get_Current_m1558578463_gshared)(__this, method)
