﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Nullable`1<System.Int32>>
struct ValueCollection_t2925031030;
// System.Collections.Generic.Dictionary`2<System.Object,System.Nullable`1<System.Int32>>
struct Dictionary_2_t4221971187;
// System.Collections.Generic.IEnumerator`1<System.Nullable`1<System.Int32>>
struct IEnumerator_1_t2105434886;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Nullable`1<System.Int32>[]
struct Nullable_1U5BU5D_t1781475394;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1613536655.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Nullable`1<System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m2787300724_gshared (ValueCollection_t2925031030 * __this, Dictionary_2_t4221971187 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m2787300724(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2925031030 *, Dictionary_2_t4221971187 *, const MethodInfo*))ValueCollection__ctor_m2787300724_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Nullable`1<System.Int32>>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2821050894_gshared (ValueCollection_t2925031030 * __this, Nullable_1_t334943763  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2821050894(__this, ___item0, method) ((  void (*) (ValueCollection_t2925031030 *, Nullable_1_t334943763 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2821050894_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Nullable`1<System.Int32>>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2396611473_gshared (ValueCollection_t2925031030 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2396611473(__this, method) ((  void (*) (ValueCollection_t2925031030 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2396611473_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Nullable`1<System.Int32>>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3120340138_gshared (ValueCollection_t2925031030 * __this, Nullable_1_t334943763  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3120340138(__this, ___item0, method) ((  bool (*) (ValueCollection_t2925031030 *, Nullable_1_t334943763 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3120340138_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Nullable`1<System.Int32>>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3655682469_gshared (ValueCollection_t2925031030 * __this, Nullable_1_t334943763  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3655682469(__this, ___item0, method) ((  bool (*) (ValueCollection_t2925031030 *, Nullable_1_t334943763 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3655682469_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Nullable`1<System.Int32>>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1637599523_gshared (ValueCollection_t2925031030 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1637599523(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2925031030 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1637599523_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Nullable`1<System.Int32>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m489934271_gshared (ValueCollection_t2925031030 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m489934271(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2925031030 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m489934271_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Nullable`1<System.Int32>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1431982042_gshared (ValueCollection_t2925031030 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1431982042(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2925031030 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1431982042_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Nullable`1<System.Int32>>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m564702409_gshared (ValueCollection_t2925031030 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m564702409(__this, method) ((  bool (*) (ValueCollection_t2925031030 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m564702409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Nullable`1<System.Int32>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1863260351_gshared (ValueCollection_t2925031030 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1863260351(__this, method) ((  bool (*) (ValueCollection_t2925031030 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1863260351_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Nullable`1<System.Int32>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1684233055_gshared (ValueCollection_t2925031030 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1684233055(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2925031030 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1684233055_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Nullable`1<System.Int32>>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m3484540765_gshared (ValueCollection_t2925031030 * __this, Nullable_1U5BU5D_t1781475394* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m3484540765(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2925031030 *, Nullable_1U5BU5D_t1781475394*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3484540765_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Nullable`1<System.Int32>>::GetEnumerator()
extern "C"  Enumerator_t1613536655  ValueCollection_GetEnumerator_m1858427074_gshared (ValueCollection_t2925031030 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1858427074(__this, method) ((  Enumerator_t1613536655  (*) (ValueCollection_t2925031030 *, const MethodInfo*))ValueCollection_GetEnumerator_m1858427074_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Nullable`1<System.Int32>>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1476684799_gshared (ValueCollection_t2925031030 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1476684799(__this, method) ((  int32_t (*) (ValueCollection_t2925031030 *, const MethodInfo*))ValueCollection_get_Count_m1476684799_gshared)(__this, method)
