﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va984569266MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m789386422(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3152553099 *, Dictionary_2_t154525960 *, const MethodInfo*))ValueCollection__ctor_m1801851342_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2104850004(__this, ___item0, method) ((  void (*) (ValueCollection_t3152553099 *, QuerySpec_t377558711 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1477647540_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2887928437(__this, method) ((  void (*) (ValueCollection_t3152553099 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m573646175_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3539936176(__this, ___item0, method) ((  bool (*) (ValueCollection_t3152553099 *, QuerySpec_t377558711 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1598273024_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3434206337(__this, ___item0, method) ((  bool (*) (ValueCollection_t3152553099 *, QuerySpec_t377558711 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3764375695_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m988577423(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3152553099 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3036711881_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m968421527(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3152553099 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3792551117_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2955510176(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3152553099 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1773104428_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m377641997(__this, method) ((  bool (*) (ValueCollection_t3152553099 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1530798787_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3929365695(__this, method) ((  bool (*) (ValueCollection_t3152553099 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3044620153_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2424794131(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3152553099 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m919209341_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m964085993(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3152553099 *, QuerySpecU5BU5D_t595868814*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m927881183_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1850204526(__this, method) ((  Enumerator_t1841058724  (*) (ValueCollection_t3152553099 *, const MethodInfo*))ValueCollection_GetEnumerator_m401908452_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Firebase.Database.Internal.Core.Tag,Firebase.Database.Internal.Core.View.QuerySpec>::get_Count()
#define ValueCollection_get_Count_m2172839847(__this, method) ((  int32_t (*) (ValueCollection_t3152553099 *, const MethodInfo*))ValueCollection_get_Count_m3718352161_gshared)(__this, method)
