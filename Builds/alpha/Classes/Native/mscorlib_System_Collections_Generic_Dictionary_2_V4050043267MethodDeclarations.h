﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>
struct ValueCollection_t4050043267;
// System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>
struct Dictionary_2_t1052016128;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2738548892.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m528907331_gshared (ValueCollection_t4050043267 * __this, Dictionary_2_t1052016128 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m528907331(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t4050043267 *, Dictionary_2_t1052016128 *, const MethodInfo*))ValueCollection__ctor_m528907331_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2997578925_gshared (ValueCollection_t4050043267 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2997578925(__this, ___item0, method) ((  void (*) (ValueCollection_t4050043267 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2997578925_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3985935842_gshared (ValueCollection_t4050043267 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3985935842(__this, method) ((  void (*) (ValueCollection_t4050043267 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3985935842_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2065206015_gshared (ValueCollection_t4050043267 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2065206015(__this, ___item0, method) ((  bool (*) (ValueCollection_t4050043267 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2065206015_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1391941520_gshared (ValueCollection_t4050043267 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1391941520(__this, ___item0, method) ((  bool (*) (ValueCollection_t4050043267 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1391941520_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3863514446_gshared (ValueCollection_t4050043267 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3863514446(__this, method) ((  Il2CppObject* (*) (ValueCollection_t4050043267 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3863514446_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3018122718_gshared (ValueCollection_t4050043267 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3018122718(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4050043267 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3018122718_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4059773577_gshared (ValueCollection_t4050043267 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4059773577(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4050043267 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4059773577_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1633525764_gshared (ValueCollection_t4050043267 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1633525764(__this, method) ((  bool (*) (ValueCollection_t4050043267 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1633525764_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2540133054_gshared (ValueCollection_t4050043267 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2540133054(__this, method) ((  bool (*) (ValueCollection_t4050043267 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2540133054_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m4124236526_gshared (ValueCollection_t4050043267 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m4124236526(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4050043267 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m4124236526_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m3965274650_gshared (ValueCollection_t4050043267 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m3965274650(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4050043267 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3965274650_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2738548892  ValueCollection_GetEnumerator_m3869196287_gshared (ValueCollection_t4050043267 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m3869196287(__this, method) ((  Enumerator_t2738548892  (*) (ValueCollection_t4050043267 *, const MethodInfo*))ValueCollection_GetEnumerator_m3869196287_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m2529817150_gshared (ValueCollection_t4050043267 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m2529817150(__this, method) ((  int32_t (*) (ValueCollection_t4050043267 *, const MethodInfo*))ValueCollection_get_Count_m2529817150_gshared)(__this, method)
