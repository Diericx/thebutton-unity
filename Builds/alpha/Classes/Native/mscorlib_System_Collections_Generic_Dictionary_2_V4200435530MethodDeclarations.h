﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Int32>
struct Dictionary_2_t2513902766;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4200435530.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4094254752_gshared (Enumerator_t4200435530 * __this, Dictionary_2_t2513902766 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m4094254752(__this, ___host0, method) ((  void (*) (Enumerator_t4200435530 *, Dictionary_2_t2513902766 *, const MethodInfo*))Enumerator__ctor_m4094254752_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1738297215_gshared (Enumerator_t4200435530 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1738297215(__this, method) ((  Il2CppObject * (*) (Enumerator_t4200435530 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1738297215_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1672746039_gshared (Enumerator_t4200435530 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1672746039(__this, method) ((  void (*) (Enumerator_t4200435530 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1672746039_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m2630138556_gshared (Enumerator_t4200435530 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2630138556(__this, method) ((  void (*) (Enumerator_t4200435530 *, const MethodInfo*))Enumerator_Dispose_m2630138556_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1519402707_gshared (Enumerator_t4200435530 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1519402707(__this, method) ((  bool (*) (Enumerator_t4200435530 *, const MethodInfo*))Enumerator_MoveNext_m1519402707_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.IntPtr,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m577317777_gshared (Enumerator_t4200435530 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m577317777(__this, method) ((  int32_t (*) (Enumerator_t4200435530 *, const MethodInfo*))Enumerator_get_Current_m577317777_gshared)(__this, method)
