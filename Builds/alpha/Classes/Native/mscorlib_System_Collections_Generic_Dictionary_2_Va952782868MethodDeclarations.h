﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2925031030MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Nullable`1<System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m3480470010(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t952782868 *, Dictionary_2_t2249723025 *, const MethodInfo*))ValueCollection__ctor_m2787300724_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Nullable`1<System.Int32>>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m616745700(__this, ___item0, method) ((  void (*) (ValueCollection_t952782868 *, Nullable_1_t334943763 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2821050894_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Nullable`1<System.Int32>>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2263771877(__this, method) ((  void (*) (ValueCollection_t952782868 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2396611473_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Nullable`1<System.Int32>>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2224948404(__this, ___item0, method) ((  bool (*) (ValueCollection_t952782868 *, Nullable_1_t334943763 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3120340138_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Nullable`1<System.Int32>>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m963259289(__this, ___item0, method) ((  bool (*) (ValueCollection_t952782868 *, Nullable_1_t334943763 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3655682469_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Nullable`1<System.Int32>>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m991481911(__this, method) ((  Il2CppObject* (*) (ValueCollection_t952782868 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1637599523_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Nullable`1<System.Int32>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m289121035(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t952782868 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m489934271_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Nullable`1<System.Int32>>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2800312624(__this, method) ((  Il2CppObject * (*) (ValueCollection_t952782868 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1431982042_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Nullable`1<System.Int32>>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3011410253(__this, method) ((  bool (*) (ValueCollection_t952782868 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m564702409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Nullable`1<System.Int32>>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1623700115(__this, method) ((  bool (*) (ValueCollection_t952782868 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1863260351_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Nullable`1<System.Int32>>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1243512491(__this, method) ((  Il2CppObject * (*) (ValueCollection_t952782868 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1684233055_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Nullable`1<System.Int32>>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m3540430177(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t952782868 *, Nullable_1U5BU5D_t1781475394*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3484540765_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Nullable`1<System.Int32>>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3566679868(__this, method) ((  Enumerator_t3936255789  (*) (ValueCollection_t952782868 *, const MethodInfo*))ValueCollection_GetEnumerator_m1858427074_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Nullable`1<System.Int32>>::get_Count()
#define ValueCollection_get_Count_m597977171(__this, method) ((  int32_t (*) (ValueCollection_t952782868 *, const MethodInfo*))ValueCollection_get_Count_m1476684799_gshared)(__this, method)
