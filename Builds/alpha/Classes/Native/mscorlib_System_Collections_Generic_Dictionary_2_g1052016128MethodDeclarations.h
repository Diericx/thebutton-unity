﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>
struct Dictionary_2_t1052016128;
// System.Collections.Generic.IEqualityComparer`1<PubNubMessaging.Core.CurrentRequestType>
struct IEqualityComparer_1_t2462860764;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.ICollection`1<PubNubMessaging.Core.CurrentRequestType>
struct ICollection_1_t4202303291;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>[]
struct KeyValuePair_2U5BU5D_t828907107;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>>
struct IEnumerator_1_t579852473;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>
struct KeyCollection_t3535513899;
// System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.CurrentRequestType,System.Object>
struct ValueCollection_t4050043267;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23104328646.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2372040830.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m981263572_gshared (Dictionary_2_t1052016128 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m981263572(__this, method) ((  void (*) (Dictionary_2_t1052016128 *, const MethodInfo*))Dictionary_2__ctor_m981263572_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2371241561_gshared (Dictionary_2_t1052016128 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m2371241561(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1052016128 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2371241561_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2133867907_gshared (Dictionary_2_t1052016128 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2133867907(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1052016128 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2133867907_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m1150578061_gshared (Dictionary_2_t1052016128 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m1150578061(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1052016128 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m1150578061_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3885572394_gshared (Dictionary_2_t1052016128 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3885572394(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1052016128 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3885572394_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m527949682_gshared (Dictionary_2_t1052016128 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m527949682(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1052016128 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m527949682_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m2101440082_gshared (Dictionary_2_t1052016128 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m2101440082(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1052016128 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m2101440082_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m4232980584_gshared (Dictionary_2_t1052016128 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m4232980584(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1052016128 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m4232980584_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m1551419148_gshared (Dictionary_2_t1052016128 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1551419148(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1052016128 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1551419148_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1771182569_gshared (Dictionary_2_t1052016128 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1771182569(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1052016128 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1771182569_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m901948454_gshared (Dictionary_2_t1052016128 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m901948454(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1052016128 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m901948454_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2125457086_gshared (Dictionary_2_t1052016128 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m2125457086(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1052016128 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2125457086_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2090652069_gshared (Dictionary_2_t1052016128 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2090652069(__this, ___key0, method) ((  void (*) (Dictionary_2_t1052016128 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2090652069_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1204644900_gshared (Dictionary_2_t1052016128 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1204644900(__this, method) ((  bool (*) (Dictionary_2_t1052016128 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1204644900_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1586524256_gshared (Dictionary_2_t1052016128 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1586524256(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1052016128 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1586524256_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1121828426_gshared (Dictionary_2_t1052016128 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1121828426(__this, method) ((  bool (*) (Dictionary_2_t1052016128 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1121828426_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3117250637_gshared (Dictionary_2_t1052016128 * __this, KeyValuePair_2_t3104328646  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3117250637(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1052016128 *, KeyValuePair_2_t3104328646 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3117250637_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m683904151_gshared (Dictionary_2_t1052016128 * __this, KeyValuePair_2_t3104328646  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m683904151(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1052016128 *, KeyValuePair_2_t3104328646 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m683904151_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3922845553_gshared (Dictionary_2_t1052016128 * __this, KeyValuePair_2U5BU5D_t828907107* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3922845553(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1052016128 *, KeyValuePair_2U5BU5D_t828907107*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3922845553_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2659815120_gshared (Dictionary_2_t1052016128 * __this, KeyValuePair_2_t3104328646  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2659815120(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1052016128 *, KeyValuePair_2_t3104328646 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2659815120_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m4114413136_gshared (Dictionary_2_t1052016128 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m4114413136(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1052016128 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m4114413136_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m783918927_gshared (Dictionary_2_t1052016128 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m783918927(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1052016128 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m783918927_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2231161650_gshared (Dictionary_2_t1052016128 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2231161650(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1052016128 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2231161650_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m431086693_gshared (Dictionary_2_t1052016128 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m431086693(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1052016128 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m431086693_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m1845613700_gshared (Dictionary_2_t1052016128 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1845613700(__this, method) ((  int32_t (*) (Dictionary_2_t1052016128 *, const MethodInfo*))Dictionary_2_get_Count_m1845613700_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m1106759373_gshared (Dictionary_2_t1052016128 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m1106759373(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1052016128 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m1106759373_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m4135133594_gshared (Dictionary_2_t1052016128 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m4135133594(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1052016128 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m4135133594_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m1817210572_gshared (Dictionary_2_t1052016128 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m1817210572(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1052016128 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1817210572_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2002874551_gshared (Dictionary_2_t1052016128 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2002874551(__this, ___size0, method) ((  void (*) (Dictionary_2_t1052016128 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2002874551_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3141588701_gshared (Dictionary_2_t1052016128 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3141588701(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1052016128 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3141588701_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t3104328646  Dictionary_2_make_pair_m4202050139_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m4202050139(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3104328646  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m4202050139_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m2025640963_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m2025640963(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m2025640963_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m2997136067_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2997136067(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m2997136067_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m787845406_gshared (Dictionary_2_t1052016128 * __this, KeyValuePair_2U5BU5D_t828907107* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m787845406(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1052016128 *, KeyValuePair_2U5BU5D_t828907107*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m787845406_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m4140629278_gshared (Dictionary_2_t1052016128 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m4140629278(__this, method) ((  void (*) (Dictionary_2_t1052016128 *, const MethodInfo*))Dictionary_2_Resize_m4140629278_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m330661863_gshared (Dictionary_2_t1052016128 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m330661863(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1052016128 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m330661863_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m894876711_gshared (Dictionary_2_t1052016128 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m894876711(__this, method) ((  void (*) (Dictionary_2_t1052016128 *, const MethodInfo*))Dictionary_2_Clear_m894876711_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m1098665637_gshared (Dictionary_2_t1052016128 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1098665637(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1052016128 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m1098665637_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m803351077_gshared (Dictionary_2_t1052016128 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m803351077(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1052016128 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m803351077_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2491182692_gshared (Dictionary_2_t1052016128 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2491182692(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1052016128 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m2491182692_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2262751098_gshared (Dictionary_2_t1052016128 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2262751098(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1052016128 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2262751098_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2500241119_gshared (Dictionary_2_t1052016128 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2500241119(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1052016128 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m2500241119_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m1162287878_gshared (Dictionary_2_t1052016128 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1162287878(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1052016128 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m1162287878_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Keys()
extern "C"  KeyCollection_t3535513899 * Dictionary_2_get_Keys_m4111885217_gshared (Dictionary_2_t1052016128 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m4111885217(__this, method) ((  KeyCollection_t3535513899 * (*) (Dictionary_2_t1052016128 *, const MethodInfo*))Dictionary_2_get_Keys_m4111885217_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Values()
extern "C"  ValueCollection_t4050043267 * Dictionary_2_get_Values_m4258405833_gshared (Dictionary_2_t1052016128 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m4258405833(__this, method) ((  ValueCollection_t4050043267 * (*) (Dictionary_2_t1052016128 *, const MethodInfo*))Dictionary_2_get_Values_m4258405833_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m1068704368_gshared (Dictionary_2_t1052016128 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1068704368(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t1052016128 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1068704368_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m3335299608_gshared (Dictionary_2_t1052016128 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m3335299608(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t1052016128 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3335299608_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m787991582_gshared (Dictionary_2_t1052016128 * __this, KeyValuePair_2_t3104328646  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m787991582(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1052016128 *, KeyValuePair_2_t3104328646 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m787991582_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2372040830  Dictionary_2_GetEnumerator_m411729471_gshared (Dictionary_2_t1052016128 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m411729471(__this, method) ((  Enumerator_t2372040830  (*) (Dictionary_2_t1052016128 *, const MethodInfo*))Dictionary_2_GetEnumerator_m411729471_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::<CopyTo>m__2(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__2_m2844780586_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__2_m2844780586(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m2844780586_gshared)(__this /* static, unused */, ___key0, ___value1, method)
