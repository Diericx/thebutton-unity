﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::.ctor()
#define Dictionary_2__ctor_m344256166(__this, method) ((  void (*) (Dictionary_2_t3159087724 *, const MethodInfo*))Dictionary_2__ctor_m584589095_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m2793435914(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3159087724 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m406310120_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::.ctor(System.Int32)
#define Dictionary_2__ctor_m2994113242(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3159087724 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m206582704_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m3009282068(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3159087724 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m1206668798_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3002495721(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3159087724 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m853262843_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2714126153(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3159087724 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2954370043_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m4205365931(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3159087724 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m673000885_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::System.Collections.IDictionary.get_Values()
#define Dictionary_2_System_Collections_IDictionary_get_Values_m2249732155(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3159087724 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1552474645_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3931936269(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3159087724 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m237963271_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m134667628(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3159087724 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3775521570_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m891076023(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3159087724 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m984276885_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m967738499(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3159087724 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2868006769_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m2947354632(__this, ___key0, method) ((  void (*) (Dictionary_2_t3159087724 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2017099222_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3880204901(__this, method) ((  bool (*) (Dictionary_2_t3159087724 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m960517203_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3794592205(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3159087724 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1900166091_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2677626715(__this, method) ((  bool (*) (Dictionary_2_t3159087724 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4094240197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1207429986(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3159087724 *, KeyValuePair_2_t916432946 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m990341268_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1364857122(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3159087724 *, KeyValuePair_2_t916432946 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1058501024_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1948424038(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3159087724 *, KeyValuePair_2U5BU5D_t2077787847*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m976354816_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2264079681(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3159087724 *, KeyValuePair_2_t916432946 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1705959559_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1195658521(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3159087724 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3578539931_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1066434408(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3159087724 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3100111910_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m696158503(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3159087724 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2925090477_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1059018474(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3159087724 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2684932776_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::get_Count()
#define Dictionary_2_get_Count_m3041032641(__this, method) ((  int32_t (*) (Dictionary_2_t3159087724 *, const MethodInfo*))Dictionary_2_get_Count_m3636113691_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::get_Item(TKey)
#define Dictionary_2_get_Item_m323556482(__this, ___key0, method) ((  Repo_t1244308462 * (*) (Dictionary_2_t3159087724 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m2413909512_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m3305167633(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3159087724 *, String_t*, Repo_t1244308462 *, const MethodInfo*))Dictionary_2_set_Item_m458653679_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m1465557805(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3159087724 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1045257495_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m1800359026(__this, ___size0, method) ((  void (*) (Dictionary_2_t3159087724 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2270022740_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m3733600880(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3159087724 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2147716750_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m2317672554(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t916432946  (*) (Il2CppObject * /* static, unused */, String_t*, Repo_t1244308462 *, const MethodInfo*))Dictionary_2_make_pair_m2631942124_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m3105451308(__this /* static, unused */, ___key0, ___value1, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, String_t*, Repo_t1244308462 *, const MethodInfo*))Dictionary_2_pick_key_m2840829442_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m1101464492(__this /* static, unused */, ___key0, ___value1, method) ((  Repo_t1244308462 * (*) (Il2CppObject * /* static, unused */, String_t*, Repo_t1244308462 *, const MethodInfo*))Dictionary_2_pick_value_m1872663242_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m2621673789(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3159087724 *, KeyValuePair_2U5BU5D_t2077787847*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1495142643_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::Resize()
#define Dictionary_2_Resize_m4218836839(__this, method) ((  void (*) (Dictionary_2_t3159087724 *, const MethodInfo*))Dictionary_2_Resize_m2672264133_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::Add(TKey,TValue)
#define Dictionary_2_Add_m971582054(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3159087724 *, String_t*, Repo_t1244308462 *, const MethodInfo*))Dictionary_2_Add_m1708621268_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::Clear()
#define Dictionary_2_Clear_m573426242(__this, method) ((  void (*) (Dictionary_2_t3159087724 *, const MethodInfo*))Dictionary_2_Clear_m2325793156_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m727204066(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3159087724 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m3553426152_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m321537098(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3159087724 *, Repo_t1244308462 *, const MethodInfo*))Dictionary_2_ContainsValue_m2375979648_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m1224567509(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3159087724 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m2864531407_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m3328729417(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3159087724 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2160537783_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::Remove(TKey)
#define Dictionary_2_Remove_m4054844786(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3159087724 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m1366616528_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m4227741369(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3159087724 *, String_t*, Repo_t1244308462 **, const MethodInfo*))Dictionary_2_TryGetValue_m1120370623_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::get_Keys()
#define Dictionary_2_get_Keys_m3833005110(__this, method) ((  KeyCollection_t1347618199 * (*) (Dictionary_2_t3159087724 *, const MethodInfo*))Dictionary_2_get_Keys_m1635778172_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::get_Values()
#define Dictionary_2_get_Values_m3531668694(__this, method) ((  ValueCollection_t1862147567 * (*) (Dictionary_2_t3159087724 *, const MethodInfo*))Dictionary_2_get_Values_m825860460_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m1415081783(__this, ___key0, method) ((  String_t* (*) (Dictionary_2_t3159087724 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m4209561517_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m989806743(__this, ___value0, method) ((  Repo_t1244308462 * (*) (Dictionary_2_t3159087724 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1381983709_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m391799385(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3159087724 *, KeyValuePair_2_t916432946 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m663697471_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m185530132(__this, method) ((  Enumerator_t184145130  (*) (Dictionary_2_t3159087724 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1752238884_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,Firebase.Database.Internal.Core.Repo>::<CopyTo>m__2(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__2_m3344526601(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, String_t*, Repo_t1244308462 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m3918717819_gshared)(__this /* static, unused */, ___key0, ___value1, method)
