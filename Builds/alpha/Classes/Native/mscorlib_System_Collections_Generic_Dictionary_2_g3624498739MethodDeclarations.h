﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>
struct Dictionary_2_t3624498739;
// System.Collections.Generic.IEqualityComparer`1<PubNubMessaging.Core.ChannelIdentity>
struct IEqualityComparer_1_t359795045;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.ICollection`1<PubNubMessaging.Core.ChannelIdentity>
struct ICollection_1_t2099237572;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>[]
struct KeyValuePair_2U5BU5D_t402044932;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>>
struct IEnumerator_1_t3152335084;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>
struct KeyCollection_t1813029214;
// System.Collections.Generic.Dictionary`2/ValueCollection<PubNubMessaging.Core.ChannelIdentity,System.Object>
struct ValueCollection_t2327558582;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21381843961.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelIden1147162267.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En649556145.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m3631657027_gshared (Dictionary_2_t3624498739 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3631657027(__this, method) ((  void (*) (Dictionary_2_t3624498739 *, const MethodInfo*))Dictionary_2__ctor_m3631657027_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1193681792_gshared (Dictionary_2_t3624498739 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m1193681792(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3624498739 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1193681792_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m4176811600_gshared (Dictionary_2_t3624498739 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m4176811600(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3624498739 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m4176811600_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m1601328190_gshared (Dictionary_2_t3624498739 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m1601328190(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3624498739 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m1601328190_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1379224923_gshared (Dictionary_2_t3624498739 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1379224923(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3624498739 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1379224923_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1421686971_gshared (Dictionary_2_t3624498739 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1421686971(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3624498739 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1421686971_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m4097409913_gshared (Dictionary_2_t3624498739 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m4097409913(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3624498739 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m4097409913_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m3195468185_gshared (Dictionary_2_t3624498739 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m3195468185(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3624498739 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3195468185_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m279344727_gshared (Dictionary_2_t3624498739 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m279344727(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3624498739 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m279344727_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1958346594_gshared (Dictionary_2_t3624498739 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1958346594(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3624498739 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1958346594_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3816397585_gshared (Dictionary_2_t3624498739 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3816397585(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3624498739 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3816397585_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2330616661_gshared (Dictionary_2_t3624498739 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m2330616661(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3624498739 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2330616661_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m777989198_gshared (Dictionary_2_t3624498739 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m777989198(__this, ___key0, method) ((  void (*) (Dictionary_2_t3624498739 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m777989198_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3603485479_gshared (Dictionary_2_t3624498739 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3603485479(__this, method) ((  bool (*) (Dictionary_2_t3624498739 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3603485479_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m128418435_gshared (Dictionary_2_t3624498739 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m128418435(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3624498739 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m128418435_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m181004137_gshared (Dictionary_2_t3624498739 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m181004137(__this, method) ((  bool (*) (Dictionary_2_t3624498739 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m181004137_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1729072556_gshared (Dictionary_2_t3624498739 * __this, KeyValuePair_2_t1381843961  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1729072556(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3624498739 *, KeyValuePair_2_t1381843961 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1729072556_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2714295552_gshared (Dictionary_2_t3624498739 * __this, KeyValuePair_2_t1381843961  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2714295552(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3624498739 *, KeyValuePair_2_t1381843961 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2714295552_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2041247392_gshared (Dictionary_2_t3624498739 * __this, KeyValuePair_2U5BU5D_t402044932* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2041247392(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3624498739 *, KeyValuePair_2U5BU5D_t402044932*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2041247392_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m387196323_gshared (Dictionary_2_t3624498739 * __this, KeyValuePair_2_t1381843961  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m387196323(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3624498739 *, KeyValuePair_2_t1381843961 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m387196323_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m3984832175_gshared (Dictionary_2_t3624498739 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3984832175(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3624498739 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3984832175_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3110614342_gshared (Dictionary_2_t3624498739 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3110614342(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3624498739 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3110614342_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2016119177_gshared (Dictionary_2_t3624498739 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2016119177(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3624498739 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2016119177_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3886336508_gshared (Dictionary_2_t3624498739 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3886336508(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3624498739 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3886336508_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m79072543_gshared (Dictionary_2_t3624498739 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m79072543(__this, method) ((  int32_t (*) (Dictionary_2_t3624498739 *, const MethodInfo*))Dictionary_2_get_Count_m79072543_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m3873800640_gshared (Dictionary_2_t3624498739 * __this, ChannelIdentity_t1147162267  ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3873800640(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , const MethodInfo*))Dictionary_2_get_Item_m3873800640_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m583699995_gshared (Dictionary_2_t3624498739 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m583699995(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m583699995_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3600060979_gshared (Dictionary_2_t3624498739 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3600060979(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3624498739 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3600060979_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2822387868_gshared (Dictionary_2_t3624498739 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2822387868(__this, ___size0, method) ((  void (*) (Dictionary_2_t3624498739 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2822387868_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m2715819622_gshared (Dictionary_2_t3624498739 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m2715819622(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3624498739 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2715819622_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1381843961  Dictionary_2_make_pair_m623521052_gshared (Il2CppObject * __this /* static, unused */, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m623521052(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1381843961  (*) (Il2CppObject * /* static, unused */, ChannelIdentity_t1147162267 , Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m623521052_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::pick_key(TKey,TValue)
extern "C"  ChannelIdentity_t1147162267  Dictionary_2_pick_key_m1652870970_gshared (Il2CppObject * __this /* static, unused */, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m1652870970(__this /* static, unused */, ___key0, ___value1, method) ((  ChannelIdentity_t1147162267  (*) (Il2CppObject * /* static, unused */, ChannelIdentity_t1147162267 , Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m1652870970_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m3277653690_gshared (Il2CppObject * __this /* static, unused */, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m3277653690(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, ChannelIdentity_t1147162267 , Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m3277653690_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m4026893463_gshared (Dictionary_2_t3624498739 * __this, KeyValuePair_2U5BU5D_t402044932* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m4026893463(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3624498739 *, KeyValuePair_2U5BU5D_t402044932*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m4026893463_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m3544978177_gshared (Dictionary_2_t3624498739 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3544978177(__this, method) ((  void (*) (Dictionary_2_t3624498739 *, const MethodInfo*))Dictionary_2_Resize_m3544978177_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m2635526188_gshared (Dictionary_2_t3624498739 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m2635526188(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m2635526188_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m2660979596_gshared (Dictionary_2_t3624498739 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2660979596(__this, method) ((  void (*) (Dictionary_2_t3624498739 *, const MethodInfo*))Dictionary_2_Clear_m2660979596_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2884736240_gshared (Dictionary_2_t3624498739 * __this, ChannelIdentity_t1147162267  ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2884736240(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , const MethodInfo*))Dictionary_2_ContainsKey_m2884736240_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m1890058296_gshared (Dictionary_2_t3624498739 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1890058296(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3624498739 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m1890058296_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2275439115_gshared (Dictionary_2_t3624498739 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2275439115(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3624498739 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m2275439115_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m3342962179_gshared (Dictionary_2_t3624498739 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m3342962179(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3624498739 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3342962179_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m1104295360_gshared (Dictionary_2_t3624498739 * __this, ChannelIdentity_t1147162267  ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m1104295360(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , const MethodInfo*))Dictionary_2_Remove_m1104295360_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m3154495819_gshared (Dictionary_2_t3624498739 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m3154495819(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3624498739 *, ChannelIdentity_t1147162267 , Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m3154495819_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Keys()
extern "C"  KeyCollection_t1813029214 * Dictionary_2_get_Keys_m415560056_gshared (Dictionary_2_t3624498739 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m415560056(__this, method) ((  KeyCollection_t1813029214 * (*) (Dictionary_2_t3624498739 *, const MethodInfo*))Dictionary_2_get_Keys_m415560056_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Values()
extern "C"  ValueCollection_t2327558582 * Dictionary_2_get_Values_m1868821720_gshared (Dictionary_2_t3624498739 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m1868821720(__this, method) ((  ValueCollection_t2327558582 * (*) (Dictionary_2_t3624498739 *, const MethodInfo*))Dictionary_2_get_Values_m1868821720_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::ToTKey(System.Object)
extern "C"  ChannelIdentity_t1147162267  Dictionary_2_ToTKey_m1784315961_gshared (Dictionary_2_t3624498739 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1784315961(__this, ___key0, method) ((  ChannelIdentity_t1147162267  (*) (Dictionary_2_t3624498739 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1784315961_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m2602673817_gshared (Dictionary_2_t3624498739 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m2602673817(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t3624498739 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m2602673817_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m3939019707_gshared (Dictionary_2_t3624498739 * __this, KeyValuePair_2_t1381843961  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m3939019707(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3624498739 *, KeyValuePair_2_t1381843961 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m3939019707_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::GetEnumerator()
extern "C"  Enumerator_t649556145  Dictionary_2_GetEnumerator_m994830798_gshared (Dictionary_2_t3624498739 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m994830798(__this, method) ((  Enumerator_t649556145  (*) (Dictionary_2_t3624498739 *, const MethodInfo*))Dictionary_2_GetEnumerator_m994830798_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::<CopyTo>m__2(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__2_m1113978779_gshared (Il2CppObject * __this /* static, unused */, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__2_m1113978779(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, ChannelIdentity_t1147162267 , Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m1113978779_gshared)(__this /* static, unused */, ___key0, ___value1, method)
