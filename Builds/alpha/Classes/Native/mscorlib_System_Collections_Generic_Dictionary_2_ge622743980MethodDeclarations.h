﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1697274930MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::.ctor()
#define Dictionary_2__ctor_m1928557225(__this, method) ((  void (*) (Dictionary_2_t622743980 *, const MethodInfo*))Dictionary_2__ctor_m3997847064_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m981619596(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t622743980 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2284756127_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::.ctor(System.Int32)
#define Dictionary_2__ctor_m4008561164(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t622743980 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3111963761_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m2812348794(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t622743980 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m965168575_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1309087665(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t622743980 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m579362412_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3449759353(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t622743980 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3255449836_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1485959671(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t622743980 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m4245826226_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.IDictionary.get_Values()
#define Dictionary_2_System_Collections_IDictionary_get_Values_m1520070127(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t622743980 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3427730524_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2214138297(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t622743980 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2945412702_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1987752754(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t622743980 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m941667911_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m2066588211(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t622743980 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3189569330_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m1346411095(__this, ___key0, method) ((  bool (*) (Dictionary_2_t622743980 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m3937948050_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m3742793134(__this, ___key0, method) ((  void (*) (Dictionary_2_t622743980 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m3199539467_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m445014685(__this, method) ((  bool (*) (Dictionary_2_t622743980 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m304009368_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3903385865(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t622743980 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2487129350_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1156147151(__this, method) ((  bool (*) (Dictionary_2_t622743980 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1111602362_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3338358468(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t622743980 *, KeyValuePair_2_t2675056498 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1043757703_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2528932732(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t622743980 *, KeyValuePair_2_t2675056498 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1927335261_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3467796680(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t622743980 *, KeyValuePair_2U5BU5D_t3420129927*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3678641635_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2476512745(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t622743980 *, KeyValuePair_2_t2675056498 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m181279132_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1827891385(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t622743980 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1985034736_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1407274026(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t622743980 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3830548821_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1429593047(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t622743980 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m631947640_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1240672876(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t622743980 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1284065099_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::get_Count()
#define Dictionary_2_get_Count_m1995781757(__this, method) ((  int32_t (*) (Dictionary_2_t622743980 *, const MethodInfo*))Dictionary_2_get_Count_m2168147420_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::get_Item(TKey)
#define Dictionary_2_get_Item_m3321374644(__this, ___key0, method) ((  Action_t1614918345 * (*) (Dictionary_2_t622743980 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m4277290203_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m3243981592(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t622743980 *, int32_t, Action_t1614918345 *, const MethodInfo*))Dictionary_2_set_Item_m3121864719_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m293203061(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t622743980 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3666073812_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m652855812(__this, ___size0, method) ((  void (*) (Dictionary_2_t622743980 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3810830177_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m832588806(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t622743980 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1541945891_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m3500781184(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2675056498  (*) (Il2CppObject * /* static, unused */, int32_t, Action_t1614918345 *, const MethodInfo*))Dictionary_2_make_pair_m90480045_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m1215176778(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Action_t1614918345 *, const MethodInfo*))Dictionary_2_pick_key_m761174441_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m1243135114(__this /* static, unused */, ___key0, ___value1, method) ((  Action_t1614918345 * (*) (Il2CppObject * /* static, unused */, int32_t, Action_t1614918345 *, const MethodInfo*))Dictionary_2_pick_value_m353965321_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m4249732725(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t622743980 *, KeyValuePair_2U5BU5D_t3420129927*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1956977846_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::Resize()
#define Dictionary_2_Resize_m2837298347(__this, method) ((  void (*) (Dictionary_2_t622743980 *, const MethodInfo*))Dictionary_2_Resize_m2532139610_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::Add(TKey,TValue)
#define Dictionary_2_Add_m2093442000(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t622743980 *, int32_t, Action_t1614918345 *, const MethodInfo*))Dictionary_2_Add_m2839642701_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::Clear()
#define Dictionary_2_Clear_m678906868(__this, method) ((  void (*) (Dictionary_2_t622743980 *, const MethodInfo*))Dictionary_2_Clear_m899854001_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m229781324(__this, ___key0, method) ((  bool (*) (Dictionary_2_t622743980 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m255952723_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m2882965484(__this, ___value0, method) ((  bool (*) (Dictionary_2_t622743980 *, Action_t1614918345 *, const MethodInfo*))Dictionary_2_ContainsValue_m392092147_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m3947212333(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t622743980 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m233109612_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m570714129(__this, ___sender0, method) ((  void (*) (Dictionary_2_t622743980 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2092139626_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::Remove(TKey)
#define Dictionary_2_Remove_m3017600130(__this, ___key0, method) ((  bool (*) (Dictionary_2_t622743980 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m602713029_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m1413651722(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t622743980 *, int32_t, Action_t1614918345 **, const MethodInfo*))Dictionary_2_TryGetValue_m3187691483_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::get_Keys()
#define Dictionary_2_get_Keys_m992532008(__this, method) ((  KeyCollection_t3106241751 * (*) (Dictionary_2_t622743980 *, const MethodInfo*))Dictionary_2_get_Keys_m1900997095_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::get_Values()
#define Dictionary_2_get_Values_m1832003280(__this, method) ((  ValueCollection_t3620771119 * (*) (Dictionary_2_t622743980 *, const MethodInfo*))Dictionary_2_get_Values_m372946023_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m3676997803(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t622743980 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m2900575080_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m2703937171(__this, ___value0, method) ((  Action_t1614918345 * (*) (Dictionary_2_t622743980 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m14471464_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m3306123897(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t622743980 *, KeyValuePair_2_t2675056498 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m790970878_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m3247034088(__this, method) ((  Enumerator_t1942768682  (*) (Dictionary_2_t622743980 *, const MethodInfo*))Dictionary_2_GetEnumerator_m706253773_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::<CopyTo>m__2(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__2_m2996543601(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, Action_t1614918345 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__2_m3167711988_gshared)(__this /* static, unused */, ___key0, ___value1, method)
