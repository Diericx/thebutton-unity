﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>
struct DefaultComparer_t1059976544;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23733013679.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::.ctor()
extern "C"  void DefaultComparer__ctor_m2786820777_gshared (DefaultComparer_t1059976544 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2786820777(__this, method) ((  void (*) (DefaultComparer_t1059976544 *, const MethodInfo*))DefaultComparer__ctor_m2786820777_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3475309146_gshared (DefaultComparer_t1059976544 * __this, KeyValuePair_2_t3733013679  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3475309146(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1059976544 *, KeyValuePair_2_t3733013679 , const MethodInfo*))DefaultComparer_GetHashCode_m3475309146_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m80735114_gshared (DefaultComparer_t1059976544 * __this, KeyValuePair_2_t3733013679  ___x0, KeyValuePair_2_t3733013679  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m80735114(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1059976544 *, KeyValuePair_2_t3733013679 , KeyValuePair_2_t3733013679 , const MethodInfo*))DefaultComparer_Equals_m80735114_gshared)(__this, ___x0, ___y1, method)
