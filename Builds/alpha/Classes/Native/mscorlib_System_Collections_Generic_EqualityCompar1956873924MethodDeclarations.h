﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Nullable`1<System.Int32>>
struct DefaultComparer_t1956873924;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Nullable`1<System.Int32>>::.ctor()
extern "C"  void DefaultComparer__ctor_m3000604546_gshared (DefaultComparer_t1956873924 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3000604546(__this, method) ((  void (*) (DefaultComparer_t1956873924 *, const MethodInfo*))DefaultComparer__ctor_m3000604546_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Nullable`1<System.Int32>>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3885822215_gshared (DefaultComparer_t1956873924 * __this, Nullable_1_t334943763  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3885822215(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1956873924 *, Nullable_1_t334943763 , const MethodInfo*))DefaultComparer_GetHashCode_m3885822215_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Nullable`1<System.Int32>>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2229060815_gshared (DefaultComparer_t1956873924 * __this, Nullable_1_t334943763  ___x0, Nullable_1_t334943763  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2229060815(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1956873924 *, Nullable_1_t334943763 , Nullable_1_t334943763 , const MethodInfo*))DefaultComparer_Equals_m2229060815_gshared)(__this, ___x0, ___y1, method)
