﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<PubNubMessaging.Core.ChannelIdentity>
struct DefaultComparer_t2769092428;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelIden1147162267.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<PubNubMessaging.Core.ChannelIdentity>::.ctor()
extern "C"  void DefaultComparer__ctor_m1809745662_gshared (DefaultComparer_t2769092428 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1809745662(__this, method) ((  void (*) (DefaultComparer_t2769092428 *, const MethodInfo*))DefaultComparer__ctor_m1809745662_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<PubNubMessaging.Core.ChannelIdentity>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1835863629_gshared (DefaultComparer_t2769092428 * __this, ChannelIdentity_t1147162267  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1835863629(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2769092428 *, ChannelIdentity_t1147162267 , const MethodInfo*))DefaultComparer_GetHashCode_m1835863629_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<PubNubMessaging.Core.ChannelIdentity>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3604229145_gshared (DefaultComparer_t2769092428 * __this, ChannelIdentity_t1147162267  ___x0, ChannelIdentity_t1147162267  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3604229145(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2769092428 *, ChannelIdentity_t1147162267 , ChannelIdentity_t1147162267 , const MethodInfo*))DefaultComparer_Equals_m3604229145_gshared)(__this, ___x0, ___y1, method)
