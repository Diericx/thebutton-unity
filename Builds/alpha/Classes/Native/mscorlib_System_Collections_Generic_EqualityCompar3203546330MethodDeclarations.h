﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<System.Nullable`1<System.Int32>>
struct EqualityComparer_1_t3203546330;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.EqualityComparer`1<System.Nullable`1<System.Int32>>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2640532825_gshared (EqualityComparer_1_t3203546330 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m2640532825(__this, method) ((  void (*) (EqualityComparer_1_t3203546330 *, const MethodInfo*))EqualityComparer_1__ctor_m2640532825_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<System.Nullable`1<System.Int32>>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2940842250_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m2940842250(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m2940842250_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Nullable`1<System.Int32>>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2616791516_gshared (EqualityComparer_1_t3203546330 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2616791516(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t3203546330 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2616791516_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Nullable`1<System.Int32>>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3920972518_gshared (EqualityComparer_1_t3203546330 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3920972518(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t3203546330 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3920972518_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Nullable`1<System.Int32>>::get_Default()
extern "C"  EqualityComparer_1_t3203546330 * EqualityComparer_1_get_Default_m3023410033_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m3023410033(__this /* static, unused */, method) ((  EqualityComparer_1_t3203546330 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m3023410033_gshared)(__this /* static, unused */, method)
