﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<PubNubMessaging.Core.ChannelIdentity>
struct EqualityComparer_1_t4015764834;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.EqualityComparer`1<PubNubMessaging.Core.ChannelIdentity>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3637155407_gshared (EqualityComparer_1_t4015764834 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m3637155407(__this, method) ((  void (*) (EqualityComparer_1_t4015764834 *, const MethodInfo*))EqualityComparer_1__ctor_m3637155407_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<PubNubMessaging.Core.ChannelIdentity>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m483621632_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m483621632(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m483621632_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<PubNubMessaging.Core.ChannelIdentity>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2234165490_gshared (EqualityComparer_1_t4015764834 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2234165490(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t4015764834 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2234165490_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<PubNubMessaging.Core.ChannelIdentity>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3721394328_gshared (EqualityComparer_1_t4015764834 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3721394328(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t4015764834 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m3721394328_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<PubNubMessaging.Core.ChannelIdentity>::get_Default()
extern "C"  EqualityComparer_1_t4015764834 * EqualityComparer_1_get_Default_m1358738303_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m1358738303(__this /* static, unused */, method) ((  EqualityComparer_1_t4015764834 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m1358738303_gshared)(__this /* static, unused */, method)
