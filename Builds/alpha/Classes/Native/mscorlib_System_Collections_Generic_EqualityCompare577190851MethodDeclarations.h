﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<PubNubMessaging.Core.CurrentRequestType>
struct DefaultComparer_t577190851;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<PubNubMessaging.Core.CurrentRequestType>::.ctor()
extern "C"  void DefaultComparer__ctor_m861080623_gshared (DefaultComparer_t577190851 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m861080623(__this, method) ((  void (*) (DefaultComparer_t577190851 *, const MethodInfo*))DefaultComparer__ctor_m861080623_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<PubNubMessaging.Core.CurrentRequestType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1302694914_gshared (DefaultComparer_t577190851 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1302694914(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t577190851 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m1302694914_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<PubNubMessaging.Core.CurrentRequestType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2920517642_gshared (DefaultComparer_t577190851 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2920517642(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t577190851 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m2920517642_gshared)(__this, ___x0, ___y1, method)
