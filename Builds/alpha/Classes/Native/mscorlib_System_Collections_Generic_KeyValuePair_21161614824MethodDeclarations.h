﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Snapshot.Node>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1678119338(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1161614824 *, ChildKey_t1197802383 *, Node_t2640059010 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Snapshot.Node>::get_Key()
#define KeyValuePair_2_get_Key_m2563717733(__this, method) ((  ChildKey_t1197802383 * (*) (KeyValuePair_2_t1161614824 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Snapshot.Node>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m571191437(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1161614824 *, ChildKey_t1197802383 *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Snapshot.Node>::get_Value()
#define KeyValuePair_2_get_Value_m1633514962(__this, method) ((  Node_t2640059010 * (*) (KeyValuePair_2_t1161614824 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Snapshot.Node>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m990360285(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1161614824 *, Node_t2640059010 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Snapshot.Node>::ToString()
#define KeyValuePair_2_ToString_m2738728399(__this, method) ((  String_t* (*) (KeyValuePair_2_t1161614824 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
