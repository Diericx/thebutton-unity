﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Context,System.Collections.Generic.IDictionary`2<System.String,Firebase.Database.Internal.Core.Repo>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m928954390(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1203317345 *, Context_t3486154757 *, Il2CppObject*, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Context,System.Collections.Generic.IDictionary`2<System.String,Firebase.Database.Internal.Core.Repo>>::get_Key()
#define KeyValuePair_2_get_Key_m432104336(__this, method) ((  Context_t3486154757 * (*) (KeyValuePair_2_t1203317345 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Context,System.Collections.Generic.IDictionary`2<System.String,Firebase.Database.Internal.Core.Repo>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2752964787(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1203317345 *, Context_t3486154757 *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Context,System.Collections.Generic.IDictionary`2<System.String,Firebase.Database.Internal.Core.Repo>>::get_Value()
#define KeyValuePair_2_get_Value_m3895215480(__this, method) ((  Il2CppObject* (*) (KeyValuePair_2_t1203317345 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Context,System.Collections.Generic.IDictionary`2<System.String,Firebase.Database.Internal.Core.Repo>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1524715467(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1203317345 *, Il2CppObject*, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Context,System.Collections.Generic.IDictionary`2<System.String,Firebase.Database.Internal.Core.Repo>>::ToString()
#define KeyValuePair_2_ToString_m423479321(__this, method) ((  String_t* (*) (KeyValuePair_2_t1203317345 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
