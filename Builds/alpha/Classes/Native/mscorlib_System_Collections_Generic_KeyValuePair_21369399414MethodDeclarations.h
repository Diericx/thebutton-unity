﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m4156369375(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1369399414 *, int32_t, Dictionary_2_t309261261 *, const MethodInfo*))KeyValuePair_2__ctor_m3201181706_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_Key()
#define KeyValuePair_2_get_Key_m1147722017(__this, method) ((  int32_t (*) (KeyValuePair_2_t1369399414 *, const MethodInfo*))KeyValuePair_2_get_Key_m1435832840_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1578232096(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1369399414 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1350990071_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_Value()
#define KeyValuePair_2_get_Value_m4142944513(__this, method) ((  Dictionary_2_t309261261 * (*) (KeyValuePair_2_t1369399414 *, const MethodInfo*))KeyValuePair_2_get_Value_m3690000728_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3421609192(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1369399414 *, Dictionary_2_t309261261 *, const MethodInfo*))KeyValuePair_2_set_Value_m2726037047_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,System.Object>>::ToString()
#define KeyValuePair_2_ToString_m1152565332(__this, method) ((  String_t* (*) (KeyValuePair_2_t1369399414 *, const MethodInfo*))KeyValuePair_2_ToString_m1391611625_gshared)(__this, method)
