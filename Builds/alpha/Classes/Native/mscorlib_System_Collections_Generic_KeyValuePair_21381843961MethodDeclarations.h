﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21381843961.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelIden1147162267.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m623995145_gshared (KeyValuePair_2_t1381843961 * __this, ChannelIdentity_t1147162267  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m623995145(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1381843961 *, ChannelIdentity_t1147162267 , Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m623995145_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Key()
extern "C"  ChannelIdentity_t1147162267  KeyValuePair_2_get_Key_m359652951_gshared (KeyValuePair_2_t1381843961 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m359652951(__this, method) ((  ChannelIdentity_t1147162267  (*) (KeyValuePair_2_t1381843961 *, const MethodInfo*))KeyValuePair_2_get_Key_m359652951_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m346790296_gshared (KeyValuePair_2_t1381843961 * __this, ChannelIdentity_t1147162267  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m346790296(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1381843961 *, ChannelIdentity_t1147162267 , const MethodInfo*))KeyValuePair_2_set_Key_m346790296_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m2841136671_gshared (KeyValuePair_2_t1381843961 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2841136671(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1381843961 *, const MethodInfo*))KeyValuePair_2_get_Value_m2841136671_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2410839120_gshared (KeyValuePair_2_t1381843961 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2410839120(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1381843961 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m2410839120_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3003107526_gshared (KeyValuePair_2_t1381843961 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m3003107526(__this, method) ((  String_t* (*) (KeyValuePair_2_t1381843961 *, const MethodInfo*))KeyValuePair_2_ToString_m3003107526_gshared)(__this, method)
