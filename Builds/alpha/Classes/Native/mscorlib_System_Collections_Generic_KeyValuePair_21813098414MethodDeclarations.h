﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.EventRegistration,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.EventRegistration>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2905432458(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1813098414 *, EventRegistration_t4222917807 *, Il2CppObject*, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.EventRegistration,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.EventRegistration>>::get_Key()
#define KeyValuePair_2_get_Key_m4159520856(__this, method) ((  EventRegistration_t4222917807 * (*) (KeyValuePair_2_t1813098414 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.EventRegistration,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.EventRegistration>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m505241619(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1813098414 *, EventRegistration_t4222917807 *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.EventRegistration,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.EventRegistration>>::get_Value()
#define KeyValuePair_2_get_Value_m3670070744(__this, method) ((  Il2CppObject* (*) (KeyValuePair_2_t1813098414 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.EventRegistration,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.EventRegistration>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1239916083(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1813098414 *, Il2CppObject*, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.EventRegistration,System.Collections.Generic.IList`1<Firebase.Database.Internal.Core.EventRegistration>>::ToString()
#define KeyValuePair_2_ToString_m3362200033(__this, method) ((  String_t* (*) (KeyValuePair_2_t1813098414 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
