﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21979316409.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m848228575_gshared (KeyValuePair_2_t1979316409 * __this, Il2CppObject * ___key0, Nullable_1_t334943763  ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m848228575(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1979316409 *, Il2CppObject *, Nullable_1_t334943763 , const MethodInfo*))KeyValuePair_2__ctor_m848228575_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2225619841_gshared (KeyValuePair_2_t1979316409 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2225619841(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1979316409 *, const MethodInfo*))KeyValuePair_2_get_Key_m2225619841_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3360419590_gshared (KeyValuePair_2_t1979316409 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3360419590(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1979316409 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m3360419590_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>::get_Value()
extern "C"  Nullable_1_t334943763  KeyValuePair_2_get_Value_m2361283873_gshared (KeyValuePair_2_t1979316409 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2361283873(__this, method) ((  Nullable_1_t334943763  (*) (KeyValuePair_2_t1979316409 *, const MethodInfo*))KeyValuePair_2_get_Value_m2361283873_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m4140760758_gshared (KeyValuePair_2_t1979316409 * __this, Nullable_1_t334943763  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m4140760758(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1979316409 *, Nullable_1_t334943763 , const MethodInfo*))KeyValuePair_2_set_Value_m4140760758_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Int32>>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2017689366_gshared (KeyValuePair_2_t1979316409 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2017689366(__this, method) ((  String_t* (*) (KeyValuePair_2_t1979316409 *, const MethodInfo*))KeyValuePair_2_ToString_m2017689366_gshared)(__this, method)
