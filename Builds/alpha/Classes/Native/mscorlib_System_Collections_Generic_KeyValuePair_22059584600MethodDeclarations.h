﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.View.QueryParams,Firebase.Database.Internal.Core.View.View>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3086542350(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2059584600 *, QueryParams_t526937568 *, View_t798282663 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.View.QueryParams,Firebase.Database.Internal.Core.View.View>::get_Key()
#define KeyValuePair_2_get_Key_m2573496411(__this, method) ((  QueryParams_t526937568 * (*) (KeyValuePair_2_t2059584600 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.View.QueryParams,Firebase.Database.Internal.Core.View.View>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3938696779(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2059584600 *, QueryParams_t526937568 *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.View.QueryParams,Firebase.Database.Internal.Core.View.View>::get_Value()
#define KeyValuePair_2_get_Value_m1629354590(__this, method) ((  View_t798282663 * (*) (KeyValuePair_2_t2059584600 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.View.QueryParams,Firebase.Database.Internal.Core.View.View>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1661885219(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2059584600 *, View_t798282663 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.View.QueryParams,Firebase.Database.Internal.Core.View.View>::ToString()
#define KeyValuePair_2_ToString_m2131153805(__this, method) ((  String_t* (*) (KeyValuePair_2_t2059584600 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
