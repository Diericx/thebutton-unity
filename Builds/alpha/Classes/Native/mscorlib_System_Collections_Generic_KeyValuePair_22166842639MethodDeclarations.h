﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityTest.ITestComponent,System.Collections.Generic.HashSet`1<UnityTest.ITestComponent>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m4000610601(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2166842639 *, Il2CppObject *, HashSet_1_t1254222372 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityTest.ITestComponent,System.Collections.Generic.HashSet`1<UnityTest.ITestComponent>>::get_Key()
#define KeyValuePair_2_get_Key_m974448619(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2166842639 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityTest.ITestComponent,System.Collections.Generic.HashSet`1<UnityTest.ITestComponent>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m870758334(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2166842639 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityTest.ITestComponent,System.Collections.Generic.HashSet`1<UnityTest.ITestComponent>>::get_Value()
#define KeyValuePair_2_get_Value_m1716756795(__this, method) ((  HashSet_1_t1254222372 * (*) (KeyValuePair_2_t2166842639 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityTest.ITestComponent,System.Collections.Generic.HashSet`1<UnityTest.ITestComponent>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m819437006(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2166842639 *, HashSet_1_t1254222372 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityTest.ITestComponent,System.Collections.Generic.HashSet`1<UnityTest.ITestComponent>>::ToString()
#define KeyValuePair_2_ToString_m592241730(__this, method) ((  String_t* (*) (KeyValuePair_2_t2166842639 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
