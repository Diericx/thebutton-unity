﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_888819835MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,Firebase.FirebaseApp>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2740270120(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2705045562 *, IntPtr_t, FirebaseApp_t210707726 *, const MethodInfo*))KeyValuePair_2__ctor_m477426041_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.IntPtr,Firebase.FirebaseApp>::get_Key()
#define KeyValuePair_2_get_Key_m1316089970(__this, method) ((  IntPtr_t (*) (KeyValuePair_2_t2705045562 *, const MethodInfo*))KeyValuePair_2_get_Key_m1574332879_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,Firebase.FirebaseApp>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m4220891081(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2705045562 *, IntPtr_t, const MethodInfo*))KeyValuePair_2_set_Key_m4146602710_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.IntPtr,Firebase.FirebaseApp>::get_Value()
#define KeyValuePair_2_get_Value_m3742201178(__this, method) ((  FirebaseApp_t210707726 * (*) (KeyValuePair_2_t2705045562 *, const MethodInfo*))KeyValuePair_2_get_Value_m544293807_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,Firebase.FirebaseApp>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m4016041313(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2705045562 *, FirebaseApp_t210707726 *, const MethodInfo*))KeyValuePair_2_set_Value_m1938889438_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.IntPtr,Firebase.FirebaseApp>::ToString()
#define KeyValuePair_2_ToString_m3334037271(__this, method) ((  String_t* (*) (KeyValuePair_2_t2705045562 *, const MethodInfo*))KeyValuePair_2_ToString_m2882821022_gshared)(__this, method)
