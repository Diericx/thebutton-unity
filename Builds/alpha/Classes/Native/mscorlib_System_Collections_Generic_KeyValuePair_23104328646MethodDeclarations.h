﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23104328646.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_CurrentRequ3250227986.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3851400962_gshared (KeyValuePair_2_t3104328646 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3851400962(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3104328646 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m3851400962_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m266456220_gshared (KeyValuePair_2_t3104328646 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m266456220(__this, method) ((  int32_t (*) (KeyValuePair_2_t3104328646 *, const MethodInfo*))KeyValuePair_2_get_Key_m266456220_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3491359045_gshared (KeyValuePair_2_t3104328646 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3491359045(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3104328646 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3491359045_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1416610908_gshared (KeyValuePair_2_t3104328646 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1416610908(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3104328646 *, const MethodInfo*))KeyValuePair_2_get_Value_m1416610908_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1564234917_gshared (KeyValuePair_2_t3104328646 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1564234917(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3104328646 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m1564234917_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.CurrentRequestType,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m582161815_gshared (KeyValuePair_2_t3104328646 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m582161815(__this, method) ((  String_t* (*) (KeyValuePair_2_t3104328646 *, const MethodInfo*))KeyValuePair_2_ToString_m582161815_gshared)(__this, method)
