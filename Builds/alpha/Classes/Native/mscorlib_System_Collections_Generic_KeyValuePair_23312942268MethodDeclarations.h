﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.CompoundWrite>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m157467455(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3312942268 *, ChildKey_t1197802383 *, CompoundWrite_t496419158 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.CompoundWrite>::get_Key()
#define KeyValuePair_2_get_Key_m1087030204(__this, method) ((  ChildKey_t1197802383 * (*) (KeyValuePair_2_t3312942268 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.CompoundWrite>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m502994388(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3312942268 *, ChildKey_t1197802383 *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.CompoundWrite>::get_Value()
#define KeyValuePair_2_get_Value_m37149527(__this, method) ((  CompoundWrite_t496419158 * (*) (KeyValuePair_2_t3312942268 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.CompoundWrite>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2955351412(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3312942268 *, CompoundWrite_t496419158 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.CompoundWrite>::ToString()
#define KeyValuePair_2_ToString_m107522434(__this, method) ((  String_t* (*) (KeyValuePair_2_t3312942268 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
