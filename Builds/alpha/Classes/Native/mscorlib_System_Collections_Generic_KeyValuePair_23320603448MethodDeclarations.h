﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.SparseSnapshotTree>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m955525195(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3320603448 *, ChildKey_t1197802383 *, SparseSnapshotTree_t504080338 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.SparseSnapshotTree>::get_Key()
#define KeyValuePair_2_get_Key_m1528568510(__this, method) ((  ChildKey_t1197802383 * (*) (KeyValuePair_2_t3320603448 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.SparseSnapshotTree>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3965811876(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3320603448 *, ChildKey_t1197802383 *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.SparseSnapshotTree>::get_Value()
#define KeyValuePair_2_get_Value_m132186147(__this, method) ((  SparseSnapshotTree_t504080338 * (*) (KeyValuePair_2_t3320603448 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.SparseSnapshotTree>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1428901732(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3320603448 *, SparseSnapshotTree_t504080338 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Snapshot.ChildKey,Firebase.Database.Internal.Core.SparseSnapshotTree>::ToString()
#define KeyValuePair_2_ToString_m4256793968(__this, method) ((  String_t* (*) (KeyValuePair_2_t3320603448 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
