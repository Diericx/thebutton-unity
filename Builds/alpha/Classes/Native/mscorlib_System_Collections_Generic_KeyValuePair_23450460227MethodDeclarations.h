﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23733013679MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3805376634(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3450460227 *, Path_t2568473163 *, Nullable_1_t2088641033 , const MethodInfo*))KeyValuePair_2__ctor_m132962385_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>::get_Key()
#define KeyValuePair_2_get_Key_m4262956231(__this, method) ((  Path_t2568473163 * (*) (KeyValuePair_2_t3450460227 *, const MethodInfo*))KeyValuePair_2_get_Key_m2110315743_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3110034735(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3450460227 *, Path_t2568473163 *, const MethodInfo*))KeyValuePair_2_set_Key_m2138772518_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>::get_Value()
#define KeyValuePair_2_get_Value_m1109091056(__this, method) ((  Nullable_1_t2088641033  (*) (KeyValuePair_2_t3450460227 *, const MethodInfo*))KeyValuePair_2_get_Value_m2343278975_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2345150463(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3450460227 *, Nullable_1_t2088641033 , const MethodInfo*))KeyValuePair_2_set_Value_m194990542_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>::ToString()
#define KeyValuePair_2_ToString_m3383949865(__this, method) ((  String_t* (*) (KeyValuePair_2_t3450460227 *, const MethodInfo*))KeyValuePair_2_ToString_m1503965782_gshared)(__this, method)
