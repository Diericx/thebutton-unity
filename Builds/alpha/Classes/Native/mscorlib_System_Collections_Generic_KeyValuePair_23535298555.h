﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PubNubMessaging.Core.ChannelParameters
struct ChannelParameters_t547936593;

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_PubNubMessaging_Core_ChannelIden1147162267.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>
struct  KeyValuePair_2_t3535298555 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	ChannelIdentity_t1147162267  ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	ChannelParameters_t547936593 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3535298555, ___key_0)); }
	inline ChannelIdentity_t1147162267  get_key_0() const { return ___key_0; }
	inline ChannelIdentity_t1147162267 * get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(ChannelIdentity_t1147162267  value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3535298555, ___value_1)); }
	inline ChannelParameters_t547936593 * get_value_1() const { return ___value_1; }
	inline ChannelParameters_t547936593 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(ChannelParameters_t547936593 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
