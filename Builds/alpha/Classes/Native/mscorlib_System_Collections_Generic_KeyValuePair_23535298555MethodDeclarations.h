﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21381843961MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m4243977501(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3535298555 *, ChannelIdentity_t1147162267 , ChannelParameters_t547936593 *, const MethodInfo*))KeyValuePair_2__ctor_m623995145_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::get_Key()
#define KeyValuePair_2_get_Key_m1403758392(__this, method) ((  ChannelIdentity_t1147162267  (*) (KeyValuePair_2_t3535298555 *, const MethodInfo*))KeyValuePair_2_get_Key_m359652951_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1387600050(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3535298555 *, ChannelIdentity_t1147162267 , const MethodInfo*))KeyValuePair_2_set_Key_m346790296_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::get_Value()
#define KeyValuePair_2_get_Value_m2870212979(__this, method) ((  ChannelParameters_t547936593 * (*) (KeyValuePair_2_t3535298555 *, const MethodInfo*))KeyValuePair_2_get_Value_m2841136671_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2937140354(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3535298555 *, ChannelParameters_t547936593 *, const MethodInfo*))KeyValuePair_2_set_Value_m2410839120_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<PubNubMessaging.Core.ChannelIdentity,PubNubMessaging.Core.ChannelParameters>::ToString()
#define KeyValuePair_2_ToString_m3995973218(__this, method) ((  String_t* (*) (KeyValuePair_2_t3535298555 *, const MethodInfo*))KeyValuePair_2_ToString_m3003107526_gshared)(__this, method)
