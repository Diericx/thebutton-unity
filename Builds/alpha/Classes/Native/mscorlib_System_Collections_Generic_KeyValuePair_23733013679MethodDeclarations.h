﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23733013679.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m132962385_gshared (KeyValuePair_2_t3733013679 * __this, Il2CppObject * ___key0, Nullable_1_t2088641033  ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m132962385(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3733013679 *, Il2CppObject *, Nullable_1_t2088641033 , const MethodInfo*))KeyValuePair_2__ctor_m132962385_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2110315743_gshared (KeyValuePair_2_t3733013679 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2110315743(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3733013679 *, const MethodInfo*))KeyValuePair_2_get_Key_m2110315743_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2138772518_gshared (KeyValuePair_2_t3733013679 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m2138772518(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3733013679 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m2138772518_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>::get_Value()
extern "C"  Nullable_1_t2088641033  KeyValuePair_2_get_Value_m2343278975_gshared (KeyValuePair_2_t3733013679 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2343278975(__this, method) ((  Nullable_1_t2088641033  (*) (KeyValuePair_2_t3733013679 *, const MethodInfo*))KeyValuePair_2_get_Value_m2343278975_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m194990542_gshared (KeyValuePair_2_t3733013679 * __this, Nullable_1_t2088641033  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m194990542(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3733013679 *, Nullable_1_t2088641033 , const MethodInfo*))KeyValuePair_2_set_Value_m194990542_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1503965782_gshared (KeyValuePair_2_t3733013679 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1503965782(__this, method) ((  String_t* (*) (KeyValuePair_2_t3733013679 *, const MethodInfo*))KeyValuePair_2_ToString_m1503965782_gshared)(__this, method)
