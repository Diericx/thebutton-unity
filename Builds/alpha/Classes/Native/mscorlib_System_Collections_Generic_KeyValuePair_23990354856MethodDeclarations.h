﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3623280436(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3990354856 *, ListenQuerySpec_t2050960365 *, OutstandingListen_t67058648 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen>::get_Key()
#define KeyValuePair_2_get_Key_m2948899089(__this, method) ((  ListenQuerySpec_t2050960365 * (*) (KeyValuePair_2_t3990354856 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3718911969(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3990354856 *, ListenQuerySpec_t2050960365 *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen>::get_Value()
#define KeyValuePair_2_get_Value_m983402556(__this, method) ((  OutstandingListen_t67058648 * (*) (KeyValuePair_2_t3990354856 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3412866641(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3990354856 *, OutstandingListen_t67058648 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Connection.PersistentConnectionImpl/ListenQuerySpec,Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingListen>::ToString()
#define KeyValuePair_2_ToString_m3558811611(__this, method) ((  String_t* (*) (KeyValuePair_2_t3990354856 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
