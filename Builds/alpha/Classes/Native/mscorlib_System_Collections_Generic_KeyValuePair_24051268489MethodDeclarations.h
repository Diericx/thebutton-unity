﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m21461360(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4051268489 *, Path_t2568473163 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m921617746(__this, method) ((  Path_t2568473163 * (*) (KeyValuePair_2_t4051268489 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Object>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3170843059(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4051268489 *, Path_t2568473163 *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m350727234(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t4051268489 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Object>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3678315923(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4051268489 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Object>::ToString()
#define KeyValuePair_2_ToString_m2155614785(__this, method) ((  String_t* (*) (KeyValuePair_2_t4051268489 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
