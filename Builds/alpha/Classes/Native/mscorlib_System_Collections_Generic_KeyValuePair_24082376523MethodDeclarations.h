﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.SyncPoint>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1037091847(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4082376523 *, Path_t2568473163 *, SyncPoint_t2720557329 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.SyncPoint>::get_Key()
#define KeyValuePair_2_get_Key_m3279776285(__this, method) ((  Path_t2568473163 * (*) (KeyValuePair_2_t4082376523 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.SyncPoint>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3633307444(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4082376523 *, Path_t2568473163 *, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.SyncPoint>::get_Value()
#define KeyValuePair_2_get_Value_m740721741(__this, method) ((  SyncPoint_t2720557329 * (*) (KeyValuePair_2_t4082376523 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.SyncPoint>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2781472980(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4082376523 *, SyncPoint_t2720557329 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,Firebase.Database.Internal.Core.SyncPoint>::ToString()
#define KeyValuePair_2_ToString_m4044239210(__this, method) ((  String_t* (*) (KeyValuePair_2_t4082376523 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
