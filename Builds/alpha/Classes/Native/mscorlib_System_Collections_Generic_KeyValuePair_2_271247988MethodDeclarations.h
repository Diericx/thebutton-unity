﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_271247988.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m649163880_gshared (KeyValuePair_2_t271247988 * __this, IntPtr_t ___key0, int32_t ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m649163880(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t271247988 *, IntPtr_t, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m649163880_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>::get_Key()
extern "C"  IntPtr_t KeyValuePair_2_get_Key_m3024592206_gshared (KeyValuePair_2_t271247988 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m3024592206(__this, method) ((  IntPtr_t (*) (KeyValuePair_2_t271247988 *, const MethodInfo*))KeyValuePair_2_get_Key_m3024592206_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m641311851_gshared (KeyValuePair_2_t271247988 * __this, IntPtr_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m641311851(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t271247988 *, IntPtr_t, const MethodInfo*))KeyValuePair_2_set_Key_m641311851_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m2072891806_gshared (KeyValuePair_2_t271247988 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2072891806(__this, method) ((  int32_t (*) (KeyValuePair_2_t271247988 *, const MethodInfo*))KeyValuePair_2_get_Value_m2072891806_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1650401931_gshared (KeyValuePair_2_t271247988 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1650401931(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t271247988 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m1650401931_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.IntPtr,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m362466773_gshared (KeyValuePair_2_t271247988 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m362466773(__this, method) ((  String_t* (*) (KeyValuePair_2_t271247988 *, const MethodInfo*))KeyValuePair_2_ToString_m362466773_gshared)(__this, method)
