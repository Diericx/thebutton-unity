﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22553450683MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2649562217(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t581202521 *, String_t*, int64_t, const MethodInfo*))KeyValuePair_2__ctor_m2353880237_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::get_Key()
#define KeyValuePair_2_get_Key_m649853915(__this, method) ((  String_t* (*) (KeyValuePair_2_t581202521 *, const MethodInfo*))KeyValuePair_2_get_Key_m289678647_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m121231744(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t581202521 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m2592036086_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::get_Value()
#define KeyValuePair_2_get_Value_m2256717611(__this, method) ((  int64_t (*) (KeyValuePair_2_t581202521 *, const MethodInfo*))KeyValuePair_2_get_Value_m2459084999_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3353625664(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t581202521 *, int64_t, const MethodInfo*))KeyValuePair_2_set_Value_m99955926_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::ToString()
#define KeyValuePair_2_ToString_m589274058(__this, method) ((  String_t* (*) (KeyValuePair_2_t581202521 *, const MethodInfo*))KeyValuePair_2_ToString_m2938889728_gshared)(__this, method)
