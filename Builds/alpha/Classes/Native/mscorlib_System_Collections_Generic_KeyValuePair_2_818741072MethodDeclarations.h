﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_818741072.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1716701620_gshared (KeyValuePair_2_t818741072 * __this, int64_t ___key0, int32_t ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1716701620(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t818741072 *, int64_t, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m1716701620_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>::get_Key()
extern "C"  int64_t KeyValuePair_2_get_Key_m4268935818_gshared (KeyValuePair_2_t818741072 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m4268935818(__this, method) ((  int64_t (*) (KeyValuePair_2_t818741072 *, const MethodInfo*))KeyValuePair_2_get_Key_m4268935818_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2118445715_gshared (KeyValuePair_2_t818741072 * __this, int64_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m2118445715(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t818741072 *, int64_t, const MethodInfo*))KeyValuePair_2_set_Key_m2118445715_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3901703242_gshared (KeyValuePair_2_t818741072 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m3901703242(__this, method) ((  int32_t (*) (KeyValuePair_2_t818741072 *, const MethodInfo*))KeyValuePair_2_get_Value_m3901703242_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3426946603_gshared (KeyValuePair_2_t818741072 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m3426946603(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t818741072 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m3426946603_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int64,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1399338921_gshared (KeyValuePair_2_t818741072 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1399338921(__this, method) ((  String_t* (*) (KeyValuePair_2_t818741072 *, const MethodInfo*))KeyValuePair_2_ToString_m1399338921_gshared)(__this, method)
