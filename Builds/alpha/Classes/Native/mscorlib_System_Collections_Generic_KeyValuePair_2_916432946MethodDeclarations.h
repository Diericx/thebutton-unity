﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Firebase.Database.Internal.Core.Repo>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m110300351(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t916432946 *, String_t*, Repo_t1244308462 *, const MethodInfo*))KeyValuePair_2__ctor_m1640124561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Firebase.Database.Internal.Core.Repo>::get_Key()
#define KeyValuePair_2_get_Key_m179943221(__this, method) ((  String_t* (*) (KeyValuePair_2_t916432946 *, const MethodInfo*))KeyValuePair_2_get_Key_m2561166459_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Firebase.Database.Internal.Core.Repo>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1213672434(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t916432946 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m744486900_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Firebase.Database.Internal.Core.Repo>::get_Value()
#define KeyValuePair_2_get_Value_m3725236093(__this, method) ((  Repo_t1244308462 * (*) (KeyValuePair_2_t916432946 *, const MethodInfo*))KeyValuePair_2_get_Value_m499643803_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Firebase.Database.Internal.Core.Repo>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m144275082(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t916432946 *, Repo_t1244308462 *, const MethodInfo*))KeyValuePair_2_set_Value_m1416408204_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Firebase.Database.Internal.Core.Repo>::ToString()
#define KeyValuePair_2_ToString_m2058710656(__this, method) ((  String_t* (*) (KeyValuePair_2_t916432946 *, const MethodInfo*))KeyValuePair_2_ToString_m2613351884_gshared)(__this, method)
