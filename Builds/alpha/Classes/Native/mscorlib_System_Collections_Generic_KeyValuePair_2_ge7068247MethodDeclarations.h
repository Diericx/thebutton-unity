﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21979316409MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Nullable`1<System.Int32>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2609092363(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t7068247 *, String_t*, Nullable_1_t334943763 , const MethodInfo*))KeyValuePair_2__ctor_m848228575_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Nullable`1<System.Int32>>::get_Key()
#define KeyValuePair_2_get_Key_m585013269(__this, method) ((  String_t* (*) (KeyValuePair_2_t7068247 *, const MethodInfo*))KeyValuePair_2_get_Key_m2225619841_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Nullable`1<System.Int32>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m758628044(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t7068247 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m3360419590_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Nullable`1<System.Int32>>::get_Value()
#define KeyValuePair_2_get_Value_m3474594741(__this, method) ((  Nullable_1_t334943763  (*) (KeyValuePair_2_t7068247 *, const MethodInfo*))KeyValuePair_2_get_Value_m2361283873_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Nullable`1<System.Int32>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2295646524(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t7068247 *, Nullable_1_t334943763 , const MethodInfo*))KeyValuePair_2_set_Value_m4140760758_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Nullable`1<System.Int32>>::ToString()
#define KeyValuePair_2_ToString_m3145477040(__this, method) ((  String_t* (*) (KeyValuePair_2_t7068247 *, const MethodInfo*))KeyValuePair_2_ToString_m2017689366_gshared)(__this, method)
