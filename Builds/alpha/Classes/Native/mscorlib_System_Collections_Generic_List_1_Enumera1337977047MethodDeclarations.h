﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Snapshot.RangeMerge>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m4039419895(__this, ___l0, method) ((  void (*) (Enumerator_t1337977047 *, List_1_t1803247373 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Snapshot.RangeMerge>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2627016699(__this, method) ((  void (*) (Enumerator_t1337977047 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Snapshot.RangeMerge>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1462706787(__this, method) ((  Il2CppObject * (*) (Enumerator_t1337977047 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Snapshot.RangeMerge>::Dispose()
#define Enumerator_Dispose_m1668331840(__this, method) ((  void (*) (Enumerator_t1337977047 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Snapshot.RangeMerge>::VerifyState()
#define Enumerator_VerifyState_m2901608921(__this, method) ((  void (*) (Enumerator_t1337977047 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Snapshot.RangeMerge>::MoveNext()
#define Enumerator_MoveNext_m1025163903(__this, method) ((  bool (*) (Enumerator_t1337977047 *, const MethodInfo*))Enumerator_MoveNext_m3604048390_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Snapshot.RangeMerge>::get_Current()
#define Enumerator_get_Current_m4199777548(__this, method) ((  RangeMerge_t2434126241 * (*) (Enumerator_t1337977047 *, const MethodInfo*))Enumerator_get_Current_m4242858252_gshared)(__this, method)
