﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityTest.TestComponent>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m4198460449(__this, ___l0, method) ((  void (*) (Enumerator_t1420362407 *, List_1_t1885632733 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityTest.TestComponent>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1307160369(__this, method) ((  void (*) (Enumerator_t1420362407 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityTest.TestComponent>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m623118385(__this, method) ((  Il2CppObject * (*) (Enumerator_t1420362407 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityTest.TestComponent>::Dispose()
#define Enumerator_Dispose_m2189221652(__this, method) ((  void (*) (Enumerator_t1420362407 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityTest.TestComponent>::VerifyState()
#define Enumerator_VerifyState_m4241478227(__this, method) ((  void (*) (Enumerator_t1420362407 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityTest.TestComponent>::MoveNext()
#define Enumerator_MoveNext_m4102372260(__this, method) ((  bool (*) (Enumerator_t1420362407 *, const MethodInfo*))Enumerator_MoveNext_m3604048390_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityTest.TestComponent>::get_Current()
#define Enumerator_get_Current_m3211768222(__this, method) ((  TestComponent_t2516511601 * (*) (Enumerator_t1420362407 *, const MethodInfo*))Enumerator_get_Current_m4242858252_gshared)(__this, method)
