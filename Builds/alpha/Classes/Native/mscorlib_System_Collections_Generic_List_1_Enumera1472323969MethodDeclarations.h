﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.Path>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2030425396(__this, ___l0, method) ((  void (*) (Enumerator_t1472323969 *, List_1_t1937594295 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.Path>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2659696422(__this, method) ((  void (*) (Enumerator_t1472323969 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.Path>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m173284024(__this, method) ((  Il2CppObject * (*) (Enumerator_t1472323969 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.Path>::Dispose()
#define Enumerator_Dispose_m3755260235(__this, method) ((  void (*) (Enumerator_t1472323969 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.Path>::VerifyState()
#define Enumerator_VerifyState_m4204659362(__this, method) ((  void (*) (Enumerator_t1472323969 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.Path>::MoveNext()
#define Enumerator_MoveNext_m3941812238(__this, method) ((  bool (*) (Enumerator_t1472323969 *, const MethodInfo*))Enumerator_MoveNext_m3604048390_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.Path>::get_Current()
#define Enumerator_get_Current_m2043855295(__this, method) ((  Path_t2568473163 * (*) (Enumerator_t1472323969 *, const MethodInfo*))Enumerator_get_Current_m4242858252_gshared)(__this, method)
