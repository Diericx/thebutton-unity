﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingDisconnect>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m247569524(__this, ___l0, method) ((  void (*) (Enumerator_t1921614159 *, List_1_t2386884485 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingDisconnect>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2223820174(__this, method) ((  void (*) (Enumerator_t1921614159 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingDisconnect>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4071863760(__this, method) ((  Il2CppObject * (*) (Enumerator_t1921614159 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingDisconnect>::Dispose()
#define Enumerator_Dispose_m4195182605(__this, method) ((  void (*) (Enumerator_t1921614159 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingDisconnect>::VerifyState()
#define Enumerator_VerifyState_m3219759574(__this, method) ((  void (*) (Enumerator_t1921614159 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingDisconnect>::MoveNext()
#define Enumerator_MoveNext_m1240145194(__this, method) ((  bool (*) (Enumerator_t1921614159 *, const MethodInfo*))Enumerator_MoveNext_m3604048390_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Connection.PersistentConnectionImpl/OutstandingDisconnect>::get_Current()
#define Enumerator_get_Current_m3678052597(__this, method) ((  OutstandingDisconnect_t3017763353 * (*) (Enumerator_t1921614159 *, const MethodInfo*))Enumerator_get_Current_m4242858252_gshared)(__this, method)
