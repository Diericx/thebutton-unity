﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2636864485MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3615860005(__this, ___l0, method) ((  void (*) (Enumerator_t2354311033 *, List_1_t2819581359 *, const MethodInfo*))Enumerator__ctor_m409392040_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3497363189(__this, method) ((  void (*) (Enumerator_t2354311033 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3973999194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3242918261(__this, method) ((  Il2CppObject * (*) (Enumerator_t2354311033 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3799314788_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>>::Dispose()
#define Enumerator_Dispose_m3537087858(__this, method) ((  void (*) (Enumerator_t2354311033 *, const MethodInfo*))Enumerator_Dispose_m3784523327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>>::VerifyState()
#define Enumerator_VerifyState_m3379572371(__this, method) ((  void (*) (Enumerator_t2354311033 *, const MethodInfo*))Enumerator_VerifyState_m2470994862_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>>::MoveNext()
#define Enumerator_MoveNext_m941312253(__this, method) ((  bool (*) (Enumerator_t2354311033 *, const MethodInfo*))Enumerator_MoveNext_m1939373706_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>>::get_Current()
#define Enumerator_get_Current_m2453062832(__this, method) ((  KeyValuePair_2_t3450460227  (*) (Enumerator_t2354311033 *, const MethodInfo*))Enumerator_get_Current_m2358138955_gshared)(__this, method)
