﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>
struct List_1_t3102134811;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2636864485.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23733013679.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m409392040_gshared (Enumerator_t2636864485 * __this, List_1_t3102134811 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m409392040(__this, ___l0, method) ((  void (*) (Enumerator_t2636864485 *, List_1_t3102134811 *, const MethodInfo*))Enumerator__ctor_m409392040_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3973999194_gshared (Enumerator_t2636864485 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3973999194(__this, method) ((  void (*) (Enumerator_t2636864485 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3973999194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3799314788_gshared (Enumerator_t2636864485 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3799314788(__this, method) ((  Il2CppObject * (*) (Enumerator_t2636864485 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3799314788_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::Dispose()
extern "C"  void Enumerator_Dispose_m3784523327_gshared (Enumerator_t2636864485 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3784523327(__this, method) ((  void (*) (Enumerator_t2636864485 *, const MethodInfo*))Enumerator_Dispose_m3784523327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2470994862_gshared (Enumerator_t2636864485 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2470994862(__this, method) ((  void (*) (Enumerator_t2636864485 *, const MethodInfo*))Enumerator_VerifyState_m2470994862_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1939373706_gshared (Enumerator_t2636864485 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1939373706(__this, method) ((  bool (*) (Enumerator_t2636864485 *, const MethodInfo*))Enumerator_MoveNext_m1939373706_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::get_Current()
extern "C"  KeyValuePair_2_t3733013679  Enumerator_get_Current_m2358138955_gshared (Enumerator_t2636864485 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2358138955(__this, method) ((  KeyValuePair_2_t3733013679  (*) (Enumerator_t2636864485 *, const MethodInfo*))Enumerator_get_Current_m2358138955_gshared)(__this, method)
