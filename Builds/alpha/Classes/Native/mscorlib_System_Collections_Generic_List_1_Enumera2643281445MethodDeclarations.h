﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<PubNubMessaging.Core.SubscribeMessage>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m780681811(__this, ___l0, method) ((  void (*) (Enumerator_t2643281445 *, List_1_t3108551771 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PubNubMessaging.Core.SubscribeMessage>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1162064679(__this, method) ((  void (*) (Enumerator_t2643281445 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<PubNubMessaging.Core.SubscribeMessage>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1422273315(__this, method) ((  Il2CppObject * (*) (Enumerator_t2643281445 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PubNubMessaging.Core.SubscribeMessage>::Dispose()
#define Enumerator_Dispose_m2004347654(__this, method) ((  void (*) (Enumerator_t2643281445 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PubNubMessaging.Core.SubscribeMessage>::VerifyState()
#define Enumerator_VerifyState_m1608501285(__this, method) ((  void (*) (Enumerator_t2643281445 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<PubNubMessaging.Core.SubscribeMessage>::MoveNext()
#define Enumerator_MoveNext_m2931864851(__this, method) ((  bool (*) (Enumerator_t2643281445 *, const MethodInfo*))Enumerator_MoveNext_m3604048390_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<PubNubMessaging.Core.SubscribeMessage>::get_Current()
#define Enumerator_get_Current_m4100115272(__this, method) ((  SubscribeMessage_t3739430639 * (*) (Enumerator_t2643281445 *, const MethodInfo*))Enumerator_get_Current_m4242858252_gshared)(__this, method)
