﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Serialization.JsonFx.JsonConverter>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1989818517(__this, ___l0, method) ((  void (*) (Enumerator_t2996273410 *, List_1_t3461543736 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Serialization.JsonFx.JsonConverter>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m738264429(__this, method) ((  void (*) (Enumerator_t2996273410 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.Serialization.JsonFx.JsonConverter>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3218827753(__this, method) ((  Il2CppObject * (*) (Enumerator_t2996273410 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Serialization.JsonFx.JsonConverter>::Dispose()
#define Enumerator_Dispose_m705726876(__this, method) ((  void (*) (Enumerator_t2996273410 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Serialization.JsonFx.JsonConverter>::VerifyState()
#define Enumerator_VerifyState_m3937945791(__this, method) ((  void (*) (Enumerator_t2996273410 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.Serialization.JsonFx.JsonConverter>::MoveNext()
#define Enumerator_MoveNext_m275035685(__this, method) ((  bool (*) (Enumerator_t2996273410 *, const MethodInfo*))Enumerator_MoveNext_m3604048390_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.Serialization.JsonFx.JsonConverter>::get_Current()
#define Enumerator_get_Current_m734198460(__this, method) ((  JsonConverter_t4092422604 * (*) (Enumerator_t2996273410 *, const MethodInfo*))Enumerator_get_Current_m4242858252_gshared)(__this, method)
