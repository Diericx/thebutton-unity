﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityTest.IntegrationTestRunner.ITestRunnerCallback>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2138409586(__this, ___l0, method) ((  void (*) (Enumerator_t3526011514 *, List_1_t3991281840 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityTest.IntegrationTestRunner.ITestRunnerCallback>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1533458464(__this, method) ((  void (*) (Enumerator_t3526011514 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityTest.IntegrationTestRunner.ITestRunnerCallback>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2019236520(__this, method) ((  Il2CppObject * (*) (Enumerator_t3526011514 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityTest.IntegrationTestRunner.ITestRunnerCallback>::Dispose()
#define Enumerator_Dispose_m3838472709(__this, method) ((  void (*) (Enumerator_t3526011514 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityTest.IntegrationTestRunner.ITestRunnerCallback>::VerifyState()
#define Enumerator_VerifyState_m837519596(__this, method) ((  void (*) (Enumerator_t3526011514 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityTest.IntegrationTestRunner.ITestRunnerCallback>::MoveNext()
#define Enumerator_MoveNext_m302127531(__this, method) ((  bool (*) (Enumerator_t3526011514 *, const MethodInfo*))Enumerator_MoveNext_m3604048390_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityTest.IntegrationTestRunner.ITestRunnerCallback>::get_Current()
#define Enumerator_get_Current_m153703127(__this, method) ((  Il2CppObject * (*) (Enumerator_t3526011514 *, const MethodInfo*))Enumerator_get_Current_m4242858252_gshared)(__this, method)
