﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2082619391(__this, ___l0, method) ((  void (*) (Enumerator_t3576376813 *, List_1_t4041647139 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m967685787(__this, method) ((  void (*) (Enumerator_t3576376813 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m304522051(__this, method) ((  Il2CppObject * (*) (Enumerator_t3576376813 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec>::Dispose()
#define Enumerator_Dispose_m3133930438(__this, method) ((  void (*) (Enumerator_t3576376813 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec>::VerifyState()
#define Enumerator_VerifyState_m3885018461(__this, method) ((  void (*) (Enumerator_t3576376813 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec>::MoveNext()
#define Enumerator_MoveNext_m373253679(__this, method) ((  bool (*) (Enumerator_t3576376813 *, const MethodInfo*))Enumerator_MoveNext_m3604048390_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.QuerySpec>::get_Current()
#define Enumerator_get_Current_m1783411146(__this, method) ((  QuerySpec_t377558711 * (*) (Enumerator_t3576376813 *, const MethodInfo*))Enumerator_get_Current_m4242858252_gshared)(__this, method)
