﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.UserWriteRecord>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3464628952(__this, ___l0, method) ((  void (*) (Enumerator_t3587495681 *, List_1_t4052766007 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.UserWriteRecord>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2887223690(__this, method) ((  void (*) (Enumerator_t3587495681 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.UserWriteRecord>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1550216310(__this, method) ((  Il2CppObject * (*) (Enumerator_t3587495681 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.UserWriteRecord>::Dispose()
#define Enumerator_Dispose_m2898584107(__this, method) ((  void (*) (Enumerator_t3587495681 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.UserWriteRecord>::VerifyState()
#define Enumerator_VerifyState_m2596625786(__this, method) ((  void (*) (Enumerator_t3587495681 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.UserWriteRecord>::MoveNext()
#define Enumerator_MoveNext_m2192366026(__this, method) ((  bool (*) (Enumerator_t3587495681 *, const MethodInfo*))Enumerator_MoveNext_m3604048390_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.UserWriteRecord>::get_Current()
#define Enumerator_get_Current_m190621595(__this, method) ((  UserWriteRecord_t388677579 * (*) (Enumerator_t3587495681 *, const MethodInfo*))Enumerator_get_Current_m4242858252_gshared)(__this, method)
