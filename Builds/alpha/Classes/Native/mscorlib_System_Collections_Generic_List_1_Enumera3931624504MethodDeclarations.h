﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.Event>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2035738596(__this, ___l0, method) ((  void (*) (Enumerator_t3931624504 *, List_1_t101927534 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.Event>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3330527446(__this, method) ((  void (*) (Enumerator_t3931624504 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.Event>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m880551400(__this, method) ((  Il2CppObject * (*) (Enumerator_t3931624504 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.Event>::Dispose()
#define Enumerator_Dispose_m3628998525(__this, method) ((  void (*) (Enumerator_t3931624504 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.Event>::VerifyState()
#define Enumerator_VerifyState_m407830866(__this, method) ((  void (*) (Enumerator_t3931624504 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.Event>::MoveNext()
#define Enumerator_MoveNext_m3198466193(__this, method) ((  bool (*) (Enumerator_t3931624504 *, const MethodInfo*))Enumerator_MoveNext_m3604048390_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.Event>::get_Current()
#define Enumerator_get_Current_m909590981(__this, method) ((  Event_t732806402 * (*) (Enumerator_t3931624504 *, const MethodInfo*))Enumerator_get_Current_m4242858252_gshared)(__this, method)
