﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.View>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3556585633(__this, ___l0, method) ((  void (*) (Enumerator_t3997100765 *, List_1_t167403795 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.View>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4257674713(__this, method) ((  void (*) (Enumerator_t3997100765 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.View>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3962181497(__this, method) ((  Il2CppObject * (*) (Enumerator_t3997100765 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.View>::Dispose()
#define Enumerator_Dispose_m1539247710(__this, method) ((  void (*) (Enumerator_t3997100765 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.View>::VerifyState()
#define Enumerator_VerifyState_m2315279251(__this, method) ((  void (*) (Enumerator_t3997100765 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.View>::MoveNext()
#define Enumerator_MoveNext_m3283675273(__this, method) ((  bool (*) (Enumerator_t3997100765 *, const MethodInfo*))Enumerator_MoveNext_m3604048390_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Firebase.Database.Internal.Core.View.View>::get_Current()
#define Enumerator_get_Current_m2696787540(__this, method) ((  View_t798282663 * (*) (Enumerator_t3997100765 *, const MethodInfo*))Enumerator_get_Current_m4242858252_gshared)(__this, method)
