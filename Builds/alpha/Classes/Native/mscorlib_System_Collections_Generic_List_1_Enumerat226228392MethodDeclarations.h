﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Google.Sharpen.Thread>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1222184107(__this, ___l0, method) ((  void (*) (Enumerator_t226228392 *, List_1_t691498718 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Google.Sharpen.Thread>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1923902135(__this, method) ((  void (*) (Enumerator_t226228392 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Google.Sharpen.Thread>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2706886771(__this, method) ((  Il2CppObject * (*) (Enumerator_t226228392 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Google.Sharpen.Thread>::Dispose()
#define Enumerator_Dispose_m754374312(__this, method) ((  void (*) (Enumerator_t226228392 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Google.Sharpen.Thread>::VerifyState()
#define Enumerator_VerifyState_m529648609(__this, method) ((  void (*) (Enumerator_t226228392 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Google.Sharpen.Thread>::MoveNext()
#define Enumerator_MoveNext_m1484295475(__this, method) ((  bool (*) (Enumerator_t226228392 *, const MethodInfo*))Enumerator_MoveNext_m3604048390_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Google.Sharpen.Thread>::get_Current()
#define Enumerator_get_Current_m2523095566(__this, method) ((  Thread_t1322377586 * (*) (Enumerator_t226228392 *, const MethodInfo*))Enumerator_get_Current_m4242858252_gshared)(__this, method)
