﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>
struct List_1_t3102134811;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>
struct IEnumerable_1_t4025140724;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>
struct IEnumerator_1_t1208537506;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>
struct ICollection_1_t390121688;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>
struct ReadOnlyCollection_1_t3918799371;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>[]
struct KeyValuePair_2U5BU5D_t1559396278;
// System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>
struct Predicate_1_t2175983794;
// System.Collections.Generic.IComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>
struct IComparer_1_t1687476801;
// System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>
struct Comparison_1_t699785234;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23733013679.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2636864485.h"

// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::.ctor()
extern "C"  void List_1__ctor_m1657323767_gshared (List_1_t3102134811 * __this, const MethodInfo* method);
#define List_1__ctor_m1657323767(__this, method) ((  void (*) (List_1_t3102134811 *, const MethodInfo*))List_1__ctor_m1657323767_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1231075440_gshared (List_1_t3102134811 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1231075440(__this, ___collection0, method) ((  void (*) (List_1_t3102134811 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1231075440_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m902857370_gshared (List_1_t3102134811 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m902857370(__this, ___capacity0, method) ((  void (*) (List_1_t3102134811 *, int32_t, const MethodInfo*))List_1__ctor_m902857370_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::.cctor()
extern "C"  void List_1__cctor_m3196953274_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3196953274(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3196953274_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2174102047_gshared (List_1_t3102134811 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2174102047(__this, method) ((  Il2CppObject* (*) (List_1_t3102134811 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2174102047_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1493489003_gshared (List_1_t3102134811 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1493489003(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3102134811 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1493489003_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m4163015316_gshared (List_1_t3102134811 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m4163015316(__this, method) ((  Il2CppObject * (*) (List_1_t3102134811 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m4163015316_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2280900763_gshared (List_1_t3102134811 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2280900763(__this, ___item0, method) ((  int32_t (*) (List_1_t3102134811 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2280900763_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1993279107_gshared (List_1_t3102134811 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1993279107(__this, ___item0, method) ((  bool (*) (List_1_t3102134811 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1993279107_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3555423057_gshared (List_1_t3102134811 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m3555423057(__this, ___item0, method) ((  int32_t (*) (List_1_t3102134811 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3555423057_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3389289742_gshared (List_1_t3102134811 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3389289742(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3102134811 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3389289742_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1907591560_gshared (List_1_t3102134811 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1907591560(__this, ___item0, method) ((  void (*) (List_1_t3102134811 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1907591560_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1570933256_gshared (List_1_t3102134811 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1570933256(__this, method) ((  bool (*) (List_1_t3102134811 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1570933256_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3183571395_gshared (List_1_t3102134811 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3183571395(__this, method) ((  bool (*) (List_1_t3102134811 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3183571395_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2538051271_gshared (List_1_t3102134811 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2538051271(__this, method) ((  Il2CppObject * (*) (List_1_t3102134811 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2538051271_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3337993314_gshared (List_1_t3102134811 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3337993314(__this, method) ((  bool (*) (List_1_t3102134811 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3337993314_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1725398735_gshared (List_1_t3102134811 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1725398735(__this, method) ((  bool (*) (List_1_t3102134811 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1725398735_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2035656500_gshared (List_1_t3102134811 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2035656500(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3102134811 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2035656500_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m1580598213_gshared (List_1_t3102134811 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1580598213(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3102134811 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1580598213_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::Add(T)
extern "C"  void List_1_Add_m3239709954_gshared (List_1_t3102134811 * __this, KeyValuePair_2_t3733013679  ___item0, const MethodInfo* method);
#define List_1_Add_m3239709954(__this, ___item0, method) ((  void (*) (List_1_t3102134811 *, KeyValuePair_2_t3733013679 , const MethodInfo*))List_1_Add_m3239709954_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1888936791_gshared (List_1_t3102134811 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1888936791(__this, ___newCount0, method) ((  void (*) (List_1_t3102134811 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1888936791_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m2823179642_gshared (List_1_t3102134811 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m2823179642(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3102134811 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m2823179642_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1549268391_gshared (List_1_t3102134811 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m1549268391(__this, ___collection0, method) ((  void (*) (List_1_t3102134811 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1549268391_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2144121703_gshared (List_1_t3102134811 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2144121703(__this, ___enumerable0, method) ((  void (*) (List_1_t3102134811 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2144121703_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3070180836_gshared (List_1_t3102134811 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3070180836(__this, ___collection0, method) ((  void (*) (List_1_t3102134811 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3070180836_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3918799371 * List_1_AsReadOnly_m3973638919_gshared (List_1_t3102134811 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3973638919(__this, method) ((  ReadOnlyCollection_1_t3918799371 * (*) (List_1_t3102134811 *, const MethodInfo*))List_1_AsReadOnly_m3973638919_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::Clear()
extern "C"  void List_1_Clear_m3519950686_gshared (List_1_t3102134811 * __this, const MethodInfo* method);
#define List_1_Clear_m3519950686(__this, method) ((  void (*) (List_1_t3102134811 *, const MethodInfo*))List_1_Clear_m3519950686_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::Contains(T)
extern "C"  bool List_1_Contains_m256535404_gshared (List_1_t3102134811 * __this, KeyValuePair_2_t3733013679  ___item0, const MethodInfo* method);
#define List_1_Contains_m256535404(__this, ___item0, method) ((  bool (*) (List_1_t3102134811 *, KeyValuePair_2_t3733013679 , const MethodInfo*))List_1_Contains_m256535404_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1938005222_gshared (List_1_t3102134811 * __this, KeyValuePair_2U5BU5D_t1559396278* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m1938005222(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3102134811 *, KeyValuePair_2U5BU5D_t1559396278*, int32_t, const MethodInfo*))List_1_CopyTo_m1938005222_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::Find(System.Predicate`1<T>)
extern "C"  KeyValuePair_2_t3733013679  List_1_Find_m2417419440_gshared (List_1_t3102134811 * __this, Predicate_1_t2175983794 * ___match0, const MethodInfo* method);
#define List_1_Find_m2417419440(__this, ___match0, method) ((  KeyValuePair_2_t3733013679  (*) (List_1_t3102134811 *, Predicate_1_t2175983794 *, const MethodInfo*))List_1_Find_m2417419440_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m2673453475_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2175983794 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m2673453475(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2175983794 *, const MethodInfo*))List_1_CheckMatch_m2673453475_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m635463622_gshared (List_1_t3102134811 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2175983794 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m635463622(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3102134811 *, int32_t, int32_t, Predicate_1_t2175983794 *, const MethodInfo*))List_1_GetIndex_m635463622_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::GetEnumerator()
extern "C"  Enumerator_t2636864485  List_1_GetEnumerator_m2723815991_gshared (List_1_t3102134811 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m2723815991(__this, method) ((  Enumerator_t2636864485  (*) (List_1_t3102134811 *, const MethodInfo*))List_1_GetEnumerator_m2723815991_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2073063016_gshared (List_1_t3102134811 * __this, KeyValuePair_2_t3733013679  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2073063016(__this, ___item0, method) ((  int32_t (*) (List_1_t3102134811 *, KeyValuePair_2_t3733013679 , const MethodInfo*))List_1_IndexOf_m2073063016_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m1803699143_gshared (List_1_t3102134811 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m1803699143(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3102134811 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1803699143_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2383314054_gshared (List_1_t3102134811 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2383314054(__this, ___index0, method) ((  void (*) (List_1_t3102134811 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2383314054_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m1980570409_gshared (List_1_t3102134811 * __this, int32_t ___index0, KeyValuePair_2_t3733013679  ___item1, const MethodInfo* method);
#define List_1_Insert_m1980570409(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3102134811 *, int32_t, KeyValuePair_2_t3733013679 , const MethodInfo*))List_1_Insert_m1980570409_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3918350440_gshared (List_1_t3102134811 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3918350440(__this, ___collection0, method) ((  void (*) (List_1_t3102134811 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3918350440_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::Remove(T)
extern "C"  bool List_1_Remove_m3281065953_gshared (List_1_t3102134811 * __this, KeyValuePair_2_t3733013679  ___item0, const MethodInfo* method);
#define List_1_Remove_m3281065953(__this, ___item0, method) ((  bool (*) (List_1_t3102134811 *, KeyValuePair_2_t3733013679 , const MethodInfo*))List_1_Remove_m3281065953_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m484094555_gshared (List_1_t3102134811 * __this, Predicate_1_t2175983794 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m484094555(__this, ___match0, method) ((  int32_t (*) (List_1_t3102134811 *, Predicate_1_t2175983794 *, const MethodInfo*))List_1_RemoveAll_m484094555_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m232388661_gshared (List_1_t3102134811 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m232388661(__this, ___index0, method) ((  void (*) (List_1_t3102134811 *, int32_t, const MethodInfo*))List_1_RemoveAt_m232388661_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m1957701242_gshared (List_1_t3102134811 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m1957701242(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3102134811 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1957701242_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::Reverse()
extern "C"  void List_1_Reverse_m952758527_gshared (List_1_t3102134811 * __this, const MethodInfo* method);
#define List_1_Reverse_m952758527(__this, method) ((  void (*) (List_1_t3102134811 *, const MethodInfo*))List_1_Reverse_m952758527_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::Sort()
extern "C"  void List_1_Sort_m403961545_gshared (List_1_t3102134811 * __this, const MethodInfo* method);
#define List_1_Sort_m403961545(__this, method) ((  void (*) (List_1_t3102134811 *, const MethodInfo*))List_1_Sort_m403961545_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m4008025813_gshared (List_1_t3102134811 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m4008025813(__this, ___comparer0, method) ((  void (*) (List_1_t3102134811 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m4008025813_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m582040322_gshared (List_1_t3102134811 * __this, Comparison_1_t699785234 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m582040322(__this, ___comparison0, method) ((  void (*) (List_1_t3102134811 *, Comparison_1_t699785234 *, const MethodInfo*))List_1_Sort_m582040322_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::ToArray()
extern "C"  KeyValuePair_2U5BU5D_t1559396278* List_1_ToArray_m1079379708_gshared (List_1_t3102134811 * __this, const MethodInfo* method);
#define List_1_ToArray_m1079379708(__this, method) ((  KeyValuePair_2U5BU5D_t1559396278* (*) (List_1_t3102134811 *, const MethodInfo*))List_1_ToArray_m1079379708_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2324831556_gshared (List_1_t3102134811 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2324831556(__this, method) ((  void (*) (List_1_t3102134811 *, const MethodInfo*))List_1_TrimExcess_m2324831556_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3646655898_gshared (List_1_t3102134811 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m3646655898(__this, method) ((  int32_t (*) (List_1_t3102134811 *, const MethodInfo*))List_1_get_Capacity_m3646655898_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m393945639_gshared (List_1_t3102134811 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m393945639(__this, ___value0, method) ((  void (*) (List_1_t3102134811 *, int32_t, const MethodInfo*))List_1_set_Capacity_m393945639_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::get_Count()
extern "C"  int32_t List_1_get_Count_m3342813947_gshared (List_1_t3102134811 * __this, const MethodInfo* method);
#define List_1_get_Count_m3342813947(__this, method) ((  int32_t (*) (List_1_t3102134811 *, const MethodInfo*))List_1_get_Count_m3342813947_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::get_Item(System.Int32)
extern "C"  KeyValuePair_2_t3733013679  List_1_get_Item_m3118063769_gshared (List_1_t3102134811 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3118063769(__this, ___index0, method) ((  KeyValuePair_2_t3733013679  (*) (List_1_t3102134811 *, int32_t, const MethodInfo*))List_1_get_Item_m3118063769_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1642776100_gshared (List_1_t3102134811 * __this, int32_t ___index0, KeyValuePair_2_t3733013679  ___value1, const MethodInfo* method);
#define List_1_set_Item_m1642776100(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3102134811 *, int32_t, KeyValuePair_2_t3733013679 , const MethodInfo*))List_1_set_Item_m1642776100_gshared)(__this, ___index0, ___value1, method)
