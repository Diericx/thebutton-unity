﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::.ctor()
#define List_1__ctor_m2113363849(__this, method) ((  void (*) (List_1_t3108551771 *, const MethodInfo*))List_1__ctor_m4043684879_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1011307333(__this, ___collection0, method) ((  void (*) (List_1_t3108551771 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m454375187_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::.ctor(System.Int32)
#define List_1__ctor_m2300925215(__this, ___capacity0, method) ((  void (*) (List_1_t3108551771 *, int32_t, const MethodInfo*))List_1__ctor_m2049248404_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::.cctor()
#define List_1__cctor_m879045181(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1221178552(__this, method) ((  Il2CppObject* (*) (List_1_t3108551771 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m3110034882(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3108551771 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2022369827(__this, method) ((  Il2CppObject * (*) (List_1_t3108551771 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m2989198142(__this, ___item0, method) ((  int32_t (*) (List_1_t3108551771 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m2821552128(__this, ___item0, method) ((  bool (*) (List_1_t3108551771 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1186834936(__this, ___item0, method) ((  int32_t (*) (List_1_t3108551771 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m665338611(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3108551771 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m2458417339(__this, ___item0, method) ((  void (*) (List_1_t3108551771 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m49886527(__this, method) ((  bool (*) (List_1_t3108551771 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2675913630(__this, method) ((  bool (*) (List_1_t3108551771 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m4155406242(__this, method) ((  Il2CppObject * (*) (List_1_t3108551771 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m2480243699(__this, method) ((  bool (*) (List_1_t3108551771 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m59231218(__this, method) ((  bool (*) (List_1_t3108551771 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m2891819243(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3108551771 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m3971191516(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3108551771 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::Add(T)
#define List_1_Add_m262519493(__this, ___item0, method) ((  void (*) (List_1_t3108551771 *, SubscribeMessage_t3739430639 *, const MethodInfo*))List_1_Add_m1330780531_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m560764480(__this, ___newCount0, method) ((  void (*) (List_1_t3108551771 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m797292615(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3108551771 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1904903857_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1363536568(__this, ___collection0, method) ((  void (*) (List_1_t3108551771 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m558766648(__this, ___enumerable0, method) ((  void (*) (List_1_t3108551771 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m2653433431(__this, ___collection0, method) ((  void (*) (List_1_t3108551771 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::AsReadOnly()
#define List_1_AsReadOnly_m3432436426(__this, method) ((  ReadOnlyCollection_1_t3925216331 * (*) (List_1_t3108551771 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::Clear()
#define List_1_Clear_m1293733227(__this, method) ((  void (*) (List_1_t3108551771 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::Contains(T)
#define List_1_Contains_m3372766589(__this, ___item0, method) ((  bool (*) (List_1_t3108551771 *, SubscribeMessage_t3739430639 *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m3920776483(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3108551771 *, SubscribeMessageU5BU5D_t1769104502*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::Find(System.Predicate`1<T>)
#define List_1_Find_m3370923845(__this, ___match0, method) ((  SubscribeMessage_t3739430639 * (*) (List_1_t3108551771 *, Predicate_1_t2182400754 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m4031936434(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2182400754 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m1000036009(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3108551771 *, int32_t, int32_t, Predicate_1_t2182400754 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::GetEnumerator()
#define List_1_GetEnumerator_m4050001958(__this, method) ((  Enumerator_t2643281445  (*) (List_1_t3108551771 *, const MethodInfo*))List_1_GetEnumerator_m1812508714_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::IndexOf(T)
#define List_1_IndexOf_m1735566143(__this, ___item0, method) ((  int32_t (*) (List_1_t3108551771 *, SubscribeMessage_t3739430639 *, const MethodInfo*))List_1_IndexOf_m2070479489_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m13064078(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3108551771 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m364727467(__this, ___index0, method) ((  void (*) (List_1_t3108551771 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::Insert(System.Int32,T)
#define List_1_Insert_m2466240936(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3108551771 *, int32_t, SubscribeMessage_t3739430639 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m3885031941(__this, ___collection0, method) ((  void (*) (List_1_t3108551771 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::Remove(T)
#define List_1_Remove_m586347782(__this, ___item0, method) ((  bool (*) (List_1_t3108551771 *, SubscribeMessage_t3739430639 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m3608681270(__this, ___match0, method) ((  int32_t (*) (List_1_t3108551771 *, Predicate_1_t2182400754 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m210136676(__this, ___index0, method) ((  void (*) (List_1_t3108551771 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2873640625_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m977792719(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3108551771 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3877627853_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::Reverse()
#define List_1_Reverse_m2630544600(__this, method) ((  void (*) (List_1_t3108551771 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::Sort()
#define List_1_Sort_m3059996496(__this, method) ((  void (*) (List_1_t3108551771 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m2699773236(__this, ___comparer0, method) ((  void (*) (List_1_t3108551771 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1776685136_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m2542240997(__this, ___comparison0, method) ((  void (*) (List_1_t3108551771 *, Comparison_1_t706202194 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::ToArray()
#define List_1_ToArray_m1841895077(__this, method) ((  SubscribeMessageU5BU5D_t1769104502* (*) (List_1_t3108551771 *, const MethodInfo*))List_1_ToArray_m3072408725_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::TrimExcess()
#define List_1_TrimExcess_m2696799687(__this, method) ((  void (*) (List_1_t3108551771 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::get_Capacity()
#define List_1_get_Capacity_m2312239117(__this, method) ((  int32_t (*) (List_1_t3108551771 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m1064882808(__this, ___value0, method) ((  void (*) (List_1_t3108551771 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::get_Count()
#define List_1_get_Count_m3330296350(__this, method) ((  int32_t (*) (List_1_t3108551771 *, const MethodInfo*))List_1_get_Count_m1065152471_gshared)(__this, method)
// T System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::get_Item(System.Int32)
#define List_1_get_Item_m3254759194(__this, ___index0, method) ((  SubscribeMessage_t3739430639 * (*) (List_1_t3108551771 *, int32_t, const MethodInfo*))List_1_get_Item_m2432474082_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<PubNubMessaging.Core.SubscribeMessage>::set_Item(System.Int32,T)
#define List_1_set_Item_m3610619303(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3108551771 *, int32_t, SubscribeMessage_t3739430639 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
