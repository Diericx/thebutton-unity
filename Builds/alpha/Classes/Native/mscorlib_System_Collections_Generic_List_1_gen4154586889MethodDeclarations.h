﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::.ctor()
#define List_1__ctor_m1434038137(__this, method) ((  void (*) (List_1_t4154586889 *, const MethodInfo*))List_1__ctor_m4043684879_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1906543269(__this, ___collection0, method) ((  void (*) (List_1_t4154586889 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m454375187_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::.ctor(System.Int32)
#define List_1__ctor_m3919634851(__this, ___capacity0, method) ((  void (*) (List_1_t4154586889 *, int32_t, const MethodInfo*))List_1__ctor_m2049248404_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::.cctor()
#define List_1__cctor_m3280460477(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityTest.TestResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1090708920(__this, method) ((  Il2CppObject* (*) (List_1_t4154586889 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1365670378(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4154586889 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityTest.TestResult>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3901940631(__this, method) ((  Il2CppObject * (*) (List_1_t4154586889 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityTest.TestResult>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m3015008454(__this, ___item0, method) ((  int32_t (*) (List_1_t4154586889 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityTest.TestResult>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m3414881064(__this, ___item0, method) ((  bool (*) (List_1_t4154586889 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityTest.TestResult>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1059183096(__this, ___item0, method) ((  int32_t (*) (List_1_t4154586889 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m3140811311(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4154586889 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m2813183383(__this, ___item0, method) ((  void (*) (List_1_t4154586889 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityTest.TestResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2134337987(__this, method) ((  bool (*) (List_1_t4154586889 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityTest.TestResult>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1720751454(__this, method) ((  bool (*) (List_1_t4154586889 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityTest.TestResult>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m1754698064(__this, method) ((  Il2CppObject * (*) (List_1_t4154586889 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityTest.TestResult>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m3288167839(__this, method) ((  bool (*) (List_1_t4154586889 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityTest.TestResult>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m3527856282(__this, method) ((  bool (*) (List_1_t4154586889 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityTest.TestResult>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m4188066147(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t4154586889 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m2658313880(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4154586889 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::Add(T)
#define List_1_Add_m4073745235(__this, ___item0, method) ((  void (*) (List_1_t4154586889 *, TestResult_t490498461 *, const MethodInfo*))List_1_Add_m1330780531_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m307960200(__this, ___newCount0, method) ((  void (*) (List_1_t4154586889 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m2672094395(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t4154586889 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1904903857_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m2715298416(__this, ___collection0, method) ((  void (*) (List_1_t4154586889 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m3840927248(__this, ___enumerable0, method) ((  void (*) (List_1_t4154586889 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m264812283(__this, ___collection0, method) ((  void (*) (List_1_t4154586889 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityTest.TestResult>::AsReadOnly()
#define List_1_AsReadOnly_m631817018(__this, method) ((  ReadOnlyCollection_1_t676284153 * (*) (List_1_t4154586889 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::Clear()
#define List_1_Clear_m3058081847(__this, method) ((  void (*) (List_1_t4154586889 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityTest.TestResult>::Contains(T)
#define List_1_Contains_m1077094557(__this, ___item0, method) ((  bool (*) (List_1_t4154586889 *, TestResult_t490498461 *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m2721613711(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4154586889 *, TestResultU5BU5D_t2362815312*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UnityTest.TestResult>::Find(System.Predicate`1<T>)
#define List_1_Find_m2733562829(__this, ___match0, method) ((  TestResult_t490498461 * (*) (List_1_t4154586889 *, Predicate_1_t3228435872 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m3113912218(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3228435872 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityTest.TestResult>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m764694605(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t4154586889 *, int32_t, int32_t, Predicate_1_t3228435872 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityTest.TestResult>::GetEnumerator()
#define List_1_GetEnumerator_m2382967710(__this, method) ((  Enumerator_t3689316563  (*) (List_1_t4154586889 *, const MethodInfo*))List_1_GetEnumerator_m1812508714_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityTest.TestResult>::IndexOf(T)
#define List_1_IndexOf_m1905051147(__this, ___item0, method) ((  int32_t (*) (List_1_t4154586889 *, TestResult_t490498461 *, const MethodInfo*))List_1_IndexOf_m2070479489_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m881529998(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t4154586889 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m208047191(__this, ___index0, method) ((  void (*) (List_1_t4154586889 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::Insert(System.Int32,T)
#define List_1_Insert_m2626997036(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4154586889 *, int32_t, TestResult_t490498461 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m2544557397(__this, ___collection0, method) ((  void (*) (List_1_t4154586889 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityTest.TestResult>::Remove(T)
#define List_1_Remove_m3091792330(__this, ___item0, method) ((  bool (*) (List_1_t4154586889 *, TestResult_t490498461 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityTest.TestResult>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m1915035086(__this, ___match0, method) ((  int32_t (*) (List_1_t4154586889 *, Predicate_1_t3228435872 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m3674469600(__this, ___index0, method) ((  void (*) (List_1_t4154586889 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2873640625_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m1806279395(__this, ___index0, ___count1, method) ((  void (*) (List_1_t4154586889 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3877627853_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::Reverse()
#define List_1_Reverse_m1094869184(__this, method) ((  void (*) (List_1_t4154586889 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::Sort()
#define List_1_Sort_m3966067540(__this, method) ((  void (*) (List_1_t4154586889 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m3041224736(__this, ___comparer0, method) ((  void (*) (List_1_t4154586889 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1776685136_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m4152185909(__this, ___comparison0, method) ((  void (*) (List_1_t4154586889 *, Comparison_1_t1752237312 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnityTest.TestResult>::ToArray()
#define List_1_ToArray_m2042706553(__this, method) ((  TestResultU5BU5D_t2362815312* (*) (List_1_t4154586889 *, const MethodInfo*))List_1_ToArray_m3072408725_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::TrimExcess()
#define List_1_TrimExcess_m102311483(__this, method) ((  void (*) (List_1_t4154586889 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityTest.TestResult>::get_Capacity()
#define List_1_get_Capacity_m441842513(__this, method) ((  int32_t (*) (List_1_t4154586889 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m3686016400(__this, ___value0, method) ((  void (*) (List_1_t4154586889 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityTest.TestResult>::get_Count()
#define List_1_get_Count_m1033227945(__this, method) ((  int32_t (*) (List_1_t4154586889 *, const MethodInfo*))List_1_get_Count_m1065152471_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityTest.TestResult>::get_Item(System.Int32)
#define List_1_get_Item_m435659972(__this, ___index0, method) ((  TestResult_t490498461 * (*) (List_1_t4154586889 *, int32_t, const MethodInfo*))List_1_get_Item_m2432474082_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityTest.TestResult>::set_Item(System.Int32,T)
#define List_1_set_Item_m2040711259(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4154586889 *, int32_t, TestResult_t490498461 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
