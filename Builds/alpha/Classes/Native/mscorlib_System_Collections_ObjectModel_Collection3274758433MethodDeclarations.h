﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>
struct Collection_1_t3274758433;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>[]
struct KeyValuePair_2U5BU5D_t1559396278;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>
struct IEnumerator_1_t1208537506;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>
struct IList_1_t4273954280;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23733013679.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::.ctor()
extern "C"  void Collection_1__ctor_m1341016490_gshared (Collection_1_t3274758433 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1341016490(__this, method) ((  void (*) (Collection_1_t3274758433 *, const MethodInfo*))Collection_1__ctor_m1341016490_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m814463421_gshared (Collection_1_t3274758433 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m814463421(__this, method) ((  bool (*) (Collection_1_t3274758433 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m814463421_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m4185737098_gshared (Collection_1_t3274758433 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m4185737098(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3274758433 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m4185737098_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m849351653_gshared (Collection_1_t3274758433 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m849351653(__this, method) ((  Il2CppObject * (*) (Collection_1_t3274758433 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m849351653_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1939976046_gshared (Collection_1_t3274758433 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1939976046(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3274758433 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1939976046_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m2765325092_gshared (Collection_1_t3274758433 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m2765325092(__this, ___value0, method) ((  bool (*) (Collection_1_t3274758433 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m2765325092_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3989799584_gshared (Collection_1_t3274758433 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3989799584(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3274758433 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3989799584_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m4146966881_gshared (Collection_1_t3274758433 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m4146966881(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3274758433 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m4146966881_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2223010177_gshared (Collection_1_t3274758433 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2223010177(__this, ___value0, method) ((  void (*) (Collection_1_t3274758433 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2223010177_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m2462701762_gshared (Collection_1_t3274758433 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2462701762(__this, method) ((  bool (*) (Collection_1_t3274758433 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m2462701762_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1214471956_gshared (Collection_1_t3274758433 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1214471956(__this, method) ((  Il2CppObject * (*) (Collection_1_t3274758433 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1214471956_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2024536693_gshared (Collection_1_t3274758433 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2024536693(__this, method) ((  bool (*) (Collection_1_t3274758433 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2024536693_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2703130750_gshared (Collection_1_t3274758433 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2703130750(__this, method) ((  bool (*) (Collection_1_t3274758433 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2703130750_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m1073180273_gshared (Collection_1_t3274758433 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1073180273(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3274758433 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1073180273_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1284522148_gshared (Collection_1_t3274758433 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1284522148(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3274758433 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1284522148_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::Add(T)
extern "C"  void Collection_1_Add_m1890928821_gshared (Collection_1_t3274758433 * __this, KeyValuePair_2_t3733013679  ___item0, const MethodInfo* method);
#define Collection_1_Add_m1890928821(__this, ___item0, method) ((  void (*) (Collection_1_t3274758433 *, KeyValuePair_2_t3733013679 , const MethodInfo*))Collection_1_Add_m1890928821_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::Clear()
extern "C"  void Collection_1_Clear_m3052028521_gshared (Collection_1_t3274758433 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3052028521(__this, method) ((  void (*) (Collection_1_t3274758433 *, const MethodInfo*))Collection_1_Clear_m3052028521_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3658741159_gshared (Collection_1_t3274758433 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3658741159(__this, method) ((  void (*) (Collection_1_t3274758433 *, const MethodInfo*))Collection_1_ClearItems_m3658741159_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::Contains(T)
extern "C"  bool Collection_1_Contains_m46835731_gshared (Collection_1_t3274758433 * __this, KeyValuePair_2_t3733013679  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m46835731(__this, ___item0, method) ((  bool (*) (Collection_1_t3274758433 *, KeyValuePair_2_t3733013679 , const MethodInfo*))Collection_1_Contains_m46835731_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3093310389_gshared (Collection_1_t3274758433 * __this, KeyValuePair_2U5BU5D_t1559396278* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3093310389(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3274758433 *, KeyValuePair_2U5BU5D_t1559396278*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3093310389_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m4127779858_gshared (Collection_1_t3274758433 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m4127779858(__this, method) ((  Il2CppObject* (*) (Collection_1_t3274758433 *, const MethodInfo*))Collection_1_GetEnumerator_m4127779858_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m750485109_gshared (Collection_1_t3274758433 * __this, KeyValuePair_2_t3733013679  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m750485109(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3274758433 *, KeyValuePair_2_t3733013679 , const MethodInfo*))Collection_1_IndexOf_m750485109_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m29744652_gshared (Collection_1_t3274758433 * __this, int32_t ___index0, KeyValuePair_2_t3733013679  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m29744652(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3274758433 *, int32_t, KeyValuePair_2_t3733013679 , const MethodInfo*))Collection_1_Insert_m29744652_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m193645091_gshared (Collection_1_t3274758433 * __this, int32_t ___index0, KeyValuePair_2_t3733013679  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m193645091(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3274758433 *, int32_t, KeyValuePair_2_t3733013679 , const MethodInfo*))Collection_1_InsertItem_m193645091_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::Remove(T)
extern "C"  bool Collection_1_Remove_m2684442194_gshared (Collection_1_t3274758433 * __this, KeyValuePair_2_t3733013679  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2684442194(__this, ___item0, method) ((  bool (*) (Collection_1_t3274758433 *, KeyValuePair_2_t3733013679 , const MethodInfo*))Collection_1_Remove_m2684442194_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3981416320_gshared (Collection_1_t3274758433 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3981416320(__this, ___index0, method) ((  void (*) (Collection_1_t3274758433 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3981416320_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3992348178_gshared (Collection_1_t3274758433 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3992348178(__this, ___index0, method) ((  void (*) (Collection_1_t3274758433 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3992348178_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m165652990_gshared (Collection_1_t3274758433 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m165652990(__this, method) ((  int32_t (*) (Collection_1_t3274758433 *, const MethodInfo*))Collection_1_get_Count_m165652990_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::get_Item(System.Int32)
extern "C"  KeyValuePair_2_t3733013679  Collection_1_get_Item_m2854251892_gshared (Collection_1_t3274758433 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2854251892(__this, ___index0, method) ((  KeyValuePair_2_t3733013679  (*) (Collection_1_t3274758433 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2854251892_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m1483518901_gshared (Collection_1_t3274758433 * __this, int32_t ___index0, KeyValuePair_2_t3733013679  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m1483518901(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3274758433 *, int32_t, KeyValuePair_2_t3733013679 , const MethodInfo*))Collection_1_set_Item_m1483518901_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m3461253636_gshared (Collection_1_t3274758433 * __this, int32_t ___index0, KeyValuePair_2_t3733013679  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m3461253636(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3274758433 *, int32_t, KeyValuePair_2_t3733013679 , const MethodInfo*))Collection_1_SetItem_m3461253636_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m1279009519_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m1279009519(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1279009519_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::ConvertItem(System.Object)
extern "C"  KeyValuePair_2_t3733013679  Collection_1_ConvertItem_m894870011_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m894870011(__this /* static, unused */, ___item0, method) ((  KeyValuePair_2_t3733013679  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m894870011_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m2282507619_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m2282507619(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m2282507619_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3450841579_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3450841579(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3450841579_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3877786176_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3877786176(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3877786176_gshared)(__this /* static, unused */, ___list0, method)
