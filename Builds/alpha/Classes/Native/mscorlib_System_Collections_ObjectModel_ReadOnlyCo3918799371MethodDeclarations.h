﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>
struct ReadOnlyCollection_1_t3918799371;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>
struct IList_1_t4273954280;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>[]
struct KeyValuePair_2U5BU5D_t1559396278;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>
struct IEnumerator_1_t1208537506;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23733013679.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m1726504749_gshared (ReadOnlyCollection_1_t3918799371 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1726504749(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3918799371 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1726504749_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1738387355_gshared (ReadOnlyCollection_1_t3918799371 * __this, KeyValuePair_2_t3733013679  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1738387355(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3918799371 *, KeyValuePair_2_t3733013679 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1738387355_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2237991831_gshared (ReadOnlyCollection_1_t3918799371 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2237991831(__this, method) ((  void (*) (ReadOnlyCollection_1_t3918799371 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2237991831_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4165716542_gshared (ReadOnlyCollection_1_t3918799371 * __this, int32_t ___index0, KeyValuePair_2_t3733013679  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4165716542(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3918799371 *, int32_t, KeyValuePair_2_t3733013679 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m4165716542_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2197133576_gshared (ReadOnlyCollection_1_t3918799371 * __this, KeyValuePair_2_t3733013679  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2197133576(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3918799371 *, KeyValuePair_2_t3733013679 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2197133576_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3409783474_gshared (ReadOnlyCollection_1_t3918799371 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3409783474(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3918799371 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3409783474_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  KeyValuePair_2_t3733013679  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2087809174_gshared (ReadOnlyCollection_1_t3918799371 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2087809174(__this, ___index0, method) ((  KeyValuePair_2_t3733013679  (*) (ReadOnlyCollection_1_t3918799371 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2087809174_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1655884391_gshared (ReadOnlyCollection_1_t3918799371 * __this, int32_t ___index0, KeyValuePair_2_t3733013679  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1655884391(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3918799371 *, int32_t, KeyValuePair_2_t3733013679 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1655884391_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m386558627_gshared (ReadOnlyCollection_1_t3918799371 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m386558627(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3918799371 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m386558627_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2339906132_gshared (ReadOnlyCollection_1_t3918799371 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2339906132(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3918799371 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2339906132_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m600608935_gshared (ReadOnlyCollection_1_t3918799371 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m600608935(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3918799371 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m600608935_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2752153616_gshared (ReadOnlyCollection_1_t3918799371 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2752153616(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3918799371 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m2752153616_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m1760535530_gshared (ReadOnlyCollection_1_t3918799371 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1760535530(__this, method) ((  void (*) (ReadOnlyCollection_1_t3918799371 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1760535530_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2289051750_gshared (ReadOnlyCollection_1_t3918799371 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2289051750(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3918799371 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2289051750_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1418595998_gshared (ReadOnlyCollection_1_t3918799371 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1418595998(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3918799371 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m1418595998_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2806903159_gshared (ReadOnlyCollection_1_t3918799371 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2806903159(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3918799371 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2806903159_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1077822231_gshared (ReadOnlyCollection_1_t3918799371 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1077822231(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3918799371 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1077822231_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2338048869_gshared (ReadOnlyCollection_1_t3918799371 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2338048869(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3918799371 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2338048869_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3394103272_gshared (ReadOnlyCollection_1_t3918799371 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3394103272(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3918799371 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3394103272_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1854479550_gshared (ReadOnlyCollection_1_t3918799371 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1854479550(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3918799371 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1854479550_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1886403415_gshared (ReadOnlyCollection_1_t3918799371 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1886403415(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3918799371 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1886403415_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m402019644_gshared (ReadOnlyCollection_1_t3918799371 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m402019644(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3918799371 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m402019644_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3424965883_gshared (ReadOnlyCollection_1_t3918799371 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3424965883(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3918799371 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3424965883_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2377139078_gshared (ReadOnlyCollection_1_t3918799371 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2377139078(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3918799371 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2377139078_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m4226391313_gshared (ReadOnlyCollection_1_t3918799371 * __this, KeyValuePair_2_t3733013679  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m4226391313(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3918799371 *, KeyValuePair_2_t3733013679 , const MethodInfo*))ReadOnlyCollection_1_Contains_m4226391313_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m698798175_gshared (ReadOnlyCollection_1_t3918799371 * __this, KeyValuePair_2U5BU5D_t1559396278* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m698798175(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3918799371 *, KeyValuePair_2U5BU5D_t1559396278*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m698798175_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m4213173204_gshared (ReadOnlyCollection_1_t3918799371 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m4213173204(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3918799371 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m4213173204_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2639118579_gshared (ReadOnlyCollection_1_t3918799371 * __this, KeyValuePair_2_t3733013679  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2639118579(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3918799371 *, KeyValuePair_2_t3733013679 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2639118579_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m392523136_gshared (ReadOnlyCollection_1_t3918799371 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m392523136(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3918799371 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m392523136_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Nullable`1<System.Boolean>>>::get_Item(System.Int32)
extern "C"  KeyValuePair_2_t3733013679  ReadOnlyCollection_1_get_Item_m2238159530_gshared (ReadOnlyCollection_1_t3918799371 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m2238159530(__this, ___index0, method) ((  KeyValuePair_2_t3733013679  (*) (ReadOnlyCollection_1_t3918799371 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2238159530_gshared)(__this, ___index0, method)
