﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen699785234MethodDeclarations.h"

// System.Void System.Comparison`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m618369151(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t417231782 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m1933667716_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>>::Invoke(T,T)
#define Comparison_1_Invoke_m2897030937(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t417231782 *, KeyValuePair_2_t3450460227 , KeyValuePair_2_t3450460227 , const MethodInfo*))Comparison_1_Invoke_m1335624480_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m2233236662(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t417231782 *, KeyValuePair_2_t3450460227 , KeyValuePair_2_t3450460227 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m4257461483_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m829828483(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t417231782 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m2462477894_gshared)(__this, ___result0, method)
