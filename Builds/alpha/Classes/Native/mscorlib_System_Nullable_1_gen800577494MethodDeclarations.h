﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen800577494.h"
#include "Firebase_Database_Firebase_Database_Internal_Core_2537511179.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Nullable`1<Firebase.Database.Internal.Core.View.QueryParams/ViewFrom>::.ctor(T)
extern "C"  void Nullable_1__ctor_m231406432_gshared (Nullable_1_t800577494 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m231406432(__this, ___value0, method) ((  void (*) (Nullable_1_t800577494 *, int32_t, const MethodInfo*))Nullable_1__ctor_m231406432_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<Firebase.Database.Internal.Core.View.QueryParams/ViewFrom>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m435651625_gshared (Nullable_1_t800577494 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m435651625(__this, method) ((  bool (*) (Nullable_1_t800577494 *, const MethodInfo*))Nullable_1_get_HasValue_m435651625_gshared)(__this, method)
// T System.Nullable`1<Firebase.Database.Internal.Core.View.QueryParams/ViewFrom>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m3991471677_gshared (Nullable_1_t800577494 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m3991471677(__this, method) ((  int32_t (*) (Nullable_1_t800577494 *, const MethodInfo*))Nullable_1_get_Value_m3991471677_gshared)(__this, method)
// System.Boolean System.Nullable`1<Firebase.Database.Internal.Core.View.QueryParams/ViewFrom>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2990705349_gshared (Nullable_1_t800577494 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m2990705349(__this, ___other0, method) ((  bool (*) (Nullable_1_t800577494 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m2990705349_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<Firebase.Database.Internal.Core.View.QueryParams/ViewFrom>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3444749634_gshared (Nullable_1_t800577494 * __this, Nullable_1_t800577494  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m3444749634(__this, ___other0, method) ((  bool (*) (Nullable_1_t800577494 *, Nullable_1_t800577494 , const MethodInfo*))Nullable_1_Equals_m3444749634_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<Firebase.Database.Internal.Core.View.QueryParams/ViewFrom>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m69001007_gshared (Nullable_1_t800577494 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m69001007(__this, method) ((  int32_t (*) (Nullable_1_t800577494 *, const MethodInfo*))Nullable_1_GetHashCode_m69001007_gshared)(__this, method)
// T System.Nullable`1<Firebase.Database.Internal.Core.View.QueryParams/ViewFrom>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3686288486_gshared (Nullable_1_t800577494 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3686288486(__this, method) ((  int32_t (*) (Nullable_1_t800577494 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m3686288486_gshared)(__this, method)
// System.String System.Nullable`1<Firebase.Database.Internal.Core.View.QueryParams/ViewFrom>::ToString()
extern "C"  String_t* Nullable_1_ToString_m1257943123_gshared (Nullable_1_t800577494 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m1257943123(__this, method) ((  String_t* (*) (Nullable_1_t800577494 *, const MethodInfo*))Nullable_1_ToString_m1257943123_gshared)(__this, method)
