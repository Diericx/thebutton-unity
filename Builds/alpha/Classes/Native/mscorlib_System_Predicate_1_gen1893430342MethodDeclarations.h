﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen2175983794MethodDeclarations.h"

// System.Void System.Predicate`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m1517859779(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t1893430342 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m598398576_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>>::Invoke(T)
#define Predicate_1_Invoke_m3496849475(__this, ___obj0, method) ((  bool (*) (Predicate_1_t1893430342 *, KeyValuePair_2_t3450460227 , const MethodInfo*))Predicate_1_Invoke_m3990227616_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m2799353694(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t1893430342 *, KeyValuePair_2_t3450460227 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m1533161615_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<Firebase.Database.Internal.Core.Path,System.Nullable`1<System.Boolean>>>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m186821061(__this, ___result0, method) ((  bool (*) (Predicate_1_t1893430342 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m1467762986_gshared)(__this, ___result0, method)
