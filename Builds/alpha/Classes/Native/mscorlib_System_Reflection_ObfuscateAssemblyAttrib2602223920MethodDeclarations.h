﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.ObfuscateAssemblyAttribute
struct ObfuscateAssemblyAttribute_t2602223920;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Reflection.ObfuscateAssemblyAttribute::.ctor(System.Boolean)
extern "C"  void ObfuscateAssemblyAttribute__ctor_m1594323961 (ObfuscateAssemblyAttribute_t2602223920 * __this, bool ___assemblyIsPrivate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.ObfuscateAssemblyAttribute::set_StripAfterObfuscation(System.Boolean)
extern "C"  void ObfuscateAssemblyAttribute_set_StripAfterObfuscation_m3410718271 (ObfuscateAssemblyAttribute_t2602223920 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
